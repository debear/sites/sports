<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\ParticipantHistory as BaseParticipantHistory;

class ParticipantHistory extends BaseParticipantHistory
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_RIDER_HISTORY';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'rider_id',
    ];

    /**
     * Relationship: 1:1 participant details
     * @return HasOne
     */
    public function participant(): HasOne
    {
        return $this->hasOne(Participant::class, 'rider_id', 'rider_id');
    }
    /**
     * Relationship: 1:M results in a season
     * @return HasMany
     */
    public function races(): HasMany
    {
        return $this->hasMany(RaceParticipant::class, ['season', 'rider_id'], ['season', 'rider_id']);
    }
    /**
     * Relationship: 1:M meetings in a season
     * @return HasMany
     */
    public function calendar(): HasMany
    {
        return $this->hasMany(Race::class, ['season'], ['season']);
    }
    /**
     * Relationship: 1:M historical meetings in a season (pre-detailed data)
     * @return HasMany
     */
    public function racesHistorical(): HasMany
    {
        return $this->hasMany(RaceHistorical::class, ['season', 'rider_id'], ['season', 'rider_id']);
    }
    /**
     * Relationship: 1:M meeting-by-meeting progress that season
     * @return HasMany
     */
    public function progress(): HasMany
    {
        return $this->hasMany(ParticipantHistoryProgress::class, ['season', 'rider_id'], ['season', 'rider_id']);
    }
    /**
     * Relationship: 1:M participant's stats for this season
     * @return HasMany
     */
    public function stats(): HasMany
    {
        return $this->hasMany(ParticipantStats::class, ['season', 'rider_id'], ['season', 'rider_id']);
    }
}
