<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use DeBear\Models\Sports\Motorsport\ParticipantHistoryProgress as BaseParticipantHistoryProgress;

class ParticipantHistoryProgress extends BaseParticipantHistoryProgress
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_RIDER_HISTORY_PROGRESS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'rider_id',
        'round',
    ];
}
