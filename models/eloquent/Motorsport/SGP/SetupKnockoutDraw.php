<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use DeBear\Implementations\Model;

class SetupKnockoutDraw extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_SETUP_KNOCKOUT_DRAW';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'draw_version',
        'main_pos',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
