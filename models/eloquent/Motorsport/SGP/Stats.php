<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use DeBear\Models\Sports\Motorsport\Stats as BaseStats;

class Stats extends BaseStats
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_STATS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'stat_id';
}
