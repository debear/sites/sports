<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Implementations\Model;

class SetupRace extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_SETUP_RACES';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'season_from';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:M knockout draw orders (not in use yet)
     * @return HasMany
     * /
    public function knockoutDraw(): HasMany
    {
        return $this->hasMany(SetupKnockoutDraw::class, 'version', 'draw_version');
    }
    /* */
    /**
     * Relationship: 1:M heat-by-heat gate breakdown
     * @return HasMany
     */
    public function gates(): HasMany
    {
        return $this->hasMany(SetupGate::class, 'version', 'gates_version');
    }
}
