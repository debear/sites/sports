<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\RaceParticipant as BaseRaceParticipant;

class RaceParticipant extends BaseRaceParticipant
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_RACE_RIDERS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'bib_no',
    ];

    /**
     * Relationship: 1:1 meeting details
     * @return HasOne
     */
    public function race(): HasOne
    {
        return $this->hasOne(Race::class, ['season', 'round'], ['season', 'round']);
    }
    /**
     * Relationship: 1:1 participant details
     * @return HasOne
     */
    public function participant(): HasOne
    {
        return $this->hasOne(Participant::class, 'rider_id', 'rider_id');
    }
    /**
     * Relationship: 1:M individual meeting results
     * @return HasMany
     */
    public function result(): HasMany
    {
        return $this->hasMany(RaceResult::class, ['season', 'round', 'bib_no'], ['season', 'round', 'bib_no']);
    }
    /**
     * Relationship: 1:M individual heat results for this participant
     * @return HasMany
     */
    public function heats(): HasMany
    {
        return $this->hasMany(RaceHeats::class, ['season', 'round', 'bib_no'], ['season', 'round', 'bib_no']);
    }
}
