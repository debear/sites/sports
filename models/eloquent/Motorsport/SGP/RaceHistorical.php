<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use DeBear\Models\Sports\Motorsport\RaceHistorical as BaseRaceHistorical;

class RaceHistorical extends BaseRaceHistorical
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_RIDER_GP_HISTORY';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'round',
        'rider_id',
    ];
}
