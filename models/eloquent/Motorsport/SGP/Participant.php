<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\Participant as BaseParticipant;

class Participant extends BaseParticipant
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SPEEDWAY_RIDERS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'rider_id';

    /**
     * Relationship: 1:M season history
     * @return HasMany
     */
    public function history(): HasMany
    {
        return $this->hasMany(ParticipantHistory::class, 'rider_id', 'rider_id');
    }
}
