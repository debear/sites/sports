<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\Race as BaseRace;

class Race extends BaseRace
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_RACES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'round',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'race_time' => 'datetime',
    ];

    /**
     * Relationship: 1:M participants taking part in this meeting
     * @return HasMany
     */
    public function raceParticipants(): HasMany
    {
        return $this->hasMany(RaceParticipant::class, ['season', 'round'], ['season', 'round']);
    }
}
