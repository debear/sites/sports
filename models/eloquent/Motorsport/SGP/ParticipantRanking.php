<?php

namespace DeBear\Models\Sports\Motorsport\SGP;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Implementations\Model;

class ParticipantRanking extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_SGP_RIDER_RANKING';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'ranking',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Relationship: 1:1 participant details
     * @return HasOne
     */
    public function participant(): HasOne
    {
        return $this->hasOne(Participant::class, 'rider_id', 'rider_id');
    }
}
