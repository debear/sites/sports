<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use DeBear\Models\Sports\Motorsport\RaceFastestLap as BaseRaceFastestLap;

class RaceFastestLap extends BaseRaceFastestLap
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_RACE_FASTEST_LAP';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'session',
        'car_no',
    ];
}
