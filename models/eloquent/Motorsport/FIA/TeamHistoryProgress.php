<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use DeBear\Models\Sports\Motorsport\TeamHistoryProgress as BaseTeamHistoryProgress;

class TeamHistoryProgress extends BaseTeamHistoryProgress
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_TEAM_HISTORY_PROGRESS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'team_id',
        'round',
    ];
}
