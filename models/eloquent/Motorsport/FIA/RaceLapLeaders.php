<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use DeBear\Models\Sports\Motorsport\RaceLapLeaders as BaseRaceLapLeaders;

class RaceLapLeaders extends BaseRaceLapLeaders
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_RACE_LEADERS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'race',
        'lap_from',
    ];
}
