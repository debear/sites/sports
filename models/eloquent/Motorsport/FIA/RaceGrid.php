<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\RaceGrid as BaseRaceGrid;

class RaceGrid extends BaseRaceGrid
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_RACE_GRID';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'pos',
    ];

    /**
     * Relationship: 1:M individual qualifying session results
     * @return HasMany
     */
    public function raceQualifying(): HasMany
    {
        return $this->hasMany(
            RaceQuali::class,
            ['season', 'series', 'round', 'car_no'],
            ['season', 'series', 'round', 'car_no']
        );
    }
}
