<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\Race as BaseRace;

class Race extends BaseRace
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_RACES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
    ];

    /**
     * Relationship: 1:M participants taking part in this race
     * @return HasMany
     */
    public function raceParticipants(): HasMany
    {
        return $this->hasMany(RaceParticipant::class, ['season', 'series', 'round'], ['season', 'series', 'round']);
    }
    /**
     * Relationship: 1:M individual lap leaders during this race
     * @return HasMany
     */
    public function lapLeaders(): HasMany
    {
        return $this->hasMany(RaceLapLeaders::class, ['season', 'series', 'round'], ['season', 'series', 'round']);
    }
    /**
     * Relationship: 1:M details of the fastest laps
     * @return HasMany
     */
    public function fastestLap(): HasMany
    {
        return $this->hasMany(RaceFastestLap::class, ['season', 'series', 'round'], ['season', 'series', 'round']);
    }
}
