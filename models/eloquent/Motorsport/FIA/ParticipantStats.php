<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Models\Sports\Motorsport\ParticipantStats as BaseParticipantStats;

class ParticipantStats extends BaseParticipantStats
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_DRIVER_STATS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'driver_id',
        'stat',
    ];

    /**
     * Relationship: 1:1 stat details
     * @return HasOne
     */
    public function stat(): HasOne
    {
        return $this->hasOne(Stats::class, 'stat_id', 'stat_id');
    }
    /**
     * Relationship: 1:1 participant details
     * @return HasOne
     */
    public function participant(): HasOne
    {
        return $this->hasOne(Participant::class, 'driver_id', 'driver_id');
    }
}
