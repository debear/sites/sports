<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\Team as BaseTeam;

class Team extends BaseTeam
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_TEAMS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'team_id',
    ];

    /**
     * Relationship: 1:M season history
     * @return HasMany
     */
    public function history(): HasMany
    {
        return $this->hasMany(TeamHistory::class, ['series', 'team_id'], ['series', 'team_id']);
    }
    /**
     * Relationship: 1:M individual participants for the races the team has taken part in
     * @return HasMany
     */
    public function races(): HasMany
    {
        return $this->hasMany(RaceParticipant::class, ['season', 'series', 'team_id'], ['season', 'series', 'team_id']);
    }
    /**
     * Relationship: 1:M race-by-race progress that season
     * @return HasMany
     */
    public function progress(): HasMany
    {
        return $this->hasMany(
            TeamHistoryProgress::class,
            ['season', 'series', 'team_id'],
            ['season', 'series', 'team_id']
        );
    }
    /**
     * Relationship: 1:M team's stats for this season
     * @return HasMany
     */
    public function stats(): HasMany
    {
        return $this->hasMany(TeamStats::class, ['season', 'series', 'team_id'], ['season', 'series', 'team_id']);
    }
}
