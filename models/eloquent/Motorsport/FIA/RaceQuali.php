<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use DeBear\Models\Sports\Motorsport\RaceQuali as BaseRaceQuali;

class RaceQuali extends BaseRaceQuali
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_RACE_QUALI';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'session',
        'car_no',
    ];
}
