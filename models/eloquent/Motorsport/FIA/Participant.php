<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\Participant as BaseParticipant;

class Participant extends BaseParticipant
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_DRIVERS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'driver_id';

    /**
     * Relationship: 1:M season history
     * @return HasMany
     */
    public function history(): HasMany
    {
        return $this->hasMany(ParticipantHistory::class, 'driver_id', 'driver_id');
    }
}
