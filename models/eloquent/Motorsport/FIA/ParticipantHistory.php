<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\ParticipantHistory as BaseParticipantHistory;

class ParticipantHistory extends BaseParticipantHistory
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_DRIVER_HISTORY';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'driver_id',
    ];

    /**
     * Relationship: 1:1 participant details
     * @return HasOne
     */
    public function participant(): HasOne
    {
        return $this->hasOne(Participant::class, 'driver_id', 'driver_id');
    }
    /**
     * Relationship: 1:M results in a season
     * @return HasMany
     */
    public function races(): HasMany
    {
        return $this->hasMany(
            RaceParticipant::class,
            ['season', 'series', 'driver_id'],
            ['season', 'series', 'driver_id']
        );
    }
    /**
     * Relationship: 1:M races in a season
     * @return HasMany
     */
    public function calendar(): HasMany
    {
        return $this->hasMany(Race::class, ['season', 'series'], ['season', 'series']);
    }
    /**
     * Relationship: 1:M historical races in a season (pre-detailed data)
     * @return HasMany
     */
    public function racesHistorical(): HasMany
    {
        return $this->hasMany(RaceHistorical::class, ['season', 'driver_id'], ['season', 'driver_id']);
    }
    /**
     * Relationship: 1:M  race-by-race progress that season
     * @return HasMany
     */
    public function progress(): HasMany
    {
        return $this->hasMany(
            ParticipantHistoryProgress::class,
            ['season', 'series', 'driver_id'],
            ['season', 'series', 'driver_id']
        );
    }
    /**
     * Relationship: 1:M participant's stats for this season
     * @return HasMany
     */
    public function stats(): HasMany
    {
        return $this->hasMany(
            ParticipantStats::class,
            ['season', 'series', 'driver_id'],
            ['season', 'series', 'driver_id']
        );
    }
}
