<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use DeBear\Models\Sports\Motorsport\RaceResult as BaseRaceResult;

class RaceResult extends BaseRaceResult
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_RACE_RESULT';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'race',
        'pos',
    ];
}
