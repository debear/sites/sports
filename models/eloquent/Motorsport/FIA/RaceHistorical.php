<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Models\Sports\Motorsport\RaceHistorical as BaseRaceHistorical;

class RaceHistorical extends BaseRaceHistorical
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_DRIVER_GP_HISTORY';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'driver_id',
    ];

    /**
     * Relationship: 1:1 participant team details
     * @return HasOne
     */
    public function team(): HasOne
    {
        return $this->hasOne(Team::class, ['season', 'series', 'team_id'], ['season', 'series', 'team_id']);
    }
}
