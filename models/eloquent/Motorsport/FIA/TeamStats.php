<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasOne;
use DeBear\Models\Sports\Motorsport\TeamStats as BaseTeamStats;

class TeamStats extends BaseTeamStats
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_TEAM_STATS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'driver_id',
        'stat',
    ];

    /**
     * Relationship: 1:1 stat details
     * @return HasOne
     */
    public function stat(): HasOne
    {
        return $this->hasOne(Stats::class, 'stat_id', 'stat_id');
    }
    /**
     * Relationship: 1:1 team details
     * @return HasOne
     */
    public function team(): HasOne
    {
        return $this->hasOne(Team::class, ['season', 'series', 'team_id'], ['season', 'series', 'team_id']);
    }
}
