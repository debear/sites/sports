<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\RaceParticipant as BaseRaceParticipant;

class RaceParticipant extends BaseRaceParticipant
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_RACE_DRIVERS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'round',
        'car_no',
    ];

    /**
     * Relationship: 1:1 race history
     * @return HasOne
     */
    public function race(): HasOne
    {
        return $this->hasOne(Race::class, ['season', 'series', 'round'], ['season', 'series', 'round']);
    }
    /**
     * Relationship: 1:1 participant details
     * @return HasOne
     */
    public function participant(): HasOne
    {
        return $this->hasOne(Participant::class, 'driver_id', 'driver_id');
    }
    /**
     * Relationship: 1:1 participant team details
     * @return HasOne
     */
    public function team(): HasOne
    {
        return $this->hasOne(Team::class, ['season', 'series', 'team_id'], ['season', 'series', 'team_id']);
    }
    /**
     * Relationship: 1:M individual grid positions
     * @return HasMany
     */
    public function grid(): HasMany
    {
        return $this->hasMany(
            RaceGrid::class,
            ['season', 'series', 'round', 'car_no'],
            ['season', 'series', 'round', 'car_no']
        );
    }
    /**
     * Relationship: 1:M individual race results
     * @return HasMany
     */
    public function result(): HasMany
    {
        return $this->hasMany(
            RaceResult::class,
            ['season', 'series', 'round', 'car_no'],
            ['season', 'series', 'round', 'car_no']
        );
    }
}
