<?php

namespace DeBear\Models\Sports\Motorsport\FIA;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use DeBear\Models\Sports\Motorsport\TeamHistory as BaseTeamHistory;

class TeamHistory extends BaseTeamHistory
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_FIA_TEAM_HISTORY';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'series',
        'team_id',
    ];

    /**
     * Relationship: 1:1 team details
     * @return HasOne
     */
    public function team(): HasOne
    {
        return $this->hasOne(Team::class, ['season', 'series', 'team_id'], ['season', 'series', 'team_id']);
    }
    /**
     * Relationship: 1:M individual participants for the races the team has taken part in
     * @return HasMany
     */
    public function races(): HasMany
    {
        return $this->hasMany(RaceParticipant::class, ['season', 'series', 'team_id'], ['season', 'series', 'team_id']);
    }
    /**
     * Relationship: 1:M race-by-race progress that season
     * @return HasMany
     */
    public function progress(): HasMany
    {
        return $this->hasMany(
            TeamHistoryProgress::class,
            ['season', 'series', 'team_id'],
            ['season', 'series', 'team_id']
        );
    }
}
