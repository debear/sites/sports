<?php

namespace DeBear\Models\Sports\Motorsport;

use DeBear\Implementations\Model;

class Race extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'race_time' => 'datetime',
        'race2_time' => 'datetime',
        'qual_time' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
