<?php

namespace DeBear\Models\Sports\Motorsport;

use DeBear\Implementations\Model;

class Participant extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'dob' => 'datetime:Y-m-d',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = true;
}
