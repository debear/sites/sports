<?php

namespace DeBear\Models\Sports\Motorsport;

use DeBear\Implementations\Model;
use DeBear\Helpers\Sports\Motorsport\Traits\EntityHistory as TraitEntityHistory;

class TeamHistory extends Model
{
    use TraitEntityHistory;

    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
