<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\TeamRoster as BaseTeamRoster;

class TeamRoster extends BaseTeamRoster
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_TEAMS_ROSTERS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'the_date',
        'player_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'the_date' => 'datetime:Y-m-d',
    ];
}
