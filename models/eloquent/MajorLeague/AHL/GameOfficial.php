<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\GameOfficial as BaseGameOfficial;

class GameOfficial extends BaseGameOfficial
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_GAME_OFFICIALS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'game_id',
        'official_type',
        'official_id',
    ];
}
