<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\PlayoffSeries as BasePlayoffSeries;

class PlayoffSeries extends BasePlayoffSeries
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_PLAYOFF_SERIES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'the_date',
        'round_code',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'the_date' => 'datetime:Y-m-d',
    ];
}
