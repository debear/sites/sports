<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamSeasonGoalie extends BaseTeamSeasonStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_TEAMS_SEASON_GOALIES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'team_id',
        'stat_dir',
    ];
}
