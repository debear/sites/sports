<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\GamePPStats as BaseGamePPStats;

class GamePPStats extends BaseGamePPStats
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_GAME_PP_STATS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'game_id',
        'team_id',
        'pp_type',
    ];
}
