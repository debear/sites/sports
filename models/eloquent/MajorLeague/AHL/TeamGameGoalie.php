<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\TeamGameStat as BaseTeamGameStat;

class TeamGameGoalie extends BaseTeamGameStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_TEAMS_GAME_GOALIES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'team_id',
        'game_type',
        'game_id',
        'stat_dir',
    ];
}
