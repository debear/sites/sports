<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\PlayerHeatmap as BasePlayerHeatmap;

class PlayerSeasonSkaterHeatmap extends BasePlayerHeatmap
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_PLAYERS_SEASON_SKATERS_HEATMAP';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'player_id',
    ];
}
