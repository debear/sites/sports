<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\TeamHistoryPlayoffSummary as BaseTeamHistoryPlayoffSummary;

class TeamHistoryPlayoffSummary extends BaseTeamHistoryPlayoffSummary
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_TEAMS_HISTORY_PLAYOFF_SUMMARY';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'team_id',
        'season',
    ];
}
