<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\Position as BasePosition;

class Position extends BasePosition
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_POSITIONS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'pos_code';
}
