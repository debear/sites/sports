<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\TeamHistoryRegular as BaseTeamHistoryRegular;

class TeamHistoryRegular extends BaseTeamHistoryRegular
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_TEAMS_HISTORY_REGULAR';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'team_id',
        'season',
    ];
}
