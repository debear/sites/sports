<?php

namespace DeBear\Models\Sports\MajorLeague\AHL;

use DeBear\Models\Sports\MajorLeague\Official as BaseOfficial;

class Official extends BaseOfficial
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_AHL_OFFICIALS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'official_id',
    ];
}
