<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerInjury as BasePlayerInjury;

class PlayerInjury extends BasePlayerInjury
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_INJURIES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'player_id',
        'date_from',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date_from' => 'datetime:Y-m-d',
        'date_to' => 'datetime:Y-m-d',
        'updated' => 'datetime',
    ];
}
