<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerCareerStat as BasePlayerCareerStat;

class PlayerCareerBvP extends BasePlayerCareerStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'batter',
        'pitcher',
        'season_type',
    ];
}
