<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\TeamHistoryPlayoff as BaseTeamHistoryPlayoff;

class TeamHistoryPlayoff extends BaseTeamHistoryPlayoff
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_TEAMS_HISTORY_PLAYOFF';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'team_id',
        'round',
    ];
}
