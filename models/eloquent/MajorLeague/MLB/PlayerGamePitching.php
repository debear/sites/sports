<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;

class PlayerGamePitching extends BasePlayerGameStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_GAME_PITCHING';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'game_id',
        'team_id',
        'jersey',
    ];
}
