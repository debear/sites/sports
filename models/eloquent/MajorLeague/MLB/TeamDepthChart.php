<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\TeamDepthChart as BaseTeamDepthChart;

class TeamDepthChart extends BaseTeamDepthChart
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_TEAMS_DEPTH';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'team_id',
        'pos',
        'depth',
    ];
}
