<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerHeatmap as BasePlayerHeatmap;

class PlayerSeasonPitchingZones extends BasePlayerHeatmap
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_SEASON_PITCHING_ZONES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'player_id',
        'batter_hand',
        'pitcher_hand',
    ];
}
