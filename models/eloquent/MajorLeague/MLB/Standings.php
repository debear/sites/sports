<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\Standings as BaseStandings;

class Standings extends BaseStandings
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_STANDINGS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'the_date',
        'team_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'the_date' => 'datetime:Y-m-d',
    ];
}
