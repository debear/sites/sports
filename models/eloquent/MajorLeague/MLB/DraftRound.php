<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\DraftRound as BaseDraftRound;

class DraftRound extends BaseDraftRound
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_DRAFT_ROUNDS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'code';
}
