<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\SplitLabels as BaseSplitLabels;

class SplitLabels extends BaseSplitLabels
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_SPLIT_LABELS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'split_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'created' => 'datetime',
    ];
}
