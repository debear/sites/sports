<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\Official as BaseOfficial;

class Official extends BaseOfficial
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_UMPIRES';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'umpire_id';
}
