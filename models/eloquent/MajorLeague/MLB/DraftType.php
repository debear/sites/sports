<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\DraftType as BaseDraftType;

class DraftType extends BaseDraftType
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_DRAFT_TYPES';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'type';
}
