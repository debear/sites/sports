<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\TeamGameStat as BaseTeamGameStat;

class TeamGameFielding extends BaseTeamGameStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_TEAMS_GAME_FIELDING';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'game_id',
        'team_id',
    ];
}
