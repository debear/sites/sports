<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;

class PlayerSeasonFielding extends BasePlayerSeasonStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_SEASON_FIELDING';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'player_id',
        'team_id',
    ];
}
