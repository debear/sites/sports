<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\DraftSeason as BaseDraftSeason;

class DraftSeason extends BaseDraftSeason
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_DRAFT_SEASON';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'season';
}
