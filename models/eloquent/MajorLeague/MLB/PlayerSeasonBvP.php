<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;

class PlayerSeasonBvP extends BasePlayerSeasonStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'batter',
        'pitcher',
    ];
}
