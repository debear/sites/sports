<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerSeasonSplits as BasePlayerSeasonSplits;

class PlayerSeasonPitchingSplits extends BasePlayerSeasonSplits
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'player_id',
        'split_type',
        'split_label',
    ];
}
