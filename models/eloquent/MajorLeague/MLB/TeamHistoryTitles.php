<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\TeamHistoryTitles as BaseTeamHistoryTitles;

class TeamHistoryTitles extends BaseTeamHistoryTitles
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_TEAMS_HISTORY_TITLES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'team_id',
        'league_id',
    ];
}
