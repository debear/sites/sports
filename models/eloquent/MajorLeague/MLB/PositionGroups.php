<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PositionGroups as BasePositionGroups;

class PositionGroups extends BasePositionGroups
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_POSITIONS_GROUPS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'posgroup_id';
}
