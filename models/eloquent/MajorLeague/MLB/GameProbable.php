<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\GameLineup as BaseGameLineup;

class GameProbable extends BaseGameLineup
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_SCHEDULE_PROBABLES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'game_id',
        'team_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'game_date' => 'datetime:Y-m-d',
    ];
}
