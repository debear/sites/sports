<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PowerRankingsWeeks as BasePowerRankingsWeeks;

class PowerRankingsWeeks extends BasePowerRankingsWeeks
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_POWER_RANKINGS_WEEKS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'week',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'calc_date' => 'datetime:Y-m-d',
        'date_from' => 'datetime:Y-m-d',
        'date_to' => 'datetime:Y-m-d',
        'when_run' => 'datetime',
    ];
}
