<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\Groupings as BaseGroupings;

class Groupings extends BaseGroupings
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_GROUPINGS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'grouping_id';
}
