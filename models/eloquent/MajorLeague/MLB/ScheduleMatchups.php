<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\ScheduleMatchups as BaseScheduleMatchups;

class ScheduleMatchups extends BaseScheduleMatchups
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_SCHEDULE_MATCHUPS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'team_idA',
        'team_idB',
        'game_type',
        'game_id',
    ];
}
