<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Implementations\Model;

class PlayerRepertoire extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_REPERTOIRE';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'pos_raw';
}
