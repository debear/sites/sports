<?php

namespace DeBear\Models\Sports\MajorLeague\MLB;

use DeBear\Models\Sports\MajorLeague\PlayerCareerSplits as BasePlayerCareerSplits;

class PlayerCareerFieldingSplits extends BasePlayerCareerSplits
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_MLB_PLAYERS_CAREER_FIELDING_SPLITS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season_type',
        'player_id',
        'split_type',
        'split_label',
    ];
}
