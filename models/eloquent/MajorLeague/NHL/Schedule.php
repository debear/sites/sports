<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\Schedule as BaseSchedule;

class Schedule extends BaseSchedule
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_SCHEDULE';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'game_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'game_date' => 'datetime:Y-m-d',
    ];
}
