<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\TeamVenue as BaseTeamVenue;

class TeamVenue extends BaseTeamVenue
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_TEAMS_ARENAS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'team_id',
        'season_from',
    ];
}
