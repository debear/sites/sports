<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;

class PlayerGameGoalie extends BasePlayerGameStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_PLAYERS_GAME_GOALIES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'player_id',
        'game_type',
        'game_id',
    ];
}
