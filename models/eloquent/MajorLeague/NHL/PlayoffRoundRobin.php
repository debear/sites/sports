<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\PlayoffRoundRobin as BasePlayoffRoundRobin;

class PlayoffRoundRobin extends BasePlayoffRoundRobin
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_PLAYOFF_ROUND_ROBIN';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'conf_id',
        'team_id',
    ];
}
