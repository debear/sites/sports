<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\PlayerSeasonSorted as BasePlayerSeasonSorted;

class PlayerSeasonGoalieSorted extends BasePlayerSeasonSorted
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_PLAYERS_SEASON_GOALIES_SORTED';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'player_id',
        'team_id',
    ];
}
