<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Implementations\Model;

class GameStrengthPeriod extends Model
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_GAME_STRENGTHS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'game_id',
        'instance_id',
    ];
}
