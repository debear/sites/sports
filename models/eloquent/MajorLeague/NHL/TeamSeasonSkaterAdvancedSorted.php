<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\TeamSeasonSorted as BaseTeamSeasonSorted;

class TeamSeasonSkaterAdvancedSorted extends BaseTeamSeasonSorted
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED_SORTED';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'team_id',
    ];
}
