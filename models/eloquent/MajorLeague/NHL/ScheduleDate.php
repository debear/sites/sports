<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\ScheduleDate as BaseScheduleDate;

class ScheduleDate extends BaseScheduleDate
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_SCHEDULE_DATES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'date',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
        'date_prev' => 'datetime:Y-m-d',
        'date_next' => 'datetime:Y-m-d',
    ];
}
