<?php

namespace DeBear\Models\Sports\MajorLeague\NHL;

use DeBear\Models\Sports\MajorLeague\TeamHistoryRegular as BaseTeamHistoryRegular;

class TeamHistoryRegular extends BaseTeamHistoryRegular
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NHL_TEAMS_HISTORY_REGULAR';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'team_id',
        'season',
        'season_half',
    ];
}
