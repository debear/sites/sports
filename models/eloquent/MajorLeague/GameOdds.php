<?php

namespace DeBear\Models\Sports\MajorLeague;

use DeBear\Implementations\Model;

class GameOdds extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_COMMON_MAJORS_ODDS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'sport',
        'season',
        'game_type',
        'game_id',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
