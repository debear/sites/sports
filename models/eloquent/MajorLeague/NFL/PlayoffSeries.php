<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\PlayoffSeries as BasePlayoffSeries;

class PlayoffSeries extends BasePlayoffSeries
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_PLAYOFF_MATCHUPS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'week_playoff',
        'matchup_id',
        'calc_game_type',
        'calc_week',
    ];
}
