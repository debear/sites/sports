<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\Player as BasePlayer;

class Player extends BasePlayer
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_PLAYERS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'player_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'dob' => 'datetime:Y-m-d',
    ];
}
