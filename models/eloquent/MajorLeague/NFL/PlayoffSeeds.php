<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\PlayoffSeeds as BasePlayoffSeeds;

class PlayoffSeeds extends BasePlayoffSeeds
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_PLAYOFF_SEEDS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'conf_id',
        'seed',
    ];
}
