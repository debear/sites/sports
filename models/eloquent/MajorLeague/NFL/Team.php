<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\Team as BaseTeam;

class Team extends BaseTeam
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_TEAMS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'team_id';
}
