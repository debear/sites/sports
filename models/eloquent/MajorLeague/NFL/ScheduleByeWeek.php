<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Implementations\Model;

class ScheduleByeWeek extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_sports';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_SCHEDULE_BYES';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'team_id',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
    /**
     * The primary key is not auto-incrementing
     * @var boolean
     */
    public $incrementing = false;
}
