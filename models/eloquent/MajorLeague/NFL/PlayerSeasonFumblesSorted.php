<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\PlayerSeasonSorted as BasePlayerSeasonSorted;

class PlayerSeasonFumblesSorted extends BasePlayerSeasonSorted
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_PLAYERS_SEASON_FUMBLES_SORTED';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'player_id',
        'team_id',
    ];
}
