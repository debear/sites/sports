<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\TeamHistoryPlayoff as BaseTeamHistoryPlayoff;

class TeamHistoryPlayoff extends BaseTeamHistoryPlayoff
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_TEAMS_HISTORY_PLAYOFF';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'team_id',
        'season',
        'round',
    ];
}
