<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;

class PlayerSeasonPunts extends BasePlayerSeasonStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_PLAYERS_SEASON_PUNTS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'player_id',
        'team_id',
    ];
}
