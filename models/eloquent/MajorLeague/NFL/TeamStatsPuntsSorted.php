<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\TeamSeasonSorted as BaseTeamSeasonSorted;

class TeamStatsPuntsSorted extends BaseTeamSeasonSorted
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_TEAMS_STATS_PUNTS_SORTED';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'season_type',
        'team_id',
    ];
}
