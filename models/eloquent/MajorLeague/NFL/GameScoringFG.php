<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\GameEvent as BaseGameEvent;

class GameScoringFG extends BaseGameEvent
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_GAME_SCORING_FG';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'week',
        'game_id',
        'play_id',
    ];
}
