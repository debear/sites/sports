<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\Awards as BaseAwards;

class Awards extends BaseAwards
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_AWARDS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'award_id';
}
