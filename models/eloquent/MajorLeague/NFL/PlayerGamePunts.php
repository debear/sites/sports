<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;

class PlayerGamePunts extends BasePlayerGameStat
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_PLAYERS_GAME_PUNTS';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'week',
        'game_id',
        'team_id',
        'jersey',
    ];
}
