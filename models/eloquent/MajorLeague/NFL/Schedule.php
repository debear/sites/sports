<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\Schedule as BaseSchedule;

class Schedule extends BaseSchedule
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_SCHEDULE';
    /**
     * Compound Primary Key columns for the database table
     * @var array
     */
    protected $compoundKey = [
        'season',
        'game_type',
        'week',
        'game_id',
    ];
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'game_date' => 'datetime:Y-m-d',
    ];
}
