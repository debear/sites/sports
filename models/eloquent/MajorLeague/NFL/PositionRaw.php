<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Implementations\Model;

class PositionRaw extends Model
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_POSITIONS_RAW';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'pos_raw';
}
