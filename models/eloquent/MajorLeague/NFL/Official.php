<?php

namespace DeBear\Models\Sports\MajorLeague\NFL;

use DeBear\Models\Sports\MajorLeague\Official as BaseOfficial;

class Official extends BaseOfficial
{
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'SPORTS_NFL_OFFICIALS';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'official_id';
}
