<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;

class TeamHistoryRegular extends Instance
{
    /**
     * Convert the separate record fields in to a single value
     * @return string The team's formatted record
     */
    public function getRecordAttribute(): string
    {
        return join('&ndash;', array_filter([
            $this->wins, $this->loss, $this->ties, $this->ot_loss, $this->so_loss,
        ], function ($a) {
            return isset($a);
        }));
    }
}
