<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class TeamVenue extends Instance
{
    /**
     * State whether this venue is a dome or not, based on whether it has a usable weather aspect
     * @return boolean That the team venue should be consiered a dome
     */
    public function isDomed(): bool
    {
        $key = 'debear.sports.subsites.' . FrameworkConfig::get('debear.section.code') . '.weather.app';
        $has_weather = (bool) FrameworkConfig::get($key);
        return $has_weather && !isset($this->weather_spec);
    }
}
