<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class Groupings extends Instance
{
    /**
     * Load grouping info the the given IDs
     * @param array $ids Grouping IDs to load.
     * @return self An ORM object with the appropriate details
     */
    public static function loadByID(array $ids): self
    {
        return static::query()
            ->whereIn('grouping_id', $ids)
            ->orderBy('order')
            ->get();
    }

    /**
     * Return the CSS used to highlight the row in the grouping's specific CSS colours
     * @return string The applicable CSS attribute
     */
    public function rowCSS(): string
    {
        return 'row_' . $this->type . '-' . ($this->icon ?? $this->grouping_id);
    }

    /**
     * Return the CSS used to render a sized version of the grouping's (current) logo
     * @param string $size The requested logo size (Default: tiny).
     * @return string The applicable CSS attribute
     */
    public function logoCSS(string $size = 'tiny'): string
    {
        if (!$this->icon) {
            return '';
        }
        return 'misc-' . FrameworkConfig::get('debear.section.code') . '-'
            . ($size != 'tiny' ? "$size-" : '')
            . $this->icon;
    }

    /**
     * Return the CSS used to render a sized version of the grouping's (current) logo on the right of the content
     * @param string $size The requested logo size (Default: tiny).
     * @return string The applicable CSS attribute
     */
    public function logoRightCSS(string $size = 'tiny'): string
    {
        if (!$this->icon) {
            return '';
        }
        return 'misc-' . FrameworkConfig::get('debear.section.code') . '-'
            . ($size != 'tiny' ? "$size-" : '')
            . "right-{$this->icon}";
    }
}
