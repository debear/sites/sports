<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\Schedule as BaseSchedule;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\Helpers\Format;

class Schedule extends BaseSchedule
{
    /**
     * State if this game is special, which series makes it so
     * @return string A summary title containing just the game series
     */
    public function getSeriesTitleAttribute(): string
    {
        if ($this->game_type == 'regular') {
            // Regular Season not special enough.
            return '';
        }
        $game_id = str_pad($this->game_id, 3, '0', STR_PAD_LEFT);
        $round = substr($game_id, 0, 1);
        $series = substr($game_id, 1, 1);

        // Is the structure divisional?
        $is_divisional = false;
        foreach (FrameworkConfig::get('debear.setup.playoffs.divisional') as $period) {
            if (($period['from'] <= $this->season) && ($this->season <= $period['to'])) {
                $is_divisional = true;
                break;
            }
        }

        // Which round?
        $trophy = FrameworkConfig::get('debear.setup.playoffs.trophy');
        if ($round == 4) {
            // Cup Finals.
            $title = $trophy;
            $stage = $round;
        } elseif ($is_divisional && $round < 3) {
            // Divisional round.
            $title = $this->home->division->name_full;
            $stage = intval($round) + 1; // Round 1 == DSF, so bump up.
        } else {
            // 3rd Round is usually (but not always) conference based.
            $is_conf = FrameworkConfig::get("debear.setup.standings.hide_conf.{$this->season}") === null;
            $title = ($is_conf ? $this->home->conference->name_full : $trophy);
            $stage = ($round == 3 ? ($is_conf ? 3 : 2) : $round);
        }

        // Which stage?
        if ($stage == 0 && $series > 1) {
            $title .= ' Preliminary Round';
        } elseif ($stage == 0) {
            $title .= ' Round Robin';
        } elseif ($stage == 1) {
            $title .= ' Quarter Final';
        } elseif ($stage == 2) {
            $title .= ' Semi Final';
        } else {
            $title .= ' Final';
        }

        // Return what we have built.
        return $title;
    }

    /**
     * The number of game quarters that took place
     * @return integer The number of quarters that took place
     */
    public function getNumPeriodsAttribute(): int
    {
        if ($this->status == 'F') {
            // Regulation game.
            return 3;
        } elseif ($this->status == 'OT') {
            // Single OT period.
            return 4;
        } elseif ($this->status == 'SO') {
            // OT and Shootout.
            return 5;
        }
        // Multiple OTs.
        return 3 + $this->status[0];
    }

    /**
     * The name of a particular period
     * @param integer $num Period number.
     * @return string The short period name
     */
    public function periodName(int $num): string
    {
        if ($num == 5 && $this->game_type == 'regular') {
            // Shootout (Regular season only).
            return 'SO';
        } elseif ($num > 3) {
            return ltrim(($num - 3) . 'OT', '1');
        }
        return (string)$num;
    }

    /**
     * The summary name of a particular period
     * @param integer $num Period number.
     * @return string The "intermediate" period name
     */
    public function periodNameMid(int $num): string
    {
        if ($num == 5 && $this->game_type == 'regular') {
            // Shootout (Regular season only).
            return 'SO';
        } elseif ($num > 3) {
            return ltrim(($num - 3) . 'OT', '1');
        }
        return Format::ordinal($num);
    }

    /**
     * The full name of a particular period
     * @param integer $num Period number.
     * @return string The full period name
     */
    public function periodNameFull(int $num): string
    {
        if ($num == 5 && $this->game_type == 'regular') {
            // Shootout (Regular season only).
            return 'Shootout';
        } elseif ($num > 3) {
            return ($num > 4 || $this->game_type == 'playoff' ? Format::ordinal($num - 3) . ' ' : '') . 'Overtime';
        }
        return Format::ordinal($num) . ' Period';
    }

    /**
     * Whether the game went to a shootout
     * @return boolean That the game went to a shootout
     */
    public function hasShootout(): bool
    {
        return ($this->status == 'SO');
    }

    /**
     * Determine (as an abbreviation) the result for the team
     * @return string The result of the game for the given team
     */
    public function resultCode(): string
    {
        $team_score = "{$this->team_type}_score";
        $opp_score = "{$this->team_opp}_score";
        if ($this->$team_score > $this->$opp_score) {
            // Win.
            return 'W';
        } elseif ($this->status == 'F' || $this->isPlayoff()) {
            // Regulation loss (or any form of playoff loss).
            return 'L';
        } elseif ($this->status == 'SO') {
            // Shootout loss.
            return 'SOL';
        }
        // All other losses were in overtime.
        return 'OTL';
    }

    /**
     * Determine (as word/s) the result for the team
     * @return string The result of the game for the given team
     */
    public function result(): string
    {
        $code = $this->resultCode();
        if ($code == 'W') {
            $res = 'Win';
        } elseif ($code == 'L') {
            $res = 'Loss';
        } elseif ($code == 'OTL') {
            $res = 'OT Loss';
        } elseif ($code == 'SOL') {
            $res = 'SO Loss';
        }
        return $res ?? '';
    }

    /**
     * Load the round robin games that occurred in a particular conference
     * @param integer $season  The season the round robin occurred in.
     * @param integer $conf_id The conference ID for the round robin to be viewed.
     * @return self The resulting ORM object containing all game for the game date being passed in
     */
    public static function loadRoundRobin(int $season, int $conf_id): self
    {
        $tbl_sched = static::getTable();
        $tbl_seeds = PlayoffSeeds::getTable();
        return static::query()
            ->select("$tbl_sched.*")
            ->join($tbl_seeds, function ($join) use ($tbl_seeds, $tbl_sched, $conf_id) {
                $join->on("$tbl_seeds.season", '=', "$tbl_sched.season")
                    ->where("$tbl_seeds.conf_id", '=', $conf_id)
                    ->on("$tbl_seeds.team_id", '=', "$tbl_sched.home");
            })->where([
                ["$tbl_sched.season", '=', $season],
                ["$tbl_sched.game_type", '=', 'playoff'],
                ["$tbl_sched.game_id", '<', 20],
            ])->orderBy('game_date')
            ->orderBy('game_time')
            ->orderBy('visitor')
            ->get()
            ->loadBasicSecondaries();
    }

    /**
     * Load the secondary game information with some, but not all, info for display
     * @return BaseSchedule The resulting ORM object with some additional information loaded
     */
    public function loadBasicSecondaries(): BaseSchedule
    {
        return $this->loadSecondary(['threestars', 'goalies']);
    }

    /**
     * Load the secondary game information for display
     * @return BaseSchedule The resulting ORM object with additional information loaded
     */
    public function loadSummarySecondaries(): BaseSchedule
    {
        return $this->loadSecondary(['odds', 'periods', 'standings', 'series', 'threestars', 'goalies']);
    }

    /**
     * Load the secondary game information for displaying a single game
     * @return BaseSchedule The resulting ORM object with detailed additional information loaded
     */
    public function loadDetailedSecondaries(): BaseSchedule
    {
        return $this->loadSecondary([
            'periods', 'series', 'threestars', 'rosters', 'plays', 'playerstats', 'teamstats', 'officials', 'matchups'
        ]);
    }

    /**
     * Load the players and rosters for the teams involved
     * @return void
     */
    public function loadSecondaryRosters(): void
    {
        // The lineups.
        $tbl_lineup = GameLineup::getTable();
        $tbl_pos = Position::getTable();
        $query_games = $this->queryGames($tbl_lineup);
        $lineups = GameLineup::query()
            ->select("$tbl_lineup.*")
            ->selectRaw("$tbl_pos.pos_short AS pos")
            ->selectRaw("CONCAT($tbl_lineup.season, '-', $tbl_lineup.game_type, '-', $tbl_lineup.game_id"
                . ", '-', $tbl_lineup.team_id) AS team_ref")
            ->join($tbl_pos, "$tbl_pos.pos_code", '=', "$tbl_lineup.pos")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // The scratches.
        $query_games = $this->queryGames();
        $scratches = GameInactive::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", team_id) AS team_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Then the players, preserving for future secondary calcs.
        $this->secondary['players'] = Player::query()
            ->whereIn(
                'player_id',
                array_unique(array_merge($lineups->unique('player_id'), $scratches->unique('player_id')))
            )->get();
        foreach ([$lineups, $scratches] as $list) {
            foreach ($list as $line) {
                $line->player = $this->secondary['players']->where('player_id', $line->player_id);
            }
        }
        // Now tie together.
        foreach ($this->data as $i => $game) {
            foreach (['home', 'visitor'] as $team) {
                $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game[$team]->team_id}";
                $this->data[$i][$team]->lineup = $lineups->where('team_ref', $ref);
                $this->data[$i][$team]->scratches = $scratches->where('team_ref', $ref);
            }
        }
    }

    /**
     * Load the individual plays
     * @return void
     */
    public function loadSecondaryPlays(): void
    {
        $query_games = $this->queryGames();
        // Preserve for future secondary calcs.
        $this->secondary['plays'] = GameEvent::query()
            ->select('*')
            ->selectRaw('IFNULL(team_id, "-") AS team_id')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS ref')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", IFNULL(team_id, "-")) AS team_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('game_id')
            ->orderBy('event_id')
            ->get();
        $this->secondary['events'] = GameEventCoord::query()
            ->select(['season', 'game_type', 'game_id', 'event_type'])
            ->selectRaw('UNCOMPRESS(events) AS events')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Now tie together.
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['plays'] = $this->secondary['plays']->where('ref', $ref);
            $this->data[$i]['events'] = $this->secondary['events']->where('ref', $ref);
        }
    }

    /**
     * The list of player stats to be loaded from the database
     * @return array Key/Value pair of stats
     */
    protected function loadSecondaryPlayerStatsList(): array
    {
        return [
            'skaters' => PlayerGameSkater::class,
            'goalies' => PlayerGameGoalie::class,
        ];
    }

    /**
     * Load the game stats for the teams involved
     * @return void
     */
    public function loadSecondaryTeamStats(): void
    {
        $query_games = $this->queryGames();
        // Get the stats.
        $skater_stats = TeamGameSkater::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", team_id) AS team_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->where('stat_dir', 'for')
            ->get();
        // Now build - returning as an stdClass as we have no ORM object.
        foreach ($this->data as $i => $game) {
            $stats = [];
            // Get the home team related info.
            $home_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game['home_id']}";
            $stats['home'] = $skater_stats->where('team_ref', $home_ref);
            // Get the visiting team related info.
            $visitor_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game['visitor_id']}";
            $stats['visitor'] = $skater_stats->where('team_ref', $visitor_ref);
            // Then build the return objects.
            foreach ($stats as $key => $obj) {
                $this->data[$i][$key]->stats = (object)[
                    'goals' => $obj->goals,
                    'shots' => $obj->shots,
                    'missed_shots' => $obj->missed_shots,
                    'blocked_shots' => $obj->blocked_shots,
                    'shots_blocked' => $obj->shots_blocked,
                    'ppg' => $obj->pp_goals,
                    'ppa' => $obj->pp_opps,
                    'pk' => $obj->pk_kills,
                    'pka' => $obj->pk_opps,
                    'shg' => $obj->sh_goals,
                    'pen_num' => $obj->pens_taken,
                    'pims' => $obj->pims,
                    'hits' => $obj->hits,
                    'giveaways' => $obj->giveaways,
                    'takeaways' => $obj->takeaways,
                    'faceoffs' => $obj->fo_wins,
                ];
            }
        }
    }

    /**
     * Load the per-period scores for the teams involved
     * @return void
     */
    public function loadSecondaryPeriods(): void
    {
        $query_games = $this->queryGames();
        // Non-shootout goals.
        $scoring = GameEvent::query()
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS ref')
            ->selectRaw('team_id')
            ->selectRaw('period')
            ->selectRaw('COUNT(*) AS score')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })
            ->where('event_type', 'GOAL')
            ->groupBy(['season', 'game_type', 'game_id', 'period', 'team_id'])
            ->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('game_id')
            ->orderBy('team_id')
            ->orderBy('period')
            ->get();
        // Then shootouts (regular season only).
        $shootout = GameEvent::query()
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS ref')
            ->selectRaw('team_id')
            ->selectRaw('COUNT(*) AS score')
            ->where(function ($query) use ($query_games) {
                $where_count = 0;
                foreach ($query_games as $where) {
                    // [0][0] == '1' from our no-games catch-all.
                    // [1] == game_type WHERE, [2] == value column.
                    if (($where[0][0] != '1') && isset($where[1]) && ($where[1][2] == 'regular')) {
                        $query->orWhere(function (Builder $subwhere) use ($where) {
                            $subwhere->where($where);
                        });
                        $where_count++;
                    }
                }
                if (!$where_count) {
                    $query->where(DB::raw('1'), '=', DB::raw('0'));
                }
            })->where([
                ['event_type', '=', 'SHOOTOUT'],
                ['play', 'LIKE', '%scored%'],
            ])->groupBy('season', 'game_type', 'game_id', 'team_id')
            ->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('game_id')
            ->orderBy('team_id')
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            // Scoring.
            $by_period = $scoring->where('ref', $ref);
            $this->data[$i]['home']->scoring = $by_period->where('team_id', $game['home_id']);
            $this->data[$i]['visitor']->scoring = $by_period->where('team_id', $game['visitor_id']);
            // Shootout.
            $in_shootout = $shootout->where('ref', $ref);
            $this->data[$i]['home']->shootout = $in_shootout->where('team_id', $game['home_id']);
            $this->data[$i]['visitor']->shootout = $in_shootout->where('team_id', $game['visitor_id']);
        }
    }

    /**
     * Load the three stars for the games played
     * @return void
     */
    public function loadSecondaryThreeStars(): void
    {
        $tbl_stars = GameThreeStar::getTable();
        $tbl_player = Player::getTable();
        $tbl_lineup = GameLineup::getTable();
        // Determine the games this applies to.
        $query_games = $this->queryGames($tbl_stars);
        // Run the query.
        $three_stars = Player::query($tbl_stars)
            ->join($tbl_lineup, function ($join) use ($tbl_stars, $tbl_lineup) {
                $join->on("$tbl_lineup.season", '=', "$tbl_stars.season")
                    ->on("$tbl_lineup.game_type", '=', "$tbl_stars.game_type")
                    ->on("$tbl_lineup.game_id", '=', "$tbl_stars.game_id")
                    ->on("$tbl_lineup.team_id", '=', "$tbl_stars.team_id")
                    ->on("$tbl_lineup.jersey", '=', "$tbl_stars.jersey");
            })->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_lineup.player_id")
            ->select(["$tbl_player.*", "$tbl_stars.star", "$tbl_stars.team_id"])
            ->selectRaw("CONCAT($tbl_stars.season, '-', $tbl_stars.game_type, '-', $tbl_stars.game_id) AS ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy("$tbl_stars.season")
            ->orderBy("$tbl_stars.game_type")
            ->orderBy("$tbl_stars.game_id")
            ->get();
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['three_stars'] = $three_stars->where('ref', $ref);
        }
    }

    /**
     * Load the goalies of record for the games played
     * @return void
     */
    public function loadSecondaryGoalies(): void
    {
        $tbl_roster = GameLineup::getTable();
        $tbl_player = Player::getTable();
        $ref = "$tbl_roster.season, '-', $tbl_roster.game_type, '-', $tbl_roster.game_id, '-', $tbl_roster.team_id";
        $query = Player::query($tbl_roster)
            ->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_roster.player_id")
            ->select(["$tbl_player.*"])
            ->selectRaw("CONCAT($ref) AS ref");
        foreach (['home', 'visitor'] as $team) {
            $goalie_type = "{$team}_goalie";
            foreach ($this->data as $i => $game) {
                // Skip games that do not have an appropriate goalie.
                if (!isset($game[$goalie_type])) {
                    continue;
                }
                // Complete the query.
                $query->orWhere(function (Builder $where) use ($game, $goalie_type, $team, $tbl_roster) {
                    $where->where([
                        ["$tbl_roster.season", '=', $game['season']],
                        ["$tbl_roster.game_type", '=', $game['game_type']],
                        ["$tbl_roster.game_id", '=', $game['game_id']],
                        ["$tbl_roster.team_id", '=', $game["{$team}_id"]],
                        ["$tbl_roster.jersey", '=', $game[$goalie_type]],
                    ]);
                });
            }
        }
        $goalies = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        foreach (['home', 'visitor'] as $team) {
            $goalie_type = "{$team}_goalie";
            foreach ($this->data as $i => $game) {
                $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game[$team]->team_id}";
                $this->data[$i]["{$goalie_type}_num"] = $game[$goalie_type];
                $this->data[$i][$goalie_type] = $goalies->where('ref', $ref);
            }
        }
    }

    /**
     * Load the details of the officials involved
     * @return void
     */
    public function loadSecondaryOfficials(): void
    {
        list($season) = $this->unique('season');
        $query_games = $this->queryGames();
        $game_officials = GameOfficial::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS game_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Get the official's details.
        $officials = Official::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", official_id) AS official_ref')
            ->where('season', '=', $season)
            ->whereIn('official_id', $game_officials->unique('official_id'))
            ->get();
        // Now tie the official to the game official.
        foreach ($game_officials as $row) {
            $row->official = $officials->where('official_ref', "{$row->season}-{$row->official_id}");
        }
        // And then the officials to the game.
        foreach ($this->data as $i => $game) {
            $game_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['officials'] = $game_officials->where('game_ref', $game_ref);
        }
    }
}
