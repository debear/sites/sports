<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NHL\Traits\StatsGoalie as StatsGoalieTrait;

class PlayerGameGoalie extends BasePlayerGameStat
{
    use StatsGoalieTrait;
}
