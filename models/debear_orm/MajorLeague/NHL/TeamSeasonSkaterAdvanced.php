<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamSeasonSkaterAdvanced extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'corsi_cf' => ['s' => 'CF', 'f' => 'Corsi For', 'g' => 'Corsi'],
        'corsi_ca' => ['s' => 'CA', 'f' => 'Corsi Against', 'g' => 'Corsi'],
        'corsi_pct' => ['s' => 'C%', 'f' => 'Corsi %', 'g' => 'Corsi'],
        'fenwick_ff' => ['s' => 'FF', 'f' => 'Fenwick For', 'g' => 'Fenwick'],
        'fenwick_fa' => ['s' => 'FA', 'f' => 'Fenwick Against', 'g' => 'Fenwick'],
        'fenwick_pct' => ['s' => 'F%', 'f' => 'Fenwick %', 'g' => 'Fenwick'],
        'pdo_sh_gf' => ['s' => 'GF', 'f' => 'PDO Goals For', 'g' => 'PDO'],
        'pdo_sh_sog' => ['s' => 'SOG', 'f' => 'PDO Shots on Goal', 'g' => 'PDO'],
        'pdo_sh' => ['s' => 'Sh%', 'f' => 'PDO Shooting %age', 'g' => 'PDO'],
        'pdo_sv_sv' => ['s' => 'Sv', 'f' => 'PDO Saves', 'g' => 'PDO'],
        'pdo_sv_sog' => ['s' => 'SA', 'f' => 'PDO Shots Against', 'g' => 'PDO'],
        'pdo_sv' => ['s' => 'Sv%', 'f' => 'PDO Save %age', 'g' => 'PDO'],
        'pdo' => ['s' => 'PDO', 'f' => 'PDO', 'g' => 'PDO'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'corsi_cf' => 'TBL.corsi_cf',
        'corsi_ca' => 'TBL.corsi_ca',
        'corsi_pct' => '(100 * TBL.corsi_cf) / (TBL.corsi_cf + TBL.corsi_ca)',
        'fenwick_ff' => 'TBL.fenwick_ff',
        'fenwick_fa' => 'TBL.fenwick_fa',
        'fenwick_pct' => '(100 * TBL.fenwick_ff) / (TBL.fenwick_ff + TBL.fenwick_fa)',
        'pdo_sh_gf' => 'TBL.pdo_sh_gf',
        'pdo_sh_sog' => 'TBL.pdo_sh_sog',
        'pdo_sh' => '(100 * TBL.pdo_sh_gf) / TBL.pdo_sh_sog',
        'pdo_sv_sv' => 'TBL.pdo_sv_sv',
        'pdo_sv_sog' => 'TBL.pdo_sv_sog',
        'pdo_sv' => '(100 * TBL.pdo_sv_sv) / TBL.pdo_sv_sog',
        'pdo' => '((100 * TBL.pdo_sh_gf) / TBL.pdo_sh_sog) + ((100 * TBL.pdo_sv_sv) / TBL.pdo_sv_sog)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ((substr($column, -4) == '_pct') || in_array($column, ['pdo_sh', 'pdo_sv', 'pdo'])) {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
