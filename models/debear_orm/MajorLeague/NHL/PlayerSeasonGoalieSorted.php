<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonSorted as BasePlayerSeasonSorted;

class PlayerSeasonGoalieSorted extends BasePlayerSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = PlayerSeasonGoalie::class;
}
