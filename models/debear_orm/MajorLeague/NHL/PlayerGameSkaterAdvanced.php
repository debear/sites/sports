<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NHL\Traits\StatsSkaterAdvanced as StatsSkaterAdvancedTrait;

class PlayerGameSkaterAdvanced extends BasePlayerGameStat
{
    use StatsSkaterAdvancedTrait;
}
