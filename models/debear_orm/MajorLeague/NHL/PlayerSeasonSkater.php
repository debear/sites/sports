<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NHL\Traits\StatsSkater as StatsSkaterTrait;

class PlayerSeasonSkater extends BasePlayerSeasonStat
{
    use StatsSkaterTrait;
}
