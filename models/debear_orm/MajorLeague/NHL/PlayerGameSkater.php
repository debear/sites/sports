<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NHL\Traits\StatsSkater as StatsSkaterTrait;

class PlayerGameSkater extends BasePlayerGameStat
{
    use StatsSkaterTrait;
}
