<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerHeatmap as BasePlayerHeatmap;
use DeBear\Helpers\Sports\NHL\Traits\Heatmaps as HeatmapsTrait;

class PlayerSeasonSkaterHeatmap extends BasePlayerHeatmap
{
    use HeatmapsTrait;
}
