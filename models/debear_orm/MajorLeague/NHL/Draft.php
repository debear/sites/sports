<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\Draft as BaseDraft;
use DeBear\Helpers\Strings;

class Draft extends BaseDraft
{
    /**
     * The chain of teams the original draft pick went through
     * @return string The chain of draft pick teams
     */
    public function getTeamChainAttribute(): string
    {
        if (($this->team_id == $this->team_id_orig) && !isset($this->team_id_via)) {
            // Nothing of note...
            return '';
        }
        $ret = 'From ' . $this->renderTeam($this->team_id_orig);
        if (isset($this->team_id_via)) {
            $via = [];
            foreach (explode(',', $this->team_id_via) as $team_id) {
                $via[] = $this->renderTeam($team_id);
            }
            $ret .= ' via ' . Strings::naturalJoin($via);
        }
        return $ret;
    }

    /**
     * Load the draft results for a given season
     * @param integer $season The season's draft to load.
     * @return self The resulting ORM object of that season's draft
     */
    public static function load(int $season): self
    {
        return static::query()
            ->where('season', '=', $season)
            ->orderBy('pick')
            ->get();
    }
}
