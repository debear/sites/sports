<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamSeasonGoalie extends BaseTeamSeasonStat
{
    /**
     * Extra query clause
     * @var array
     */
    protected $queryExtra = ['stat_dir', '=', 'for'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'win' => ['s' => 'W', 'f' => 'Wins'],
        'loss' => ['s' => 'L', 'f' => 'Losses'],
        'ot_loss' => ['s' => 'OTL', 'f' => 'OT Losses'],
        'so_loss' => ['s' => 'SOL', 'f' => 'SO Losses'],
        'goals_against' => ['s' => 'GA', 'f' => 'Goals Against'],
        'shots_against' => ['s' => 'SA', 'f' => 'Shots Against'],
        'saves' => ['s' => 'SV', 'f' => 'Saves'],
        'save_pct' => ['s' => 'SV%', 'f' => 'Save %age'],
        'goals_against_avg' => ['s' => 'GAA', 'f' => 'Goals Against Avg'],
        'shutout' => ['s' => 'SHO', 'f' => 'Shutouts'],
        'minutes_played' => ['s' => 'Min', 'f' => 'Minutes Played'],
        'en_goals_against' => ['s' => 'EN GA', 'f' => 'EN Goals Against'],
        'so_goals_against' => ['s' => 'SO GA', 'f' => 'SO Goals Against'],
        'so_shots_against' => ['s' => 'SO Sh', 'f' => 'SO Shots Against'],
        'so_saves' => ['s' => 'SO SV', 'f' => 'SO Saves'],
        'so_save_pct' => ['s' => 'SO %', 'f' => 'SO Save %age'],
        'goals' => ['s' => 'G', 'f' => 'Goals'],
        'assists' => ['s' => 'A', 'f' => 'Assists'],
        'points' => ['s' => 'Pts', 'f' => 'Points'],
        'pims' => ['s' => 'PIMs', 'f' => 'PIMSs'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'win' => 'TBL.win',
        'loss' => 'TBL.loss',
        'ot_loss' => 'TBL.ot_loss',
        'so_loss' => 'TBL.so_loss',
        'goals_against' => 'TBL.goals_against',
        'shots_against' => 'TBL.shots_against',
        'saves' => 'TBL.shots_against - TBL.goals_against',
        'save_pct' => '(TBL.shots_against - TBL.goals_against) / TBL.shots_against',
        'goals_against_avg' => '(TBL.goals_against * 3600) / TBL.minutes_played',
        'shutout' => 'TBL.shutout',
        'minutes_played' => 'TBL.minutes_played',
        'en_goals_against' => 'TBL.en_goals_against',
        'so_goals_against' => 'TBL.so_goals_against',
        'so_shots_against' => 'TBL.so_shots_against',
        'so_saves' => 'TBL.so_shots_against - TBL.so_goals_against',
        'so_save_pct' => '(TBL.so_shots_against - TBL.so_goals_against) / TBL.so_shots_against',
        'goals' => 'TBL.goals',
        'assists' => 'TBL.assists',
        'points' => 'TBL.goals + TBL.assists',
        'pims' => 'TBL.pims',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'goals_against_avg') {
            return sprintf('%0.02f', $this->$column);
        } elseif ($column == 'save_pct') {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        } elseif ($column == 'so_save_pct') {
            return sprintf('%0.01f', $this->$column);
        } elseif ($column == 'minutes_played') {
            return sprintf('%d:%02d', floor($this->$column / 60), $this->$column % 60);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
