<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonSorted as BaseTeamSeasonSorted;

class TeamSeasonGoalieSorted extends BaseTeamSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = TeamSeasonGoalie::class;
}
