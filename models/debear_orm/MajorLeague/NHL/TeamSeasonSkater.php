<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamSeasonSkater extends BaseTeamSeasonStat
{
    /**
     * Extra query clause
     * @var array
     */
    protected $queryExtra = ['stat_dir', '=', 'for'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'goals' => ['s' => 'G', 'f' => 'Goals'],
        'assists' => ['s' => 'A', 'f' => 'Assists'],
        'points' => ['s' => 'Pts', 'f' => 'Points'],
        'plus_minus' => ['s' => '+/-', 'f' => 'Plus/Minus'],
        'pims' => ['s' => 'PIMs', 'f' => 'PIMs'],
        'pens_taken' => ['s' => 'Pen Tk', 'f' => 'Penalties Taken'],
        'pens_drawn' => ['s' => 'Pen Dr', 'f' => 'Penalties Drawn'],
        'pp_goals' => ['s' => 'PPG', 'f' => 'PP Goals'],
        'pp_assists' => ['s' => 'PPA', 'f' => 'PP Assists'],
        'pp_points' => ['s' => 'PPP', 'f' => 'PP Points'],
        'pp_opps' => ['s' => 'PP Op', 'f' => 'PP Opportunities'],
        'pp_pct' => ['s' => 'PP %', 'f' => 'PP %age'],
        'pk_goals' => ['s' => 'PK GA', 'f' => 'PK Goals Allowed'],
        'pk_kills' => ['s' => 'PK Kill', 'f' => 'PK Kills'],
        'pk_opps' => ['s' => 'PK Op', 'f' => 'PK Opportunities'],
        'pk_pct' => ['s' => 'PK %', 'f' => 'PK %age'],
        'sh_goals' => ['s' => 'SHG', 'f' => 'SH Goals'],
        'sh_assists' => ['s' => 'SHA', 'f' => 'SH Assists'],
        'sh_points' => ['s' => 'SHP', 'f' => 'SH Points'],
        'shots' => ['s' => 'SOG', 'f' => 'Shots'],
        'shot_pct' => ['s' => 'Sh %', 'f' => 'Shooting %age'],
        'missed_shots' => ['s' => 'Miss', 'f' => 'Missed Shots'],
        'blocked_shots' => ['s' => 'Blckd', 'f' => 'Blocked Shots'],
        'shots_blocked' => ['s' => 'Blks', 'f' => 'Shots Blocked'],
        'hits' => ['s' => 'Hits', 'f' => 'Hits'],
        'times_hit' => ['s' => 'Hit', 'f' => 'Times Hit'],
        'fo_wins' => ['s' => 'FO W', 'f' => 'Faceoffs Won'],
        'fo_loss' => ['s' => 'FO L', 'f' => 'Faceoffs Lost'],
        'fo_pct' => ['s' => 'FO %', 'f' => 'Faceoff %age'],
        'so_goals' => ['s' => 'SO G', 'f' => 'SO Goals'],
        'so_shots' => ['s' => 'SO Sh', 'f' => 'SO Shots'],
        'so_pct' => ['s' => 'SO %', 'f' => 'SO %age'],
        'giveaways' => ['s' => 'Give', 'f' => 'Giveaways'],
        'takeaways' => ['s' => 'Take', 'f' => 'Takeaways'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'goals' => 'TBL.goals',
        'assists' => 'TBL.assists',
        'points' => 'TBL.goals + TBL.assists',
        'plus_minus' => 'TBL.plus_minus',
        'pims' => 'TBL.pims',
        'pens_taken' => 'TBL.pens_taken',
        'pens_drawn' => 'TBL.pens_drawn',
        'pp_goals' => 'TBL.pp_goals',
        'pp_assists' => 'TBL.pp_assists',
        'pp_points' => 'TBL.pp_goals + TBL.pp_assists',
        'pp_opps' => 'TBL.pp_opps',
        'pp_pct' => '(100 * TBL.pp_goals) / TBL.pp_opps',
        'pk_goals' => 'TBL.pk_goals',
        'pk_kills' => 'TBL.pk_kills',
        'pk_opps' => 'TBL.pk_opps',
        'pk_pct' => '(100 * TBL.pk_kills) / TBL.pk_opps',
        'sh_goals' => 'TBL.sh_goals',
        'sh_assists' => 'TBL.sh_assists',
        'sh_points' => 'TBL.sh_goals + TBL.sh_assists',
        'shots' => 'TBL.shots',
        'shot_pct' => '(100 * TBL.goals) / TBL.shots',
        'missed_shots' => 'TBL.missed_shots',
        'blocked_shots' => 'TBL.blocked_shots',
        'shots_blocked' => 'TBL.shots_blocked',
        'hits' => 'TBL.hits',
        'times_hit' => 'TBL.times_hit',
        'fo_wins' => 'TBL.fo_wins',
        'fo_loss' => 'TBL.fo_loss',
        'fo_pct' => '(100 * TBL.fo_wins) / (TBL.fo_wins + TBL.fo_loss)',
        'so_goals' => 'TBL.so_goals',
        'so_shots' => 'TBL.so_shots',
        'so_pct' => '(100 * TBL.so_goals) / TBL.so_shots',
        'giveaways' => 'TBL.giveaways',
        'takeaways' => 'TBL.takeaways',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, -4) == '_pct') {
            return sprintf('%0.01f', $this->$column);
        } elseif ($column == 'plus_minus') {
            return sprintf('%s%d', $this->$column > 0 ? '+' : '', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
