<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\GameLineup as BaseGameLineup;
use DeBear\ORM\Sports\MajorLeague\Schedule as BaseSchedule;
use Illuminate\Database\Query\Builder;

class GameLineup extends BaseGameLineup
{
    /**
     * Load the starting lineups for a given list of games
     * @param BaseSchedule $schedule The schedule ORM object for which the starting lineups are requested.
     * @return self The resulting ORM object containing all game for the lineup date being passed in
     */
    public static function loadByGame(BaseSchedule $schedule): self
    {
        $tbl_lineup = static::getTable();
        $tbl_pos = Position::getTable();
        $tbl_pos_group = PositionGroups::getTable();
        $query = static::query()
            ->select("$tbl_lineup.*")
            ->selectRaw("$tbl_pos.pos_short AS pos")
            ->selectRaw("$tbl_pos_group.order AS sort_order")
            ->selectRaw(
                "CONCAT($tbl_lineup.game_type, '-', $tbl_lineup.game_id, '-', $tbl_lineup.team_id) AS game_team_ref"
            )->join($tbl_pos, "$tbl_pos.pos_code", '=', "$tbl_lineup.pos")
            ->join($tbl_pos_group, "$tbl_pos_group.posgroup_id", '=', "$tbl_pos.posgroup_id");
        // Determine the relevant games by game type.
        foreach ($schedule->unique('game_type') as $game_type) {
            $query->orWhere(function (Builder $where) use ($game_type, $schedule, $tbl_lineup) {
                $where->where("$tbl_lineup.season", $schedule->season)
                    ->where("$tbl_lineup.game_type", $game_type)
                    ->whereIn("$tbl_lineup.game_id", $schedule->where('game_type', $game_type)->unique('game_id'));
            });
        }
        return $query->where("$tbl_lineup.started", 1)
            ->orderBy("$tbl_lineup.game_id")
            ->orderBy("$tbl_lineup.team_id")
            ->orderBy('sort_order')
            ->orderBy("$tbl_lineup.jersey")
            ->get()
            ->loadSecondary(['players']);
    }
}
