<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NHL\Traits\StatsGoalie as StatsGoalieTrait;

class PlayerSeasonGoalie extends BasePlayerSeasonStat
{
    use StatsGoalieTrait;
}
