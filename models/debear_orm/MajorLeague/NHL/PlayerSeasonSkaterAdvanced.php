<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NHL\Traits\StatsSkaterAdvanced as StatsSkaterAdvancedTrait;

class PlayerSeasonSkaterAdvanced extends BasePlayerSeasonStat
{
    use StatsSkaterAdvancedTrait;
}
