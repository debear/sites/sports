<?php

namespace DeBear\ORM\Sports\MajorLeague\NHL;

use DeBear\ORM\Sports\MajorLeague\Player as BasePlayer;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;

class Player extends BasePlayer
{
    /**
     * Get the relevant field for the custom field section of the player page
     * @return string The value for the player's header row custom field
     */
    public function getHeaderCustomAttribute(): string
    {
        return $this->shoots_catches ?? '';
    }

    /**
     * Determine the team each player in the dataset currently plays for
     * @return void
     */
    protected function loadSecondaryLastRoster(): void
    {
        // Determine the current roster date.
        $max_date = TeamRoster::query()
            ->selectRaw('the_date')
            ->orderByDesc('season')
            ->orderByDesc('the_date')
            ->limit(1)
            ->get()
            ->the_date;
        // Then get the roster info for the player (on the expectation we are only loading a single player).
        $tbl_roster = TeamRoster::getTable();
        $tbl_pos = Position::getTable();
        $rosters = TeamRoster::query()
            ->select("$tbl_roster.*")
            ->selectRaw("$tbl_pos.pos_short AS pos")
            ->selectRaw("$tbl_roster.the_date = ? AS is_latest", [$max_date])
            ->join($tbl_pos, "$tbl_pos.pos_code", '=', "$tbl_roster.pos")
            ->where("$tbl_roster.season", '<=', FrameworkConfig::get('debear.setup.season.viewing'))
            ->where("$tbl_roster.player_id", $this->unique('player_id'))
            ->orderByDesc("$tbl_roster.season")
            ->orderByDesc("$tbl_roster.the_date")
            ->limit(1)
            ->get();
        // Expand team info on these rows.
        $teams = Team::query()
            ->whereIn('team_id', $rosters->unique('team_id'))
            ->get();
        foreach ($rosters as $row) {
            $row->team = $teams->where('team_id', $row->team_id ?? '');
        }
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['lastRoster'] = $rosters->where('player_id', $player['player_id']);
        }
    }

    /**
     * Determine the various chart data across the given season for the players in the dataset
     * @return void
     */
    protected function loadSecondaryHeatmaps(): void
    {
        // Validate the 'heatmap' argument.
        $type_opt = FrameworkConfig::get("debear.setup.stats.details.player.{$this->group}.heatmaps");
        $type = Request::get('heatmap');
        if (!isset($type) || !in_array($type, $type_opt)) {
            $type = $type_opt[0];
        }
        // Build our request.
        $class = FrameworkConfig::get("debear.setup.stats.details.player.{$this->group}.class.charts");
        $tbl_heatmap = $class::getTable();
        $class_stat = FrameworkConfig::get("debear.setup.stats.details.player.{$this->group}.class.stat");
        $tbl_stat = $class_stat::getTable();
        $season = FrameworkConfig::get('debear.setup.season');
        $query = $class::query($tbl_stat)
            ->select(["$tbl_stat.player_id"])
            ->selectRaw("UNCOMPRESS($tbl_heatmap.$type) AS zones_raw")
            ->selectRaw('? AS zone_type', [$type]);
        // Comparison average?
        $key = "{$this->group}:{$type}";
        $is_avg = ['skater:shots' => 'shot_pct'];
        if (isset($is_avg[$key])) {
            $cmp_when = $cmp_avg = [];
            foreach ($this->data as $player) {
                $cmp_when[] = 'WHEN ? THEN ?';
                $cmp_avg[] = $player['player_id'];
                $cmp_avg[] = $player['seasonStats']->{$is_avg[$key]} / 100;
            }
            $query->selectRaw("CASE $tbl_heatmap.player_id " . join(' ', $cmp_when) . ' END AS cmp_avg', $cmp_avg);
        }
        // Which fields are available?
        foreach ($type_opt as $opt) {
            $query->selectRaw("$tbl_heatmap.$opt IS NOT NULL AS has_{$opt}");
        }
        $zones = $query->leftJoin($tbl_heatmap, function ($join) use ($tbl_heatmap, $tbl_stat) {
            $join->on("$tbl_heatmap.season", '=', "$tbl_stat.season")
                ->on("$tbl_heatmap.season_type", '=', "$tbl_stat.season_type")
                ->on("$tbl_heatmap.player_id", '=', "$tbl_stat.player_id");
        })->where([
            ["$tbl_stat.season", '=', $season['viewing']],
            ["$tbl_stat.season_type", '=', $season['type']],
        ])->whereIn("$tbl_stat.player_id", $this->unique('player_id'))
            ->get();
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['heatmap'] = $zones->where('player_id', $player['player_id']);
        }
    }
}
