<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;

class GameOdds extends Instance
{
    /**
     * Converted home moneyline from decimal to american format
     * @return integer The home moneyline in american format
     */
    public function getHomeLineUSAttribute(): int
    {
        return $this->decimalToAmerican($this->home_line);
    }

    /**
     * Converted visitor moneyline from decimal to american format
     * @return integer The visitor moneyline in american format
     */
    public function getVisitorLineUSAttribute(): int
    {
        return $this->decimalToAmerican($this->visitor_line);
    }

    /**
     * Worker method for converting decimal odds into american format
     * @param float $decimal The decimal odds to be converted.
     * @return integer The supplied decimal odds in amercian format
     */
    protected function decimalToAmerican(float $decimal): int
    {
        // Decimal odds above 2.0 return a positive integer, below it is negative.
        return $decimal > 2 ? ($decimal - 1) * 100 : -100 / ($decimal - 1);
    }
}
