<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Base\Instance;

class GameDrive extends Instance
{
    /**
     * Combine the start position fields into a single value
     * @return string A combined starting position value
     */
    public function getStartPosAttribute(): string
    {
        $start_half = ($this->start_pos_half == $this->team_id ? 'Own' : 'Opp');
        return $this->combinedFieldPosition($start_half, $this->start_pos_yd);
    }

    /**
     * Determine the end position fields and combine into a single value
     * @return string A combined end position value
     */
    public function getEndPosAttribute(): string
    {
        // Calculate the position.
        $end_pos_half = ($this->start_pos_half == $this->team_id ? 'Own' : 'Opp');
        if ($end_pos_half == 'Own') {
            $end_pos_yd = $this->start_pos_yd + $this->yards_net;
            if ($end_pos_yd > 50) {
                $end_pos_yd = 100 - $end_pos_yd;
                $end_pos_half = 'Opp';
            }
        } else {
            $end_pos_yd = $this->start_pos_yd - $this->yards_net;
            if ($end_pos_yd > 50) {
                $end_pos_yd = 100 - $end_pos_yd;
                $end_pos_half = 'Own';
            }
        }
        // Combine.
        return $this->combinedFieldPosition($end_pos_half, $end_pos_yd);
    }

    /**
     * Combine the separate team ID / yardage position in to a string
     * @param string  $half The team ID of the half in question.
     * @param integer $yd   The yardage position.
     * @return string The combined position value
     */
    protected function combinedFieldPosition(?string $half, int $yd): string
    {
        if ($yd == 50 || !isset($half)) {
            // Midfield.
            return 'Midfield';
        } elseif (!$yd) {
            // Touchdown!
            return 'Endzone';
        }
        return "$half $yd";
    }
}
