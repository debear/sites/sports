<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\Team as BaseTeam;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class Team extends BaseTeam
{
    /**
     * Get the team's home venue
     * @return string The name of the team's current home venue
     */
    public function getVenueAttribute(): string
    {
        return $this->currentVenue->stadium;
    }

    /**
     * Prepare the various data for the power ranks chart rendering
     * @return array The various components for the chart
     */
    protected function powerRanksChartSetup(): array
    {
        $weeks = $ranks = [];
        $all_weeks = PowerRankingsWeeks::query()
            ->where('season', $this->power_ranks->count() ? $this->power_ranks->max('season') : 2099)
            ->orderBy('calc_date')
            ->get();
        // Default values.
        foreach ($all_weeks as $week) {
            $week_ref = $week->name;
            $weeks[$week_ref] = $week->name_full;
            $ranks[$week_ref] = null;
        }
        // Actual values.
        foreach ($this->power_ranks as $r) {
            $ranks[$r->week->name] = $r->rank;
        }
        return [$weeks, $ranks];
    }

    /**
     * Get the recent schedule for the given teams
     * @return void
     */
    protected function loadSecondaryRecentSchedule(): void
    {
        $this->loadSecondarySchedule(); // Pre-req for the recent lookup.
        foreach (array_keys($this->data) as $i) {
            // Re-use the full schedule object.
            $this->data[$i]['recentSchedule'] = $this->data[$i]['schedule'];
        }
    }

    /**
     * Get the individual team rosters for the given teams
     * @return void
     */
    protected function loadSecondaryRosters(): void
    {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        // Determine the roster date.
        $last_date = TeamRoster::query()
            ->select(['game_type', 'week'])
            ->where('season', $season)
            ->orderByRaw('game_type = "regular"') // We want 'playoff' before 'regular'.
            ->orderByDesc('week')
            ->limit(1)
            ->get();
        // And then get the rosters.
        $tbl_rosters = TeamRoster::getTable();
        $tbl_pos_raw = PositionRaw::getTable();
        $rosters = TeamRoster::query()
            ->select("$tbl_rosters.*")
            ->selectRaw("IFNULL($tbl_pos_raw.pos_code, $tbl_rosters.pos) AS pos")
            ->leftJoin($tbl_pos_raw, "$tbl_pos_raw.pos_raw", '=', "$tbl_rosters.pos")
            ->where([
                ["$tbl_rosters.season", '=', $season],
                ["$tbl_rosters.game_type", '=', $last_date->game_type],
                ["$tbl_rosters.week", '=', $last_date->week],
            ])->whereIn("$tbl_rosters.team_id", $this->unique('team_id'))
            ->orderByRaw("$tbl_rosters.`player_status` <> 'active'")
            ->orderByRaw("$tbl_rosters.`jersey` IS NULL")
            ->orderBy("$tbl_rosters.jersey")
            ->get()
            ->loadSecondary(['players']);
        // Now tie-together to the teams.
        foreach ($this->data as $i => $team) {
            $this->data[$i]['roster'] = $rosters->where('team_id', $team['team_id']);
        }
    }

    /**
     * Get the power rank history for the given teams
     * @return void
     */
    protected function loadSecondaryPowerRankings(): void
    {
        $power_ranks = PowerRankings::query()
            ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
            ->whereIn('team_id', $this->unique('team_id'))
            ->orderByRaw('game_type = "playoff"') // For reverse order: regular == 0, playoff == 1.
            ->orderBy('week')
            ->get()
            ->loadSecondary(['weeks']);
        // Now tie-together to the teams.
        foreach ($this->data as $i => $team) {
            $this->data[$i]['power_ranks'] = $power_ranks->where('team_id', $team['team_id']);
        }
    }
}
