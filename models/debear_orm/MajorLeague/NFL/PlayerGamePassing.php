<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPassing as StatsPassingTrait;

class PlayerGamePassing extends BasePlayerGameStat
{
    use StatsPassingTrait;
}
