<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayoffSeries as BasePlayoffSeries;
use DeBear\Helpers\Format;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class PlayoffSeries extends BasePlayoffSeries
{
    /**
     * Load the playoff series for a given season
     * @param integer $season The season's playoff we are viewing.
     * @param integer $code   An optional series code. If not supplied, all series will be loaded.
     * @return self An ORM object with the appropriate details
     */
    public static function load(int $season, ?int $code = null): self
    {
        // Use the parent as the main worker.
        $matchups = parent::load($season, $code);
        // But then add schedule information.
        return $matchups->loadSecondary(['schedule']);
    }

    /**
     * Load the matchups schedule instances
     * @return void
     */
    public function loadSecondarySchedule(): void
    {
        $query = Schedule::query();
        foreach ($this->data as $matchup) {
            $query->orWhere(function (Builder $where) use ($matchup) {
                $where->where([
                    ['season', '=', $matchup['season']],
                    ['game_type', '=', 'playoff'],
                    ['game_id', '=', $matchup['game_id']],
                ]);
            });
        }
        $schedule = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        foreach ($this->data as $i => $game) {
            if (isset($game['game_id'])) {
                $this->data[$i]['game'] = $schedule->where('game_id', $game['game_id']);
            }
        }
    }

    /**
     * Get the title for the championship round of the playoffs
     * @param integer $season The season's playoffs being viewed.
     * @return string The series title
     */
    public static function champSeriesTitle(int $season): string
    {
        // Number is in Roman Numerals... except SB 50.
        return 'Super Bowl ' . ($season == 2015 ? '50' : Format::decimalToRoman($season - 1965));
    }
}
