<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\Player as BasePlayer;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\Helpers\Format;
use DeBear\Helpers\Sports\Namespaces;

class Player extends BasePlayer
{
    /**
     * Determine the team each player in the dataset currently plays for
     * @return void
     */
    protected function loadSecondaryLastRoster(): void
    {
        // Determine the roster dates.
        $max_calc = 'MAX(CONCAT(season, ":", IF(game_type = "regular", 1, 2), ":", LPAD(week, 2, 0)))';
        $last_dates = TeamRoster::query()
            ->select('player_id')
            ->selectRaw("$max_calc AS last_week")
            ->where('season', '<=', FrameworkConfig::get('debear.setup.season.viewing'))
            ->whereIn('player_id', $this->unique('player_id'))
            ->get();
        $max_date = TeamRoster::query()
            ->selectRaw("$max_calc AS max_date")
            ->get()
            ->max_date;
        // Then apply to the rosters.
        $tbl_roster = TeamRoster::getTable();
        $tbl_pos_raw = PositionRaw::getTable();
        $tbl_pos = Position::getTable();
        $query = TeamRoster::query()
            ->select("$tbl_roster.*")
            ->selectRaw("$tbl_pos.pos_code AS pos")
            ->selectRaw("$tbl_pos.type AS pos_type")
            ->selectRaw("$max_calc = ? AS is_latest", [$max_date])
            ->join($tbl_pos_raw, "$tbl_pos_raw.pos_raw", '=', "$tbl_roster.pos")
            ->join($tbl_pos, "$tbl_pos.pos_code", '=', "$tbl_pos_raw.pos_code");
        foreach ($last_dates as $row) {
            list($season, $game_type, $week) = explode(':', $row->last_week ?? '0:regular:0');
            $query->orWhere(function (Builder $where) use ($tbl_roster, $season, $game_type, $week, $row) {
                $where->where([
                    ["$tbl_roster.season", '=', $season],
                    ["$tbl_roster.game_type", '=', ($game_type == 1 ? 'regular' : 'playoff')],
                    ["$tbl_roster.week", '=', ltrim($week, '0')],
                    ["$tbl_roster.player_id", '=', $row->player_id],
                ]);
            });
        }
        $rosters = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        // Expand team info on these rows.
        $teams = Team::query()
            ->whereIn('team_id', $rosters->unique('team_id'))
            ->get();
        foreach ($rosters as $row) {
            $row->team = $teams->where('team_id', $row->team_id ?? '');
        }
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['lastRoster'] = $rosters->where('player_id', $player['player_id']);
        }
    }

    /**
     * Determine the main group applicable for each player in the dataset played
     * @return void
     */
    protected function loadSecondarySortGroups(): void
    {
        foreach ($this->data as $i => $player) {
            $group = $this->lastRoster->pos_type ?? 'misc';
            $key = (!isset($player['group']) || $player['group'] == $group ? 'group' : 'group_core');
            $this->data[$i][$key] = $group;
        }
    }
    /**
     * Determine the season's in which the players in the dataset played
     * @return void
     */
    protected function loadSecondarySeasons(): void
    {
        // Get the seasons according to the player's position.
        $all = PlayerSeason::query()
            ->distinct()
            ->select(['player_id', 'season', 'gp'])
            ->whereIn('player_id', $this->unique('player_id'))
            ->orderBy('player_id')
            ->orderByDesc('season')
            ->get();
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $by_player = $all->where('player_id', $player['player_id']);
            $this->data[$i]['seasons'] = $by_player->unique('season');
            $this->data[$i]['careerGP'] = $by_player->sum('gp');
        }
    }

    /**
     * Determine the appropriate season stats for each player in the dataset according to their position
     * @return void
     */
    protected function loadSecondarySummaryStats(): void
    {
        $season = $this->getLatestSeason();
        // Get the stats according to the player's position.
        $stats = $names = [];
        foreach ($this->unique('group') as $type) {
            $stats[$type] = $names[$type] = [];
            $config = FrameworkConfig::get("debear.setup.players.groups.$type.summary");
            if (isset($config)) {
                $meta = (new $config['class']())->getSeasonStatMeta();
                foreach ($config['fields'] as $column) {
                    $stats[$type][$column] = $config['class']::load($season, 'regular', $column, [
                        'player_id' => $this->where('group', $type)->unique('player_id'),
                    ]);
                    $names[$type][$column] = $meta[$column];
                }
            }
        }
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $summary = [];
            if (isset($stats[$player['group']])) {
                foreach ($stats[$player['group']] as $column => $data) {
                    $row = $data->where('player_id', $player['player_id']);
                    if ($data->count()) {
                        $summary[$column] = [
                            'label' => $names[$player['group']][$column]['s'],
                            'value' => $row->format($column),
                            'rank' => isset($row->stat_pos) ? Format::ordinal($row->stat_pos) : 'Unranked',
                        ];
                    }
                }
            }
            if (count($summary)) {
                $this->data[$i]['statSummary'] = $summary;
            }
        }
    }

    /**
     * Determine the season's stats of players in the dataset
     * @param array $seasons An optional list of seasons to narrow our search down by.
     * @return void
     */
    protected function loadSecondaryCareerStats(array $seasons = []): void
    {
        // Get the stats according to the player's position.
        $by_player = [];
        foreach ($this->unique('group') as $type) {
            $config = FrameworkConfig::get("debear.setup.players.groups.$type.detailed");
            $players = $this->where('group', $type)->unique('player_id');
            // Start building our query.
            $tbl_misc = PlayerSeason::getTable();
            $query = PlayerSeason::query();
            $query->select([
                "$tbl_misc.season",
                "$tbl_misc.season_type",
                "$tbl_misc.player_id",
            ]);
            // Add the standard ("misc") columns.
            $calc_columns = (new PlayerSeason())->getSeasonStatColumns();
            foreach (str_replace('TBL.', "$tbl_misc.", $calc_columns) as $col => $val) {
                $query->selectRaw("$val AS `misc_$col`");
            }
            // Then any additional data as joined rows.
            foreach ($config as $stat) {
                $class = "PlayerSeason{$stat}";
                $prefix = strtolower($stat);
                $tbl_join = Namespaces::callStatic($class, 'getTable');
                // Add our join to make the extra columns available.
                $query->leftJoin($tbl_join, function ($join) use ($tbl_misc, $tbl_join) {
                    $join->on("$tbl_join.season", '=', "$tbl_misc.season")
                        ->on("$tbl_join.season_type", '=', "$tbl_misc.season_type")
                        ->on("$tbl_join.player_id", '=', "$tbl_misc.player_id")
                        ->on("$tbl_join.team_id", '=', "$tbl_misc.team_id");
                });
                // And now the columns.
                $calc_columns = Namespaces::createObject($class)->getSeasonStatColumns();
                foreach (str_replace('TBL.', "$tbl_join.", $calc_columns) as $col => $val) {
                    $query->selectRaw("$val AS `{$prefix}_{$col}`");
                }
            }
            // Logic to determine if a team row represents the aggregate season total or not.
            $query->selectRaw("IF(SUBSTRING($tbl_misc.team_id, 1, 1) = '_', 'Total', $tbl_misc.team_id) AS team_id");
            // Process the rest of our query.
            $query->selectRaw('? AS stat_group', [$type]);
            // This stat group is always available in dropdowns.
            $query->selectRaw("1 AS `can_{$type}`");
            // Is there a secondary element available?
            $principal = ($this->group_core ?? $this->group);
            if ($principal != 'defense') {
                $all_alt = array_filter([$this->group, $this->group_core ?? '', 'defense', 'returns']);
                foreach ($all_alt as $prefix) {
                    if ($prefix == $type) {
                        continue;
                    }
                    $can = [];
                    foreach (FrameworkConfig::get("debear.setup.players.groups.$prefix.detailed") as $i => $alt) {
                        $alt_class = "PlayerSeason$alt";
                        $tbl_alt = Namespaces::callStatic($alt_class, 'getTable');
                        $can[] = "$tbl_alt.season IS NOT NULL";
                        $query->leftJoin($tbl_alt, function ($join) use ($tbl_misc, $tbl_alt) {
                            $join->on("$tbl_alt.season", '=', "$tbl_misc.season")
                                ->on("$tbl_alt.season_type", '=', "$tbl_misc.season_type")
                                ->on("$tbl_alt.player_id", '=', "$tbl_misc.player_id")
                                ->on("$tbl_alt.team_id", '=', "$tbl_misc.team_id");
                        });
                    }
                    if (count($can)) {
                        $query->selectRaw(join(' OR ', $can) . " AS `can_$prefix`");
                    }
                }
            }
            // Finish our query.
            if (count($seasons)) {
                $query->whereIn("$tbl_misc.season", $seasons);
            }
            $season_stats = $query->whereIn("$tbl_misc.player_id", $players)
                ->orderByDesc("$tbl_misc.season")
                ->orderByRaw("$tbl_misc.`season_type` = 'playoff' DESC") // Regular season first.
                ->orderBy("$tbl_misc.team_order")
                ->groupBy([
                    "$tbl_misc.season",
                    "$tbl_misc.season_type",
                    "$tbl_misc.player_id",
                    "$tbl_misc.team_id",
                ])->get();
            // Break down by player.
            foreach ($players as $player_id) {
                $by_player[$player_id] = $season_stats->where('player_id', $player_id);
            }
        }
        // Now tie-together.
        $key = count($seasons) ? 'seasonStats' : 'careerStats';
        foreach ($this->data as $i => $player) {
            if (isset($by_player[$player['player_id']])) {
                $this->data[$i][$key] = $by_player[$player['player_id']];
            }
        }
    }

    /**
     * Determine the games played over the given season for the players in the dataset
     * @param integer $max_games The most games to return, or 0 to return every game played.
     * @return void
     */
    protected function loadSecondarySeasonGames(int $max_games = 0): void
    {
        // Get the stats according to the player's position.
        $by_player = [];
        foreach ($this->unique('group') as $type) {
            $config = FrameworkConfig::get("debear.setup.players.groups.$type.detailed");
            $players = $this->where('group', $type)->unique('player_id');
            // Start building our query.
            $tbl_misc = GameLineup::getTable();
            $query = GameLineup::query();
            $query->select([
                "$tbl_misc.season",
                "$tbl_misc.game_type",
                "$tbl_misc.week",
                "$tbl_misc.game_id",
                "$tbl_misc.player_id",
                "$tbl_misc.team_id",
            ]);
            $query->selectRaw("$tbl_misc.status IN ('starter', 'substitute') AS `misc_gp`")
                ->selectRaw("$tbl_misc.status = 'starter' AS `misc_gs`");
            // Then any additional data as joined rows.
            foreach ($config as $group) {
                $class = "PlayerGame{$group}";
                $prefix = strtolower($group);
                $tbl_join = Namespaces::callStatic($class, 'getTable');
                // Add our join to make the extra columns available.
                $query->leftJoin($tbl_join, function ($join) use ($tbl_misc, $tbl_join) {
                    $join->on("$tbl_join.season", '=', "$tbl_misc.season")
                        ->on("$tbl_join.game_type", '=', "$tbl_misc.game_type")
                        ->on("$tbl_join.week", '=', "$tbl_misc.week")
                        ->on("$tbl_join.game_id", '=', "$tbl_misc.game_id")
                        ->on("$tbl_join.player_id", '=', "$tbl_misc.player_id");
                });
                // And now the columns.
                $calc_columns = Namespaces::createObject($class)->getGameStatColumns();
                foreach (str_replace('TBL.', "$tbl_join.", $calc_columns) as $col => $val) {
                    $query->selectRaw("$val AS `{$prefix}_{$col}`");
                }
            }
            $query->selectRaw('? AS stat_group', [$type]);
            // This stat group is always available in dropdowns.
            $query->selectRaw("1 AS `can_{$type}`");
            // Is there a secondary element available?
            $principal = ($this->group_core ?? $this->group);
            if ($principal != 'defense') {
                $all_alt = array_filter([$this->group, $this->group_core ?? '', 'defense', 'returns']);
                foreach ($all_alt as $prefix) {
                    if ($prefix == $this->group) {
                        continue;
                    }
                    $can = [];
                    foreach (FrameworkConfig::get("debear.setup.players.groups.$prefix.detailed") as $i => $alt) {
                        $alt_class = "PlayerGame$alt";
                        $tbl_alt_raw = Namespaces::callStatic($alt_class, 'getTable');
                        $tbl_alt = "ALT_{$tbl_alt_raw}";
                        $can[] = "$tbl_alt.season IS NOT NULL";
                        $query->leftJoin("$tbl_alt_raw AS $tbl_alt", function ($join) use ($tbl_misc, $tbl_alt) {
                            $join->on("$tbl_alt.season", '=', "$tbl_misc.season")
                                ->on("$tbl_alt.game_type", '=', "$tbl_misc.game_type")
                                ->on("$tbl_alt.week", '=', "$tbl_misc.week")
                                ->on("$tbl_alt.game_id", '=', "$tbl_misc.game_id")
                                ->on("$tbl_alt.player_id", '=', "$tbl_misc.player_id");
                        });
                    }
                    if (count($can)) {
                        $query->selectRaw(join(' OR ', $can) . " AS `can_$prefix`");
                    }
                }
            }
            // Process the rest of our query.
            $season = $this->getLatestSeason();
            $game_stats = $query->where("$tbl_misc.season", $season)
                ->whereIn("$tbl_misc.player_id", $players)
                ->orderByRaw("$tbl_misc.`game_type` = 'playoff' DESC") // Playoffs first.
                ->orderByDesc("$tbl_misc.week")
                ->groupBy([
                    "$tbl_misc.season",
                    "$tbl_misc.game_type",
                    "$tbl_misc.week",
                    "$tbl_misc.game_id",
                    "$tbl_misc.player_id",
                ])->get()->loadSecondary(['games']);
            // Break down by player.
            foreach ($players as $player_id) {
                $by_player[$player_id] = $game_stats->where('player_id', $player_id);
            }
        }
        // Now tie-together.
        $key = ($max_games ? 'recentGames' : 'seasonGames');
        foreach ($this->data as $i => $player) {
            if (isset($by_player[$player['player_id']])) {
                $this->data[$i][$key] = $by_player[$player['player_id']];
            }
        }
    }

    /**
     * Determine any (current) injury news for players in the dataset
     * @return void
     */
    protected function loadSecondaryInjuryLatest(): void
    {
        $season = $this->getLatestSeason();
        $data = PlayerInjury::query()
            ->where('season', $season)
            ->whereIn('player_id', $this->unique('player_id'))
            ->orderByRaw('game_type = "regular"') // We want 'playoff' before 'regular'.
            ->orderByDesc('week')
            ->get();
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['injuryLatest'] = $data->where('player_id', $player['player_id']);
        }
    }
}
