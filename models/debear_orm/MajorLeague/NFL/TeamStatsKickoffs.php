<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsKickoffs extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'num' => ['s' => 'Num', 'f' => 'Number'],
        'end_zone' => ['s' => 'EZ', 'f' => 'End Zone'],
        'touchback' => ['s' => 'TB', 'f' => 'Touchbacks'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'num' => 'TBL.num',
        'end_zone' => 'TBL.end_zone',
        'touchback' => 'TBL.touchback',
    ];
}
