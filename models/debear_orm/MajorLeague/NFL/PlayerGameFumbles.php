<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsFumbles as StatsFumblesTrait;

class PlayerGameFumbles extends BasePlayerGameStat
{
    use StatsFumblesTrait;
}
