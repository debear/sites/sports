<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsKicking as StatsKickingTrait;

class PlayerGameKicking extends BasePlayerGameStat
{
    use StatsKickingTrait;
}
