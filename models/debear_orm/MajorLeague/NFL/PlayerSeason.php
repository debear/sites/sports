<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\NFL\Traits\StatsMisc as StatsMiscTrait;
use DeBear\Helpers\Sports\Namespaces;

class PlayerSeason extends BasePlayerSeasonStat
{
    use StatsMiscTrait;

    /**
     * Instantiated game stat objects
     * @var array
     */
    protected $stat_groups = [];

    /**
     * Setup the internal objects for parsing / formatting stats.
     * @return void
     */
    public function prepareStats(): void
    {
        $raw = $this->toArray();
        foreach (FrameworkConfig::get("debear.setup.players.groups.{$raw['stat_group']}.detailed") as $group) {
            // Get the stats in the current row with this prefix.
            $group_lc = strtolower($group);
            $prefix = $group_lc . '_';
            $prefix_len = strlen($prefix);
            $stats = array_filter($raw, function ($key) use ($prefix, $prefix_len) {
                return (substr($key, 0, $prefix_len) === $prefix);
            }, ARRAY_FILTER_USE_KEY);
            $data = array_combine(array_map(function ($key) use ($prefix_len) {
                return substr($key, $prefix_len);
            }, array_keys($stats)), array_values($stats));
            // Add this to the list.
            $this->stat_groups[$group_lc] = Namespaces::createObject("PlayerSeason{$group}", [$data]);
        }
    }

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        // Determine the object that handles the formatting.
        list($group) = explode('_', $column);
        // Which object handles the formatting?
        if ($group == 'misc') {
            // An exception: stats we have.
            return parent::format($column);
        }
        $column = substr($column, strlen($group) + 1);
        return $this->stat_groups[$group]->format($column);
    }
}
