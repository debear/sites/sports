<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsFumbles as StatsFumblesTrait;

class PlayerSeasonFumbles extends BasePlayerSeasonStat
{
    use StatsFumblesTrait;
}
