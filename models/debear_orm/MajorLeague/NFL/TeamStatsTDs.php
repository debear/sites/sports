<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsTDs extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'num_total' => ['s' => 'Total', 'f' => 'Total'],
        'num_pass' => ['s' => 'Pass', 'f' => 'Passing'],
        'num_rush' => ['s' => 'Rush', 'f' => 'Rushing'],
        'num_int' => ['s' => 'Int', 'f' => 'Interception Returns'],
        'num_fumbles' => ['s' => 'Fmb', 'f' => 'Fumble Returns'],
        'num_ko' => ['s' => 'KO', 'f' => 'Kickoff Returns'],
        'num_punt' => ['s' => 'Punt', 'f' => 'Punt Returns'],
        'num_other' => ['s' => 'Other', 'f' => 'Other'],
        'safties' => ['s' => 'Sfty', 'f' => 'Safties'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'num_total' => 'TBL.num_pass + TBL.num_rush + TBL.num_int + TBL.num_fumbles'
            . ' + TBL.num_ko + TBL.num_punt + TBL.num_other',
        'num_pass' => 'TBL.num_pass',
        'num_rush' => 'TBL.num_rush',
        'num_int' => 'TBL.num_int',
        'num_fumbles' => 'TBL.num_fumbles',
        'num_ko' => 'TBL.num_ko',
        'num_punt' => 'TBL.num_punt',
        'num_other' => 'TBL.num_other',
        'safties' => 'TBL.safties',
    ];
}
