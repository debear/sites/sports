<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\GameLineup as BaseGameLineup;
use DeBear\Helpers\Sports\NFL\Traits\StatsMisc as StatsMiscTrait;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\Helpers\Sports\Namespaces;

class GameLineup extends BaseGameLineup
{
    use StatsMiscTrait;

    /**
     * Instantiated game stat objects
     * @var array
     */
    protected $stat_groups = [];

    /**
     * Setup the internal objects for parsing / formatting stats.
     * @return void
     */
    public function prepareStats(): void
    {
        $raw = $this->toArray();
        foreach (FrameworkConfig::get("debear.setup.players.groups.{$raw['stat_group']}.detailed") as $group) {
            // Get the stats in the current row with this prefix.
            $group_lc = strtolower($group);
            $prefix = $group_lc . '_';
            $prefix_len = strlen($prefix);
            $stats = array_filter($raw, function ($key) use ($prefix, $prefix_len) {
                return (substr($key, 0, $prefix_len) === $prefix);
            }, ARRAY_FILTER_USE_KEY);
            $data = array_combine(array_map(function ($key) use ($prefix_len) {
                return substr($key, $prefix_len);
            }, array_keys($stats)), array_values($stats));
            // Add this to the list.
            $this->stat_groups[$group_lc] = Namespaces::createObject("PlayerGame{$group}", [$data]);
        }
    }

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        // Determine the object that handles the formatting.
        list($group) = explode('_', $column);
        // Which object handles the formatting?
        if ($group == 'misc') {
            // An exception: stats we have.
            $fmt = '<span class="fa fa-' . ($this->$column ? 'check' : 'times') . '-circle"></span>';
            return $column == 'misc_gs' && !$this->misc_gp ? '&ndash;' : $fmt;
        }
        $column = substr($column, strlen($group) + 1);
        return $this->stat_groups[$group]->format($column);
    }

    /**
     * Load the NFL games for a given row
     * @return void
     */
    protected function loadSecondaryGames(): void
    {
        $query = Schedule::query()
            ->select('*')
            ->selectRaw('CONCAT(season, ":", game_type, ":", week, ":", game_id) AS uniq_ref');
        foreach ($this->data as $game) {
            $query->orWhere(function (Builder $where) use ($game) {
                $where->where([
                    'season' => $game['season'],
                    'game_type' => $game['game_type'],
                    'week' => $game['week'],
                    'game_id' => $game['game_id'],
                ]);
            });
        }
        $games = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}:{$game['game_type']}:{$game['week']}:{$game['game_id']}";
            $this->data[$i]['schedule'] = $games->where('uniq_ref', $ref);
        }
    }
}
