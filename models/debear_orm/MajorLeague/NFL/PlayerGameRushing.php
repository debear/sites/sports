<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsRushing as StatsRushingTrait;

class PlayerGameRushing extends BasePlayerGameStat
{
    use StatsRushingTrait;
}
