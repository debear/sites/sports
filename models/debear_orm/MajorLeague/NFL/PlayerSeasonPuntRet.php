<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPuntRet as StatsPuntRetTrait;

class PlayerSeasonPuntRet extends BasePlayerSeasonStat
{
    use StatsPuntRetTrait;
}
