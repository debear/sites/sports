<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPassDef as StatsPassDefTrait;

class PlayerGamePassDef extends BasePlayerGameStat
{
    use StatsPassDefTrait;
}
