<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsYards extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'yards_total' => ['s' => 'Total', 'f' => 'Total Yards'],
        'yards_pass' => ['s' => 'Pass', 'f' => 'Passing Yards'],
        'yards_rush' => ['s' => 'Rush', 'f' => 'Rushing Yards'],
        'yards_rush_tfl' => ['s' => 'RFL', 'f' => 'Rushing Yards Lost'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'yards_total' => 'TBL.yards_pass + TBL.yards_rush',
        'yards_pass' => 'TBL.yards_pass',
        'yards_rush' => 'TBL.yards_rush',
        'yards_rush_tfl' => 'TBL.yards_rush_tfl',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Format with separator in all instances.
        return number_format($this->$column);
    }
}
