<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPunts as StatsPuntsTrait;

class PlayerGamePunts extends BasePlayerGameStat
{
    use StatsPuntsTrait;
}
