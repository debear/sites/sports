<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Base\Instance;

class ScheduleByeWeek extends Instance
{
    /**
     * Load the teams on a bye week for a given ScheduleDate game week
     * @param ScheduleDate $game_week The game week ORM object for which the bye week info is requested.
     * @return self The resulting ORM object containing all teams on a bye for the game week being passed in
     */
    public static function loadByWeek(ScheduleDate $game_week): self
    {
        return static::query()
            ->where([
                ['season', $game_week->season],
                ['game_type', $game_week->game_type],
                ['week', $game_week->week],
            ])
            ->orderBy('team_id')
            ->get()
            ->loadSecondary(['teams']);
    }

    /**
     * Load expanded information about the teams returned
     * @return void
     */
    public function loadSecondaryTeams(): void
    {
        $teams = Team::load($this->unique('team_id'));
        foreach ($this->data as $i => $team) {
            $this->data[$i]['team'] = $teams->where('team_id', $team['team_id']);
        }
    }
}
