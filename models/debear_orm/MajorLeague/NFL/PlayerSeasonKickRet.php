<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsKickRet as StatsKickRetTrait;

class PlayerSeasonKickRet extends BasePlayerSeasonStat
{
    use StatsKickRetTrait;
}
