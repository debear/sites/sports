<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonSorted as BaseTeamSeasonSorted;

class TeamStatsDefenseSorted extends BaseTeamSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = TeamStatsDefense::class;
}
