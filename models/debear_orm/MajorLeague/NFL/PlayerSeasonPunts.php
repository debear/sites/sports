<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPunts as StatsPuntsTrait;

class PlayerSeasonPunts extends BasePlayerSeasonStat
{
    use StatsPuntsTrait;
}
