<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsKickRet as StatsKickRetTrait;

class PlayerGameKickRet extends BasePlayerGameStat
{
    use StatsKickRetTrait;
}
