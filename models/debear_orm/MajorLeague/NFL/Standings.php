<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\Standings as BaseStandings;

class Standings extends BaseStandings
{
    /**
     * Return a column, formatted for display.
     * @param string $col The column to render.
     * @return string The formatted output
     */
    public function format(string $col): string
    {
        // Our concated 'W/L records' fields.
        $record_fields = array_flip(['record', 'home', 'visitor', 'conf', 'div']);

        // Process.
        if ($col == 'gp') {
            $ret = ($this->wins + $this->loss + $this->ties);
        } elseif (($col == 'win_pct') && $this->format('gp')) {
            $ret = ltrim(sprintf('%.03f', ($this->wins + ($this->ties / 2)) / ((int)$this->format('gp'))), '0');
        } elseif ($col == 'pd') {
            $diff = $this->pts_for - $this->pts_against;
            $diff_css = (!$diff ? 'eq' : ($diff > 0 ? 'pos' : 'neg'));
            $ret = '<span class="delta-' . $diff_css . '">' . ($diff > 0 ? '+' : '') . $diff . '</span>';
        } elseif (isset($record_fields[$col])) {
            $prefix = ($col == 'record' ? '' : "{$col}_");
            $col_w = "{$prefix}wins";
            $col_l = "{$prefix}loss";
            $col_t = "{$prefix}ties";
            $ret = preg_replace('/&ndash;0$/', '', "{$this->$col_w}&ndash;{$this->$col_l}&ndash;{$this->$col_t}");
        } elseif ($col == 'streak') {
            $txt = ucfirst("{$this->streak_type} {$this->streak_num}");
            $css = ($this->streak_type == 'w' ? 'pos' : ($this->streak_type == 'l' ? 'neg' : 'eq'));
            $ret = '<span class="delta-' . $css . '">' . $txt . '</span>';
        } else {
            // Fallback: The raw value is enough.
            $ret = $this->$col;
        }
        return $ret ?? '&ndash;';
    }
}
