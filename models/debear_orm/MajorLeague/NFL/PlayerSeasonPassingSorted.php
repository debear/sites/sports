<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonSorted as BasePlayerSeasonSorted;

class PlayerSeasonPassingSorted extends BasePlayerSeasonSorted
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected static $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = PlayerSeasonPassing::class;
}
