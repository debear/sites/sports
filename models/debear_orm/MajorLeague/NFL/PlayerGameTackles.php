<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsTackles as StatsTacklesTrait;

class PlayerGameTackles extends BasePlayerGameStat
{
    use StatsTacklesTrait;
}
