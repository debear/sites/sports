<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonSorted as BaseTeamSeasonSorted;

class TeamStatsKicksSorted extends BaseTeamSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = TeamStatsKicks::class;
}
