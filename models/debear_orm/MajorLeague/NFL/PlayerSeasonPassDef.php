<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPassDef as StatsPassDefTrait;

class PlayerSeasonPassDef extends BasePlayerSeasonStat
{
    use StatsPassDefTrait;
}
