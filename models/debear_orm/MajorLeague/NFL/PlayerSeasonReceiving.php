<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsReceiving as StatsReceivingTrait;

class PlayerSeasonReceiving extends BasePlayerSeasonStat
{
    use StatsReceivingTrait;
}
