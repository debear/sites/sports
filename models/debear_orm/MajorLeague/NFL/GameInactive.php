<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\GameLineup as BaseGameLineup;
use DeBear\ORM\Sports\MajorLeague\ScheduleDate as BaseScheduleDate;

class GameInactive extends BaseGameLineup
{
    /**
     * Load the inactive players for a given ScheduleDate game week
     * @param BaseScheduleDate $game_week The game week ORM object for which the schedule info is requested.
     * @return BaseGameLineup The resulting ORM object containing all game for the game week being passed in
     */
    public static function loadByDate(BaseScheduleDate $game_week): BaseGameLineup
    {
        $tbl_player = Player::getTable();
        $tbl_inactive = static::getTable();
        return static::query()
            ->select("$tbl_inactive.*")
            ->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_inactive.player_id")
            ->where([
                ["$tbl_inactive.season", $game_week->season],
                ["$tbl_inactive.game_type", $game_week->game_type],
                ["$tbl_inactive.week", $game_week->week],
            ])
            ->orderBy("$tbl_inactive.game_id")
            ->orderBy("$tbl_inactive.team_id")
            ->orderBy("$tbl_player.surname")
            ->orderBy("$tbl_player.first_name")
            ->get()
            ->loadSecondary(['players']);
    }
}
