<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsMisc extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'time_of_poss' => ['s' => 'ToP', 'f' => 'Time of Possession'],
        'pens_num' => ['s' => 'Pens', 'f' => 'Penalties'],
        'pens_yards' => ['s' => 'Yds', 'f' => 'Penalty Yards'],
        'rz_cnv' => ['s' => 'RZM', 'f' => 'Red Zone Conversions'],
        'rz_num' => ['s' => 'RZA', 'f' => 'Red Zone Visits'],
        'rz_pct' => ['s' => 'RZ %', 'f' => 'Red Zone %'],
        'goal_cnv' => ['s' => 'G2GM', 'f' => 'Goal-to-Go Conversions'],
        'goal_num' => ['s' => 'G2GA', 'f' => 'Goal-to-Go Situations'],
        'goal_pct' => ['s' => 'G2G %', 'f' => 'Goal-to-Go %'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'time_of_poss' => 'TBL.time_of_poss',
        'pens_num' => 'TBL.pens_num',
        'pens_yards' => 'TBL.pens_yards',
        'rz_num' => 'TBL.rz_num',
        'rz_cnv' => 'TBL.rz_cnv',
        'rz_pct' => '(100 * TBL.rz_cnv) / TBL.rz_num',
        'goal_num' => 'TBL.goal_num',
        'goal_cnv' => 'TBL.goal_cnv',
        'goal_pct' => '(100 * TBL.goal_cnv) / TBL.goal_num',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'time_of_poss') {
            return ltrim($this->$column, '0:');
        } elseif (substr($column, -4) == '_pct') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
