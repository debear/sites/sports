<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\Schedule as BaseSchedule;
use DeBear\ORM\Sports\MajorLeague\ScheduleDate as BaseScheduleDate;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\Helpers\Format;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\NFL\Traits\ScheduleDate as ScheduleDateTrait;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Sports\MajorLeague\UserFavourite;

class Schedule extends BaseSchedule
{
    use ScheduleDateTrait;

    /**
     * Check if the game object represents the Super Bowl
     * @return boolean The the game in question is the Super Bowl
     */
    public function isSuperBowl(): bool
    {
        $po_keys = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'));
        return ($this->game_type == 'playoff') && ($po_keys[$this->week - 1] == 'superbowl');
    }

    /**
     * State if this game is special, why so
     * @return string A summary title
     */
    public function getTitleAttribute(): string
    {
        if ($this->game_type == 'regular' || $this->week < 3) {
            // Regular Season / Wildcard / Div Championship rounds not special enough.
            return '';
        } elseif ($this->week == 4) {
            // The Super Bowl.
            $num = $this->season - 1965;
            return FrameworkConfig::get('debear.setup.playoffs.rounds.superbowl.full') . ' '
                . ($num != 50 ? Format::decimalToRoman($num) : $num);
        }
        // Conference Championship.
        return $this->home->conference->name . ' Conference Championship';
    }

    /**
     * Abbreviate the game title in to a shorter version
     * @return string The abbreviated game title
     */
    public function getTitleShortAttribute(): string
    {
        if ($this->game_type == 'regular') {
            // Regular Season games have no title.
            return '';
        }
        $po_keys = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'));
        $po_round = $po_keys[$this->week - 1];
        $title_short = FrameworkConfig::get("debear.setup.playoffs.rounds.$po_round.mini");
        if ($this->isSuperBowl()) {
            // Append the number.
            $sb_num = $this->season - 1965;
            $title_short .= ' ' . ($sb_num != 50 ? Format::decimalToRoman($sb_num) : $sb_num);
        } else {
            // Prepend the conference name.
            $title_short = $this->home->conference->name . " $title_short";
        }
        return $title_short;
    }

    /**
     * The number of game quarters that took place
     * @return integer The number of quarters that took place
     */
    public function getNumPeriodsAttribute(): int
    {
        if ($this->status == 'F') {
            // Regulation game.
            return 4;
        } elseif ($this->status == 'OT') {
            // Single OT period.
            return 5;
        }
        // Multiple OTs.
        return 4 + $this->status[0];
    }

    /**
     * The name of a particular quarter
     * @param integer $num Quarter number.
     * @return string The quarter name
     */
    public function periodName(int $num): string
    {
        if ($num > 4) {
            return ltrim(($num - 4) . 'OT', '1');
        }
        return (string)$num;
    }

    /**
     * The summary name of a particular period
     * @param integer $num Period number.
     * @return string The "intermediate" period name
     */
    public function periodNameMid(int $num): string
    {
        if ($num > 4) {
            return ltrim(($num - 4) . 'OT', '1');
        }
        return Format::ordinal($num);
    }

    /**
     * The full name of a particular period
     * @param integer $num Period number.
     * @return string The full period name
     */
    public function periodNameFull(int $num): string
    {
        if ($num > 4) {
            return ($num > 5 ? Format::ordinal($num - 4) . ' ' : '') . 'Overtime';
        }
        return Format::ordinal($num) . ' Quarter';
    }

    /**
     * Load the schedule for a given ScheduleDate game week
     * @param BaseScheduleDate $game_week The game week ORM object for which the schedule info is requested.
     * @param array            $opt       Processing options.
     * @return BaseSchedule The resulting ORM object containing all game for the game week being passed in
     */
    public static function loadByDate(BaseScheduleDate $game_week, array $opt = []): BaseSchedule
    {
        // Start the query.
        $query = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS weather_key')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS odds_key')
            ->where([
                ['season', $game_week->season],
                ['game_type', $game_week->game_type],
                ['week', $game_week->week],
            ]);
        // Any favourite teams to prioritise?
        $is_logged_in = (User::object() !== null) && User::object()->isLoggedIn();
        if ($is_logged_in) {
            // Get the list of teams (if any).
            $favourites = UserFavourite::query()
                ->select('char_id')
                ->where([
                    ['sport', '=', FrameworkConfig::get('debear.section.code')],
                    ['user_id', '=', User::object()->id],
                    ['type', '=', 'team'],
                ])->get();
            // Add to the query?
            if ($favourites->count()) {
                $teams = $favourites->unique('char_id');
                $q = join(', ', array_fill(0, $favourites->count(), '?'));
                $query->selectRaw("home IN ($q) OR visitor IN ($q) AS is_favourite", array_merge($teams, $teams));
                $query->orderByRaw('is_favourite DESC');
            }
        } else {
            // Ensure we have the is_favourite column for filtering.
            $query->selectRaw('0 AS is_favourite');
        }
        // Moving completed games at the end?
        if (isset($opt['order-played-last']) && $opt['order-played-last']) {
            $query->orderByRaw('status IS NOT NULL');
        }
        // Standard game order.
        $ret = $query->orderBy('game_date')
            ->orderBy('game_time')
            ->orderBy('visitor')
            ->get();
        if (isset($opt['expanded']) && $opt['expanded']) {
            $ret->loadSummarySecondaries();
        } else {
            $ret->loadBasicSecondaries();
        }
        return $ret;
    }

    /**
     * Load the basic secondary game information for display
     * @return BaseSchedule The resulting ORM object with additional information loaded
     */
    public function loadBasicSecondaries(): BaseSchedule
    {
        return $this->loadSecondary(['altvenues', 'standings']);
    }

    /**
     * Load the expanded secondary game information for display
     * @return BaseSchedule The resulting ORM object with additional information loaded
     */
    public function loadSummarySecondaries(): BaseSchedule
    {
        return $this->loadSecondary(['altvenues', 'weather', 'odds', 'periods', 'standings']);
    }

    /**
     * Load the secondary game information for displaying a single game
     * @return BaseSchedule The resulting ORM object with detailed additional information loaded
     */
    public function loadDetailedSecondaries(): BaseSchedule
    {
        return $this->loadSecondary([
            'altvenues', 'periods', 'rosters', 'plays', 'drives', 'playerstats', 'teamstats', 'officials',
        ]);
    }

    /**
     * Load the schedule date info for the games in question
     * @return void
     */
    public function loadSecondaryScheduleDate(): void
    {
        $weeks = [];
        foreach ($this->data as $game) {
            $weeks["{$game['season']}-{$game['game_type']}-{$game['week']}"] = [
                ['season', '=', $game['season']],
                ['game_type', '=', $game['game_type']],
                ['week', '=', $game['week']],
            ];
        }
        $query = Namespaces::callStatic('ScheduleDate', 'query')
            ->select('*')
            ->selectRaw('CONCAT(game_type, "-", week) AS week_ref');
        foreach ($weeks as $week_where) {
            $query->orWhere(function (Builder $where) use ($week_where) {
                $where->where($week_where);
            });
        }
        $dates = $query->get();
        // Now tie back.
        foreach ($this->data as $i => $game) {
            $this->data[$i]['scheduleDate'] = $dates->where('week_ref', "{$game['game_type']}-{$game['week']}");
        }
    }

    /**
     * Load the players and rosters for the teams involved
     * @return void
     */
    public function loadSecondaryRosters(): void
    {
        // The lineups.
        $query_games = $this->queryGames();
        $lineups = GameLineup::query()
            ->select('*')
            ->selectRaw("CONCAT(season, '-', game_type, '-', game_id, '-', team_id) AS team_ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Then the players, preserving for future secondary calcs.
        $this->secondary['players'] = Player::query()
            ->whereIn('player_id', $lineups->unique('player_id'))
            ->get();
        foreach ($lineups as $line) {
            $line->player = $this->secondary['players']->where('player_id', $line->player_id);
        }
        // Now tie together.
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game['home']->team_id}";
            $this->data[$i]['home']->lineup = $lineups->where('team_ref', $ref);
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game['visitor']->team_id}";
            $this->data[$i]['visitor']->lineup = $lineups->where('team_ref', $ref);
        }
    }

    /**
     * Load the individual plays
     * @return void
     */
    public function loadSecondaryPlays(): void
    {
        $query_games = $this->queryGames();
        $plays = GameEvent::query()
            ->select('*')
            ->selectRaw('IFNULL(team_id, "-") AS team_id')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS ref')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", play_id) AS play_ref')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", IFNULL(team_id, "-")) AS team_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('week')
            ->orderBy('game_id')
            ->orderBy('play_id')
            ->get();
        // Also get the secondary info.
        $addtl = [
            'td' => GameScoringTD::class,
            'pat' => GameScoringPAT::class,
            'twopt' => GameScoring2pt::class,
            'fg' => GameScoringFG::class,
            'sft' => GameScoringSafety::class,
        ];
        foreach ($addtl as $name => $class) {
            $data = $class::query()
                ->select('*')
                ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", play_id) AS play_ref')
                ->where(function ($query) use ($query_games) {
                    foreach ($query_games as $game_where) {
                        $query->orWhere(function (Builder $where) use ($game_where) {
                            $where->where($game_where);
                        });
                    }
                    $query->orWhere(DB::raw(1), DB::raw(0));
                })->get();
            foreach ($plays as $play) {
                $play->$name = $data->where('play_ref', $play->play_ref);
            }
        }
        // Now tie together.
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $game_plays = $plays->where('ref', $ref);
            foreach ($game_plays as $play) {
                $play->opp_team_id = ($play->team_id == $game['home_id'] ? $game['visitor_id'] : $game['home_id']);
            }
            $this->data[$i]['plays'] = $game_plays;
        }
    }

    /**
     * Load the individual drives for each team
     * @return void
     */
    public function loadSecondaryDrives(): void
    {
        // The lineups.
        $query_games = $this->queryGames();
        $drives = GameDrive::query()
            ->select('*')
            ->selectRaw("CONCAT(season, '-', game_type, '-', game_id) AS ref")
            ->selectRaw("CONCAT(season, '-', game_type, '-', game_id, '-', team_id) AS team_ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('week')
            ->orderBy('game_id')
            ->orderBy('start_quarter')
            ->orderByDesc('start_time')
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $game) {
            $game_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['drives'] = $drives->where('ref', $game_ref);
            // Also individually by team.
            $team_ref = "$game_ref-{$game['home']->team_id}";
            $this->data[$i]['home']->drives = $drives->where('team_ref', $team_ref);
            $team_ref = "$game_ref-{$game['visitor']->team_id}";
            $this->data[$i]['visitor']->drives = $drives->where('team_ref', $team_ref);
        }
    }

    /**
     * The list of player stats to be loaded from the database
     * @return array Key/Value pair of stats
     */
    protected function loadSecondaryPlayerStatsList(): array
    {
        return [
            'passing' => [
                'class' => PlayerGamePassing::class,
                'orderBy' => ['yards', 'atts', 'cmp'],
            ],
            'rushing' => [
                'class' => PlayerGameRushing::class,
                'orderBy' => ['yards', 'atts'],
            ],
            'receiving' => [
                'class' => PlayerGameReceiving::class,
                'orderBy' => ['yards', 'recept', 'targets'],
            ],
            'kicking' => [
                'class' => PlayerGameKicking::class,
                'orderBy' => ['fg_made', 'fg_att', 'xp_made', 'xp_att'],
            ],
            'punting' => [
                'class' => PlayerGamePunts::class,
                'orderBy' => ['yards', 'num'],
            ],
            'kick_ret' => [
                'class' => PlayerGameKickRet::class,
                'orderBy' => ['yards', 'num'],
            ],
            'punt_ret' => [
                'class' => PlayerGamePuntRet::class,
                'orderBy' => ['yards', 'num'],
            ],
            'fumbles' => [
                'class' => PlayerGameFumbles::class,
                'orderBy' => ['num_fumbled', 'num_lost', 'num_forced'],
            ],
            'def_tckl' => [
                'class' => PlayerGameTackles::class,
                'orderBy' => ['tckl', 'asst'],
            ],
            'def_pass' => PlayerGamePassDef::class,
        ];
    }

    /**
     * Load the game stats for the teams involved
     * @return void
     */
    public function loadSecondaryTeamStats(): void
    {
        // Get the stats.
        $tbl_yards = TeamGameStatsYards::getTable();
        $tbl_downs = TeamGameStatsDowns::getTable();
        $tbl_misc = TeamGameStatsMisc::getTable();
        $tbl_to = TeamGameStatsTurnovers::getTable();
        $tbl_ret = TeamGameStatsReturns::getTable();
        $query_games = $this->queryGames($tbl_yards);
        $stats = TeamGameStatsYards::query()
            ->select(["$tbl_yards.season", "$tbl_yards.game_type", "$tbl_yards.game_id", "$tbl_yards.team_id"])
            ->selectRaw("$tbl_yards.yards_pass + $tbl_yards.yards_rush AS yards_total")
            ->selectRaw("$tbl_yards.yards_pass")
            ->selectRaw("$tbl_yards.yards_rush")
            ->selectRaw("$tbl_ret.ko_yards + $tbl_ret.punt_yards + $tbl_ret.int_yards AS yards_spctm")
            ->selectRaw("$tbl_downs.num_1st_pass + $tbl_downs.num_1st_rush + $tbl_downs.num_1st_pen AS downs_1st")
            ->selectRaw("$tbl_downs.num_1st_pass AS downs_1st_pass")
            ->selectRaw("$tbl_downs.num_1st_rush AS downs_1st_rush")
            ->selectRaw("$tbl_downs.num_1st_pen AS downs_1st_pen")
            ->selectRaw("$tbl_downs.num_3rd_cnv AS downs_3rd_cnv")
            ->selectRaw("$tbl_downs.num_3rd AS downs_3rd_num")
            ->selectRaw("$tbl_downs.num_4th_cnv AS downs_4th_cnv")
            ->selectRaw("$tbl_downs.num_4th AS downs_4th_num")
            ->selectRaw("$tbl_misc.rz_cnv")
            ->selectRaw("$tbl_misc.rz_num")
            ->selectRaw("$tbl_misc.goal_cnv")
            ->selectRaw("$tbl_misc.goal_num")
            ->selectRaw("$tbl_misc.pens_yards")
            ->selectRaw("$tbl_misc.pens_num")
            ->selectRaw("$tbl_to.int_num + $tbl_to.fumb_num AS to_tot")
            ->selectRaw("$tbl_to.int_num AS to_int")
            ->selectRaw("$tbl_to.fumb_num AS to_fumb")
            ->selectRaw("$tbl_to.fumb_lost AS to_fumb_lost")
            ->selectRaw("$tbl_misc.time_of_poss")
            ->selectRaw("TIME_TO_SEC($tbl_misc.time_of_poss) AS time_of_poss_sec")
            ->selectRaw("CONCAT($tbl_yards.season, '-', $tbl_yards.game_type, '-', $tbl_yards.game_id,
                '-', $tbl_yards.team_id) AS team_ref")
            ->join($tbl_downs, function ($join) use ($tbl_downs, $tbl_yards) {
                $join->on("$tbl_downs.season", '=', "$tbl_yards.season")
                    ->on("$tbl_downs.game_type", '=', "$tbl_yards.game_type")
                    ->on("$tbl_downs.week", '=', "$tbl_yards.week")
                    ->on("$tbl_downs.game_id", '=', "$tbl_yards.game_id")
                    ->on("$tbl_downs.team_id", '=', "$tbl_yards.team_id");
            })->join($tbl_misc, function ($join) use ($tbl_misc, $tbl_yards) {
                $join->on("$tbl_misc.season", '=', "$tbl_yards.season")
                    ->on("$tbl_misc.game_type", '=', "$tbl_yards.game_type")
                    ->on("$tbl_misc.week", '=', "$tbl_yards.week")
                    ->on("$tbl_misc.game_id", '=', "$tbl_yards.game_id")
                    ->on("$tbl_misc.team_id", '=', "$tbl_yards.team_id");
            })->join($tbl_to, function ($join) use ($tbl_to, $tbl_yards) {
                $join->on("$tbl_to.season", '=', "$tbl_yards.season")
                    ->on("$tbl_to.game_type", '=', "$tbl_yards.game_type")
                    ->on("$tbl_to.week", '=', "$tbl_yards.week")
                    ->on("$tbl_to.game_id", '=', "$tbl_yards.game_id")
                    ->on("$tbl_to.team_id", '=', "$tbl_yards.team_id");
            })->join($tbl_ret, function ($join) use ($tbl_ret, $tbl_yards) {
                $join->on("$tbl_ret.season", '=', "$tbl_yards.season")
                    ->on("$tbl_ret.game_type", '=', "$tbl_yards.game_type")
                    ->on("$tbl_ret.week", '=', "$tbl_yards.week")
                    ->on("$tbl_ret.game_id", '=', "$tbl_yards.game_id")
                    ->on("$tbl_ret.team_id", '=', "$tbl_yards.team_id");
            })->where(function ($query) use ($query_games) {
                foreach ($query_games as $i => $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Now build - returning as an stdClass as we have no ORM object.
        foreach ($this->data as $i => $game) {
            // Get the home team related info.
            $home_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game['home_id']}";
            $this->data[$i]['home']->stats = $stats->where('team_ref', $home_ref);
            // Get the visiting team related info.
            $visitor_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game['visitor_id']}";
            $this->data[$i]['visitor']->stats = $stats->where('team_ref', $visitor_ref);
        }
    }

    /**
     * Load the per-quarter scores for the teams involved
     * @return void
     */
    public function loadSecondaryPeriods(): void
    {
        $query = GameEvent::query()
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", team_id) AS ref')
            ->selectRaw('quarter AS period')
            ->selectRaw('SUM(CASE type
                WHEN "td+2pt" THEN 8
                WHEN "td+pat" THEN 7
                WHEN "td-2pt" THEN 6
                WHEN "td-pat" THEN 6
                WHEN "td" THEN 6
                WHEN "fg" THEN 3
                WHEN "def+2pt" THEN 2
                WHEN "safety" THEN 2
            END) AS score')
            ->groupBy(['season', 'game_type', 'game_id', 'team_id', 'quarter'])
            ->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('game_id')
            ->orderBy('team_id')
            ->orderBy('quarter');
        foreach ($this->data as $i => $game) {
            $where_clause = [
                ['season', '=', $game['season']],
                ['game_type', '=', $game['game_type']],
                ['game_id', '=', $game['game_id']],
            ];
            $query->orWhere(function (Builder $where) use ($where_clause) {
                $where->where($where_clause);
            });
        }
        $by_quarter = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        foreach ($this->data as $i => $game) {
            $base_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-";
            $this->data[$i]['home']->scoring = $by_quarter->where('ref', "{$base_ref}{$game['home_id']}");
            $this->data[$i]['visitor']->scoring = $by_quarter->where('ref', "{$base_ref}{$game['visitor_id']}");
        }
    }

    /**
     * Load the team standings at the point these games where played
     * @param string $game_col  The name of the date column in the schedule table.
     * @param string $stand_col The name of the date column in the standings table.
     * @return void
     */
    public function loadSecondaryStandings(string $game_col = 'week', string $stand_col = 'week'): void
    {
        parent::loadSecondaryStandings($game_col, $stand_col);
    }

    /**
     * Load the details of the officials involved
     * @return void
     */
    public function loadSecondaryOfficials(): void
    {
        $query_games = $this->queryGames();
        $game_officials = GameOfficial::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS game_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('week')
            ->orderBy('game_id')
            ->orderBy('official_type')
            ->get();
        // Get the official's details.
        $officials = Official::query()
            ->whereIn('official_id', $game_officials->unique('official_id'))
            ->get();
        // Now tie the official to the game official.
        foreach ($game_officials as $row) {
            $row->official = $officials->where('official_id', $row->official_id);
        }
        // And then the officials to the game.
        foreach ($this->data as $i => $game) {
            $game_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['officials'] = $game_officials->where('game_ref', $game_ref);
        }
    }

    /**
     * The logo to display at midfield for the game
     * @return string CSS class for the midfield logo
     */
    public function fieldLogo(): string
    {
        // Team or NFL logo?
        return ($this->isSuperBowl() ? 'misc-nfl-large-NFL' : $this->home->logoCSS('large'));
    }
}
