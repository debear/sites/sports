<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsReceiving as StatsReceivingTrait;

class PlayerGameReceiving extends BasePlayerGameStat
{
    use StatsReceivingTrait;
}
