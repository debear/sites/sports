<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsTurnovers extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'total' => ['s' => 'TO', 'f' => 'Turnovers'],
        'int_num' => ['s' => 'Int', 'f' => 'Interceptions'],
        'fumb_num' => ['s' => 'Fmb', 'f' => 'Fumbles'],
        'fumb_lost' => ['s' => 'FL', 'f' => 'Fumbles Lost'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'total' => 'TBL.int_num + TBL.fumb_lost',
        'int_num' => 'TBL.int_num',
        'fumb_num' => 'TBL.fumb_num',
        'fumb_lost' => 'TBL.fumb_lost',
    ];
}
