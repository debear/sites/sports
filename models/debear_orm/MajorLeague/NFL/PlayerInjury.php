<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerInjury as BasePlayerInjury;
use DeBear\ORM\Sports\MajorLeague\ScheduleDate as BaseScheduleDate;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class PlayerInjury extends BasePlayerInjury
{
    /**
     * Load the player injury list for a given ScheduleDate game week
     * @param BaseScheduleDate $game_week The game week ORM object for which the schedule info is requested.
     * @return BasePlayerInjury The resulting ORM object containing all game for the game week being passed in
     */
    public static function loadByDate(BaseScheduleDate $game_week): BasePlayerInjury
    {
        $tbl_player = Player::getTable();
        $tbl_roster = TeamRoster::getTable();
        $tbl_injury = static::getTable();
        return static::query()
            ->select(["$tbl_injury.*", "$tbl_roster.pos"])
            ->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_injury.player_id")
            ->join($tbl_roster, function ($join) use ($tbl_roster, $tbl_injury) {
                $join->on("$tbl_roster.season", '=', "$tbl_injury.season")
                    ->on("$tbl_roster.game_type", '=', "$tbl_injury.game_type")
                    ->on("$tbl_roster.week", '=', "$tbl_injury.week")
                    ->on("$tbl_roster.player_id", '=', "$tbl_injury.player_id");
            })
            ->where([
                ["$tbl_injury.season", $game_week->season],
                ["$tbl_injury.game_type", $game_week->game_type],
                ["$tbl_injury.week", $game_week->week],
            ])
            ->orderBy("$tbl_injury.team_id")
            ->orderBy("$tbl_injury.status")
            ->orderBy("$tbl_player.surname")
            ->orderBy("$tbl_player.first_name")
            ->get()
            ->loadSecondary(['players']);
    }

    /**
     * Convert the status code to a short-form description
     * @return string The short-form description of the injury status
     */
    public function getStatusShortAttribute(): string
    {
        return FrameworkConfig::get("debear.setup.players.injuries.{$this->status}.short");
    }

    /**
     * Convert the status code to a long-form description
     * @return string The long-form description of the injury status
     */
    public function getStatusLongAttribute(): string
    {
        return FrameworkConfig::get("debear.setup.players.injuries.{$this->status}.long");
    }
}
