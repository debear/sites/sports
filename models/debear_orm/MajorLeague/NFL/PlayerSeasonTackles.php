<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsTackles as StatsTacklesTrait;

class PlayerSeasonTackles extends BasePlayerSeasonStat
{
    use StatsTacklesTrait;
}
