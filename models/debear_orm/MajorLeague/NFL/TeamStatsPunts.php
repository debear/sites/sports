<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsPunts extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'num' => ['s' => 'Num', 'f' => 'Number'],
        'yards' => ['s' => 'Yds', 'f' => 'Yards'],
        'avg' => ['s' => 'Avg', 'f' => 'Average'],
        'net' => ['s' => 'Net', 'f' => 'Net Yards'],
        'net_avg' => ['s' => 'Net Avg', 'f' => 'Net Average'],
        'blocked' => ['s' => 'Blk', 'f' => 'Punts Blocked'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'num' => 'TBL.num',
        'yards' => 'TBL.yards',
        'avg' => 'TBL.yards / TBL.num',
        'net' => 'TBL.net',
        'net_avg' => 'TBL.net / TBL.num',
        'blocked' => 'TBL.blocked',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (in_array($column, ['yards', 'net'])) {
            return number_format($this->$column);
        } elseif (in_array($column, ['avg', 'net_avg'])) {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
