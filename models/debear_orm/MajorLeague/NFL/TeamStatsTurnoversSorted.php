<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonSorted as BaseTeamSeasonSorted;

class TeamStatsTurnoversSorted extends BaseTeamSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = TeamStatsTurnovers::class;
}
