<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonSorted as BaseTeamSeasonSorted;

class TeamStatsPuntsSorted extends BaseTeamSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = TeamStatsPunts::class;
}
