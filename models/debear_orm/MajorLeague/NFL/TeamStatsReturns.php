<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsReturns extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'ko_num' => ['s' => 'KO', 'f' => 'Kickoff Returns'],
        'ko_yards' => ['s' => 'Yds', 'f' => 'Kickoff Return Yards'],
        'ko_avg' => ['s' => 'Avg', 'f' => 'Kickoff Return Average'],
        'punt_num' => ['s' => 'Punt', 'f' => 'Punt Returns'],
        'punt_yards' => ['s' => 'Yds', 'f' => 'Punt Return Yards'],
        'punt_avg' => ['s' => 'Avg', 'f' => 'Punt Return Average'],
        'int_num' => ['s' => 'Int', 'f' => 'Interceptions'],
        'int_yards' => ['s' => 'Yds', 'f' => 'Interception Return Yards'],
        'int_avg' => ['s' => 'Avg', 'f' => 'Interception Return Average'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'ko_num' => 'TBL.ko_num',
        'ko_yards' => 'TBL.ko_yards',
        'ko_avg' => 'TBL.ko_yards / TBL.ko_num',
        'punt_num' => 'TBL.punt_num',
        'punt_yards' => 'TBL.punt_yards',
        'punt_avg' => 'TBL.punt_yards / TBL.punt_num',
        'int_num' => 'TBL.int_num',
        'int_yards' => 'TBL.int_yards',
        'int_avg' => 'TBL.int_yards / TBL.int_num',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, -6) == '_yards') {
            return number_format($this->$column);
        } elseif (substr($column, -4) == '_avg') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
