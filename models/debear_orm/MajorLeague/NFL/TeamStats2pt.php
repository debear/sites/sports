<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStats2pt extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'pass_made' => ['s' => 'Pass', 'f' => 'Passing Made'],
        'pass_att' => ['s' => 'Att', 'f' => 'Passing Attempts'],
        'pass_pct' => ['s' => 'Pass %', 'f' => 'Passing %'],
        'rush_made' => ['s' => 'Rush', 'f' => 'Rushing Made'],
        'rush_att' => ['s' => 'Att', 'f' => 'Rushing Attempts'],
        'rush_pct' => ['s' => 'Rush %', 'f' => 'Rushing %'],
        'total_made' => ['s' => 'Tot', 'f' => 'Total Made'],
        'total_att' => ['s' => 'Att', 'f' => 'Total Attempts'],
        'total_pct' => ['s' => 'Tot %', 'f' => 'Total %'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'pass_att' => 'TBL.pass_att',
        'pass_made' => 'TBL.pass_made',
        'pass_pct' => '(100 * TBL.pass_made) / TBL.pass_att',
        'rush_att' => 'TBL.rush_att',
        'rush_made' => 'TBL.rush_made',
        'rush_pct' => '(100 * TBL.rush_made) / TBL.rush_att',
        'total_att' => 'TBL.pass_att + TBL.rush_att',
        'total_made' => 'TBL.pass_made + TBL.rush_made',
        'total_pct' => '(100 * (TBL.pass_made + TBL.rush_made)) / (TBL.pass_att + TBL.rush_att)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, -4) == '_pct') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
