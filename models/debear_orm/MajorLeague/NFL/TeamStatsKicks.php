<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsKicks extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'fg_made' => ['s' => 'FGM', 'f' => 'Field Goals Made'],
        'fg_att' => ['s' => 'FGA', 'f' => 'Field Goals Attempted'],
        'fg_pct' => ['s' => 'FG %', 'f' => 'Field Goal %'],
        'fg_blocked' => ['s' => 'FG Blk', 'f' => 'Field Goals Blocked'],
        'pat_made' => ['s' => 'XPM', 'f' => 'Extra Points Made'],
        'pat_att' => ['s' => 'XPA', 'f' => 'Extra Points Attempted'],
        'pat_pct' => ['s' => 'XP %', 'f' => 'Extra Point %'],
        'pat_blocked' => ['s' => 'XP Blk', 'f' => 'Extra Points Blocked'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'fg_att' => 'TBL.fg_att',
        'fg_made' => 'TBL.fg_made',
        'fg_pct' => '(100 * TBL.fg_made) / TBL.fg_att',
        'fg_blocked' => 'TBL.fg_blocked',
        'pat_att' => 'TBL.pat_att',
        'pat_made' => 'TBL.pat_made',
        'pat_pct' => '(100 * TBL.pat_made) / TBL.pat_att',
        'pat_blocked' => 'TBL.pat_blocked',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, -4) == '_pct') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
