<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsRushing as StatsRushingTrait;

class PlayerSeasonRushing extends BasePlayerSeasonStat
{
    use StatsRushingTrait;
}
