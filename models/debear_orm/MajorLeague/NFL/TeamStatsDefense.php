<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsDefense extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'total' => ['s' => 'Tot', 'f' => 'Total'],
        'tckl' => ['s' => 'Solo', 'f' => 'Solo'],
        'asst' => ['s' => 'Asst', 'f' => 'Assists'],
        'sacks' => ['s' => 'Sack', 'f' => 'Sacks'],
        'sacks_yards' => ['s' => 'Yds', 'f' => 'Sack Yards'],
        'pd' => ['s' => 'PD', 'f' => 'Passes Defended'],
        'fumb_forced' => ['s' => 'FF', 'f' => 'Fumbles Forced'],
        'fumb_rec' => ['s' => 'FR', 'f' => 'Fumbles Recovered'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'total' => 'TBL.tckl + TBL.asst',
        'tckl' => 'TBL.tckl',
        'asst' => 'TBL.asst',
        'sacks' => 'TBL.sacks',
        'sacks_yards' => 'TBL.sacks_yards',
        'pd' => 'TBL.pd',
        'fumb_forced' => 'TBL.fumb_forced',
        'fumb_rec' => 'TBL.fumb_rec',
    ];
}
