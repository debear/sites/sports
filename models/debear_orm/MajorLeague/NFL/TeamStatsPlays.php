<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsPlays extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'total' => ['s' => 'Plays', 'f' => 'Total Plays'],
        'num_pass_att' => ['s' => 'Att', 'f' => 'Pass Attempts'],
        'num_pass_cmp' => ['s' => 'Cmp', 'f' => 'Pass Completions'],
        'pct_pass' => ['s' => 'Pass %', 'f' => 'Pass %'],
        'num_pass_sacked' => ['s' => 'Sack', 'f' => 'Times Sacked'],
        'num_rush' => ['s' => 'Rush', 'f' => 'Rushes'],
        'num_rush_tfl' => ['s' => 'RFL', 'f' => 'Rushes for Loss'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'total' => 'TBL.total',
        'num_pass_att' => 'TBL.num_pass_att',
        'num_pass_cmp' => 'TBL.num_pass_cmp',
        'pct_pass' => '(100 * TBL.num_pass_cmp) / TBL.num_pass_att',
        'num_pass_sacked' => 'TBL.num_pass_sacked',
        'num_rush' => 'TBL.num_rush',
        'num_rush_tfl' => 'TBL.num_rush_tfl',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'total') {
            return number_format($this->$column);
        } elseif ($column == 'pct_pass') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
