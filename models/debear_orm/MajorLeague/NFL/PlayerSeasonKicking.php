<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsKicking as StatsKickingTrait;

class PlayerSeasonKicking extends BasePlayerSeasonStat
{
    use StatsKickingTrait;
}
