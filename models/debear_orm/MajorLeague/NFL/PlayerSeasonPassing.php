<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPassing as StatsPassingTrait;

class PlayerSeasonPassing extends BasePlayerSeasonStat
{
    use StatsPassingTrait;
}
