<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\GameEvent as BaseGameEvent;

class GameEvent extends BaseGameEvent
{
    /**
     * Convert the play into a descriptive string
     * @return string The play-by-play rendition of the score
     */
    public function getPlayAttribute(): string
    {
        if ($this->type == 'fg') {
            $method = 'formatScoringFG';
        } elseif ($this->type == 'def+2pt') {
            $method = 'formatScoringDef2pt';
        } elseif ($this->type == 'safety') {
            $method = 'formatScoringSafety';
        } else {
            // All other items are touchdowns.
            $method = 'formatScoringTD';
        }
        return $this->$method();
    }

    /**
     * Format the play-by-play description for a Touchdown
     * @return string The play-by-play rendition of the score
     */
    protected function formatScoringTD(): string
    {
        // First, how the TD was scored.
        $play = '{{' . $this->team_id . '_' . $this->td->scorer . '}} ';
        if ($this->td->length) {
            $play .= $this->td->length . 'yd ';
        }

        switch ($this->td->type) {
            case 'pass':
                $play .= 'pass';
                break;
            case 'rush':
                $play .= 'rush';
                break;
            case 'int_ret':
                $play .= 'interception return';
                break;
            case 'fumb_ret':
                $play .= 'fumble return';
                break;
            case 'fumb_rec':
                $play .= 'fumble recovery';
                break;
            case 'kick_ret':
                $play .= 'kickoff return';
                break;
            case 'kick_rec':
                $play .= 'kickoff recovery';
                break;
            case 'punt_ret':
                $play .= 'punt return';
                break;
            case 'block_punt_ret':
                $play .= 'blocked punt return';
                break;
            case 'block_punt_rec':
                $play .= 'blocked punt recovery';
                break;
            case 'block_fg_ret':
                $play .= 'blocked field goal return';
                break;
        }

        if ($this->td->qb) {
            $play .= ' from {{' . $this->team_id . '_' . $this->td->qb . '}}';
        }

        // Then the conversion.
        // PAT.
        if (substr($this->type, -3) == 'pat') {
            $play .= ' (';
            // Kicker?
            if (isset($this->pat->kicker)) {
                $play .= '{{' . $this->team_id . '_' . $this->pat->kicker . '}} k';
            } else {
                $play .= 'K';
            }
            $play .= 'ick';

            // Result.
            if (!$this->pat->made) {
                $play .= ' is no good';
            }
            $play .= ')';

        // 2pt.
        } elseif (substr($this->type, -3) == '2pt') {
            $play .= ' (2pt ' . ($this->twopt->type ?? 'conversion');

            // Players.
            if (isset($this->twopt->qb) && isset($this->twopt->player)) {
                $play .= ' from {{' . $this->team_id . '_' . $this->twopt->qb . '}} '
                    . 'to {{' . $this->team_id . '_' . $this->twopt->player . '}}';
            } elseif (isset($this->twopt->player)) {
                $play .= ' by {{' . $this->team_id . '_' . $this->twopt->player . '}}';
            }

            // Result.
            if (!$this->twopt->made) {
                $play .= ' failed';
            }
            $play .= ')';
        }

        return $play;
    }

    /**
     * Format the play-by-play description for a Field Goal
     * @return string The play-by-play rendition of the score
     */
    protected function formatScoringFG(): string
    {
        return '{{' . $this->team_id . '_' . $this->fg->kicker . '}} ' . $this->fg->length . 'yd field goal';
    }
    /**
     * Format the play-by-play description for a Defensive 2pt Return
     * @return string The play-by-play rendition of the score
     */
    protected function formatScoringDef2pt(): string
    {
        return '{{' . $this->team_id . '_' . $this->twopt->player . '}} defensive two point return';
    }
    /**
     * Format the play-by-play description for a Safety
     * @return string The play-by-play rendition of the score
     */
    protected function formatScoringSafety(): string
    {
        $play = 'Unknown'; // Default value.
        switch ($this->sft->type) {
            case 'penalty':
                $play = 'Penalty';
                if (isset($this->sft->player_off)) {
                    $play .= ' on {{' . $this->opp_team_id . '_' . $this->sft->player_off . '}}';
                }
                $play .= ' enforced in the end zone';
                break;

            case 'tackle':
                if (isset($this->sft->player_off)) {
                    $play = '{{' . $this->opp_team_id . '_' . $this->sft->player_off . '}} tackled';
                } else {
                    $play = 'Tackle';
                }
                $play .= ' in the end zone';
                if (isset($this->sft->player_def)) {
                    $play .= ' by {{' . $this->team_id . '_' . $this->sft->player_def . '}}';
                }
                if (isset($this->sft->player_def_alt)) {
                    $play .= ' and {{' . $this->team_id . '_' . $this->sft->player_def_alt . '}}';
                }
                break;

            case 'sack':
                if (isset($this->sft->player_off)) {
                    $play = '{{' . $this->opp_team_id . '_' . $this->sft->player_off . '}}';
                } else {
                    $play = 'Player';
                }
                $play .= ' sacked in the end zone';
                if (isset($this->sft->player_def)) {
                    $play .= ' by {{' . $this->team_id . '_' . $this->sft->player_def . '}}';
                }
                if (isset($this->sft->player_def_alt)) {
                    $play .= ' and {{' . $this->team_id . '_' . $this->sft->player_def_alt . '}}';
                }
                break;

            case 'fumble':
                if (isset($this->sft->player_off)) {
                    $play = '{{' . $this->opp_team_id . '_' . $this->sft->player_off . '}} fumbled';
                } else {
                    $play = 'Fumble';
                }
                $play .= ' in the end zone';
                if (isset($this->sft->player_def)) {
                    $play .= ', forced by {{' . $this->team_id . '_' . $this->sft->player_def . '}}';
                }
                if (isset($this->sft->player_def_alt)) {
                    $play .= ' and {{' . $this->team_id . '_' . $this->sft->player_def_alt . '}}';
                }
                break;

            case 'punt_block':
                if (isset($this->sft->player_off)) {
                    $play = 'Punt by {{' . $this->opp_team_id . '_' . $this->sft->player_off . '}}';
                } else {
                    $play = 'Punt';
                }
                $play .= ' blocked in the end zone';
                if (isset($this->sft->player_def)) {
                    $play .= ' by {{' . $this->team_id . '_' . $this->sft->player_def . '}}';
                }
                if (isset($this->sft->player_def_alt)) {
                    $play .= ' and {{' . $this->team_id . '_' . $this->sft->player_def_alt . '}}';
                }
                break;

            case 'ob':
                if (isset($this->sft->player_off)) {
                    $play = '{{' . $this->opp_team_id . '_' . $this->sft->player_off . '}}';
                } else {
                    $play = 'Player';
                }
                $play .= ' out-of-bounds';
                if (isset($this->sft->player_def)) {
                    $play .= ', forced by {{' . $this->team_id . '_' . $this->sft->player_def . '}}';
                }
                if (isset($this->sft->player_def_alt)) {
                    $play .= ' and {{' . $this->team_id . '_' . $this->sft->player_def_alt . '}}';
                }
                break;
        }

        return "Safety: $play";
    }
}
