<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PowerRankings as BasePowerRankings;
use DeBear\Helpers\Sports\Namespaces;
use Illuminate\Database\Query\Builder;

class PowerRankings extends BasePowerRankings
{
    /**
     * Get the week info for each row returned
     * @return void
     */
    protected function loadSecondaryWeeks(): void
    {
        // Determine the weeks to parse.
        $weeks = [];
        foreach ($this->data as $rank) {
            $weeks["{$rank['game_type']}-{$rank['week']}"] = [
                'game_type' => $rank['game_type'],
                'week' => $rank['week'],
            ];
        }
        // Then load them.
        $weeks = Namespaces::callStatic('PowerRankingsWeeks', 'query')
            ->select('*')
            ->selectRaw('CONCAT(game_type, "-", week) AS week_ref')
            ->where('season', $this->count() ? $this->max('season') : 2099)
            ->where(function ($where) use ($weeks) {
                $i = 0;
                foreach ($weeks as $week) {
                    $where->orWhere(function (Builder $subwhere) use ($week) {
                        $subwhere->where([
                            ['game_type', '=', $week['game_type']],
                            ['week', '=', $week['week']],
                        ]);
                    });
                }
            })->get();
        // Now tie-together.
        foreach ($this->data as $i => $rank) {
            $this->data[$i]['week_id'] = $rank['week'];
            $this->data[$i]['week'] = $weeks->where('week_ref', "{$rank['game_type']}-{$rank['week']}");
        }
    }
}
