<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamStatsDowns extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'num_1st' => ['s' => '1st', 'f' => 'Total'],
        'num_1st_rush' => ['s' => 'Rush', 'f' => 'Rushing'],
        'num_1st_pass' => ['s' => 'Pass', 'f' => 'Passing'],
        'num_1st_pen' => ['s' => 'Pen', 'f' => 'Penalty'],
        'num_3rd_cnv' => ['s' => '3rd', 'f' => '3rd Downs Converted'],
        'num_3rd' => ['s' => 'Num', 'f' => '3rd Downs'],
        'pct_3rd' => ['s' => '3rd %', 'f' => '3rd Down %'],
        'num_4th_cnv' => ['s' => '4th', 'f' => '4th Downs Converted'],
        'num_4th' => ['s' => 'Num', 'f' => '4th Downs'],
        'pct_4th' => ['s' => '4th %', 'f' => '4th Down %'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'num_1st' => 'TBL.num_1st_rush + TBL.num_1st_pass + TBL.num_1st_pen',
        'num_1st_rush' => 'TBL.num_1st_rush',
        'num_1st_pass' => 'TBL.num_1st_pass',
        'num_1st_pen' => 'TBL.num_1st_pen',
        'num_3rd' => 'TBL.num_3rd',
        'num_3rd_cnv' => 'TBL.num_3rd_cnv',
        'pct_3rd' => '(100 * TBL.num_3rd_cnv) / TBL.num_3rd',
        'num_4th' => 'TBL.num_4th',
        'num_4th_cnv' => 'TBL.num_4th_cnv',
        'pct_4th' => '(100 * TBL.num_4th_cnv) / TBL.num_4th',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, 0, 4) == 'pct_') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
