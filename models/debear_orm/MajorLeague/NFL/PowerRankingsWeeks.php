<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PowerRankingsWeeks as BasePowerRankingsWeeks;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class PowerRankingsWeeks extends BasePowerRankingsWeeks
{
    /**
     * The name of the Power Ranking Week
     * @return string The display name
     */
    public function getNameAttribute(): string
    {
        if ($this->game_type == 'regular') {
            // Regular season.
            return "Week {$this->week}";
        }
        // Playoff Round.
        $po_code = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'))[$this->week - 1];
        return FrameworkConfig::get("debear.setup.playoffs.rounds.$po_code.full");
    }

    /**
     * The expanded name of the Power Ranking Week, including date range
     * @return string The display name
     */
    public function getNameFullAttribute(): string
    {
        // In the NFL, keep the "standard" name.
        return $this->name;
    }

    /**
     * Convert the week into a URL argument
     * @return string The week as a URL code
     */
    public function getWeekCodeAttribute(): string
    {
        if ($this->game_type == 'regular') {
            // Regular season.
            return "week-{$this->week}";
        }
        // Playoffs are defined by name.
        $po_codes = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'));
        return $po_codes[$this->week - 1];
    }

    /**
     * Parse the week argument to an ORM version
     * @param string $week The raw week.
     * @return array The week parsed in to ORM arguments
     */
    protected static function parseWeekCode(string $week): array
    {
        if (substr($week, 0, 5) == 'week-') {
            // Regular season.
            return ['game_type' => 'regular', 'week' => substr($week, 5)];
        }
        // Playoffs are by name.
        $po_codes = array_flip(array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds')));
        return ['game_type' => 'playoff', 'week' => isset($po_codes[$week]) ? $po_codes[$week] + 1 : 99];
    }

    /**
     * Load the NFL games for a given power ranking week
     * @return void
     */
    protected function loadSecondaryGames(): void
    {
        $this->secondary['games'] = Schedule::query()
            ->where(array_filter([
                'season' => $this->season,
                'game_type' => $this->game_type ?? false,
                'week' => $this->week,
            ]))
            ->orderBy('game_date')
            ->orderBy('game_time')
            ->get();
        // Parse the games in to per-team results.
        $this->loadSecondaryGamesParse();
    }
}
