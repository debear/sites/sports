<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\ScheduleDate as BaseScheduleDate;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\Time;
use DeBear\Helpers\Sports\NFL\Traits\ScheduleDate as ScheduleDateTrait;

class ScheduleDate extends BaseScheduleDate
{
    use ScheduleDateTrait;

    /**
     * Return the date in a longer form
     * @return string The "long" version of the date
     */
    public function getNameFullAttribute(): string
    {
        return "{$this->season} {$this->name_short}";
    }

    /**
     * Return the date in a short form
     * @return string The "short" version of the date
     */
    public function getNameShortAttribute(): string
    {
        if ($this->game_type == 'regular') {
            // Regular season is just the week number.
            return "Week {$this->week}";
        }
        // What playoff round?
        $po = FrameworkConfig::get('debear.setup.playoffs.rounds');
        $po_keys = array_keys($po);
        return $po[$po_keys[$this->week - 1]]['short'];
    }

    /**
     * Load the full list of game weeks
     * @return self The resulting ORM object containing all game weeks for the season being viewed
     */
    public static function loadAll(): self
    {
        return static::query()
            ->select('*')
            ->selectRaw(
                '(game_type = "regular") OR (start_date <= ?) AS is_available',
                [Time::object()->getDatabaseCurDate()]
            )
            ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
            ->orderBy('start_date')
            ->get();
    }

    /**
     * Validate a supplied game week argument
     * @param string $week_arg The supplied URI argument to validate.
     * @return BaseScheduleDate The resulting ORM object for the passed argument
     */
    public static function loadFromArgument(string $week_arg): BaseScheduleDate
    {
        $query = [
            'season' => substr($week_arg, 0, 4),
        ];
        $week = substr($week_arg, 5);
        if (substr($week, 0, 5) == 'week-') {
            // Regular season.
            $query['game_type'] = 'regular';
            $query['week'] = substr($week, 5);
        } else {
            // Playoff.
            $query['game_type'] = 'playoff';
            $po_weeks = array_flip(array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds')));
            $query['week'] = isset($po_weeks[$week]) ? ($po_weeks[$week] + 1) : 0;
        }
        return static::query()->where($query)->get();
    }

    /**
     * Determine the current/latest game week
     * @param integer $season An optional season restriction.
     * @return BaseScheduleDate The game week ORM object we consider 'current'
     */
    public static function getLatest(?int $season = null): BaseScheduleDate
    {
        $cur_date = Time::object()->getDatabaseCurDate();
        // Scenario A: Direct match.
        $query = static::query()
            ->where([
                ['start_date', '<=', $cur_date],
                ['end_date', '>=', $cur_date],
            ]);
        if (isset($season)) {
            $query->where('season', $season);
        }
        $direct = $query->get();
        if ($direct->isset()) {
            return $direct;
        }
        // Scenario B: Previewing new season.
        $preview = static::query()
            ->where([
                ['season', '=', $season ?? FrameworkConfig::get('debear.setup.season.viewing')],
                ['start_date', '>=', $cur_date],
            ])->orderBy('start_date')
            ->limit(1)
            ->get();
        if ($preview->isset()) {
            return $preview;
        }
        // Scenario C (Fallback): Last known date.
        $query = static::query();
        if (isset($season)) {
            $query->where('season', $season);
        }
        return $query->where('start_date', '<=', $cur_date)
            ->orderByDesc('start_date')
            ->limit(1)
            ->get();
    }
}
