<?php

namespace DeBear\ORM\Sports\MajorLeague\NFL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\NFL\Traits\StatsPuntRet as StatsPuntRetTrait;

class PlayerGamePuntRet extends BasePlayerGameStat
{
    use StatsPuntRetTrait;
}
