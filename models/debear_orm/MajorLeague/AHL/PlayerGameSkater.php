<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\AHL\Traits\StatsSkater as StatsSkaterTrait;

class PlayerGameSkater extends BasePlayerGameStat
{
    use StatsSkaterTrait;
}
