<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\AHL\Traits\StatsSkater as StatsSkaterTrait;

class PlayerSeasonSkater extends BasePlayerSeasonStat
{
    use StatsSkaterTrait;
}
