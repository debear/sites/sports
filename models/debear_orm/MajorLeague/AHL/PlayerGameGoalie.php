<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\AHL\Traits\StatsGoalie as StatsGoalieTrait;

class PlayerGameGoalie extends BasePlayerGameStat
{
    use StatsGoalieTrait;
}
