<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\AHL\Traits\StatsGoalie as StatsGoalieTrait;

class PlayerSeasonGoalie extends BasePlayerSeasonStat
{
    use StatsGoalieTrait;
}
