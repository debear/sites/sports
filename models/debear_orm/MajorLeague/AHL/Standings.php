<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\Standings as BaseStandings;

class Standings extends BaseStandings
{
    /**
     * The display version of the team's record
     * @return string The team's record in W-L(-T) form
     */
    public function getRecordAttribute(): string
    {
        return $this->format('record');
    }

    /**
     * Return a column, formatted for display.
     * @param string $col The column to render.
     * @return string The formatted output
     */
    public function format(string $col): string
    {
        // Our concated 'W/L records' fields.
        $record_fields = array_flip(['record', 'home', 'visitor', 'div', 'recent']);

        // Process.
        if ($col == 'gp') {
            $ret = ($this->wins + $this->loss + $this->ot_loss + $this->so_loss);
        } elseif ($col == 'pts_pct') {
            $ret = ltrim($this->pts_pct, '0');
        } elseif ($col == 'gd') {
            $diff = $this->goals_for - $this->goals_against;
            $diff_css = (!$diff ? 'eq' : ($diff > 0 ? 'pos' : 'neg'));
            $ret = '<span class="delta-' . $diff_css . '">' . ($diff > 0 ? '+' : '') . $diff . '</span>';
        } elseif (isset($record_fields[$col])) {
            $prefix = ($col == 'record' ? '' : "{$col}_");
            $col_w = "{$prefix}wins";
            $col_l = "{$prefix}loss";
            $col_otl = "{$prefix}ot_loss";
            $col_sol = "{$prefix}so_loss";
            $ret = "{$this->$col_w}&ndash;{$this->$col_l}&ndash;{$this->$col_otl}&ndash;{$this->$col_sol}";
        } else {
            // Fallback: The raw value is enough.
            $ret = $this->$col;
        }
        return $ret ?? '&ndash;';
    }
}
