<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\PlayerHeatmap as BasePlayerHeatmap;
use DeBear\Helpers\Sports\AHL\Traits\Heatmaps as HeatmapsTrait;

class PlayerSeasonGoalieHeatmap extends BasePlayerHeatmap
{
    use HeatmapsTrait;
}
