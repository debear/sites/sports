<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\PlayoffSeries as BasePlayoffSeries;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class PlayoffSeries extends BasePlayoffSeries
{
    /**
     * Get the title for the championship round of the playoffs
     * @param integer $season The season's playoffs being viewed.
     * @return string The series title
     */
    public static function champSeriesTitle(int $season): string
    {
        return FrameworkConfig::get('debear.setup.playoffs.trophy') . ' Finals';
    }
}
