<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\Team as BaseTeam;

class Team extends BaseTeam
{
    /**
     * Get the team's home venue
     * @return string The name of the team's current home venue
     */
    public function getVenueAttribute(): string
    {
        return $this->currentVenue->arena;
    }
}
