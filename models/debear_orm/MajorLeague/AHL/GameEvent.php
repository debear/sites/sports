<?php

namespace DeBear\ORM\Sports\MajorLeague\AHL;

use DeBear\ORM\Sports\MajorLeague\GameEvent as BaseGameEvent;

class GameEvent extends BaseGameEvent
{
    /**
     * Return the event time as MM:SS.
     * @return string The time minus the hour part.
     */
    public function getTimeAttribute(): string
    {
        return substr($this->event_time, 3);
    }
}
