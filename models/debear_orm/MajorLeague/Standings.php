<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Sports\Namespaces;

class Standings extends Instance
{
    /**
     * The display version of the team's record
     * @return string The team's record in W-L(-T) form
     */
    public function getRecordAttribute(): string
    {
        return $this->format('record');
    }

    /**
     * Load the Standings for a given season
     * @param integer $season The season to load (the latest) standings.
     * @return self An ORM object with the appropriate details
     */
    public static function load(int $season): self
    {
        return static::query()
            ->where([
                ['season', '=', $season],
                static::getCurrentDate($season),
            ])->orderByDesc('pos_league')
            ->get();
    }

    /**
     * Load expanded information about the teams
     * @return void
     */
    public function loadSecondaryTeams(): void
    {
        // Do we need to narrow down the season?
        $load_args = [$this->unique('team_id')];
        if ($this->season != FrameworkConfig::get('debear.setup.season.viewing')) {
            $load_args[1] = []; // No additional secondary info required.
            $load_args[2] = ['season' => $this->season];
        }
        // Now get the team details.
        $teams = Namespaces::callStatic('Team', 'load', $load_args);
        foreach ($this->data as $i => $standing) {
            $this->data[$i]['team'] = $teams->where('team_id', $standing['team_id']);
        }
    }

    /**
     * Get the constraint for determining the current standings date
     * @param integer $season The season to load (the latest) standings.
     * @return array The appropriate ORM query clause
     */
    public static function getCurrentDate(int $season): array
    {
        $date_col = FrameworkConfig::get('debear.setup.standings.date-col');
        $latest = static::query()
            ->select($date_col)
            ->where('season', $season)
            ->orderByDesc($date_col)
            ->limit(1)
            ->get()
            ->$date_col;
        return [$date_col, '=', $latest];
    }

    /**
     * Convert the raw column configuration in to a stable usable version
     * @param integer $season The season we are viewing standings for.
     * @return array An array of column configs
     */
    public static function parseColumns(int $season): array
    {
        $ret = [];
        foreach (['div', 'conf'] as $type) {
            $cols = FrameworkConfig::get("debear.setup.standings.columns");
            $ret[$type] = [];
            foreach ($cols as $code => $col) {
                // If we have type-specific configuration, merge it in now.
                if (isset($col[$type]) && is_array($col[$type])) {
                    $col = Arrays::merge($col, $col[$type]);
                }
                // Not relevant to this type?
                if (isset($col['skip']) && $col['skip']) {
                    continue;
                }
                // No label?
                if (!isset($col['label'])) {
                    $col['label'] = '';
                }
                // No CSS?
                if (!isset($col['css'])) {
                    $col['css'] = '';
                }
                // No column definition?
                if (!isset($col['col']) && !isset($col['add'])) {
                    $col['col'] = $code;
                }
                // Is this a conditional column?
                if (isset($col['periods']) && is_array($col['periods'])) {
                    $in_period = false; // Assume not until we find a match.
                    $num_periods = count($col['periods']);
                    for ($i = 0; !$in_period && $i < $num_periods; $i++) {
                        $period = $col['periods'][$i];
                        $from = $period['from'] ?? FrameworkConfig::get('debear.setup.season.min');
                        $to = $period['to'] ?? FrameworkConfig::get('debear.setup.season.max');
                        $in_period = ($from <= $season && $season <= $to);
                    }
                    if (!$in_period) {
                        // Skip if we did not find any matching periods.
                        continue;
                    }
                }
                // Add to the list to be returned.
                $ret[$type][$code] = $col;
            }
        }
        return $ret;
    }

    /**
     * Convert the raw key configuration in to a stable usable version
     * @param integer $season The season we are viewing standings for.
     * @return array An array of appropriate keys
     */
    public static function parseKey(int $season): array
    {
        $ret = [];
        foreach (FrameworkConfig::get('debear.setup.standings.key') as $code => $label) {
            if (!is_string($label)) {
                // Is this a conditional key?
                if (isset($label['from']) || isset($label['to'])) {
                    $from = $label['from'] ?? FrameworkConfig::get('debear.setup.season.min');
                    $to = $label['to'] ?? FrameworkConfig::get('debear.setup.season.max');
                    if ($season < $from || $season > $to) {
                        // Skip, not relevant for this season.
                        continue;
                    }
                }
                // Convert from the array to the label.
                $label = $label['label'];
            }
            $ret[$code] = $label;
        }
        return $ret;
    }
}
