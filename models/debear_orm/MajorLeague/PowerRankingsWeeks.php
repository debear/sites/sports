<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\Namespaces;

class PowerRankingsWeeks extends Instance
{
    /**
     * The name of the Power Ranking Week
     * @return string The display name
     */
    public function getNameAttribute(): string
    {
        return "Week {$this->week}";
    }

    /**
     * The expanded name of the Power Ranking Week, including date range
     * @return string The display name
     */
    public function getNameFullAttribute(): string
    {
        $from_fmt = (substr($this->date_from, 5, 2) == substr($this->date_to, 5, 2) ? 'jS' : 'jS M');
        return $this->name . ', ' . $this->date_from->format($from_fmt) . '&ndash;' . $this->date_to->format('jS M');
    }

    /**
     * Convert the week into a URL argument
     * @return string The week as a URL code
     */
    public function getWeekCodeAttribute(): string
    {
        return $this->week;
    }

    /**
     * Parse the week argument to an ORM version
     * @param string $week The raw week.
     * @return array The week parsed in to ORM arguments
     */
    protected static function parseWeekCode(string $week): array
    {
        return ['week' => $week];
    }

    /**
     * Load the Power Ranks for the supplied week
     * @param integer $season The season to load, or false if nothing explicitly passed.
     * @param string  $week   The week we want to view, or the latest if not passed.
     * @return self An ORM object with the appropriate details
     */
    public static function load(?int $season = null, ?string $week = null): self
    {
        // Convert the season/week in to database arguments.
        $select = array_filter([
            'season',
            FrameworkConfig::get('debear.setup.power-ranks.by-game_type') ? 'game_type' : false,
            'week',
        ]);
        if (!isset($season) || !isset($week)) {
            // Request the latest from the current season if none supplied.
            if (!isset($season)) {
                $season = FrameworkConfig::get('debear.setup.season.viewing');
            }
            $parsed_week = static::query()
                ->select($select)
                ->where('season', $season)
                ->whereNotNull('when_run')
                ->orderByDesc('calc_date')
                ->limit(1)
                ->get();

            // If this does not return a date, then the season hasn't started, so return an (empty) first week.
            if (!$parsed_week->isset()) {
                return static::query()
                    ->where('season', $season)
                    ->orderBy('calc_date')
                    ->limit(1)
                    ->get();
            }
        } else {
            // Validate the argument passed in.
            $parsed_week = static::query()
                ->select($select)
                ->where(array_merge(['season' => $season], static::parseWeekCode($week)))
                ->whereNotNull('when_run')
                ->get();
            // If this does not return a date, then the input is not valid.
            if (!$parsed_week->isset()) {
                return $parsed_week;
            }
        }

        // Now get the data out.
        return static::query()
            ->where(array_filter([
                'season' => $parsed_week->season,
                'game_type' => $parsed_week->game_type ?? false,
                'week' => $parsed_week->week,
            ]))
            ->get()
            ->loadSecondary([
                'teams',
                'previous',
                'games',
            ]);
    }

    /**
     * Load the teams and their ranks for the current power ranking week
     * @return void
     */
    protected function loadSecondaryTeams(): void
    {
        $this->secondary['ranks'] = Namespaces::callStatic('PowerRankings', 'query')
            ->where(array_filter([
                'season' => $this->season,
                'game_type' => $this->game_type ?? false,
                'week' => $this->week,
            ]))
            ->orderBy('rank')
            ->get();
        // The team list.
        $teams = Namespaces::callStatic('Team', 'load', [
            $this->secondary['ranks']->unique('team_id'),
            [], // No additional secondaries.
            ['season' => $this->season],
        ]);
        // Tie back to the data.
        foreach ($this->secondary['ranks'] as $rank) {
            $rank->team = $teams->where('team_id', $rank->team_id);
        }
        $this->data[0]['teams'] = $this->secondary['ranks'];
    }

    /**
     * Load the previous week's ranks
     * @return void
     */
    protected function loadSecondaryPrevious(): void
    {
        $prev = static::query()
            ->where([
                ['season', '=', $this->season],
                ['calc_date', '<', $this->calc_date],
            ])
            ->orderByDesc('calc_date')
            ->limit(1)
            ->get();
        // Skip if no previous week found.
        if (!$prev->isset()) {
            return;
        }
        $this->secondary['prev'] = Namespaces::callStatic('PowerRankings', 'query')
            ->where(array_filter([
                'season' => $prev->season,
                'game_type' => $prev->game_type ?? false,
                'week' => $prev->week,
            ]))
            ->get();
        foreach ($this->secondary['ranks'] as $rank) {
            $rank->last_week = $this->secondary['prev']->where('team_id', $rank->team_id)->rank;
        }
    }

    /**
     * Load the games for a given power ranking week
     * @return void
     */
    protected function loadSecondaryGames(): void
    {
        $this->secondary['games'] = Namespaces::callStatic('Schedule', 'query')
            ->where(array_filter([
                'season' => $this->season,
                'game_type' => $this->game_type ?? false,
            ]))
            ->whereBetween('game_date', [
                $this->date_from,
                $this->date_to,
            ])
            ->whereNotNull('status')
            ->whereNotIn('status', ['PPD', 'CNC'])
            ->orderBy('game_date')
            ->orderBy('game_time')
            ->get();
        // Parse the games in to per-team results.
        $this->loadSecondaryGamesParse();
    }

    /**
     * Parse the loaded games into a list for each team
     * @return void
     */
    protected function loadSecondaryGamesParse(): void
    {
        $by_team = array_fill_keys($this->secondary['ranks']->unique('team_id'), []);
        foreach ($this->secondary['games'] as $game) {
            $is_tie = ($game->home_score == $game->visitor_score);
            $status = $game->status_disp;
            // Home.
            $by_team[$game->home_id][] = (object)[
                'venue' => 'v',
                'opp' => $game->visitor_id,
                'res' => ($is_tie ? 'tie' : ($game->home_score > $game->visitor_score ? 'win' : 'loss')),
                'score_for' => $game->home_score,
                'score_agst' => $game->visitor_score,
                'status' => $status,
            ];
            // Visitor.
            $by_team[$game->visitor_id][] = (object)[
                'venue' => '@',
                'opp' => $game->home_id,
                'res' => ($is_tie ? 'tie' : ($game->visitor_score > $game->home_score ? 'win' : 'loss')),
                'score_for' => $game->visitor_score,
                'score_agst' => $game->home_score,
                'status' => $status,
            ];
        }

        // Tie back to the data.
        foreach ($this->secondary['ranks'] as $rank) {
            $rank->games = $by_team[$rank->team_id];
        }
    }

    /**
     * Get the full list of available, calculated, power rank weeks
     * @param integer $season The season being viewed.
     * @return self Collection of Power Rank week objects
     */
    public static function getAvailableWeeks(int $season): self
    {
        return static::query()
            ->where('season', $season)
            ->whereNotNull('when_run')
            ->orderByDesc('calc_date')
            ->get();
    }
}
