<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;

class PowerRankings extends Instance
{
    /**
     * Get the week info for each row returned
     * @return void
     */
    protected function loadSecondaryWeeks(): void
    {
        $weeks = Namespaces::callStatic('PowerRankingsWeeks', 'query')
            ->where('season', $this->count() ? $this->max('season') : 2099)
            ->whereIn('week', $this->unique('week'))
            ->get();
        // Now tie-together.
        foreach ($this->data as $i => $rank) {
            $this->data[$i]['week_id'] = $rank['week'];
            $this->data[$i]['week'] = $weeks->where('week', $rank['week']);
        }
    }
}
