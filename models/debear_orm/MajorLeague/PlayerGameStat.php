<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Traits\Stats as StatsTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class PlayerGameStat extends Instance
{
    use StatsTrait;

    /**
     * Get the full game info for the games in the dataset
     * @return void
     */
    protected function loadSecondaryGames(): void
    {
        // Determine the games to load.
        $games = array_unique(array_map(function ($stat) {
            return "{$stat['season']}:{$stat['game_type']}:{$stat['game_id']}";
        }, $this->data));
        // Load the info for these days.
        $query = Namespaces::callStatic('Schedule', 'query')
            ->select('*')
            ->selectRaw('CONCAT(season, ":", game_type, ":", game_id) AS uniq_ref');
        foreach ($games as $i => $game) {
            list($season, $game_type, $game_id) = explode(':', $game);
            $query->orWhere(function (Builder $where) use ($season, $game_type, $game_id) {
                $where->where([
                    ['season', '=', $season],
                    ['game_type', '=', $game_type],
                    ['game_id', '=', $game_id],
                ]);
            });
        }
        // Run, and tie back.
        $sched = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        foreach ($this->data as $i => $stat) {
            $ref = "{$stat['season']}:{$stat['game_type']}:{$stat['game_id']}";
            $this->data[$i]['schedule'] = $sched->where('uniq_ref', $ref);
        }
    }
}
