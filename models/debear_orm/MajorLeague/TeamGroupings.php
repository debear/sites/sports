<?php

namespace DeBear\ORM\Sports\MajorLeague;

use Illuminate\Support\Facades\DB;
use DeBear\ORM\Base\Instance;
use DeBear\ORM\Sports\MajorLeague\Team;
use DeBear\Helpers\Sports\Namespaces;

class TeamGroupings extends Instance
{
    /**
     * Load the team groupings for the season
     * @param integer $season The season we are processing.
     * @return self The teams and their groupings
     */
    public static function load(int $season) /*: self */
    {
        // The current teams, wih their groupings.
        $teams = static::loadTeams($season);

        // Then their grouping details.
        $div_ids = $teams->unique('grouping_id');
        $tbl_group = Namespaces::callStatic('Groupings', 'getTable');
        $groupings = Namespaces::callStatic('Groupings', 'query')
            ->select("$tbl_group.*")
            ->leftJoin("$tbl_group AS group_conf", "group_conf.parent_id", '=', "$tbl_group.grouping_id")
            ->whereIn("$tbl_group.grouping_id", $div_ids)
            ->orWhereIn("group_conf.grouping_id", $div_ids)
            ->groupBy("$tbl_group.grouping_id")
            ->orderBy("$tbl_group.type")
            ->orderBy("$tbl_group.order")
            ->get();

        // Now merge the two together.
        $confs = $groupings->where('type', 'conf');
        foreach ($confs as $conf) {
            // Divisions.
            $conf->divs = $groupings->where('parent_id', $conf->grouping_id);
            foreach ($conf->divs as $div) {
                // Teams in this division.
                $div->teams = $teams->where('grouping_id', $div->grouping_id);
            }
            // Teams in this conference.
            $conf->teams = $teams->whereIn('grouping_id', $conf->divs->unique('grouping_id'));
        }
        return $confs;
    }

    /**
     * Get the list of team taking part in a given season
     * @param integer $season The season we are processing.
     * @return Team The list of team in that season
     */
    public static function loadTeams(int $season) /*: Team */
    {
        $tbl_team = Namespaces::callStatic('Team', 'getTable');
        $tbl_team_group = Namespaces::callStatic('TeamGroupings', 'getTable');
        $tbl_naming = Namespaces::callStatic('TeamNaming', 'getTable');
        return Namespaces::callStatic('Team', 'query')
            ->select("$tbl_team.*", "$tbl_team_group.grouping_id")
            ->selectRaw("IFNULL($tbl_naming.alt_city, $tbl_team.city) AS city")
            ->selectRaw("IFNULL($tbl_naming.alt_franchise, $tbl_team.franchise) AS franchise")
            ->leftJoin($tbl_naming, function ($join) use ($tbl_naming, $tbl_team, $season) {
                $join->whereRaw("IFNULL($tbl_naming.alt_team_id, $tbl_naming.team_id) = $tbl_team.team_id")
                    ->where("$tbl_naming.season_from", '<=', $season)
                    ->where(DB::raw("IFNULL($tbl_naming.season_to, 2099)"), '>=', $season);
            })->join($tbl_team_group, "$tbl_team_group.team_id", '=', "$tbl_team.team_id")
            ->where([
                ["$tbl_team_group.season_from", '<=', $season],
                [DB::raw("IFNULL($tbl_team_group.season_to, 2099)"), '>=', $season],
            ])->orderBy('city')
            ->orderBy('franchise')
            ->get();
    }
}
