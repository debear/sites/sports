<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Highcharts;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Repositories\Time;

class Team extends Instance
{
    /**
     * Combine the city and franchise in to a single attribute
     * @return string The team name combined from its constituent parts
     */
    public function getNameAttribute(): string
    {
        return join(' ', [$this->city, $this->franchise]);
    }

    /**
     * Convert the team into a codified name
     * @return string The codified team city and franchise names
     */
    public function getNameCodifiedAttribute(): string
    {
        return Strings::codifyURL($this->name);
    }

    /**
     * Create a responsive team name, where the city is hidden in narrower views
     * @return string The markup for a responded team name
     */
    public function getNameResponsiveAttribute(): string
    {
        return '<span class="hidden-m hidden-tp">' . $this->city . '</span> ' . $this->franchise;
    }

    /**
     * Form the URL to link to this team
     * @return string The team URL
     */
    public function getLinkAttribute(): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . '/teams/' . $this->name_codified;
    }

    /**
     * Return the CSS used to highlight the row in the team's specific CSS colours
     * @return string The applicable CSS attribute
     */
    public function rowCSS(): string
    {
        return 'row_' . FrameworkConfig::get('debear.section.code') . '-' . $this->team_id;
    }

    /**
     * Return the CSS used to render a sized version of the team's (current) logo
     * @param string $size The requested logo size (Default: tiny).
     * @return string The applicable CSS attribute
     */
    public function logoCSS(string $size = 'tiny'): string
    {
        return static::buildLogoCSS($this->team_id, $size);
    }

    /**
     * Build the generic CSS attribute for a team logo
     * @param string $team_id The team ID for the logo we want built.
     * @param string $size    The requested logo size (Default: tiny).
     * @return string The applicable CSS attribute
     */
    public static function buildLogoCSS(string $team_id, string $size = 'tiny'): string
    {
        return 'team-' . FrameworkConfig::get('debear.section.code') . '-'
            . ($size != 'tiny' ? "$size-" : '')
            . $team_id;
    }

    /**
     * Return the CSS used to render a sized version of the team's (current) logo on the right of the content
     * @param string $size The requested logo size (Default: tiny).
     * @return string The applicable CSS attribute
     */
    public function logoRightCSS(string $size = 'tiny'): string
    {
        return 'team-' . FrameworkConfig::get('debear.section.code') . '-'
            . ($size != 'tiny' ? "$size-" : '')
            . "right-{$this->team_id}";
    }

    /**
     * Return the URL for the team's logo
     * @param string|array $size The requested image size (Default: large), as a string or array.
     * @return string URL for the team's logo
     */
    public function imageURL(string|array $size = 'large'): string
    {
        return static::buildImageURL($this->team_id ?? '', $size);
    }

    /**
     * Return the URL for a missing team logo
     * @param string|array $size The requested image size (Default: large), as a string or array.
     * @return string URL for the missing team logo
     */
    public static function silhouetteURL(string|array $size = 'large'): string
    {
        return static::buildImageURL(null, $size);
    }

    /**
     * Get the URL for the team's logo
     * @param string       $file The team ID, null if a placeholder image is to be rendered.
     * @param string|array $opt  The requested image size (Default: large), as a string or array.
     * @return string URL for the team's logo
     */
    protected static function buildImageURL(?string $file, string|array $opt): string
    {
        $img = static::buildImagePath($file, $opt);
        // Return through the obfuscator.
        $size = is_string($opt) ? $opt : $opt['size'];
        $size_opt = FrameworkConfig::get("debear.cdn.sizes.$size");
        return HTTP::buildCDNURLs("rs=1&preserve=1&i=$img&w={$size_opt['w']}&h={$size_opt['h']}");
    }

    /**
     * Ensure the passed image options meet our needs (consistently)
     * @param string|array $opt Customisation options.
     * @return array The parsed image options
     */
    protected static function imageOpt(string|array $opt): array
    {
        return Arrays::merge(
            // Defaults.
            [
                'size' => 'large',
                'site' => FrameworkConfig::get('debear.url.sub'),
                'sport' => FrameworkConfig::get('debear.section.endpoint'),
            ],
            // Passed in.
            is_string($opt) ? ['size' => $opt] : $opt
        );
    }

    /**
     * Determine the on-disk path of the file to use for this entity's mugshot/logo
     * @param string       $file The team ID, null if a placeholder image is to be rendered.
     * @param string|array $opt  Customisation options.
     * @return string The path-on-disk of the appropriate image for the mugshot/logo to use for this entity
     */
    public static function buildImagePath(?string $file, string|array $opt): string
    {
        $opt = static::imageOpt($opt);
        return '/' . join('/', [
            $opt['site'],
            $opt['sport'],
            isset($file) ? 'team' : 'misc',
            $opt['size'],
            ($file ?? strtoupper($opt['sport'])) . '.png',
        ]);
    }

    /**
     * Return the list of available (detailed) seasons played by this team
     * @return array The sorted (reverse chronologically) list of season numbers
     */
    public function seasonList(): array
    {
        $seasons = [];
        foreach ($this->history->groupings as $g) {
            $config = FrameworkConfig::get('debear.setup.season');
            $season_to = ($g->season_to ?? $config['max']);
            if ($g->season_from > $config['max'] || $season_to < $config['min']) {
                continue;
            }
            $seasons = array_merge($seasons, range(
                max($g->season_from, $config['min']),
                $season_to ?? $config['max']
            ));
        }
        rsort($seasons);
        return $seasons;
    }

    /**
     * Convert the schedule to a summary JSON object
     * @return array The schedule as an array of games
     */
    public function scheduleJson(): array
    {
        $ret = [];
        foreach ($this->schedule as $game) {
            $ret[] = [
                'game_type' => $game->game_type,
                'date_fmt' => $game->scheduleDate->name_short,
                'date_ymd' => (string)$game->game_date,
                'venue' => $game->team_venue,
                'opp' => $game->{$game->team_opp}->logoRightCSS(),
                'opp_wide' => $game->{$game->team_opp}->logoRightCSS('narrow_small'),
                'opp_id' => $game->{$game->team_opp}->team_id,
                'opp_city' => $game->{$game->team_opp}->city,
                'opp_franchise' => $game->{$game->team_opp}->franchise,
                'link' => ($game->isComplete() ? $game->link : ''),
                'info' => ($game->isComplete()
                    ? '<span class="' . $game->resultCSS() . '">' . $game->resultCode() . ' ' . $game->resultScore()
                        . '</span>'
                    : ($game->isAbandoned() ? $game->status_full : $game->start_time)),
            ];
        }
        return $ret;
    }

    /**
     * Build the Highcharts configuration for the Standings chart
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the team's standings progress
     */
    public function standingsChart(string $dom_id): array
    {
        $chart = Highcharts::new($dom_id);
        list($dates, $records, $pos, $wins, $loss) = $this->standingsChartSetup();
        $max_pos = Namespaces::callStatic('Standings', 'query')
            ->selectRaw('COUNT(DISTINCT pos_div) AS max_pos')
            ->where([
                ['season', '=', $this->standings->count() ? $this->standings->max('season') : 2099],
            ])->get()->max_pos;
        if (($max_pos % 2) == 0) {
            $max_pos++;
        }
        $max_res = max(max($wins), abs(min($loss)));
        // Setup the x-axis.
        $chart['xAxis'] = [
            'categories' => array_values($dates),
        ];
        $chart['extraJS'] = [
            'standingsRecords' => $records,
        ];
        // Setup the y-axis.
        $chart['yAxis'] = [
            [
                'labels' => [
                    'formatter' => '', // Defined in team.js.
                ],
                'title' => ['text' => 'Pos'],
                'reversed' => true,
                'startOnTick' => true,
                'endOnTick' => false,
                'tickPositions' => array_values(array_filter(range(1, $max_pos), function ($a) {
                    return ($a % 2) !== 0;
                })),
                'min' => 1, 'max' => $max_pos,
            ],
            [
                'labels' => ['enabled' => false],
                'title' => ['enabled' => false],
                'opposite' => true,
                'gridZIndex' => 2,
                'tickPositions' => [0 - $max_res, 0, $max_res],
                'min' => 0 - $max_res, 'max' => $max_res,
            ],
        ];
        // General chart setup.
        $chart['plotOptions'] = [
            'spline' => [
                'marker' => ['enabled' => false],
                'events' => ['legendItemClick' => ''], // Defined in team.js.
            ],
            'column' => [
                'stacking' => 'normal',
                'pointPadding' => 0,
                'groupPadding' => 0,
                'events' => ['legendItemClick' => ''], // Defined in team.js.
            ],
        ];
        $chart['legend'] = [
            'enabled' => false,
        ];
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => '', // Defined in team.js.
        ];
        // The data points.
        $chart['series'] = [
            ['type' => 'spline', 'yAxis' => 0, 'zIndex' => 1, 'data' => array_values($pos)],
            ['type' => 'column', 'yAxis' => 1, 'zIndex' => 0, 'data' => array_values($wins)],
            ['type' => 'column', 'yAxis' => 1, 'zIndex' => 0, 'data' => array_values($loss)],
        ];
        return $chart;
    }

    /**
     * Prepare the various data for the standings chart rendering
     * @return array The various components for the chart
     */
    protected function standingsChartSetup(): array
    {
        $season = $this->standings->count() ? $this->standings->max('season') : 2099;
        $date_col = FrameworkConfig::get('debear.setup.standings.date-col');
        $is_weekly = ($date_col == 'week');
        $scheddate_col = ($is_weekly ? $date_col : 'date');
        $sched_col = ($is_weekly ? $date_col : 'game_date');
        $dates = $records = $pos = $wins = $loss = [];
        $query = Namespaces::callStatic('ScheduleDate', 'query')
            ->select('*')
            ->selectRaw("$scheddate_col AS date_fmt")
            ->where('season', $season);
        if ($is_weekly) {
            $query->where('game_type', 'regular');
        } else {
            // On or before the last regular season date.
            $last_regular = Namespaces::callStatic('Schedule', 'query')
                ->selectRaw('MAX(game_date) AS last_regular')
                ->where('season', $season)
                ->where('game_type', 'regular')
                ->get(['info-query' => true])
                ->last_regular;
            $query->where($scheddate_col, '<=', $last_regular);
        }
        $all_dates = $query->orderBy($scheddate_col)->get();
        // Default values.
        foreach ($all_dates as $i => $d) {
            $date = $d->date_fmt;
            $dates[$date] = $d->name_short;
            $s = $this->standings->where($date_col, $date);
            if ($s->isset()) {
                $pos[$date] = $s->pos_div;
                $records[$i] = $s->record . (isset($s->pts) ? ', ' . Format::pluralise($s->pts, 'pt') : '');
                // Was there a game on this date too?
                $game = $this->schedule->where($sched_col, $date);
                if ($game->isset() && $game->isComplete()) {
                    // Win, Loss or Tie?
                    if ($game->team_score >= $game->opp_score) {
                        $wins[$date] = $game->team_score - $game->opp_score;
                        $loss[$date] = 0;
                    } else {
                        $wins[$date] = 0;
                        $loss[$date] = $game->team_score - $game->opp_score;
                    }
                } else {
                    $wins[$date] = $loss[$date] = 0;
                }
            } else {
                $pos[$date] = $wins[$date] = $loss[$date] = null;
            }
        }
        return [$dates, $records, $pos, $wins, $loss];
    }

    /**
     * Build the Highcharts configuration for the Power Ranks chart
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the team's power ranking progress
     */
    public function powerRanksChart(string $dom_id): array
    {
        $chart = Highcharts::new($dom_id);
        list($weeks, $ranks) = $this->powerRanksChartSetup();
        $max_pos = Namespaces::callStatic('PowerRankings', 'query')
            ->selectRaw('COUNT(DISTINCT `rank`) AS max_pos')
            ->where([
                ['season', '=', $this->power_ranks->count() ? $this->power_ranks->max('season') : 2099],
                ['week', '=', 1],
            ])->get()->max_pos;
        // Setup the x-axis.
        $chart['xAxis'] = [
            'categories' => array_keys($ranks),
            'title' => ['text' => 'Week'],
        ];
        $chart['extraJS'] = [
            'powerRankWeeks' => $weeks,
        ];
        // Setup the y-axis.
        $chart['yAxis'] = [[
            'labels' => [
                'formatter' => '', // Defined in team.js.
            ],
            'title' => ['text' => 'Ranking'],
            'reversed' => true,
            'startOnTick' => true,
            'endOnTick' => false,
            'tickPositions' => array_values(array_filter(range(1, $max_pos), function ($a) {
                return ($a % 2) !== 0;
            })),
            'min' => 1, 'max' => $max_pos,
        ]];
        // General chart setup.
        $chart['plotOptions'] = [
            'spline' => [
                'marker' => ['enabled' => false],
                'events' => ['legendItemClick' => ''], // Defined in team.js.
            ],
        ];
        $chart['legend'] = [
            'enabled' => false,
        ];
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => '', // Defined in team.js.
        ];
        // The data points.
        $chart['series'] = [[
            'type' => 'spline',
            'data' => array_values($ranks),
        ]];
        return $chart;
    }

    /**
     * Prepare the various data for the power ranks chart rendering
     * @return array The various components for the chart
     */
    protected function powerRanksChartSetup(): array
    {
        $weeks = $ranks = [];
        $all_weeks = Namespaces::callStatic('PowerRankingsWeeks', 'query')
            ->where('season', $this->power_ranks->count() ? $this->power_ranks->max('season') : 2099)
            ->orderBy('calc_date')
            ->get();
        // Default values.
        foreach ($all_weeks as $week) {
            $weeks[$week->week] = $week->name_full;
            $ranks[$week->week] = null;
        }
        // Actual values.
        foreach ($this->power_ranks as $r) {
            $ranks[$r->week_id] = $r->rank;
        }
        return [$weeks, $ranks];
    }

    /**
     * Load team info for a given list of ID's
     * @param array $team_ids  The team_id's to lookup.
     * @param array $secondary Additional information to load with the team.
     * @param array $opt       Optional query configuration details.
     * @return self The ORM objects
     */
    public static function load(array $team_ids, array $secondary = [], array $opt = []): self
    {
        $query_season = $opt['season'] ?? FrameworkConfig::get('debear.setup.season.viewing');
        $tbl_team = static::getTable();
        $tbl_naming = Namespaces::callStatic('TeamNaming', 'getTable');
        $query = static::query()
            ->select("$tbl_team.*")
            ->selectRaw("IFNULL($tbl_naming.alt_city, $tbl_team.city) AS city")
            ->selectRaw("IFNULL($tbl_naming.alt_franchise, $tbl_team.franchise) AS franchise");
        if (isset($opt['season'])) {
            $query_season = str_replace('TBL', $tbl_team, $query_season);
            $query->selectRaw("$query_season AS season");
        }
        return $query->leftJoin($tbl_naming, function ($join) use ($tbl_naming, $tbl_team, $query_season) {
            $join->whereRaw("IFNULL($tbl_naming.alt_team_id, $tbl_naming.team_id) = $tbl_team.team_id")
                ->where("$tbl_naming.season_from", '<=', DB::raw($query_season))
                ->where(DB::raw("IFNULL($tbl_naming.season_to, 2099)"), '>=', DB::raw($query_season));
        })->whereIn("$tbl_team.team_id", $team_ids)
        ->get()
        ->loadSecondary(array_unique(array_merge(['venue', 'groupings'], $secondary)));
    }

    /**
     * Determine the appropriate team from a given name slug
     * @param string $team_name The codified city and franchise name.
     * @return ?string The equivalent team ID for the given codified name
     */
    public static function getIDFromCodifiedName(string $team_name): ?string
    {
        $tbl_team = static::getTable();
        $tbl_naming = Namespaces::callStatic('TeamNaming', 'getTable');
        $seasons = FrameworkConfig::get('debear.setup.season');
        return static::query($tbl_naming)
            ->select([
                "$tbl_naming.team_id",
                "$tbl_naming.alt_team_id",
                "$tbl_naming.season_from",
                "$tbl_naming.season_to",
            ])->selectRaw("IFNULL($tbl_naming.alt_city, $tbl_team.city) AS city")
            ->selectRaw("IFNULL($tbl_naming.alt_franchise, $tbl_team.franchise) AS franchise")
            ->leftJoin($tbl_team, function ($join) use ($tbl_team, $tbl_naming) {
                $join->whereRaw("IFNULL($tbl_naming.alt_team_id, $tbl_naming.team_id) = $tbl_team.team_id");
            })->whereRaw("IFNULL($tbl_naming.season_to, ?) >= ?", [$seasons['max'], $seasons['min']])
            ->orderByDesc("$tbl_naming.season_from")
            ->get()
            ->filter(function ($a) use ($team_name) {
                return (new static($a))->name_codified == $team_name;
            })->team_id;
    }

    /**
     * Load the additional secondary info for when viewing an individual team
     * @param string $tab The information to be loaded.
     * @return void
     */
    public function loadDetailedSecondary(string $tab): void
    {
        $tab_map = [
            'home' => [
                'divstandings', // Current divisional standings.
                'recentschedule',
                'stats',
                'leaders',
            ],
            'schedule' => ['schedule'],
            'roster' => ['rosters'],
            'standings' => [
                'standings', // Team's historical standings for the season.
                'schedule',
            ],
            'power-ranks' => ['powerrankings'],
            'history' => ['history'],
        ];
        $this->loadSecondary($tab_map[$tab]);
    }

    /**
     * Determine the current venue for each team in the dataset
     * @return void
     */
    protected function loadSecondaryVenue(): void
    {
        $season = $this->data[0]['season'] ?? FrameworkConfig::get('debear.setup.season.viewing');
        $this->secondary['venue'] = Namespaces::callStatic('TeamVenue', 'query')
            ->whereIn('team_id', $this->unique('team_id'))
            ->where([
                ['season_from', '<=', $season],
                [DB::Raw('IFNULL(season_to, 2099)'), '>=', $season],
            ])
            ->get();
        foreach ($this->data as $i => $team) {
            $this->data[$i]['currentVenue'] = $this->secondary['venue']->where('team_id', $team['team_id']);
        }
    }

    /**
     * Determine the groupings for each team in the dataset
     * @return void
     */
    protected function loadSecondaryGroupings(): void
    {
        $season = $this->data[0]['season'] ?? FrameworkConfig::get('debear.setup.season.viewing');
        $team_ids = $this->unique('team_id');
        $tbl_grp = Namespaces::callStatic('Groupings', 'getTable');
        $tbl_teamgrp = Namespaces::callStatic('TeamGroupings', 'getTable');
        // First, the divisions.
        $this->secondary['divs'] = Namespaces::callStatic('Groupings', 'query')
            ->select(["$tbl_grp.*", "$tbl_teamgrp.team_id"])
            ->join($tbl_teamgrp, function ($join) use ($tbl_teamgrp, $tbl_grp, $season, $team_ids) {
                $join->whereIn("$tbl_teamgrp.team_id", $team_ids)
                    ->where("$tbl_teamgrp.season_from", '<=', $season)
                    ->where(DB::Raw("IFNULL($tbl_teamgrp.season_to, 2099)"), '>=', $season)
                    ->on("$tbl_teamgrp.grouping_id", '=', "$tbl_grp.grouping_id");
            })->get();
        // Then the conferences.
        $this->secondary['conf'] = Namespaces::callStatic('Groupings', 'query')
            ->whereIn('grouping_id', $this->secondary['divs']->unique('parent_id'))
            ->get();
        // And finally tie together.
        $conf_map = [];
        foreach ($this->data as $i => $team) {
            // Team is tied to the division.
            $this->data[$i]['division'] = $this->secondary['divs']->where('team_id', $team['team_id']);
            // And the division to the conference.
            $conf_id = $this->data[$i]['division']->parent_id ?? -1;
            if (!isset($conf_map[$conf_id])) {
                $conf_map[$conf_id] = $this->secondary['conf']->where('grouping_id', $conf_id);
            }
            $this->data[$i]['conference'] = $conf_map[$conf_id];
        }
    }

    /**
     * Get the current division standings for the given teams
     * @return void
     */
    protected function loadSecondaryDivStandings(): void
    {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $groupings = $this->secondary['divs']->unique('grouping_id');
        // Get the appropriate teams.
        $groups = Namespaces::callStatic('TeamGroupings', 'query')
            ->select(['team_id', 'grouping_id'])
            ->where('season_from', '<=', $season)
            ->where(DB::raw('IFNULL(season_to, 2099)'), '>=', $season)
            ->whereIn('grouping_id', $groupings)
            ->get();
        // And now standings.
        $standings = Namespaces::callStatic('Standings', 'query')
            ->where([
                ['season', '=', $season],
                Namespaces::callStatic('Standings', 'getCurrentDate', [$season]),
            ])->whereIn('team_id', $groups->unique('team_id'))
            ->orderBy('pos_div')
            ->get()
            ->loadSecondary(['teams']);
        // Tie-together into per-div objects.
        $div_standings = [
            '-1' => $standings->where('team_id', ''), // Empty matches.
        ];
        foreach ($groupings as $id) {
            $div_standings[$id] = $standings->whereIn('team_id', $groups->where('grouping_id', $id)->unique('team_id'));
        }
        // And then bind to the team(s).
        foreach ($this->data as $i => $team) {
            $grouping_id = $groups->where('team_id', $team['team_id'])->grouping_id ?? -1;
            $this->data[$i]['divStandings'] = $div_standings[$grouping_id];
            $this->data[$i]['currentStandings'] = $div_standings[$grouping_id]->where('team_id', $team['team_id']);
        }
    }

    /**
     * Get the historical standings for the given teams this season
     * @return void
     */
    protected function loadSecondaryStandings(): void
    {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $standings = Namespaces::callStatic('Standings', 'query')
            ->where('season', '=', $season)
            ->whereIn('team_id', $this->unique('team_id'))
            ->orderBy(FrameworkConfig::get('debear.setup.standings.date-col'))
            ->get();
        // And then bind to the team(s).
        foreach ($this->data as $i => $team) {
            $this->data[$i]['standings'] = $standings->where('team_id', $team['team_id']);
        }
    }

    /**
     * Get the complete schedule for the given teams
     * @return void
     */
    protected function loadSecondarySchedule(): void
    {
        // NOTE: This logic assumes 1, or very few, total rows found.
        // It will need a re-think if used by a large number of rows.
        foreach ($this->data as $i => $team) {
            $this->data[$i]['schedule'] = Namespaces::callStatic('Schedule', 'query')
                ->select('*')
                ->selectRaw('IF(home = ?, "v", "@") AS team_venue', [$team['team_id']])
                ->selectRaw('IF(home = ?, "home", "visitor") AS team_type', [$team['team_id']])
                ->selectRaw('IF(home = ?, "visitor", "home") AS team_opp', [$team['team_id']])
                ->selectRaw('IF(home = ?, home_score, visitor_score) AS team_score', [$team['team_id']])
                ->selectRaw('IF(home = ?, visitor_score, home_score) AS opp_score', [$team['team_id']])
                ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
                ->whereRaw('? IN (home, visitor)', [$team['team_id']])
                ->orderBy('game_date')
                ->orderBy('game_time')
                ->get()
                ->loadSecondary(['scheduledate']);
        }
    }

    /**
     * Get the recent schedule for the given teams
     * @return void
     */
    protected function loadSecondaryRecentSchedule(): void
    {
        // NOTE: This logic assumes 1, or very few, total rows found.
        // It will need a re-think if used by a large number of rows.
        $num_games = FrameworkConfig::get('debear.setup.teams.summary-games');
        $max_sched = floor($num_games / 2);
        foreach ($this->data as $i => $team) {
            // Unplayed games.
            $games_sched = Namespaces::callStatic('Schedule', 'query')
                ->select('*')
                ->selectRaw('IF(home = ?, "v", "@") AS team_venue', [$team['team_id']])
                ->selectRaw('IF(home = ?, "home", "visitor") AS team_type', [$team['team_id']])
                ->selectRaw('IF(home = ?, "visitor", "home") AS team_opp', [$team['team_id']])
                ->whereRaw('? IN (home, visitor)', [$team['team_id']])
                ->where('game_date', '>=', Time::object()->getCurDate())
                ->orderBy('game_date')
                ->orderBy('game_time')
                ->limit($max_sched)
                ->get();
            // Completed games.
            $games_cmpl = Namespaces::callStatic('Schedule', 'query')
                ->select('*')
                ->selectRaw('IF(home = ?, "v", "@") AS team_venue', [$team['team_id']])
                ->selectRaw('IF(home = ?, "home", "visitor") AS team_type', [$team['team_id']])
                ->selectRaw('IF(home = ?, "visitor", "home") AS team_opp', [$team['team_id']])
                ->whereRaw('? IN (home, visitor)', [$team['team_id']])
                ->where('game_date', '<', Time::object()->getCurDate())
                ->orderByDesc('game_date')
                ->orderByDesc('game_time')
                ->limit($num_games - $games_sched->count())
                ->get()
                ->reverse();
            // Now tie together.
            $this->data[$i]['recentSchedule'] = $games_cmpl->merge($games_sched);
        }
    }

    /**
     * Get the individual team rosters for the given teams
     * @return void
     */
    protected function loadSecondaryRosters(): void
    {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        // Determine the roster date.
        $the_date = Namespaces::callStatic('TeamRoster', 'query')
            ->selectRaw('MAX(the_date) AS max_date')
            ->where('season', $season)
            ->get()
            ->max_date;
        // And then get the rosters.
        $query = Namespaces::callStatic('TeamRoster', 'query')
            ->where([
                ['season', '=', $season],
                ['the_date', '=', $the_date],
            ])->whereIn('team_id', $this->unique('team_id'));
        if (!FrameworkConfig::get('debear.setup.rosters.no_status_column')) {
            $query->orderByRaw("`player_status` <> 'active'");
        }
        $rosters = $query->orderByRaw('`jersey` IS NULL')
            ->orderBy('jersey')
            ->get()
            ->loadSecondary(['players']);
        // Now tie-together to the teams.
        foreach ($this->data as $i => $team) {
            $this->data[$i]['roster'] = $rosters->where('team_id', $team['team_id']);
        }
    }

    /**
     * Get the individual team leaders for the given teams
     * @return void
     */
    protected function loadSecondaryLeaders(): void
    {
        $teams = $this->unique('team_id');
        // Get the summary player stats.
        $stats = FrameworkConfig::get('debear.setup.stats');
        $player_stats = array_filter($stats['summary'], function ($a) {
            return $a['type'] == 'player';
        });
        $classes = array_unique(array_column($player_stats, 'class'));
        // Get the appropriate stats for each of the teams.
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $season_type = 'regular';
        $team_stats = [];
        foreach ($classes as $class) {
            $obj_sort = $stats['details']['player'][$class]['class']['sort'];
            $obj_stat = (new $obj_sort())->getStatsClass();
            // Determine the categories this class applies to and load the stat leaders.
            $cols = array_values(array_column(array_filter($player_stats, function ($a) use ($class) {
                return $a['class'] == $class;
            }), 'column'));
            $team_sort = $obj_sort::loadTeamLeaders($season, $season_type, $teams, $cols);
            // Get the player equivalents.
            $team_stat = $obj_stat::loadByTeamPlayers($season, $season_type, $team_sort->unique('player_key'))
                ->loadSecondary(['players']);
            // Bind for later.
            $team_stats[$class] = [
                'sort' => $team_sort,
                'stat' => $team_stat,
            ];
        }
        // Now tie-together to the teams.
        foreach ($this->data as $i => $team) {
            $s = [];
            foreach ($team_stats as $c => $b) {
                $s[$c] = (object)[
                    'sort' => $b['sort']->where('team_id', $team['team_id']),
                    'stat' => $b['stat']->where('team_id', $team['team_id']),
                ];
            }
            $this->data[$i]['leaders'] = (object)$s;
        }
    }

    /**
     * Get the team stats for the given teams
     * @return void
     */
    protected function loadSecondaryStats(): void
    {
        $teams = $this->unique('team_id');
        // Get the summary team stats.
        $stats = FrameworkConfig::get('debear.setup.stats');
        $classes = array_unique(array_column(array_filter($stats['summary'], function ($a) {
            return $a['type'] == 'team';
        }), 'class'));
        // Get the appropriate stats for each of the teams.
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $season_type = 'regular';
        $team_stats = [];
        foreach ($classes as $class) {
            $obj_sort = $stats['details']['team'][$class]['class']['sort'];
            $obj_stat = (new $obj_sort())->getStatsClass();
            $where = array_filter([
                ['season', '=', $season],
                ['season_type', '=', $season_type],
                $obj_sort::getQueryExtra() ?? [],
            ]);
            $team_stats[$class] = [
                'sort' => $obj_sort::query()->where($where)->whereIn('team_id', $teams)->get(),
                'stat' => $obj_stat::loadByTeam($season, $season_type, $teams),
            ];
        }
        // Now tie-together to the teams.
        foreach ($this->data as $i => $team) {
            $s = [];
            foreach ($team_stats as $c => $b) {
                $s[$c] = (object)[
                    'sort' => $b['sort']->where('team_id', $team['team_id']),
                    'stat' => $b['stat']->where('team_id', $team['team_id']),
                ];
            }
            $this->data[$i]['stats'] = (object)$s;
        }
    }

    /**
     * Get the power rank history for the given teams
     * @return void
     */
    protected function loadSecondaryPowerRankings(): void
    {
        $power_ranks = Namespaces::callStatic('PowerRankings', 'query')
            ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
            ->whereIn('team_id', $this->unique('team_id'))
            ->orderBy('week')
            ->get()
            ->loadSecondary(['weeks']);
        // Now tie-together to the teams.
        foreach ($this->data as $i => $team) {
            $this->data[$i]['power_ranks'] = $power_ranks->where('team_id', $team['team_id']);
        }
    }

    /**
     * Get the historical info for the given teams
     * @return void
     */
    protected function loadSecondaryHistory(): void
    {
        $teams = $this->unique('team_id');
        // Names.
        $tbl_teams = static::getTable();
        $tbl_names = Namespaces::callStatic('TeamNaming', 'getTable');
        $names = Namespaces::callStatic('TeamNaming', 'query')
            ->select(["$tbl_names.team_id", "$tbl_names.alt_team_id", "$tbl_names.season_from", "$tbl_names.season_to"])
            ->selectRaw("IFNULL($tbl_names.alt_city, $tbl_teams.city) AS city")
            ->selectRaw("IFNULL($tbl_names.alt_franchise, $tbl_teams.franchise) AS franchise")
            ->leftJoin($tbl_teams, function ($join) use ($tbl_names, $tbl_teams) {
                $join->whereRaw("IFNULL($tbl_names.alt_team_id, $tbl_names.team_id) = $tbl_teams.team_id");
            })
            ->whereIn("$tbl_names.team_id", $teams)
            ->orderBy("$tbl_names.team_id")
            ->orderBy("$tbl_names.season_from")
            ->get();
        $teams_all = array_unique(array_filter(array_merge($names->unique('alt_team_id'), $teams)));
        // Groupings.
        $team_groupings = Namespaces::callStatic('TeamGroupings', 'query')
            ->whereIn('team_id', $teams_all)
            ->orderByDesc('season_from')
            ->orderBy('team_id')
            ->get();
        $tbl_group = Namespaces::callStatic('Groupings', 'getTable');
        $groupings = Namespaces::callStatic('Groupings', 'query')
            ->select("$tbl_group.*")
            ->selectRaw("IF($tbl_group.type = 'league', $tbl_group.grouping_id,
                IF(TBL_CONF.type = 'league', TBL_CONF.grouping_id, TBL_LEAGUE.grouping_id)) AS league_id")
            ->leftJoin("$tbl_group AS TBL_CONF", "$tbl_group.parent_id", '=', 'TBL_CONF.grouping_id')
            ->leftJoin("$tbl_group AS TBL_LEAGUE", 'TBL_CONF.parent_id', '=', 'TBL_LEAGUE.grouping_id')
            ->whereIn("$tbl_group.grouping_id", $team_groupings->unique('grouping_id'))
            ->get();
        $leagues = Namespaces::callStatic('Groupings', 'query')
            ->whereIn('grouping_id', $groupings->unique('league_id'))
            ->get();
        foreach ($groupings as $g) {
            $g->league = $leagues->where('grouping_id', $g->league_id);
        }
        foreach ($team_groupings as $g) {
            $g->grouping = $groupings->where('grouping_id', $g->grouping_id);
        }
        // Season-by-season Records.
        $history_reg = Namespaces::callStatic('TeamHistoryRegular', 'query')
            ->whereIn('team_id', $teams_all)
            ->orderByDesc('season')
            ->orderBy('team_id')
            ->get();
        $tbl_hpo = Namespaces::callStatic('TeamHistoryPlayoff', 'getTable');
        $history_po = Namespaces::callStatic('TeamHistoryPlayoff', 'query')
            ->select("$tbl_hpo.*")
            ->selectRaw(
                "IFNULL($tbl_hpo.opp_team_alt, CONCAT(
                    IFNULL($tbl_names.alt_city, $tbl_teams.city),
                    ' ',
                    IFNULL($tbl_names.alt_franchise, $tbl_teams.franchise)
                )) AS opp_team_alt"
            )->leftJoin($tbl_names, function ($join) use ($tbl_hpo, $tbl_names) {
                $join->whereRaw("IFNULL($tbl_names.alt_team_id, $tbl_names.team_id) = $tbl_hpo.opp_team_id")
                    ->whereBetween("$tbl_hpo.season", [
                        DB::raw("$tbl_names.season_from"),
                        DB::raw("IFNULL($tbl_names.season_to, 2099)"),
                    ]);
            })->leftJoin($tbl_teams, function ($join) use ($tbl_names, $tbl_teams) {
                $join->where("$tbl_teams.team_id", '=', DB::raw("IFNULL($tbl_names.alt_team_id, $tbl_names.team_id)"));
            })->whereIn("$tbl_hpo.team_id", $teams_all)
            ->orderBy("$tbl_hpo.season")
            ->orderBy("$tbl_hpo.team_id")
            ->orderByDesc("$tbl_hpo.round")
            ->get();
        // Title counts.
        $titles = Namespaces::callStatic('TeamHistoryTitles', 'query')
            ->whereIn('team_id', $teams_all)
            ->get();

        // Now tie all these back to the original teams.
        foreach ($this->data as $i => $team) {
            $team_ids = array_filter($names->where('team_id', $team['team_id'])->unique('alt_team_id')); // Historical.
            $team_ids[] = $team['team_id']; // Current.
            $this->data[$i]['history'] = (object)[
                'naming' => $names->whereIn('team_id', $team_ids),
                'groupings' => $team_groupings->whereIn('team_id', $team_ids),
                'titles' => $titles->whereIn('team_id', $team_ids),
                'seasons' => (object)[
                    'regular' => $history_reg->whereIn('team_id', $team_ids),
                    'playoff' => $history_po->whereIn('team_id', $team_ids),
                ],
            ];
        }
    }

    /**
     * Get the list of teams available in the database
     * @return self An ORM model containing all the (usable) teams
     */
    public static function getAllSitemap(): self
    {
        $tbl_team = static::getTable();
        $tbl_group = Namespaces::callStatic('TeamGroupings', 'getTable');
        $season_max = FrameworkConfig::get('debear.setup.season.max');
        return static::query()
            ->select("$tbl_team.*")
            ->selectRaw("MAX(IFNULL($tbl_group.season_to, ?)) AS max_season", [$season_max])
            ->join($tbl_group, function ($join) use ($tbl_group, $tbl_team, $season_max) {
                $join->on("$tbl_group.team_id", '=', "$tbl_team.team_id")
                    ->where(DB::raw("IFNULL($tbl_group.season_to, 2099)"), '>=', $season_max);
            })->groupBy("$tbl_team.team_id")
            ->orderBy("$tbl_team.team_id")
            ->get();
    }
}
