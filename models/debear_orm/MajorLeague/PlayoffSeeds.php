<?php

namespace DeBear\ORM\Sports\MajorLeague;

use Illuminate\Support\Facades\DB;
use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Repositories\InternalCache;

class PlayoffSeeds extends Instance
{
    /**
     * Return the seed as a display string
     * @return string The full seed display
     */
    public function getSeedFullAttribute(): string
    {
        // Divisional format?
        $config = InternalCache::object()->get('playoff-config');
        if (!$config['divisional']) {
            // No, so use the conference seed directly.
            return $this->seed;
        }
        // Determine if this is a divisional or wildcard place.
        $div_seed = substr($this->team->division->name, 0, 1) . $this->pos_div;
        return ($config['divisional']['wc'] && $this->pos_div > $config['divisional']['num']) ? 'WC' : $div_seed;
    }

    /**
     * Load the playoff seeds for a given season
     * @param integer $season The season's playoff we are viewing.
     * @param array   $seeds  An optional array of specfic seeds to load.
     * @return self An ORM object with the appropriate details
     */
    public static function load(int $season, ?array $seeds = null): self
    {
        $query = static::query()
            ->select('*')
            ->selectRaw('CONCAT(conf_id, ":", seed) AS seed_ref')
            ->where('season', $season);
        if (isset($seeds)) {
            $query->whereIn(DB::raw('CONCAT(conf_id, ":", seed)'), $seeds);
        }
        return $query->orderBy('conf_id')
            ->orderBy('seed')
            ->get()
            ->loadSecondary(['teams']);
    }

    /**
     * Load the team details for the relevant seeds found
     * @return void
     */
    public function loadSecondaryTeams(): void
    {
        $teams = Namespaces::callStatic('Team', 'load', [
            $this->unique('team_id'),
            ['venue', 'groupings'],
        ]);
        foreach ($this->data as $i => $seed) {
            $this->data[$i]['team'] = $teams->where('team_id', $seed['team_id']);
        }
    }
}
