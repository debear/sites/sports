<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Repositories\InternalCache;
use Illuminate\Database\Query\Builder;

class PlayoffSeries extends Instance
{
    /**
     * Summarise the playoff series
     * @return string Series summary
     */
    public function getSeriesSummaryAttribute(): string
    {
        // Championship round, thus trophy up for grabs?
        $all_series = FrameworkConfig::get('debear.setup.playoffs.series');
        $not_awarded = (bool) FrameworkConfig::get("debear.setup.playoffs.not-awarded.{$this->season}");
        $championship_round = ($not_awarded ? 0 : max(array_keys($all_series)));
        $round = substr(str_pad($this->round_code, 2, '0', STR_PAD_LEFT), 0, 1);
        $event = ($round == $championship_round ? FrameworkConfig::get('debear.setup.playoffs.trophy') : 'series');
        // Tied series?
        if ($this->higher_games == $this->lower_games) {
            return ucfirst($event) . " tied {$this->higher_games}&ndash;{$this->lower_games}";
        }
        // Which team leads?
        if ($this->higher_games > $this->lower_games) {
            $leader = $this->higher;
            $score = "{$this->higher_games}&ndash;{$this->lower_games}";
        } else {
            $leader = $this->lower;
            $score = "{$this->lower_games}&ndash;{$this->higher_games}";
        }
        // How long is the series?
        $series = $all_series[$round];
        if (!isset($series['wins'])) {
            foreach ($series as $s) {
                if (($s['from'] <= $this->season) && ($this->season <= $s['to'])) {
                    $series = $s;
                    break;
                }
            }
        }
        if ($series['wins'] == 1) {
            return "{$leader->franchise} advance to the next round";
        }
        // Series leading or won?
        $type = (substr($score, 0, 1) == $series['wins'] ? 'win' : 'lead');
        // Return.
        return "{$leader->franchise} $type the $event, $score";
    }

    /**
     * Parse the various configuration options into a consistent array
     * @param integer $season The season's playoff we are viewing.
     * @return array A deterministic array of options
     */
    public static function parseConfig(int $season): array
    {
        $ret = [];
        $config = FrameworkConfig::get('debear.setup.playoffs');
        // Header info?
        foreach (['info', 'template'] as $field) {
            if (isset($config[$field]) && is_array($config[$field])) {
                foreach ($config[$field] as $opt) {
                    if ($opt['from'] <= $season && $season <= $opt['to']) {
                        $ret[$field] = $opt[$field];
                    }
                }
            }
        }
        // Round robin and/or Divisional matchups?
        foreach (['round_robin', 'divisional'] as $field) {
            $ret[$field] = false;
            if (isset($config[$field]) && is_array($config[$field])) {
                foreach ($config[$field] as $opt) {
                    if (($opt['from'] <= $season) && ($season <= $opt['to'])) {
                        $ret[$field] = array_diff_key($opt, ['from' => true, 'to' => true]);
                    }
                }
            }
        }
        // Which series, and how may wins required?
        $ret['series'] = [];
        foreach (FrameworkConfig::get('debear.setup.playoffs.series') as $round => $opt) {
            if (isset($opt['wins'])) {
                // No logic, add unconditionally.
                $ret['series'][$round] = $opt;
            } else {
                // Find (possibly...) a season range that matches.
                foreach ($opt as $o) {
                    if (($o['from'] <= $season) && ($season <= $o['to'])) {
                        $ret['series'][$round] = array_diff_key($o, ['from' => true, 'to' => true]);
                    }
                }
            }
        }
        return $ret;
    }

    /**
     * Load the playoff series for a given season
     * @param integer $season The season's playoff we are viewing.
     * @param integer $code   An optional series code. If not supplied, all series will be loaded.
     * @return self An ORM object with the appropriate details
     */
    public static function load(int $season, ?int $code = null): self
    {
        $config = InternalCache::object()->get('playoff-config');
        $date_col = FrameworkConfig::get('debear.setup.playoffs.date_col');
        // First we need to determine the last date for each series.
        $max_query = static::query()
            ->selectRaw("MAX(CONCAT(round_code, ':', $date_col)) AS series_max")
            ->where('season', $season);
        if (isset($code)) {
            $max_query->where('round_code', $code);
        }
        $max_dates = $max_query->groupBy('round_code')->get();
        // Now build and run the query.
        $tbl_series = static::getTable();
        $tbl_sched = Namespaces::callStatic('Schedule', 'getTable');
        $query = static::query()
            ->select("$tbl_series.*");
        if (
            is_array($config)
            && ((isset($config['round_robin']) && is_array($config['round_robin']))
                || (isset($config['series'][0]) && is_array($config['series'][0])))
        ) {
            $query->selectRaw("SUBSTRING(LPAD($tbl_series.round_code, 2, '0'), 1, 1) AS round_num");
        } else {
            $query->selectRaw("IF(CAST(CAST($tbl_series.round_code AS CHAR) AS UNSIGNED) < 10, 1,
            SUBSTRING(LPAD($tbl_series.round_code, 2, '0'), 1, 1)) AS round_num");
        }
        $query->selectRaw("CONCAT($tbl_series.higher_conf_id, ':', $tbl_series.higher_seed) AS higher_seed_ref")
            ->selectRaw("CONCAT($tbl_series.lower_conf_id, ':', $tbl_series.lower_seed) AS lower_seed_ref")
            ->selectRaw("COUNT($tbl_sched.game_id) AS games_sched")
            ->leftJoin($tbl_sched, function ($join) use ($tbl_sched, $tbl_series) {
                $join->on("$tbl_sched.season", '=', "$tbl_series.season")
                    ->where("$tbl_sched.game_type", '=', 'playoff')
                    ->where("$tbl_sched.game_id", 'LIKE', DB::raw("CONCAT($tbl_series.round_code, '_')"));
            });
        foreach ($max_dates as $date) {
            list($round_code, $date) = explode(':', $date->series_max);
            $query->orWhere(function (Builder $where) use ($tbl_series, $date_col, $season, $date, $round_code) {
                $where->where([
                    ["$tbl_series.season", '=', $season],
                    ["$tbl_series.$date_col", '=', $date],
                    ["$tbl_series.round_code", '=', $round_code],
                ]);
            });
        }
        $res = $query->orWhere(DB::raw(1), DB::raw(0))
            ->groupBy("$tbl_series.round_code")
            ->orderBy("$tbl_series.round_code")
            ->get();
        if ($res->count()) {
            $res->loadSecondary(['teamshort']);
        }
        return $res;
    }

    /**
     * Load the summary team information for the participants in each playoff series.
     * @return void
     */
    protected function loadSecondaryTeamShort(): void
    {
        list($season) = $this->unique('season');
        $seed_refs = array_merge($this->unique('higher_seed_ref'), $this->unique('lower_seed_ref'));
        $tbl_seed = Namespaces::callStatic('PlayoffSeeds', 'getTable');
        $tbl_team = Namespaces::callStatic('Team', 'getTable');
        $tbl_naming = Namespaces::callStatic('TeamNaming', 'getTable');
        $tbl_group_team = Namespaces::callStatic('TeamGroupings', 'getTable');
        $tbl_group = Namespaces::callStatic('Groupings', 'getTable');
        $team_info = Namespaces::callStatic('Team', 'query', [$tbl_seed])
            ->select([
                "$tbl_team.*",
                DB::raw("CONCAT($tbl_seed.conf_id, ':', $tbl_seed.seed) AS seed_ref"),
                DB::raw("IFNULL($tbl_naming.alt_city, $tbl_team.city) AS city"),
                DB::raw("IFNULL($tbl_naming.alt_franchise, $tbl_team.franchise) AS franchise"),
                "group_conf.grouping_id AS conf_id",
                "group_conf.name AS conf_name",
                "group_conf.name_full AS conf_name_full",
                "group_div.grouping_id AS div_id",
                "group_div.name AS div_name",
                "group_div.name_full AS div_name_full",
            ])
            ->join($tbl_team, "$tbl_team.team_id", '=', "$tbl_seed.team_id")
            ->leftJoin($tbl_naming, function ($join) use ($tbl_naming, $tbl_seed) {
                $join->whereRaw("IFNULL($tbl_naming.alt_team_id, $tbl_naming.team_id) = $tbl_seed.team_id")
                    ->whereBetween("$tbl_seed.season", [
                        DB::raw("$tbl_naming.season_from"),
                        DB::raw("IFNULL($tbl_naming.season_to, 2099)")
                    ]);
            })->join($tbl_group_team, function ($join) use ($tbl_group_team, $tbl_seed) {
                $join->on("$tbl_group_team.team_id", '=', "$tbl_seed.team_id")
                    ->whereBetween("$tbl_seed.season", [
                        DB::raw("$tbl_group_team.season_from"),
                        DB::raw("IFNULL($tbl_group_team.season_to, 2099)")
                    ]);
            })->join("$tbl_group AS group_div", 'group_div.grouping_id', '=', "$tbl_group_team.grouping_id")
            ->join("$tbl_group AS group_conf", 'group_conf.grouping_id', '=', 'group_div.parent_id')
            ->where("$tbl_seed.season", $season)
            ->whereIn(DB::raw("CONCAT($tbl_seed.conf_id, ':', $tbl_seed.seed)"), $seed_refs)
            ->get();
        // Now tie to the series.
        foreach ($this->data as $i => $series) {
            $this->data[$i]['higher'] = $team_info->where('seed_ref', $series['higher_seed_ref']);
            $this->data[$i]['lower'] = $team_info->where('seed_ref', $series['lower_seed_ref']);
        }
    }

    /**
     * Load the matchups schedule instances
     * @return void
     */
    public function loadSecondarySchedule(): void
    {
        $query = Namespaces::callStatic('Schedule', 'query')
            ->select('*')
            ->selectRaw('SUBSTRING(LPAD(game_id, 3, "0"), 1, 2) AS round_code');
        foreach ($this->data as $i => $matchup) {
            $query->orWhere(function (Builder $where) use ($matchup) {
                $where->where([
                    ['season', '=', $matchup['season']],
                    ['game_type', '=', 'playoff'],
                    ['game_id', '>=', $matchup['round_code'] * 10],
                    ['game_id', '<', ($matchup['round_code'] + 1) * 10],
                ]);
            });
        }
        $schedule = $query->orWhere(DB::raw(1), DB::raw(0))
            ->orderBy('game_date')
            ->orderBy('game_time')
            ->get()
            ->loadBasicSecondaries();
        foreach ($this->data as $i => $matchup) {
            $this->data[$i]['schedule'] = $schedule->where('round_code', $matchup['round_code']);
        }
    }

    /**
     * The full URL for the series summary
     * @return string The full series summary link
     */
    public function getLinkAttribute(): string
    {
        return '/' . join('/', [
            FrameworkConfig::get('debear.section.endpoint'),
            'playoffs',
            $this->season,
            $this->linkRound(),
        ]);
    }

    /**
     * The round-specific series element of the link
     * @return string The final, round specific, part of the series summary link
     */
    public function linkRound(): string
    {
        $higher = preg_replace('/[^a-z]/', '', strtolower($this->higher->franchise));
        $lower = preg_replace('/[^a-z]/', '', strtolower($this->lower->franchise));
        $round = str_pad($this->round_code, 2, '0', STR_PAD_LEFT);
        return "$higher-$lower-$round";
    }
}
