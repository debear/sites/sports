<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;

class UserFavourite extends Instance
{
    /**
     * Return the appropriate icon to show the current status on the Add/Remove button
     * @return string The CSS for the current status icon
     */
    public function icon(): string
    {
        return 'icon_favourite_' . ($this->isset() ? 'is' : 'not');
    }

    /**
     * Return the appropriate icon to use when hovering over the Add/Remove button
     * @return string The CSS for the 'hover' icon
     */
    public function hover(): string
    {
        return 'icon_right_favourite_' . ($this->isset() ? 'remove' : 'add');
    }
}
