<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Format;

class Draft extends Instance
{
    /**
     * Convert the draft pick's height in to feet and inches
     * @return string The pick's formatted height
     */
    public function getHeightFtInAttribute(): string
    {
        $ft = floor($this->height / 12);
        $in = $this->height % 12;
        return "$ft'" . ($in ? " $in\"" : '');
    }

    /**
     * Summarise the draft record in to a display string
     * @return string The pick as a formatted string
     */
    public function getSummaryAttribute(): string
    {
        return Format::ordinal($this->round) . ' Round, ' . Format::ordinal($this->pick) . ' Overall'
            . " in {$this->season} by "
            . '<span class="' . $this->team->logoCSS() . '">' . $this->team->name_responsive . '</span>';
    }

    /**
     * Render a team_id attribute into display markup
     * @param string $team_id The team_id to render.
     * @return string The team's markup
     */
    protected function renderTeam(string $team_id): string
    {
        return '<span class="' . Team::buildLogoCSS($team_id) . '">' . $team_id . '</span>';
    }

    /**
     * Get the list of seasons in which we have draft info
     * @return array The list of seasons with available draft results
     */
    public static function getAllSeasons(): array
    {
        return static::query()
            ->select('season')
            ->distinct()
            ->get()
            ->unique('season');
    }
}
