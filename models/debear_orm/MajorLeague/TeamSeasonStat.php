<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Traits\Stats as StatsTrait;

class TeamSeasonStat extends Instance
{
    use StatsTrait;

    /**
     * Fallback formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        return $this->$column ?? '&ndash;';
    }

    /**
     * Where the query requires an additional constraint, return it
     * @return ?array An (optional) additional query restriction for getting stats out.
     */
    public function getQueryExtra(): ?array
    {
        return $this->queryExtra;
    }

    /**
     * Load the team info for the returned stats
     * @return void
     */
    protected function loadSecondaryTeams(): void
    {
        $teams = Namespaces::callStatic('Team', 'query')
            ->whereIn('team_id', $this->unique('team_id'))
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $leader) {
            $this->data[$i]['team'] = $teams->where('team_id', $leader['team_id']);
        }
    }

    /**
     * Load the stats data for one or more given teams
     * @param integer $season      The season being interrogated.
     * @param string  $season_type The type of season being viewed.
     * @param array   $ids         The team(s) we want stat info loaded for.
     * @return self The resulting ORM object containing the stat data
     */
    public static function loadByTeam(int $season, string $season_type, array $ids): self
    {
        $query = static::query();
        // Do we need to manipulate columns?
        $stat_columns = (new static())->getSortableStatColumns();
        if (isset($stat_columns)) {
            $query->select(['season', 'season_type', 'team_id']);
            foreach (str_replace('TBL.', '', $stat_columns) as $col => $val) {
                $query->selectRaw("$val AS `$col`");
            }
        }
        // Build the rest of our query.
        return $query->where('season', $season)
            ->where('season_type', $season_type)
            ->whereIn('team_id', $ids)
            ->get();
    }
}
