<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Strings;
use DeBear\Repositories\Time;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\ORM\Base\Helpers\QueryExecutor;

class Player extends Instance
{
    /**
     * Format the player's (full) name
     * @return string The player's name formatted for display
     */
    public function getNameAttribute(): string
    {
        return "{$this->first_name} {$this->surname}";
    }

    /**
     * Format the player's (short) name
     * @return string The player's name formatted for shortened display
     */
    public function getNameShortAttribute(): string
    {
        return preg_replace('/[^A-Z]/', '', $this->first_name) . '. ' . $this->surname;
    }

    /**
     * Format the player's (full) name
     * @return string The player's name formatted for display
     */
    public function getNameSortAttribute(): string
    {
        return Strings::codifyURL($this->surname) . ', ' . Strings::codifyURL($this->first_name);
    }

    /**
     * Format the player's best-known name in a codified version (e.g., for links or files)
     * @return string The player's name standardised and run through the codifier
     */
    public function getNameCodifiedAttribute(): string
    {
        return Strings::codifyURL(str_replace('.', '', $this->name));
    }

    /**
     * Get the URL to access this player
     * @return string URL for the player
     */
    public function getLinkAttribute(): string
    {
        return '/' . join('/', array_filter([
            FrameworkConfig::get('debear.section.endpoint'),
            ltrim(FrameworkConfig::get('debear.links.players.url'), '/'),
            join('-', [
                $this->name_codified,
                $this->player_id,
            ]),
        ]));
    }

    /**
     * Convert the player's height to a display version
     * @return string Formatted player height
     */
    public function getHeightFmtAttribute(): string
    {
        return (isset($this->height) ? sprintf('%d&#39; %d&quot;', floor($this->height / 12), $this->height % 12) : '');
    }

    /**
     * Convert the player's weight to a display version
     * @return string Formatted player weight
     */
    public function getWeightFmtAttribute(): string
    {
        return (isset($this->weight) ? "{$this->weight}lb" : '');
    }

    /**
     * Convert the player's date of birth to a display version
     * @return string Formatted player date of birth
     */
    public function getDOBFmtAttribute(): string
    {
        return (isset($this->dob) && is_object($this->dob) ? $this->dob->format('jS M Y') : '');
    }

    /**
     * Convert the player's place of birth to a display version
     * @return string Formatted player place of birth
     */
    public function getBirthplaceFmtAttribute(): string
    {
        if (!isset($this->birthplace)) {
            return '';
        }
        $ret_pre = $ret_end = '';
        if (isset($this->birthplace_country)) {
            // Pre-pend a birthplace country.
            $ret_pre .= '<span class="flag_right flag16_right_' . $this->birthplace_country . '">';
            $ret_end .= '</span>';
        }
        if (isset($this->birthplace_state)) {
            // Pre-pend a birthplace state.
            $ret_pre .= '<span class="flag_right flag16_right_us-' . strtolower($this->birthplace_state) . '">';
            $ret_end .= ', ' . $this->birthplace_state . '</span>';
        }
        // Return the birthplace with the pre-/post-pended info.
        return "$ret_pre{$this->birthplace}$ret_end";
    }

    /**
     * Get the latest season we should refer to this player as being included in
     * @return integer The appropriate season
     */
    public function getLatestSeason(): int|string
    {
        $season = (isset($this->seasons) && !empty($this->seasons)
            ? max($this->seasons)
            : ($this->lastRoster->season ?? FrameworkConfig::get('debear.setup.season.viewing')));
        return intval($season);
    }

    /**
     * Render the player's mugshot with an <img tag
     * @param string|array $size The requested image size (Default: large), as a string or array.
     * @return string Markup to render the player's mugshot
     */
    public function image(string|array $size = 'large'): string
    {
        return '<img class="mugshot-' . $size . '" loading="lazy" src="' . $this->imageURL($size) . '"'
            . ' alt="Profile photo of ' . $this->name . '">';
    }

    /**
     * Return the URL for the player's mugshot
     * @param string|array $size The requested image size (Default: large), as a string or array.
     * @return string URL for the player's mugshot
     */
    public function imageURL(string|array $size = 'large'): string
    {
        return static::buildImageURL($this->player_id ?? '', $size);
    }

    /**
     * Render a silhouetted player's mugshot with an <img tag
     * @param string|array $size The requested image size (Default: large), as a string or array.
     * @return string Markup to render the silhouetted mugshot
     */
    public static function silhouette(string|array $size = 'large'): string
    {
        $src = static::silhouetteURL($size);
        return '<img class="mugshot-' . $size . '" loading="lazy" src="' . $src . '"'
            . ' alt="Profile photo silhouette">';
    }

    /**
     * Return the URL for the silhoutted player
     * @param string|array $size The requested image size (Default: large), as a string or array.
     * @return string URL for the silhoutted player
     */
    public static function silhouetteURL(string|array $size = 'large'): string
    {
        return static::buildImageURL('no_photo', $size);
    }

    /**
     * Get the URL for the player's mugshot
     * @param string       $file The player ID or placeholder image to render.
     * @param string|array $opt  The requested image size (Default: large), as a string or array.
     * @return string URL for the player's mugshot
     */
    protected static function buildImageURL(string $file, string|array $opt): string
    {
        $img = static::buildImagePath($file, $opt);
        // Return through the obfuscator.
        $size = is_string($opt) ? $opt : $opt['size'];
        $size_opt = FrameworkConfig::get("debear.cdn.sizes.$size");
        return HTTP::buildCDNURLs("rs=1&i=$img&w={$size_opt['w']}&h={$size_opt['h']}");
    }

    /**
     * Ensure the passed image options meet our needs (consistently)
     * @param string|array $opt Customisation options.
     * @return array The parsed image options
     */
    protected static function imageOpt(string|array $opt): array
    {
        return Arrays::merge(
            // Defaults.
            [
                'size' => 'large',
                'site' => FrameworkConfig::get('debear.url.sub'),
                'sport' => FrameworkConfig::get('debear.section.endpoint'),
            ],
            // Passed in.
            is_string($opt) ? ['size' => $opt] : $opt
        );
    }

    /**
     * Determine the on-disk path of the file to use for this entity's mugshot/logo
     * @param string       $file The team ID, null if a placeholder image is to be rendered.
     * @param string|array $opt  Customisation options.
     * @return string The path-on-disk of the appropriate image for the mugshot/logo to use for this entity
     */
    public static function buildImagePath(?string $file, string|array $opt): string
    {
        $opt = static::imageOpt($opt);
        $default = FrameworkConfig::get("debear.sports.subsites.{$opt['sport']}.cdn.ext");
        $ext = '.' . (FrameworkConfig::get('debear.cdn.ext') ?? $default);
        $img = '/' . join('/', [
            $opt['site'],
            $opt['sport'],
            'players',
            "$file$ext",
        ]);
        // Do we need to use a fallback (always in test)?
        if (!file_exists(resource_path(FrameworkConfig::get('debear.dirs.images')) . $img) || HTTP::isTest()) {
            $img = "/{$opt['site']}/{$opt['sport']}/players/no_photo$ext";
        }
        return $img;
    }

    /**
     * Get the list of players whose birthday is today
     * @return self The resulting ORM object containing birthday players
     */
    public static function loadBirthdays(): self
    {
        return static::query()
            ->where('dob', 'LIKE', '%' . substr(Time::object()->getCurDate(), 4))
            ->orderByDesc('dob')
            ->get();
    }

    /**
     * Search for players by an arbitrary search term
     * @param string $term The haystack.
     * @return self The resulting ORM object of search results
     */
    public static function searchByName(string $term): self
    {
        return static::query()
            ->where(DB::raw('CONCAT(first_name, " ", surname)'), 'LIKE', "%{$term}%")
            ->orderBy('surname')
            ->orderBy('first_name')
            ->orderByDesc('dob')
            ->get();
    }

    /**
     * Load the additional secondary info for when viewing an individual player
     * @param string $tab The information to be loaded.
     * @return void
     */
    public function loadDetailedSecondary(string $tab): void
    {
        $config = FrameworkConfig::get('debear.setup.players.subnav');
        $tab_map = array_combine(array_keys($config), array_column($config, 'secondaries'));
        $this->tab_viewing = $tab;
        $this->loadSecondary($tab_map[$tab]);
    }

    /**
     * Determine the draft details of players in the dataset
     * @return void
     */
    protected function loadSecondaryDraft(): void
    {
        // In some instances, there may not be draft info available, so skip if so.
        if (!Namespaces::classExists('Draft')) {
            return;
        }
        // Then load.
        $draft = Namespaces::callStatic('Draft', 'query')
            ->whereIn('player_id', $this->unique('player_id'))
            ->orderByDesc('season')
            ->get();
        // Expand team info on these rows.
        $teams = Namespaces::callStatic('Team', 'query')
            ->whereIn('team_id', $draft->unique('team_id'))
            ->get();
        foreach ($draft as $row) {
            $row->team = $teams->where('team_id', $row->team_id ?? '');
        }
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['draft'] = $draft->where('player_id', $player['player_id']);
        }
    }

    /**
     * Determine the award details of players in the dataset
     * @return void
     */
    protected function loadSecondaryAwards(): void
    {
        $tbl_awards = Namespaces::callStatic('PlayerAward', 'getTable');
        $tbl_details = Namespaces::callStatic('Awards', 'getTable');
        $awards = Namespaces::callStatic('PlayerAward', 'query')
            ->join($tbl_details, "$tbl_details.award_id", '=', "$tbl_awards.award_id")
            ->whereIn("$tbl_awards.player_id", $this->unique('player_id'))
            ->orderByDesc("$tbl_awards.season")
            ->orderBy("$tbl_details.disp_order")
            ->get();
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['awards'] = $awards->where('player_id', $player['player_id']);
        }
    }

    /**
     * Determine the team each player in the dataset currently plays for
     * @return void
     */
    protected function loadSecondaryLastRoster(): void
    {
        // Determine the current roster date.
        $max_date = Namespaces::callStatic('TeamRoster', 'query')
            ->selectRaw('the_date')
            ->orderByDesc('season')
            ->orderByDesc('the_date')
            ->limit(1)
            ->get()
            ->the_date;
        // Then get the roster info for the player (on the expectation we are only loading a single player).
        $query = Namespaces::callStatic('TeamRoster', 'query')
            ->select('*')
            ->selectRaw('the_date = ? AS is_latest', [$max_date]);
        if (FrameworkConfig::get('debear.setup.season.viewing') != '0000') {
            $query->where('season', '<=', FrameworkConfig::get('debear.setup.season.viewing'));
        }
        $rosters = $query->where('player_id', $this->unique('player_id'))
            ->orderByDesc('season')
            ->orderByDesc('the_date')
            ->limit(1)
            ->get();
        // Expand team info on these rows.
        $teams = Namespaces::callStatic('Team', 'query')
            ->whereIn('team_id', $rosters->unique('team_id'))
            ->get();
        foreach ($rosters as $row) {
            $row->team = $teams->where('team_id', $row->team_id ?? '');
        }
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['lastRoster'] = $rosters->where('player_id', $player['player_id']);
        }
    }

    /**
     * Determine the main group applicable for each player in the dataset played
     * @return void
     */
    protected function loadSecondarySortGroups(): void
    {
        $groups = FrameworkConfig::get('debear.setup.players.groups');
        foreach ($this->data as $i => $player) {
            if (!isset($player['group'])) {
                $this->data[$i]['group'] = ($player['lastRoster']->pos == $groups['pos']
                    ? $groups['is']
                    : $groups['not']);
            }
        }
    }

    /**
     * Determine the season's in which the players in the dataset played
     * @return void
     */
    protected function loadSecondarySeasons(): void
    {
        // Get the seasons according to the player's position.
        $seasons = $career_gp = [];
        foreach ($this->unique('group') as $group) {
            $list = $this->where('group', $group)->unique('player_id');
            $class = FrameworkConfig::get("debear.setup.stats.details.player.$group.class.stat");
            $gp_stat = FrameworkConfig::get("debear.setup.stats.details.player.$group.gp_col") ?? 'gp';
            $all = $class::query()
                ->distinct()
                ->select('player_id')
                ->selectRaw('CONCAT(season, ":", season_type) AS season')
                ->selectRaw("$gp_stat AS gp_col")
                ->whereIn('player_id', $list)
                ->orderBy('player_id')
                ->orderByDesc('season')
                ->get();
            // Now prepare for tying together later.
            foreach ($list as $id) {
                $by_player = $all->where('player_id', $id);
                $seasons[$id] = $by_player->unique('season');
                $career_gp[$id] = $by_player->sum('gp_col');
            }
        }
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['seasons'] = $seasons[$player['player_id']];
            $this->data[$i]['careerGP'] = $career_gp[$player['player_id']];
        }
    }

    /**
     * Determine the appropriate season stats for each player in the dataset according to their position
     * @return void
     */
    protected function loadSecondarySummaryStats(): void
    {
        $season = $this->getLatestSeason();
        // Get the stats according to the player's position.
        $group = $this->unique('group');
        $stats = array_fill_keys($group, []);
        foreach (FrameworkConfig::get('debear.setup.stats.summary') as $key => $config) {
            if ($config['type'] == 'team' || !in_array($config['class'], $group)) {
                // We only want to focus on player stats, for groups we have players for.
                continue;
            }
            $class = FrameworkConfig::get("debear.setup.stats.details.{$config['type']}.{$config['class']}.class.sort");
            $stats[$config['class']][$key] = $class::load($season, 'regular', $config['column'], [
                'player_id' => $this->where('group', $config['class'])->unique('player_id'),
            ]);
        }
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $summary = [];
            foreach ($stats[$player['group']] as $key => $data) {
                $row = $data->where('player_id', $player['player_id']);
                if ($data->count()) {
                    $config = FrameworkConfig::get("debear.setup.stats.summary.$key");
                    $summary[$key] = [
                        'label' => $config['name_short'] ?? $config['name'],
                        'value' => $row->format($config['column']),
                        'rank' => isset($row->stat_pos) ? Format::ordinal($row->stat_pos) : 'Unranked',
                    ];
                }
            }
            if (count($summary)) {
                $this->data[$i]['statSummary'] = $summary;
            }
        }
    }

    /**
     * Determine the season's stats of players in the dataset
     * @return void
     */
    protected function loadSecondarySeasonStats(): void
    {
        $season = $this->getLatestSeason();
        $this->loadSecondaryCareerStats([$season]);
    }

    /**
     * Determine the season's stats of players in the dataset
     * @param array $seasons An optional list of seasons to narrow our search down by.
     * @return void
     */
    protected function loadSecondaryCareerStats(array $seasons = []): void
    {
        // Get the stats according to the player's position.
        $player_stats = [];
        foreach ($this->unique('group') as $group) {
            $list = $this->where('group', $group)->unique('player_id');
            // Determine base query logic for this class.
            $class = FrameworkConfig::get("debear.setup.stats.details.player.$group.class.stat");
            $tbl_stat = $class::getTable();
            $calc_columns = (new $class())->getSeasonStatColumns();
            $query = $class::query()->select([
                "$tbl_stat.season",
                "$tbl_stat.season_type",
                "$tbl_stat.player_id",
            ]);
            foreach (str_replace('TBL.', "$tbl_stat.", $calc_columns) as $col => $val) {
                $query->selectRaw("$val AS `$col`");
            }
            $query->selectRaw("1 AS `can_$group`"); // This stat group is always available in dropdowns.
            // Logic to determine if a team row represents the aggregate season total or not.
            $query->selectRaw("IF(SUBSTRING($tbl_stat.team_id, 1, 1) = '_', 'Total', $tbl_stat.team_id) AS team_id");
            // Now determine suitability of the other stat groups available.
            foreach (FrameworkConfig::get('debear.setup.stats.details.player') as $alt_group => $alt_config) {
                if ($alt_group == $group) {
                    // We have already covered the current group above.
                    continue;
                }
                $tbl_alt = $alt_config['class']['stat']::getTable();
                $query->selectRaw("$tbl_alt.season IS NOT NULL AS `can_$alt_group`")
                    ->leftJoin($tbl_alt, function ($join) use ($tbl_alt, $tbl_stat) {
                        $join->on("$tbl_alt.season", '=', "$tbl_stat.season")
                        ->on("$tbl_alt.season_type", '=', "$tbl_stat.season_type")
                        ->on("$tbl_alt.player_id", '=', "$tbl_stat.player_id")
                        ->on("$tbl_alt.team_id", '=', "$tbl_stat.team_id");
                    });
            }
            // Process the rest of our query.
            $query->selectRaw('? AS stat_group', [$group]);
            if (count($seasons)) {
                $query->whereIn("$tbl_stat.season", $seasons);
            }
            $query->whereIn("$tbl_stat.player_id", $list)
                ->orderByDesc("$tbl_stat.season")
                ->orderByRaw("$tbl_stat.`season_type` = 'regular' DESC") // Regular season first.
                ->orderBy("$tbl_stat.team_id")
                ->groupBy([
                    "$tbl_stat.season",
                    "$tbl_stat.season_type",
                    "$tbl_stat.player_id",
                    "$tbl_stat.team_id",
                ]);
            // Finally run our query.
            $stats = $query->get();
            // Now prepare for tying together later.
            foreach ($list as $id) {
                $player_stats[$id] = $stats->where('player_id', $id);
            }
        }
        // Now tie-together.
        $key = count($seasons) ? 'seasonStats' : 'careerStats';
        foreach ($this->data as $i => $player) {
            $this->data[$i][$key] = $player_stats[$player['player_id']];
        }
    }

    /**
     * Determine the recent games for the players in the dataset
     * @return void
     */
    protected function loadSecondaryRecentGames(): void
    {
        $this->loadSecondarySeasonGames(FrameworkConfig::get('debear.setup.players.recent_games'));
    }

    /**
     * Determine the games played over the given season for the players in the dataset
     * @param integer $max_games The most games to return, or 0 to return every game played.
     * @return void
     */
    protected function loadSecondarySeasonGames(int $max_games = 0): void
    {
        // On the expectation we're only processing one player, process per-player.
        foreach ($this->data as $i => $player) {
            // From the player group, determine the class to use.
            $class = FrameworkConfig::get("debear.setup.stats.details.player.{$player['group']}.class.game");
            $calc_columns = (new $class())->getGameStatColumns();
            // Build the status query.
            $tbl_stat = $class::getTable();
            $tbl_schedule = Namespaces::callStatic('Schedule', 'getTable');
            $query = $class::query($tbl_schedule)->select([
                "$tbl_stat.season",
                "$tbl_stat.game_type",
                "$tbl_stat.game_id",
                "$tbl_stat.player_id",
                "$tbl_stat.team_id",
            ]);
            foreach (str_replace('TBL.', "$tbl_stat.", $calc_columns) as $col => $val) {
                $query->selectRaw("$val AS `$col`");
            }
            $query->selectRaw('? AS stat_group', [$player['group']])
                ->selectRaw("1 AS `can_{$player['group']}`") // This stat group is always available in dropdowns.
                ->join($tbl_stat, function ($join) use ($tbl_stat, $tbl_schedule, $player) {
                    $join->on("$tbl_stat.season", '=', "$tbl_schedule.season")
                    ->on("$tbl_stat.game_type", '=', "$tbl_schedule.game_type")
                    ->on("$tbl_stat.game_id", '=', "$tbl_schedule.game_id")
                    ->where("$tbl_stat.player_id", '=', $player['player_id']);
                });
            // Now determine suitability of the other stat groups available.
            foreach (FrameworkConfig::get('debear.setup.stats.details.player') as $alt_group => $alt_config) {
                if ($alt_group == $player['group']) {
                    // We have already covered the current group above.
                    continue;
                }
                $tbl_alt = $alt_config['class']['game']::getTable();
                $query->selectRaw("$tbl_alt.season IS NOT NULL AS `can_$alt_group`")
                    ->leftJoin($tbl_alt, function ($join) use ($tbl_alt, $tbl_stat) {
                        $join->on("$tbl_alt.season", '=', "$tbl_stat.season")
                        ->on("$tbl_alt.game_type", '=', "$tbl_stat.game_type")
                        ->on("$tbl_alt.game_id", '=', "$tbl_stat.game_id")
                        ->on("$tbl_alt.team_id", '=', "$tbl_stat.team_id")
                        ->on("$tbl_alt.player_id", '=', "$tbl_stat.player_id");
                    });
            }
            // If we're not limiting to recent games, then apply for the current season only.
            if (!$max_games) {
                $season = $this->getLatestSeason();
                $query->where([
                    ["$tbl_schedule.season", '=', $season],
                    ["$tbl_schedule.game_type", '=', FrameworkConfig::get('debear.setup.season.type')],
                ]);
            }
            $query->orderByDesc("$tbl_schedule.game_date")
                ->orderByDesc("$tbl_schedule.game_time")
                ->limit($max_games ?: null) // Return all games if no max value passed.
                ->groupBy([
                    "$tbl_stat.season",
                    "$tbl_stat.game_type",
                    "$tbl_stat.game_id",
                    "$tbl_stat.player_id",
                ]);
            // Finally run our query.
            $key = ($max_games ? 'recentGames' : 'seasonGames');
            $this->data[$i][$key] = $query->get()->loadSecondary(['games']);
        }
    }

    /**
     * Get the list of players available in the database
     * @return self An ORM model containing all the (usable) players
     */
    public static function getAllSitemap(): self
    {
        // Simplify the roster table to identify current players.
        $tbl_roster = Namespaces::callStatic('TeamRoster', 'getTable');
        $season = FrameworkConfig::get('debear.setup.season.max');
        QueryExecutor::statement(static::getConnectionName(), "CREATE TEMPORARY TABLE tmp_roster (
  player_id SMALLINT UNSIGNED PRIMARY KEY
) ENGINE = MEMORY
  SELECT DISTINCT player_id
  FROM $tbl_roster
  WHERE season = $season;");
        // Then run the player getter.
        $tbl_player = static::getTable();
        return static::query()
            ->select("$tbl_player.*")
            ->selectRaw('tmp_roster.player_id IS NOT NULL AS is_curr')
            ->leftJoin('tmp_roster', function ($join) use ($tbl_player) {
                $join->on('tmp_roster.player_id', '=', "$tbl_player.player_id");
            })->groupBy("$tbl_player.player_id")
            ->orderBy("$tbl_player.surname")
            ->orderBy("$tbl_player.first_name")
            ->orderBy("$tbl_player.player_id")
            ->get();
    }
}
