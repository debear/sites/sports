<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;

class PlayerInjury extends Instance
{
    /**
     * Load the player details for the relevant players found
     * @return void
     */
    public function loadSecondaryPlayers(): void
    {
        $players = Namespaces::callStatic('Player', 'query')->whereIn('player_id', $this->unique('player_id'))->get();
        foreach ($this->data as $i => $lineup) {
            $this->data[$i]['player'] = $players->where('player_id', $lineup['player_id']);
        }
    }
}
