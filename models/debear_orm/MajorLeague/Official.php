<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;

class Official extends Instance
{
    /**
     * Format the official's (full) name
     * @return string The official's name formatted for display
     */
    public function getNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->surname;
    }
}
