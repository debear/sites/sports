<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\Time;
use DeBear\Helpers\Sports\MajorLeague\Traits\ScheduleDate as ScheduleDateTrait;

class ScheduleDate extends Instance
{
    use ScheduleDateTrait;

    /**
     * Return the date in a longer form
     * @return string The "long" version of the date
     */
    public function getNameFullAttribute(): string
    {
        return $this->date->format('jS F Y');
    }

    /**
     * Return the date in a short form
     * @return string The "short" version of the date
     */
    public function getNameShortAttribute(): string
    {
        return $this->date->format('jS M');
    }

    /**
     * Validate a supplied game date argument
     * @param string $date_arg The supplied URI argument to validate.
     * @return self The resulting ORM object for the passed argument
     */
    public static function loadFromArgument(string $date_arg): self
    {
        $date = preg_replace('/^(20\d{2})(\d{2})(\d{2})$/', '$1-$2-$3', $date_arg);
        // Pass One: Direct match.
        $direct = static::query()->where('date', $date)->get();
        if ($direct->isset()) {
            return $direct;
        }
        // Pass Two: Closest to the argument.
        return static::query()
            ->orderByRaw('ABS(DATEDIFF(`date`, ?))', [$date])
            ->limit(1)
            ->get();
    }

    /**
     * Determine the current/latest game date
     * @param integer $season An optional season restriction.
     * @return self The game date ORM object we consider 'current'
     */
    public static function getLatest(?int $season = null): self
    {
        $now = (new Carbon(Time::object()->getDatabaseNowFmt(), FrameworkConfig::get('debear.datetime.timezone_db')))
            ->subHours(12)
            ->toDateString();
        // Scenario A: Direct match.
        $direct = static::query()
            ->where([
                ['season', '=', $season ?? DB::raw('`season`')],
                ['date', '<=', $now],
                [DB::raw('IFNULL(`date_next`, `date`)'), '>', $now],
            ])->get();
        if ($direct->isset()) {
            return $direct;
        }
        // Scenario B: Previewing new season.
        $preview = static::query()
            ->where([
                ['season', '=', $season ?? FrameworkConfig::get('debear.setup.season.viewing')],
                ['date', '>=', $now],
            ])->orderBy('date')
            ->limit(1)
            ->get();
        if ($preview->isset()) {
            return $preview;
        }
        // Scenario C (Fallback): Last known date.
        $query = static::query();
        if (isset($season)) {
            $query->where('season', $season);
        }
        return $query->where('date', '<=', $now)
            ->orderByDesc('date')
            ->limit(1)
            ->get();
    }

    /**
     * Determine some characteristics of the scheduled season.
     * @param integer $season The season to process.
     * @return self The game date info as an ORM object
     */
    public static function getDateRange(int $season): self
    {
        return static::query()
            ->selectRaw('MIN(`date`) AS date_min')
            ->selectRaw('MAX(`date`) AS date_max')
            ->where('season', $season)
            ->get();
    }

    /**
     * Get the list of dates that do not have any games played on them
     * @param integer $season The season to be calculated.
     * @return array The dates to exclude from the calendar widget, in its expected format
     */
    public static function getExceptions(int $season): array
    {
        $ret = [];
        $raw = static::query()
            ->selectRaw('DATE_ADD(`date`, INTERVAL 1 DAY) AS `date_from`')
            ->selectRaw('DATE_SUB(`date_next`, INTERVAL 1 DAY) AS `date_to`')
            ->where('season', $season)
            ->whereNotNull('date_next')
            ->where(DB::raw('DATE_ADD(`date`, INTERVAL 1 DAY)'), '!=', DB::raw('`date_next`'))
            ->orderBy('date')
            ->get();
        foreach ($raw as $period) {
            $p = new Carbon($period->date_from, FrameworkConfig::get('debear.datetime.timezone_db'));
            while ($p->toDateString() <= $period->date_to) {
                // Add to our list.
                $ret[$p->toDateString()] = true;
                // Move on to the next date in the period.
                $p->addDays(1);
            }
        }
        return ['exceptions' => $ret];
    }
}
