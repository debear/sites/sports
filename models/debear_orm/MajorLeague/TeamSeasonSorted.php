<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Traits\SortedStats as SortedStatsTrait;

class TeamSeasonSorted extends Instance
{
    use SortedStatsTrait;

    /**
     * Where the query requires an additional constraint, return it
     * @return ?array An (optional) additional query restriction for getting stats out.
     */
    public static function getQueryExtra(): ?array
    {
        return (new static())->getStatsObject()->getQueryExtra();
    }

    /**
     * Load the data for our stats table
     * @param integer $season      The season being interrogated.
     * @param string  $season_type The type of season being viewed.
     * @param string  $column      The column from the object we want the leaders for.
     * @param array   $opt         Optional additional query configuration.
     * @return self The resulting ORM object containing the stat data
     */
    public static function load(int $season, string $season_type, string $column, array $opt = []) /* : self */
    {
        $tbl_sort = static::getTable();
        $tbl_stat = static::getStatTable();
        // Build our WHERE clause.
        $join_extra = static::getQueryExtra();
        $where = [
            ["$tbl_sort.season", '=', $season],
            ["$tbl_sort.season_type", '=', $season_type],
        ];
        if (isset($join_extra)) {
            $where_extra = $join_extra;
            $where_extra[0] = "$tbl_sort.{$where_extra[0]}";
            $where[] = $where_extra;
        }
        // Now run.
        $query = (new static())->getStatsClass()::query($tbl_sort);
        // Which stat columns are we loading?
        $calc_columns = (new static())->getSortableStatColumns();
        if (isset($calc_columns)) {
            $query->select([
                "$tbl_stat.season",
                "$tbl_stat.season_type",
                "$tbl_stat.team_id",
            ]);
            foreach (str_replace('TBL.', "$tbl_stat.", $calc_columns) as $col => $val) {
                $query->selectRaw("$val AS $col");
            }
        } else {
            $query->select("$tbl_stat.*");
        }
        // Main query, including the join to the stats themselves.
        $ret = $query->selectRaw("$tbl_sort.`$column` AS stat_pos")
            ->join($tbl_stat, function ($join) use ($tbl_sort, $tbl_stat, $join_extra) {
                $join->on("$tbl_stat.season", '=', "$tbl_sort.season")
                    ->on("$tbl_stat.season_type", '=', "$tbl_sort.season_type")
                    ->on("$tbl_stat.team_id", '=', "$tbl_sort.team_id");
                if (isset($join_extra)) {
                    $join->on("$tbl_stat.{$join_extra[0]}", '=', "$tbl_sort.{$join_extra[0]}");
                }
            })->where($where)
            ->orderBy("$tbl_sort.$column")
            ->orderBy("$tbl_sort.team_id")
            ->get();
        if ($ret->count()) {
            $ret->loadSecondary(['teams']);
        }
        return $ret;
    }

    /**
     * Get the leaders for a given statistic
     * @param string  $column  The column from the object we want the leaders for.
     * @param integer $max_num The number of teams to list.
     * @return self The resulting ORM object containing the leaders
     */
    public static function loadLeaders(string $column, int $max_num): self
    {
        // Build our WHERE clause.
        $where = array_filter([
            ['season', '=', FrameworkConfig::get('debear.setup.season.viewing')],
            ['season_type', '=', 'regular'],
            static::getQueryExtra() ?? [],
        ]);
        $where[] = [$column, '<=', $max_num];
        // Now run.
        $ret = static::query()
            ->select(['season', 'season_type', 'team_id'])
            ->selectRaw("`$column` AS pos")
            ->selectRaw("'$column' AS stat")
            ->where($where)
            ->orderBy($column)
            ->get();
        if ($ret->count()) {
            $ret->loadSecondary(['singleStat', 'teams']);
        }
        return $ret;
    }

    /**
     * Load the full stat info for the returned sorted rows
     * @return void
     * /
    protected function loadSecondaryStats(): void
    {
        $random = $this->random();
        $where = array_filter([
            ['season', '=', $random->season],
            ['season_type', '=', $random->season_type],
            static::getQueryExtra() ?? [],
        ]);
        // And now load the stats.
        $stats = $this->getStatsClass()::query()
            ->where($where)
            ->whereIn('team_id', $this->unique('team_id'))
            ->groupBy('season')
            ->groupBy('season_type')
            ->groupBy('team_id')
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $row) {
            $this->data[$i]['stats'] = $stats->where('team_id', $row['team_id']);
        }
    }
    /* */

    /**
     * Load the stat info for the returned stats
     * @return void
     */
    protected function loadSecondarySingleStat(): void
    {
        $random = $this->random();
        $where = array_filter([
            ['season', '=', $random->season],
            ['season_type', '=', $random->season_type],
            static::getQueryExtra() ?? [],
        ]);
        $stat_columns = $this->getSeasonStatColumns();
        $stat_col = (isset($stat_columns)
            ? str_replace('TBL.', '', $stat_columns[$random->stat])
            : $random->stat);
        // And now load the stats.
        $stats = $this->getStatsClass()::query()
            ->select('team_id')
            ->selectRaw("$stat_col AS `{$random->stat}`")
            ->where($where)
            ->whereIn('team_id', $this->unique('team_id'))
            ->groupBy('season')
            ->groupBy('season_type')
            ->groupBy('team_id')
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $leader) {
            $this->data[$i]['stat_value'] = $stats->where('team_id', $leader['team_id'])->format($random->stat);
        }
    }

    /**
     * Load the team info for the returned stats
     * @return void
     */
    protected function loadSecondaryTeams(): void
    {
        $teams = Namespaces::callStatic('Team', 'query')
            ->whereIn('team_id', $this->unique('team_id'))
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $leader) {
            $this->data[$i]['team'] = $teams->where('team_id', $leader['team_id']);
        }
    }
}
