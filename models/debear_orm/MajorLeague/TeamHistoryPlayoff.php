<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\Namespaces;

class TeamHistoryPlayoff extends Instance
{
    /**
     * Convert the matchup in to a final result
     * @return string The overall playoff result for this team
     */
    public function getResultAttribute(): string
    {
        if ($this->for == $this->against) {
            // Likely series has not started yet.
            return ($this->for ? 'Tied' : '');
        }

        if ($this->season == FrameworkConfig::get('debear.setup.season.viewing')) {
            // In the current season, has the series been completed? If so, we need different terms.
            $config = Namespaces::callStatic('PlayoffSeries', 'parseConfig', [$this->season]);
            $wins_required = $config['series'][$this->round]['wins'];
            // Written this way for code coverage.
            $is_current = ($this->for < $wins_required) && ($this->agaisnt < $wins_required);
            $term_w = ($is_current ? 'Leading' : null);
            $term_l = ($is_current ? 'Behind' : null);
        }
        return $this->for > $this->against ? ($term_w ?? 'Won') : ($term_l ?? 'Lost');
    }

    /**
     * Return the CSS used to highlight the result of the playoff matchup / series for this team
     * @return string The applicable CSS attribute
     */
    public function textCSS(): string
    {
        if ($this->for == $this->against) {
            // Likely series has not started yet.
            return 'result-tie';
        }
        return $this->for > $this->against ? 'result-win' : 'result-loss';
    }
}
