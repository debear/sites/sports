<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\GameLineup as BaseGameLineup;

class GameIndividual extends BaseGameLineup
{
    /**
     * Return a label indicating our confidence in the catcher starting
     * @return string Certainty label
     */
    public function getCatcherRatingAttribute(): string
    {
        return ($this->catcher_reason == 'depth-chart' ? 'Possible' : 'Probable');
    }
}
