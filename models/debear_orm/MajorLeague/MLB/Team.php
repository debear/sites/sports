<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\Team as BaseTeam;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class Team extends BaseTeam
{
    /**
     * Get the team's home venue
     * @return string The name of the team's current home venue
     */
    public function getVenueAttribute(): string
    {
        return $this->currentVenue->stadium;
    }

    /**
     * Load the additional secondary info for when viewing an individual team
     * @param string $tab The information to be loaded.
     * @return void
     */
    public function loadDetailedSecondary(string $tab): void
    {
        // Loading MLB-specific information.
        if ($tab == 'depth-chart') {
            $this->loadSecondary(['depthcharts']);
            return;
        }
        // Falling back to the standard loader.
        parent::loadDetailedSecondary($tab);
    }

    /**
     * Get the individual team depth charts for the given teams
     * @return void
     */
    protected function loadSecondaryDepthCharts(): void
    {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $depth = TeamDepthChart::query()
            ->where('season', $season)
            ->whereIn('team_id', $this->unique('team_id'))
            ->orderBy('team_id')
            ->orderBy('pos')
            ->orderBy('depth')
            ->get()
            ->loadSecondary(['players']);
        // Now tie together.
        foreach ($this->data as $i => $team) {
            $this->data[$i]['depth_chart'] = $depth->where('team_id', $team['team_id']);
        }
    }
}
