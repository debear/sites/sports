<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerHeatmap as BasePlayerHeatmap;
use DeBear\Helpers\Sports\MLB\Traits\Heatzones as HeatzonesTrait;

class PlayerSeasonPitchingZones extends BasePlayerHeatmap
{
    use HeatzonesTrait;
}
