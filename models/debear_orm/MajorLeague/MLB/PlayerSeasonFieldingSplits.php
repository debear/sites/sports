<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonSplits as BasePlayerSeasonSplits;
use DeBear\Helpers\Sports\MLB\Traits\StatsSplit as StatsSplitTrait;
use DeBear\Helpers\Sports\MLB\Traits\StatsFielding as StatsFieldingTrait;

class PlayerSeasonFieldingSplits extends BasePlayerSeasonSplits
{
    use StatsSplitTrait;
    use StatsFieldingTrait;
}
