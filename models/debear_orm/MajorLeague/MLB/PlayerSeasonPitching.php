<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsPitching as StatsPitchingTrait;

class PlayerSeasonPitching extends BasePlayerSeasonStat
{
    use StatsPitchingTrait;
}
