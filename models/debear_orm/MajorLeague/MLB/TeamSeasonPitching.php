<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamSeasonPitching extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'ip' => ['s' => 'IP', 'f' => 'Innings Pitched'],
        'out' => ['s' => 'Out', 'f' => 'Outs Recorded'],
        'bf' => ['s' => 'BF', 'f' => 'Batters Faced'],
        'pt' => ['s' => 'PT', 'f' => 'Pitches Thrown'],
        'b' => ['s' => 'B', 'f' => 'Balls'],
        's' => ['s' => 'S', 'f' => 'Strikes'],
        'k' => ['s' => 'K', 'f' => 'Strikeouts'],
        'h' => ['s' => 'H', 'f' => 'Hits Allowed'],
        'hr' => ['s' => 'HR', 'f' => 'Home Runs Allowed'],
        'bb' => ['s' => 'BB', 'f' => 'Base on Balls'],
        'ibb' => ['s' => 'IBB', 'f' => 'Intentional Walks'],
        'whip' => ['s' => 'WHIP', 'f' => 'Walks + Hits per IP'],
        'r' => ['s' => 'R', 'f' => 'Runs Allowed'],
        'ra' => ['s' => 'RA', 'f' => 'Run Average'],
        'er' => ['s' => 'ER', 'f' => 'Earned Runs Allowed'],
        'era' => ['s' => 'ERA', 'f' => 'Earned Run Average'],
        'ur' => ['s' => 'UR', 'f' => 'Unearned Runs Allowed'],
        'ura' => ['s' => 'URA', 'f' => 'Unearned Run Average'],
        'ir' => ['s' => 'IR', 'f' => 'Inherited Runners'],
        'ira' => ['s' => 'IRA', 'f' => 'Inherited Runs Allowed'],
        'wp' => ['s' => 'WP', 'f' => 'Wild Pitches'],
        'bk' => ['s' => 'BK', 'f' => 'Balks'],
        'sb' => ['s' => 'SB', 'f' => 'Stolen Bases'],
        'cs' => ['s' => 'CS', 'f' => 'Caught Stealing'],
        'po' => ['s' => 'PO', 'f' => 'Pick Offs'],
        'go' => ['s' => 'GO', 'f' => 'Groundouts'],
        'fo' => ['s' => 'FO', 'f' => 'Flyouts'],
        'go_fo' => ['s' => 'GO/FO', 'f' => 'Groundout/Flyout Ratio'],
        'cg' => ['s' => 'CG', 'f' => 'Complete Games'],
        'sho' => ['s' => 'SHO', 'f' => 'Shutouts'],
        'qs' => ['s' => 'QS', 'f' => 'Quality Starts'],
        'w' => ['s' => 'W', 'f' => 'Wins'],
        'l' => ['s' => 'L', 'f' => 'Losses'],
        'sv' => ['s' => 'SV', 'f' => 'Saves'],
        'hld' => ['s' => 'HLD', 'f' => 'Holds'],
        'bs' => ['s' => 'BS', 'f' => 'Blown Saves'],
        'svo' => ['s' => 'SVO', 'f' => 'Save Opportunities'],
        'game_score' => ['s' => 'Score', 'f' => 'Avg Game Score'],
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (in_array($column, ['era', 'ra', 'ura'])) {
            return sprintf('%0.02f', $this->$column);
        } elseif ($column == 'go_fo') {
            return sprintf('%0.02f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
