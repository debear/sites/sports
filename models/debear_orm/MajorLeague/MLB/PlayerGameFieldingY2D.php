<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsFielding as StatsFieldingTrait;

class PlayerGameFieldingY2D extends BasePlayerGameStat
{
    use StatsFieldingTrait;
}
