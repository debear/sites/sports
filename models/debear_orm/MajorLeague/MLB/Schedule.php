<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\Schedule as BaseSchedule;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\Helpers\Highcharts;

class Schedule extends BaseSchedule
{
    /**
     * State if this game is special, why so
     * @return string A summary title
     */
    public function getTitleAttribute(): string
    {
        $title = $this->series_title;
        if ($title && substr($title, -4) != 'Game') {
            // Add on the Game number.
            $game_num = substr(str_pad($this->game_id, 3, '0', STR_PAD_LEFT), -1);
            $title .= ", Game $game_num";
        }
        return $title ?? '';
    }

    /**
     * State if this game is special, which series makes it so
     * @return string A summary title containing just the game series
     */
    public function getSeriesTitleAttribute(): string
    {
        if ($this->game_type == 'regular') {
            // Regular Season not special enough.
            return '';
        }
        $round = substr($this->game_id, 0, 1);

        // Which round?
        $title = $this->home->conference->name_full . ' ';
        if ($round == 4) {
            // Championship.
            $title = FrameworkConfig::get('debear.setup.playoffs.trophy');
        } elseif ($round == 3) {
            // LCS.
            $title .= 'Championship Series';
        } elseif ($round == 2) {
            // LDS.
            $title .= 'Division Series';
        } else {
            // Wildcard Series or Game?
            $wins_required = -1;
            foreach (FrameworkConfig::get('debear.setup.playoffs.series.1') as $period) {
                if (($period['from'] <= $this->season) && ($this->season <= $period['to'])) {
                    $wins_required = $period['wins'];
                    break;
                }
            }
            if ($wins_required > 1) {
                // Wildcard Series.
                $title .= 'Wild Card Series';
            } else {
                // Wildcard Game.
                return "{$title}Wild Card Game";
            }
        }

        // Return what we have built.
        return $title;
    }

    /**
     * Build a full version of the game's status for display
     * @return string The full display version of the game's status
     */
    public function getStatusFullAttribute(): string
    {
        // Run the standard process.
        $ret = parent::getStatusFullAttribute();
        // However, "Final" needs treating slightly differently.
        if ($ret == 'Final' && $this->innings != 9) {
            $ret = "Final/{$this->innings}";
        }
        return $ret;
    }

    /**
     * Build a version of the game's status for display
     * @return string The display version of the game's status
     */
    public function getStatusDispAttribute(): string
    {
        // Run the standard process.
        $ret = parent::getStatusDispAttribute();
        // However, "Final" needs treating slightly differently.
        if ($ret == '' && $this->innings != 9) {
            $ret = "F/{$this->innings}";
        }
        return $ret;
    }

    /**
     * Load the secondary game information with some, but not all, info for display
     * @return BaseSchedule The resulting ORM object with some additional information loaded
     */
    public function loadBasicSecondaries(): BaseSchedule
    {
        return $this->loadSecondary(['altvenues', 'pitchers', 'probables', 'homeruns']);
    }

    /**
     * Load the secondary game information for display
     * @return BaseSchedule The resulting ORM object with additional information loaded
     */
    public function loadSummarySecondaries(): BaseSchedule
    {
        return $this->loadSecondary([
            'altvenues', 'weather', 'odds', 'periods', 'standings', 'series', 'pitchers', 'probables', 'homeruns',
        ]);
    }

    /**
     * Load the secondary game information for displaying a single game
     * @return BaseSchedule The resulting ORM object with detailed additional information loaded
     */
    public function loadDetailedSecondaries(): BaseSchedule
    {
        return $this->loadSecondary([
            'altvenues', 'periods', 'rosters', 'plays', 'playerstats', 'officials', 'matchups'
        ]);
    }

    /**
     * Load the players and rosters for the teams involved
     * @return void
     */
    public function loadSecondaryRosters(): void
    {
        // The lineups.
        $query_games = $this->queryGames();
        $lineups = GameRoster::query()
            ->select('*')
            ->selectRaw("CONCAT(season, '-', game_type, '-', game_id, '-', team_id) AS team_ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        $pitchers = GamePitcher::query()
            ->select('*')
            ->selectRaw("CONCAT(season, '-', game_type, '-', game_id, '-', team_id) AS team_ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy('from_outs')->get();
        // Then the players, preserving for future secondary calcs.
        $this->secondary['players'] = Player::query()
            ->whereIn('player_id', $lineups->unique('player_id'))
            ->get();
        foreach ($lineups as $line) {
            $line->player = $this->secondary['players']->where('player_id', $line->player_id);
        }
        // Now tie together.
        foreach ($this->data as $i => $game) {
            foreach (['home', 'visitor'] as $team) {
                $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game[$team]->team_id}";
                $this->data[$i][$team]->lineup = $lineups->where('team_ref', $ref);
                $this->data[$i][$team]->pitchers = $pitchers->where('team_ref', $ref);
            }
        }
    }

    /**
     * Load the individual plays
     * @return void
     */
    public function loadSecondaryPlays(): void
    {
        // First get the scoring plays.
        $query_games = $this->queryGames(GameEvent::getTable());
        $plays = GameEvent::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS ref')
            ->selectRaw('CONCAT(inning, "-", half_inning) AS inning_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('game_id')
            ->orderBy('play_id')
            ->get();
        // Then the winprob data.
        $query_games = $this->queryGames(GameWinProb::getTable());
        $winprob = GameWinProb::query()
            ->select(['season', 'game_type', 'game_id'])
            ->selectRaw('UNCOMPRESS(winprob) AS winprob')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Now tie together.
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['plays'] = $plays->where('ref', $ref);
            $this->data[$i]['winprob'] = json_decode($winprob->where('ref', $ref)->winprob ?? '[]');
        }
    }

    /**
     * Load the game stats for the players involved
     * @return void
     */
    public function loadSecondaryPlayerStats(): void
    {
        $batting = $this->getSecondaryPlayerStatsBatting();
        $pitching = $this->getSecondaryPlayerStatsPitching();
        $fielding = $this->getSecondaryPlayerStatsFielding();
        // Link the players (found previously) to the stat rows.
        foreach ([$batting, $pitching, $fielding] as $list) {
            foreach ($list as $line) {
                $line->player = $this->secondary['players']->where('player_id', $line->player_id);
                $line->name_sort = $line->player->name_sort;
            }
        }
        // But also get the heatzones for the batters.
        $tbl_heatmap = PlayerSeasonBattingZones::getTable();
        $tbl_batsplit = PlayerSeasonBattingSplits::getTable();
        $query_games = $this->queryGames($tbl_heatmap);
        $batter_zones = PlayerSeasonBattingZones::query()
            ->leftJoin("$tbl_batsplit AS SPLITS_vLHP", function ($join) use ($tbl_heatmap) {
                $join->on('SPLITS_vLHP.season', '=', "$tbl_heatmap.season")
                    ->on('SPLITS_vLHP.season_type', '=', "$tbl_heatmap.season_type")
                    ->on('SPLITS_vLHP.player_id', '=', "$tbl_heatmap.player_id")
                    ->where('SPLITS_vLHP.split_type', '=', 'pitcher-info')
                    ->where('SPLITS_vLHP.split_label', '=', 3); // Split v LHP.
            })
            ->leftJoin("$tbl_batsplit AS SPLITS_vRHP", function ($join) use ($tbl_heatmap) {
                $join->on('SPLITS_vRHP.season', '=', "$tbl_heatmap.season")
                    ->on('SPLITS_vRHP.season_type', '=', "$tbl_heatmap.season_type")
                    ->on('SPLITS_vRHP.player_id', '=', "$tbl_heatmap.player_id")
                    ->where('SPLITS_vRHP.split_type', '=', 'pitcher-info')
                    ->where('SPLITS_vRHP.split_label', '=', 4); // Split v RHP.
            })
            ->select("$tbl_heatmap.*")
            ->selectRaw("UNCOMPRESS($tbl_heatmap.heatzones) AS heatzones")
            ->selectRaw('SPLITS_vLHP.avg AS avg_vLHP')
            ->selectRaw('SPLITS_vRHP.avg AS avg_vRHP')
            ->where([
                ["$tbl_heatmap.season", '=', $query_games[0][0][2]],
                ["$tbl_heatmap.season_type", '=', $query_games[0][1][2]],
            ])->whereIn("$tbl_heatmap.player_id", $this->secondary['players']->unique('player_id'))
            ->where([
                ["$tbl_heatmap.batter_hand", '!=', 'CMB'],
                ["$tbl_heatmap.pitcher_hand", '!=', 'CMB'],
            ])->get();
        // Now tie together.
        foreach ($this->data as $i => $game) {
            foreach (['home', 'visitor'] as $team) {
                $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game[$team]->team_id}";
                $this->data[$i][$team]->batting = $batting->where('team_ref', $ref);
                $this->data[$i][$team]->pitching = $pitching->where('team_ref', $ref);
                $this->data[$i][$team]->fielding = $fielding->where('team_ref', $ref);
                $players = $this->data[$i][$team]->lineup->unique('player_id');
                $this->data[$i][$team]->batter_zones = $batter_zones->whereIn('player_id', $players);
            }
        }
    }

    /**
     * Return the game hitting for the players involved
     * @return PlayerGameBatting The ORM object of hitting stats
     */
    protected function getSecondaryPlayerStatsBatting(): PlayerGameBatting
    {
        $tbl_batting = PlayerGameBatting::getTable();
        $tbl_batting_y2d = PlayerGameBattingY2D::getTable();
        $query_games = $this->queryGames($tbl_batting);
        return PlayerGameBatting::query()
            ->join($tbl_batting_y2d, function ($join) use ($tbl_batting_y2d, $tbl_batting) {
                $join->on("$tbl_batting_y2d.season", '=', "$tbl_batting.season")
                    ->on("$tbl_batting_y2d.game_type", '=', "$tbl_batting.game_type")
                    ->on("$tbl_batting_y2d.game_id", '=', "$tbl_batting.game_id")
                    ->on("$tbl_batting_y2d.player_id", '=', "$tbl_batting.player_id")
                    ->where("$tbl_batting_y2d.is_totals", '=', '1');
            })->select("$tbl_batting.*")
            ->selectRaw("$tbl_batting_y2d.avg AS avg_y2d")
            ->selectRaw("$tbl_batting_y2d.obp AS obp_y2d")
            ->selectRaw("$tbl_batting_y2d.ops AS ops_y2d")
            ->selectRaw("$tbl_batting_y2d.2b AS 2b_y2d")
            ->selectRaw("$tbl_batting_y2d.3b AS 3b_y2d")
            ->selectRaw("$tbl_batting_y2d.hr AS hr_y2d")
            ->selectRaw("$tbl_batting_y2d.rbi AS rbi_y2d")
            ->selectRaw("CONCAT($tbl_batting.season, '-', $tbl_batting.game_type, '-', "
                . "$tbl_batting.game_id, '-', $tbl_batting.team_id) AS team_ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->orderBy('season')
            ->orderBy('game_type')
            ->orderBy('game_id')
            ->orderBy('order')
            ->get();
    }

    /**
     * Return the game pitching for the players involved
     * @return PlayerGamePitching The ORM object of pitching stats
     */
    protected function getSecondaryPlayerStatsPitching(): PlayerGamePitching
    {
        $tbl_pitching = PlayerGamePitching::getTable();
        $tbl_pitching_y2d = PlayerGamePitchingY2D::getTable();
        $query_games = $this->queryGames($tbl_pitching);
        return PlayerGamePitching::query()
            ->join($tbl_pitching_y2d, function ($join) use ($tbl_pitching_y2d, $tbl_pitching) {
                $join->on("$tbl_pitching_y2d.season", '=', "$tbl_pitching.season")
                    ->on("$tbl_pitching_y2d.game_type", '=', "$tbl_pitching.game_type")
                    ->on("$tbl_pitching_y2d.game_id", '=', "$tbl_pitching.game_id")
                    ->on("$tbl_pitching_y2d.player_id", '=', "$tbl_pitching.player_id")
                    ->where("$tbl_pitching_y2d.is_totals", '=', '1');
            })->select("$tbl_pitching.*")
            ->selectRaw("$tbl_pitching_y2d.w AS w_y2d")
            ->selectRaw("$tbl_pitching_y2d.l AS l_y2d")
            ->selectRaw("$tbl_pitching_y2d.sv AS sv_y2d")
            ->selectRaw("$tbl_pitching_y2d.hld AS hld_y2d")
            ->selectRaw("$tbl_pitching_y2d.bs AS bs_y2d")
            ->selectRaw("$tbl_pitching_y2d.k AS k_y2d")
            ->selectRaw("$tbl_pitching_y2d.era AS era_y2d")
            ->selectRaw("$tbl_pitching_y2d.whip AS whip_y2d")
            ->selectRaw("CONCAT($tbl_pitching.season, '-', $tbl_pitching.game_type, '-', "
                . "$tbl_pitching.game_id, '-', $tbl_pitching.team_id) AS team_ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
    }

    /**
     * Return the game fielding for the players involved
     * @return PlayerGameFielding The ORM object of fielding stats
     */
    protected function getSecondaryPlayerStatsFielding(): PlayerGameFielding
    {
        $tbl_fielding = PlayerGameFielding::getTable();
        $tbl_fielding_y2d = PlayerGameFieldingY2D::getTable();
        $query_games = $this->queryGames($tbl_fielding);
        return PlayerGameFielding::query()
            ->join($tbl_fielding_y2d, function ($join) use ($tbl_fielding_y2d, $tbl_fielding) {
                $join->on("$tbl_fielding_y2d.season", '=', "$tbl_fielding.season")
                    ->on("$tbl_fielding_y2d.game_type", '=', "$tbl_fielding.game_type")
                    ->on("$tbl_fielding_y2d.game_id", '=', "$tbl_fielding.game_id")
                    ->on("$tbl_fielding_y2d.player_id", '=', "$tbl_fielding.player_id")
                    ->where("$tbl_fielding_y2d.is_totals", '=', '1');
            })->select("$tbl_fielding.*")
            ->selectRaw("$tbl_fielding_y2d.e AS e_y2d")
            ->selectRaw("$tbl_fielding_y2d.pct AS pct_y2d")
            ->selectRaw("CONCAT($tbl_fielding.season, '-', $tbl_fielding.game_type, '-', "
                . "$tbl_fielding.game_id, '-', $tbl_fielding.team_id) AS team_ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
    }

    /**
     * Load the linescores for the teams involved
     * @return void
     */
    public function loadSecondaryPeriods(): void
    {
        // Run the query.
        $query = GamePeriod::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", team_id) AS ref');
        foreach ($this->queryGames() as $game_where) {
            $query->orWhere(function (Builder $where) use ($game_where) {
                $where->where($game_where);
            });
        }
        $linescores = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        foreach ($this->data as $i => $game) {
            $base_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-";
            $this->data[$i]['home']->scoring = $linescores->where('ref', "{$base_ref}{$game['home_id']}");
            $this->data[$i]['visitor']->scoring = $linescores->where('ref', "{$base_ref}{$game['visitor_id']}");
        }
    }

    /**
     * Load information about the pitchers of record
     * @return void
     */
    public function loadSecondaryPitchers(): void
    {
        $tbl_roster = GameRoster::getTable();
        $tbl_player = Player::getTable();
        $tbl_stats = PlayerGamePitchingY2D::getTable();
        foreach (['winning', 'losing', 'saving'] as $type) {
            $pitcher_type = "{$type}_pitcher";
            $query = Player::query($tbl_roster)
                ->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_roster.player_id")
                ->join($tbl_stats, function ($join) use ($tbl_stats, $tbl_roster) {
                    $join->on("$tbl_stats.season", '=', "$tbl_roster.season")
                        ->on("$tbl_stats.game_type", '=', "$tbl_roster.game_type")
                        ->on("$tbl_stats.game_id", '=', "$tbl_roster.game_id")
                        ->on("$tbl_stats.team_id", '=', "$tbl_roster.team_id")
                        ->on("$tbl_stats.player_id", '=', "$tbl_roster.player_id");
                })->select(["$tbl_player.*", "$tbl_stats.w", "$tbl_stats.l", "$tbl_stats.sv"])
                ->selectRaw("CONCAT($tbl_roster.season, '-', $tbl_roster.game_type, '-', $tbl_roster.game_id) AS ref");
            foreach ($this->data as $i => $game) {
                // Skip games that do not have an appropriate pitcher.
                if (!isset($game[$pitcher_type])) {
                    continue;
                }
                // Complete the query.
                if ($type != 'losing') {
                    $team_id = ($game['home_score'] > $game['visitor_score'] ? $game['home_id'] : $game['visitor_id']);
                } else {
                    $team_id = ($game['home_score'] < $game['visitor_score'] ? $game['home_id'] : $game['visitor_id']);
                }
                $where_clause = [
                    ["$tbl_roster.season", '=', $game['season']],
                    ["$tbl_roster.game_type", '=', $game['game_type']],
                    ["$tbl_roster.game_id", '=', $game['game_id']],
                    ["$tbl_roster.team_id", '=', $team_id],
                    ["$tbl_roster.jersey", '=', $game[$pitcher_type]],
                ];
                $query->orWhere(function (Builder $where) use ($where_clause) {
                    $where->where($where_clause);
                });
            }
            $pitchers = $query->orWhere(DB::raw(1), DB::raw(0))->get();
            foreach ($this->data as $i => $game) {
                $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
                $this->data[$i]["{$pitcher_type}_num"] = $game[$pitcher_type];
                $this->data[$i][$pitcher_type] = $pitchers->where('ref', $ref);
            }
        }
    }

    /**
     * Load information about the probable pitchers
     * @return void
     */
    public function loadSecondaryProbables(): void
    {
        $tbl_player = Player::getTable();
        $tbl_probables = GameProbable::getTable();
        $tbl_stats = PlayerSeasonPitching::getTable();
        // Determine the games this applies to.
        $query_games = $this->queryGames($tbl_probables, true);
        // Build our query from the games being loaded.
        $probables = Player::query($tbl_probables)
            ->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_probables.player_id")
            ->leftJoin($tbl_stats, function ($join) use ($tbl_stats, $tbl_probables) {
                $join->on("$tbl_stats.season", '=', "$tbl_probables.season")
                    ->on("$tbl_stats.season_type", '=', "$tbl_probables.game_type")
                    ->on("$tbl_stats.player_id", '=', "$tbl_probables.player_id")
                    ->where("$tbl_stats.is_totals", '=', 1);
            })->select([
                "$tbl_player.*",
                "$tbl_probables.team_id",
                "$tbl_stats.w",
                "$tbl_stats.l",
                "$tbl_stats.era",
                "$tbl_stats.whip",
            ])->selectRaw("CONCAT($tbl_probables.season, '-', $tbl_probables.game_type, '-', $tbl_probables.game_id, "
                . "'-', $tbl_probables.team_id) AS ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        foreach ($this->data as $i => $game) {
            $base_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-";
            $this->data[$i]['home']->probable = $probables->where('ref', "{$base_ref}{$game['home_id']}");
            $this->data[$i]['visitor']->probable = $probables->where('ref', "{$base_ref}{$game['visitor_id']}");
        }
    }

    /**
     * Load information about the game's home runs
     * @return void
     */
    public function loadSecondaryHomeRuns(): void
    {
        $tbl_player = Player::getTable();
        $tbl_batting = PlayerGameBatting::getTable();
        $tbl_batting_y2d = PlayerGameBattingY2D::getTable();
        // Determine the games this applies to.
        $query_games = $this->queryGames($tbl_batting);
        // Build our query from the games being loaded.
        $home_runs = Player::query($tbl_batting)
            ->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_batting.player_id")
            ->join($tbl_batting_y2d, function ($join) use ($tbl_batting_y2d, $tbl_batting) {
                $join->on("$tbl_batting_y2d.season", '=', "$tbl_batting.season")
                    ->on("$tbl_batting_y2d.game_type", '=', "$tbl_batting.game_type")
                    ->on("$tbl_batting_y2d.game_id", '=', "$tbl_batting.game_id")
                    ->on("$tbl_batting_y2d.player_id", '=', "$tbl_batting.player_id")
                    ->where("$tbl_batting_y2d.is_totals", '=', '1');
            })->select(["$tbl_player.*", "$tbl_batting.team_id", "$tbl_batting.hr"])
            ->selectRaw("$tbl_batting_y2d.hr AS hr_y2d")
            ->selectRaw("CONCAT($tbl_batting.season, '-', $tbl_batting.game_type, '-', $tbl_batting.game_id) AS ref")
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->where("$tbl_batting.hr", '>', '0')
            ->orderBy("$tbl_batting.game_id")
            ->orderBy("$tbl_batting.team_id")
            ->orderBy("$tbl_player.surname")
            ->orderBy("$tbl_player.first_name")
            ->get();
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['home_runs'] = $home_runs->where('ref', $ref);
        }
    }

    /**
     * Load the details of the officials involved
     * @return void
     */
    public function loadSecondaryOfficials(): void
    {
        $query_games = $this->queryGames();
        $game_officials = GameOfficial::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS game_ref')
            ->where(function ($query) use ($query_games) {
                foreach ($query_games as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Get the official's details.
        $officials = Official::query()
            ->whereIn('umpire_id', $game_officials->unique('umpire_id'))
            ->get();
        // Now tie the official to the game official.
        foreach ($game_officials as $row) {
            $row->official = $officials->where('umpire_id', $row->umpire_id);
        }
        // And then the officials to the game.
        foreach ($this->data as $i => $game) {
            $game_ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}";
            $this->data[$i]['officials'] = $game_officials->where('game_ref', $game_ref);
        }
    }

    /**
     * Build the Highcharts configuration for the Win Probability chart
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the game's win probability progress
     */
    public function winProbabilityChart(string $dom_id): array
    {
        $chart = Highcharts::new($dom_id, ['annotations' => true]);
        list($winprob, $x_spacing, $x_disp, $plays, $scoring) = $this->winProbabilityChartSetup();
        // Setup the x-axis.
        $chart['xAxis'] = [
            'categories' => array_keys($winprob),
            'labels' => [
                'formatter' => 'function() { return winprobXLabels[this.value]; }',
                'rotation' => 45,
            ],
            'tickPositions' => $x_spacing,
        ];
        $chart['extraJS'] = [
            'winprobXLabels' => $x_disp,
            'winprobPlays' => $plays,
        ];
        // Setup the y-axis.
        $y_home = '<span class="icon team-mlb-' . $this->home_id . '">100%</span>';
        $y_visitor = '<span class="icon team-mlb-' . $this->visitor_id . '">100%</span>';
        $chart['yAxis'] = [[
            'labels' => [
                'useHTML' => true,
                'formatter' => "function() { if (this.value == -50) { return '$y_visitor'; } "
                    . "else if (this.value == 50) { return '$y_home'; } "
                    . "return (Math.abs(this.value) + 50) + '%' ; }"
            ],
            'title' => [
                'text' => 'Game Winner',
            ],
            'startOnTick' => true,
            'endOnTick' => true,
            'tickPositions' => range(50, -50, -10),
            'min' => -50, 'max' => 50,
            'plotBands' => [
                [
                    'from' => -50,
                    'to' => 0,
                ],
                [
                    'from' => 0,
                    'to' => 50,
                ],
            ],
        ]];
        // General chart setup.
        $chart['plotOptions'] = [
            'spline' => [
                'marker' => [
                    'enabled' => false,
                ],
                'events' => [
                    'legendItemClick' => 'function() { return false; }',
                ],
            ],
        ];
        $chart['legend'] = [
            'enabled' => false,
        ];
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { return winprobHover(this); }',
        ];
        // The data points.
        $chart['series'] = [[
            'yAxis' => 0,
            'type' => 'spline',
            'data' => array_values($winprob),
        ]];
        // Annotations of scoring plays.
        $chart['annotations'] = [[
            'labels' => $scoring,
        ]];
        return $chart;
    }

    /**
     * Process the data in to an easier form to be used by the Highcharts builder
     * Array elements:
     *   0 - inning
     *   1 - half_inning
     *   2 - batter_team_id
     *   3 - batter
     *   4 - pitcher_team_id
     *   5 - pitcher
     *   6 - result
     *   7 - on_base
     *   8 - ob_321b
     *   9 - out
     *  10 - scoring
     *  11 - home_score
     *  12 - visitor_score
     *  13 - home_winprob
     *  14 - visitor_winprob
     * @return array The various setup elements
     */
    protected function winProbabilityChartSetup(): array
    {
        $rosters = $winprob = $x_spacing = $x_disp = $plays = $scoring = [];
        // We need to determine player lists.
        foreach (['home', 'visitor'] as $team) {
            foreach ($this->$team->lineup as $row) {
                $rosters["{$row->team_id}-{$row->jersey}"] = $row->player;
            }
        }
        // Loop through the plays, adding to the appropriate lists.
        $last_half = '';
        $last_half_id = null;
        $last_home = $last_visitor = 0;
        foreach ($this->winprob as $play_id => $play) {
            $play_index = count($winprob);
            // New half inning?
            $inning_ref = "{$play[0]}-{$play[1]}";
            if ($inning_ref != $last_half) {
                $x_spacing[] = $play_index;
                $x_disp[$play_id] = ucfirst($play[1])
                    . ' ' . \DeBear\Helpers\Format::ordinal($play[0]);
                $last_half = $inning_ref;
                $last_half_id = $play_id;
            }
            // Chart y-axis value.
            $y = (float)sprintf('%0.01f', $play[13] - 50); // -50..50
            // Scoring annotations?
            if ($play[10]) {
                $scoring[] = [
                    'point' => [
                        'x' => $play_index,
                        'y' => $y,
                        'xAxis' => 0,
                        'yAxis' => 0,
                    ],
                    'text' => ($play[1] == 'top'
                        ? $play[12] - $last_visitor
                        : $play[11] - $last_home) . " Run {$play[6]}",
                    'y' => ($y < -30 ? 35 : -10),
                ];
            }
            $last_home = $play[11];
            $last_visitor = $play[12];
            // Base off the home team's win probability.
            $winprob[$play_id] = $y;
            // Detailed play info.
            $plays[$play_id] = [
                'inn_index' => $last_half_id,
                'home_score' => $last_home,
                'visitor_score' => $last_visitor,
                'winprob' => abs($y) + 50,
                'winprob_team' => ($play[13] > 50
                    ? $this->home_id
                    : ($play[14] > 50 ? $this->visitor_id : null)),
                'out' => $play[9],
                'ob_tot' => $play[7],
                'ob_det' => $play[8],
                'batter_team' => $play[2],
                'batter' => $rosters["{$play[2]}-{$play[3]}"]->name,
                'pitcher_team' => $play[4],
                'pitcher' => $rosters["{$play[4]}-{$play[5]}"]->name,
                'result' => $play[6],
            ];
        }

        // Return our various components.
        return [$winprob, $x_spacing, $x_disp, $plays, $scoring];
    }
}
