<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamSeasonBatting extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'pa' => ['s' => 'PA', 'f' => 'Plate Appearances'],
        'ab' => ['s' => 'AB', 'f' => 'At Bats'],
        'h' => ['s' => 'H', 'f' => 'Hits'],
        '1b' => ['s' => '1B', 'f' => 'Singles'],
        '2b' => ['s' => '2B', 'f' => 'Doubles'],
        '3b' => ['s' => '3B', 'f' => 'Triples'],
        'hr' => ['s' => 'HR', 'f' => 'Home Runs'],
        'xbh' => ['s' => 'XBH', 'f' => 'Extra-Base Hits'],
        'tb' => ['s' => 'TB', 'f' => 'Total Bases'],
        'bb' => ['s' => 'BB', 'f' => 'Base on Balls'],
        'ibb' => ['s' => 'IBB', 'f' => 'Intentional Walks'],
        'hbp' => ['s' => 'HBP', 'f' => 'Hit by Pitch'],
        'k' => ['s' => 'K', 'f' => 'Strikeouts'],
        'r' => ['s' => 'R', 'f' => 'Runs'],
        'rbi' => ['s' => 'RBI', 'f' => 'Runs Batted In'],
        'sb' => ['s' => 'SB', 'f' => 'Stolen Bases'],
        'cs' => ['s' => 'CS', 'f' => 'Caught Stealing'],
        'po' => ['s' => 'PO', 'f' => 'Pick Offs'],
        'sac_fly' => ['s' => 'SacFly', 'f' => 'Sacrifice Flys'],
        'sac_hit' => ['s' => 'SacHit', 'f' => 'Sacrifice Hits'],
        'go' => ['s' => 'GO', 'f' => 'Ground Outs'],
        'fo' => ['s' => 'FO', 'f' => 'Fly Outs'],
        'gidp' => ['s' => 'GIDP', 'f' => 'Grounded into Double Plays'],
        'lob' => ['s' => 'LOB', 'f' => 'Left on Base'],
        'avg' => ['s' => 'Avg', 'f' => 'Batting Average'],
        'obp' => ['s' => 'OBP', 'f' => 'On Base %age'],
        'slg' => ['s' => 'SLG', 'f' => 'Slugging %age'],
        'ops' => ['s' => 'OPS', 'f' => 'On Base Plus Slugging'],
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (in_array($column, ['avg', 'obp', 'slg', 'ops'])) {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
