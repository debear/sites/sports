<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

class PlayerSeasonFieldingSorted extends PlayerSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = PlayerSeasonFielding::class;
}
