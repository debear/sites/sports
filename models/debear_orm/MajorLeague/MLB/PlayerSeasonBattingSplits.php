<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonSplits as BasePlayerSeasonSplits;
use DeBear\Helpers\Sports\MLB\Traits\StatsSplit as StatsSplitTrait;
use DeBear\Helpers\Sports\MLB\Traits\StatsBatting as StatsBattingTrait;

class PlayerSeasonBattingSplits extends BasePlayerSeasonSplits
{
    use StatsSplitTrait;
    use StatsBattingTrait;
}
