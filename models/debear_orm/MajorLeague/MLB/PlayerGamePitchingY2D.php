<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsPitching as StatsPitchingTrait;

class PlayerGamePitchingY2D extends BasePlayerGameStat
{
    use StatsPitchingTrait;
}
