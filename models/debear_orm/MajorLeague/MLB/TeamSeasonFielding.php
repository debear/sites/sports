<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat as BaseTeamSeasonStat;

class TeamSeasonFielding extends BaseTeamSeasonStat
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'tc' => ['s' => 'TC', 'f' => 'Total Chances'],
        'po' => ['s' => 'PO', 'f' => 'Putouts'],
        'a' => ['s' => 'A', 'f' => 'Assists'],
        'e' => ['s' => 'E', 'f' => 'Errors'],
        'pct' => ['s' => 'Pct', 'f' => 'Fielding Percentage'],
        'dp' => ['s' => 'DP', 'f' => 'Double Plays'],
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'pct') {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
