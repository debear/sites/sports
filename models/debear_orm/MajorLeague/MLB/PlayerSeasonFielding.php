<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsFielding as StatsFieldingTrait;

class PlayerSeasonFielding extends BasePlayerSeasonStat
{
    use StatsFieldingTrait;
}
