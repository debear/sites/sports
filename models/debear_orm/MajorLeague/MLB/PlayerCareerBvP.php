<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerCareerStat as BasePlayerCareerStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsSplit as StatsSplitTrait;
use DeBear\Helpers\Sports\MLB\Traits\StatsBvP as StatsBvPTrait;

class PlayerCareerBvP extends BasePlayerCareerStat
{
    use StatsSplitTrait;
    use StatsBvPTrait;
}
