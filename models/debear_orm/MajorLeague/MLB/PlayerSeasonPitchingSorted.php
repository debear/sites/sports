<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

class PlayerSeasonPitchingSorted extends PlayerSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = PlayerSeasonPitching::class;
}
