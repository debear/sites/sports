<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\Player as BasePlayer;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;

class Player extends BasePlayer
{
    /**
     * Get the relevant field for the custom field section of the player page
     * @return string The value for the player's header row custom field
     */
    public function getHeaderCustomAttribute(): string
    {
        return join(' / ', [
            $this->stands ?? '?',
            $this->throws ?? '?',
        ]);
    }

    /**
     * Determine the season's stats of players in the dataset
     * @param array $seasons An optional list of seasons to narrow our search down by.
     * @return void
     */
    protected function loadSecondaryCareerStats(array $seasons = []): void
    {
        // Get the stats according to the player's position.
        $player_stats = [];
        foreach ($this->unique('group') as $group) {
            $list = $this->where('group', $group)->unique('player_id');
            // Determine base query logic for this class.
            $class = FrameworkConfig::get("debear.setup.stats.details.player.$group.class.stat");
            $tbl_stat = $class::getTable();
            $tbl_misc = PlayerSeasonMisc::getTable();
            $query = $class::query()
                ->select("$tbl_stat.*", "$tbl_misc.gp", "$tbl_misc.gs")
                ->selectRaw("1 AS `can_$group`") // This stat group is always available in dropdowns.
                // Logic to determine if a team row represents the aggregate season total or not.
                ->selectRaw("IF(SUBSTRING($tbl_stat.team_id, 1, 1) = '_', 'Total', $tbl_stat.team_id) AS team_id")
                ->selectRaw('? AS stat_group', [$group])
                ->join($tbl_misc, function ($join) use ($tbl_misc, $tbl_stat) {
                    return $join->on("$tbl_misc.season", '=', "$tbl_stat.season")
                        ->on("$tbl_misc.season_type", '=', "$tbl_stat.season_type")
                        ->on("$tbl_misc.player_id", '=', "$tbl_stat.player_id")
                        ->on("$tbl_misc.team_id", '=', "$tbl_stat.team_id");
                });
            // Now determine suitability of the other stat groups available.
            foreach (FrameworkConfig::get('debear.setup.stats.details.player') as $alt_group => $alt_config) {
                if ($alt_group == $group) {
                    // We have already covered the current group above.
                    continue;
                }
                $tbl_alt = $alt_config['class']['stat']::getTable();
                $query->selectRaw("$tbl_alt.season IS NOT NULL AS `can_$alt_group`")
                    ->leftJoin($tbl_alt, function ($join) use ($tbl_alt, $tbl_stat) {
                        $join->on("$tbl_alt.season", '=', "$tbl_stat.season")
                        ->on("$tbl_alt.season_type", '=', "$tbl_stat.season_type")
                        ->on("$tbl_alt.player_id", '=', "$tbl_stat.player_id")
                        ->on("$tbl_alt.team_id", '=', "$tbl_stat.team_id");
                    });
            }
            // Process the rest of our query.
            $query->selectRaw('? AS stat_group', [$group]);
            if (count($seasons)) {
                $query->whereIn("$tbl_stat.season", $seasons);
            }
            $stats = $query->whereIn("$tbl_stat.player_id", $list)
                ->orderByDesc("$tbl_stat.season")
                ->orderByRaw("$tbl_stat.`season_type` = 'regular' DESC") // Regular season first.
                ->orderBy("$tbl_stat.team_id")
                ->get();
            // Now prepare for tying together later.
            foreach ($list as $id) {
                $player_stats[$id] = $stats->where('player_id', $id);
            }
        }
        // Now tie-together.
        $key = count($seasons) ? 'seasonStats' : 'careerStats';
        foreach ($this->data as $i => $player) {
            $this->data[$i][$key] = $player_stats[$player['player_id']];
        }
    }

    /**
     * Determine the games played over the given season for the players in the dataset
     * @param integer $max_games The most games to return, or 0 to return every game played.
     * @return void
     */
    protected function loadSecondarySeasonGames(int $max_games = 0): void
    {
        // On the expectation we're only processing one player, process per-player.
        foreach ($this->data as $i => $player) {
            // From the player group, determine the class to use.
            $class = FrameworkConfig::get("debear.setup.stats.details.player.{$player['group']}.class.game");
            // But then also which columns should be tracked from the "year to date" tables.
            $class_y2d = FrameworkConfig::get("debear.setup.stats.details.player.{$player['group']}.class.y2d");
            $cols_y2d = FrameworkConfig::get("debear.setup.stats.details.player.{$player['group']}.y2d");
            // Build the status query.
            $tbl_stat = $class::getTable();
            $tbl_y2d = $class_y2d::getTable();
            $tbl_schedule = Schedule::getTable();
            $query = $class::query($tbl_schedule)
                ->select("$tbl_stat.*");
            foreach ($cols_y2d as $field => $col) {
                $field = is_numeric($field) ? $col : $field;
                $query->selectRaw("$tbl_y2d.`$col` as `$field`");
            }
            $query->selectRaw('? AS stat_group', [$player['group']])
                ->selectRaw("1 AS `can_{$player['group']}`") // This stat group is always available in dropdowns.
                ->join($tbl_stat, function ($join) use ($tbl_stat, $tbl_schedule, $player) {
                    $join->on("$tbl_stat.season", '=', "$tbl_schedule.season")
                        ->on("$tbl_stat.game_type", '=', "$tbl_schedule.game_type")
                        ->on("$tbl_stat.game_id", '=', "$tbl_schedule.game_id")
                        ->where("$tbl_stat.player_id", '=', $player['player_id']);
                })->join($tbl_y2d, function ($join) use ($tbl_y2d, $tbl_stat) {
                    $join->on("$tbl_y2d.season", '=', "$tbl_stat.season")
                        ->on("$tbl_y2d.game_type", '=', "$tbl_stat.game_type")
                        ->on("$tbl_y2d.game_id", '=', "$tbl_stat.game_id")
                        ->on("$tbl_y2d.team_id", '=', "$tbl_stat.team_id")
                        ->on("$tbl_y2d.player_id", '=', "$tbl_stat.player_id");
                });
            // Now determine suitability of the other stat groups available.
            foreach (FrameworkConfig::get('debear.setup.stats.details.player') as $alt_group => $alt_config) {
                if ($alt_group == $player['group']) {
                    // We have already covered the current group above.
                    continue;
                }
                $tbl_alt = $alt_config['class']['game']::getTable();
                $query->selectRaw("$tbl_alt.season IS NOT NULL AS `can_$alt_group`")
                    ->leftJoin($tbl_alt, function ($join) use ($tbl_alt, $tbl_stat) {
                        $join->on("$tbl_alt.season", '=', "$tbl_stat.season")
                        ->on("$tbl_alt.game_type", '=', "$tbl_stat.game_type")
                        ->on("$tbl_alt.game_id", '=', "$tbl_stat.game_id")
                        ->on("$tbl_alt.team_id", '=', "$tbl_stat.team_id")
                        ->on("$tbl_alt.player_id", '=', "$tbl_stat.player_id");
                    });
            }
            // If we're not limiting to recent games, then apply for the current season only.
            if (!$max_games) {
                $query->where([
                    ["$tbl_schedule.season", '=', FrameworkConfig::get('debear.setup.season.viewing')],
                    ["$tbl_schedule.game_type", '=', FrameworkConfig::get('debear.setup.season.type')],
                ]);
            }
            // Finally finish our query.
            $key = ($max_games ? 'recentGames' : 'seasonGames');
            $this->data[$i][$key] = $query->orderByDesc("$tbl_schedule.game_date")
                ->orderByDesc("$tbl_schedule.game_time")
                ->limit($max_games ?: null) // Return all games if no max value passed.
                ->get()
                ->loadSecondary(['games']);
        }
    }

    /**
     * Determine the repertoires of the pitchers in the dataset
     * @return void
     */
    protected function loadSecondaryPitcherRepertoire(): void
    {
        $season = $this->getLatestSeason();
        $data = PlayerRepertoire::query()
            ->where('season', $season)
            ->whereIn('player_id', $this->unique('player_id'))
            ->get();
        foreach ($this->data as $i => $player) {
            $this->data[$i]['repertoire'] = $data->where('player_id', $player['player_id']);
        }
    }

    /**
     * Determine the split stats across the given season for the players in the dataset
     * @return void
     */
    protected function loadSecondarySplitStats(): void
    {
        $this->loadSecondarySplitsWorker('split');
    }

    /**
     * Determine the situational stats across the given season for the players in the dataset
     * @return void
     */
    protected function loadSecondarySituationalStats(): void
    {
        $this->loadSecondarySplitsWorker('situational');
    }

    /**
     * Determine the split stats across the given season for the players in the dataset
     * @param string $type The type of splits to load from the database.
     * @return void
     */
    protected function loadSecondarySplitsWorker(string $type): void
    {
        // On the expectation we're only processing one player, process per-player.
        foreach ($this->data as $i => $player) {
            // Determine the split stat availability for this player.
            $can = [];
            $method = "get{$type}StatTypes";
            foreach (FrameworkConfig::get('debear.setup.stats.details.player') as $alt_group => $alt_config) {
                if ($alt_group == $player['group']) {
                    // We have already covered the current group in the main query.
                    continue;
                }
                $can[$alt_group] = $alt_config['class']['split']::query()
                    ->selectRaw('COUNT(*) > 0 AS has_stats')
                    ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
                    ->where('season_type', FrameworkConfig::get('debear.setup.season.type'))
                    ->where('player_id', $player['player_id'])
                    ->whereIn('split_type', array_keys((new $alt_config['class']['split']())->$method()))
                    ->get()
                    ->has_stats;
            }
            // From the player group, determine the class to use.
            $class = FrameworkConfig::get("debear.setup.stats.details.player.{$player['group']}.class.split");
            // Build the status query.
            $tbl_split = $class::getTable();
            $tbl_label = SplitLabels::getTable();
            $query = $class::query()
                ->select(["$tbl_split.*", "$tbl_label.split_label"])
                ->selectRaw('? AS stat_group', [$player['group']])
                ->selectRaw("1 AS can_{$player['group']}");
            foreach ($can as $alt_group => $alt_can) {
                $query->selectRaw("? AS can_$alt_group", [$alt_can]);
            }
            $key = "{$type}Stats";
            $this->data[$i][$key] = $query->join($tbl_label, "$tbl_label.split_id", '=', "$tbl_split.split_label")
                ->where("$tbl_split.season", FrameworkConfig::get('debear.setup.season.viewing'))
                ->where("$tbl_split.season_type", FrameworkConfig::get('debear.setup.season.type'))
                ->where("$tbl_split.player_id", $player['player_id'])
                ->orderBy("$tbl_label.split_type")
                ->orderBy("$tbl_label.order")
                ->orderBy("$tbl_label.split_label")
                ->get()
                ->loadSecondary(['teams', 'stadia']);
        }
    }

    /**
     * Determine the batter-vs-pitcher stats across the given season for the players in the dataset
     * @return void
     */
    protected function loadSecondaryBatterVsPitcher(): void
    {
        $season = FrameworkConfig::get('debear.setup.season');
        $is_career = ($season['viewing'] === '0000');
        // Determine suitability for batting/pitching dropdowns.
        $can = [];
        foreach (FrameworkConfig::get('debear.setup.stats.details.player') as $group => $config) {
            if (isset($config['class']['bvp'])) {
                $class = (!$is_career ? $config['class']['bvp']['season'] : $config['class']['bvp']['career']);
                $by_group = $class::query()
                    ->selectRaw("DISTINCT {$config['class']['bvp']['is']} AS players")
                    ->where(array_filter([
                        !$is_career ? ['season', '=', $season['viewing']] : false,
                        ['season_type', '=', $season['type']],
                    ]))->whereIn($config['class']['bvp']['is'], $this->unique('player_id'))->get();
                $can[$group] = array_fill_keys($by_group->unique('players'), true);
            }
        }
        // On the expectation we're only processing one player, process per-player.
        foreach ($this->data as $i => $player) {
            $config = FrameworkConfig::get("debear.setup.stats.details.player.{$player['group']}.class.bvp");
            $class = (!$is_career ? $config['season'] : $config['career']);
            // Build our query.
            $tbl_bvp = $class::getTable();
            $tbl_player = Player::getTable();
            $query = $class::query()
                ->select("$tbl_bvp.*")
                ->selectRaw("CONCAT($tbl_player.surname, ', ', $tbl_player.first_name) AS name_sort")
                ->selectRaw('? AS opp_column', [$config['opp']])
                ->selectRaw('? AS stat_group', [$is_career ? 'career' : 'season']);
            foreach (array_keys(FrameworkConfig::get('debear.setup.stats.details.player')) as $group) {
                $player_can = $group == $player['group'] || ($can[$group][$player['player_id']] ?? false);
                $query->selectRaw("? AS can_$group", [intval($player_can)]);
            }
            $this->data[$i]['batterVsPitcher'] = $query
                ->join($tbl_player, "$tbl_player.player_id", '=', "$tbl_bvp.{$config['opp']}")
                ->where(array_filter([
                    !$is_career ? ["$tbl_bvp.season", '=', $season['viewing']] : false,
                    ["$tbl_bvp.season_type", '=', $season['type']],
                    ["$tbl_bvp.{$config['is']}", '=', $player['player_id']],
                ]))->get()
                ->loadSecondary(['players', 'bvpteams'])
                ->sortBy('name_sort');
        }
    }

    /**
     * Determine the various chart data across the given season for the players in the dataset
     * @return void
     */
    protected function loadSecondaryZones(): void
    {
        // Validate the 'hand' arguments.
        $hands = Request::get('hands');
        if (!isset($hands) || !preg_match('/^(CMB|L|R)-(CMB|L|R)$/', $hands)) {
            $hands = 'CMB-CMB';
        }
        list($bh, $ph) = explode('-', $hands);
        // Comparison average.
        $cmp_when = $cmp_avg = [];
        foreach ($this->data as $player) {
            $cmp_when[] = 'WHEN ? THEN ?';
            $cmp_avg[] = $player['player_id'];
            // Player group dictates where we get the average.
            $s = $player['seasonStats'];
            if ($player['group'] == 'batting') {
                $cmp_avg[] = $s->avg;
            } else {
                // An approximation, 100% accuracy isn't strictly needed.
                $ab = $s->bf - $s->bb;
                $cmp_avg[] = ($ab ? $s->h / $ab : 0.0000);
            }
        }
        // Build our request.
        $class = FrameworkConfig::get("debear.setup.stats.details.player.{$this->group}.class.charts");
        $season = FrameworkConfig::get('debear.setup.season');
        $zones = $class::query()
            ->select(['player_id', 'batter_hand', 'pitcher_hand', 'spray_chart'])
            ->selectRaw('UNCOMPRESS(heatzones) AS heatzones')
            ->selectRaw('batter_hand = ? AND pitcher_hand = ? AS is_sel', [$bh, $ph])
            ->selectRaw('CASE player_id ' . join(' ', $cmp_when) . ' END AS cmp_avg', $cmp_avg)
            ->where([
                ['season', '=', $season['viewing']],
                ['season_type', '=', $season['type']],
            ])->whereIn('player_id', $this->unique('player_id'))
            ->get();
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $data = $zones->where('player_id', $player['player_id']);
            // Does the requested option exist in the data set?
            if (!$data->where('is_sel', 1)->count()) {
                // Batter missing?
                if (!$data->where('batter_hand', $bh)->count()) {
                    $opt = $data->unique('batter_hand');
                    $bh = $opt[0] ?? null;
                }
                // Pitcher missing?
                $rplc = $data->where('batter_hand', $bh ?? 'UNK');
                if (!$rplc->where('pitcher_hand', $ph)->count()) {
                    $opt = $rplc->unique('pitcher_hand');
                    $ph = $opt[0] ?? null;
                }
                // Re-calibrate 'is_sel', if a match has been found.
                $cmb = $data->where('batter_hand', $bh ?? 'UNK')->where('pitcher_hand', $ph ?? 'UNK');
                if ($cmb->isset()) {
                    $cmb->is_sel = 1;
                }
            }
            // Bind.
            $this->data[$i]['zones'] = $data;
        }
    }
}
