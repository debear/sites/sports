<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

class PlayerSeasonBattingSorted extends PlayerSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = PlayerSeasonBatting::class;
}
