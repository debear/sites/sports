<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat as BasePlayerSeasonStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsSplit as StatsSplitTrait;
use DeBear\Helpers\Sports\MLB\Traits\StatsBvP as StatsBvPTrait;

class PlayerSeasonBvP extends BasePlayerSeasonStat
{
    use StatsSplitTrait;
    use StatsBvPTrait;
}
