<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsBatting as StatsBattingTrait;

class PlayerGameBattingY2D extends BasePlayerGameStat
{
    use StatsBattingTrait;
}
