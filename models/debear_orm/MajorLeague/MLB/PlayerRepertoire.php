<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Highcharts;

class PlayerRepertoire extends Instance
{
    /**
     * Build the Highcharts configuration for the Win Probability chart
     * @param string $dom_id DOM element ID the chart will be rendered within.
     * @return array Appropriate highcharts configurations for the game's win probability progress
     */
    public function repertoireChart(string $dom_id): array
    {
        // Current usage as a pie chart.
        $chart = Highcharts::new($dom_id);
        // Customise - type.
        $chart['chart']['type'] = 'pie';
        // Customise - series.
        $chart['series'] = [['name' => 'Pitch Type', 'colorByPoint' => true, 'data' => []]];
        for ($i = 1; $i <= 4; $i++) {
            $col_label = "pitch{$i}";
            $col_pct = "{$col_label}_pct";
            if (!isset($this->$col_pct)) {
                // If we reach a null value, no more pitches are available.
                break;
            }
            $chart['series'][0]['data'][] = [
                'name' => FrameworkConfig::get("debear.setup.pitches.{$this->$col_label}"),
                'y' => floatval($this->$col_pct),
            ];
        }
        // Customise - tooltips.
        $chart['tooltip'] = [
            'shared' => false,
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.key + \'</strong>: \' + this.y + \'% of'
                . ' pitches thrown</tooltip>\'; }',
        ];
        return $chart;
    }
}
