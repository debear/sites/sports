<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonSplits as BasePlayerSeasonSplits;
use DeBear\Helpers\Sports\MLB\Traits\StatsSplit as StatsSplitTrait;
use DeBear\Helpers\Sports\MLB\Traits\StatsPitching as StatsPitchingTrait;

class PlayerSeasonPitchingSplits extends BasePlayerSeasonSplits
{
    use StatsSplitTrait;
    use StatsPitchingTrait;
}
