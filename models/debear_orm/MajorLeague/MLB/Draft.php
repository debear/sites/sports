<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\Draft as BaseDraft;

class Draft extends BaseDraft
{
    /**
     * Convert our ORM object to JSON
     * @return array The selections as an array
     */
    public function toJson(): array
    {
        $ret = [];
        foreach ($this->unique('draft_type') as $type) {
            $ret[$type] = [];
            $picks = $this->where('draft_type', $type);
            foreach ($picks->unique('round_descrip') as $round) {
                $ret[$type][$round] = [];
                foreach ($picks->where('round_descrip', $round) as $pick) {
                    $ret[$type][$round][] = [
                        'round' => $pick->round,
                        'pick' => $pick->pick,
                        'round_pick' => $pick->round_pick,
                        'team' => $pick->renderTeam($pick->team_id),
                        'player' => $pick->player,
                        'pos' => $pick->pos,
                        'height' => isset($pick->height) ? $pick->height_ft_in : null,
                        'weight' => $pick->weight,
                        'bats' => $pick->bats,
                        'throws' => $pick->throws,
                        'school' => $pick->school,
                    ];
                }
            }
        }
        return $ret;
    }

    /**
     * Load the draft results for a given season
     * @param integer $season The season's draft to load.
     * @return self The resulting ORM object of that season's draft
     */
    public static function load(int $season): self
    {
        return static::query()
            ->where('season', '=', $season)
            ->orderBy('draft_type')
            ->orderBy('pick')
            ->get();
    }
}
