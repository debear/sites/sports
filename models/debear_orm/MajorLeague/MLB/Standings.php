<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\Standings as BaseStandings;

class Standings extends BaseStandings
{
    /**
     * Return a column, formatted for display.
     * @param string $col The column to render.
     * @return string The formatted output
     */
    public function format(string $col): string
    {
        // Our concated 'W/L records' fields.
        $record_fields = array_flip(['record', 'home', 'visitor', 'conf', 'div', 'extras', 'onerun', 'recent']);

        // Process.
        if ($col == 'win_pct') {
            $ret = ltrim(sprintf('%.03f', $this->win_pct), '0');
        } elseif ($col == 'rd') {
            $diff = $this->runs_for - $this->runs_against;
            $diff_css = (!$diff ? 'eq' : ($diff > 0 ? 'pos' : 'neg'));
            $ret = '<span class="delta-' . $diff_css . '">' . ($diff > 0 ? '+' : '') . $diff . '</span>';
        } elseif (isset($record_fields[$col])) {
            $prefix = ($col == 'record' ? '' : "{$col}_");
            $col_w = "{$prefix}wins";
            $col_l = "{$prefix}loss";
            $col_t = "{$prefix}ties";
            $ret = preg_replace('/&ndash;0$/', '', "{$this->$col_w}&ndash;{$this->$col_l}&ndash;{$this->$col_t}");
        } else {
            // Fallback: The raw value is enough.
            $ret = $this->$col;
        }
        return $ret ?? '&ndash;';
    }
}
