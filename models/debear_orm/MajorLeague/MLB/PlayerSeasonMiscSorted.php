<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

class PlayerSeasonMiscSorted extends PlayerSeasonSorted
{
    /**
     * Our corresponding detailed stat object
     * @var string
     */
    protected $statInstance = PlayerSeasonMisc::class;
}
