<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\Schedule as BaseSchedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\Helpers\Sports\Namespaces;

class GameProbable extends GameIndividual
{
    /**
     * Load the probable pitchers for a given list of games
     * @param BaseSchedule $schedule The schedule ORM object for which the starting lineups are requested.
     * @return self The resulting ORM object containing all game for the probables date being passed in
     */
    public static function loadByGame(BaseSchedule $schedule): self
    {
        $tbl_probables = static::getTable();
        $tbl_sched = Schedule::getTable();
        return static::query()
            ->join($tbl_sched, function ($join) use ($tbl_sched, $tbl_probables) {
                $join->on("$tbl_sched.season", '=', "$tbl_probables.season")
                    ->on("$tbl_sched.game_type", '=', "$tbl_probables.game_type")
                    ->on("$tbl_sched.game_id", '=', "$tbl_probables.game_id");
            })->select(["$tbl_probables.*", "$tbl_sched.game_time_ref"])
            ->selectRaw("CONCAT($tbl_probables.game_type, '-', $tbl_probables.game_id, '-', $tbl_probables.team_id) "
                . 'AS game_team_ref')
            ->where([
                ["$tbl_probables.season", $schedule->season],
                ["$tbl_probables.game_type", $schedule->game_type],
                ["$tbl_probables.game_date", $schedule->game_date],
            ])
            ->orderBy("$tbl_probables.game_id")
            ->orderBy("$tbl_probables.venue")
            ->get()
            ->loadSecondary(['players', 'stats', 'catchers']);
    }

    /**
     * Load the pitcher's Y2D stats for the relevant players found
     * @return void
     */
    public function loadSecondaryStats(): void
    {
        // Build and run our query.
        $query = PlayerGamePitchingY2D::query()
            ->select(['season', 'game_type', 'player_id'])
            ->selectRaw('MAX(CONCAT(LPAD(game_time_ref, 5, "0"), ":", game_id)) AS game_time_ref');
        // Determine the per-player boundaries.
        foreach ($this->data as $probable) {
            $query->orWhere(function (Builder $where) use ($probable) {
                $where->where([
                    ['season', '=', $probable['season']],
                    ['game_type', '=', $probable['game_type']],
                    ['player_id', '=', $probable['player_id']],
                    ['game_time_ref', '<', $probable['game_time_ref']],
                ]);
            });
        }
        $last_y2d = $query->orWhere(DB::raw(1), DB::raw(0))->groupBy(['season', 'game_type', 'player_id'])->get();
        // Now use this to build our main data getter.
        $query = PlayerGamePitchingY2D::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", player_id) AS player_ref');
        foreach ($last_y2d as $i => $probable) {
            $game_id = substr($probable->game_time_ref, strpos($probable->game_time_ref, ':') + 1);
            $query->orWhere(function (Builder $where) use ($probable, $game_id) {
                $where->where([
                    ['season', '=', $probable->season],
                    ['game_type', '=', $probable->game_type],
                    ['game_id', '=', $game_id],
                    ['player_id', '=', $probable->player_id],
                    ['is_totals', '=', 1],
                ]);
            });
        }
        $pitching_y2d = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        // Now tie-back to our probables.
        foreach ($this->data as $i => $probable) {
            $player_ref = join('-', [
                $probable['season'],
                $probable['game_type'],
                $probable['player_id'],
            ]);
            $this->data[$i]['stats'] = $pitching_y2d->where('player_ref', $player_ref);
        }
    }

    /**
     * Load the player details for the relevant catchers found
     * @return void
     */
    public function loadSecondaryCatchers(): void
    {
        $players = Namespaces::callStatic('Player', 'query')->whereIn('player_id', $this->unique('catcher_id'))->get();
        foreach ($this->data as $i => $probable) {
            $this->data[$i]['catcher'] = $players->where('player_id', $probable['catcher_id'] ?? -1);
        }
    }
}
