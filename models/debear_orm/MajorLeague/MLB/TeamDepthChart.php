<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\TeamDepthChart as BaseTeamDepthChart;

class TeamDepthChart extends BaseTeamDepthChart
{
    /**
     * Load the player info for the returned stats
     * @return void
     */
    protected function loadSecondaryPlayers(): void
    {
        $players = Player::query()
            ->whereIn('player_id', $this->unique('player_id'))
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $leader) {
            $this->data[$i]['player'] = $players->where('player_id', $leader['player_id']);
        }
    }
}
