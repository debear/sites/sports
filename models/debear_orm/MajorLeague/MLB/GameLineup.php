<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\Schedule as BaseSchedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class GameLineup extends GameIndividual
{
    /**
     * Load the starting lineups for a given list of games
     * @param BaseSchedule $schedule The schedule ORM object for which the starting lineups are requested.
     * @return self The resulting ORM object containing all game for the lineup date being passed in
     */
    public static function loadByGame(BaseSchedule $schedule): self
    {
        $query = static::query()
            ->select('*')
            ->selectRaw('CONCAT(game_type, "-", game_id, "-", team_id) AS game_team_ref');
        // Determine the relevant games by game type.
        foreach ($schedule->unique('game_type') as $game_type) {
            $query->orWhere(function (Builder $where) use ($game_type, $schedule) {
                $where->where('season', $schedule->season)
                    ->where('game_type', $game_type)
                    ->whereIn('game_id', $schedule->where('game_type', $game_type)->unique('game_id'));
            });
        }
        return $query->orWhere(DB::raw(1), DB::raw(0))->orderBy('game_id')
            ->orderBy('team_id')
            ->orderByRaw('spot = 0') // SP at the bottom.
            ->orderBy('spot')
            ->get()
            ->loadSecondary(['players']);
    }
}
