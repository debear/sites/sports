<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use Illuminate\Support\Facades\DB;
use DeBear\ORM\Sports\MajorLeague\PlayerSeasonSorted as BasePlayerSeasonSorted;

class PlayerSeasonSorted extends BasePlayerSeasonSorted
{
    /**
     * Load the data for our stats table
     * @param integer $season      The season being interrogated.
     * @param string  $season_type The type of season being viewed.
     * @param string  $column      The column from the object we want the leaders for.
     * @param array   $opt         Optional additional query configuration.
     * @return self The resulting ORM object containing the stat data
     */
    public static function load(int $season, string $season_type, string $column, array $opt = []) /* : self */
    {
        // Get from the parent.
        $ret = parent::load($season, $season_type, $column, $opt);
        // Post-process any _ML (merged) teams (not applicable when viewing a single team).
        if (isset($opt['team_id'])) {
            // Adjust the league-wide ranks to be team-wide instead.
            $rerank = [];
            foreach ($ret as $row) {
                if (!isset($rerank[$row->stat_pos])) {
                    $rerank[$row->stat_pos] = count($rerank) + 1;
                }
                $row->stat_pos = $rerank[$row->stat_pos];
            }
            // Return.
            return $ret;
        }
        $multi_team = $ret->where('team_id', '_ML');
        if ($multi_team->count()) {
            // Get the raw teams for this/these player(s).
            $rand = $multi_team->random();
            $raw_teams = (new static())->getStatsClass()::query()
                ->select('player_id')
                ->selectRaw('GROUP_CONCAT(team_id) AS team_id')
                ->where([
                    ['season', '=', $rand->season],
                    ['season_type', '=', $rand->season_type],
                ])->whereIn('player_id', $multi_team->unique('player_id'))
                ->where('is_totals', '=', '0') // Not the _ML row.
                ->groupBy('player_id')
                ->get();
            foreach ($raw_teams as $player) {
                $ret->where('player_id', $player->player_id)->team_id = $player->team_id;
            }
        }
        // Return.
        return $ret;
    }

    /**
     * Get the team leader rows for given teams for given stats
     * @param integer $season      The season being interrogated.
     * @param string  $season_type The type of season being viewed.
     * @param array   $teams       The array of team(s) to load.
     * @param array   $cols        The column from the object we want the leaders for.
     * @return self The resulting ORM object containing the leaders
     */
    public static function loadTeamLeaders(int $season, string $season_type, array $teams, array $cols): self
    {
        $where = [
            ['season', '=', $season],
            ['season_type', '=', $season_type],
        ];
        $team_qm = join(', ', array_fill(0, count($teams), '?'));
        $team_empty = array_fill(0, count($teams), 'NO-MATCH');
        // Determine the team leaders per column (not necessarily ranked #1).
        $concats = [];
        $query = static::query()
            ->select('team_id');
        foreach ($cols as $col) {
            $concats[$col] = "CONCAT(LPAD(team_id, 3, '_'), ':', LPAD(`$col`, 4, '0'), ':', LPAD(player_id, 5, '0'))";
            $query->selectRaw("MIN({$concats[$col]}) AS `$col`");
        }
        $leaders_raw = $query->where($where)
            ->whereIn('team_id', $teams)
            ->groupBy('team_id')
            ->get();
        $leaders = [];
        foreach ($cols as $col) {
            $leaders[$col] = ($leaders_raw->unique($col) ?: $team_empty);
        }
        // And return the appropriate rows (with modified rank column).
        $query = static::query()
            ->select('*')
            ->selectRaw('CONCAT(team_id, ":", player_id) AS player_key');
        foreach ($cols as $col) {
            $query->selectRaw("({$concats[$col]} IN ($team_qm)) AS `$col`", $leaders[$col]);
        }
        return $query->where($where)
            ->where(function ($query_where) use ($concats, $leaders) {
                foreach (array_keys($concats) as $i => $col) {
                    $method = (!$i ? 'whereIn' : 'orWhereIn');
                    $query_where->$method(DB::raw($concats[$col]), $leaders[$col]);
                }
            })->get();
    }
}
