<?php

namespace DeBear\ORM\Sports\MajorLeague\MLB;

use DeBear\ORM\Sports\MajorLeague\PlayerGameStat as BasePlayerGameStat;
use DeBear\Helpers\Sports\MLB\Traits\StatsBatting as StatsBattingTrait;

class PlayerGameBatting extends BasePlayerGameStat
{
    use StatsBattingTrait;

    /**
     * Format a Hits per At Bats string
     * @return ?string The appropriate label
     */
    public function getHABAttribute(): ?string
    {
        return "{$this->h}/{$this->ab}";
    }
}
