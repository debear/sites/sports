<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;

class PlayerAward extends Instance
{
    /**
     * Load the player info for each award winning row
     * @return void
     */
    public function loadSecondaryPlayers(): void
    {
        $players = Namespaces::callStatic('Player', 'query')
            ->whereIn('player_id', array_filter($this->unique('player_id')))
            ->get();
        $teams = Namespaces::callStatic('Team', 'query')
            ->whereIn('team_id', array_filter($this->unique('team_id')))
            ->get();
        foreach ($this->data as $i => $row) {
            if (isset($row['player_id'])) {
                $this->data[$i]['player'] = $players->where('player_id', $row['player_id']);
            }
            $this->data[$i]['team'] = $teams->where('team_id', $row['team_id'] ?? 'WONTMATCH');
        }
    }
}
