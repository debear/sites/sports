<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;

class PlayoffRoundRobin extends Instance
{
    /**
     * Load the round robin standings for a given season
     * @param integer $season The season's playoff we are viewing.
     * @return self An ORM object with the appropriate details
     */
    public static function load(int $season): self
    {
        return static::query()
            ->where('season', $season)
            ->orderBy('conf_id')
            ->orderBy('seed')
            ->get();
    }
}
