<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;

class PositionGroups extends Instance
{
    /**
     * Get the relevant details of the position groups and their positions
     * @return self The ORM objects
     */
    public static function load(): self
    {
        return static::query()
            ->orderBy('order')
            ->get()
            ->loadSecondary(['positions']);
    }

    /**
     * Also load the positional info for each position group
     * @return void
     */
    protected function loadSecondaryPositions(): void
    {
        $pos = Namespaces::callStatic('Position', 'query')
            ->whereIn('posgroup_id', $this->unique('posgroup_id'))
            ->get();
        foreach ($this->data as $i => $posgroup) {
            $this->data[$i]['positions'] = $pos->where('posgroup_id', $posgroup['posgroup_id']);
        }
    }
}
