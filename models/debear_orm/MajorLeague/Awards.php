<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\Namespaces;

class Awards extends Instance
{
    /**
     * Get the URL to access the detailed history
     * @return string URL for the history
     */
    public function getLinkAttribute(): string
    {
        return '/' . join('/', array_filter([
            FrameworkConfig::get('debear.section.endpoint'),
            ltrim(FrameworkConfig::get('debear.links.awards.url'), '/'),
            join('-', [$this->name_link, $this->award_id]),
        ]));
    }

    /**
     * Form the award name part of the URL to access the detailed history
     * @return string Awawrd name converted to the URL format
     */
    public function getNameLinkAttribute(): string
    {
        return Strings::codifyURL($this->name);
    }

    /**
     * Return the list of awards, with most recent winners as secondary information
     * @return self The resulting ORM object of awards and recent winners
     */
    public static function loadOverview(): self
    {
        return static::query()
            ->orderBy('disp_order')
            ->get()
            ->loadSecondary(['recent']);
    }

    /**
     * Return the award and history of winners for a single award
     * @param integer $award_id The ID of the award to load.
     * @return self The resulting ORM object of award and historical winners
     */
    public static function loadHistory(int $award_id): self
    {
        $history = static::query()
            ->where('award_id', $award_id)
            ->get();
        return ($history->isset() ? $history->loadSecondary(['winners']) : $history);
    }

    /**
     * Load the most recent winners for each award
     * @return void
     */
    public function loadSecondaryRecent(): void
    {
        // Identify the most recent season.
        $season = Namespaces::callStatic('PlayerAward', 'query')
            ->select('season')
            ->orderByDesc('season')
            ->limit(1)
            ->get()
            ->season;
        // Get the winners for that season.
        $recent = Namespaces::callStatic('PlayerAward', 'query')
            ->where('season', $season)
            ->orderBy('season')
            ->orderBy('award_id')
            ->orderBy('pos')
            ->get()
            ->loadSecondary(['players']);
        // Now tie together.
        foreach ($this->data as $i => $award) {
            $this->data[$i]['season'] = $season;
            $this->data[$i]['recent'] = $recent->where('award_id', $award['award_id']);
        }
    }

    /**
     * Load the historical winners for an award
     * @return void
     */
    public function loadSecondaryWinners(): void
    {
        $all_time = Namespaces::callStatic('PlayerAward', 'query')
            ->where('award_id', $this->unique('award_id'))
            ->orderByDesc('season')
            ->orderBy('pos')
            ->get()
            ->loadSecondary(['players']);
        foreach ($this->data as $i => $award) {
            $this->data[$i]['winners'] = $all_time->where('award_id', $award['award_id']);
        }
    }
}
