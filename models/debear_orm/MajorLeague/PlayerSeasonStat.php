<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Traits\Stats as StatsTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class PlayerSeasonStat extends Instance
{
    use StatsTrait;

    /**
     * Fallback formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        return $this->$column ?? '&ndash;';
    }

    /**
     * Load the player info for the returned stats
     * @return void
     */
    protected function loadSecondaryPlayers(): void
    {
        $players = Namespaces::callStatic('Player', 'query')
            ->whereIn('player_id', $this->unique('player_id'))
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $leader) {
            $this->data[$i]['player'] = $players->where('player_id', $leader['player_id']);
        }
    }

    /**
     * Load the stats data for one or more given teams
     * @param integer $season      The season being interrogated.
     * @param string  $season_type The type of season being viewed.
     * @param array   $ids         The team/players key combo we want stat info loaded for.
     * @return self The resulting ORM object containing the stat data
     */
    public static function loadByTeamPlayers(int $season, string $season_type, array $ids): self
    {
        $query = static::query();
        // Do we need to manipulate columns?
        $stat_columns = (new static())->getSortableStatColumns();
        if (isset($stat_columns)) {
            $query->select(['season', 'season_type', 'player_id', 'team_id']);
            foreach (str_replace('TBL.', '', $stat_columns) as $col => $val) {
                $query->selectRaw("$val AS `$col`");
            }
        }
        // Build the rest of our query.
        $query->where('season', $season)
            ->where('season_type', $season_type)
            ->where(function ($where) use ($ids) {
                foreach ($ids as $id) {
                    list($team_id, $player_id) = explode(':', $id);
                    $where->orWhere(function (Builder $subwhere) use ($player_id, $team_id) {
                        $subwhere->where([
                            ['player_id', '=', $player_id],
                            ['team_id', '=', $team_id],
                        ]);
                    });
                    $where->orWhere(DB::raw(1), DB::raw(0));
                }
            })->groupBy(['season', 'season_type', 'player_id']);
        return $query->get();
    }
}
