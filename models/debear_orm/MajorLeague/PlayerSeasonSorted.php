<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Traits\SortedStats as SortedStatsTrait;

class PlayerSeasonSorted extends Instance
{
    use SortedStatsTrait;

    /**
     * Load the data for our stats table
     * @param integer $season      The season being interrogated.
     * @param string  $season_type The type of season being viewed.
     * @param string  $column      The column from the object we want the leaders for.
     * @param array   $opt         Optional additional query configuration.
     * @return self The resulting ORM object containing the stat data
     */
    public static function load(int $season, string $season_type, string $column, array $opt = []) /* : self */
    {
        $tbl_sort = static::getTable();
        $tbl_stat = static::getStatTable();
        $can_paginate = false;
        // Now build the query.
        $query = (new static())->getStatsClass()::query($tbl_sort);
        // Which stat columns are we loading?
        $calc_columns = (new static())->getSortableStatColumns();
        if (isset($calc_columns)) {
            $query->select([
                "$tbl_stat.season",
                "$tbl_stat.season_type",
                "$tbl_stat.player_id",
            ]);
            $query->selectRaw("GROUP_CONCAT($tbl_stat.team_id) AS team_id");
            foreach (str_replace('TBL.', "$tbl_stat.", $calc_columns) as $col => $val) {
                $query->selectRaw("$val AS `$col`");
            }
            $tbl_join_extra = (isset($opt['team_id']) ? 'team_id' : null);
        } else {
            $query->select("$tbl_stat.*");
            if (isset($opt['team_id'])) {
                $tbl_join_extra = 'team_id';
            } else {
                $raw_totals_sort = static::getWhereIsTotalsClause();
                $tbl_join_extra = $raw_totals_sort[0];
            }
        }
        // If we have any qualifying columns, add them to the output.
        $qual_columns = (new static())->getStatQualified();
        foreach (($qual_columns ?? []) as $col) {
            $query->selectRaw("($tbl_sort.`$col` <= $tbl_sort.`{$col}_all`) AS qual_$col");
        }
        // Main query, including the join to the stats themselves.
        $query->selectRaw("$tbl_sort.`$column` AS stat_pos")
            ->join($tbl_stat, function ($join) use ($tbl_sort, $tbl_stat, $tbl_join_extra) {
                $join->on("$tbl_stat.season", '=', "$tbl_sort.season")
                    ->on("$tbl_stat.season_type", '=', "$tbl_sort.season_type")
                    ->on("$tbl_stat.player_id", '=', "$tbl_sort.player_id");
                if (isset($tbl_join_extra)) {
                    $join->on("$tbl_stat.$tbl_join_extra", '=', "$tbl_sort.$tbl_join_extra");
                }
            })->where([
                ["$tbl_sort.season", '=', $season],
                ["$tbl_sort.season_type", '=', $season_type],
            ]);
        // Specific team?
        if (isset($opt['team_id'])) {
            $team_clause = ["$tbl_sort.team_id", '=', $opt['team_id']];
        } else {
            // No, so get full-season values.
            $team_clause = static::getWhereIsTotalsClause();
            $team_clause[0] = "$tbl_sort.{$team_clause[0]}";
            $can_paginate = true;
        }
        $query->where(...$team_clause);
        // Specific player?
        if (isset($opt['player_id'])) {
            $query->whereIn("$tbl_sort.player_id", (array)$opt['player_id']);
        }
        // Form the part of the query that ensures we only have players with appropriate stats.
        if (isset($calc_columns)) {
            preg_match_all('/(TBL\.`?\w+`?)/', $calc_columns[$column], $stat_cols);
            $stat_cols = join(' IS NOT NULL AND ', array_unique($stat_cols[0])) . ' IS NOT NULL';
            $query->whereRaw('(' . str_replace('TBL.', "$tbl_stat.", $stat_cols) . ')');
        } else {
            $query->whereNotNull("$tbl_stat.$column");
        }
        // If we're customising the columns, we'll need a grouping too.
        if (isset($calc_columns)) {
            $query->groupBy([
                "$tbl_stat.season",
                "$tbl_stat.season_type",
                "$tbl_stat.player_id",
            ]);
        }
        // Order by the column in question.
        $query->orderBy("$tbl_sort.$column")
            ->orderBy("$tbl_sort.player_id");
        // Pagination?
        if (isset($opt['page']) && $can_paginate) {
            // Pages are 1-indexed, OFFSETs are 0-indexed.
            $query->offset(($opt['page'] - 1) * $opt['per_page'])->limit($opt['per_page']);
        }
        $ret = $query->get();
        if ($ret->count() && !isset($opt['player_id'])) {
            $ret->loadSecondary(['players']);
        }
        return $ret;
    }

    /**
     * Get the leaders for a given statistic
     * @param string  $column  The column from the object we want the leaders for.
     * @param integer $max_num The number of players to list.
     * @return self The resulting ORM object containing the leaders
     */
    public static function loadLeaders(string $column, int $max_num): self
    {
        $ret = static::query()
            ->select(['season', 'season_type', 'player_id'])
            ->selectRaw("`$column` AS pos")
            ->selectRaw("'$column' AS stat")
            ->where([
                ['season', '=', FrameworkConfig::get('debear.setup.season.viewing')],
                ['season_type', '=', 'regular'],
                static::getWhereIsTotalsClause(),
                [$column, '<=', $max_num],
            ])->orderBy($column)
            ->get();
        if ($ret->count()) {
            $ret->loadSecondary(['singleStat', 'players']);
        }
        return $ret;
    }

    /**
     * Get the team leader rows for given teams for given stats
     * @param integer $season      The season being interrogated.
     * @param string  $season_type The type of season being viewed.
     * @param array   $teams       The array of team(s) to load.
     * @param array   $cols        The column from the object we want the leaders for.
     * @return self The resulting ORM object containing the leaders
     */
    public static function loadTeamLeaders(int $season, string $season_type, array $teams, array $cols): self
    {
        return static::query()
            ->select('*')
            ->selectRaw('CONCAT(team_id, ":", player_id) AS player_key')
            ->where([
                ['season', '=', $season],
                ['season_type', '=', $season_type],
            ])
            ->whereIn('team_id', $teams)
            ->where(function (Builder $query) use ($cols) {
                foreach ($cols as $col) {
                    $query->orWhere($col, 1);
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
    }

    /**
     * Load the full stat info for the returned sorted rows
     * @return void
     * /
    protected function loadSecondaryStats(): void
    {
        $random = $this->random();
        $where = [
            ['season', '=', $random->season],
            ['season_type', '=', $random->season_type],
        ];
        // Process the subtleties of the model.
        $is_totals_sort = static::getWhereIsTotalsClause();
        if ($is_totals_sort[0] != 'team_id') {
            // We have merged rows available.
            $where[] = static::getWhereIsTotalsClause();
        }
        // And now load the stats.
        $stats = $this->getStatsClass()::query()
            ->select('*')
            ->selectRaw('GROUP_CONCAT(team_id) AS team_id')
            ->where($where)
            ->whereIn('player_id', $this->unique('player_id'))
            ->groupBy('season')
            ->groupBy('season_type')
            ->groupBy('player_id')
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $row) {
            $this->data[$i]['stats'] = $stats->where('player_id', $row['player_id']);
            $this->data[$i]['team_id'] = $this->data[$i]['stats']->team_id;
        }
    }
    /* */

    /**
     * Load the stat info for the returned stats
     * @return void
     */
    protected function loadSecondarySingleStat(): void
    {
        $random = $this->random();
        $where = [
            ['season', '=', $random->season],
            ['season_type', '=', $random->season_type],
        ];
        $stat_columns = $this->getSortableStatColumns();
        // Process the subtleties of the model.
        $is_totals_sort = static::getWhereIsTotalsClause();
        if ($is_totals_sort[0] == 'team_id') {
            // We are merging rows.
            $stat_col = str_replace('TBL.', '', $stat_columns[$random->stat]);
        } else {
            // We have merged rows available.
            $where[] = static::getWhereIsTotalsClause();
            $stat_col = $random->stat;
        }
        // And now load the stats.
        $stats = $this->getStatsClass()::query()
            ->select('player_id')
            ->selectRaw('GROUP_CONCAT(team_id) AS team_id')
            ->selectRaw("$stat_col AS `{$random->stat}`")
            ->where($where)
            ->whereIn('player_id', $this->unique('player_id'))
            ->groupBy('season')
            ->groupBy('season_type')
            ->groupBy('player_id')
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $leader) {
            $player = $stats->where('player_id', $leader['player_id']);
            $this->data[$i]['stat_value'] = $player->format($random->stat);
            $this->data[$i]['team_id'] = $player->team_id;
        }
    }

    /**
     * Load the player info for the returned stats
     * @return void
     */
    protected function loadSecondaryPlayers(): void
    {
        $players = Namespaces::callStatic('Player', 'query')
            ->whereIn('player_id', $this->unique('player_id'))
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $leader) {
            $this->data[$i]['player'] = $players->where('player_id', $leader['player_id']);
        }
    }
}
