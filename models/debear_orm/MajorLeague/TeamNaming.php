<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class TeamNaming extends Instance
{
    /**
     * Return the CSS used to highlight the row in the team's specific CSS colours
     * @return string The applicable CSS attribute
     */
    public function rowCSS(): string
    {
        return 'row_' . FrameworkConfig::get('debear.section.code') . '-' . ($this->alt_team_id ?? $this->team_id);
    }

    /**
     * Return the CSS used to render a sized version of the team's (current) logo
     * @param string $size The requested logo size (Default: tiny).
     * @return string The applicable CSS attribute
     */
    public function logoCSS(string $size = 'tiny'): string
    {
        return Team::buildLogoCSS($this->alt_team_id ?? $this->team_id, $size);
    }
}
