<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\MajorLeague\Traits\Stats as StatsTrait;

class PlayerCareerSplits extends Instance
{
    use StatsTrait;
}
