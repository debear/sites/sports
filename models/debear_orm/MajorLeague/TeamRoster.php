<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Namespaces;

class TeamRoster extends Instance
{
    /**
     * Load the player info for the returned rosters
     * @return void
     */
    protected function loadSecondaryPlayers(): void
    {
        $players = Namespaces::callStatic('Player', 'query')
            ->whereIn('player_id', $this->unique('player_id'))
            ->get();
        // Now tie together.
        foreach ($this->data as $i => $roster) {
            $this->data[$i]['player'] = $players->where('player_id', $roster['player_id']);
        }
    }

    /**
     * Load the team info for the returned roster rows
     * @return void
     */
    public function loadSecondaryTeams(): void
    {
        $teams = Namespaces::callStatic('Team', 'query')
            ->whereIn('team_id', $this->unique('team_id'))
            ->get();
        // Now tie-together.
        foreach ($this->data as $i => $roster) {
            $this->data[$i]['team'] = $teams->where('team_id', $roster['team_id']);
        }
    }
}
