<?php

namespace DeBear\ORM\Sports\MajorLeague;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use Carbon\Carbon;
use Illuminate\Http\Response;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Traits\ScheduleDate as ScheduleDateTrait;
use DeBear\ORM\Skeleton\WeatherForecast;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;

class Schedule extends Instance
{
    use ScheduleDateTrait;

    /**
     * These secondaries need to be loaded when a query is run
     * @var array
     */
    protected $requiredSecondaries = ['teams'];

    /**
     * Determine where the game was played
     * @return string The name of the Arena/Stadium the game was played
     */
    public function getVenueAttribute(): string
    {
        return $this->alt_venue ?? $this->home->venue;
    }

    /**
     * Format the game_date field for a summary date
     * @return string The game_date column formatted for 'short' display
     */
    public function getDateShortAttribute(): string
    {
        return $this->game_date->format('D jS M');
    }

    /**
     * Format the game_date field for the full date
     * @return string The game_date column formatted for 'full' display
     */
    public function getDateFullAttribute(): string
    {
        return $this->game_date->format('l jS F');
    }

    /**
     * Format the game_time field for display
     * @return string The game_time column formatted for display
     */
    public function getStartTimeAttribute(): string
    {
        $dt = new Carbon("{$this->game_date} {$this->game_time}", FrameworkConfig::get('debear.datetime.timezone_db'));
        return $dt->setTimezone(Time::object()->getUserTimezone())->format('g:ia T');
    }

    /**
     * Build a full version of the game's status for display
     * @return string The full display version of the game's status
     */
    public function getStatusFullAttribute(): string
    {
        // Main info.
        if ($this->status == 'PPD') {
            $ret = 'Postponed';
        } elseif ($this->status == 'SSP') {
            $ret = 'Suspended';
        } elseif ($this->status == 'CNC') {
            $ret = 'Cancelled';
        } elseif ($this->status == 'F') {
            $ret = 'Final';
        } else {
            $ret = "Final/{$this->status}";
        }
        // Return, appending any secondary info.
        return $ret . (isset($this->status_info) ? " ({$this->status_info})" : '');
    }

    /**
     * Build a short version of the game's status for display
     * @return string The display version of the game's status
     */
    public function getStatusDispAttribute(): string
    {
        if (preg_match('/^(PPD|SSP|CNC)$/', $this->status)) {
            return $this->status;
        } elseif ($this->status == 'F') {
            return '';
        }
        return "F/{$this->status}";
    }

    /**
     * State if this game is special, why so
     * @return string A summary title
     */
    public function getTitleAttribute(): string
    {
        $title = $this->series_title;
        if ($title) {
            // Add on the Game number.
            $game_num = substr(str_pad($this->game_id, 3, '0', STR_PAD_LEFT), -1);
            $title .= ", Game $game_num";
        }
        return $title ?? '';
    }

    /**
     * Abbreviate the game title in to a shorter version
     * @return string The abbreviated game title
     */
    public function getTitleShortAttribute(): string
    {
        $full = $this->title ?? ''; // Static analysis satisfaction.
        if (!$full) {
            return $full;
        }
        // Strip out lower characters, but preserve any "Game" aspect.
        return str_replace('@@', ' Gm ', preg_replace('/[^A-Z0-9@]/', '', str_replace(', Game', '@@', $full)));
    }

    /**
     * Summarise the playoff series
     * @return string Series summary
     */
    public function getSeriesSummaryAttribute(): string
    {
        // If no object, no summary (e.g., Round Robin games).
        if (!isset($this->series) || !$this->series->isset()) {
            return '';
        }
        return $this->series->series_summary;
    }

    /**
     * Determine (as an abbreviation) the result for the team
     * @return string The result of the game for the given team
     */
    public function resultCode(): string
    {
        $team_score = "{$this->team_type}_score";
        $opp_score = "{$this->team_opp}_score";
        if ($this->$team_score > $this->$opp_score) {
            // Win.
            return 'W';
        } elseif ($this->$team_score == $this->$opp_score) {
            // Tie.
            return 'T';
        }
        // All other results were losses.
        return 'L';
    }

    /**
     * Determine (as word/s) the result for the team
     * @return string The result of the game for the given team
     */
    public function result(): string
    {
        $code = $this->resultCode();
        if ($code == 'W') {
            $res = 'Win';
        } elseif ($code == 'L') {
            $res = 'Loss';
        } elseif ($code == 'T') {
            $res = 'Tie';
        }
        return $res ?? '';
    }

    /**
     * Return the appropriate CSS class for the result
     * @return string The relevant CSS class
     */
    public function resultCSS(): string
    {
        $res = strtolower($this->result());
        if (in_array($res, ['win', 'tie'])) {
            return "result-{$res}";
        }
        // All the remaining permutations are forms of losses.
        return 'result-loss';
    }

    /**
     * Form the final score, with the winner always listed first
     * @return string The game's final score
     */
    public function resultScore(): string
    {
        return ($this->home_score > $this->visitor_score
            ? "{$this->home_score} &ndash; {$this->visitor_score}"
            : "{$this->visitor_score} &ndash; {$this->home_score}");
    }

    /**
     * Whether the game is a regular season game
     * @return boolean That the game is a regular season game
     */
    public function isRegular(): bool
    {
        return ($this->game_type == 'regular');
    }

    /**
     * Whether the game is a playoff game
     * @return boolean That the game is playoff
     */
    public function isPlayoff(): bool
    {
        return ($this->game_type == 'playoff');
    }

    /**
     * Whether the game is still in 'preview' mode (pre-game)
     * @return boolean That the game has yet to be played
     */
    public function isPreview(): bool
    {
        return !isset($this->status);
    }

    /**
     * Whether the game was completed
     * @return boolean That the game has been played and completed
     */
    public function isComplete(): bool
    {
        return isset($this->status) && !preg_match('/^(PPD|SSP|CNC)$/', $this->status);
    }

    /**
     * Whether the game was not completed and will not be
     * @return boolean That the game has passed but was not completed
     */
    public function isAbandoned(): bool
    {
        return !$this->isPreview() && !$this->isComplete();
    }

    /**
     * Apply the player links to the game content
     * @param Response $response The built response to manipulate.
     * @return void
     */
    public function applyPlayerLinks(Response $response): void
    {
        // Get the raw content from the response.
        $content = $response->getContent();
        // Loop through the players and replace play-by-play references.
        $search = $replace = [];
        foreach (['home', 'visitor'] as $team) {
            foreach ($this->$team->lineup as $lineup) {
                $player_link = '<a href="' . $lineup->player->link . '">' . $lineup->player->name . '</a>';
                // By jersey.
                $search[] = '{{' . $lineup->team_id . '_' . $lineup->jersey . '}}';
                $replace[] = $player_link;
                // By player ID.
                $search[] = '{{PLAYER_' . $lineup->player_id . '}}';
                $replace[] = $player_link;
            }
            $search[] = '{{TEAM_' . $this->$team->team_id . '}}';
            $replace[] = '<a href="' . $this->$team->link . '">' . $this->$team->name . '</a>';
        }
        $content = str_replace($search, $replace, $content);
        // Finally a catch-all for unknown instances.
        $content = preg_replace('({{[A-Z]{2,3}_\d+}})', 'Unknown Player', $content);
        // Now re-apply the modified content.
        $response->setContent($content);
    }

    /**
     * Get the URL to access this game
     * @return string URL for the game
     */
    public function getLinkAttribute(): string
    {
        return '/' . join('/', [
            FrameworkConfig::get('debear.section.endpoint'),
            ltrim(FrameworkConfig::get('debear.links.schedule.url'), '/'),
            $this->linkDate(),
            join('-', [
                Strings::codify($this->visitor->franchise),
                'at',
                Strings::codify($this->home->franchise),
                substr($this->game_type, 0, 1) . $this->game_id,
            ]),
        ]);
    }

    /**
     * Load the game details for an individual game
     * @param integer $season    The season the requested game was played in.
     * @param string  $game_type The game_type of the game being loaded.
     * @param string  $game_id   The ID of the game.
     * @return self The resulting ORM object containing the base game info for the requested game
     */
    public static function load(int $season, string $game_type, string $game_id): self
    {
        return static::query()
            ->where([
                ['season', '=', $season],
                ['game_type', '=', $game_type],
                ['game_id', '=', $game_id],
            ])->get();
    }

    /**
     * Load the schedule for a given ScheduleDate game date
     * @param ScheduleDate $game_date The game date ORM object for which the schedule info is requested.
     * @param array        $opt       Processing options.
     * @return self The resulting ORM object containing all game for the game date being passed in
     */
    public static function loadByDate(ScheduleDate $game_date, array $opt = []): self
    {
        // Start the query.
        $query = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS weather_key')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS odds_key')
            ->where([
                ['season', '=', $game_date->season],
                ['game_date', '=', $game_date->date],
            ]);
        // Any favourite teams to prioritise?
        $is_logged_in = (User::object() !== null) && User::object()->isLoggedIn();
        if ($is_logged_in) {
            // Get the list of teams (if any).
            $favourites = UserFavourite::query()
                ->select('char_id')
                ->where([
                    ['sport', '=', FrameworkConfig::get('debear.section.code')],
                    ['user_id', '=', User::object()->id],
                    ['type', '=', 'team'],
                ])->get();
            // Add to the query?
            if ($favourites->count()) {
                $teams = $favourites->unique('char_id');
                $q = join(', ', array_fill(0, $favourites->count(), '?'));
                $query->selectRaw("home IN ($q) OR visitor IN ($q) AS is_favourite", array_merge($teams, $teams));
                $query->orderByRaw('is_favourite DESC');
            }
        } else {
            // Ensure we have the is_favourite column for filtering.
            $query->selectRaw('0 AS is_favourite');
        }
        // Moving completed games at the end?
        if (isset($opt['order-played-last']) && $opt['order-played-last']) {
            $query->orderByRaw('status IS NOT NULL')
                ->orderByRaw('status IN ("PPD", "CNC", "SSP")');
        }
        // Standard game order.
        $ret = $query->orderBy('game_date')
            ->orderBy('game_time')
            ->orderBy('visitor')
            ->get();
        $method = (isset($opt['expanded']) && $opt['expanded'] ? 'loadSummarySecondaries' : 'loadBasicSecondaries');
        return $ret->$method();
    }

    /**
     * Build the ORM query arguments for the games a Schedule query returned.
     * @param string  $table     An optional table to bind the clause to.
     * @param boolean $force_all Whether all games, not just completed games, should be parsed.
     * @return array The Query Builder-compatible array
     */
    protected function queryGames(string $table = '', bool $force_all = false): array
    {
        // If we've been passed a table to bind the clause to, append the separator.
        if ($table) {
            $table .= '.';
        }
        // Now build.
        $query_games = [];
        foreach ($this->data as $game) {
            if ($force_all || (isset($game['status']) && !in_array($game['status'], ['PPD', 'SSP', 'CNC']))) {
                $query_games[] = [
                    ["{$table}season", '=', $game['season']],
                    ["{$table}game_type", '=', $game['game_type']],
                    ["{$table}game_id", '=', $game['game_id']],
                ];
            }
        }
        // Ensure the query runs properly (but fails fast) if no games apply.
        if (!count($query_games)) {
            $query_games[] = [[DB::raw('1'), '=', DB::raw('0')]];
        }
        return $query_games;
    }

    /**
     * Check if the given season has completed the Regular Season part of the schedule
     * @param integer $season The season to be checked.
     * @return boolean That the given season has completed its Regular Season
     */
    public static function isRegularSeasonComplete(int $season): bool
    {
        $num_regular_games = static::query()
            ->where([
                ['season', '=', $season],
                ['game_type', '=', 'regular'],
            ])->whereNull('status')
            ->count();
        return ($num_regular_games > 0);
    }

    /**
     * Load the schedule date info for the games in question
     * @return void
     */
    public function loadSecondaryScheduleDate(): void
    {
        $schedule_dates = Namespaces::callStatic('ScheduleDate', 'query')
            ->whereIn('date', $this->unique('game_date'))
            ->get();
        // Now tie back.
        foreach ($this->data as $i => $game) {
            $this->data[$i]['scheduleDate'] = $schedule_dates->where('date', $game['game_date']);
        }
    }

    /**
     * Load expanded information about the teams involved
     * @return void
     */
    public function loadSecondaryTeams(): void
    {
        $team_ids = array_unique(array_merge($this->unique('home'), $this->unique('visitor')));
        // Define our team/season mappings.
        $team_seasons = [];
        foreach ($this->data as $game) {
            $team_seasons["{$game['home']}:{$game['season']}"] = $game['season'];
            $team_seasons["{$game['visitor']}:{$game['season']}"] = $game['season'];
        }
        $seasons = array_values(array_unique($team_seasons));
        // Do we need to narrow down the season?
        $load_args = [$team_ids];
        if (count($seasons) > 1 || $seasons[0] != FrameworkConfig::get('debear.setup.season.viewing')) {
            $load_args[1] = []; // No additional secondary info required.
            $wheres = [];
            foreach (array_unique(array_keys($team_seasons)) as $team_season) {
                list($team_id, $season) = explode(':', $team_season);
                $wheres[] = "WHEN '$team_id' THEN $season";
            }
            $load_args[2] = ['season' => 'CASE TBL.team_id ' . join(' ', $wheres) . ' END'];
        }
        // Now get the team details.
        $this->secondary['teams'] = Namespaces::callStatic('Team', 'load', $load_args);
        foreach ($this->data as $i => $game) {
            // Home.
            $this->data[$i]['home_id'] = $game['home'];
            $this->data[$i]['home'] = $this->secondary['teams']->where('team_id', $game['home']);
            // Visitor.
            $this->data[$i]['visitor_id'] = $game['visitor'];
            $this->data[$i]['visitor'] = $this->secondary['teams']->where('team_id', $game['visitor']);
        }
    }

    /**
     * Load alternative venue information for the scheduled games
     * @return void
     */
    public function loadSecondaryAltVenues(): void
    {
        $venue_ids = array_filter($this->unique('alt_venue'));
        $venues = Namespaces::callStatic('TeamVenue', 'query')
            ->where('team_id', '_ALT')
            ->whereIn('building_id', $venue_ids)
            ->get();
        $alt_venues = array_combine($venues->pluck('building_id'), $venues->pluck('stadium'));
        // Now replace the ID with the name.
        foreach ($this->data as $i => $game) {
            if (isset($game['alt_venue'])) {
                $this->data[$i]['alt_venue_model'] = $venues->where('building_id', $game['alt_venue']);
                $this->data[$i]['alt_venue'] = $alt_venues[$game['alt_venue']];
            }
        }
    }

    /**
     * Load the weather information for the scheduled games
     * @return void
     */
    public function loadSecondaryWeather(): void
    {
        $weather = WeatherForecast::query()
            ->where('app', FrameworkConfig::get('debear.weather.app'))
            ->whereIn('instance_key', $this->pluck('weather_key'))
            ->get();
        // Now tie back.
        foreach ($this->data as $i => $game) {
            $this->data[$i]['weather'] = $weather->where('instance_key', $game['weather_key']);
        }
    }

    /**
     * Load the game odds for the scheduled games
     * @return void
     */
    public function loadSecondaryOdds(): void
    {
        $odds = GameOdds::query()
            ->select('*')
            ->selectRaw('replace(over_under, ".0", "") AS over_under')
            ->selectRaw('CONCAT(season, "-", game_type, "-", game_id) AS odds_key')
            ->where('sport', FrameworkConfig::get('debear.section.code'))
            ->whereIn(DB::raw('CONCAT(season, "-", game_type, "-", game_id)'), $this->pluck('odds_key'))
            ->get();
        // Now tie back.
        foreach ($this->data as $i => $game) {
            $this->data[$i]['odds'] = $odds->where('odds_key', $game['odds_key']);
        }
    }

    /**
     * Load the team standings at the point these games where played
     * @param string $game_col  The name of the date column in the schedule table.
     * @param string $stand_col The name of the date column in the standings table.
     * @return void
     */
    public function loadSecondaryStandings(string $game_col = 'game_date', string $stand_col = 'the_date'): void
    {
        $standings = [];
        $season = $this->season;
        $reg_games = $this->where('game_type', 'regular');
        $dates = $reg_games->unique($game_col);
        foreach ($dates as $date) {
            // Determine the appropriate standings date for this game date.
            $standing_date = Namespaces::callStatic('Standings', 'query')
                ->selectRaw("MAX($stand_col) AS date")
                ->where('season', $season)
                ->where($stand_col, '<=', $date)
                ->get();
            // And now the appropriate standings.
            $date_games = $reg_games->where($game_col, $date);
            $team_ids = array_unique(array_merge($date_games->unique('home_id'), $date_games->unique('visitor_id')));
            $date_standings = Namespaces::callStatic('Standings', 'query')
                ->where('season', $season)
                ->where($stand_col, $standing_date->date)
                ->whereIn('team_id', $team_ids)
                ->groupBy('team_id')
                ->get();
            foreach ($date_standings->unique('team_id') as $team_id) {
                $standings["$team_id-$date"] = $date_standings->where('team_id', $team_id);
            }
        }
        // Now tie back.
        foreach ($this->data as $i => $game) {
            // Home.
            $ref = "{$game['home_id']}-{$game[$game_col]}";
            if (isset($standings[$ref])) {
                $this->data[$i]['home']->standings = $standings[$ref];
            }
            // Visitor.
            $ref = "{$game['visitor_id']}-{$game[$game_col]}";
            if (isset($standings[$ref])) {
                $this->data[$i]['visitor']->standings = $standings[$ref];
            }
        }
    }
    /**
     * Load the game stats for the players involved
     * @return void
     */
    public function loadSecondaryPlayerStats(): void
    {
        $query_games = $this->queryGames();
        $stats_list = $this->loadSecondaryPlayerStatsList();
        // Get the stats.
        $stats = [];
        foreach ($stats_list as $key => $stat_config) {
            if (!is_array($stat_config)) {
                $stat_config = [
                    'class' => $stat_config,
                    'orderBy' => null,
                ];
            }
            $query = $stat_config['class']::query()
                ->select('*')
                ->selectRaw('CONCAT(season, "-", game_type, "-", game_id, "-", team_id) AS team_ref')
                ->where(function ($query) use ($query_games) {
                    foreach ($query_games as $game_where) {
                        $query->orWhere(function (Builder $where) use ($game_where) {
                            $where->where($game_where);
                        });
                    }
                    $query->orWhere(DB::raw(1), DB::raw(0));
                });
            if (isset($stat_config['orderBy']) && is_array($stat_config['orderBy'])) {
                foreach ($stat_config['orderBy'] as $order_by) {
                    $query->orderByDesc($order_by);
                }
            }
            $stats[$key] = $query->get();
        }
        // Link the players (found previously) to the stat rows.
        foreach (array_keys($stats) as $key) {
            foreach ($stats[$key] as $line) {
                $line->player = $this->secondary['players']->where('player_id', $line->player_id);
                $line->name_sort = $line->player->name_sort;
            }
        }
        // Now tie together.
        foreach ($this->data as $i => $game) {
            foreach (['home', 'visitor'] as $team) {
                $ref = "{$game['season']}-{$game['game_type']}-{$game['game_id']}-{$game[$team]->team_id}";
                foreach ($stats as $key => $list) {
                    $this->data[$i][$team]->$key = $list->where('team_ref', $ref);
                }
            }
        }
    }

    /**
     * Load the season matchup details
     * @return void
     */
    protected function loadSecondaryMatchups(): void
    {
        $this->matchupsRegular();
        $this->matchupsPlayoff();
    }

    /**
     * Worker method for determining regular season matchup details
     * @return void
     */
    protected function matchupsRegular(): void
    {
        $tbl_s = Namespaces::callStatic('Schedule', 'getTable');
        $tbl_m = Namespaces::callStatic('ScheduleMatchups', 'getTable');
        // Build our query team pairs.
        $lookups = [];
        foreach ($this->data as $i => $game) {
            if ($game['game_type'] == 'regular') {
                $lookups[] = [
                    'index' => $i,
                    'query' => [
                        "$tbl_m.season" => $game['season'],
                        "$tbl_m.game_type" => $game['game_type'],
                        "$tbl_m.team_idA" => min($game['home_id'], $game['visitor_id']),
                        "$tbl_m.team_idB" => max($game['home_id'], $game['visitor_id']),
                    ],
                ];
            }
        }
        if (!$lookups) {
            // Skip if this object has no regular season games to process.
            return;
        }
        // Perform the lookup.
        $query = Namespaces::callStatic('Schedule', 'query')
            ->select("$tbl_s.*")
            ->selectRaw("CONCAT($tbl_m.team_idA, '-', $tbl_m.team_idB, '-', $tbl_m.game_type) AS sched_ref")
            ->join($tbl_m, function ($join) use ($tbl_m, $tbl_s) {
                $join->on("$tbl_s.season", '=', "$tbl_m.season")
                    ->on("$tbl_s.game_type", '=', "$tbl_m.game_type")
                    ->on("$tbl_s.game_id", '=', "$tbl_m.game_id");
            });
        foreach (array_values(array_unique($lookups)) as $lookup) {
            $query->orWhere(function (Builder $where) use ($lookup) {
                $where->where($lookup['query']);
            });
        }
        $schedule = $query->orWhere(DB::raw(1), DB::raw(0))
            ->orderBy("$tbl_s.game_date")
            ->orderBy("$tbl_s.game_time")
            ->get();
        // Now tie together, updating only the regular season games we've just processed.
        foreach ($lookups as $lookup) {
            $game = $this->data[$lookup['index']];
            $team_idA = min($game['home_id'], $game['visitor_id']);
            $team_idB = max($game['home_id'], $game['visitor_id']);
            $sched_ref = "$team_idA-$team_idB-{$game['game_type']}";
            $this->data[$lookup['index']]['matchups'] = $schedule->where('sched_ref', $sched_ref);
        }
    }

    /**
     * Worker method for determining playoff matchup details
     * @return void
     */
    protected function matchupsPlayoff(): void
    {
        // Build our query ID lookup.
        $lookups = [];
        foreach ($this->data as $i => $game) {
            if ($game['game_type'] == 'playoff') {
                $lookups[] = [
                    'index' => $i,
                    'query' => [
                        'season' => $game['season'],
                        'game_type' => $game['game_type'],
                        'round_code' => substr(str_pad($game['game_id'], 3, '0', STR_PAD_LEFT), 0, 2),
                    ],
                ];
            }
        }
        if (!$lookups) {
            // Skip if this object has no playoff games to process.
            return;
        }
        // Perform the lookup.
        $query = Namespaces::callStatic('Schedule', 'query')
            ->select("*")
            ->selectRaw('SUBSTRING(LPAD(game_id, 3, "0"), 1, 2) AS round_code');
        foreach (array_values(array_unique($lookups)) as $i => $lookup) {
            $query->orWhere(function (Builder $where) use ($lookup) {
                $clause = $lookup['query'];
                $where->where('season', '=', $clause['season'])
                    ->where('game_type', '=', $clause['game_type'])
                    ->whereBetween('game_id', ["{$clause['round_code']}0", "{$clause['round_code']}9"]);
            });
        }
        $schedule = $query->orWhere(DB::raw(1), DB::raw(0))
            ->orderBy('game_date')
            ->orderBy('game_time')
            ->get()
            ->loadSecondary(['series']);
        // Now tie together, updating only the playoff games we've just processed.
        foreach ($lookups as $lookup) {
            $game = $this->data[$lookup['index']];
            $this->data[$lookup['index']]['matchups'] = $schedule->where('round_code', $lookup['query']['round_code']);
        }
    }

    /**
     * Load the playoff series details
     * @return void
     */
    public function loadSecondarySeries(): void
    {
        $tbl_series = Namespaces::callStatic('PlayoffSeries', 'getTable');
        $tbl_seeds = Namespaces::callStatic('PlayoffSeeds', 'getTable');
        // Determine the relevant series dates.
        $max_dates = Namespaces::callStatic('PlayoffSeries', 'query')
            ->select(['season', 'round_code'])
            ->selectRaw('MAX(the_date) AS the_date')
            ->where(function ($query) {
                foreach ($this->data as $game) {
                    $round_code = substr(str_pad($game['game_id'], 3, '0', STR_PAD_LEFT), 0, -1);
                    $where_clause = [
                        ['season', '=', $game['season']],
                        ['round_code', '=', (int)$round_code],
                        ['the_date', '<=', $game['game_date']],
                    ];
                    $query->orWhere(function (Builder $where) use ($where_clause) {
                        $where->where($where_clause);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->groupBy('round_code')
            ->get();
        $query_dates = [];
        foreach ($max_dates as $date) {
            $query_dates[] = [
                ["$tbl_series.season", '=', $date->season],
                ["$tbl_series.the_date", '=', $date->the_date->toDateString()],
                ["$tbl_series.round_code", '=', $date->round_code],
            ];
        }
        // Ensure the query runs properly (but fails fast) if no dates apply.
        if (!count($query_dates)) {
            $query_dates[] = [[DB::raw('1'), '=', DB::raw('0')]];
        }
        // Then get the series.
        $series = Namespaces::callStatic('PlayoffSeries', 'query')
            ->join("$tbl_seeds AS seed_high", function ($join) use ($tbl_series) {
                $join->on('seed_high.season', '=', "$tbl_series.season")
                    ->on('seed_high.conf_id', '=', "$tbl_series.higher_conf_id")
                    ->on('seed_high.seed', '=', "$tbl_series.higher_seed");
            })->join("$tbl_seeds AS seed_low", function ($join) use ($tbl_series) {
                $join->on('seed_low.season', '=', "$tbl_series.season")
                    ->on('seed_low.conf_id', '=', "$tbl_series.lower_conf_id")
                    ->on('seed_low.seed', '=', "$tbl_series.lower_seed");
            })->select(["$tbl_series.*"])
            ->selectRaw("CONCAT($tbl_series.higher_conf_id, ':', $tbl_series.higher_seed) AS higher_seed_ref")
            ->selectRaw("CONCAT($tbl_series.lower_conf_id, ':', $tbl_series.lower_seed) AS lower_seed_ref")
            ->selectRaw('seed_high.team_id AS higher_team_id')
            ->selectRaw('seed_low.team_id AS lower_team_id')
            ->selectRaw("CONCAT($tbl_series.season, '-', $tbl_series.round_code) AS ref")
            ->where(function ($query) use ($query_dates) {
                foreach ($query_dates as $game_where) {
                    $query->orWhere(function (Builder $where) use ($game_where) {
                        $where->where($game_where);
                    });
                }
                $query->orWhere(DB::raw(1), DB::raw(0));
            })->get();
        // Add the teams from our earlier query.
        foreach ($series as $matchup) {
            $matchup->higher = $this->secondary['teams']->where('team_id', $matchup->higher_team_id);
            $matchup->lower = $this->secondary['teams']->where('team_id', $matchup->lower_team_id);
        }
        // Now bind to the schedule.
        foreach ($this->data as $i => $game) {
            $ref = "{$game['season']}-" . substr($game['game_id'], 0, -1);
            $this->data[$i]['series'] = $series->where('ref', $ref);
        }
    }

    /**
     * Get the list of games available in the database
     * @param integer $season The season to be loaded.
     * @return self An ORM model containing all the (usable) games
     */
    public static function getAllSitemap(int $season): self
    {
        return static::query()
            ->where('season', $season)
            ->whereNotNull('status')
            ->whereNotIn('status', ['PPD', 'CNC', 'SSP'])
            ->orderBy('game_date')
            ->orderBy('game_time')
            ->orderBy('game_id')
            ->get();
    }

    /**
     * Build the list of events to display on the global homepage
     * @param array  $date_maps An array of dates we are focusing on.
     * @param string $sport     The sport we are procssing.
     * @return array The appropriate events to focus on
     */
    public static function getHomepageEvents(array $date_maps, string $sport): array
    {
        // Some config jiggery-pokery.
        $orig = FrameworkConfig::get('debear.setup.season');
        FrameworkConfig::set(['debear.setup.season' => FrameworkConfig::get("debear.subsites.$sport.setup.season")]);

        // Load the games on these days.
        $data = static::query()
            ->select('*')
            ->selectRaw('"major_league" AS template')
            ->selectRaw('? AS template_icon', [$sport])
            ->whereIn('game_date', array_keys($date_maps))
            ->orderByDesc(DB::raw('status = "F"'))
            ->orderBy('game_date')
            ->orderBy('game_time')
            ->get();
        if ($sport != 'nfl') {
            $data->loadSecondary(['series']);
        }

        // Revert our config tweak.
        FrameworkConfig::set(['debear.setup.season' => $orig]);

        // Build our return object.
        $ret = array_fill_keys(array_values($date_maps), []);
        foreach ($date_maps as $d => $c) {
            $games = $data->where('game_date', $d);
            if ($games->count()) {
                $ret[$c] = $games;
            }
        }
        return $ret;
    }
}
