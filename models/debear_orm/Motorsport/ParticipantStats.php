<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Motorsport\Traits\Stats as TraitStats;

class ParticipantStats extends Instance
{
    use TraitStats;
}
