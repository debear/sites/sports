<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\Highcharts;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\Motorsport\Header;
use DeBear\Helpers\Sports\Motorsport\Traits\Entity as TraitEntity;

class Team extends Instance
{
    use TraitEntity;

    /**
     * Logo filesystem folder
     * @var string
     */
    protected static $image_dir = 'teams';
    /**
     * Cached list of championship winning seasons
     * @var array
     */
    protected $champs = [];

    /**
     * Format the team's best-known name
     * @return string The team's name formatted for display
     */
    public function getNameAttribute(): string
    {
        return $this->team_name;
    }

    /**
     * Format the team's best-known name in a codified version (e.g., for links or files)
     * @return string The team's name standardised and run through the codified
     */
    public function getNameCodifiedAttribute(): string
    {
        return Strings::codifyURL($this->name);
    }

    /**
     * Get the listed base for the team
     * @return string The team's base as a formatted string
     */
    public function getBaseLocationAttribute(): string
    {
        // Don't display the flag if it's the same as the registration country.
        if ($this->base_country == $this->team_country) {
            return $this->base;
        }
        return '<span class="flag_right flag16_right_' . $this->base_country . '">' . $this->base . '</span>';
    }

    /**
     * Determine the link for this team
     * @return string The appropriate URL
     */
    public function link(): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . "/teams/{$this->name_codified}/{$this->season}";
    }

    /**
     * Format the participant's name with a link and flag
     * @param boolean $inc_link Whether we include the link portion.
     * @return string The participant's formatted details
     */
    public function nameLinkFlag(bool $inc_link = true): string
    {
        $flag = ($this->team_country ?: false);
        $link = ($inc_link && ($this->season >= FrameworkConfig::get('debear.setup.season.min'))
            ? $this->link() : false);
        return ($flag ? "<span class=\"flag_right flag16_right_$flag\">" : '')
            . ($link ? "<a href=\"$link\" " . $this->hoverAttributes() . '>' : '')
            . $this->name
            . ($link ? '</a>' : '')
            . ($flag ? '</span>' : '');
    }

    /**
     * Format the participant's name with a flag (no link)
     * @return string The participant's formatted details
     */
    public function nameFlag(): string
    {
        return $this->nameLinkFlag(false);
    }

    /**
     * The championships won by this team
     * @return array List of seasons in which the team won the championship
     */
    public function championships(): array
    {
        // Cached.
        if (isset($this->champs[$this->team_id])) {
            return $this->champs[$this->team_id];
        }

        // Build.
        $champs = array_flip($this->history
            ->where('season', '<=', FrameworkConfig::get('debear.setup.season.viewing'))
            ->where('pos', '=', 1)
            ->pluck('season'));
        // Annoying edge-case - the current season leader isn't necessarily the World Champion!
        $curr_season = FrameworkConfig::get('debear.setup.season.max');
        if (isset($champs[$curr_season])) {
            // If the last race wasn't completed, then they are merely a leader and not a champion...
            if (!Header::getCalendar()->last()->isComplete()) {
                // Remove from the list we return, as this season's champ hasn't been crowned yet.
                unset($champs[$curr_season]);
            }
        }
        // Return.
        $this->champs[$this->team_id] = array_keys($champs);
        return $this->champs[$this->team_id];
    }

    /**
     * The number of Grand Prix this team has won
     * @return integer Number of Grand Prix won by the team
     */
    public function gpVictories(): int
    {
        return $this->standingsCurr->num_wins;
    }

    /**
     * The number of Grand Prix this team has appeared on the podium
     * @return integer Number of Grand Prix podiums by the team
     */
    public function gpPodiums(): int
    {
        return $this->standingsCurr->num_podiums;
    }

    /**
     * The number of pole positions this team has recorded
     * @return integer Number of pole positions by the team
     */
    public function gpPoles(): int
    {
        return $this->standingsCurr->num_poles;
    }

    /**
     * The number of fastest laps this team has recorded
     * @return integer Number of fastest laps by the team
     */
    public function gpFastestLaps(): int
    {
        return $this->standingsCurr->num_fastest_laps;
    }

    /**
     * Build the Highcharts configuration array to display the team's progress within the season
     * @param string  $dom_id  DOM element ID the chart will be rendered within.
     * @param integer $max_pos Lowest position the team could achieve in the standings.
     * @return array Appropriate highcharts configuration for the team's season progress
     */
    public function progressChart(string $dom_id, int $max_pos): array
    {
        $type_label = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
        // Generate the base object.
        $chart = Highcharts::new($dom_id);
        // Process the data into a format we can use.
        list($race_list, $categories, $champ_pos, $participants, $race_pos) = $this->progessChartSetup($type_label);
        // Base chart setup.
        $chart['title']['text'] = "{$this->season} $type_label-by-$type_label Progress for "
            . html_entity_decode($this->name, ENT_QUOTES, 'UTF-8');
        // Setup the race list (x-axis).
        $chart['xAxis'] = [
            'categories' => $categories,
            'labels' => ['useHTML' => true],
        ];
        // Setup the various y-xais.
        $chart['yAxis'] = [
            [
                'labels' => [
                    'formatter' => 'function() { return this.value == 0 ? \'\' : Numbers.ordinal(this.value); }',
                ],
                'title' => [
                    'text' => 'Constructors\' Championship',
                ],
                'reversed' => true,
                'startOnTick' => true,
                'endOnTick' => false,
                'tickPositions' => array_values(array_filter(range(1, $max_pos), function ($a) {
                    return ($a % 2) !== 0;
                })),
                'min' => 1,
                'max' => $max_pos,
            ],
            [
                'labels' => [
                    'formatter' => 'function() { return this.value + \'pt\' + (this.value == 1 ? \'\' : \'s\'); }',
                ],
                'title' => [
                    'text' => 'Championship Points',
                ],
                'opposite' => true,
                'gridZIndex' => 2,
                'min' => 0,
                'max' => 4 * ceil($this->points() / 4),
            ],
        ];
        // General chart setup.
        $chart['plotOptions'] = [
            'column' => [
                'stacking' => 'normal',
                'pointPadding' => 0,
                'groupPadding' => 0,
                'events' => [
                    'legendItemClick' => 'function() { return false; }',
                ],
            ],
            'spline' => [
                'events' => [
                    'legendItemClick' => 'function() { return false; }',
                ],
            ],
        ];
        // Our series.
        $chart['series'] = [
            [
                'name' => 'Championship Position',
                'yAxis' => 0,
                'type' => 'spline',
                'data' => $champ_pos,
                'zIndex' => 2,
            ],
        ];
        foreach ($participants as $part) {
            $chart['series'][] = [
                'name' => html_entity_decode($part['name'], ENT_QUOTES, 'UTF-8'),
                'yAxis' => 1,
                'type' => 'column',
                'data' => $part['data'],
                'zIndex' => 1,
                'stack' => 0,
            ];
        }
        // The tooltip.
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { return progressHover(this); }',
        ];
        // Custom JS elements.
        $chart['js-obj'] = [
            'races' => $race_list,
            'racePos' => $race_pos,
        ];

        return $chart;
    }

    /**
     * Prepare the various data for the progress chart rendering
     * @param string $type_label The type of 'race' we are displaying.
     * @return array The various components for the chart
     */
    protected function progessChartSetup(string $type_label): array
    {
        $races = Header::getCalendar();
        $cmpl_races = 0;
        $race_list = $categories = $champ_pos = $participants = $race_pos = [];
        foreach ($races as $race) {
            // Per-round info.
            $race_parts = $this->races->where('round', $race->round);
            // Per-race processing.
            $first_race = ($race->quirk('sprint_race') ? 2 : 1); // Skip the sprint race.
            $last_race = (isset($race->race2_time) ? 2 : 1);
            for ($race_num = $first_race; $race_num <= $last_race; $race_num++) {
                // Our main x-axis label.
                $race_key = $race->round . '-' . $race_num;
                $categories[] = '<span class="flag16_' . $race->flag() . '" '
                    . 'info="Round ' . $race->round_order . ': ' . $race->name_short
                    . ($race_num > 1 ? " ($type_label $race_num)" : '')
                    . '" data-race="' . $race->round . '-' . $race_num . '"></span>';
                $race_list[$race_key] = $race->nameFlag();
                if ($race->isComplete($race_num)) {
                    $cmpl_races++;
                }
                // Championship Position.
                $champ_progress = $this->standingsProg->where('race_key', $race->round . '-' . $race_num);
                $champ_pos[] = (isset($champ_progress) ? $champ_progress->pos : null);
                // Participants.
                foreach ($race_parts as $part) {
                    $race_res = $part->results->where('race', $race_num);
                    if (!$race_res->isset()) {
                        continue;
                    }
                    // Add the individual contributions.
                    if (!isset($participants[$part->participant->id])) {
                        $participants[$part->participant->id] = [
                            'id' => $part->participant->id,
                            'name' => $part->participant->nameFlag(),
                            'contrib' => 0,
                            'data' => [],
                            'res' => [],
                        ];
                        if ($race->round_order > 1) {
                            $participants[$part->participant->id]['data'] = array_fill(0, $race->round_order - 1, null);
                        }
                    }
                    // If there was a score, include it.
                    if (isset($race_res->pts)) {
                        $participants[$part->participant->id]['contrib'] += $race_res->pts;
                    }
                    $res = $race_res->result;
                    $participants[$part->participant->id]['res'][$race_key] = (is_numeric($res)
                        ? Format::ordinal($res)
                        : $res);
                    $participants[$part->participant->id]['data'][] = $participants[$part->participant->id]['contrib'];
                }
            }
        }
        // Ensure the contribution lists are filled to the end of the season.
        foreach ($participants as &$contrib) {
            $num_list = count($contrib['data']);
            if ($num_list < $cmpl_races) {
                $contrib['data'] = array_merge(
                    $contrib['data'],
                    array_fill($num_list + 1, $cmpl_races - $num_list, $contrib['contrib'])
                );
            }
        }
        unset($contrib); // Remove reference.
        // Sort.
        usort($participants, function ($a, $b) {
            return ($a['contrib'] <=> $b['contrib']) * -1; // Reverse spaceship.
        });
        // Record the race positions.
        foreach ($participants as $i => $part) {
            $j = $i + 1;
            foreach ($part['res'] as $race_key => $res) {
                $race_pos["$j-$race_key"] = $res;
            }
        }

        return [$race_list, $categories, $champ_pos, $participants, $race_pos];
    }
}
