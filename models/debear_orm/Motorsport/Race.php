<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Format;
use DeBear\Helpers\Highcharts;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Motorsport\Header;
use DeBear\Helpers\Sports\Motorsport\Traits\Race as TraitRace;
use DeBear\Models\Skeleton\User;
use DeBear\Repositories\Time;

class Race extends Instance
{
    use TraitRace;

    /**
     * Get the display string for the name of the race
     * @param string $name The alternative version of the name to use. (Optional).
     * @return string Display version of the race name
     */
    public function nameFlag(?string $name = null): string
    {
        return '<span class="flag_right flag16_right_' . $this->flag() . '">' . ($name ?? $this->name_full) . '</span>';
    }

    /**
     * Get the display string for the short name of the race
     * @return string Display version of the short race name
     */
    public function nameShortFlag(): string
    {
        return $this->nameFlag($this->name_short);
    }

    /**
     * Format the race's name with a link and flag
     * @param string $name The alternative version of the name to use.
     * @return string The race's formatted details
     */
    public function nameLinkFlag(?string $name = null): string
    {
        return '<span class="flag_right flag16_right_' . $this->flag() . '">'
            . '<a href="' . $this->link() . '">' . ($name ?? $this->name_full) . '</a>'
            . '</span>';
    }

    /**
     * Format the race's short name with a link and flag
     * @return string The race's formatted details
     */
    public function nameShortLinkFlag(): string
    {
        return $this->nameLinkFlag($this->name_short);
    }

    /**
     * Build the name (and possible link) for the global homepage
     * @return string The formatted name (and possible link)
     */
    public function nameHomepage(): string
    {
        $name = $this->name_full;
        if ($this->template_type == 'qual') {
            $name .= ' Qualifying';
        } elseif ($this->template_type == 'race' && $this->quirk('sprint_race')) {
            $name .= ' Sprint Qualifying';
        } elseif ($this->template_type == 'race2' && !$this->quirk('sprint_race')) {
            $name .= ' Race 2';
        }
        return '<span class="flag_right flag16_right_' . $this->flag() . '">' . $name . '</span>';
    }

    /**
     * Determine if the race has been completed or not
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return boolean If the race has been completed
     */
    public function isComplete(int $race_num = 1): bool
    {
        $laps = ($race_num == 1 ? 'race_laps_completed' : "race{$race_num}_laps_completed");
        return isset($this->$laps) && $this->$laps;
    }

    /**
     * Determine if the race was cancelled or not
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return boolean If the race has been cancelled
     */
    public function isCancelled(int $race_num = 1): bool
    {
        $laps = ($race_num == 1 ? 'race_laps_completed' : "race{$race_num}_laps_completed");
        return isset($this->$laps) && !$this->$laps;
    }

    /**
     * Determine if this is the first race of the season
     * @return boolean If this is the first race of the season
     */
    public function isFirst(): bool
    {
        return ($this->round_order == 1);
    }

    /**
     * Determine if this is the last race of the season
     * @return boolean If this is the last race of the season
     */
    public function isLast(): bool
    {
        return ($this->round_order == Header::getCalendar()->count());
    }

    /**
     * Verify whether the race has a quirk or not
     * @param string $key The quirk to be queried.
     * @return boolean That the current race has the queried quirk or not
     */
    public function quirk(string $key): bool
    {
        return isset($this->quirks) && $this->quirks->$key;
    }

    /**
     * State which race instance we should be previewing
     * @return integer Race instance to preview
     */
    public function previewRaceNum(): int
    {
        return (isset($this->race2_time) ? 2 : 1);
    }

    /**
     * Get the list of race participants sorted by the race result
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return RaceResult The race's result as an ordered collection
     */
    public function sortedResult(int $race_num = 1): RaceResult
    {
        return $this->results->where('race', $race_num);
    }

    /**
     * Get the podium participant list for this race
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return RaceResult The raw RaceResult records for the podium
     */
    public function getPodium(int $race_num = 1): RaceResult
    {
        return $this->sortedResult($race_num)->whereIn('pos', [1, 2, 3]);
    }

    /**
     * Get the winning participant for this race
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return RaceResult The raw RaceResult record of the race winner
     */
    public function getWinner(int $race_num = 1): RaceResult
    {
        return $this->sortedResult($race_num)->where('pos', '1');
    }

    /**
     * Get the list of race participants sorted by the grid order
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return RaceGrid The race's grid as an ordered collection
     */
    public function sortedGrid(int $race_num = 1): RaceGrid
    {
        return $this->grid->where('race', $race_num);
    }

    /**
     * Get the participant list for the front of the race's grid
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return RaceGrid The RaceGrid records for the front of the grid
     */
    public function getGridFront(int $race_num = 1): RaceGrid
    {
        return $this->sortedGrid($race_num)->whereIn('grid_pos', [1, 2, 3]);
    }

    /**
     * Get the participant details for the participant in Pole Position
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return RaceGrid The raw RaceGrid record for the front of the grid
     */
    public function getPolePosition(int $race_num = 1): RaceGrid
    {
        return $this->sortedGrid($race_num)->where('grid_pos', '1');
    }

    /**
     * Get the participant for the fastest lap
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return RaceFastestLap The raw RaceFastestLap record for the fastest lap
     */
    public function getFastestLap(int $race_num = 1): RaceFastestLap
    {
        $fl = $this->fastestLap->where('race', $race_num);
        return $fl;
    }

    /**
     * Prepend the Pole sitter to the lap leaders for this race
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return array List of lap leaders, starting with the pole participant (who may not have led Lap 1)
     */
    public function getLapLeadersWithPole(int $race_num = 1): array
    {
        // Build the list from the database.
        $ret = [];
        $leaders = $this->lapLeaders->where('race', $race_num)->sortBy('lap_from');
        foreach ($leaders as $leader) {
            $ret[] = (object)[
                'participant_num' => $leader->raceParticipant->raceNumber(),
                'participant' => $leader->raceParticipant->participant,
                'team' => $leader->raceParticipant->team,
                'lap_from' => $leader->lap_from,
                'lap_to' => $leader->lap_to,
                'num' => $leader->lap_to - $leader->lap_from + 1,
            ];
        }

        // And adjust according to the pole sitter.
        $pole = $this->getPolePosition($race_num);
        if ($pole->raceParticipant->raceNumber() != $ret[0]->participant_num) {
            // New entry required.
            array_unshift($ret, (object)[
                'participant_num' => $pole->raceParticipant->raceNumber(),
                'participant' => $pole->raceParticipant->participant,
                'team' => $pole->raceParticipant->team,
                'lap_from' => 0,
                'lap_to' => 0,
                'num' => 1,
            ]);
        } else {
            // First entry needs extending.
            $ret[0]->lap_from = 0;
            $ret[0]->num++;
        }

        // Finally, return the array.
        return $ret;
    }

    /**
     * Build the Highcharts configuration array to display the individual lap leaders
     * @param string  $dom_id   DOM element ID the chart will be rendered within.
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return array Appropriate highcharts configuration for the race's lap leaders
     */
    public function getLapLeadersChart(string $dom_id, int $race_num = 1): array
    {
        // Generate the base object.
        $chart = Highcharts::new($dom_id);
        // Load the data.
        $data = $this->getLapLeadersWithPole($race_num);
        $last_key = array_key_last($data);
        $max_ticks = $data[$last_key]->lap_to + 1;
        // Customise - type and layout.
        $chart['chart'] = Arrays::merge($chart['chart'], [
            'type' => 'bar',
            'spacingTop' => 5,
            'spacingBottom' => 5,
        ]);
        $chart['plotOptions'] = [
            'series' => ['stacking' => 'normal'],
            'bar' => [
                'pointPadding' => 0,
                'groupPadding' => 0,
            ],
        ];
        $chart['legend'] = [
            'enabled' => false,
        ];
        // Our x-axis (which is technically along the left).
        $chart['xAxis'] = [
            'labels' => ['enabled' => false],
            'categories' => [ucfirst(FrameworkConfig::get('debear.setup.participants.url'))],
        ];
        // Our y-axis (which is technically along the bottom).
        $chart['yAxis'] = [
            'title' => false,
            'min' => 0,
            'max' => $max_ticks,
            'tickInterval' => 1,
            'endOnTick' => false,
            // Hide the "last" lap (which is actually last lap number + 1; e.g., 57 lap race would have 58 rendered).
            'labels' => [
                'formatter' => 'function() { return (this.value == ' . $max_ticks . ') ? \'\' : this.value; }',
            ],
        ];
        // Customise the tooltip.
        $chart['tooltip'] = [
            'useHTML' => true,
            'formatter' => 'function() { return \'<tooltip><strong>\' + this.series.name + \'</strong><br>\''
                . ' + this.series.options.tooltip + \'</tooltip>\'; }',
        ];
        // Add the data.
        $chart['series'] = $colours = [];
        foreach (array_reverse($data) as $ll) {
            // Build the full tooltip.
            $tooltip = '';
            if (!$ll->lap_from) {
                // Our Pole Position.
                $tooltip = 'Pole Position';
                if ($ll->lap_to) {
                    // Who also lead the first lap (and maybe more?).
                    $tooltip .= ' and ';
                }
            }
            if ($ll->lap_to) {
                // A race-lap leader.
                $tooltip .= join(' ', [
                    Format::pluralise($ll->num, 'Lap', ['hide-num' => true]),
                    Format::groupRanges(range(max($ll->lap_from, 1), $ll->lap_to)),
                ]);
            }
            // What name will we use?
            $name = $ll->participant->nameFlag();
            if (FrameworkConfig::get('debear.setup.teams.history')) {
                $name .= ' &ndash; ' . $ll->team->nameFlag();
            }
            // Colour index?
            if (isset($colours[$ll->participant_num])) {
                $colour = $colours[$ll->participant_num];
            } else {
                $colour = $colours[$ll->participant_num] = sizeof($colours);
            }
            // Add to the list.
            $chart['series'][] = [
                'name' => $name,
                'data' => [$ll->num],
                'tooltip' => $tooltip,
                'colorIndex' => $colour,
            ];
        }

        return $chart;
    }

    /**
     * Build the championship standings for the participants after the completion of this race.
     * @return ParticipantHistoryProgress The championship standings, ordered by current position
     */
    public function getParticipantStandings(): ParticipantHistoryProgress
    {
        return $this->getStandingsBuilder('participant');
    }

    /**
     * Build the championship standings for the teams after the completion of this race.
     * @return TeamHistoryProgress The championship standings, ordered by current position
     */
    public function getTeamStandings(): TeamHistoryProgress
    {
        return $this->getStandingsBuilder('team');
    }

    /**
     * Build the championship standings for a type after the completion of this race.
     * @param string $type The type of classification to load - participant or team.
     * @return ParticipantHistoryProgress|TeamHistoryProgress The championship standings, ordered by current position
     */
    protected function getStandingsBuilder(string $type): ParticipantHistoryProgress|TeamHistoryProgress
    {
        $data = $this->secondary["{$type}sStandingsRace"];
        // Build current and previous ranges.
        $standings_curr = $data->where('round_order', $this->round_order)->sortBy('pos');
        $standings_prev = $data->where('round_order', $this->round_order - 1);
        $key_lookup = array_flip($standings_prev->pluck('part_key'));
        // Shortcut our participant/team.
        $entities = $this->secondary["{$type}s"];
        $entity_key = ($type == 'participant' ? Namespaces::callStatic($type, 'getPrimaryKey') : 'team_id');
        $entity_lookup = array_flip($entities->pluck($entity_key));
        // Then combine into our return object.
        foreach ($standings_curr as $curr) {
            // Get the previous race instance.
            $prev = isset($key_lookup[$curr->part_key_prev])
                ? $standings_prev->getRow($key_lookup[$curr->part_key_prev])
                : null;
            // Add our calculations.
            if (isset($prev)) {
                $curr->pts_diff = $curr->pts - $prev->pts;
                $curr->pos_diff = $curr->pos - $prev->pos;
                $curr->prev = (object)['pos' => $prev->pos, 'pts' => $prev->pts];
            } else {
                $curr->pts_diff = 0;
                $curr->pos_diff = 0;
                $curr->prev = false;
            }
            $curr->css = $curr->css($curr->pos);
            // Add on participant and/or team info.
            $curr->$type = $entities->getRow($entity_lookup[$curr->$entity_key]);
        }
        return $standings_curr;
    }

    /**
     * State whether or not we can link to the race
     * @return boolean That the race is linkable
     */
    public function canLink(): bool
    {
        return $this->isComplete() && !$this->isCancelled();
    }

    /**
     * Get the URL to access this race
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return string URL for the race
     */
    public function link(int $race_num = 1): string
    {
        $url = '/' . join('/', [
            $this->sectionEndpoint(),
            ltrim(FrameworkConfig::get('debear.links.races.url'), '/'),
            $this->season . '-' . $this->linkVenue(),
        ]);
        return $url . ($race_num > 1 ? "#result$race_num" : '');
    }

    /**
     * The section endpoint that applies to this race record
     * @return string The URL component to use for this race record
     */
    public function sectionEndpoint(): string
    {
        return FrameworkConfig::get('debear.section.endpoint') ?? FrameworkConfig::get('debear.section.code');
    }

    /**
     * Convert the race name to a URL component
     * @return string The race name as a URL component
     */
    public function linkVenue(): string
    {
        return Strings::codifyURL($this->name_short);
    }

    /**
     * Return info about the race to display in the header nav
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @param boolean $short    Produce a short summary, rather than the "full" details (Default: false).
     * @return string Summary string for the given race
     */
    public function summary(int $race_num = 1, bool $short = false): string
    {
        if ($this->isCancelled($race_num)) {
            // Cancelled race?
            return 'Cnc';
        } elseif ($this->isComplete($race_num)) {
            // Completed race lists the winner.
            return $this->summariseWinner($race_num);
        }
        // No, upcoming race lists the date.
        $time = ($race_num == 1 ? 'race_time' : "race{$race_num}_time");
        return $this->$time->format($short ? 'j/m' : 'jS M');
    }

    /**
     * Summarise the winner for the navigation
     * @param integer $race_num The race number we want the winner's details of.
     * @return string Summary string
     */
    public function summariseWinner(int $race_num): string
    {
        $result = $this->results->where('race', $race_num)->where('pos', 1);
        // Convert to participant details.
        $race_num_attrib = FrameworkConfig::get('debear.setup.participants.race-num');
        $winner = $this->raceParticipants->where($race_num_attrib, $result->$race_num_attrib);
        // Convert to a summarised string.
        if (FrameworkConfig::get('debear.setup.participants.summarised')) {
            $field = FrameworkConfig::get('debear.setup.participants.summarised');
            $summary = $winner->$field;
        } else {
            // In the absence of any summary field, use the participant's surname.
            $summary = $winner->participant->surname;
        }
        // Format our summary.
        return "<span class=\"flag_right flag16_right_{$winner->participant->nationality}\">$summary</span>";
    }

    /**
     * A summarised information message about this race
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return string Info message string for the given race
     */
    public function infoMessage(int $race_num = 1): string
    {
        // By default, we don't have one.
        return '';
    }

    /**
     * Format the qualifying start time for a race
     * @return string The formatted time
     */
    public function getQualStartTimeAttribute(): string
    {
        return $this->formatTime($this->qual_time);
    }

    /**
     * Format the race start time
     * @return string The formatted time
     */
    public function getRaceStartTimeAttribute(): string
    {
        return $this->formatTime($this->race_time);
    }

    /**
     * Format the race 2 start time
     * @return string The formatted time
     */
    public function getRace2StartTimeAttribute(): string
    {
        return $this->formatTime($this->race2_time);
    }

    /**
     * Format a given input time for display
     * @param Carbon $time The time to format.
     * @return string The formatted time
     */
    protected function formatTime(Carbon $time): string
    {
        $tz = (User::object()->isLoggedIn() ? Time::object()->getUserTimezone() : $this->timezone);
        return $time->copy()->setTimezone($tz)
            ->format('l jS F, H:i') . (User::object()->isLoggedIn() ? '' : ' (Local)');
    }
}
