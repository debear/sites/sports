<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;

class RaceFastestLap extends Instance
{
    /**
     * Summarise the fastest lap time
     * @return string The time formatted for display
     */
    public function getTimeAttribute(): string
    {
        return sprintf('%s.%03d', ltrim($this->lap_time, '0:'), $this->lap_time_ms);
    }
}
