<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class RaceParticipant extends Instance
{
    /**
     * Get the race number used by the participant in this race
     * @return integer The participant's race number
     */
    public function raceNumber(): int
    {
        $race_num_attrib = FrameworkConfig::get('debear.setup.participants.race-num');
        return $this->$race_num_attrib;
    }
}
