<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Motorsport\Traits\Race as TraitRace;

class RaceResult extends Instance
{
    use TraitRace;

    /**
     * Return the appropriate position
     * @return string Position, either the numeric value or a code
     */
    public function pos(): string
    {
        return (string)$this->pos;
    }

    /**
     * Summarise the participant's progress in the race
     * @return string The time/notes for the participant
     */
    public function formatResult(): string
    {
        if (isset($this->gap_time)) {
            // Relative time.
            return '+' . sprintf('%s.%03d', ltrim($this->gap_time, '0:') ?: '0', $this->gap_time_ms);
        } elseif ($this->pos == 1) {
            // Race time.
            return sprintf('%s.%03d', ltrim($this->race_time, '0:') ?: '0', $this->race_time_ms);
        } else {
            // Race notes.
            $note = ($this->notes ?? '');
            if (substr($this->notes, 0, 1) != '+') {
                $note = "<em>$note</em>";
            }
            return $note;
        }
    }

    /**
     * The race row CSS style
     * @param Race $info Race object for the race we are displaying a cell for or specific values. (Optional).
     * @return string The CSS to represent this race result
     */
    public function css(?Race $info = null): string
    {
        $have_info = isset($info) && is_object($info);
        // If the race was known not to have been completed yet.
        if ($have_info && $info->isCancelled($this->race ?? 1)) {
            // Cancelled raced.
            $base_css = 'res-no_race';
        } elseif ($have_info && !$info->isComplete($this->race ?? 1)) {
            // Not raced.
            $base_css = 'res-not_yet_raced';
        } elseif (!isset($this->result) || !$this->result) {
            // No result.
            $base_css = 'res-no_race';
        } elseif (isset($this->pos) && ($this->pos < 4)) {
            // Podium.
            $base_css = 'res-pos_' . $this->pos;
        } elseif (!is_numeric($this->result)) {
            // Non-finishing result.
            $base_css = 'res-' . strtolower($this->result);
        } elseif ($this->pts) {
            // Points finish.
            $base_css = 'res-pts_finish';
        } else {
            // Non-points finish.
            $base_css = 'res-non_pts_finish';
        }

        // Return with any sport-specific extras?
        return "$base_css " . $this->cssBespoke($info);
    }

    /**
     * Get the list of trophies won by the participant in this season
     * @return array Array of trophies won with codes and counts for each
     */
    public function getTrophyCounts(): array
    {
        $trophies = [ 1 => 0, 2 => 0, 3 => 0, 'fl' => 0, ];
        foreach ($this as $result) {
            $pos = $result->pos();
            if (is_numeric($pos) && $pos > 0 && $pos < 4) {
                $trophies[$pos]++;
            }
            if ($result->fastest_lap) {
                $trophies['fl']++;
            }
        }
        return array_filter($trophies);
    }

    /**
     * A display text version of the race result
     * @return string The formatted result
     */
    public function displayResult(): string
    {
        return (string)($this->result ?? '');
    }

    /**
     * A display numeric version of the points total
     * @return string The formatted total
     */
    public function displayPts(): string
    {
        if (!$this->pts && $this->fastest_lap) {
            // If no points were scored, but the participant had the fastest lap, then include a specific code.
            return 'FL';
        }
        return str_replace('.0', '', $this->pts);
    }

    /**
     * A display text version of the points total, with pt(s) appended to the string (not in use yet)
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return string The formatted total
     * /
    public function displayPtsFull(int $race_num = 1): string
    {
        return Format::pluralise($this->pts($race_num), 'pt');
    }
    /* */

    /**
     * A display version of the grid position
     * @return string The formatted grid positin
     */
    public function displayGrid(): string
    {
        return ($this->grid_pos ?? '');
    }
}
