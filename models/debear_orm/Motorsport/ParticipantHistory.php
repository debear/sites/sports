<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\Highcharts;
use DeBear\Helpers\Sports\Motorsport\Traits\EntityHistory as TraitEntityHistory;

class ParticipantHistory extends Instance
{
    use TraitEntityHistory;

    /**
     * Build the Highcharts configuration array to display the participant's progress within a season
     * @param string  $dom_id  DOM element ID the chart will be rendered within.
     * @param integer $max_pos Lowest position the participant could achieve in the standings.
     * @return array Appropriate highcharts configurations for the participant's season progress
     */
    public function progressChart(string $dom_id, int $max_pos): array
    {
        $type_race = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
        $chart = Highcharts::new($dom_id);
        $setup = $this->progessChartSetup($type_race);
        list($categories, $champ_pos, $champ_pts, $race_pos, $race_grid, $tooltips) = $setup;
        $chart['title']['text'] = "{$this->season} $type_race-by-$type_race Progress for "
            . html_entity_decode($this->participant->name, ENT_QUOTES, 'UTF-8');
        // Setup the race list (x-axis).
        $chart['xAxis'] = [
            'categories' => $categories,
            'labels' => ['useHTML' => true],
        ];
        // Setup the various y-axis.
        $chart['yAxis'] = [
            [
                'labels' => [
                    'formatter' => 'function() { return this.value == 0 ? \'\' : Numbers.ordinal(this.value); }',
                ],
                'title' => [
                    'text' => ucfirst(FrameworkConfig::get('debear.setup.participants.url')) . "' Championship",
                ],
                'reversed' => true,
                'startOnTick' => true,
                'endOnTick' => false,
                'tickPositions' => array_values(array_filter(range(1, $max_pos), function ($a) {
                    return ($a % 2) !== 0;
                })),
                'min' => 1, 'max' => $max_pos,
            ],
            [
                'labels' => [
                    'formatter' => 'function() { return this.value + \'pt\' + (this.value == 1 ? \'\' : \'s\'); }',
                ],
                'title' => [
                    'text' => 'Championship Points',
                ],
                'opposite' => true,
                'gridZIndex' => 2,
                'min' => 0, 'max' => 4 * ceil($this->pts / 4),
            ],
        ];
        // General chart setup.
        $chart['plotOptions'] = [
            'column' => [
                'stacking' => 'normal',
                'pointPadding' => 0,
                'groupPadding' => 0,
                'events' => [
                    'legendItemClick' => 'function() { return false; }',
                ],
            ],
            'spline' => [
                'events' => [
                    'legendItemClick' => 'function() { return false; }',
                ],
            ],
        ];
        // Our series.
        $chart['series'] = [
            [
                'name' => "Championship Position",
                'yAxis' => 0,
                'type' => 'spline',
                'data' => $champ_pos,
                'zIndex' => 3,
            ],
            [
                'name' => 'Championship Points',
                'yAxis' => 1,
                'type' => 'column',
                'data' => $champ_pts,
                'zIndex' => 0,
            ],
            [
                'name' => "$type_race Result",
                'yAxis' => 0,
                'type' => 'spline',
                'data' => $race_pos,
                'zIndex' => 2,
            ],
        ];
        if (FrameworkConfig::get('debear.setup.type') == 'circuit') {
            $chart['series'][] = [
                'name' => 'Grid Position',
                'yAxis' => 0,
                'type' => 'spline',
                'data' => $race_grid,
                'zIndex' => 1,
            ];
        }
        $chart['tooltip'] = [
            'shared' => true,
            'useHTML' => true,
            'formatter' => 'function() { return progressHover(this); }',
            'info' => $tooltips,
        ];
        return $chart;
    }

    /**
     * Prepare the various data for the progress chart rendering
     * @param string $type_race The type of 'race' we are displaying.
     * @return array The various components for the chart
     */
    protected function progessChartSetup(string $type_race): array
    {
        $is_circuit = (FrameworkConfig::get('debear.setup.type') == 'circuit');
        $categories = $champ_pos = $champ_pts = $race_pos = $race_grid = $tooltips = [];

        $race_keys = [];
        foreach ($this->calendar as $race) {
            $first_race = ($race->quirk('sprint_race') ? 2 : 1); // Skip the sprint race.
            $last_race = (isset($race->race2_time) ? 2 : 1);
            for ($race_num = $first_race; $race_num <= $last_race; $race_num++) {
                $race_keys[] = $race->round . '-' . $race_num;
            }
        }

        // Pre-process the results.
        $parsed_res = array_fill_keys($race_keys, [
            'pos' => null,
            'grid' => null,
            'res' => null,
        ]);
        foreach ($this->results as $result) {
            $parsed_res[$result->race_key] = [
                'pos' => (int)$result->pos(),
                'grid' => null,
                'res' => (is_numeric($result->result) ? Format::ordinal($result->result) : $result->result),
            ];
            if ($is_circuit && ($result->displayGrid() > 0)) {
                $parsed_res[$result->race_key]['grid'] = (int)$result->displayGrid();
            }
        }
        // Short-cut the championship progress.
        $parsed_prog = array_fill_keys($race_keys, [
            'pts' => null,
            'pos' => null,
        ]);
        foreach ($this->progress as $progress) {
            $parsed_prog[$progress->race_key] = [
                'pts' => is_integer($progress->pts) ? (int)$progress->pts : (float)$progress->pts,
                'pos' => $progress->pos,
            ];
        }
        // Now process.
        foreach ($this->calendar as $race) {
            // Per-race processing.
            $first_race = ($race->quirk('sprint_race') ? 2 : 1); // Skip the sprint race.
            $last_race = (isset($race->race2_time) ? 2 : 1);
            for ($race_num = $first_race; $race_num <= $last_race; $race_num++) {
                // Our main x-axis label.
                $race_key = $race->round . '-' . $race_num;
                $categories[] = '<span class="flag16_' . $race->flag() . '" '
                    . 'info="Round ' . $race->round_order . ': ' . $race->name_short
                    . ($race_num > 1 ? " ($type_race $race_num)" : '')
                    . '" data-season="' . $race->season
                    . '" data-race="' . $race->round . '-' . $race_num . '"></span>';
                // Championship Position and Points.
                $champ_pos[] = $parsed_prog[$race_key]['pos'];
                $champ_pts[] = $parsed_prog[$race_key]['pts'];
                // Race details.
                $race_pos[] = $parsed_res[$race_key]['pos'];
                if ($is_circuit) {
                    $race_grid[] = $parsed_res[$race_key]['grid'];
                }
                // Build up the tooltip version.
                $tooltips[$race_key] = array_filter([
                    'name' => $race->nameFlag(),
                    'res' => $parsed_res[$race_key]['res'],
                    'grid' => (isset($parsed_res[$race_key]['grid'])
                        ? Format::ordinal($parsed_res[$race_key]['grid'])
                        : null),
                ]);
            }
        }

        return [$categories, $champ_pos, $champ_pts, $race_pos, $race_grid, $tooltips];
    }
}
