<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Motorsport\Traits\EntityHistory as TraitEntityHistory;

class ParticipantHistoryProgress extends Instance
{
    use TraitEntityHistory;
}
