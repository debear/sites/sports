<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class RaceQuali extends Instance
{
    /**
     * Summarise the participant's qualifying time
     * @return string The time/notes for the participant
     */
    public function getTimeAttribute(): string
    {
        if ($this->pos == 1) {
            // Lap time.
            return $this->time_abs;
        } else {
            // Relative time.
            return $this->time_rel;
        }
    }

    /**
     * The participant's absolute qualifying time
     * @return string The absolute session time for the participant
     */
    public function getTimeAbsAttribute(): string
    {
        if (!isset($this->q_time)) {
            return '<em>No Time</em>';
        }
        return sprintf('%s.%03d', ltrim($this->q_time, '0:'), $this->q_time_ms);
    }

    /**
     * The participant's relative qualifying time to the session leader
     * @return string The relative session time for the participant
     */
    public function getTimeRelAttribute(): string
    {
        if (!isset($this->q_time)) {
            return '<em>No Time</em>';
        }
        return '+' . sprintf('%0d.%03d', ltrim($this->gap_time, '0:'), $this->gap_time_ms);
    }

    /**
     * State the number of qualifying sessions
     * @return integer The number of qualifying sessions in a race for this season
     */
    public static function numSessions(): int
    {
        return FrameworkConfig::get('debear.setup.quali.sessions');
    }
}
