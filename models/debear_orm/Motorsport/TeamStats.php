<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use DeBear\Helpers\Sports\Motorsport\Traits\Stats as TraitStats;

class TeamStats extends Instance
{
    use TraitStats;
}
