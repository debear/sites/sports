<?php

namespace DeBear\ORM\Sports\Motorsport\FIA;

use DeBear\ORM\Sports\Motorsport\ParticipantHistory as BaseParticipantHistory;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;

class ParticipantHistory extends BaseParticipantHistory
{
    /**
     * Format the series name (not in use yet)
     * @return string The series name formatted for display
     * /
    public function getGroupAttribute(): string
    {
        return ($this->series == 'fe' ? 'Formula E' : strtoupper($this->series));
    }
    /* */

    /**
     * The unique reference for the season
     * @return string A unique reference that applies specifically to this season
     */
    public function getReferenceAttribute(): string
    {
        return $this->season . '-' . $this->series;
    }

    /**
     * Get the standings for the current season we are viewing
     * @param array $opt Query configuration options. (Optional).
     * @return Participant The standings (sorted) for the viewing season
     */
    public static function standingsBySeason(array $opt = []): Participant
    {
        $secondary = ['standings'];
        if (isset($opt['expanded']) && $opt['expanded']) {
            $secondary = array_merge($secondary, [
                'raceResults',
            ]);
        }
        $season = ($opt['season'] ?? FrameworkConfig::get('debear.setup.season.viewing'));
        $tbl_participant = Participant::getTable();
        $tbl_history = static::getTable();
        return Participant::query()
            ->select("$tbl_participant.*", "$tbl_history.season")
            ->join($tbl_history, function ($join) use ($tbl_participant, $tbl_history, $season) {
                $join->where("$tbl_history.season", '=', $season)
                    ->where("$tbl_history.series", '=', FrameworkConfig::get('debear.section.code'))
                    ->on("$tbl_history.driver_id", '=', "$tbl_participant.driver_id");
            })
            ->orderBy("$tbl_history.pos")
            ->get()
            ->loadSecondary($secondary);
    }

    /**
     * Determine the total number of participants in a given list of seasons
     * @param array $seasons List of seasons to be checked.
     * @return array Array of Season/Number pairs
     */
    public static function totalParticipantsBySeason(array $seasons): array
    {
        $ret = static::query()
            ->select(DB::raw('season, MAX(pos) AS max_pos'))
            ->whereIn('season', $seasons)
            ->where('series', FrameworkConfig::get('debear.section.code'))
            ->groupBy('season')
            ->get();
        return array_column($ret->getData(), 'max_pos', 'season');
    }
}
