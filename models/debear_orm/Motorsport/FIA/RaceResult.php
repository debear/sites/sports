<?php

namespace DeBear\ORM\Sports\Motorsport\FIA;

use DeBear\ORM\Sports\Motorsport\RaceResult as BaseRaceResult;

class RaceResult extends BaseRaceResult
{
    /**
     * Sport-specific CSS classes to try and include
     * @param Race $info Race object for the race we are displaying a cell for or specific values. (Optional).
     * @return string Additional CSS classes
     */
    protected function cssBespoke(?Race $info = null): string
    {
        // Fastest lap?
        $css_fl = 'res-fast_lap';
        if (isset($this->fastest_lap)) {
            // Attribute defined, so take our lead from the attribute.
            return $this->fastest_lap ? $css_fl : '';
        } elseif (is_object($info)) {
            // How about via the race object?
            $race_fl = $info->getFastestLap($this->race ?? 1);
            if (is_object($race_fl) && $race_fl->isset() && ($race_fl->car_no == $this->car_no)) {
                return $css_fl;
            }
        }
        // Nothing additional.
        return '';
    }
}
