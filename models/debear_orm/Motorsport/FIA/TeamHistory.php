<?php

namespace DeBear\ORM\Sports\Motorsport\FIA;

use DeBear\ORM\Sports\Motorsport\TeamHistory as BaseTeamHistory;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class TeamHistory extends BaseTeamHistory
{
    /**
     * Get the standings for the current season we are viewing
     * @param array $opt Query configuration options. (Optional).
     * @return Team The standings (sorted) for the viewing season
     */
    public static function standingsBySeason(array $opt = []): Team
    {
        $secondary = ['standings'];
        if (isset($opt['expanded']) && $opt['expanded']) {
            $secondary = array_merge($secondary, [
                'raceParticipants',
                'raceResults',
            ]);
        }
        $season = ($opt['season'] ?? FrameworkConfig::get('debear.setup.season.viewing'));
        $tbl_team = Team::getTable();
        $tbl_history = static::getTable();
        return Team::query()
            ->select("$tbl_team.*", "$tbl_history.season")
            ->selectRaw("CONCAT($tbl_team.season, '-', $tbl_team.series, '-', $tbl_team.team_id) AS team_key")
            ->join($tbl_history, function ($join) use ($tbl_team, $tbl_history, $season) {
                $join->on("$tbl_history.season", '=', "$tbl_team.season")
                    ->on("$tbl_history.series", '=', "$tbl_team.series")
                    ->on("$tbl_history.team_id", '=', "$tbl_team.team_id");
            })->where([
                ["$tbl_team.season", '=', $season],
                ["$tbl_team.series", '=', FrameworkConfig::get('debear.section.code')],
            ])->orderBy("$tbl_history.pos")
            ->get()
            ->loadSecondary($secondary);
    }
}
