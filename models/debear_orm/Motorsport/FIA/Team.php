<?php

namespace DeBear\ORM\Sports\Motorsport\FIA;

use DeBear\ORM\Sports\Motorsport\Team as BaseTeam;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

class Team extends BaseTeam
{
    /**
     * Get the numeric ID for this participant
     * @return integer The particpant's ID
     */
    public function getIDAttribute(): int
    {
        return $this->team_id;
    }

    /**
     * Parse the race list into an array of driver-specific drives for this team
     * @return array Loopable array of driver objects
     */
    public function getParticipantLoops(): array
    {
        $ret = [];

        // Get the drivers, and when their first race was (as we want to order by first appearance).
        $drivers = $this->races->unique('driver_code');
        foreach ($drivers as $driver) {
            // Build the array object we will later sort (so slightly more complex).
            $ret[] = [
                'driver' => $driver,
                'races' => $this->results->where('driver_code', $driver),
                'first' => $this->races->where('driver_code', $driver)->min('round_order'),
            ];
        }
        // Sort and return the array.
        usort($ret, function ($a, $b) {
            // First appearance is preferable.
            if ($a['first'] != $b['first']) {
                return $a['first'] <=> $b['first'];
            }
            // Tie-breaker: name...
            return $a['driver'] <=> $b['driver'];
        });
        return array_column($ret, 'races', 'driver');
    }

    /**
     * Get the list of teams for the current season we are viewing
     * @return self The teams (sorted - as collection - by position) for the viewing season
     */
    public static function getBySeason(): self
    {
        // Get the teams.
        $tbl_team = static::getTable();
        $tbl_teamhist = TeamHistory::getTable();
        $teams = static::query()
            ->where([
                ["$tbl_team.season", '=', FrameworkConfig::get('debear.setup.season.viewing')],
                ["$tbl_team.series", '=', FrameworkConfig::get('debear.section.code')],
            ])->join("$tbl_teamhist", function ($join) use ($tbl_teamhist, $tbl_team) {
                $join->on("$tbl_teamhist.season", '=', "$tbl_team.season")
                    ->on("$tbl_teamhist.series", '=', "$tbl_team.series")
                    ->on("$tbl_teamhist.team_id", '=', "$tbl_team.team_id");
            })->orderBy("$tbl_teamhist.pos")
            ->get();
        // Then the drivers, with the historical info.
        $tbl_part = Participant::getTable();
        $tbl_racepart = RaceParticipant::getTable();
        $tbl_race = Race::getTable();
        $drivers = Participant::query()
            ->join($tbl_racepart, function ($join) use ($tbl_racepart, $tbl_part) {
                $join->where("$tbl_racepart.season", '=', FrameworkConfig::get('debear.setup.season.viewing'))
                    ->where("$tbl_racepart.series", '=', FrameworkConfig::get('debear.section.code'))
                    ->on("$tbl_racepart.driver_id", '=', "$tbl_part.driver_id");
            })
            ->join($tbl_race, function ($join) use ($tbl_race, $tbl_racepart) {
                $join->on("$tbl_race.season", '=', "$tbl_racepart.season")
                    ->on("$tbl_race.series", '=', "$tbl_racepart.series")
                    ->on("$tbl_race.round", '=', "$tbl_racepart.round");
            })
            ->select("$tbl_part.*", "$tbl_racepart.team_id", "$tbl_racepart.car_no")
            ->selectRaw("MIN($tbl_race.round_order) AS first_round")
            ->groupBy("$tbl_part.driver_id", "$tbl_racepart.team_id")
            ->orderBy("$tbl_part.surname")
            ->orderBy("$tbl_part.first_name")
            ->get();
        // What is our driver sorting method - car_no/first -vs- pts-desc?
        $sort_by_pts = true;
        $car_num_perm = FrameworkConfig::get('debear.setup.season.car_num_perm');
        if (FrameworkConfig::get('debear.setup.season.viewing') < $car_num_perm) {
            // Car Number, then first race.
            $drivers = $drivers->sort(function ($a, $b) {
                if ($a['car_no'] != $b['car_no']) {
                    return $a['car_no'] <=> $b['car_no'];
                }
                return $a['first_round'] <=> $b['first_round'];
            });
            $sort_by_pts = false;
        }
        $driver_ids = $drivers->unique('driver_id');
        $standings_drivers = ParticipantHistory::query()
            ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
            ->whereIn('driver_id', $driver_ids)
            ->get();
        $standings_teams = TeamHistory::query()
            ->whereIn('team_id', $teams->unique('team_id'))
            ->get();
        $tbl_res = RaceResult::getTable();
        $results = RaceResult::query()
            ->join($tbl_racepart, function ($join) use ($tbl_racepart, $tbl_res) {
                $join->where("$tbl_racepart.season", '=', FrameworkConfig::get('debear.setup.season.viewing'))
                    ->where("$tbl_racepart.series", '=', FrameworkConfig::get('debear.section.code'))
                    ->on("$tbl_racepart.round", '=', "$tbl_res.round")
                    ->on("$tbl_racepart.car_no", '=', "$tbl_res.car_no");
            })
            ->select("$tbl_res.*", "$tbl_racepart.driver_id", "$tbl_racepart.team_id")
            ->selectRaw("CONCAT($tbl_racepart.team_id, '-', $tbl_racepart.driver_id) AS driver_team_key")
            ->where([
                ["$tbl_res.season", '=', FrameworkConfig::get('debear.setup.season.viewing')],
                ["$tbl_res.series", '=', FrameworkConfig::get('debear.section.code')]
            ])
            ->whereIn("$tbl_racepart.driver_id", $driver_ids)
            ->get();
        // Bind the history and results to the driver, then the drivers to the teams.
        foreach ($drivers as $driver) {
            $driver->standingsCurr = $standings_drivers->where('driver_id', $driver->driver_id);
            $driver->results = $results->where('driver_team_key', "{$driver->team_id}-{$driver->driver_id}");
            $driver->pts = $driver->results->sum('pts');
        }
        foreach ($teams as $team) {
            $team->history = $standings_teams->where('team_id', $team->team_id);
            $team->standingsCurr = $team->history->where('season', FrameworkConfig::get('debear.setup.season.viewing'));
            $team->drivers = $drivers->where('team_id', $team->team_id);
            if ($sort_by_pts) {
                $team->drivers = $team->drivers->sortByDesc('pts');
            }
        }
        return $teams;
    }

    /**
     * Get the list of all teams for the XML sitemap
     * @return self The teams as collection
     */
    public static function getAllHistorical(): self
    {
        return static::query()
            ->where([
                ['season', '>=', FrameworkConfig::get('debear.setup.season.min')],
                ['series', '=', FrameworkConfig::get('debear.section.code')],
            ])->orderBy('season')
            ->orderBy('team_name')
            ->get();
    }

    /**
     * Try and find a team passed in by codified name
     * @param string  $team_name The codified name being looked up.
     * @param integer $season    The season being requested (which may not be available, but isn't a blocker).
     * @return ?self The found (shortened) matching info, or null if not found
     */
    public static function findByCodified(string $team_name, int $season): ?self
    {
        // Get the list of possible options.
        $all_names = static::query()
            ->select('team_id', 'series', 'team_name')
            ->selectRaw('MIN(season) AS first_season')
            ->selectRaw('MAX(season) AS last_season')
            ->selectRaw('MAX(season = ?) AS inc_season', [$season])
            ->where('series', FrameworkConfig::get('debear.section.code'))
            ->groupBy('team_id', 'team_name')
            ->orderByDesc('inc_season')
            ->get();
        foreach ($all_names as $name) {
            // If we have a match then use what we've found a potential team.
            if ($name->name_codified == $team_name) {
                $match = $name->getCurrent();
                break;
            }
        }
        // Return what we find.
        if (!isset($match)) {
            // No match.
            return null;
        }
        $season_query = $match->inc_season
            ? $season
            : ($season > $match->last_season
                ? $match->last_season
                : $match->first_season);
        return static::query()->where([
            ['season', $season_query],
            ['series', $match->series],
            ['team_id', $match->team_id],
        ])->get();
    }

    /**
     * Load the full information about a given team
     * @param self $found The abbreviated team info from the validation phase.
     * @return self The fully loaded information for the single team
     */
    public static function load(self $found): self
    {
        return static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series, "-", team_id) AS team_key')
            ->where([
                ['season', '=', $found->season],
                ['series', '=', $found->series],
                ['team_id', '=', $found->team_id],
            ])->get()
            ->loadSecondary([
                'standings',
                'history',
                'raceParticipants',
                'participants',
                'raceResults',
                'seasonProgress',
                'seasonStats',
            ]);
    }

    /**
     * The default handler for loading secondary team information.
     * @param string $name The requested secondary team information.
     * @return void
     */
    protected function loadSecondaryDefault(string $name): void
    {
        $maps = [
            'standings' => [
                'class' => TeamHistory::class,
                'name' => 'standingsCurr',
                'select-extra' => 'CONCAT(season, "-", series, "-", team_id) AS lookup_key',
                'lookup-key' => 'team_key',
            ],
            'seasonProgress' => [
                'class' => TeamHistoryProgress::class,
                'name' => 'standingsProg',
                'select-extra' => 'CONCAT(round, "-", race) AS race_key',
                'multi-lookup' => true,
                'lookup-key' => 'team_id',
            ],
        ];
        $map = $maps[$name];
        $team_ids = $this->unique('team_id');
        // Build our query and get the data.
        $query = $map['class']::query()
            ->select("*")
            ->selectRaw($map['select-extra']);
        foreach ($this->data as $i => $team) {
            $query->orWhere(function (Builder $where) use ($team) {
                $where->where([
                    ['season', '=', $team['season']],
                    ['series', '=', FrameworkConfig::get('debear.section.code')],
                    ['team_id', '=', $team['team_id']],
                ]);
            });
        }
        $this->secondary[$name] = $query->orWhere(DB::raw(1), DB::raw(0))->get();
        // Tie back to the teams.
        if (isset($map['multi-lookup']) && $map['multi-lookup']) {
            $method = 'getByKeys';
            $raw = $this->secondary[$name]->pluck('team_id');
            $key_lookup = array_fill_keys($team_ids, []);
            foreach ($raw as $id => $team_id) {
                $key_lookup[$team_id][] = $id;
            }
        } else {
            $method = 'getRow';
            $key_lookup = array_flip($this->secondary[$name]->pluck('lookup_key'));
        }
        $instance = ($map['name'] ?? $name);
        foreach ($this->data as $i => $team) {
            $this->data[$i][$instance] = $this->secondary[$name]->$method($key_lookup[$team[$map['lookup-key']]]);
        }
    }

    /**
     * Load the information about the race participants for each team.
     * @return void
     */
    protected function loadSecondaryRaceParticipants(): void
    {
        $team_ids = $this->unique('team_id');
        $tbl_racepart = RaceParticipant::getTable();
        $tbl_team = Team::getTable();
        $tbl_race = Race::getTable();
        $this->secondary['raceParticipants'] = RaceParticipant::query()
            ->select("$tbl_racepart.*", "$tbl_race.round_order")
            ->selectRaw(
                "CONCAT($tbl_racepart.season, '-', $tbl_racepart.series, '-', $tbl_racepart.round, '-', "
                    . "$tbl_racepart.car_no) AS part_key"
            )
            ->join($tbl_team, function ($join) use ($tbl_team, $tbl_racepart) {
                return $join->on("$tbl_team.season", '=', "$tbl_racepart.season")
                    ->on("$tbl_team.series", '=', "$tbl_racepart.series")
                    ->on("$tbl_team.team_id", '=', "$tbl_racepart.team_id");
            })->join($tbl_race, function ($join) use ($tbl_race, $tbl_racepart) {
                return $join->on("$tbl_race.season", '=', "$tbl_racepart.season")
                    ->on("$tbl_race.series", '=', "$tbl_racepart.series")
                    ->on("$tbl_race.round", '=', "$tbl_racepart.round");
            })->where("$tbl_racepart.season", '=', $this->first()->season)
            ->where("$tbl_racepart.series", '=', FrameworkConfig::get('debear.section.code'))
            ->whereIn("$tbl_racepart.team_id", $team_ids)
            ->orderByDesc("$tbl_racepart.season")
            ->get();
        $raw = $this->secondary['raceParticipants']->pluck('team_id');
        $key_lookup = array_fill_keys($team_ids, []);
        foreach ($raw as $id => $team_id) {
            $key_lookup[$team_id][] = $id;
        }
        foreach ($this->data as $i => $team) {
            $this->data[$i]['races'] = $this->secondary['raceParticipants']->getByKeys($key_lookup[$team['team_id']]);
        }
    }

    /**
     * Load the information about the history for each team.
     * @return void
     */
    protected function loadSecondaryHistory(): void
    {
        $team_ids = $this->unique('team_id');
        $tbl_teamhist = TeamHistory::getTable();
        $tbl_team = Team::getTable();
        $this->secondary['history'] = TeamHistory::query()
            ->select("$tbl_teamhist.*", "$tbl_team.team_name", "$tbl_team.official_name", "$tbl_team.team_country")
            ->join($tbl_team, function ($join) use ($tbl_team, $tbl_teamhist) {
                return $join->on("$tbl_team.season", '=', "$tbl_teamhist.season")
                    ->on("$tbl_team.series", '=', "$tbl_teamhist.series")
                    ->on("$tbl_team.team_id", '=', "$tbl_teamhist.team_id");
            })->where("$tbl_teamhist.series", '=', FrameworkConfig::get('debear.section.code'))
            ->whereIn("$tbl_teamhist.team_id", $team_ids)
            ->orderByDesc("$tbl_teamhist.season")
            ->get();
        $raw = $this->secondary['history']->pluck('team_id');
        $key_lookup = array_fill_keys($team_ids, []);
        foreach ($raw as $id => $team_id) {
            $key_lookup[$team_id][] = $id;
        }
        foreach ($this->data as $i => $team) {
            $this->data[$i]['history'] = $this->secondary['history']->getByKeys($key_lookup[$team['team_id']]);
        }
    }

    /**
     * Load the information about the stats for each team.
     * @return void
     */
    protected function loadSecondarySeasonStats(): void
    {
        $season = $this->first()->season;
        $team_ids = $this->unique('team_id');
        $tbl_teamstats = TeamStats::getTable();
        $tbl_stats = Stats::getTable();
        $this->secondary['seasonStats'] = TeamStats::query()
            ->select("$tbl_teamstats.*", "$tbl_stats.name_short", "$tbl_stats.name_full", "$tbl_stats.type")
            ->join($tbl_stats, "$tbl_stats.stat_id", '=', "$tbl_teamstats.stat_id")
            ->where([
                ["$tbl_teamstats.season", '=', $season],
                ["$tbl_teamstats.series", '=', FrameworkConfig::get('debear.section.code')]
            ])
            ->whereIn("$tbl_teamstats.team_id", $team_ids)
            ->orderBy("$tbl_stats.disp_order")
            ->get();
        $raw = $this->secondary['seasonStats']->pluck('team_id');
        $key_lookup = array_fill_keys($team_ids, []);
        foreach ($raw as $id => $team_id) {
            $key_lookup[$team_id][] = $id;
        }
        foreach ($this->data as $i => $team) {
            $this->data[$i]['seasonStats'] = $this->secondary['seasonStats']
                ->getByKeys($key_lookup[$team['team_id']]);
        }
    }

    /**
     * Load the information about the races taken part by each team.
     * @return void
     */
    protected function loadSecondaryRaceResults(): void
    {
        $season = $this->first()->season;
        $team_ids = $this->unique('team_id');
        $tbl_race = Race::getTable();
        $tbl_racepart = RaceParticipant::getTable();
        $tbl_res = RaceResult::getTable();
        $tbl_grid = RaceGrid::getTable();
        $tbl_fl = RaceFastestLap::getTable();
        $this->secondary['raceResults'] = RaceResult::query()
            ->select(
                "$tbl_res.*",
                "$tbl_race.round_order",
                "$tbl_racepart.team_id",
                "$tbl_racepart.driver_code",
                "$tbl_grid.grid_pos"
            )
            ->selectRaw("$tbl_fl.lap_num IS NOT NULL AS fastest_lap")
            ->selectRaw("CONCAT($tbl_res.round, '-', $tbl_res.race) AS race_key")
            ->selectRaw(
                "CONCAT($tbl_res.season, '-', $tbl_res.series, '-', $tbl_res.round, '-', $tbl_res.car_no) AS part_key"
            )
            ->join($tbl_race, function ($join) use ($tbl_race, $tbl_res) {
                return $join->on("$tbl_race.season", '=', "$tbl_res.season")
                    ->on("$tbl_race.series", '=', "$tbl_res.series")
                    ->on("$tbl_race.round", '=', "$tbl_res.round");
            })
            ->join($tbl_racepart, function ($join) use ($tbl_racepart, $tbl_res) {
                return $join->on("$tbl_racepart.season", '=', "$tbl_res.season")
                    ->on("$tbl_racepart.series", '=', "$tbl_res.series")
                    ->on("$tbl_racepart.round", '=', "$tbl_res.round")
                    ->on("$tbl_racepart.car_no", '=', "$tbl_res.car_no");
            })
            ->leftJoin($tbl_grid, function ($join) use ($tbl_grid, $tbl_res) {
                return $join->on("$tbl_grid.season", '=', "$tbl_res.season")
                    ->on("$tbl_grid.series", '=', "$tbl_res.series")
                    ->on("$tbl_grid.round", '=', "$tbl_res.round")
                    ->on("$tbl_grid.race", '=', "$tbl_res.race")
                    ->on("$tbl_grid.car_no", '=', "$tbl_res.car_no");
            })
            ->leftJoin($tbl_fl, function ($join) use ($tbl_fl, $tbl_res) {
                return $join->on("$tbl_fl.season", '=', "$tbl_res.season")
                    ->on("$tbl_fl.series", '=', "$tbl_res.series")
                    ->on("$tbl_fl.round", '=', "$tbl_res.round")
                    ->on("$tbl_fl.race", '=', "$tbl_res.race")
                    ->on("$tbl_fl.car_no", '=', "$tbl_res.car_no");
            })
            ->where([
                ["$tbl_res.season", '=', $season],
                ["$tbl_res.series", '=', FrameworkConfig::get('debear.section.code')]
            ])
            ->whereIn("$tbl_racepart.team_id", $team_ids)
            ->get();
        // Bind to the teams.
        $raw = $this->secondary['raceResults']->pluck('team_id');
        $key_lookup = array_fill_keys($team_ids, []);
        foreach ($raw as $id => $team_id) {
            $key_lookup[$team_id][] = $id;
        }
        foreach ($this->data as $i => $team) {
            $this->data[$i]['results'] = $this->secondary['raceResults']->getByKeys($key_lookup[$team['team_id']]);
        }
        // Do we also have raceParticipants to bind to?
        if (isset($this->secondary['raceParticipants'])) {
            $raw = $this->secondary['raceResults']->pluck('part_key');
            $key_lookup = array_fill_keys($this->secondary['raceParticipants']->pluck('part_key'), []);
            foreach ($raw as $id => $part_key) {
                $key_lookup[$part_key][] = $id;
            }
            foreach ($this->secondary['raceParticipants'] as $race_part) {
                $race_part->results = $this->secondary['raceResults']->getByKeys($key_lookup[$race_part->part_key]);
            }
        }
    }

    /**
     * Load the information about the participants taking part for each team.
     * @return void
     */
    protected function loadSecondaryParticipants(): void
    {
        $driver_ids = $this->secondary['raceParticipants']->pluck('driver_id');
        $this->secondary['participants'] = Participant::query()
            ->whereIn('driver_id', $driver_ids)
            ->get();
        $key_lookup = array_flip($this->secondary['participants']->pluck('driver_id'));
        foreach ($this->secondary['raceParticipants'] as $race_part) {
            $race_part->participant = $this->secondary['participants']->getRow($key_lookup[$race_part->driver_id]);
        }
    }
}
