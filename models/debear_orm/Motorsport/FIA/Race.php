<?php

namespace DeBear\ORM\Sports\Motorsport\FIA;

use DeBear\ORM\Sports\Motorsport\Race as BaseRace;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\ORM\Skeleton\WeatherForecast;

class Race extends BaseRace
{
    /**
     * SET columns and their values
     * @var array
     */
    protected $sets = [
        'quirks' => ['sprint_race', 'sprint_shootout'],
    ];

    /**
     * Summary details about the individual race
     * @return array Key/Value pair of details to add to the top of the individual race page
     */
    public function headerInfo(): array
    {
        $sprint_race = $this->quirk('sprint_race');
        $race_distance = ($this->race_laps_completed * $this->lap_length);
        $info = [
            'Circuit' => $this->venue,
            'Lap Length' => $this->lap_length . 'km',
            '-',
            'Qualifying' => $this->qual_time->format('l jS F Y'),
            '-',
            ($sprint_race ? 'Sprint ' : '') . 'Race' => $this->race_time->format('l jS F Y'),
            ($sprint_race ? 'Sprint Race ' : '') . 'Laps' => $this->race_laps_completed,
            ($sprint_race ? 'Sprint ' : '') . 'Race Distance' => $race_distance . 'km',
        ];
        if (isset($this->race2_time)) {
            $is_race2 = (!$sprint_race ? ' 2' : '');
            $info = array_merge($info, [
                '-',
                "Race$is_race2" => $this->race2_time->format('l jS F Y'),
                "Race$is_race2 Laps" => $this->race2_laps_completed,
                "Race$is_race2 Distance" => ($this->race2_laps_completed * $this->lap_length) . 'km',
            ]);
        }
        return $info;
    }

    /**
     * State where the race is to take place
     * @return string The summarised location details
     */
    public function getVenueAttribute(): string
    {
        return join(', ', array_unique([$this->circuit, $this->location]));
    }

    /**
     * The section endpoint that applies to this race record
     * @return string The URL component to use for this race record
     */
    public function sectionEndpoint(): string
    {
        return $this->series ?? parent::sectionEndpoint();
    }

    /**
     * Get the two races to focus on on the homepage
     * @return array An array with two elements, either last/next or last two (if no known next)
     */
    public static function getHomepageFocus(): array
    {
        // Get a potential future race.
        $next = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", series, "-", flag) AS weather_key')
            ->where('series', '=', FrameworkConfig::get('debear.section.code'))
            ->whereNull('race_laps_completed')
            ->orderBy('race_time')
            ->limit(1)
            ->get()
            ->loadSecondary([
                'forecast',
            ]);
        // Get the last two races, so we can determine which to use.
        $prev = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", series, "-", flag) AS weather_key')
            ->where('series', '=', FrameworkConfig::get('debear.section.code'))
            ->whereNotNull('race_laps_completed')
            ->orderByDesc('race_time')
            ->limit(2)
            ->get()
            ->loadSecondary([
                'forecast',
                'raceParticipants',
                'participants',
                'teams',
                'results',
            ]);
        // Build our return object.
        $ret = [
            // A previous race. If no next, it's the previous previous.
            'left' => $next->count() ? $prev->first() : $prev->last(),
            // The next race, unless we have no next in which case it's the last race that occurred.
            'right' => $next->count() ? $next : $prev->first(),
        ];
        return $ret;
    }

    /**
     * Get the list of all completed races for the XML sitemap
     * @return self The list of Race objects of all the completed races stored in the database
     */
    public static function getAllCompleted(): self
    {
        return static::query()
            ->where([
                ['series', '=', FrameworkConfig::get('debear.section.code')],
                ['race_laps_completed', '>', 0],
            ])->orderBy('season')
            ->orderBy('round_order')
            ->get();
    }

    /**
     * Load the calendar of races for a given season
     * @param integer $season    The season to load.
     * @param array   $secondary Additional eager loading list.
     * @return self Object of (ordered) races for that season
     */
    public static function loadCalendar(int $season, array $secondary = []): self
    {
        // Get our raw races list.
        $calendar = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", series, "-", flag) AS weather_key')
            ->where([
                ['season', '=', $season],
                ['series', '=', FrameworkConfig::get('debear.section.code')],
            ])->orderBy('round_order')
            ->get();

        // Then the secondary data.
        if (count($calendar)) {
            $calendar->loadSecondary(array_merge([
                'raceParticipants',
                'participants',
                'teams',
                'results',
                'fastestLap',
            ], $secondary));
        }

        // Return what we have built.
        return $calendar;
    }

    /**
     * Get the list of races from the previous season
     * @param integer $season The current season we are viewing.
     * @return self The list of Race objects of all the races the previous season
     */
    public static function previousCalendar(int $season): self
    {
        return static::loadCalendar($season - 1, [
            'participantsStandingsCurrent',
            'teamsStandingsCurrent',
            'qualifying',
            'grid',
        ]);
    }

    /**
     * Load an individual race for rendering as the individual results
     * @param integer $season The season to load.
     * @param integer $round  The round ID of the race to load.
     * @return self Object, with appropriate additional info eager loaded, for the race to render
     */
    public static function loadRace(int $season, int $round): self
    {
        // The primary info (as a collection of a single race we'll reduce later).
        $race = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", series, "-", flag) AS weather_key')
            ->where([
                ['season', '=', $season],
                ['series', '=', FrameworkConfig::get('debear.section.code')],
                ['round', '=', $round],
            ])
            ->get();

        // Then the secondary data.
        if (count($race)) {
            $race->loadSecondary([
                'raceParticipants',
                'participants',
                'participantsStandingsCurrent',
                'participantsStandingsRace',
                'teams',
                'teamsStandingsCurrent',
                'teamsStandingsRace',
                'qualifying',
                'grid',
                'results',
                'lapLeaders',
                'fastestLap',
            ]);
        }

        // Return what we have built.
        return $race;
    }

    /**
     * The mappings for our default secondary loader
     * @return array The mappings keyed by secondary name
     */
    protected function loadSecondaryDefaultMaps(): array
    {
        return [
            'raceParticipants' => [
                'class' => RaceParticipant::class,
            ],
            'qualifying' => [
                'class' => RaceQuali::class,
                'select' => ['CAST(RIGHT(session, 1) AS UNSIGNED) AS session_num'],
                'sort' => 'season, series, round, session, pos',
                'link' => ['raceParticipants' => 'raceParticipant'],
            ],
            'grid' => [
                'class' => RaceGrid::class,
                'sort' => 'season, series, round, race, grid_pos',
                'link' => [
                    'raceParticipants' => 'raceParticipant',
                    'qualifying' => 'qualifying',
                ],
            ],
            'results' => [
                'class' => RaceResult::class,
                'sort' => 'season, series, round, race, pos',
                'link' => ['raceParticipants' => 'raceParticipant'],
            ],
            'lapLeaders' => [
                'class' => RaceLapLeaders::class,
                'sort' => 'season, series, round, race, lap_from',
                'link' => ['raceParticipants' => 'raceParticipant'],
            ],
            'fastestLap' => [
                'class' => RaceFastestLap::class,
                'link' => ['raceParticipants' => 'raceParticipant'],
            ],
        ];
    }

    /**
     * The default handler for loading secondary race information.
     * @param string $name The requested secondary race information.
     * @return void
     */
    protected function loadSecondaryDefault(string $name): void
    {
        // Some standardised methods affecting multiple $name.
        if (preg_match('/^(?:participants|teams)StandingsCurrent$/', $name)) {
            $this->loadSecondaryEntityCurrentStanding($name);
            return;
        } elseif (preg_match('/^(?:participants|teams)StandingsRace$/', $name)) {
            $this->loadSecondaryEntityRaceStanding($name);
            return;
        }

        // In all other instances, we request all the info per-race.
        // Define our specific mapping details.
        $maps = $this->loadSecondaryDefaultMaps();
        $map = $maps[$name];
        // Get the data for all matching race(s).
        $query = $map['class']::query();
        $tbl = $query->from;
        $query = $query->select("$tbl.*")
            ->selectRaw("CONCAT($tbl.season, '-', $tbl.series, '-', $tbl.round) AS race_key")
            ->selectRaw("CONCAT($tbl.season, '-', $tbl.series, '-', $tbl.round, '-', $tbl.car_no) AS part_key");
        if (isset($map['select']) && is_array($map['select'])) {
            foreach ($map['select'] as $select) {
                $query = $query->selectRaw($select);
            }
        }
        // Build the where clause, depending on the number of seasons loaded.
        $seasons = $this->unique('season');
        // Assumes a single season (a simpler query!).
        $query = $query->where([
            ['season', '=', $seasons[0]],
            ['series', '=', FrameworkConfig::get('debear.section.code')],
        ])->whereIn('round', $this->pluck('round'));
        // Any sorting?
        if (isset($map['sort'])) {
            $query = $query->orderByRaw($map['sort']);
        }
        // Get the data, and distribute across the races.
        $instance = ($map['name'] ?? $name);
        $this->secondary[$name] = $query->get();
        // Any data to link at this level?
        if (isset($map['link'])) {
            foreach ($map['link'] as $link_from => $link_to) {
                $link_from = $this->secondary[$link_from];
                // Map our part_key's to data indices.
                if ($link_to == 'qualifying') {
                    // An exception - 1:M.
                    $method = 'getByKeys';
                    $raw = $link_from->pluck('part_key');
                    $key_lookup = array_fill_keys(array_unique($raw), []);
                    foreach ($raw as $id => $rider_id) {
                        $key_lookup[$rider_id][] = $id;
                    }
                } else {
                    // Standard - 1:1.
                    $method = 'getRow';
                    $key_lookup = array_flip($link_from->pluck('part_key'));
                }
                // Now tie together.
                foreach ($this->secondary[$name] as $item) {
                    $match = isset($key_lookup[$item->part_key]);
                    $method_name = $match ? $method : 'where';
                    $method_args = $match ? [$key_lookup[$item->part_key]] : ['unknown-key', 1];
                    $item->$link_to = $link_from->$method_name(...$method_args);
                }
            }
        }
        // Then tie to the races.
        foreach ($this->data as $i => $race) {
            $this->data[$i][$instance] = $this->secondary[$name]->where('race_key', $race['race_key']);
        }
    }

    /**
     * Load the appropriate weather information
     * @return void
     */
    protected function loadSecondaryForecast(): void
    {
        $instances = [];
        foreach ($this->data as $race) {
            $base = "{$race['season']}-{$race['series']}-{$race['flag']}";
            $instances[] = "$base-quali";
            $instances[] = "$base-race";
            $instances[] = "$base-race2";
        }
        $weather = WeatherForecast::query()
            ->where('app', '=', FrameworkConfig::get('debear.weather.app'))
            ->whereIn('instance_key', $instances)
            ->get();
        // Then tie to the races.
        foreach ($this->data as $i => $race) {
            $this->data[$i]['forecast'] = (object)[
                'quali' => $weather->where('instance_key', "{$race['weather_key']}-quali"),
                'race' => $weather->where('instance_key', "{$race['weather_key']}-race"),
                'race2' => $weather->where('instance_key', "{$race['weather_key']}-race2"),
            ];
        }
    }

    /**
     * Load the information about the participants taking part in the races.
     * @return void
     */
    protected function loadSecondaryParticipants(): void
    {
        $driver_ids = $this->secondary['raceParticipants']->unique('driver_id');
        $this->secondary['participants'] = Participant::query()
            ->whereIn('driver_id', $driver_ids)
            ->get();
        foreach ($driver_ids as $driver_id) {
            $driver = $this->secondary['participants']->where('driver_id', $driver_id);
            foreach ($this->secondary['raceParticipants']->where('driver_id', $driver_id) as $race_part) {
                $race_part->participant = $driver;
            }
        }
    }

    /**
     * Load the information about the teams taking part in the races.
     * @return void
     */
    protected function loadSecondaryTeams(): void
    {
        $team_ids = $this->secondary['raceParticipants']->unique('team_id');
        $rand = $this->secondary['raceParticipants']->random();
        $this->secondary['teams'] = Team::query()
            ->where('season', $rand->season)
            ->where('series', $rand->series)
            ->whereIn('team_id', $team_ids)
            ->get();
        foreach ($team_ids as $team_id) {
            $team = $this->secondary['teams']->where('team_id', $team_id);
            foreach ($this->secondary['raceParticipants']->where('team_id', $team_id) as $race_part) {
                $race_part->team = $team;
            }
        }
    }

    /**
     * Load the specific standings for participants/teams immediately after the races.
     * @param string $name The name of the additional data requested.
     * @return void
     */
    protected function loadSecondaryEntityRaceStanding(string $name): void
    {
        // Participant standings after this specific race.
        $name_pri = substr($name, 0, -13);
        // Get the data.
        $classes = [
            'participants' => [
                'class' => ParticipantHistoryProgress::class,
                'key' => 'driver_id',
            ],
            'teams' => [
                'class' => TeamHistoryProgress::class,
                'key' => 'team_id',
            ],
        ];
        $class = $classes[$name_pri];
        // Determine the round_order's to load.
        $orders_curr = $this->unique('round_order');
        $orders_prev = array_filter(array_map(function ($order) {
            return $order - 1;
        }, $orders_curr));
        $orders_all = array_unique(array_merge($orders_curr, $orders_prev));
        // Build and run the query.
        $tbl_stand = $class['class']::getTable();
        $tbl_race = static::getTable();
        $rand = $this->random();
        $this->secondary[$name] = $class['class']::query()
            ->join($tbl_race, function ($join) use ($tbl_stand, $tbl_race) {
                $join->on("$tbl_stand.season", '=', "$tbl_race.season")
                    ->on("$tbl_stand.series", '=', "$tbl_race.series")
                    ->on("$tbl_stand.round", '=', "$tbl_race.round")
                    ->on("$tbl_stand.race", '=', DB::raw("IF($tbl_race.race2_time IS NULL, 1, 2)"));
            })
            ->select("$tbl_stand.*", "$tbl_race.round_order")
            ->selectRaw("CONCAT($tbl_stand.season, '-', $tbl_stand.series, '-', $tbl_stand.{$class['key']}, "
                . "'-', $tbl_race.round_order) AS part_key")
            ->selectRaw("CONCAT($tbl_stand.season, '-', $tbl_stand.series, '-', $tbl_stand.{$class['key']}, "
                . "'-', $tbl_race.round_order - 1) AS part_key_prev")
            ->where([
                ["$tbl_stand.season", '=', $rand->season],
                ["$tbl_stand.series", '=', $rand->series],
            ])
            ->whereIn("$tbl_race.round_order", $orders_all)
            ->orderByRaw("$tbl_stand.season, $tbl_stand.series, $tbl_stand.{$class['key']}, $tbl_race.round_order")
            ->get();
        // Distribute across the entities.
        $adding = $this->secondary[$name]->unique($class['key']);
        foreach ($adding as $entity_id) {
            $entity = $this->secondary[$name_pri]->where($class['key'], $entity_id);
            $entity->standingsRace = $this->secondary[$name]->where($class['key'], $entity_id);
        }
    }

    /**
     * Load the current standings for participants/teams in the races.
     * @param string $name The name of the secondary data requested.
     * @return void
     */
    protected function loadSecondaryEntityCurrentStanding(string $name): void
    {
        // Current Participant / Team standings.
        $name_pri = substr($name, 0, -16);
        // Get the data.
        $classes = [
            'participants' => [
                'class' => ParticipantHistory::class,
                'class_ind' => Participant::class,
                'key' => 'driver_id',
            ],
            'teams' => [
                'class' => TeamHistory::class,
                'class_ind' => Team::class,
                'key' => 'team_id',
            ],
        ];
        $class = $classes[$name_pri];
        // Build and run the query.
        $rand = $this->random();
        $this->secondary[$name] = $class['class']::query()
            ->where([
                ['season', '=', $rand->season],
                ['series', '=', $rand->series],
            ])
            ->get();
        // How about additional participants not already loaded?
        $loaded = $this->secondary[$name_pri]->unique($class['key']);
        $adding = $this->secondary[$name]->unique($class['key']);
        $missing_ids = array_diff($adding, $loaded);
        if ($missing_ids) {
            $missing = $class['class_ind']::query()
                ->whereIn($class['key'], $missing_ids)
                ->get();
            $this->secondary[$name_pri]->merge($missing);
        }
        // Distribute across the entities.
        foreach ($adding as $entity_id) {
            $entity = $this->secondary[$name_pri]->where($class['key'], $entity_id);
            $entity->standingsCurr = $this->secondary[$name]->where($class['key'], $entity_id);
        }
    }

    /**
     * Build the list of events to display on the global homepage
     * @param array  $date_maps An array of dates we are focusing on.
     * @param string $series    The race series we are procssing.
     * @return array The appropriate events to focus on
     */
    public static function getHomepageEvents(array $date_maps, string $series): array
    {
        $ret = array_fill_keys(array_values($date_maps), []);
        // Parse each session.
        $sessions = [
            'qual' => ['qualifying', 'grid'],
            'race' => ['results'],
            'race2' => ['results'],
        ];
        foreach ($sessions as $key => $secondary) {
            $data = static::query()
                ->select('*')
                ->selectRaw('CONCAT(season, "-", series, "-", round) AS race_key')
                ->selectRaw('"motorsport" AS template')
                ->selectRaw('? AS template_icon', [$series])
                ->selectRaw('? AS template_type', [$key])
                ->selectRaw("DATE({$key}_time) AS parse_date")
                ->where('series', $series)
                ->whereIn(DB::raw("DATE({$key}_time)"), array_keys($date_maps))
                ->get()
                ->loadSecondary(array_merge(['raceParticipants', 'participants', 'teams'], $secondary));

            // Add any rows to our return object.
            foreach ($data->unique('parse_date') as $d) {
                $ret[$date_maps[$d]][] = $data->where('parse_date', $d);
            }
        }
        return $ret;
    }
}
