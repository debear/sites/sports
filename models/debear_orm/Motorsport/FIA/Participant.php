<?php

namespace DeBear\ORM\Sports\Motorsport\FIA;

use DeBear\ORM\Sports\Motorsport\Participant as BaseParticipant;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class Participant extends BaseParticipant
{
    /**
     * Get the numeric ID for this participant
     * @return integer The particpant's ID
     */
    public function getIDAttribute(): int
    {
        return $this->driver_id;
    }

    /**
     * Get the list of all participants for the XML sitemap
     * @return self The participants as collection
     */
    public static function getAllHistorical(): self
    {
        $tbl_part = static::getTable();
        $tbl_racepart = RaceParticipant::getTable();
        return static::query()
            ->selectRaw("$tbl_part.*, MAX($tbl_racepart.season) AS max_season")
            ->join($tbl_racepart, function ($join) use ($tbl_part, $tbl_racepart) {
                return $join->where("$tbl_racepart.season", '>=', FrameworkConfig::get('debear.setup.season.min'))
                    ->where("$tbl_racepart.series", '=', FrameworkConfig::get('debear.section.code'))
                    ->on("$tbl_racepart.driver_id", '=', "$tbl_part.driver_id");
            })->groupBy("$tbl_part.driver_id")
            ->orderBy("$tbl_part.surname")
            ->orderBy("$tbl_part.first_name")
            ->get();
    }

    /**
     * Try and find a participant passed in by codified name and ID
     * @param integer $driver_id   The ID of the participant being looked up.
     * @param string  $driver_name The codified name being looked up.
     * @param integer $season      The season being requested (which may not be available, but isn't a blocker).
     * @return ?self The found (shortened) matching info, or null if not found
     */
    public static function findByCodifiedID(int $driver_id, string $driver_name, int $season): ?self
    {
        // Get the list of possible options.
        $min_season = FrameworkConfig::get('debear.setup.season.min');
        $tbl_part = static::getTable();
        $tbl_parthist = ParticipantHistory::getTable();
        $all_names = static::query()
            ->select(
                "$tbl_part.driver_id",
                "$tbl_part.first_name",
                "$tbl_part.surname"
            )
            ->selectRaw("MIN(IF($tbl_parthist.season >= ?, $tbl_parthist.season, NULL)) AS first_season", [
                $min_season,
            ])
            ->selectRaw("MAX(IF($tbl_parthist.season >= ?, $tbl_parthist.season, NULL)) AS last_season", [
                $min_season,
            ])
            ->selectRaw("MAX($tbl_parthist.season >= ? AND $tbl_parthist.season = ?) AS inc_season", [
                $min_season,
                $season,
            ])
            ->join($tbl_parthist, function ($join) use ($tbl_parthist, $tbl_part) {
                return $join->on("$tbl_parthist.driver_id", '=', "$tbl_part.driver_id")
                    ->where("$tbl_parthist.series", FrameworkConfig::get('debear.section.code'));
            })
            ->where("$tbl_part.driver_id", $driver_id)
            ->groupBy("$tbl_part.surname", "$tbl_part.first_name")
            ->orderByDesc('inc_season')
            ->get();
        foreach ($all_names as $name) {
            // If we have a match then use what we've found a potential participant.
            if ($name->name_codified == $driver_name) {
                $match = $name->getCurrent();
                break;
            }
        }
        // Return what we find.
        if (!isset($match)) {
            // No match.
            return null;
        }
        $season_query = $match->inc_season
            ? $season
            : ($season > $match->last_season
                ? $match->last_season
                : $match->first_season);
        return static::query()
            ->select('*')
            ->selectRaw("$season_query AS season")
            ->where([
                ['driver_id', $match->driver_id],
            ])->get();
    }

    /**
     * Load the full information about a given participant
     * @param self $found The abbreviated participant info from the validation phase.
     * @return self The fully loaded information for the single participant
     */
    public static function load(self $found): self
    {
        return static::query()
            ->select('*')
            ->selectRaw("{$found->season} AS season")
            ->where([
                ['driver_id', '=', $found->driver_id],
            ])->get()
            ->loadSecondary([
                'standings',
                'history',
                'historyProgress',
                'historyCalendar',
                'historyResults',
                'historyTeams',
                'legacyResults',
                'legacyTeams',
                'seasonStats',
            ]);
    }

    /**
     * The default handler for loading secondary participant information.
     * @param string $name The requested secondary participant information.
     * @return void
     */
    protected function loadSecondaryDefault(string $name): void
    {
        $maps = [
            'standings' => [
                'class' => ParticipantHistory::class,
                'name' => 'standingsCurr',
            ],
        ];
        $map = $maps[$name];
        $season = $this->first()->season;
        $driver_ids = $this->unique('driver_id');
        // Build our query and get the data.
        $query = $map['class']::query();
        $this->secondary[$name] = $query->where([
                ['season', '=', $season],
                ['series', '=', FrameworkConfig::get('debear.section.code')]
            ])
            ->whereIn('driver_id', $driver_ids)
            ->get();
        // Tie back to the participants.
        $lookup = array_flip($this->secondary[$name]->pluck('driver_id'));
        $instance = ($map['name'] ?? $name);
        foreach ($this->data as $i => $driver) {
            $this->data[$i][$instance] = $this->secondary[$name]->getRow($lookup[$driver['driver_id']]);
        }
    }

    /**
     * Load the information about the history for each participant.
     * @return void
     */
    protected function loadSecondaryHistory(): void
    {
        $driver_ids = $this->unique('driver_id');
        $this->secondary['history'] = ParticipantHistory::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series) AS series_key')
            ->selectRaw('CONCAT(season, "-", series, "-", driver_id) AS season_key')
            ->selectRaw('(season >= ?) AS is_detailed', [FrameworkConfig::get('debear.setup.season.min')])
            ->selectRaw('1 AS is_relevant')
            ->whereIn('driver_id', $driver_ids)
            ->orderByDesc('season')
            ->orderBy('series')
            ->get();
        $raw = $this->secondary['history']->pluck('driver_id');
        $lookup = array_fill_keys($driver_ids, []);
        foreach ($raw as $id => $driver_id) {
            $lookup[$driver_id][] = $id;
        }
        foreach ($this->data as $i => $driver) {
            $this->data[$i]['history'] = $this->secondary['history']->getByKeys($lookup[$driver['driver_id']]);
        }
    }

    /**
     * Load the information about the driver's progress across the historical seasons we have info for..
     * @return void
     */
    protected function loadSecondaryHistoryProgress(): void
    {
        $series_keys = $this->secondary['history']->unique('series_key');
        $season_keys = $this->secondary['history']->unique('season_key');
        $season_keys_bind = join(',', array_fill(0, count($season_keys), '?'));
        $this->secondary['historyProgress'] = ParticipantHistoryProgress::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series) AS series_key')
            ->selectRaw('CONCAT(round, "-", race) AS race_key')
            ->whereRaw('CONCAT(season, "-", series, "-", driver_id) IN (' . $season_keys_bind . ')', $season_keys)
            ->orderBy('season')
            ->orderBy('series')
            ->orderBy('round')
            ->orderBy('race')
            ->get();
        $raw = $this->secondary['historyProgress']->pluck('series_key');
        $lookup = array_fill_keys($series_keys, []);
        foreach ($raw as $id => $series_key) {
            $lookup[$series_key][] = $id;
        }
        foreach ($this->secondary['history'] as $history) {
            $history->progress = $this->secondary['historyProgress']->getByKeys($lookup[$history->series_key]);
        }
    }

    /**
     * Load the information about the race history for each participant that we only have summary details about.
     * @return void
     */
    protected function loadSecondaryHistoryCalendar(): void
    {
        $series_keys = $this->secondary['history']->unique('series_key');
        $series_keys_bind = join(',', array_fill(0, count($series_keys), '?'));
        $this->secondary['historyCalendar'] = Race::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", series) AS series_key')
            ->selectRaw('CONCAT(season, "-", series, "-", round) AS race_key')
            ->whereRaw('CONCAT(season, "-", series) IN (' . $series_keys_bind . ')', $series_keys)
            ->orderBy('season')
            ->orderBy('series')
            ->orderBy('round_order')
            ->get();
        $raw = $this->secondary['historyCalendar']->pluck('series_key');
        $lookup = array_fill_keys($series_keys, []);
        foreach ($raw as $id => $series_key) {
            $lookup[$series_key][] = $id;
        }
        foreach ($this->secondary['history'] as $history) {
            $history->calendar = $this->secondary['historyCalendar']->getByKeys($lookup[$history->series_key]);
        }
    }

    /**
     * Load the information about the teams in each participant's detailed history.
     * @return void
     */
    protected function loadSecondaryHistoryTeams(): void
    {
        $this->loadSecondaryTeamsWorker('history');
    }

    /**
     * Load the information about the teams in each participant's legacy history.
     * @return void
     */
    protected function loadSecondaryLegacyTeams(): void
    {
        $this->loadSecondaryTeamsWorker('legacy');
    }

    /**
     * Worker method to determine the participant's teams and link.
     * @param string $type Whether we're viewing detailed ('history') or historical ('legacy') teams.
     * @return void
     */
    protected function loadSecondaryTeamsWorker(string $type): void
    {
        $is_legacy = ($type == 'legacy');
        $is_legacy_int = 1 - (int)$is_legacy;
        $sec_team_key = "{$type}Teams";
        $sec_res_key = "{$type}Results";
        // Determine who to load.
        $season_keys = $this->secondary['history']
            ->where('is_detailed', $is_legacy_int)
            ->unique('season_key');
        if (!count($season_keys)) {
            // Skip if no legacy (probably) rows.
            return;
        }
        $season_keys_bind = join(',', array_fill(0, count($season_keys), '?'));
        // And load them, with order details within a season.
        $tbl_team = Team::getTable();
        $tbl_racepart = ($is_legacy ? RaceHistorical::getTable() : RaceParticipant::getTable());
        $tbl_race = Race::getTable();
        $this->secondary[$sec_team_key] = Team::query()
            ->select("$tbl_team.*")
            ->selectRaw("MIN(IFNULL($tbl_race.round_order, $tbl_racepart.round)) AS first_round")
            ->selectRaw(
                "CONCAT($tbl_racepart.season, '-', $tbl_racepart.series, '-', $tbl_racepart.driver_id) AS season_key"
            )
            ->selectRaw(
                "CONCAT($tbl_racepart.season, '-', $tbl_racepart.series, '-', $tbl_racepart.team_id) AS team_key"
            )
            ->join($tbl_racepart, function ($join) use ($tbl_racepart, $tbl_team) {
                return $join->on("$tbl_racepart.season", '=', "$tbl_team.season")
                    ->on("$tbl_racepart.series", '=', "$tbl_team.series")
                    ->on("$tbl_racepart.team_id", '=', "$tbl_team.team_id");
            })
            ->leftJoin($tbl_race, function ($join) use ($tbl_race, $tbl_racepart) {
                return $join->on("$tbl_race.season", '=', "$tbl_racepart.season")
                    ->on("$tbl_race.series", '=', "$tbl_racepart.series")
                    ->on("$tbl_race.round", '=', "$tbl_racepart.round");
            })
            ->whereRaw(
                "CONCAT($tbl_racepart.season, '-', $tbl_racepart.series, '-', $tbl_racepart.driver_id) IN "
                    . "($season_keys_bind)",
                $season_keys
            )
            ->groupBy("$tbl_team.season", "$tbl_team.series", "$tbl_team.team_id")
            ->orderByDesc("$tbl_team.season")
            ->orderBy("$tbl_team.series")
            ->orderBy('first_round')
            ->get();
        // For teams with detailed history, we need a little more information too.
        if ($type == 'history') {
            $this->secondary[$sec_team_key]->loadSecondary(['standings']);
        }
        // Bind the results to this team.
        $raw = $this->secondary[$sec_res_key]->pluck('team_key');
        $lookup = array_fill_keys($raw, []);
        foreach ($raw as $id => $team_id) {
            $lookup[$team_id][] = $id;
        }
        foreach ($this->secondary[$sec_team_key] as $team) {
            $team->results = $this->secondary[$sec_res_key]->getByKeys($lookup[$team->team_key] ?? []);
        }
        // Bind to the season.
        $raw = $this->secondary[$sec_team_key]->pluck('season_key');
        $lookup = array_fill_keys($raw, []);
        foreach ($raw as $id => $team_id) {
            $lookup[$team_id][] = $id;
        }
        foreach ($this->secondary['history'] as $history) {
            if ($history->is_detailed == $is_legacy_int) {
                $history->teams = $this->secondary[$sec_team_key]->getByKeys($lookup[$history->season_key] ?? []);
            }
        }
    }

    /**
     * Load the information about the stats for each participant.
     * @return void
     */
    protected function loadSecondarySeasonStats(): void
    {
        $driver_ids = $this->unique('driver_id');
        $tbl_partstats = ParticipantStats::getTable();
        $tbl_stats = Stats::getTable();
        $this->secondary['seasonStats'] = ParticipantStats::query()
            ->select(
                "$tbl_partstats.*",
                "$tbl_stats.section",
                "$tbl_stats.name_short",
                "$tbl_stats.name_full",
                "$tbl_stats.type",
                "$tbl_stats.disp_order"
            )
            ->selectRaw(
                "CONCAT($tbl_partstats.season, '-', $tbl_partstats.series, '-', $tbl_partstats.driver_id) AS season_key"
            )
            ->join($tbl_stats, "$tbl_stats.stat_id", '=', "$tbl_partstats.stat_id")
            ->where("$tbl_partstats.series", FrameworkConfig::get('debear.section.code'))
            ->whereIn("$tbl_partstats.driver_id", $driver_ids)
            ->orderByDesc("$tbl_partstats.season")
            ->orderBy("$tbl_stats.disp_order")
            ->get();
        $raw = $this->secondary['seasonStats']->pluck('driver_id');
        $lookup = array_fill_keys($driver_ids, []);
        foreach ($raw as $id => $driver_id) {
            $lookup[$driver_id][] = $id;
        }
        foreach ($this->data as $i => $team) {
            $this->data[$i]['seasonStats'] = $this->secondary['seasonStats']
                ->getByKeys($lookup[$team['driver_id']]);
        }
        // Do we also have a history to bind to?
        if (isset($this->secondary['history'])) {
            $raw = $this->secondary['seasonStats']->pluck('season_key');
            $lookup = array_fill_keys($this->secondary['history']->pluck('season_key'), []);
            foreach ($raw as $id => $season_key) {
                $lookup[$season_key][] = $id;
            }
            foreach ($this->secondary['history'] as $history) {
                $history->stats = $this->secondary['seasonStats']->getByKeys($lookup[$history->season_key]);
            }
        }
    }

    /**
     * Load the information about the races taken part by each participant this season.
     * @return void
     */
    protected function loadSecondaryRaceResults(): void
    {
        $this->loadSecondaryResultsWorker('raceResults');
    }

    /**
     * Load the information about the races taken part by each participant across their entire career.
     * @return void
     */
    protected function loadSecondaryHistoryResults(): void
    {
        $this->loadSecondaryResultsWorker('historyResults');
        // Do we also have a history to bind to?
        if (isset($this->secondary['history'])) {
            $raw = $this->secondary['historyResults']->pluck('driver_key');
            $lookup = array_fill_keys($this->secondary['history']->pluck('season_key'), []);
            foreach ($raw as $id => $season_key) {
                $lookup[$season_key][] = $id;
            }
            foreach ($this->secondary['history'] as $history) {
                if ($history->is_detailed) {
                    $history->results = $this->secondary['historyResults']->getByKeys($lookup[$history->season_key]);
                }
            }
        }
    }

    /**
     * Worker method to build consistent result objects.
     * @param string $name The targetted results name.
     * @return void
     */
    protected function loadSecondaryResultsWorker(string $name): void
    {
        $all = ($name == 'historyResults');
        $driver_ids = $this->unique('driver_id');
        $tbl_racepart = RaceParticipant::getTable();
        $tbl_res = RaceResult::getTable();
        $tbl_grid = RaceGrid::getTable();
        $tbl_fl = RaceFastestLap::getTable();
        $query = RaceResult::query()
            ->select("$tbl_res.*", "$tbl_racepart.driver_id", "$tbl_racepart.team_id", "$tbl_grid.grid_pos")
            ->selectRaw("$tbl_fl.lap_num IS NOT NULL AS fastest_lap")
            ->selectRaw("CONCAT($tbl_res.round, '-', $tbl_res.race) AS race_key")
            ->selectRaw(
                "CONCAT($tbl_res.season, '-', $tbl_res.series, '-', $tbl_racepart.team_id, '-', $tbl_res.round, "
                    . "'-', $tbl_res.race) AS full_key"
            )
            ->selectRaw(
                "CONCAT($tbl_res.season, '-', $tbl_res.series, '-', $tbl_racepart.driver_id) AS driver_key"
            )
            ->selectRaw(
                "CONCAT($tbl_res.season, '-', $tbl_res.series, '-', $tbl_racepart.team_id) AS team_key"
            )
            ->join($tbl_racepart, function ($join) use ($tbl_racepart, $tbl_res) {
                return $join->on("$tbl_racepart.season", '=', "$tbl_res.season")
                    ->on("$tbl_racepart.series", '=', "$tbl_res.series")
                    ->on("$tbl_racepart.round", '=', "$tbl_res.round")
                    ->on("$tbl_racepart.car_no", '=', "$tbl_res.car_no");
            })
            ->leftJoin($tbl_grid, function ($join) use ($tbl_grid, $tbl_res) {
                return $join->on("$tbl_grid.season", '=', "$tbl_res.season")
                    ->on("$tbl_grid.series", '=', "$tbl_res.series")
                    ->on("$tbl_grid.round", '=', "$tbl_res.round")
                    ->on("$tbl_grid.race", '=', "$tbl_res.race")
                    ->on("$tbl_grid.car_no", '=', "$tbl_res.car_no");
            })
            ->leftJoin($tbl_fl, function ($join) use ($tbl_fl, $tbl_res) {
                return $join->on("$tbl_fl.season", '=', "$tbl_res.season")
                    ->on("$tbl_fl.series", '=', "$tbl_res.series")
                    ->on("$tbl_fl.round", '=', "$tbl_res.round")
                    ->on("$tbl_fl.race", '=', "$tbl_res.race")
                    ->on("$tbl_fl.car_no", '=', "$tbl_res.car_no");
            });
        // Now build up our where clause (dependent on the participant info loaded).
        if (!$all) {
            // All the same season, which makes for a simpler query.
            $season = $this->first()->season;
            $query = $query->where([
                    ["$tbl_racepart.season", '=', $season],
                    ["$tbl_racepart.series", '=', FrameworkConfig::get('debear.section.code')],
                ])
                ->whereIn("$tbl_racepart.driver_id", $driver_ids);
        } else {
            // A mix of seasons, so target specific keys.
            $query = $query->whereIn("$tbl_racepart.driver_id", $driver_ids)
                ->orderBy("$tbl_res.season")
                ->orderBy("$tbl_res.series")
                ->orderBy("$tbl_racepart.driver_id")
                ->orderBy("$tbl_res.round")
                ->orderBy("$tbl_res.race");
        }
        // Finally, run the query.
        $this->secondary[$name] = $query->get();
        // Bind to the participants.
        $raw = $this->secondary[$name]->pluck('driver_id');
        $lookup = array_fill_keys($driver_ids, []);
        foreach ($raw as $id => $driver_id) {
            $lookup[$driver_id][] = $id;
        }
        foreach ($this->data as $i => $driver) {
            $this->data[$i]['results'] = $this->secondary[$name]->getByKeys($lookup[$driver['driver_id']]);
        }
    }

    /**
     * Load the information about the race history for each participant that we only have summary details about.
     * @return void
     */
    protected function loadSecondaryLegacyResults(): void
    {
        $driver_ids = $this->unique('driver_id');
        $this->secondary['legacyResults'] = RaceResult::query()
            ->from(RaceHistorical::getTable()) // Shape-shift from RaceHistorical table to RaceResult object.
            ->select(
                '*',
                'round AS round_order',
                'gp_flag AS flag',
                'qual AS grid_pos'
            )
            ->selectRaw('CONCAT(season, "-", series, "-", team_id, "-", round, "-1") AS full_key')
            ->selectRaw('CONCAT(season, "-", series, "-", driver_id) AS driver_key')
            ->selectRaw('CONCAT(season, "-", series, "-", team_id) AS team_key')
            ->whereIn('driver_id', $driver_ids)
            ->orderByDesc('season')
            ->orderBy('round')
            ->get();
        $raw = $this->secondary['legacyResults']->pluck('driver_id');
        $lookup = array_fill_keys($driver_ids, []);
        foreach ($raw as $id => $driver_id) {
            $lookup[$driver_id][] = $id;
        }
        foreach ($this->data as $i => $driver) {
            $this->data[$i]['legacyResults'] = $this->secondary['legacyResults']
                ->getByKeys($lookup[$driver['driver_id']]);
        }
        // Do we also have a history to bind to?
        if (isset($this->secondary['history'])) {
            $raw = $this->secondary['legacyResults']->pluck('driver_key');
            $lookup = array_fill_keys($this->secondary['history']->pluck('season_key'), []);
            foreach ($raw as $id => $season_key) {
                $lookup[$season_key][] = $id;
            }
            foreach ($this->secondary['history'] as $history) {
                if (!$history->is_detailed) {
                    $history->results = $this->secondary['legacyResults']->getByKeys($lookup[$history->season_key]);
                }
            }
        }
    }
}
