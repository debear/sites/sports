<?php

namespace DeBear\ORM\Sports\Motorsport\FIA;

use DeBear\ORM\Sports\Motorsport\ParticipantStats as BaseParticipantStats;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Repositories\InternalCache;

class ParticipantStats extends BaseParticipantStats
{
    /**
     * Perform the loading query to get the stat leaders
     * @return array The (ordered) list of stats with whatever original info was requested
     */
    public static function loadBySeason(): array
    {
        $default_stat = FrameworkConfig::get('debear.setup.stats.default');
        $stats_meta = InternalCache::object()->get('stats_meta');
        $stats_meta_lookup = array_flip($stats_meta->pluck('stat_id'));
        // Get the raw data.
        $tbl_partstats = static::getTable();
        $tbl_stats = Stats::getTable();
        $data = static::query()
            ->join($tbl_stats, "$tbl_stats.stat_id", '=', "$tbl_partstats.stat_id")
            ->select("$tbl_partstats.*", "$tbl_stats.disp_order")
            ->where([
                ['season', '=', FrameworkConfig::get('debear.setup.season.viewing')],
                ['series', '=', FrameworkConfig::get('debear.section.code')],
            ])
            ->get();
        // Associated drivers and history info.
        $driver_ids = $data->unique('driver_id');
        $drivers = Participant::query()
            ->whereIn('driver_id', $driver_ids)
            ->get();
        $drivers_lookup = array_flip($drivers->pluck('driver_id'));
        $standings = ParticipantHistory::query()
            ->where([
                ['season', '=', FrameworkConfig::get('debear.setup.season.viewing')],
                ['series', '=', FrameworkConfig::get('debear.section.code')],
            ])
            ->whereIn('driver_id', $driver_ids)
            ->get();
        $standings_lookup = array_flip($drivers->pluck('driver_id'));

        // Merge everything together and return.
        foreach ($data as $item) {
            // Add the associated driver info.
            $driver = $drivers->getRow($drivers_lookup[$item->driver_id]);
            $driver->standingsCurr = $standings->getRow($standings_lookup[$item->driver_id]);
            $item->entity = $driver;
            $item->entity_id = $driver->driver_id;
            $item->stat = $stats_meta->getRow($stats_meta_lookup[$item->stat_id]);
        }

        // Perform the sort.
        $data = $data->sort(function ($a, $b) use ($default_stat) {
            // Is one, but not both, is our default stat then it gains priority.
            if (($a['stat_id'] == $default_stat) && ($b['stat_id'] != $default_stat)) {
                return -1; // $a takes priority.
            } elseif (($a['stat_id'] != $default_stat) && ($b['stat_id'] == $default_stat)) {
                return 1; // $b takes priority.
            } elseif ($a['stat_id'] != $b['stat_id']) {
                // When they're different stats, order by stat order.
                return ($a['disp_order'] <=> $b['disp_order']);
            }
            // Otherwise, order by rank.
            return ($a['pos'] <=> $b['pos']);
        });

        // Then group by driver and return.
        $raw = $data->pluck('entity_id');
        $key_lookup = array_fill_keys(array_unique($raw), []);
        foreach ($raw as $id => $entity_id) {
            $key_lookup[$entity_id][] = $id;
        }
        $ret = [];
        foreach (array_keys($key_lookup) as $entity_id) {
            $ret[$entity_id] = $data->getByKeys($key_lookup[$entity_id]);
        }
        return $ret;
    }
}
