<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;

class RaceGrid extends Instance
{
    /**
     * Return the appropriate position
     * @return string Grid Position
     */
    public function pos(): string
    {
        return ($this->grid_pos ?? '');
    }

    /**
     * The grid position's row CSS style
     * @return string The CSS to use
     */
    public function css(): string
    {
        // Podium place?
        if ($this->pos() < 4) {
            return 'podium-' . $this->pos();
        }
        // Alternating.
        return 'row_' . (((int)$this->pos()) % 2);
    }

    /**
     * State whether the participant was penalised between qualifying and the grid formation
     * @return boolean That the participant was penalised
     */
    public function hadPenalty(): bool
    {
        return ($this->grid_pos > $this->qual_pos);
    }

    /**
     * Summarise the participant's progress in qualifying
     * @return string The time/notes for the participant
     */
    public function formatResult(): string
    {
        // Get the qualifying time from the last session participated in.
        $session = $this->qualifying->last();
        if ($this->hadPenalty()) {
            // Penalty added after qualifying?
            $time = 'Penalty';
        } else {
            // Format the time.
            $time = $session->time;
        }
        // Flag if not the final qualifying session.
        if ($session->session_num < RaceQuali::numSessions()) {
            $time .= " (Q{$session->session_num})";
        }
        return $time;
    }
}
