<?php

namespace DeBear\ORM\Sports\Motorsport;

use DeBear\ORM\Base\Instance;
use stdClass;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\Motorsport\Header;
use DeBear\Helpers\Sports\Motorsport\Traits\Entity as TraitEntity;

class Participant extends Instance
{
    use TraitEntity;

    /**
     * Mugshot filesystem folder
     * @var string
     */
    protected static $image_dir = 'drivers';
    /**
     * Cached parsing of the participant's history details
     * @var array
     */
    protected $historyParsed = [];

    /**
     * Format the participant's (full) name
     * @return string The participant's name formatted for display
     */
    public function getNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->surname;
    }

    /**
     * Format the participant's best-known name in a codified version (e.g., for links or files)
     * @return string The participant's name standardised and run through the codifier
     */
    public function getNameCodifiedAttribute(): string
    {
        return $this->codifier($this->name);
    }

    /**
     * State where and when the participant was born
     * @return string The participant's birthplace as a display string
     */
    public function getBornAttribute(): string
    {
        $ret = [];
        // Date.
        if ($this->dob && $this->dob->year > 1900) {
            $ret[] = $this->dob->format('jS F Y');
        }
        // Place.
        if ($this->birthplace) {
            $place = $this->birthplace;
            if ($this->birthplace_country && ($this->birthplace_country != $this->nationality)) {
                $place = "<span class=\"flag_right flag16_right_{$this->birthplace_country}\">$place</span>";
            }
            $ret[] = $place;
        }
        // Ensure we have a value.
        if (!$ret) {
            $ret[] = '<em>Unknown</em>';
        }
        return join(', ', $ret);
    }

    /**
     * Codify and parse a name variant
     * @param string $name The base name to codify.
     * @return string The participant's name standardised and run through the codified
     */
    protected function codifier(string $name): string
    {
        return Strings::codifyURL($name);
    }

    /**
     * Determine the link for this participant
     * @param boolean $inc_season Whether we include the season portion.
     * @return string The appropriate URL
     */
    public function link(bool $inc_season = true): string
    {
        return '/' . join('/', array_filter([
            FrameworkConfig::get('debear.section.endpoint'),
            FrameworkConfig::get('debear.setup.participants.url'),
            $this->name_codified . '-' . $this->id,
            $inc_season ? FrameworkConfig::get('debear.setup.season.viewing') : false,
        ]));
    }

    /**
     * Determine the link for this participant, with no season part specified
     * @return string The appropriate URL
     */
    public function linkDefault(): string
    {
        return $this->link(false);
    }

    /**
     * Format the participant's name with a link and flag
     * @param boolean $inc_link Whether we include the link portion.
     * @return string The participant's formatted details
     */
    public function nameLinkFlag(bool $inc_link = true): string
    {
        return '<span class="flag_right flag16_right_' . $this->nationality . '">'
            . ($inc_link ? '<a href="' . $this->link() . '" ' . $this->hoverAttributes() . '>' : '')
            . $this->name
            . ($inc_link ? '</a>' : '')
            . '</span>';
    }

    /**
     * Format the participant's name with a flag (no link)
     * @return string The participant's formatted details
     */
    public function nameFlag(): string
    {
        return $this->nameLinkFlag(false);
    }

    /**
     * Get the collection of historical season records
     * @return stdClass The appropriate history records for this participant
     */
    protected function historyRecords(): stdClass
    {
        if (isset($this->historyParsed[$this->rider_id])) {
            return $this->historyParsed[$this->rider_id];
        }
        $this->historyParsed[$this->rider_id] = (object)[
            'count' => $this->history->count(),
            'num_wins' => $this->history->sum('num_wins'),
            'num_podiums' => $this->history->sum('num_podiums'),
            'num_poles' => $this->history->sum('num_poles'),
            'num_fastest_laps' => $this->history->sum('num_fastest_laps'),
            'pts' => $this->history->sum('pts'),
        ];
        return $this->historyParsed[$this->rider_id];
    }

    /**
     * The formatted number of seasons this participant has taken part
     * @return string Seasons taken part by the participant
     */
    public function experience(): string
    {
        $exp = $this->experienceRaw();
        return ($exp > 1 ? Format::ordinal($exp) : 'First');
    }

    /**
     * The raw number of seasons this participant has taken part
     * @return integer Number of seasons taken part by the participant
     */
    public function experienceRaw(): int
    {
        return $this->historyRecords()->count;
    }

    /**
     * The championships won by this participant
     * @return array List of seasons in which the participant won the championship
     */
    public function championships(): array
    {
        $champs = array_flip($this->history
            ->where('pos', '=', 1)
            ->pluck('season'));
        // Annoying edge-case - the current season leader isn't necessarily the World Champion!
        $curr_season = FrameworkConfig::get('debear.setup.season.max');
        if (isset($champs[$curr_season])) {
            // If the last race wasn't completed, then they are merely a leader and not a champion...
            if (!Header::getCalendar()->last()->isComplete()) {
                // Remove from the list we return, as this season's champ hasn't been crowned yet.
                unset($champs[$curr_season]);
            }
        }
        // Return.
        return array_keys($champs);
    }

    /**
     * The number of Grand Prix this participant has won
     * @return integer Number of Grand Prix won by the participant
     */
    public function gpVictories(): int
    {
        return $this->historyRecords()->num_wins;
    }

    /**
     * The number of Grand Prix this participant has appeared on the podium
     * @return integer Number of Grand Prix podiums by the participant
     */
    public function gpPodiums(): int
    {
        return $this->historyRecords()->num_podiums;
    }

    /**
     * The number of Grand Prix this participant started from pole position
     * @return integer Number of Grand Prix pole positions by the participant
     */
    public function gpPoles(): int
    {
        return $this->historyRecords()->num_poles;
    }

    /**
     * The number of Grand Prix this participant registered the fastest lap
     * @return integer Number of Grand Prix fastest laps by the participant
     */
    public function gpFastestLaps(): int
    {
        return $this->historyRecords()->num_fastest_laps;
    }

    /**
     * The formatted number of points this participant has recorded
     * @return string Points scored by the participant
     */
    public function gpPoints(): string
    {
        return Format::pluralise($this->gpPointsRaw(), ' pt', ['format-number' => true]);
    }

    /**
     * The raw number of points this participant has recorded
     * @return float Number of points scored by the participant
     */
    public function gpPointsRaw(): float
    {
        return $this->historyRecords()->pts;
    }

    /**
     * Build the Highcharts configuration array to display the participant's progress within each season
     * @param string $dom_id  DOM element ID the chart will be rendered within.
     * @param array  $max_pos Lowest position the participant could achieve in the standings, by season.
     * @return array Appropriate highcharts configurations for the participant's season progress
     */
    public function progressChart(string $dom_id, array $max_pos): array
    {
        $ret = [];
        foreach ($this->history as $history) {
            if ($history->is_detailed && $history->is_relevant) {
                $history->participant = $this->getCurrent();
                $ret[$history->season] = $history->progressChart($dom_id, $max_pos[$history->season]);
            }
        }
        return $ret;
    }

    /**
     * Build an array of stat info, separated out as stat names and numbers
     * @return array The array of stats broken down by season and their meta info
     */
    public function parseStats(): array
    {
        $ret = [
            'stats' => [],
            'history' => [],
            'info' => [],
        ];
        foreach ($this->history as $season) {
            // Skip legacy seasons.
            if (!$season->is_detailed || !$season->is_relevant) {
                continue;
            }
            // Build the stats info?
            if (!count($ret['stats'])) {
                foreach ($season->stats as $stat) {
                    $ret['stats'][] = [
                        'stat_id' => $stat->stat_id,
                        'section' => $stat->section,
                        'name_short' => $stat->name_short,
                        'name_full' => $stat->name_full,
                        'disp_order' => $stat->disp_order,
                    ];
                }
            }

            // Split the numbers.
            $ret['history'][$season->season] = [];
            foreach ($season->stats as $key => $stat) {
                $ret['history'][$season->season][$stat->stat_id] = [
                    'pos' => $stat->displayPos(),
                    'pos_raw' => $stat->pos,
                    'pos_tied' => $stat->pos_tied,
                    'pos_css' => $stat->posCSS($key),
                    'value' => $stat->rendered(),
                    'value_css' => $stat->valueCSS(),
                ];
            }
        }
        // Participant with what sort of previous history?
        $ret['info']['preseason'] = isset($ret['history'][FrameworkConfig::get('debear.setup.season.default')])
            && !count($ret['history'][FrameworkConfig::get('debear.setup.season.default')]);
        $ret['info']['no-history'] = !count(array_filter($ret['history']));

        return $ret;
    }
}
