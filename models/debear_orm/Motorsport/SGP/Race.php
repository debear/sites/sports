<?php

namespace DeBear\ORM\Sports\Motorsport\SGP;

use DeBear\ORM\Sports\Motorsport\Race as BaseRace;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\SGP\Traits\Race as TraitRace;
use DeBear\ORM\Skeleton\WeatherForecast;
use DeBear\Repositories\InternalCache;

class Race extends BaseRace
{
    use TraitRace;

    /**
     * SET columns and their values
     * @var array
     */
    protected $sets = [
        'quirks' => ['quali_sprint'],
    ];

    /**
     * The configuration of how our meeting should be run.
     * @var SetupRace
     */
    protected $setupRaceModel;
    /**
     * The configuration of how our knockout heats are configured.
     * @var SetupKnockoutDraw
     */
    protected $setupKODrawModel;

    /**
     * Determine if the meeting has been completed or not
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return boolean If the meeting has been completed
     */
    public function isComplete(int $race_num = 1): bool
    {
        return (bool)$this->completed_heats;
    }

    /**
     * Determine if the meeting was abandoned before it was completed in full
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return boolean If the meeting has been abandoned
     */
    public function isAbandoned(int $race_num = 1): bool
    {
        $num_expected = $this->getNumExpectedHeats()
            + $this->setupRace->quarter_finals
            + $this->setupRace->semi_finals
            + $this->setupRace->last_chance_qualifiers
            + $this->setupRace->grand_finals;
        return ($this->completed_heats < $num_expected);
    }

    /**
     * Get the appropriate setup details for this meeting (as a faux relationship)
     * @return SetupRace Model containing the appropriate details
     */
    public function getSetupRaceAttribute(): SetupRace
    {
        if (!isset($this->setupRaceModel)) {
            // Pre-loaded?
            $cache_key = "setupRace:{$this->season}";
            $this->setupRaceModel = InternalCache::object()->get($cache_key);
            if (!is_object($this->setupRaceModel)) {
                // Build, save and return.
                $this->setupRaceModel = SetupRace::query()
                    ->where([
                        ['season_from', '<=', $this->season],
                        ['season_to', '>=', $this->season],
                    ])
                    ->get();
                InternalCache::object()->set($cache_key, $this->setupRaceModel);
            }
        }
        return $this->setupRaceModel;
    }

    /**
     * Get the appropriate setup details for this meeting's heats (as a faux relationship)
     * @return SetupKnockoutDraw Collection of SetupGate models containing the appropriate details
     */
    public function getSetupKnockoutDrawAttribute(): SetupKnockoutDraw
    {
        if (!isset($this->setupKODrawModel)) {
            // Pre-loaded?
            $cache_key = "setupKnockoutDraw:{$this->season}";
            $this->setupKODrawModel = InternalCache::object()->get($cache_key);
            if (!is_object($this->setupKODrawModel)) {
                // Build, save and return.
                $this->setupKODrawModel = SetupKnockoutDraw::query()
                    ->where('version', $this->setupRace->draw_version)
                    ->get();
                InternalCache::object()->set($cache_key, $this->setupKODrawModel);
            }
        }
        return $this->setupKODrawModel;
    }

    /**
     * The list of heats that make up the knockout stages
     * @return array
     */
    public function knockoutHeats(): array
    {
        $setup = $this->setupRace;
        // Build our heat list.
        $heat_types = [
            'qf' => [
                'num' => $setup->quarter_finals,
                'title' => 'Quarter-Final',
                'qual' => $setup->quarter_finals ? 4 / $setup->quarter_finals : 0,
            ],
            'sf' => [
                'num' => $setup->semi_finals,
                'title' => 'Semi-Final',
                'qual' => $setup->semi_finals ? 4 / $setup->semi_finals : 0,
            ],
            'lcq' => [
                'num' => $setup->last_chance_qualifiers,
                'title' => 'Last-Chance Qualifier',
                'qual' => $setup->last_chance_qualifiers ? 2 / $setup->last_chance_qualifiers : 0,
            ],
            'gf' => [
                'num' => $setup->grand_finals,
                'title' => 'Grand Final',
                'qual' => 0,
                'draw' => [
                    ['title' => 'D-1', 'attrib' => '1:0'],
                    ['title' => 'D-2', 'attrib' => '1:1'],
                    ['title' => 'D-3', 'attrib' => '2:0'],
                    ['title' => 'D-4', 'attrib' => '2:1'],
                ],
            ],
        ];
        $ret = [];
        foreach (array_filter($heat_types, fn($h) => ($h['num'] > 0)) as $heat_type => $heat_config) {
            for ($i = 1; $i <= $heat_config['num']; $i++) {
                $ret[] = array_filter([
                    'heat_type' => $heat_type,
                    'heat' => $i,
                    'title' => $heat_config['title'] . ($heat_config['num'] > 1 ? " $i" : ''),
                    'qual' => $heat_config['qual'],
                    'draw' => ($heat_config['draw'] ?? '*unset*'),
                ], fn($h) => ($h != '*unset*'));
            }
        }
        // And return.
        foreach ($ret as $i => $heat) {
            if (isset($heat['draw'])) {
                continue;
            }
            $ret[$i]['draw'] = $this->setupKnockoutDraw
                ->where('heat_type', $heat['heat_type'])
                ->where('heat', $heat['heat'])
                ->sortBy('sel_pos');
        }
        return $ret;
    }

    /**
     * Get the number of heats we would expect to be run in a 'standard' meeting
     * @return integer The number of heats that would be held
     */
    public function getNumExpectedHeats(): int
    {
        // Pre-loaded?
        $cache_key = "setupGatesNum:{$this->season}";
        if (InternalCache::object()->cacheKeyExists($cache_key)) {
            return InternalCache::object()->get($cache_key);
        }
        // Build, save and return.
        $tbl_races = SetupRace::getTable();
        $tbl_gates = SetupGate::getTable();
        $ret = SetupRace::query()
            ->join($tbl_gates, "$tbl_gates.version", '=', "$tbl_races.gates_version")
            ->where([
                ["$tbl_races.season_from", '<=', $this->season],
                ["$tbl_races.season_to", '>=', $this->season],
            ])
            ->count(DB::raw("DISTINCT $tbl_gates.heat"));
        InternalCache::object()->set($cache_key, $ret);
        return $ret;
    }

    /**
     * A summarised information message about this race
     * @param integer $race_num Instance of the race within the weekend we are linking to (Default: 1).
     * @return string Info message string for the given race
     */
    public function infoMessage(int $race_num = 1): string
    {
        // If not all heats were completed, warn the meeting was not completed as usual.
        if ($this->isAbandoned()) {
            return 'The meeting was abandoned with the Grand Prix and Championship Points awarded after Heat '
                . $this->completed_heats . '.';
        }
        return '';
    }

    /**
     * Summary details about the individual race
     * @return array Key/Value pair of details to add to the top of the individual race page
     */
    public function headerInfo(): array
    {
        return [
            'Track' => $this->venue,
            'Length' => $this->track_length . 'm',
            '-',
            $this->race_time->format('l jS F Y'),
        ];
    }

    /**
     * Convert the meeting location to a URL component
     * @return string The meeting name as a URL component
     */
    public function linkVenue(): string
    {
        return Strings::codifyURL($this->location) . (substr($this->name_short, -3) == ' II' ? '-ii' : '');
    }

    /**
     * State where the race is to take place
     * @return string The summarised location details
     */
    public function getVenueAttribute(): string
    {
        return join(', ', array_unique([$this->track, $this->location]));
    }

    /**
     * Group the heat results by heat for more efficient lookups
     * @return array The array of heat results by individual heat
     */
    public function getGroupedHeatResults(): array
    {
        // Group the heat results efficiently.
        $raw = $this->heats->pluck('heat_ref');
        $ref_lookup = array_fill_keys(array_unique($raw), []);
        foreach ($raw as $id => $ref) {
            $ref_lookup[$ref][] = $id;
        }
        foreach ($ref_lookup as $ref => &$ids) {
            $ids = $this->heats->getByKeys($ids);
        }
        return $ref_lookup;
    }

    /**
     * Get the list of participants ordered by standings after the completion of the main heats
     * @return RaceResult The main heat participants ordered by standings before knockout stages
     */
    public function getStandingsMain(): RaceResult
    {
        $main_res = $this->results->whereNotNull('main_pos')->sortBy('main_pos');
        foreach ($main_res as $item) {
            $rides = $this->heats
                ->where('heat_type', 'main')
                ->where('bib_no', $item->bib_no)
                ->whereNotNull('result');
            $item->stats = (object)[
                'rides' => $rides->count(),
                'num_1st' => $rides->where('result', 1)->count(),
                'num_2nd' => $rides->where('result', 2)->count(),
                'num_3rd' => $rides->where('result', 3)->count(),
                'num_4th' => $rides->where('result', 4)->count(),
            ];
        }
        return $main_res;
    }

    /**
     * Determine the gate-level results during the main heats
     * @return array The list of gates and appropriate counts
     */
    public function getMeetingGateStats(): array
    {
        $ret = array_fill(1, 4, array_fill(1, 4, 0));
        $heat_res = $this->heats
            ->where('heat_type', 'main')
            ->whereIn('result', [1, 2, 3, 4]);
        foreach ($heat_res as $h) {
            $ret[$h->gate][$h->result]++;
        }
        return $ret;
    }

    /**
     * Get the two meetings to focus on on the homepage
     * @return array An array with two elements, either last/next or last two (if no known next)
     */
    public static function getHomepageFocus(): array
    {
        // Get a potential future meeting.
        $next = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", flag) AS weather_key')
            ->where('completed_heats', 0)
            ->orderBy('race_time')
            ->limit(1)
            ->get()
            ->loadSecondary([
                'forecast',
            ]);
        // Get the last two meetings, so we can determine which to use.
        $prev = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", flag) AS weather_key')
            ->where('completed_heats', '>', 0)
            ->orderByDesc('race_time')
            ->limit(2)
            ->get()
            ->loadSecondary([
                'forecast',
                'raceParticipants',
                'participants',
                'results',
            ]);
        // Build our return object.
        $ret = [
            // A previous meeting. If no next, it's the previous previous.
            'left' => $next->count() ? $prev->first() : $prev->last(),
            // The next meeting, unless we have no next in which case it's the last meeting that occurred.
            'right' => $next->count() ? $next : $prev->first(),
        ];
        return $ret;
    }

    /**
     * Get the list of all completed meetings for the XML sitemap
     * @return self The list of Race objects of all the completed meetings stored in the database
     */
    public static function getAllCompleted(): self
    {
        return static::query()
            ->where([
                ['completed_heats', '>', 0],
            ])->orderBy('season')
            ->orderBy('round_order')
            ->get();
    }

    /**
     * Load the calendar of races for a given season
     * @param integer $season    The season to load.
     * @param array   $secondary Additional eager loading list.
     * @return self Object of (ordered) races for that season
     */
    public static function loadCalendar(int $season, array $secondary = []): self
    {
        // Get our raw races list.
        $calendar = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", flag) AS weather_key')
            ->where('season', $season)
            ->orderBy('round_order')
            ->get();

        // Then the secondary data.
        if (count($calendar)) {
            $calendar->loadSecondary(array_merge([
                'raceParticipants',
                'participants',
                'results',
            ], $secondary));
        }

        // Return what we have built.
        return $calendar;
    }

    /**
     * Get the list of races from the previous season
     * @param integer $season The current season we are viewing.
     * @return self The list of Race objects of all the races the previous season
     */
    public static function previousCalendar(int $season): self
    {
        return static::loadCalendar($season - 1, [
            'participantsStandingsCurrent',
        ]);
    }

    /**
     * Load an individual race for rendering as the individual results
     * @param integer $season The season to load.
     * @param integer $round  The round ID of the race to load.
     * @return self Object, with appropriate additional info eager loaded, for the race to render
     */
    public static function loadRace(int $season, int $round): self
    {
        // The primary info (as a collection of a single race we'll reduce later).
        $race = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", round) AS race_key')
            ->selectRaw('CONCAT(season, "-", flag) AS weather_key')
            ->where([
                ['season', '=', $season],
                ['round', '=', $round],
            ])
            ->get();

        // Then the secondary data.
        if (count($race)) {
            $race->loadSecondary([
                'raceParticipants',
                'participants',
                'participantsStandingsCurrent',
                'participantsStandingsRace',
                'results',
                'heats',
                'setupRace',
                'setupGates',
                'setupKnockoutDraw',
            ]);
        }

        // Return what we have built.
        return $race;
    }

    /**
     * The default handler for loading secondary race information.
     * @param string $name The requested secondary race information.
     * @return void
     */
    protected function loadSecondaryDefault(string $name): void
    {
        // Define our specific mapping details.
        $maps = [
            'raceParticipants' => [
                'class' => RaceParticipant::class,
            ],
            'results' => [
                'class' => RaceResult::class,
                // Using "pos IS NULL" to put track reserves who did not ride after the main riders (who did).
                'sort' => 'season, round, pos IS NULL, pos, bib_no',
                'link' => ['raceParticipants' => 'raceParticipant'],
            ],
            'heats' => [
                'class' => RaceHeats::class,
                'select' => [
                    'CONCAT(_TBL_.heat_type, "::", _TBL_.heat) AS heat_ref',
                    'CONCAT(_TBL_.heat_type, "::", _TBL_.heat, "::", _TBL_.bib_no) AS rider_ref',
                ],
                'sort' => 'season, round, heat_type, heat, result',
                'link' => ['raceParticipants' => 'raceParticipant'],
            ],
        ];
        $map = $maps[$name];
        // Get the data for all matching race(s).
        $query = $map['class']::query();
        $tbl = $query->from;
        $query = $query->select("$tbl.*")
            ->selectRaw("1 AS race")
            ->selectRaw("CONCAT($tbl.season, '-', $tbl.round) AS race_key")
            ->selectRaw("CONCAT($tbl.season, '-', $tbl.round, '-', $tbl.bib_no) AS part_key");
        if (isset($map['select']) && is_array($map['select'])) {
            foreach ($map['select'] as $select) {
                $query = $query->selectRaw(str_replace('_TBL_', $tbl, $select));
            }
        }
        // Build the where clause, depending on the number of seasons loaded.
        $seasons = $this->unique('season');
        // Assumes a single season (a simpler query!).
        $query = $query->where('season', $seasons[0])->whereIn('round', $this->pluck('round'));
        // Any sorting?
        if (isset($map['sort'])) {
            $query = $query->orderByRaw($map['sort']);
        }
        // Get the data, and distribute across the races.
        $instance = ($map['name'] ?? $name);
        $this->secondary[$name] = $query->get();
        // Any data to link at this level?
        if (isset($map['link'])) {
            foreach ($map['link'] as $link_from => $link_to) {
                $link_from = $this->secondary[$link_from];
                $key_lookup = array_flip($link_from->pluck('part_key'));
                foreach ($this->secondary[$name] as $item) {
                    if (isset($key_lookup[$item->part_key])) {
                        $item->$link_to = $link_from->getRow($key_lookup[$item->part_key]);
                    }
                }
            }
        }
        // Then tie to the races.
        foreach ($this->data as $i => $race) {
            $this->data[$i][$instance] = $this->secondary[$name]->where('race_key', $race['race_key']);
        }
    }

    /**
     * Load the appropriate weather information
     * @return void
     */
    protected function loadSecondaryForecast(): void
    {
        $instances = [];
        foreach ($this->data as $race) {
            $instances[] = "{$race['season']}-{$race['flag']}";
        }
        $weather = WeatherForecast::query()
            ->where('app', '=', FrameworkConfig::get('debear.weather.app'))
            ->whereIn('instance_key', $instances)
            ->get();
        // Then tie to the races.
        foreach ($this->data as $i => $race) {
            $this->data[$i]['forecast'] = (object)[
                'race' => $weather->where('instance_key', $race['weather_key']),
            ];
        }
    }

    /**
     * Load the information about the participants taking part in the races.
     * @return void
     */
    protected function loadSecondaryParticipants(): void
    {
        $rider_ids = $this->secondary['raceParticipants']->unique('rider_id');
        $this->secondary['participants'] = Participant::query()
            ->whereIn('rider_id', $rider_ids)
            ->get();
        foreach ($rider_ids as $rider_id) {
            $rider = $this->secondary['participants']->where('rider_id', $rider_id);
            foreach ($this->secondary['raceParticipants']->where('rider_id', $rider_id) as $race_part) {
                $race_part->participant = $rider;
            }
        }
    }

    /**
     * Load the specific standings for participants immediately after the races.
     * @return void
     */
    protected function loadSecondaryParticipantsStandingsRace(): void
    {
        // Determine the round_order's to load.
        $orders_curr = $this->unique('round_order');
        $orders_prev = array_filter(array_map(function ($order) {
            return $order - 1;
        }, $orders_curr));
        $orders_all = array_unique(array_merge($orders_curr, $orders_prev));
        // Build and run the query.
        $tbl_stand = ParticipantHistoryProgress::getTable();
        $tbl_race = static::getTable();
        $rand = $this->random();
        $this->secondary['participantsStandingsRace'] = ParticipantHistoryProgress::query()
            ->join($tbl_race, function ($join) use ($tbl_stand, $tbl_race) {
                $join->on("$tbl_stand.season", '=', "$tbl_race.season")
                    ->on("$tbl_stand.round", '=', "$tbl_race.round");
            })
            ->select("$tbl_stand.*", "$tbl_race.round_order")
            ->selectRaw("CONCAT($tbl_stand.season, '-', $tbl_stand.rider_id, "
                . "'-', $tbl_race.round_order) AS part_key")
            ->selectRaw("CONCAT($tbl_stand.season, '-', $tbl_stand.rider_id, "
                . "'-', $tbl_race.round_order - 1) AS part_key_prev")
            ->where("$tbl_stand.season", $rand->season)
            ->whereIn("$tbl_race.round_order", $orders_all)
            ->orderByRaw("$tbl_stand.season, $tbl_stand.rider_id, $tbl_race.round_order")
            ->get();
        // Distribute across the entities.
        $adding = $this->secondary['participantsStandingsRace']->unique('rider_id');
        foreach ($adding as $entity_id) {
            $entity = $this->secondary['participants']->where('rider_id', $entity_id);
            $entity->standingsRace = $this->secondary['participantsStandingsRace']->where('rider_id', $entity_id);
        }
    }

    /**
     * Load the current standings for participants in the races.
     * @return void
     */
    protected function loadSecondaryParticipantsStandingsCurrent(): void
    {
        // Build and run the query.
        $rand = $this->random();
        $this->secondary['participantsStandingsCurrent'] = ParticipantHistory::query()
            ->where('season', $rand->season)
            ->get();
        // How about additional participants not already loaded?
        $loaded = $this->secondary['participants']->unique('rider_id');
        $adding = $this->secondary['participantsStandingsCurrent']->unique('rider_id');
        $missing_ids = array_diff($adding, $loaded);
        if ($missing_ids) {
            $missing = Participant::query()
                ->whereIn('rider_id', $missing_ids)
                ->get();
            $this->secondary['participants']->merge($missing);
        }
        // Distribute across the entities.
        foreach ($adding as $entity_id) {
            $entity = $this->secondary['participants']->where('rider_id', $entity_id);
            $entity->standingsCurr = $this->secondary['participantsStandingsCurrent']->where('rider_id', $entity_id);
        }
    }

    /**
     * Load the race configuration for the meetings.
     * @return void
     */
    protected function loadSecondarySetupRace(): void
    {
        $name = 'setupRace';
        $this->secondary[$name] = $this->random()->$name;
        // Then tie to the races.
        foreach (array_keys($this->data) as $i) {
            $this->data[$i][$name] = $this->secondary[$name];
        }
    }

    /**
     * Load the gate setup details for the meetings.
     * @return void
     */
    protected function loadSecondarySetupGates(): void
    {
        $name = 'setupGates';
        $this->secondary[$name] = $this->random()->$name;
        // Then tie to the races.
        foreach (array_keys($this->data) as $i) {
            $this->data[$i][$name] = $this->secondary[$name];
        }
    }

    /**
     * Load the knockout draw selection order for the meetings.
     * @return void
     */
    protected function loadSecondarySetupKnockoutDraw(): void
    {
        $name = 'setupKnockoutDraw';
        $this->secondary[$name] = $this->random()->$name;
        // Then tie to the races.
        foreach (array_keys($this->data) as $i) {
            $this->data[$i][$name] = $this->secondary[$name];
        }
    }

    /**
     * Build the list of events to display on the global homepage
     * @param array  $date_maps An array of dates we are focusing on.
     * @param string $series    The race series we are procssing.
     * @return array The appropriate events to focus on
     */
    public static function getHomepageEvents(array $date_maps, string $series): array
    {
        $ret = array_fill_keys(array_values($date_maps), []);
        // Parse each session - in SGP, just the meeting.
        $data = static::query()
            ->select('*')
            ->selectRaw('CONCAT(season, "-", round) AS race_key')
            ->selectRaw('"motorsport" AS template')
            ->selectRaw('? AS template_icon', [$series])
            ->selectRaw('"race" AS template_type')
            ->selectRaw("DATE(race_time) AS parse_date")
            ->whereIn(DB::raw("DATE(race_time)"), array_keys($date_maps))
            ->get()
            ->loadSecondary(['raceParticipants', 'participants', 'results']);

        // Add any rows to our return object.
        foreach ($data->unique('parse_date') as $d) {
            $ret[$date_maps[$d]][] = $data->where('parse_date', $d);
        }
        return $ret;
    }
}
