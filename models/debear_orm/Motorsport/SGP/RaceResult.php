<?php

namespace DeBear\ORM\Sports\Motorsport\SGP;

use DeBear\ORM\Sports\Motorsport\RaceResult as BaseRaceResult;
use DeBear\ORM\Sports\Motorsport\Race as BaseRace;
use DeBear\Helpers\Format;
use DeBear\Helpers\Sports\SGP\Traits\Race as TraitRace;

class RaceResult extends BaseRaceResult
{
    use TraitRace;

    /**
     * The race row CSS style
     * @param BaseRace $info Race object for the race we are displaying a cell for or specific values. (Optional).
     * @return string The CSS to represent this race result
     */
    public function css(?BaseRace $info = null): string
    {
        // If the race was known not to have been completed yet.
        $abandoned_meeting = isset($info) && $info->isAbandoned();
        if (isset($info) && !$info->isComplete()) {
            // Not raced.
            $css = 'res-not_yet_raced';
        } elseif ($this->is_nc) {
            // Not Classified.
            $css = 'res-nc';
        } elseif (!isset($this->pos)) {
            // Not Classified.
            $css = 'res-no_race';
        } elseif ($this->pos < 4) {
            // Podium.
            $css = 'res-pos_' . $this->pos;
        } elseif (!$abandoned_meeting && $this->pos == 4) {
            // Non-Podium Finalist.
            $css = 'res-pts_final';
        } elseif (!$abandoned_meeting && $this->pos < 9) {
            // Semi-Finalist.
            $css = 'res-pts_semi';
        } elseif (is_numeric($this->pts)) {
            // Points finish.
            $css = 'res-pts_finish';
        }
        return $css ?? '';
    }

    /**
     * Return the appropriate position
     * @return string Position, either the numeric value or a code
     */
    public function pos(): string
    {
        if (isset($this->pos)) {
            return $this->pos;
        }
        // Fallback to their bib_no (targetting Track Reserve).
        return (string)$this->bib_no;
    }

    /**
     * An SGP specific customisation to the display text version of the race result
     * @return string The formatted result
     */
    public function displayResult(): string
    {
        if (!$this->isset()) {
            // Empty string if no result object available.
            return '';
        } elseif ($this->is_nc) {
            // Rider not classified.
            return 'NC';
        }
        return (string)$this->pos;
    }

    /**
     * An SGP specific customisation to the display numeric version of the points total
     * @return string The formatted total
     */
    public function displayPts(): string
    {
        if ($this->is_nc) {
            // Rider not classified.
            return 'NC';
        }
        // Fall back to standard processing.
        return parent::displayPts();
    }

    /**
     * Summarise the participant's progress in the meeting
     * @return string The score for the participant
     */
    public function formatResult(): string
    {
        return isset($this->pts) ? Format::pluralise($this->pts, ' pt') : '';
    }
}
