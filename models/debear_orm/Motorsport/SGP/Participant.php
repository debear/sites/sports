<?php

namespace DeBear\ORM\Sports\Motorsport\SGP;

use DeBear\ORM\Sports\Motorsport\Participant as BaseParticipant;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class Participant extends BaseParticipant
{
    /**
     * Get the numeric ID for this participant
     * @return integer The particpant's ID
     */
    public function getIDAttribute(): int
    {
        return $this->rider_id;
    }

    /**
     * Get the list of all participants for the XML sitemap
     * @return self The participants as collection
     */
    public static function getAllHistorical(): self
    {
        $tbl_part = static::getTable();
        $tbl_racepart = RaceParticipant::getTable();
        return static::query()
            ->selectRaw("$tbl_part.*, MAX($tbl_racepart.season) AS max_season")
            ->join($tbl_racepart, function ($join) use ($tbl_part, $tbl_racepart) {
                return $join->where("$tbl_racepart.season", '>=', FrameworkConfig::get('debear.setup.season.min'))
                    ->on("$tbl_racepart.rider_id", '=', "$tbl_part.rider_id");
            })->groupBy("$tbl_part.rider_id")
            ->orderBy("$tbl_part.surname")
            ->orderBy("$tbl_part.first_name")
            ->get();
    }

    /**
     * Any additional info relevant to the participant that season
     * @return string The extra info
     */
    public function extraInfo(): string
    {
        // Determine if the rider was a Substitute, Wild Card or Track Reserve at any point.
        $is = ['wc' => 0, 'trsv' => 0, 'sub' => 0];
        foreach ($this->results as $race) {
            if (isset($race->status)) {
                $is[$race->status]++;
            }
        }
        // Reduce to those that are of interest.
        $is = array_filter($is);
        if (!$is) {
            return '';
        }
        // What were they then?
        $abbrev = [
            'sub' => '<abbr title="Qualified Substitute">Subs</abbr>',
            'wc' => '<abbr title="Wildcard Rider">WC</abbr>',
            'trsv' => '<abbr title="Track Reserve">TrRsv</abbr>',
        ];
        return join(', ', array_intersect_key($abbrev, $is));
    }

    /**
     * Try and find a participant passed in by codified name and ID
     * @param integer $rider_id   The ID of the participant being looked up.
     * @param string  $rider_name The codified name being looked up.
     * @param integer $season     The season being requested (which may not be available, but isn't a blocker).
     * @return ?self The found (shortened) matching info, or null if not found
     */
    public static function findByCodifiedID(int $rider_id, string $rider_name, int $season): ?self
    {
        // Get the list of possible options.
        $min_season = FrameworkConfig::get('debear.setup.season.min');
        $tbl_part = static::getTable();
        $tbl_parthist = ParticipantHistory::getTable();
        $all_names = static::query()
            ->select(
                "$tbl_part.rider_id",
                "$tbl_part.first_name",
                "$tbl_part.surname"
            )
            ->selectRaw("MIN(IF($tbl_parthist.season >= ?, $tbl_parthist.season, NULL)) AS first_season", [
                $min_season,
            ])
            ->selectRaw("MAX(IF($tbl_parthist.season >= ?, $tbl_parthist.season, NULL)) AS last_season", [
                $min_season,
            ])
            ->selectRaw("MAX($tbl_parthist.season >= ? AND $tbl_parthist.season = ?) AS inc_season", [
                $min_season,
                $season,
            ])
            ->join($tbl_parthist, function ($join) use ($tbl_parthist, $tbl_part) {
                return $join->on("$tbl_parthist.rider_id", '=', "$tbl_part.rider_id");
            })
            ->where("$tbl_part.rider_id", $rider_id)
            ->groupBy("$tbl_part.surname", "$tbl_part.first_name")
            ->orderByDesc('inc_season')
            ->get();
        foreach ($all_names as $name) {
            // If we have a match then use what we've found a potential participant.
            if ($name->name_codified == $rider_name) {
                $match = $name->getCurrent();
                break;
            }
        }
        // Return what we find.
        if (!isset($match)) {
            // No match.
            return null;
        }
        $season_query = $match->inc_season
            ? $season
            : ($season > $match->last_season
                ? $match->last_season
                : $match->first_season);
        return static::query()
            ->select('*')
            ->selectRaw("$season_query AS season")
            ->where([
                ['rider_id', $match->rider_id],
            ])->get();
    }

    /**
     * Group through all of the participants by their category (e.g., permanent, wild card, etc)
     * @return array The participants, grouped by their category
     */
    public static function getBySeason(): array
    {
        $setup = SetupRace::query()
            ->where([
                ['season_from', '<=', FrameworkConfig::get('debear.setup.season.viewing')],
                ['season_to', '>=', FrameworkConfig::get('debear.setup.season.viewing')],
            ])->get();
        $riders = static::getBySeasonBuild($setup);
        return static::getByseasonProcess($riders, $setup);
    }

    /**
     * Build the raw rider data to be processed in to groups
     * @param SetupRace $setup How the race riders are configured in this season.
     * @return self The participants, grouped by their category
     */
    protected static function getBySeasonBuild(SetupRace $setup): self
    {
        $tbl_part = static::getTable();
        $tbl_race = Race::getTable();
        $tbl_racepart = RaceParticipant::getTable();
        $tbl_raceres = RaceResult::getTable();
        $tbl_ranking = ParticipantRanking::getTable();
        $all = static::query()
            ->join($tbl_racepart, "$tbl_part.rider_id", '=', "$tbl_racepart.rider_id")
            ->join($tbl_race, function ($join) use ($tbl_racepart, $tbl_race) {
                $join->on("$tbl_race.season", '=', "$tbl_racepart.season")
                    ->on("$tbl_race.round", '=', "$tbl_racepart.round");
            })
            ->leftJoin($tbl_ranking, function ($join) use ($tbl_racepart, $tbl_ranking) {
                $join->on("$tbl_ranking.season", '=', "$tbl_racepart.season")
                    ->on("$tbl_ranking.rider_id", '=', "$tbl_racepart.rider_id");
            })
            ->select("$tbl_part.*", "$tbl_ranking.ranking")
            ->selectRaw('GROUP_CONCAT('
                . "IF($tbl_racepart.bib_no BETWEEN ? AND ?, $tbl_race.round_order, NULL)"
            . "ORDER BY $tbl_race.round_order) AS round_list", [
                $setup->regulars + 1,
                $setup->regulars + $setup->wildcards + $setup->track_reserves + $setup->substitutes,
            ])
            ->selectRaw("IF($tbl_ranking.season IS NOT NULL, 'permanent', "
                . "IF(SUM($tbl_racepart.bib_no BETWEEN ? AND ?), 'substitute', 'per-meeting')) AS rider_type", [
                $setup->regulars + $setup->wildcards + $setup->track_reserves + 1,
                $setup->regulars + $setup->wildcards + $setup->track_reserves + $setup->substitutes,
            ])
            ->selectRaw("(SUM($tbl_racepart.bib_no BETWEEN ? AND ?) > 0) AS is_per_meeting", [
                $setup->regulars + 1,
                $setup->regulars + $setup->wildcards + $setup->track_reserves,
            ])
            ->where("$tbl_racepart.season", FrameworkConfig::get('debear.setup.season.viewing'))
            ->groupBy("$tbl_part.rider_id")
            ->get();
        // Get additional data.
        $standings_curr = ParticipantHistory::query()
            ->where('season', FrameworkConfig::get('debear.setup.season.viewing'))
            ->whereIn('rider_id', $all->unique('rider_id'))
            ->get();
        $standings_all = ParticipantHistory::query()
            ->whereIn(
                'rider_id',
                $all->where('rider_type', '!=', 'per-meeting')->unique('rider_id')
            )->get();
        $results = RaceResult::query()
            ->select("$tbl_raceres.*", "$tbl_racepart.rider_id", "$tbl_race.round_order")
            ->selectRaw("($tbl_racepart.bib_no BETWEEN ? AND ?) AS is_wildcard", [
                $setup->regulars + 1,
                $setup->regulars + $setup->wildcards,
            ])
            ->selectRaw("($tbl_racepart.bib_no BETWEEN ? AND ?) AS is_track_reserve", [
                $setup->regulars + $setup->wildcards + 1,
                $setup->regulars + $setup->wildcards + $setup->track_reserves,
            ])
            ->join($tbl_racepart, function ($join) use ($tbl_racepart, $tbl_raceres) {
                $join->on("$tbl_racepart.season", '=', "$tbl_raceres.season")
                    ->on("$tbl_racepart.round", '=', "$tbl_raceres.round")
                    ->on("$tbl_racepart.bib_no", '=', "$tbl_raceres.bib_no");
            })
            ->join($tbl_race, function ($join) use ($tbl_racepart, $tbl_race) {
                $join->on("$tbl_race.season", '=', "$tbl_racepart.season")
                    ->on("$tbl_race.round", '=', "$tbl_racepart.round");
            })
            ->where("$tbl_raceres.season", FrameworkConfig::get('debear.setup.season.viewing'))
            ->whereIn(
                "$tbl_racepart.rider_id",
                $all->where('rider_type', '!=', 'permanent')->unique('rider_id')
            )
            ->orderBy("$tbl_race.round_order")
            ->get();
        // Tie-together the appropriate info and return.
        foreach ($all as $rider) {
            $not_perm = ($rider->rider_type != 'permanent');
            if ($not_perm) {
                $rider->round_list_raw = $rider->round_list;
                $rider->round_list = explode(',', $rider->round_list);
            }
            $rider->standingsCurr = $standings_curr->where('rider_id', $rider->rider_id);
            if ($rider->rider_type != 'per-meeting') {
                $rider->history = $standings_all->where('rider_id', $rider->rider_id);
            }
            if ($not_perm) {
                $rider->results = $results->where('rider_id', $rider->rider_id)
                    ->whereIn('round_order', $rider->round_list);
                $rider->race_list = array_combine(
                    $rider->results->pluck('round'),
                    $rider->results->pluck('bib_no')
                );
            }
        }
        return $all;
    }

    /**
     * Process the rider list into the appropriate groups
     * @param self      $riders The full list of riders and appropraite info for the current season.
     * @param SetupRace $setup  How the race riders are configured in this season.
     * @return array The participants, grouped by their category
     */
    protected static function getByseasonProcess(self $riders, SetupRace $setup): array
    {
        // Build the per-meeting role titles (because array_merge and array_fill_key doesn't work with numeric indices).
        $per_meeting_roles = [];
        $bib_no = $setup->regulars + 1;
        for ($i = 0; $i < $setup->wildcards; $i++) {
            $per_meeting_roles[$bib_no++] = 'Wildcard';
        }
        for ($i = 0; $i < $setup->track_reserves; $i++) {
            $per_meeting_roles[$bib_no++] = 'Track Reserve';
        }

        return [
            /* Permanents */
            'perm' => (object)[
                'name' => "Permanent Riders",
                'type' => 'perm',
                'list' => $riders->where('rider_type', 'permanent')->sortBy('ranking'),
            ],

            /* Substitutes */
            'subs' => (object)[
                'name' => 'Substitutes',
                'type' => 'subs',
                'list' => $riders->where('rider_type', 'substitute')->sortBy('round_list_raw'),
            ],

            /* Per-meeting Riders */
            'meeting' => (object)[
                'name' => 'Wild Cards and Track Reserves',
                'type' => 'meeting',
                'list' => $riders->where('is_per_meeting', '1'),
                'roles' => $per_meeting_roles,
            ],
        ];
    }

    /**
     * Load the full information about a given participant
     * @param self $found The abbreviated participant info from the validation phase.
     * @return self The fully loaded information for the single participant
     */
    public static function load(self $found): self
    {
        return static::query()
            ->select('*')
            ->selectRaw("{$found->season} AS season")
            ->where([
                ['rider_id', '=', $found->rider_id],
            ])->get()
            ->loadSecondary([
                'standings',
                'history',
                'historyProgress',
                'historyCalendar',
                'historyResults',
                'legacyResults',
                'seasonStats',
            ]);
    }

    /**
     * The default handler for loading secondary participant information.
     * @param string $name The requested secondary participant information.
     * @return void
     */
    protected function loadSecondaryDefault(string $name): void
    {
        $maps = [
            'standings' => [
                'class' => ParticipantHistory::class,
                'name' => 'standingsCurr',
            ],
        ];
        $map = $maps[$name];
        $season = $this->first()->season;
        $rider_ids = $this->unique('rider_id');
        // Build our query and get the data.
        $query = $map['class']::query();
        $this->secondary[$name] = $query->where('season', $season)
            ->whereIn('rider_id', $rider_ids)
            ->get();
        // Tie back to the participants.
        $lookup = array_flip($this->secondary[$name]->pluck('rider_id'));
        $instance = ($map['name'] ?? $name);
        foreach ($this->data as $i => $rider) {
            $this->data[$i][$instance] = $this->secondary[$name]->getRow($lookup[$rider['rider_id']]);
        }
    }

    /**
     * Load the information about the history for each participant.
     * @return void
     */
    protected function loadSecondaryHistory(): void
    {
        $rider_ids = $this->unique('rider_id');
        $this->secondary['history'] = ParticipantHistory::query()
            ->select('*')
            ->selectRaw('season AS series_key')
            ->selectRaw('CONCAT(season, "-", rider_id) AS season_key')
            ->selectRaw('(season >= ?) AS is_detailed', [FrameworkConfig::get('debear.setup.season.min')])
            ->selectRaw('1 AS is_relevant')
            ->whereIn('rider_id', $rider_ids)
            ->orderByDesc('season')
            ->get();
        // Setup our replicated Team object (which is really just a faux ORM object hack).
        foreach ($this->secondary['history'] as $history) {
            $history->teams = new ParticipantHistory([['team_key' => $history->season, 'results' => []]]);
        }
        // Then assign to the participants.
        $raw = $this->secondary['history']->pluck('rider_id');
        $lookup = array_fill_keys($rider_ids, []);
        foreach ($raw as $id => $rider_id) {
            $lookup[$rider_id][] = $id;
        }
        foreach ($this->data as $i => $rider) {
            $this->data[$i]['history'] = $this->secondary['history']->getByKeys($lookup[$rider['rider_id']]);
        }
    }

    /**
     * Load the information about the rider's progress across the historical seasons we have info for..
     * @return void
     */
    protected function loadSecondaryHistoryProgress(): void
    {
        $series_keys = $this->secondary['history']->unique('series_key');
        $season_keys = $this->secondary['history']->unique('season_key');
        $season_keys_bind = join(',', array_fill(0, count($season_keys), '?'));
        $this->secondary['historyProgress'] = ParticipantHistoryProgress::query()
            ->select('*')
            ->selectRaw('season AS series_key')
            ->selectRaw('CONCAT(round, "-1") AS race_key')
            ->whereRaw('CONCAT(season, "-", rider_id) IN (' . $season_keys_bind . ')', $season_keys)
            ->orderBy('season')
            ->orderBy('round')
            ->get();
        $raw = $this->secondary['historyProgress']->pluck('series_key');
        $lookup = array_fill_keys($series_keys, []);
        foreach ($raw as $id => $series_key) {
            $lookup[$series_key][] = $id;
        }
        foreach ($this->secondary['history'] as $history) {
            $history->progress = $this->secondary['historyProgress']->getByKeys($lookup[$history->series_key]);
        }
    }

    /**
     * Load the information about the race history for each participant that we only have summary details about.
     * @return void
     */
    protected function loadSecondaryHistoryCalendar(): void
    {
        $series_keys = $this->secondary['history']->unique('series_key');
        $series_keys_bind = join(',', array_fill(0, count($series_keys), '?'));
        $this->secondary['historyCalendar'] = Race::query()
            ->select('*')
            ->selectRaw('season AS series_key')
            ->selectRaw('CONCAT(season, "-", round) AS race_key')
            ->whereRaw('CONCAT(season) IN (' . $series_keys_bind . ')', $series_keys)
            ->orderBy('season')
            ->orderBy('round_order')
            ->get();
        $raw = $this->secondary['historyCalendar']->pluck('series_key');
        $lookup = array_fill_keys($series_keys, []);
        foreach ($raw as $id => $series_key) {
            $lookup[$series_key][] = $id;
        }
        foreach ($this->secondary['history'] as $history) {
            $history->calendar = $this->secondary['historyCalendar']->getByKeys($lookup[$history->series_key]);
        }
    }

    /**
     * Load the information about the stats for each participant.
     * @return void
     */
    protected function loadSecondarySeasonStats(): void
    {
        $rider_ids = $this->unique('rider_id');
        $tbl_partstats = ParticipantStats::getTable();
        $tbl_stats = Stats::getTable();
        $this->secondary['seasonStats'] = ParticipantStats::query()
            ->select(
                "$tbl_partstats.*",
                "$tbl_stats.section",
                "$tbl_stats.name_short",
                "$tbl_stats.name_full",
                "$tbl_stats.type",
                "$tbl_stats.disp_order"
            )
            ->selectRaw(
                "CONCAT($tbl_partstats.season, '-', $tbl_partstats.rider_id) AS season_key"
            )
            ->join($tbl_stats, "$tbl_stats.stat_id", '=', "$tbl_partstats.stat_id")
            ->whereIn("$tbl_partstats.rider_id", $rider_ids)
            ->orderByDesc("$tbl_partstats.season")
            ->orderBy("$tbl_stats.disp_order")
            ->get();
        $raw = $this->secondary['seasonStats']->pluck('rider_id');
        $lookup = array_fill_keys($rider_ids, []);
        foreach ($raw as $id => $rider_id) {
            $lookup[$rider_id][] = $id;
        }
        foreach ($this->data as $i => $team) {
            $this->data[$i]['seasonStats'] = $this->secondary['seasonStats']
                ->getByKeys($lookup[$team['rider_id']]);
        }
        // Do we also have a history to bind to?
        if (isset($this->secondary['history'])) {
            $raw = $this->secondary['seasonStats']->pluck('season_key');
            $lookup = array_fill_keys($this->secondary['history']->pluck('season_key'), []);
            foreach ($raw as $id => $season_key) {
                $lookup[$season_key][] = $id;
            }
            foreach ($this->secondary['history'] as $history) {
                $history->stats = $this->secondary['seasonStats']->getByKeys($lookup[$history->season_key]);
            }
        }
    }

    /**
     * Load the information about the races taken part by each participant this season.
     * @return void
     */
    protected function loadSecondaryRaceResults(): void
    {
        $this->loadSecondaryResultsWorker('raceResults');
    }

    /**
     * Load the information about the races taken part by each participant across their entire career.
     * @return void
     */
    protected function loadSecondaryHistoryResults(): void
    {
        $this->loadSecondaryResultsWorker('historyResults');
        // Do we also have a history to bind to?
        if (isset($this->secondary['history'])) {
            $raw = $this->secondary['historyResults']->pluck('rider_key');
            $lookup = array_fill_keys($this->secondary['history']->pluck('season_key'), []);
            foreach ($raw as $id => $season_key) {
                $lookup[$season_key][] = $id;
            }
            foreach ($this->secondary['history'] as $history) {
                if ($history->is_detailed) {
                    $history->results
                        = $history->teams->results
                        = $this->secondary['historyResults']->getByKeys($lookup[$history->season_key]);
                }
            }
        }
    }

    /**
     * Worker method to build consistent result objects.
     * @param string $name The targetted results name.
     * @return void
     */
    protected function loadSecondaryResultsWorker(string $name): void
    {
        $all = ($name == 'historyResults');
        $rider_ids = $this->unique('rider_id');
        $tbl_racepart = RaceParticipant::getTable();
        $tbl_res = RaceResult::getTable();
        $query = RaceResult::query()
            ->select("$tbl_res.*", "$tbl_racepart.rider_id", "$tbl_racepart.status")
            ->selectRaw("$tbl_res.pos AS result")
            ->selectRaw("$tbl_res.pos IS NULL AS is_nc")
            ->selectRaw("CONCAT($tbl_res.round, '-1') AS race_key")
            ->selectRaw(
                "CONCAT($tbl_res.season, '-', $tbl_res.round, '-1') AS full_key"
            )
            ->selectRaw(
                "CONCAT($tbl_res.season, '-', $tbl_racepart.rider_id) AS rider_key"
            )
            ->join($tbl_racepart, function ($join) use ($tbl_racepart, $tbl_res) {
                return $join->on("$tbl_racepart.season", '=', "$tbl_res.season")
                    ->on("$tbl_racepart.round", '=', "$tbl_res.round")
                    ->on("$tbl_racepart.bib_no", '=', "$tbl_res.bib_no");
            });
        // Now build up our where clause (dependent on the participant info loaded).
        if (!$all) {
            // All the same season, which makes for a simpler query.
            $season = $this->first()->season;
            $query = $query->where("$tbl_res.season", $season)
                ->whereIn("$tbl_racepart.rider_id", $rider_ids);
        } else {
            // A mix of seasons, so target specific keys.
            $query = $query->whereIn("$tbl_racepart.rider_id", $rider_ids)
                ->orderBy("$tbl_res.season")
                ->orderBy("$tbl_racepart.rider_id")
                ->orderBy("$tbl_res.round");
        }
        // Finally, run the query.
        $this->secondary[$name] = $query->get();
        // Bind to the participants.
        $raw = $this->secondary[$name]->pluck('rider_id');
        $key_lookup = array_fill_keys($rider_ids, []);
        foreach ($raw as $id => $rider_id) {
            $key_lookup[$rider_id][] = $id;
        }
        foreach ($this->data as $i => $rider) {
            $this->data[$i]['results'] = $this->secondary[$name]->getByKeys($key_lookup[$rider['rider_id']]);
        }
    }

    /**
     * Load the information about the race history for each participant that we only have summary details about.
     * @return void
     */
    protected function loadSecondaryLegacyResults(): void
    {
        $rider_ids = $this->unique('rider_id');
        $this->secondary['legacyResults'] = RaceResult::query()
            ->from(RaceHistorical::getTable()) // Shape-shift from RaceHistorical table to RaceResult object.
            ->select(
                '*',
                'round AS round_order',
                'gp_flag AS flag'
            )
            ->selectRaw("pos IS NOT NULL AND pts IS NULL AS is_nc")
            ->selectRaw('CONCAT(season, "-", round, "-1") AS full_key')
            ->selectRaw('CONCAT(season, "-", rider_id) AS rider_key')
            ->whereIn('rider_id', $rider_ids)
            ->orderByDesc('season')
            ->orderBy('round')
            ->get();
        $raw = $this->secondary['legacyResults']->pluck('rider_id');
        $lookup = array_fill_keys($rider_ids, []);
        foreach ($raw as $id => $rider_id) {
            $lookup[$rider_id][] = $id;
        }
        foreach ($this->data as $i => $rider) {
            $this->data[$i]['legacyResults'] = $this->secondary['legacyResults']
                ->getByKeys($lookup[$rider['rider_id']]);
        }
        // Do we also have a history to bind to?
        if (isset($this->secondary['history'])) {
            $raw = $this->secondary['legacyResults']->pluck('rider_key');
            $lookup = array_fill_keys($this->secondary['history']->pluck('season_key'), []);
            foreach ($raw as $id => $season_key) {
                $lookup[$season_key][] = $id;
            }
            foreach ($this->secondary['history'] as $history) {
                if (!$history->is_detailed) {
                    $history->results
                        = $history->teams->results
                        = $this->secondary['legacyResults']->getByKeys($lookup[$history->season_key]);
                }
            }
        }
    }
}
