##
## Identify how many teams are still tied
##
DROP FUNCTION IF EXISTS `nfl_standings_tiebrk_num`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_tiebrk_num`(
  v_group_id TINYINT UNSIGNED,
  v_group_pos TINYINT UNSIGNED
) RETURNS TINYINT UNSIGNED
    NOT DETERMINISTIC
    COMMENT 'Identify how many teams we have left to break'
BEGIN

  DECLARE v_num_tied TINYINT UNSIGNED;

  SELECT SUM(group_pos_tied)
         INTO v_num_tied
  FROM tmp_tiebrk_list
  WHERE group_id = v_group_id
  AND   group_pos = v_group_pos;

  RETURN v_num_tied;

END $$

DELIMITER ;

##
## Result of a tie break?
##
DROP FUNCTION IF EXISTS `nfl_standings_tiebrk_status`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_tiebrk_status`() RETURNS ENUM('success', 'partial', 'fail')
    NOT DETERMINISTIC
    COMMENT 'Identify the status of an NFL tiebreak test'
BEGIN

  # Declare vars
  DECLARE v_num_tied TINYINT UNSIGNED;
  DECLARE v_num_total TINYINT UNSIGNED;

  # Identify how we did...
  SELECT SUM(new_pos = 0 AND new_pos_tied = 1),
         COUNT(*)
         INTO v_num_tied,
              v_num_total
  FROM tmp_tiebrk_teams;

  # Then return...
  IF v_num_tied = 0 THEN
    # Only one team in first position, tie broken
    RETURN 'success';

  ELSEIF v_num_tied < v_num_total THEN
    # Multiple - but not all - teams tied in first position, so we have only partially broken the tie
    RETURN 'partial';

  ELSE
    # Nothing broken
    RETURN 'fail';
  END IF;

END $$

DELIMITER ;

