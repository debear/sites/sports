##
## Division calcs
##
DROP PROCEDURE IF EXISTS `nfl_standings_pos_div`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_pos_div`()
    COMMENT 'Calculate NFL division standings'
BEGIN

  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN div_pos TINYINT UNSIGNED;

  INSERT INTO tmp_pos (team_id, div_pos)
    SELECT tmp_pos_cpA.team_id, COUNT(tmp_pos_cpB.team_id) + 1 AS div_pos
    FROM tmp_pos_cpA
    LEFT JOIN tmp_pos_cpB
      ON (tmp_pos_cpB.div_id = tmp_pos_cpA.div_id
      AND tmp_pos_cpB.win_pct > tmp_pos_cpA.win_pct)
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE div_pos = VALUES(div_pos);

  # Identify ties...
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN div_pos_tied TINYINT UNSIGNED DEFAULT 0;

  INSERT INTO tmp_pos (team_id, div_pos_tied)
    SELECT tmp_pos_cpA.team_id, 1
    FROM tmp_pos_cpA
    JOIN tmp_pos_cpB
      ON (tmp_pos_cpB.div_id = tmp_pos_cpA.div_id
      AND tmp_pos_cpB.div_pos = tmp_pos_cpA.div_pos
      AND tmp_pos_cpB.team_id <> tmp_pos_cpA.team_id)
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE div_pos_tied = VALUES(div_pos_tied);

  # Now break...
  CALL nfl_standings_break('div');
  ALTER TABLE tmp_pos DROP COLUMN div_pos_tied;

END $$

DELIMITER ;

##
## Conference calcs
##
DROP PROCEDURE IF EXISTS `nfl_standings_pos_conf`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_pos_conf`()
    COMMENT 'Calculate NFL conference standings'
BEGIN

  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN conf_pos TINYINT UNSIGNED DEFAULT 0;

  # Team B > Team A iff:
  #  - Both A and B division winners and Team B has a higher Win %age
  #  - Team A is not a division winner and, Team B either:
  #     - Is a division winner, or
  #     - Has a higher Win %age
  INSERT INTO tmp_pos (team_id, conf_pos)
    SELECT tmp_pos_cpA.team_id, COUNT(tmp_pos_cpB.team_id) + 1 AS conf_pos
    FROM tmp_pos_cpA
    LEFT JOIN tmp_pos_cpB
      ON (tmp_pos_cpB.conf_id = tmp_pos_cpA.conf_id
      AND ((tmp_pos_cpA.div_pos = 1 AND tmp_pos_cpB.div_pos = 1 AND tmp_pos_cpA.win_pct < tmp_pos_cpB.win_pct)
       OR  (tmp_pos_cpA.div_pos <> 1 AND (tmp_pos_cpB.div_pos = 1 OR tmp_pos_cpA.win_pct < tmp_pos_cpB.win_pct))))
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE conf_pos = VALUES(conf_pos);

  # Identify ties...
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN conf_pos_tied TINYINT UNSIGNED DEFAULT 0;

  INSERT INTO tmp_pos (team_id, conf_pos_tied)
    SELECT tmp_pos_cpA.team_id, 1
    FROM tmp_pos_cpA
    JOIN tmp_pos_cpB
      ON (tmp_pos_cpB.conf_id = tmp_pos_cpA.conf_id
      AND tmp_pos_cpB.conf_pos = tmp_pos_cpA.conf_pos
      AND tmp_pos_cpB.team_id <> tmp_pos_cpA.team_id)
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE conf_pos_tied = VALUES(conf_pos_tied);

  # Now break...
  CALL nfl_standings_break('conf');
  ALTER TABLE tmp_pos DROP COLUMN conf_pos_tied;

END $$

DELIMITER ;

##
## League calcs
##
DROP PROCEDURE IF EXISTS `nfl_standings_pos_league`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_pos_league`()
    COMMENT 'Calculate NFL league standings'
BEGIN

  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN league_pos TINYINT UNSIGNED DEFAULT 0;

  INSERT INTO tmp_pos (team_id, league_pos)
    SELECT tmp_pos_cpA.team_id, COUNT(tmp_pos_cpB.team_id) + 1 AS league_pos
    FROM tmp_pos_cpA
    LEFT JOIN tmp_pos_cpB
      ON ((tmp_pos_cpA.div_pos = 1 AND tmp_pos_cpB.div_pos = 1 AND tmp_pos_cpA.win_pct < tmp_pos_cpB.win_pct)
       OR (tmp_pos_cpA.div_pos <> 1 AND (tmp_pos_cpB.div_pos = 1 OR tmp_pos_cpA.win_pct < tmp_pos_cpB.win_pct)))
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE league_pos = VALUES(league_pos);

  # Identify ties...
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN league_pos_tied TINYINT UNSIGNED DEFAULT 0;

  INSERT INTO tmp_pos (team_id, league_pos_tied)
    SELECT tmp_pos_cpA.team_id, 1
    FROM tmp_pos_cpA
    JOIN tmp_pos_cpB
      ON (tmp_pos_cpB.league_pos = tmp_pos_cpA.league_pos
      AND tmp_pos_cpB.team_id <> tmp_pos_cpA.team_id)
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE league_pos_tied = VALUES(league_pos_tied);

  # Now break...
  CALL nfl_standings_break('league');
  ALTER TABLE tmp_pos DROP COLUMN league_pos_tied;

END $$

DELIMITER ;

##
## Draft order calcs
##
DROP PROCEDURE IF EXISTS `nfl_standings_pos_draft`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_pos_draft`()
    COMMENT 'Calculate NFL draft order'
BEGIN

  # Identify last playoff round played by team
  DROP TEMPORARY TABLE IF EXISTS tmp_sched_po;
  CREATE TEMPORARY TABLE tmp_sched_po (
    team_id VARCHAR(3),
    week TINYINT UNSIGNED,
    result ENUM('w','l'),
    PRIMARY KEY (team_id, week)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id,
           IFNULL(SPORTS_NFL_SCHEDULE.week, 0) AS week,
           IF((SPORTS_NFL_SCHEDULE.home = tmp_sched.team_id AND SPORTS_NFL_SCHEDULE.home_score > SPORTS_NFL_SCHEDULE.visitor_score)
               OR (SPORTS_NFL_SCHEDULE.visitor = tmp_sched.team_id AND SPORTS_NFL_SCHEDULE.home_score < SPORTS_NFL_SCHEDULE.visitor_score),
              'w', 'l') AS result
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.week = 1)
    LEFT JOIN SPORTS_NFL_SCHEDULE
      ON (SPORTS_NFL_SCHEDULE.season = tmp_sched.season
      AND SPORTS_NFL_SCHEDULE.game_type = 'playoff'
      AND (SPORTS_NFL_SCHEDULE.home = tmp_sched.team_id
        OR SPORTS_NFL_SCHEDULE.visitor = tmp_sched.team_id))
    GROUP BY tmp_teams.team_id, SPORTS_NFL_SCHEDULE.week;

  ALTER TABLE tmp_pos ADD COLUMN playoff_week TINYINT UNSIGNED AFTER win_pct;

  INSERT INTO tmp_pos (team_id, playoff_week)
    SELECT tmp_teams.team_id,
           IFNULL(MAX(IF(tmp_sched_po.week < 4 OR tmp_sched_po.result = 'l', tmp_sched_po.week, 5)), 0) AS playoff_week
    FROM tmp_teams
    LEFT JOIN tmp_sched_po
      ON (tmp_sched_po.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id
  ON DUPLICATE KEY UPDATE playoff_week = VALUES(playoff_week);

  # And calculate the positions...
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN draft_pos TINYINT UNSIGNED;

  INSERT INTO tmp_pos (team_id, draft_pos)
    SELECT tmp_pos_cpA.team_id, COUNT(tmp_pos_cpB.team_id) + 1 AS draft_pos
    FROM tmp_pos_cpA
    LEFT JOIN tmp_pos_cpB
      ON (tmp_pos_cpA.playoff_week > tmp_pos_cpB.playoff_week
       OR (tmp_pos_cpA.playoff_week = tmp_pos_cpB.playoff_week
       AND tmp_pos_cpA.win_pct > tmp_pos_cpB.win_pct))
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE draft_pos = VALUES(draft_pos);

  # Identify ties...
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');
  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpB');

  ALTER TABLE tmp_pos ADD COLUMN draft_pos_tied TINYINT UNSIGNED DEFAULT 0;

  INSERT INTO tmp_pos (team_id, draft_pos_tied)
    SELECT tmp_pos_cpA.team_id, 1
    FROM tmp_pos_cpA
    JOIN tmp_pos_cpB
      ON (tmp_pos_cpB.draft_pos = tmp_pos_cpA.draft_pos
      AND tmp_pos_cpB.team_id <> tmp_pos_cpA.team_id)
    GROUP BY tmp_pos_cpA.team_id
  ON DUPLICATE KEY UPDATE draft_pos_tied = VALUES(draft_pos_tied);

  # Create our temporary table that will store genuinely tied teams (that we cannot break correctly, only guess)
  DROP TEMPORARY TABLE IF EXISTS tmp_draft_coin_toss;
  CREATE TEMPORARY TABLE tmp_draft_coin_toss (
    team_id VARCHAR(3),
    draft_pos TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY;

  # Now break and update with tied team info
  CALL nfl_standings_break('draft');
  ALTER TABLE tmp_pos DROP COLUMN draft_pos_tied,
                      ADD COLUMN draft_pos_coin_toss TINYINT UNSIGNED DEFAULT 0;

  CALL _duplicate_tmp_table('tmp_pos', 'tmp_pos_cpA');

  INSERT INTO tmp_pos (team_id, draft_pos_coin_toss)
    SELECT tmp_pos_cpA.team_id, 1
    FROM tmp_draft_coin_toss
    JOIN tmp_pos_cpA
      ON (tmp_pos_cpA.team_id = tmp_draft_coin_toss.team_id)
  ON DUPLICATE KEY UPDATE draft_pos_coin_toss = VALUES(draft_pos_coin_toss);

END $$

DELIMITER ;

