#
# Status Codes: Home Field, Bye, Div Winner, Wild Card, Eliminated
#
DROP PROCEDURE IF EXISTS `nfl_standings_codes`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Determine NFL standing status codes'
BEGIN

  # Create the table that will store this data
  DROP TEMPORARY TABLE IF EXISTS tmp_standings_codes;
  CREATE TEMPORARY TABLE tmp_standings_codes (
    team_id VARCHAR(3),
    code CHAR(1),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY;

  # Determine game counts by team
  DROP TEMPORARY TABLE IF EXISTS tmp_records_games;
  CREATE TEMPORARY TABLE tmp_records_games (
    team_id VARCHAR(3),
    num_home TINYINT UNSIGNED DEFAULT 0,
    num_away TINYINT UNSIGNED DEFAULT 0,
    num_games TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY;
  INSERT INTO tmp_records_games (team_id, num_home)
    SELECT home AS team_id, COUNT(*) AS num_games
    FROM SPORTS_NFL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    AND   IFNULL(status, 'TBP') NOT IN ('PPD', 'CNC')
    GROUP BY home;
  INSERT INTO tmp_records_games (team_id, num_away)
    SELECT visitor AS team_id, COUNT(*) AS num_games
    FROM SPORTS_NFL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    AND   IFNULL(status, 'TBP') NOT IN ('PPD', 'CNC')
    GROUP BY visitor
  ON DUPLICATE KEY UPDATE num_away = VALUES(num_away);
  UPDATE tmp_records_games SET num_games = num_home + num_away;

  # Establish the best possible records
  DROP TEMPORARY TABLE IF EXISTS tmp_records_best;
  CREATE TEMPORARY TABLE tmp_records_best (
    team_id VARCHAR(3),
    num_gp TINYINT UNSIGNED,
    wins TINYINT UNSIGNED,
    loss TINYINT UNSIGNED,
    ties TINYINT UNSIGNED,
    win_pct FLOAT,
    num_gp_left TINYINT UNSIGNED,
    best_wins TINYINT UNSIGNED,
    best_loss TINYINT UNSIGNED,
    best_ties TINYINT UNSIGNED,
    best_win_pct FLOAT,
    worst_wins TINYINT UNSIGNED,
    worst_loss TINYINT UNSIGNED,
    worst_ties TINYINT UNSIGNED,
    worst_win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_records.team_id,
           tmp_records.wins + tmp_records.loss + tmp_records.ties AS num_gp,
           tmp_records.wins,
           tmp_records.loss,
           tmp_records.ties,
           NULL AS win_pct,
           tmp_records_games.num_games - tmp_records.wins - tmp_records.loss - tmp_records.ties AS num_gp_left,
           tmp_records_games.num_games - tmp_records.loss - tmp_records.ties AS best_wins,
           tmp_records.loss AS best_loss,
           tmp_records.ties AS best_ties,
           NULL AS best_win_pct,
           tmp_records.wins AS worst_wins,
           tmp_records_games.num_games - tmp_records.wins - tmp_records.ties AS worst_loss,
           tmp_records.ties AS worst_ties,
           NULL AS worst_win_pct
    FROM tmp_records
    JOIN tmp_records_games
      ON (tmp_records_games.team_id = tmp_records.team_id);

  UPDATE tmp_records_best
  SET win_pct = (wins + (ties / 2)) / (wins + loss + ties),
      best_win_pct = (best_wins + (best_ties / 2)) / (best_wins + best_loss + best_ties),
      worst_win_pct = (worst_wins + (worst_ties / 2)) / (worst_wins + worst_loss + worst_ties);

  # Run the tests
  CALL nfl_standings_codes_exec(v_season);

END $$

DELIMITER ;

#
# Determine codes for each team individually
#
DROP PROCEDURE IF EXISTS `nfl_standings_codes_exec`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_exec`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Determine the NFL standing codes for each team'
BEGIN

  # Declare vars
  DECLARE v_team_id VARCHAR(3);
  DECLARE v_mode ENUM('actual','estimate');
  DECLARE v_team_code CHAR(1) DEFAULT NULL;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Our cursor to loop through the teams
  DECLARE cur_teams CURSOR FOR
    SELECT team_id FROM tmp_teams_loop ORDER BY team_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Copy the temporary table to prevent re-opening issues
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_loop');

  # Determine if all teams have played their games, in which case we use actual div/conf positions
  SELECT IF(MAX(num_gp_left) > 0, 'estimate', 'actual') INTO v_mode FROM tmp_records_best;

  OPEN cur_teams;
  loop_teams: LOOP

    FETCH cur_teams INTO v_team_id;
    IF v_done = 1 THEN LEAVE loop_teams; END IF;

    # If estimating, build the standings
    IF v_mode = 'estimate' THEN
      CALL nfl_standings_codes_build_standings(v_team_id);
    END IF;

    # Eliminated from the playoffs?
    CALL nfl_standings_codes_process_eliminated(v_season, v_team_id, v_mode, v_team_code);
    IF v_team_code IS NULL THEN

      # Home Field advantage?
      CALL nfl_standings_codes_process_homefield(v_season, v_team_id, v_mode, v_team_code);
      IF v_team_code IS NULL THEN

        # First Round Bye?
        CALL nfl_standings_codes_process_bye(v_season, v_team_id, v_mode, v_team_code);
        IF v_team_code IS NULL THEN

          # Division Winner?
          CALL nfl_standings_codes_process_divwinner(v_season, v_team_id, v_mode, v_team_code);
          IF v_team_code IS NULL THEN

            # Wildcard Team?
            CALL nfl_standings_codes_process_wildcard(v_season, v_team_id, v_mode, v_team_code);

          END IF; # Division Winner

        END IF; # First Round Bye

      END IF; # Home Field

    END IF; # Eliminated

    # Store
    INSERT INTO tmp_standings_codes (team_id, code)
      VALUES (v_team_id, v_team_code);

  END LOOP loop_teams;
  CLOSE cur_teams;

END $$

DELIMITER ;

