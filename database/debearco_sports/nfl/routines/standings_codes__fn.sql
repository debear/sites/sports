##
## Get a numeric configuration value from our config store
##
DROP FUNCTION IF EXISTS `nfl_standings_codes_config_numeric`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_codes_config_numeric`(
  v_name VARCHAR(20)
) RETURNS SMALLINT
    DETERMINISTIC
    COMMENT 'Get a numeric configuration value for the standing codes run'
BEGIN

  DECLARE v_return SMALLINT;
  SELECT value_num INTO v_return
  FROM tmp_config
  WHERE name = v_name;

  RETURN v_return;

END $$

DELIMITER ;

##
## Determine who a team is in a given teams division
##
DROP FUNCTION IF EXISTS `nfl_standings_codes_team_bydiv`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_codes_team_bydiv`(
  v_div_pos TINYINT UNSIGNED
) RETURNS VARCHAR(3)
    NOT DETERMINISTIC
    COMMENT 'Identify a team based on a division pos'
BEGIN

  DECLARE v_team_id VARCHAR(3);

  SELECT team_id INTO v_team_id
  FROM tmp_est_records_div
  WHERE is_team_div = 1
  AND   div_pos = v_div_pos;

  RETURN v_team_id;

END $$

DELIMITER ;

##
## Determine who a team is in a given teams conference
##
DROP FUNCTION IF EXISTS `nfl_standings_codes_team_byconf`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_codes_team_byconf`(
  v_conf_pos TINYINT UNSIGNED
) RETURNS VARCHAR(3)
    NOT DETERMINISTIC
    COMMENT 'Identify a team based on a conference pos'
BEGIN

  DECLARE v_team_id VARCHAR(3);

  SELECT team_id INTO v_team_id
  FROM tmp_est_records_conf
  WHERE conf_pos = v_conf_pos;

  RETURN v_team_id;

END $$

DELIMITER ;

##
## Determine a teams conference or division position
##
DROP FUNCTION IF EXISTS `nfl_standings_codes_team_pos`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_codes_team_pos`(
  v_team_id VARCHAR(3),
  v_group ENUM('conf','div')
) RETURNS TINYINT UNSIGNED
    NOT DETERMINISTIC
    COMMENT 'Establish where a team finished in its division/conference'
BEGIN

  DECLARE v_pos TINYINT UNSIGNED;

  SELECT IF(v_group = 'conf', conf_pos, div_pos) INTO v_pos
  FROM tmp_pos
  WHERE team_id = v_team_id;

  RETURN v_pos;

END $$

DELIMITER ;

##
## Identify a teams current record
##
DROP FUNCTION IF EXISTS `nfl_standings_codes_record_worst`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_codes_record_worst`(
  v_team_id VARCHAR(3)
) RETURNS FLOAT
    NOT DETERMINISTIC
    COMMENT 'Identify a team''s worst win %age'
BEGIN

  DECLARE v_win_pct FLOAT;

  SELECT worst_win_pct INTO v_win_pct
  FROM tmp_records_best
  WHERE team_id = v_team_id;

  RETURN v_win_pct;

END $$

DELIMITER ;

##
## Identify a teams current record
##
DROP FUNCTION IF EXISTS `nfl_standings_codes_record_best`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_standings_codes_record_best`(
  v_team_id VARCHAR(3)
) RETURNS FLOAT
    NOT DETERMINISTIC
    COMMENT 'Identify a team''s best possible win %age'
BEGIN

  DECLARE v_win_pct FLOAT;

  SELECT best_win_pct INTO v_win_pct
  FROM tmp_records_best
  WHERE team_id = v_team_id;

  RETURN v_win_pct;

END $$

DELIMITER ;

