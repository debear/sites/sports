#
# Regular history for a season
#
DROP PROCEDURE IF EXISTS `nfl_history_regular`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_history_regular`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Establish which teams did what in the regular season'
proc_hist: BEGIN

  # Declare vars
  DECLARE v_run TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_week TINYINT UNSIGNED;

  # Determine if run already
  SELECT COUNT(*) INTO v_run
  FROM SPORTS_NFL_TEAMS_HISTORY_REGULAR
  WHERE season = v_season;

  IF v_run > 0 THEN
    LEAVE proc_hist;
  END IF;

  # Copy from standings and satellite tables into SPORTS_NFL_TEAMS_HISTORY_REGULAR
  SELECT MAX(week) INTO v_week FROM SPORTS_NFL_SCHEDULE WHERE season = v_season AND game_type = 'regular';

  INSERT INTO SPORTS_NFL_TEAMS_HISTORY_REGULAR (team_id, season, wins, loss, ties, pos)
    SELECT team_id, season, wins, loss, ties, pos_div AS pos
    FROM SPORTS_NFL_STANDINGS
    WHERE season = v_season
    AND   week = v_week;

END $$

DELIMITER ;

