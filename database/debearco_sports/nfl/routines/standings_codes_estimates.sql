##
## Determine the positions in an estimated situation
##
DROP PROCEDURE IF EXISTS `nfl_standings_codes_build_standings`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_build_standings`(
  v_team_id VARCHAR(3)
)
    COMMENT 'Determine NFL standings based on estimated records'
BEGIN

  DECLARE v_team_div TINYINT UNSIGNED;

  SELECT div_id INTO v_team_div
  FROM tmp_teams
  WHERE team_id = v_team_id;

  #
  # Add div/conf info to the records for sorting later
  #
  CALL _duplicate_tmp_table('tmp_records_best', 'tmp_records_best_cpA');
  ALTER TABLE tmp_records_best_cpA ADD COLUMN div_id TINYINT UNSIGNED AFTER team_id,
                                   ADD COLUMN conf_id TINYINT UNSIGNED AFTER div_id;
  UPDATE tmp_records_best_cpA
  LEFT JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_records_best_cpA.team_id)
  SET tmp_records_best_cpA.div_id = tmp_teams.div_id,
      tmp_records_best_cpA.conf_id = tmp_teams.conf_id;

  DELETE tmp_records_best_cpA.*
  FROM tmp_teams
  JOIN tmp_records_best_cpA
    ON (tmp_records_best_cpA.conf_id <> tmp_teams.conf_id)
  WHERE tmp_teams.team_id = v_team_id;

  CALL _duplicate_tmp_table('tmp_records_best_cpA', 'tmp_records_best_cpB');

  #
  # First, div positions
  #
  DROP TEMPORARY TABLE IF EXISTS tmp_est_records_div;
  CREATE TEMPORARY TABLE tmp_est_records_div (
    team_id VARCHAR(3),
    is_team_div TINYINT UNSIGNED,
    win_pct FLOAT,
    div_pos TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_records_best_cpA.team_id,
           tmp_records_best_cpA.div_id = v_team_div AS is_team_div,
           IF(tmp_records_best_cpA.team_id = v_team_id,
              tmp_records_best_cpA.win_pct,
              tmp_records_best_cpA.best_win_pct) AS win_pct,
           COUNT(tmp_records_best_cpB.team_id) + 1 AS div_pos
    FROM tmp_records_best_cpA
    LEFT JOIN tmp_records_best_cpB
      ON (tmp_records_best_cpB.team_id <> v_team_id
      AND tmp_records_best_cpB.team_id <> tmp_records_best_cpA.team_id
      AND tmp_records_best_cpB.div_id = tmp_records_best_cpA.div_id
      AND (tmp_records_best_cpB.best_win_pct > IF(tmp_records_best_cpA.team_id = v_team_id,
                                                  tmp_records_best_cpA.win_pct,
                                                  tmp_records_best_cpA.best_win_pct)
        OR (tmp_records_best_cpB.best_win_pct = IF(tmp_records_best_cpA.team_id = v_team_id,
                                                   tmp_records_best_cpA.win_pct,
                                                   tmp_records_best_cpA.best_win_pct)
        AND STRCMP(tmp_records_best_cpB.team_id, tmp_records_best_cpA.team_id) = 1)))
    WHERE tmp_records_best_cpA.team_id <> v_team_id
    GROUP BY tmp_records_best_cpA.team_id;

  #
  # Then, conference
  #
  CALL _duplicate_tmp_table('tmp_est_records_div', 'tmp_est_records_div_cp');

  DROP TEMPORARY TABLE IF EXISTS tmp_est_records_conf;
  CREATE TEMPORARY TABLE tmp_est_records_conf (
    team_id VARCHAR(3),
    conf_pos TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_est_records_div.team_id,
           COUNT(tmp_est_records_div_cp.team_id) + 1 AS conf_pos
    FROM tmp_est_records_div
    LEFT JOIN tmp_est_records_div_cp
      ON (tmp_est_records_div_cp.team_id <> v_team_id
      AND tmp_est_records_div_cp.team_id <> tmp_est_records_div.team_id
      AND ((tmp_est_records_div.div_pos = 1
            AND tmp_est_records_div_cp.div_pos = 1
            AND (tmp_est_records_div.win_pct < tmp_est_records_div_cp.win_pct
                 OR (tmp_est_records_div.win_pct = tmp_est_records_div_cp.win_pct
                     AND STRCMP(tmp_est_records_div.team_id, tmp_est_records_div_cp.team_id) = 1)))
       OR  (tmp_est_records_div.div_pos <> 1
            AND (tmp_est_records_div_cp.div_pos = 1
                 OR tmp_est_records_div.win_pct < tmp_est_records_div_cp.win_pct
                 OR (tmp_est_records_div.win_pct = tmp_est_records_div_cp.win_pct
                     AND STRCMP(tmp_est_records_div.team_id, tmp_est_records_div_cp.team_id) = 1)))))
    GROUP BY tmp_est_records_div.team_id;

  ALTER TABLE tmp_est_records_div DROP COLUMN win_pct;

END $$

DELIMITER ;

