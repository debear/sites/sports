##
## Individual build components
##
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build`()
    COMMENT 'Calculate the NFL power ranks component parts'
BEGIN

  # Temporary table our intermediate results will be stored in
  DROP TEMPORARY TABLE IF EXISTS tmp_stats;
  CREATE TEMPORARY TABLE tmp_stats (
    team_id VARCHAR(3),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id
    FROM tmp_teams;

  # Win %age
  CALL nfl_power_ranks_build_winpct(10);

  # TDs
  CALL nfl_power_ranks_build_td(3);

  # Net Yardage
  CALL nfl_power_ranks_build_netyards(6);

  # Time of Possession
  CALL nfl_power_ranks_build_top(5);

  # 3rd/4th Down %age
  CALL nfl_power_ranks_build_34dnpct(4);

  # Red Zone %age
  CALL nfl_power_ranks_build_rzpct(7);

  # Penalty Yardage
  CALL nfl_power_ranks_build_penyards(5);

  # Net Turnovers
  CALL nfl_power_ranks_build_netto(8);

END $$

DELIMITER ;

#
# Win %age
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_winpct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_winpct`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks win percentages'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('DECIMAL(5,3) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT team_id,
           SUM(IF(result = 'w', 1, IF(result = 't', 0.5, 0))) / COUNT(*) AS tot_ov,
           IF(SUM(is_recent) = 0, 0,
              SUM(IF(is_recent AND result = 'w', 1, IF(is_recent AND result = 't', 0.5, 0))) / SUM(is_recent)) AS tot_rec,
           IF(SUM(is_div) = 0, 0,
              SUM(IF(is_div AND result = 'w', 1, IF(is_div AND result = 't', 0.5, 0))) / SUM(is_div)) AS tot_div
    FROM tmp_sched
    GROUP BY team_id;

  # Process
  CALL nfl_power_ranks_build_process('win_pct', 0, v_weight);

END $$

DELIMITER ;

#
# TDs
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_td`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_td`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks touchdown numbers'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('TINYINT UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT tmp_sched.team_id,
           SUM(NUM.num_pass + NUM.num_rush + NUM.num_int + NUM.num_ko + NUM.num_punt) AS tot_ov,
           SUM(IF(is_recent, NUM.num_pass + NUM.num_rush + NUM.num_int + NUM.num_ko + NUM.num_punt, 0)) AS tot_rec,
           SUM(IF(is_div, NUM.num_pass + NUM.num_rush + NUM.num_int + NUM.num_ko + NUM.num_punt, 0)) AS tot_div
    FROM tmp_sched
    LEFT JOIN SPORTS_NFL_GAME_STATS_TDS AS NUM
      ON (NUM.season = tmp_sched.season
      AND NUM.game_type = tmp_sched.game_type
      AND NUM.week = tmp_sched.week
      AND NUM.game_id = tmp_sched.game_id
      AND NUM.team_id = tmp_sched.team_id)
    GROUP BY tmp_sched.team_id;

  # Process
  CALL nfl_power_ranks_build_process('num_td', 0, v_weight);

END $$

DELIMITER ;

#
# Net Yardage
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_netyards`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_netyards`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks net yardage numbers'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('SMALLINT SIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT tmp_sched.team_id,
           SUM((NUM_ME.yards_pass + NUM_ME.yards_rush) - (NUM_OPP.yards_pass + NUM_OPP.yards_rush)) AS tot_ov,
           SUM(IF(is_recent, (NUM_ME.yards_pass + NUM_ME.yards_rush) - (NUM_OPP.yards_pass + NUM_OPP.yards_rush), 0)) AS tot_rec,
           SUM(IF(is_div, (NUM_ME.yards_pass + NUM_ME.yards_rush) - (NUM_OPP.yards_pass + NUM_OPP.yards_rush), 0)) AS tot_div
    FROM tmp_sched
    LEFT JOIN SPORTS_NFL_GAME_STATS_YARDS AS NUM_ME
      ON (NUM_ME.season = tmp_sched.season
      AND NUM_ME.game_type = tmp_sched.game_type
      AND NUM_ME.week = tmp_sched.week
      AND NUM_ME.game_id = tmp_sched.game_id
      AND NUM_ME.team_id = tmp_sched.team_id)
    LEFT JOIN SPORTS_NFL_GAME_STATS_YARDS AS NUM_OPP
      ON (NUM_OPP.season = tmp_sched.season
      AND NUM_OPP.game_type = tmp_sched.game_type
      AND NUM_OPP.week = tmp_sched.week
      AND NUM_OPP.game_id = tmp_sched.game_id
      AND NUM_OPP.team_id = tmp_sched.opp_id)
    GROUP BY tmp_sched.team_id;

  # Process
  CALL nfl_power_ranks_build_process('net_yards', 0, v_weight);

END $$

DELIMITER ;

#
# Time of Possession
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_top`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_top`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks time of possession'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('SMALLINT UNSIGNED'); # Storing as number of seconds...
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT tmp_sched.team_id,
           AVG(TIME_TO_SEC(time_of_poss)) AS tot_ov,
           AVG(IF(is_recent, TIME_TO_SEC(time_of_poss), NULL)) AS tot_rec,
           AVG(IF(is_div, TIME_TO_SEC(time_of_poss), NULL)) AS tot_div
    FROM tmp_sched
    LEFT JOIN SPORTS_NFL_GAME_STATS_MISC AS NUM
      ON (NUM.season = tmp_sched.season
      AND NUM.game_type = tmp_sched.game_type
      AND NUM.week = tmp_sched.week
      AND NUM.game_id = tmp_sched.game_id
      AND NUM.team_id = tmp_sched.team_id)
    GROUP BY tmp_sched.team_id;

  # Process
  CALL nfl_power_ranks_build_process('time_of_poss', 0, v_weight);

END $$

DELIMITER ;

#
# 3rd/4th Down %age
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_34dnpct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_34dnpct`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks 3rd/4th down percentages'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('FLOAT UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT tmp_sched.team_id,
           (SUM(NUM.num_3rd_cnv) + SUM(NUM.num_4th_cnv)) / (SUM(NUM.num_3rd) + SUM(NUM.num_4th)) AS tot_ov,
           IF((SUM(IF(is_recent, NUM.num_3rd, 0)) + SUM(IF(is_recent, NUM.num_4th, 0))) = 0, 0,
              (SUM(IF(is_recent, NUM.num_3rd_cnv, 0)) + SUM(IF(is_recent, NUM.num_4th_cnv, 0)))
                / (SUM(IF(is_recent, NUM.num_3rd, 0)) + SUM(IF(is_recent, NUM.num_4th, 0)))) AS tot_rec,
           IF((SUM(IF(is_div, NUM.num_3rd, 0)) + SUM(IF(is_div, NUM.num_4th, 0))) = 0, 0,
              (SUM(IF(is_div, NUM.num_3rd_cnv, 0)) + SUM(IF(is_div, NUM.num_4th_cnv, 0)))
                / (SUM(IF(is_div, NUM.num_3rd, 0)) + SUM(IF(is_div, NUM.num_4th, 0)))) AS tot_div
    FROM tmp_sched
    LEFT JOIN SPORTS_NFL_GAME_STATS_DOWNS AS NUM
      ON (NUM.season = tmp_sched.season
      AND NUM.game_type = tmp_sched.game_type
      AND NUM.week = tmp_sched.week
      AND NUM.game_id = tmp_sched.game_id
      AND NUM.team_id = tmp_sched.team_id)
    GROUP BY tmp_sched.team_id;

  # Process
  CALL nfl_power_ranks_build_process('34dn_pct', 0, v_weight);

END $$

DELIMITER ;

#
# Red Zone %age
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_rzpct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_rzpct`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks red zone percentages'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('FLOAT UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT tmp_sched.team_id,
           IF(SUM(NUM.rz_num), SUM(NUM.rz_cnv) / SUM(NUM.rz_num), 0) AS tot_ov,
           IF(SUM(IF(is_recent, NUM.rz_num, 0)) = 0, 0,
              SUM(IF(is_recent, NUM.rz_cnv, 0)) / SUM(IF(is_recent, NUM.rz_num, 0))) AS tot_rec,
           IF(SUM(IF(is_div, NUM.rz_num, 0)) = 0, 0,
              SUM(IF(is_div, NUM.rz_cnv, 0)) / SUM(IF(is_div, NUM.rz_num, 0))) AS tot_div
    FROM tmp_sched
    LEFT JOIN SPORTS_NFL_GAME_STATS_MISC AS NUM
      ON (NUM.season = tmp_sched.season
      AND NUM.game_type = tmp_sched.game_type
      AND NUM.week = tmp_sched.week
      AND NUM.game_id = tmp_sched.game_id
      AND NUM.team_id = tmp_sched.team_id)
    GROUP BY tmp_sched.team_id;

  # Process
  CALL nfl_power_ranks_build_process('rz_pct', 0, v_weight);

END $$

DELIMITER ;

#
# Penalty Yardage
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_penyards`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_penyards`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks penalty yard numbers'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('SMALLINT UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT tmp_sched.team_id,
           SUM(NUM.pens_yards) AS tot_ov,
           SUM(IF(is_recent, NUM.pens_yards, 0)) AS tot_rec,
           SUM(IF(is_div, NUM.pens_yards, 0)) AS tot_rec
    FROM tmp_sched
    LEFT JOIN SPORTS_NFL_GAME_STATS_MISC AS NUM
      ON (NUM.season = tmp_sched.season
      AND NUM.game_type = tmp_sched.game_type
      AND NUM.week = tmp_sched.week
      AND NUM.game_id = tmp_sched.game_id
      AND NUM.team_id = tmp_sched.team_id)
    GROUP BY tmp_sched.team_id;

  # Process
  CALL nfl_power_ranks_build_process('pen_yards', 1, v_weight);

END $$

DELIMITER ;

#
# Net Turnovers
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_netto`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_netto`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks net turnover numbers'
BEGIN

  # Build the data
  CALL nfl_power_ranks_build_schema('TINYINT SIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_rec, tot_div)
    SELECT tmp_sched.team_id,
           SUM((CAST(NUM_OPP.int_num AS SIGNED) + CAST(NUM_OPP.fumb_lost AS SIGNED))
                - (CAST(NUM_ME.int_num AS SIGNED) + CAST(NUM_ME.fumb_lost AS SIGNED))) AS tot_ov,
           SUM(IF(is_recent, (CAST(NUM_OPP.int_num AS SIGNED) + CAST(NUM_OPP.fumb_lost AS SIGNED))
                              - (CAST(NUM_ME.int_num AS SIGNED) + CAST(NUM_ME.fumb_lost AS SIGNED)),
                  0)) AS tot_rec,
           SUM(IF(is_div, (CAST(NUM_OPP.int_num AS SIGNED) + CAST(NUM_OPP.fumb_lost AS SIGNED))
                           - (CAST(NUM_ME.int_num AS SIGNED) + CAST(NUM_ME.fumb_lost AS SIGNED)),
                  0)) AS tot_div
    FROM tmp_sched
    LEFT JOIN SPORTS_NFL_GAME_STATS_TURNOVERS AS NUM_ME
      ON (NUM_ME.season = tmp_sched.season
      AND NUM_ME.game_type = tmp_sched.game_type
      AND NUM_ME.week = tmp_sched.week
      AND NUM_ME.game_id = tmp_sched.game_id
      AND NUM_ME.team_id = tmp_sched.team_id)
    LEFT JOIN SPORTS_NFL_GAME_STATS_TURNOVERS AS NUM_OPP
      ON (NUM_OPP.season = tmp_sched.season
      AND NUM_OPP.game_type = tmp_sched.game_type
      AND NUM_OPP.week = tmp_sched.week
      AND NUM_OPP.game_id = tmp_sched.game_id
      AND NUM_OPP.team_id = tmp_sched.opp_id)
    GROUP BY tmp_sched.team_id;

  # Process
  CALL nfl_power_ranks_build_process('net_to', 0, v_weight);

END $$

DELIMITER ;

