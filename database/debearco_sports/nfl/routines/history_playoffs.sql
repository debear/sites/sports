#
# Playoff history for a season
#
DROP PROCEDURE IF EXISTS `nfl_history_playoffs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_history_playoffs`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Establish which teams did what in the playoffs'
BEGIN

  # Declare vars
  DECLARE v_week TINYINT UNSIGNED;
  DECLARE v_league_id TINYINT UNSIGNED;

  # Determine the league_id
  SELECT CONF.parent_id INTO v_league_id
  FROM SPORTS_NFL_TEAMS_GROUPINGS AS TEAM
  JOIN SPORTS_NFL_GROUPINGS AS DIVISION
    ON (DIVISION.grouping_id = TEAM.grouping_id)
  JOIN SPORTS_NFL_GROUPINGS AS CONF
    ON (CONF.grouping_id = DIVISION.parent_id)
  WHERE v_season BETWEEN TEAM.season_from AND IFNULL(TEAM.season_to, 2099)
  LIMIT 1;

  # Which teams made the playoffs, and where did they finish in the division
  SELECT MAX(week) INTO v_week FROM SPORTS_NFL_STANDINGS WHERE season = v_season;

  # Summarise the playoffs
  # - Home team
  INSERT IGNORE INTO SPORTS_NFL_TEAMS_HISTORY_PLAYOFF (team_id, season, league_id, round, round_code, opp_team_id, `for`, against, info)
    SELECT home AS team_id, season, v_league_id AS league_id, week AS round,
           CASE week WHEN 1 THEN 'WC' WHEN 2 THEN 'DIV' WHEN 3 THEN 'CONF' ELSE 'SB' END AS round_code,
           visitor AS opp_team_id, home_score AS `for`, visitor_score AS against, IF(status = 'F', NULL, status) AS info
    FROM SPORTS_NFL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'playoff'
    AND   IFNULL(status, 'TBP') NOT IN ('TBP', 'PPD', 'CNC');
  # - Visiting team
  INSERT IGNORE INTO SPORTS_NFL_TEAMS_HISTORY_PLAYOFF (team_id, season, league_id, round, round_code, opp_team_id, `for`, against, info)
    SELECT visitor AS team_id, season, v_league_id AS league_id, week AS round,
           CASE week WHEN 1 THEN 'WC' WHEN 2 THEN 'DIV' WHEN 3 THEN 'CONF' ELSE 'SB' END AS round_code,
           home AS opp_team_id, visitor_score AS `for`, home_score AS against, IF(status = 'F', NULL, status) AS info
    FROM SPORTS_NFL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'playoff'
    AND   IFNULL(status, 'TBP') NOT IN ('TBP', 'PPD', 'CNC');

  # And order
  ALTER TABLE SPORTS_NFL_TEAMS_HISTORY_PLAYOFF ORDER BY team_id, season, round;

  # Aggregate the data
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    season SMALLINT UNSIGNED,
    league_id TINYINT UNSIGNED,
    one_game TINYINT UNSIGNED,
    wildcard TINYINT UNSIGNED,
    div_champ TINYINT UNSIGNED,
    conf_champ TINYINT UNSIGNED,
    league_champ TINYINT UNSIGNED,
    playoff_champ TINYINT UNSIGNED,
    PRIMARY KEY (team_id),
    INDEX alt_pri (season, team_id)
  ) ENGINE = MEMORY
    SELECT team_id, season, v_league_id AS league_id,
           0 AS one_game,
           pos_div <> 1 AS wildcard, pos_div = 1 AS div_champ,
           0 AS conf_champ, 0 AS league_champ, 0 AS playoff_champ
    FROM SPORTS_NFL_STANDINGS
    WHERE season = v_season
    AND   week = v_week
    AND   IFNULL(status_code, 'e') <> 'e';

  # Then those that made the Super Bowl => conf_champ and playoff_champ fields
  SELECT 4 INTO v_week;
  UPDATE tmp_teams
  JOIN SPORTS_NFL_SCHEDULE
    ON (SPORTS_NFL_SCHEDULE.season = tmp_teams.season
    AND SPORTS_NFL_SCHEDULE.game_type = 'playoff'
    AND SPORTS_NFL_SCHEDULE.week = v_week
    AND (SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id
      OR SPORTS_NFL_SCHEDULE.visitor = tmp_teams.team_id))
  SET conf_champ = 1,
      playoff_champ = (tmp_teams.team_id = SPORTS_NFL_SCHEDULE.home AND SPORTS_NFL_SCHEDULE.home_score > SPORTS_NFL_SCHEDULE.visitor_score)
                   OR (tmp_teams.team_id = SPORTS_NFL_SCHEDULE.visitor AND SPORTS_NFL_SCHEDULE.visitor_score > SPORTS_NFL_SCHEDULE.home_score);

  # Merge in to the final table and tidy
  INSERT INTO SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY (team_id, season, league_id, one_game, wildcard, div_champ, conf_champ, league_champ, playoff_champ)
    SELECT * FROM tmp_teams
  ON DUPLICATE KEY UPDATE one_game = VALUES(one_game),
                          wildcard = VALUES(wildcard),
                          div_champ = VALUES(div_champ),
                          conf_champ = VALUES(conf_champ),
                          league_champ = VALUES(league_champ),
                          playoff_champ = VALUES(playoff_champ);
  ALTER TABLE SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY ORDER BY team_id, season;

  # Combine into the title table
  INSERT INTO SPORTS_NFL_TEAMS_HISTORY_TITLES (team_id, league_id, div_champ, conf_champ, league_champ, playoff_champ)
    SELECT team_id, league_id,
           SUM(div_champ) AS div_champ,
           SUM(conf_champ) AS conf_champ,
           SUM(league_champ) AS league_champ,
           SUM(playoff_champ) AS playoff_champ
    FROM SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY
    GROUP BY team_id, league_id
  ON DUPLICATE KEY UPDATE div_champ = VALUES(div_champ),
                          conf_champ = VALUES(conf_champ),
                          league_champ = VALUES(league_champ),
                          playoff_champ = VALUES(playoff_champ);

  ALTER TABLE SPORTS_NFL_TEAMS_HISTORY_TITLES ORDER BY team_id, league_id;

END $$

DELIMITER ;

