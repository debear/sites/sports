#
# Initial standings
#
DROP PROCEDURE IF EXISTS `nfl_standings_initial`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_initial`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Create the initial NFL standings for a season'
BEGIN

  # Get the list of teams
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    city VARCHAR(20),
    franchise VARCHAR(20),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id),
    INDEX by_def (team_id, conf_id, div_id)
  ) ENGINE = MEMORY
    SELECT TEAM_GROUPING.team_id, TEAM.city, TEAM.franchise,
           GROUPING_DEF.parent_id AS conf_id,
           TEAM_GROUPING.grouping_id AS div_id
    FROM SPORTS_NFL_TEAMS_GROUPINGS AS TEAM_GROUPING
    JOIN SPORTS_NFL_TEAMS AS TEAM
      ON (TEAM.team_id = TEAM_GROUPING.team_id)
    JOIN SPORTS_NFL_GROUPINGS AS GROUPING_DEF
      ON (GROUPING_DEF.grouping_id = TEAM_GROUPING.grouping_id)
    WHERE v_season BETWEEN TEAM_GROUPING.season_from AND IFNULL(TEAM_GROUPING.season_to, 2099)
    ORDER BY TEAM_GROUPING.team_id;
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # Store our table
  DELETE FROM SPORTS_NFL_STANDINGS WHERE season = v_season;
  INSERT INTO SPORTS_NFL_STANDINGS (season, week, team_id, wins, loss, ties, pts_for, pts_against, home_wins, home_loss, home_ties, visitor_wins, visitor_loss, visitor_ties, conf_wins, conf_loss, conf_ties, div_wins, div_loss, div_ties, recent_wins, recent_loss, recent_ties, pos_league, pos_conf, pos_div, pos_draft, pos_draft_ct, streak_type, streak_num, status_code)
    SELECT v_season, 1, team_id, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL
    FROM tmp_teams;

  # Sort league
  INSERT INTO SPORTS_NFL_STANDINGS (season, week, team_id, pos_league, pos_draft)
    SELECT v_season, 1, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id),
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_league = VALUES(pos_league),
                          pos_draft = VALUES(pos_draft);

  # Sort conference
  INSERT INTO SPORTS_NFL_STANDINGS (season, week, team_id, pos_conf)
    SELECT v_season, 1, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.conf_id = ME.conf_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_conf = VALUES(pos_conf);

  # Sort division
  INSERT INTO SPORTS_NFL_STANDINGS (season, week, team_id, pos_div)
    SELECT v_season, 1, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.div_id = ME.div_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_div = VALUES(pos_div);

  ALTER TABLE SPORTS_NFL_STANDINGS ORDER BY season, week, team_id;

END $$

DELIMITER ;

