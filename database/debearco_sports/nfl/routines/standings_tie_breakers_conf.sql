##
## Break ties within the conference
##
# http://www.nfl.com/standings/tiebreakingprocedures
DROP PROCEDURE IF EXISTS `nfl_standings_break_conf`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_conf`(
  v_conf_id TINYINT UNSIGNED,
  v_conf_pos TINYINT UNSIGNED
)
    COMMENT 'Break an individual NFL conference tie'
proc_conf: BEGIN

  # Declare vars
  DECLARE v_num_teams TINYINT UNSIGNED;
  DECLARE v_num_div TINYINT UNSIGNED;
  DECLARE v_test_success TINYINT UNSIGNED;

  # Which teams are involved?
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_teams;
  CREATE TEMPORARY TABLE tmp_tiebrk_teams (
    team_id VARCHAR(3),
    base_pos TINYINT UNSIGNED,
    new_pos TINYINT UNSIGNED,
    new_pos_tied TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, group_pos AS base_pos, 0 AS new_pos, 1 AS new_pos_tied
    FROM tmp_tiebrk_list
    WHERE group_id = v_conf_id
    AND   group_pos = v_conf_pos;

  # Conference ties depend on how many divisions are involved:
  #  - All in one division, just uses division rank
  #  - Across multiple, in which case only one team progresses from each division (the highest div ranked)
  SELECT COUNT(tmp_teams.team_id), COUNT(DISTINCT tmp_teams.div_id)
         INTO v_num_teams, v_num_div
  FROM tmp_tiebrk_teams
  JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_tiebrk_teams.team_id);

  IF v_num_div = 1 THEN
    # Only one div, use the div rank as the only test
    CALL nfl_standings_tie_div_pos();
    LEAVE proc_conf;

  ELSEIF v_num_div <> v_num_teams THEN
    # At least one division represented multiple times, so 'drop' the lowest ranked team(s)
    ALTER TABLE tmp_tiebrk_teams ADD COLUMN div_id TINYINT UNSIGNED,
                                 ADD COLUMN div_pos TINYINT UNSIGNED;

    UPDATE tmp_tiebrk_teams
    JOIN tmp_pos
      ON (tmp_pos.team_id = tmp_tiebrk_teams.team_id)
    SET tmp_tiebrk_teams.div_id = tmp_pos.div_id,
        tmp_tiebrk_teams.div_pos = tmp_pos.div_pos;

    DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_teams_div;
    CREATE TEMPORARY TABLE tmp_tiebrk_teams_div (
      div_id TINYINT UNSIGNED,
      best_pos TINYINT UNSIGNED,
      PRIMARY KEY (div_id)
    ) ENGINE = MEMORY
      SELECT div_id, MIN(div_pos) AS best_pos
      FROM tmp_tiebrk_teams
      GROUP BY div_id;

    UPDATE tmp_tiebrk_teams
    LEFT JOIN tmp_tiebrk_teams_div
      ON (tmp_tiebrk_teams_div.div_id = tmp_tiebrk_teams.div_id
      AND tmp_tiebrk_teams_div.best_pos = tmp_tiebrk_teams.div_pos)
    SET new_pos = v_num_div
    WHERE tmp_tiebrk_teams_div.div_id IS NULL;

    ALTER TABLE tmp_tiebrk_teams DROP COLUMN div_id, DROP COLUMN div_pos;

  END IF;

  # Reset our num_teams counter
  SELECT COUNT(*) INTO v_num_teams FROM tmp_tiebrk_teams WHERE new_pos = 0;

  ## Now run our tests...

  #
  # 2 Clubs: Head-to-head, if applicable.
  # 3 Clubs: Head-to-head sweep.
  #
  IF v_num_teams = 2 THEN
    CALL nfl_standings_tie_h2h();
  ELSE
    CALL nfl_standings_tie_h2h_sweep();
  END IF;
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Best won-lost-tied percentage in games played within the conference.
  #
  CALL nfl_standings_tie_pct_conf();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Best won-lost-tied percentage in common games, minimum of four.
  #
  CALL nfl_standings_tie_pct_common(4);
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Strength of victory.
  #
  CALL nfl_standings_tie_str_vict();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Strength of schedule.
  #
  CALL nfl_standings_tie_str_sched('>');
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Best combined ranking among conference teams in points scored and points allowed.
  #
  CALL nfl_standings_tie_pts_rank_conf();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Best combined ranking among all teams in points scored and points allowed.
  #
  CALL nfl_standings_tie_pts_rank_all();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Best net points in conference games.
  #
  CALL nfl_standings_tie_pts_net_common();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Best net points in all games.
  #
  CALL nfl_standings_tie_pts_net_all();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Best net touchdowns in all games.
  #
  CALL nfl_standings_tie_td_net();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

  #
  # Coin toss (Which we will interpret as alphabetical order on franchise...)
  #
  CALL nfl_standings_tie_coin_toss();
  CALL nfl_standings_break_group_test('conf', v_conf_id, v_conf_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_conf;
  END IF;

END $$

DELIMITER ;

