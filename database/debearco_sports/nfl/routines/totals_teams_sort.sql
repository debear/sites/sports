##
## Team totals sorted
##
DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for NFL teams'
BEGIN

  # Broken down by individual routines
  CALL nfl_totals_teams_sort_downs(v_season, v_season_type);
  CALL nfl_totals_teams_sort_plays(v_season, v_season_type);
  CALL nfl_totals_teams_sort_yards(v_season, v_season_type);
  CALL nfl_totals_teams_sort_kickoffs(v_season, v_season_type);
  CALL nfl_totals_teams_sort_punts(v_season, v_season_type);
  CALL nfl_totals_teams_sort_returns(v_season, v_season_type);
  CALL nfl_totals_teams_sort_tds(v_season, v_season_type);
  CALL nfl_totals_teams_sort_2pt(v_season, v_season_type);
  CALL nfl_totals_teams_sort_kicks(v_season, v_season_type);
  CALL nfl_totals_teams_sort_turnovers(v_season, v_season_type);
  CALL nfl_totals_teams_sort_misc(v_season, v_season_type);
  CALL nfl_totals_teams_sort_defense(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_downs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_downs`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort downs season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_DOWNS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'DOWNS',
                                  'num_1st SMALLINT UNSIGNED,
                                   num_1st_rush SMALLINT UNSIGNED,
                                   num_1st_pass SMALLINT UNSIGNED,
                                   num_1st_pen SMALLINT UNSIGNED,
                                   num_3rd SMALLINT UNSIGNED,
                                   num_3rd_cnv SMALLINT UNSIGNED,
                                   pct_3rd DECIMAL(6,5),
                                   num_4th SMALLINT UNSIGNED,
                                   num_4th_cnv SMALLINT UNSIGNED,
                                   pct_4th DECIMAL(6,5)',
                                  'num_1st_rush + num_1st_pass + num_1st_pen AS num_1st,
                                   num_1st_rush,
                                   num_1st_pass,
                                   num_1st_pen,
                                   num_3rd,
                                   num_3rd_cnv,
                                   IF(num_3rd > 0, num_3rd_cnv / num_3rd, 0) AS pct_3rd,
                                   num_4th,
                                   num_4th_cnv,
                                   IF(num_4th > 0, num_4th_cnv / num_4th, 0) AS pct_4th');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_1st', 'B.num_1st > A.num_1st');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_1st_rush', 'B.num_1st_rush > A.num_1st_rush');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_1st_pass', 'B.num_1st_pass > A.num_1st_pass');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_1st_pen', 'B.num_1st_pen > A.num_1st_pen');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_3rd', 'B.num_3rd > A.num_3rd');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_3rd_cnv', 'B.num_3rd_cnv > A.num_3rd_cnv');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'pct_3rd', 'B.pct_3rd > A.pct_3rd');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_4th', 'B.num_4th > A.num_4th');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'num_4th_cnv', 'B.num_4th_cnv > A.num_4th_cnv');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DOWNS', 'pct_4th', 'B.pct_4th > A.pct_4th');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_DOWNS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_plays`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_plays`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort plays season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_PLAYS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'PLAYS',
                                  'total SMALLINT UNSIGNED,
                                   num_pass_att SMALLINT UNSIGNED,
                                   num_pass_cmp SMALLINT UNSIGNED,
                                   pct_pass DECIMAL(6,5),
                                   num_pass_sacked TINYINT UNSIGNED,
                                   num_rush SMALLINT UNSIGNED,
                                   num_rush_tfl SMALLINT UNSIGNED',
                                  'total,
                                   num_pass_att,
                                   num_pass_cmp,
                                   IF(num_pass_att > 0, num_pass_cmp / num_pass_att, 0) AS pct_pass,
                                   num_pass_sacked,
                                   num_rush,
                                   num_rush_tfl');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PLAYS', 'total', 'B.total > A.total');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PLAYS', 'num_pass_att', 'B.num_pass_att > A.num_pass_att');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PLAYS', 'num_pass_cmp', 'B.num_pass_cmp > A.num_pass_cmp');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PLAYS', 'pct_pass', 'B.pct_pass > A.pct_pass');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PLAYS', 'num_pass_sacked', 'B.num_pass_sacked < A.num_pass_sacked');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PLAYS', 'num_rush', 'B.num_rush > A.num_rush');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PLAYS', 'num_rush_tfl', 'B.num_rush_tfl < A.num_rush_tfl');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_PLAYS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_yards`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_yards`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort yardage season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_YARDS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'YARDS',
                                  'yards_total MEDIUMINT SIGNED,
                                   yards_pass MEDIUMINT SIGNED,
                                   yards_rush MEDIUMINT SIGNED,
                                   yards_rush_tfl MEDIUMINT SIGNED',
                                  'yards_pass + yards_rush AS yards_total,
                                   yards_pass,
                                   yards_rush,
                                   yards_rush_tfl');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'YARDS', 'yards_total', 'B.yards_total > A.yards_total');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'YARDS', 'yards_pass', 'B.yards_pass > A.yards_pass');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'YARDS', 'yards_rush', 'B.yards_rush > A.yards_rush');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'YARDS', 'yards_rush_tfl', 'B.yards_rush_tfl < A.yards_rush_tfl');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_YARDS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_kickoffs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_kickoffs`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort kickoff season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_KICKOFFS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'KICKOFFS',
                                  'num SMALLINT UNSIGNED,
                                   end_zone SMALLINT UNSIGNED,
                                   touchback SMALLINT UNSIGNED',
                                  'num,
                                   end_zone,
                                   touchback');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKOFFS', 'num', 'B.num > A.num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKOFFS', 'end_zone', 'B.end_zone > A.end_zone');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKOFFS', 'touchback', 'B.touchback > A.touchback');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_KICKOFFS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_punts`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_punts`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort punting season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_PUNTS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'PUNTS',
                                  'num SMALLINT UNSIGNED,
                                   yards SMALLINT SIGNED,
                                   avg DECIMAL(4,2),
                                   net SMALLINT SIGNED,
                                   net_avg DECIMAL(4,2),
                                   blocked SMALLINT UNSIGNED',
                                  'num,
                                   yards,
                                   IF(num > 0, yards / num, 0) AS avg,
                                   net,
                                   IF(num > 0, net / num, 0) AS net_avg,
                                   blocked');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PUNTS', 'num', 'B.num > A.num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PUNTS', 'yards', 'B.yards > A.yards');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PUNTS', 'avg', 'B.avg > A.avg');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PUNTS', 'net', 'B.net > A.net');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PUNTS', 'net_avg', 'B.net_avg > A.net_avg');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'PUNTS', 'blocked', 'B.blocked < A.blocked');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_PUNTS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_returns`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_returns`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort returns season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_RETURNS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'RETURNS',
                                  'ko_num SMALLINT UNSIGNED,
                                   ko_yards SMALLINT SIGNED,
                                   ko_avg DECIMAL(4,2),
                                   punt_num SMALLINT UNSIGNED,
                                   punt_yards SMALLINT SIGNED,
                                   punt_avg DECIMAL(4,2),
                                   int_num SMALLINT UNSIGNED,
                                   int_yards SMALLINT SIGNED,
                                   int_avg DECIMAL(4,2)',
                                  'ko_num,
                                   ko_yards,
                                   IF(ko_num > 0, ko_yards / ko_num, 0) AS ko_avg,
                                   punt_num,
                                   punt_yards,
                                   IF(punt_num > 0, punt_yards / punt_num, 0) AS punt_avg,
                                   int_num,
                                   int_yards,
                                   IF(int_num > 0, int_yards / int_num, 0) AS int_avg');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'ko_num', 'B.ko_num > A.ko_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'ko_yards', 'B.ko_yards > A.ko_yards');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'ko_avg', 'B.ko_avg > A.ko_avg');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'punt_num', 'B.punt_num > A.punt_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'punt_yards', 'B.punt_yards > A.punt_yards');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'punt_avg', 'B.punt_avg > A.punt_avg');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'int_num', 'B.int_num > A.int_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'int_yards', 'B.int_yards > A.int_yards');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'RETURNS', 'int_avg', 'B.int_avg > A.int_avg');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_RETURNS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_tds`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_tds`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort TDs season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_TDS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'TDS',
                                  'num_total SMALLINT UNSIGNED,
                                   num_pass SMALLINT UNSIGNED,
                                   num_rush SMALLINT UNSIGNED,
                                   num_int SMALLINT UNSIGNED,
                                   num_fumbles SMALLINT UNSIGNED,
                                   num_fumbles_own SMALLINT UNSIGNED,
                                   num_fumbles_opp SMALLINT UNSIGNED,
                                   num_ko SMALLINT UNSIGNED,
                                   num_punt SMALLINT UNSIGNED,
                                   num_other SMALLINT UNSIGNED,
                                   safties SMALLINT UNSIGNED',
                                  'num_pass + num_rush + num_int + num_fumbles + num_ko + num_punt + num_other AS num_total,
                                   num_pass,
                                   num_rush,
                                   num_int,
                                   num_fumbles,
                                   num_fumbles_own,
                                   num_fumbles_opp,
                                   num_ko,
                                   num_punt,
                                   num_other,
                                   safties');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_total', 'B.num_total > A.num_total');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_pass', 'B.num_pass > A.num_pass');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_rush', 'B.num_rush > A.num_rush');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_int', 'B.num_int > A.num_int');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_fumbles', 'B.num_fumbles > A.num_fumbles');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_fumbles_own', 'B.num_fumbles_own > A.num_fumbles_own');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_fumbles_opp', 'B.num_fumbles_opp > A.num_fumbles_opp');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_ko', 'B.num_ko > A.num_ko');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_punt', 'B.num_punt > A.num_punt');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'num_other', 'B.num_other > A.num_other');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TDS', 'safties', 'B.safties > A.safties');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_TDS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_2pt`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_2pt`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort 2pt cnv season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_2PT_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, '2PT',
                                  'pass_att SMALLINT UNSIGNED,
                                   pass_made SMALLINT UNSIGNED,
                                   pass_pct DECIMAL(6,5),
                                   rush_att SMALLINT UNSIGNED,
                                   rush_made SMALLINT UNSIGNED,
                                   rush_pct DECIMAL(6,5),
                                   total_att SMALLINT UNSIGNED,
                                   total_made SMALLINT UNSIGNED,
                                   total_pct DECIMAL(6,5)',
                                  'pass_att,
                                   pass_made,
                                   IF(pass_att > 0, pass_made / pass_att, 0) AS pass_pct,
                                   rush_att,
                                   rush_made,
                                   IF(rush_att > 0, rush_made / rush_att, 0) AS rush_pct,
                                   pass_att + rush_att AS total_att,
                                   pass_made + rush_made AS total_made,
                                   IF((pass_att + rush_att) > 0, (pass_made + rush_made) / (pass_att + rush_att), 0) AS total_pct');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'pass_att', 'B.pass_att > A.pass_att');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'pass_made', 'B.pass_made > A.pass_made');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'pass_pct', 'B.pass_pct > A.pass_pct');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'rush_att', 'B.rush_att > A.rush_att');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'rush_made', 'B.rush_made > A.rush_made');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'rush_pct', 'B.rush_pct > A.pass_pct');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'total_att', 'B.total_att > A.total_att');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'total_made', 'B.total_made > A.total_made');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, '2PT', 'total_pct', 'B.total_pct > A.total_pct');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_2PT_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_kicks`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_kicks`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort kicking season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_KICKS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'KICKS',
                                  'fg_att SMALLINT UNSIGNED,
                                   fg_made SMALLINT UNSIGNED,
                                   fg_pct DECIMAL(6,5),
                                   fg_blocked SMALLINT UNSIGNED,
                                   pat_att SMALLINT UNSIGNED,
                                   pat_made SMALLINT UNSIGNED,
                                   pat_pct DECIMAL(6,5),
                                   pat_blocked SMALLINT UNSIGNED',
                                  'fg_att,
                                   fg_made,
                                   IF(fg_att > 0, fg_made / fg_att, 0) AS fg_pct,
                                   fg_blocked,
                                   pat_att,
                                   pat_made,
                                   IF(pat_att > 0, pat_made / pat_att, 0) AS pat_pct,
                                   pat_blocked');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'fg_att', 'B.fg_att > A.fg_att');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'fg_made', 'B.fg_made > A.fg_made');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'fg_pct', 'B.fg_pct > A.fg_pct');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'fg_blocked', 'B.fg_blocked < A.fg_blocked');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'pat_att', 'B.pat_att > A.pat_att');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'pat_made', 'B.pat_made > A.pat_made');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'pat_pct', 'B.pat_pct > A.pat_pct');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'KICKS', 'pat_blocked', 'B.pat_blocked < A.pat_blocked');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_KICKS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_turnovers`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_turnovers`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort turnover season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_TURNOVERS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'TURNOVERS',
                                  'total SMALLINT UNSIGNED,
                                   int_num SMALLINT UNSIGNED,
                                   fumb_num SMALLINT UNSIGNED,
                                   fumb_lost SMALLINT UNSIGNED',
                                  'int_num + fumb_lost AS total,
                                   int_num,
                                   fumb_num,
                                   fumb_lost');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TURNOVERS', 'total', 'B.total < A.total');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TURNOVERS', 'int_num', 'B.int_num < A.int_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TURNOVERS', 'fumb_num', 'B.fumb_num < A.fumb_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'TURNOVERS', 'fumb_lost', 'B.fumb_lost < A.fumb_lost');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_TURNOVERS_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_misc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_misc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort misc season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_MISC_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'MISC',
                                  'time_of_poss TIME,
                                   pens_num SMALLINT UNSIGNED,
                                   pens_yards MEDIUMINT UNSIGNED,
                                   rz_num SMALLINT UNSIGNED,
                                   rz_cnv SMALLINT UNSIGNED,
                                   rz_pct DECIMAL(6,5),
                                   goal_num SMALLINT UNSIGNED,
                                   goal_cnv SMALLINT UNSIGNED,
                                   goal_pct DECIMAL(6,5)',
                                  'time_of_poss,
                                   pens_num,
                                   pens_yards,
                                   rz_num,
                                   rz_cnv,
                                   IF(rz_num > 0, rz_cnv / rz_num, 0) AS pct_3rd,
                                   goal_num,
                                   goal_cnv,
                                   IF(goal_num > 0, goal_cnv / goal_num, 0) AS pct_4th');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'time_of_poss', 'B.time_of_poss > A.time_of_poss');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'pens_num', 'B.pens_num < A.pens_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'pens_yards', 'B.pens_yards < A.pens_yards');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'rz_num', 'B.rz_num > A.rz_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'rz_cnv', 'B.rz_cnv > A.rz_cnv');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'rz_pct', 'IF(B.rz_num > 0, B.rz_cnv / B.rz_num, 0) > IF(A.rz_num > 0, A.rz_cnv / A.rz_num, 0)');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'goal_num', 'B.goal_num > A.goal_num');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'goal_cnv', 'B.goal_cnv > A.goal_cnv');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'MISC', 'goal_pct', 'IF(B.goal_num > 0, B.goal_cnv / B.goal_num, 0) > IF(A.goal_num > 0, A.goal_cnv / A.goal_num, 0)');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_MISC_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort_defense`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort_defense`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort defensive season totals for NFL teams'
BEGIN

  DELETE FROM SPORTS_NFL_TEAMS_STATS_DEFENSE_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_teams_sort__tmp(v_season, v_season_type, 'DEFENSE',
                                  'total SMALLINT UNSIGNED,
                                   tckl SMALLINT UNSIGNED,
                                   asst SMALLINT UNSIGNED,
                                   sacks TINYINT UNSIGNED,
                                   sacks_yards SMALLINT SIGNED,
                                   pd TINYINT UNSIGNED,
                                   fumb_forced TINYINT UNSIGNED,
                                   fumb_rec TINYINT UNSIGNED',
                                  'tckl + asst AS total,
                                   tckl,
                                   asst,
                                   sacks,
                                   sacks_yards,
                                   pd,
                                   fumb_forced,
                                   fumb_rec');

  # Calcs
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'total', 'B.total > A.total');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'tckl', 'B.tckl > A.tckl');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'asst', 'B.asst > A.asst');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'sacks', 'B.sacks < A.sacks');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'sacks_yards', 'B.sacks_yards < A.sacks_yards');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'pd', 'B.pd > A.pd');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'fumb_forced', 'B.fumb_forced > A.fumb_forced');
  CALL nfl_totals_teams_sort__calc(v_season, v_season_type, 'DEFENSE', 'fumb_rec', 'B.fumb_rec > A.fumb_rec');

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_DEFENSE_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_cols VARCHAR(2048),
  v_calcs VARCHAR(2048)
)
    COMMENT 'Create temporary table for NHL team season stat sort'
BEGIN

  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_TEAMS_STATS_', v_table));
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_NFL_TEAMS_STATS_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      team_id VARCHAR(3) NOT NULL,
      ', v_cols, ',
      PRIMARY KEY (season, season_type, team_id)
    ) ENGINE = MEMORY
      SELECT season, season_type, team_id, ', v_calcs, '
      FROM SPORTS_NFL_TEAMS_STATS_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '";'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_NFL_TEAMS_STATS_', v_table), CONCAT('tmp_SPORTS_NFL_TEAMS_STATS_', v_table, '_cp'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_teams_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col VARCHAR(15),
  v_calc VARCHAR(255)
)
    COMMENT 'Sort season totals for NFL teams'
BEGIN

  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_NFL_TEAMS_STATS_', v_table, '_SORTED (season, season_type, team_id, `', v_col, '`)
      SELECT A.season, A.season_type, A.team_id,
             COUNT(B.team_id) + 1 AS `', v_col, '`
      FROM tmp_SPORTS_NFL_TEAMS_STATS_', v_table, ' AS A
      LEFT JOIN tmp_SPORTS_NFL_TEAMS_STATS_', v_table, '_cp AS B
        ON (B.season = A.season
        AND B.season_type = A.season_type
        AND ', v_calc, ')
      WHERE A.season = ', v_season, '
      AND   A.season_type = "', v_season_type, '"
      GROUP BY A.season, A.season_type, A.team_id
    ON DUPLICATE KEY UPDATE `', v_col, '` = VALUES(`', v_col, '`);'));

END $$

DELIMITER ;

