#
# Records: Overall, Home, Away, Conference, Division, Recent
#
DROP PROCEDURE IF EXISTS `nfl_standings_records`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_records`(
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate basic NFL win/loss records'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_records;
  CREATE TEMPORARY TABLE tmp_records (
    team_id VARCHAR(3),
    wins TINYINT UNSIGNED,
    loss TINYINT UNSIGNED,
    ties TINYINT UNSIGNED,
    pts_for SMALLINT UNSIGNED,
    pts_against SMALLINT UNSIGNED,
    home_wins TINYINT UNSIGNED,
    home_loss TINYINT UNSIGNED,
    home_ties TINYINT UNSIGNED,
    visitor_wins TINYINT UNSIGNED,
    visitor_loss TINYINT UNSIGNED,
    visitor_ties TINYINT UNSIGNED,
    conf_wins TINYINT UNSIGNED,
    conf_loss TINYINT UNSIGNED,
    conf_ties TINYINT UNSIGNED,
    div_wins TINYINT UNSIGNED,
    div_loss TINYINT UNSIGNED,
    div_ties TINYINT UNSIGNED,
    recent_wins TINYINT UNSIGNED,
    recent_loss TINYINT UNSIGNED,
    recent_ties TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id,
           SUM(result = 'w') AS wins,
           SUM(result = 'l') AS loss,
           SUM(result = 't') AS ties,
           SUM(pts_for) AS pts_for,
           SUM(pts_against) AS pts_against,
           SUM(is_home = 1 AND result = 'w') AS home_wins,
           SUM(is_home = 1 AND result = 'l') AS home_loss,
           SUM(is_home = 1 AND result = 't') AS home_ties,
           SUM(is_home = 0 AND result = 'w') AS visitor_wins,
           SUM(is_home = 0 AND result = 'l') AS visitor_loss,
           SUM(is_home = 0 AND result = 't') AS visitor_ties,
           SUM(opp_is_conf = 1 AND result = 'w') AS conf_wins,
           SUM(opp_is_conf = 1 AND result = 'l') AS conf_loss,
           SUM(opp_is_conf = 1 AND result = 't') AS conf_ties,
           SUM(opp_is_div = 1 AND result = 'w') AS div_wins,
           SUM(opp_is_div = 1 AND result = 'l') AS div_loss,
           SUM(opp_is_div = 1 AND result = 't') AS div_ties,
           SUM(num_later < v_recent_games AND result = 'w') AS recent_wins,
           SUM(num_later < v_recent_games AND result = 'l') AS recent_loss,
           SUM(num_later < v_recent_games AND result = 't') AS recent_ties
    FROM tmp_sched
    GROUP BY team_id;

END $$

DELIMITER ;

