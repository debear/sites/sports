##
## Player totals sorted
##
DROP PROCEDURE IF EXISTS `nfl_totals_players_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for NFL players'
BEGIN

  # Prep
  CALL nfl_totals_players_sort__tmp_qual_prep(v_season, v_season_type);

  # Broken down by individual routines
  CALL nfl_totals_players_sort_passing(v_season, v_season_type);
  CALL nfl_totals_players_sort_rushing(v_season, v_season_type);
  CALL nfl_totals_players_sort_receiving(v_season, v_season_type);
  CALL nfl_totals_players_sort_fumbles(v_season, v_season_type);
  CALL nfl_totals_players_sort_tackles(v_season, v_season_type);
  CALL nfl_totals_players_sort_passdef(v_season, v_season_type);
  CALL nfl_totals_players_sort_kicking(v_season, v_season_type);
  CALL nfl_totals_players_sort_punts(v_season, v_season_type);
  CALL nfl_totals_players_sort_kickret(v_season, v_season_type);
  CALL nfl_totals_players_sort_puntret(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_passing`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_passing`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort passing season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_PASSING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'PASSING',
                                    'atts SMALLINT UNSIGNED,
                                     cmp SMALLINT UNSIGNED,
                                     pct DECIMAL(6,5),
                                     yards SMALLINT SIGNED,
                                     avg DECIMAL(7,5),
                                     `long` TINYINT SIGNED,
                                     td TINYINT UNSIGNED,
                                     `int` TINYINT UNSIGNED,
                                     sacked TINYINT UNSIGNED,
                                     sack_yards SMALLINT UNSIGNED,
                                     rating DECIMAL(4,1),
                                     qual TINYINT UNSIGNED',
                                    'SUM(atts) AS atts,
                                     SUM(cmp) AS cmp,
                                     IF(SUM(atts) > 0, SUM(cmp) / SUM(atts), 0) AS pct,
                                     SUM(yards) AS yards,
                                     IF(SUM(atts) > 0, SUM(yards) / SUM(atts), 0) AS avg,
                                     MAX(`long`) AS `long`,
                                     SUM(td) AS td,
                                     SUM(`int`) AS `int`,
                                     SUM(sacked) AS sacked,
                                     SUM(sack_yards) AS sack_yards,
                                     nfl_passer_rating(SUM(atts), SUM(cmp), SUM(yards), SUM(td), SUM(`int`)) AS rating,
                                     0 AS qual',
                                    'SUM(atts) > 0');

  # Qualification Criteria
  CALL nfl_totals_players_sort__tmp_qual_passing(v_season, v_season_type);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'atts', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'cmp', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'pct', 'DECIMAL(6,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'avg', 'DECIMAL(7,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'avg_all', 'DECIMAL(7,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'long', 'TINYINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'td', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'int', 'TINYINT UNSIGNED', '<', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'int_all', 'TINYINT UNSIGNED', '<', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'sacked', 'TINYINT UNSIGNED', '<', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'sacked_all', 'TINYINT UNSIGNED', '<', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'sack_yards', 'SMALLINT UNSIGNED', '<', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'sack_yards_all', 'SMALLINT UNSIGNED', '<', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'rating', 'DECIMAL(4,1)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSING', 'rating_all', 'DECIMAL(4,1)', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PASSING_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_rushing`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_rushing`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort rushing season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_RUSHING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'RUSHING',
                                    'atts SMALLINT UNSIGNED,
                                     yards SMALLINT SIGNED,
                                     avg DECIMAL(7,5),
                                     `long` TINYINT SIGNED,
                                     td TINYINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'SUM(atts) AS atts,
                                     SUM(yards) AS yards,
                                     IF(SUM(atts) > 0, SUM(yards) / SUM(atts), 0) AS avg,
                                     MAX(`long`) AS `long`,
                                     SUM(td) AS td,
                                     0 AS qual',
                                    'SUM(atts) > 0');

  # Qualification Criteria
  CALL nfl_totals_players_sort__tmp_qual_rushing(v_season, v_season_type);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RUSHING', 'atts', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RUSHING', 'yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RUSHING', 'avg', 'DECIMAL(7,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RUSHING', 'avg_all', 'DECIMAL(7,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RUSHING', 'long', 'TINYINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RUSHING', 'td', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_RUSHING_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_receiving`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_receiving`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort receiving season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_RECEIVING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'RECEIVING',
                                    'recept SMALLINT UNSIGNED,
                                     yards SMALLINT SIGNED,
                                     avg DECIMAL(7,5),
                                     `long` TINYINT SIGNED,
                                     td TINYINT UNSIGNED,
                                     targets SMALLINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'SUM(recept) AS recept,
                                     SUM(yards) AS yards,
                                     IF(SUM(recept) > 0, SUM(yards) / SUM(recept), 0) AS avg,
                                     MAX(`long`) AS `long`,
                                     SUM(td) AS td,
                                     SUM(targets) AS targets,
                                     0 AS qual',
                                    'SUM(targets) > 0');

  # Qualification Criteria
  CALL nfl_totals_players_sort__tmp_qual_receiving(v_season, v_season_type);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RECEIVING', 'recept', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RECEIVING', 'yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RECEIVING', 'avg', 'DECIMAL(7,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RECEIVING', 'avg_all', 'DECIMAL(7,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RECEIVING', 'long', 'TINYINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RECEIVING', 'td', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'RECEIVING', 'targets', 'SMALLINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_RECEIVING_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_fumbles`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_fumbles`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort fumble season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_FUMBLES_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'FUMBLES',
                                    'num_fumbled TINYINT UNSIGNED,
                                     num_lost TINYINT UNSIGNED,
                                     num_forced TINYINT UNSIGNED,
                                     rec_own TINYINT UNSIGNED,
                                     rec_opp TINYINT UNSIGNED,
                                     num_rec TINYINT UNSIGNED,
                                     rec_own_td TINYINT UNSIGNED,
                                     rec_opp_td TINYINT UNSIGNED,
                                     num_rec_td TINYINT UNSIGNED,
                                     oob TINYINT UNSIGNED',
                                    'SUM(num_fumbled) AS num_fumbled,
                                     SUM(num_lost) AS num_lost,
                                     SUM(num_forced) AS num_forced,
                                     SUM(rec_own) AS rec_own,
                                     SUM(rec_opp) AS rec_opp,
                                     SUM(rec_own) + SUM(rec_opp) AS num_rec,
                                     SUM(rec_own_td) AS rec_own_td,
                                     SUM(rec_opp_td) AS rec_opp_td,
                                     SUM(rec_own_td) + SUM(rec_opp_td) AS num_rec_td,
                                     SUM(oob) AS oob',
                                    NULL);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'num_fumbled', 'TINYINT UNSIGNED', '<', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'num_lost', 'TINYINT UNSIGNED', '<', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'num_forced', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'rec_own', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'rec_opp', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'num_rec', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'rec_own_td', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'rec_opp_td', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'num_rec_td', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'FUMBLES', 'oob', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_FUMBLES_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_tackles`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_tackles`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort tackle season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_TACKLES_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'TACKLES',
                                    'total SMALLINT UNSIGNED,
                                     tckl TINYINT UNSIGNED,
                                     asst TINYINT UNSIGNED,
                                     sacks DECIMAL(3,1) UNSIGNED,
                                     sacks_yards SMALLINT SIGNED,
                                     tfl TINYINT UNSIGNED,
                                     qb_hit TINYINT UNSIGNED,
                                     kick_block TINYINT UNSIGNED',
                                    'SUM(tckl + asst) AS total,
                                     SUM(tckl) AS tckl,
                                     SUM(asst) AS asst,
                                     SUM(sacks) AS sacks,
                                     SUM(sacks_yards) AS sacks_yards,
                                     SUM(tfl) AS tfl,
                                     SUM(qb_hit) AS qb_hit,
                                     SUM(kick_block) AS kick_block',
                                    '(SUM(tckl) + SUM(asst) + SUM(qb_hit) + SUM(kick_block)) > 0');

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'total', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'tckl', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'asst', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'sacks', 'DECIMAL(3,1)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'sacks_yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'tfl', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'qb_hit', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'TACKLES', 'kick_block', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_TACKLES_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_passdef`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_passdef`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort pass def season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_PASSDEF_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'PASSDEF',
                                    'pd TINYINT UNSIGNED,
                                     `int` TINYINT UNSIGNED,
                                     int_yards SMALLINT SIGNED,
                                     int_td TINYINT UNSIGNED',
                                    'SUM(pd) AS pd,
                                     SUM(`int`) AS `int`,
                                     SUM(int_yards) AS int_yards,
                                     SUM(int_td) AS int_td',
                                    '(SUM(pd) + SUM(`int`)) > 0');

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSDEF', 'pd', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSDEF', 'int', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSDEF', 'int_yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PASSDEF', 'int_td', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PASSDEF_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_kicking`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_kicking`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort kicking season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_KICKING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'KICKING',
                                    'fg_att TINYINT UNSIGNED,
                                     fg_made TINYINT UNSIGNED,
                                     fg_pct DECIMAL(6,5),
                                     fg_u20 TINYINT UNSIGNED,
                                     fg_u30 TINYINT UNSIGNED,
                                     fg_u40 TINYINT UNSIGNED,
                                     fg_u50 TINYINT UNSIGNED,
                                     fg_o50 TINYINT UNSIGNED,
                                     xp_att TINYINT UNSIGNED,
                                     xp_made TINYINT UNSIGNED,
                                     xp_pct DECIMAL(6,5),
                                     pts TINYINT UNSIGNED,
                                     qual_fg TINYINT UNSIGNED,
                                     qual_xp TINYINT UNSIGNED',
                                    'SUM(fg_att) AS fg_att,
                                     SUM(fg_made) AS fg_made,
                                     IF(SUM(fg_att) > 0, SUM(fg_made) / SUM(fg_att), 0) AS fg_pct,
                                     SUM(fg_u20) AS fg_u20,
                                     SUM(fg_u30) AS fg_u30,
                                     SUM(fg_u40) AS fg_u40,
                                     SUM(fg_u50) AS fg_u50,
                                     SUM(fg_o50) AS fg_o50,
                                     SUM(xp_att) AS xp_att,
                                     SUM(xp_made) AS xp_made,
                                     (3 * SUM(fg_made)) + SUM(xp_made) AS pts,
                                     IF(SUM(xp_att) > 0, SUM(xp_made) / SUM(xp_att), 0) AS xp_pct,
                                     0 AS qual_fg,
                                     0 AS qual_xp',
                                    '(SUM(fg_att) + SUM(xp_att)) > 0');

  # Qualification Criteria
  CALL nfl_totals_players_sort__tmp_qual_kicking(v_season, v_season_type);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_att', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_made', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_pct', 'DECIMAL(6,5)', '>', 'qual_fg');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_u20', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_u30', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_u40', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_u50', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'fg_o50', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'xp_att', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'xp_made', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'xp_pct', 'DECIMAL(6,5)', '>', 'qual_xp');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'xp_pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKING', 'pts', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_KICKING_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_punts`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_punts`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort punting season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_PUNTS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'PUNTS',
                                    'num TINYINT UNSIGNED,
                                     yards SMALLINT SIGNED,
                                     avg DECIMAL(7,5),
                                     net SMALLINT SIGNED,
                                     net_avg DECIMAL(7,5),
                                     `long` TINYINT SIGNED,
                                     tb TINYINT UNSIGNED,
                                     inside20 TINYINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'SUM(num) AS num,
                                     SUM(yards) AS yards,
                                     IF(SUM(num) > 0, SUM(yards) / SUM(num), 0) AS avg,
                                     SUM(net) AS net,
                                     IF(SUM(num) > 0, SUM(net) / SUM(num), 0) AS net_avg,
                                     MAX(`long`) AS `long`,
                                     SUM(tb) AS tb,
                                     SUM(inside20) AS inside20,
                                     0 AS qual',
                                    'SUM(num) > 0');

  # Qualification Criteria
  CALL nfl_totals_players_sort__tmp_qual_punts(v_season, v_season_type);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'num', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'avg', 'DECIMAL(7,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'avg_all', 'DECIMAL(7,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'net', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'net_avg', 'DECIMAL(7,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'net_avg_all', 'DECIMAL(7,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'long', 'TINYINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'tb', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTS', 'inside20', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PUNTS_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_kickret`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_kickret`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort kick return season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_KICKRET_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'KICKRET',
                                    'num TINYINT UNSIGNED,
                                     yards SMALLINT SIGNED,
                                     avg DECIMAL(7,5),
                                     `long` TINYINT SIGNED,
                                     fair_catch TINYINT UNSIGNED,
                                     td TINYINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'SUM(num) AS num,
                                     SUM(yards) AS yards,
                                     IF(SUM(num) > 0, SUM(yards) / SUM(num), 0) AS avg,
                                     MAX(`long`) AS `long`,
                                     SUM(fair_catch) AS fair_catch,
                                     SUM(td) AS td,
                                     0 AS qual',
                                    '(SUM(num) + SUM(fair_catch)) > 0');

  # Qualification Criteria
  CALL nfl_totals_players_sort__tmp_qual_kickret(v_season, v_season_type);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKRET', 'num', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKRET', 'yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKRET', 'avg', 'DECIMAL(7,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKRET', 'avg_all', 'DECIMAL(7,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKRET', 'long', 'TINYINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKRET', 'fair_catch', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'KICKRET', 'td', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_KICKRET_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort_puntret`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort_puntret`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort punt return season totals for NFL players'
BEGIN

  DELETE FROM SPORTS_NFL_PLAYERS_SEASON_PUNTRET_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nfl_totals_players_sort__tmp(v_season, v_season_type, 'PUNTRET',
                                    'num TINYINT UNSIGNED,
                                     yards SMALLINT SIGNED,
                                     avg DECIMAL(7,5),
                                     `long` TINYINT SIGNED,
                                     fair_catch TINYINT UNSIGNED,
                                     td TINYINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'SUM(num) AS num,
                                     SUM(yards) AS yards,
                                     IF(SUM(num) > 0, SUM(yards) / SUM(num), 0) AS avg,
                                     MAX(`long`) AS `long`,
                                     SUM(fair_catch) AS fair_catch,
                                     SUM(td) AS td,
                                     0 AS qual',
                                    '(SUM(num) + SUM(fair_catch)) > 0');

  # Qualification Criteria
  CALL nfl_totals_players_sort__tmp_qual_puntret(v_season, v_season_type);

  # Calcs
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTRET', 'num', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTRET', 'yards', 'SMALLINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTRET', 'avg', 'DECIMAL(7,5)', '>', 'qual');
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTRET', 'avg_all', 'DECIMAL(7,5)', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTRET', 'long', 'TINYINT SIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTRET', 'fair_catch', 'TINYINT UNSIGNED', '>', NULL);
  CALL nfl_totals_players_sort__calc(v_season, v_season_type, 'PUNTRET', 'td', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PUNTRET_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_cols VARCHAR(1024),
  v_calcs VARCHAR(1024),
  v_having VARCHAR(100)
)
    COMMENT 'Create temporary table for NFL player season stat sort'
BEGIN

  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table));
  # When first creating the table, get player total - one row per player
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      player_id SMALLINT UNSIGNED NOT NULL,
      team_id VARCHAR(4),
      ', v_cols, ',
      PRIMARY KEY (season, season_type, player_id, team_id)
    ) ENGINE = MEMORY
      SELECT season, season_type, player_id, \'_NFL\' AS team_id, ', v_calcs, '
      FROM SPORTS_NFL_PLAYERS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '"
      GROUP BY season, season_type, player_id
      ', IF(v_having IS NOT NULL, CONCAT('HAVING ', v_having), ''), ';'));
  # But then also get team_id grouped stats for each player
  CALL _exec(CONCAT(
   'INSERT INTO tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '
      SELECT season, season_type, player_id, team_id, ', v_calcs, '
      FROM SPORTS_NFL_PLAYERS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '"
      GROUP BY season, season_type, player_id, team_id
      ', IF(v_having IS NOT NULL, CONCAT('HAVING ', v_having), ''), ';'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col_sort VARCHAR(25),
  v_col_def VARCHAR(50),
  v_sort_dir VARCHAR(5),
  v_qual  VARCHAR(10)
)
    COMMENT 'Sort season totals for NFL players'
BEGIN

  # Data column (which may not be v_col_sort)
  DECLARE v_col_data VARCHAR(25);
  IF SUBSTRING(v_col_sort, -4) = '_all' THEN
    SET v_col_data = LEFT(v_col_sort, LENGTH(v_col_sort) - 4);
  ELSE
    SET v_col_data = v_col_sort;
  END IF;

  # Determine unique values for this column for sorting
  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT;'));
  CALL _exec(CONCAT(
    'CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT (
       team_id VARCHAR(4),
       `', v_col_data, '` ', v_col_def, ',
       ', IF(v_qual IS NOT NULL, CONCAT('`', v_qual, '` TINYINT UNSIGNED,'), ''), '
       num_players SMALLINT UNSIGNED,
       value_order SMALLINT UNSIGNED,
       PRIMARY KEY (team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ')
     ) ENGINE = MEMORY
       SELECT team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', COUNT(player_id) AS num_players, 9999 AS value_order
       FROM tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '
       GROUP BY team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ';'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT_cpA'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT_cpB'));

  # Sort these values
  CALL _exec(CONCAT(
    'INSERT INTO tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT (team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', value_order)
       SELECT A.team_id, A.', v_col_data, ', ', IF(v_qual IS NOT NULL, CONCAT('A.', v_qual, ', '), ''), 'IFNULL(SUM(B.num_players), 0) + 1 AS value_order
       FROM tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT_cpA AS A
       LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT_cpB AS B
         ON (B.team_id = A.team_id
         AND (', IF(v_qual IS NOT NULL,
                    CONCAT('B.', v_qual, ' > A.', v_qual, ' OR (B.', v_qual, ' = A.', v_qual, ' AND '), ''),
              'B.', v_col_data, ' ', v_sort_dir, ' A.', v_col_data,
              IF (v_qual IS NOT NULL, ')', ''), '))
       GROUP BY A.team_id, A.', v_col_data, IF(v_qual IS NOT NULL, CONCAT(', A.', v_qual), ''), '
     ON DUPLICATE KEY UPDATE value_order = VALUES(value_order);'));

  # Update sorting for the players
  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_NFL_PLAYERS_SEASON_', v_table, '_SORTED (season, season_type, player_id, team_id, `', v_col_sort, '`)
      SELECT PLAYER.season, PLAYER.season_type, PLAYER.player_id, PLAYER.team_id,
             STAT.value_order AS `', v_col_sort, '`
      FROM tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, ' AS PLAYER
      JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_STAT AS STAT
        ON (STAT.team_id = PLAYER.team_id
        AND STAT.', v_col_data, ' = PLAYER.', v_col_data,
            IF(v_qual IS NOT NULL,
               CONCAT(' AND STAT.', v_qual, '= PLAYER.', v_qual), ''), ')
      WHERE PLAYER.season = ', v_season, '
      AND   PLAYER.season_type = "', v_season_type, '"
    ON DUPLICATE KEY UPDATE `', v_col_sort, '` = VALUES(`', v_col_sort, '`);'));

END $$

DELIMITER ;

