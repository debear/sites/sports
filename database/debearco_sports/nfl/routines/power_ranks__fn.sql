##
## Determine whether all games have been played in a given week
##
DROP FUNCTION IF EXISTS `nfl_power_ranks_ready`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_power_ranks_ready`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_week TINYINT UNSIGNED
) RETURNS TINYINT UNSIGNED
    NOT DETERMINISTIC
    COMMENT 'Can we run the NFL power ranks for a given week?'
BEGIN

  DECLARE v_test TINYINT UNSIGNED;

  SELECT SUM(IF(status IS NULL, 1, 0)) INTO v_test
  FROM SPORTS_NFL_SCHEDULE
  WHERE season = v_season
  AND   game_type = v_game_type
  AND   week = v_week;

  IF v_test = 0 THEN
    RETURN 1;
  END IF;

  RETURN 0;

END $$

DELIMITER ;

