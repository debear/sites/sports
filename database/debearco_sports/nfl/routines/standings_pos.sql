##
## Calculate the league/conf/div positions
##
DROP PROCEDURE IF EXISTS `nfl_standings_pos`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_pos`()
    COMMENT 'Calculate exact NFL positions'
BEGIN

  # We use recursion, so up the internal limit by 5
  DECLARE v_recursion_limit TINYINT UNSIGNED;
  SET v_recursion_limit = @@max_sp_recursion_depth;
  SET max_sp_recursion_depth = IF(v_recursion_limit < 10, v_recursion_limit + 5, v_recursion_limit);

  # Now run...
  CALL nfl_standings_pos_base();
  CALL nfl_standings_pos_div();
  CALL nfl_standings_pos_conf();
  CALL nfl_standings_pos_league();
  CALL nfl_standings_pos_draft();

  # Reset the recursion limit
  SET max_sp_recursion_depth = v_recursion_limit;

END $$

DELIMITER ;

