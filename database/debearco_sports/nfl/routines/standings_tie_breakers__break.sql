##
## Break ties within the divisions
##
DROP PROCEDURE IF EXISTS `nfl_standings_break`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break`(
  v_group_type ENUM('div','conf','league','draft')
)
    COMMENT 'Break NFL standing ties'
BEGIN

  # Declare vars
  DECLARE v_group_id TINYINT UNSIGNED;
  DECLARE v_group_pos TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Declare cursor
  DECLARE cur_ties CURSOR FOR
    SELECT group_id, group_pos FROM tmp_tiebrk_groups;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Stage 1: Identify the ties
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_list;
  CREATE TEMPORARY TABLE tmp_tiebrk_list (
    team_id VARCHAR(3),
    group_id TINYINT UNSIGNED,
    group_pos TINYINT UNSIGNED,
    group_pos_tied TINYINT UNSIGNED,
    PRIMARY KEY (team_id, group_id)
  ) ENGINE = MEMORY;

  CALL _exec(CONCAT('INSERT INTO tmp_tiebrk_list (team_id, group_id, group_pos, group_pos_tied)
    SELECT team_id, ', IF(v_group_type NOT IN ('league', 'draft'), CONCAT(v_group_type, '_id'), 0), ' AS group_id, ', v_group_type, '_pos AS group_pos, 1 AS is_tied
    FROM tmp_pos
    WHERE ', v_group_type, '_pos_tied = 1;'));

  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_groups;
  CREATE TEMPORARY TABLE tmp_tiebrk_groups (
    group_id TINYINT UNSIGNED,
    group_pos TINYINT UNSIGNED,
    PRIMARY KEY (group_id, group_pos)
  ) ENGINE = MEMORY
    SELECT DISTINCT group_id, group_pos
    FROM tmp_tiebrk_list;

  # Stage 2: Loop through them, breaking each individually
  OPEN cur_ties;
  loop_ties: LOOP

    FETCH cur_ties INTO v_group_id, v_group_pos;
    IF v_done = 1 THEN LEAVE loop_ties; END IF;

    # Break the tie in our temporary table
    CALL nfl_standings_break_group(v_group_type, v_group_id, v_group_pos);

    # Then transfer to our 'real' table
    CALL _exec(CONCAT('UPDATE tmp_pos
    JOIN tmp_tiebrk_list
      ON (tmp_tiebrk_list.team_id = tmp_pos.team_id)
    SET tmp_pos.', v_group_type, '_pos = tmp_tiebrk_list.group_pos,
        tmp_pos.', v_group_type, '_pos_tied = tmp_tiebrk_list.group_pos_tied;'));

    # Remove from our temporary table
    CALL _exec(CONCAT('DELETE FROM tmp_tiebrk_list WHERE group_id = ', v_group_id, ' AND group_pos_tied = 0;'));

  END LOOP loop_ties;
  CLOSE cur_ties;

END $$

DELIMITER ;

##
## Break a tie in a group
##
DROP PROCEDURE IF EXISTS `nfl_standings_break_group`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_group`(
  v_group_type ENUM('div','conf','league','draft'),
  v_group_id TINYINT UNSIGNED,
  v_group_pos TINYINT UNSIGNED
)
    COMMENT 'Break NFL group ties'
BEGIN

  # Declare vars
  DECLARE v_team_id VARCHAR(3);

  ## Break the tie
  WHILE nfl_standings_tiebrk_num(v_group_id, v_group_pos) > 1 DO
    CALL nfl_standings_break_group_exec(v_group_type, v_group_id, v_group_pos);

    # Identify the 'winning' team
    SELECT team_id INTO v_team_id
    FROM tmp_tiebrk_teams
    WHERE new_pos = 0;

    # Record their as being broken
    UPDATE tmp_tiebrk_list
    SET group_pos_tied = 0
    WHERE team_id = v_team_id
    AND   group_id = v_group_id;

    # Bump the other teams
    UPDATE tmp_tiebrk_list
    SET group_pos = group_pos + 1
    WHERE group_id = v_group_id
    AND   group_pos = v_group_pos
    AND   group_pos_tied = 1;

    SET v_group_pos = v_group_pos + 1;
  END WHILE;

  # Un-mark the last tied team
  UPDATE tmp_tiebrk_list
  SET group_pos_tied = 0
  WHERE group_id = v_group_id
  AND   group_pos = v_group_pos;

END $$

DELIMITER ;

##
## Break a tie in a group
##
DROP PROCEDURE IF EXISTS `nfl_standings_break_group_exec`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_group_exec`(
  v_group_type ENUM('div','conf','league','draft'),
  v_group_id TINYINT UNSIGNED,
  v_group_pos TINYINT UNSIGNED
)
    COMMENT 'Break NFL group ties'
BEGIN

  # Run (this way as using 'CALL _exec' results in a recursive call error
  IF v_group_type = 'div' THEN
    CALL nfl_standings_break_div(v_group_id, v_group_pos);
  ELSEIF v_group_type = 'conf' THEN
    CALL nfl_standings_break_conf(v_group_id, v_group_pos);
  ELSEIF v_group_type = 'league' THEN
    CALL nfl_standings_break_league(v_group_id, v_group_pos);
  ELSEIF v_group_type = 'draft' THEN
    CALL nfl_standings_break_draft(v_group_id, v_group_pos);
  END IF;

END $$

DELIMITER ;

#
# Test
#
DROP PROCEDURE IF EXISTS `nfl_standings_break_group_test`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_group_test`(
  IN v_group_type ENUM('div','conf','league','draft'),
  IN v_group_id TINYINT UNSIGNED,
  IN v_group_pos TINYINT UNSIGNED,
  OUT v_out TINYINT UNSIGNED
)
    COMMENT 'Process the success of NFL standings test'
BEGIN

  DECLARE v_test_status ENUM('success', 'partial', 'fail');

  SET v_out = 0;
  SET v_test_status = nfl_standings_tiebrk_status();
  IF v_test_status = 'success' THEN
    # Tie broken, return
    SET v_out = 1;

  ELSEIF v_test_status = 'partial' THEN
    # Tie only partially broken, need to break the sub-tie
    CALL nfl_standings_break_group_recurse(v_group_type, v_group_id, v_group_pos);
    SET v_out = 1;

  END IF;

END $$

DELIMITER ;

#
# Trigger (and tidy up after) a recursive tiebreak
#
DROP PROCEDURE IF EXISTS `nfl_standings_break_group_recurse`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_group_recurse`(
  v_group_type ENUM('div','conf','league','draft'),
  v_group_id TINYINT UNSIGNED,
  v_group_pos TINYINT UNSIGNED
)
    COMMENT 'Break an NFL standings recursive sub-tiebreak'
BEGIN

  DECLARE v_tmp_group_id TINYINT UNSIGNED;
  DECLARE v_team_id VARCHAR(3);

  # Create as a new group
  SELECT MAX(group_id) + 1 INTO v_tmp_group_id FROM tmp_tiebrk_list;
  INSERT INTO tmp_tiebrk_list (team_id, group_id, group_pos, group_pos_tied)
    SELECT team_id, v_tmp_group_id, v_group_pos, 1
    FROM tmp_tiebrk_teams
    WHERE new_pos = 0
    AND   new_pos_tied = 1;

  # Backup the temporary table we will be over-writing
  CALL _duplicate_tmp_table('tmp_tiebrk_teams', CONCAT('tmp_tiebrk_teams_', v_group_type, v_tmp_group_id));

  # Break that temporary group
  CALL nfl_standings_break_group_exec(v_group_type, v_tmp_group_id, v_group_pos);

  # Update the original group with the result
  SELECT team_id INTO v_team_id
  FROM tmp_tiebrk_teams
  WHERE new_pos = 0;

  # Restore the over-written temporary table
  CALL _duplicate_tmp_table(CONCAT('tmp_tiebrk_teams_', v_group_type, v_tmp_group_id), 'tmp_tiebrk_teams');

  # Bump the remaining teams...
  UPDATE tmp_tiebrk_teams
  SET new_pos = new_pos + (team_id <> v_team_id),
      new_pos_tied = (team_id <> v_team_id);

  # Delete the temporary group
  DELETE FROM tmp_tiebrk_list
  WHERE group_id = v_tmp_group_id
  AND   group_pos = v_group_pos;

  CALL _exec(CONCAT('DROP TEMPORARY TABLE tmp_tiebrk_teams_', v_group_type, v_tmp_group_id, ';'));

END $$

DELIMITER ;

