##
## Calculate the standings
##
DROP PROCEDURE IF EXISTS `nfl_standings`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings`(
  v_season SMALLINT UNSIGNED,
  v_week TINYINT UNSIGNED,
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate NFL standings'
BEGIN

  # Clear a previous run and get info
  CALL nfl_standings_setup(v_season, v_week);

  # Win/Loss records
  CALL nfl_standings_records(v_recent_games);

  # Current streak
  CALL nfl_standings_streak();

  # League/Conf/Div Positions
  CALL nfl_standings_pos();

  # Status Codes
  CALL nfl_standings_codes(v_season);

  # Finally, store...
  CALL nfl_standings_store(v_season, v_week);

END $$

DELIMITER ;

#
# Setup
#
DROP PROCEDURE IF EXISTS `nfl_standings_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_setup`(
  v_season SMALLINT UNSIGNED,
  v_week TINYINT UNSIGNED
)
    COMMENT 'Setup the NFL standing calculations'
BEGIN

  # Some config
  CALL nfl_standings_config(v_season, v_week);

  # Get the teams and their schedules up to the stated week
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    div_id TINYINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT TEAM.team_id,
           TEAM.grouping_id AS div_id,
           CONF.parent_id AS conf_id
    FROM SPORTS_NFL_TEAMS_GROUPINGS AS TEAM
    JOIN SPORTS_NFL_GROUPINGS AS CONF
      ON (CONF.grouping_id = TEAM.grouping_id)
    WHERE v_season BETWEEN TEAM.season_from AND IFNULL(TEAM.season_to, 2099);

  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  DROP TEMPORARY TABLE IF EXISTS tmp_sched;
  CREATE TEMPORARY TABLE tmp_sched (
    team_id VARCHAR(3),
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    week TINYINT UNSIGNED,
    game_id SMALLINT UNSIGNED,
    opp_team_id VARCHAR(3),
    result CHAR(1),
    is_home TINYINT(1) UNSIGNED,
    pts_for TINYINT UNSIGNED,
    pts_against TINYINT UNSIGNED,
    opp_is_div TINYINT(1) UNSIGNED,
    opp_is_conf TINYINT(1) UNSIGNED,
    num_later TINYINT(1) UNSIGNED,
    PRIMARY KEY (team_id, week),
    INDEX by_game (season, game_type, week, game_id, team_id),
    INDEX by_opp (team_id, opp_team_id),
    INDEX by_result (team_id, result),
    INDEX by_div (team_id, opp_is_div),
    INDEX by_conf (team_id, opp_is_conf),
    INDEX by_week (week)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id,
           SPORTS_NFL_SCHEDULE.season,
           SPORTS_NFL_SCHEDULE.game_type,
           SPORTS_NFL_SCHEDULE.week,
           SPORTS_NFL_SCHEDULE.game_id,
           IF(SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id, SPORTS_NFL_SCHEDULE.visitor, SPORTS_NFL_SCHEDULE.home) AS opp_team_id,
           IF(SPORTS_NFL_SCHEDULE.home_score = SPORTS_NFL_SCHEDULE.visitor_score,
              't',
              IF((SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id AND SPORTS_NFL_SCHEDULE.home_score > SPORTS_NFL_SCHEDULE.visitor_score)
                  OR (SPORTS_NFL_SCHEDULE.visitor = tmp_teams.team_id AND SPORTS_NFL_SCHEDULE.home_score < SPORTS_NFL_SCHEDULE.visitor_score),
                 'w', 'l')) AS result,
           SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id AS is_home,
           IF(SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id, SPORTS_NFL_SCHEDULE.home_score, SPORTS_NFL_SCHEDULE.visitor_score) AS pts_for,
           IF(SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id, SPORTS_NFL_SCHEDULE.visitor_score, SPORTS_NFL_SCHEDULE.home_score) AS pts_against,
           tmp_teams.div_id = tmp_teams_cp.div_id AS opp_is_div,
           tmp_teams.conf_id = tmp_teams_cp.conf_id AS opp_is_conf,
           0 AS num_later
    FROM tmp_teams
    JOIN SPORTS_NFL_SCHEDULE
      ON (SPORTS_NFL_SCHEDULE.season = v_season
      AND SPORTS_NFL_SCHEDULE.game_type = 'regular'
      AND SPORTS_NFL_SCHEDULE.week <= v_week
      AND (SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id
        OR SPORTS_NFL_SCHEDULE.visitor = tmp_teams.team_id)
      AND IFNULL(SPORTS_NFL_SCHEDULE.status, 'TBP') NOT IN ('TBP', 'PPD', 'CNC'))
    JOIN tmp_teams_cp
      ON (tmp_teams_cp.team_id = IF(SPORTS_NFL_SCHEDULE.home = tmp_teams.team_id, SPORTS_NFL_SCHEDULE.visitor, SPORTS_NFL_SCHEDULE.home));

  # Identify the recent games
  CALL _duplicate_tmp_table('tmp_sched', 'tmp_sched_cpA');
  CALL _duplicate_tmp_table('tmp_sched', 'tmp_sched_cpB');

  INSERT INTO tmp_sched (team_id, week, num_later)
    SELECT tmp_sched_cpA.team_id, tmp_sched_cpA.week,
           COUNT(tmp_sched_cpB.week) AS num_later
    FROM tmp_sched_cpA
    LEFT JOIN tmp_sched_cpB
      ON (tmp_sched_cpB.team_id = tmp_sched_cpA.team_id
      AND tmp_sched_cpB.week > tmp_sched_cpA.week)
    GROUP BY tmp_sched_cpA.team_id, tmp_sched_cpA.week
  ON DUPLICATE KEY UPDATE num_later = VALUES(num_later);

END $$

DELIMITER ;

#
# Internal config options
#
DROP PROCEDURE IF EXISTS `nfl_standings_config`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_config`(
  v_season SMALLINT UNSIGNED,
  v_week TINYINT UNSIGNED
)
    COMMENT 'Config options for the NFL standing calculations'
BEGIN

  DECLARE v_team_id CHAR(3);
  DECLARE v_tmp_num SMALLINT;

  DROP TEMPORARY TABLE IF EXISTS tmp_config;
  CREATE TEMPORARY TABLE tmp_config (
    name VARCHAR(20),
    value_num SMALLINT NULL DEFAULT NULL,
    value_txt VARCHAR(200) NULL DEFAULT NULL,
    PRIMARY KEY (name)
  ) ENGINE = MEMORY;

  # Maximum games played per-team
  SELECT team_id INTO v_team_id
  FROM SPORTS_NFL_TEAMS_GROUPINGS
  WHERE v_season BETWEEN season_from AND IFNULL(season_to, 2099)
  LIMIT 1;

  INSERT INTO tmp_config (name, value_num)
    SELECT 'max_games' AS name,
           COUNT(game_id) AS value_num
    FROM SPORTS_NFL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    AND   v_team_id IN (home, visitor);

  # Number of teams with a playoff bye (From 2020: 1 team; Before: 2 teams)
  SET v_tmp_num = IF(v_season <= 2019, 2, 1);
  INSERT INTO tmp_config (name, value_num) VALUES ('num_playoff_byes', v_tmp_num);

  # Number of playoff teams (From 2020: 7 teams; Before: 6 teams - aka. 8 minus number of byes)
  INSERT INTO tmp_config (name, value_num) VALUES ('num_playoff_teams', 8 - v_tmp_num);

END $$

DELIMITER ;

#
# Merge all the info into the final table
#
DROP PROCEDURE IF EXISTS `nfl_standings_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_store`(
  v_season SMALLINT UNSIGNED,
  v_week TINYINT UNSIGNED
)
    COMMENT 'Store the NFL standings into the permanent table'
BEGIN

  # Clear a previous run
  DELETE FROM SPORTS_NFL_STANDINGS WHERE season = v_season AND week = v_week;

  # Now add
  INSERT INTO SPORTS_NFL_STANDINGS (season, week, team_id,
                                    wins, loss, ties, pts_for, pts_against,
                                    home_wins, home_loss, home_ties, visitor_wins, visitor_loss, visitor_ties,
                                    conf_wins, conf_loss, conf_ties, div_wins, div_loss, div_ties,
                                    recent_wins, recent_loss, recent_ties,
                                    pos_league, pos_conf, pos_div, pos_draft, pos_draft_ct,
                                    streak_type, streak_num, status_code)
     SELECT v_season, v_week, tmp_teams.team_id,
            -- Overall
            IFNULL(tmp_records.wins, 0) AS wins,
            IFNULL(tmp_records.loss, 0) AS loss,
            IFNULL(tmp_records.ties, 0) AS ties,
            IFNULL(tmp_records.pts_for, 0) AS pts_for,
            IFNULL(tmp_records.pts_against, 0) AS pts_against,
            -- Home
            IFNULL(tmp_records.home_wins, 0) AS home_wins,
            IFNULL(tmp_records.home_loss, 0) AS home_loss,
            IFNULL(tmp_records.home_ties, 0) AS home_ties,
            -- Visitor
            IFNULL(tmp_records.visitor_wins, 0) AS visitor_wins,
            IFNULL(tmp_records.visitor_loss, 0) AS visitor_loss,
            IFNULL(tmp_records.visitor_ties, 0) AS visitor_ties,
            -- v Conf
            IFNULL(tmp_records.conf_wins, 0) AS conf_wins,
            IFNULL(tmp_records.conf_loss, 0) AS conf_loss,
            IFNULL(tmp_records.conf_ties, 0) AS conf_ties,
            -- v Div
            IFNULL(tmp_records.div_wins, 0) AS div_wins,
            IFNULL(tmp_records.div_loss, 0) AS div_loss,
            IFNULL(tmp_records.div_ties, 0) AS div_ties,
            -- Recent
            IFNULL(tmp_records.recent_wins, 0) AS recent_wins,
            IFNULL(tmp_records.recent_loss, 0) AS recent_loss,
            IFNULL(tmp_records.recent_ties, 0) AS recent_ties,
            -- Pos
            tmp_pos.league_pos AS pos_league,
            tmp_pos.conf_pos AS pos_conf,
            tmp_pos.div_pos AS pos_div,
            tmp_pos.draft_pos AS pos_draft,
            tmp_pos.draft_pos_coin_toss AS pos_draft_ct,
            -- Streak
            tmp_streak.streak_type,
            tmp_streak.streak_num,
            tmp_standings_codes.code AS status_code
     FROM tmp_teams
     LEFT JOIN tmp_records
       ON (tmp_records.team_id = tmp_teams.team_id)
     LEFT JOIN tmp_pos
       ON (tmp_pos.team_id = tmp_teams.team_id)
     LEFT JOIN tmp_streak
       ON (tmp_streak.team_id = tmp_teams.team_id)
     LEFT JOIN tmp_standings_codes
       ON (tmp_standings_codes.team_id = tmp_teams.team_id);

  ALTER TABLE SPORTS_NFL_STANDINGS ORDER BY season, week, team_id;

END $$

DELIMITER ;
