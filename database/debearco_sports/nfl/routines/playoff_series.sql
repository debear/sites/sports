##
## Playoff series
##
DROP PROCEDURE IF EXISTS `nfl_playoff_series`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_series`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate NFL playoff series'
BEGIN

  # Conference-based matchups
  UPDATE SPORTS_NFL_PLAYOFF_MATCHUPS AS MATCHUP
  JOIN SPORTS_NFL_PLAYOFF_SEEDS AS HIGHER_SEED
    ON (HIGHER_SEED.season = MATCHUP.season
    AND HIGHER_SEED.conf_id = MATCHUP.higher_conf_id
    AND HIGHER_SEED.seed = MATCHUP.higher_seed)
  JOIN SPORTS_NFL_PLAYOFF_SEEDS AS LOWER_SEED
    ON (LOWER_SEED.season = MATCHUP.season
    AND LOWER_SEED.conf_id = MATCHUP.lower_conf_id
    AND LOWER_SEED.seed = MATCHUP.lower_seed)
  JOIN SPORTS_NFL_SCHEDULE AS SCHEDULE
    ON (SCHEDULE.season = MATCHUP.season
    AND SCHEDULE.game_type = 'playoff'
    AND SCHEDULE.week = MATCHUP.week
    AND SCHEDULE.home = HIGHER_SEED.team_id
    AND SCHEDULE.visitor = LOWER_SEED.team_id
    AND IFNULL(SCHEDULE.status, 'TBP') NOT IN ('TBP', 'PPD', 'CNC'))
  SET MATCHUP.game_id = SCHEDULE.game_id,
      MATCHUP.higher_score = SCHEDULE.home_score,
      MATCHUP.lower_score = SCHEDULE.visitor_score,
      MATCHUP.complete = 1
  WHERE MATCHUP.season = v_season
  AND   MATCHUP.week < 4
  AND   MATCHUP.game_id IS NULL;

  # Super Bowl (non-deterministic home/visitor)
  UPDATE SPORTS_NFL_PLAYOFF_MATCHUPS AS MATCHUP
  JOIN SPORTS_NFL_PLAYOFF_SEEDS AS HIGHER_SEED
    ON (HIGHER_SEED.season = MATCHUP.season
    AND HIGHER_SEED.conf_id = MATCHUP.higher_conf_id
    AND HIGHER_SEED.seed = MATCHUP.higher_seed)
  JOIN SPORTS_NFL_PLAYOFF_SEEDS AS LOWER_SEED
    ON (LOWER_SEED.season = MATCHUP.season
    AND LOWER_SEED.conf_id = MATCHUP.lower_conf_id
    AND LOWER_SEED.seed = MATCHUP.lower_seed)
  JOIN SPORTS_NFL_SCHEDULE AS SCHEDULE
    ON (SCHEDULE.season = MATCHUP.season
    AND SCHEDULE.game_type = 'playoff'
    AND SCHEDULE.week = MATCHUP.week
    AND HIGHER_SEED.team_id IN (SCHEDULE.home, SCHEDULE.visitor)
    AND LOWER_SEED.team_id IN (SCHEDULE.home, SCHEDULE.visitor)
    AND IFNULL(SCHEDULE.status, 'TBP') NOT IN ('TBP', 'PPD', 'CNC'))
  SET MATCHUP.game_id = SCHEDULE.game_id,
      MATCHUP.higher_score = IF(HIGHER_SEED.team_id = SCHEDULE.home, SCHEDULE.home_score, SCHEDULE.visitor_score),
      MATCHUP.lower_score = IF(LOWER_SEED.team_id = SCHEDULE.home, SCHEDULE.home_score, SCHEDULE.visitor_score),
      MATCHUP.complete = 1
  WHERE MATCHUP.season = v_season
  AND   MATCHUP.week = 4
  AND   MATCHUP.game_id IS NULL;

END $$

DELIMITER ;
