##
## NFL power rank calculations
##
DROP PROCEDURE IF EXISTS `nfl_power_ranks`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_week TINYINT UNSIGNED,
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks for a given week'
proc_ranks: BEGIN

  # All games must have been played this week
  IF nfl_power_ranks_ready(v_season, v_game_type, v_week) = 0 THEN
    LEAVE proc_ranks;
  END IF;

  # Build the schedule info to know which games to process
  CALL nfl_power_ranks_build_sched(v_season, v_game_type, v_week, v_recent_games);

  # Table to store our results
  DROP TEMPORARY TABLE IF EXISTS tmp_ranks;
  CREATE TEMPORARY TABLE tmp_ranks (
    team_id VARCHAR(3),
    `rank` TINYINT UNSIGNED,
    score DECIMAL(5,3),
    has_gp TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id, 0 AS `rank`, 0 AS score, COUNT(tmp_sched.game_id) > 0 AS has_gp
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Build the individual components and combine into a single score
  CALL nfl_power_ranks_build();

  # Rank
  CALL nfl_power_ranks_sort(v_season, v_game_type, v_week);

  # Finally, store...
  CALL nfl_power_ranks_store(v_season, v_game_type, v_week);

END $$

DELIMITER ;

#
# Sort by rank
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_sort`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_week TINYINT UNSIGNED
)
    COMMENT 'Sort the NFL power ranks'
BEGIN

  DECLARE v_test_week TINYINT UNSIGNED;

  CALL _duplicate_tmp_table('tmp_ranks', 'tmp_ranks_cpA');

  # If we are processing a playoff week, rank teams still alive above those who were eliminated
  ALTER TABLE tmp_ranks_cpA ADD COLUMN playoff_status TINYINT UNSIGNED AFTER team_id;
  IF v_game_type = 'playoff' THEN
    # Column to store our info

    # Establish who missed the playoffs, our lowest playoff status
    SELECT MAX(week) INTO v_test_week FROM SPORTS_NFL_STANDINGS WHERE season = v_season;
    UPDATE tmp_ranks_cpA
    JOIN SPORTS_NFL_STANDINGS
      ON (SPORTS_NFL_STANDINGS.season = v_season
      AND SPORTS_NFL_STANDINGS.week = v_test_week
      AND SPORTS_NFL_STANDINGS.team_id = tmp_ranks_cpA.team_id
      AND SPORTS_NFL_STANDINGS.status_code = 'e')
    SET tmp_ranks_cpA.playoff_status = 0;

    # Then losers by playoff round
    SET v_test_week = 1;
    WHILE v_test_week <= v_week DO
      # Update the losers for this playoff week
      UPDATE tmp_ranks_cpA
      JOIN tmp_sched
        ON (tmp_sched.season = v_season
        AND tmp_sched.game_type = v_game_type
        AND tmp_sched.week = v_test_week
        AND tmp_sched.team_id = tmp_ranks_cpA.team_id
        AND tmp_sched.result = 'l')
      SET tmp_ranks_cpA.playoff_status = v_test_week;

      # Increment our counter
      SET v_test_week = v_test_week + 1;
    END WHILE;

    # Anyone still here (status is still NULL) is still alive
    UPDATE tmp_ranks_cpA SET playoff_status = 5 WHERE playoff_status IS NULL;

  # Regular season, so set the column to something arbitrary (not NULL) be *equal* among all teams
  ELSE
    UPDATE tmp_ranks_cpA SET playoff_status = 0;
  END IF;

  # Then run
  CALL _duplicate_tmp_table('tmp_ranks_cpA', 'tmp_ranks_cpB');

  INSERT INTO tmp_ranks (team_id, `rank`)
    SELECT tmp_ranks_cpA.team_id, COUNT(tmp_ranks_cpB.team_id) + 1 AS `rank`
    FROM tmp_ranks_cpA
    LEFT JOIN tmp_ranks_cpB
      # Seperate on playoff tiers
      ON (tmp_ranks_cpB.playoff_status > tmp_ranks_cpA.playoff_status
      # Seperate teams that have played from those who haven't
      OR (tmp_ranks_cpB.playoff_status = tmp_ranks_cpA.playoff_status
      AND tmp_ranks_cpB.has_gp > tmp_ranks_cpA.has_gp)
      # Rank below teams with a higher score
      OR (tmp_ranks_cpB.playoff_status = tmp_ranks_cpA.playoff_status
      AND tmp_ranks_cpB.has_gp = tmp_ranks_cpA.has_gp
      AND tmp_ranks_cpB.score > tmp_ranks_cpA.score)
      # Or alphabetically when tied
      OR (tmp_ranks_cpB.playoff_status = tmp_ranks_cpA.playoff_status
      AND tmp_ranks_cpB.has_gp = tmp_ranks_cpA.has_gp
      AND tmp_ranks_cpB.score = tmp_ranks_cpA.score
      AND STRCMP(tmp_ranks_cpB.team_id, tmp_ranks_cpA.team_id) = -1))
    GROUP BY tmp_ranks_cpA.team_id
  ON DUPLICATE KEY UPDATE `rank` = VALUES(`rank`);

  # We don't care about the numeric score for teams who haven't played yet
  UPDATE tmp_ranks
  SET score = NULL,
      win_pct = NULL,
      num_td = NULL,
      net_yards = NULL,
      time_of_poss = NULL,
      34dn_pct = NULL,
      rz_pct = NULL,
      pen_yards = NULL,
      net_to = NULL
  WHERE has_gp = 0;

END $$

DELIMITER ;

#
# Store the final power ranks
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_store`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_week TINYINT UNSIGNED
)
    COMMENT 'Store the NFL power ranks'
BEGIN

  DELETE FROM SPORTS_NFL_POWER_RANKINGS
  WHERE season = v_season
  AND   game_type = v_game_type
  AND   week = v_week;

  ALTER TABLE tmp_ranks DROP COLUMN has_gp;

  INSERT INTO SPORTS_NFL_POWER_RANKINGS
    SELECT v_season AS season, v_game_type AS game_type, v_week AS week, tmp_ranks.*, NULL AS comment
    FROM tmp_ranks;

  UPDATE SPORTS_NFL_POWER_RANKINGS_WEEKS
  SET when_run = NOW()
  WHERE season = v_season
  AND   game_type = v_game_type
  AND   week = v_week;

  ALTER TABLE SPORTS_NFL_POWER_RANKINGS ORDER BY season, game_type, week, team_id;

END $$

DELIMITER ;

