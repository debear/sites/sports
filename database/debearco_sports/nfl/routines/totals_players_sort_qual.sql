##
## Player sorting qualification logic
##
DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_prep`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_prep`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Standard NFL qualifying criteria pre work'
BEGIN

  # Build from a small table of players that occur in every game
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP (
    team_id VARCHAR(3),
    num TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, COUNT(DISTINCT game_id) AS num
    FROM SPORTS_NFL_PLAYERS_GAME_PASSING
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_update`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_update`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col VARCHAR(10)
)
    COMMENT 'Standard NFL qualifying criteria pre work'
BEGIN

  # And update our table
  CALL _exec(CONCAT(
   'UPDATE tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, '_QUAL AS QUAL
    JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_', v_table, ' AS STAT
      ON (STAT.season = QUAL.season
      AND STAT.season_type = QUAL.season_type
      AND STAT.player_id = QUAL.player_id)
    SET STAT.', v_col, ' = IF(STAT.team_id = \'_NFL\', QUAL.', v_col, ', 1);')); # Players always qualify for team grouped lists

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_passing`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_passing`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NFL passer sorting'
BEGIN

  # Completion Percentage; >= 14 att / gm
  # Yards per Completion; >= 14 att / gm
  # Passer Rating; >= 14 att / gm
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_PASSING_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_PASSING_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           SUM(STAT.atts) >= (MIN(TEAM_GP.num) * 14) AS qual
    FROM SPORTS_NFL_PLAYERS_SEASON_PASSING AS STAT
    LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP AS TEAM_GP
      ON (TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'PASSING', 'qual');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_rushing`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_rushing`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NFL rushing sorting'
BEGIN

  # Yards per Rush; >= 6.25 att / gm
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_RUSHING_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_RUSHING_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           SUM(STAT.atts) >= (MIN(TEAM_GP.num) * 6.25) AS qual
    FROM SPORTS_NFL_PLAYERS_SEASON_RUSHING AS STAT
    LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP AS TEAM_GP
      ON (TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'RUSHING', 'qual');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_receiving`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_receiving`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NFL receiving sorting'
BEGIN

  # Yards per Reception; >= 1.875 recep / gm
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_RECEIVING_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_RECEIVING_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           SUM(STAT.recept) >= (MIN(TEAM_GP.num) * 1.875) AS qual
    FROM SPORTS_NFL_PLAYERS_SEASON_RECEIVING AS STAT
    LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP AS TEAM_GP
      ON (TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'RECEIVING', 'qual');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_kicking`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_kicking`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NFL kicking sorting'
BEGIN

  # FG %age; >= 0.75 att / gm
  # XP %age; >= 1.5 att / gm
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_KICKING_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_KICKING_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual_fg TINYINT UNSIGNED,
    qual_xp TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           SUM(STAT.fg_att) >= (MIN(TEAM_GP.num) * 0.75) AS qual_fg,
           SUM(STAT.xp_att) >= (MIN(TEAM_GP.num) * 1.5) AS qual_xp
    FROM SPORTS_NFL_PLAYERS_SEASON_KICKING AS STAT
    LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP AS TEAM_GP
      ON (TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'KICKING', 'qual_fg');
  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'KICKING', 'qual_xp');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_punts`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_punts`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NFL kicking punting'
BEGIN

  # Average; >= 1 punt / gm
  # Net Average; >= 1 punt / gm
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_PUNTS_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_PUNTS_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           SUM(STAT.num) >= MIN(TEAM_GP.num) AS qual
    FROM SPORTS_NFL_PLAYERS_SEASON_PUNTS AS STAT
    LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP AS TEAM_GP
      ON (TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'PUNTS', 'qual');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_kickret`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_kickret`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NFL kicking kick returns'
BEGIN

  # Average; >= 1 ret / gm
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_KICKRET_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_KICKRET_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           SUM(STAT.num) >= MIN(TEAM_GP.num) AS qual
    FROM SPORTS_NFL_PLAYERS_SEASON_KICKRET AS STAT
    LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP AS TEAM_GP
      ON (TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'KICKRET', 'qual');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nfl_totals_players_sort__tmp_qual_puntret`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_sort__tmp_qual_puntret`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NFL kicking punt returns'
BEGIN

  # Average; >= 1 ret / gm
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NFL_PLAYERS_SEASON_PUNTRET_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NFL_PLAYERS_SEASON_PUNTRET_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           SUM(STAT.num) >= MIN(TEAM_GP.num) AS qual
    FROM SPORTS_NFL_PLAYERS_SEASON_PUNTRET AS STAT
    LEFT JOIN tmp_SPORTS_NFL_PLAYERS_SEASON_teamGP AS TEAM_GP
      ON (TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  CALL nfl_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'PUNTRET', 'qual');

END $$

DELIMITER ;

