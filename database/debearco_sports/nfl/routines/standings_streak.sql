#
# Current Streak
#
DROP PROCEDURE IF EXISTS `nfl_standings_streak`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_streak`()
    COMMENT 'Calculate current win/loss/tie streak'
BEGIN

  # Identify each teams last result
  DROP TEMPORARY TABLE IF EXISTS tmp_streak0;
  CREATE TEMPORARY TABLE tmp_streak0 (
    team_id VARCHAR(3),
    tmp VARCHAR(4),
    week TINYINT UNSIGNED,
    result ENUM('w','l','t'),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, MAX(CONCAT(LPAD(week, 2, '0'), ':', result)) AS tmp, NULL AS week, NULL AS result
    FROM tmp_sched
    GROUP BY team_id;

  UPDATE tmp_streak0
  SET week = SUBSTRING(tmp, 1, LOCATE(':', tmp) - 1),
      result = SUBSTRING(tmp, LOCATE(':', tmp) + 1);

  ALTER TABLE tmp_streak0 DROP COLUMN tmp;

  # Identify all instances of last result
  # Minor fudge: remove the bye week (so if you have the bye in Week 8, consider Week 9 as Week 8, 10 as 9, etc)
  DROP TEMPORARY TABLE IF EXISTS tmp_streak1;
  CREATE TEMPORARY TABLE tmp_streak1 (
    team_id VARCHAR(3),
    week TINYINT UNSIGNED,
    result ENUM('w','l','t'),
    last_week TINYINT UNSIGNED,
    PRIMARY KEY (team_id, week)
  ) ENGINE = MEMORY
    SELECT tmp_sched.team_id,
           IF(tmp_sched.week > SPORTS_NFL_SCHEDULE_BYES.week,
              tmp_sched.week - 1,
              tmp_sched.week) AS week,
           tmp_sched.result,
           IF(tmp_streak0.week > SPORTS_NFL_SCHEDULE_BYES.week,
              tmp_streak0.week - 1,
              tmp_streak0.week) AS last_week
    FROM tmp_streak0
    JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_streak0.team_id
      AND tmp_sched.result = tmp_streak0.result)
    JOIN SPORTS_NFL_SCHEDULE_BYES
      ON (SPORTS_NFL_SCHEDULE_BYES.season = tmp_sched.season
      AND SPORTS_NFL_SCHEDULE_BYES.game_type = 'regular'
      AND SPORTS_NFL_SCHEDULE_BYES.team_id = tmp_sched.team_id);

  # Flag those where there was the same result the week after (after => WHERE test = 1 OR week = v_week)
  ALTER TABLE tmp_streak1 ADD COLUMN week_test TINYINT UNSIGNED DEFAULT 0;

  CALL _duplicate_tmp_table('tmp_streak1', 'tmp_streak1_cp');

  UPDATE tmp_streak1
  LEFT JOIN tmp_streak1_cp
    ON (tmp_streak1_cp.team_id = tmp_streak1.team_id
    AND tmp_streak1_cp.week = tmp_streak1.week + 1)
  SET tmp_streak1.week_test = 1
  WHERE tmp_streak1_cp.week IS NOT NULL
  OR    tmp_streak1.week = tmp_streak1.last_week;

  # Max week where test = 0
  DROP TEMPORARY TABLE IF EXISTS tmp_streak2;
  CREATE TEMPORARY TABLE tmp_streak2 (
    team_id VARCHAR(3),
    week TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, IFNULL(MAX(IF(week_test = 0, week, NULL)), MIN(week) - 1) AS week
    FROM tmp_streak1
    GROUP BY team_id;

  # Streak Length is count() where week > ifnull(max, 1)
  DROP TEMPORARY TABLE IF EXISTS tmp_streak;
  CREATE TEMPORARY TABLE tmp_streak (
    team_id VARCHAR(3),
    streak_type ENUM('w','l','t'),
    streak_num TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id,
           tmp_streak0.result AS streak_type,
           COUNT(tmp_streak1.week) AS streak_num
    FROM tmp_teams
    JOIN tmp_streak0
      ON (tmp_streak0.team_id = tmp_teams.team_id)
    JOIN tmp_streak2
      ON (tmp_streak2.team_id = tmp_teams.team_id)
    JOIN tmp_streak1
      ON (tmp_streak1.team_id = tmp_teams.team_id
      AND tmp_streak1.week > IFNULL(tmp_streak2.week, 1))
    GROUP BY tmp_teams.team_id;

END $$

DELIMITER ;

