##
## Set up our standings
##
DROP PROCEDURE IF EXISTS `nfl_standings_pos_base`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_pos_base`()
    COMMENT 'Base NFL standing calcs'
BEGIN

  # Records and win %ages for each team
  DROP TEMPORARY TABLE IF EXISTS tmp_pos;
  CREATE TEMPORARY TABLE tmp_pos (
    team_id VARCHAR(3),
    div_id TINYINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    wins TINYINT UNSIGNED,
    loss TINYINT UNSIGNED,
    ties TINYINT UNSIGNED,
    win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id, tmp_teams.div_id, tmp_teams.conf_id,
           IFNULL(tmp_records.wins, 0) AS wins, IFNULL(tmp_records.loss, 0) AS loss, IFNULL(tmp_records.ties, 0) AS ties,
           IFNULL((tmp_records.wins + (tmp_records.ties / 2)) / (tmp_records.wins + tmp_records.loss + tmp_records.ties), 0) AS win_pct
    FROM tmp_teams
    LEFT JOIN tmp_records
      ON (tmp_records.team_id = tmp_teams.team_id);

END $$

DELIMITER ;

