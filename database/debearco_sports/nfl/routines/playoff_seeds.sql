##
## Playoff seeds
##
DROP PROCEDURE IF EXISTS `nfl_playoff_seeds`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_seeds`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate NFL playoff seeds'
BEGIN

  # Our variables
  DECLARE v_standing_week TINYINT UNSIGNED;
  DECLARE v_num_teams TINYINT UNSIGNED;
  DECLARE v_num_byes TINYINT UNSIGNED;

  # Populate
  SELECT MAX(week) INTO v_standing_week FROM SPORTS_NFL_STANDINGS WHERE season = v_season;
  IF v_season <= 2019 THEN
    # Until 2019, 6 teams qualify and 2 receive a bye
    SET v_num_teams = 6;
    SET v_num_byes = 2;
  ELSE
    # From 2020, 6 teams qualify and 2 receive a bye
    SET v_num_teams = 7;
    SET v_num_byes = 1;
  END IF;

  # Clear previous run
  DELETE FROM SPORTS_NFL_PLAYOFF_SEEDS WHERE season = v_season;

  # Calculate
  INSERT INTO SPORTS_NFL_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league, has_bye)
    SELECT SPORTS_NFL_STANDINGS.season,
           SPORTS_NFL_GROUPINGS.parent_id AS conf_id,
           SPORTS_NFL_STANDINGS.pos_conf AS seed,
           SPORTS_NFL_STANDINGS.team_id,
           SPORTS_NFL_STANDINGS.pos_div,
           SPORTS_NFL_STANDINGS.pos_conf,
           SPORTS_NFL_STANDINGS.pos_league,
           SPORTS_NFL_STANDINGS.pos_conf <= v_num_byes AS has_bye
    FROM SPORTS_NFL_STANDINGS
    JOIN SPORTS_NFL_TEAMS_GROUPINGS
      ON (SPORTS_NFL_TEAMS_GROUPINGS.team_id = SPORTS_NFL_STANDINGS.team_id
      AND v_season BETWEEN SPORTS_NFL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_NFL_TEAMS_GROUPINGS.season_to, 2099))
    JOIN SPORTS_NFL_GROUPINGS
      ON (SPORTS_NFL_GROUPINGS.grouping_id = SPORTS_NFL_TEAMS_GROUPINGS.grouping_id)
    WHERE SPORTS_NFL_STANDINGS.season = v_season
    AND   SPORTS_NFL_STANDINGS.week = v_standing_week
    AND   SPORTS_NFL_STANDINGS.pos_conf <= v_num_teams;

  # Final ordering
  ALTER TABLE SPORTS_NFL_PLAYOFF_SEEDS ORDER BY season, conf_id, seed;

END $$

DELIMITER ;
