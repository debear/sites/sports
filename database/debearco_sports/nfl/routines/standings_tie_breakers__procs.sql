##
## Individual tie-breaker tests
##
#
# Head-to-head (best won-lost-tied percentage in games between the clubs).
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_h2h`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_h2h`()
    COMMENT 'Break NFL standing ties on Head to Head records'
BEGIN

  # Run the main H2H code
  CALL nfl_standings_tie_h2h__exec();

  # Then break
  CALL nfl_standings_tie_part_break('win_pct', '>');

END $$

DELIMITER ;

#
# Head-to-head sweep.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_h2h_sweep`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_h2h_sweep`()
    COMMENT 'Break NFL standing ties on Head to Head sweep'
BEGIN

  DECLARE v_num_teams TINYINT UNSIGNED DEFAULT 0;

  # Run the main H2H code
  CALL nfl_standings_tie_h2h__exec();

  # But tidy it up - we are looking for a sweep of all teams, so if win_pct <> 1.000 or num_teams <> number of teams, then we consider it 0.000...
  SELECT COUNT(*) INTO v_num_teams FROM tmp_tiebrk_test;
  UPDATE tmp_tiebrk_test
  SET win_pct = 0
  WHERE win_pct <> 1
  OR    num_teams <> (v_num_teams - 1);

  # Then break
  CALL nfl_standings_tie_part_break('win_pct', '>');

END $$

DELIMITER ;

#
# Head-to-head, all must play each other.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_h2h_rqd`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_h2h_rqd`()
    COMMENT 'Break NFL standing ties on Head to Head records (all to play all)'
BEGIN

  DECLARE v_num_teams TINYINT UNSIGNED;
  DECLARE v_min_teams_pl TINYINT UNSIGNED;

  # Run the main H2H code
  CALL nfl_standings_tie_h2h__exec();

  # But tidy it up - we are looking for all teams to play each other at least once
  SELECT COUNT(*), MIN(num_teams) INTO v_num_teams, v_min_teams_pl FROM tmp_tiebrk_test;
  IF v_min_teams_pl < v_num_teams THEN
    UPDATE tmp_tiebrk_test
    SET win_pct = 0;
  END IF;

  # Then break
  CALL nfl_standings_tie_part_break('win_pct', '>');

END $$

DELIMITER ;

#
# Actual procedure for H2H code
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_h2h__exec`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_h2h__exec`()
    COMMENT 'Calculate NFL standing ties based on Head to Head records'
BEGIN

  # Copy tmp_tiebrk_teams as we need two copies of it
  CALL _duplicate_tmp_table('tmp_tiebrk_teams', 'tmp_tiebrk_teams_cp');
  DELETE FROM tmp_tiebrk_teams_cp WHERE new_pos <> 0;

  # Get the info in a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    num_teams TINYINT UNSIGNED,
    win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           COUNT(tmp_sched.week) AS num_gp,
           COUNT(DISTINCT tmp_sched.opp_team_id) AS num_teams,
           IF(COUNT(tmp_sched.week) > 0,
              (SUM(tmp_sched.result = 'w') + (SUM(tmp_sched.result = 't') / 2)) / COUNT(tmp_sched.week),
              0) AS win_pct
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_tiebrk_teams_cp
      ON (tmp_tiebrk_teams_cp.team_id <> tmp_tiebrk_teams.team_id)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id
      AND tmp_sched.opp_team_id = tmp_tiebrk_teams_cp.team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

END $$

DELIMITER ;

#
# Best won-lost-tied percentage in games played within the division.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_pct_div`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_pct_div`()
    COMMENT 'Break NFL standing ties on division win %age'
BEGIN

  # Get the info in a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           COUNT(tmp_sched.week) AS num_gp,
           IF(COUNT(tmp_sched.week) > 0,
              (SUM(tmp_sched.result = 'w') + (SUM(tmp_sched.result = 't') / 2)) / COUNT(tmp_sched.week),
              0) AS win_pct
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id
      AND tmp_sched.opp_is_div = 1)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  CALL nfl_standings_tie_part_break('win_pct', '>');

END $$

DELIMITER ;

#
# Best won-lost-tied percentage in common games.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_pct_common`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_pct_common`(
  v_min_gp_rqd TINYINT UNSIGNED
)
    COMMENT 'Break NFL standing ties on common game win %age'
BEGIN

  # Declare vars
  DECLARE v_min_gp TINYINT UNSIGNED;

  # Identify the common teams
  CALL nfl_standings_tie_identify_common();

  # Get the info in a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           SUM(tmp_common_opps.team_id IS NOT NULL) AS num_gp,
           IF(SUM(tmp_common_opps.team_id IS NOT NULL) > 0,
              (SUM(tmp_common_opps.team_id IS NOT NULL AND tmp_sched.result = 'w') + (SUM(tmp_common_opps.team_id IS NOT NULL AND tmp_sched.result = 't') / 2)) / SUM(tmp_common_opps.team_id IS NOT NULL),
              0) AS win_pct
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id)
    LEFT JOIN tmp_common_opps
      ON (tmp_common_opps.team_id = tmp_sched.opp_team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  # Validate against minimum
  SELECT MIN(num_gp) INTO v_min_gp FROM tmp_tiebrk_test;
  IF v_min_gp < v_min_gp_rqd THEN
    UPDATE tmp_tiebrk_test SET win_pct = 0;
  END IF;

  CALL nfl_standings_tie_part_break('win_pct', '>');

END $$

DELIMITER ;

#
# Best won-lost-tied percentage in games played within the conference.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_pct_conf`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_pct_conf`()
    COMMENT 'Break NFL standing ties on in-conf win %age'
BEGIN

  # Get the info in a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           COUNT(tmp_sched.week) AS num_gp,
           IF(COUNT(tmp_sched.week) > 0,
              (SUM(tmp_sched.result = 'w') + (SUM(tmp_sched.result = 't') / 2)) / COUNT(tmp_sched.week),
              0) AS win_pct
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id
      AND tmp_sched.opp_is_conf = 1)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  CALL nfl_standings_tie_part_break('win_pct', '>');

END $$

DELIMITER ;

#
# Strength of victory.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_str_vict`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_str_vict`()
    COMMENT 'Break NFL standing ties on Strength of Victory'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           SUM(tmp_records.wins + tmp_records.ties + tmp_records.loss) AS num_gp,
           IF(SUM(tmp_records.wins + tmp_records.ties + tmp_records.loss) > 0,
              (SUM(tmp_records.wins) + (SUM(tmp_records.ties) / 2)) / SUM(tmp_records.wins + tmp_records.ties + tmp_records.loss),
              0) AS win_pct
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id
      AND tmp_sched.result = 'w')
    LEFT JOIN tmp_records
      ON (tmp_records.team_id = tmp_sched.opp_team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  CALL nfl_standings_tie_part_break('win_pct', '>');

END $$

DELIMITER ;

#
# Strength of schedule.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_str_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_str_sched`(
  v_test_type CHAR(1)
)
    COMMENT 'Break NFL standing ties on Strength of Schedule'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    win_pct FLOAT,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           SUM(tmp_records.wins + tmp_records.ties + tmp_records.loss) AS num_gp,
           IF(SUM(tmp_records.wins + tmp_records.ties + tmp_records.loss) > 0,
              (SUM(tmp_records.wins) + (SUM(tmp_records.ties) / 2)) / SUM(tmp_records.wins + tmp_records.ties + tmp_records.loss),
              0) AS win_pct
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id)
    LEFT JOIN tmp_records
      ON (tmp_records.team_id = tmp_sched.opp_team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  CALL nfl_standings_tie_part_break('win_pct', v_test_type);

END $$

DELIMITER ;

#
# Best combined ranking among conference teams in points scored and points allowed.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_pts_rank_conf`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_pts_rank_conf`()
    COMMENT 'Break NFL standing ties on conference points ranking'
BEGIN

  # Build the scoring table
  CALL `nfl_standings_tie_scoring_ranks`(1);

  # And break on it...
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    `rank` TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           IFNULL(tmp_team_pts.pts_rank_cmb_rank, 9999) AS `rank`
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_team_pts
      ON (tmp_team_pts.team_id = tmp_tiebrk_teams.team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0;

  CALL nfl_standings_tie_part_break('rank', '>');

END $$

DELIMITER ;

#
# Best combined ranking among all teams in points scored and points allowed.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_pts_rank_all`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_pts_rank_all`()
    COMMENT 'Break NFL standing ties on league-wide points ranking'
BEGIN

  # Build the scoring table
  CALL `nfl_standings_tie_scoring_ranks`(0);

  # And break on it...
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    `rank` TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           IFNULL(tmp_team_pts.pts_rank_cmb_rank, 9999) AS `rank`
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_team_pts
      ON (tmp_team_pts.team_id = tmp_tiebrk_teams.team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0;

  CALL nfl_standings_tie_part_break('rank', '>');

END $$

DELIMITER ;

#
# Best net points in common games.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_pts_net_common`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_pts_net_common`()
    COMMENT 'Break NFL standing ties on common game net points'
BEGIN

  # Identify the common teams
  CALL nfl_standings_tie_identify_common();

  # Get the info in a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    net_pts SMALLINT SIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           SUM(tmp_common_opps.team_id IS NOT NULL) AS num_gp,
           IFNULL(SUM(CAST(IF(tmp_common_opps.team_id IS NOT NULL, tmp_sched.pts_for, 0) AS SIGNED) - CAST(IF(tmp_common_opps.team_id IS NOT NULL, tmp_sched.pts_against, 0) AS SIGNED)), 0) AS net_pts
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id)
    LEFT JOIN tmp_common_opps
      ON (tmp_common_opps.team_id = tmp_sched.opp_team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  CALL nfl_standings_tie_part_break('net_pts', '>');

END $$

DELIMITER ;

#
# Best net points in all games.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_pts_net_all`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_pts_net_all`()
    COMMENT 'Break NFL standing ties on overall net points'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    net_pts SMALLINT SIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           COUNT(tmp_sched.week) AS num_gp,
           IFNULL(SUM(CAST(tmp_sched.pts_for AS SIGNED) - CAST(tmp_sched.pts_against AS SIGNED)), 0) AS net_pts
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  CALL nfl_standings_tie_part_break('net_pts', '>');

END $$

DELIMITER ;

#
# Best net touchdowns in all games.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_td_net`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_td_net`()
    COMMENT 'Break NFL standing ties on overall net touchdowns'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    num_gp SMALLINT UNSIGNED,
    net_td SMALLINT SIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           COUNT(DISTINCT tmp_sched.week) AS num_gp,
           CAST(COUNT(td_for.play_id) - COUNT(td_against.play_id) AS SIGNED) AS net_td
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_tiebrk_teams.team_id)
    LEFT JOIN SPORTS_NFL_GAME_SCORING AS td_for
      ON (td_for.season = tmp_sched.season
      AND td_for.game_type = tmp_sched.game_type
      AND td_for.week = tmp_sched.week
      AND td_for.game_id = tmp_sched.game_id
      AND td_for.team_id = tmp_sched.team_id
      AND td_for.type LIKE 'td%')
    LEFT JOIN SPORTS_NFL_GAME_SCORING AS td_against
      ON (td_against.season = tmp_sched.season
      AND td_against.game_type = tmp_sched.game_type
      AND td_against.week = tmp_sched.week
      AND td_against.game_id = tmp_sched.game_id
      AND td_against.team_id = tmp_sched.opp_team_id
      AND td_against.type LIKE 'td%')
    WHERE tmp_tiebrk_teams.new_pos = 0
    GROUP BY tmp_tiebrk_teams.team_id;

  CALL nfl_standings_tie_part_break('net_td', '>');

END $$

DELIMITER ;

#
# Coin toss (Which we will interpret as alphabetical order on franchise...)
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_coin_toss`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_coin_toss`()
    COMMENT 'Break NFL standing ties on arbitrary coin toss (ie alphabetical)'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    franchise VARCHAR(20),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           LOWER(SPORTS_NFL_TEAMS.franchise) AS franchise
    FROM tmp_tiebrk_teams
    JOIN SPORTS_NFL_TEAMS
      ON (SPORTS_NFL_TEAMS.team_id = tmp_tiebrk_teams.team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0;

  CALL nfl_standings_tie_part_break('franchise', '<');

END $$

DELIMITER ;

#
# Division rank.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_div_pos`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_div_pos`()
    COMMENT 'Break NFL standing ties on Division Rank'
BEGIN

  # Get the info in a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    div_pos TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           tmp_pos.div_pos
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_pos
      ON (tmp_pos.team_id = tmp_tiebrk_teams.team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0;

  CALL nfl_standings_tie_part_break('div_pos', '<');

END $$

DELIMITER ;

#
# Conference rank.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_conf_pos`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_conf_pos`()
    COMMENT 'Break NFL standing ties on Conference Rank'
BEGIN

  # Get the info in a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_test;
  CREATE TEMPORARY TABLE tmp_tiebrk_test (
    team_id VARCHAR(3),
    conf_pos TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_tiebrk_teams.team_id,
           tmp_pos.conf_pos
    FROM tmp_tiebrk_teams
    LEFT JOIN tmp_pos
      ON (tmp_pos.team_id = tmp_tiebrk_teams.team_id)
    WHERE tmp_tiebrk_teams.new_pos = 0;

  CALL nfl_standings_tie_part_break('conf_pos', '<');

END $$

DELIMITER ;

##################################################################################
##################################################################################

#
# Identify the common opponents of teams in tmp_tiebrk_teams
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_identify_common`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_identify_common`()
    COMMENT 'Identify common opponents'
BEGIN

  DECLARE v_num_tied TINYINT UNSIGNED;

  # Need to know how many teams we have tied
  SELECT COUNT(team_id) INTO v_num_tied FROM tmp_tiebrk_teams;

  # Get the list
  DROP TEMPORARY TABLE IF EXISTS tmp_common_opps;
  CREATE TEMPORARY TABLE tmp_common_opps (
    team_id VARCHAR(3),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_sched.team_id
    FROM tmp_sched
    LEFT JOIN tmp_tiebrk_teams
      ON (tmp_tiebrk_teams.team_id = tmp_sched.opp_team_id
      AND tmp_tiebrk_teams.new_pos = 0)
    GROUP BY tmp_sched.team_id
    HAVING COUNT(DISTINCT tmp_tiebrk_teams.team_id) = v_num_tied;

END $$

DELIMITER ;

#
# Identify scoring ranks
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_scoring_ranks`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_scoring_ranks`(
  v_conf_only TINYINT UNSIGNED
)
    COMMENT 'Build a list of team scoring ranks'
BEGIN

  # Get the raw point totals
  DROP TEMPORARY TABLE IF EXISTS tmp_team_pts;
  CREATE TEMPORARY TABLE tmp_team_pts (
    team_id VARCHAR(3),
    div_id TINYINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    pts_for SMALLINT UNSIGNED,
    pts_against SMALLINT UNSIGNED,
    PRIMARY KEY (team_id),
    INDEX by_div (div_id),
    INDEX by_conf (conf_id),
    INDEX pts_for (pts_for),
    INDEX pts_against (pts_against)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id, tmp_teams.div_id, tmp_teams.conf_id,
           IFNULL(SUM(tmp_sched.pts_for), 0) AS pts_for,
           IFNULL(SUM(tmp_sched.pts_against), 0) AS pts_against
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND ((v_conf_only = 1 AND tmp_sched.opp_is_conf = 1) OR (1 = 1)))
    GROUP BY tmp_teams.team_id;

  # Establish pts_for rank
  CALL _duplicate_tmp_table('tmp_team_pts', 'tmp_team_pts_cpA');
  CALL _duplicate_tmp_table('tmp_team_pts', 'tmp_team_pts_cpB');

  ALTER TABLE tmp_team_pts ADD COLUMN pts_for_rank TINYINT UNSIGNED AFTER pts_for;
  INSERT INTO tmp_team_pts (team_id, div_id, conf_id, pts_for, pts_for_rank, pts_against)
    SELECT tmp_team_pts_cpA.team_id, tmp_team_pts_cpA.div_id, tmp_team_pts_cpA.conf_id,
           tmp_team_pts_cpA.pts_for, COUNT(tmp_team_pts_cpB.team_id) + 1 AS pts_for_rank,
           tmp_team_pts_cpA.pts_against
    FROM tmp_team_pts_cpA
    LEFT JOIN tmp_team_pts_cpB
      ON (tmp_team_pts_cpB.pts_for > tmp_team_pts_cpA.pts_for)
    GROUP BY tmp_team_pts_cpA.team_id
  ON DUPLICATE KEY UPDATE pts_for_rank = VALUES(pts_for_rank);

  # And the same for points against
  CALL _duplicate_tmp_table('tmp_team_pts', 'tmp_team_pts_cpA');
  CALL _duplicate_tmp_table('tmp_team_pts', 'tmp_team_pts_cpB');

  ALTER TABLE tmp_team_pts ADD COLUMN pts_against_rank TINYINT UNSIGNED AFTER pts_against;
  INSERT INTO tmp_team_pts (team_id, div_id, conf_id, pts_for, pts_for_rank, pts_against, pts_against_rank)
    SELECT tmp_team_pts_cpA.*, COUNT(tmp_team_pts_cpB.team_id) + 1 AS pts_against_rank
    FROM tmp_team_pts_cpA
    LEFT JOIN tmp_team_pts_cpB
      ON (tmp_team_pts_cpB.pts_against < tmp_team_pts_cpA.pts_against)
    GROUP BY tmp_team_pts_cpA.team_id
  ON DUPLICATE KEY UPDATE pts_against_rank = VALUES(pts_against_rank);

  # And finally add a combined column, and rank that
  CALL _duplicate_tmp_table('tmp_team_pts', 'tmp_team_pts_cpA');
  CALL _duplicate_tmp_table('tmp_team_pts', 'tmp_team_pts_cpB');

  ALTER TABLE tmp_team_pts ADD COLUMN pts_rank_cmb TINYINT UNSIGNED,
                           ADD COLUMN pts_rank_cmb_rank TINYINT UNSIGNED;
  INSERT INTO tmp_team_pts
    SELECT tmp_team_pts_cpA.*,
           tmp_team_pts_cpA.pts_for_rank + tmp_team_pts_cpA.pts_against_rank AS pts_rank_cmb,
           COUNT(tmp_team_pts_cpB.team_id) + 1 AS pts_rank_cmb_rank
    FROM tmp_team_pts_cpA
    LEFT JOIN tmp_team_pts_cpB
      ON ((tmp_team_pts_cpB.pts_for_rank + tmp_team_pts_cpB.pts_against_rank) < (tmp_team_pts_cpA.pts_for_rank + tmp_team_pts_cpA.pts_against_rank))
    GROUP BY tmp_team_pts_cpA.team_id
  ON DUPLICATE KEY UPDATE pts_rank_cmb = VALUES(pts_rank_cmb),
                          pts_rank_cmb_rank = VALUES(pts_rank_cmb_rank);

END $$

DELIMITER ;


#
# Break an individual test result.
#
DROP PROCEDURE IF EXISTS `nfl_standings_tie_part_break`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_tie_part_break`(
  v_col VARCHAR(30),
  v_test VARCHAR(15)
)
    COMMENT 'Break test for NFL standing ties'
BEGIN

  # Remove the tested teams that are not in contention
  DELETE tmp_tiebrk_test.*
  FROM tmp_tiebrk_teams
  JOIN tmp_tiebrk_test
    ON (tmp_tiebrk_test.team_id  = tmp_tiebrk_teams.team_id)
  WHERE tmp_tiebrk_teams.new_pos <> 0;

  # Duplicate our base and test tables
  CALL _duplicate_tmp_table('tmp_tiebrk_teams', 'tmp_tiebrk_teams_cp');
  CALL _duplicate_tmp_table('tmp_tiebrk_test', 'tmp_tiebrk_test_cp');

  # Truncate the original table, as we are going to repopulate it
  TRUNCATE TABLE tmp_tiebrk_teams;

  # Now try and break...
  CALL _exec(CONCAT('INSERT INTO tmp_tiebrk_teams (team_id, base_pos, new_pos, new_pos_tied)
    SELECT tmp_tiebrk_teams_cp.team_id,
           tmp_tiebrk_teams_cp.base_pos,
           COUNT(tmp_tiebrk_test_cp.team_id) AS new_pos,
           0 AS new_pos_tied
    FROM tmp_tiebrk_teams_cp
    JOIN tmp_tiebrk_test
      ON (tmp_tiebrk_test.team_id = tmp_tiebrk_teams_cp.team_id)
    LEFT JOIN tmp_tiebrk_test_cp
      ON (tmp_tiebrk_test_cp.team_id <> tmp_tiebrk_test.team_id
      AND tmp_tiebrk_test_cp.', v_col , ' ', v_test , ' tmp_tiebrk_test.', v_col , ')
    WHERE tmp_tiebrk_teams_cp.new_pos = 0
    GROUP BY tmp_tiebrk_teams_cp.team_id;'));

  # Identify any remaining ties
  CALL _duplicate_tmp_table('tmp_tiebrk_teams', 'tmp_tiebrk_teams_cpA');
  CALL _duplicate_tmp_table('tmp_tiebrk_teams', 'tmp_tiebrk_teams_cpB');

  INSERT INTO tmp_tiebrk_teams (team_id, base_pos, new_pos, new_pos_tied)
    SELECT tmp_tiebrk_teams_cpA.team_id,
           tmp_tiebrk_teams_cpA.base_pos,
           tmp_tiebrk_teams_cpA.new_pos,
           1 AS new_pos_tied
    FROM tmp_tiebrk_teams_cpA
    JOIN tmp_tiebrk_teams_cpB
      ON (tmp_tiebrk_teams_cpB.team_id <> tmp_tiebrk_teams_cpA.team_id
      AND tmp_tiebrk_teams_cpB.base_pos = tmp_tiebrk_teams_cpA.base_pos
      AND tmp_tiebrk_teams_cpB.new_pos = tmp_tiebrk_teams_cpA.new_pos)
    GROUP BY tmp_tiebrk_teams_cpA.team_id
  ON DUPLICATE KEY UPDATE new_pos_tied = VALUES(new_pos_tied);

END $$

DELIMITER ;

