##
## Break ties within the divisions
##
# http://www.nfl.com/standings/tiebreakingprocedures
DROP PROCEDURE IF EXISTS `nfl_standings_break_div`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_div`(
  v_div_id TINYINT UNSIGNED,
  v_div_pos TINYINT UNSIGNED
)
    COMMENT 'Break an individual NFL division ties'
proc_div: BEGIN

  # Declare vars
  DECLARE v_test_success TINYINT UNSIGNED;

  # Which teams are involved?
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_teams;
  CREATE TEMPORARY TABLE tmp_tiebrk_teams (
    team_id VARCHAR(3),
    base_pos TINYINT UNSIGNED,
    new_pos TINYINT UNSIGNED,
    new_pos_tied TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, group_pos AS base_pos, 0 AS new_pos, 1 AS new_pos_tied
    FROM tmp_tiebrk_list
    WHERE group_id = v_div_id
    AND   group_pos = v_div_pos;

  ## Now run our tests...

  #
  # Head-to-head (best won-lost-tied percentage in games between the clubs).
  #
  CALL nfl_standings_tie_h2h();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best won-lost-tied percentage in games played within the division.
  #
  CALL nfl_standings_tie_pct_div();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best won-lost-tied percentage in common games.
  #  - No minimum, unlike conference tie break
  #
  CALL nfl_standings_tie_pct_common(0);
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best won-lost-tied percentage in games played within the conference.
  #
  CALL nfl_standings_tie_pct_conf();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Strength of victory.
  #
  CALL nfl_standings_tie_str_vict();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Strength of schedule.
  #
  CALL nfl_standings_tie_str_sched('>');
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best combined ranking among conference teams in points scored and points allowed.
  #
  CALL nfl_standings_tie_pts_rank_conf();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best combined ranking among all teams in points scored and points allowed.
  #
  CALL nfl_standings_tie_pts_rank_all();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best net points in common games.
  #
  CALL nfl_standings_tie_pts_net_common();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best net points in all games.
  #
  CALL nfl_standings_tie_pts_net_all();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Best net touchdowns in all games.
  #
  CALL nfl_standings_tie_td_net();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

  #
  # Coin toss (Which we will interpret as alphabetical order on franchise...)
  #
  CALL nfl_standings_tie_coin_toss();
  CALL nfl_standings_break_group_test('div', v_div_id, v_div_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_div;
  END IF;

END $$

DELIMITER ;

