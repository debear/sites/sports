##
## Break ties within the league as a whole
##
# Not officially defined, as this is separate from draft order
DROP PROCEDURE IF EXISTS `nfl_standings_break_league`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_league`(
  v_league_id TINYINT UNSIGNED,
  v_league_pos TINYINT UNSIGNED
)
    COMMENT 'Break an individual NFL league-wide tie'
proc_league: BEGIN

  ##
  ## Concept copied from Conference tie-breaker
  ##

  # Declare vars
  DECLARE v_num_teams TINYINT UNSIGNED;
  DECLARE v_num_conf TINYINT UNSIGNED;
  DECLARE v_test_success TINYINT UNSIGNED;

  # Which teams are involved?
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_teams;
  CREATE TEMPORARY TABLE tmp_tiebrk_teams (
    team_id VARCHAR(3),
    base_pos TINYINT UNSIGNED,
    new_pos TINYINT UNSIGNED,
    new_pos_tied TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, group_pos AS base_pos, 0 AS new_pos, 1 AS new_pos_tied
    FROM tmp_tiebrk_list
    WHERE group_id = v_league_id
    AND   group_pos = v_league_pos;

  # League ties depend on how many leagues are involved:
  #  - All in one conference, just uses conference rank
  #  - Across multiple, in which case only one team progresses from each conference (the highest conf ranked)
  SELECT COUNT(tmp_teams.team_id), COUNT(DISTINCT tmp_teams.conf_id)
         INTO v_num_teams, v_num_conf
  FROM tmp_tiebrk_teams
  JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_tiebrk_teams.team_id);

  IF v_num_conf = 1 THEN
    # Only one conf, use the conf rank as the only test
    CALL nfl_standings_tie_conf_pos();
    LEAVE proc_league;

  ELSEIF v_num_conf <> v_num_teams THEN
    # At least one conference represented multiple times, so 'drop' the lowest ranked team(s)
    ALTER TABLE tmp_tiebrk_teams ADD COLUMN conf_id TINYINT UNSIGNED,
                                 ADD COLUMN conf_pos TINYINT UNSIGNED;

    UPDATE tmp_tiebrk_teams
    JOIN tmp_pos
      ON (tmp_pos.team_id = tmp_tiebrk_teams.team_id)
    SET tmp_tiebrk_teams.conf_id = tmp_pos.conf_id,
        tmp_tiebrk_teams.conf_pos = tmp_pos.conf_pos;

    DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_teams_conf;
    CREATE TEMPORARY TABLE tmp_tiebrk_teams_conf (
      conf_id TINYINT UNSIGNED,
      best_pos TINYINT UNSIGNED,
      PRIMARY KEY (conf_id)
    ) ENGINE = MEMORY
      SELECT conf_id, MIN(conf_pos) AS best_pos
      FROM tmp_tiebrk_teams
      GROUP BY conf_id;

    UPDATE tmp_tiebrk_teams
    LEFT JOIN tmp_tiebrk_teams_conf
      ON (tmp_tiebrk_teams_conf.conf_id = tmp_tiebrk_teams.conf_id
      AND tmp_tiebrk_teams_conf.best_pos = tmp_tiebrk_teams.conf_pos)
    SET new_pos = v_num_conf
    WHERE tmp_tiebrk_teams_conf.conf_id IS NULL;

    ALTER TABLE tmp_tiebrk_teams DROP COLUMN conf_id, DROP COLUMN conf_pos;

  END IF;

  # Reset our num_teams counter
  SELECT COUNT(*) INTO v_num_teams FROM tmp_tiebrk_teams WHERE new_pos = 0;

  ## Now run our tests...

  #
  # 2 Clubs: Head-to-head, if applicable.
  # 3 Clubs: Head-to-head sweep.
  #
  IF v_num_teams = 2 THEN
    CALL nfl_standings_tie_h2h();
  ELSE
    CALL nfl_standings_tie_h2h_sweep();
  END IF;
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

  #
  # Best won-lost-tied percentage in games played within the conference.
  #
  ## Not relevant in a league-wide tie-break...

  #
  # Best won-lost-tied percentage in common games, minimum of four.
  #
  CALL nfl_standings_tie_pct_common(4);
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

  #
  # Strength of victory.
  #
  CALL nfl_standings_tie_str_vict();
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

  #
  # Strength of schedule.
  #
  CALL nfl_standings_tie_str_sched('>');
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

  #
  # Best combined ranking among conference teams in points scored and points allowed.
  #
  ## Not relevant in a league-wide tie-break...

  #
  # Best combined ranking among all teams in points scored and points allowed.
  #
  CALL nfl_standings_tie_pts_rank_all();
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

  #
  # Best net points in conference games.
  #
  ## Not relevant in a league-wide tie-break...

  #
  # Best net points in all games.
  #
  CALL nfl_standings_tie_pts_net_all();
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

  #
  # Best net touchdowns in all games.
  #
  CALL nfl_standings_tie_td_net();
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

  #
  # Coin toss (Which we will interpret as alphabetical order on franchise...)
  #
  CALL nfl_standings_tie_coin_toss();
  CALL nfl_standings_break_group_test('league', v_league_id, v_league_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_league;
  END IF;

END $$

DELIMITER ;

