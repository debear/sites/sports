##
## Player season totals
##
DROP PROCEDURE IF EXISTS `nfl_totals_players`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for NFL players'
BEGIN

  # Games Played (requires temp table for ordering)
  DROP TEMPORARY TABLE IF EXISTS tmp_player_teams;
  CREATE TEMPORARY TABLE tmp_player_teams (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    team_id VARCHAR(3),
    gp TINYINT UNSIGNED,
    gs TINYINT UNSIGNED,
    team_order TINYINT UNSIGNED,
    first_week TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id, team_id),
    INDEX by_team (season, season_type, player_id, team_id, first_week)
  ) ENGINE = MEMORY
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(status IN ('starter', 'substitute')) AS gp,
           SUM(status = 'starter') AS gs,
           0 AS team_order,
           MIN(week) AS first_week
    FROM SPORTS_NFL_GAME_LINEUP
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id;

  # And order the teams
  CALL _duplicate_tmp_table('tmp_player_teams', 'tmp_player_teams_cpA');
  CALL _duplicate_tmp_table('tmp_player_teams', 'tmp_player_teams_cpB');

  INSERT INTO tmp_player_teams (season, season_type, player_id, team_id, team_order)
    SELECT tmp_player_teams_cpA.season, tmp_player_teams_cpA.season_type, tmp_player_teams_cpA.player_id, tmp_player_teams_cpA.team_id,
           COUNT(tmp_player_teams_cpB.team_id) AS team_order
    FROM tmp_player_teams_cpA
    LEFT JOIN tmp_player_teams_cpB
      ON (tmp_player_teams_cpB.season = tmp_player_teams_cpA.season
      AND tmp_player_teams_cpB.season_type = tmp_player_teams_cpA.season_type
      AND tmp_player_teams_cpB.player_id = tmp_player_teams_cpA.player_id
      AND tmp_player_teams_cpB.team_id <> tmp_player_teams_cpA.team_id
      AND tmp_player_teams_cpB.first_week < tmp_player_teams_cpA.first_week)
    GROUP BY tmp_player_teams_cpA.season, tmp_player_teams_cpA.season_type, tmp_player_teams_cpA.player_id, tmp_player_teams_cpA.team_id
  ON DUPLICATE KEY UPDATE team_order = VALUES(team_order);

  ALTER TABLE tmp_player_teams DROP COLUMN first_week;

  # Finally store
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON (season, season_type, player_id, team_id, gp, gs, team_order)
    SELECT * FROM tmp_player_teams
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          gs = VALUES(gs),
                          team_order = VALUES(team_order);

  # Passing
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_PASSING (season, season_type, player_id, team_id, atts, cmp, yards, `long`, td, `int`, sacked, sack_yards, rating)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(atts), SUM(cmp), SUM(yards), MAX(`long`), SUM(td), SUM(`int`), SUM(sacked), SUM(sack_yards),
           nfl_passer_rating(SUM(atts), SUM(cmp), SUM(yards), SUM(td), SUM(`int`))
    FROM SPORTS_NFL_PLAYERS_GAME_PASSING
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE atts = VALUES(atts),
                          cmp = VALUES(cmp),
                          yards = VALUES(yards),
                          `long` = VALUES(`long`),
                          td = VALUES(td),
                          `int` = VALUES(`int`),
                          sacked = VALUES(sacked),
                          sack_yards = VALUES(sack_yards),
                          rating = VALUES(rating);

  # Rushing
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_RUSHING (season, season_type, player_id, team_id, atts, yards, `long`, td)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(atts), SUM(yards), MAX(`long`), SUM(td)
    FROM SPORTS_NFL_PLAYERS_GAME_RUSHING
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE atts = VALUES(atts),
                          yards = VALUES(yards),
                          `long` = VALUES(`long`),
                          td = VALUES(td);

  # Receiving
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_RECEIVING (season, season_type, player_id, team_id, recept, yards, `long`, td, targets)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(recept), SUM(yards), MAX(`long`), SUM(td), SUM(targets)
    FROM SPORTS_NFL_PLAYERS_GAME_RECEIVING
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE recept = VALUES(recept),
                          yards = VALUES(yards),
                          `long` = VALUES(`long`),
                          td = VALUES(td),
                          targets = VALUES(targets);

  # Fumbles
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_FUMBLES (season, season_type, player_id, team_id, num_fumbled, num_lost, num_forced, rec_own, rec_opp, rec_own_td, rec_opp_td, oob)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(num_fumbled), SUM(num_lost), SUM(num_forced), SUM(rec_own), SUM(rec_opp), SUM(rec_own_td), SUM(rec_opp_td), SUM(oob)
    FROM SPORTS_NFL_PLAYERS_GAME_FUMBLES
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE num_fumbled = VALUES(num_fumbled),
                          num_lost = VALUES(num_lost),
                          num_forced = VALUES(num_forced),
                          rec_own = VALUES(rec_own),
                          rec_opp = VALUES(rec_opp),
                          rec_own_td = VALUES(rec_own_td),
                          rec_opp_td = VALUES(rec_opp_td),
                          oob = VALUES(oob);

  # Tackles
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_TACKLES (season, season_type, player_id, team_id, tckl, asst, sacks, sacks_yards, tfl, qb_hit, kick_block)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(tckl), SUM(asst), SUM(sacks), SUM(sacks_yards), SUM(tfl), SUM(qb_hit), SUM(kick_block)
    FROM SPORTS_NFL_PLAYERS_GAME_TACKLES
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE tckl = VALUES(tckl),
                          asst = VALUES(asst),
                          sacks = VALUES(sacks),
                          sacks_yards = VALUES(sacks_yards),
                          tfl = VALUES(tfl),
                          qb_hit = VALUES(qb_hit),
                          kick_block = VALUES(kick_block);

  # Pass Defense
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_PASSDEF (season, season_type, player_id, team_id, pd, `int`, int_yards, int_td)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(pd), SUM(`int`), SUM(int_yards), SUM(int_td)
    FROM SPORTS_NFL_PLAYERS_GAME_PASSDEF
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE pd = VALUES(pd),
                          `int` = VALUES(`int`),
                          int_yards = VALUES(int_yards),
                          int_td = VALUES(int_td);

  # Kicking
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_KICKING (season, season_type, player_id, team_id, fg_att, fg_made, fg_u20, fg_u30, fg_u40, fg_u50, fg_o50, xp_att, xp_made)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(fg_att), SUM(fg_made), SUM(fg_u20), SUM(fg_u30), SUM(fg_u40), SUM(fg_u50), SUM(fg_o50), SUM(xp_att), SUM(xp_made)
    FROM SPORTS_NFL_PLAYERS_GAME_KICKING
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE fg_att = VALUES(fg_att),
                          fg_made = VALUES(fg_made),
                          fg_u20 = VALUES(fg_u20),
                          fg_u30 = VALUES(fg_u30),
                          fg_u40 = VALUES(fg_u40),
                          fg_u50 = VALUES(fg_u50),
                          fg_o50 = VALUES(fg_o50),
                          xp_att = VALUES(xp_att),
                          xp_made = VALUES(xp_made);

  # Punts
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_PUNTS (season, season_type, player_id, team_id, num, yards, net, `long`, tb, inside20)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(num), SUM(yards), SUM(net), MAX(`long`), SUM(tb), SUM(inside20)
    FROM SPORTS_NFL_PLAYERS_GAME_PUNTS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE num = VALUES(num),
                          yards = VALUES(yards),
                          net = VALUES(net),
                          `long` = VALUES(`long`),
                          tb = VALUES(tb),
                          inside20 = VALUES(inside20);

  # Kick Returns
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_KICKRET (season, season_type, player_id, team_id, num, yards, `long`, fair_catch, td)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(num), SUM(yards), MAX(`long`), SUM(fair_catch), SUM(td)
    FROM SPORTS_NFL_PLAYERS_GAME_KICKRET
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE num = VALUES(num),
                          yards = VALUES(yards),
                          `long` = VALUES(`long`),
                          fair_catch = VALUES(fair_catch),
                          td = VALUES(td);

  # Punt Returns
  INSERT INTO SPORTS_NFL_PLAYERS_SEASON_PUNTRET (season, season_type, player_id, team_id, num, yards, `long`, fair_catch, td)
    SELECT season, game_type AS season_type, player_id, team_id,
           SUM(num), SUM(yards), MAX(`long`), SUM(fair_catch), SUM(td)
    FROM SPORTS_NFL_PLAYERS_GAME_PUNTRET
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, player_id, team_id
  ON DUPLICATE KEY UPDATE num = VALUES(num),
                          yards = VALUES(yards),
                          `long` = VALUES(`long`),
                          fair_catch = VALUES(fair_catch),
                          td = VALUES(td);

END $$

DELIMITER ;

##
## Player season total table maintenance
##
DROP PROCEDURE IF EXISTS `nfl_totals_players_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_players_order`()
    COMMENT 'Order the NFL player total tables'
BEGIN

  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON ORDER BY season, season_type, player_id, team_order;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PASSING ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_RUSHING ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_RECEIVING ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_FUMBLES ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_TACKLES ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PASSDEF ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_KICKING ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PUNTS ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_KICKRET ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NFL_PLAYERS_SEASON_PUNTRET ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

