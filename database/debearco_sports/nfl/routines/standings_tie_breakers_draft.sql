##
## Break ties within the draft order
##
# http://www.nfl.com/standings/tiebreakingprocedures
DROP PROCEDURE IF EXISTS `nfl_standings_break_draft`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_break_draft`(
  v_draft_id TINYINT UNSIGNED,
  v_draft_pos TINYINT UNSIGNED
)
    COMMENT 'Break NFL draft order ties'
proc_draft: BEGIN

  # Declare vars
  DECLARE v_num_teams TINYINT UNSIGNED;
  DECLARE v_num_divconf TINYINT UNSIGNED;
  DECLARE v_test_success TINYINT UNSIGNED;

  # Which teams are involved?
  DROP TEMPORARY TABLE IF EXISTS tmp_tiebrk_teams;
  CREATE TEMPORARY TABLE tmp_tiebrk_teams (
    team_id VARCHAR(3),
    base_pos TINYINT UNSIGNED,
    new_pos TINYINT UNSIGNED,
    new_pos_tied TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, group_pos AS base_pos, 0 AS new_pos, 1 AS new_pos_tied
    FROM tmp_tiebrk_list
    WHERE group_id = v_draft_id
    AND   group_pos = v_draft_pos;

  #
  # Strength of schedule.
  #
  CALL nfl_standings_tie_str_sched('<');
  CALL nfl_standings_break_group_test('draft', v_draft_id, v_draft_pos, v_test_success);
  IF v_test_success THEN
    LEAVE proc_draft;
  END IF;

  #
  # Division Rank (If all in same division).
  #
  SELECT COUNT(tmp_teams.team_id), COUNT(DISTINCT tmp_teams.div_id)
         INTO v_num_teams, v_num_divconf
  FROM tmp_tiebrk_teams
  JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_tiebrk_teams.team_id);

  IF v_num_divconf = 1 THEN
    CALL nfl_standings_tie_div_pos();
    LEAVE proc_draft;
  END IF;

  #
  # Conference Rank (If all in same conference).
  #
  SELECT COUNT(tmp_teams.team_id), COUNT(DISTINCT tmp_teams.conf_id)
         INTO v_num_teams, v_num_divconf
  FROM tmp_tiebrk_teams
  JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_tiebrk_teams.team_id);

  IF v_num_divconf = 1 THEN
    CALL nfl_standings_tie_conf_pos();
    LEAVE proc_draft;
  END IF;

  #
  # Unable to break algorithmically, so would be broken by arbitrary coin toss.
  # Break alphabetically on franchise (our equivalent of coin toss) but record these teams as tied
  #
  CALL nfl_standings_tie_coin_toss();
  INSERT IGNORE INTO tmp_draft_coin_toss (team_id, draft_pos)
    SELECT team_id, base_pos + new_pos
    FROM tmp_tiebrk_teams;

END $$

DELIMITER ;

