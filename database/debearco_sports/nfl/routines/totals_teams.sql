##
## Team season totals
##
DROP PROCEDURE IF EXISTS `nfl_totals_teams`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for NFL teams'
BEGIN

  #
  # From GAME_STATS_*
  #

  # Downs
  INSERT INTO SPORTS_NFL_TEAMS_STATS_DOWNS (season, season_type, team_id, num_1st_rush, num_1st_pass, num_1st_pen, num_3rd, num_3rd_cnv, num_4th, num_4th_cnv)
    SELECT season, game_type AS season_type, team_id,
           SUM(num_1st_rush), SUM(num_1st_pass), SUM(num_1st_pen), SUM(num_3rd), SUM(num_3rd_cnv), SUM(num_4th), SUM(num_4th_cnv)
    FROM SPORTS_NFL_GAME_STATS_DOWNS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE num_1st_rush = VALUES(num_1st_rush),
                          num_1st_pass = VALUES(num_1st_pass),
                          num_1st_pen = VALUES(num_1st_pen),
                          num_3rd = VALUES(num_3rd),
                          num_3rd_cnv = VALUES(num_3rd_cnv),
                          num_4th = VALUES(num_4th),
                          num_4th_cnv = VALUES(num_4th_cnv);

  # Number of plays
  INSERT INTO SPORTS_NFL_TEAMS_STATS_PLAYS (season, season_type, team_id, total, num_pass_att, num_pass_cmp, num_rush, num_rush_tfl)
    SELECT season, game_type AS season_type, team_id,
           SUM(total), SUM(num_pass_att), SUM(num_pass_cmp), SUM(num_rush), SUM(num_rush_tfl)
    FROM SPORTS_NFL_GAME_STATS_PLAYS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE total = VALUES(total),
                          num_pass_att = VALUES(num_pass_att),
                          num_pass_cmp = VALUES(num_pass_cmp),
                          num_rush = VALUES(num_rush),
                          num_rush_tfl = VALUES(num_rush_tfl);

  # Yards gained
  INSERT INTO SPORTS_NFL_TEAMS_STATS_YARDS (season, season_type, team_id, yards_pass, yards_rush, yards_rush_tfl)
    SELECT season, game_type AS season_type, team_id,
           SUM(yards_pass), SUM(yards_rush), SUM(yards_rush_tfl)
    FROM SPORTS_NFL_GAME_STATS_YARDS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE yards_pass = VALUES(yards_pass),
                          yards_rush = VALUES(yards_rush),
                          yards_rush_tfl = VALUES(yards_rush_tfl);

  # Kickoffs
  INSERT INTO SPORTS_NFL_TEAMS_STATS_KICKOFFS (season, season_type, team_id, num, end_zone, touchback)
    SELECT season, game_type AS season_type, team_id,
           SUM(num), SUM(end_zone), SUM(touchback)
    FROM SPORTS_NFL_GAME_STATS_KICKOFFS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE num = VALUES(num),
                          end_zone = VALUES(end_zone),
                          touchback = VALUES(touchback);

  # Punts
  INSERT INTO SPORTS_NFL_TEAMS_STATS_PUNTS (season, season_type, team_id, num, yards, net, blocked)
    SELECT season, game_type AS season_type, team_id,
           SUM(num), SUM(yards), SUM(net), SUM(blocked)
    FROM SPORTS_NFL_GAME_STATS_PUNTS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE num = VALUES(num),
                          yards = VALUES(yards),
                          net = VALUES(net),
                          blocked = VALUES(blocked);

  # Returns
  INSERT INTO SPORTS_NFL_TEAMS_STATS_RETURNS (season, season_type, team_id, ko_num, ko_yards, punt_num, punt_yards, int_num, int_yards)
    SELECT season, game_type AS season_type, team_id,
           SUM(ko_num), SUM(ko_yards), SUM(punt_num), SUM(punt_yards), SUM(int_num), SUM(int_yards)
    FROM SPORTS_NFL_GAME_STATS_RETURNS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE ko_num = VALUES(ko_num),
                          ko_yards = VALUES(ko_yards),
                          punt_num = VALUES(punt_num),
                          punt_yards = VALUES(punt_yards),
                          int_num = VALUES(int_num),
                          int_yards = VALUES(int_yards);

  # Touchdowns
  INSERT INTO SPORTS_NFL_TEAMS_STATS_TDS (season, season_type, team_id, num_pass, num_rush, num_int, num_fumbles, num_fumbles_own, num_fumbles_opp, num_ko, num_punt, num_other, safties)
    SELECT season, game_type AS season_type, team_id,
           SUM(num_pass), SUM(num_rush), SUM(num_int), SUM(num_fumbles), SUM(num_fumbles_own), SUM(num_fumbles_opp), SUM(num_ko), SUM(num_punt), SUM(num_other), SUM(safties)
    FROM SPORTS_NFL_GAME_STATS_TDS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE num_pass = VALUES(num_pass),
                          num_rush = VALUES(num_rush),
                          num_int = VALUES(num_int),
                          num_fumbles = VALUES(num_fumbles),
                          num_fumbles_own = VALUES(num_fumbles_own),
                          num_fumbles_opp = VALUES(num_fumbles_opp),
                          num_ko = VALUES(num_ko),
                          num_punt = VALUES(num_punt),
                          num_other = VALUES(num_other),
                          safties = VALUES(safties);

  # Two point conversion attempts
  INSERT INTO SPORTS_NFL_TEAMS_STATS_2PT (season, season_type, team_id, pass_att, pass_made, rush_att, rush_made)
    SELECT season, game_type AS season_type, team_id,
           SUM(pass_att), SUM(pass_made), SUM(rush_att), SUM(rush_made)
    FROM SPORTS_NFL_GAME_STATS_2PT
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE pass_att = VALUES(pass_att),
                          pass_made = VALUES(pass_made),
                          rush_att = VALUES(rush_att),
                          rush_made = VALUES(rush_made);

  # Kicking
  INSERT INTO SPORTS_NFL_TEAMS_STATS_KICKS (season, season_type, team_id, fg_att, fg_made, fg_blocked, pat_att, pat_made, pat_blocked)
    SELECT season, game_type AS season_type, team_id,
           SUM(fg_att), SUM(fg_made), SUM(fg_blocked), SUM(pat_att), SUM(pat_made), SUM(pat_blocked)
    FROM SPORTS_NFL_GAME_STATS_KICKS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE fg_att = VALUES(fg_att),
                          fg_made = VALUES(fg_made),
                          fg_blocked = VALUES(fg_blocked),
                          pat_att = VALUES(pat_att),
                          pat_made = VALUES(pat_made),
                          pat_blocked = VALUES(pat_blocked);

  # Turnovers
  INSERT INTO SPORTS_NFL_TEAMS_STATS_TURNOVERS (season, season_type, team_id, int_num, fumb_num, fumb_lost)
    SELECT season, game_type AS season_type, team_id,
           SUM(int_num), SUM(fumb_num), SUM(fumb_lost)
    FROM SPORTS_NFL_GAME_STATS_TURNOVERS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE int_num = VALUES(int_num),
                          fumb_num = VALUES(fumb_num),
                          fumb_lost = VALUES(fumb_lost);

  # Other
  INSERT INTO SPORTS_NFL_TEAMS_STATS_MISC (season, season_type, team_id, time_of_poss, pens_num, pens_yards, rz_num, rz_cnv, goal_num, goal_cnv)
    SELECT season, game_type AS season_type, team_id,
           SEC_TO_TIME(AVG(TIME_TO_SEC(time_of_poss))), SUM(pens_num), SUM(pens_yards), SUM(rz_num), SUM(rz_cnv), SUM(goal_num), SUM(goal_cnv)
    FROM SPORTS_NFL_GAME_STATS_MISC
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE time_of_poss = VALUES(time_of_poss),
                          pens_num = VALUES(pens_num),
                          pens_yards = VALUES(pens_yards),
                          rz_num = VALUES(rz_num),
                          rz_cnv = VALUES(rz_cnv),
                          goal_num = VALUES(goal_num),
                          goal_cnv = VALUES(goal_cnv);

  #
  # From PLAYERS_SEASON_*
  #

  # Misc Offense
  INSERT INTO SPORTS_NFL_TEAMS_STATS_PLAYS (season, season_type, team_id, num_pass_sacked)
    SELECT season, season_type, team_id,
           SUM(sacked)
    FROM SPORTS_NFL_PLAYERS_SEASON_PASSING
    WHERE season = v_season
    AND   season_type = v_season_type
    GROUP BY season, season_type, team_id
  ON DUPLICATE KEY UPDATE num_pass_sacked = VALUES(num_pass_sacked);

  # Defense
  INSERT INTO SPORTS_NFL_TEAMS_STATS_DEFENSE (season, season_type, team_id, tckl, asst, sacks, sacks_yards)
    SELECT season, season_type, team_id,
           SUM(tckl), SUM(asst) / 2, SUM(sacks), SUM(sacks_yards)
    FROM SPORTS_NFL_PLAYERS_SEASON_TACKLES
    WHERE season = v_season
    AND   season_type = v_season_type
    GROUP BY season, season_type, team_id
  ON DUPLICATE KEY UPDATE tckl = VALUES(tckl),
                          asst = VALUES(asst),
                          sacks = VALUES(sacks),
                          sacks_yards = VALUES(sacks_yards);

  INSERT INTO SPORTS_NFL_TEAMS_STATS_DEFENSE (season, season_type, team_id, pd)
    SELECT season, season_type, team_id,
           SUM(pd)
    FROM SPORTS_NFL_PLAYERS_SEASON_PASSDEF
    WHERE season = v_season
    AND   season_type = v_season_type
    GROUP BY season, season_type, team_id
  ON DUPLICATE KEY UPDATE pd = VALUES(pd);

  INSERT INTO SPORTS_NFL_TEAMS_STATS_DEFENSE (season, season_type, team_id, fumb_forced, fumb_rec)
    SELECT season, season_type, team_id,
           SUM(num_forced), SUM(rec_opp)
    FROM SPORTS_NFL_PLAYERS_SEASON_FUMBLES
    WHERE season = v_season
    AND   season_type = v_season_type
    GROUP BY season, season_type, team_id
  ON DUPLICATE KEY UPDATE fumb_forced = VALUES(fumb_forced),
                          fumb_rec = VALUES(fumb_rec);

END $$

DELIMITER ;

##
## Team season totals table maintenance
##
DROP PROCEDURE IF EXISTS `nfl_totals_teams_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_totals_teams_order`()
    COMMENT 'Order the NFL team total tables'
BEGIN

  ALTER TABLE SPORTS_NFL_TEAMS_STATS_DOWNS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_PLAYS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_YARDS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_KICKOFFS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_PUNTS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_RETURNS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_TDS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_2PT ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_KICKS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_TURNOVERS ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_MISC ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NFL_TEAMS_STATS_DEFENSE ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

