##
## Calculate a passer rating
##
DROP FUNCTION IF EXISTS `nfl_passer_rating`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nfl_passer_rating`(
  num_att SMALLINT UNSIGNED,
  num_cmp SMALLINT UNSIGNED,
  num_yards SMALLINT SIGNED,
  num_td TINYINT UNSIGNED,
  num_int TINYINT UNSIGNED
) RETURNS DECIMAL(4,1)
    DETERMINISTIC
    COMMENT 'Calculate the NFL passer rating'
BEGIN

  DECLARE part_cmp FLOAT;
  DECLARE part_yards FLOAT;
  DECLARE part_td  FLOAT;
  DECLARE part_int FLOAT;

  # If no attempts, return a rating of 0
  IF num_att = 0 THEN
    RETURN 0;
  END IF;

  # Calculate the component parts
  SET part_cmp = (((num_cmp / num_att) - 0.3) * 5);
  SET part_yards = (((num_yards / num_att) - 3) * 0.25);
  SET part_td  = ((num_td/ num_att) * 20);
  SET part_int = (2.375 - ((num_int / num_att) * 25));

  # Ensure they are within the 0 -> 2.375 range
  IF part_cmp < 0 THEN SET part_cmp = 0; ELSEIF part_cmp > 2.375 THEN SET part_cmp = 2.375; END IF;
  IF part_yards < 0 THEN SET part_yards = 0; ELSEIF part_yards > 2.375 THEN SET part_yards = 2.375; END IF;
  IF part_td  < 0 THEN SET part_td  = 0; ELSEIF part_td  > 2.375 THEN SET part_td  = 2.375; END IF;
  IF part_int < 0 THEN SET part_int = 0; ELSEIF part_int > 2.375 THEN SET part_int = 2.375; END IF;

  # Then return...
  RETURN (((part_cmp + part_yards + part_td + part_int) / 6) * 100);

END $$

DELIMITER ;

