##
## Determine codes for individual teams
##
#
# Home Field
#  - Can not be beaten by any other team in the conference
#
DROP PROCEDURE IF EXISTS `nfl_standings_codes_process_homefield`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_process_homefield`(
  IN v_season SMALLINT UNSIGNED,
  IN v_team_id VARCHAR(3),
  IN v_mode ENUM('actual','estimate'),
  OUT v_team_code CHAR(1)
)
    COMMENT 'Determine NFL Homefield Advantage'
BEGIN

  CALL nfl_standings_codes_process__exec(v_team_id, v_mode, 'conf', 1, '*', v_team_code);

END $$

DELIMITER ;

#
# First Round Bye
#  - Can not finish below second amongst all teams with best max record in div (not nec *current* div leader)
#
DROP PROCEDURE IF EXISTS `nfl_standings_codes_process_bye`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_process_bye`(
  IN v_season SMALLINT UNSIGNED,
  IN v_team_id VARCHAR(3),
  IN v_mode ENUM('actual','estimate'),
  OUT v_team_code CHAR(1)
)
    COMMENT 'Determine NFL First Round Bye'
BEGIN

  DECLARE v_bye_teams TINYINT UNSIGNED;
  SET v_bye_teams = nfl_standings_codes_config_numeric('num_playoff_byes');

  CALL nfl_standings_codes_process__exec(v_team_id, v_mode, 'conf', v_bye_teams, 'z', v_team_code);

END $$

DELIMITER ;

#
# Div Winner
#  - Can not be beaten by the other teams in the div
#
DROP PROCEDURE IF EXISTS `nfl_standings_codes_process_divwinner`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_process_divwinner`(
  IN v_season SMALLINT UNSIGNED,
  IN v_team_id VARCHAR(3),
  IN v_mode ENUM('actual','estimate'),
  OUT v_team_code CHAR(1)
)
    COMMENT 'Determine NFL Division Winners'
BEGIN

  CALL nfl_standings_codes_process__exec(v_team_id, v_mode, 'div', 1, 'y', v_team_code);

END $$

DELIMITER ;

#
# Wild Card
#  - Can no longer match final playoff team in conf
#
DROP PROCEDURE IF EXISTS `nfl_standings_codes_process_wildcard`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_process_wildcard`(
  IN v_season SMALLINT UNSIGNED,
  IN v_team_id VARCHAR(3),
  IN v_mode ENUM('actual','estimate'),
  OUT v_team_code CHAR(1)
)
    COMMENT 'Determine NFL Wildcard Teams'
BEGIN

  DECLARE v_playoff_teams TINYINT UNSIGNED;
  SET v_playoff_teams = nfl_standings_codes_config_numeric('num_playoff_teams');

  CALL nfl_standings_codes_process__exec(v_team_id, v_mode, 'conf', v_playoff_teams, 'x', v_team_code);

END $$

DELIMITER ;

#
# Eliminated
#  - Can no longer match final playoff team in conf *or* div leader
#  - Non-standard due to the direction of testing (below, not above...)
#
DROP PROCEDURE IF EXISTS `nfl_standings_codes_process_eliminated`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_process_eliminated`(
  IN v_season SMALLINT UNSIGNED,
  IN v_team_id VARCHAR(3),
  IN v_mode ENUM('actual','estimate'),
  OUT v_team_code CHAR(1)
)
    COMMENT 'Determine NFL Eliminated Teams'
BEGIN

  # Declare vars
  DECLARE v_char CHAR(1) DEFAULT 'e';
  DECLARE v_team_conf_pos TINYINT UNSIGNED;
  DECLARE v_win_pct_team FLOAT;
  DECLARE v_win_pct_wc FLOAT;
  DECLARE v_win_pct_div FLOAT;
  DECLARE v_playoff_teams TINYINT UNSIGNED;
  SET v_playoff_teams = nfl_standings_codes_config_numeric('num_playoff_teams');

  # Based on pre-determined (final) positions
  IF v_mode = 'actual' THEN
    SET v_team_conf_pos = nfl_standings_codes_team_pos(v_team_id, 'conf');
    IF v_team_conf_pos > v_playoff_teams THEN
      SET v_team_code = v_char;
    END IF;

  # Based on guesstimates
  ELSE
    # Our current record
    SET v_win_pct_team = nfl_standings_codes_record_best(v_team_id);

    # Remaining teams current record
    SET v_win_pct_wc = nfl_standings_codes_record_worst(nfl_standings_codes_team_byconf(v_playoff_teams));
    SET v_win_pct_div = nfl_standings_codes_record_worst(nfl_standings_codes_team_bydiv(1));

    # If our best record is below the last wildcard and division leader, then we are officially eliminated
    IF v_win_pct_team < v_win_pct_wc AND v_win_pct_team < v_win_pct_div THEN
      SET v_team_code = v_char;
    END IF;
  END IF;

END $$

DELIMITER ;

##
## Generic code identifier
##
DROP PROCEDURE IF EXISTS `nfl_standings_codes_process__exec`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_standings_codes_process__exec`(
  IN v_team_id VARCHAR(3),
  IN v_mode ENUM('actual','estimate'),
  IN v_group ENUM('conf','div'),
  IN v_test_pos TINYINT UNSIGNED,
  IN v_char CHAR(1),
  OUT v_char_out CHAR(1)
)
    COMMENT 'Determine if standing code criteria was met'
BEGIN

  # Declare vars
  DECLARE v_team_pos TINYINT UNSIGNED;
  DECLARE v_win_pct_team FLOAT;
  DECLARE v_team_opp VARCHAR(3);
  DECLARE v_win_pct_opp FLOAT;

  # Default value
  SET v_char_out = NULL;

  # Based on pre-determined (final) positions
  IF v_mode = 'actual' THEN
    SET v_team_pos = nfl_standings_codes_team_pos(v_team_id, v_group);
    IF v_team_pos <= v_test_pos THEN
      SET v_char_out = v_char;
    END IF;

  # Based on guesstimates
  ELSE
    # Our worst record
    SET v_win_pct_team = nfl_standings_codes_record_worst(v_team_id);

    # Remaining teams best record
    IF v_group = 'conf' THEN
      SET v_team_opp = nfl_standings_codes_team_byconf(v_test_pos);
    ELSE
      SET v_team_opp = nfl_standings_codes_team_bydiv(v_test_pos);
    END IF;
    SET v_win_pct_opp = nfl_standings_codes_record_best(v_team_opp);

    # If our current record is above the best possible, then we have a successful match...
    IF v_win_pct_team > v_win_pct_opp THEN
      SET v_char_out = v_char;
    END IF;

  END IF;

END $$

DELIMITER ;

