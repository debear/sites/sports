##
## Calculate the playoff standings after games for a given week
##
DROP PROCEDURE IF EXISTS `nfl_playoff_matchups`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_matchups`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate playoff matchups'
proc: BEGIN

  # Matchup details
  DECLARE v_round_num TINYINT UNSIGNED;
  DECLARE v_round_started TINYINT UNSIGNED;
  DECLARE v_round_complete TINYINT UNSIGNED;
  DECLARE v_counter TINYINT UNSIGNED DEFAULT 0;

  # Identify the conferences to loop through
  DECLARE v_conf_id TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  DECLARE cur_conf CURSOR FOR
    SELECT DISTINCT conf_id FROM tmp_seeds ORDER BY conf_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Determine the round to process (ignoring future rounds, where not all teams are known at this stage)
  DROP TEMPORARY TABLE IF EXISTS tmp_matchups_rounds_stage0;
  CREATE TEMPORARY TABLE tmp_matchups_rounds_stage0 (
    round_num TINYINT UNSIGNED,
    week TINYINT UNSIGNED,
    num_unknown TINYINT UNSIGNED,
    num_rows TINYINT UNSIGNED,
    started TINYINT UNSIGNED,
    complete TINYINT UNSIGNED,
    PRIMARY KEY (round_num)
  ) ENGINE = MEMORY
    SELECT SUBSTRING(round_code, 1, 1) AS round_num, MAX(week) AS week, SUM(0 IN (higher_seed, lower_seed)) AS num_unknown, COUNT(DISTINCT week) AS num_rows, NULL AS started, NULL AS complete
    FROM SPORTS_NFL_PLAYOFF_MATCHUPS
    WHERE season = v_season
    GROUP BY round_num;
  DELETE FROM tmp_matchups_rounds_stage0 WHERE num_unknown > 0;
  ALTER TABLE tmp_matchups_rounds_stage0 DROP COLUMN num_unknown;
  DROP TEMPORARY TABLE IF EXISTS tmp_matchups_rounds;
  CREATE TEMPORARY TABLE tmp_matchups_rounds LIKE tmp_matchups_rounds_stage0;
  INSERT INTO tmp_matchups_rounds
    SELECT ROUNDS.round_num, ROUNDS.week, ROUNDS.num_rows, MAX(MATCHUPS.higher_score IS NOT NULL) AS started, MIN(MATCHUPS.complete) AS complete
    FROM tmp_matchups_rounds_stage0 AS ROUNDS
    JOIN SPORTS_NFL_PLAYOFF_MATCHUPS AS MATCHUPS
      ON (MATCHUPS.season = v_season
      AND SUBSTRING(MATCHUPS.round_code, 1, 1) = ROUNDS.round_num
      AND MATCHUPS.week = ROUNDS.week)
    GROUP BY ROUNDS.round_num;

  # Get the current round info out
  SELECT round_num, started, complete
    INTO v_round_num, v_round_started, v_round_complete
  FROM tmp_matchups_rounds
  ORDER BY week DESC
  LIMIT 1;

  # If no matches (start of playoffs...), base from end of season
  IF v_done THEN
    # Ensure non-null values
    SET v_round_num := 1;
    SET v_round_complete := 0;
    # Re-set our CONTINUE HANDLER checker
    SET v_done := 0;

  # We'll only process after a completed round
  ELSEIF v_round_complete = 0 THEN
    LEAVE proc;

  # Ensure the round and series date are correct
  ELSE
    # The last round was completed, move on to the next round
    SET v_round_num := (v_round_num + 1);

    # Catch end of playoff scenario
    IF v_round_num > 4 THEN
      LEAVE proc;
    END IF;
  END IF;

  # Prep
  CALL nfl_playoff_matchups_setup(v_season, v_round_num, v_round_num > 1 OR v_round_started = 0);

  # Conference-based matchups
  IF v_round_num < 4 THEN

    # Loop through the conferences
    SET v_done := 0; # Reset the continue handler tracker
    OPEN cur_conf;
    loop_conf: LOOP

      FETCH cur_conf INTO v_conf_id;
      IF v_done = 1 THEN LEAVE loop_conf; END IF;

      # Wildcard Round
      IF v_round_num = 1 THEN
        # Until 2019, two matchups
        IF v_season <= 2019 THEN
          CALL nfl_playoff_matchups_wc_two(v_season, v_round_num, v_conf_id, v_counter);
        # From 2020, three matchups
        ELSE
          CALL nfl_playoff_matchups_wc_three(v_season, v_round_num, v_conf_id, v_counter);

        END IF;

      # Division Round
      ELSEIF v_round_num = 2 THEN
        CALL nfl_playoff_matchups_div(v_season, v_round_num, v_conf_id, v_counter);

      # Conference Championship
      ELSEIF v_round_num = 3 THEN
        CALL nfl_playoff_matchups_conf(v_season, v_round_num, v_conf_id, v_counter);

      END IF;

    END LOOP loop_conf;
    CLOSE cur_conf;

  # Super Bowl is a fixed method
  ELSE
    CALL nfl_playoff_matchups_sb(v_season, v_round_num, v_counter);
  END IF;

  # Final ordering
  ALTER TABLE SPORTS_NFL_PLAYOFF_MATCHUPS ORDER BY season, week, round_code;

END $$

DELIMITER ;

##
## Setup
##
DROP PROCEDURE IF EXISTS `nfl_playoff_matchups_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_matchups_setup`(
  v_season SMALLINT UNSIGNED,
  v_round_num TINYINT UNSIGNED,
  v_prune TINYINT UNSIGNED
)
    COMMENT 'Prepare for our NFL playoff matchup calcs'
BEGIN

  DECLARE v_standing_week TINYINT UNSIGNED;

  # Clear previous run
  IF v_prune THEN
    DELETE FROM SPORTS_NFL_PLAYOFF_MATCHUPS WHERE season = v_season AND SUBSTRING(round_code, 1, 1) = v_round_num;
  END IF;

  #
  # Build our temp table of seedings
  #
  SELECT MAX(week) INTO v_standing_week FROM SPORTS_NFL_STANDINGS WHERE season = v_season;
  DROP TEMPORARY TABLE IF EXISTS tmp_seeds;
  CREATE TEMPORARY TABLE tmp_seeds (
    team_id VARCHAR(3),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_div TINYINT UNSIGNED,
    PRIMARY KEY (conf_id, pos_conf)
  ) ENGINE = MEMORY
    SELECT STANDINGS.team_id,
           DIVN.parent_id AS conf_id, DIVN.grouping_id AS div_id,
           STANDINGS.pos_league, SEEDS.seed AS pos_conf, STANDINGS.pos_div
    FROM SPORTS_NFL_TEAMS_GROUPINGS AS TEAM_DIVN
    JOIN SPORTS_NFL_GROUPINGS AS DIVN
      ON (DIVN.grouping_id = TEAM_DIVN.grouping_id)
    JOIN SPORTS_NFL_STANDINGS AS STANDINGS
      ON (STANDINGS.season = v_season
      AND STANDINGS.week = v_standing_week
      AND STANDINGS.team_id = TEAM_DIVN.team_id)
    JOIN SPORTS_NFL_PLAYOFF_SEEDS AS SEEDS
      ON (SEEDS.season = STANDINGS.season
      AND SEEDS.conf_id = DIVN.parent_id
      AND SEEDS.team_id = STANDINGS.team_id)
    WHERE v_season BETWEEN TEAM_DIVN.season_from AND IFNULL(TEAM_DIVN.season_to, 2099);

  # Adjust to only include teams to have progressed
  IF IFNULL(v_round_num, 1) > 1 THEN
    # Who hasn't progressed this far?
    DROP TEMPORARY TABLE IF EXISTS tmp_playoff_losers;
    CREATE TEMPORARY TABLE tmp_playoff_losers (
      conf_id TINYINT UNSIGNED,
      pos_conf TINYINT UNSIGNED,
      PRIMARY KEY (conf_id, pos_conf)
    ) ENGINE = MEMORY
      SELECT IF(higher_score IS NOT NULL AND higher_score < lower_score, higher_conf_id, lower_conf_id) AS conf_id,
             IF(higher_score IS NOT NULL AND higher_score < lower_score, higher_seed, lower_seed) AS pos_conf
      FROM SPORTS_NFL_PLAYOFF_MATCHUPS
      WHERE season = v_season
      AND   SUBSTRING(round_code, 1, 1) < v_round_num
      AND   complete = 1
      GROUP BY conf_id, pos_conf;

    # Remove from tmp_seeds
    DELETE tmp_seeds.*
    FROM tmp_playoff_losers
    JOIN tmp_seeds
      ON (tmp_seeds.conf_id = tmp_playoff_losers.conf_id
      AND tmp_seeds.pos_conf = tmp_playoff_losers.pos_conf);
  END IF;

END $$

DELIMITER ;

#
# Wildcard Round (2 games, 2 byes)
#
DROP PROCEDURE IF EXISTS `nfl_playoff_matchups_wc_two`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_matchups_wc_two`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_round_num TINYINT UNSIGNED,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NFL playoff Wildcard Round matchup calcs (two games)'
BEGIN

  INSERT INTO SPORTS_NFL_PLAYOFF_MATCHUPS (season, week, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, game_id, higher_score, lower_score, complete) VALUES
    (v_season, v_round_num, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, 3, v_conf_id, 6, NULL, NULL, NULL, 0),
    (v_season, v_round_num, CAST(10 + v_counter + 2 AS CHAR(2)), v_conf_id, 4, v_conf_id, 5, NULL, NULL, NULL, 0);
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

#
# Wildcard Round (3 games, 1 byes)
#
DROP PROCEDURE IF EXISTS `nfl_playoff_matchups_wc_three`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_matchups_wc_three`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_round_num TINYINT UNSIGNED,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NFL playoff Wildcard Round matchup calcs (three games)'
BEGIN

  INSERT INTO SPORTS_NFL_PLAYOFF_MATCHUPS (season, week, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, game_id, higher_score, lower_score, complete) VALUES
    (v_season, v_round_num, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, 2, v_conf_id, 7, NULL, NULL, NULL, 0),
    (v_season, v_round_num, CAST(10 + v_counter + 2 AS CHAR(2)), v_conf_id, 3, v_conf_id, 6, NULL, NULL, NULL, 0),
    (v_season, v_round_num, CAST(10 + v_counter + 3 AS CHAR(2)), v_conf_id, 4, v_conf_id, 5, NULL, NULL, NULL, 0);
  SET v_counter = v_counter + 3;

END $$

DELIMITER ;

#
# Divisional Round
#
DROP PROCEDURE IF EXISTS `nfl_playoff_matchups_div`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_matchups_div`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_round_num TINYINT UNSIGNED,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NFL playoff Divisional Round matchup calcs'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;
  DECLARE v_seed_c TINYINT UNSIGNED;
  DECLARE v_seed_d TINYINT UNSIGNED;

  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf LIMIT 0, 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf LIMIT 1, 1;
  SELECT pos_conf INTO v_seed_c FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf LIMIT 2, 1;
  SELECT pos_conf INTO v_seed_d FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf LIMIT 3, 1;

  INSERT INTO SPORTS_NFL_PLAYOFF_MATCHUPS (season, week, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, game_id, higher_score, lower_score, complete) VALUES
    (v_season, v_round_num, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_d, NULL, NULL, NULL, 0),
    (v_season, v_round_num, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_b, v_conf_id, v_seed_c, NULL, NULL, NULL, 0);
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

#
# Conference Championship
#
DROP PROCEDURE IF EXISTS `nfl_playoff_matchups_conf`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_matchups_conf`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_round_num TINYINT UNSIGNED,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NFL playoff Conference Championship matchup calcs'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_NFL_PLAYOFF_MATCHUPS (season, week, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, game_id, higher_score, lower_score, complete) VALUES
    (v_season, v_round_num, CAST(30 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, NULL, NULL, NULL, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;

#
# Super Bowl
#
DROP PROCEDURE IF EXISTS `nfl_playoff_matchups_sb`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_playoff_matchups_sb`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_round_num TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NFL playoff Super Bowl matchup calcs'
BEGIN

  DECLARE v_conf_a TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_conf_b TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT conf_id, pos_conf INTO v_conf_a, v_seed_a FROM tmp_seeds ORDER BY pos_league  ASC LIMIT 1;
  SELECT conf_id, pos_conf INTO v_conf_b, v_seed_b FROM tmp_seeds ORDER BY pos_league DESC LIMIT 1;

  INSERT INTO SPORTS_NFL_PLAYOFF_MATCHUPS (season, week, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, game_id, higher_score, lower_score, complete) VALUES
    (v_season, v_round_num, CAST(40 + v_counter + 1 AS CHAR(2)), v_conf_a, v_seed_a, v_conf_b, v_seed_b, NULL, NULL, NULL, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;
