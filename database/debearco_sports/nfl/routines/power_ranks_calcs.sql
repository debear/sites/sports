#
# Team schedules
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_sched`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_week TINYINT UNSIGNED,
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate the NFL power ranks team schedules'
BEGIN

  # First, shorten the team list
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, grouping_id AS div_id
    FROM SPORTS_NFL_TEAMS_GROUPINGS
    WHERE v_season BETWEEN season_from AND IFNULL(season_to, 2099);

  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # Then build...
  DROP TEMPORARY TABLE IF EXISTS tmp_sched;
  CREATE TEMPORARY TABLE tmp_sched (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    week TINYINT UNSIGNED,
    cmb_week TINYINT UNSIGNED,
    team_id VARCHAR(3),
    opp_id VARCHAR(3),
    game_id SMALLINT UNSIGNED,
    result ENUM('w','l','t'),
    is_recent TINYINT UNSIGNED,
    is_div TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, week, team_id),
    INDEX by_team (team_id),
    INDEX by_game_team (season, game_type, week, game_id, team_id),
    INDEX by_game_opp (season, game_type, week, game_id, opp_id)
  ) ENGINE = MEMORY
    SELECT SCHED.season, SCHED.game_type, SCHED.week,
           SCHED.week + IF(SCHED.game_type = 'regular', 0, 17) AS cmb_week,
           tmp_teams.team_id, tmp_teams_cp.team_id AS opp_id,
           SCHED.game_id,
           IF(SCHED.home_score = SCHED.visitor_score,
              't',
              IF((SCHED.home = tmp_teams.team_id AND SCHED.home_score > SCHED.visitor_score)
                 OR (SCHED.home <> tmp_teams.team_id AND SCHED.home_score < SCHED.visitor_score),
                 'w', 'l')) AS result,
           0 AS is_recent,
           SCHED.game_type = 'regular' AND tmp_teams.div_id = tmp_teams_cp.div_id AS is_div
    FROM tmp_teams
    LEFT JOIN SPORTS_NFL_SCHEDULE AS SCHED
      ON (SCHED.season = v_season
      AND ((SCHED.game_type = v_game_type AND SCHED.week <= v_week)
        OR (v_game_type = 'playoff' AND SCHED.game_type = 'regular'))
      AND (SCHED.home = tmp_teams.team_id
        OR SCHED.visitor = tmp_teams.team_id))
    LEFT JOIN tmp_teams_cp
      ON (tmp_teams_cp.team_id = IF(tmp_teams.team_id = SCHED.home, SCHED.visitor, SCHED.home));

  CALL _duplicate_tmp_table('tmp_sched', 'tmp_sched_cpA');
  CALL _duplicate_tmp_table('tmp_sched', 'tmp_sched_cpB');

  INSERT INTO tmp_sched (season, game_type, week, team_id, is_recent)
    SELECT tmp_sched_cpA.season, tmp_sched_cpA.game_type, tmp_sched_cpA.week, tmp_sched_cpA.team_id, 1 AS is_recent
    FROM tmp_sched_cpA
    LEFT JOIN tmp_sched_cpB
      ON (tmp_sched_cpB.cmb_week > tmp_sched_cpA.cmb_week
      AND tmp_sched_cpB.team_id = tmp_sched_cpA.team_id)
    GROUP BY tmp_sched_cpA.season, tmp_sched_cpA.game_type, tmp_sched_cpA.week, tmp_sched_cpA.team_id
    HAVING COUNT(tmp_sched_cpB.week) < v_recent_games
  ON DUPLICATE KEY UPDATE is_recent = VALUES(is_recent);

END $$

DELIMITER ;

#
# Create the table to store the raw data for a stat
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_schema`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_schema`(
  v_type VARCHAR(30)
)
    COMMENT 'Create the table for processing NFL power rank data'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_data;
  CALL _exec(CONCAT('CREATE TEMPORARY TABLE tmp_data (
    team_id VARCHAR(3),
    tot_ov ', v_type, ',
    tot_rec ', v_type, ',
    tot_div ', v_type, ',
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY;'));

END $$

DELIMITER ;

#
# Process a stat
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_process`(
  v_col VARCHAR(15),
  v_asc TINYINT UNSIGNED,
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Process and individual NFL power rank'
BEGIN

  # Add to the table
  CALL _exec(CONCAT('ALTER TABLE tmp_stats ADD COLUMN ', v_col, '_ov DECIMAL(5,3) SIGNED,
                                           ADD COLUMN ', v_col, '_rec DECIMAL(5,3) SIGNED,
                                           ADD COLUMN ', v_col, '_div DECIMAL(5,3) SIGNED;'));

  # Process the individual parts: overall, recent, division
  CALL nfl_power_ranks_build_process_part(v_col, v_asc, 'ov');
  CALL nfl_power_ranks_build_process_part(v_col, v_asc, 'rec');
  CALL nfl_power_ranks_build_process_part(v_col, v_asc, 'div');

  # And add to the final table
  CALL _exec(CONCAT('ALTER TABLE tmp_ranks ADD COLUMN ', v_col, ' DECIMAL(5,3) SIGNED;'));

  # Season total accounts for 17% of our score
  # Recent games account for 33% of our score
  # Divisional value worth half our score
  CALL _exec(CONCAT('UPDATE tmp_ranks
  JOIN tmp_stats
    ON (tmp_stats.team_id = tmp_ranks.team_id)
  SET tmp_ranks.', v_col, ' = (tmp_stats.', v_col, '_ov  * ', v_weight, ' * 0.17)
                            + (tmp_stats.', v_col, '_rec * ', v_weight, ' * 0.33)
                            + (tmp_stats.', v_col, '_div * ', v_weight, ' * 0.50);'));
  CALL _exec(CONCAT('UPDATE tmp_ranks SET score = score + ', v_col, ';'));

END $$

DELIMITER ;

#
# Process a stat part
#
DROP PROCEDURE IF EXISTS `nfl_power_ranks_build_process_part`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nfl_power_ranks_build_process_part`(
  v_col VARCHAR(15),
  v_asc TINYINT UNSIGNED,
  v_part ENUM('ov','rec','div')
)
    COMMENT 'Process and individual NFL power rank part'
BEGIN

  # Declare vars
  DECLARE v_median FLOAT;
  DECLARE v_std_dev FLOAT;

  # Calculate the median and standard deviations of the data set (using temporary vars to get info out of _exec)
  CALL _exec(CONCAT('SELECT AVG(tot_', v_part, '), STDDEV_POP(tot_', v_part, ')
         INTO @v_median, @v_std_dev
  FROM tmp_data;'));
  SELECT @v_median, @v_std_dev INTO v_median, v_std_dev;

  # And score each team based on multiples of the standard deviation from the mean
  CALL _exec(CONCAT('UPDATE tmp_stats
  JOIN tmp_data
    ON (tmp_data.team_id = tmp_stats.team_id)
  SET tmp_stats.', v_col, '_', v_part, ' = IF(', v_asc, ' = 0,
        CAST(IFNULL(tmp_data.tot_', v_part, ', 0) AS DECIMAL(11,7)) - CAST(', v_median, ' AS DECIMAL(11,7)),
        CAST(', v_median, ' AS DECIMAL(11,7)) - CAST(IFNULL(tmp_data.tot_', v_part, ', 0) AS DECIMAL(11,7))) / ', v_std_dev, ';'));

END $$

DELIMITER ;

