CREATE TABLE `SPORTS_NFL_PLAYERS` (
  `player_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `dob` DATE DEFAULT NULL,
  `birthplace` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `birthplace_state` ENUM('AL','AK','AZ','AR','CA','CO','CT','DC','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY','AB','BC','MB','NB','NL','NS','ON','PE','QC','SK','NT','NU','YT') COLLATE latin1_general_ci DEFAULT NULL,
  `birthplace_country` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `height` TINYINT(3) UNSIGNED DEFAULT NULL,
  `weight` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `college` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_IMPORT` (
  `player_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `remote_internal_id` CHAR(10) COLLATE latin1_general_ci DEFAULT NULL,
  `remote_url_id` MEDIUMINT(6) UNSIGNED DEFAULT NULL,
  `remote_url_name` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `remote_profile_id` CHAR(9) COLLATE latin1_general_ci DEFAULT NULL,
  `remote_url_name_v2` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `profile_imported` DATETIME DEFAULT NULL,
  PRIMARY KEY (`player_id`),
  UNIQUE KEY `remote_internal_id` (`remote_internal_id`),
  UNIQUE KEY `remote_url_id` (`remote_url_id`),
  UNIQUE KEY `remote_profile_id` (`remote_profile_id`),
  UNIQUE KEY `remote_url_name_v2` (`remote_url_name_v2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS` (
  `player_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `remote_mugshot` CHAR(20) COLLATE latin1_general_ci,
  `first_seen` DATETIME DEFAULT NULL,
  PRIMARY KEY (`player_id`, `remote_mugshot`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_AWARDS` (
  `season` YEAR NOT NULL,
  `award_id` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `player_name` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `pos` ENUM('QB','RB','WR','TE','C','G','T','DT','DE','LB','CB','S','K','P','KR','PR') COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `FAUX_PRIMARY` (`season`,`award_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
