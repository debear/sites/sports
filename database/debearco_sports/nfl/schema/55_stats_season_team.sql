CREATE TABLE `SPORTS_NFL_TEAMS_STATS_YARDS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `yards_pass` SMALLINT(6) DEFAULT 0,
  `yards_rush` SMALLINT(6) DEFAULT 0,
  `yards_rush_tfl` SMALLINT(6) DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_TDS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num_pass` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_rush` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_int` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_fumbles` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_fumbles_own` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_fumbles_opp` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_ko` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_punt` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_other` TINYINT(3) UNSIGNED DEFAULT 0,
  `safties` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_2PT` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `pass_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `pass_made` TINYINT(3) UNSIGNED DEFAULT 0,
  `rush_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `rush_made` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_KICKS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `fg_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_made` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_blocked` TINYINT(3) UNSIGNED DEFAULT 0,
  `pat_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `pat_made` TINYINT(3) UNSIGNED DEFAULT 0,
  `pat_blocked` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_KICKOFFS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num` TINYINT(3) UNSIGNED DEFAULT 0,
  `end_zone` TINYINT(3) UNSIGNED DEFAULT 0,
  `touchback` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_PUNTS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `net` SMALLINT(6) DEFAULT 0,
  `blocked` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_RETURNS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `ko_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `ko_yards` SMALLINT(6) DEFAULT 0,
  `punt_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `punt_yards` SMALLINT(6) DEFAULT 0,
  `int_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `int_yards` SMALLINT(6) DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_TURNOVERS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `int_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `fumb_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `fumb_lost` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_DOWNS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num_1st_rush` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_1st_pass` SMALLINT(5) UNSIGNED DEFAULT 0,
  `num_1st_pen` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_3rd` SMALLINT(5) UNSIGNED DEFAULT 0,
  `num_3rd_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_4th` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_4th_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_DEFENSE` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `tckl` SMALLINT(5) UNSIGNED DEFAULT 0,
  `asst` SMALLINT(5) UNSIGNED DEFAULT 0,
  `sacks` TINYINT(3) UNSIGNED DEFAULT 0,
  `sacks_yards` SMALLINT(6) DEFAULT 0,
  `pd` TINYINT(3) UNSIGNED DEFAULT 0,
  `fumb_forced` TINYINT(3) UNSIGNED DEFAULT 0,
  `fumb_rec` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_PLAYS` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `total` SMALLINT(5) UNSIGNED DEFAULT 0,
  `num_pass_att` SMALLINT(5) UNSIGNED DEFAULT 0,
  `num_pass_cmp` SMALLINT(5) UNSIGNED DEFAULT 0,
  `num_pass_sacked` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_rush` SMALLINT(5) UNSIGNED DEFAULT 0,
  `num_rush_tfl` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_MISC` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `time_of_poss` TIME DEFAULT NULL,
  `pens_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `pens_yards` SMALLINT(5) UNSIGNED DEFAULT 0,
  `rz_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `rz_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  `goal_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `goal_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_YARDS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `yards_total` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `yards_pass` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `yards_rush` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `yards_rush_tfl` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_TDS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num_total` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_pass` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_rush` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_int` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_fumbles` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_fumbles_own` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_fumbles_opp` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_ko` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_punt` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_other` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `safties` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_2PT_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `pass_att` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pass_made` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pass_pct` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `rush_att` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `rush_made` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `rush_pct` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `total_att` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `total_made` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `total_pct` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_KICKS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `fg_att` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `fg_made` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `fg_pct` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `fg_blocked` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pat_att` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pat_made` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pat_pct` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pat_blocked` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_KICKOFFS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `end_zone` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `touchback` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_PUNTS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `yards` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `avg` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `net` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `net_avg` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `blocked` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_RETURNS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `ko_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `ko_yards` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `ko_avg` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `punt_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `punt_yards` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `punt_avg` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `int_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `int_yards` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `int_avg` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_TURNOVERS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `total` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `int_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `fumb_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `fumb_lost` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_DOWNS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num_1st` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_1st_rush` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_1st_pass` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_1st_pen` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_3rd` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_3rd_cnv` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pct_3rd` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_4th` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_4th_cnv` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pct_4th` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_DEFENSE_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `total` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `tckl` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `asst` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `sacks` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `sacks_yards` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pd` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `fumb_forced` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `fumb_rec` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_PLAYS_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `total` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_pass_att` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_pass_cmp` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pct_pass` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_pass_sacked` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_rush` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `num_rush_tfl` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STATS_MISC_SORTED` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `time_of_poss` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pens_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `pens_yards` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `rz_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `rz_cnv` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `rz_pct` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `goal_num` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `goal_cnv` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  `goal_pct` TINYINT(3) UNSIGNED NOT NULL DEFAULT 99,
  PRIMARY KEY (`season`,`season_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
