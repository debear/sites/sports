CREATE TABLE `SPORTS_NFL_TEAMS_ROSTERS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `pos` ENUM('0LB','3CB','BLB','C','CB','D','DB','DE','DL','DS','DT','END','F','FB','FL','FS','FSW','G','GT','H','HB','HR','ILB','ILBV','JACK','JLB','K','KR','L','LB','LCB','LDB','LDE','LDT','LE','LEO','LG','LILB','LLB','LOLB','LOT','LS','LT','LWR','MCB','MIKE','MILB','MLB','MO','MOLB','N','NB','NDB','NE','NOSE','NT','OBL','OC','OG','OL','OLB','OT','OTTO','OW','P','PD','PK','PR','Q','QB','RB','RCB','RDE','RDT','RE','RG','RILB','RLB','ROLB','ROT','RS','RSB','RT','RUSH','RWR','S','SAF','SAM','SCB','SE','SLB','SS','T','TE','TED','UT','WIL','WILL','WLB','WR','WR T','WS') COLLATE latin1_general_ci DEFAULT NULL,
  `player_status` ENUM('active','ir','ir-r','susp','pup','ps','nfi','na') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_DEPTH` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `pos` CHAR(5) COLLATE latin1_general_ci NOT NULL,
  `depth` TINYINT(1) UNSIGNED NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED,
  PRIMARY KEY (`season`,`game_type`,`week`,`pos`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
