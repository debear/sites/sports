CREATE TABLE `SPORTS_NFL_PLAYERS_INJURIES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `status` ENUM('p','q','d','o') COLLATE latin1_general_ci NOT NULL,
  `injury` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
