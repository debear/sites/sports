CREATE TABLE `SPORTS_NFL_SCHEDULE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `nfl_id` CHAR(10) COLLATE latin1_general_ci NOT NULL,
  `nfl_gsis_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `game_date` DATE NOT NULL,
  `game_time` TIME DEFAULT NULL,
  `visitor` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `home` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `visitor_score` TINYINT(2) UNSIGNED DEFAULT NULL,
  `home_score` TINYINT(2) UNSIGNED DEFAULT NULL,
  `status` ENUM('F','OT','2OT','3OT','PPD','CNC') COLLATE latin1_general_ci DEFAULT NULL,
  `attendance` MEDIUMINT(8) UNSIGNED DEFAULT NULL,
  `alt_venue` TINYINT(2) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`),
  KEY `weekly_teams` (`season`,`game_type`,`week`,`visitor`,`home`),
  KEY `weekly_home` (`season`,`game_type`,`week`,`home`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_SCHEDULE_WEEKS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_SCHEDULE_BYES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
