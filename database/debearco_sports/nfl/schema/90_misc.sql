CREATE TABLE `SPORTS_NFL_POSITIONS` (
  `pos_code` ENUM('C','CB','DB','DE','DL','DT','FB','G','K','KR','LB','LS','NT','P','PR','QB','RB','S','T','TE','WR') COLLATE latin1_general_ci NOT NULL,
  `pos_long` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `posgroup_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `type` ENUM('passing','rushing','receiving','kicking','punting','defense','misc') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pos_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_POSITIONS_GROUPS` (
  `posgroup_id` TINYINT(3) UNSIGNED NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`posgroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_POSITIONS_RAW` (
  `pos_raw` CHAR(5) COLLATE latin1_general_ci NOT NULL,
  `pos_code` ENUM('C','CB','DB','DE','DL','DT','FB','G','K','KR','LB','LS','NT','P','PR','QB','RB','S','T','TE','WR') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pos_raw`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_STATUS` (
  `type` ENUM('player') COLLATE latin1_general_ci NOT NULL,
  `code` ENUM('active','ir','ir-r','na','nfi','pup','susp') COLLATE latin1_general_ci NOT NULL,
  `disp_code` ENUM('IR','IR-R','SUSP','PUP','NFI','NA') COLLATE latin1_general_ci DEFAULT NULL,
  `disp_name` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`type`,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_OFFICIALS` (
  `official_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`official_id`),
  UNIQUE KEY `by_name` (`first_name`,`surname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GROUPINGS` (
  `grouping_id` TINYINT(1) UNSIGNED NOT NULL,
  `parent_id` TINYINT(1) UNSIGNED DEFAULT NULL,
  `type` ENUM('league','conf','div') COLLATE latin1_general_ci NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `name_full` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED NOT NULL,
  `icon` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`grouping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_AWARDS` (
  `award_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `description` VARCHAR(250) COLLATE latin1_general_ci NOT NULL,
  `disp_order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`award_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
