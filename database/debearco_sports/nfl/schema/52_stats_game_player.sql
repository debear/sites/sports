CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_PASSING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `atts` TINYINT(3) UNSIGNED DEFAULT 0,
  `cmp` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `long` TINYINT(4) DEFAULT 0,
  `td` TINYINT(3) UNSIGNED DEFAULT 0,
  `int` TINYINT(3) UNSIGNED DEFAULT 0,
  `sacked` TINYINT(3) UNSIGNED DEFAULT 0,
  `sack_yards` TINYINT(3) UNSIGNED DEFAULT 0,
  `rating` DECIMAL(4,1) UNSIGNED DEFAULT 0.0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_RUSHING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `atts` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `long` TINYINT(4) DEFAULT 0,
  `td` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_RECEIVING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `recept` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `long` TINYINT(4) DEFAULT 0,
  `td` TINYINT(3) UNSIGNED DEFAULT 0,
  `targets` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_KICKING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `fg_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_made` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_u20` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_u30` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_u40` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_u50` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_o50` TINYINT(3) UNSIGNED DEFAULT 0,
  `xp_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `xp_made` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_KICKRET` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `num` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `long` TINYINT(4) DEFAULT 0,
  `fair_catch` TINYINT(3) UNSIGNED DEFAULT 0,
  `td` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_PUNTS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `num` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `net` SMALLINT(6) DEFAULT 0,
  `long` TINYINT(4) DEFAULT 0,
  `tb` TINYINT(3) UNSIGNED DEFAULT 0,
  `inside20` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_PUNTRET` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `num` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `long` TINYINT(4) DEFAULT 0,
  `fair_catch` TINYINT(3) UNSIGNED DEFAULT 0,
  `td` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_TACKLES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `tckl` TINYINT(3) UNSIGNED DEFAULT 0,
  `asst` TINYINT(3) UNSIGNED DEFAULT 0,
  `sacks` DECIMAL(3,1) UNSIGNED DEFAULT 0.0,
  `sacks_yards` TINYINT(4) DEFAULT 0,
  `tfl` TINYINT(3) UNSIGNED DEFAULT 0,
  `qb_hit` TINYINT(3) UNSIGNED DEFAULT 0,
  `kick_block` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_PASSDEF` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `pd` TINYINT(3) UNSIGNED DEFAULT 0,
  `int` TINYINT(3) UNSIGNED DEFAULT 0,
  `int_yards` TINYINT(4) DEFAULT 0,
  `int_td` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_PLAYERS_GAME_FUMBLES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `num_fumbled` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_lost` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_forced` TINYINT(3) UNSIGNED DEFAULT 0,
  `rec_own` TINYINT(3) UNSIGNED DEFAULT 0,
  `rec_opp` TINYINT(3) UNSIGNED DEFAULT 0,
  `rec_own_td` TINYINT(3) UNSIGNED DEFAULT 0,
  `rec_opp_td` TINYINT(3) UNSIGNED DEFAULT 0,
  `oob` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`jersey`),
  KEY `by_player` (`season`,`game_type`,`week`,`game_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
