CREATE TABLE `SPORTS_NFL_TEAMS` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `city` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `franchise` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `founded` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_NAMING` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `alt_team_id` ENUM('BLC','BOP','BOR','CHC','CLR','CP','DAT','DEC','HOO','LRI','LRM','OAK','PHC','POR','PPS','SD','SLC','STL') COLLATE latin1_general_ci DEFAULT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `alt_city` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  `alt_franchise` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_GROUPINGS` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `grouping_id` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_STADIA` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH','_ALT') COLLATE latin1_general_ci NOT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `building_id` TINYINT(1) UNSIGNED NOT NULL,
  `stadium` VARCHAR(55) COLLATE latin1_general_ci NOT NULL,
  `weather_spec` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season_from`,`building_id`),
  KEY `team_season` (`team_id`,`season_from`,`season_to`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_HISTORY_TITLES` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `league_id` TINYINT(3) UNSIGNED NOT NULL,
  `div_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `conf_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `league_champ` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `playoff_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`team_id`,`league_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_HISTORY_REGULAR` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `wins` TINYINT(2) UNSIGNED DEFAULT 0,
  `loss` TINYINT(2) UNSIGNED DEFAULT 0,
  `ties` TINYINT(1) UNSIGNED DEFAULT 0,
  `pos` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `league_id` TINYINT(3) UNSIGNED NOT NULL,
  `one_game` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `wildcard` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `div_champ` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `conf_champ` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `league_champ` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `playoff_champ` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`team_id`,`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_TEAMS_HISTORY_PLAYOFF` (
  `team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `league_id` TINYINT(3) UNSIGNED NOT NULL,
  `round` TINYINT(3) UNSIGNED NOT NULL,
  `round_code` ENUM('PO','WC','SF','DIV','CG','CONF','SB') COLLATE latin1_general_ci NOT NULL,
  `opp_team_id` ENUM('ARI','ATL','BAL','BLC','BOP','BOR','BUF','CAR','CHC','CHI','CIN','CLE','CLR','CP','DAL','DAT','DEC','DEN','DET','GB','HOO','HOU','IND','JAX','KC','LA','LAC','LRI','LRM','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHC','PHI','PIT','POR','PPS','SD','SEA','SF','SLC','STL','TB','TEN','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `opp_team_alt` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  `for` TINYINT(3) UNSIGNED DEFAULT 0,
  `against` TINYINT(3) UNSIGNED DEFAULT 0,
  `info` ENUM('OT','2OT','3OT') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season`,`round`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
