CREATE TABLE `SPORTS_NFL_GAME_LINEUP` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pos` CHAR(5) COLLATE latin1_general_ci DEFAULT NULL,
  `status` ENUM('starter','substitute','dnp','inactive') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`player_id`),
  KEY `by_player__fantasy` (`season`,`game_type`,`week`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_INACTIVES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pos` ENUM('C','CB','DB','DE','DT','FB','FS','G','ILB','K','LB','LS','MLB','NT','OG','OL','OLB','OT','P','QB','RB','SAF','SS','T','TE','WR') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`,`player_id`),
  KEY `by_player__fantasy` (`season`,`game_type`,`week`,`player_id`),
  KEY `by_team` (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_OFFICIALS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `official_type` ENUM('r','u','hl','lj','sj','fj','bj','ro') COLLATE latin1_general_ci NOT NULL,
  `official_id` SMALLINT(5) UNSIGNED NOT NULL,
  `official_num` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`official_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_DRIVES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `drive_id` TINYINT(1) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `start_quarter` TINYINT(3) UNSIGNED NOT NULL,
  `start_time` TIME NOT NULL,
  `end_quarter` TINYINT(3) UNSIGNED NOT NULL,
  `end_time` TIME NOT NULL,
  `time_of_poss` TIME NOT NULL,
  `obtained` ENUM('Blocked FG','Blocked Punt','Downs','Fumble','Illegal Touch','Interception','Kickoff','Missed FG','Muffed Kickoff','Muffed Punt','Onside Kick','Own Kickoff','Punt') COLLATE latin1_general_ci DEFAULT NULL,
  `start_pos_half` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `start_pos_yd` TINYINT(3) UNSIGNED NOT NULL,
  `num_plays` TINYINT(3) UNSIGNED NOT NULL,
  `yards_gained` TINYINT(3) NOT NULL,
  `yards_penalties` TINYINT(3) NOT NULL,
  `yards_net` TINYINT(3) NOT NULL,
  `num_first_downs` TINYINT(3) UNSIGNED NOT NULL,
  `end_result` ENUM('Blocked FG','Blocked Punt','Downs','End of Game','End of Half','Field Goal','Fumble','Fumble, Safety','Illegal Touch','Interception','Missed FG','Punt','Safety','Touchdown') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`drive_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
