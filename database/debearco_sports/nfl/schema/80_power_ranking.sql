CREATE TABLE `SPORTS_NFL_POWER_RANKINGS_WEEKS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `calc_date` DATE NOT NULL,
  `when_run` DATETIME DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_POWER_RANKINGS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `rank` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `score` DECIMAL(5,3) NOT NULL DEFAULT 0.000,
  `win_pct` DECIMAL(5,3) DEFAULT NULL,
  `num_td` DECIMAL(5,3) DEFAULT NULL,
  `net_yards` DECIMAL(5,3) DEFAULT NULL,
  `time_of_poss` DECIMAL(5,3) DEFAULT NULL,
  `34dn_pct` DECIMAL(5,3) DEFAULT NULL,
  `rz_pct` DECIMAL(5,3) DEFAULT NULL,
  `pen_yards` DECIMAL(5,3) DEFAULT NULL,
  `net_to` DECIMAL(5,3) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
