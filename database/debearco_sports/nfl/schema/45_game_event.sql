CREATE TABLE `SPORTS_NFL_GAME_SCORING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `play_id` TINYINT(3) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `quarter` TINYINT(3) UNSIGNED NOT NULL,
  `time` TIME NOT NULL,
  `type` ENUM('fg','td','td+pat','td-pat','td+2pt','td-2pt','def+2pt','safety') COLLATE latin1_general_ci NOT NULL,
  `drive_plays` TINYINT(3) UNSIGNED DEFAULT NULL,
  `drive_yards` TINYINT(4) DEFAULT NULL,
  `drive_top` TIME DEFAULT NULL,
  `visitor_score` TINYINT(3) UNSIGNED NOT NULL,
  `home_score` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`play_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_SCORING_TD` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `play_id` TINYINT(3) UNSIGNED NOT NULL,
  `type` ENUM('pass','rush','int_ret','fumb_ret','fumb_rec','kick_ret','kick_rec','punt_ret','fg_ret','block_punt_ret','block_punt_rec','block_fg_ret') COLLATE latin1_general_ci NOT NULL,
  `scorer` TINYINT(3) UNSIGNED DEFAULT NULL,
  `qb` TINYINT(3) UNSIGNED DEFAULT NULL,
  `length` TINYINT(3) UNSIGNED DEFAULT NULL,
  `xp_type` ENUM('pat','2pt') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`play_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_SCORING_PAT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `play_id` TINYINT(3) UNSIGNED NOT NULL,
  `made` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `kicker` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`play_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_SCORING_2PT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `play_id` TINYINT(3) UNSIGNED NOT NULL,
  `made` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `type` ENUM('pass','rush','def') COLLATE latin1_general_ci NOT NULL,
  `qb` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`play_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_SCORING_FG` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `play_id` TINYINT(3) UNSIGNED NOT NULL,
  `kicker` TINYINT(3) UNSIGNED DEFAULT NULL,
  `length` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`play_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_SCORING_SAFETY` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `play_id` TINYINT(3) UNSIGNED NOT NULL,
  `type` ENUM('penalty','tackle','sack','fumble','punt_block','ob') COLLATE latin1_general_ci NOT NULL,
  `player_def` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_def_alt` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_off` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`play_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
