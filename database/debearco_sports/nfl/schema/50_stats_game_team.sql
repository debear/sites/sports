CREATE TABLE `SPORTS_NFL_GAME_STATS_YARDS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `yards_pass` SMALLINT(6) DEFAULT 0,
  `yards_rush` SMALLINT(6) DEFAULT 0,
  `yards_rush_tfl` TINYINT(4) DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_TDS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num_pass` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_rush` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_int` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_fumbles` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_fumbles_own` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_fumbles_opp` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_ko` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_punt` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_other` TINYINT(3) UNSIGNED DEFAULT 0,
  `safties` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_2PT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `pass_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `pass_made` TINYINT(3) UNSIGNED DEFAULT 0,
  `rush_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `rush_made` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_KICKS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `fg_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_made` TINYINT(3) UNSIGNED DEFAULT 0,
  `fg_blocked` TINYINT(3) UNSIGNED DEFAULT 0,
  `pat_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `pat_made` TINYINT(3) UNSIGNED DEFAULT 0,
  `pat_blocked` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_KICKOFFS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num` TINYINT(3) UNSIGNED DEFAULT 0,
  `end_zone` TINYINT(3) UNSIGNED DEFAULT 0,
  `touchback` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_PUNTS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num` TINYINT(3) UNSIGNED DEFAULT 0,
  `yards` SMALLINT(6) DEFAULT 0,
  `net` SMALLINT(6) DEFAULT 0,
  `blocked` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_RETURNS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `ko_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `ko_yards` SMALLINT(6) DEFAULT 0,
  `punt_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `punt_yards` SMALLINT(6) DEFAULT 0,
  `int_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `int_yards` SMALLINT(6) DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_TURNOVERS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `int_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `fumb_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `fumb_lost` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_DOWNS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `num_1st_rush` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_1st_pass` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_1st_pen` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_3rd` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_3rd_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_4th` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_4th_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_PLAYS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `total` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_pass_att` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_pass_cmp` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_rush` TINYINT(3) UNSIGNED DEFAULT 0,
  `num_rush_tfl` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NFL_GAME_STATS_MISC` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BUF','CAR','CHI','CIN','CLE','DAL','DEN','DET','GB','HOU','IND','JAX','KC','LA','LAC','LV','MIA','MIN','NE','NO','NYG','NYJ','OAK','PHI','PIT','SD','SEA','SF','STL','TB','TEN','WSH') COLLATE latin1_general_ci NOT NULL,
  `time_of_poss` TIME DEFAULT NULL,
  `pens_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `pens_yards` TINYINT(3) UNSIGNED DEFAULT 0,
  `rz_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `rz_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  `goal_num` TINYINT(3) UNSIGNED DEFAULT 0,
  `goal_cnv` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`week`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
