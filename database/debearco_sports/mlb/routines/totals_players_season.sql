#
# Aggregate Pitch Zone info from the play-by-play
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_season`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_season`()
    COMMENT 'MLB player totals that were not / cannot be previously calced'
BEGIN

  CALL mlb_totals_players_season_zones();

END $$

DELIMITER ;
