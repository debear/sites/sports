##
## Status codes
##
DROP PROCEDURE IF EXISTS `mlb_standings_codes`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_codes`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate status codes for MLB standings'
BEGIN

  DECLARE v_num_divs TINYINT UNSIGNED;
  DECLARE v_num_div_berths TINYINT UNSIGNED;
  DECLARE v_num_wildcard TINYINT UNSIGNED;

  #
  # Run some pre-processing calcs
  #
  CALL mlb_standings_codes_setup(v_season);

  #
  # Individual processes
  #
  SET v_num_divs = mlb_standings_num_divs(v_season);
  SET v_num_div_berths = mlb_standings_num_div_berths(v_season);
  SET v_num_wildcard = mlb_standings_num_wildcards(v_season);

  # Playoff Spot
  IF v_season <> 2020 THEN
    CALL mlb_standings_codes_process('x', 'conf', v_num_divs + v_num_wildcard);
  ELSE
    # 2020: Top 2 in each div, plus next two in conf
    CALL mlb_standings_codes_div_conf_mix('x', (v_num_divs * v_num_div_berths) + v_num_wildcard);
    CALL mlb_standings_codes_process('x', 'div', v_num_div_berths);
  END IF;

  # Division Winner
  CALL mlb_standings_codes_process('y', 'div', 1);

  # Wild Card (confirmed)
  CALL mlb_standings_codes_wildcards('x', 'w', 'y');

  # Home-Field (in league)
  CALL mlb_standings_codes_process('z', 'conf', 1);

  # Home-Field (in World Series - 2017 onwards)
  IF v_season >= 2017 THEN
    CALL mlb_standings_codes_process('*', 'league', 1);
  END IF;

END $$

DELIMITER ;
