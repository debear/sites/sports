#
# Playoff history for a season
#
DROP PROCEDURE IF EXISTS `mlb_history_playoffs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_history_playoffs`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Establish which teams did what in the playoffs'
BEGIN

  DECLARE v_date DATE;
  DECLARE v_league_id TINYINT UNSIGNED DEFAULT 1;

  # Determine appropriate standing date
  SELECT MAX(the_date) INTO v_date FROM SPORTS_MLB_STANDINGS WHERE season = v_season;

  #
  # Round-by-Round details
  #
  DROP TEMPORARY TABLE IF EXISTS tmp_round_dates;
  CREATE TEMPORARY TABLE tmp_round_dates (
    season SMALLINT UNSIGNED,
    the_date DATE,
    round_code CHAR(2),
    PRIMARY KEY (season, round_code),
    INDEX all_date (season, the_date, round_code)
  ) ENGINE = MEMORY
    SELECT season, MAX(the_date) AS the_date, round_code
    FROM SPORTS_MLB_PLAYOFF_SERIES
    WHERE season = v_season
    GROUP BY season, round_code;

  DROP TEMPORARY TABLE IF EXISTS tmp_condensed_results;
  CREATE TEMPORARY TABLE tmp_condensed_results (
    season SMALLINT UNSIGNED,
    round TINYINT(3) UNSIGNED,
    round_code VARCHAR(3),
    higher_team VARCHAR(3),
    higher_games TINYINT(1) UNSIGNED,
    lower_team VARCHAR(3),
    lower_games TINYINT(1) UNSIGNED,
    PRIMARY KEY (season, round, higher_team)
  ) ENGINE = MEMORY
    SELECT series.season,
           CAST(SUBSTR(series.round_code, 1, 1) AS UNSIGNED) AS round,
           CASE SUBSTR(series.round_code, 1, 1)
             WHEN '1' THEN 'WC'
             WHEN '2' THEN 'LDS'
             WHEN '3' THEN 'LCS'
             WHEN '4' THEN 'WS'
           END AS round_code,
           higher_seed.team_id AS higher_team,
           series.higher_games,
           lower_seed.team_id AS lower_team,
           series.lower_games
    FROM tmp_round_dates
    LEFT JOIN SPORTS_MLB_PLAYOFF_SERIES AS series
      ON (series.season = tmp_round_dates.season
      AND series.the_date = tmp_round_dates.the_date
      AND series.round_code = tmp_round_dates.round_code)
    LEFT JOIN SPORTS_MLB_PLAYOFF_SEEDS AS higher_seed
      ON (higher_seed.season = series.season
      AND higher_seed.conf_id = series.higher_conf_id
      AND higher_seed.seed = series.higher_seed)
    LEFT JOIN SPORTS_MLB_PLAYOFF_SEEDS AS lower_seed
      ON (lower_seed.season = series.season
      AND lower_seed.conf_id = series.lower_conf_id
      AND lower_seed.seed = series.lower_seed);

  # Insert higher seeded teams
  INSERT INTO SPORTS_MLB_TEAMS_HISTORY_PLAYOFF (season, team_id, round, round_code, opp_team_id, `for`, against)
    SELECT season, higher_team, round, round_code, lower_team, higher_games, lower_games
    FROM tmp_condensed_results
    WHERE higher_team IS NOT NULL
  ON DUPLICATE KEY UPDATE `for` = VALUES(`for`), against = VALUES(against);

  # Insert lower seeded teams
  INSERT INTO SPORTS_MLB_TEAMS_HISTORY_PLAYOFF (season, team_id, round, round_code, opp_team_id, `for`, against)
    SELECT season, lower_team, round, round_code, higher_team, lower_games, higher_games
    FROM tmp_condensed_results
    WHERE lower_team IS NOT NULL
  ON DUPLICATE KEY UPDATE `for` = VALUES(`for`), against = VALUES(against);

  # Re-order
  ALTER TABLE SPORTS_MLB_TEAMS_HISTORY_PLAYOFF ORDER BY team_id, season, round;

  #
  # Progress summaries
  #
  INSERT INTO SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY (season, team_id, wildcard, div_champ, conf_champ, playoff_champ)
    SELECT PLAYOFF.season, PLAYOFF.team_id,
           STANDING.pos_div > 1 AS wildcard,
           MAX(STANDING.pos_div = 1) AS div_champ,
           MAX(PLAYOFF.round_code = 'LCS' AND PLAYOFF.`for` > PLAYOFF.against) AS conf_champ,
           MAX(PLAYOFF.round_code = 'WS' AND PLAYOFF.`for` > PLAYOFF.against) AS playoff_champ
    FROM SPORTS_MLB_TEAMS_HISTORY_PLAYOFF AS PLAYOFF
    JOIN SPORTS_MLB_STANDINGS AS STANDING
      ON (STANDING.season = PLAYOFF.season
      AND STANDING.the_date = v_date
      AND STANDING.team_id = PLAYOFF.team_id)
    WHERE PLAYOFF.season = v_season
    GROUP BY PLAYOFF.season, PLAYOFF.team_id
  ON DUPLICATE KEY UPDATE wildcard = VALUES(wildcard),
                          div_champ = VALUES(div_champ),
                          conf_champ = VALUES(conf_champ),
                          playoff_champ = VALUES(playoff_champ);

  ALTER TABLE SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY ORDER BY team_id, season;

  # And team titles
  INSERT INTO SPORTS_MLB_TEAMS_HISTORY_TITLES (team_id, league_id, div_champ, conf_champ, playoff_champ)
    SELECT PLAYOFF.team_id, v_league_id AS league_id,
           COUNT(DISTINCT IF(IFNULL(PLAYOFF.div_champ, 0) = 1, PLAYOFF.season, NULL)) AS div_champ,
           COUNT(DISTINCT IF(IFNULL(PLAYOFF.conf_champ, 0) = 1, PLAYOFF.season, NULL)) AS conf_champ,
           COUNT(DISTINCT IF(IFNULL(PLAYOFF.playoff_champ, 0) = 1, PLAYOFF.season, NULL)) AS playoff_champ
    FROM SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY AS PLAYOFF
    JOIN SPORTS_MLB_TEAMS_HISTORY_REGULAR AS REGULAR
      ON (REGULAR.team_id = PLAYOFF.team_id
      AND REGULAR.season = PLAYOFF.season)
    GROUP BY PLAYOFF.team_id
  ON DUPLICATE KEY UPDATE div_champ = VALUES(div_champ),
                          conf_champ = VALUES(conf_champ),
                          playoff_champ = VALUES(playoff_champ);

  ALTER TABLE SPORTS_MLB_TEAMS_HISTORY_TITLES ORDER BY team_id, league_id;

END $$

DELIMITER ;

#
# Back-date playoff history
#
DROP PROCEDURE IF EXISTS `mlb_history_playoffs_backdate`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_history_playoffs_backdate`()
    COMMENT 'Backdate team playoff history'
BEGIN

  DECLARE v_league_id TINYINT UNSIGNED DEFAULT 1;

  # Season-by-Season Summary
  TRUNCATE TABLE SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY;
  INSERT INTO SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY (season, team_id, wildcard, div_champ, conf_champ, playoff_champ)
    SELECT PLAYOFF.season, PLAYOFF.team_id,
           STANDING.pos > 1 AND GROUPING_DEF.type = 'div' AS wildcard,
           MAX(STANDING.season <> 1981 AND STANDING.pos = 1 AND GROUPING_DEF.type = 'div') AS div_champ,
           MAX(PLAYOFF.round_code = 'LCS' AND PLAYOFF.`for` > PLAYOFF.against) AS conf_champ,
           MAX(PLAYOFF.round_code = 'WS' AND PLAYOFF.`for` > PLAYOFF.against) AS playoff_champ
    FROM SPORTS_MLB_TEAMS_HISTORY_PLAYOFF AS PLAYOFF
    JOIN SPORTS_MLB_TEAMS_HISTORY_REGULAR AS STANDING
      ON (STANDING.season = PLAYOFF.season
      AND STANDING.team_id = PLAYOFF.team_id)
    JOIN SPORTS_MLB_TEAMS_GROUPINGS AS TEAM_GROUPING
      ON (TEAM_GROUPING.team_id = STANDING.team_id
      AND STANDING.season BETWEEN TEAM_GROUPING.season_from AND IFNULL(TEAM_GROUPING.season_to, 2099))
    LEFT JOIN SPORTS_MLB_GROUPINGS AS GROUPING_DEF
      ON (GROUPING_DEF.grouping_id = TEAM_GROUPING.grouping_id)
    GROUP BY PLAYOFF.season, PLAYOFF.team_id
  ON DUPLICATE KEY UPDATE wildcard = VALUES(wildcard),
                          div_champ = VALUES(div_champ),
                          conf_champ = VALUES(conf_champ),
                          playoff_champ = VALUES(playoff_champ);

  # Merge...
  INSERT INTO SPORTS_MLB_TEAMS_HISTORY_TITLES (team_id, league_id, div_champ, conf_champ, playoff_champ)
    SELECT PLAYOFF.team_id, v_league_id AS league_id,
           COUNT(DISTINCT IF(IFNULL(PLAYOFF.div_champ, 0) = 1, PLAYOFF.season, NULL)) AS div_champ,
           COUNT(DISTINCT IF(IFNULL(PLAYOFF.conf_champ, 0) = 1, PLAYOFF.season, NULL)) AS conf_champ,
           COUNT(DISTINCT IF(IFNULL(PLAYOFF.playoff_champ, 0) = 1, PLAYOFF.season, NULL)) AS playoff_champ
    FROM SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY AS PLAYOFF
    JOIN SPORTS_MLB_TEAMS_HISTORY_REGULAR AS REGULAR
      ON (REGULAR.team_id = PLAYOFF.team_id
      AND REGULAR.season = PLAYOFF.season)
    GROUP BY PLAYOFF.team_id
  ON DUPLICATE KEY UPDATE div_champ = VALUES(div_champ),
                          conf_champ = VALUES(conf_champ),
                          playoff_champ = VALUES(playoff_champ);

  ALTER TABLE SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY ORDER BY team_id, season;
  ALTER TABLE SPORTS_MLB_TEAMS_HISTORY_TITLES ORDER BY team_id, league_id;

END $$

DELIMITER ;
