##
## Standings info for tie-breaking
##
DROP PROCEDURE IF EXISTS `mlb_standings_calcs_tiebreak`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_calcs_tiebreak`()
    COMMENT 'Tie-break data in MLB standings'
BEGIN

  # Note: We can only deal with absolutes at this stage, as "head-to-head" tie-break needs to know the teams involved, which we do not at this stage

  # League / Conf / Div IDs
  UPDATE tmp_standings
  JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_standings.team_id)
  SET tmp_standings._tb_league_id = tmp_teams.league_id,
      tmp_standings._tb_conf_id = tmp_teams.conf_id,
      tmp_standings._tb_div_id = tmp_teams.div_id;

  # Intra-Division Points Percentage (intra == "within")
  INSERT INTO tmp_standings (season, the_date, team_id, _tb_intra_div_pct)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) / IF(IFNULL(SUM(tmp_sched.result = 'w'), 0) + IFNULL(SUM(tmp_sched.result = 'l'), 0) = 0, 1, -- Div by zero check...
                                                       IFNULL(SUM(tmp_sched.result = 'w'), 0) + IFNULL(SUM(tmp_sched.result = 'l'), 0)) AS _tb_intra_div_pct
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_div_opp = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE _tb_intra_div_pct = VALUES(_tb_intra_div_pct);

  # Intra-Conference Points Percentage (intra == "within")
  INSERT INTO tmp_standings (season, the_date, team_id, _tb_intra_conf_pct)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) / IF(IFNULL(SUM(tmp_sched.result = 'w'), 0) + IFNULL(SUM(tmp_sched.result = 'l'), 0) = 0, 1, -- Div by zero check...
                                                       IFNULL(SUM(tmp_sched.result = 'w'), 0) + IFNULL(SUM(tmp_sched.result = 'l'), 0)) AS _tb_intra_conf_pct
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_conf_opp = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE _tb_intra_conf_pct = VALUES(_tb_intra_conf_pct);

  # Get the appropriate (conf) game counter for each team for each date
  DROP TEMPORARY TABLE IF EXISTS tmp_standings_recent_max;
  CREATE TEMPORARY TABLE tmp_standings_recent_max (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    min_game_num TINYINT UNSIGNED,
    max_game_num TINYINT UNSIGNED,
    PRIMARY KEY (season, the_date, team_id),
    INDEX (team_id, min_game_num, max_game_num)
  ) ENGINE = MEMORY
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           0 AS min_game_num,
           IFNULL(MAX(tmp_sched.game_num), 0) AS max_game_num
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_conf_opp = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id;

  # Records in final 81..85 intra-conference games (theoretically could keep going, but not sure we'll ever need it??)
  CALL mlb_standings_calcs_tiebreak_prev_intraconf(81);
  CALL mlb_standings_calcs_tiebreak_prev_intraconf(82);
  CALL mlb_standings_calcs_tiebreak_prev_intraconf(83);
  CALL mlb_standings_calcs_tiebreak_prev_intraconf(84);
  CALL mlb_standings_calcs_tiebreak_prev_intraconf(85);

END $$

DELIMITER ;

#
# Tie-breaking based on win %age of the previous X intra-conference games
#
DROP PROCEDURE IF EXISTS `mlb_standings_calcs_tiebreak_prev_intraconf`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_calcs_tiebreak_prev_intraconf`(
  v_prev_games TINYINT UNSIGNED
)
    COMMENT 'Tie-break prev intra-conf data in MLB standings'
BEGIN

  # Determine the range
  CALL _duplicate_tmp_table('tmp_standings_recent_max', 'tmp_standings_recent');
  UPDATE tmp_standings_recent
  SET min_game_num = IF(max_game_num < v_prev_games, 0, max_game_num - v_prev_games + 1); # +1 because Last 10 => 6..15, not 5..15

  # Run
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, _tb_prev', v_prev_games, '_intraconf_pct)
      SELECT tmp_standings_recent.season,
             tmp_standings_recent.the_date,
             tmp_standings_recent.team_id,
             IFNULL(SUM(tmp_sched.result = \'w\'), 0) / IF(IFNULL(SUM(tmp_sched.result = \'w\'), 0) + IFNULL(SUM(tmp_sched.result = \'l\'), 0) = 0, 1, -- Div by zero check...
                                                           IFNULL(SUM(tmp_sched.result = \'w\'), 0) + IFNULL(SUM(tmp_sched.result = \'l\'), 0)) AS _tb_prev', v_prev_games, '_intraconf_pct
      FROM tmp_date_list
      JOIN tmp_teams ON (1 = 1)
      JOIN tmp_standings_recent
        ON (tmp_standings_recent.season = tmp_date_list.season
        AND tmp_standings_recent.the_date = tmp_date_list.the_date
        AND tmp_standings_recent.team_id = tmp_teams.team_id)
      LEFT JOIN tmp_sched
        ON (tmp_sched.team_id = tmp_teams.team_id
        AND tmp_sched.is_conf_opp = 1
        AND tmp_sched.game_num_conf BETWEEN tmp_standings_recent.min_game_num AND tmp_standings_recent.max_game_num)
      GROUP BY tmp_date_list.season,
               tmp_date_list.the_date,
               tmp_teams.team_id
    ON DUPLICATE KEY UPDATE _tb_prev', v_prev_games, '_intraconf_pct = VALUES(_tb_prev', v_prev_games, '_intraconf_pct);
  '));

END $$

DELIMITER ;
