#
# Pitcher-based split and situational stats
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_pitchers`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_pitchers`()
    COMMENT 'Determine split/situational stats for pitchers over a season'
BEGIN

  # Prepare the temporary table
  CALL mlb_totals_players_splits_pitchers_setup();

  # As SP / RP
  CALL mlb_totals_players_splits_pitchers_calc('pitcher-info', 'IF(BASE.started, "As SP", "As RP")', 'BASE.started', NULL);

  # v LHB / LHB
  CALL mlb_totals_players_splits_pitchers_calc('batter-info', 'IF(AB.batter_hand = "L", "v LHB", "v RHB")', 'AB.batter_hand', NULL);

  # Home / Road
  CALL mlb_totals_players_splits_pitchers_calc('home-road', 'IF(BASE.is_home, "Home", "Road")', 'BASE.is_home', NULL);

  # By Month
  CALL mlb_totals_players_splits_pitchers_calc('month', 'BASE.month', 'BASE.month', NULL);

  # Pre-/Post-ASB (regular season only)
  CALL mlb_totals_players_splits_pitchers_calc('asb', 'IF(BASE.pre_asb, "Pre All-Star Break", "Post All-Star Break")', 'BASE.pre_asb', 'BASE.game_type = "regular"');

  # By Opp
  CALL mlb_totals_players_splits_pitchers_calc('opponent', 'BASE.opp_team_id', 'BASE.opp_team_id', NULL);

  # By Stadium
  CALL mlb_totals_players_splits_pitchers_calc('stadium', 'BASE.stadium', 'BASE.stadium', NULL);

  # Bases Empty, Runners On, RISP, RISP w/2 outs, Bases Loaded, Man on 3rd <2 outs
  CALL mlb_totals_players_splits_pitchers_calc('with-baserunners', '"Bases Empty"', NULL, 'AB.on_base = 0');
  CALL mlb_totals_players_splits_pitchers_calc('with-baserunners', '"Runners On"', NULL, 'AB.on_base > 0');
  CALL mlb_totals_players_splits_pitchers_calc('with-baserunners', '"RISP"', NULL, 'AB.risp = 1');
  CALL mlb_totals_players_splits_pitchers_calc('with-baserunners', '"RISP, 2 Outs"', NULL, 'AB.risp = 1 AND AB.out = 2');
  CALL mlb_totals_players_splits_pitchers_calc('with-baserunners', '"Bases Loaded"', NULL, 'AB.on_base = 7');
  CALL mlb_totals_players_splits_pitchers_calc('with-baserunners', '"Man on 3rd, <2 Outs"', NULL, 'FIND_IN_SET("3rd", AB.on_base) > 0 AND AB.out < 2');

  # By Counts (on & after)
  CALL mlb_totals_players_splits_pitchers_calc('counts-on', 'CONCAT("Count ", AB.count_final)', 'AB.count_final', NULL);
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 0-1"', NULL, 'FIND_IN_SET("0-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 0-2"', NULL, 'FIND_IN_SET("0-2", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 1-0"', NULL, 'FIND_IN_SET("1-0", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 1-1"', NULL, 'FIND_IN_SET("1-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 1-2"', NULL, 'FIND_IN_SET("1-2", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 2-0"', NULL, 'FIND_IN_SET("2-0", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 2-1"', NULL, 'FIND_IN_SET("2-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 2-2"', NULL, 'FIND_IN_SET("2-2", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 3-0"', NULL, 'FIND_IN_SET("3-0", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 3-1"', NULL, 'FIND_IN_SET("3-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_pitchers_calc('counts-after', '"After 3-2"', NULL, 'FIND_IN_SET("3-2", AB.counts) > 0');

  # By Pitch Counts (at end of AB? 1-10, 11-20, etc)
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 1-10"',   NULL, 'AB.pitcher_pitches BETWEEN 1 AND 10');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 11-20"',  NULL, 'AB.pitcher_pitches BETWEEN 11 AND 20');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 21-30"',  NULL, 'AB.pitcher_pitches BETWEEN 21 AND 30');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 31-40"',  NULL, 'AB.pitcher_pitches BETWEEN 31 AND 40');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 41-50"',  NULL, 'AB.pitcher_pitches BETWEEN 41 AND 50');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 51-60"',  NULL, 'AB.pitcher_pitches BETWEEN 51 AND 60');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 61-70"',  NULL, 'AB.pitcher_pitches BETWEEN 61 AND 70');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 71-80"',  NULL, 'AB.pitcher_pitches BETWEEN 71 AND 80');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 81-90"',  NULL, 'AB.pitcher_pitches BETWEEN 81 AND 90');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 91-100"', NULL, 'AB.pitcher_pitches BETWEEN 91 AND 100');
  CALL mlb_totals_players_splits_pitchers_calc('pitch-count', '"Pitches 101+"',   NULL, 'AB.pitcher_pitches > 100');

  # By AB Pitch Length
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"1 Pitch"',     NULL, 'AB.ab_pitches = 1');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"2 Pitches"',   NULL, 'AB.ab_pitches = 2');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"3 Pitches"',   NULL, 'AB.ab_pitches = 3');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"4 Pitches"',   NULL, 'AB.ab_pitches = 4');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"5 Pitches"',   NULL, 'AB.ab_pitches = 5');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"6 Pitches"',   NULL, 'AB.ab_pitches = 6');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"7 Pitches"',   NULL, 'AB.ab_pitches = 7');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"8 Pitches"',   NULL, 'AB.ab_pitches = 8');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"9 Pitches"',   NULL, 'AB.ab_pitches = 9');
  CALL mlb_totals_players_splits_pitchers_calc('ab-length', '"10+ Pitches"', NULL, 'AB.ab_pitches >= 10');

  # By Inning
  CALL mlb_totals_players_splits_pitchers_calc('inning', 'CONCAT(display_ordinal_number(AB.inning), " Inning")', 'AB.inning', NULL);

  # v Batting Order
  CALL mlb_totals_players_splits_pitchers_calc('batting-order', 'CONCAT("Batting ", display_ordinal_number(AB.batting_order))', 'AB.batting_order', NULL);

  # Commit any changes
  CALL mlb_totals_players_splits_pitchers_commit();

  # Career versions (not linked to this season)
  CALL mlb_totals_players_splits_pitchers_career();

END $$

DELIMITER ;

# Prepare our work
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_pitchers_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_pitchers_setup`()
    COMMENT 'Pitcher split stat preparation'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_PITCHING_SPLITS;
  CREATE TEMPORARY TABLE tmp_PITCHING_SPLITS LIKE SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS;
  ALTER TABLE tmp_PITCHING_SPLITS
    ENGINE = MEMORY,
    MODIFY COLUMN split_label VARCHAR(50);

  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_GAME_PITCHING;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_GAME_PITCHING LIKE SPORTS_MLB_PLAYERS_GAME_PITCHING;
  INSERT INTO tmp_SPORTS_MLB_PLAYERS_GAME_PITCHING
    SELECT STATS.*
    FROM tmp_splits_rosters AS BASE
    JOIN SPORTS_MLB_PLAYERS_GAME_PITCHING AS STATS
      ON (STATS.season = BASE.season
      AND STATS.game_type = BASE.game_type
      AND STATS.game_id = BASE.game_id
      AND STATS.player_id = BASE.player_id);

END $$

DELIMITER ;

# Worker method
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_pitchers_calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_pitchers_calc`(
  v_split_type VARCHAR(50),
  v_split_label VARCHAR(100),
  v_group_by VARCHAR(255),
  v_where VARCHAR(255)
)
    COMMENT 'Pitcher split stat calculation'
BEGIN

  CALL _exec(CONCAT('
    INSERT INTO tmp_PITCHING_SPLITS (season, season_type, player_id, split_type, split_label, gp, gs, bf, ab, obp_evt, h, hr, bb, ibb, hbp, k, tb)
      SELECT AB.season, AB.game_type AS season_type, AB.pitcher AS player_id,
             "', v_split_type, '" AS split_type,
             ', v_split_label, ' AS split_label,
             COUNT(DISTINCT AB.game_id) AS gp,
             COUNT(DISTINCT IF(BASE.started, AB.game_id, NULL)) AS gs,
             COUNT(*) AS bf,
             SUM(AB.is_ab) AS ab,
             SUM(AB.is_obp_evt) AS obp_evt,
             SUM(AB.is_hit) AS h,
             SUM(AB.tb = 4) AS hr,
             SUM(AB.is_bb) AS bb,
             SUM(AB.is_ibb) AS ibb,
             SUM(AB.is_hbp) AS hbp,
             SUM(AB.is_k) AS k,
             SUM(AB.tb) AS tb
      FROM tmp_splits_rosters AS BASE
      JOIN tmp_SPORTS_MLB_GAME_ATBAT_FLAGS AS AB
        ON (AB.season = BASE.season
        AND AB.game_type = BASE.game_type
        AND AB.game_id = BASE.game_id
        AND AB.pitcher = BASE.player_id
        AND AB.is_pa = 1)
      ', IF(v_where IS NOT NULL, CONCAT('WHERE ', v_where), ''), '
      GROUP BY AB.season, AB.game_type, AB.pitcher', IF(v_group_by IS NOT NULL, CONCAT(', ', v_group_by), ''), '
    ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                            gs = VALUES(gs),
                            bf = VALUES(bf),
                            ab = VALUES(ab),
                            obp_evt = VALUES(obp_evt),
                            h = VALUES(h),
                            hr = VALUES(hr),
                            bb = VALUES(bb),
                            ibb = VALUES(ibb),
                            hbp = VALUES(hbp),
                            k = VALUES(k),
                            tb = VALUES(tb);'));

  # Split stats, which are more relevant game-wide stats than cumulation of ABs
  IF SUBSTRING(v_group_by, 1, 5) = 'BASE.' THEN
    CALL _exec(CONCAT('
      INSERT INTO tmp_PITCHING_SPLITS (season, season_type, player_id, split_type, split_label, `out`, pt, b, s, r, er, wp, bk, sb, cs, cg, sho, qs, w, l, sv, hld, bs, svo, winprob, game_score)
        SELECT BASE.season, BASE.game_type, BASE.player_id,
               "', v_split_type, '" AS split_type,
               ', v_split_label, ' AS split_label,
               SUM(STATS.`out`) AS `out`,
               SUM(STATS.pt) AS pt,
               SUM(STATS.b) AS b,
               SUM(STATS.s) AS s,
               SUM(STATS.r) AS r,
               SUM(STATS.er) AS er,
               SUM(STATS.wp) AS wp,
               SUM(STATS.bk) AS bk,
               SUM(STATS.sb) AS sb,
               SUM(STATS.cs) AS cs,
               SUM(STATS.cg) AS cg,
               SUM(STATS.sho) AS sho,
               SUM(STATS.qs) AS qs,
               SUM(STATS.w) AS w,
               SUM(STATS.l) AS l,
               SUM(STATS.sv) AS sv,
               SUM(STATS.hld) AS hld,
               SUM(STATS.bs) AS bs,
               SUM(STATS.svo) AS svo,
               SUM(STATS.winprob) AS winprob,
               IF(SUM(STATS.game_score IS NOT NULL), FORMAT(SUM(IFNULL(STATS.game_score, 0)) / SUM(STATS.game_score IS NOT NULL), 2), NULL) AS game_score
        FROM tmp_splits_rosters AS BASE
        JOIN tmp_SPORTS_MLB_PLAYERS_GAME_PITCHING AS STATS
          ON (STATS.season = BASE.season
          AND STATS.game_type = BASE.game_type
          AND STATS.game_id = BASE.game_id
          AND STATS.player_id = BASE.player_id)
        ', IF(v_where IS NOT NULL, CONCAT('WHERE ', v_where), ''), '
        GROUP BY BASE.season, BASE.game_type, BASE.player_id, ', v_group_by, '
      ON DUPLICATE KEY UPDATE `out` = VALUES(`out`),
                              pt = VALUES(pt),
                              b = VALUES(b),
                              s = VALUES(s),
                              r = VALUES(r),
                              er = VALUES(er),
                              wp = VALUES(wp),
                              bk = VALUES(bk),
                              sb = VALUES(sb),
                              cs = VALUES(cs),
                              cg = VALUES(cg),
                              sho = VALUES(sho),
                              qs = VALUES(qs),
                              w = VALUES(w),
                              l = VALUES(l),
                              sv = VALUES(sv),
                              hld = VALUES(hld),
                              bs = VALUES(bs),
                              svo = VALUES(svo),
                              winprob = VALUES(winprob),
                              game_score = VALUES(game_score);'));
  END IF;

END $$

DELIMITER ;

# Save changes back to the database
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_pitchers_commit`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_pitchers_commit`()
    COMMENT 'Pitcher split stat storing to main table'
BEGIN

  # Composite calcs
  UPDATE tmp_PITCHING_SPLITS
  SET ip = CONCAT(FLOOR(`out` / 3), '.', `out` % 3),
      ra = IF(`out` > 0, (27 * r) / `out`, NULL),
      era = IF(`out` > 0, (27 * er) / `out`, NULL),
      whip = IF(`out` > 0, ((bb + h) * 3) / `out`, NULL),
      avg_agst = IF(ab > 0, h / ab, NULL),
      obp_agst = IF(obp_evt > 0, (h + bb + hbp) / obp_evt, NULL),
      slg_agst = IF(ab > 0, tb / ab, NULL),
      ops_agst = IF(obp_evt > 0, ((h + bb + hbp) / obp_evt) + IF(ab > 0, tb / ab, 0), NULL);

  # Convert the split_labels in to an ID
  INSERT IGNORE INTO SPORTS_MLB_PLAYERS_SPLIT_LABELS (split_id, split_type, split_label, created)
    SELECT DISTINCT NULL, split_type, split_label, NOW() as created
    FROM tmp_PITCHING_SPLITS
    ORDER BY split_type, split_label;
  UPDATE tmp_PITCHING_SPLITS AS SPLITS
  JOIN SPORTS_MLB_PLAYERS_SPLIT_LABELS AS LABELS
    ON (LABELS.split_type = SPLITS.split_type
    AND LABELS.split_label = SPLITS.split_label)
  SET SPLITS.split_label = LABELS.split_id;

  # Save to the main table
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS
    SELECT *
    FROM tmp_PITCHING_SPLITS
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          gs = VALUES(gs),
                          ip = VALUES(ip),
                          `out` = VALUES(`out`),
                          bf = VALUES(bf),
                          ab = VALUES(ab),
                          pt = VALUES(pt),
                          b = VALUES(b),
                          s = VALUES(s),
                          obp_evt = VALUES(obp_evt),
                          h = VALUES(h),
                          hr = VALUES(hr),
                          bb = VALUES(bb),
                          ibb = VALUES(ibb),
                          r = VALUES(r),
                          ra = VALUES(ra),
                          er = VALUES(er),
                          era = VALUES(era),
                          whip = VALUES(whip),
                          hbp = VALUES(hbp),
                          k = VALUES(k),
                          tb = VALUES(tb),
                          wp = VALUES(wp),
                          bk = VALUES(bk),
                          sb = VALUES(sb),
                          cs = VALUES(cs),
                          avg_agst = VALUES(avg_agst),
                          obp_agst = VALUES(obp_agst),
                          slg_agst = VALUES(slg_agst),
                          ops_agst = VALUES(ops_agst),
                          cg = VALUES(cg),
                          sho = VALUES(sho),
                          qs = VALUES(qs),
                          w = VALUES(w),
                          l = VALUES(l),
                          sv = VALUES(sv),
                          hld = VALUES(hld),
                          bs = VALUES(bs),
                          svo = VALUES(svo),
                          winprob = VALUES(winprob),
                          game_score = VALUES(game_score);

END $$

DELIMITER ;

# Career stats
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_pitchers_career`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_pitchers_career`()
    COMMENT 'Pitcher split stat over a career'
BEGIN

  # Up memory limit to 256meg for the temporary table (as data is in FIXED format, which is wider, and oft exceeds the 16meg default)
  SET @@session.max_heap_table_size = 268435456; # 268,435,456 = 256 * 1024 * 1024
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

  # Copy appropriate stats into a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_splits_season;
  CREATE TEMPORARY TABLE tmp_splits_season LIKE SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS;
  ALTER TABLE tmp_splits_season ENGINE = MEMORY;
  INSERT INTO tmp_splits_season
    SELECT STATS.*
    FROM tmp_split_players
    JOIN SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS AS STATS
      ON (STATS.player_id = tmp_split_players.player_id);

  # Then aggregate
  INSERT INTO SPORTS_MLB_PLAYERS_CAREER_PITCHING_SPLITS (player_id, season_type, split_type, split_label, gp, gs, ip, `out`, bf, ab, pt, b, s, obp_evt, h, hr, bb, ibb, r, ra, er, era, whip, hbp, k, tb, wp, bk, sb, cs, avg_agst, obp_agst, slg_agst, ops_agst, cg, sho, qs, w, l, sv, hld, bs, svo, winprob)
    SELECT player_id, season_type, split_type, split_label,
           SUM(gp) AS gp,
           SUM(gs) AS gs,
           CONCAT(FLOOR(SUM(`out`) / 3), '.', SUM(`out`) % 3) AS ip,
           SUM(`out`) AS `out`,
           SUM(bf) AS bf,
           SUM(ab) AS ab,
           SUM(pt) AS pt,
           SUM(b) AS b,
           SUM(s) AS s,
           SUM(obp_evt) AS obp_evt,
           SUM(h) AS h,
           SUM(hr) AS hr,
           SUM(bb) AS bb,
           SUM(ibb) AS ibb,
           SUM(r) AS r,
           IF(SUM(`out`) > 0, (27 * SUM(r)) / SUM(`out`), NULL) AS ra,
           SUM(er) AS er,
           IF(SUM(`out`) > 0, (27 * SUM(er)) / SUM(`out`), NULL) AS era,
           IF(SUM(`out`) > 0, ((SUM(bb) + SUM(h)) * 3) / SUM(`out`), NULL) AS whip,
           SUM(hbp) AS hbp,
           SUM(k) AS k,
           SUM(tb) AS tb,
           SUM(wp) AS wp,
           SUM(bk) AS bk,
           SUM(sb) AS sb,
           SUM(cs) AS cs,
           IF(SUM(ab) > 0, SUM(h) / SUM(ab), NULL) AS avg_agst,
           IF(SUM(obp_evt) > 0, SUM(h + bb + hbp) / SUM(obp_evt), NULL) AS obp_agst,
           IF(SUM(ab) > 0, SUM(tb) / SUM(ab), NULL) AS slg_agst,
           IF(SUM(obp_evt) > 0, (SUM(h + bb + hbp) / SUM(obp_evt)) + IF(SUM(ab) > 0, SUM(tb) / SUM(ab), 0), NULL) AS ops_agst,
           SUM(cg) AS cg,
           SUM(sho) AS sho,
           SUM(qs) AS qs,
           SUM(w) AS w,
           SUM(l) AS l,
           SUM(sv) AS sv,
           SUM(hld) AS hld,
           SUM(bs) AS bs,
           SUM(svo) AS svo,
           SUM(winprob) AS winprob
    FROM tmp_splits_season
    WHERE split_type IN ("pitcher-info", "home-road", "month", "asb", "opponent", "stadium")
    GROUP BY player_id, season_type, split_type, split_label
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          gs = VALUES(gs),
                          ip = VALUES(ip),
                          `out` = VALUES(`out`),
                          bf = VALUES(bf),
                          ab = VALUES(ab),
                          pt = VALUES(pt),
                          b = VALUES(b),
                          s = VALUES(s),
                          obp_evt = VALUES(obp_evt),
                          h = VALUES(h),
                          hr = VALUES(hr),
                          bb = VALUES(bb),
                          ibb = VALUES(ibb),
                          r = VALUES(r),
                          ra = VALUES(ra),
                          er = VALUES(er),
                          era = VALUES(era),
                          whip = VALUES(whip),
                          hbp = VALUES(hbp),
                          k = VALUES(k),
                          tb = VALUES(tb),
                          wp = VALUES(wp),
                          bk = VALUES(bk),
                          sb = VALUES(sb),
                          cs = VALUES(cs),
                          avg_agst = VALUES(avg_agst),
                          obp_agst = VALUES(obp_agst),
                          slg_agst = VALUES(slg_agst),
                          ops_agst = VALUES(ops_agst),
                          cg = VALUES(cg),
                          sho = VALUES(sho),
                          qs = VALUES(qs),
                          w = VALUES(w),
                          l = VALUES(l),
                          sv = VALUES(sv),
                          hld = VALUES(hld),
                          bs = VALUES(bs),
                          svo = VALUES(svo),
                          winprob = VALUES(winprob);

  # Revert the memory change
  SET @@session.max_heap_table_size = DEFAULT; # DEFAULT = global value (i.e., what we had before...)
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

END $$

DELIMITER ;
