##
## Season Series Point Percentage
##
DROP PROCEDURE IF EXISTS `mlb_standings_ties_h2h_pct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_ties_h2h_pct`()
    COMMENT 'Attempt to break MLB standing ties on H2H Win %age'
BEGIN

  # Determine common game results
  CALL mlb_standings_ties_common();
  CALL mlb_standings_ties_h2h_pct_process();

  # Process
  INSERT INTO tmp_standings (season, the_date, team_id, _tb_pos_adj)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           COUNT(tmp_standings_cpB.team_id) AS _tb_pos_adj
    FROM tmp_standings_cpA
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos = tmp_standings_cpA._tb_pos
      AND tmp_standings_cpB._tb_h2h_pct > tmp_standings_cpA._tb_h2h_pct)
    WHERE tmp_standings_cpA._tb_is_tied = 1
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
  ON DUPLICATE KEY UPDATE _tb_pos_adj = VALUES(_tb_pos_adj);

END $$

DELIMITER ;

#
# Process the common game list to determine each teams point percentage
#
DROP PROCEDURE IF EXISTS `mlb_standings_ties_h2h_pct_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_ties_h2h_pct_process`()
    COMMENT 'Process MLB standing ties to determine Series Pts %age'
BEGIN

  UPDATE tmp_standings
  JOIN tmp_standings_common_sched
    ON (tmp_standings_common_sched.season = tmp_standings.season
    AND tmp_standings_common_sched.the_date = tmp_standings.the_date
    AND tmp_standings_common_sched.team_id = tmp_standings.team_id)
  SET tmp_standings._tb_h2h_pct = tmp_standings_common_sched.win_pct;
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

END $$

DELIMITER ;

##
## Determine common games for our tied teams
##
DROP PROCEDURE IF EXISTS `mlb_standings_ties_common`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_ties_common`()
    COMMENT 'Identify MLB standing tie common games'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_standings_common_teams;
  CREATE TEMPORARY TABLE tmp_standings_common_teams (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    opp_team_id VARCHAR(3),
    PRIMARY KEY (season, the_date, team_id, opp_team_id)
  ) ENGINE = MEMORY
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id, tmp_standings_cpB.team_id AS opp_team_id
    FROM tmp_standings_cpA
    JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos = tmp_standings_cpA._tb_pos
      AND tmp_standings_cpB.team_id <> tmp_standings_cpA.team_id)
    WHERE tmp_standings_cpA._tb_is_tied = 1
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_standings_common_sched;
  CREATE TEMPORARY TABLE tmp_standings_common_sched (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    num_opp TINYINT UNSIGNED,
    num_gp TINYINT UNSIGNED,
    num_w TINYINT UNSIGNED,
    num_l TINYINT UNSIGNED,
    win_pct DECIMAL(5,4)
  ) ENGINE = MEMORY
    SELECT tmp_standings_common_teams.season, tmp_standings_common_teams.the_date, tmp_standings_common_teams.team_id,
           COUNT(DISTINCT tmp_standings_common_teams.opp_team_id) AS num_opp,
           IFNULL(COUNT(tmp_sched.game_id), 0) AS num_gp,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS num_w,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS num_l,
           NULL AS win_pct
    FROM tmp_standings_common_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_standings_common_teams.team_id
      AND tmp_sched.opp_id = tmp_standings_common_teams.opp_team_id
      AND tmp_sched.game_date <= tmp_standings_common_teams.the_date)
    GROUP BY tmp_standings_common_teams.season, tmp_standings_common_teams.the_date, tmp_standings_common_teams.team_id;

  UPDATE tmp_standings_common_sched
  SET win_pct = IF(num_gp > 0, num_w / num_gp, 0);

END $$

DELIMITER ;
