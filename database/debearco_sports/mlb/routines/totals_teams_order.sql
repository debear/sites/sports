##
## Team total table maintenance
##
DROP PROCEDURE IF EXISTS `mlb_totals_teams_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams_order`()
    COMMENT 'Order the MLB team total tables'
BEGIN

  # Game totals
  ALTER TABLE SPORTS_MLB_TEAMS_GAME_BATTING ORDER BY season, game_type, game_id, team_id;
  ALTER TABLE SPORTS_MLB_TEAMS_GAME_FIELDING ORDER BY season, game_type, game_id, team_id;
  ALTER TABLE SPORTS_MLB_TEAMS_GAME_PITCHING ORDER BY season, game_type, game_id, team_id;

  # Season totals
  ALTER TABLE SPORTS_MLB_TEAMS_SEASON_BATTING ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_MLB_TEAMS_SEASON_FIELDING ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_MLB_TEAMS_SEASON_PITCHING ORDER BY season, season_type, team_id;

  # Sorted totals
  ALTER TABLE SPORTS_MLB_TEAMS_SEASON_BATTING_SORTED ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_MLB_TEAMS_SEASON_FIELDING_SORTED ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_MLB_TEAMS_SEASON_PITCHING_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;
