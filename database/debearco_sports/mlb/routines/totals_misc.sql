#
# Miscellaneous game stats
#
DROP PROCEDURE IF EXISTS `mlb_totals_misc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_misc`()
    COMMENT 'MLB misc totals to be calced'
BEGIN

  CALL mlb_totals_umpire_zones();

END $$

DELIMITER ;
