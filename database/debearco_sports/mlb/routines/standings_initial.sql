#
# Initial standings
#
DROP PROCEDURE IF EXISTS `mlb_standings_initial`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_initial`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Create the initial MLB standings for a season'
BEGIN

  # Declare our vars
  DECLARE v_date DATE;

  # Get meta out
  SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) INTO v_date FROM SPORTS_MLB_SCHEDULE WHERE season = v_season;

  # Get the list of teams
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    city VARCHAR(20),
    franchise VARCHAR(20),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id),
    INDEX by_def (team_id, conf_id, div_id)
  ) ENGINE = MEMORY
    SELECT TEAM_DIV.team_id, TEAM.city, TEAM.franchise,
           DIVN.parent_id AS conf_id,
           DIVN.grouping_id AS div_id
    FROM SPORTS_MLB_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_MLB_TEAMS AS TEAM
      ON (TEAM.team_id = TEAM_DIV.team_id)
    JOIN SPORTS_MLB_GROUPINGS AS DIVN
      ON (DIVN.grouping_id = TEAM_DIV.grouping_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099)
    ORDER BY TEAM_DIV.team_id;
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # Store our table
  DELETE FROM SPORTS_MLB_STANDINGS WHERE season = v_season;
  INSERT INTO SPORTS_MLB_STANDINGS (season, the_date, team_id, wins, loss, ties, win_pct, runs_for, runs_against, home_wins, home_loss, home_ties, visitor_wins, visitor_loss, visitor_ties, conf_wins, conf_loss, conf_ties, div_wins, div_loss, div_ties, recent_wins, recent_loss, recent_ties, extras_wins, extras_loss, extras_ties, onerun_wins, onerun_loss, onerun_ties, pos_league, pos_conf, pos_div, games_back_div, games_back_wildcard, status_code)
    SELECT v_season, v_date, tmp_teams.team_id,
           0, 0, 0, 0.0000, 0, 0,
           0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0,
           99, 99, 99,
           NULL, NULL, NULL
    FROM tmp_teams;

  # Sort league
  INSERT INTO SPORTS_MLB_STANDINGS (season, the_date, team_id, pos_league)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_league = VALUES(pos_league);

  # Sort conference
  INSERT INTO SPORTS_MLB_STANDINGS (season, the_date, team_id, pos_conf)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.conf_id = ME.conf_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_conf = VALUES(pos_conf);

  # Sort division
  INSERT INTO SPORTS_MLB_STANDINGS (season, the_date, team_id, pos_div)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.conf_id = ME.conf_id
      AND OTHER.div_id = ME.div_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_div = VALUES(pos_div);

  ALTER TABLE SPORTS_MLB_STANDINGS ORDER BY season, the_date, team_id;

END $$

DELIMITER ;
