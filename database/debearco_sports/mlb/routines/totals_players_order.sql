##
## Player total table maintenance
##
DROP PROCEDURE IF EXISTS `mlb_totals_players_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_order`()
    COMMENT 'Order the MLB player total tables'
BEGIN

  # Game totals
  ALTER TABLE SPORTS_MLB_PLAYERS_GAME_BATTING ORDER BY season, game_type, game_id, player_id;
  ALTER TABLE SPORTS_MLB_PLAYERS_GAME_FIELDING ORDER BY season, game_type, game_id, player_id;
  ALTER TABLE SPORTS_MLB_PLAYERS_GAME_PITCHING ORDER BY season, game_type, game_id, player_id;

  # Y2D totals
  ALTER TABLE SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D ORDER BY season, game_type, game_id, player_id;
  ALTER TABLE SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D ORDER BY season, game_type, game_id, player_id;
  ALTER TABLE SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D ORDER BY season, game_type, game_id, player_id;

  # Season totals
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_MISC ORDER BY season, season_type, player_id, is_totals;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_BATTING ORDER BY season, season_type, player_id, is_totals;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_FIELDING ORDER BY season, season_type, player_id, is_totals;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_PITCHING ORDER BY season, season_type, player_id, is_totals;

  # Sorted totals
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED ORDER BY season, season_type, player_id;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED ORDER BY season, season_type, player_id;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED ORDER BY season, season_type, player_id;

END $$

DELIMITER ;
