##
## Base MLB standings
##
DROP PROCEDURE IF EXISTS `mlb_standings_calcs_main`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_calcs_main`()
    COMMENT 'Establish MLB teams W/L records'
BEGIN

  # Home Games
  INSERT INTO tmp_standings (season, the_date, team_id, home_wins, home_loss, home_ties)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS home_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS home_loss,
           IFNULL(SUM(tmp_sched.result = 't'), 0) AS home_ties
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.venue = 'home'
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE home_wins = VALUES(home_wins),
                          home_loss = VALUES(home_loss),
                          home_ties = VALUES(home_ties);

  # Road Games
  INSERT INTO tmp_standings (season, the_date, team_id, visitor_wins, visitor_loss, visitor_ties)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS visitor_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS visitor_loss,
           IFNULL(SUM(tmp_sched.result = 't'), 0) AS visitor_ties
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.venue = 'visitor'
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE visitor_wins = VALUES(visitor_wins),
                          visitor_loss = VALUES(visitor_loss),
                          visitor_ties = VALUES(visitor_ties);

  # Calculate totals based on home/road split
  UPDATE tmp_date_list
  JOIN tmp_standings
    ON (tmp_standings.season = tmp_date_list.season
    AND tmp_standings.the_date = tmp_date_list.the_date)
  SET tmp_standings.wins = tmp_standings.home_wins + tmp_standings.visitor_wins,
      tmp_standings.loss = tmp_standings.home_loss + tmp_standings.visitor_loss,
      tmp_standings.ties = tmp_standings.home_ties + tmp_standings.visitor_ties;

  # Win Percentages
  UPDATE tmp_date_list
  JOIN tmp_standings
    ON (tmp_standings.season = tmp_date_list.season
    AND tmp_standings.the_date = tmp_date_list.the_date)
  JOIN tmp_teams_max_gp
    ON (tmp_teams_max_gp.team_id = tmp_standings.team_id)
  SET tmp_standings.win_pct   = tmp_standings.wins / IF(tmp_standings.wins + tmp_standings.loss = 0, 1, -- Div by zero check...
                                                        tmp_standings.wins + tmp_standings.loss),
      tmp_standings._max_wins = tmp_teams_max_gp.max_gp - tmp_standings.loss - tmp_standings.ties,
      tmp_standings._max_win_pct = (tmp_teams_max_gp.max_gp - tmp_standings.loss - tmp_standings.ties) / tmp_teams_max_gp.max_gp,
      tmp_standings._worst_win_pct = tmp_standings.wins / tmp_teams_max_gp.max_gp;

  # Division Record
  INSERT INTO tmp_standings (season, the_date, team_id, div_wins, div_loss, div_ties)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS div_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS div_loss,
           IFNULL(SUM(tmp_sched.result = 't'), 0) AS div_ties
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_div_opp = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE div_wins = VALUES(div_wins),
                          div_loss = VALUES(div_loss),
                          div_ties = VALUES(div_ties);

  # Conference Record
  INSERT INTO tmp_standings (season, the_date, team_id, conf_wins, conf_loss, conf_ties)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS conf_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS conf_loss,
           IFNULL(SUM(tmp_sched.result = 't'), 0) AS conf_ties
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_conf_opp = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE conf_wins = VALUES(conf_wins),
                          conf_loss = VALUES(conf_loss),
                          conf_ties = VALUES(conf_ties);

END $$

DELIMITER ;
