##
## Misc standings info
##
DROP PROCEDURE IF EXISTS `mlb_standings_calcs_misc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_calcs_misc`()
    COMMENT 'Calculate additional info in MLB standings'
BEGIN

  # Record in extras
  INSERT INTO tmp_standings (season, the_date, team_id, extras_wins, extras_loss, extras_ties)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS extras_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS extras_loss,
           IFNULL(SUM(tmp_sched.result = 't'), 0) AS extras_ties
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_extras = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE extras_wins = VALUES(extras_wins),
                          extras_loss = VALUES(extras_loss),
                          extras_ties = VALUES(extras_ties);

  # Record in one run games
  INSERT INTO tmp_standings (season, the_date, team_id, onerun_wins, onerun_loss, onerun_ties)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS onerun_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS onerun_loss,
           IFNULL(SUM(tmp_sched.result = 't'), 0) AS onerun_ties
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_onerun = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE onerun_wins = VALUES(onerun_wins),
                          onerun_loss = VALUES(onerun_loss),
                          onerun_ties = VALUES(onerun_ties);

  # Runs Scored
  INSERT INTO tmp_standings (season, the_date, team_id, runs_for, runs_against)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.runs_for), 0) AS runs_for,
           IFNULL(SUM(tmp_sched.runs_against), 0) AS runs_against
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE runs_for = VALUES(runs_for),
                          runs_against = VALUES(runs_against);

END $$

DELIMITER ;
