#
# Aggregate Pitch Zone info from the play-by-play
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_season_zones`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_season_zones`()
    COMMENT 'Bubble up MLB pitch zone stats after processing the play-by-play'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_game_type ENUM('regular', 'playoff');
  SELECT season, game_type
    INTO v_season, v_game_type
  FROM tmp_game_list
  LIMIT 1;

  # Determine the list of players affected
  DROP TEMPORARY TABLE IF EXISTS tmp_players;
  CREATE TEMPORARY TABLE tmp_players (
    player_id SMALLINT UNSIGNED,
    PRIMARY KEY (player_id)
  ) SELECT DISTINCT SPORTS_MLB_GAME_ROSTERS.player_id
    FROM tmp_game_list
    JOIN SPORTS_MLB_GAME_ROSTERS
      USING (season, game_type, game_id);

  # Shortcut the ATBAT_FLAGS table
  DROP TEMPORARY TABLE IF EXISTS tmp_game_atbat_flags;
  CREATE TEMPORARY TABLE tmp_game_atbat_flags LIKE SPORTS_MLB_GAME_ATBAT_FLAGS;
  ALTER TABLE tmp_game_atbat_flags
    ADD KEY by_batter (season, game_type, batter, batter_hand, pitcher_hand) USING BTREE,
    ADD KEY by_pitcher (season, game_type, pitcher, batter_hand, pitcher_hand) USING BTREE,
    ADD KEY has_zone (zone) USING BTREE;
  INSERT INTO tmp_game_atbat_flags
    SELECT SPORTS_MLB_GAME_ATBAT_FLAGS.*
    FROM SPORTS_MLB_GAME_ATBAT_FLAGS
    WHERE season = v_season
    AND   game_type = v_game_type;
  DELETE FROM tmp_game_atbat_flags WHERE zone IS NULL;
  ALTER TABLE tmp_game_atbat_flags
    ADD COLUMN itl_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itl_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itl_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itl_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itl_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN itc_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itc_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itc_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itc_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itc_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN itr_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itr_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itr_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itr_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN itr_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN iml_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN iml_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN iml_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN iml_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN iml_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN imc_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imc_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imc_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imc_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imc_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN imr_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imr_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imr_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imr_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN imr_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN ibl_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibl_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibl_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibl_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibl_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN ibc_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibc_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibc_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibc_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibc_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN ibr_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibr_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibr_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibr_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN ibr_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN otl_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otl_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otl_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otl_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otl_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN otr_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otr_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otr_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otr_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN otr_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN obl_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obl_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obl_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obl_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obl_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN obr_obe TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obr_ab TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obr_h TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obr_ob TINYINT UNSIGNED DEFAULT 0, ADD COLUMN obr_tb TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN sp_f TINYINT UNSIGNED DEFAULT 0, ADD COLUMN sp_l TINYINT UNSIGNED DEFAULT 0, ADD COLUMN sp_l_c TINYINT UNSIGNED DEFAULT 0, ADD COLUMN sp_c TINYINT UNSIGNED DEFAULT 0, ADD COLUMN sp_c_r TINYINT UNSIGNED DEFAULT 0, ADD COLUMN sp_r TINYINT UNSIGNED DEFAULT 0;
  UPDATE tmp_game_atbat_flags
  SET itl_obe = (zone = 'ITL' AND is_obp_evt), itl_ab = (zone = 'ITL' AND is_ab), itl_h = (zone = 'ITL' AND is_hit), itl_ob = (zone = 'ITL' AND (is_hit OR is_bb OR is_hbp)), itl_tb = IF(zone = 'ITL', tb, 0),
      itc_obe = (zone = 'ITC' AND is_obp_evt), itc_ab = (zone = 'ITC' AND is_ab), itc_h = (zone = 'ITC' AND is_hit), itc_ob = (zone = 'ITC' AND (is_hit OR is_bb OR is_hbp)), itc_tb = IF(zone = 'ITC', tb, 0),
      itr_obe = (zone = 'ITR' AND is_obp_evt), itr_ab = (zone = 'ITR' AND is_ab), itr_h = (zone = 'ITR' AND is_hit), itr_ob = (zone = 'ITR' AND (is_hit OR is_bb OR is_hbp)), itr_tb = IF(zone = 'ITR', tb, 0),
      iml_obe = (zone = 'IML' AND is_obp_evt), iml_ab = (zone = 'IML' AND is_ab), iml_h = (zone = 'IML' AND is_hit), iml_ob = (zone = 'IML' AND (is_hit OR is_bb OR is_hbp)), iml_tb = IF(zone = 'IML', tb, 0),
      imc_obe = (zone = 'IMC' AND is_obp_evt), imc_ab = (zone = 'IMC' AND is_ab), imc_h = (zone = 'IMC' AND is_hit), imc_ob = (zone = 'IMC' AND (is_hit OR is_bb OR is_hbp)), imc_tb = IF(zone = 'IMC', tb, 0),
      imr_obe = (zone = 'IMR' AND is_obp_evt), imr_ab = (zone = 'IMR' AND is_ab), imr_h = (zone = 'IMR' AND is_hit), imr_ob = (zone = 'IMR' AND (is_hit OR is_bb OR is_hbp)), imr_tb = IF(zone = 'IMR', tb, 0),
      ibl_obe = (zone = 'IBL' AND is_obp_evt), ibl_ab = (zone = 'IBL' AND is_ab), ibl_h = (zone = 'IBL' AND is_hit), ibl_ob = (zone = 'IBL' AND (is_hit OR is_bb OR is_hbp)), ibl_tb = IF(zone = 'IBL', tb, 0),
      ibc_obe = (zone = 'IBC' AND is_obp_evt), ibc_ab = (zone = 'IBC' AND is_ab), ibc_h = (zone = 'IBC' AND is_hit), ibc_ob = (zone = 'IBC' AND (is_hit OR is_bb OR is_hbp)), ibc_tb = IF(zone = 'IBC', tb, 0),
      ibr_obe = (zone = 'IBR' AND is_obp_evt), ibr_ab = (zone = 'IBR' AND is_ab), ibr_h = (zone = 'IBR' AND is_hit), ibr_ob = (zone = 'IBR' AND (is_hit OR is_bb OR is_hbp)), ibr_tb = IF(zone = 'IBR', tb, 0),
      otl_obe = (zone = 'OTL' AND is_obp_evt), otl_ab = (zone = 'OTL' AND is_ab), otl_h = (zone = 'OTL' AND is_hit), otl_ob = (zone = 'OTL' AND (is_hit OR is_bb OR is_hbp)), otl_tb = IF(zone = 'OTL', tb, 0),
      otr_obe = (zone = 'OTR' AND is_obp_evt), otr_ab = (zone = 'OTR' AND is_ab), otr_h = (zone = 'OTR' AND is_hit), otr_ob = (zone = 'OTR' AND (is_hit OR is_bb OR is_hbp)), otr_tb = IF(zone = 'OTR', tb, 0),
      obl_obe = (zone = 'OBL' AND is_obp_evt), obl_ab = (zone = 'OBL' AND is_ab), obl_h = (zone = 'OBL' AND is_hit), obl_ob = (zone = 'OBL' AND (is_hit OR is_bb OR is_hbp)), obl_tb = IF(zone = 'OBL', tb, 0),
      obr_obe = (zone = 'OBR' AND is_obp_evt), obr_ab = (zone = 'OBR' AND is_ab), obr_h = (zone = 'OBR' AND is_hit), obr_ob = (zone = 'OBR' AND (is_hit OR is_bb OR is_hbp)), obr_tb = IF(zone = 'OBR', tb, 0),
      sp_f = (spray = 'F'), sp_l = (spray = 'L'), sp_l_c = (spray = 'L-C'), sp_c = (spray = 'C'), sp_c_r = (spray = 'C-R'), sp_r = (spray = 'R');

  #
  # As batter - season
  #
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_batting;
  CREATE TEMPORARY TABLE tmp_zones_batting (
    season SMALLINT UNSIGNED NULL,
    game_type ENUM('regular', 'playoff') NULL,
    player_id SMALLINT UNSIGNED NULL,
    batter_hand ENUM('L', 'R', 'CMB') NULL,
    pitcher_hand ENUM('L', 'R', 'CMB') NULL,
    itl_obe MEDIUMINT UNSIGNED DEFAULT 0, itl_ab MEDIUMINT UNSIGNED DEFAULT 0, itl_h MEDIUMINT UNSIGNED DEFAULT 0, itl_ob MEDIUMINT UNSIGNED DEFAULT 0, itl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    itc_obe MEDIUMINT UNSIGNED DEFAULT 0, itc_ab MEDIUMINT UNSIGNED DEFAULT 0, itc_h MEDIUMINT UNSIGNED DEFAULT 0, itc_ob MEDIUMINT UNSIGNED DEFAULT 0, itc_tb MEDIUMINT UNSIGNED DEFAULT 0,
    itr_obe MEDIUMINT UNSIGNED DEFAULT 0, itr_ab MEDIUMINT UNSIGNED DEFAULT 0, itr_h MEDIUMINT UNSIGNED DEFAULT 0, itr_ob MEDIUMINT UNSIGNED DEFAULT 0, itr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    iml_obe MEDIUMINT UNSIGNED DEFAULT 0, iml_ab MEDIUMINT UNSIGNED DEFAULT 0, iml_h MEDIUMINT UNSIGNED DEFAULT 0, iml_ob MEDIUMINT UNSIGNED DEFAULT 0, iml_tb MEDIUMINT UNSIGNED DEFAULT 0,
    imc_obe MEDIUMINT UNSIGNED DEFAULT 0, imc_ab MEDIUMINT UNSIGNED DEFAULT 0, imc_h MEDIUMINT UNSIGNED DEFAULT 0, imc_ob MEDIUMINT UNSIGNED DEFAULT 0, imc_tb MEDIUMINT UNSIGNED DEFAULT 0,
    imr_obe MEDIUMINT UNSIGNED DEFAULT 0, imr_ab MEDIUMINT UNSIGNED DEFAULT 0, imr_h MEDIUMINT UNSIGNED DEFAULT 0, imr_ob MEDIUMINT UNSIGNED DEFAULT 0, imr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    ibl_obe MEDIUMINT UNSIGNED DEFAULT 0, ibl_ab MEDIUMINT UNSIGNED DEFAULT 0, ibl_h MEDIUMINT UNSIGNED DEFAULT 0, ibl_ob MEDIUMINT UNSIGNED DEFAULT 0, ibl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    ibc_obe MEDIUMINT UNSIGNED DEFAULT 0, ibc_ab MEDIUMINT UNSIGNED DEFAULT 0, ibc_h MEDIUMINT UNSIGNED DEFAULT 0, ibc_ob MEDIUMINT UNSIGNED DEFAULT 0, ibc_tb MEDIUMINT UNSIGNED DEFAULT 0,
    ibr_obe MEDIUMINT UNSIGNED DEFAULT 0, ibr_ab MEDIUMINT UNSIGNED DEFAULT 0, ibr_h MEDIUMINT UNSIGNED DEFAULT 0, ibr_ob MEDIUMINT UNSIGNED DEFAULT 0, ibr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    otl_obe MEDIUMINT UNSIGNED DEFAULT 0, otl_ab MEDIUMINT UNSIGNED DEFAULT 0, otl_h MEDIUMINT UNSIGNED DEFAULT 0, otl_ob MEDIUMINT UNSIGNED DEFAULT 0, otl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    otr_obe MEDIUMINT UNSIGNED DEFAULT 0, otr_ab MEDIUMINT UNSIGNED DEFAULT 0, otr_h MEDIUMINT UNSIGNED DEFAULT 0, otr_ob MEDIUMINT UNSIGNED DEFAULT 0, otr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    obl_obe MEDIUMINT UNSIGNED DEFAULT 0, obl_ab MEDIUMINT UNSIGNED DEFAULT 0, obl_h MEDIUMINT UNSIGNED DEFAULT 0, obl_ob MEDIUMINT UNSIGNED DEFAULT 0, obl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    obr_obe MEDIUMINT UNSIGNED DEFAULT 0, obr_ab MEDIUMINT UNSIGNED DEFAULT 0, obr_h MEDIUMINT UNSIGNED DEFAULT 0, obr_ob MEDIUMINT UNSIGNED DEFAULT 0, obr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    sp_f SMALLINT UNSIGNED DEFAULT 0, sp_l SMALLINT UNSIGNED DEFAULT 0, sp_l_c SMALLINT UNSIGNED DEFAULT 0, sp_c SMALLINT UNSIGNED DEFAULT 0, sp_c_r SMALLINT UNSIGNED DEFAULT 0, sp_r SMALLINT UNSIGNED DEFAULT 0,
    UNIQUE faux_primary (season, game_type, player_id, batter_hand, pitcher_hand) USING BTREE
  ) SELECT ATBAT_FLAGS.season, ATBAT_FLAGS.game_type, ATBAT_FLAGS.batter AS player_id,
           ATBAT_FLAGS.batter_hand, ATBAT_FLAGS.pitcher_hand,
           SUM(itl_obe) AS itl_obe, SUM(itl_ab) AS itl_ab, SUM(itl_h) AS itl_h, SUM(itl_ob) AS itl_ob, SUM(itl_tb) AS itl_tb,
           SUM(itc_obe) AS itc_obe, SUM(itc_ab) AS itc_ab, SUM(itc_h) AS itc_h, SUM(itc_ob) AS itc_ob, SUM(itc_tb) AS itc_tb,
           SUM(itr_obe) AS itr_obe, SUM(itr_ab) AS itr_ab, SUM(itr_h) AS itr_h, SUM(itr_ob) AS itr_ob, SUM(itr_tb) AS itr_tb,
           SUM(iml_obe) AS iml_obe, SUM(iml_ab) AS iml_ab, SUM(iml_h) AS iml_h, SUM(iml_ob) AS iml_ob, SUM(iml_tb) AS iml_tb,
           SUM(imc_obe) AS imc_obe, SUM(imc_ab) AS imc_ab, SUM(imc_h) AS imc_h, SUM(imc_ob) AS imc_ob, SUM(imc_tb) AS imc_tb,
           SUM(imr_obe) AS imr_obe, SUM(imr_ab) AS imr_ab, SUM(imr_h) AS imr_h, SUM(imr_ob) AS imr_ob, SUM(imr_tb) AS imr_tb,
           SUM(ibl_obe) AS ibl_obe, SUM(ibl_ab) AS ibl_ab, SUM(ibl_h) AS ibl_h, SUM(ibl_ob) AS ibl_ob, SUM(ibl_tb) AS ibl_tb,
           SUM(ibc_obe) AS ibc_obe, SUM(ibc_ab) AS ibc_ab, SUM(ibc_h) AS ibc_h, SUM(ibc_ob) AS ibc_ob, SUM(ibc_tb) AS ibc_tb,
           SUM(ibr_obe) AS ibr_obe, SUM(ibr_ab) AS ibr_ab, SUM(ibr_h) AS ibr_h, SUM(ibr_ob) AS ibr_ob, SUM(ibr_tb) AS ibr_tb,
           SUM(otl_obe) AS otl_obe, SUM(otl_ab) AS otl_ab, SUM(otl_h) AS otl_h, SUM(otl_ob) AS otl_ob, SUM(otl_tb) AS otl_tb,
           SUM(otr_obe) AS otr_obe, SUM(otr_ab) AS otr_ab, SUM(otr_h) AS otr_h, SUM(otr_ob) AS otr_ob, SUM(otr_tb) AS otr_tb,
           SUM(obl_obe) AS obl_obe, SUM(obl_ab) AS obl_ab, SUM(obl_h) AS obl_h, SUM(obl_ob) AS obl_ob, SUM(obl_tb) AS obl_tb,
           SUM(obr_obe) AS obr_obe, SUM(obr_ab) AS obr_ab, SUM(obr_h) AS obr_h, SUM(obr_ob) AS obr_ob, SUM(obr_tb) AS obr_tb,
           SUM(sp_f) AS sp_f, SUM(sp_l) AS sp_l, SUM(sp_l_c) AS sp_l_c, SUM(sp_c) AS sp_c, SUM(sp_c_r) AS sp_c_r, SUM(sp_r) AS sp_r
    FROM tmp_players AS PLAYERS
    JOIN tmp_game_atbat_flags AS ATBAT_FLAGS
      ON (ATBAT_FLAGS.season = v_season
      AND ATBAT_FLAGS.game_type = v_game_type
      AND ATBAT_FLAGS.batter = PLAYERS.player_id)
    GROUP BY ATBAT_FLAGS.season, ATBAT_FLAGS.game_type, ATBAT_FLAGS.batter,
             ATBAT_FLAGS.batter_hand, ATBAT_FLAGS.pitcher_hand
    WITH ROLLUP;
  DELETE FROM tmp_zones_batting WHERE player_id IS NULL;
  UPDATE tmp_zones_batting SET batter_hand = 'CMB' WHERE batter_hand IS NULL;
  UPDATE tmp_zones_batting SET pitcher_hand = 'CMB' WHERE pitcher_hand IS NULL;

  # Prune "duplicated" pitcher_hand rollup rows (e.g., only R/R, which is also aggregated to R/CMB)
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_batting_dupe;
  CREATE TEMPORARY TABLE tmp_zones_batting_dupe (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    player_id SMALLINT UNSIGNED,
    batter_hand ENUM('L', 'R'),
    num_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, player_id, batter_hand),
    KEY by_num (num_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, player_id, batter_hand, COUNT(DISTINCT pitcher_hand) AS num_hands
    FROM tmp_zones_batting
    WHERE batter_hand <> 'CMB'
    AND   pitcher_hand <> 'CMB'
    GROUP BY season, game_type, player_id, batter_hand;

  DELETE STAT.*
  FROM tmp_zones_batting_dupe AS DUPE
  JOIN tmp_zones_batting AS STAT
    ON (STAT.season = DUPE.season
    AND STAT.game_type = DUPE.game_type
    AND STAT.player_id = DUPE.player_id
    AND STAT.batter_hand = DUPE.batter_hand
    AND STAT.pitcher_hand = 'CMB')
  WHERE DUPE.num_hands = 1;

  # And then the same for batter_hand
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_batting_dupe;
  CREATE TEMPORARY TABLE tmp_zones_batting_dupe (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    player_id SMALLINT UNSIGNED,
    num_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, player_id),
    KEY by_num (num_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, player_id, COUNT(DISTINCT batter_hand) AS num_hands
    FROM tmp_zones_batting
    WHERE batter_hand <> 'CMB'
    AND   pitcher_hand <> 'CMB'
    GROUP BY season, game_type, player_id;

  DELETE STAT.*
  FROM tmp_zones_batting_dupe AS DUPE
  JOIN tmp_zones_batting AS STAT
    ON (STAT.season = DUPE.season
    AND STAT.game_type = DUPE.game_type
    AND STAT.player_id = DUPE.player_id
    AND STAT.batter_hand = 'CMB')
  WHERE DUPE.num_hands = 1;

  # Fix ROLLUP aggregation where pitcher_hand is converted to CMB, when it should be L/R
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_batting_fix;
  CREATE TEMPORARY TABLE tmp_zones_batting_fix (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    player_id SMALLINT UNSIGNED,
    pitcher_hand CHAR(3),
    num_pitcher_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, player_id),
    KEY by_num (num_pitcher_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, player_id,
           GROUP_CONCAT(DISTINCT pitcher_hand) AS pitcher_hand,
           COUNT(DISTINCT pitcher_hand) AS num_pitcher_hands
    FROM tmp_zones_batting
    WHERE pitcher_hand <> 'CMB'
    GROUP BY season, game_type, player_id;

  UPDATE tmp_zones_batting_fix AS FIXES
  JOIN tmp_zones_batting AS STAT
    ON (STAT.season = FIXES.season
    AND STAT.game_type = FIXES.game_type
    AND STAT.player_id = FIXES.player_id
    AND STAT.pitcher_hand = 'CMB')
  SET STAT.pitcher_hand = FIXES.pitcher_hand
  WHERE FIXES.num_pitcher_hands = 1;

  # Then add to the main table
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_BATTING_ZONES
    SELECT season, game_type AS season_type, player_id, batter_hand, pitcher_hand,
           COMPRESS(CONCAT(
             '[', itl_obe, ',', itl_ab, ',', itl_h, ',', itl_ob, ',', itl_tb, '],',
             '[', itc_obe, ',', itc_ab, ',', itc_h, ',', itc_ob, ',', itc_tb, '],',
             '[', itr_obe, ',', itr_ab, ',', itr_h, ',', itr_ob, ',', itr_tb, '],',
             '[', iml_obe, ',', iml_ab, ',', iml_h, ',', iml_ob, ',', iml_tb, '],',
             '[', imc_obe, ',', imc_ab, ',', imc_h, ',', imc_ob, ',', imc_tb, '],',
             '[', imr_obe, ',', imr_ab, ',', imr_h, ',', imr_ob, ',', imr_tb, '],',
             '[', ibl_obe, ',', ibl_ab, ',', ibl_h, ',', ibl_ob, ',', ibl_tb, '],',
             '[', ibc_obe, ',', ibc_ab, ',', ibc_h, ',', ibc_ob, ',', ibc_tb, '],',
             '[', ibr_obe, ',', ibr_ab, ',', ibr_h, ',', ibr_ob, ',', ibr_tb, '],',
             '[', otl_obe, ',', otl_ab, ',', otl_h, ',', otl_ob, ',', otl_tb, '],',
             '[', otr_obe, ',', otr_ab, ',', otr_h, ',', otr_ob, ',', otr_tb, '],',
             '[', obl_obe, ',', obl_ab, ',', obl_h, ',', obl_ob, ',', obl_tb, '],',
             '[', obr_obe, ',', obr_ab, ',', obr_h, ',', obr_ob, ',', obr_tb, ']'
           )) AS heatzones,
           CONCAT(sp_f, ',', sp_l, ',', sp_l_c, ',', sp_c, ',', sp_c_r, ',', sp_r) AS spray_chart
    FROM tmp_zones_batting
  ON DUPLICATE KEY UPDATE heatzones = VALUES(heatzones), spray_chart = VALUES(spray_chart);

  #
  # As pitcher - season
  #
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_pitching;
  CREATE TEMPORARY TABLE tmp_zones_pitching (
    season SMALLINT UNSIGNED NULL,
    game_type ENUM('regular', 'playoff') NULL,
    player_id SMALLINT UNSIGNED NULL,
    batter_hand ENUM('L', 'R', 'CMB') NULL,
    pitcher_hand ENUM('L', 'R', 'CMB') NULL,
    itl_obe MEDIUMINT UNSIGNED DEFAULT 0, itl_ab MEDIUMINT UNSIGNED DEFAULT 0, itl_h MEDIUMINT UNSIGNED DEFAULT 0, itl_ob MEDIUMINT UNSIGNED DEFAULT 0, itl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    itc_obe MEDIUMINT UNSIGNED DEFAULT 0, itc_ab MEDIUMINT UNSIGNED DEFAULT 0, itc_h MEDIUMINT UNSIGNED DEFAULT 0, itc_ob MEDIUMINT UNSIGNED DEFAULT 0, itc_tb MEDIUMINT UNSIGNED DEFAULT 0,
    itr_obe MEDIUMINT UNSIGNED DEFAULT 0, itr_ab MEDIUMINT UNSIGNED DEFAULT 0, itr_h MEDIUMINT UNSIGNED DEFAULT 0, itr_ob MEDIUMINT UNSIGNED DEFAULT 0, itr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    iml_obe MEDIUMINT UNSIGNED DEFAULT 0, iml_ab MEDIUMINT UNSIGNED DEFAULT 0, iml_h MEDIUMINT UNSIGNED DEFAULT 0, iml_ob MEDIUMINT UNSIGNED DEFAULT 0, iml_tb MEDIUMINT UNSIGNED DEFAULT 0,
    imc_obe MEDIUMINT UNSIGNED DEFAULT 0, imc_ab MEDIUMINT UNSIGNED DEFAULT 0, imc_h MEDIUMINT UNSIGNED DEFAULT 0, imc_ob MEDIUMINT UNSIGNED DEFAULT 0, imc_tb MEDIUMINT UNSIGNED DEFAULT 0,
    imr_obe MEDIUMINT UNSIGNED DEFAULT 0, imr_ab MEDIUMINT UNSIGNED DEFAULT 0, imr_h MEDIUMINT UNSIGNED DEFAULT 0, imr_ob MEDIUMINT UNSIGNED DEFAULT 0, imr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    ibl_obe MEDIUMINT UNSIGNED DEFAULT 0, ibl_ab MEDIUMINT UNSIGNED DEFAULT 0, ibl_h MEDIUMINT UNSIGNED DEFAULT 0, ibl_ob MEDIUMINT UNSIGNED DEFAULT 0, ibl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    ibc_obe MEDIUMINT UNSIGNED DEFAULT 0, ibc_ab MEDIUMINT UNSIGNED DEFAULT 0, ibc_h MEDIUMINT UNSIGNED DEFAULT 0, ibc_ob MEDIUMINT UNSIGNED DEFAULT 0, ibc_tb MEDIUMINT UNSIGNED DEFAULT 0,
    ibr_obe MEDIUMINT UNSIGNED DEFAULT 0, ibr_ab MEDIUMINT UNSIGNED DEFAULT 0, ibr_h MEDIUMINT UNSIGNED DEFAULT 0, ibr_ob MEDIUMINT UNSIGNED DEFAULT 0, ibr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    otl_obe MEDIUMINT UNSIGNED DEFAULT 0, otl_ab MEDIUMINT UNSIGNED DEFAULT 0, otl_h MEDIUMINT UNSIGNED DEFAULT 0, otl_ob MEDIUMINT UNSIGNED DEFAULT 0, otl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    otr_obe MEDIUMINT UNSIGNED DEFAULT 0, otr_ab MEDIUMINT UNSIGNED DEFAULT 0, otr_h MEDIUMINT UNSIGNED DEFAULT 0, otr_ob MEDIUMINT UNSIGNED DEFAULT 0, otr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    obl_obe MEDIUMINT UNSIGNED DEFAULT 0, obl_ab MEDIUMINT UNSIGNED DEFAULT 0, obl_h MEDIUMINT UNSIGNED DEFAULT 0, obl_ob MEDIUMINT UNSIGNED DEFAULT 0, obl_tb MEDIUMINT UNSIGNED DEFAULT 0,
    obr_obe MEDIUMINT UNSIGNED DEFAULT 0, obr_ab MEDIUMINT UNSIGNED DEFAULT 0, obr_h MEDIUMINT UNSIGNED DEFAULT 0, obr_ob MEDIUMINT UNSIGNED DEFAULT 0, obr_tb MEDIUMINT UNSIGNED DEFAULT 0,
    sp_f SMALLINT UNSIGNED DEFAULT 0, sp_l SMALLINT UNSIGNED DEFAULT 0, sp_l_c SMALLINT UNSIGNED DEFAULT 0, sp_c SMALLINT UNSIGNED DEFAULT 0, sp_c_r SMALLINT UNSIGNED DEFAULT 0, sp_r SMALLINT UNSIGNED DEFAULT 0,
    UNIQUE faux_primary (season, game_type, player_id, batter_hand, pitcher_hand) USING BTREE
  ) SELECT ATBAT_FLAGS.season, ATBAT_FLAGS.game_type, ATBAT_FLAGS.pitcher AS player_id,
           ATBAT_FLAGS.batter_hand, ATBAT_FLAGS.pitcher_hand,
           SUM(itl_obe) AS itl_obe, SUM(itl_ab) AS itl_ab, SUM(itl_h) AS itl_h, SUM(itl_ob) AS itl_ob, SUM(itl_tb) AS itl_tb,
           SUM(itc_obe) AS itc_obe, SUM(itc_ab) AS itc_ab, SUM(itc_h) AS itc_h, SUM(itc_ob) AS itc_ob, SUM(itc_tb) AS itc_tb,
           SUM(itr_obe) AS itr_obe, SUM(itr_ab) AS itr_ab, SUM(itr_h) AS itr_h, SUM(itr_ob) AS itr_ob, SUM(itr_tb) AS itr_tb,
           SUM(iml_obe) AS iml_obe, SUM(iml_ab) AS iml_ab, SUM(iml_h) AS iml_h, SUM(iml_ob) AS iml_ob, SUM(iml_tb) AS iml_tb,
           SUM(imc_obe) AS imc_obe, SUM(imc_ab) AS imc_ab, SUM(imc_h) AS imc_h, SUM(imc_ob) AS imc_ob, SUM(imc_tb) AS imc_tb,
           SUM(imr_obe) AS imr_obe, SUM(imr_ab) AS imr_ab, SUM(imr_h) AS imr_h, SUM(imr_ob) AS imr_ob, SUM(imr_tb) AS imr_tb,
           SUM(ibl_obe) AS ibl_obe, SUM(ibl_ab) AS ibl_ab, SUM(ibl_h) AS ibl_h, SUM(ibl_ob) AS ibl_ob, SUM(ibl_tb) AS ibl_tb,
           SUM(ibc_obe) AS ibc_obe, SUM(ibc_ab) AS ibc_ab, SUM(ibc_h) AS ibc_h, SUM(ibc_ob) AS ibc_ob, SUM(ibc_tb) AS ibc_tb,
           SUM(ibr_obe) AS ibr_obe, SUM(ibr_ab) AS ibr_ab, SUM(ibr_h) AS ibr_h, SUM(ibr_ob) AS ibr_ob, SUM(ibr_tb) AS ibr_tb,
           SUM(otl_obe) AS otl_obe, SUM(otl_ab) AS otl_ab, SUM(otl_h) AS otl_h, SUM(otl_ob) AS otl_ob, SUM(otl_tb) AS otl_tb,
           SUM(otr_obe) AS otr_obe, SUM(otr_ab) AS otr_ab, SUM(otr_h) AS otr_h, SUM(otr_ob) AS otr_ob, SUM(otr_tb) AS otr_tb,
           SUM(obl_obe) AS obl_obe, SUM(obl_ab) AS obl_ab, SUM(obl_h) AS obl_h, SUM(obl_ob) AS obl_ob, SUM(obl_tb) AS obl_tb,
           SUM(obr_obe) AS obr_obe, SUM(obr_ab) AS obr_ab, SUM(obr_h) AS obr_h, SUM(obr_ob) AS obr_ob, SUM(obr_tb) AS obr_tb,
           SUM(sp_f) AS sp_f, SUM(sp_l) AS sp_l, SUM(sp_l_c) AS sp_l_c, SUM(sp_c) AS sp_c, SUM(sp_c_r) AS sp_c_r, SUM(sp_r) AS sp_r
    FROM tmp_players AS PLAYERS
    JOIN tmp_game_atbat_flags AS ATBAT_FLAGS
      ON (ATBAT_FLAGS.season = v_season
      AND ATBAT_FLAGS.game_type = v_game_type
      AND ATBAT_FLAGS.pitcher = PLAYERS.player_id)
    GROUP BY ATBAT_FLAGS.season, ATBAT_FLAGS.game_type, ATBAT_FLAGS.pitcher,
             ATBAT_FLAGS.batter_hand, ATBAT_FLAGS.pitcher_hand
    WITH ROLLUP;
  DELETE FROM tmp_zones_pitching WHERE player_id IS NULL;
  UPDATE tmp_zones_pitching SET batter_hand = 'CMB' WHERE batter_hand IS NULL;
  UPDATE tmp_zones_pitching SET pitcher_hand = 'CMB' WHERE pitcher_hand IS NULL;

  # Prune "duplicated" pitcher_hand rollup rows (e.g., only R/R, which is also aggregated to R/CMB)
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_pitching_dupe;
  CREATE TEMPORARY TABLE tmp_zones_pitching_dupe (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    player_id SMALLINT UNSIGNED,
    batter_hand ENUM('L', 'R'),
    num_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, player_id, batter_hand),
    KEY by_num (num_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, player_id, batter_hand, COUNT(DISTINCT pitcher_hand) AS num_hands
    FROM tmp_zones_pitching
    WHERE batter_hand <> 'CMB'
    AND   pitcher_hand <> 'CMB'
    GROUP BY season, game_type, player_id, batter_hand;

  DELETE STAT.*
  FROM tmp_zones_pitching_dupe AS DUPE
  JOIN tmp_zones_pitching AS STAT
    ON (STAT.season = DUPE.season
    AND STAT.game_type = DUPE.game_type
    AND STAT.player_id = DUPE.player_id
    AND STAT.batter_hand = DUPE.batter_hand
    AND STAT.pitcher_hand = 'CMB')
  WHERE DUPE.num_hands = 1;

  # And then the same for batter_hand
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_pitching_dupe;
  CREATE TEMPORARY TABLE tmp_zones_pitching_dupe (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    player_id SMALLINT UNSIGNED,
    num_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, player_id),
    KEY by_num (num_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, player_id, COUNT(DISTINCT batter_hand) AS num_hands
    FROM tmp_zones_pitching
    WHERE batter_hand <> 'CMB'
    AND   pitcher_hand <> 'CMB'
    GROUP BY season, game_type, player_id;

  DELETE STAT.*
  FROM tmp_zones_pitching_dupe AS DUPE
  JOIN tmp_zones_pitching AS STAT
    ON (STAT.season = DUPE.season
    AND STAT.game_type = DUPE.game_type
    AND STAT.player_id = DUPE.player_id
    AND STAT.batter_hand = 'CMB')
  WHERE DUPE.num_hands = 1;

  # Fix ROLLUP aggregation where pitcher_hand is converted to CMB, when it should be L/R
  DROP TEMPORARY TABLE IF EXISTS tmp_zones_pitching_fix;
  CREATE TEMPORARY TABLE tmp_zones_pitching_fix (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    player_id SMALLINT UNSIGNED,
    pitcher_hand CHAR(3),
    num_pitcher_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, player_id),
    KEY by_num (num_pitcher_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, player_id,
           GROUP_CONCAT(DISTINCT pitcher_hand) AS pitcher_hand,
           COUNT(DISTINCT pitcher_hand) AS num_pitcher_hands
    FROM tmp_zones_pitching
    WHERE pitcher_hand <> 'CMB'
    GROUP BY season, game_type, player_id;

  UPDATE tmp_zones_pitching_fix AS FIXES
  JOIN tmp_zones_pitching AS STAT
    ON (STAT.season = FIXES.season
    AND STAT.game_type = FIXES.game_type
    AND STAT.player_id = FIXES.player_id
    AND STAT.pitcher_hand = 'CMB')
  SET STAT.pitcher_hand = FIXES.pitcher_hand
  WHERE FIXES.num_pitcher_hands = 1;

  # Then add to the main table
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_PITCHING_ZONES
    SELECT season, game_type AS season_type, player_id, batter_hand, pitcher_hand,
           COMPRESS(CONCAT(
             '[', itl_obe, ',', itl_ab, ',', itl_h, ',', itl_ob, ',', itl_tb, '],',
             '[', itc_obe, ',', itc_ab, ',', itc_h, ',', itc_ob, ',', itc_tb, '],',
             '[', itr_obe, ',', itr_ab, ',', itr_h, ',', itr_ob, ',', itr_tb, '],',
             '[', iml_obe, ',', iml_ab, ',', iml_h, ',', iml_ob, ',', iml_tb, '],',
             '[', imc_obe, ',', imc_ab, ',', imc_h, ',', imc_ob, ',', imc_tb, '],',
             '[', imr_obe, ',', imr_ab, ',', imr_h, ',', imr_ob, ',', imr_tb, '],',
             '[', ibl_obe, ',', ibl_ab, ',', ibl_h, ',', ibl_ob, ',', ibl_tb, '],',
             '[', ibc_obe, ',', ibc_ab, ',', ibc_h, ',', ibc_ob, ',', ibc_tb, '],',
             '[', ibr_obe, ',', ibr_ab, ',', ibr_h, ',', ibr_ob, ',', ibr_tb, '],',
             '[', otl_obe, ',', otl_ab, ',', otl_h, ',', otl_ob, ',', otl_tb, '],',
             '[', otr_obe, ',', otr_ab, ',', otr_h, ',', otr_ob, ',', otr_tb, '],',
             '[', obl_obe, ',', obl_ab, ',', obl_h, ',', obl_ob, ',', obl_tb, '],',
             '[', obr_obe, ',', obr_ab, ',', obr_h, ',', obr_ob, ',', obr_tb, ']'
           )) AS heatzones,
           CONCAT(sp_f, ',', sp_l, ',', sp_l_c, ',', sp_c, ',', sp_c_r, ',', sp_r) AS spray_chart
    FROM tmp_zones_pitching
  ON DUPLICATE KEY UPDATE heatzones = VALUES(heatzones), spray_chart = VALUES(spray_chart);

  # Table management
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_PITCHING_ZONES
    ORDER BY season, season_type, player_id, batter_hand, pitcher_hand;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_BATTING_ZONES
    ORDER BY season, season_type, player_id, batter_hand, pitcher_hand;

END $$

DELIMITER ;
