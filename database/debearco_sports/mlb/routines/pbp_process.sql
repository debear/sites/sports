#
# Post-process a game _after_ the play-by-play
#
DROP PROCEDURE IF EXISTS `mlb_game_pbp_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_game_pbp_process`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular', 'playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Post-process the MLB play-by-play list for a single game'
BEGIN

    CALL mlb_game_pbp_process_denorm(v_season, v_game_type, v_game_id);
    CALL mlb_game_pbp_process_pitches(v_season, v_game_type, v_game_id);
    CALL mlb_game_pbp_process_winprob(v_season, v_game_type, v_game_id);

END $$

DELIMITER ;

#
# Denormalise data to be uploaded to live
#
DROP PROCEDURE IF EXISTS `mlb_game_pbp_process_denorm`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_game_pbp_process_denorm`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular', 'playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Denormalise MLB plays to live uploadable tables'
BEGIN

  # Scoring plays
  INSERT INTO SPORTS_MLB_GAME_SCORING (season, game_type, game_id, play_id, inning, half_inning, batter_team_id, batter, pitcher_team_id, pitcher, on_base, ob_1b, ob_2b, ob_3b, `out`, home_score, visitor_score, home_winprob, visitor_winprob, change_winprob, description)
    SELECT season, game_type, game_id, play_id, inning, half_inning,
           batter_team_id, batter, pitcher_team_id, pitcher,
           on_base, ob_1b, ob_2b, ob_3b, `out`,
           home_score, visitor_score, home_winprob, visitor_winprob, change_winprob,
           description
    FROM SPORTS_MLB_GAME_PLAYS
    WHERE season = v_season
    AND   game_type = v_game_type
    AND   game_id = v_game_id
    AND   scoring = 1;

  # WinProb-by-play
  INSERT INTO SPORTS_MLB_GAME_WINPROB (season, game_type, game_id, winprob)
    SELECT PLAY.season, PLAY.game_type, PLAY.game_id,
            COMPRESS(CONCAT('[', GROUP_CONCAT(CONCAT('[',
              PLAY.inning, ',',
              '"', PLAY.half_inning, '",',
              '"', PLAY.batter_team_id, '",',
              PLAY.batter, ',',
              '"', PLAY.pitcher_team_id, '",',
              PLAY.pitcher, ',',
              '"', ATBAT.result, '",',
              PLAY.on_base, ',',
              '"',
                IF(PLAY.ob_3b, '3', '-'),
                IF(PLAY.ob_2b, '2', '-'),
                IF(PLAY.ob_1b, '1', '-'),
              '",',
              PLAY.out, ',',
              PLAY.scoring, ',',
              PLAY.home_score, ',',
              PLAY.visitor_score, ',',
              PLAY.home_winprob, ',',
              PLAY.visitor_winprob,
            ']') ORDER BY PLAY.play_id), ']')) AS winprob
    FROM SPORTS_MLB_GAME_PLAYS AS PLAY
    JOIN SPORTS_MLB_GAME_ATBAT AS ATBAT
      ON (ATBAT.season = PLAY.season
      AND ATBAT.game_type = PLAY.game_type
      AND ATBAT.game_id = PLAY.game_id
      AND ATBAT.play_id = PLAY.play_id)
    WHERE PLAY.season = v_season
    AND   PLAY.game_type = v_game_type
    AND   PLAY.game_id = v_game_id
    AND   PLAY.home_winprob IS NOT NULL
    GROUP BY PLAY.season, PLAY.game_type, PLAY.game_id;

END $$

DELIMITER ;

#
# Compress the pitch-list in a single hit
#
DROP PROCEDURE IF EXISTS `mlb_game_pbp_process_pitches`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_game_pbp_process_pitches`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular', 'playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Compress the pitch list in each MLB play-by-play'
BEGIN

  UPDATE SPORTS_MLB_GAME_ATBAT
  SET pitches = COMPRESS(pitches)
  WHERE season = v_season
  AND   game_type = v_game_type
  AND   game_id = v_game_id;

END $$

DELIMITER ;

#
# Bubble-up totals for player win probability changes
#
DROP PROCEDURE IF EXISTS `mlb_game_pbp_process_winprob`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_game_pbp_process_winprob`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular', 'playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Bubble up MLB winprob stats after processing the play-by-play'
BEGIN

    DECLARE v_game_time_ref SMALLINT UNSIGNED;
    SELECT game_time_ref INTO v_game_time_ref
    FROM SPORTS_MLB_SCHEDULE
    WHERE season = v_season
    AND   game_type = v_game_type
    AND   game_id = v_game_id;

    # Batters - Per Game
    INSERT INTO SPORTS_MLB_PLAYERS_GAME_BATTING (season, game_type, game_id, team_id, jersey, winprob)
      SELECT season, game_type, game_id, batter_team_id, batter,
             SUM(change_winprob) AS winprob
      FROM SPORTS_MLB_GAME_PLAYS
      WHERE season = v_season
      AND   game_type = v_game_type
      AND   game_id = v_game_id
      AND   change_winprob IS NOT NULL
      GROUP BY season, game_type, game_id, batter_team_id, batter
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Batters - Y2D
    UPDATE tmp_y2d_batting_data
    JOIN SPORTS_MLB_PLAYERS_GAME_BATTING
      ON (SPORTS_MLB_PLAYERS_GAME_BATTING.season = tmp_y2d_batting_data.season
      AND SPORTS_MLB_PLAYERS_GAME_BATTING.game_type = tmp_y2d_batting_data.game_type
      AND SPORTS_MLB_PLAYERS_GAME_BATTING.game_id = tmp_y2d_batting_data.game_id
      AND SPORTS_MLB_PLAYERS_GAME_BATTING.player_id = tmp_y2d_batting_data.player_id)
    SET tmp_y2d_batting_data.winprob = SPORTS_MLB_PLAYERS_GAME_BATTING.winprob
    WHERE tmp_y2d_batting_data.season = v_season
    AND   tmp_y2d_batting_data.game_type = v_game_type
    AND   tmp_y2d_batting_data.game_id = v_game_id;

    INSERT INTO SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D (season, game_type, game_id, game_time_ref, team_id, player_id, winprob)
      SELECT v_season, v_game_type, v_game_id, v_game_time_ref AS game_time_ref, team_id, player_id,
             SUM(winprob) AS winprob
      FROM tmp_y2d_batting_data
      GROUP BY season, game_type, team_id, player_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Batters - Y2D Multiple
    UPDATE tmp_y2d_batting_data_multiple
    JOIN SPORTS_MLB_PLAYERS_GAME_BATTING
      ON (SPORTS_MLB_PLAYERS_GAME_BATTING.season = tmp_y2d_batting_data_multiple.season
      AND SPORTS_MLB_PLAYERS_GAME_BATTING.game_type = tmp_y2d_batting_data_multiple.game_type
      AND SPORTS_MLB_PLAYERS_GAME_BATTING.game_id = tmp_y2d_batting_data_multiple.game_id
      AND SPORTS_MLB_PLAYERS_GAME_BATTING.player_id = tmp_y2d_batting_data_multiple.player_id)
    SET tmp_y2d_batting_data_multiple.winprob = SPORTS_MLB_PLAYERS_GAME_BATTING.winprob
    WHERE tmp_y2d_batting_data_multiple.season = v_season
    AND   tmp_y2d_batting_data_multiple.game_type = v_game_type
    AND   tmp_y2d_batting_data_multiple.game_id = v_game_id;

    INSERT INTO SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D (season, game_type, game_id, game_time_ref, team_id, player_id, winprob)
      SELECT v_season, v_game_type, v_game_id, v_game_time_ref AS game_time_ref, '_ML' AS team_id, player_id,
             SUM(winprob) AS winprob
      FROM tmp_y2d_batting_data_multiple
      GROUP BY season, game_type, player_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Batters - Season
    INSERT INTO SPORTS_MLB_PLAYERS_SEASON_BATTING (season, season_type, team_id, player_id, winprob)
      SELECT season, game_type, team_id, player_id, winprob
      FROM SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D
      WHERE season = v_season
      AND   game_type = v_game_type
      AND   game_id = v_game_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Pitchers - Per Game
    INSERT INTO SPORTS_MLB_PLAYERS_GAME_PITCHING (season, game_type, game_id, team_id, jersey, winprob)
      SELECT season, game_type, game_id, pitcher_team_id, pitcher,
             SUM(0 - change_winprob) AS winprob
      FROM SPORTS_MLB_GAME_PLAYS
      WHERE season = v_season
      AND   game_type = v_game_type
      AND   game_id = v_game_id
      AND   change_winprob IS NOT NULL
      GROUP BY season, game_type, game_id, pitcher_team_id, pitcher
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Pitchers - Y2D
    UPDATE tmp_y2d_pitching_data
    JOIN SPORTS_MLB_PLAYERS_GAME_PITCHING
      ON (SPORTS_MLB_PLAYERS_GAME_PITCHING.season = tmp_y2d_pitching_data.season
      AND SPORTS_MLB_PLAYERS_GAME_PITCHING.game_type = tmp_y2d_pitching_data.game_type
      AND SPORTS_MLB_PLAYERS_GAME_PITCHING.game_id = tmp_y2d_pitching_data.game_id
      AND SPORTS_MLB_PLAYERS_GAME_PITCHING.player_id = tmp_y2d_pitching_data.player_id)
    SET tmp_y2d_pitching_data.winprob = SPORTS_MLB_PLAYERS_GAME_PITCHING.winprob
    WHERE tmp_y2d_pitching_data.season = v_season
    AND   tmp_y2d_pitching_data.game_type = v_game_type
    AND   tmp_y2d_pitching_data.game_id = v_game_id;

    INSERT INTO SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D (season, game_type, game_id, game_time_ref, team_id, player_id, winprob)
      SELECT v_season, v_game_type, v_game_id, v_game_time_ref AS game_time_ref, team_id, player_id,
             SUM(winprob) AS winprob
      FROM tmp_y2d_pitching_data
      GROUP BY season, game_type, team_id, player_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Pitchers - Y2D Multiple
    UPDATE tmp_y2d_pitching_data_multiple
    JOIN SPORTS_MLB_PLAYERS_GAME_PITCHING
      ON (SPORTS_MLB_PLAYERS_GAME_PITCHING.season = tmp_y2d_pitching_data_multiple.season
      AND SPORTS_MLB_PLAYERS_GAME_PITCHING.game_type = tmp_y2d_pitching_data_multiple.game_type
      AND SPORTS_MLB_PLAYERS_GAME_PITCHING.game_id = tmp_y2d_pitching_data_multiple.game_id
      AND SPORTS_MLB_PLAYERS_GAME_PITCHING.player_id = tmp_y2d_pitching_data_multiple.player_id)
    SET tmp_y2d_pitching_data_multiple.winprob = SPORTS_MLB_PLAYERS_GAME_PITCHING.winprob
    WHERE tmp_y2d_pitching_data_multiple.season = v_season
    AND   tmp_y2d_pitching_data_multiple.game_type = v_game_type
    AND   tmp_y2d_pitching_data_multiple.game_id = v_game_id;

    INSERT INTO SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D (season, game_type, game_id, game_time_ref, team_id, player_id, winprob)
      SELECT v_season, v_game_type, v_game_id, v_game_time_ref AS game_time_ref, '_ML' AS team_id, player_id,
             SUM(winprob) AS winprob
      FROM tmp_y2d_pitching_data_multiple
      GROUP BY season, game_type, player_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Pitchers - Season
    INSERT INTO SPORTS_MLB_PLAYERS_SEASON_PITCHING (season, season_type, team_id, player_id, winprob)
      SELECT season, game_type, team_id, player_id, winprob
      FROM SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D
      WHERE season = v_season
      AND   game_type = v_game_type
      AND   game_id = v_game_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    # Misc / Combined WinProb
    INSERT INTO SPORTS_MLB_PLAYERS_SEASON_MISC (season, season_type, player_id, team_id, winprob)
      SELECT ROSTER.season, ROSTER.game_type AS season_type, ROSTER.player_id, ROSTER.team_id,
             IFNULL(BATTING.winprob, 0) + IFNULL(PITCHING.winprob, 0) AS winprob
      FROM SPORTS_MLB_GAME_ROSTERS AS ROSTER
      LEFT JOIN SPORTS_MLB_PLAYERS_SEASON_BATTING AS BATTING
        ON (BATTING.season = ROSTER.season
        AND BATTING.season_type = ROSTER.game_type
        AND BATTING.team_id = ROSTER.team_id
        AND BATTING.player_id = ROSTER.player_id)
      LEFT JOIN SPORTS_MLB_PLAYERS_SEASON_PITCHING AS PITCHING
        ON (PITCHING.season = ROSTER.season
        AND PITCHING.season_type = ROSTER.game_type
        AND PITCHING.team_id = ROSTER.team_id
        AND PITCHING.player_id = ROSTER.player_id)
      WHERE ROSTER.season = v_season
      AND   ROSTER.game_type = v_game_type
      AND   ROSTER.game_id = v_game_id
      GROUP BY ROSTER.season, ROSTER.game_type, ROSTER.player_id, ROSTER.team_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

    INSERT INTO SPORTS_MLB_PLAYERS_SEASON_MISC (season, season_type, player_id, team_id, winprob)
      SELECT ROSTER.season, ROSTER.game_type AS season_type, ROSTER.player_id, '_ML' AS team_id,
             IFNULL(BATTING.winprob, 0) + IFNULL(PITCHING.winprob, 0) AS winprob
      FROM SPORTS_MLB_GAME_ROSTERS AS ROSTER
      LEFT JOIN SPORTS_MLB_PLAYERS_SEASON_BATTING AS BATTING
        ON (BATTING.season = ROSTER.season
        AND BATTING.season_type = ROSTER.game_type
        AND BATTING.team_id = '_ML'
        AND BATTING.player_id = ROSTER.player_id)
      LEFT JOIN SPORTS_MLB_PLAYERS_SEASON_PITCHING AS PITCHING
        ON (PITCHING.season = ROSTER.season
        AND PITCHING.season_type = ROSTER.game_type
        AND PITCHING.team_id = '_ML'
        AND PITCHING.player_id = ROSTER.player_id)
      WHERE ROSTER.season = v_season
      AND   ROSTER.game_type = v_game_type
      AND   ROSTER.game_id = v_game_id
      AND  (BATTING.team_id IS NOT NULL OR PITCHING.team_id IS NOT NULL)
      GROUP BY ROSTER.season, ROSTER.game_type, ROSTER.player_id
    ON DUPLICATE KEY UPDATE winprob = VALUES(winprob);

END $$

DELIMITER ;
