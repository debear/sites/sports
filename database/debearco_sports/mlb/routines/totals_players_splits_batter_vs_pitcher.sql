#
# Batter-based split and situational stats
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batter_vs_pitcher`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batter_vs_pitcher`()
    COMMENT 'Batter vs Pitcher stat calculations'
BEGIN

  # Prepare
  CALL mlb_totals_players_splits_batter_vs_pitcher_setup();

  # Season
  CALL mlb_totals_players_splits_batter_vs_pitcher_season();

  # Career
  CALL mlb_totals_players_splits_batter_vs_pitcher_career();

  # Tidy
  CALL mlb_totals_players_splits_batter_vs_pitcher_tidy();

END $$

DELIMITER ;

#
# Prepare BvP calcs
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batter_vs_pitcher_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batter_vs_pitcher_setup`()
    COMMENT 'Batter vs Pitcher stats over the course of a season'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_season_types;
  CREATE TEMPORARY TABLE tmp_season_types (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    PRIMARY KEY (season, game_type)
  ) ENGINE = MEMORY
    SELECT DISTINCT season, game_type
    FROM tmp_game_list;

  # Whittle the full data down to info for the appropriate season / season_type
  DROP TEMPORARY TABLE IF EXISTS tmp_AB_FLAGS;
  CREATE TEMPORARY TABLE tmp_AB_FLAGS LIKE SPORTS_MLB_GAME_ATBAT_FLAGS;
  ALTER TABLE tmp_AB_FLAGS
    ADD COLUMN winprob DECIMAL(4, 1) SIGNED,
    ADD COLUMN is_game TINYINT UNSIGNED,
    ADD KEY is_game (is_game),
    ADD KEY by_matchup_join (game_type, batter, pitcher),
    ADD KEY by_matchup_ins (season, game_type, batter, pitcher);
  INSERT INTO tmp_AB_FLAGS
    SELECT AB_FLAGS.*,
           PLAY.change_winprob AS winprob,
           tmp_game_list.game_id IS NOT NULL AS is_game
    FROM tmp_season_types
    JOIN SPORTS_MLB_GAME_ATBAT_FLAGS AS AB_FLAGS
      ON (AB_FLAGS.season = tmp_season_types.season
      AND AB_FLAGS.game_type = tmp_season_types.game_type)
    JOIN SPORTS_MLB_GAME_PLAYS AS PLAY
      ON (PLAY.season = AB_FLAGS.season
      AND PLAY.game_type = AB_FLAGS.game_type
      AND PLAY.game_id = AB_FLAGS.game_id
      AND PLAY.play_id = AB_FLAGS.play_id)
    LEFT JOIN tmp_game_list
      ON (tmp_game_list.season = AB_FLAGS.season
      AND tmp_game_list.game_type = AB_FLAGS.game_type
      AND tmp_game_list.game_id = AB_FLAGS.game_id);

  # Determine appropriate matchups from the game list
  DROP TEMPORARY TABLE IF EXISTS tmp_batvpit_matchups;
  CREATE TEMPORARY TABLE tmp_batvpit_matchups (
    batter MEDIUMINT UNSIGNED,
    pitcher MEDIUMINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    PRIMARY KEY (batter, pitcher, season_type),
    KEY rev (season_type, batter, pitcher)
  ) ENGINE = MEMORY
    SELECT DISTINCT batter, pitcher, game_type AS season_type
    FROM tmp_AB_FLAGS
    WHERE is_game = 1;

  # Then get just the instances of these matchups in the appropriate season types
  DROP TEMPORARY TABLE IF EXISTS tmp_AB_FLAGS_matchups;
  CREATE TEMPORARY TABLE tmp_AB_FLAGS_matchups LIKE tmp_AB_FLAGS;
  INSERT INTO tmp_AB_FLAGS_matchups
    SELECT tmp_AB_FLAGS.*
    FROM tmp_batvpit_matchups
    JOIN tmp_AB_FLAGS
      ON (tmp_AB_FLAGS.game_type = tmp_batvpit_matchups.season_type
      AND tmp_AB_FLAGS.batter = tmp_batvpit_matchups.batter
      AND tmp_AB_FLAGS.pitcher = tmp_batvpit_matchups.pitcher);

END $$

DELIMITER ;

#
# Single Season totals
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batter_vs_pitcher_season`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batter_vs_pitcher_season`()
    COMMENT 'Batter vs Pitcher stats over the course of a season'
BEGIN

  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER (season, season_type, batter, pitcher, gp, pa, ab, obp_evt, h, 1b, 2b, 3b, hr, rbi, bb, ibb, hbp, k, tb, avg, obp, slg, ops, winprob)
    SELECT season, game_type AS season_type, batter, pitcher,
           COUNT(DISTINCT game_id) AS gp,
           SUM(is_pa) AS pa,
           SUM(is_ab) AS ab,
           SUM(is_obp_evt) AS obp_evt,
           SUM(is_hit) AS h,
           SUM(tb = 1) AS 1b,
           SUM(tb = 2) AS 2b,
           SUM(tb = 3) AS 3b,
           SUM(tb = 4) AS hr,
           SUM(rbi) AS rbi,
           SUM(is_bb) AS bb,
           SUM(is_ibb) AS ibb,
           SUM(is_hbp) AS hbp,
           SUM(is_k) AS k,
           SUM(tb) AS tb,
           IF(SUM(is_ab) > 0, SUM(is_hit) / SUM(is_ab), NULL) AS avg,
           IF(SUM(is_obp_evt) > 0, SUM(is_hit + is_bb + is_hbp) / SUM(is_obp_evt), NULL) AS obp,
           IF(SUM(is_ab) > 0, SUM(tb) / SUM(is_ab), NULL) AS slg,
           IF(SUM(is_obp_evt) > 0, (SUM(is_hit + is_bb + is_hbp) / SUM(is_obp_evt)) + IF(SUM(is_ab) > 0, SUM(tb) / SUM(is_ab), 0), NULL) AS ops,
           SUM(winprob) AS winprob
    FROM tmp_AB_FLAGS_matchups
    GROUP BY season, game_type, batter, pitcher
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          pa = VALUES(pa),
                          ab = VALUES(ab),
                          obp_evt = VALUES(obp_evt),
                          h = VALUES(h),
                          1b = VALUES(1b),
                          2b = VALUES(2b),
                          3b = VALUES(3b),
                          hr = VALUES(hr),
                          rbi = VALUES(rbi),
                          bb = VALUES(bb),
                          ibb = VALUES(ibb),
                          hbp = VALUES(hbp),
                          k = VALUES(k),
                          tb = VALUES(tb),
                          avg = VALUES(avg),
                          obp = VALUES(obp),
                          slg = VALUES(slg),
                          ops = VALUES(ops),
                          winprob = VALUES(winprob);

END $$

DELIMITER ;

#
# Career totals
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batter_vs_pitcher_career`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batter_vs_pitcher_career`()
    COMMENT 'Batter vs Pitcher stats over the course of a career'
BEGIN

  INSERT INTO SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER (batter, pitcher, season_type, seasons, gp, pa, ab, obp_evt, h, 1b, 2b, 3b, hr, rbi, bb, ibb, hbp, k, tb, avg, obp, slg, ops, winprob)
    SELECT INFO.batter, INFO.pitcher, INFO.season_type,
           COUNT(DISTINCT STATS.season) AS seasons,
           SUM(STATS.gp) AS gp,
           SUM(STATS.pa) AS pa,
           SUM(STATS.ab) AS ab,
           SUM(STATS.obp_evt) AS obp_evt,
           SUM(STATS.h) AS h,
           SUM(STATS.1b) AS 1b,
           SUM(STATS.2b) AS 2b,
           SUM(STATS.3b) AS 3b,
           SUM(STATS.hr) AS hr,
           SUM(STATS.rbi) AS rbi,
           SUM(STATS.bb) AS bb,
           SUM(STATS.ibb) AS ibb,
           SUM(STATS.hbp) AS hbp,
           SUM(STATS.k) AS k,
           SUM(STATS.tb) AS tb,
           IF(SUM(STATS.ab) > 0, SUM(STATS.h) / SUM(STATS.ab), NULL) AS avg,
           IF(SUM(STATS.obp_evt) > 0, SUM(STATS.h + STATS.bb + STATS.hbp) / SUM(STATS.obp_evt), NULL) AS obp,
           IF(SUM(STATS.ab) > 0, SUM(STATS.tb) / SUM(STATS.ab), NULL) AS slg,
           IF(SUM(STATS.obp_evt) > 0, (SUM(STATS.h + STATS.bb + STATS.hbp) / SUM(STATS.obp_evt)) + IF(SUM(STATS.ab) > 0, SUM(STATS.tb) / SUM(STATS.ab), 0), NULL) AS ops,
           SUM(STATS.winprob) AS winprob
    FROM tmp_batvpit_matchups AS INFO
    JOIN SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER AS STATS
      ON (STATS.batter = INFO.batter
      AND STATS.pitcher = INFO.pitcher
      AND STATS.season_type = INFO.season_type)
    GROUP BY INFO.batter, INFO.pitcher, INFO.season_type
  ON DUPLICATE KEY UPDATE seasons = VALUES(seasons),
                          gp = VALUES(gp),
                          pa = VALUES(pa),
                          ab = VALUES(ab),
                          obp_evt = VALUES(obp_evt),
                          h = VALUES(h),
                          1b = VALUES(1b),
                          2b = VALUES(2b),
                          3b = VALUES(3b),
                          hr = VALUES(hr),
                          rbi = VALUES(rbi),
                          bb = VALUES(bb),
                          ibb = VALUES(ibb),
                          hbp = VALUES(hbp),
                          k = VALUES(k),
                          tb = VALUES(tb),
                          avg = VALUES(avg),
                          obp = VALUES(obp),
                          slg = VALUES(slg),
                          ops = VALUES(ops),
                          winprob = VALUES(winprob);

END $$

DELIMITER ;

#
# Table maintenance
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batter_vs_pitcher_tidy`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batter_vs_pitcher_tidy`()
    COMMENT 'Career totals for MLB players post-processing'
BEGIN

  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER ORDER BY season, season_type, batter, pitcher;
  ALTER TABLE SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER ORDER BY batter, pitcher, season_type;

END $$

DELIMITER ;
