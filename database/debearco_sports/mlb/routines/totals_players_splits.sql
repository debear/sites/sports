#
# Break down player stats in to Splits/Situational
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits`()
    COMMENT 'Determine split/situational stats for players over a season'
BEGIN

  # Setup the temporary tables (players to update and stats to calc)
  CALL mlb_totals_players_splits_setup();

  # Do the batters
  CALL mlb_totals_players_splits_batters();
  # Then the pitchers
  CALL mlb_totals_players_splits_pitchers();
  CALL mlb_pitcher_repertoire();
  # Finally the fielders
  CALL mlb_totals_players_splits_fielders();

  # End with Batter v Pitchers
  CALL mlb_totals_players_splits_batter_vs_pitcher();

  # Table maintenance
  CALL mlb_totals_players_splits_tidy();

END $$

DELIMITER ;

#
# Basic setup
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_setup`()
    COMMENT 'Determine split/situational stats for players over a season'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_post_asb_date DATE;

  # Season type(s) we're processing and the players we're processing (save doing everyone every time...)
  DROP TEMPORARY TABLE IF EXISTS tmp_splits_season_players;
  CREATE TEMPORARY TABLE tmp_splits_season_players (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    player_id MEDIUMINT UNSIGNED,
    PRIMARY KEY by_player (season, game_type, player_id)
  ) ENGINE = MEMORY
    SELECT DISTINCT SPORTS_MLB_GAME_ROSTERS.season, SPORTS_MLB_GAME_ROSTERS.game_type, SPORTS_MLB_GAME_ROSTERS.player_id
    FROM tmp_game_list
    JOIN SPORTS_MLB_GAME_ROSTERS
      ON (SPORTS_MLB_GAME_ROSTERS.season = tmp_game_list.season
      AND SPORTS_MLB_GAME_ROSTERS.game_type = tmp_game_list.game_type
      AND SPORTS_MLB_GAME_ROSTERS.game_id = tmp_game_list.game_id);

  DROP TEMPORARY TABLE IF EXISTS tmp_split_players;
  CREATE TEMPORARY TABLE tmp_split_players (
    player_id MEDIUMINT UNSIGNED,
    PRIMARY KEY (player_id)
  ) ENGINE = MEMORY
    SELECT DISTINCT player_id
    FROM tmp_splits_season_players;

  # Short-cut rosters for this season
  DROP TEMPORARY TABLE IF EXISTS tmp_splits_types;
  CREATE TEMPORARY TABLE tmp_splits_types (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    PRIMARY KEY (season, game_type)
  ) ENGINE = MEMORY
    SELECT DISTINCT season, game_type
    FROM tmp_game_list;

  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_GAME_ROSTERS;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_GAME_ROSTERS LIKE SPORTS_MLB_GAME_ROSTERS;
  ALTER TABLE tmp_SPORTS_MLB_GAME_ROSTERS ENGINE = MEMORY;
  INSERT INTO tmp_SPORTS_MLB_GAME_ROSTERS
    SELECT SPORTS_MLB_GAME_ROSTERS.*
    FROM tmp_splits_types
    JOIN SPORTS_MLB_GAME_ROSTERS
      ON (SPORTS_MLB_GAME_ROSTERS.season = tmp_splits_types.season
      AND SPORTS_MLB_GAME_ROSTERS.game_type = tmp_splits_types.game_type);

  # Determine the all-star break
  SELECT season INTO v_season FROM tmp_game_list LIMIT 1;
  DROP TEMPORARY TABLE IF EXISTS tmp_sched_dates;
  CREATE TEMPORARY TABLE tmp_sched_dates (
    game_date DATE,
    date_diff TINYINT UNSIGNED,
    PRIMARY KEY (game_date)
  ) ENGINE = MEMORY
    SELECT DISTINCT game_date, 0 AS date_diff
    FROM SPORTS_MLB_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    ORDER BY game_date;

  CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpA');
  CALL _duplicate_tmp_table('tmp_sched_dates', 'tmp_sched_dates_cpB');
  INSERT INTO tmp_sched_dates (game_date, date_diff)
    SELECT tmp_sched_dates_cpA.game_date,
           DATEDIFF(tmp_sched_dates_cpA.game_date, MAX(tmp_sched_dates_cpB.game_date)) AS date_diff
    FROM tmp_sched_dates_cpA
    JOIN tmp_sched_dates_cpB
      ON (tmp_sched_dates_cpB.game_date < tmp_sched_dates_cpA.game_date)
    GROUP BY tmp_sched_dates_cpA.game_date
  ON DUPLICATE KEY UPDATE date_diff = VALUES(date_diff);

  SELECT game_date INTO v_post_asb_date FROM tmp_sched_dates ORDER BY date_diff DESC LIMIT 1;

  # And game info too (to identify stadia, opponent)
  DROP TEMPORARY TABLE IF EXISTS tmp_splits_games;
  CREATE TEMPORARY TABLE tmp_splits_games (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    game_id SMALLINT UNSIGNED,
    home VARCHAR(3),
    visitor VARCHAR(3),
    month VARCHAR(9),
    pre_asb TINYINT UNSIGNED,
    stadium VARCHAR(55),
    PRIMARY KEY (season, game_type, game_id)
  ) ENGINE = MEMORY
    SELECT SCHED.season,
           SCHED.game_type,
           SCHED.game_id,
           SCHED.home,
           SCHED.visitor,
           DATE_FORMAT(SCHED.game_date, '%M') AS month,
           SCHED.game_date < v_post_asb_date AS pre_asb,
           IFNULL(IFNULL(ALT_STADIUM.alt_linked, CONCAT(ALT_STADIUM.team_id, ':', ALT_STADIUM.building_id)), CONCAT(STADIUM.team_id, ':', STADIUM.building_id)) AS stadium
    FROM tmp_splits_types
    JOIN SPORTS_MLB_SCHEDULE AS SCHED
      ON (SCHED.season = tmp_splits_types.season
      AND SCHED.game_type = tmp_splits_types.game_type)
    LEFT JOIN SPORTS_MLB_TEAMS_STADIA AS STADIUM
      ON (STADIUM.team_id = SCHED.home
      AND SCHED.season BETWEEN STADIUM.season_from AND IFNULL(STADIUM.season_to, 2099))
    LEFT JOIN SPORTS_MLB_TEAMS_STADIA AS ALT_STADIUM
      ON (ALT_STADIUM.team_id = '_ALT'
      AND ALT_STADIUM.season_from <= SCHED.season
      AND ALT_STADIUM.season_to >= IFNULL(SCHED.season, 2099)
      AND ALT_STADIUM.building_id = SCHED.alt_venue);

  # Get relevant games played info (just once for all calcs...)
  DROP TEMPORARY TABLE IF EXISTS tmp_splits_rosters;
  CREATE TEMPORARY TABLE tmp_splits_rosters (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    game_id SMALLINT UNSIGNED,
    player_id MEDIUMINT UNSIGNED,
    team_id VARCHAR(3),
    opp_team_id VARCHAR(3),
    month VARCHAR(9),
    is_home TINYINT UNSIGNED,
    pre_asb TINYINT UNSIGNED,
    stadium VARCHAR(55),
    played TINYINT UNSIGNED,
    started TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, game_id, player_id),
    KEY by_player (season, game_type, player_id)
  ) ENGINE = MEMORY
    SELECT tmp_SPORTS_MLB_GAME_ROSTERS.season,
           tmp_SPORTS_MLB_GAME_ROSTERS.game_type,
           tmp_SPORTS_MLB_GAME_ROSTERS.game_id,
           tmp_SPORTS_MLB_GAME_ROSTERS.player_id,
           tmp_SPORTS_MLB_GAME_ROSTERS.team_id,
           IF(tmp_SPORTS_MLB_GAME_ROSTERS.team_id = tmp_splits_games.home,
              tmp_splits_games.visitor,
              tmp_splits_games.home) AS opp_team_id,
           tmp_splits_games.month,
           tmp_SPORTS_MLB_GAME_ROSTERS.team_id = tmp_splits_games.home AS is_home,
           tmp_splits_games.pre_asb,
           tmp_splits_games.stadium,
           tmp_SPORTS_MLB_GAME_ROSTERS.played,
           tmp_SPORTS_MLB_GAME_ROSTERS.started
    FROM tmp_splits_season_players
    JOIN tmp_SPORTS_MLB_GAME_ROSTERS
      ON (tmp_SPORTS_MLB_GAME_ROSTERS.season = tmp_splits_season_players.season
      AND tmp_SPORTS_MLB_GAME_ROSTERS.game_type = tmp_splits_season_players.game_type
      AND tmp_SPORTS_MLB_GAME_ROSTERS.player_id = tmp_splits_season_players.player_id)
    JOIN tmp_splits_games
      ON (tmp_splits_games.season = tmp_SPORTS_MLB_GAME_ROSTERS.season
      AND tmp_splits_games.game_type = tmp_SPORTS_MLB_GAME_ROSTERS.game_type
      AND tmp_splits_games.game_id = tmp_SPORTS_MLB_GAME_ROSTERS.game_id)
    WHERE tmp_SPORTS_MLB_GAME_ROSTERS.played = 1;

  # Finally, take a reduced copy of the ab-flags
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_GAME_ATBAT_FLAGS;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_GAME_ATBAT_FLAGS LIKE SPORTS_MLB_GAME_ATBAT_FLAGS;
  ALTER TABLE tmp_SPORTS_MLB_GAME_ATBAT_FLAGS ENGINE = MEMORY;
  INSERT INTO tmp_SPORTS_MLB_GAME_ATBAT_FLAGS
    SELECT SPORTS_MLB_GAME_ATBAT_FLAGS.*
    FROM tmp_splits_games
    JOIN SPORTS_MLB_GAME_ATBAT_FLAGS
      ON (SPORTS_MLB_GAME_ATBAT_FLAGS.season = tmp_splits_games.season
      AND SPORTS_MLB_GAME_ATBAT_FLAGS.game_type = tmp_splits_games.game_type
      AND SPORTS_MLB_GAME_ATBAT_FLAGS.game_id = tmp_splits_games.game_id);

END $$

DELIMITER ;

#
# Table maintenance
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_tidy`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_tidy`()
    COMMENT 'Tidy split player tables'
BEGIN

  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS ORDER BY season, season_type, player_id, split_type, split_label;
  ALTER TABLE SPORTS_MLB_PLAYERS_CAREER_BATTING_SPLITS ORDER BY player_id, season_type, split_type, split_label;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS ORDER BY season, season_type, player_id, split_type, split_label;
  ALTER TABLE SPORTS_MLB_PLAYERS_CAREER_PITCHING_SPLITS ORDER BY player_id, season_type, split_type, split_label;
  ALTER TABLE SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS ORDER BY season, season_type, player_id, split_type, split_label;
  ALTER TABLE SPORTS_MLB_PLAYERS_CAREER_FIELDING_SPLITS ORDER BY player_id, season_type, split_type, split_label;

END $$

DELIMITER ;
