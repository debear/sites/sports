##
## Player sorting qualification logic
##
DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__tmp_qual_prep`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__tmp_qual_prep`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Standard MLB qualifying criteria pre work'
BEGIN

  # Build from a small table of players that occur in every game
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_SEASON_teamGP;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_SEASON_teamGP (
    team_id VARCHAR(3),
    num TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, COUNT(DISTINCT game_id) AS num
    FROM SPORTS_MLB_PLAYERS_GAME_BATTING
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__tmp_qual_calc_gp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__tmp_qual_calc_gp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15)
)
    COMMENT 'Determine player-by-player MLB GP for sorting calcs'
BEGIN

  # First, the obvious mapping
  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_TEAMGP;'));
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_TEAMGP (
      season SMALLINT UNSIGNED,
      season_type ENUM(\'regular\',\'playoff\'),
      player_id SMALLINT UNSIGNED,
      team_id VARCHAR(3),
      num_gp TINYINT UNSIGNED,
      PRIMARY KEY (season, season_type, player_id, team_id)
    ) ENGINE = MEMORY
      SELECT STAT.season, STAT.season_type, STAT.player_id, STAT.team_id,
             TEAM_GP.num AS num_gp
      FROM SPORTS_MLB_PLAYERS_SEASON_', v_table, ' AS STAT
      JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_teamGP AS TEAM_GP
        ON (TEAM_GP.team_id = STAT.team_id)
      WHERE STAT.season = ', v_season, '
      AND   STAT.season_type = \'', v_season_type, '\'
      AND   STAT.team_id <> \'_ML\';'));

  # Then group together the season aggregation / composite rows
  CALL _exec(CONCAT(
   'INSERT INTO tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_TEAMGP (season, season_type, player_id, team_id, num_gp)
      SELECT MULTI.season, MULTI.season_type, MULTI.player_id, MULTI.team_id,
             MIN(TEAM_GP.num) AS num_gp
      FROM SPORTS_MLB_PLAYERS_SEASON_', v_table, ' AS MULTI
      JOIN SPORTS_MLB_PLAYERS_SEASON_', v_table, ' AS COMPONENTS
        ON (COMPONENTS.season = MULTI.season
        AND COMPONENTS.season_type = MULTI.season_type
        AND COMPONENTS.player_id = MULTI.player_id
        AND COMPONENTS.team_id <> \'_ML\')
      JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_teamGP AS TEAM_GP
        ON (TEAM_GP.team_id = COMPONENTS.team_id)
      WHERE MULTI.season = ', v_season, '
      AND   MULTI.season_type = \'', v_season_type, '\'
      AND   MULTI.team_id = \'_ML\'
      GROUP BY MULTI.season, MULTI.season_type, MULTI.player_id;'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__tmp_qual_update`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__tmp_qual_update`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col VARCHAR(10)
)
    COMMENT 'Standard MLB qualifying criteria pre work'
BEGIN

  CALL _exec(CONCAT(
   'UPDATE tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_QUAL AS QUAL
    JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, ' AS STAT
      ON (STAT.season = QUAL.season
      AND STAT.season_type = QUAL.season_type
      AND STAT.player_id = QUAL.player_id
      AND STAT.team_id = QUAL.team_id)
    SET STAT.', v_col, ' = QUAL.', v_col, ';'));

END $$

DELIMITER ;

##
## Batting
##
DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__tmp_qual_batting`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__tmp_qual_batting`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for MLB batting stats'
BEGIN

  CALL mlb_totals_players_sort__tmp_qual_calc_gp(v_season, v_season_type, 'BATTING');

  # AVG; >= 3.1 PA / team game
  # OBP; >= 3.1 PA / team game
  # SLG; >= 3.1 PA / team game
  # OPS; >= 3.1 PA / team game
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_SEASON_BATTING_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_SEASON_BATTING_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    team_id VARCHAR(3),
    is_totals TINYINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id, team_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id, STAT.team_id, STAT.is_totals,
           STAT.pa >= (TEAM_GP.num_gp * 3.1) AS qual
    FROM SPORTS_MLB_PLAYERS_SEASON_BATTING AS STAT
    JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_BATTING_TEAMGP AS TEAM_GP
      ON (TEAM_GP.season = STAT.season
      AND TEAM_GP.season_type = STAT.season_type
      AND TEAM_GP.player_id = STAT.player_id
      AND TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type;

  CALL mlb_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'BATTING', 'qual');

END $$

DELIMITER ;

##
## Pitching
##
DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__tmp_qual_pitching`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__tmp_qual_pitching`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for MLB pitching stats'
BEGIN

  CALL mlb_totals_players_sort__tmp_qual_calc_gp(v_season, v_season_type, 'PITCHING');

  # ERA:  >= 1.0 IP / team game (aka >= 3 outs / team game)
  # WHIP: >= 1.0 IP / team game (aka >= 3 outs / team game)
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_SEASON_PITCHING_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_SEASON_PITCHING_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    team_id VARCHAR(3),
    is_totals TINYINT UNSIGNED,
    qual_ratio TINYINT UNSIGNED,
    qual_rp TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id, team_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id, STAT.team_id, STAT.is_totals,
           STAT.out >= (TEAM_GP.num_gp * 3) AS qual_ratio,
           STAT.svo > 0 AS qual_rp
    FROM SPORTS_MLB_PLAYERS_SEASON_PITCHING AS STAT
    JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_PITCHING_TEAMGP AS TEAM_GP
      ON (TEAM_GP.season = STAT.season
      AND TEAM_GP.season_type = STAT.season_type
      AND TEAM_GP.player_id = STAT.player_id
      AND TEAM_GP.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type;

  CALL mlb_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'PITCHING', 'qual_ratio');
  CALL mlb_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'PITCHING', 'qual_rp');

END $$

DELIMITER ;

##
## Fielding
##
DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__tmp_qual_fielding`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__tmp_qual_fielding`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for MLB fielding stats'
BEGIN

  # Qualified Batter or Pitcher...
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_SEASON_FIELDING_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_SEASON_FIELDING_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    team_id VARCHAR(3),
    is_totals TINYINT UNSIGNED,
    qual TINYINT UNSIGNED,
    PRIMARY KEY (season, season_type, player_id, team_id)
  ) ENGINE = MEMORY
    SELECT STAT.season, STAT.season_type, STAT.player_id, STAT.team_id, STAT.is_totals,
           IFNULL(STAT_BAT.qual, 0) OR IFNULL(STAT_PITCH.qual_ratio, 0) AS qual
    FROM SPORTS_MLB_PLAYERS_SEASON_FIELDING AS STAT
    LEFT JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_BATTING_QUAL AS STAT_BAT
      ON (STAT_BAT.season = STAT.season
      AND STAT_BAT.season_type = STAT.season_type
      AND STAT_BAT.player_id = STAT.player_id
      AND STAT_BAT.team_id = STAT.team_id)
    LEFT JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_PITCHING_QUAL AS STAT_PITCH
      ON (STAT_PITCH.season = STAT.season
      AND STAT_PITCH.season_type = STAT.season_type
      AND STAT_PITCH.player_id = STAT.player_id
      AND STAT_PITCH.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    GROUP BY STAT.season, STAT.season_type, STAT.player_id, STAT.team_id;

  CALL mlb_totals_players_sort__tmp_qual_update(v_season, v_season_type, 'FIELDING', 'qual');

END $$

DELIMITER ;
