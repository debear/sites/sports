##
## MLB power rank calculations
##
DROP PROCEDURE IF EXISTS `mlb_power_ranks_dates`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_dates`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate when to do the weekly MLB power ranks'
BEGIN

  # Declare vars
  DECLARE v_first_game DATE;
  DECLARE v_first_week DATE;
  DECLARE v_last_week DATE;
  DECLARE v_post_asb_date DATE;

  # Clear a previous run
  DELETE FROM SPORTS_MLB_POWER_RANKINGS WHERE season = v_season;
  DELETE FROM SPORTS_MLB_POWER_RANKINGS_WEEKS WHERE season = v_season;

  # List of game dates
  DROP TEMPORARY TABLE IF EXISTS tmp_game_list;
  CREATE TEMPORARY TABLE tmp_game_list (
    the_date DATE,
    PRIMARY KEY (the_date)
  ) ENGINE = MEMORY
    SELECT game_date AS the_date
    FROM SPORTS_MLB_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    GROUP BY game_date
    ORDER BY game_date;

  # Get first and last days of the season (to calculate when to start Power Ranks)
  SELECT MIN(the_date), MIN(the_date), MAX(the_date)
           INTO v_first_game, v_first_week, v_last_week
  FROM tmp_game_list;

  # Adjust v_first_week to the following Monday, unless it falls no later than a Wednesday
  SELECT IF(DATE_FORMAT(v_first_week, '%w') IN (1, 2, 3),
            DATE_ADD(v_first_week, INTERVAL 8 - DATE_FORMAT(v_first_week, '%w') DAY),
            DATE_ADD(v_first_week, INTERVAL 15 - IF(DATE_FORMAT(v_first_week, '%w') = 0, 7, DATE_FORMAT(v_first_week, '%w')) DAY))
          INTO v_first_week;

  # Adjust v_last_week to the following Monday it falls on a Thursday or later
  SELECT IF(DATE_FORMAT(v_last_week, '%w') IN (0, 4, 5, 6),
            DATE_ADD(v_last_week, INTERVAL 8 - IF(DATE_FORMAT(v_last_week, '%w') = 0, 7, DATE_FORMAT(v_last_week, '%w')) DAY),
            v_last_week)
          INTO v_last_week;

  # Build our table of Mondays
  CALL mlb_power_ranks_dates_Mon(v_first_week, v_last_week);

  # Delete the power rank weeks where <4 days of games were played
  DROP TEMPORARY TABLE IF EXISTS tmp_date_gap;
  CREATE TEMPORARY TABLE tmp_date_gap (
    the_date DATE,
    days_games TINYINT UNSIGNED,
    PRIMARY KEY (the_date),
    INDEX days_games (days_games)
  ) ENGINE = MEMORY
    SELECT tmp_date_list.the_date,
           COUNT(DISTINCT tmp_game_list.the_date) AS days_games
    FROM tmp_date_list
    LEFT JOIN tmp_game_list
      ON (tmp_game_list.the_date BETWEEN DATE_SUB(tmp_date_list.the_date, INTERVAL 7 DAY)
                                     AND DATE_SUB(tmp_date_list.the_date, INTERVAL 1 DAY))
    GROUP BY tmp_date_list.the_date
    ORDER BY tmp_date_list.the_date;

  DELETE tmp_date_list.*
  FROM tmp_date_gap
  JOIN tmp_date_list
    ON (tmp_date_list.the_date = tmp_date_gap.the_date)
  WHERE tmp_date_gap.days_games < 4;

  # Create from temp table
  CALL _duplicate_tmp_table('tmp_date_list', 'tmp_date_list_cp');

  INSERT INTO SPORTS_MLB_POWER_RANKINGS_WEEKS (season, week, calc_date, date_from, date_to, when_run)
    SELECT v_season, COUNT(tmp_date_list_cp.the_date) + 1, tmp_date_list.the_date, '1970-01-01', DATE_SUB(tmp_date_list.the_date, INTERVAL 1 DAY), NULL
    FROM tmp_date_list
    LEFT JOIN tmp_date_list_cp
      ON (tmp_date_list_cp.the_date < tmp_date_list.the_date)
    GROUP BY tmp_date_list.the_date
    ORDER BY tmp_date_list.the_date;

  UPDATE SPORTS_MLB_POWER_RANKINGS_WEEKS AS WEEK
  LEFT JOIN SPORTS_MLB_POWER_RANKINGS_WEEKS AS PREV_WEEK
    ON (PREV_WEEK.season = WEEK.season
    AND PREV_WEEK.week = WEEK.week - 1)
  SET WEEK.date_from = IF(PREV_WEEK.date_to IS NOT NULL,
                          DATE_ADD(PREV_WEEK.date_to, INTERVAL 1 DAY),
                          v_first_game)
  WHERE WEEK.season = v_season;

  # Tidy
  ALTER TABLE SPORTS_MLB_POWER_RANKINGS_WEEKS ORDER BY season, week;

END $$

DELIMITER ;

#
# Create the list of Mondays between two dates
#
DELIMITER $$
DROP PROCEDURE IF EXISTS  `mlb_power_ranks_dates_Mon` $$

CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_dates_Mon`
(
  v_start_date DATE,
  v_end_date DATE
)
    MODIFIES SQL DATA
    COMMENT 'Creates tmp table of Mondays between two dates'
BEGIN

    DECLARE v_in_date DATE;
    DECLARE v_counter INT DEFAULT 0;
    SET v_in_date = v_start_date;

    DROP TEMPORARY TABLE IF EXISTS tmp_date_list;
    CREATE TEMPORARY TABLE IF NOT EXISTS tmp_date_list (
        the_date DATE NOT NULL,
        PRIMARY KEY (the_date)
    ) ENGINE = MEMORY;

    REPEAT
        SET v_counter = v_counter + 1;
        INSERT IGNORE INTO tmp_date_list (the_date) SELECT v_in_date AS the_date;
        SELECT DATE_ADD(v_in_date, INTERVAL 7 DAY) INTO v_in_date;
    UNTIL v_in_date > v_end_date OR v_counter > 50
    END REPEAT;

END $$
DELIMITER ;

