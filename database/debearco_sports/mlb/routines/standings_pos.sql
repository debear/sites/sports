##
## Determine positions for a particular position type
##
DROP PROCEDURE IF EXISTS `mlb_standings_pos`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_pos`(
  v_season SMALLINT UNSIGNED,
  v_col VARCHAR(6)
)
    COMMENT 'Calculate MLB standing positions'
BEGIN

  DECLARE v_loop TINYINT UNSIGNED;

  #
  # Stage 0: Simplify our query by generalising our position and join cols
  #
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = IF("', v_col, '" = "league", _tb_league_id, IF("', v_col, '" = "conf", _tb_conf_id, _tb_div_id));'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  #
  # Stage 1: Basic sort
  #
  CALL mlb_standings_ties_sort(v_season, v_col);

  #
  # Stage 2: Tie-breaks...
  #

  # Head-to-head records
  CALL mlb_standings_ties_identify();
  IF mlb_standings_ties_num() > 0 THEN
    CALL mlb_standings_ties_h2h_pct();
    CALL mlb_standings_ties_break();
  END IF;

  # Intra-division win percentage
  CALL mlb_standings_ties_identify();
  IF mlb_standings_ties_num() > 0 THEN
    CALL mlb_standings_ties_intra_div();
    CALL mlb_standings_ties_break();
  END IF;

  # Intra-conference win percentage
  CALL mlb_standings_ties_identify();
  IF mlb_standings_ties_num() > 0 THEN
    CALL mlb_standings_ties_intra_conf();
    CALL mlb_standings_ties_break();
  END IF;

  # Final 81-85 intr-conf records
  SET v_loop = 81;
  WHILE v_loop <= 85 DO
    CALL mlb_standings_ties_identify();
    IF mlb_standings_ties_num() > 0 THEN
      CALL mlb_standings_ties_final_games(v_loop);
      CALL mlb_standings_ties_break();
    END IF;
    SET v_loop = v_loop + 1;
  END WHILE;

  # Aribtrary final (guaranteed) sort
  CALL mlb_standings_ties_identify();
  IF mlb_standings_ties_num() > 0 THEN
    CALL mlb_standings_ties_guaranteed();
    CALL mlb_standings_ties_break();
  END IF;

  #
  # Stage Z: Propogate back to standings table
  #
  CALL _exec(CONCAT('UPDATE tmp_standings SET pos_', v_col, ' = _tb_pos;'));

END $$

DELIMITER ;
