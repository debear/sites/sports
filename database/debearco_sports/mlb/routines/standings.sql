##
## Calculate the standings
##
DROP PROCEDURE IF EXISTS `mlb_standings`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings`(
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate MLB standings'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;

  # Setup our run
  CALL mlb_standings_setup(v_season);

  # Basic standing details
  CALL mlb_standings_calcs(v_recent_games);

  # Calculate positions
  CALL mlb_standings_pos(v_season, 'div');
  CALL mlb_standings_pos(v_season, 'conf');
  CALL mlb_standings_pos(v_season, 'league');

  # Games Back calcs
  CALL mlb_standings_games_back(v_season);

  # Status Codes
  CALL mlb_standings_codes(v_season);

  # Store in the final table...
  CALL mlb_standings_store();

  # Tidy the table up...
  CALL mlb_standings_tidy();

  # Copy in to the season archive table
  CALL mlb_standings_history();

END $$

DELIMITER ;
