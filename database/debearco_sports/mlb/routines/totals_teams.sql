##
## Team season totals
##
DROP PROCEDURE IF EXISTS `mlb_totals_teams`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for MLB teams'
BEGIN

  #
  # Batting
  #
  INSERT INTO `SPORTS_MLB_TEAMS_SEASON_BATTING` (`season`, `season_type`, `team_id`, `ab`, `pa`, `h`, `1b`, `2b`, `3b`, `xbh`, `bb`, `ibb`, `r`, `hr`, `rbi`, `sb`, `cs`, `po`, `avg`, `obp`, `tb`, `slg`, `ops`, `sac_fly`, `sac_hit`, `hbp`, `k`, `go`, `fo`, `gidp`, `lob`)
    SELECT `season`, `game_type`, `team_id`,
           SUM(`ab`) AS `ab`,
           SUM(`pa`) AS `pa`,
           SUM(`h`) AS `h`,
           SUM(`1b`) AS `1b`,
           SUM(`2b`) AS `2b`,
           SUM(`3b`) AS `3b`,
           SUM(`xbh`) AS `xbh`,
           SUM(`bb`) AS `bb`,
           SUM(`ibb`) AS `ibb`,
           SUM(`r`) AS `r`,
           SUM(`hr`) AS `hr`,
           SUM(`rbi`) AS `rbi`,
           SUM(`sb`) AS `sb`,
           SUM(`cs`) AS `cs`,
           SUM(`po`) AS `po`,
           IF(SUM(`ab`) > 0, SUM(`h`) / SUM(`ab`), NULL) AS `avg`,
           IF(SUM(`ab`) + SUM(`bb`) + SUM(`hbp`) + SUM(`sac_fly`) > 0, (SUM(`h`) + SUM(`bb`) + SUM(`hbp`)) / (SUM(`ab`) + SUM(`bb`) + SUM(`hbp`) + SUM(`sac_fly`)), NULL) AS `obp`,
           SUM(`tb`) AS `tb`,
           IF(SUM(`ab`) > 0, SUM(`tb`) / SUM(`ab`), NULL) AS `slg`,
           IF((IF(SUM(`ab`) + SUM(`bb`) + SUM(`hbp`) + SUM(`sac_fly`) > 0, (SUM(`h`) + SUM(`bb`) + SUM(`hbp`)) / (SUM(`ab`) + SUM(`bb`) + SUM(`hbp`) + SUM(`sac_fly`)), NULL) IS NOT NULL) OR (IF(SUM(`ab`) > 0, SUM(`tb`) / SUM(`ab`), NULL) IS NOT NULL),
              IFNULL(IF(SUM(`ab`) + SUM(`bb`) + SUM(`hbp`) + SUM(`sac_fly`) > 0, (SUM(`h`) + SUM(`bb`) + SUM(`hbp`)) / (SUM(`ab`) + SUM(`bb`) + SUM(`hbp`) + SUM(`sac_fly`)), NULL), 0) + IFNULL(IF(SUM(`ab`) > 0, SUM(`tb`) / SUM(`ab`), NULL), 0),
              NULL) AS `ops`,
           SUM(`sac_fly`) AS `sac_fly`,
           SUM(`sac_hit`) AS `sac_hit`,
           SUM(`hbp`) AS `hbp`,
           SUM(`k`) AS `k`,
           SUM(`go`) AS `go`,
           SUM(`fo`) AS `fo`,
           SUM(`gidp`) AS `gidp`,
           SUM(`lob`) AS `lob`
    FROM `SPORTS_MLB_TEAMS_GAME_BATTING`
    WHERE `season` = v_season
    AND   `game_type` = v_season_type
    GROUP BY `season`, `game_type`, `team_id`
  ON DUPLICATE KEY UPDATE `ab` = VALUES(`ab`),
                          `pa` = VALUES(`pa`),
                          `h` = VALUES(`h`),
                          `1b` = VALUES(`1b`),
                          `2b` = VALUES(`2b`),
                          `3b` = VALUES(`3b`),
                          `xbh` = VALUES(`xbh`),
                          `bb` = VALUES(`bb`),
                          `ibb` = VALUES(`ibb`),
                          `r` = VALUES(`r`),
                          `hr` = VALUES(`hr`),
                          `rbi` = VALUES(`rbi`),
                          `sb` = VALUES(`sb`),
                          `cs` = VALUES(`cs`),
                          `po` = VALUES(`po`),
                          `avg` = VALUES(`avg`),
                          `obp` = VALUES(`obp`),
                          `tb` = VALUES(`tb`),
                          `slg` = VALUES(`slg`),
                          `ops` = VALUES(`ops`),
                          `sac_fly` = VALUES(`sac_fly`),
                          `sac_hit` = VALUES(`sac_hit`),
                          `hbp` = VALUES(`hbp`),
                          `k` = VALUES(`k`),
                          `go` = VALUES(`go`),
                          `fo` = VALUES(`fo`),
                          `gidp` = VALUES(`gidp`),
                          `lob` = VALUES(`lob`);

  #
  # Pitching
  #
  INSERT INTO `SPORTS_MLB_TEAMS_SEASON_PITCHING` (`season`, `season_type`, `team_id`, `ip`, `out`, `bf`, `pt`, `b`, `s`, `k`, `h`, `hr`, `bb`, `ibb`, `wp`, `go`, `fo`, `go_fo`, `r`, `ra`, `er`, `era`, `ur`, `ura`, `ir`, `ira`, `whip`, `bk`, `sb`, `cs`, `po`, `w`, `l`, `sv`, `hld`, `bs`, `svo`, `cg`, `sho`, `qs`, `game_score`)
    SELECT `season`, `game_type`, `team_id`,
           CONCAT(FLOOR(SUM(`out`) / 3), '.', SUM(`out`) % 3) AS `ip`,
           SUM(`out`) AS `out`,
           SUM(`bf`) AS `bf`,
           SUM(`pt`) AS `pt`,
           SUM(`b`) AS `b`,
           SUM(`s`) AS `s`,
           SUM(`k`) AS `k`,
           SUM(`h`) AS `h`,
           SUM(`hr`) AS `hr`,
           SUM(`bb`) AS `bb`,
           SUM(`ibb`) AS `ibb`,
           SUM(`wp`) AS `wp`,
           SUM(`go`) AS `go`,
           SUM(`fo`) AS `fo`,
           IF(SUM(`fo`) > 0, SUM(`go`) / SUM(`fo`), NULL) AS `go_fo`,
           SUM(`r`) AS `r`,
           IF(SUM(`bf`) > 0, (SUM(`r`) / SUM(`out`)) * 27, NULL) AS `ra`,
           SUM(`er`) AS `er`,
           IF(SUM(`bf`) > 0, (SUM(`er`) / SUM(`out`)) * 27, NULL) AS `era`,
           SUM(`ur`) AS `ur`,
           IF(SUM(`bf`) > 0, (SUM(`ur`) / SUM(`out`)) * 27, NULL) AS `ura`,
           SUM(`ir`) AS `ir`,
           SUM(`ira`) AS `ira`,
           IF(SUM(`out`) > 0, ((SUM(`bb`) + SUM(`h`)) * 3) / SUM(`out`), NULL) AS `whip`,
           SUM(`bk`) AS `bk`,
           SUM(`sb`) AS `sb`,
           SUM(`cs`) AS `cs`,
           SUM(`po`) AS `po`,
           SUM(`w`) AS `w`,
           SUM(`l`) AS `l`,
           SUM(`sv`) AS `sv`,
           SUM(`hld`) AS `hld`,
           SUM(`bs`) AS `bs`,
           SUM(`svo`) AS `svo`,
           SUM(`cg`) AS `cg`,
           SUM(`sho`) AS `sho`,
           SUM(`qs`) AS `qs`,
           IF(SUM(`game_score` IS NOT NULL) > 0, SUM(IFNULL(`game_score`, 0)) / SUM(`game_score` IS NOT NULL), NULL) AS `game_score`
    FROM `SPORTS_MLB_TEAMS_GAME_PITCHING`
    WHERE `season` = v_season
    AND   `game_type` = v_season_type
    GROUP BY `season`, `game_type`, `team_id`
  ON DUPLICATE KEY UPDATE `ip` = VALUES(`ip`),
                          `out` = VALUES(`out`),
                          `bf` = VALUES(`bf`),
                          `pt` = VALUES(`pt`),
                          `b` = VALUES(`b`),
                          `s` = VALUES(`s`),
                          `k` = VALUES(`k`),
                          `h` = VALUES(`h`),
                          `hr` = VALUES(`hr`),
                          `bb` = VALUES(`bb`),
                          `ibb` = VALUES(`ibb`),
                          `wp` = VALUES(`wp`),
                          `go` = VALUES(`go`),
                          `fo` = VALUES(`fo`),
                          `go_fo` = VALUES(`go_fo`),
                          `r` = VALUES(`r`),
                          `ra` = VALUES(`ra`),
                          `er` = VALUES(`er`),
                          `era` = VALUES(`era`),
                          `ur` = VALUES(`ur`),
                          `ura` = VALUES(`ura`),
                          `ir` = VALUES(`ir`),
                          `ira` = VALUES(`ira`),
                          `whip` = VALUES(`whip`),
                          `bk` = VALUES(`bk`),
                          `sb` = VALUES(`sb`),
                          `cs` = VALUES(`cs`),
                          `po` = VALUES(`po`),
                          `w` = VALUES(`w`),
                          `l` = VALUES(`l`),
                          `sv` = VALUES(`sv`),
                          `hld` = VALUES(`hld`),
                          `bs` = VALUES(`bs`),
                          `svo` = VALUES(`svo`),
                          `cg` = VALUES(`cg`),
                          `sho` = VALUES(`sho`),
                          `qs` = VALUES(`qs`),
                          `game_score` = VALUES(`game_score`);

  #
  # Fielding
  #
  INSERT INTO `SPORTS_MLB_TEAMS_SEASON_FIELDING` (`season`, `season_type`, `team_id`, `tc`, `po`, `a`, `e`, `pct`, `dp`)
    SELECT `season`, `game_type`, `team_id`,
           SUM(`tc`) AS `tc`,
           SUM(`po`) AS `po`,
           SUM(`a`) AS `a`,
           SUM(`e`) AS `e`,
           IF((SUM(`po`) + SUM(`a`) + SUM(`e`)) > 0, (SUM(`po`) + SUM(`a`)) / (SUM(`po`) + SUM(`a`) + SUM(`e`)), NULL) AS `pct`,
           SUM(`dp`) AS `dp`
    FROM `SPORTS_MLB_TEAMS_GAME_FIELDING`
    WHERE `season` = v_season
    AND   `game_type` = v_season_type
    GROUP BY `season`, `game_type`, `team_id`
  ON DUPLICATE KEY UPDATE `tc` = VALUES(`tc`),
                          `po` = VALUES(`po`),
                          `a` = VALUES(`a`),
                          `e` = VALUES(`e`),
                          `pct` = VALUES(`pct`),
                          `dp` = VALUES(`dp`);

END $$

DELIMITER ;
