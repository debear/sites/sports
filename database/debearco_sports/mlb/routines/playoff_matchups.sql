##
## Playoff matchups
##  Need to process all four rounds, as this generates the round codes used in game_id allocation
##
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Determine MLB playoff matchups, allocating round_codes'
proc: BEGIN

  # Matchup details
  DECLARE v_series_date DATE;
  DECLARE v_round_num TINYINT UNSIGNED;
  DECLARE v_round_rows TINYINT UNSIGNED;
  DECLARE v_round_started TINYINT UNSIGNED;
  DECLARE v_round_complete TINYINT UNSIGNED;
  DECLARE v_counter TINYINT UNSIGNED DEFAULT 0;

  # Identify the conferences to loop through
  DECLARE v_conf_id TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Default first round (wildcard game started in 2012)
  DECLARE v_round_first TINYINT UNSIGNED DEFAULT 1;

  DECLARE cur_conf CURSOR FOR
    SELECT DISTINCT conf_id FROM tmp_seeds ORDER BY conf_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  IF v_season <= 2011 THEN
    SET v_round_first = 2;
  END IF;

  # Determine the round to process (ignoring future rounds, where not all teams are known at this stage)
  DROP TEMPORARY TABLE IF EXISTS tmp_series_rounds_stage0;
  CREATE TEMPORARY TABLE tmp_series_rounds_stage0 (
    round_num TINYINT UNSIGNED,
    last_date DATE,
    num_unknown TINYINT UNSIGNED,
    num_rows TINYINT UNSIGNED,
    started TINYINT UNSIGNED,
    complete TINYINT UNSIGNED,
    PRIMARY KEY (round_num)
  ) ENGINE = MEMORY
    SELECT SUBSTRING(round_code, 1, 1) AS round_num, MAX(the_date) AS last_date, SUM(0 IN (higher_seed, lower_seed)) AS num_unknown, COUNT(DISTINCT the_date) AS num_rows, NULL AS started, NULL AS complete
    FROM SPORTS_MLB_PLAYOFF_SERIES
    WHERE season = v_season
    GROUP BY round_num;
  DELETE FROM tmp_series_rounds_stage0 WHERE num_unknown > 0;
  ALTER TABLE tmp_series_rounds_stage0 DROP COLUMN num_unknown;
  DROP TEMPORARY TABLE IF EXISTS tmp_series_rounds;
  CREATE TEMPORARY TABLE tmp_series_rounds LIKE tmp_series_rounds_stage0;
  INSERT INTO tmp_series_rounds
    SELECT ROUNDS.round_num, ROUNDS.last_date, ROUNDS.num_rows, MAX(SERIES.higher_games + SERIES.lower_games) AS started, MIN(SERIES.complete) AS complete
    FROM tmp_series_rounds_stage0 AS ROUNDS
    JOIN SPORTS_MLB_PLAYOFF_SERIES AS SERIES
      ON (SERIES.season = v_season
      AND SUBSTRING(SERIES.round_code, 1, 1) = ROUNDS.round_num
      AND SERIES.the_date = ROUNDS.last_date)
    GROUP BY ROUNDS.round_num;

  # Get the current round info out
  SELECT round_num, last_date, num_rows, started, complete
    INTO v_round_num, v_series_date, v_round_rows, v_round_started, v_round_complete
  FROM tmp_series_rounds
  ORDER BY last_date DESC
  LIMIT 1;

  # If no matches (start of playoffs...), base from end of season
  IF v_done OR (v_round_num = v_round_first AND v_round_started = 0) THEN
    SELECT DATE_ADD(MAX(game_date), INTERVAL 1 DAY)
             INTO v_series_date
    FROM SPORTS_MLB_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular';

    # Ensure non-null values
    SET v_round_num := v_round_first;
    SET v_round_complete := 0;
    # Re-set our CONTINUE HANDLER checker
    SET v_done := 0;

  # We'll only process after completed round or if the current round is the wildcard game (not series) and it is in progress
  ELSEIF v_round_complete = 0 AND (v_season = 2020 OR v_season >= 2022 OR v_round_num > v_round_first OR (v_round_num = 1 AND v_round_started = 0)) THEN
    LEAVE proc;

  # Ensure the round and series date are correct
  ELSE
    # If the last round was completed, move on to the next round
    IF v_round_complete = 1 THEN
      SET v_round_num := (v_round_num + 1);
    END IF;

    # Catch end of playoff scenario
    IF v_round_num > 4 THEN
      LEAVE proc;
    END IF;

    # During regular season, v_series_date is constant - day after the regular season
    # Between rounds, the initial series date is the day after the last game of the previous series
    IF v_round_num > v_round_first OR v_round_started = 1 THEN
      SET v_series_date := DATE_ADD(v_series_date, INTERVAL 1 DAY);
    END IF;
  END IF;

  # Prep
  CALL mlb_playoff_matchups_setup(v_season, v_round_num, v_round_first, v_round_num > 1 OR v_round_started = 0);

  # Conference-based matchups
  IF v_round_num < 4 THEN

    # Loop through the conferences
    SET v_done := 0; # Reset the continue handler tracker
    OPEN cur_conf;
    loop_conf: LOOP

      FETCH cur_conf INTO v_conf_id;
      IF v_done = 1 THEN LEAVE loop_conf; END IF;

      # Wildcard (2012 onwards)
      IF v_round_num = 1 THEN
        IF v_season >= 2022 THEN
          # From 2022: Wildcard Series (with 6 teams)
          CALL mlb_playoff_matchups_wildcard_series_6team(v_season, v_series_date, v_conf_id, v_counter);
        ELSEIF v_season = 2020 THEN
          # 2020: Wildcard Series (with 8 teams)
          CALL mlb_playoff_matchups_wildcard_series_8team(v_season, v_series_date, v_conf_id, v_counter);
        ELSE
          # Wildcard Game
          CALL mlb_playoff_matchups_wildcard_game(v_season, v_series_date, v_conf_id, v_counter);
        END IF;

      # LDS
      ELSEIF v_round_num = 2 THEN
        # From 2022: Winners of wildcard series (with 6 teams)
        IF v_season >= 2022 THEN
          CALL mlb_playoff_matchups_lds_wc_series_6team(v_season, v_series_date, v_conf_id, v_counter);
        # 2020: Winners of wildcard series (with 8 teams)
        ELSEIF v_season = 2020 THEN
          CALL mlb_playoff_matchups_lds_wc_series_8team(v_season, v_series_date, v_conf_id, v_counter);
        # From 2012: Winner of wildcard game
        ELSEIF v_season >= 2012 THEN
          CALL mlb_playoff_matchups_lds_two_wc(v_season, v_series_date, v_conf_id, v_counter);
        # Until 2011: Includes wildcard team
        ELSE
          CALL mlb_playoff_matchups_lds_one_wc(v_season, v_series_date, v_conf_id, v_counter);
        END IF;

      # LCS
      ELSEIF v_round_num = 3 THEN
        CALL mlb_playoff_matchups_lcs(v_season, v_series_date, v_conf_id, v_counter);

      END IF;

    END LOOP loop_conf;
    CLOSE cur_conf;

  # World Series is a fixed method
  ELSE
    CALL mlb_playoff_matchups_ws(v_season, v_series_date, v_counter);
  END IF;

  # Final ordering
  ALTER TABLE SPORTS_MLB_PLAYOFF_SERIES ORDER BY season, the_date, round_code;

END $$

DELIMITER ;

##
## Setup
##
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_setup`(
  v_season SMALLINT UNSIGNED,
  v_round_num TINYINT UNSIGNED,
  v_round_first TINYINT UNSIGNED,
  v_prune TINYINT UNSIGNED
)
    COMMENT 'Prepare for our MLB playoff matchup calcs'
BEGIN

  DECLARE v_standing_date DATE;

  # Clear previous run
  IF v_prune THEN
    DELETE FROM SPORTS_MLB_PLAYOFF_SERIES WHERE season = v_season AND SUBSTRING(round_code, 1, 1) = v_round_num;
    IF v_round_num = 1 THEN
      # Wildcard matchup also generates an LDS matchup, so clear that too
      DELETE FROM SPORTS_MLB_PLAYOFF_SERIES WHERE season = v_season AND SUBSTRING(round_code, 1, 1) = 2;
    END IF;
  END IF;

  #
  # Build our temp table of seedings
  #
  SELECT DATE(MAX(the_date)) INTO v_standing_date FROM SPORTS_MLB_STANDINGS WHERE season = v_season;
  DROP TEMPORARY TABLE IF EXISTS tmp_seeds;
  CREATE TEMPORARY TABLE tmp_seeds (
    team_id VARCHAR(3),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_div TINYINT UNSIGNED,
    PRIMARY KEY (conf_id, pos_conf)
  ) ENGINE = MEMORY
    SELECT STANDINGS.team_id,
           DIVN.parent_id AS conf_id, DIVN.grouping_id AS div_id,
           STANDINGS.pos_league, SEEDS.seed AS pos_conf, STANDINGS.pos_div
    FROM SPORTS_MLB_TEAMS_GROUPINGS AS TEAM_DIVN
    JOIN SPORTS_MLB_GROUPINGS AS DIVN
      ON (DIVN.grouping_id = TEAM_DIVN.grouping_id)
    JOIN SPORTS_MLB_STANDINGS AS STANDINGS
      ON (STANDINGS.season = v_season
      AND STANDINGS.the_date = v_standing_date
      AND STANDINGS.team_id = TEAM_DIVN.team_id)
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS SEEDS
      ON (SEEDS.season = STANDINGS.season
      AND SEEDS.conf_id = DIVN.parent_id
      AND SEEDS.team_id = STANDINGS.team_id)
    WHERE v_season BETWEEN TEAM_DIVN.season_from AND IFNULL(TEAM_DIVN.season_to, 2099);

  # Adjust to only include teams to have progressed
  IF IFNULL(v_round_num, v_round_first) > v_round_first THEN
    # Who hasn't progressed this far?
    DROP TEMPORARY TABLE IF EXISTS tmp_playoff_losers;
    CREATE TEMPORARY TABLE tmp_playoff_losers (
      conf_id TINYINT UNSIGNED,
      pos_conf TINYINT UNSIGNED,
      PRIMARY KEY (conf_id, pos_conf)
    ) ENGINE = MEMORY
      SELECT IF(higher_games < lower_games, higher_conf_id, lower_conf_id) AS conf_id,
             IF(higher_games < lower_games, higher_seed, lower_seed) AS pos_conf
      FROM SPORTS_MLB_PLAYOFF_SERIES
      WHERE season = v_season
      AND   SUBSTRING(round_code, 1, 1) < v_round_num
      AND   complete = 1
      GROUP BY conf_id, pos_conf;

    # Remove from tmp_seeds
    DELETE tmp_seeds.*
    FROM tmp_playoff_losers
    JOIN tmp_seeds
      ON (tmp_seeds.conf_id = tmp_playoff_losers.conf_id
      AND tmp_seeds.pos_conf = tmp_playoff_losers.pos_conf);
  END IF;

END $$

DELIMITER ;

#
# Wildcard Game (includes subsequent LDS...)
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_wildcard_game`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_wildcard_game`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff wildcard game matchup calcs'
BEGIN

  DECLARE v_higher_games TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_lower_games TINYINT UNSIGNED DEFAULT 0;

  # In-series info?
  # Higher Seed
  SELECT IFNULL(SERIES.higher_games, 0) INTO v_higher_games
  FROM tmp_seeds AS SEED
  LEFT JOIN SPORTS_MLB_PLAYOFF_SERIES AS SERIES
    ON (SERIES.season = v_season
    AND SERIES.round_code = CAST(10 + v_counter + 1 AS CHAR(2)))
  WHERE SEED.conf_id = v_conf_id
  AND   SEED.pos_conf = 4
  ORDER BY SERIES.the_date DESC
  LIMIT 1;
  # Lower Seed
  SELECT IFNULL(SERIES.lower_games, 0) INTO v_lower_games
  FROM tmp_seeds AS SEED
  LEFT JOIN SPORTS_MLB_PLAYOFF_SERIES AS SERIES
    ON (SERIES.season = v_season
    AND SERIES.round_code = CAST(10 + v_counter + 1 AS CHAR(2)))
  WHERE SEED.conf_id = v_conf_id
  AND   SEED.pos_conf = 5
  ORDER BY SERIES.the_date DESC
  LIMIT 1;

  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, 4, v_conf_id, 5, v_higher_games, v_lower_games, v_higher_games = 1 OR v_lower_games = 1),
    (v_season, v_series_date, CAST(20 + (2 * v_counter) + 1 AS CHAR(2)), v_conf_id, 1, v_conf_id, IF(v_higher_games > v_lower_games, 4, IF(v_higher_games < v_lower_games, 5, 0)), 0, 0, 0),
    (v_season, v_series_date, CAST(20 + (2 * v_counter) + 2 AS CHAR(2)), v_conf_id, 2, v_conf_id, 3, 0, 0, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;

#
# Wildcard Series (6 teams, includes subsequent LDS for byes)
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_wildcard_series_6team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_wildcard_series_6team`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff 6 team wildcard series matchup calcs'
BEGIN

  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, 3, v_conf_id, 6, 0, 0, 0),
    (v_season, v_series_date, CAST(10 + v_counter + 2 AS CHAR(2)), v_conf_id, 4, v_conf_id, 5, 0, 0, 0),
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, 1, v_conf_id, 0, 0, 0, 0),
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, 2, v_conf_id, 0, 0, 0, 0);
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

#
# Wildcard Series (8 teams)
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_wildcard_series_8team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_wildcard_series_8team`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff 8 team wildcard series matchup calcs'
BEGIN

  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, 1, v_conf_id, 8, 0, 0, 0),
    (v_season, v_series_date, CAST(10 + v_counter + 2 AS CHAR(2)), v_conf_id, 2, v_conf_id, 7, 0, 0, 0),
    (v_season, v_series_date, CAST(10 + v_counter + 3 AS CHAR(2)), v_conf_id, 3, v_conf_id, 6, 0, 0, 0),
    (v_season, v_series_date, CAST(10 + v_counter + 4 AS CHAR(2)), v_conf_id, 4, v_conf_id, 5, 0, 0, 0);
  SET v_counter = v_counter + 4;

END $$

DELIMITER ;

#
# LDS - Winner from Wildcard Game
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_lds_two_wc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_lds_two_wc`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff LDS (two wc) matchup calcs'
BEGIN

  DECLARE v_seed_wc TINYINT UNSIGNED;
  SELECT pos_conf INTO v_seed_wc FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, 1, v_conf_id, v_seed_wc, 0, 0, 0),
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, 2, v_conf_id, 3, 0, 0, 0);
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

#
# LDS - Single wildcard team
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_lds_one_wc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_lds_one_wc`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff LDS (one wc) matchup calcs'
BEGIN

  DECLARE v_wc_div TINYINT UNSIGNED;
  DECLARE v_wc_opp TINYINT UNSIGNED;
  DECLARE v_wc_opp_div TINYINT UNSIGNED;
  DECLARE v_other_a TINYINT UNSIGNED;
  DECLARE v_other_b TINYINT UNSIGNED;

  # Matchup had div considerations: wildcard team played the div winner outside its div that had the better record
  SELECT div_id INTO v_wc_div FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf = 4;
  SELECT pos_conf, div_id INTO v_wc_opp, v_wc_opp_div FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id <> v_wc_div ORDER BY pos_conf ASC LIMIT 1;
  # Our other matchups is between the two other division winners
  SELECT pos_conf INTO v_other_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id <> v_wc_opp_div AND pos_div = 1 ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_other_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id <> v_wc_opp_div AND pos_div = 1 ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, v_wc_opp, v_conf_id, 4, 0, 0, 0),
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, v_other_a, v_conf_id, v_other_b, 0, 0, 0);
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

#
# LDS - Winner from Wildcard Series (which had 6 teams)
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_lds_wc_series_6team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_lds_wc_series_6team`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff LDS (6 team wildcard series) matchup calcs'
BEGIN

  DECLARE v_seed TINYINT UNSIGNED;

  # 1 vs Winner 4v5
  SELECT pos_conf INTO v_seed FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf IN (4, 5) ORDER BY pos_conf ASC LIMIT 1;
  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, 1, v_conf_id, v_seed, 0, 0, 0);

  # 2 vs Winner 3v6
  SELECT pos_conf INTO v_seed FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf IN (3, 6) ORDER BY pos_conf ASC LIMIT 1;
  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, 2, v_conf_id, v_seed, 0, 0, 0);

  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

#
# LDS - Winner from Wildcard Series (which had 8 teams)
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_lds_wc_series_8team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_lds_wc_series_8team`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff LDS (8 team wildcard series) matchup calcs'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  # Winner 1v8 vs 4v5
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf IN (1, 4, 5, 8) ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf IN (1, 4, 5, 8) ORDER BY pos_conf DESC LIMIT 1;
  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Winner 2v7 vs 3v6
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf IN (2, 3, 6, 7) ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf IN (2, 3, 6, 7) ORDER BY pos_conf DESC LIMIT 1;
  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

#
# League Championship Series
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_lcs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_lcs`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff LCS matchup calcs'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(30 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;

#
# World Series
#
DROP PROCEDURE IF EXISTS `mlb_playoff_matchups_ws`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_matchups_ws`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'MLB playoff World Series matchup calcs'
BEGIN

  DECLARE v_conf_a TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_conf_b TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT conf_id, pos_conf INTO v_conf_a, v_seed_a FROM tmp_seeds ORDER BY pos_league  ASC LIMIT 1;
  SELECT conf_id, pos_conf INTO v_conf_b, v_seed_b FROM tmp_seeds ORDER BY pos_league DESC LIMIT 1;

  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(40 + v_counter + 1 AS CHAR(2)), v_conf_a, v_seed_a, v_conf_b, v_seed_b, 0, 0, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;
