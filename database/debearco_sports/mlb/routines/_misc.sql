##
## Misc MLB calcs
##

#
# Identify series date range
#
DROP PROCEDURE IF EXISTS `mlb_sequential_date_range`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_sequential_date_range` (
  v_season SMALLINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Create temp table of date ranges for MLB calcs'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_date_range;
  CREATE TEMPORARY TABLE tmp_date_range (
    season SMALLINT UNSIGNED,
    the_date DATE,
    PRIMARY KEY (season, the_date)
  ) ENGINE = MEMORY;

  WHILE v_start_date <= v_end_date DO
    INSERT INTO tmp_date_range (season, the_date) VALUES (v_season, v_start_date);
    SELECT DATE_ADD(v_start_date, INTERVAL 1 DAY) INTO v_start_date;
  END WHILE;

END $$

DELIMITER ;

