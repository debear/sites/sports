##
## Player totals sorted
##
DROP PROCEDURE IF EXISTS `mlb_totals_players_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for MLB players'
BEGIN

  # Prep
  CALL mlb_totals_players_sort__tmp_qual_prep(v_season, v_season_type);

  # Broken down by individual routines
  CALL mlb_totals_players_sort_misc(v_season, v_season_type);
  CALL mlb_totals_players_sort_batting(v_season, v_season_type);
  CALL mlb_totals_players_sort_pitching(v_season, v_season_type);
  CALL mlb_totals_players_sort_fielding(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort_misc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort_misc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort misc season totals for MLB players'
BEGIN

  DELETE FROM SPORTS_MLB_PLAYERS_SEASON_MISC_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL mlb_totals_players_sort__tmp(v_season, v_season_type, 'MISC',
                                    'gp TINYINT UNSIGNED,
                                     gs TINYINT UNSIGNED,
                                     gp_C TINYINT UNSIGNED,
                                     gs_C TINYINT UNSIGNED,
                                     ga_C TINYINT UNSIGNED,
                                     gp_1B TINYINT UNSIGNED,
                                     gs_1B TINYINT UNSIGNED,
                                     ga_1B TINYINT UNSIGNED,
                                     gp_2B TINYINT UNSIGNED,
                                     gs_2B TINYINT UNSIGNED,
                                     ga_2B TINYINT UNSIGNED,
                                     gp_SS TINYINT UNSIGNED,
                                     gs_SS TINYINT UNSIGNED,
                                     ga_SS TINYINT UNSIGNED,
                                     gp_3B TINYINT UNSIGNED,
                                     gs_3B TINYINT UNSIGNED,
                                     ga_3B TINYINT UNSIGNED,
                                     gp_LF TINYINT UNSIGNED,
                                     gs_LF TINYINT UNSIGNED,
                                     ga_LF TINYINT UNSIGNED,
                                     gp_CF TINYINT UNSIGNED,
                                     gs_CF TINYINT UNSIGNED,
                                     ga_CF TINYINT UNSIGNED,
                                     gp_RF TINYINT UNSIGNED,
                                     gs_RF TINYINT UNSIGNED,
                                     ga_RF TINYINT UNSIGNED,
                                     gp_DH TINYINT UNSIGNED,
                                     gs_DH TINYINT UNSIGNED,
                                     ga_DH TINYINT UNSIGNED,
                                     ga_PH TINYINT UNSIGNED,
                                     ga_PR TINYINT UNSIGNED,
                                     gp_P TINYINT UNSIGNED,
                                     gs_SP TINYINT UNSIGNED,
                                     ga_RP TINYINT UNSIGNED,
                                     winprob DECIMAL(4,1) SIGNED',
                                    'STAT.gp,
                                     STAT.gs,
                                     STAT.gp_C,
                                     STAT.gs_C,
                                     STAT.ga_C,
                                     STAT.gp_1B,
                                     STAT.gs_1B,
                                     STAT.ga_1B,
                                     STAT.gp_2B,
                                     STAT.gs_2B,
                                     STAT.ga_2B,
                                     STAT.gp_SS,
                                     STAT.gs_SS,
                                     STAT.ga_SS,
                                     STAT.gp_3B,
                                     STAT.gs_3B,
                                     STAT.ga_3B,
                                     STAT.gp_LF,
                                     STAT.gs_LF,
                                     STAT.ga_LF,
                                     STAT.gp_CF,
                                     STAT.gs_CF,
                                     STAT.ga_CF,
                                     STAT.gp_RF,
                                     STAT.gs_RF,
                                     STAT.ga_RF,
                                     STAT.gp_DH,
                                     STAT.gs_DH,
                                     STAT.ga_DH,
                                     STAT.ga_PH,
                                     STAT.ga_PR,
                                     STAT.gp_P,
                                     STAT.gs_SP,
                                     STAT.ga_RP,
                                     STAT.winprob',
                                    'STAT.gp > 0');

  # Calcs
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_C', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_C', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_C', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_1B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_1B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_1B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_2B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_2B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_2B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_SS', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_SS', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_SS', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_3B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_3B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_3B', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_LF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_LF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_LF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_CF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_CF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_CF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_RF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_RF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_RF', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_DH', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_DH', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_DH', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_PH', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_PR', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gp_P', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'gs_SP', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'ga_RP', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'MISC', 'winprob', 'DECIMAL(4,1) SIGNED', '>', NULL);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort_batting`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort_batting`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort batting season totals for MLB players'
BEGIN

  DELETE FROM SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL mlb_totals_players_sort__tmp(v_season, v_season_type, 'BATTING',
                                    'ab SMALLINT UNSIGNED,
                                     pa SMALLINT UNSIGNED,
                                     h TINYINT UNSIGNED,
                                     1b TINYINT UNSIGNED,
                                     2b TINYINT UNSIGNED,
                                     3b TINYINT UNSIGNED,
                                     xbh TINYINT UNSIGNED,
                                     bb TINYINT UNSIGNED,
                                     ibb TINYINT UNSIGNED,
                                     r TINYINT UNSIGNED,
                                     hr TINYINT UNSIGNED,
                                     rbi TINYINT UNSIGNED,
                                     sb TINYINT UNSIGNED,
                                     cs TINYINT UNSIGNED,
                                     po TINYINT UNSIGNED,
                                     avg DECIMAL(5,4) SIGNED,
                                     obp DECIMAL(5,4) SIGNED,
                                     tb SMALLINT UNSIGNED,
                                     slg DECIMAL(5,4) SIGNED,
                                     ops DECIMAL(5,4) SIGNED,
                                     sac_fly TINYINT UNSIGNED,
                                     sac_hit TINYINT UNSIGNED,
                                     hbp TINYINT UNSIGNED,
                                     k TINYINT UNSIGNED,
                                     go TINYINT UNSIGNED,
                                     fo TINYINT UNSIGNED,
                                     gidp TINYINT UNSIGNED,
                                     lob SMALLINT UNSIGNED,
                                     winprob DECIMAL(4,1) SIGNED,
                                     qual TINYINT UNSIGNED',
                                    'STAT.ab,
                                     STAT.pa,
                                     STAT.h,
                                     STAT.1b,
                                     STAT.2b,
                                     STAT.3b,
                                     STAT.xbh,
                                     STAT.bb,
                                     STAT.ibb,
                                     STAT.r,
                                     STAT.hr,
                                     STAT.rbi,
                                     STAT.sb,
                                     STAT.cs,
                                     STAT.po,
                                     STAT.avg,
                                     STAT.obp,
                                     STAT.tb,
                                     STAT.slg,
                                     STAT.ops,
                                     STAT.sac_fly,
                                     STAT.sac_hit,
                                     STAT.hbp,
                                     STAT.k,
                                     STAT.go,
                                     STAT.fo,
                                     STAT.gidp,
                                     STAT.lob,
                                     STAT.winprob,
                                     0 AS qual',
                                    'STAT.pa > 0');

  # Qualification Criteria
  CALL mlb_totals_players_sort__tmp_qual_batting(v_season, v_season_type);

  # Base row
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED (season, season_type, player_id, team_id, is_totals, qual)
    SELECT season, season_type, player_id, team_id, is_totals, qual
    FROM tmp_SPORTS_MLB_PLAYERS_SEASON_BATTING_QUAL;

  # Calcs
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'ab', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'pa', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'h', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', '1b', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', '2b', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', '3b', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'xbh', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'bb', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'ibb', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'r', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'hr', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'rbi', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'sb', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'cs', 'TINYINT UNSIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'cs_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'po', 'TINYINT UNSIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'po_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'avg', 'DECIMAL(5,4) SIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'avg_all', 'DECIMAL(5,4) SIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'obp', 'DECIMAL(5,4) SIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'obp_all', 'DECIMAL(5,4) SIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'tb', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'slg', 'DECIMAL(5,4) SIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'slg_all', 'DECIMAL(5,4) SIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'ops', 'DECIMAL(5,4) SIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'ops_all', 'DECIMAL(5,4) SIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'sac_fly', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'sac_hit', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'hbp', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'k', 'TINYINT UNSIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'k_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'go', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'fo', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'gidp', 'TINYINT UNSIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'gidp_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'lob', 'SMALLINT UNSIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'lob_all', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'winprob', 'DECIMAL(4,1) SIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'BATTING', 'winprob_all', 'DECIMAL(4,1) SIGNED', '>', NULL);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort_pitching`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort_pitching`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort pitching season totals for MLB players'
BEGIN

  DELETE FROM SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL mlb_totals_players_sort__tmp(v_season, v_season_type, 'PITCHING',
                                    'ip DECIMAL(4,1) UNSIGNED,
                                     `out` SMALLINT UNSIGNED,
                                     bf SMALLINT UNSIGNED,
                                     pt SMALLINT UNSIGNED,
                                     b SMALLINT UNSIGNED,
                                     s SMALLINT UNSIGNED,
                                     k SMALLINT UNSIGNED,
                                     h SMALLINT UNSIGNED,
                                     hr TINYINT UNSIGNED,
                                     bb TINYINT UNSIGNED,
                                     ibb TINYINT UNSIGNED,
                                     wp TINYINT UNSIGNED,
                                     go SMALLINT UNSIGNED,
                                     fo SMALLINT UNSIGNED,
                                     go_fo DECIMAL(6,4) UNSIGNED,
                                     r TINYINT UNSIGNED,
                                     ra DECIMAL(6,3) UNSIGNED,
                                     er TINYINT UNSIGNED,
                                     era DECIMAL(6,3) UNSIGNED,
                                     ur TINYINT UNSIGNED,
                                     ura DECIMAL(6,3) UNSIGNED,
                                     ir TINYINT UNSIGNED,
                                     ira TINYINT UNSIGNED,
                                     whip DECIMAL(6,3) UNSIGNED,
                                     bk TINYINT UNSIGNED,
                                     sb TINYINT UNSIGNED,
                                     cs TINYINT UNSIGNED,
                                     po TINYINT UNSIGNED,
                                     w TINYINT UNSIGNED,
                                     l TINYINT UNSIGNED,
                                     sv TINYINT UNSIGNED,
                                     hld TINYINT UNSIGNED,
                                     bs TINYINT UNSIGNED,
                                     svo TINYINT UNSIGNED,
                                     cg TINYINT UNSIGNED,
                                     sho TINYINT UNSIGNED,
                                     qs TINYINT UNSIGNED,
                                     winprob DECIMAL(4,1) SIGNED,
                                     game_score DECIMAL(5,2) SIGNED,
                                     qual_ratio TINYINT UNSIGNED,
                                     qual_rp TINYINT UNSIGNED',
                                    'STAT.ip,
                                     STAT.`out`,
                                     STAT.bf,
                                     STAT.pt,
                                     STAT.b,
                                     STAT.s,
                                     STAT.k,
                                     STAT.h,
                                     STAT.hr,
                                     STAT.bb,
                                     STAT.ibb,
                                     STAT.wp,
                                     STAT.go,
                                     STAT.fo,
                                     STAT.go_fo,
                                     STAT.r,
                                     STAT.ra,
                                     STAT.er,
                                     STAT.era,
                                     STAT.ur,
                                     STAT.ura,
                                     STAT.ir,
                                     STAT.ira,
                                     STAT.whip,
                                     STAT.bk,
                                     STAT.sb,
                                     STAT.cs,
                                     STAT.po,
                                     STAT.w,
                                     STAT.l,
                                     STAT.sv,
                                     STAT.hld,
                                     STAT.bs,
                                     STAT.svo,
                                     STAT.cg,
                                     STAT.sho,
                                     STAT.qs,
                                     STAT.winprob,
                                     STAT.game_score,
                                     0 AS qual_ratio,
                                     0 AS qual_rp',
                                    'STAT.bf > 0');

  # Qualification Criteria
  CALL mlb_totals_players_sort__tmp_qual_pitching(v_season, v_season_type);

  # Base row
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED (season, season_type, player_id, team_id, is_totals, qual_ratio, qual_rp)
    SELECT season, season_type, player_id, team_id, is_totals, qual_ratio, qual_rp
    FROM tmp_SPORTS_MLB_PLAYERS_SEASON_PITCHING_QUAL;

  # Calcs
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ip', 'DECIMAL(4,1) UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'out', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'bf', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'pt', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'b', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 's', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'k', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'h', 'SMALLINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'h_all', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'hr', 'TINYINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'hr_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'bb', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ibb', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'wp', 'TINYINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'wp_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'go', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'fo', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'go_fo', 'DECIMAL(6,4) UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'r', 'TINYINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'r_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ra', 'DECIMAL(6,3) UNSIGNED', '<', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ra_all', 'DECIMAL(6,3) UNSIGNED', '<', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'er', 'TINYINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'er_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'era', 'DECIMAL(6,3) UNSIGNED', '<', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'era_all', 'DECIMAL(6,3) UNSIGNED', '<', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ur', 'TINYINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ur_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ura', 'DECIMAL(6,3) UNSIGNED', '<', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ura_all', 'DECIMAL(6,3) UNSIGNED', '<', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ir', 'TINYINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ir_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ira', 'TINYINT UNSIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'ira_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'whip', 'DECIMAL(6,3) UNSIGNED', '<', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'whip_all', 'DECIMAL(6,3) UNSIGNED', '<', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'bk', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'sb', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'cs', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'po', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'w', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'l', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'sv', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'hld', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'bs', 'TINYINT UNSIGNED', '>', 'qual_rp');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'bs_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'svo', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'cg', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'sho', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'qs', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'winprob', 'DECIMAL(4,1) SIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'winprob_all', 'DECIMAL(4,1) SIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'game_score', 'DECIMAL(5,2) SIGNED', '>', 'qual_ratio');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'PITCHING', 'game_score_all', 'DECIMAL(5,2) SIGNED', '>', NULL);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort_fielding`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort_fielding`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort fielding season totals for MLB players'
BEGIN

  DELETE FROM SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL mlb_totals_players_sort__tmp(v_season, v_season_type, 'FIELDING',
                                    'tc SMALLINT UNSIGNED,
                                     po SMALLINT UNSIGNED,
                                     a SMALLINT UNSIGNED,
                                     e TINYINT UNSIGNED,
                                     pct DECIMAL(5,4) SIGNED,
                                     dp TINYINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'STAT.tc,
                                     STAT.po,
                                     STAT.a,
                                     STAT.e,
                                     STAT.pct,
                                     STAT.dp,
                                     0 AS qual',
                                    'STAT.tc > 0');

  # Qualification Criteria
  CALL mlb_totals_players_sort__tmp_qual_fielding(v_season, v_season_type);

  # Base row
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED (season, season_type, player_id, team_id, is_totals, qual)
    SELECT season, season_type, player_id, team_id, is_totals, qual
    FROM tmp_SPORTS_MLB_PLAYERS_SEASON_FIELDING_QUAL;

  # Calcs
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'tc', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'po', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'a', 'SMALLINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'e', 'TINYINT UNSIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'e_all', 'TINYINT UNSIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'pct', 'DECIMAL(5,4) SIGNED', '>', 'qual');
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'pct_all', 'DECIMAL(5,4) SIGNED', '>', NULL);
  CALL mlb_totals_players_sort__calc(v_season, v_season_type, 'FIELDING', 'dp', 'TINYINT UNSIGNED', '>', NULL);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_cols VARCHAR(4096),
  v_calcs VARCHAR(4096),
  v_where VARCHAR(100)
)
    COMMENT 'Create temporary table for MLB player season stat sort'
BEGIN

  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table));
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      player_id SMALLINT UNSIGNED NOT NULL,
      team_id VARCHAR(3),
      is_totals TINYINT UNSIGNED,
      ', v_cols, ',
      PRIMARY KEY (season, season_type, player_id, team_id)
    ) ENGINE = MEMORY
      SELECT STAT.season, STAT.season_type, STAT.player_id, STAT.team_id, STAT.is_totals, ', v_calcs, '
      FROM SPORTS_MLB_PLAYERS_SEASON_', v_table, ' AS STAT
      LEFT JOIN SPORTS_MLB_PLAYERS_SEASON_MISC AS MISC
        ON (MISC.season = STAT.season
        AND MISC.season_type = STAT.season_type
        AND MISC.player_id = STAT.player_id
        AND MISC.team_id = STAT.team_id)
      WHERE STAT.season = ', v_season, '
      AND   STAT.season_type = "', v_season_type, '"
      ', IF(v_where IS NOT NULL, CONCAT('AND ', v_where), ''), '
      GROUP BY STAT.season, STAT.season_type, STAT.player_id, STAT.team_id;'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_players_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col_sort VARCHAR(25),
  v_col_def VARCHAR(50),
  v_sort_dir VARCHAR(5),
  v_qual  VARCHAR(10)
)
    COMMENT 'Sort season totals for MLB players'
BEGIN

  # Data column (which may not be v_col_sort)
  DECLARE v_col_data VARCHAR(25);
  IF SUBSTRING(v_col_sort, -4) = '_all' THEN
    SET v_col_data = LEFT(v_col_sort, LENGTH(v_col_sort) - 4);
  ELSE
    SET v_col_data = v_col_sort;
  END IF;

  # Determine unique values for this column for sorting
  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT;'));
  CALL _exec(CONCAT(
    'CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT (
       `', v_col_data, '` ', v_col_def, ',
       ', IF(v_qual IS NOT NULL, CONCAT('`', v_qual, '` TINYINT UNSIGNED,'), ''), '
       num_players SMALLINT UNSIGNED,
       value_order SMALLINT UNSIGNED,
       PRIMARY KEY (`', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ')
     ) ENGINE = MEMORY;'));
  # (Note: This query explicity needs to be split out like this, and not CREATE TABLE ... SELECT due to issues with the `ip` column calcs...)
  CALL _exec(CONCAT('
    INSERT INTO tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT
       SELECT IFNULL(`', v_col_data, '`, 0) AS `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', COUNT(player_id) AS num_players, 9999 AS value_order
       FROM tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '
       GROUP BY IFNULL(`', v_col_data, '`, 0)', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ';'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT_cpA'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT_cpB'));

  # Sort these values
  CALL _exec(CONCAT(
    'INSERT INTO tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT (`', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', value_order)
       SELECT A.', v_col_data, ', ', IF(v_qual IS NOT NULL, CONCAT('A.', v_qual, ', '), ''), 'IFNULL(SUM(B.num_players), 0) + 1 AS value_order
       FROM tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT_cpA AS A
       LEFT JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT_cpB AS B
         ON (', IF(v_qual IS NOT NULL,
                   CONCAT('B.', v_qual, ' > A.', v_qual, ' OR (B.', v_qual, ' = A.', v_qual, ' AND '), ''),
             'B.', v_col_data, ' ', v_sort_dir, ' A.', v_col_data,
             IF (v_qual IS NOT NULL, ')', ''), ')
       GROUP BY A.', v_col_data, IF(v_qual IS NOT NULL, CONCAT(', A.', v_qual), ''), '
     ON DUPLICATE KEY UPDATE value_order = VALUES(value_order);'));

  # Update sorting for the players
  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_MLB_PLAYERS_SEASON_', v_table, '_SORTED (season, season_type, player_id, team_id, is_totals, `', v_col_sort, '`)
      SELECT PLAYER.season, PLAYER.season_type, PLAYER.player_id, PLAYER.team_id, PLAYER.is_totals,
             STAT.value_order AS `', v_col_sort, '`
      FROM tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, ' AS PLAYER
      JOIN tmp_SPORTS_MLB_PLAYERS_SEASON_', v_table, '_STAT AS STAT
        ON (STAT.', v_col_data, ' = PLAYER.', v_col_data,
            IF(v_qual IS NOT NULL,
               CONCAT(' AND STAT.', v_qual, '= PLAYER.', v_qual), ''), ')
      WHERE PLAYER.season = ', v_season, '
      AND   PLAYER.season_type = "', v_season_type, '"
    ON DUPLICATE KEY UPDATE `', v_col_sort, '` = VALUES(`', v_col_sort, '`);'));

END $$

DELIMITER ;
