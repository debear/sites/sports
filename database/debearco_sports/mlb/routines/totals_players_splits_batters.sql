#
# Batter-based split and situational stats
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batters`()
    COMMENT 'Determine split/situational stats for batters over a season'
BEGIN

  # Prepare the temporary table
  CALL mlb_totals_players_splits_batters_setup();

  # v LHP / RHP, v SP / RP
  CALL mlb_totals_players_splits_batters_calc('pitcher-info', 'IF(AB.pitcher_hand = "L", "v LHP", "v RHP")', 'AB.pitcher_hand', NULL);
  CALL mlb_totals_players_splits_batters_calc('pitcher-info', 'IF(AB.sp_rp = "SP", "v SP", "v RP")',         'AB.sp_rp', NULL);

  # As LHB / LHB
  CALL mlb_totals_players_splits_batters_calc('batter-info', 'IF(AB.batter_hand = "L", "As LHB", "As RHB")', 'AB.batter_hand', NULL);

  # Home / Road
  CALL mlb_totals_players_splits_batters_calc('home-road', 'IF(BASE.is_home, "Home", "Road")', 'BASE.is_home', NULL);

  # By Month
  CALL mlb_totals_players_splits_batters_calc('month', 'BASE.month', 'BASE.month', NULL);

  # Pre-/Post-ASB (regular season only)
  CALL mlb_totals_players_splits_batters_calc('asb', 'IF(BASE.pre_asb, "Pre All-Star Break", "Post All-Star Break")', 'BASE.pre_asb', 'BASE.game_type = "regular"');

  # By Opp
  CALL mlb_totals_players_splits_batters_calc('opponent', 'BASE.opp_team_id', 'BASE.opp_team_id', NULL);

  # By Stadium
  CALL mlb_totals_players_splits_batters_calc('stadium', 'BASE.stadium', 'BASE.stadium', NULL);

  # Bases Empty, Runners On, RISP, RISP w/2 outs, Bases Loaded, Man on 3rd <2 outs
  CALL mlb_totals_players_splits_batters_calc('with-baserunners', '"Bases Empty"', NULL, 'AB.on_base = 0');
  CALL mlb_totals_players_splits_batters_calc('with-baserunners', '"Runners On"', NULL, 'AB.on_base > 0');
  CALL mlb_totals_players_splits_batters_calc('with-baserunners', '"RISP"', NULL, 'AB.risp = 1');
  CALL mlb_totals_players_splits_batters_calc('with-baserunners', '"RISP, 2 Outs"', NULL, 'AB.risp = 1 AND AB.out = 2');
  CALL mlb_totals_players_splits_batters_calc('with-baserunners', '"Bases Loaded"', NULL, 'AB.on_base = 7');
  CALL mlb_totals_players_splits_batters_calc('with-baserunners', '"Man on 3rd, <2 Outs"', NULL, 'FIND_IN_SET("3rd", AB.on_base) > 0 AND AB.out < 2');

  # By Counts (on & after)
  CALL mlb_totals_players_splits_batters_calc('counts-on', 'CONCAT("Count ", AB.count_final)', 'AB.count_final', NULL);
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 0-1"', NULL, 'FIND_IN_SET("0-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 0-2"', NULL, 'FIND_IN_SET("0-2", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 1-0"', NULL, 'FIND_IN_SET("1-0", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 1-1"', NULL, 'FIND_IN_SET("1-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 1-2"', NULL, 'FIND_IN_SET("1-2", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 2-0"', NULL, 'FIND_IN_SET("2-0", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 2-1"', NULL, 'FIND_IN_SET("2-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 2-2"', NULL, 'FIND_IN_SET("2-2", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 3-0"', NULL, 'FIND_IN_SET("3-0", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 3-1"', NULL, 'FIND_IN_SET("3-1", AB.counts) > 0');
  CALL mlb_totals_players_splits_batters_calc('counts-after', '"After 3-2"', NULL, 'FIND_IN_SET("3-2", AB.counts) > 0');

  # By Inning
  CALL mlb_totals_players_splits_batters_calc('inning', 'CONCAT(display_ordinal_number(AB.inning), " Inning")', 'AB.inning', NULL);

  # By Batting Order
  CALL mlb_totals_players_splits_batters_calc('batting-order', 'CONCAT("Batting ", display_ordinal_number(AB.batting_order))', 'AB.batting_order', NULL);

  # By Fielding Pos
  CALL mlb_totals_players_splits_batters_calc('fielding-pos', 'CONCAT("As ", AB.batter_pos)', 'AB.batter_pos', NULL);

  # Commit any changes
  CALL mlb_totals_players_splits_batters_commit();

  # Career versions (not linked to this season)
  CALL mlb_totals_players_splits_batters_career();

END $$

DELIMITER ;

# Prepare our work
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batters_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batters_setup`()
    COMMENT 'Batter split stat preparation'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_BATTING_SPLITS;
  CREATE TEMPORARY TABLE tmp_BATTING_SPLITS LIKE SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS;
  ALTER TABLE tmp_BATTING_SPLITS
    ENGINE = MEMORY,
    MODIFY COLUMN split_label VARCHAR(50);

  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_GAME_BATTING;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_GAME_BATTING LIKE SPORTS_MLB_PLAYERS_GAME_BATTING;
  INSERT INTO tmp_SPORTS_MLB_PLAYERS_GAME_BATTING
    SELECT STATS.*
    FROM tmp_splits_rosters AS BASE
    JOIN SPORTS_MLB_PLAYERS_GAME_BATTING AS STATS
      ON (STATS.season = BASE.season
      AND STATS.game_type = BASE.game_type
      AND STATS.game_id = BASE.game_id
      AND STATS.player_id = BASE.player_id);

END $$

DELIMITER ;

# Worker method
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batters_calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batters_calc`(
  v_split_type VARCHAR(50),
  v_split_label VARCHAR(100),
  v_group_by VARCHAR(255),
  v_where VARCHAR(255)
)
    COMMENT 'Batter split stat calculation'
BEGIN

  CALL _exec(CONCAT('
    INSERT INTO tmp_BATTING_SPLITS (season, season_type, player_id, split_type, split_label, gp, pa, ab, obp_evt, h, 1b, 2b, 3b, hr, rbi, bb, ibb, hbp, k, tb)
      SELECT AB.season, AB.game_type AS season_type, AB.batter AS player_id,
             "', v_split_type, '" AS split_type,
             ', v_split_label, ' AS split_label,
             COUNT(DISTINCT AB.game_id) AS gp,
             SUM(AB.is_pa) AS pa,
             SUM(AB.is_ab) AS ab,
             SUM(AB.is_obp_evt) AS obp_evt,
             SUM(AB.is_hit) AS h,
             SUM(AB.tb = 1) AS 1b,
             SUM(AB.tb = 2) AS 2b,
             SUM(AB.tb = 3) AS 3b,
             SUM(AB.tb = 4) AS hr,
             SUM(AB.rbi) AS rbi,
             SUM(AB.is_bb) AS bb,
             SUM(AB.is_ibb) AS ibb,
             SUM(AB.is_hbp) AS hbp,
             SUM(AB.is_k) AS k,
             SUM(AB.tb) AS tb
      FROM tmp_splits_rosters AS BASE
      JOIN tmp_SPORTS_MLB_GAME_ATBAT_FLAGS AS AB
        ON (AB.season = BASE.season
        AND AB.game_type = BASE.game_type
        AND AB.game_id = BASE.game_id
        AND AB.batter = BASE.player_id
        AND AB.is_pa = 1)
      ', IF(v_where IS NOT NULL, CONCAT('WHERE ', v_where), ''), '
      GROUP BY AB.season, AB.game_type, AB.batter', IF(v_group_by IS NOT NULL, CONCAT(', ', v_group_by), ''), '
    ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                            pa = VALUES(pa),
                            ab = VALUES(ab),
                            obp_evt = VALUES(obp_evt),
                            h = VALUES(h),
                            1b = VALUES(1b),
                            2b = VALUES(2b),
                            3b = VALUES(3b),
                            hr = VALUES(hr),
                            rbi = VALUES(rbi),
                            bb = VALUES(bb),
                            ibb = VALUES(ibb),
                            hbp = VALUES(hbp),
                            k = VALUES(k),
                            tb = VALUES(tb);'));

  # Split stats, which are more relevant game-wide stats than cumulation of ABs
  IF SUBSTRING(v_group_by, 1, 5) = 'BASE.' THEN
    CALL _exec(CONCAT('
      INSERT INTO tmp_BATTING_SPLITS (season, season_type, player_id, split_type, split_label, r, sb, cs, sac_fly, sac_hit, gidp, lob, winprob)
        SELECT BASE.season, BASE.game_type, BASE.player_id,
               "', v_split_type, '" AS split_type,
               ', v_split_label, ' AS split_label,
               SUM(STATS.r) AS r,
               SUM(STATS.sb) AS sb,
               SUM(STATS.cs) AS cs,
               SUM(STATS.sac_fly) AS sac_fly,
               SUM(STATS.sac_hit) AS sac_hit,
               SUM(STATS.gidp) AS gidp,
               SUM(STATS.lob) AS lob,
               SUM(STATS.winprob) AS winprob
        FROM tmp_splits_rosters AS BASE
        JOIN tmp_SPORTS_MLB_PLAYERS_GAME_BATTING AS STATS
          ON (STATS.season = BASE.season
          AND STATS.game_type = BASE.game_type
          AND STATS.game_id = BASE.game_id
          AND STATS.player_id = BASE.player_id)
        ', IF(v_where IS NOT NULL, CONCAT('WHERE ', v_where), ''), '
        GROUP BY BASE.season, BASE.game_type, BASE.player_id, ', v_group_by, '
      ON DUPLICATE KEY UPDATE r = VALUES(r),
                              sb = VALUES(sb),
                              cs = VALUES(cs),
                              sac_fly = VALUES(sac_fly),
                              sac_hit = VALUES(sac_hit),
                              gidp = VALUES(gidp),
                              lob = VALUES(lob),
                              winprob = VALUES(winprob);'));
  END IF;

END $$

DELIMITER ;

# Save changes back to the database
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batters_commit`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batters_commit`()
    COMMENT 'Batter split stat storing to main table'
BEGIN

  # Composite calcs
  UPDATE tmp_BATTING_SPLITS
  SET avg = IF(ab > 0, h / ab, NULL),
      obp = IF(obp_evt > 0, (h + bb + hbp) / obp_evt, NULL),
      slg = IF(ab > 0, tb / ab, NULL),
      ops = IF(obp_evt > 0, ((h + bb + hbp) / obp_evt) + IF(ab > 0, tb / ab, 0), NULL);

  # Convert the split_labels in to an ID
  INSERT IGNORE INTO SPORTS_MLB_PLAYERS_SPLIT_LABELS (split_id, split_type, split_label, created)
    SELECT DISTINCT NULL, split_type, split_label, NOW() as created
    FROM tmp_BATTING_SPLITS
    ORDER BY split_type, split_label;
  UPDATE tmp_BATTING_SPLITS AS SPLITS
  JOIN SPORTS_MLB_PLAYERS_SPLIT_LABELS AS LABELS
    ON (LABELS.split_type = SPLITS.split_type
    AND LABELS.split_label = SPLITS.split_label)
  SET SPLITS.split_label = LABELS.split_id;

  # Save to the main table
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS
    SELECT *
    FROM tmp_BATTING_SPLITS
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          pa = VALUES(pa),
                          ab = VALUES(ab),
                          obp_evt = VALUES(obp_evt),
                          h = VALUES(h),
                          1b = VALUES(1b),
                          2b = VALUES(2b),
                          3b = VALUES(3b),
                          hr = VALUES(hr),
                          r = VALUES(r),
                          rbi = VALUES(rbi),
                          bb = VALUES(bb),
                          ibb = VALUES(ibb),
                          hbp = VALUES(hbp),
                          k = VALUES(k),
                          sb = VALUES(sb),
                          cs = VALUES(cs),
                          sac_fly = VALUES(sac_fly),
                          sac_hit = VALUES(sac_hit),
                          tb = VALUES(tb),
                          gidp = VALUES(gidp),
                          lob = VALUES(lob),
                          avg = VALUES(avg),
                          obp = VALUES(obp),
                          slg = VALUES(slg),
                          ops = VALUES(ops),
                          winprob = VALUES(winprob);

END $$

DELIMITER ;

# Career stats
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_batters_career`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_batters_career`()
    COMMENT 'Batter split stat over a career'
BEGIN

  # Up memory limit to 256meg for the temporary table (as data is in FIXED format, which is wider, and oft exceeds the 16meg default)
  SET @@session.max_heap_table_size = 268435456; # 268,435,456 = 256 * 1024 * 1024
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

  # Copy appropriate stats into a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_splits_season;
  CREATE TEMPORARY TABLE tmp_splits_season LIKE SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS;
  ALTER TABLE tmp_splits_season ENGINE = MEMORY;
  INSERT INTO tmp_splits_season
    SELECT STATS.*
    FROM tmp_split_players
    JOIN SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS AS STATS
      ON (STATS.player_id = tmp_split_players.player_id);

  # Then aggregate
  INSERT INTO SPORTS_MLB_PLAYERS_CAREER_BATTING_SPLITS (player_id, season_type, split_type, split_label, gp, pa, ab, obp_evt, h, 1b, 2b, 3b, hr, r, rbi, bb, ibb, hbp, k, sb, cs, sac_fly, sac_hit, tb, gidp, lob, avg, obp, slg, ops, winprob)
    SELECT player_id, season_type, split_type, split_label,
           SUM(gp) AS gp,
           SUM(pa) AS pa,
           SUM(ab) AS ab,
           SUM(obp_evt) AS obp_evt,
           SUM(h) AS h,
           SUM(1b) AS 1b,
           SUM(2b) AS 2b,
           SUM(3b) AS 3b,
           SUM(hr) AS hr,
           SUM(r) AS r,
           SUM(rbi) AS rbi,
           SUM(bb) AS bb,
           SUM(ibb) AS ibb,
           SUM(hbp) AS hbp,
           SUM(k) AS k,
           SUM(sb) AS sb,
           SUM(cs) AS cs,
           SUM(sac_fly) AS sac_fly,
           SUM(sac_hit) AS sac_hit,
           SUM(tb) AS tb,
           SUM(gidp) AS gidp,
           SUM(lob) AS lob,
           IF(SUM(ab) > 0, SUM(h) / SUM(ab), NULL) AS avg,
           IF(SUM(obp_evt) > 0, SUM(h + bb + hbp) / SUM(obp_evt), NULL) AS obp,
           IF(SUM(ab) > 0, SUM(tb) / SUM(ab), NULL) AS slg,
           IF(SUM(obp_evt) > 0, (SUM(h + bb + hbp) / SUM(obp_evt)) + IF(SUM(ab) > 0, SUM(tb) / SUM(ab), 0), NULL) AS ops,
           SUM(winprob) AS winprob
    FROM tmp_splits_season
    WHERE split_type IN ("home-road", "month", "asb", "opponent", "stadium")
    GROUP BY player_id, season_type, split_type, split_label
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          pa = VALUES(pa),
                          ab = VALUES(ab),
                          obp_evt = VALUES(obp_evt),
                          h = VALUES(h),
                          1b = VALUES(1b),
                          2b = VALUES(2b),
                          3b = VALUES(3b),
                          hr = VALUES(hr),
                          r = VALUES(r),
                          rbi = VALUES(rbi),
                          bb = VALUES(bb),
                          ibb = VALUES(ibb),
                          hbp = VALUES(hbp),
                          k = VALUES(k),
                          sb = VALUES(sb),
                          cs = VALUES(cs),
                          sac_fly = VALUES(sac_fly),
                          sac_hit = VALUES(sac_hit),
                          tb = VALUES(tb),
                          gidp = VALUES(gidp),
                          lob = VALUES(lob),
                          avg = VALUES(avg),
                          obp = VALUES(obp),
                          slg = VALUES(slg),
                          ops = VALUES(ops),
                          winprob = VALUES(winprob);

  # Revert the memory change
  SET @@session.max_heap_table_size = DEFAULT; # DEFAULT = global value (i.e., what we had before...)
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

END $$

DELIMITER ;
