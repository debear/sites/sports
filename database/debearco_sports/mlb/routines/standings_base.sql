##
## Standings
##
DROP PROCEDURE IF EXISTS `mlb_standings_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_setup`(
  OUT v_season SMALLINT UNSIGNED
)
    COMMENT 'Base MLB standings config'
BEGIN

  DECLARE v_start_date DATE;
  DECLARE v_end_date DATE;

  # Get vars
  SELECT season, MIN(the_date), MAX(the_date) INTO v_season, v_start_date, v_end_date FROM tmp_date_list;

  # Identify the teams to calc
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    league_id TINYINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT TEAM_DIV.team_id, CONFERENCE.parent_id AS league_id, CONFERENCE.grouping_id AS conf_id, DIVISION.grouping_id AS div_id
    FROM SPORTS_MLB_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_MLB_GROUPINGS AS DIVISION
      ON (DIVISION.grouping_id = TEAM_DIV.grouping_id)
    JOIN SPORTS_MLB_GROUPINGS AS CONFERENCE
      ON (CONFERENCE.grouping_id = DIVISION.parent_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099);
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # Determine the schedule
  CALL mlb_standings_setup_sched(v_season, v_end_date);

  # Max GP by each team (Note: tmp_sched only includes results)
  DROP TEMPORARY TABLE IF EXISTS tmp_teams_max_gp;
  CREATE TEMPORARY TABLE tmp_teams_max_gp (
    team_id VARCHAR(3),
    max_gp TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id, COUNT(*) AS max_gp
    FROM tmp_teams
    JOIN SPORTS_MLB_SCHEDULE
      ON (SPORTS_MLB_SCHEDULE.season = v_season
      AND SPORTS_MLB_SCHEDULE.game_type = 'regular'
      AND tmp_teams.team_id IN (SPORTS_MLB_SCHEDULE.home, SPORTS_MLB_SCHEDULE.visitor)
      AND IFNULL(status, 'F') = 'F')
    GROUP BY tmp_teams.team_id;

  # Create our temporary table for caching before one mass insertion at the end
  CALL mlb_standings_setup_create();

  # Clear what was already there
  DELETE SPORTS_MLB_STANDINGS.*
  FROM tmp_date_list
  JOIN SPORTS_MLB_STANDINGS
    ON (SPORTS_MLB_STANDINGS.season = tmp_date_list.season
    AND SPORTS_MLB_STANDINGS.the_date = tmp_date_list.the_date);

END $$

DELIMITER ;

##
## Standings
##
DROP PROCEDURE IF EXISTS `mlb_standings_setup_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_setup_sched`(
  v_season SMALLINT UNSIGNED,
  v_max_date DATE
)
    COMMENT 'Schedule contributing to MLB standings'
BEGIN

  # Base info
  #  Note: we'll still include tied games, even though they won't count towards the W/L records...
  DROP TEMPORARY TABLE IF EXISTS tmp_sched_stage0;
  CREATE TEMPORARY TABLE tmp_sched_stage0 (
    team_id VARCHAR(3),
    game_id SMALLINT UNSIGNED,
    game_num TINYINT UNSIGNED,
    game_num_conf TINYINT UNSIGNED,
    opp_id VARCHAR(3),
    game_date DATE,
    game_time TIME,
    venue ENUM('home', 'visitor'),
    result ENUM('w', 'l', 't'),
    runs_for SMALLINT UNSIGNED,
    runs_against SMALLINT UNSIGNED,
    innings TINYINT UNSIGNED,
    is_conf_opp TINYINT UNSIGNED,
    is_div_opp TINYINT UNSIGNED,
    is_onerun TINYINT UNSIGNED,
    is_extras TINYINT UNSIGNED,
    was_suspended TINYINT UNSIGNED,
    PRIMARY KEY (team_id, game_id) USING BTREE,
    INDEX team_game_num (team_id, game_num) USING BTREE,
    INDEX team_game_num_conf (team_id, game_num_conf) USING BTREE,
    INDEX team_game_date (team_id, game_date, game_time) USING BTREE,
    INDEX team_venue (team_id, venue, game_date) USING BTREE,
    INDEX team_conf (team_id, is_conf_opp, game_date) USING BTREE,
    INDEX team_conf_num (team_id, is_conf_opp, game_num_conf) USING BTREE,
    INDEX team_div (team_id, is_div_opp, game_date) USING BTREE,
    INDEX team_onerun (team_id, is_onerun, game_date) USING BTREE,
    INDEX team_extras (team_id, is_extras, game_date) USING BTREE,
    INDEX team_extras_dh (team_id, game_date, opp_id, game_id) USING BTREE
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id,
           SPORTS_MLB_SCHEDULE.game_id,
           NULL AS game_num,
           NULL AS game_num_conf,
           IF(SPORTS_MLB_SCHEDULE.home = tmp_teams.team_id,
              SPORTS_MLB_SCHEDULE.visitor,
              SPORTS_MLB_SCHEDULE.home) AS opp_id,
           SPORTS_MLB_SCHEDULE.game_date,
           SPORTS_MLB_SCHEDULE.game_time,
           IF(SPORTS_MLB_SCHEDULE.home = tmp_teams.team_id,
              'home', 'visitor') AS venue,
           NULL AS result,
           IF(SPORTS_MLB_SCHEDULE.home = tmp_teams.team_id,
              SPORTS_MLB_SCHEDULE.home_score,
              SPORTS_MLB_SCHEDULE.visitor_score) AS runs_for,
           IF(SPORTS_MLB_SCHEDULE.home = tmp_teams.team_id,
              SPORTS_MLB_SCHEDULE.visitor_score,
              SPORTS_MLB_SCHEDULE.home_score) AS runs_against,
           SPORTS_MLB_SCHEDULE.innings,
           tmp_teams.conf_id = tmp_teams_cp.conf_id AS is_conf_opp,
           tmp_teams.conf_id = tmp_teams_cp.conf_id
             AND tmp_teams.div_id = tmp_teams_cp.div_id AS is_div_opp,
           ABS(CAST(SPORTS_MLB_SCHEDULE.home_score AS SIGNED) - CAST(SPORTS_MLB_SCHEDULE.visitor_score AS SIGNED)) = 1 AS is_onerun,
           SPORTS_MLB_SCHEDULE.innings > 9 AS is_extras,
           SPORTS_MLB_SCHEDULE.started_date IS NOT NULL AS was_suspended
    FROM SPORTS_MLB_SCHEDULE
    JOIN tmp_teams
      ON (tmp_teams.team_id = SPORTS_MLB_SCHEDULE.home
       OR tmp_teams.team_id = SPORTS_MLB_SCHEDULE.visitor)
    JOIN tmp_teams_cp
      ON (tmp_teams_cp.team_id
            = IF(SPORTS_MLB_SCHEDULE.home = tmp_teams.team_id,
                 SPORTS_MLB_SCHEDULE.visitor,
                 SPORTS_MLB_SCHEDULE.home))
    WHERE SPORTS_MLB_SCHEDULE.season = v_season
    AND   SPORTS_MLB_SCHEDULE.game_type = 'regular'
    AND   SPORTS_MLB_SCHEDULE.game_date <= v_max_date
    AND   SPORTS_MLB_SCHEDULE.status = 'F';

  # 2020-specific difference: Doubleheader extra innings after 7 innings, not 9
  IF v_season = 2020 THEN
    CALL _duplicate_tmp_table('tmp_sched_stage0', 'tmp_sched_stage0_cpA');
    CALL _duplicate_tmp_table('tmp_sched_stage0', 'tmp_sched_stage0_cpB');
    INSERT INTO tmp_sched_stage0 (team_id, game_id, is_extras)
      SELECT tmp_sched_stage0_cpA.team_id, tmp_sched_stage0_cpA.game_id,
             tmp_sched_stage0_cpA.innings > 7 AS is_extras
      FROM tmp_sched_stage0_cpA
      JOIN tmp_sched_stage0_cpB
        ON (tmp_sched_stage0_cpB.team_id = tmp_sched_stage0_cpA.team_id
        AND tmp_sched_stage0_cpB.game_date = tmp_sched_stage0_cpA.game_date
        AND tmp_sched_stage0_cpB.opp_id = tmp_sched_stage0_cpA.opp_id
        AND tmp_sched_stage0_cpB.was_suspended = 0
        AND tmp_sched_stage0_cpB.game_id <> tmp_sched_stage0_cpA.game_id)
      WHERE tmp_sched_stage0_cpA.was_suspended = 0
      GROUP BY tmp_sched_stage0_cpA.team_id, tmp_sched_stage0_cpA.game_id
    ON DUPLICATE KEY UPDATE is_extras = VALUES(is_extras);
  END IF;

  ALTER TABLE tmp_sched_stage0
    DROP COLUMN innings,
    DROP COLUMN was_suspended,
    DROP KEY team_extras_dh;

  # Calculate result info
  CALL _duplicate_tmp_table('tmp_sched_stage0', 'tmp_sched_stage1');
  INSERT INTO tmp_sched_stage1 (team_id, game_id, result)
    SELECT team_id, game_id,
           IF(runs_for > runs_against, 'w',
              IF(runs_for < runs_against, 'l',
                't')) AS result
    FROM tmp_sched_stage0
  ON DUPLICATE KEY UPDATE result = VALUES(result);

  # Calculate the game numbers - league wide
  CALL _duplicate_tmp_table('tmp_sched_stage1', 'tmp_sched_stage1_cp');
  CALL _duplicate_tmp_table('tmp_sched_stage1', 'tmp_sched_stage2');
  INSERT INTO tmp_sched_stage2 (team_id, game_id, game_num)
    SELECT tmp_sched_stage1.team_id, tmp_sched_stage1.game_id,
           COUNT(tmp_sched_stage1_cp.game_id) + 1 AS game_num
    FROM tmp_sched_stage1
    LEFT JOIN tmp_sched_stage1_cp
      ON (tmp_sched_stage1_cp.team_id = tmp_sched_stage1.team_id
      AND (tmp_sched_stage1_cp.game_date < tmp_sched_stage1.game_date
       OR (tmp_sched_stage1_cp.game_date = tmp_sched_stage1.game_date
       AND (tmp_sched_stage1_cp.game_time < tmp_sched_stage1.game_time
         OR (tmp_sched_stage1_cp.game_time = tmp_sched_stage1.game_time
         AND tmp_sched_stage1_cp.game_id < tmp_sched_stage1.game_id)))))
    GROUP BY tmp_sched_stage1.team_id, tmp_sched_stage1.game_id
  ON DUPLICATE KEY UPDATE game_num = VALUES(game_num);

  # Calculate the game numbers - conference wide
  CALL _duplicate_tmp_table('tmp_sched_stage2', 'tmp_sched_stage2_cp');
  CALL _duplicate_tmp_table('tmp_sched_stage2', 'tmp_sched');
  INSERT INTO tmp_sched (team_id, game_id, game_num_conf)
    SELECT tmp_sched_stage2.team_id, tmp_sched_stage2.game_id,
           COUNT(tmp_sched_stage2_cp.game_id) + 1 AS game_num_conf
    FROM tmp_sched_stage2
    LEFT JOIN tmp_sched_stage2_cp
      ON (tmp_sched_stage2_cp.team_id = tmp_sched_stage2.team_id
      AND tmp_sched_stage2_cp.is_conf_opp = 1
      AND (tmp_sched_stage2_cp.game_date < tmp_sched_stage2.game_date
       OR (tmp_sched_stage2_cp.game_date = tmp_sched_stage2.game_date
       AND (tmp_sched_stage2_cp.game_time < tmp_sched_stage2.game_time
         OR (tmp_sched_stage2_cp.game_time = tmp_sched_stage2.game_time
         AND tmp_sched_stage2_cp.game_id < tmp_sched_stage2.game_id)))))
    GROUP BY tmp_sched_stage2.team_id, tmp_sched_stage2.game_id
  ON DUPLICATE KEY UPDATE game_num_conf = VALUES(game_num_conf);

END $$

DELIMITER ;

##
## Store permenantly
##
DROP PROCEDURE IF EXISTS `mlb_standings_setup_create`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_setup_create`()
    COMMENT 'Create the temporary table to store working MLB standings'
BEGIN

  # Copy from base table
  DROP TEMPORARY TABLE IF EXISTS tmp_standings;
  CREATE TEMPORARY TABLE tmp_standings LIKE SPORTS_MLB_STANDINGS;
  ALTER TABLE tmp_standings ENGINE = MEMORY;

  # Add our tie-breaking columns
  ALTER TABLE tmp_standings
    ENGINE = MEMORY,
    ADD COLUMN _max_wins TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _max_win_pct DECIMAL(5,4),
    ADD COLUMN _worst_win_pct DECIMAL(5,4),
    ADD COLUMN _tb_final TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_league_id TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_conf_id TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_div_id TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_join TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_pos TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_is_tied TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_pos_adj TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN _tb_h2h_pct DECIMAL(5,4),
    ADD COLUMN _tb_intra_conf_pct DECIMAL(5,4),
    ADD COLUMN _tb_intra_div_pct DECIMAL(5,4),
    ADD COLUMN _tb_prev81_intraconf_pct DECIMAL(5,4),
    ADD COLUMN _tb_prev82_intraconf_pct DECIMAL(5,4),
    ADD COLUMN _tb_prev83_intraconf_pct DECIMAL(5,4),
    ADD COLUMN _tb_prev84_intraconf_pct DECIMAL(5,4),
    ADD COLUMN _tb_prev85_intraconf_pct DECIMAL(5,4),
    ADD INDEX is_tied (_tb_is_tied) USING BTREE,
    ADD INDEX sort (season, the_date, _tb_join, win_pct) USING BTREE,
    ADD INDEX tiebreak (season, the_date, _tb_join, _tb_pos) USING BTREE,
    ADD INDEX common (season, the_date, _tb_join, _tb_pos, team_id) USING BTREE,
    ADD INDEX stat_cod (_tb_final, pos_div) USING BTREE;

END $$

DELIMITER ;

##
## Store permenantly
##
DROP PROCEDURE IF EXISTS `mlb_standings_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_store`()
    COMMENT 'Store the MLB standings'
BEGIN

  # Remove our (temporary) tie-breaking columns (defined above)
  ALTER TABLE tmp_standings
    DROP COLUMN _max_wins,
    DROP COLUMN _max_win_pct,
    DROP COLUMN _worst_win_pct,
    DROP COLUMN _tb_final,
    DROP COLUMN _tb_league_id,
    DROP COLUMN _tb_conf_id,
    DROP COLUMN _tb_div_id,
    DROP COLUMN _tb_join,
    DROP COLUMN _tb_pos,
    DROP COLUMN _tb_is_tied,
    DROP COLUMN _tb_pos_adj,
    DROP COLUMN _tb_h2h_pct,
    DROP COLUMN _tb_intra_conf_pct,
    DROP COLUMN _tb_intra_div_pct,
    DROP COLUMN _tb_prev81_intraconf_pct,
    DROP COLUMN _tb_prev82_intraconf_pct,
    DROP COLUMN _tb_prev83_intraconf_pct,
    DROP COLUMN _tb_prev84_intraconf_pct,
    DROP COLUMN _tb_prev85_intraconf_pct;

  INSERT INTO SPORTS_MLB_STANDINGS
    SELECT * FROM tmp_standings;

END $$

DELIMITER ;

##
## End the script
##
DROP PROCEDURE IF EXISTS `mlb_standings_tidy`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_tidy`()
    COMMENT 'Tidy MLB standings calcs'
BEGIN

  ALTER TABLE SPORTS_MLB_STANDINGS ORDER BY season, the_date, team_id;

END $$

DELIMITER ;

##
## Misc: Number of divisions
##
DROP FUNCTION IF EXISTS `mlb_standings_num_divs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `mlb_standings_num_divs`(
  v_season SMALLINT UNSIGNED
) RETURNS TINYINT UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine number of MLB divisions in a league'
BEGIN

  DECLARE v_num_divs TINYINT UNSIGNED;
  SELECT COUNT(DISTINCT div_id) / COUNT(DISTINCT conf_id) INTO v_num_divs FROM tmp_teams;
  RETURN v_num_divs;

END $$

DELIMITER ;

##
## Misc: Number of division spots
##
DROP FUNCTION IF EXISTS `mlb_standings_num_div_berths`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `mlb_standings_num_div_berths`(
  v_season SMALLINT UNSIGNED
) RETURNS TINYINT UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine number of MLB divition teams in a league'
BEGIN

  # Normally, just one
  IF v_season <> 2020 THEN
    RETURN 1;
  # Except 2020, when there were two
  ELSE
    RETURN 2;
  END IF;

END $$

DELIMITER ;

##
## Misc: Number of wild card spots
##
DROP FUNCTION IF EXISTS `mlb_standings_num_wildcards`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `mlb_standings_num_wildcards`(
  v_season SMALLINT UNSIGNED
) RETURNS TINYINT UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine number of MLB wildcard teams in a league'
BEGIN

  IF v_season >= 2022 THEN
    # From 2022, three per league
    RETURN 3;
  ELSEIF v_season >= 2012 THEN
    # From 2012 (sort of excluding 2020), two per league
    RETURN 2;
  ELSE
    # Until 2011, just the one
    RETURN 1;
  END IF;

END $$

DELIMITER ;
