##
## Basic sorting
##
DROP PROCEDURE IF EXISTS `mlb_standings_ties_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_ties_sort`(
  v_season SMALLINT UNSIGNED,
  v_col VARCHAR(6)
)
    COMMENT 'Initial MLB standing calcs'
BEGIN

  DECLARE v_sort_col VARCHAR(7);
  DECLARE v_num_div_berths TINYINT UNSIGNED;

  SET v_num_div_berths = mlb_standings_num_div_berths(v_season);

  # Note: Slightly different query if Conf comparison, as opposed to Div / League
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, _tb_pos)
      SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
             COUNT(tmp_standings_cpB.team_id) + 1 AS _tb_pos
      FROM tmp_standings_cpA
      LEFT JOIN tmp_standings_cpB
        ON (tmp_standings_cpB.season = tmp_standings_cpA.season
        AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
        AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
        AND IF("', v_col, '" <> "conf",
               tmp_standings_cpB.win_pct > tmp_standings_cpA.win_pct,
               (tmp_standings_cpB.pos_div <= ', v_num_div_berths, ' AND tmp_standings_cpA.pos_div > ', v_num_div_berths, '
                 OR (tmp_standings_cpB.pos_div <= ', v_num_div_berths, ' AND tmp_standings_cpA.pos_div > tmp_standings_cpB.pos_div)
                 OR (tmp_standings_cpB.pos_div <= ', v_num_div_berths, ' AND tmp_standings_cpA.pos_div = tmp_standings_cpB.pos_div AND tmp_standings_cpB.win_pct > tmp_standings_cpA.win_pct)
                 OR (tmp_standings_cpB.pos_div > ', v_num_div_berths, ' AND tmp_standings_cpA.pos_div > ', v_num_div_berths, ' AND tmp_standings_cpB.win_pct > tmp_standings_cpA.win_pct))))
      GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
    ON DUPLICATE KEY UPDATE _tb_pos = VALUES(_tb_pos);'));

  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

END $$

DELIMITER ;

##
## Identify which teams are tied
##
DROP PROCEDURE IF EXISTS `mlb_standings_ties_identify`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_ties_identify`()
    COMMENT 'Identify MLB standing ties'
BEGIN

  INSERT INTO tmp_standings (season, the_date, team_id, _tb_is_tied)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           tmp_standings_cpB.team_id IS NOT NULL AS _tb_is_tied
    FROM tmp_standings_cpA
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos = tmp_standings_cpA._tb_pos
      AND tmp_standings_cpB.team_id <> tmp_standings_cpA.team_id)
  ON DUPLICATE KEY UPDATE _tb_is_tied = VALUES(_tb_is_tied);

  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

END $$

DELIMITER ;

##
## Determine how many ties are remaining
##
DROP FUNCTION IF EXISTS `mlb_standings_ties_num`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `mlb_standings_ties_num`() RETURNS SMALLINT UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine remaining intermediate MLB standing ties'
BEGIN

  DECLARE v_num SMALLINT UNSIGNED;
  SELECT SUM(_tb_is_tied = 1) INTO v_num FROM tmp_standings;
  RETURN v_num;

END $$

DELIMITER ;

##
## Break ties by updating their resolved positions
##
DROP PROCEDURE IF EXISTS `mlb_standings_ties_break`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_ties_break`()
    COMMENT 'Identify MLB broken standing ties'
BEGIN

  UPDATE tmp_standings
  SET _tb_pos = _tb_pos + _tb_pos_adj
  WHERE _tb_is_tied = 1;

  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

END $$

DELIMITER ;
