##
## Base MLB standings
##
DROP PROCEDURE IF EXISTS `mlb_standings_calcs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_calcs`(
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'MLB standings calculations'
BEGIN

  # Standings columns
  CALL mlb_standings_calcs_main();
  CALL mlb_standings_calcs_recent(v_recent_games);
  CALL mlb_standings_calcs_misc();

  # Tie-break facilitators
  CALL mlb_standings_calcs_tiebreak();

END $$

DELIMITER ;
