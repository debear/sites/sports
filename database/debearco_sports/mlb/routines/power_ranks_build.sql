##
## Individual build components
##
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build`()
    COMMENT 'Calculate the MLB power ranks component parts'
BEGIN

  # Temporary table our intermediate results will be stored in
  DROP TEMPORARY TABLE IF EXISTS tmp_stats;
  CREATE TEMPORARY TABLE tmp_stats (
    team_id VARCHAR(3),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id
    FROM tmp_teams;

  # Win Percentage
  CALL mlb_power_ranks_build_winpct(10);

  # One Run Win Percentage
  CALL mlb_power_ranks_build_winpct_onerun(4);

  # Run Differential
  CALL mlb_power_ranks_build_rundiff(7);

  # OPS
  CALL mlb_power_ranks_build_ops(9);

  # Home Runs
  CALL mlb_power_ranks_build_hr(5);

  # WHIP
  CALL mlb_power_ranks_build_whip(6);

  # Fielding Percentage
  CALL mlb_power_ranks_build_fieldpct(2);

END $$

DELIMITER ;

#
# Win %age
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build_winpct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build_winpct`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks win percentages'
BEGIN

  # Build the data
  CALL mlb_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              SUM(tmp_sched.result = 'w') / SUM(tmp_sched.result IN ('w','l'))) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              SUM(tmp_sched.is_ft AND tmp_sched.result = 'w') / SUM(tmp_sched.is_ft AND tmp_sched.result IN ('w','l'))) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              SUM(tmp_sched.is_wk AND tmp_sched.result = 'w') / SUM(tmp_sched.is_wk AND tmp_sched.result IN ('w','l'))) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL mlb_power_ranks_build_process('win_pct', 0, v_weight);

END $$

DELIMITER ;

#
# Win %age (one run games)
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build_winpct_onerun`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build_winpct_onerun`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks win percentages in one run games'
BEGIN

  # Build the data
  CALL mlb_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(SUM(tmp_sched.is_one_run AND tmp_sched.result IN ('w','l')) = 0, 0.500,
              SUM(tmp_sched.is_one_run AND tmp_sched.result = 'w') / SUM(tmp_sched.is_one_run AND tmp_sched.result IN ('w','l'))) AS tot_ov,
           IF(SUM(tmp_sched.is_ft AND tmp_sched.is_one_run AND tmp_sched.result IN ('w','l')) = 0, 0.500,
              SUM(tmp_sched.is_ft AND tmp_sched.is_one_run AND tmp_sched.result = 'w') / SUM(tmp_sched.is_ft AND tmp_sched.is_one_run AND tmp_sched.result IN ('w','l'))) AS tot_ft,
           IF(SUM(tmp_sched.is_wk AND tmp_sched.is_one_run AND tmp_sched.result IN ('w','l')) = 0, 0.500,
              SUM(tmp_sched.is_wk AND tmp_sched.is_one_run AND tmp_sched.result = 'w') / SUM(tmp_sched.is_wk AND tmp_sched.is_one_run AND tmp_sched.result IN ('w','l'))) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL mlb_power_ranks_build_process('win_pct_onerun', 0, v_weight);

END $$

DELIMITER ;

#
# Run Differential
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build_rundiff`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build_rundiff`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks run differential'
BEGIN

  # Build the data
  CALL mlb_power_ranks_build_schema('SMALLINT SIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              SUM(IF(tmp_sched.team_id = SCHED.home,
                     CAST(SCHED.home_score AS SIGNED) - CAST(SCHED.visitor_score AS SIGNED),
                     CAST(SCHED.visitor_score AS SIGNED) - CAST(SCHED.home_score AS SIGNED)))) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              SUM(IF(tmp_sched.is_ft,
                     IF(tmp_sched.team_id = SCHED.home,
                        CAST(SCHED.home_score AS SIGNED) - CAST(SCHED.visitor_score AS SIGNED),
                        CAST(SCHED.visitor_score AS SIGNED) - CAST(SCHED.home_score AS SIGNED)),
                     0))) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              SUM(IF(tmp_sched.is_wk,
                     IF(tmp_sched.team_id = SCHED.home,
                        CAST(SCHED.home_score AS SIGNED) - CAST(SCHED.visitor_score AS SIGNED),
                        CAST(SCHED.visitor_score AS SIGNED) - CAST(SCHED.home_score AS SIGNED)),
                     0))) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_MLB_SCHEDULE AS SCHED
      ON (SCHED.season = tmp_sched.season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_id = tmp_sched.game_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL mlb_power_ranks_build_process('run_diff', 0, v_weight);

END $$

DELIMITER ;

#
# OPS
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build_ops`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build_ops`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks OPS'
BEGIN

  # Build the data
  CALL mlb_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              IF(SUM(STATS.ab) > 0, SUM(STATS.tb) / SUM(STATS.ab) + SUM(STATS.h + STATS.bb + STATS.hbp) / SUM(STATS.ab + STATS.bb + STATS.sac_fly + STATS.hbp), 0)) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              IF(SUM(IF(tmp_sched.is_ft, STATS.ab, 0)) > 0, SUM(IF(tmp_sched.is_ft, STATS.tb, 0)) / SUM(IF(tmp_sched.is_ft, STATS.ab, 0)) + SUM(IF(tmp_sched.is_ft, STATS.h + STATS.bb + STATS.hbp, 0)) / SUM(IF(tmp_sched.is_ft, STATS.ab + STATS.bb + STATS.sac_fly + STATS.hbp, 0)), 0)) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              IF(SUM(IF(tmp_sched.is_wk, STATS.ab, 0)) > 0, SUM(IF(tmp_sched.is_wk, STATS.tb, 0)) / SUM(IF(tmp_sched.is_wk, STATS.ab, 0)) + SUM(IF(tmp_sched.is_wk, STATS.h + STATS.bb + STATS.hbp, 0)) / SUM(IF(tmp_sched.is_wk, STATS.ab + STATS.bb + STATS.sac_fly + STATS.hbp, 0)), 0)) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_MLB_TEAMS_GAME_BATTING AS STATS
      ON (STATS.season = tmp_sched.season
      AND STATS.game_type = 'regular'
      AND STATS.game_id = tmp_sched.game_id
      AND STATS.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL mlb_power_ranks_build_process('ops', 0, v_weight);

END $$

DELIMITER ;

#
# Home Runs
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build_hr`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build_hr`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks home runs'
BEGIN

  # Build the data
  CALL mlb_power_ranks_build_schema('SMALLINT UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0, SUM(STATS.hr)) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              SUM(IF(tmp_sched.is_ft, STATS.hr, 0))) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              SUM(IF(tmp_sched.is_wk, STATS.hr, 0))) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_MLB_TEAMS_GAME_BATTING AS STATS
      ON (STATS.season = tmp_sched.season
      AND STATS.game_type = 'regular'
      AND STATS.game_id = tmp_sched.game_id
      AND STATS.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL mlb_power_ranks_build_process('hr', 0, v_weight);

END $$

DELIMITER ;

#
# WHIP
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build_whip`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build_whip`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks WHIP'
BEGIN

  # Build the data
  CALL mlb_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              IF(SUM(STATS.out) > 0, SUM(STATS.bb + STATS.h) / (SUM(STATS.out) / 3), 0)) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              IF(SUM(IF(tmp_sched.is_ft, STATS.out, 0)) > 0, SUM(IF(tmp_sched.is_ft, STATS.bb + STATS.h, 0)) / (SUM(IF(tmp_sched.is_ft, STATS.out, 0)) / 3), 0)) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              IF(SUM(IF(tmp_sched.is_wk, STATS.out, 0)) > 0, SUM(IF(tmp_sched.is_wk, STATS.bb + STATS.h, 0)) / (SUM(IF(tmp_sched.is_wk, STATS.out, 0)) / 3), 0)) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_MLB_TEAMS_GAME_PITCHING AS STATS
      ON (STATS.season = tmp_sched.season
      AND STATS.game_type = 'regular'
      AND STATS.game_id = tmp_sched.game_id
      AND STATS.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL mlb_power_ranks_build_process('whip', 0, v_weight);

END $$

DELIMITER ;

#
# Fielding %age
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_build_fieldpct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_build_fieldpct`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks fielding percentage'
BEGIN

  # Build the data
  CALL mlb_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              IF(SUM(STATS.po + STATS.a + STATS.e) > 0, SUM(STATS.po + STATS.a) / SUM(STATS.po + STATS.a + STATS.e), 0)) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              IF(SUM(IF(tmp_sched.is_ft, STATS.po + STATS.a + STATS.e, 0)) > 0, SUM(IF(tmp_sched.is_ft, STATS.po + STATS.a, 0)) / SUM(IF(tmp_sched.is_ft, STATS.po + STATS.a + STATS.e, 0)), 0)) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              IF(SUM(IF(tmp_sched.is_wk, STATS.po + STATS.a + STATS.e, 0)) > 0, SUM(IF(tmp_sched.is_wk, STATS.po + STATS.a, 0)) / SUM(IF(tmp_sched.is_wk, STATS.po + STATS.a + STATS.e, 0)), 0)) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_MLB_TEAMS_GAME_FIELDING AS STATS
      ON (STATS.season = tmp_sched.season
      AND STATS.game_type = 'regular'
      AND STATS.game_id = tmp_sched.game_id
      AND STATS.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL mlb_power_ranks_build_process('field_pct', 0, v_weight);

END $$

DELIMITER ;
