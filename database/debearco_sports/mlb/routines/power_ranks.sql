##
## MLB power rank calculations
##
DROP PROCEDURE IF EXISTS `mlb_power_ranks`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate the MLB power ranks'
BEGIN

  # Loop vars
  DECLARE v_week TINYINT UNSIGNED;
  DECLARE v_calc_date DATE;
  DECLARE v_max_game_date DATE;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Loop through the weeks to be calculated
  DECLARE cur_weeks CURSOR FOR
    SELECT week, calc_date FROM SPORTS_MLB_POWER_RANKINGS_WEEKS WHERE season = v_season AND date_to <= v_max_game_date AND when_run IS NULL ORDER BY calc_date;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Identify the max game date for this season to know when to run until
  SELECT MAX(game_date) INTO v_max_game_date
  FROM SPORTS_MLB_SCHEDULE
  WHERE season = v_season
  AND   game_type = 'regular'
  AND   IFNULL(status, 'NULL') NOT IN ('NULL','PPD','CNC','SSP');

  OPEN cur_weeks;
  loop_weeks: LOOP

    FETCH cur_weeks INTO v_week, v_calc_date;
    IF v_done = 1 THEN LEAVE loop_weeks; END IF;

    # Build the schedule info to know which games to process
    CALL mlb_power_ranks_build_sched(v_season, v_calc_date);

    # Table to store our results
    DROP TEMPORARY TABLE IF EXISTS tmp_ranks;
    CREATE TEMPORARY TABLE tmp_ranks (
      team_id VARCHAR(3),
      `rank` TINYINT UNSIGNED,
      score DECIMAL(5,3),
      has_gp TINYINT UNSIGNED,
      PRIMARY KEY (team_id)
    ) ENGINE = MEMORY
      SELECT tmp_teams.team_id, 0 AS `rank`, 0 AS score, COUNT(tmp_sched.game_id) > 0 AS has_gp
      FROM tmp_teams
      LEFT JOIN tmp_sched
        ON (tmp_sched.team_id = tmp_teams.team_id)
      GROUP BY tmp_teams.team_id;

    # Build the individual components and combine into a single score
    CALL mlb_power_ranks_build();

    # Rank
    CALL mlb_power_ranks_sort();

    # Finally, store...
    CALL mlb_power_ranks_store(v_season, v_week);

  END LOOP loop_weeks;
  CLOSE cur_weeks;

END $$

DELIMITER ;

#
# Sort by rank
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_sort`()
    COMMENT 'Sort the MLB power ranks'
BEGIN

  CALL _duplicate_tmp_table('tmp_ranks', 'tmp_ranks_cpA');
  CALL _duplicate_tmp_table('tmp_ranks', 'tmp_ranks_cpB');

  INSERT INTO tmp_ranks (team_id, `rank`)
    SELECT tmp_ranks_cpA.team_id, COUNT(tmp_ranks_cpB.team_id) + 1 AS `rank`
    FROM tmp_ranks_cpA
    LEFT JOIN tmp_ranks_cpB
      # Seperate teams that have played from those who haven't
      ON (tmp_ranks_cpB.has_gp > tmp_ranks_cpA.has_gp
      # Rank below teams with a higher score
      OR (tmp_ranks_cpB.has_gp = tmp_ranks_cpA.has_gp
      AND tmp_ranks_cpB.score > tmp_ranks_cpA.score)
      # Or alphabetically when tied
      OR (tmp_ranks_cpB.has_gp = tmp_ranks_cpA.has_gp
      AND tmp_ranks_cpB.score = tmp_ranks_cpA.score
      AND STRCMP(tmp_ranks_cpB.team_id, tmp_ranks_cpA.team_id) = -1))
    GROUP BY tmp_ranks_cpA.team_id
  ON DUPLICATE KEY UPDATE `rank` = VALUES(`rank`);

  # We don't care about the numeric score for teams who haven't played yet
  UPDATE tmp_ranks
  SET score = NULL,
      win_pct = NULL,
      win_pct_onerun = NULL,
      run_diff = NULL,
      ops = NULL,
      hr = NULL,
      whip = NULL,
      field_pct = NULL
  WHERE has_gp = 0;

END $$

DELIMITER ;

#
# Store the final power ranks
#
DROP PROCEDURE IF EXISTS `mlb_power_ranks_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_power_ranks_store`(
  v_season SMALLINT UNSIGNED,
  v_week TINYINT UNSIGNED
)
    COMMENT 'Store the MLB power ranks'
BEGIN

  DELETE FROM SPORTS_MLB_POWER_RANKINGS
  WHERE season = v_season
  AND   week = v_week;

  ALTER TABLE tmp_ranks DROP COLUMN has_gp;

  INSERT INTO SPORTS_MLB_POWER_RANKINGS
    SELECT v_season AS season, v_week AS week, tmp_ranks.*, NULL AS comment
    FROM tmp_ranks;

  UPDATE SPORTS_MLB_POWER_RANKINGS_WEEKS
  SET when_run = NOW()
  WHERE season = v_season
  AND   week = v_week;

  ALTER TABLE SPORTS_MLB_POWER_RANKINGS ORDER BY season, week, team_id;

END $$

DELIMITER ;
