#
# Fielder-based split stats (no situational available)
#
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_fielders`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_fielders`()
    COMMENT 'Determine split stats for fielders over a season'
BEGIN

  # Prepare the temporary table
  CALL mlb_totals_players_splits_fielders_setup();

  # Home / Road
  CALL mlb_totals_players_splits_fielders_calc('home-road', 'IF(BASE.is_home, "Home", "Road")', 'BASE.is_home', NULL);

  # By Month
  CALL mlb_totals_players_splits_fielders_calc('month', 'BASE.month', 'BASE.month', NULL);

  # Pre-/Post-ASB (regular season only)
  CALL mlb_totals_players_splits_fielders_calc('asb', 'IF(BASE.pre_asb, "Pre All-Star Break", "Post All-Star Break")', 'BASE.pre_asb', 'BASE.game_type = "regular"');

  # By Opp
  CALL mlb_totals_players_splits_fielders_calc('opponent', 'BASE.opp_team_id', 'BASE.opp_team_id', NULL);

  # By Stadium
  CALL mlb_totals_players_splits_fielders_calc('stadium', 'BASE.stadium', 'BASE.stadium', NULL);

  # Commit any changes
  CALL mlb_totals_players_splits_fielders_commit();

  # Career versions (not linked to this season)
  CALL mlb_totals_players_splits_fielders_career();

END $$

DELIMITER ;

# Prepare our work
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_fielders_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_fielders_setup`()
    COMMENT 'Fielder split stat preparation'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_FIELDING_SPLITS;
  CREATE TEMPORARY TABLE tmp_FIELDING_SPLITS LIKE SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS;
  ALTER TABLE tmp_FIELDING_SPLITS
    ENGINE = MEMORY,
    MODIFY COLUMN split_label VARCHAR(50);

  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_PLAYERS_GAME_FIELDING;
  CREATE TEMPORARY TABLE tmp_SPORTS_MLB_PLAYERS_GAME_FIELDING LIKE SPORTS_MLB_PLAYERS_GAME_FIELDING;
  INSERT INTO tmp_SPORTS_MLB_PLAYERS_GAME_FIELDING
    SELECT STATS.*
    FROM tmp_splits_rosters AS BASE
    JOIN SPORTS_MLB_PLAYERS_GAME_FIELDING AS STATS
      ON (STATS.season = BASE.season
      AND STATS.game_type = BASE.game_type
      AND STATS.game_id = BASE.game_id
      AND STATS.player_id = BASE.player_id);

END $$

DELIMITER ;

# Worker method
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_fielders_calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_fielders_calc`(
  v_split_type VARCHAR(50),
  v_split_label VARCHAR(100),
  v_group_by VARCHAR(255),
  v_where VARCHAR(255)
)
    COMMENT 'Fielder split stat calculation'
BEGIN

  # Split stats, which are more relevant game-wide stats than cumulation of ABs
  CALL _exec(CONCAT('
    INSERT INTO tmp_FIELDING_SPLITS (season, season_type, player_id, split_type, split_label, tc, po, a, e, dp)
      SELECT BASE.season, BASE.game_type, BASE.player_id,
             "', v_split_type, '" AS split_type,
             ', v_split_label, ' AS split_label,
             SUM(STATS.tc) AS tc,
             SUM(STATS.po) AS po,
             SUM(STATS.a) AS a,
             SUM(STATS.e) AS e,
             SUM(STATS.dp) AS dp
      FROM tmp_splits_rosters AS BASE
      JOIN tmp_SPORTS_MLB_PLAYERS_GAME_FIELDING AS STATS
        ON (STATS.season = BASE.season
        AND STATS.game_type = BASE.game_type
        AND STATS.game_id = BASE.game_id
        AND STATS.player_id = BASE.player_id)
      ', IF(v_where IS NOT NULL, CONCAT('WHERE ', v_where), ''), '
      GROUP BY BASE.season, BASE.game_type, BASE.player_id, ', v_group_by, '
    ON DUPLICATE KEY UPDATE tc = VALUES(tc),
                            po = VALUES(po),
                            a = VALUES(a),
                            e = VALUES(e),
                            dp = VALUES(dp);'));

END $$

DELIMITER ;

# Save changes back to the database
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_fielders_commit`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_fielders_commit`()
    COMMENT 'Fielder split stat storing to main table'
BEGIN

  # Composite calcs
  UPDATE tmp_FIELDING_SPLITS
  SET pct = IF(tc > 0, (po + a) / tc, NULL);

  # Convert the split_labels in to an ID
  INSERT IGNORE INTO SPORTS_MLB_PLAYERS_SPLIT_LABELS (split_id, split_type, split_label, created)
    SELECT DISTINCT NULL, split_type, split_label, NOW() as created
    FROM tmp_FIELDING_SPLITS
    ORDER BY split_type, split_label;
  UPDATE tmp_FIELDING_SPLITS AS SPLITS
  JOIN SPORTS_MLB_PLAYERS_SPLIT_LABELS AS LABELS
    ON (LABELS.split_type = SPLITS.split_type
    AND LABELS.split_label = SPLITS.split_label)
  SET SPLITS.split_label = LABELS.split_id;

  # Save to the main table
  INSERT INTO SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS
    SELECT *
    FROM tmp_FIELDING_SPLITS
  ON DUPLICATE KEY UPDATE tc = VALUES(tc),
                          po = VALUES(po),
                          a = VALUES(a),
                          e = VALUES(e),
                          pct = VALUES(pct),
                          dp = VALUES(dp);

END $$

DELIMITER ;

# Career stats
DROP PROCEDURE IF EXISTS `mlb_totals_players_splits_fielders_career`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_players_splits_fielders_career`()
    COMMENT 'Fielder split stat over a career'
BEGIN

  # Up memory limit to 256meg for the temporary table (as data is in FIXED format, which is wider, and oft exceeds the 16meg default)
  SET @@session.max_heap_table_size = 268435456; # 268,435,456 = 256 * 1024 * 1024
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

  # Copy appropriate stats into a temporary table
  DROP TEMPORARY TABLE IF EXISTS tmp_splits_season;
  CREATE TEMPORARY TABLE tmp_splits_season LIKE SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS;
  ALTER TABLE tmp_splits_season ENGINE = MEMORY;
  INSERT INTO tmp_splits_season
    SELECT STATS.*
    FROM tmp_split_players
    JOIN SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS AS STATS
      ON (STATS.player_id = tmp_split_players.player_id);

  # Then aggregate
  INSERT INTO SPORTS_MLB_PLAYERS_CAREER_FIELDING_SPLITS (player_id, season_type, split_type, split_label, tc, po, a, e, pct, dp)
    SELECT player_id, season_type, split_type, split_label,
           SUM(tc) AS tc,
           SUM(po) AS po,
           SUM(a) AS a,
           SUM(e) AS e,
           IF(SUM(tc) > 0, (SUM(po) + SUM(a)) / SUM(tc), NULL) AS pct,
           SUM(dp) AS dp
    FROM tmp_splits_season
    WHERE split_type IN ("home-road", "month", "asb", "opponent", "stadium")
    GROUP BY player_id, season_type, split_type, split_label
  ON DUPLICATE KEY UPDATE tc = VALUES(tc),
                          po = VALUES(po),
                          a = VALUES(a),
                          e = VALUES(e),
                          pct = VALUES(pct),
                          dp = VALUES(dp);

  # Revert the memory change
  SET @@session.max_heap_table_size = DEFAULT; # DEFAULT = global value (i.e., what we had before...)
  SET @@session.tmp_table_size = @@session.max_heap_table_size;

END $$

DELIMITER ;
