##
## Team rosters
##
DROP PROCEDURE IF EXISTS `mlb_rosters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_rosters`()
    COMMENT 'Update MLB team rosters with info from game lineups'
BEGIN

  INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
    SELECT SPORTS_MLB_SCHEDULE.season,
           SPORTS_MLB_SCHEDULE.game_date AS the_date,
           SPORTS_MLB_GAME_ROSTERS.team_id,
           SPORTS_MLB_GAME_ROSTERS.player_id,
           SPORTS_MLB_GAME_ROSTERS.jersey,
           IF(SPORTS_MLB_TEAMS_ROSTERS.pos = 'TW', SPORTS_MLB_TEAMS_ROSTERS.pos,
              IFNULL(SPORTS_MLB_GAME_ROSTERS.roster_pos, SPORTS_MLB_TEAMS_ROSTERS.pos)) AS pos,
           'active' AS player_status,
           '25-man' AS roster_type
    FROM tmp_game_list
    JOIN SPORTS_MLB_SCHEDULE
      ON (SPORTS_MLB_SCHEDULE.season = tmp_game_list.season
      AND SPORTS_MLB_SCHEDULE.game_type = tmp_game_list.game_type
      AND SPORTS_MLB_SCHEDULE.game_id = tmp_game_list.game_id)
    JOIN SPORTS_MLB_GAME_ROSTERS
      ON (SPORTS_MLB_GAME_ROSTERS.season = SPORTS_MLB_SCHEDULE.season
      AND SPORTS_MLB_GAME_ROSTERS.game_type = SPORTS_MLB_SCHEDULE.game_type
      AND SPORTS_MLB_GAME_ROSTERS.game_id = SPORTS_MLB_SCHEDULE.game_id)
    LEFT JOIN SPORTS_MLB_TEAMS_ROSTERS
      ON (SPORTS_MLB_TEAMS_ROSTERS.season = SPORTS_MLB_SCHEDULE.season
      AND SPORTS_MLB_TEAMS_ROSTERS.the_date = SPORTS_MLB_SCHEDULE.game_date
      AND SPORTS_MLB_TEAMS_ROSTERS.player_id = SPORTS_MLB_GAME_ROSTERS.player_id)
  ON DUPLICATE KEY UPDATE pos = VALUES(pos);

  ALTER TABLE SPORTS_MLB_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;

END $$

DELIMITER ;
