##
##
##
DROP PROCEDURE IF EXISTS `mlb_standings_codes_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_codes_setup`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Prepare MLB standing code processing'
BEGIN

  DECLARE v_season_finale DATE;
  # Determine which standings are final (so we can more accurately process the codes)
  SELECT MAX(game_date) INTO v_season_finale FROM SPORTS_MLB_SCHEDULE WHERE season = v_season AND game_type = 'regular';
  UPDATE tmp_standings SET _tb_final = (the_date = v_season_finale);

END $$

DELIMITER ;

##
## Process status codes
##
DROP PROCEDURE IF EXISTS `mlb_standings_codes_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_codes_process`(
  v_code CHAR(1),
  v_col VARCHAR(6),
  v_cutoff_pos TINYINT UNSIGNED
)
    COMMENT 'Process MLB standing codes'
BEGIN

  # Simplify our query by generalising our position and join cols
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = IF("', v_col, '" = "league", _tb_league_id, IF("', v_col, '" = "conf", _tb_conf_id, _tb_div_id)),
                                              _tb_pos = IF("', v_col, '" = "league", pos_league, IF("', v_col, '" = "conf", pos_conf, pos_div));'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  # Process - Guesstimate
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, status_code)
      SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
             IF(COUNT(tmp_standings_cpB.team_id) < ', v_cutoff_pos, ', "', v_code, '", tmp_standings_cpA.status_code) AS status_code
      FROM tmp_standings_cpA
      LEFT JOIN tmp_standings_cpB
        ON (tmp_standings_cpB.season = tmp_standings_cpA.season
        AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
        AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
        AND tmp_standings_cpB.team_id <> tmp_standings_cpA.team_id
        AND tmp_standings_cpB._max_win_pct > tmp_standings_cpA._worst_win_pct)
      WHERE tmp_standings_cpA._tb_final = 0
      GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
      HAVING status_code IS NOT NULL
    ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);'));

  # Process - Final
  INSERT INTO tmp_standings (season, the_date, team_id, status_code)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           v_code AS status_code
    FROM tmp_standings_cpA
    WHERE tmp_standings_cpA._tb_final = 1
    AND   tmp_standings_cpA._tb_pos <= v_cutoff_pos
  ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);

END $$

DELIMITER ;

##
## Process status codes for our div/conf count "mix" scenario
##
DROP PROCEDURE IF EXISTS `mlb_standings_codes_div_conf_mix`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_codes_div_conf_mix`(
  v_code CHAR(1),
  v_conf_count TINYINT UNSIGNED
)
    COMMENT 'Process MLB standing codes for div/conf mix count'
BEGIN

  # Determine our pool of affected teams - and "re-seed"
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_mix');
  ALTER TABLE tmp_standings_mix
    ADD COLUMN _mix_pos_conf TINYINT UNSIGNED;

  CALL _duplicate_tmp_table('tmp_standings_mix', 'tmp_standings_mix_cpA');
  CALL _duplicate_tmp_table('tmp_standings_mix', 'tmp_standings_mix_cpB');
  INSERT INTO tmp_standings_mix (season, the_date, team_id, _mix_pos_conf)
    SELECT tmp_standings_mix_cpA.season, tmp_standings_mix_cpA.the_date, tmp_standings_mix_cpA.team_id,
           COUNT(tmp_standings_mix_cpA.team_id) + 1 AS _mix_pos_conf
    FROM tmp_standings_mix_cpA
    LEFT JOIN tmp_standings_mix_cpB
      ON (tmp_standings_mix_cpB.season = tmp_standings_mix_cpA.season
      AND tmp_standings_mix_cpB.the_date = tmp_standings_mix_cpA.the_date
      AND tmp_standings_mix_cpB._tb_conf_id = tmp_standings_mix_cpA._tb_conf_id
      AND tmp_standings_mix_cpB.pos_conf < tmp_standings_mix_cpA.pos_conf)
    GROUP BY tmp_standings_mix_cpA.season, tmp_standings_mix_cpA.the_date, tmp_standings_mix_cpA.team_id
  ON DUPLICATE KEY UPDATE _mix_pos_conf = VALUES(_mix_pos_conf);

  # Now process the calcs using this re-seeded table
  CALL _duplicate_tmp_table('tmp_standings_mix', 'tmp_standings_mix_cpA');
  CALL _duplicate_tmp_table('tmp_standings_mix', 'tmp_standings_mix_cpB');

  # Process - Guesstimate
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, status_code)
      SELECT tmp_standings_mix_cpA.season, tmp_standings_mix_cpA.the_date, tmp_standings_mix_cpA.team_id,
             IF(COUNT(tmp_standings_mix_cpB.team_id) <= ', v_conf_count, ', "', v_code, '", tmp_standings_mix_cpA.status_code) AS status_code
      FROM tmp_standings_mix_cpA
      LEFT JOIN tmp_standings_mix_cpB
        ON (tmp_standings_mix_cpB.season = tmp_standings_mix_cpA.season
        AND tmp_standings_mix_cpB.the_date = tmp_standings_mix_cpA.the_date
        AND tmp_standings_mix_cpB._tb_conf_id = tmp_standings_mix_cpA._tb_conf_id
        AND tmp_standings_mix_cpB._max_win_pct > tmp_standings_mix_cpA._worst_win_pct)
      WHERE tmp_standings_mix_cpA._tb_final = 0
      GROUP BY tmp_standings_mix_cpA.season, tmp_standings_mix_cpA.the_date, tmp_standings_mix_cpA.team_id
      HAVING status_code IS NOT NULL
    ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);'));

  # Process - Final
  INSERT INTO tmp_standings (season, the_date, team_id, status_code)
    SELECT tmp_standings_mix_cpA.season, tmp_standings_mix_cpA.the_date, tmp_standings_mix_cpA.team_id,
           v_code AS status_code
    FROM tmp_standings_mix_cpA
    WHERE tmp_standings_mix_cpA._tb_final = 1
    AND   tmp_standings_mix_cpA._mix_pos_conf <= v_conf_count
  ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);

END $$

DELIMITER ;

##
## Identify (confirmed) wilcard teams - non-div leader where division winner already crowned
##
DROP PROCEDURE IF EXISTS `mlb_standings_codes_wildcards`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_codes_wildcards`(
  v_code_po CHAR(1),
  v_code_wc CHAR(1),
  v_code_div CHAR(1)
)
    COMMENT 'Process MLB confirmed wildcard standing codes'
BEGIN

  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  UPDATE tmp_standings
  JOIN tmp_standings_cpA
    ON (tmp_standings_cpA.season = tmp_standings.season
    AND tmp_standings_cpA.the_date = tmp_standings.the_date
    AND tmp_standings_cpA._tb_div_id = tmp_standings._tb_div_id
    AND tmp_standings_cpA.status_code = v_code_div)
  SET tmp_standings.status_code = v_code_wc
  WHERE tmp_standings.status_code = v_code_po;

END $$

DELIMITER ;
