##
## Team rosters
##
DROP PROCEDURE IF EXISTS `mlb_rosters_backdate`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_rosters_backdate`()
    COMMENT 'Calculate MLB team rosters from game lineups'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_date DATE;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Cursor
  DECLARE cur_date CURSOR FOR
    SELECT the_date FROM tmp_date_list ORDER BY the_date;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  SELECT DISTINCT season INTO v_season FROM tmp_date_list;

  # Clear previous calculations
  DELETE SPORTS_MLB_TEAMS_ROSTERS.*
  FROM tmp_date_list
  JOIN SPORTS_MLB_TEAMS_ROSTERS
    ON (SPORTS_MLB_TEAMS_ROSTERS.season = tmp_date_list.season
    AND SPORTS_MLB_TEAMS_ROSTERS.the_date = tmp_date_list.the_date);

  # Loop through each of the days and process individually
  OPEN cur_date;
  loop_date: LOOP

    FETCH cur_date INTO v_date;
    IF v_done = 1 THEN LEAVE loop_date; END IF;

    CALL mlb_rosters_backdate_calc(v_season, v_date);

  END LOOP loop_date;
  CLOSE cur_date;

  # Final ordering
  ALTER TABLE SPORTS_MLB_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_rosters_backdate_calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_rosters_backdate_calc`(
  v_season SMALLINT UNSIGNED,
  v_date DATE
)
    COMMENT 'Calculate daily MLB team rosters from game lineups'
BEGIN

  DECLARE v_copy_date DATE;

  # Create from the previous day
  SELECT MAX(the_date) INTO v_copy_date FROM SPORTS_MLB_TEAMS_ROSTERS WHERE season = v_season AND the_date < v_date;
  INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status)
    SELECT season, v_date, team_id, player_id, jersey, pos, player_status
    FROM SPORTS_MLB_TEAMS_ROSTERS
    WHERE season = v_season
    AND   the_date = v_copy_date;

  # Calculate the appropriate game for each date
  DROP TEMPORARY TABLE IF EXISTS tmp_mlb_game;
  CREATE TEMPORARY TABLE tmp_mlb_game (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    game_type ENUM('regular','playoff'),
    game_id SMALLINT(5) UNSIGNED,
    PRIMARY KEY(season, the_date, team_id),
    INDEX active_roster (season, game_type, game_id, team_id)
  ) ENGINE = MEMORY
    SELECT v_season AS season,
           v_date AS the_date,
           SPORTS_MLB_TEAMS_GROUPINGS.team_id,
           SUBSTRING(MAX(CONCAT(SPORTS_MLB_SCHEDULE.game_date, ';', SPORTS_MLB_SCHEDULE.game_type)), 12) AS game_type,
           SUBSTRING(MAX(CONCAT(SPORTS_MLB_SCHEDULE.game_date, ';', SPORTS_MLB_SCHEDULE.game_id)), 12) AS game_id
    FROM SPORTS_MLB_TEAMS_GROUPINGS
    LEFT JOIN SPORTS_MLB_SCHEDULE
      ON (SPORTS_MLB_SCHEDULE.season = v_season
      AND (SPORTS_MLB_SCHEDULE.home = SPORTS_MLB_TEAMS_GROUPINGS.team_id
        OR SPORTS_MLB_SCHEDULE.visitor = SPORTS_MLB_TEAMS_GROUPINGS.team_id)
      AND SPORTS_MLB_SCHEDULE.game_date = v_date)
    WHERE v_season BETWEEN SPORTS_MLB_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_MLB_TEAMS_GROUPINGS.season_to, 2099)
    GROUP BY SPORTS_MLB_TEAMS_GROUPINGS.team_id;

  # Build the active roster from the team's game
  DROP TEMPORARY TABLE IF EXISTS tmp_mlb_active_rosters;
  CREATE TEMPORARY TABLE tmp_mlb_active_rosters (
    season SMALLINT UNSIGNED,
    the_date DATE,
    game_type ENUM('regular','playoff'),
    game_id SMALLINT(3) UNSIGNED,
    team_id VARCHAR(3),
    player_id SMALLINT(5) UNSIGNED,
    jersey TINYINT(3) UNSIGNED,
    pos VARCHAR(3) NULL,
    player_status ENUM('active','na') NULL,
    PRIMARY KEY (season, the_date, game_type, game_id, team_id, player_id),
    INDEX player (season, player_id),
    INDEX team (team_id, game_type, game_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_MLB_GAME_ROSTERS.season,
           v_date AS the_date,
           SPORTS_MLB_GAME_ROSTERS.game_type,
           SPORTS_MLB_GAME_ROSTERS.game_id,
           SPORTS_MLB_GAME_ROSTERS.team_id,
           SPORTS_MLB_GAME_ROSTERS.player_id,
           SPORTS_MLB_GAME_ROSTERS.jersey,
           -- At first, use the roster_pos from game data
           IF(IFNULL(SPORTS_MLB_GAME_ROSTERS.roster_pos, '') <> '',
              SPORTS_MLB_GAME_ROSTERS.roster_pos,
              -- Failing that, try the previous roster position
              IF(IFNULL(SPORTS_MLB_TEAMS_ROSTERS.pos, '') <> '',
                 SPORTS_MLB_TEAMS_ROSTERS.pos,
                 -- Finally, for an initial position use the game position (bearing in mind we could have pinch hitters/runners or relievers as initial position)
                 IF(IFNULL(SPORTS_MLB_GAME_ROSTERS.pos, '') <> '',
                    SPORTS_MLB_GAME_ROSTERS.pos,
                    CASE IFNULL(SPORTS_MLB_GAME_ROSTERS.pos_alt1, '')
                      WHEN 'PH' THEN SPORTS_MLB_GAME_ROSTERS.pos_alt2
                      WHEN 'PR' THEN SPORTS_MLB_GAME_ROSTERS.pos_alt2
                      WHEN 'RP' THEN 'P'
                      ELSE SPORTS_MLB_GAME_ROSTERS.pos_alt1
                    END))) AS pos,
           'active' AS player_status
    FROM tmp_mlb_game
    JOIN SPORTS_MLB_GAME_ROSTERS
      ON (SPORTS_MLB_GAME_ROSTERS.season = tmp_mlb_game.season
      AND SPORTS_MLB_GAME_ROSTERS.game_type = tmp_mlb_game.game_type
      AND SPORTS_MLB_GAME_ROSTERS.game_id = tmp_mlb_game.game_id
      AND SPORTS_MLB_GAME_ROSTERS.team_id = tmp_mlb_game.team_id)
    LEFT JOIN SPORTS_MLB_TEAMS_ROSTERS
      ON (SPORTS_MLB_TEAMS_ROSTERS.season = v_season
      AND SPORTS_MLB_TEAMS_ROSTERS.the_date = v_date
      AND SPORTS_MLB_TEAMS_ROSTERS.team_id = SPORTS_MLB_GAME_ROSTERS.team_id
      AND SPORTS_MLB_TEAMS_ROSTERS.player_id = SPORTS_MLB_GAME_ROSTERS.player_id);

  # And then update according to active rosters from the previous game
  INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
    SELECT SPORTS_MLB_TEAMS_ROSTERS.season,
           SPORTS_MLB_TEAMS_ROSTERS.the_date,
           IF(tmp_mlb_active_rosters.player_id IS NOT NULL,
              tmp_mlb_active_rosters.team_id,
              SPORTS_MLB_TEAMS_ROSTERS.team_id) AS team_id,
           SPORTS_MLB_TEAMS_ROSTERS.player_id,
           IF(tmp_mlb_active_rosters.player_id IS NOT NULL,
              tmp_mlb_active_rosters.jersey,
              SPORTS_MLB_TEAMS_ROSTERS.jersey) AS jersey,
           IF(tmp_mlb_active_rosters.player_id IS NOT NULL,
              tmp_mlb_active_rosters.pos,
              SPORTS_MLB_TEAMS_ROSTERS.pos) AS pos,
           IF(tmp_mlb_active_rosters.player_id IS NOT NULL,
              'active',
              IF(tmp_mlb_game.game_id IS NULL,
                 SPORTS_MLB_TEAMS_ROSTERS.player_status, 'na')) AS player_status,
           IF(tmp_mlb_active_rosters.player_id IS NOT NULL OR tmp_mlb_game.game_id IS NULL,
              '25-man', '40-man') AS roster_type
    FROM SPORTS_MLB_TEAMS_ROSTERS
    LEFT JOIN tmp_mlb_active_rosters
      ON (tmp_mlb_active_rosters.season = SPORTS_MLB_TEAMS_ROSTERS.season
      AND tmp_mlb_active_rosters.the_date = SPORTS_MLB_TEAMS_ROSTERS.the_date
      AND tmp_mlb_active_rosters.player_id = SPORTS_MLB_TEAMS_ROSTERS.player_id)
    LEFT JOIN tmp_mlb_game
      ON (tmp_mlb_game.season = SPORTS_MLB_TEAMS_ROSTERS.season
      AND tmp_mlb_game.the_date = SPORTS_MLB_TEAMS_ROSTERS.the_date
      AND tmp_mlb_game.team_id = SPORTS_MLB_TEAMS_ROSTERS.team_id)
    WHERE SPORTS_MLB_TEAMS_ROSTERS.season = v_season
    AND   SPORTS_MLB_TEAMS_ROSTERS.the_date = v_date
  ON DUPLICATE KEY UPDATE team_id       = VALUES(team_id),
                          jersey        = VALUES(jersey),
                          player_status = VALUES(player_status),
                          roster_type   = VALUES(roster_type);

  # Add any new players
  INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
    SELECT tmp_mlb_active_rosters.season,
           tmp_mlb_active_rosters.the_date,
           tmp_mlb_active_rosters.team_id,
           tmp_mlb_active_rosters.player_id,
           tmp_mlb_active_rosters.jersey,
           tmp_mlb_active_rosters.pos,
           tmp_mlb_active_rosters.player_status,
           '25-man' AS roster_type
    FROM tmp_mlb_active_rosters
    LEFT JOIN SPORTS_MLB_TEAMS_ROSTERS
      ON (SPORTS_MLB_TEAMS_ROSTERS.season = tmp_mlb_active_rosters.season
      AND SPORTS_MLB_TEAMS_ROSTERS.the_date = tmp_mlb_active_rosters.the_date
      AND SPORTS_MLB_TEAMS_ROSTERS.player_id = tmp_mlb_active_rosters.player_id)
    WHERE tmp_mlb_active_rosters.season = v_season
    AND   tmp_mlb_active_rosters.the_date = v_date
    AND   SPORTS_MLB_TEAMS_ROSTERS.player_id IS NULL
  ON DUPLICATE KEY UPDATE team_id       = VALUES(team_id),
                          jersey        = VALUES(jersey),
                          player_status = VALUES(player_status),
                          roster_type   = VALUES(roster_type);

END $$

DELIMITER ;
