#
# Post-process the list of probables
#
DROP PROCEDURE IF EXISTS `mlb_probables_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_probables_process`()
    COMMENT 'Post-process the MLB probable pitcher info'
BEGIN

  DECLARE v_roster_season SMALLINT UNSIGNED;
  DECLARE v_roster_date DATE;

  # Pitcher list
  DROP TEMPORARY TABLE IF EXISTS tmp_probables_pitchers;
  CREATE TEMPORARY TABLE tmp_probables_pitchers (
    player_id SMALLINT UNSIGNED,
    game_date DATE,
    team_id CHAR(3),
    catcher_id SMALLINT UNSIGNED,
    catcher_reason ENUM('usual-catcher', 'team-starter', 'depth-chart'),
    PRIMARY KEY (player_id)
  ) SELECT SPORTS_MLB_SCHEDULE_PROBABLES.player_id,
           MIN(SPORTS_MLB_SCHEDULE_PROBABLES.game_date) AS game_date,
           SPORTS_MLB_SCHEDULE_PROBABLES.team_id,
           NULL AS catcher_id,
           NULL AS catcher_reason
    FROM tmp_probables_dates
    JOIN SPORTS_MLB_SCHEDULE_PROBABLES
      ON (SPORTS_MLB_SCHEDULE_PROBABLES.season = tmp_probables_dates.season
      AND SPORTS_MLB_SCHEDULE_PROBABLES.game_date = tmp_probables_dates.game_date
      AND SPORTS_MLB_SCHEDULE_PROBABLES.player_id IS NOT NULL)
    GROUP BY SPORTS_MLB_SCHEDULE_PROBABLES.player_id;

  # Load the roster details
  SELECT season, MIN(game_date) INTO v_roster_season, v_roster_date FROM tmp_probables_dates;

  SELECT MAX(the_date) INTO v_roster_date
  FROM SPORTS_MLB_TEAMS_ROSTERS
  WHERE season = v_roster_season
  AND   the_date <= v_roster_date;

  DROP TEMPORARY TABLE IF EXISTS tmp_probables_rosters;
  CREATE TEMPORARY TABLE tmp_probables_rosters LIKE SPORTS_MLB_TEAMS_ROSTERS;
  INSERT INTO tmp_probables_rosters
    SELECT *
    FROM SPORTS_MLB_TEAMS_ROSTERS
    WHERE season = v_roster_season
    AND   the_date = v_roster_date;

  # Probable's usual starting catcher
  CALL mlb_probables_process_battery();
  # Team's usual starter
  CALL mlb_probables_process_recency();
  # Depth chart
  CALL mlb_probables_process_depth();

  # And store
  UPDATE tmp_probables_dates
  JOIN SPORTS_MLB_SCHEDULE_PROBABLES
    ON (SPORTS_MLB_SCHEDULE_PROBABLES.season = tmp_probables_dates.season
    AND SPORTS_MLB_SCHEDULE_PROBABLES.game_date = tmp_probables_dates.game_date
    AND SPORTS_MLB_SCHEDULE_PROBABLES.player_id IS NOT NULL)
  JOIN tmp_probables_pitchers
    ON (tmp_probables_pitchers.player_id = SPORTS_MLB_SCHEDULE_PROBABLES.player_id
    AND tmp_probables_pitchers.catcher_id IS NOT NULL)
  SET SPORTS_MLB_SCHEDULE_PROBABLES.catcher_id = tmp_probables_pitchers.catcher_id,
      SPORTS_MLB_SCHEDULE_PROBABLES.catcher_reason = tmp_probables_pitchers.catcher_reason;

END $$

DELIMITER ;

#
# Identify catcher from the battery
#
DROP PROCEDURE IF EXISTS `mlb_probables_process_battery`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_probables_process_battery`()
    COMMENT 'Post-process the MLB probable pitcher info by battery'
BEGIN

  DECLARE v_season_lookback TINYINT UNSIGNED DEFAULT 2;
  DECLARE v_start_lookback TINYINT UNSIGNED DEFAULT 20;
  DECLARE v_start_overlap TINYINT UNSIGNED DEFAULT 13; # Must be at least half v_start_lookback

  DECLARE v_season SMALLINT UNSIGNED;
  SELECT MAX(season) INTO v_season FROM tmp_probables_dates;

  # Build their last _x_ many starts
  DROP TEMPORARY TABLE IF EXISTS tmp_probables_starts;
  CREATE TEMPORARY TABLE tmp_probables_starts (
    player_id SMALLINT UNSIGNED,
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    jersey TINYINT UNSIGNED,
    game_time_ref MEDIUMINT UNSIGNED,
    game_order SMALLINT UNSIGNED,
    PRIMARY KEY (player_id, season, game_type, game_id),
    KEY by_ref (player_id, game_time_ref)
  ) SELECT tmp_probables_pitchers.player_id,
           SPORTS_MLB_SCHEDULE.season,
           SPORTS_MLB_SCHEDULE.game_type,
           SPORTS_MLB_SCHEDULE.game_id,
           SPORTS_MLB_GAME_ROSTERS.team_id,
           SPORTS_MLB_GAME_ROSTERS.jersey,
           (100000 * (SPORTS_MLB_SCHEDULE.season % 100)) + SPORTS_MLB_SCHEDULE.game_time_ref AS game_time_ref,
           0 AS game_order
    FROM tmp_probables_pitchers
    JOIN SPORTS_MLB_GAME_ROSTERS
      ON (SPORTS_MLB_GAME_ROSTERS.season >= (v_season - v_season_lookback)
      AND SPORTS_MLB_GAME_ROSTERS.player_id = tmp_probables_pitchers.player_id
      AND SPORTS_MLB_GAME_ROSTERS.pos = 'SP')
    JOIN SPORTS_MLB_SCHEDULE
      ON (SPORTS_MLB_SCHEDULE.season = SPORTS_MLB_GAME_ROSTERS.season
      AND SPORTS_MLB_SCHEDULE.game_type = SPORTS_MLB_GAME_ROSTERS.game_type
      AND SPORTS_MLB_SCHEDULE.game_id = SPORTS_MLB_GAME_ROSTERS.game_id
      AND SPORTS_MLB_SCHEDULE.game_date < tmp_probables_pitchers.game_date);

  # Restrict to just those games we want to analyse
  CALL _duplicate_tmp_table('tmp_probables_starts', 'tmp_probables_starts_cpA');
  CALL _duplicate_tmp_table('tmp_probables_starts', 'tmp_probables_starts_cpB');
  INSERT INTO tmp_probables_starts (player_id, season, game_type, game_id, game_time_ref, game_order)
    SELECT tmp_probables_starts_cpA.player_id,
           tmp_probables_starts_cpA.season, tmp_probables_starts_cpA.game_type, tmp_probables_starts_cpA.game_id,
           tmp_probables_starts_cpA.game_time_ref,
           COUNT(tmp_probables_starts_cpB.game_time_ref) + 1 AS game_order
    FROM tmp_probables_starts_cpA
    LEFT JOIN tmp_probables_starts_cpB
      ON (tmp_probables_starts_cpB.player_id = tmp_probables_starts_cpA.player_id
      AND tmp_probables_starts_cpB.game_time_ref > tmp_probables_starts_cpA.game_time_ref)
    GROUP BY tmp_probables_starts_cpA.player_id, tmp_probables_starts_cpA.game_time_ref
  ON DUPLICATE KEY UPDATE game_order = VALUES(game_order);

  DELETE FROM tmp_probables_starts WHERE game_order > v_start_lookback;

  # Who was the catcher in those games?
  ALTER TABLE tmp_probables_starts
    ADD COLUMN catcher_id SMALLINT UNSIGNED;
  UPDATE tmp_probables_starts
  JOIN SPORTS_MLB_GAME_ROSTERS
    ON (SPORTS_MLB_GAME_ROSTERS.season = tmp_probables_starts.season
    AND SPORTS_MLB_GAME_ROSTERS.game_type = tmp_probables_starts.game_type
    AND SPORTS_MLB_GAME_ROSTERS.game_id = tmp_probables_starts.game_id
    AND SPORTS_MLB_GAME_ROSTERS.team_id = tmp_probables_starts.team_id
    AND SPORTS_MLB_GAME_ROSTERS.pos = 'C')
  SET tmp_probables_starts.catcher_id = SPORTS_MLB_GAME_ROSTERS.player_id;

  # Group these by pitcher
  DROP TEMPORARY TABLE IF EXISTS tmp_pitchers_catcher;
  CREATE TEMPORARY TABLE tmp_pitchers_catcher (
    player_id SMALLINT UNSIGNED,
    catcher_id SMALLINT UNSIGNED,
    catcher_team_id CHAR(3),
    num_starts TINYINT UNSIGNED,
    PRIMARY KEY (player_id, catcher_id)
  ) SELECT player_id, catcher_id, NULL AS catcher_team_id, COUNT(*) AS num_starts
    FROM tmp_probables_starts
    WHERE catcher_id IS NOT NULL
    GROUP BY player_id, catcher_id;

  # Determine if they are still on the same team or not
  UPDATE tmp_pitchers_catcher
  JOIN tmp_probables_rosters
    ON (tmp_probables_rosters.player_id = tmp_pitchers_catcher.catcher_id)
  SET tmp_pitchers_catcher.catcher_team_id = tmp_probables_rosters.team_id;

  # And write back where we meet our threshold
  UPDATE tmp_probables_pitchers
  JOIN tmp_pitchers_catcher
    ON (tmp_pitchers_catcher.player_id = tmp_probables_pitchers.player_id
    AND tmp_pitchers_catcher.catcher_team_id = tmp_probables_pitchers.team_id
    AND tmp_pitchers_catcher.num_starts >= v_start_overlap)
  SET tmp_probables_pitchers.catcher_id = tmp_pitchers_catcher.catcher_id,
      tmp_probables_pitchers.catcher_reason = 'usual-catcher'
  WHERE tmp_probables_pitchers.catcher_id IS NULL;

END $$

DELIMITER ;

#
# Identify catcher from recent starts
#
DROP PROCEDURE IF EXISTS `mlb_probables_process_recency`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_probables_process_recency`()
    COMMENT 'Post-process the MLB probable pitcher info by rececnt starts'
BEGIN

  DECLARE v_season_lookback TINYINT UNSIGNED DEFAULT 1;
  DECLARE v_start_lookback TINYINT UNSIGNED DEFAULT 20;
  DECLARE v_start_overlap TINYINT UNSIGNED DEFAULT 13; # Must be at least half v_start_lookback

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_date DATE;
  SELECT season, MIN(game_date) INTO v_season, v_date FROM tmp_probables_dates;

  # Game list
  DROP TEMPORARY TABLE IF EXISTS tmp_probables_schedule;
  CREATE TEMPORARY TABLE tmp_probables_schedule (
    team_id CHAR(3),
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    game_time_ref MEDIUMINT UNSIGNED,
    game_order SMALLINT UNSIGNED,
   PRIMARY KEY (team_id, season, game_type, game_id),
    KEY by_ref (team_id, game_time_ref)
  ) (
      SELECT home AS team_id,
             season, game_type, game_id,
             (100000 * (season % 100)) + game_time_ref AS game_time_ref,
             NULL AS game_order
      FROM SPORTS_MLB_SCHEDULE
      WHERE season >= v_season - v_season_lookback
      AND   status = 'F'
    ) UNION (
      SELECT visitor AS team_id,
             season, game_type, game_id,
             (100000 * (season % 100)) + game_time_ref AS game_time_ref,
             NULL AS game_order
      FROM SPORTS_MLB_SCHEDULE
      WHERE season >= v_season - v_season_lookback
      AND   status = 'F'
    );

  # Restrict to just those games we want to analyse
  CALL _duplicate_tmp_table('tmp_probables_schedule', 'tmp_probables_schedule_cpA');
  CALL _duplicate_tmp_table('tmp_probables_schedule', 'tmp_probables_schedule_cpB');
  INSERT INTO tmp_probables_schedule (team_id, season, game_type, game_id, game_time_ref, game_order)
    SELECT tmp_probables_schedule_cpA.team_id,
           tmp_probables_schedule_cpA.season, tmp_probables_schedule_cpA.game_type, tmp_probables_schedule_cpA.game_id,
           tmp_probables_schedule_cpA.game_time_ref,
           COUNT(tmp_probables_schedule_cpB.game_time_ref) + 1 AS game_order
    FROM tmp_probables_schedule_cpA
    LEFT JOIN tmp_probables_schedule_cpB
      ON (tmp_probables_schedule_cpB.team_id = tmp_probables_schedule_cpA.team_id
      AND tmp_probables_schedule_cpB.game_time_ref > tmp_probables_schedule_cpA.game_time_ref)
    GROUP BY tmp_probables_schedule_cpA.team_id, tmp_probables_schedule_cpA.game_time_ref
  ON DUPLICATE KEY UPDATE game_order = VALUES(game_order);

  DELETE FROM tmp_probables_schedule WHERE game_order > v_start_lookback;

  # Who was the catcher in those games?
  ALTER TABLE tmp_probables_schedule
    ADD COLUMN catcher_id SMALLINT UNSIGNED;
  UPDATE tmp_probables_schedule
  JOIN SPORTS_MLB_GAME_ROSTERS
    ON (SPORTS_MLB_GAME_ROSTERS.season = tmp_probables_schedule.season
    AND SPORTS_MLB_GAME_ROSTERS.game_type = tmp_probables_schedule.game_type
    AND SPORTS_MLB_GAME_ROSTERS.game_id = tmp_probables_schedule.game_id
    AND SPORTS_MLB_GAME_ROSTERS.team_id = tmp_probables_schedule.team_id
    AND SPORTS_MLB_GAME_ROSTERS.pos = 'C')
  SET tmp_probables_schedule.catcher_id = SPORTS_MLB_GAME_ROSTERS.player_id;

  # Group these by pitcher
  DROP TEMPORARY TABLE IF EXISTS tmp_pitchers_catcher;
  CREATE TEMPORARY TABLE tmp_pitchers_catcher (
    team_id CHAR(3), # Team started for
    catcher_id SMALLINT UNSIGNED,
    catcher_team_id CHAR(3), # Current team
    num_starts TINYINT UNSIGNED,
    PRIMARY KEY (team_id, catcher_id)
  ) SELECT team_id, catcher_id, NULL AS catcher_team_id, COUNT(*) AS num_starts
    FROM tmp_probables_schedule
    WHERE catcher_id IS NOT NULL
    GROUP BY team_id, catcher_id;

  # Determine if they are still on the same team or not
  UPDATE tmp_pitchers_catcher
  JOIN tmp_probables_rosters
    ON (tmp_probables_rosters.player_id = tmp_pitchers_catcher.catcher_id)
  SET tmp_pitchers_catcher.catcher_team_id = tmp_probables_rosters.team_id;

  # And write back where we meet our threshold
  UPDATE tmp_probables_pitchers
  JOIN tmp_pitchers_catcher
    ON (tmp_pitchers_catcher.team_id = tmp_pitchers_catcher.catcher_team_id # Still on the team
    AND tmp_pitchers_catcher.team_id = tmp_probables_pitchers.team_id # Link to probable pitcher
    AND tmp_pitchers_catcher.num_starts >= v_start_overlap)
  SET tmp_probables_pitchers.catcher_id = tmp_pitchers_catcher.catcher_id,
      tmp_probables_pitchers.catcher_reason = 'team-starter'
  WHERE tmp_probables_pitchers.catcher_id IS NULL;

END $$

DELIMITER ;

#
# Identify catcher from the team's depth chart
#
DROP PROCEDURE IF EXISTS `mlb_probables_process_depth`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_probables_process_depth`()
    COMMENT 'Post-process the MLB probable pitcher info by depth chart'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  SELECT DISTINCT season INTO v_season FROM tmp_probables_dates;

  UPDATE tmp_probables_pitchers
  JOIN SPORTS_MLB_TEAMS_DEPTH
    ON (SPORTS_MLB_TEAMS_DEPTH.season = v_season
    AND SPORTS_MLB_TEAMS_DEPTH.team_id = tmp_probables_pitchers.team_id
    AND SPORTS_MLB_TEAMS_DEPTH.pos = 'C'
    AND SPORTS_MLB_TEAMS_DEPTH.depth = 1)
  SET tmp_probables_pitchers.catcher_id = SPORTS_MLB_TEAMS_DEPTH.player_id,
      tmp_probables_pitchers.catcher_reason = 'depth-chart'
  WHERE tmp_probables_pitchers.catcher_id IS NULL;

END $$

DELIMITER ;
