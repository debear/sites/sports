##
## Copy in to team history
##
DROP PROCEDURE IF EXISTS `mlb_standings_history`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_history`()
    COMMENT 'Copy MLB team standings to the history table'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_the_date DATE;

  SELECT DISTINCT season INTO v_season
  FROM tmp_date_list;

  SELECT MAX(the_date) INTO v_the_date
  FROM SPORTS_MLB_STANDINGS
  WHERE season = v_season;

  INSERT INTO SPORTS_MLB_TEAMS_HISTORY_REGULAR (season, season_half, team_id, wins, loss, pct, games_back, pos)
    SELECT STANDINGS.season, 1 AS season_half, STANDINGS.team_id,
           STANDINGS.wins, STANDINGS.loss, STANDINGS.win_pct AS pct, STANDINGS.games_back_div,
           STANDINGS.pos_div AS pos
    FROM SPORTS_MLB_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_MLB_STANDINGS AS STANDINGS
      ON (STANDINGS.season = v_season
      AND STANDINGS.the_date = v_the_date
      AND STANDINGS.team_id = TEAM_DIV.team_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099)
  ON DUPLICATE KEY UPDATE wins = VALUES(wins),
                          loss = VALUES(loss),
                          pct = VALUES(pct),
                          games_back = VALUES(games_back),
                          pos = VALUES(pos);

  ALTER TABLE SPORTS_MLB_TEAMS_HISTORY_REGULAR ORDER BY team_id, season, season_half;

END $$

DELIMITER ;
