##
## Copy in to team history
##
DROP PROCEDURE IF EXISTS `mlb_standings_games_back`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_standings_games_back`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Determine how far teams are behind div leaders / wild card places'
BEGIN

  DECLARE v_num_divs TINYINT UNSIGNED;
  DECLARE v_num_div_berths TINYINT UNSIGNED;
  DECLARE v_num_div_teams TINYINT UNSIGNED;
  DECLARE v_num_wildcard TINYINT UNSIGNED;

  # Define the divison link so we can use our temporary table's key
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = _tb_div_id, _tb_pos = pos_div;'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cp');

  # These counts are to handle the minor permutations:
  # - Up until 2011 there was only a single wildcard; from 2012 onwards there were two wildcard berths
  # - In 2020, the top 2 in each division qualified, along with the next two wildcards
  SET v_num_divs = mlb_standings_num_divs(v_season);
  SET v_num_div_berths = mlb_standings_num_div_berths(v_season);
  SET v_num_div_teams = (v_num_divs * v_num_div_berths);
  SET v_num_wildcard = mlb_standings_num_wildcards(v_season);

  # Division Leaders
  UPDATE tmp_standings
  JOIN tmp_standings_cp
    ON (tmp_standings_cp.season = tmp_standings.season
    AND tmp_standings_cp.the_date = tmp_standings.the_date
    AND tmp_standings_cp._tb_join = tmp_standings._tb_join
    AND tmp_standings_cp._tb_pos = v_num_div_berths)
  SET tmp_standings.games_back_div = ((CAST(tmp_standings_cp.wins AS SIGNED) - CAST(tmp_standings.wins AS SIGNED)) + (CAST(tmp_standings.loss AS SIGNED) - CAST(tmp_standings_cp.loss AS SIGNED))) / 2
  WHERE tmp_standings.pos_div <> v_num_div_berths;

  # Wildcard
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = _tb_conf_id, _tb_pos = pos_conf;'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cp');

  UPDATE tmp_standings
  JOIN tmp_standings_cp
    ON (tmp_standings_cp.season = tmp_standings.season
    AND tmp_standings_cp.the_date = tmp_standings.the_date
    AND tmp_standings_cp._tb_join = tmp_standings._tb_join
    AND tmp_standings_cp._tb_pos = (v_num_div_teams + v_num_wildcard))
  SET tmp_standings.games_back_wildcard = ((CAST(tmp_standings_cp.wins AS SIGNED) - CAST(tmp_standings.wins AS SIGNED)) + (CAST(tmp_standings.loss AS SIGNED) - CAST(tmp_standings_cp.loss AS SIGNED))) / 2
  WHERE tmp_standings._tb_pos > v_num_div_teams -- Do not calculate for division spots...
  AND   tmp_standings._tb_pos <> (v_num_div_teams + v_num_wildcard);

END $$

DELIMITER ;
