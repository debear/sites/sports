#
# Implied strikezone tendencies by umpire
#
DROP PROCEDURE IF EXISTS `mlb_totals_umpire_zones`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_umpire_zones`()
    COMMENT 'Bubble up MLB pitch zone calls per umpire'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_game_type ENUM('regular', 'playoff');
  SELECT season, game_type
    INTO v_season, v_game_type
  FROM tmp_game_list
  LIMIT 1;

  # Determine the list of umpires affected
  DROP TEMPORARY TABLE IF EXISTS tmp_umpires;
  CREATE TEMPORARY TABLE tmp_umpires (
    umpire_id SMALLINT UNSIGNED,
    PRIMARY KEY (umpire_id)
  ) ENGINE = MEMORY
    SELECT DISTINCT SPORTS_MLB_GAME_UMPIRES.umpire_id
    FROM tmp_game_list
    JOIN SPORTS_MLB_GAME_UMPIRES
      ON (SPORTS_MLB_GAME_UMPIRES.season = tmp_game_list.season
      AND SPORTS_MLB_GAME_UMPIRES.game_type = tmp_game_list.game_type
      AND SPORTS_MLB_GAME_UMPIRES.game_id = tmp_game_list.game_id
      AND SPORTS_MLB_GAME_UMPIRES.umpire_pos = 'HP');

  # The games they called
  DROP TEMPORARY TABLE IF EXISTS tmp_umpires_games;
  CREATE TEMPORARY TABLE tmp_umpires_games (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    umpire_id SMALLINT UNSIGNED,
    PRIMARY KEY (season, game_type, game_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_MLB_GAME_UMPIRES.season, SPORTS_MLB_GAME_UMPIRES.game_type,
           SPORTS_MLB_GAME_UMPIRES.game_id, SPORTS_MLB_GAME_UMPIRES.umpire_id
    FROM tmp_umpires
    JOIN SPORTS_MLB_GAME_UMPIRES
      ON (SPORTS_MLB_GAME_UMPIRES.season = v_season
      AND SPORTS_MLB_GAME_UMPIRES.game_type = v_game_type
      AND SPORTS_MLB_GAME_UMPIRES.umpire_id = tmp_umpires.umpire_id
      AND SPORTS_MLB_GAME_UMPIRES.umpire_pos = 'HP');

  # Shortcut the ATBAT_FLAGS table
  DROP TEMPORARY TABLE IF EXISTS tmp_game_atbat_pitches;
  CREATE TEMPORARY TABLE tmp_game_atbat_pitches LIKE SPORTS_MLB_GAME_ATBAT_PITCHES;
  ALTER TABLE tmp_game_atbat_pitches
    ADD COLUMN umpire_id SMALLINT UNSIGNED,
    ADD COLUMN batter_hand ENUM('L', 'R'),
    ADD COLUMN pitcher_hand ENUM('L', 'R'),
    ADD KEY by_umpire (season, game_type, umpire_id, batter_hand, pitcher_hand) USING BTREE,
    ADD KEY has_zone (zone) USING BTREE,
    ADD KEY by_result (result) USING BTREE,
    ADD KEY by_type_loc (`call`, pitch_x, pitch_y) USING BTREE;

  DROP TEMPORARY TABLE IF EXISTS tmp_game_pitches;
  CREATE TEMPORARY TABLE tmp_game_pitches LIKE SPORTS_MLB_GAME_ATBAT_PITCHES;
  ALTER TABLE tmp_game_pitches ADD COLUMN umpire_id SMALLINT UNSIGNED;
  INSERT INTO tmp_game_pitches
    SELECT SPORTS_MLB_GAME_ATBAT_PITCHES.*, tmp_umpires_games.umpire_id
    FROM tmp_umpires_games
    JOIN SPORTS_MLB_GAME_ATBAT_PITCHES
      ON (SPORTS_MLB_GAME_ATBAT_PITCHES.season = tmp_umpires_games.season
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.game_type = tmp_umpires_games.game_type
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.game_id = tmp_umpires_games.game_id
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.`call` IN ('B', 'S')
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.result IN ('Ball','Ball In Dirt','Called Strike')
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.zone IS NOT NULL);

  DROP TEMPORARY TABLE IF EXISTS tmp_game_pitches_flags;
  CREATE TEMPORARY TABLE tmp_game_pitches_flags LIKE SPORTS_MLB_GAME_ATBAT_FLAGS;
  INSERT INTO tmp_game_pitches_flags
    SELECT SPORTS_MLB_GAME_ATBAT_FLAGS.*
    FROM tmp_umpires_games
    JOIN SPORTS_MLB_GAME_ATBAT_FLAGS
      ON (SPORTS_MLB_GAME_ATBAT_FLAGS.season = tmp_umpires_games.season
      AND SPORTS_MLB_GAME_ATBAT_FLAGS.game_type = tmp_umpires_games.game_type
      AND SPORTS_MLB_GAME_ATBAT_FLAGS.game_id = tmp_umpires_games.game_id);

  # Merged version
  INSERT INTO tmp_game_atbat_pitches
    SELECT tmp_game_pitches.*,
           tmp_game_pitches_flags.batter_hand,
           tmp_game_pitches_flags.pitcher_hand
    FROM tmp_game_pitches
    JOIN tmp_game_pitches_flags
      ON (tmp_game_pitches_flags.season = tmp_game_pitches.season
      AND tmp_game_pitches_flags.game_type = tmp_game_pitches.game_type
      AND tmp_game_pitches_flags.game_id = tmp_game_pitches.game_id
      AND tmp_game_pitches_flags.play_id = tmp_game_pitches.play_id);
  ALTER TABLE tmp_game_atbat_pitches
    ADD COLUMN offset_top DECIMAL(5, 3) DEFAULT 0.000,
    ADD COLUMN offset_right DECIMAL(5, 3) DEFAULT 0.000,
    ADD COLUMN offset_bottom DECIMAL(5, 3) DEFAULT 0.000,
    ADD COLUMN offset_left DECIMAL(5, 3) DEFAULT 0.000;

  # Top: Low Ball? (Call: B; pitch_y <= 1.000; Negative Score: Contracted Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_top = pitch_y - 1.001
  WHERE `call` = 'B'
  AND   pitch_x BETWEEN -1.000 AND 1.000
  AND   pitch_y BETWEEN 0.500 AND 1.000;
  # Top: High Strike? (Call: S; pitch_y > 1.000; Positive Score: Expanded Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_top = pitch_y - 1.000
  WHERE `call` = 'S'
  AND   pitch_x BETWEEN -1.000 AND 1.000
  AND   pitch_y > 1.000;

  # Right: Inside Ball? (Call: B; pitch_x <= 1.000; Negative Score: Contracted Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_right = pitch_x - 1.001
  WHERE `call` = 'B'
  AND   pitch_x BETWEEN 0.000 AND 1.000
  AND   pitch_y BETWEEN 0.000 AND 1.000;
  # Right: Wide Strike? (Call: S; pitch_x > 1.000; Positive Score: Expanded Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_right = pitch_x - 1.000
  WHERE `call` = 'S'
  AND   pitch_x > 1.000
  AND   pitch_y BETWEEN 0.000 AND 1.000;

  # Bottom: High Ball? (Call: B; pitch_y >= 0.000; Negative Score: Contracted Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_bottom = 0 - pitch_y
  WHERE `call` = 'B'
  AND   pitch_x BETWEEN -1.000 AND 1.000
  AND   pitch_y BETWEEN 0.000 AND 0.499;
  # Bottom: Low Strike? (Call: S; pitch_y < 0.000; Positive Score: Expanded Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_bottom = ABS(pitch_y)
  WHERE `call` = 'S'
  AND   pitch_x BETWEEN -1.000 AND 1.000
  AND   pitch_y < 0.000;

  # Left: Inside Ball? (Call: B; pitch_x >= -1.000; Negative Score: Contracted Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_left = ABS(pitch_x) - 1.001
  WHERE `call` = 'B'
  AND   pitch_x BETWEEN -1.000 AND -0.500
  AND   pitch_y BETWEEN 0.000 AND 1.000;
  # Left: Wide Strike? (Call: S; pitch_x < -1.000; Positive Score: Expanded Zone)
  UPDATE tmp_game_atbat_pitches
  SET offset_left = ABS(pitch_x) - 1.000
  WHERE `call` = 'S'
  AND   pitch_x < -1.000
  AND   pitch_y BETWEEN 0.000 AND 1.000;

  #
  # Build the various aggregates
  #
  DROP TEMPORARY TABLE IF EXISTS tmp_umpires_zones;
  CREATE TEMPORARY TABLE tmp_umpires_zones (
    season SMALLINT UNSIGNED NULL,
    game_type ENUM('regular', 'playoff') NULL,
    umpire_id SMALLINT UNSIGNED NULL,
    batter_hand ENUM('L', 'R', 'CMB') NULL,
    pitcher_hand ENUM('L', 'R', 'CMB') NULL,
    num_pitches INT UNSIGNED DEFAULT 0,
    offset_top DECIMAL(5, 3) DEFAULT 0.000,
    offset_right DECIMAL(5, 3) DEFAULT 0.000,
    offset_bottom DECIMAL(5, 3) DEFAULT 0.000,
    offset_left DECIMAL(5, 3) DEFAULT 0.000,
    UNIQUE faux_primary (season, game_type, umpire_id, batter_hand, pitcher_hand) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, umpire_id, batter_hand, pitcher_hand,
           COUNT(*) AS num_pitches,
           AVG(offset_top) AS offset_top,
           AVG(offset_right) AS offset_right,
           AVG(offset_bottom) AS offset_bottom,
           AVG(offset_left) AS offset_left
    FROM tmp_game_atbat_pitches
    GROUP BY season, game_type, umpire_id, batter_hand, pitcher_hand
    WITH ROLLUP;
  DELETE FROM tmp_umpires_zones WHERE umpire_id IS NULL;
  UPDATE tmp_umpires_zones SET batter_hand = 'CMB' WHERE batter_hand IS NULL;
  UPDATE tmp_umpires_zones SET pitcher_hand = 'CMB' WHERE pitcher_hand IS NULL;

  # Prune "duplicated" pitcher_hand rollup rows (e.g., only R/R, which is also aggregated to R/CMB)
  DROP TEMPORARY TABLE IF EXISTS tmp_umpires_zones_dupe;
  CREATE TEMPORARY TABLE tmp_umpires_zones_dupe (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    umpire_id SMALLINT UNSIGNED,
    batter_hand ENUM('L', 'R'),
    num_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, umpire_id, batter_hand),
    KEY by_num (num_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, umpire_id, batter_hand, COUNT(DISTINCT pitcher_hand) AS num_hands
    FROM tmp_umpires_zones
    WHERE batter_hand <> 'CMB'
    AND   pitcher_hand <> 'CMB'
    GROUP BY season, game_type, umpire_id, batter_hand;

  DELETE STAT.*
  FROM tmp_umpires_zones_dupe AS DUPE
  JOIN tmp_umpires_zones AS STAT
    ON (STAT.season = DUPE.season
    AND STAT.game_type = DUPE.game_type
    AND STAT.umpire_id = DUPE.umpire_id
    AND STAT.batter_hand = DUPE.batter_hand
    AND STAT.pitcher_hand = 'CMB')
  WHERE DUPE.num_hands = 1;

  # And then the same for batter_hand
  DROP TEMPORARY TABLE IF EXISTS tmp_umpires_zones_dupe;
  CREATE TEMPORARY TABLE tmp_umpires_zones_dupe (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    umpire_id SMALLINT UNSIGNED,
    num_hands TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, umpire_id),
    KEY by_num (num_hands) USING BTREE
  ) ENGINE = MEMORY
    SELECT season, game_type, umpire_id, COUNT(DISTINCT batter_hand) AS num_hands
    FROM tmp_umpires_zones
    WHERE batter_hand <> 'CMB'
    AND   pitcher_hand <> 'CMB'
    GROUP BY season, game_type, umpire_id;

  DELETE STAT.*
  FROM tmp_umpires_zones_dupe AS DUPE
  JOIN tmp_umpires_zones AS STAT
    ON (STAT.season = DUPE.season
    AND STAT.game_type = DUPE.game_type
    AND STAT.umpire_id = DUPE.umpire_id
    AND STAT.batter_hand = 'CMB')
  WHERE DUPE.num_hands = 1;

  # Then add to the main table
  INSERT INTO SPORTS_MLB_UMPIRES_SEASON_ZONES
    SELECT season, game_type AS season_type, umpire_id, batter_hand, pitcher_hand,
           num_pitches, offset_top, offset_right, offset_bottom, offset_left
    FROM tmp_umpires_zones
  ON DUPLICATE KEY UPDATE num_pitches = VALUES(num_pitches),
                          offset_top = VALUES(offset_top),
                          offset_right = VALUES(offset_right),
                          offset_bottom = VALUES(offset_bottom),
                          offset_left = VALUES(offset_left);

  # Table management
  ALTER TABLE SPORTS_MLB_UMPIRES_SEASON_ZONES
    ORDER BY season, season_type, umpire_id, batter_hand, pitcher_hand;

END $$

DELIMITER ;
