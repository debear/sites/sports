##
## Playoff series
##
DROP PROCEDURE IF EXISTS `mlb_playoff_series`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_series`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate MLB playoff series'
BEGIN

  # Declare vars
  DECLARE v_start_date DATE;
  DECLARE v_end_date DATE;

  # Default first round (wildcard game started in 2012)
  DECLARE v_round_first TINYINT UNSIGNED DEFAULT 1;
  IF v_season <= 2011 THEN
    SET v_round_first = 2;
  END IF;

  # Appropriate dates for playoff summaries
  SELECT MIN(SPORTS_MLB_SCHEDULE.game_date),
         MAX(SPORTS_MLB_SCHEDULE.game_date)
           INTO v_start_date, v_end_date
  FROM tmp_game_list
  JOIN SPORTS_MLB_SCHEDULE
    ON (SPORTS_MLB_SCHEDULE.season = tmp_game_list.season
    AND SPORTS_MLB_SCHEDULE.game_type = tmp_game_list.game_type
    AND SPORTS_MLB_SCHEDULE.game_id = tmp_game_list.game_id)
  WHERE tmp_game_list.season = v_season;

  # Get the summary dates of each round ("period start", first game, last game)
  DROP TEMPORARY TABLE IF EXISTS tmp_round_status;
  CREATE TEMPORARY TABLE tmp_round_status (
    season SMALLINT UNSIGNED,
    round_num TINYINT(1) UNSIGNED,
    round_start DATE,
    first_game DATE,
    last_game DATE,
    PRIMARY KEY (season, round_num)
  ) ENGINE = MEMORY
    SELECT season,
           SUBSTRING(game_id, 1, 1) AS round_num,
           NULL AS round_start,
           MIN(game_date) AS first_game,
           MAX(game_date) AS last_game
    FROM SPORTS_MLB_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'playoff'
    AND   status = 'F'
    GROUP BY round_num;

  # Calculate the period start: day after completion of the last round
  CALL _duplicate_tmp_table('tmp_round_status', 'tmp_round_status_cp1');
  CALL _duplicate_tmp_table('tmp_round_status', 'tmp_round_status_cp2');

  INSERT INTO tmp_round_status (season, round_num, round_start)
    SELECT tmp_round_status_cp1.season,
           tmp_round_status_cp1.round_num,
           DATE_ADD(tmp_round_status_cp2.last_game, INTERVAL 1 DAY) AS round_start
    FROM tmp_round_status_cp1
    JOIN tmp_round_status_cp2
      ON (tmp_round_status_cp2.round_num = tmp_round_status_cp1.round_num - 1)
    GROUP BY tmp_round_status_cp1.round_num
  ON DUPLICATE KEY UPDATE round_start = VALUES(round_start);

  # Remove the duplicate tables we no longer need
  DROP TEMPORARY TABLE IF EXISTS tmp_round_status_cp1;
  DROP TEMPORARY TABLE IF EXISTS tmp_round_status_cp2;

  # Start of the first round is day after last game of regular season
  INSERT INTO tmp_round_status (season, round_num, round_start)
    SELECT season,
           v_round_first AS round_num,
           DATE_ADD(MAX(SPORTS_MLB_SCHEDULE.game_date), INTERVAL 1 DAY) AS round_start
    FROM SPORTS_MLB_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
  ON DUPLICATE KEY UPDATE round_start = VALUES(round_start);

  # Populate series dates
  CALL mlb_sequential_date_range(v_season, v_start_date, v_end_date);

  # Clear previous run
  DELETE SPORTS_MLB_PLAYOFF_SERIES.*
  FROM tmp_date_range
  JOIN SPORTS_MLB_PLAYOFF_SERIES
    ON (tmp_date_range.season = SPORTS_MLB_PLAYOFF_SERIES.season
    AND tmp_date_range.the_date = SPORTS_MLB_PLAYOFF_SERIES.the_date);

  # And now calculate the series positions
  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, complete)
    SELECT tmp_date_range.season,
           tmp_date_range.the_date,
            CAST(SUBSTRING(SPORTS_MLB_SCHEDULE.game_id, 1, 2) AS CHAR(2)) AS round_code,
            IF((HOME_SEED.conf_id = AWAY_SEED.conf_id AND HOME_SEED.pos_conf < AWAY_SEED.pos_conf)
               OR (HOME_SEED.conf_id != AWAY_SEED.conf_id AND HOME_SEED.pos_league < AWAY_SEED.pos_league),
                HOME_SEED.conf_id, AWAY_SEED.conf_id) AS higher_conf_id,
            IF((HOME_SEED.conf_id = AWAY_SEED.conf_id AND HOME_SEED.pos_conf < AWAY_SEED.pos_conf)
               OR (HOME_SEED.conf_id != AWAY_SEED.conf_id AND HOME_SEED.pos_league < AWAY_SEED.pos_league),
                HOME_SEED.seed, AWAY_SEED.seed) AS higher_seed,
            IF((HOME_SEED.conf_id = AWAY_SEED.conf_id AND HOME_SEED.pos_conf > AWAY_SEED.pos_conf)
               OR (HOME_SEED.conf_id != AWAY_SEED.conf_id AND HOME_SEED.pos_league > AWAY_SEED.pos_league),
                HOME_SEED.conf_id, AWAY_SEED.conf_id) AS lower_conf_id,
            IF((HOME_SEED.conf_id = AWAY_SEED.conf_id AND HOME_SEED.pos_conf > AWAY_SEED.pos_conf)
               OR (HOME_SEED.conf_id != AWAY_SEED.conf_id AND HOME_SEED.pos_league > AWAY_SEED.pos_league),
                HOME_SEED.seed, AWAY_SEED.seed) AS lower_seed,
            0 AS complete
    FROM tmp_date_range
    JOIN tmp_round_status
      ON (tmp_date_range.season = tmp_round_status.season
      AND tmp_date_range.the_date BETWEEN tmp_round_status.round_start
                                          AND tmp_round_status.last_game)
    JOIN SPORTS_MLB_SCHEDULE
      ON (SPORTS_MLB_SCHEDULE.season = tmp_round_status.season
      AND SPORTS_MLB_SCHEDULE.game_type = 'playoff'
      AND SUBSTRING(SPORTS_MLB_SCHEDULE.game_id, 1, 1) = tmp_round_status.round_num)
    JOIN SPORTS_MLB_TEAMS AS HOME_TEAM
      ON (HOME_TEAM.team_id = SPORTS_MLB_SCHEDULE.home)
    JOIN SPORTS_MLB_TEAMS_GROUPINGS AS HOME_DIV
      ON (HOME_DIV.team_id = HOME_TEAM.team_id
      AND tmp_date_range.season BETWEEN HOME_DIV.season_from AND IFNULL(HOME_DIV.season_to, 2099))
    JOIN SPORTS_MLB_GROUPINGS AS HOME_DIV_INFO
      ON (HOME_DIV_INFO.grouping_id = HOME_DIV.grouping_id)
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS HOME_SEED
      ON (HOME_SEED.season = tmp_date_range.season
      AND HOME_SEED.conf_id = HOME_DIV_INFO.parent_id
      AND HOME_SEED.team_id = HOME_TEAM.team_id)
    JOIN SPORTS_MLB_TEAMS AS AWAY_TEAM
      ON (AWAY_TEAM.team_id = SPORTS_MLB_SCHEDULE.visitor)
    JOIN SPORTS_MLB_TEAMS_GROUPINGS AS AWAY_DIV
      ON (AWAY_DIV.team_id = AWAY_TEAM.team_id
      AND tmp_date_range.season BETWEEN AWAY_DIV.season_from AND IFNULL(AWAY_DIV.season_to, 2099))
    JOIN SPORTS_MLB_GROUPINGS AS AWAY_DIV_INFO
      ON (AWAY_DIV_INFO.grouping_id = AWAY_DIV.grouping_id)
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS AWAY_SEED
      ON (AWAY_SEED.season = tmp_date_range.season
      AND AWAY_SEED.conf_id = AWAY_DIV_INFO.parent_id
      AND AWAY_SEED.team_id = AWAY_TEAM.team_id)
    GROUP BY tmp_date_range.season,
             tmp_date_range.the_date,
             SUBSTRING(SPORTS_MLB_SCHEDULE.game_id, 1, 2);

  # Pre-calc the schedule and results
  DROP TEMPORARY TABLE IF EXISTS tmp_sched;
  CREATE TEMPORARY TABLE tmp_sched (
    team_id VARCHAR(3),
    round_code CHAR(2),
    game_id SMALLINT UNSIGNED,
    game_date DATE,
    venue ENUM('home', 'visitor'),
    result ENUM('w', 'l'),
    goals_for SMALLINT UNSIGNED,
    goals_against SMALLINT UNSIGNED,
    PRIMARY KEY (team_id, game_id),
    INDEX (team_id, round_code, game_date)
  ) ENGINE = MEMORY
    SELECT SPORTS_MLB_PLAYOFF_SEEDS.team_id,
           CAST(SUBSTRING(SPORTS_MLB_SCHEDULE.game_id, 1, 2) AS CHAR(2)) AS round_code,
           SPORTS_MLB_SCHEDULE.game_id,
           SPORTS_MLB_SCHEDULE.game_date,
           IF(SPORTS_MLB_PLAYOFF_SEEDS.team_id = SPORTS_MLB_SCHEDULE.home, 'home', 'visitor') AS venue,
           IF((SPORTS_MLB_PLAYOFF_SEEDS.team_id = SPORTS_MLB_SCHEDULE.home AND SPORTS_MLB_SCHEDULE.home_score > SPORTS_MLB_SCHEDULE.visitor_score)
              OR (SPORTS_MLB_PLAYOFF_SEEDS.team_id = SPORTS_MLB_SCHEDULE.visitor AND SPORTS_MLB_SCHEDULE.home_score < SPORTS_MLB_SCHEDULE.visitor_score),
              'w', 'l') AS result,
           IF(SPORTS_MLB_PLAYOFF_SEEDS.team_id = SPORTS_MLB_SCHEDULE.home, SPORTS_MLB_SCHEDULE.home_score, SPORTS_MLB_SCHEDULE.visitor_score) AS goals_for,
           IF(SPORTS_MLB_PLAYOFF_SEEDS.team_id = SPORTS_MLB_SCHEDULE.home, SPORTS_MLB_SCHEDULE.visitor_score, SPORTS_MLB_SCHEDULE.home_score) AS goals_against
    FROM SPORTS_MLB_PLAYOFF_SEEDS
    JOIN SPORTS_MLB_SCHEDULE
      ON (SPORTS_MLB_SCHEDULE.season = SPORTS_MLB_PLAYOFF_SEEDS.season
      AND SPORTS_MLB_SCHEDULE.game_type = 'playoff'
      AND (SPORTS_MLB_SCHEDULE.home = SPORTS_MLB_PLAYOFF_SEEDS.team_id
        OR SPORTS_MLB_SCHEDULE.visitor = SPORTS_MLB_PLAYOFF_SEEDS.team_id)
      AND SPORTS_MLB_SCHEDULE.status = 'F')
    WHERE SPORTS_MLB_PLAYOFF_SEEDS.season = v_season;

  # Now the game results
  INSERT INTO SPORTS_MLB_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, higher_games, lower_conf_id, lower_seed, lower_games, complete)
    SELECT SPORTS_MLB_PLAYOFF_SERIES.season,
           SPORTS_MLB_PLAYOFF_SERIES.the_date,
           SPORTS_MLB_PLAYOFF_SERIES.round_code,
           SPORTS_MLB_PLAYOFF_SERIES.higher_conf_id,
           SPORTS_MLB_PLAYOFF_SERIES.higher_seed,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS higher_games,
           SPORTS_MLB_PLAYOFF_SERIES.lower_conf_id,
           SPORTS_MLB_PLAYOFF_SERIES.lower_seed,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS lower_games,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) = mlb_playoff_round_wins_required(SPORTS_MLB_PLAYOFF_SERIES.season, SUBSTRING(SPORTS_MLB_PLAYOFF_SERIES.round_code, 1, 1))
             OR IFNULL(SUM(tmp_sched.result = 'l'), 0) = mlb_playoff_round_wins_required(SPORTS_MLB_PLAYOFF_SERIES.season, SUBSTRING(SPORTS_MLB_PLAYOFF_SERIES.round_code, 1, 1)) AS complete
    FROM SPORTS_MLB_PLAYOFF_SERIES
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS HIGHER_SEED
      ON (HIGHER_SEED.season = SPORTS_MLB_PLAYOFF_SERIES.season
      AND HIGHER_SEED.conf_id = SPORTS_MLB_PLAYOFF_SERIES.higher_conf_id
      AND HIGHER_SEED.seed = SPORTS_MLB_PLAYOFF_SERIES.higher_seed)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = HIGHER_SEED.team_id
      AND tmp_sched.round_code = SPORTS_MLB_PLAYOFF_SERIES.round_code
      AND tmp_sched.game_date <= SPORTS_MLB_PLAYOFF_SERIES.the_date)
    WHERE SPORTS_MLB_PLAYOFF_SERIES.season = v_season
    GROUP BY SPORTS_MLB_PLAYOFF_SERIES.season,
             SPORTS_MLB_PLAYOFF_SERIES.the_date,
             SPORTS_MLB_PLAYOFF_SERIES.round_code
  ON DUPLICATE KEY UPDATE higher_games = VALUES(higher_games),
                          lower_games = VALUES(lower_games),
                          complete = VALUES(complete);

  # Sort
  ALTER TABLE SPORTS_MLB_PLAYOFF_SERIES ORDER BY season, the_date, round_code;

  # Finally, delete any future scheduled games for this series if it was completed
  DROP TEMPORARY TABLE IF EXISTS tmp_clinched_series;
  CREATE TEMPORARY TABLE tmp_clinched_series (
    season SMALLINT UNSIGNED,
    round_code CHAR(2),
    clinch_date DATE,
    PRIMARY KEY (season, round_code)
  ) ENGINE = MEMORY
    SELECT season, round_code, MIN(the_date) AS clinch_date
    FROM SPORTS_MLB_PLAYOFF_SERIES
    WHERE season = v_season
    AND   complete = 1
    GROUP BY season, round_code;

  DELETE SPORTS_MLB_SCHEDULE.*
  FROM tmp_clinched_series
  JOIN SPORTS_MLB_SCHEDULE
    ON (SPORTS_MLB_SCHEDULE.season = tmp_clinched_series.season
    AND SPORTS_MLB_SCHEDULE.game_type = 'playoff'
    AND SUBSTRING(SPORTS_MLB_SCHEDULE.game_id, 1, 2) = tmp_clinched_series.round_code
    AND SPORTS_MLB_SCHEDULE.game_date > tmp_clinched_series.clinch_date);

  # Having changed the dates, we should re-calc date ranges
  CALL mlb_schedule_dates(v_season);

END $$

DELIMITER ;

#
# Determine the number of games in a playoff round
#
DROP FUNCTION IF EXISTS `mlb_playoff_round_wins_required`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `mlb_playoff_round_wins_required`(
  v_season SMALLINT UNSIGNED,
  v_round TINYINT UNSIGNED
) RETURNS TINYINT UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine the number of games required to win an MLB playoff round'
BEGIN

  # Wildcard Game (Round 1, until 2021 but not 2020) - one game playoff
  IF v_season < 2022 AND v_season <> 2020 AND v_round = 1 THEN
    RETURN 1;

  # Wildcard Series (2020, 2022 onwards) - best of three
  ELSEIF v_round = 1 THEN
    RETURN 2;

  # LDS - best of five
  ELSEIF v_round = 2 THEN
    RETURN 3;

  # LCS - best of seven
  # WS - best of seven
  ELSE
    RETURN 4;
  END IF;

END $$

DELIMITER ;
