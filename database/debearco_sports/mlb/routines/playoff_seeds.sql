##
## Playoff seeds
##
DROP PROCEDURE IF EXISTS `mlb_playoff_seeds`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_seeds`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate MLB playoff seeds'
BEGIN

  DECLARE v_standing_date DATE;
  DECLARE v_outstanding SMALLINT UNSIGNED;

  SELECT MAX(the_date) INTO v_standing_date FROM SPORTS_MLB_STANDINGS WHERE season = v_season;

  # Clear previous run
  DELETE FROM SPORTS_MLB_PLAYOFF_SEEDS WHERE season = v_season;

  # Determine if in-season or after the season was finished
  SELECT COUNT(*) INTO v_outstanding FROM SPORTS_MLB_SCHEDULE WHERE season = v_season AND game_type = 'regular' AND status IS NULL;

  # Calculate - in-season
  IF v_outstanding > 0 THEN
    # Calculate by hand
    IF v_season = 2020 THEN
      # 2020: Top 2 in each div, plus next two in conf
      CALL mlb_playoff_seeds_mix(v_season, v_standing_date);
    ELSEIF v_season >= 2022 THEN
      # From 2022: 3x Div Winners, 3x Wild Card
      CALL mlb_playoff_seeds_wc(v_season, v_standing_date, 3);
    ELSE
      # Up to 2021, excluding 2020: 3x Div Winners, 2x Wild Card
      CALL mlb_playoff_seeds_wc(v_season, v_standing_date, 2);
    END IF;

  # Calculate - after season
  ELSE
    INSERT INTO SPORTS_MLB_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
      SELECT SPORTS_MLB_STANDINGS.season,
             SPORTS_MLB_GROUPINGS.parent_id AS conf_id,
             SPORTS_MLB_STANDINGS.pos_conf AS seed,
             SPORTS_MLB_STANDINGS.team_id,
             SPORTS_MLB_STANDINGS.pos_div,
             SPORTS_MLB_STANDINGS.pos_conf,
             SPORTS_MLB_STANDINGS.pos_league
      FROM SPORTS_MLB_STANDINGS
      JOIN SPORTS_MLB_TEAMS_GROUPINGS
        ON (SPORTS_MLB_TEAMS_GROUPINGS.team_id = SPORTS_MLB_STANDINGS.team_id
        AND v_season BETWEEN SPORTS_MLB_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_MLB_TEAMS_GROUPINGS.season_to, 2099))
      JOIN SPORTS_MLB_GROUPINGS
        ON (SPORTS_MLB_GROUPINGS.grouping_id = SPORTS_MLB_TEAMS_GROUPINGS.grouping_id)
      WHERE SPORTS_MLB_STANDINGS.season = v_season
      AND   SPORTS_MLB_STANDINGS.the_date = v_standing_date
      AND   SPORTS_MLB_STANDINGS.status_code IS NOT NULL;
  END IF;

  # Final ordering
  ALTER TABLE SPORTS_MLB_PLAYOFF_SEEDS ORDER BY season, conf_id, seed;

END $$

DELIMITER ;

##
## In-season calcs using a set number of wild-card spots
##
DROP PROCEDURE IF EXISTS `mlb_playoff_seeds_wc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_seeds_wc`(
  v_season SMALLINT UNSIGNED,
  v_standing_date DATE,
  v_num_wc TINYINT UNSIGNED
)
    COMMENT 'Calculate MLB playoff seeds: passed number of wild-cards'
BEGIN

  INSERT INTO SPORTS_MLB_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
    SELECT SPORTS_MLB_STANDINGS.season,
           SPORTS_MLB_GROUPINGS.parent_id AS conf_id,
           SPORTS_MLB_STANDINGS.pos_conf AS seed,
           SPORTS_MLB_STANDINGS.team_id,
           SPORTS_MLB_STANDINGS.pos_div,
           SPORTS_MLB_STANDINGS.pos_conf,
           SPORTS_MLB_STANDINGS.pos_league
    FROM SPORTS_MLB_STANDINGS
    JOIN SPORTS_MLB_TEAMS_GROUPINGS
      ON (SPORTS_MLB_TEAMS_GROUPINGS.team_id = SPORTS_MLB_STANDINGS.team_id
      AND v_season BETWEEN SPORTS_MLB_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_MLB_TEAMS_GROUPINGS.season_to, 2099))
    JOIN SPORTS_MLB_GROUPINGS
      ON (SPORTS_MLB_GROUPINGS.grouping_id = SPORTS_MLB_TEAMS_GROUPINGS.grouping_id)
    WHERE SPORTS_MLB_STANDINGS.season = v_season
    AND   SPORTS_MLB_STANDINGS.the_date = v_standing_date
    AND   SPORTS_MLB_STANDINGS.pos_conf <= (3 + v_num_wc); # 3 = Num Div

END $$

DELIMITER ;

##
## In-season calcs using mix of div and conf positions
##
DROP PROCEDURE IF EXISTS `mlb_playoff_seeds_mix`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_playoff_seeds_mix`(
  v_season SMALLINT UNSIGNED,
  v_standing_date DATE
)
    COMMENT 'Calculate MLB playoff seeds: div/conf position mix'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_standings_mix;
  CREATE TEMPORARY TABLE tmp_standings_mix LIKE SPORTS_MLB_STANDINGS;
  INSERT INTO tmp_standings_mix
    SELECT *
    FROM SPORTS_MLB_STANDINGS
    WHERE season = v_season
    AND   the_date = v_standing_date;
  ALTER TABLE tmp_standings_mix
    ADD COLUMN _mix_conf_id TINYINT UNSIGNED,
    ADD COLUMN _mix_pos_conf TINYINT UNSIGNED;

  UPDATE tmp_standings_mix
  JOIN SPORTS_MLB_TEAMS_GROUPINGS
    ON (SPORTS_MLB_TEAMS_GROUPINGS.team_id = tmp_standings_mix.team_id
    AND v_season BETWEEN SPORTS_MLB_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_MLB_TEAMS_GROUPINGS.season_to, 2099))
  JOIN SPORTS_MLB_GROUPINGS
    ON (SPORTS_MLB_GROUPINGS.grouping_id = SPORTS_MLB_TEAMS_GROUPINGS.grouping_id)
  SET tmp_standings_mix._mix_conf_id = SPORTS_MLB_GROUPINGS.parent_id;

  CALL _duplicate_tmp_table('tmp_standings_mix', 'tmp_standings_mix_cpA');
  CALL _duplicate_tmp_table('tmp_standings_mix', 'tmp_standings_mix_cpB');
  INSERT INTO tmp_standings_mix (season, the_date, team_id, _mix_pos_conf)
    SELECT tmp_standings_mix_cpA.season, tmp_standings_mix_cpA.the_date, tmp_standings_mix_cpA.team_id,
           COUNT(tmp_standings_mix_cpB.team_id) + 1 AS _mix_pos_conf
    FROM tmp_standings_mix_cpA
    LEFT JOIN tmp_standings_mix_cpB
      ON (tmp_standings_mix_cpB.season = tmp_standings_mix_cpA.season
      AND tmp_standings_mix_cpB.the_date = tmp_standings_mix_cpA.the_date
      AND tmp_standings_mix_cpB._mix_conf_id = tmp_standings_mix_cpA._mix_conf_id
      # Div Leaders
      AND ((tmp_standings_mix_cpB.pos_div = 1
        AND tmp_standings_mix_cpA.pos_div = 1
        AND tmp_standings_mix_cpB.pos_conf < tmp_standings_mix_cpA.pos_conf)
      # Div Runner-up
        OR (tmp_standings_mix_cpB.pos_div = 2
        AND tmp_standings_mix_cpA.pos_div = 2
        AND tmp_standings_mix_cpB.pos_conf < tmp_standings_mix_cpA.pos_conf)
      # Conf Wildcards
        OR (tmp_standings_mix_cpB.pos_div > 2
        AND tmp_standings_mix_cpA.pos_div > 2
        AND tmp_standings_mix_cpB.pos_conf < tmp_standings_mix_cpA.pos_conf)))
    GROUP BY tmp_standings_mix_cpA.season, tmp_standings_mix_cpA.the_date, tmp_standings_mix_cpA.team_id
  ON DUPLICATE KEY UPDATE _mix_pos_conf = VALUES(_mix_pos_conf);

  INSERT INTO SPORTS_MLB_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
    SELECT season,
           _mix_conf_id AS conf_id,
           _mix_pos_conf + CASE pos_div
             WHEN 1 THEN 0
             WHEN 2 THEN 3
             ELSE 6
           END AS seed,
           team_id,
           pos_div,
           pos_conf,
           pos_league
    FROM tmp_standings_mix
    WHERE season = v_season
    AND   the_date = v_standing_date
    AND   (pos_div <= 2 OR _mix_pos_conf <= 2);

END $$

DELIMITER ;
