##
## Team totals sorted
##
DROP PROCEDURE IF EXISTS `mlb_totals_teams_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for MLB teams'
BEGIN

  # Broken down by individual routines
  CALL mlb_totals_teams_sort_batting(v_season, v_season_type);
  CALL mlb_totals_teams_sort_pitching(v_season, v_season_type);
  CALL mlb_totals_teams_sort_fielding(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_teams_sort_batting`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams_sort_batting`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort batting season totals for MLB teams'
BEGIN

  DELETE FROM SPORTS_MLB_TEAMS_SEASON_BATTING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL mlb_totals_teams_sort__tmp(v_season, v_season_type, 'BATTING',
                                  'ab SMALLINT UNSIGNED,
                                   pa SMALLINT UNSIGNED,
                                   h SMALLINT UNSIGNED,
                                   1b SMALLINT UNSIGNED,
                                   2b SMALLINT UNSIGNED,
                                   3b TINYINT UNSIGNED,
                                   xbh SMALLINT UNSIGNED,
                                   bb SMALLINT UNSIGNED,
                                   ibb TINYINT UNSIGNED,
                                   r SMALLINT UNSIGNED,
                                   hr TINYINT UNSIGNED,
                                   rbi SMALLINT UNSIGNED,
                                   sb TINYINT UNSIGNED,
                                   cs TINYINT UNSIGNED,
                                   po TINYINT UNSIGNED,
                                   avg DECIMAL(5,4) SIGNED,
                                   obp DECIMAL(5,4) SIGNED,
                                   tb SMALLINT UNSIGNED,
                                   slg DECIMAL(5,4) SIGNED,
                                   ops DECIMAL(5,4) SIGNED,
                                   sac_fly TINYINT UNSIGNED,
                                   sac_hit TINYINT UNSIGNED,
                                   hbp TINYINT UNSIGNED,
                                   k SMALLINT UNSIGNED,
                                   go SMALLINT UNSIGNED,
                                   fo SMALLINT UNSIGNED,
                                   gidp TINYINT UNSIGNED,
                                   lob SMALLINT UNSIGNED',
                                  'ab, pa, h, 1b, 2b, 3b, xbh, bb, ibb, r, hr, rbi, sb, cs, po, avg, obp, tb, slg, ops, sac_fly, sac_hit, hbp, k, go, fo, gidp, lob');

  # Calcs
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'ab', 'B.ab > A.ab');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'pa', 'B.pa > A.pa');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'h', 'B.h > A.h');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', '1b', 'B.1b > A.1b');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', '2b', 'B.2b > A.2b');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', '3b', 'B.3b > A.3b');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'xbh', 'B.xbh > A.xbh');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'bb', 'B.bb > A.bb');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'ibb', 'B.ibb > A.ibb');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'r', 'B.r > A.r');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'hr', 'B.hr > A.hr');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'rbi', 'B.rbi > A.rbi');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'sb', 'B.sb > A.sb');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'cs', 'B.cs < A.cs');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'po', 'B.po < A.po');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'avg', 'B.avg > A.avg');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'obp', 'B.obp > A.obp');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'tb', 'B.tb > A.tb');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'slg', 'B.slg > A.slg');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'ops', 'B.ops > A.ops');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'sac_fly', 'B.sac_fly > A.sac_fly');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'sac_hit', 'B.sac_hit > A.sac_hit');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'hbp', 'B.hbp > A.hbp');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'k', 'B.k < A.k');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'go', 'B.go > A.go');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'fo', 'B.fo > A.fo');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'gidp', 'B.gidp < A.gidp');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'BATTING', 'lob', 'B.lob < A.lob');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_teams_sort_pitching`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams_sort_pitching`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort pitching season totals for MLB teams'
BEGIN

  DELETE FROM SPORTS_MLB_TEAMS_SEASON_PITCHING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL mlb_totals_teams_sort__tmp(v_season, v_season_type, 'PITCHING',
                                  'ip DECIMAL(5,1) UNSIGNED,
                                   `out` SMALLINT UNSIGNED,
                                   bf SMALLINT UNSIGNED,
                                   pt SMALLINT UNSIGNED,
                                   b SMALLINT UNSIGNED,
                                   s SMALLINT UNSIGNED,
                                   k SMALLINT UNSIGNED,
                                   h SMALLINT UNSIGNED,
                                   hr SMALLINT UNSIGNED,
                                   bb SMALLINT UNSIGNED,
                                   ibb TINYINT UNSIGNED,
                                   wp TINYINT UNSIGNED,
                                   go SMALLINT UNSIGNED,
                                   fo SMALLINT UNSIGNED,
                                   go_fo DECIMAL(5,4) UNSIGNED,
                                   r SMALLINT UNSIGNED,
                                   ra DECIMAL(6,3) UNSIGNED,
                                   er SMALLINT UNSIGNED,
                                   era DECIMAL(6,3) UNSIGNED,
                                   ur TINYINT UNSIGNED,
                                   ura DECIMAL(6,3) UNSIGNED,
                                   ir TINYINT UNSIGNED,
                                   ira TINYINT UNSIGNED,
                                   whip DECIMAL(6,3) UNSIGNED,
                                   bk TINYINT UNSIGNED,
                                   sb TINYINT UNSIGNED,
                                   cs TINYINT UNSIGNED,
                                   po TINYINT UNSIGNED,
                                   w TINYINT UNSIGNED,
                                   l TINYINT UNSIGNED,
                                   sv TINYINT UNSIGNED,
                                   hld TINYINT UNSIGNED,
                                   bs TINYINT UNSIGNED,
                                   svo TINYINT UNSIGNED,
                                   cg TINYINT UNSIGNED,
                                   sho TINYINT UNSIGNED,
                                   qs TINYINT UNSIGNED,
                                   game_score DECIMAL(5,2) SIGNED',
                                  'ip, `out`, bf, pt, b, s, k, h, hr, bb, ibb, wp, go, fo, go_fo, r, ra, er, era, ur, ura, ir, ira, whip, bk, sb, cs, po, w, l, sv, hld, bs, svo, cg, sho, qs, game_score');

  # Calcs
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'ip', 'B.ip > A.ip');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'out', 'B.`out` > A.`out`');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'bf', 'B.bf > A.bf');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'pt', 'B.pt > A.pt');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'b', 'B.b > A.b');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 's', 'B.s > A.s');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'k', 'B.k > A.k');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'h', 'B.h < A.h');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'hr', 'B.hr < A.hr');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'bb', 'B.bb < A.bb');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'ibb', 'B.ibb < A.ibb');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'wp', 'B.wp < A.wp');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'go', 'B.go > A.go');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'fo', 'B.fo > A.fo');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'go_fo', 'B.go_fo > A.go_fo');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'r', 'B.r < A.r');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'ra', 'B.ra < A.ra');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'er', 'B.er < A.er');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'era', 'B.era < A.era');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'ur', 'B.ur < A.ur');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'ura', 'B.ura < A.ura');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'ir', 'B.ir < A.ir');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'ira', 'B.ira < A.ira');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'whip', 'B.whip < A.whip');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'bk', 'B.bk < A.bk');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'sb', 'B.sb < A.sb');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'cs', 'B.cs > A.cs');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'po', 'B.po > A.po');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'w', 'B.w > A.w');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'l', 'B.l > A.l');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'sv', 'B.sv > A.sv');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'hld', 'B.hld > A.hld');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'bs', 'B.bs < A.bs');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'svo', 'B.svo > A.svo');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'cg', 'B.cg > A.cg');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'sho', 'B.sho > A.sho');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'qs', 'B.qs > A.qs');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'PITCHING', 'game_score', 'B.game_score > A.game_score');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_teams_sort_fielding`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams_sort_fielding`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort fielding season totals for MLB teams'
BEGIN

  DELETE FROM SPORTS_MLB_TEAMS_SEASON_FIELDING_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL mlb_totals_teams_sort__tmp(v_season, v_season_type, 'FIELDING',
                                  'tc SMALLINT UNSIGNED,
                                   po SMALLINT UNSIGNED,
                                   a SMALLINT UNSIGNED,
                                   e TINYINT UNSIGNED,
                                   pct DECIMAL(5,4) SIGNED,
                                   dp TINYINT UNSIGNED',
                                  'tc, po, a, e, pct, dp');

  # Calcs
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'FIELDING', 'tc', 'B.tc > A.tc');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'FIELDING', 'po', 'B.po > A.po');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'FIELDING', 'a', 'B.a > A.a');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'FIELDING', 'e', 'B.e < A.e');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'FIELDING', 'pct', 'B.pct > A.pct');
  CALL mlb_totals_teams_sort__calc(v_season, v_season_type, 'FIELDING', 'dp', 'B.dp > A.dp');

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_teams_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_cols VARCHAR(4096),
  v_calcs VARCHAR(2048)
)
    COMMENT 'Create temporary table for MLB team season stat sort'
BEGIN

  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_MLB_TEAMS_SEASON_', v_table));
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_MLB_TEAMS_SEASON_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      team_id VARCHAR(3) NOT NULL,
      ', v_cols, ',
      PRIMARY KEY (season, season_type, team_id)
    ) ENGINE = MEMORY
      SELECT season, season_type, team_id, ', v_calcs, '
      FROM SPORTS_MLB_TEAMS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '";'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_MLB_TEAMS_SEASON_', v_table), CONCAT('tmp_SPORTS_MLB_TEAMS_SEASON_', v_table, '_cp'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `mlb_totals_teams_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_totals_teams_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col VARCHAR(15),
  v_calc VARCHAR(255)
)
    COMMENT 'Sort season totals for MLB teams'
BEGIN

  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_MLB_TEAMS_SEASON_', v_table, '_SORTED (season, season_type, team_id, `', v_col, '`)
      SELECT A.season, A.season_type, A.team_id,
             COUNT(B.team_id) + 1 AS `', v_col, '`
      FROM tmp_SPORTS_MLB_TEAMS_SEASON_', v_table, ' AS A
      LEFT JOIN tmp_SPORTS_MLB_TEAMS_SEASON_', v_table, '_cp AS B
        ON (B.season = A.season
        AND B.season_type = A.season_type
        AND ', v_calc, ')
      WHERE A.season = ', v_season, '
      AND   A.season_type = "', v_season_type, '"
      GROUP BY A.season, A.season_type, A.team_id
    ON DUPLICATE KEY UPDATE `', v_col, '` = VALUES(`', v_col, '`);'));

END $$

DELIMITER ;
