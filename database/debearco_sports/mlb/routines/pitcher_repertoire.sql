#
# Determine pitcher repertoires from a recent period
#
DROP PROCEDURE IF EXISTS `mlb_pitcher_repertoire`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `mlb_pitcher_repertoire` (
)
    COMMENT 'Determine the pitch types thrown by MLB pitchers'
BEGIN

  DECLARE v_season_lookback TINYINT UNSIGNED DEFAULT 2;
  DECLARE v_game_lookback TINYINT UNSIGNED DEFAULT 30; # In weighted points

  DECLARE v_season SMALLINT UNSIGNED;
  SELECT MAX(season) INTO v_season FROM tmp_game_list;

  # Pitchers to run this on
  DROP TEMPORARY TABLE IF EXISTS tmp_pitchers;
  CREATE TEMPORARY TABLE tmp_pitchers (
    player_id SMALLINT UNSIGNED,
    PRIMARY KEY (player_id)
  ) SELECT DISTINCT player_id
    FROM SPORTS_MLB_TEAMS_ROSTERS
    WHERE season = v_season
    AND   pos IN ('P', 'TW');

  # The games they played, ordered and weighted
  DROP TEMPORARY TABLE IF EXISTS tmp_pitchers_games;
  CREATE TEMPORARY TABLE tmp_pitchers_games (
    player_id SMALLINT UNSIGNED,
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    jersey TINYINT UNSIGNED,
    game_time_ref MEDIUMINT UNSIGNED,
    game_weight TINYINT UNSIGNED,
    game_order SMALLINT UNSIGNED,
    PRIMARY KEY (player_id, season, game_type, game_id),
    KEY by_ref (player_id, game_time_ref)
  ) SELECT tmp_pitchers.player_id,
           SPORTS_MLB_SCHEDULE.season,
           SPORTS_MLB_SCHEDULE.game_type,
           SPORTS_MLB_SCHEDULE.game_id,
           SPORTS_MLB_GAME_ROSTERS.team_id,
           SPORTS_MLB_GAME_ROSTERS.jersey,
           (100000 * (SPORTS_MLB_SCHEDULE.season % 100)) + SPORTS_MLB_SCHEDULE.game_time_ref AS game_time_ref,
           IF(SPORTS_MLB_GAME_PITCHERS.from_outs = 0, 2, 1) AS game_weight, # SP = 2, RP = 1
           0 AS game_order
    FROM tmp_pitchers
    JOIN SPORTS_MLB_GAME_ROSTERS
      ON (SPORTS_MLB_GAME_ROSTERS.season BETWEEN (v_season - v_season_lookback) AND v_season
      AND SPORTS_MLB_GAME_ROSTERS.player_id = tmp_pitchers.player_id)
    JOIN SPORTS_MLB_GAME_PITCHERS
      ON (SPORTS_MLB_GAME_PITCHERS.season = SPORTS_MLB_GAME_ROSTERS.season
      AND SPORTS_MLB_GAME_PITCHERS.game_type = SPORTS_MLB_GAME_ROSTERS.game_type
      AND SPORTS_MLB_GAME_PITCHERS.game_id = SPORTS_MLB_GAME_ROSTERS.game_id
      AND SPORTS_MLB_GAME_PITCHERS.team_id = SPORTS_MLB_GAME_ROSTERS.team_id
      AND SPORTS_MLB_GAME_PITCHERS.jersey = SPORTS_MLB_GAME_ROSTERS.jersey)
    JOIN SPORTS_MLB_SCHEDULE
      ON (SPORTS_MLB_SCHEDULE.season = SPORTS_MLB_GAME_PITCHERS.season
      AND SPORTS_MLB_SCHEDULE.game_type = SPORTS_MLB_GAME_PITCHERS.game_type
      AND SPORTS_MLB_SCHEDULE.game_id = SPORTS_MLB_GAME_PITCHERS.game_id);

  # Restrict to just those games we want to analyse
  CALL _duplicate_tmp_table('tmp_pitchers_games', 'tmp_pitchers_games_cpA');
  CALL _duplicate_tmp_table('tmp_pitchers_games', 'tmp_pitchers_games_cpB');
  INSERT INTO tmp_pitchers_games (player_id, season, game_type, game_id, game_time_ref, game_order)
    SELECT tmp_pitchers_games_cpA.player_id,
           tmp_pitchers_games_cpA.season, tmp_pitchers_games_cpA.game_type, tmp_pitchers_games_cpA.game_id,
           tmp_pitchers_games_cpA.game_time_ref,
           IFNULL(SUM(tmp_pitchers_games_cpB.game_weight) + tmp_pitchers_games_cpA.game_weight, 0) AS game_order
    FROM tmp_pitchers_games_cpA
    LEFT JOIN tmp_pitchers_games_cpB
      ON (tmp_pitchers_games_cpB.player_id = tmp_pitchers_games_cpA.player_id
      AND tmp_pitchers_games_cpB.game_time_ref > tmp_pitchers_games_cpA.game_time_ref)
    GROUP BY tmp_pitchers_games_cpA.player_id, tmp_pitchers_games_cpA.game_time_ref
  ON DUPLICATE KEY UPDATE game_order = VALUES(game_order);

  DELETE FROM tmp_pitchers_games WHERE game_order > v_game_lookback;

  # Group the pitchers and pitch types from these games
  DROP TEMPORARY TABLE IF EXISTS tmp_pitches_atbat;
  CREATE TEMPORARY TABLE tmp_pitches_atbat LIKE SPORTS_MLB_GAME_ATBAT;
  ALTER TABLE tmp_pitches_atbat
    ADD COLUMN player_id SMALLINT UNSIGNED FIRST;
  INSERT INTO tmp_pitches_atbat
    SELECT tmp_pitchers_games.player_id, SPORTS_MLB_GAME_ATBAT.*
    FROM tmp_pitchers_games
    JOIN SPORTS_MLB_GAME_ATBAT
      ON (SPORTS_MLB_GAME_ATBAT.season = tmp_pitchers_games.season
      AND SPORTS_MLB_GAME_ATBAT.game_type = tmp_pitchers_games.game_type
      AND SPORTS_MLB_GAME_ATBAT.game_id = tmp_pitchers_games.game_id
      AND SPORTS_MLB_GAME_ATBAT.pitcher_team_id = tmp_pitchers_games.team_id
      AND SPORTS_MLB_GAME_ATBAT.pitcher = tmp_pitchers_games.jersey);
  ALTER TABLE tmp_pitches_atbat
    ADD KEY by_pitcher (player_id, season, game_type, game_id);

  DROP TEMPORARY TABLE IF EXISTS tmp_pitches_thrown;
  CREATE TEMPORARY TABLE tmp_pitches_thrown (
    player_id SMALLINT UNSIGNED,
    pitch CHAR(2),
    num_thrown SMALLINT UNSIGNED,
    avg_speed DECIMAL(5, 2) UNSIGNED DEFAULT NULL,
    pitch_order TINYINT UNSIGNED,
    PRIMARY KEY (player_id, pitch),
    INDEX by_count (player_id, num_thrown)
  ) SELECT tmp_pitchers_games.player_id,
           SPORTS_MLB_GAME_ATBAT_PITCHES.pitch,
           COUNT(*) AS num_thrown,
           AVG(speed) AS avg_speed,
           0 AS pitch_order
    FROM tmp_pitchers_games
    JOIN tmp_pitches_atbat
      ON (tmp_pitches_atbat.player_id = tmp_pitchers_games.player_id
      AND tmp_pitches_atbat.season = tmp_pitchers_games.season
      AND tmp_pitches_atbat.game_type = tmp_pitchers_games.game_type
      AND tmp_pitches_atbat.game_id = tmp_pitchers_games.game_id)
    JOIN SPORTS_MLB_GAME_ATBAT_PITCHES
      ON (SPORTS_MLB_GAME_ATBAT_PITCHES.season = tmp_pitches_atbat.season
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.game_type = tmp_pitches_atbat.game_type
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.game_id = tmp_pitches_atbat.game_id
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.play_id = tmp_pitches_atbat.play_id
      AND SPORTS_MLB_GAME_ATBAT_PITCHES.pitch IS NOT NULL)
    GROUP BY tmp_pitches_atbat.player_id, SPORTS_MLB_GAME_ATBAT_PITCHES.pitch;

  DROP TEMPORARY TABLE IF EXISTS tmp_pitches_thrown_total;
  CREATE TEMPORARY TABLE tmp_pitches_thrown_total (
    player_id SMALLINT UNSIGNED,
    num_total SMALLINT UNSIGNED,
    PRIMARY KEY (player_id)
  ) SELECT player_id, SUM(num_thrown) AS num_total
    FROM tmp_pitches_thrown
    GROUP BY player_id;

  # Sort the pitches thrown
  CALL _duplicate_tmp_table('tmp_pitches_thrown', 'tmp_pitches_thrown_cpA');
  CALL _duplicate_tmp_table('tmp_pitches_thrown', 'tmp_pitches_thrown_cpB');
  INSERT INTO tmp_pitches_thrown (player_id, pitch, pitch_order)
    SELECT tmp_pitches_thrown_cpA.player_id, tmp_pitches_thrown_cpA.pitch,
           COUNT(tmp_pitches_thrown_cpB.pitch) + 1 AS pitch_order
    FROM tmp_pitches_thrown_cpA
    LEFT JOIN tmp_pitches_thrown_cpB
      ON (tmp_pitches_thrown_cpB.player_id = tmp_pitches_thrown_cpA.player_id
      AND ((tmp_pitches_thrown_cpB.num_thrown > tmp_pitches_thrown_cpA.num_thrown)
        OR (tmp_pitches_thrown_cpB.num_thrown = tmp_pitches_thrown_cpA.num_thrown
        AND tmp_pitches_thrown_cpB.pitch > tmp_pitches_thrown_cpA.pitch)))
    GROUP BY tmp_pitches_thrown_cpA.player_id, tmp_pitches_thrown_cpA.pitch
  ON DUPLICATE KEY UPDATE pitch_order = VALUES(pitch_order);

  # Determine fastball average speeds for each pitcher
  DROP TEMPORARY TABLE IF EXISTS tmp_pitches_fastballs;
  CREATE TEMPORARY TABLE tmp_pitches_fastballs (
    player_id SMALLINT UNSIGNED,
    num_thrown SMALLINT UNSIGNED,
    avg_speed DECIMAL(5, 2) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (player_id)
  ) SELECT player_id,
           SUM(num_thrown) AS num_thrown,
           SUM(num_thrown * avg_speed) / SUM(num_thrown) AS avg_speed
    FROM tmp_pitches_thrown
    WHERE pitch IN ('FF', 'FT', 'FC', 'SI')
    GROUP BY player_id;

  # Store
  DELETE SPORTS_MLB_PLAYERS_REPERTOIRE.*
  FROM tmp_pitchers
  JOIN SPORTS_MLB_PLAYERS_REPERTOIRE
    ON (SPORTS_MLB_PLAYERS_REPERTOIRE.player_id = tmp_pitchers.player_id)
  WHERE SPORTS_MLB_PLAYERS_REPERTOIRE.season = v_season;
  CALL _duplicate_tmp_table('tmp_pitches_thrown', 'tmp_pitches_thrown_p1');
  CALL _duplicate_tmp_table('tmp_pitches_thrown', 'tmp_pitches_thrown_p2');
  CALL _duplicate_tmp_table('tmp_pitches_thrown', 'tmp_pitches_thrown_p3');
  CALL _duplicate_tmp_table('tmp_pitches_thrown', 'tmp_pitches_thrown_p4');
  INSERT INTO SPORTS_MLB_PLAYERS_REPERTOIRE (season, player_id, pitch1, pitch1_pct, pitch2, pitch2_pct, pitch3, pitch3_pct, pitch4, pitch4_pct, fastball_avg, fastball_diff)
    SELECT v_season AS season,
           tmp_pitches_thrown_total.player_id,
           tpt_p1.pitch AS pitch1,
           (100 * tpt_p1.num_thrown) / tmp_pitches_thrown_total.num_total AS pitch1_pct,
           tpt_p2.pitch AS pitch2,
           (100 * tpt_p2.num_thrown) / tmp_pitches_thrown_total.num_total AS pitch2_pct,
           tpt_p3.pitch AS pitch3,
           (100 * tpt_p3.num_thrown) / tmp_pitches_thrown_total.num_total AS pitch3_pct,
           tpt_p4.pitch AS pitch4,
           (100 * tpt_p4.num_thrown) / tmp_pitches_thrown_total.num_total AS pitch4_pct,
           tmp_pitches_fastballs.avg_speed AS fastball_avg,
           CAST(tmp_pitches_fastballs.avg_speed AS SIGNED) - CAST(prev_season.fastball_avg AS SIGNED) AS fastball_diff
    FROM tmp_pitches_thrown_total
    JOIN tmp_pitches_thrown_p1 AS tpt_p1
      ON (tpt_p1.player_id = tmp_pitches_thrown_total.player_id
      AND tpt_p1.pitch_order = 1)
    LEFT JOIN tmp_pitches_thrown_p2 AS tpt_p2
      ON (tpt_p2.player_id = tmp_pitches_thrown_total.player_id
      AND tpt_p2.pitch_order = 2)
    LEFT JOIN tmp_pitches_thrown_p3 AS tpt_p3
      ON (tpt_p3.player_id = tmp_pitches_thrown_total.player_id
      AND tpt_p3.pitch_order = 3)
    LEFT JOIN tmp_pitches_thrown_p4 AS tpt_p4
      ON (tpt_p4.player_id = tmp_pitches_thrown_total.player_id
      AND tpt_p4.pitch_order = 4)
    LEFT JOIN tmp_pitches_fastballs
      ON (tmp_pitches_fastballs.player_id = tmp_pitches_thrown_total.player_id)
    LEFT JOIN SPORTS_MLB_PLAYERS_REPERTOIRE AS prev_season
      ON (prev_season.season = v_season - 1
      AND prev_season.player_id = tmp_pitches_thrown_total.player_id)
  ON DUPLICATE KEY UPDATE pitch1 = VALUES(pitch1),
                          pitch1_pct = VALUES(pitch1_pct),
                          pitch2 = VALUES(pitch2),
                          pitch2_pct = VALUES(pitch2_pct),
                          pitch3 = VALUES(pitch3),
                          pitch3_pct = VALUES(pitch3_pct),
                          pitch4 = VALUES(pitch4),
                          pitch4_pct = VALUES(pitch4_pct),
                          fastball_avg = VALUES(fastball_avg),
                          fastball_diff = VALUES(fastball_diff);

  ALTER TABLE SPORTS_MLB_PLAYERS_REPERTOIRE ORDER BY season, player_id;

END $$

DELIMITER ;

