CREATE TABLE `SPORTS_MLB_PLAYERS` (
  `player_id` SMALLINT(8) UNSIGNED NOT NULL,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `stands` ENUM('L','R','S') COLLATE latin1_general_ci DEFAULT NULL,
  `throws` ENUM('L','R') COLLATE latin1_general_ci DEFAULT NULL,
  `pronunciation` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `height` TINYINT(2) UNSIGNED DEFAULT NULL,
  `weight` SMALLINT(3) UNSIGNED DEFAULT NULL,
  `dob` DATE DEFAULT NULL,
  `birthplace` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `high_school` VARCHAR(60) COLLATE latin1_general_ci DEFAULT NULL,
  `college` VARCHAR(60) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_IMPORT` (
  `player_id` SMALLINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `remote_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `profile_imported` DATETIME DEFAULT NULL,
  PRIMARY KEY (`player_id`),
  UNIQUE KEY `by_remote` (`remote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_AWARDS` (
  `season` YEAR NOT NULL,
  `award_id` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `player_name` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci DEFAULT NULL,
  `pos` ENUM('C','1B','2B','SS','3B','LF','CF','RF','OF','DH','P','Util','TW') COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `FAUX_PRIMARY` (`season`,`award_id`,`player_id`,`pos`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
