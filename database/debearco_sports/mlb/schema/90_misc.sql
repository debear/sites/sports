CREATE TABLE `SPORTS_MLB_POSITIONS` (
  `pos_code` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','P','SP','RP','TW') COLLATE latin1_general_ci NOT NULL,
  `pos_long` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `posgroup_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `type` ENUM('batters','pitchers') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pos_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_POSITIONS_GROUPS` (
  `posgroup_id` TINYINT(3) UNSIGNED NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`posgroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_STATUS` (
  `type` ENUM('player') COLLATE latin1_general_ci NOT NULL,
  `code` ENUM('active','dl-7','dl-10','dl-15','dl-45','dl-60','paternity','bereavement','suspended','na') COLLATE latin1_general_ci NOT NULL,
  `disp_code` ENUM('IL7','IL10','IL15','IL45','IL60','NA','SUSP','BRV','PAT') COLLATE latin1_general_ci DEFAULT NULL,
  `disp_name` VARCHAR(25) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`type`,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_UMPIRES` (
  `umpire_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`umpire_id`),
  UNIQUE KEY `by_name` (`first_name`,`surname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_UMPIRES_SEASON_ZONES` (
  `season` YEAR NOT NULL,
  `season_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `umpire_id` SMALLINT(5) UNSIGNED NOT NULL,
  `batter_hand` ENUM('L','R','CMB') COLLATE latin1_general_ci NOT NULL,
  `pitcher_hand` ENUM('L','R','CMB') COLLATE latin1_general_ci NOT NULL,
  `num_pitches` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `offset_top` DECIMAL(5,3) DEFAULT NULL,
  `offset_right` DECIMAL(5,3) DEFAULT NULL,
  `offset_bottom` DECIMAL(5,3) DEFAULT NULL,
  `offset_left` DECIMAL(5,3) DEFAULT NULL,
  PRIMARY KEY (`season`,`season_type`,`umpire_id`,`batter_hand`,`pitcher_hand`),
  KEY `by_umpire` (`umpire_id`,`season_type`,`batter_hand`,`pitcher_hand`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_GROUPINGS` (
  `grouping_id` TINYINT(1) UNSIGNED NOT NULL,
  `parent_id` TINYINT(1) UNSIGNED DEFAULT NULL,
  `type` ENUM('league','conf','div') COLLATE latin1_general_ci NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `name_full` VARCHAR(60) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED NOT NULL,
  `icon` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`grouping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_SPLIT_LABELS` (
  `split_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `split_type` ENUM('pitcher-info','batter-info','home-road','month','asb','opponent','stadium','with-baserunners','counts-on','counts-after','pitch-count','ab-length','inning','batting-order','fielding-pos') COLLATE latin1_general_ci NOT NULL,
  `split_label` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `order` TINYINT UNSIGNED DEFAULT 250,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`split_id`),
  UNIQUE KEY `FAUX_PRIMARY` (`split_type`,`split_label`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_AWARDS` (
  `award_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `description` VARCHAR(250) COLLATE latin1_general_ci NOT NULL,
  `disp_order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`award_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_DRAFT_TYPES` (
  `type` ENUM('amateur','r5-maj','r5-aaa','r5-aa','august','jan-am','jan-sec','jun-sec','jun-sec-del') COLLATE latin1_general_ci NOT NULL,
  `name` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `historical` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `month` TINYINT(3) UNSIGNED NOT NULL,
  `mlb_code` CHAR(2) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_DRAFT_ROUNDS` (
  `code` ENUM('C1','C2','C3','C4','CBA','CBB','SUP','PPI') COLLATE latin1_general_ci NOT NULL,
  `name` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(3) UNSIGNED DEFAULT NULL,
  `round_order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
