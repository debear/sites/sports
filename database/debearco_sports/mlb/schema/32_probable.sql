CREATE TABLE `SPORTS_MLB_SCHEDULE_PROBABLES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `game_date` DATE NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `opp_team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `venue` ENUM('home','away') COLLATE latin1_general_ci NOT NULL,
  `notes` VARCHAR(2048) COLLATE latin1_general_ci DEFAULT NULL,
  `catcher_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `catcher_reason` ENUM('usual-catcher', 'team-starter', 'depth-chart') DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
