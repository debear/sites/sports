CREATE TABLE `SPORTS_MLB_SCHEDULE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mlb_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `gameday_link` CHAR(26) COLLATE latin1_general_ci DEFAULT NULL,
  `home` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `visitor` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `game_date` DATE NOT NULL,
  `game_time` TIME DEFAULT NULL,
  `game_time_ref` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `started_date` DATE DEFAULT NULL,
  `home_score` TINYINT(2) UNSIGNED DEFAULT NULL,
  `visitor_score` TINYINT(2) UNSIGNED DEFAULT NULL,
  `status` ENUM('F','PPD','CNC','SSP') COLLATE latin1_general_ci DEFAULT NULL,
  `status_info` VARCHAR(25) COLLATE latin1_general_ci DEFAULT NULL,
  `innings` TINYINT(3) UNSIGNED DEFAULT NULL,
  `attendance` MEDIUMINT(5) UNSIGNED DEFAULT NULL,
  `weather_temp` TINYINT(3) UNSIGNED DEFAULT NULL,
  `weather_cond` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `weather_wind_sp` TINYINT(2) UNSIGNED DEFAULT NULL,
  `weather_wind_dir` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `winning_pitcher` TINYINT(3) UNSIGNED DEFAULT NULL,
  `losing_pitcher` TINYINT(3) UNSIGNED DEFAULT NULL,
  `saving_pitcher` TINYINT(3) UNSIGNED DEFAULT NULL,
  `playin_game` TINYINT(1) UNSIGNED DEFAULT 0,
  `perfect_game` TINYINT(1) UNSIGNED DEFAULT 0,
  `no_hitter` TINYINT(1) UNSIGNED DEFAULT 0,
  `alt_venue` TINYINT(2) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`),
  UNIQUE KEY `by_remote` (`season`,`game_type`,`mlb_id`,`game_date`),
  KEY `by_status` (`season`,`game_type`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_SCHEDULE_DATES` (
  `season` YEAR NOT NULL,
  `date` DATE NOT NULL,
  `num_games` TINYINT(3) UNSIGNED NOT NULL,
  `date_prev` DATE DEFAULT NULL,
  `date_next` DATE DEFAULT NULL,
  PRIMARY KEY (`season`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_SCHEDULE_MATCHUPS` (
  `season` YEAR NOT NULL,
  `team_idA` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `team_idB` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`team_idA`,`team_idB`,`game_type`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
