CREATE TABLE `SPORTS_MLB_PLAYERS_GAME_BATTING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `spot` TINYINT(1) UNSIGNED DEFAULT NULL,
  `order` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `pa` TINYINT(2) UNSIGNED DEFAULT 0,
  `ab` TINYINT(2) UNSIGNED DEFAULT 0,
  `h` TINYINT(2) UNSIGNED DEFAULT 0,
  `1b` TINYINT(2) UNSIGNED DEFAULT 0,
  `2b` TINYINT(1) UNSIGNED DEFAULT 0,
  `3b` TINYINT(1) UNSIGNED DEFAULT 0,
  `hr` TINYINT(1) UNSIGNED DEFAULT 0,
  `xbh` TINYINT(1) UNSIGNED DEFAULT 0,
  `tb` TINYINT(2) UNSIGNED DEFAULT 0,
  `bb` TINYINT(1) UNSIGNED DEFAULT 0,
  `ibb` TINYINT(1) UNSIGNED DEFAULT 0,
  `hbp` TINYINT(1) UNSIGNED DEFAULT 0,
  `k` TINYINT(2) UNSIGNED DEFAULT 0,
  `r` TINYINT(1) UNSIGNED DEFAULT 0,
  `rbi` TINYINT(2) UNSIGNED DEFAULT 0,
  `sb` TINYINT(2) UNSIGNED DEFAULT 0,
  `cs` TINYINT(2) UNSIGNED DEFAULT 0,
  `po` TINYINT(1) UNSIGNED DEFAULT 0,
  `sac_fly` TINYINT(1) UNSIGNED DEFAULT 0,
  `sac_hit` TINYINT(1) UNSIGNED DEFAULT 0,
  `go` TINYINT(2) UNSIGNED DEFAULT 0,
  `fo` TINYINT(2) UNSIGNED DEFAULT 0,
  `gidp` TINYINT(1) UNSIGNED DEFAULT 0,
  `lob` TINYINT(2) UNSIGNED DEFAULT 0,
  `avg` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `obp` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `slg` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `ops` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `winprob` DECIMAL(4,1) DEFAULT NULL,
  `note` VARCHAR(250) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`),
  UNIQUE KEY `by_player` (`season`,`game_type`,`game_id`,`team_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_GAME_PITCHING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `ip` DECIMAL(3,1) UNSIGNED DEFAULT 0.0,
  `out` TINYINT(2) UNSIGNED DEFAULT 0,
  `bf` TINYINT(2) UNSIGNED DEFAULT 0,
  `pt` TINYINT(3) UNSIGNED DEFAULT 0,
  `b` TINYINT(3) UNSIGNED DEFAULT 0,
  `s` TINYINT(3) UNSIGNED DEFAULT 0,
  `k` TINYINT(2) UNSIGNED DEFAULT 0,
  `h` TINYINT(2) UNSIGNED DEFAULT 0,
  `hr` TINYINT(2) UNSIGNED DEFAULT 0,
  `bb` TINYINT(2) UNSIGNED DEFAULT 0,
  `ibb` TINYINT(2) UNSIGNED DEFAULT 0,
  `whip` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `r` TINYINT(2) UNSIGNED DEFAULT 0,
  `ra` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `er` TINYINT(2) UNSIGNED DEFAULT 0,
  `era` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `ur` TINYINT(2) UNSIGNED DEFAULT 0,
  `ura` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `ir` TINYINT(2) UNSIGNED DEFAULT 0,
  `ira` TINYINT(1) UNSIGNED DEFAULT 0,
  `wp` TINYINT(2) UNSIGNED DEFAULT 0,
  `bk` TINYINT(1) UNSIGNED DEFAULT 0,
  `sb` TINYINT(1) UNSIGNED DEFAULT 0,
  `cs` TINYINT(1) UNSIGNED DEFAULT 0,
  `po` TINYINT(1) UNSIGNED DEFAULT 0,
  `go` TINYINT(2) UNSIGNED DEFAULT 0,
  `fo` TINYINT(2) UNSIGNED DEFAULT 0,
  `go_fo` DECIMAL(6,4) UNSIGNED DEFAULT NULL,
  `cg` TINYINT(1) UNSIGNED DEFAULT 0,
  `sho` TINYINT(1) UNSIGNED DEFAULT 0,
  `qs` TINYINT(1) UNSIGNED DEFAULT 0,
  `w` TINYINT(1) UNSIGNED DEFAULT 0,
  `l` TINYINT(1) UNSIGNED DEFAULT 0,
  `sv` TINYINT(1) UNSIGNED DEFAULT 0,
  `hld` TINYINT(1) UNSIGNED DEFAULT 0,
  `bs` TINYINT(1) UNSIGNED DEFAULT 0,
  `svo` TINYINT(1) UNSIGNED DEFAULT 0,
  `winprob` DECIMAL(4,1) DEFAULT NULL,
  `game_score` TINYINT(3) DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`),
  UNIQUE KEY `by_player` (`season`,`game_type`,`game_id`,`team_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_GAME_FIELDING` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `tc` TINYINT(2) UNSIGNED DEFAULT 0,
  `po` TINYINT(2) UNSIGNED DEFAULT 0,
  `a` TINYINT(2) UNSIGNED DEFAULT 0,
  `e` TINYINT(1) UNSIGNED DEFAULT 0,
  `pct` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `dp` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`),
  UNIQUE KEY `by_player` (`season`,`game_type`,`game_id`,`team_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `game_time_ref` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH','_ML') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED NOT NULL,
  `is_totals` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `pa` SMALLINT(5) UNSIGNED DEFAULT 0,
  `ab` SMALLINT(5) UNSIGNED DEFAULT 0,
  `h` TINYINT(3) UNSIGNED DEFAULT 0,
  `1b` TINYINT(3) UNSIGNED DEFAULT 0,
  `2b` TINYINT(2) UNSIGNED DEFAULT 0,
  `3b` TINYINT(2) UNSIGNED DEFAULT 0,
  `hr` TINYINT(2) UNSIGNED DEFAULT 0,
  `xbh` TINYINT(2) UNSIGNED DEFAULT 0,
  `tb` SMALLINT(5) UNSIGNED DEFAULT 0,
  `bb` TINYINT(3) UNSIGNED DEFAULT 0,
  `ibb` TINYINT(2) UNSIGNED DEFAULT 0,
  `hbp` TINYINT(2) UNSIGNED DEFAULT 0,
  `k` TINYINT(3) UNSIGNED DEFAULT 0,
  `r` TINYINT(3) UNSIGNED DEFAULT 0,
  `rbi` TINYINT(3) UNSIGNED DEFAULT 0,
  `sb` TINYINT(3) UNSIGNED DEFAULT 0,
  `cs` TINYINT(2) UNSIGNED DEFAULT 0,
  `po` TINYINT(2) UNSIGNED DEFAULT 0,
  `sac_fly` TINYINT(2) UNSIGNED DEFAULT 0,
  `sac_hit` TINYINT(2) UNSIGNED DEFAULT 0,
  `go` TINYINT(3) UNSIGNED DEFAULT 0,
  `fo` TINYINT(3) UNSIGNED DEFAULT 0,
  `gidp` TINYINT(2) UNSIGNED DEFAULT 0,
  `lob` SMALLINT(3) UNSIGNED DEFAULT 0,
  `avg` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `obp` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `slg` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `ops` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `winprob` DECIMAL(5,1) DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`player_id`),
  KEY `by_player_date` (`season`,`game_type`,`player_id`,`game_time_ref`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `game_time_ref` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH','_ML') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED NOT NULL,
  `is_totals` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `ip` DECIMAL(4,1) UNSIGNED DEFAULT 0.0,
  `out` SMALLINT(5) UNSIGNED DEFAULT 0,
  `bf` SMALLINT(5) UNSIGNED DEFAULT 0,
  `pt` SMALLINT(5) UNSIGNED DEFAULT 0,
  `b` SMALLINT(5) UNSIGNED DEFAULT 0,
  `s` SMALLINT(5) UNSIGNED DEFAULT 0,
  `k` SMALLINT(5) UNSIGNED DEFAULT 0,
  `h` SMALLINT(5) UNSIGNED DEFAULT 0,
  `hr` TINYINT(2) UNSIGNED DEFAULT 0,
  `bb` TINYINT(3) UNSIGNED DEFAULT 0,
  `ibb` TINYINT(2) UNSIGNED DEFAULT 0,
  `whip` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `r` TINYINT(3) UNSIGNED DEFAULT 0,
  `ra` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `er` TINYINT(3) UNSIGNED DEFAULT 0,
  `era` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `ur` TINYINT(2) UNSIGNED DEFAULT 0,
  `ura` DECIMAL(6,3) UNSIGNED DEFAULT NULL,
  `ir` TINYINT(3) UNSIGNED DEFAULT 0,
  `ira` TINYINT(2) UNSIGNED DEFAULT 0,
  `wp` TINYINT(2) UNSIGNED DEFAULT 0,
  `bk` TINYINT(2) UNSIGNED DEFAULT 0,
  `sb` TINYINT(2) UNSIGNED DEFAULT 0,
  `cs` TINYINT(2) UNSIGNED DEFAULT 0,
  `po` TINYINT(2) UNSIGNED DEFAULT 0,
  `go` SMALLINT(2) UNSIGNED DEFAULT 0,
  `fo` SMALLINT(2) UNSIGNED DEFAULT 0,
  `go_fo` DECIMAL(6,4) UNSIGNED DEFAULT NULL,
  `cg` TINYINT(2) UNSIGNED DEFAULT 0,
  `sho` TINYINT(1) UNSIGNED DEFAULT 0,
  `qs` TINYINT(2) UNSIGNED DEFAULT 0,
  `w` TINYINT(2) UNSIGNED DEFAULT 0,
  `l` TINYINT(2) UNSIGNED DEFAULT 0,
  `sv` TINYINT(2) UNSIGNED DEFAULT 0,
  `hld` TINYINT(2) UNSIGNED DEFAULT 0,
  `bs` TINYINT(2) UNSIGNED DEFAULT 0,
  `svo` TINYINT(2) UNSIGNED DEFAULT 0,
  `winprob` DECIMAL(5,1) DEFAULT NULL,
  `game_score` DECIMAL(5,2) DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`player_id`),
  KEY `by_player_date` (`season`,`game_type`,`player_id`,`game_time_ref`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `game_time_ref` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH','_ML') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED NOT NULL,
  `is_totals` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `tc` SMALLINT(4) UNSIGNED DEFAULT 0,
  `po` SMALLINT(5) UNSIGNED DEFAULT 0,
  `a` SMALLINT(5) UNSIGNED DEFAULT 0,
  `e` TINYINT(2) UNSIGNED DEFAULT 0,
  `pct` DECIMAL(5,4) UNSIGNED DEFAULT NULL,
  `dp` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`player_id`),
  KEY `by_player_date` (`season`,`game_type`,`player_id`,`game_time_ref`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
