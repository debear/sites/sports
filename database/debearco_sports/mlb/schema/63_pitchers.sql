CREATE TABLE `SPORTS_MLB_PLAYERS_REPERTOIRE` (
  `season` YEAR NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pitch1` ENUM('AB','CH','CS','CU','EP','FA','FC','FF','FO','FS','FT','IN','KC','KN','PO','SC','SI','SL','ST','SV','UN') COLLATE latin1_general_ci NOT NULL,
  `pitch1_pct` DECIMAL(4, 1) UNSIGNED NOT NULL,
  `pitch2` ENUM('AB','CH','CS','CU','EP','FA','FC','FF','FO','FS','FT','IN','KC','KN','PO','SC','SI','SL','ST','SV','UN') COLLATE latin1_general_ci DEFAULT NULL,
  `pitch2_pct` DECIMAL(4, 1) UNSIGNED DEFAULT NULL,
  `pitch3` ENUM('AB','CH','CS','CU','EP','FA','FC','FF','FO','FS','FT','IN','KC','KN','PO','SC','SI','SL','ST','SV','UN') COLLATE latin1_general_ci DEFAULT NULL,
  `pitch3_pct` DECIMAL(4, 1) UNSIGNED DEFAULT NULL,
  `pitch4` ENUM('AB','CH','CS','CU','EP','FA','FC','FF','FO','FS','FT','IN','KC','KN','PO','SC','SI','SL','ST','SV','UN') COLLATE latin1_general_ci DEFAULT NULL,
  `pitch4_pct` DECIMAL(4, 1) UNSIGNED DEFAULT NULL,
  `fastball_avg` DECIMAL(5, 2) UNSIGNED DEFAULT NULL,
  `fastball_diff` DECIMAL(4, 2) DEFAULT NULL,
  PRIMARY KEY (`season`, `player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
