CREATE TABLE `SPORTS_MLB_TEAMS` (
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci NOT NULL,
  `city` VARCHAR(40) COLLATE latin1_general_ci NOT NULL,
  `franchise` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `founded` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_NAMING` (
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `alt_team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci DEFAULT NULL,
  `season_from` SMALLINT(5) UNSIGNED NOT NULL,
  `season_to` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `alt_city` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  `alt_franchise` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_GROUPINGS` (
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci NOT NULL,
  `season_from` SMALLINT(5) UNSIGNED NOT NULL,
  `season_to` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `grouping_id` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_STADIA` (
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS','_ALT') COLLATE latin1_general_ci NOT NULL,
  `season_from` SMALLINT(5) UNSIGNED NOT NULL,
  `season_to` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `building_id` TINYINT(3) UNSIGNED NOT NULL,
  `stadium` VARCHAR(55) COLLATE latin1_general_ci NOT NULL,
  `weather_spec` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `alt_linked` CHAR(6) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season_from`,`building_id`),
  KEY `team_season` (`team_id`,`season_from`,`season_to`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_HISTORY_TITLES` (
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci NOT NULL,
  `league_id` TINYINT(3) UNSIGNED NOT NULL,
  `div_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `conf_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `playoff_champ` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`,`league_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_HISTORY_REGULAR` (
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci NOT NULL,
  `season` SMALLINT(5) UNSIGNED NOT NULL,
  `season_half` TINYINT(1) UNSIGNED NOT NULL,
  `wins` TINYINT(1) UNSIGNED DEFAULT 0,
  `loss` TINYINT(1) UNSIGNED DEFAULT 0,
  `pct` DECIMAL(4,3) UNSIGNED DEFAULT 0.000,
  `games_back` DECIMAL(3,1) UNSIGNED DEFAULT NULL,
  `pos` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season`,`season_half`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY` (
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `wildcard` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `div_champ` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `conf_champ` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `playoff_champ` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`team_id`,`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_HISTORY_PLAYOFF` (
  `team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci NOT NULL,
  `season` SMALLINT(5) UNSIGNED NOT NULL,
  `round` TINYINT(3) UNSIGNED NOT NULL,
  `round_code` ENUM('WC','LDS','LCS','HWS','WS') COLLATE latin1_general_ci NOT NULL,
  `opp_team_id` ENUM('ARI','ATL','BAL','BBR','BKD','BLO','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MBW','MIA','MIL','MIN','MTE','NGI','NYM','NYY','OAK','PHA','PHI','PIT','SAC','SD','SEA','SEP','SF','SLB','STL','TB','TEX','TOR','WSH','WSN','WSS') COLLATE latin1_general_ci DEFAULT NULL,
  `opp_team_alt` VARCHAR(90) COLLATE latin1_general_ci DEFAULT NULL,
  `for` TINYINT(3) UNSIGNED DEFAULT 0,
  `against` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`team_id`,`round`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
