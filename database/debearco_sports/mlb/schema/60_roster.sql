CREATE TABLE `SPORTS_MLB_TEAMS_ROSTERS` (
  `season` YEAR NOT NULL,
  `the_date` DATE NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `pos` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','P','TW') COLLATE latin1_general_ci DEFAULT NULL,
  `player_status` ENUM('active','dl-7','dl-10','dl-15','dl-45','dl-60','paternity','bereavement','suspended','na') COLLATE latin1_general_ci DEFAULT NULL,
  `roster_type` ENUM('25-man','40-man') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`the_date`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_TEAMS_DEPTH` (
  `season` YEAR NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `pos` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','SP','RP') COLLATE latin1_general_ci NOT NULL,
  `depth` TINYINT(1) UNSIGNED NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`team_id`,`pos`,`depth`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
