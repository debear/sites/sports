CREATE TABLE `SPORTS_MLB_DRAFT_SEASON` (
  `season` YEAR NOT NULL,
  `amateur` TINYINT(3) UNSIGNED DEFAULT 0,
  `r5-maj` TINYINT(3) UNSIGNED DEFAULT 0,
  `r5-aaa` TINYINT(3) UNSIGNED DEFAULT 0,
  `r5-aa` TINYINT(3) UNSIGNED DEFAULT 0,
  `august` TINYINT(3) UNSIGNED DEFAULT 0,
  `jan-am` TINYINT(3) UNSIGNED DEFAULT 0,
  `jan-sec` TINYINT(3) UNSIGNED DEFAULT 0,
  `jun-sec` TINYINT(3) UNSIGNED DEFAULT 0,
  `jun-sec-del` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_DRAFT` (
  `season` YEAR NOT NULL,
  `draft_type` ENUM('amateur','r5-maj','r5-aaa','r5-aa','august','jan-am','jan-sec','jun-sec','jun-sec-del') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(2) UNSIGNED NOT NULL,
  `round_descrip` ENUM('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99','100','C1','C2','C3','C4','CBA','CBB','SUP','PPI') COLLATE latin1_general_ci NOT NULL,
  `round_pick` TINYINT(2) UNSIGNED NOT NULL,
  `pick` SMALLINT(4) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','KCA','LAA','LAD','MBR','MIA','MIL','MIN','MTE','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SEP','SF','STL','TB','TEX','TOR','WSH','WSS') COLLATE latin1_general_ci NOT NULL,
  `remote_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL,
  `player_id` SMALLINT(8) UNSIGNED DEFAULT NULL,
  `player` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `pos` ENUM('C','1B','2B','SS','3B','LF','CF','RF','OF','DH','SP','RP','P','TW') COLLATE latin1_general_ci DEFAULT NULL,
  `height` TINYINT(3) UNSIGNED DEFAULT NULL,
  `weight` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `school` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `bats` ENUM('L','R','S') COLLATE latin1_general_ci DEFAULT NULL,
  `throws` ENUM('L','R','S') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`draft_type`,`pick`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
