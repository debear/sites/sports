CREATE TABLE `SPORTS_MLB_PLAYERS_INJURIES` (
  `season` YEAR NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `date_from` DATE NOT NULL,
  `date_to` DATE DEFAULT NULL,
  `description` VARCHAR(200) COLLATE latin1_general_ci DEFAULT NULL,
  `status` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `latest` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `due_back` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `updated` DATETIME DEFAULT NULL,
  PRIMARY KEY (`season`,`player_id`,`date_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
