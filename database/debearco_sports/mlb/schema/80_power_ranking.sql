CREATE TABLE `SPORTS_MLB_POWER_RANKINGS_WEEKS` (
  `season` YEAR NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `calc_date` DATE NOT NULL,
  `date_from` DATE NOT NULL,
  `date_to` DATE NOT NULL,
  `when_run` DATETIME DEFAULT NULL,
  PRIMARY KEY (`season`,`week`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_POWER_RANKINGS` (
  `season` YEAR NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `rank` TINYINT(3) UNSIGNED DEFAULT 0,
  `score` DECIMAL(5,3) DEFAULT 0.000,
  `win_pct` DECIMAL(5,3) DEFAULT NULL,
  `win_pct_onerun` DECIMAL(5,3) DEFAULT NULL,
  `run_diff` DECIMAL(5,3) DEFAULT NULL,
  `ops` DECIMAL(5,3) DEFAULT NULL,
  `hr` DECIMAL(5,3) DEFAULT NULL,
  `whip` DECIMAL(5,3) DEFAULT NULL,
  `field_pct` DECIMAL(5,3) DEFAULT NULL,
  `comment` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`week`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
