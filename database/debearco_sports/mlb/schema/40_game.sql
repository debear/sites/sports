CREATE TABLE `SPORTS_MLB_GAME_LINEUPS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `spot` TINYINT(3) UNSIGNED NOT NULL,
  `remote_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `pos` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','P','SP') COLLATE latin1_general_ci DEFAULT NULL,
  `bat_throw` ENUM('L','R','S') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`spot`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_GAME_ROSTERS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `player_id` SMALLINT(8) UNSIGNED DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `played` TINYINT(1) UNSIGNED DEFAULT 0,
  `started` TINYINT(1) UNSIGNED DEFAULT 0,
  `roster_pos` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','P','TW') COLLATE latin1_general_ci DEFAULT NULL,
  `pos` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','SP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt1` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt2` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt3` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt4` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt5` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt6` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt7` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  `pos_alt8` ENUM('C','1B','2B','SS','3B','LF','CF','RF','DH','PH','PR','RP') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`),
  UNIQUE KEY `by_player` (`season`,`game_type`,`game_id`,`team_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_GAME_PITCHERS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `from_outs` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `from_ip` DECIMAL(3,1) UNSIGNED NOT NULL DEFAULT 0.0,
  `to_outs` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `to_ip` DECIMAL(3,1) UNSIGNED NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_GAME_UMPIRES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `umpire_pos` ENUM('HP','1B','2B','3B','LF','RF') COLLATE latin1_general_ci NOT NULL,
  `umpire_id` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`umpire_pos`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_MLB_GAME_LINESCORE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ARI','ATL','BAL','BOS','CHC','CIN','CLE','COL','CWS','DET','HOU','KC','LAA','LAD','MIA','MIL','MIN','NYM','NYY','OAK','PHI','PIT','SAC','SD','SEA','SF','STL','TB','TEX','TOR','WSH') COLLATE latin1_general_ci NOT NULL,
  `runs` TINYINT(2) UNSIGNED DEFAULT 0,
  `hits` TINYINT(2) UNSIGNED DEFAULT 0,
  `errors` TINYINT(2) UNSIGNED DEFAULT 0,
  `inn01` TINYINT(2) UNSIGNED DEFAULT 0,
  `inn02` TINYINT(2) UNSIGNED DEFAULT 0,
  `inn03` TINYINT(2) UNSIGNED DEFAULT 0,
  `inn04` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn05` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn06` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn07` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn08` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn09` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn10` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn11` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn12` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn13` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn14` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn15` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn16` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn17` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn18` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn19` TINYINT(2) UNSIGNED DEFAULT NULL,
  `inn20` TINYINT(2) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
