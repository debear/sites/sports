##
## Process per-season SGP rider stats
##
DROP PROCEDURE IF EXISTS `sgp_season_stats`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `sgp_season_stats`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate aggregated single-season SGP rider stats'
BEGIN

  DECLARE v_stat_id TINYINT UNSIGNED;
  DECLARE v_calc_tbl VARCHAR(30);
  DECLARE v_calc_val VARCHAR(30);
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  DECLARE cur_conf CURSOR FOR
    SELECT stat_id, calc_tbl, calc_val
    FROM tmp_stats_list;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Determine our stats to process
  DROP TEMPORARY TABLE IF EXISTS tmp_stats_list;
  CREATE TEMPORARY TABLE tmp_stats_list (
    stat_id TINYINT UNSIGNED NOT NULL,
    calc_tbl VARCHAR(30),
    calc_val VARCHAR(30),
    PRIMARY KEY (stat_id)
  ) ENGINE = MEMORY;
  INSERT INTO tmp_stats_list (stat_id, calc_tbl, calc_val) VALUES
    # 1: Grand Prix
    (1, 'SPORTS_SGP_RACE_RESULT', 'SUM(VAL.pos IS NOT NULL)'),
    # 3: Grand Finals
    (3, 'SPORTS_SGP_RACE_RESULT', 'SUM(VAL.pos < 5)'),
    # 4: Podiums
    (4, 'SPORTS_SGP_RACE_RESULT', 'SUM(VAL.pos < 4)'),
    # 5: Grand Prix Wins
    (5, 'SPORTS_SGP_RACE_RESULT', 'SUM(VAL.pos = 1)'),
    # 6: Num Heats
    (6, 'SPORTS_SGP_RACE_HEATS', 'SUM(VAL.heat IS NOT NULL)'),
    # 7: Heats Completed
    (7, 'SPORTS_SGP_RACE_HEATS', 'SUM(VAL.pts IS NOT NULL)'),
    # 8: Points Scored
    (8, 'SPORTS_SGP_RACE_RESULT', 'SUM(IFNULL(VAL.pts, 0))'),
    # 9: Points per Heat
    (9, 'SPORTS_SGP_RACE_HEATS', 'AVG(VAL.pts)'),
    # 10: Heat Wins
    (10, 'SPORTS_SGP_RACE_HEATS', 'SUM(VAL.result = "1")');
  # Season dependent stats
  IF v_season <= 2024 THEN
    INSERT INTO tmp_stats_list (stat_id, calc_tbl, calc_val) VALUES
      # 2: Semi-Finals
      (2, 'SPORTS_SGP_RACE_RESULT', 'SUM(VAL.pos < 9)');
  ELSE
    INSERT INTO tmp_stats_list (stat_id, calc_tbl, calc_val) VALUES
      # 11: Last-Chance Qualifiers
      (11, 'SPORTS_SGP_RACE_RESULT', 'SUM(VAL.pos BETWEEN 3 AND 10)');
  END IF;
  DELETE FROM SPORTS_SGP_RIDER_STATS WHERE season = v_season;

  # Calculate the stats
  OPEN cur_conf;
  loop_conf: LOOP

    FETCH cur_conf INTO v_stat_id, v_calc_tbl, v_calc_val;
    IF v_done = 1 THEN LEAVE loop_conf; END IF;

    CALL _exec(CONCAT('INSERT INTO SPORTS_SGP_RIDER_STATS (season, rider_id, stat_id, value, extra, pos, pos_tied)
      SELECT RIDERS.season, RIDERS.rider_id,
      ', v_stat_id, ' AS stat_id, ', v_calc_val, ' AS value,
             NULL AS extra,
             NULL AS pos,
             NULL AS pos_tied
      FROM SPORTS_SGP_RACE_RIDERS AS RIDERS
      LEFT JOIN ', v_calc_tbl, ' AS VAL
        ON (VAL.season = RIDERS.season
        AND VAL.round = RIDERS.round
        AND VAL.bib_no = RIDERS.bib_no)
      WHERE RIDERS.season = ', v_season, '
      GROUP BY RIDERS.season, RIDERS.rider_id')); # '

  END LOOP loop_conf;
  CLOSE cur_conf;

  # Sort Positions
  INSERT INTO SPORTS_SGP_RIDER_STATS (season, rider_id, stat_id, pos, pos_tied)
    SELECT A.season, A.rider_id, A.stat_id,
            COUNT(DISTINCT B.rider_id) + 1 AS pos, 0 AS pos_tied
    FROM SPORTS_SGP_RIDER_STATS AS A
    JOIN SPORTS_SGP_STATS AS STAT
      ON (STAT.stat_id = A.stat_id)
    LEFT JOIN SPORTS_SGP_RIDER_STATS AS B
      ON (B.season = A.season
      AND B.stat_id = A.stat_id
      AND ((STAT.best_desc = 1 AND IFNULL(B.value, -99999.99) > IFNULL(A.value, -99999.99))
        OR (STAT.best_desc = 0 AND IFNULL(B.value, 99999.99) < IFNULL(A.value, 99999.99))))
    WHERE A.season = v_season
    GROUP BY A.season, A.rider_id, A.stat_id
  ON DUPLICATE KEY UPDATE pos = VALUES(pos), pos_tied = VALUES(pos_tied);

  # Break Ties
  INSERT INTO SPORTS_SGP_RIDER_STATS (season, rider_id, stat_id, pos, pos_tied)
    SELECT A.season, A.rider_id, A.stat_id, A.pos, 1 AS pos_tied
    FROM SPORTS_SGP_RIDER_STATS AS A
    JOIN SPORTS_SGP_RIDER_STATS AS B
      ON (B.season = A.season
      AND B.stat_id = A.stat_id
      AND B.pos = A.pos
      AND B.rider_id <> A.rider_id)
    WHERE A.season = v_season
    GROUP BY A.season, A.rider_id, A.stat_id
  ON DUPLICATE KEY UPDATE pos_tied = VALUES(pos_tied);

  ALTER TABLE SPORTS_SGP_RIDER_STATS ORDER BY season, rider_id, stat_id;

END $$

DELIMITER ;
