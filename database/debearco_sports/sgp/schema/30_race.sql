CREATE TABLE `SPORTS_SGP_RACES` (
  `season` YEAR NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `name_full` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `name_short` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `flag` CHAR(6) COLLATE latin1_general_ci DEFAULT NULL,
  `track` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `location` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `timezone` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `quirks` SET('QUALI_SPRINT') COLLATE latin1_general_ci DEFAULT NULL,
  `race_time` DATETIME NOT NULL,
  `round_order` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `completed_heats` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `track_length` SMALLINT(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`round`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RACE_RIDERS` (
  `season` YEAR NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `bib_no` SMALLINT(5) UNSIGNED NOT NULL,
  `rider_id` SMALLINT(5) UNSIGNED NOT NULL,
  `status` ENUM('sub','wc','trsv') DEFAULT NULL,
  PRIMARY KEY (`season`,`round`,`bib_no`),
  KEY `by_rider` (`season`,`rider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RACE_HEATS` (
  `season` YEAR NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `heat_type` ENUM('sprint','main','qf','sf','lcq','gf') COLLATE latin1_general_ci NOT NULL,
  `heat` TINYINT(1) UNSIGNED NOT NULL,
  `bib_no` SMALLINT(5) UNSIGNED NOT NULL,
  `gate` TINYINT(3) UNSIGNED DEFAULT NULL,
  `result` ENUM('1','2','3','4','r','m','t','f','x') COLLATE latin1_general_ci DEFAULT NULL,
  `pts` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`round`,`heat_type`,`heat`,`bib_no`),
  KEY `by_rider` (`season`,`round`,`bib_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RACE_RESULT` (
  `season` YEAR NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `bib_no` SMALLINT(5) UNSIGNED NOT NULL,
  `draw` TINYINT(3) UNSIGNED DEFAULT NULL,
  `pos` TINYINT(3) UNSIGNED DEFAULT NULL,
  `pts` TINYINT(3) UNSIGNED DEFAULT NULL,
  `main_pos` TINYINT(3) UNSIGNED DEFAULT NULL,
  `main_pts` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`round`,`bib_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
