CREATE TABLE `SPORTS_SGP_SETUP_RACES` (
  `season_from` YEAR NOT NULL,
  `season_to` YEAR NOT NULL,
  `gates_version` TINYINT(3) UNSIGNED NOT NULL,
  `draw_version` TINYINT(3) UNSIGNED NOT NULL,
  `regulars` TINYINT(3) UNSIGNED NOT NULL,
  `wildcards` TINYINT(3) UNSIGNED NOT NULL,
  `track_reserves` TINYINT(3) UNSIGNED NOT NULL,
  `substitutes` TINYINT(3) UNSIGNED NOT NULL,
  `quarter_finals` TINYINT(3) UNSIGNED NOT NULL,
  `semi_finals` TINYINT(3) UNSIGNED NOT NULL,
  `last_chance_qualifiers` TINYINT(3) UNSIGNED NOT NULL,
  `grand_finals` TINYINT(3) UNSIGNED NOT NULL,
  `grand_final_dbl_pts` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `fixed_champ_pts` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_SETUP_GATES` (
  `version` TINYINT(1) UNSIGNED NOT NULL,
  `draw` TINYINT(3) UNSIGNED NOT NULL,
  `heat` TINYINT(3) UNSIGNED NOT NULL,
  `gate` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`version`,`draw`,`heat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_SETUP_KNOCKOUT_DRAW` (
  `version` TINYINT(3) UNSIGNED NOT NULL,
  `main_pos` TINYINT(1) UNSIGNED NOT NULL,
  `heat_type` ENUM('qf','sf','lcq') COLLATE latin1_general_ci NOT NULL,
  `heat` TINYINT(3) UNSIGNED NOT NULL,
  `sel_pos` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`version`,`main_pos`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_STATS` (
  `stat_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `section` ENUM('meeting','heat') COLLATE latin1_general_ci NOT NULL,
  `name_short` VARCHAR(40) COLLATE latin1_general_ci NOT NULL,
  `name_full` VARCHAR(200) COLLATE latin1_general_ci NOT NULL,
  `best_desc` TINYINT(1) UNSIGNED DEFAULT 1,
  `type` ENUM('abs','avg','pct','delta') COLLATE latin1_general_ci NOT NULL,
  `disp_order` TINYINT(3) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
