CREATE TABLE `SPORTS_SPEEDWAY_RIDERS` (
  `rider_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(25) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(25) COLLATE latin1_general_ci NOT NULL,
  `birthplace` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `birthplace_country` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `nationality` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  PRIMARY KEY (`rider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RIDER_HISTORY` (
  `season` YEAR NOT NULL,
  `rider_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pos` TINYINT(3) UNSIGNED NOT NULL,
  `pts` TINYINT(3) UNSIGNED NOT NULL,
  `pos_raceoff` ENUM('1','2','3','4','r','m','t','f','x') COLLATE latin1_general_ci DEFAULT NULL,
  `num_wins` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `num_podiums` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`rider_id`),
  KEY `by_rider` (`rider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RIDER_HISTORY_PROGRESS` (
  `season` YEAR NOT NULL,
  `rider_id` SMALLINT(5) UNSIGNED NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `pos` TINYINT(3) UNSIGNED NOT NULL,
  `pts` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`rider_id`,`round`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RIDER_GP_HISTORY` (
  `season` YEAR NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `rider_id` SMALLINT(5) UNSIGNED NOT NULL,
  `gp_flag` CHAR(6) COLLATE latin1_general_ci DEFAULT NULL,
  `pos` TINYINT(1) UNSIGNED DEFAULT NULL,
  `pts` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`round`,`rider_id`),
  KEY `by_rider_season` (`season`,`rider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RIDER_RANKING` (
  `season` YEAR NOT NULL,
  `ranking` TINYINT(3) UNSIGNED NOT NULL,
  `rider_id` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`ranking`),
  KEY `by_rider` (`season`,`rider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_SGP_RIDER_STATS` (
  `season` YEAR NOT NULL,
  `rider_id` SMALLINT(5) UNSIGNED NOT NULL,
  `stat_id` TINYINT(3) UNSIGNED NOT NULL,
  `value` DECIMAL(7,2) DEFAULT 0.00,
  `extra` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `pos` TINYINT(3) UNSIGNED DEFAULT 0,
  `pos_tied` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`rider_id`,`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
