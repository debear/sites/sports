##
## Base AHL standings
##
DROP PROCEDURE IF EXISTS `ahl_standings_calcs_main`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_calcs_main`()
    COMMENT 'Establish AHL teams W/L/(OT|SO)L records'
BEGIN

  # Home Games
  INSERT INTO tmp_standings (season, the_date, team_id, home_wins, home_loss, home_ot_loss, home_so_loss)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS home_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS home_loss,
           IFNULL(SUM(tmp_sched.result = 'otl'), 0) AS home_ot_loss,
           IFNULL(SUM(tmp_sched.result = 'sol'), 0) AS home_so_loss
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.venue = 'home'
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE home_wins = VALUES(home_wins),
                          home_loss = VALUES(home_loss),
                          home_ot_loss = VALUES(home_ot_loss),
                          home_so_loss = VALUES(home_so_loss);

  # Road Games
  INSERT INTO tmp_standings (season, the_date, team_id, visitor_wins, visitor_loss, visitor_ot_loss, visitor_so_loss)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS visitor_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS visitor_loss,
           IFNULL(SUM(tmp_sched.result = 'otl'), 0) AS visitor_ot_loss,
           IFNULL(SUM(tmp_sched.result = 'sol'), 0) AS visitor_so_loss
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.venue = 'visitor'
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE visitor_wins = VALUES(visitor_wins),
                          visitor_loss = VALUES(visitor_loss),
                          visitor_ot_loss = VALUES(visitor_ot_loss),
                          visitor_so_loss = VALUES(visitor_so_loss);

  # Calculate totals based on home/road split
  UPDATE tmp_date_list
  JOIN tmp_standings
    ON (tmp_standings.season = tmp_date_list.season
    AND tmp_standings.the_date = tmp_date_list.the_date)
  SET tmp_standings.wins    = tmp_standings.home_wins    + tmp_standings.visitor_wins,
      tmp_standings.loss    = tmp_standings.home_loss    + tmp_standings.visitor_loss,
      tmp_standings.ot_loss = tmp_standings.home_ot_loss + tmp_standings.visitor_ot_loss,
      tmp_standings.so_loss = tmp_standings.home_so_loss + tmp_standings.visitor_so_loss;

  # Absolute points
  UPDATE tmp_date_list
  JOIN tmp_standings
    ON (tmp_standings.season = tmp_date_list.season
    AND tmp_standings.the_date = tmp_date_list.the_date)
  JOIN tmp_teams_max_gp
    ON (tmp_teams_max_gp.team_id = tmp_standings.team_id)
  SET tmp_standings.pts     = (2 * tmp_standings.wins) + tmp_standings.ot_loss + tmp_standings.so_loss,
      tmp_standings.max_pts = (2 * tmp_standings.wins) + tmp_standings.ot_loss + tmp_standings.so_loss
                               + ((tmp_teams_max_gp.max_gp - tmp_standings.wins - tmp_standings.loss - tmp_standings.ot_loss - tmp_standings.so_loss) * 2);

  # Point Percentages
  UPDATE tmp_date_list
  JOIN tmp_standings
    ON (tmp_standings.season = tmp_date_list.season
    AND tmp_standings.the_date = tmp_date_list.the_date)
  JOIN tmp_teams_max_gp
    ON (tmp_teams_max_gp.team_id = tmp_standings.team_id)
  SET tmp_standings.pts_pct     = tmp_standings.pts / IF(tmp_standings.wins + tmp_standings.loss + tmp_standings.ot_loss + tmp_standings.so_loss = 0, 1, -- Div by zero check...
                                                         (2 * (tmp_standings.wins + tmp_standings.loss + tmp_standings.ot_loss + tmp_standings.so_loss))),
      tmp_standings.max_pts_pct = (tmp_standings.max_pts / (2 * tmp_teams_max_gp.max_gp)),
      tmp_standings._tb_worst_pts_pct = (tmp_standings.pts / (2 * tmp_teams_max_gp.max_gp));

END $$

DELIMITER ;

##
## Shootout Wins
##
DROP PROCEDURE IF EXISTS `ahl_standings_calcs_shootout`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_calcs_shootout`()
    COMMENT 'Establish AHL teams shootout wins'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_standings_so_wins;
  CREATE TEMPORARY TABLE tmp_standings_so_wins (
    season SMALLINT UNSIGNED NOT NULL,
    the_date DATE NOT NULL,
    team_id VARCHAR(3) NOT NULL,
    wins_so TINYINT(3) UNSIGNED DEFAULT 0,
    PRIMARY KEY (season, the_date, team_id)
  ) ENGINE = MEMORY
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS wins_so
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.status = 'SO'
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id;

  UPDATE tmp_date_list
  JOIN tmp_standings_so_wins
    ON (tmp_standings_so_wins.season = tmp_date_list.season
    AND tmp_standings_so_wins.the_date = tmp_date_list.the_date)
  JOIN tmp_standings
    ON (tmp_standings.season = tmp_standings_so_wins.season
    AND tmp_standings.the_date = tmp_standings_so_wins.the_date
    AND tmp_standings.team_id = tmp_standings_so_wins.team_id)
  JOIN tmp_teams_max_gp
    ON (tmp_teams_max_gp.team_id = tmp_standings.team_id)
  SET tmp_standings.reg_ot_wins = tmp_standings.wins - tmp_standings_so_wins.wins_so,
      tmp_standings._tb_max_reg_ot_wins = tmp_teams_max_gp.max_gp - tmp_standings_so_wins.wins_so - tmp_standings.loss - tmp_standings.ot_loss - tmp_standings.so_loss;

END $$

DELIMITER ;

##
## Division Record
##
DROP PROCEDURE IF EXISTS `ahl_standings_calcs_div`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_calcs_div`()
    COMMENT 'Establish AHL teams divisional records'
BEGIN

  INSERT INTO tmp_standings (season, the_date, team_id, div_wins, div_loss, div_ot_loss, div_so_loss)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS div_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS div_loss,
           IFNULL(SUM(tmp_sched.result = 'otl'), 0) AS div_ot_loss,
           IFNULL(SUM(tmp_sched.result = 'sol'), 0) AS div_so_loss
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_div_opp = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE div_wins = VALUES(div_wins),
                          div_loss = VALUES(div_loss),
                          div_ot_loss = VALUES(div_ot_loss),
                          div_so_loss = VALUES(div_so_loss);

END $$

DELIMITER ;

