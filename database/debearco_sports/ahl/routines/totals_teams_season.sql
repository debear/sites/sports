##
## Team season totals
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams_season`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_season`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for AHL teams'
BEGIN

  CALL `ahl_totals_teams_season_skaters`(v_season, v_game_type);
  CALL `ahl_totals_teams_season_goalies`(v_season, v_game_type);

END $$

DELIMITER ;

##
## Team season totals table maintenance
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams_season_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_season_order`()
    COMMENT 'Order the AHL team total tables'
BEGIN

  ALTER TABLE SPORTS_AHL_TEAMS_SEASON_SKATERS ORDER BY season, season_type, team_id, stat_dir;
  ALTER TABLE SPORTS_AHL_TEAMS_SEASON_GOALIES ORDER BY season, season_type, team_id, stat_dir;

END $$

DELIMITER ;

