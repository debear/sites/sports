##
## Base AHL standings
##
DROP PROCEDURE IF EXISTS `ahl_standings_calcs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_calcs`(
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'AHL standings calculations'
BEGIN

  # Standings columns
  CALL ahl_standings_calcs_main();
  CALL ahl_standings_calcs_shootout();
  CALL ahl_standings_calcs_div();
  CALL ahl_standings_calcs_recent(v_recent_games);
  CALL ahl_standings_calcs_misc();

  # Tie-break facilitators
  CALL ahl_standings_calcs_tiebreak();

END $$

DELIMITER ;

