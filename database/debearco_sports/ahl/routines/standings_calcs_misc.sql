##
## Misc standings info
##
DROP PROCEDURE IF EXISTS `ahl_standings_calcs_misc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_calcs_misc`()
    COMMENT 'Calculate GF/GA in AHL standings'
BEGIN

  # Goals Scored
  INSERT INTO tmp_standings (season, the_date, team_id, goals_for, goals_against)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.goals_for), 0) AS goals_for,
           IFNULL(SUM(tmp_sched.goals_against), 0) AS goals_against
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE goals_for = VALUES(goals_for),
                          goals_against = VALUES(goals_against);

END $$

DELIMITER ;

