##
## Recent Games
##
DROP PROCEDURE IF EXISTS `ahl_standings_calcs_recent`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_calcs_recent`(
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate AHL standings over the last v_recent_games games'
BEGIN

  # Get the appropriate game counter for each team for each date
  DROP TEMPORARY TABLE IF EXISTS tmp_standings_recent_max;
  CREATE TEMPORARY TABLE tmp_standings_recent_max (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    min_game_num TINYINT UNSIGNED,
    max_game_num TINYINT UNSIGNED,
    PRIMARY KEY (season, the_date, team_id),
    INDEX (team_id, min_game_num, max_game_num)
  ) ENGINE = MEMORY
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           0 AS min_game_num,
           IFNULL(MAX(tmp_sched.game_num), 0) AS max_game_num
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id;

  # And the games we need to include
  CALL _duplicate_tmp_table('tmp_standings_recent_max', 'tmp_standings_recent_num');
  UPDATE tmp_standings_recent_num
  SET min_game_num = IF(max_game_num < v_recent_games, 0, max_game_num - v_recent_games + 1); # +1 because Last 10 => 6..15, not 5..15

  # And now calculate
  INSERT INTO tmp_standings (season, the_date, team_id, recent_wins, recent_loss, recent_ot_loss, recent_so_loss)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           IFNULL(SUM(tmp_sched.result = 'w'), 0) AS recent_wins,
           IFNULL(SUM(tmp_sched.result = 'l'), 0) AS recent_loss,
           IFNULL(SUM(tmp_sched.result = 'otl'), 0) AS recent_ot_loss,
           IFNULL(SUM(tmp_sched.result = 'sol'), 0) AS recent_so_loss
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_standings_recent_num
      ON (tmp_standings_recent_num.season = tmp_date_list.season
      AND tmp_standings_recent_num.the_date = tmp_date_list.the_date
      AND tmp_standings_recent_num.team_id = tmp_teams.team_id)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_standings_recent_num.team_id
      AND tmp_sched.game_num BETWEEN tmp_standings_recent_num.min_game_num AND tmp_standings_recent_num.max_game_num)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE recent_wins = VALUES(recent_wins),
                          recent_loss = VALUES(recent_loss),
                          recent_ot_loss = VALUES(recent_ot_loss),
                          recent_so_loss = VALUES(recent_so_loss);

END $$

DELIMITER ;

