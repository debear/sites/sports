## ######################### ##
## Calder Cup Final Matchups ##
## ######################### ##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_cup_final`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_cup_final`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff CCF matchup calcs'
BEGIN

  DECLARE v_conf_a TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_conf_b TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT conf_id, seed INTO v_conf_a, v_seed_a FROM tmp_seeds ORDER BY pos_league  ASC LIMIT 1;
  SELECT conf_id, seed INTO v_conf_b, v_seed_b FROM tmp_seeds ORDER BY pos_league DESC LIMIT 1;

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(40 + v_counter + 1 AS CHAR(2)), v_conf_a, v_seed_a, v_conf_b, v_seed_b, 0, 0, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;
