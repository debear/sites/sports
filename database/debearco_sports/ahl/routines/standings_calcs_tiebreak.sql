##
## Standings info for tie-breaking
##
# See: http://theahl.com/qualification-rules-s11572
DROP PROCEDURE IF EXISTS `ahl_standings_calcs_tiebreak`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_calcs_tiebreak`()
    COMMENT 'Tie-break data in AHL standings'
BEGIN

  # Note: We can only deal with absolutes at this stage, as "points in season series" tie-break needs to know the teams involved, which we do not at this stage

  # League / Conf / Div IDs
  UPDATE tmp_standings
  JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_standings.team_id)
  SET tmp_standings._tb_league_id = tmp_teams.league_id,
      tmp_standings._tb_conf_id = tmp_teams.conf_id,
      tmp_standings._tb_div_id = tmp_teams.div_id;

  # Games Played / Goal Differential
  UPDATE tmp_standings
  SET _tb_gp = wins + loss + ot_loss + so_loss,
      _tb_goal_diff = CAST(goals_for AS SIGNED) - CAST(goals_against AS SIGNED);

  # Regulation wins
  INSERT INTO tmp_standings (season, the_date, team_id, _tb_reg_wins)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           SUM(tmp_sched.result = 'w' AND tmp_sched.status = 'F') AS _tb_reg_wins
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE _tb_reg_wins = VALUES(_tb_reg_wins);

  # Win percentages
  UPDATE tmp_standings
  SET _tb_win_reg_pct = IF(_tb_gp > 0, _tb_reg_wins / _tb_gp, 0),
      _tb_win_row_pct = IF(_tb_gp > 0, reg_ot_wins / _tb_gp, 0),
      _tb_win_all_pct = IF(_tb_gp > 0, wins / _tb_gp, 0);

  # Intra-Conference Points Percentage (intra == "within")
  INSERT INTO tmp_standings (season, the_date, team_id, _tb_intra_conf_pts, _tb_intra_conf_pts_pct)
    SELECT tmp_date_list.season,
           tmp_date_list.the_date,
           tmp_teams.team_id,
           (2 * IFNULL(SUM(tmp_sched.result = 'w'), 0)) + IFNULL(SUM(tmp_sched.result IN ('otl', 'sol')), 0) AS _tb_intra_conf_pts,
           IF(COUNT(tmp_sched.game_id) = 0, 0,
              ((2 * IFNULL(SUM(tmp_sched.result = 'w'), 0)) + IFNULL(SUM(tmp_sched.result IN ('otl', 'sol')), 0)) / COUNT(tmp_sched.game_id)) AS _tb_intra_conf_pts_pct
    FROM tmp_date_list
    JOIN tmp_teams ON (1 = 1)
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id
      AND tmp_sched.is_conf_opp = 1
      AND tmp_sched.game_date <= tmp_date_list.the_date)
    GROUP BY tmp_date_list.season,
             tmp_date_list.the_date,
             tmp_teams.team_id
  ON DUPLICATE KEY UPDATE _tb_intra_conf_pts = VALUES(_tb_intra_conf_pts),
                          _tb_intra_conf_pts_pct = VALUES(_tb_intra_conf_pts_pct);

END $$

DELIMITER ;

