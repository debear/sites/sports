##
## Player totals sorted
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for AHL players'
BEGIN

  # Broken down by individual routines
  CALL ahl_totals_players_season_sort_skaters(v_season, v_season_type);
  CALL ahl_totals_players_season_sort_goalies(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_players_season_sort_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_sort_skaters`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort skater season totals for AHL players'
BEGIN

  DELETE FROM SPORTS_AHL_PLAYERS_SEASON_SKATERS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL ahl_totals_players_sort__tmp(v_season, v_season_type, 'SKATERS',
                                    'goals TINYINT UNSIGNED,
                                     assists TINYINT UNSIGNED,
                                     points TINYINT UNSIGNED,
                                     plus_minus TINYINT SIGNED,
                                     pims SMALLINT UNSIGNED,
                                     pp_goals TINYINT UNSIGNED,
                                     pp_assists TINYINT UNSIGNED,
                                     pp_points TINYINT UNSIGNED,
                                     sh_goals TINYINT UNSIGNED,
                                     sh_assists TINYINT UNSIGNED,
                                     sh_points TINYINT UNSIGNED,
                                     gw_goals TINYINT UNSIGNED,
                                     shots SMALLINT UNSIGNED,
                                     shot_pct DECIMAL(6,5),
                                     so_goals TINYINT UNSIGNED,
                                     so_shots TINYINT UNSIGNED,
                                     so_pct DECIMAL(6,5),
                                     qual_shot TINYINT UNSIGNED',
                                    'SUM(goals) AS goals,
                                     SUM(assists) AS assists,
                                     SUM(goals) + SUM(assists) AS points,
                                     SUM(plus_minus) AS plus_minus,
                                     SUM(pims) AS pims,
                                     SUM(pp_goals) AS pp_goals,
                                     SUM(pp_assists) AS pp_assists,
                                     SUM(pp_goals) + SUM(pp_assists) AS pp_points,
                                     SUM(sh_goals) AS sh_goals,
                                     SUM(sh_assists) AS sh_assists,
                                     SUM(sh_goals) + SUM(sh_assists) AS sh_points,
                                     SUM(gw_goals) AS gw_goals,
                                     SUM(shots) AS shots,
                                     IF(SUM(shots) > 0, SUM(goals) / SUM(shots), 0) AS shot_pct,
                                     SUM(so_goals) AS so_goals,
                                     SUM(so_shots) AS so_shots,
                                     IF(SUM(so_shots) > 0, SUM(so_goals) / SUM(so_shots), 0) AS so_pct,
                                     0 AS qual_shot');

  # Shooting and Faceoff sorting qualification
  CALL ahl_totals_players_sort__tmp_qual_skaters(v_season, v_season_type);

  # Calcs
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'points', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'plus_minus', 'TINYINT SIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pims', 'SMALLINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_points', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_points', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'gw_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'shots', 'SMALLINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'shot_pct', 'DECIMAL(6,5)', '>', 'qual_shot');
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'shot_pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'so_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'so_shots', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'so_pct', 'DECIMAL(6,5)', '>', NULL);

  # Order
  ALTER TABLE SPORTS_AHL_PLAYERS_SEASON_SKATERS_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_players_season_sort_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_sort_goalies`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort goalie season totals for AHL players'
BEGIN

  DELETE FROM SPORTS_AHL_PLAYERS_SEASON_GOALIES_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge the data
  CALL ahl_totals_players_sort__tmp(v_season, v_season_type, 'GOALIES',
                                    'gp TINYINT UNSIGNED,
                                     starts TINYINT UNSIGNED,
                                     win TINYINT UNSIGNED,
                                     loss TINYINT UNSIGNED,
                                     ot_loss TINYINT UNSIGNED,
                                     so_loss TINYINT UNSIGNED,
                                     goals_against TINYINT UNSIGNED,
                                     shots_against SMALLINT UNSIGNED,
                                     saves SMALLINT UNSIGNED,
                                     save_pct DECIMAL(6,5),
                                     goals_against_avg DECIMAL(8,5),
                                     shutout TINYINT UNSIGNED,
                                     minutes_played MEDIUMINT UNSIGNED,
                                     so_goals_against TINYINT UNSIGNED,
                                     so_shots_against TINYINT UNSIGNED,
                                     so_saves TINYINT UNSIGNED,
                                     so_save_pct DECIMAL(6,5),
                                     goals TINYINT UNSIGNED,
                                     assists TINYINT UNSIGNED,
                                     points TINYINT UNSIGNED,
                                     pims TINYINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'SUM(gp) AS gp,
                                     SUM(starts) AS starts,
                                     SUM(win) AS win,
                                     SUM(loss) AS loss,
                                     SUM(ot_loss) AS ot_loss,
                                     SUM(so_loss) AS so_loss,
                                     SUM(goals_against) AS goals_against,
                                     SUM(shots_against) AS shots_against,
                                     SUM(shots_against) - SUM(goals_against) AS saves,
                                     IF(SUM(shots_against) > 0, (SUM(shots_against) - SUM(goals_against)) / SUM(shots_against), 0) AS save_pct,
                                     IF(SUM(minutes_played) > 0, (SUM(goals_against) * 3600) / SUM(minutes_played), 0) AS goals_against_avg,
                                     SUM(shutout) AS shutout,
                                     SUM(minutes_played) AS minutes_played,
                                     SUM(so_goals_against) AS so_goals_against,
                                     SUM(so_shots_against) AS so_shots_against,
                                     SUM(so_shots_against) - SUM(so_goals_against) AS so_saves,
                                     IF(SUM(so_shots_against) > 0, (SUM(so_shots_against) - SUM(so_goals_against)) / SUM(so_shots_against), 0) AS so_save_pct,
                                     SUM(goals) AS goals,
                                     SUM(assists) AS assists,
                                     SUM(goals) + SUM(assists) AS points,
                                     SUM(pims) AS pims,
                                     0 AS qual');

  # Games Played Qualification
  CALL ahl_totals_players_sort__tmp_qual_goalies(v_season, v_season_type);

  # Calcs
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'gp', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'starts', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'win', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'loss', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'ot_loss', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_loss', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against', 'TINYINT UNSIGNED', '<', 'qual');
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_all', 'TINYINT UNSIGNED', '<', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'shots_against', 'SMALLINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'saves', 'SMALLINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'save_pct', 'DECIMAL(6,5)', '>', 'qual');
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'save_pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_avg', 'DECIMAL(8,5)', '<', 'qual');
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_avg_all', 'DECIMAL(8,5)', '<', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'shutout', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'minutes_played', 'MEDIUMINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_goals_against', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_shots_against', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_saves', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_save_pct', 'DECIMAL(6,5)', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'points', 'TINYINT UNSIGNED', '>', NULL);
  CALL ahl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'pims', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_AHL_PLAYERS_SEASON_GOALIES_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_players_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_cols VARCHAR(2048),
  v_calcs VARCHAR(2560)
)
    COMMENT 'Create temporary table for AHL player season stat sort'
BEGIN

  # Get the stats
  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table));
  # When first creating the table, get player total - one row per player
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      player_id SMALLINT UNSIGNED NOT NULL,
      team_id VARCHAR(3) NOT NULL,
      ', v_cols, ',
      PRIMARY KEY (season, season_type, player_id, team_id)
    ) ENGINE = MEMORY
      SELECT season, season_type, player_id, \'_LG\' AS team_id, ', v_calcs, '
      FROM SPORTS_AHL_PLAYERS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '"
      GROUP BY season, season_type, player_id;'));
  # But then also get team_id grouped stats for each player
  CALL _exec(CONCAT(
   'INSERT INTO tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '
      SELECT season, season_type, player_id, team_id, ', v_calcs, '
      FROM SPORTS_AHL_PLAYERS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '"
      GROUP BY season, season_type, player_id, team_id;'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_players_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col_sort VARCHAR(25),
  v_col_def VARCHAR(50),
  v_sort_dir VARCHAR(5),
  v_qual  VARCHAR(10)
)
    COMMENT 'Sort season totals for AHL players'
BEGIN

  DECLARE v_col_data VARCHAR(25);
  DECLARE v_col_qual_flag VARCHAR(25) DEFAULT NULL;

  # Data column (which may not be v_col_sort)
  IF SUBSTRING(v_col_sort, -4) = '_all' THEN
    SET v_col_data = LEFT(v_col_sort, LENGTH(v_col_sort) - 4);
  ELSE
    SET v_col_data = v_col_sort;
  END IF;

  # Storing the qual flag?
  IF v_qual IS NOT NULL THEN
    SET v_col_qual_flag = CONCAT(v_col_data, '_qual');
  END IF;

  # Determine unique values for this column for sorting
  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT;'));
  CALL _exec(CONCAT(
    'CREATE TEMPORARY TABLE tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT (
       team_id VARCHAR(3) NOT NULL,
       `', v_col_data, '` ', v_col_def, ',
       ', IF(v_qual IS NOT NULL, CONCAT('`', v_qual, '` TINYINT UNSIGNED,'), ''), '
       num_players SMALLINT UNSIGNED,
       value_order SMALLINT UNSIGNED,
       PRIMARY KEY (team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ')
     ) ENGINE = MEMORY
       SELECT team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', COUNT(player_id) AS num_players, 9999 AS value_order
       FROM tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '
       GROUP BY team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ';'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT_cpA'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT_cpB'));

  # Sort these values
  CALL _exec(CONCAT(
    'INSERT INTO tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT (team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', value_order)
       SELECT A.team_id, A.', v_col_data, ', ', IF(v_qual IS NOT NULL, CONCAT('A.', v_qual, ', '), ''), 'IFNULL(SUM(B.num_players), 0) + 1 AS value_order
       FROM tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT_cpA AS A
       LEFT JOIN tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT_cpB AS B
         ON (B.team_id = A.team_id
         AND (', IF(v_qual IS NOT NULL,
                    CONCAT('B.', v_qual, ' > A.', v_qual, ' OR (B.', v_qual, ' = A.', v_qual, ' AND '), ''),
              'B.', v_col_data, ' ', v_sort_dir, ' A.', v_col_data,
              IF (v_qual IS NOT NULL, ')', ''), '))
       GROUP BY A.team_id, A.', v_col_data, IF(v_qual IS NOT NULL, CONCAT(', A.', v_qual), ''), '
     ON DUPLICATE KEY UPDATE value_order = VALUES(value_order);'));

  # Update sorting for the players
  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_AHL_PLAYERS_SEASON_', v_table, '_SORTED (season, season_type, player_id, team_id, `', v_col_sort, '`', IF(v_col_qual_flag IS NOT NULL, CONCAT(', `', v_col_qual_flag, '`'), ''), ')
      SELECT PLAYER.season, PLAYER.season_type, PLAYER.player_id, PLAYER.team_id,
             STAT.value_order AS `', v_col_sort, '`',
             IF(v_col_qual_flag IS NOT NULL, CONCAT(', STAT.', v_qual, ' AS `', v_col_qual_flag, '`'), ''), '
      FROM tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, ' AS PLAYER
      JOIN tmp_SPORTS_AHL_PLAYERS_SEASON_', v_table, '_STAT AS STAT
        ON (STAT.team_id = PLAYER.team_id
        AND STAT.', v_col_data, ' = PLAYER.', v_col_data,
            IF(v_qual IS NOT NULL,
               CONCAT(' AND STAT.', v_qual, '= PLAYER.', v_qual), ''), ')
      WHERE PLAYER.season = ', v_season, '
      AND   PLAYER.season_type = "', v_season_type, '"
    ON DUPLICATE KEY UPDATE `', v_col_sort, '` = VALUES(`', v_col_sort, '`)',
                            IF(v_col_qual_flag IS NOT NULL, CONCAT(', `', v_col_qual_flag, '` = VALUES(`', v_col_qual_flag, '`)'), ''), ';'));

END $$

DELIMITER ;

