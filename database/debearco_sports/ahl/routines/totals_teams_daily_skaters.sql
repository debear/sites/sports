##
## Player daily game totals
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams_daily_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_daily_skaters`()
    COMMENT 'Calculate daily game totals for AHL team skaters'
BEGIN

  ##
  ## Combine player totals for the teams
  ##
  INSERT INTO SPORTS_AHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, gp, goals, assists, plus_minus, pims, pp_goals, pp_assists, sh_goals, sh_assists, shots, so_goals, so_shots)
    SELECT SPORTS_AHL_PLAYERS_GAME_SKATERS.season,
           SPORTS_AHL_PLAYERS_GAME_SKATERS.team_id,
           SPORTS_AHL_PLAYERS_GAME_SKATERS.game_type,
           SPORTS_AHL_PLAYERS_GAME_SKATERS.game_id,
           'for' AS stat_dir,
           1 AS gp,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.goals) AS goals,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.assists) AS assists,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.plus_minus) AS plus_minus,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.pims) AS pims,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.pp_goals) AS pp_goals,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.pp_assists) AS pp_assists,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.sh_goals) AS sh_goals,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.sh_assists) AS sh_assists,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.shots) AS shots,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.so_goals) AS so_goals,
           SUM(SPORTS_AHL_PLAYERS_GAME_SKATERS.so_shots) AS so_shots
    FROM tmp_game_list
    JOIN SPORTS_AHL_PLAYERS_GAME_SKATERS
      ON (SPORTS_AHL_PLAYERS_GAME_SKATERS.season = tmp_game_list.season
      AND SPORTS_AHL_PLAYERS_GAME_SKATERS.game_type = tmp_game_list.game_type
      AND SPORTS_AHL_PLAYERS_GAME_SKATERS.game_id = tmp_game_list.game_id)
    GROUP BY SPORTS_AHL_PLAYERS_GAME_SKATERS.season,
             SPORTS_AHL_PLAYERS_GAME_SKATERS.team_id,
             SPORTS_AHL_PLAYERS_GAME_SKATERS.game_type,
             SPORTS_AHL_PLAYERS_GAME_SKATERS.game_id
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          plus_minus = VALUES(plus_minus),
                          pims = VALUES(pims),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          shots = VALUES(shots),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots);

  ##
  ## Goalie scoring / pens
  ##
  INSERT INTO SPORTS_AHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, gp, goals, assists, pims)
    SELECT SPORTS_AHL_PLAYERS_GAME_GOALIES.season,
           SPORTS_AHL_PLAYERS_GAME_GOALIES.team_id,
           SPORTS_AHL_PLAYERS_GAME_GOALIES.game_type,
           SPORTS_AHL_PLAYERS_GAME_GOALIES.game_id,
           'for' AS stat_dir,
           1 AS gp,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.goals) AS goals,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.assists) AS assists,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.pims) AS pims
    FROM tmp_game_list
    JOIN SPORTS_AHL_PLAYERS_GAME_GOALIES
      ON (SPORTS_AHL_PLAYERS_GAME_GOALIES.season = tmp_game_list.season
      AND SPORTS_AHL_PLAYERS_GAME_GOALIES.game_type = tmp_game_list.game_type
      AND SPORTS_AHL_PLAYERS_GAME_GOALIES.game_id = tmp_game_list.game_id)
    GROUP BY SPORTS_AHL_PLAYERS_GAME_GOALIES.season,
             SPORTS_AHL_PLAYERS_GAME_GOALIES.team_id,
             SPORTS_AHL_PLAYERS_GAME_GOALIES.game_type,
             SPORTS_AHL_PLAYERS_GAME_GOALIES.game_id
  ON DUPLICATE KEY UPDATE goals = goals + VALUES(goals),
                          assists = assists + VALUES(assists),
                          pims = pims + VALUES(pims);

  ##
  ## Other composite stats
  ##
  INSERT INTO SPORTS_AHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, pp_opps, pk_goals, pk_kills, pk_opps)
    SELECT PP.season,
           PP.team_id,
           PP.game_type,
           PP.game_id,
           'for' AS stat_dir,
           PP.pp_opps,
           PK.pp_goals AS pk_goals,
           PK.pp_opps - PK.pp_goals AS pk_opps,
           PK.pp_opps AS pk_opps
    FROM tmp_game_list
    JOIN SPORTS_AHL_GAME_PP_STATS AS PP
      ON (PP.season = tmp_game_list.season
      AND PP.game_type = tmp_game_list.game_type
      AND PP.game_id = tmp_game_list.game_id
      AND PP.pp_type = 'pp')
    JOIN SPORTS_AHL_GAME_PP_STATS AS PK
      ON (PK.season = tmp_game_list.season
      AND PK.game_type = tmp_game_list.game_type
      AND PK.game_id = tmp_game_list.game_id
      AND PK.team_id = PP.team_id
      AND PK.pp_type = 'pk')
    GROUP BY PP.season,
             PP.team_id,
             PP.game_type,
             PP.game_id
  ON DUPLICATE KEY UPDATE pp_opps = VALUES(pp_opps),
                          pk_goals = VALUES(pk_goals),
                          pk_kills = VALUES(pk_kills),
                          pk_opps = VALUES(pk_opps);

  ##
  ## And get oppositions totals for these...
  ##
  INSERT INTO SPORTS_AHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, gp, goals, assists, plus_minus, pims, pp_goals, pp_assists, pp_opps, pk_goals, pk_kills, pk_opps, sh_goals, sh_assists, shots, so_goals, so_shots)
    SELECT ME.season, ME.team_id, ME.game_type, ME.game_id,
           'against' AS stat_dir,
           THEM.gp,
           THEM.goals,
           THEM.assists,
           THEM.plus_minus,
           THEM.pims,
           THEM.pp_goals,
           THEM.pp_assists,
           THEM.pp_opps,
           THEM.pk_goals,
           THEM.pk_kills,
           THEM.pk_opps,
           THEM.sh_goals,
           THEM.sh_assists,
           THEM.shots,
           THEM.so_goals,
           THEM.so_shots
    FROM tmp_game_list
    JOIN SPORTS_AHL_TEAMS_GAME_SKATERS AS ME
      ON (ME.season = tmp_game_list.season
      AND ME.game_type = tmp_game_list.game_type
      AND ME.game_id = tmp_game_list.game_id
      AND ME.stat_dir = 'for')
    JOIN SPORTS_AHL_SCHEDULE
      ON (SPORTS_AHL_SCHEDULE.season = ME.season
      AND SPORTS_AHL_SCHEDULE.game_type = ME.game_type
      AND SPORTS_AHL_SCHEDULE.game_id = ME.game_id)
    JOIN SPORTS_AHL_TEAMS_GAME_SKATERS AS THEM
      ON (THEM.season = SPORTS_AHL_SCHEDULE.season
      AND THEM.team_id = IF(ME.team_id = SPORTS_AHL_SCHEDULE.home, SPORTS_AHL_SCHEDULE.visitor, SPORTS_AHL_SCHEDULE.home)
      AND THEM.game_type = SPORTS_AHL_SCHEDULE.game_type
      AND THEM.game_id = SPORTS_AHL_SCHEDULE.game_id
      AND THEM.stat_dir = 'for')
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          plus_minus = VALUES(plus_minus),
                          pims = VALUES(pims),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          pp_opps = VALUES(pp_opps),
                          pk_goals = VALUES(pk_goals),
                          pk_kills = VALUES(pk_kills),
                          pk_opps = VALUES(pk_opps),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          shots = VALUES(shots),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots);

END $$

DELIMITER ;

