##
## Playoff seeds
##
DROP PROCEDURE IF EXISTS `ahl_playoff_seeds`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_seeds`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate AHL playoff seeds'
proc: BEGIN

  DECLARE v_standing_date DATE;
  DECLARE v_outstanding SMALLINT UNSIGNED;

  # Does not apply in 2019/20 when no playoffs were held
  IF v_season = 2019 THEN
    LEAVE proc;
  END IF;

  SELECT MAX(the_date) INTO v_standing_date FROM SPORTS_AHL_STANDINGS WHERE season = v_season;

  # Clear previous run
  DELETE FROM SPORTS_AHL_PLAYOFF_SEEDS WHERE season = v_season;

  # Determine if in-season or after the season was finished
  SELECT COUNT(*) INTO v_outstanding FROM SPORTS_AHL_SCHEDULE WHERE season = v_season AND game_type = 'regular' AND status IS NULL;

  # 2020/21 specific - Pacific Division only
  IF v_season = 2020 THEN
    INSERT INTO SPORTS_AHL_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
      SELECT SPORTS_AHL_STANDINGS.season,
             SPORTS_AHL_GROUPINGS.parent_id AS conf_id,
             SPORTS_AHL_STANDINGS.pos_div AS seed,
             SPORTS_AHL_STANDINGS.team_id,
             SPORTS_AHL_STANDINGS.pos_div,
             SPORTS_AHL_STANDINGS.pos_div AS pos_conf,
             SPORTS_AHL_STANDINGS.pos_div AS pos_league
      FROM SPORTS_AHL_STANDINGS
      JOIN SPORTS_AHL_TEAMS_GROUPINGS
        ON (SPORTS_AHL_TEAMS_GROUPINGS.team_id = SPORTS_AHL_STANDINGS.team_id
        AND v_season BETWEEN SPORTS_AHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_AHL_TEAMS_GROUPINGS.season_to, 2099))
      JOIN SPORTS_AHL_GROUPINGS
        ON (SPORTS_AHL_GROUPINGS.grouping_id = SPORTS_AHL_TEAMS_GROUPINGS.grouping_id
        AND SPORTS_AHL_GROUPINGS.grouping_id = 64) # The 2020/21 Pacific Division only
      WHERE SPORTS_AHL_STANDINGS.season = v_season
      AND   SPORTS_AHL_STANDINGS.the_date = v_standing_date;

  # Calculate - in-season
  ELSEIF v_outstanding > 0 THEN
    # Calculate by hand
    IF v_season < 2020 THEN
      # Up to 2019/20: Top 4 in each division qualify
      CALL ahl_playoff_seeds_div_top4(v_season, v_standing_date);
    ELSE
      # From 2021/22: All bar bottom 2 in each division qualify
      CALL ahl_playoff_seeds_div_exp2021(v_season, v_standing_date);
    END IF;

  # Calculate - after season
  ELSE
    INSERT INTO SPORTS_AHL_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
      SELECT SPORTS_AHL_STANDINGS.season,
             SPORTS_AHL_GROUPINGS.parent_id AS conf_id,
             SPORTS_AHL_STANDINGS.pos_conf AS seed,
             SPORTS_AHL_STANDINGS.team_id,
             SPORTS_AHL_STANDINGS.pos_div,
             SPORTS_AHL_STANDINGS.pos_conf,
             SPORTS_AHL_STANDINGS.pos_league
      FROM SPORTS_AHL_STANDINGS
      JOIN SPORTS_AHL_TEAMS_GROUPINGS
        ON (SPORTS_AHL_TEAMS_GROUPINGS.team_id = SPORTS_AHL_STANDINGS.team_id
        AND v_season BETWEEN SPORTS_AHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_AHL_TEAMS_GROUPINGS.season_to, 2099))
      JOIN SPORTS_AHL_GROUPINGS
        ON (SPORTS_AHL_GROUPINGS.grouping_id = SPORTS_AHL_TEAMS_GROUPINGS.grouping_id)
      WHERE SPORTS_AHL_STANDINGS.season = v_season
      AND   SPORTS_AHL_STANDINGS.the_date = v_standing_date
      AND   SPORTS_AHL_STANDINGS.status_code IS NOT NULL;
  END IF;

  # Final ordering
  ALTER TABLE SPORTS_AHL_PLAYOFF_SEEDS ORDER BY season, conf_id, seed;

END $$

DELIMITER ;

##
## In-season calcs when Top 4 in division qualify
##
DROP PROCEDURE IF EXISTS `ahl_playoff_seeds_div_top4`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_seeds_div_top4`(
  v_season SMALLINT UNSIGNED,
  v_standing_date DATE
)
    COMMENT 'Calculate AHL playoff seeds: Top 4 in each division'
BEGIN

  # Get
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    season SMALLINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    seed TINYINT UNSIGNED,
    team_id CHAR(3),
    pos_div TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    PRIMARY KEY (season, conf_id, team_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_AHL_STANDINGS.season,
           SPORTS_AHL_GROUPINGS.parent_id AS conf_id,
           0 AS seed,
           SPORTS_AHL_STANDINGS.team_id,
           SPORTS_AHL_STANDINGS.pos_div,
           SPORTS_AHL_STANDINGS.pos_conf,
           SPORTS_AHL_STANDINGS.pos_league
    FROM SPORTS_AHL_STANDINGS
    JOIN SPORTS_AHL_TEAMS_GROUPINGS
      ON (SPORTS_AHL_TEAMS_GROUPINGS.team_id = SPORTS_AHL_STANDINGS.team_id
      AND v_season BETWEEN SPORTS_AHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_AHL_TEAMS_GROUPINGS.season_to, 2099))
    JOIN SPORTS_AHL_GROUPINGS
      ON (SPORTS_AHL_GROUPINGS.grouping_id = SPORTS_AHL_TEAMS_GROUPINGS.grouping_id)
    WHERE SPORTS_AHL_STANDINGS.season = v_season
    AND   SPORTS_AHL_STANDINGS.the_date = v_standing_date
    AND   SPORTS_AHL_STANDINGS.pos_div <= 4;

  # Sort
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cpA');
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cpB');
  INSERT INTO tmp_teams (season, conf_id, team_id, seed)
    SELECT tmp_teams_cpA.season, tmp_teams_cpA.conf_id, tmp_teams_cpA.team_id,
           COUNT(tmp_teams_cpB.team_id) + 1 AS seed
    FROM tmp_teams_cpA
    LEFT JOIN tmp_teams_cpB
      ON (tmp_teams_cpB.season = tmp_teams_cpA.season
      AND tmp_teams_cpB.conf_id = tmp_teams_cpA.conf_id
      AND tmp_teams_cpB.pos_conf < tmp_teams_cpA.pos_conf)
    GROUP BY tmp_teams_cpA.season, tmp_teams_cpA.conf_id, tmp_teams_cpA.team_id
  ON DUPLICATE KEY UPDATE seed = VALUES(seed);

  INSERT INTO SPORTS_AHL_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
    SELECT * FROM tmp_teams;

END $$

DELIMITER ;

##
## In-season calcs when all bar bottom 2/3 in division qualify
##
DROP PROCEDURE IF EXISTS `ahl_playoff_seeds_div_exp2021`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_seeds_div_exp2021`(
  v_season SMALLINT UNSIGNED,
  v_standing_date DATE
)
    COMMENT 'Calculate AHL playoff seeds: All bar bottom 2/3 in division'
BEGIN

  # Get the division sizes
  DROP TEMPORARY TABLE IF EXISTS tmp_div_sizes;
  CREATE TEMPORARY TABLE tmp_div_sizes (
    div_id TINYINT UNSIGNED,
    num_teams TINYINT UNSIGNED,
    qual_teams TINYINT UNSIGNED,
    PRIMARY KEY (div_id)
  ) ENGINE = MEMORY
    SELECT grouping_id AS div_id, COUNT(*) AS num_teams, 0 AS qual_teams
    FROM SPORTS_AHL_TEAMS_GROUPINGS
    WHERE v_season BETWEEN season_from AND IFNULL(season_to, 2099)
    GROUP BY grouping_id;
  UPDATE tmp_div_sizes SET qual_teams = LEAST(num_teams - 2, 7); # Appears no more than 7 teams qualify

  # Get the teams
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    season SMALLINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    seed TINYINT UNSIGNED,
    team_id CHAR(3),
    pos_div TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    PRIMARY KEY (season, conf_id, team_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_AHL_STANDINGS.season,
           SPORTS_AHL_GROUPINGS.parent_id AS conf_id,
           0 AS seed,
           SPORTS_AHL_STANDINGS.team_id,
           SPORTS_AHL_STANDINGS.pos_div,
           SPORTS_AHL_STANDINGS.pos_conf,
           SPORTS_AHL_STANDINGS.pos_league
    FROM SPORTS_AHL_STANDINGS
    JOIN SPORTS_AHL_TEAMS_GROUPINGS
      ON (SPORTS_AHL_TEAMS_GROUPINGS.team_id = SPORTS_AHL_STANDINGS.team_id
      AND v_season BETWEEN SPORTS_AHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_AHL_TEAMS_GROUPINGS.season_to, 2099))
    JOIN SPORTS_AHL_GROUPINGS
      ON (SPORTS_AHL_GROUPINGS.grouping_id = SPORTS_AHL_TEAMS_GROUPINGS.grouping_id)
    JOIN tmp_div_sizes
      ON (tmp_div_sizes.div_id = SPORTS_AHL_TEAMS_GROUPINGS.grouping_id)
    WHERE SPORTS_AHL_STANDINGS.season = v_season
    AND   SPORTS_AHL_STANDINGS.the_date = v_standing_date
    AND   SPORTS_AHL_STANDINGS.pos_div <= tmp_div_sizes.qual_teams;

  # Sort
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cpA');
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cpB');
  INSERT INTO tmp_teams (season, conf_id, team_id, seed)
    SELECT tmp_teams_cpA.season, tmp_teams_cpA.conf_id, tmp_teams_cpA.team_id,
           COUNT(tmp_teams_cpB.team_id) + 1 AS seed
    FROM tmp_teams_cpA
    LEFT JOIN tmp_teams_cpB
      ON (tmp_teams_cpB.season = tmp_teams_cpA.season
      AND tmp_teams_cpB.conf_id = tmp_teams_cpA.conf_id
      AND tmp_teams_cpB.pos_conf < tmp_teams_cpA.pos_conf)
    GROUP BY tmp_teams_cpA.season, tmp_teams_cpA.conf_id, tmp_teams_cpA.team_id
  ON DUPLICATE KEY UPDATE seed = VALUES(seed);

  INSERT INTO SPORTS_AHL_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
    SELECT * FROM tmp_teams;

END $$

DELIMITER ;
