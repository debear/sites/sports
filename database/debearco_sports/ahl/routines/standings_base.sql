##
## Standings
##
DROP PROCEDURE IF EXISTS `ahl_standings_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_setup`(
  OUT v_season SMALLINT UNSIGNED
)
    COMMENT 'Base AHL standings config'
BEGIN

  DECLARE v_start_date DATE;
  DECLARE v_end_date DATE;

  # Get vars
  SELECT season, MIN(the_date), MAX(the_date) INTO v_season, v_start_date, v_end_date FROM tmp_date_list;

  # Identify the teams to calc
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    league_id TINYINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT TEAM_DIV.team_id, CONFERENCE.parent_id AS league_id, CONFERENCE.grouping_id AS conf_id, DIVISION.grouping_id AS div_id
    FROM SPORTS_AHL_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_AHL_GROUPINGS AS DIVISION
      ON (DIVISION.grouping_id = TEAM_DIV.grouping_id)
    JOIN SPORTS_AHL_GROUPINGS AS CONFERENCE
      ON (CONFERENCE.grouping_id = DIVISION.parent_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099);
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # Determine the schedule
  CALL ahl_standings_setup_sched(v_season, v_end_date);

  # Max GP by each team (Note: tmp_sched only includes results)
  DROP TEMPORARY TABLE IF EXISTS tmp_teams_max_gp;
  CREATE TEMPORARY TABLE tmp_teams_max_gp (
    team_id VARCHAR(3),
    max_gp TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id, COUNT(SPORTS_AHL_SCHEDULE.game_id) AS max_gp
    FROM tmp_teams
    LEFT JOIN SPORTS_AHL_SCHEDULE
      ON (SPORTS_AHL_SCHEDULE.season = v_season
      AND SPORTS_AHL_SCHEDULE.game_type = 'regular'
      AND tmp_teams.team_id IN (SPORTS_AHL_SCHEDULE.home, SPORTS_AHL_SCHEDULE.visitor))
    GROUP BY tmp_teams.team_id;

  # Create our temporary table for caching before one mass insertion at the end
  CALL ahl_standings_setup_create();

  # Clear what was already there
  DELETE SPORTS_AHL_STANDINGS.*
  FROM tmp_date_list
  JOIN SPORTS_AHL_STANDINGS
    ON (SPORTS_AHL_STANDINGS.season = tmp_date_list.season
    AND SPORTS_AHL_STANDINGS.the_date = tmp_date_list.the_date);

END $$

DELIMITER ;

##
## Standings
##
DROP PROCEDURE IF EXISTS `ahl_standings_setup_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_setup_sched`(
  v_season SMALLINT UNSIGNED,
  v_max_date DATE
)
    COMMENT 'Schedule contributing to AHL standings'
BEGIN

  # Base info
  DROP TEMPORARY TABLE IF EXISTS tmp_sched_stage0;
  CREATE TEMPORARY TABLE tmp_sched_stage0 (
    team_id VARCHAR(3),
    game_id SMALLINT UNSIGNED,
    game_num TINYINT UNSIGNED,
    opp_id VARCHAR(3),
    game_date DATE,
    venue ENUM('home', 'visitor'),
    result ENUM('w', 'l', 'otl', 'sol'),
    status ENUM('F', 'OT', 'SO'),
    pts TINYINT UNSIGNED,
    goals_for SMALLINT UNSIGNED,
    goals_against SMALLINT UNSIGNED,
    is_conf_opp TINYINT UNSIGNED,
    is_div_opp TINYINT UNSIGNED,
    PRIMARY KEY (team_id, game_id) USING BTREE,
    INDEX team_game_num (team_id, game_num) USING BTREE,
    INDEX team_game_date (team_id, game_date) USING BTREE,
    INDEX team_venue (team_id, venue, game_date) USING BTREE,
    INDEX team_status (team_id, status, game_date) USING BTREE,
    INDEX team_conf (team_id, is_conf_opp, game_date) USING BTREE,
    INDEX team_div (team_id, is_div_opp, game_date) USING BTREE
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id,
           SPORTS_AHL_SCHEDULE.game_id,
           NULL AS game_num,
           IF(SPORTS_AHL_SCHEDULE.home = tmp_teams.team_id,
              SPORTS_AHL_SCHEDULE.visitor,
              SPORTS_AHL_SCHEDULE.home) AS opp_id,
           SPORTS_AHL_SCHEDULE.game_date,
           IF(SPORTS_AHL_SCHEDULE.home = tmp_teams.team_id,
              'home', 'visitor') AS venue,
           NULL AS result,
           SPORTS_AHL_SCHEDULE.status,
           0 AS pts,
           IF(SPORTS_AHL_SCHEDULE.home = tmp_teams.team_id,
              SPORTS_AHL_SCHEDULE.home_score,
              SPORTS_AHL_SCHEDULE.visitor_score) AS goals_for,
           IF(SPORTS_AHL_SCHEDULE.home = tmp_teams.team_id,
              SPORTS_AHL_SCHEDULE.visitor_score,
              SPORTS_AHL_SCHEDULE.home_score) AS goals_against,
           tmp_teams.conf_id = tmp_teams_cp.conf_id AS is_conf_opp,
           tmp_teams.conf_id = tmp_teams_cp.conf_id
             AND tmp_teams.div_id = tmp_teams_cp.div_id AS is_div_opp
    FROM SPORTS_AHL_SCHEDULE
    JOIN tmp_teams
      ON (tmp_teams.team_id = SPORTS_AHL_SCHEDULE.home
       OR tmp_teams.team_id = SPORTS_AHL_SCHEDULE.visitor)
    JOIN tmp_teams_cp
      ON (tmp_teams_cp.team_id
            = IF(SPORTS_AHL_SCHEDULE.home = tmp_teams.team_id,
                 SPORTS_AHL_SCHEDULE.visitor,
                 SPORTS_AHL_SCHEDULE.home))
    WHERE SPORTS_AHL_SCHEDULE.season = v_season
    AND   SPORTS_AHL_SCHEDULE.game_type = 'regular'
    AND   SPORTS_AHL_SCHEDULE.game_date <= v_max_date
    AND   SPORTS_AHL_SCHEDULE.status IS NOT NULL;

  # Calculate result info
  CALL _duplicate_tmp_table('tmp_sched_stage0', 'tmp_sched_stage1');
  INSERT INTO tmp_sched_stage1 (team_id, game_id, result)
    SELECT team_id, game_id,
           IF(goals_for > goals_against, 'w',
              CASE status
                WHEN 'F' THEN 'l'
                WHEN 'OT' THEN 'otl'
                WHEN 'SO' THEN 'sol'
              END) AS result
    FROM tmp_sched_stage0
  ON DUPLICATE KEY UPDATE result = VALUES(result);

  # Calculate point totals
  CALL _duplicate_tmp_table('tmp_sched_stage1', 'tmp_sched_stage2');
  INSERT INTO tmp_sched_stage2 (team_id, game_id, pts)
    SELECT team_id, game_id,
           IF(result = 'w', 2,
              IF(result IN ('otl', 'sol'), 1, 0)) AS pts
    FROM tmp_sched_stage1
  ON DUPLICATE KEY UPDATE pts = VALUES(pts);

  # Calculate the game numbers
  CALL _duplicate_tmp_table('tmp_sched_stage2', 'tmp_sched_stage2_cp');
  CALL _duplicate_tmp_table('tmp_sched_stage2', 'tmp_sched');
  INSERT INTO tmp_sched (team_id, game_id, game_num)
    SELECT tmp_sched_stage2.team_id, tmp_sched_stage2.game_id,
           COUNT(tmp_sched_stage2_cp.game_id) + 1 AS game_num
    FROM tmp_sched_stage2
    LEFT JOIN tmp_sched_stage2_cp
      ON (tmp_sched_stage2_cp.team_id = tmp_sched_stage2.team_id
      AND tmp_sched_stage2_cp.game_date < tmp_sched_stage2.game_date)
    GROUP BY tmp_sched_stage2.team_id, tmp_sched_stage2.game_id
  ON DUPLICATE KEY UPDATE game_num = VALUES(game_num);

END $$

DELIMITER ;

##
## Store permenantly
##
DROP PROCEDURE IF EXISTS `ahl_standings_setup_create`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_setup_create`()
    COMMENT 'Create the temporary table to store working AHL standings'
BEGIN

  # Copy from base table
  DROP TEMPORARY TABLE IF EXISTS tmp_standings;
  CREATE TEMPORARY TABLE tmp_standings LIKE SPORTS_AHL_STANDINGS;
  ALTER TABLE tmp_standings ENGINE = MEMORY;

  # Add our tie-breaking columns
  ALTER TABLE tmp_standings ENGINE = MEMORY,
                            ADD COLUMN _tb_worst_pts_pct DECIMAL(4,3) DEFAULT 0,
                            ADD COLUMN _tb_max_reg_ot_wins TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_final TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_league_id TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_conf_id TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_div_id TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_join TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_pos TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_is_tied TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_pos_adj TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_gp TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_reg_wins TINYINT UNSIGNED DEFAULT 0,
                            ADD COLUMN _tb_win_reg_pct DECIMAL(4,3) DEFAULT 0,
                            ADD COLUMN _tb_win_row_pct DECIMAL(4,3) DEFAULT 0,
                            ADD COLUMN _tb_win_all_pct DECIMAL(4,3) DEFAULT 0,
                            ADD COLUMN _tb_series_pts_pct DECIMAL(5,4),
                            ADD COLUMN _tb_goal_diff SMALLINT SIGNED,
                            ADD COLUMN _tb_series_goals TINYINT SIGNED,
                            ADD COLUMN _tb_intra_conf_pts TINYINT UNSIGNED,
                            ADD COLUMN _tb_intra_conf_pts_pct DECIMAL(5,4),
                            ADD INDEX is_tied (_tb_is_tied) USING BTREE,
                            ADD INDEX sort (season, the_date, _tb_join, pts) USING BTREE,
                            ADD INDEX tiebreak (season, the_date, _tb_join, _tb_pos) USING BTREE,
                            ADD INDEX common (season, the_date, _tb_join, _tb_pos, team_id) USING BTREE,
                            ADD INDEX stat_cod (_tb_final, pos_div) USING BTREE;

END $$

DELIMITER ;

##
## Store permenantly
##
DROP PROCEDURE IF EXISTS `ahl_standings_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_store`()
    COMMENT 'Store the AHL standings'
BEGIN

  # Remove our (temporary) tie-breaking columns (defined above)
  ALTER TABLE tmp_standings DROP COLUMN _tb_worst_pts_pct,
                            DROP COLUMN _tb_max_reg_ot_wins,
                            DROP COLUMN _tb_final,
                            DROP COLUMN _tb_league_id,
                            DROP COLUMN _tb_conf_id,
                            DROP COLUMN _tb_div_id,
                            DROP COLUMN _tb_join,
                            DROP COLUMN _tb_pos,
                            DROP COLUMN _tb_is_tied,
                            DROP COLUMN _tb_pos_adj,
                            DROP COLUMN _tb_gp,
                            DROP COLUMN _tb_reg_wins,
                            DROP COLUMN _tb_win_reg_pct,
                            DROP COLUMN _tb_win_row_pct,
                            DROP COLUMN _tb_win_all_pct,
                            DROP COLUMN _tb_series_pts_pct,
                            DROP COLUMN _tb_goal_diff,
                            DROP COLUMN _tb_series_goals,
                            DROP COLUMN _tb_intra_conf_pts,
                            DROP COLUMN _tb_intra_conf_pts_pct;

  INSERT INTO SPORTS_AHL_STANDINGS
    SELECT * FROM tmp_standings;

END $$

DELIMITER ;

##
## End the script
##
DROP PROCEDURE IF EXISTS `ahl_standings_tidy`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_tidy`()
    COMMENT 'Tidy AHL standings calcs'
BEGIN

  ALTER TABLE SPORTS_AHL_STANDINGS ORDER BY season, the_date, team_id;

END $$

DELIMITER ;

