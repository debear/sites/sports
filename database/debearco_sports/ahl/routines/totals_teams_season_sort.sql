##
## Player totals sorted
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams_season_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_season_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for AHL teams'
BEGIN

  # Broken down by individual routines
  CALL ahl_totals_teams_season_sort_skaters(v_season, v_season_type);
  CALL ahl_totals_teams_season_sort_goalies(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_teams_season_sort_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_season_sort_skaters`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort skater season totals for AHL teams'
BEGIN

  DELETE FROM SPORTS_AHL_TEAMS_SEASON_SKATERS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL ahl_totals_teams_sort__tmp(v_season, v_season_type, 'SKATERS',
                                  'goals SMALLINT UNSIGNED,
                                   assists SMALLINT UNSIGNED,
                                   points SMALLINT UNSIGNED,
                                   plus_minus SMALLINT SIGNED,
                                   pims SMALLINT UNSIGNED,
                                   pp_goals TINYINT UNSIGNED,
                                   pp_assists TINYINT UNSIGNED,
                                   pp_points SMALLINT UNSIGNED,
                                   pp_opps SMALLINT UNSIGNED,
                                   pp_pct DECIMAL(6,5),
                                   pk_goals TINYINT UNSIGNED,
                                   pk_kills SMALLINT UNSIGNED,
                                   pk_opps SMALLINT UNSIGNED,
                                   pk_pct DECIMAL(6,5),
                                   sh_goals TINYINT UNSIGNED,
                                   sh_assists TINYINT UNSIGNED,
                                   sh_points SMALLINT UNSIGNED,
                                   shots SMALLINT UNSIGNED,
                                   shot_pct DECIMAL(6,5),
                                   so_goals TINYINT UNSIGNED,
                                   so_shots TINYINT UNSIGNED,
                                   so_pct DECIMAL(6,5)',
                                  'goals,
                                   assists,
                                   goals + assists AS points,
                                   plus_minus,
                                   pims,
                                   pp_goals,
                                   pp_assists,
                                   pp_goals + pp_assists AS pp_points,
                                   pp_opps,
                                   IF(pp_opps > 0, pp_goals / pp_opps, 0) AS pp_pct,
                                   pk_goals,
                                   pk_kills,
                                   pk_opps,
                                   IF(pk_opps > 0, pk_kills / pk_opps, 0) AS pk_pct,
                                   sh_goals,
                                   sh_assists,
                                   sh_goals + sh_assists AS sh_points,
                                   shots,
                                   IF(shots > 0, goals / shots, 0) AS shot_pct,
                                   so_goals,
                                   so_shots,
                                   IF(so_shots > 0, so_goals / so_shots, 0) AS so_pct');

  # Calcs
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'goals', 'B.goals > A.goals');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'assists', 'B.assists > A.assists');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'points', 'B.points > A.points');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'plus_minus', 'B.plus_minus > A.plus_minus');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pims', 'B.pims > A.pims');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_goals', 'B.pp_goals > A.pp_goals');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_assists', 'B.pp_assists > A.pp_assists');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_points', 'B.pp_points > A.pp_points');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_opps', 'B.pp_opps > A.pp_opps');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_pct', 'B.pp_pct > A.pp_pct');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_goals', 'B.pk_goals < A.pk_goals');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_kills', 'B.pk_kills > A.pk_kills');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_opps', 'B.pk_opps < A.pk_opps');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_pct', 'B.pk_pct > A.pk_pct');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_goals', 'B.sh_goals > A.sh_goals');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_assists', 'B.sh_assists > A.sh_assists');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_points', 'B.sh_points > A.sh_points');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'shots', 'B.shots > A.shots');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'shot_pct', 'B.shot_pct > A.shot_pct');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'so_goals', 'B.so_goals > A.so_goals');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'so_shots', 'B.so_shots > A.so_shots');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'so_pct', 'B.so_pct > A.so_pct');

  # Order
  ALTER TABLE SPORTS_AHL_TEAMS_SEASON_SKATERS_SORTED ORDER BY season, season_type, team_id, stat_dir;

END $$

DELIMITER ;

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_teams_season_sort_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_season_sort_goalies`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort goalie season totals for AHL teams'
BEGIN

  DELETE FROM SPORTS_AHL_TEAMS_SEASON_GOALIES_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL ahl_totals_teams_sort__tmp(v_season, v_season_type, 'GOALIES',
                                  'win TINYINT UNSIGNED,
                                   loss TINYINT UNSIGNED,
                                   ot_loss TINYINT UNSIGNED,
                                   so_loss TINYINT UNSIGNED,
                                   goals_against TINYINT UNSIGNED,
                                   shots_against SMALLINT UNSIGNED,
                                   saves SMALLINT UNSIGNED,
                                   save_pct DECIMAL(6,5),
                                   goals_against_avg DECIMAL(8,5),
                                   shutout TINYINT UNSIGNED,
                                   minutes_played MEDIUMINT UNSIGNED,
                                   so_goals_against TINYINT UNSIGNED,
                                   so_shots_against TINYINT UNSIGNED,
                                   so_saves TINYINT UNSIGNED,
                                   so_save_pct DECIMAL(6,5),
                                   goals SMALLINT UNSIGNED,
                                   assists SMALLINT UNSIGNED,
                                   points SMALLINT UNSIGNED,
                                   pims SMALLINT UNSIGNED',
                                  'win,
                                   loss,
                                   ot_loss,
                                   so_loss,
                                   goals_against,
                                   shots_against,
                                   shots_against - goals_against AS saves,
                                   IF(shots_against > 0, (shots_against - goals_against) / shots_against, 0) AS save_pct,
                                   IF(minutes_played > 0, (goals_against * 3600) / minutes_played, 0) AS goals_against_avg,
                                   shutout AS shutout,
                                   minutes_played AS minutes_played,
                                   so_goals_against AS so_goals_against,
                                   so_shots_against AS so_shots_against,
                                   so_shots_against - so_goals_against AS so_saves,
                                   IF(so_shots_against > 0, (so_shots_against - so_goals_against) / so_shots_against, 0) AS so_save_pct,
                                   goals,
                                   assists,
                                   goals + assists AS points,
                                   pims');

  # Calcs
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'win', 'B.win > A.win');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'loss', 'B.loss > A.loss');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'ot_loss', 'B.ot_loss > A.ot_loss');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_loss', 'B.so_loss > A.so_loss');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against', 'B.goals_against < A.goals_against');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'shots_against', 'B.shots_against > A.shots_against');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'saves', 'B.saves > A.saves');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'save_pct', 'B.save_pct > A.save_pct');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_avg', 'B.goals_against_avg < A.goals_against_avg');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'shutout', 'B.shutout > A.shutout');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'minutes_played', 'B.minutes_played > A.minutes_played');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_goals_against', 'B.so_goals_against > A.so_goals_against');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_shots_against', 'B.so_shots_against > A.so_shots_against');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_saves', 'B.so_saves > A.so_saves');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_save_pct', 'B.so_save_pct > A.so_save_pct');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'goals', 'B.goals > A.goals');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'assists', 'B.assists > A.assists');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'points', 'B.points > A.points');
  CALL ahl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'pims', 'B.pims > A.pims');

  # Order
  ALTER TABLE SPORTS_AHL_TEAMS_SEASON_GOALIES_SORTED ORDER BY season, season_type, team_id, stat_dir;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_teams_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_cols VARCHAR(2048),
  v_calcs VARCHAR(2048)
)
    COMMENT 'Create temporary table for AHL team season stat sort'
BEGIN

  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_AHL_TEAMS_SEASON_', v_table));
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_AHL_TEAMS_SEASON_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      team_id VARCHAR(3) NOT NULL,
      stat_dir ENUM(\'for\', \'against\') NOT NULL,
      ', v_cols, ',
      PRIMARY KEY (season, season_type, team_id, stat_dir),
      KEY by_stat_dit (season, season_type, stat_dir)
    ) ENGINE = MEMORY
      SELECT season, season_type, team_id, stat_dir, ', v_calcs, '
      FROM SPORTS_AHL_TEAMS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '";'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_AHL_TEAMS_SEASON_', v_table), CONCAT('tmp_SPORTS_AHL_TEAMS_SEASON_', v_table, '_cp'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `ahl_totals_teams_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table VARCHAR(15),
  v_col VARCHAR(25),
  v_calc VARCHAR(1024)
)
    COMMENT 'Sort season totals for AHL teams'
BEGIN

  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_AHL_TEAMS_SEASON_', v_table, '_SORTED (season, season_type, team_id, stat_dir, `', v_col, '`)
      SELECT A.season, A.season_type, A.team_id, A.stat_dir,
             COUNT(B.team_id) + 1 AS `', v_col, '`
      FROM tmp_SPORTS_AHL_TEAMS_SEASON_', v_table, ' AS A
      LEFT JOIN tmp_SPORTS_AHL_TEAMS_SEASON_', v_table, '_cp AS B
        ON (B.season = A.season
        AND B.season_type = A.season_type
        AND B.stat_dir = A.stat_dir
        AND ', v_calc, ')
      WHERE A.season = ', v_season, '
      AND   A.season_type = "', v_season_type, '"
      GROUP BY A.season, A.season_type, A.team_id, A.stat_dir
    ON DUPLICATE KEY UPDATE `', v_col, '` = VALUES(`', v_col, '`);'));

END $$

DELIMITER ;

