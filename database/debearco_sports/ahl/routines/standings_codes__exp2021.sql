##
## From 2021/22, all bar bottom two teams qualify for a divisional playoff format
##

## Setup the dynamic parts of the calculation
DROP PROCEDURE IF EXISTS `ahl_standings_codes_setup_exp2021`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_codes_setup_exp2021`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Prepare for the 2021/22 AHL format standing codes calcs'
BEGIN

  # Determine the number of teams in each division, then the number of playoff teams and first round byes
  DROP TEMPORARY TABLE IF EXISTS tmp_div_sizes;
  CREATE TEMPORARY TABLE tmp_div_sizes (
    div_id TINYINT UNSIGNED,
    num_teams TINYINT UNSIGNED,
    qual_teams TINYINT UNSIGNED,
    bye_teams TINYINT UNSIGNED,
    PRIMARY KEY (div_id)
  ) ENGINE = MEMORY
    SELECT grouping_id AS div_id, COUNT(*) AS num_teams, 0 AS qual_teams, 0 AS bye_teams
    FROM SPORTS_AHL_TEAMS_GROUPINGS
    WHERE v_season BETWEEN season_from AND IFNULL(season_to, 2099)
    GROUP BY grouping_id;
  UPDATE tmp_div_sizes SET qual_teams = LEAST(num_teams - 2, 7); # Appears no more than 7 teams qualify
  UPDATE tmp_div_sizes SET bye_teams = 4 - (qual_teams MOD 4);

END $$

DELIMITER ;

## Teams qualified for the playoffs
DROP PROCEDURE IF EXISTS `ahl_standings_codes_process_exp2021`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_codes_process_exp2021`(
  v_season SMALLINT UNSIGNED,
  v_code CHAR(1),
  v_field VARCHAR(4)
)
    COMMENT 'Process the 2021/22 AHL format clinched standing codes'
BEGIN

  DECLARE v_sort_col VARCHAR(20);
  DECLARE v_sort_cmp VARCHAR(20);

  # Simplify our query by generalising our position and join cols
  UPDATE tmp_standings SET _tb_join = _tb_div_id, _tb_pos = pos_div;
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  # From 2022/23, when each team played same number of games, initial sort uses points
  IF v_season >= 2022 THEN
    SET v_sort_col := 'pts';
    SET v_sort_cmp := 'pts';

  # In 20221/22, where each team did not play the same number of games, initial sort was points percentage
  ELSE
    SET v_sort_col := 'pts_pct';
    SET v_sort_cmp := '_tb_worst_pts_pct';

  END IF;

  # Process - Guesstimate
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, status_code)
      SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
             IF(tmp_standings_cpB.team_id IS NULL, "', v_code, '", tmp_standings_cpA.status_code) AS status_code
      FROM tmp_standings_cpA
      JOIN tmp_div_sizes
        ON (tmp_div_sizes.div_id = tmp_standings_cpA._tb_div_id)
      LEFT JOIN tmp_standings_cpB
        ON (tmp_standings_cpB.season = tmp_standings_cpA.season
        AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
        AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
        AND tmp_standings_cpB._tb_pos > tmp_div_sizes.', v_field, '_teams
        AND (tmp_standings_cpB.max_', v_sort_col, ' > tmp_standings_cpA.', v_sort_cmp, '
          OR tmp_standings_cpB.max_', v_sort_col, ' = tmp_standings_cpA.', v_sort_cmp, '
           AND tmp_standings_cpB._tb_max_reg_ot_wins > tmp_standings_cpA.reg_ot_wins))
      WHERE (tmp_standings_cpA.wins + tmp_standings_cpA.loss + tmp_standings_cpA.ot_loss + tmp_standings_cpA.so_loss) > 10
      AND   tmp_standings_cpA._tb_final = 0
      AND   tmp_standings_cpA._tb_pos <= tmp_div_sizes.', v_field, '_teams
      GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
    ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);'));

  # Process - Final
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, status_code)
      SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
            "', v_code, '" AS status_code
      FROM tmp_standings_cpA
      JOIN tmp_div_sizes
        ON (tmp_div_sizes.div_id = tmp_standings_cpA._tb_div_id)
      WHERE tmp_standings_cpA._tb_final = 1
      AND   tmp_standings_cpA._tb_pos <= tmp_div_sizes.', v_field, '_teams
    ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);'));

END $$

DELIMITER ;
