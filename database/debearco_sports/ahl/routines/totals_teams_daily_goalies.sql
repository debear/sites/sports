##
## Player daily game totals
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams_daily_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_daily_goalies`()
    COMMENT 'Calculate daily game totals for AHL team goalies'
BEGIN

  ##
  ## Combine player totals for the teams
  ##
  INSERT INTO SPORTS_AHL_TEAMS_GAME_GOALIES (season, team_id, game_type, game_id, stat_dir, gp, win, loss, ot_loss, so_loss, goals_against, shots_against, shutout, minutes_played, so_goals_against, so_shots_against, goals, assists, pims)
    SELECT SPORTS_AHL_PLAYERS_GAME_GOALIES.season,
           SPORTS_AHL_PLAYERS_GAME_GOALIES.team_id,
           SPORTS_AHL_PLAYERS_GAME_GOALIES.game_type,
           SPORTS_AHL_PLAYERS_GAME_GOALIES.game_id,
           'for' AS stat_dir,
           1 AS gp,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.win) AS win,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.loss) AS loss,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.ot_loss) AS ot_loss,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.so_loss) AS so_loss,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.goals_against) AS goals_against,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.shots_against) AS shots_against,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.shutout) AS shutout,
           SEC_TO_TIME(SUM(TIME_TO_SEC(SPORTS_AHL_PLAYERS_GAME_GOALIES.minutes_played))) AS minutes_played,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.so_goals_against) AS so_goals_against,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.so_shots_against) AS so_shots_against,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.goals) AS goals,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.assists) AS assists,
           SUM(SPORTS_AHL_PLAYERS_GAME_GOALIES.pims) AS pims
    FROM tmp_game_list
    JOIN SPORTS_AHL_PLAYERS_GAME_GOALIES
      ON (SPORTS_AHL_PLAYERS_GAME_GOALIES.season = tmp_game_list.season
      AND SPORTS_AHL_PLAYERS_GAME_GOALIES.game_type = tmp_game_list.game_type
      AND SPORTS_AHL_PLAYERS_GAME_GOALIES.game_id = tmp_game_list.game_id)
    GROUP BY SPORTS_AHL_PLAYERS_GAME_GOALIES.season,
             SPORTS_AHL_PLAYERS_GAME_GOALIES.team_id,
             SPORTS_AHL_PLAYERS_GAME_GOALIES.game_type,
             SPORTS_AHL_PLAYERS_GAME_GOALIES.game_id
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          win = VALUES(win),
                          loss = VALUES(loss),
                          ot_loss = VALUES(ot_loss),
                          so_loss = VALUES(so_loss),
                          goals_against = VALUES(goals_against),
                          shots_against = VALUES(shots_against),
                          shutout = VALUES(shutout),
                          minutes_played = VALUES(minutes_played),
                          so_goals_against = VALUES(so_goals_against),
                          so_shots_against = VALUES(so_shots_against),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          pims = VALUES(pims);

  ##
  ## And get oppositions totals for these...
  ##
  INSERT INTO SPORTS_AHL_TEAMS_GAME_GOALIES (season, team_id, game_type, game_id, stat_dir, gp, win, loss, ot_loss, so_loss, goals_against, shots_against, shutout, minutes_played, so_goals_against, so_shots_against, goals, assists, pims)
    SELECT ME.season, ME.team_id, ME.game_type, ME.game_id,
           'against' AS stat_dir,
           THEM.gp,
           THEM.win,
           THEM.loss,
           THEM.ot_loss,
           THEM.so_loss,
           THEM.goals_against,
           THEM.shots_against,
           THEM.shutout,
           THEM.minutes_played,
           THEM.so_goals_against,
           THEM.so_shots_against,
           THEM.goals,
           THEM.assists,
           THEM.pims
    FROM tmp_game_list
    JOIN SPORTS_AHL_TEAMS_GAME_GOALIES AS ME
      ON (ME.season = tmp_game_list.season
      AND ME.game_type = tmp_game_list.game_type
      AND ME.game_id = tmp_game_list.game_id
      AND ME.stat_dir = 'for')
    JOIN SPORTS_AHL_SCHEDULE
      ON (SPORTS_AHL_SCHEDULE.season = ME.season
      AND SPORTS_AHL_SCHEDULE.game_type = ME.game_type
      AND SPORTS_AHL_SCHEDULE.game_id = ME.game_id)
    JOIN SPORTS_AHL_TEAMS_GAME_GOALIES AS THEM
      ON (THEM.season = SPORTS_AHL_SCHEDULE.season
      AND THEM.team_id = IF(ME.team_id = SPORTS_AHL_SCHEDULE.home, SPORTS_AHL_SCHEDULE.visitor, SPORTS_AHL_SCHEDULE.home)
      AND THEM.game_type = SPORTS_AHL_SCHEDULE.game_type
      AND THEM.game_id = SPORTS_AHL_SCHEDULE.game_id
      AND THEM.stat_dir = 'for')
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          win = VALUES(win),
                          loss = VALUES(loss),
                          ot_loss = VALUES(ot_loss),
                          so_loss = VALUES(so_loss),
                          goals_against = VALUES(goals_against),
                          shots_against = VALUES(shots_against),
                          shutout = VALUES(shutout),
                          minutes_played = VALUES(minutes_played),
                          so_goals_against = VALUES(so_goals_against),
                          so_shots_against = VALUES(so_shots_against),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          pims = VALUES(pims);

END $$

DELIMITER ;

