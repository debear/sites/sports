## ######################### ##
## Divisional Based Matchups ##
## ######################### ##

##
## Division Semi Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_divisional_semi`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_divisional_semi`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff DSF matchup calcs'
BEGIN

  DECLARE v_div_a TINYINT UNSIGNED;
  DECLARE v_div_b TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  # Determine the order of the two divs
  SELECT div_id INTO v_div_a FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 1;
  SELECT div_id INTO v_div_b FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 2;

  # Note: Crossover teams are seeded based on points scored, so entirely valid for them to be 3rd in the crossed over div if scored more points than the team in 4th

  # Matchup 1: First Div: 1st v 4th
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE (div_id = v_div_a) OR (div_id = v_div_b AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 0, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE (div_id = v_div_a) OR (div_id = v_div_b AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 3, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Matchup 2: First Div: 2nd v 3rd
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE (div_id = v_div_a) OR (div_id = v_div_b AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 1, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE (div_id = v_div_a) OR (div_id = v_div_b AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 2, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Matchup 3: Second Div 1st v 4th
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE (div_id = v_div_b) OR (div_id = v_div_a AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 0, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE (div_id = v_div_b) OR (div_id = v_div_a AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 3, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 3 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Matchup 4: Second Div 2nd v 3rd
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE (div_id = v_div_b) OR (div_id = v_div_a AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 1, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE (div_id = v_div_b) OR (div_id = v_div_a AND pos_div = 5 AND div_xo = 'larger') ORDER BY pos_league LIMIT 2, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 4 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Increment the counter
  SET v_counter = v_counter + 4;

END $$

DELIMITER ;

##
## Division Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_divisional_final`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_divisional_final`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff DF matchup calcs'
BEGIN

  DECLARE v_div_a TINYINT UNSIGNED;
  DECLARE v_div_b TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  # Determine the order of the two divs and reseed
  SELECT div_id INTO v_div_a FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 1;
  SELECT div_id INTO v_div_b FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 2;

  # First Div Matchup
  CALL ahl_playoff_matchups_reseed_divisional(v_conf_id, v_div_a);
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_a AND reseed = 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = IF(pos_div < 5, v_div_a, v_div_b) AND reseed = 2;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Second Div Matchup
  CALL ahl_playoff_matchups_reseed_divisional(v_conf_id, v_div_b);
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_b AND reseed = 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = IF(pos_div < 5, v_div_b, v_div_a) AND reseed = 2;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Increment the counter
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

##
## Reseed according to divisional rules
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_reseed_divisional`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_reseed_divisional`(
  v_conf_id TINYINT UNSIGNED,
  v_div_id TINYINT UNSIGNED
)
    COMMENT 'AHL playoff divisional reseeding'
BEGIN

  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpA');
  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpB');
  INSERT INTO tmp_seeds (team_id, reseed)
    SELECT tmp_seeds_cpA.team_id, COUNT(tmp_seeds_cpB.team_id) + 1
    FROM tmp_seeds_cpA
    LEFT JOIN tmp_seeds_cpB
      ON (tmp_seeds_cpB.conf_id = tmp_seeds_cpA.conf_id
      AND ((tmp_seeds_cpA.pos_div < 5 AND (tmp_seeds_cpB.div_id = tmp_seeds_cpA.div_id OR tmp_seeds_cpB.pos_div = 5))  # Include a potential cross-over team
        OR (tmp_seeds_cpA.pos_div = 5 AND tmp_seeds_cpB.div_id <> tmp_seeds_cpA.div_id))                               # This team _is_ the cross-over team
      AND tmp_seeds_cpB.pos_conf < tmp_seeds_cpA.pos_conf)
    WHERE tmp_seeds_cpA.conf_id = v_conf_id
    GROUP BY tmp_seeds_cpA.team_id
  ON DUPLICATE KEY UPDATE reseed = VALUES(reseed);

END $$

DELIMITER ;

##
## Check for playoff XO instances
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_divisional_xo`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_divisional_xo`()
    COMMENT 'Check for AHL divisional XO'
BEGIN

  # Base on tmp_div_size
  DROP TEMPORARY TABLE IF EXISTS tmp_div_xo;
  CREATE TEMPORARY TABLE tmp_div_xo (
    conf_id TINYINT UNSIGNED,
    larger_div_id TINYINT UNSIGNED,
    smaller_div_id TINYINT UNSIGNED,
    larger_div_size TINYINT UNSIGNED,
    smaller_div_size TINYINT UNSIGNED,
    PRIMARY KEY (conf_id)
  ) ENGINE = MEMORY
    SELECT conf_id,
           SUBSTRING(MAX(CONCAT(div_size, ':', div_id)), LOCATE(':', MAX(CONCAT(div_size, ':', div_id))) + 1) AS larger_div_id,
           SUBSTRING(MIN(CONCAT(div_size, ':', div_id)), LOCATE(':', MIN(CONCAT(div_size, ':', div_id))) + 1) AS smaller_div_id,
           SUBSTRING(MAX(CONCAT(div_size, ':', div_id)), 1, LOCATE(':', MAX(CONCAT(div_size, ':', div_id))) - 1) AS larger_div_size,
           SUBSTRING(MIN(CONCAT(div_size, ':', div_id)), 1, LOCATE(':', MIN(CONCAT(div_size, ':', div_id))) - 1) AS smaller_div_size
    FROM tmp_div_list
    GROUP BY conf_id;
  DELETE FROM tmp_div_xo WHERE larger_div_size = smaller_div_size;

  # Write back to tmp_seeds
  UPDATE tmp_seeds
  JOIN tmp_div_xo
    ON (tmp_div_xo.larger_div_id = tmp_seeds.div_id)
  SET tmp_seeds.div_xo = 'larger';
  UPDATE tmp_seeds
  JOIN tmp_div_xo
    ON (tmp_div_xo.smaller_div_id = tmp_seeds.div_id)
  SET tmp_seeds.div_xo = 'smaller';

END $$

DELIMITER ;
