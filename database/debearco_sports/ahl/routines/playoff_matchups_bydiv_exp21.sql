## ############################################ ##
## Divisional Matchups, Expanded 2021/22 Format ##
## ############################################ ##

##
## Expanded 2021/22 format setup
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_divisional_exp21_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_divisional_exp21_setup`()
    COMMENT 'AHL playoff matchup setup for expanded 2021/22 format'
BEGIN

  ALTER TABLE tmp_div_list
    ADD COLUMN qual_teams TINYINT UNSIGNED,
    ADD COLUMN bye_teams TINYINT UNSIGNED;
  UPDATE tmp_div_list SET qual_teams = LEAST(div_size - 2, 7); # Appears no more than 7 teams qualify
  UPDATE tmp_div_list SET bye_teams = 4 - (qual_teams MOD 4);

END $$

DELIMITER ;

##
## First Round
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_divisional_exp21_first`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_divisional_exp21_first`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff 1R matchup calcs for expanded 2021/22 format'
BEGIN

  # Identify the conferences to loop through
  DECLARE v_div_id TINYINT UNSIGNED;
  DECLARE v_num_teams TINYINT UNSIGNED;
  DECLARE v_num_series TINYINT UNSIGNED;
  DECLARE v_bye_teams TINYINT UNSIGNED;
  DECLARE v_bye_series TINYINT UNSIGNED;
  DECLARE v_series TINYINT UNSIGNED;
  DECLARE v_series_base TINYINT UNSIGNED;
  DECLARE v_seed TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Loop through the divisions individually, due to their variable size
  DECLARE cur_div CURSOR FOR
    SELECT div_id, qual_teams - bye_teams, bye_teams FROM tmp_div_list WHERE conf_id = v_conf_id ORDER BY div_order;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  OPEN cur_div;
  loop_div: LOOP

    FETCH cur_div INTO v_div_id, v_num_teams, v_bye_teams;
    IF v_done = 1 THEN LEAVE loop_div; END IF;

    # Loop through the series
    SET v_num_series = v_num_teams / 2;
    SET v_series = 0;
    WHILE v_series < v_num_series DO
      # Determine the higher seed
      SET v_seed = v_bye_teams + v_series;
      SELECT seed INTO v_seed_a FROM tmp_seeds WHERE div_id = v_div_id ORDER BY pos_league LIMIT v_seed, 1;
      # Determine the lower seed
      SET v_seed = v_bye_teams + v_num_teams - v_series - 1;
      SELECT seed INTO v_seed_b FROM tmp_seeds WHERE div_id = v_div_id ORDER BY pos_league LIMIT v_seed, 1;
      # Add the matchup
      INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
        (v_season, v_series_date, CAST(v_counter + v_series + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);
      # Bump our series counter
      SET v_series = v_series + 1;
    END WHILE;

    # Increment the main counter
    SET v_counter = v_counter + v_num_series;

    # Then the DSF placeholder for the teams with byes
    IF v_bye_teams > 0 THEN
      SELECT 10 + (2 * COUNT(DISTINCT tmp_div_list.div_id)) INTO v_bye_series
      FROM tmp_div_list_cpA
      LEFT JOIN tmp_div_list
        ON (tmp_div_list.div_order_raw < tmp_div_list_cpA.div_order_raw)
      WHERE tmp_div_list_cpA.div_id = v_div_id;

      # Always two matchups - need to determine if higher/lower is fixed yet (and skip if no higher seed!)
      # 1 v 4
      SELECT seed INTO v_seed_a FROM tmp_seeds WHERE div_id = v_div_id ORDER BY pos_league LIMIT 0, 1;
      SELECT IF(v_bye_teams = 4, seed, 0) INTO v_seed_b FROM tmp_seeds WHERE div_id = v_div_id ORDER BY pos_league LIMIT 3, 1;
      INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
        (v_season, v_series_date, CAST(v_bye_series + 1 AS CHAR(2)), v_conf_id, v_seed_a, IF(v_seed_b > 0, v_conf_id, 0), v_seed_b, 0, 0, 0);

      # 2 v 3 (if set?)
      IF v_bye_teams > 1 THEN
        SELECT seed INTO v_seed_a FROM tmp_seeds WHERE div_id = v_div_id ORDER BY pos_league LIMIT 1, 1;
        SELECT IF(v_bye_teams > 2, seed, 0) INTO v_seed_b FROM tmp_seeds WHERE div_id = v_div_id ORDER BY pos_league LIMIT 2, 1;
        INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
          (v_season, v_series_date, CAST(v_bye_series + 2 AS CHAR(2)), v_conf_id, v_seed_a, IF(v_seed_b > 0, v_conf_id, 0), v_seed_b, 0, 0, 0);
      END IF;
    END IF;

  END LOOP loop_div;
  CLOSE cur_div;

END $$

DELIMITER ;

##
## Division Semi Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_divisional_exp21_semi`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_divisional_exp21_semi`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff DSF matchup calcs for expanded 2021/22 format'
BEGIN

  DECLARE v_div_a TINYINT UNSIGNED;
  DECLARE v_div_b TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  # Determine the order of the two divs
  SELECT div_id INTO v_div_a FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 1;
  SELECT div_id INTO v_div_b FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 2;

  # Matchup 1: First Div: 1st v 4th
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE div_id = v_div_a ORDER BY pos_league LIMIT 0, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE div_id = v_div_a ORDER BY pos_league LIMIT 3, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Matchup 2: First Div: 2nd v 3rd
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE div_id = v_div_a ORDER BY pos_league LIMIT 1, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE div_id = v_div_a ORDER BY pos_league LIMIT 2, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Matchup 3: Second Div 1st v 4th
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE div_id = v_div_b ORDER BY pos_league LIMIT 0, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE div_id = v_div_b ORDER BY pos_league LIMIT 3, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 3 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Matchup 4: Second Div 2nd v 3rd
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE div_id = v_div_b ORDER BY pos_league LIMIT 1, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE div_id = v_div_b ORDER BY pos_league LIMIT 2, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 4 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Increment the counter
  SET v_counter = v_counter + 4;

END $$

DELIMITER ;

##
## Division Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_divisional_exp21_final`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_divisional_exp21_final`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff DF matchup calcs for expanded 2021/22 format'
BEGIN

  DECLARE v_div_a TINYINT UNSIGNED;
  DECLARE v_div_b TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  # Determine the order of the two divs and reseed
  SELECT div_id INTO v_div_a FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 1;
  SELECT div_id INTO v_div_b FROM tmp_div_list WHERE conf_id = v_conf_id AND div_order = 2;

  # First Div Matchup
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_a ORDER BY pos_league LIMIT 0, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_a ORDER BY pos_league LIMIT 1, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Second Div Matchup
  SELECT seed INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_b ORDER BY pos_league LIMIT 0, 1;
  SELECT seed INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_b ORDER BY pos_league LIMIT 1, 1;
  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

  # Increment the counter
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;
