##
##
##
DROP PROCEDURE IF EXISTS `ahl_standings_codes_process_div_xo`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_codes_process_div_xo`(
  v_season SMALLINT UNSIGNED,
  v_code CHAR(1)
)
    COMMENT 'Exception to the AHL standing codes: division xo'
BEGIN

  DECLARE v_sort_col VARCHAR(7);

  # Pre-2015, when each team played same number of games, initial sort was points
  IF v_season < 2015 THEN
    SET v_sort_col := 'max_pts';

  # 2015 onwards, where each team may not play the same number ofgames, initial sort was points percentage
  ELSE
    SET v_sort_col := 'max_pts_pct';

  END IF;

  # Determine XO applicable divisions
  DROP TEMPORARY TABLE IF EXISTS tmp_div_sizes;
  CREATE TEMPORARY TABLE tmp_div_sizes (
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    num_teams TINYINT UNSIGNED,
    PRIMARY KEY (div_id)
  ) ENGINE = MEMORY
    SELECT conf_id, div_id, COUNT(*) AS num_teams
    FROM tmp_teams
    GROUP BY div_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_div_xo;
  CREATE TEMPORARY TABLE tmp_div_xo (
    conf_id TINYINT UNSIGNED,
    larger_div_id TINYINT UNSIGNED,
    smaller_div_id TINYINT UNSIGNED,
    larger_div_size TINYINT UNSIGNED,
    smaller_div_size TINYINT UNSIGNED,
    PRIMARY KEY (conf_id)
  ) ENGINE = MEMORY
    SELECT conf_id,
           SUBSTRING(MAX(CONCAT(num_teams, ':', div_id)), LOCATE(':', MAX(CONCAT(num_teams, ':', div_id))) + 1) AS larger_div_id,
           SUBSTRING(MIN(CONCAT(num_teams, ':', div_id)), LOCATE(':', MIN(CONCAT(num_teams, ':', div_id))) + 1) AS smaller_div_id,
           SUBSTRING(MAX(CONCAT(num_teams, ':', div_id)), 1, LOCATE(':', MAX(CONCAT(num_teams, ':', div_id))) - 1) AS larger_div_size,
           SUBSTRING(MIN(CONCAT(num_teams, ':', div_id)), 1, LOCATE(':', MIN(CONCAT(num_teams, ':', div_id))) - 1) AS smaller_div_size
    FROM tmp_div_sizes
    GROUP BY conf_id;
  DELETE FROM tmp_div_xo WHERE larger_div_size = smaller_div_size;

  # Then look for a potential XO in this/ese divs
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  # v1: Guesstimate
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, status_code)
      SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
             IF(tmp_standings_cpB.team_id IS NULL, "', v_code, '", tmp_standings_cpA.status_code) AS status_code
      FROM tmp_standings_cpA
      JOIN tmp_div_xo
        ON (tmp_div_xo.larger_div_id = tmp_standings_cpA._tb_div_id)
      LEFT JOIN tmp_standings_cpB
        ON (tmp_standings_cpB.season = tmp_standings_cpA.season
        AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
        AND tmp_standings_cpB._tb_div_id = tmp_div_xo.smaller_div_id
        AND tmp_standings_cpB.pos_div > 3
        AND (tmp_standings_cpB.', v_sort_col, ' > tmp_standings_cpA.pts
          OR tmp_standings_cpB.', v_sort_col, ' = tmp_standings_cpA.pts
           AND tmp_standings_cpB.reg_ot_wins > tmp_standings_cpA.reg_ot_wins))
      WHERE tmp_standings_cpA._tb_final = 0
      AND   tmp_standings_cpA.pos_div = 5
      GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
    ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);'));

  # v2: Final
  INSERT INTO tmp_standings (season, the_date, team_id, status_code)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           IF(tmp_standings_cpB.team_id IS NULL, v_code, tmp_standings_cpA.status_code) AS status_code
    FROM tmp_standings_cpA
    JOIN tmp_div_xo
      ON (tmp_div_xo.larger_div_id = tmp_standings_cpA._tb_div_id)
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_div_id = tmp_div_xo.smaller_div_id
      AND tmp_standings_cpB.pos_div = 4
      AND (tmp_standings_cpB.pts > tmp_standings_cpA.pts
        OR tmp_standings_cpB.pts = tmp_standings_cpA.pts
         AND tmp_standings_cpB.pos_conf < tmp_standings_cpA.pos_conf))
    WHERE tmp_standings_cpA._tb_final = 1
    AND   tmp_standings_cpA.pos_div = 5
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
  ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);

  # Unset the XO Div 4th pos code
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  UPDATE tmp_standings_cpA
  JOIN tmp_div_xo
    ON (tmp_div_xo.larger_div_id = tmp_standings_cpA._tb_div_id)
  JOIN tmp_standings
    ON (tmp_standings.season = tmp_standings_cpA.season
    AND tmp_standings.the_date = tmp_standings_cpA.the_date
    AND tmp_standings.pos_div > 3
    AND tmp_standings.status_code IS NOT NULL
    AND tmp_standings._tb_div_id = tmp_div_xo.smaller_div_id)
  SET tmp_standings.status_code = NULL
  WHERE tmp_standings_cpA.status_code = v_code;

END $$

DELIMITER ;

