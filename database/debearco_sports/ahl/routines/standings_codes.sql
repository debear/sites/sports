##
## Status codes
##
DROP PROCEDURE IF EXISTS `ahl_standings_codes`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_codes`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate status codes for AHL standings'
BEGIN

  #
  # Run some pre-processing calcs
  #
  CALL ahl_standings_codes_setup(v_season);

  #
  # Individual processes
  #
  # Playoff Spot
  IF v_season >= 2021 THEN
    # From 2021/22, all bar bottom two in each division qualify
    CALL ahl_standings_codes_setup_exp2021(v_season);
    CALL ahl_standings_codes_process_exp2021(v_season, 'x', 'qual');
    # Any teams with a first round bye?
    CALL ahl_standings_codes_process_exp2021(v_season, 'b', 'bye');

  # Not applicable for 2019/20 or 2020/21, where only div champs were recognised
  ELSEIF v_season NOT IN (2019, 2020) THEN
    # Until 2010/11 and from 2015/16, Divisional format (Top 4, w/XO for 5th place team in larger div until 2015/16)
    IF v_season <= 2011 OR v_season >= 2015 THEN
      CALL ahl_standings_codes_process(v_season, 'x', 'div', 4);
      IF v_season = 2015 THEN
        CALL ahl_standings_codes_process_div_xo(v_season, 'c');
      END IF;
    # From 2011/12 to 2014/15, Top 8 in conference
    ELSE
      CALL ahl_standings_codes_process(v_season, 'x', 'conf', 8);
    END IF;
  END IF;

  # Division Winner
  CALL ahl_standings_codes_process(v_season, 'y', 'div', 1);

  # Home-Ice (except 2020/21, which had no playoffs and a conference-less structure, so code does not apply)
  IF v_season <> 2020 THEN
    CALL ahl_standings_codes_process(v_season, 'z', 'conf', 1);
  END IF;

  # Kilpatrick Trophy
  CALL ahl_standings_codes_process(v_season, '*', 'league', 1);

END $$

DELIMITER ;

