##
## Calculate the standings
##
DROP PROCEDURE IF EXISTS `ahl_standings`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings`(
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate AHL standings'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;

  # Setup our run
  CALL ahl_standings_setup(v_season);

  # Basic standing details
  CALL ahl_standings_calcs(v_recent_games);

  # Calculate positions
  CALL ahl_standings_pos(v_season, 'div');
  CALL ahl_standings_pos(v_season, 'conf');
  CALL ahl_standings_pos(v_season, 'league');

  # Status Codes
  CALL ahl_standings_codes(v_season);

  # Store in the final table...
  CALL ahl_standings_store();

  # Tidy the table up...
  CALL ahl_standings_tidy();

  # Copy in to the season archive table
  CALL ahl_standings_history();

END $$

DELIMITER ;

