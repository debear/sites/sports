##
## Player season totals
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_goalies`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for AHL goalies'
BEGIN

  ##
  ## Combine daily figures into a single season
  ##
  INSERT INTO SPORTS_AHL_PLAYERS_SEASON_GOALIES (season, season_type, player_id, team_id, gp, starts, win, loss, ot_loss, so_loss, goals_against, shots_against, shutout, minutes_played, so_goals_against, so_shots_against, goals, assists, pims)
    SELECT season,
           game_type AS season_type,
           player_id,
           team_id,
           SUM(IFNULL(gp, 0)) AS gp,
           SUM(IFNULL(start, 0)) AS starts,
           SUM(IFNULL(win, 0)) AS win,
           SUM(IFNULL(loss, 0)) AS loss,
           SUM(IFNULL(ot_loss, 0)) AS ot_loss,
           SUM(IFNULL(so_loss, 0)) AS so_loss,
           SUM(IFNULL(goals_against, 0)) AS goals_against,
           SUM(IFNULL(shots_against, 0)) AS shots_against,
           SUM(IFNULL(shutout, 0)) AS shutout,
           SUM(TIME_TO_SEC(minutes_played)) AS minutes_played,
           SUM(IFNULL(so_goals_against, 0)) AS so_goals_against,
           SUM(IFNULL(so_shots_against, 0)) AS so_shots_against,
           SUM(IFNULL(goals, 0)) AS goals,
           SUM(IFNULL(assists, 0)) AS assists,
           SUM(IFNULL(pims, 0)) AS pims
    FROM SPORTS_AHL_PLAYERS_GAME_GOALIES
    WHERE season = v_season
    AND   game_type = v_game_type
    GROUP BY season,
             game_type,
             player_id,
             team_id
    ORDER BY player_id, season, MIN(game_id)
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          starts = VALUES(starts),
                          win = VALUES(win),
                          loss = VALUES(loss),
                          ot_loss = VALUES(ot_loss),
                          so_loss = VALUES(so_loss),
                          goals_against = VALUES(goals_against),
                          shots_against = VALUES(shots_against),
                          shutout = VALUES(shutout),
                          minutes_played = VALUES(minutes_played),
                          so_goals_against = VALUES(so_goals_against),
                          so_shots_against = VALUES(so_shots_against),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          pims = VALUES(pims);

END $$

DELIMITER ;

