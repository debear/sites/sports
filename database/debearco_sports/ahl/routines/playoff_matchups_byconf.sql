## ######################### ##
## Conference Based Matchups ##
## ######################### ##

##
## Conference Quarter Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_conference_quarter`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_conference_quarter`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff CQF matchup calcs'
BEGIN

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(10 + v_counter + 1 AS CHAR(2)), v_conf_id, 1, v_conf_id, 8, 0, 0, 0),
    (v_season, v_series_date, CAST(10 + v_counter + 2 AS CHAR(2)), v_conf_id, 2, v_conf_id, 7, 0, 0, 0),
    (v_season, v_series_date, CAST(10 + v_counter + 3 AS CHAR(2)), v_conf_id, 3, v_conf_id, 6, 0, 0, 0),
    (v_season, v_series_date, CAST(10 + v_counter + 4 AS CHAR(2)), v_conf_id, 4, v_conf_id, 5, 0, 0, 0);
  SET v_counter = v_counter + 4;

END $$

DELIMITER ;

##
## Conference Semi Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_conference_semi`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_conference_semi`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff CSF matchup calcs'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;
  DECLARE v_seed_c TINYINT UNSIGNED;
  DECLARE v_seed_d TINYINT UNSIGNED;

  # Re-seed into a sequential list
  CALL ahl_playoff_matchups_reseed_conference(v_conf_id);
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 2;
  SELECT pos_conf INTO v_seed_c FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 3;
  SELECT pos_conf INTO v_seed_d FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 4;

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(20 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_d, 0, 0, 0),
    (v_season, v_series_date, CAST(20 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_b, v_conf_id, v_seed_c, 0, 0, 0);
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

##
## Conference Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_conference_final`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_conference_final`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'AHL playoff CF matchup calcs'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, CAST(30 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;

##
## Reseed according to conference rules
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_reseed_conference`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_reseed_conference`(
  v_conf_id TINYINT UNSIGNED
)
    COMMENT 'AHL playoff conference reseeding'
BEGIN

  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpA');
  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpB');
  INSERT INTO tmp_seeds (team_id, reseed)
    SELECT tmp_seeds_cpA.team_id, COUNT(tmp_seeds_cpB.team_id) + 1
    FROM tmp_seeds_cpA
    LEFT JOIN tmp_seeds_cpB
      ON (tmp_seeds_cpB.conf_id = tmp_seeds_cpA.conf_id
      AND tmp_seeds_cpB.pos_conf < tmp_seeds_cpA.pos_conf)
    WHERE tmp_seeds_cpA.conf_id = v_conf_id
    GROUP BY tmp_seeds_cpA.team_id
  ON DUPLICATE KEY UPDATE reseed = VALUES(reseed);

END $$

DELIMITER ;
