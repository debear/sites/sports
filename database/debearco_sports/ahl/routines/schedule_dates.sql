#
# Dates on which games occur
#
DROP PROCEDURE IF EXISTS `ahl_schedule_dates`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_schedule_dates`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Establish the dates on which games occur'
BEGIN

  # Clear a previous version
  DELETE FROM SPORTS_AHL_SCHEDULE_DATES WHERE season = v_season;

  # (Re-)Build the base info.
  INSERT INTO SPORTS_AHL_SCHEDULE_DATES (season, `date`, num_games)
    SELECT season, game_date AS `date`, COUNT(*) AS num_games
    FROM SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    GROUP BY game_date
    ORDER BY game_date;

  # Previous/Next dates
  INSERT INTO SPORTS_AHL_SCHEDULE_DATES (season, `date`, num_games, date_prev, date_next)
    SELECT CURR.season, CURR.`date`, CURR.num_games, MAX(PREV.`date`) AS date_prev, MIN(NEXT.`date`) AS date_next
    FROM SPORTS_AHL_SCHEDULE_DATES AS CURR
    LEFT JOIN SPORTS_AHL_SCHEDULE_DATES AS PREV
      ON (PREV.season = CURR.season
      AND PREV.`date` < CURR.`date`)
    LEFT JOIN SPORTS_AHL_SCHEDULE_DATES AS NEXT
      ON (NEXT.season = CURR.season
      AND NEXT.`date` > CURR.`date`)
    WHERE CURR.season = v_season
    GROUP BY CURR.season, CURR.`date`
  ON DUPLICATE KEY UPDATE date_prev = VALUES(date_prev), date_next = VALUES(date_next);

  # Re-order
  ALTER TABLE SPORTS_AHL_SCHEDULE_DATES ORDER BY season, `date`;

END $$

DELIMITER ;

