##
## Team rosters
##
DROP PROCEDURE IF EXISTS `ahl_rosters_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_rosters_process`(
  v_date DATE
)
    COMMENT 'Calculate daily AHL team rosters'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_prev_date DATE;

  #
  # Determine the season (via date, not lookup)
  #
  SET v_season := fn_ahl_season_from_date(v_date);

  #
  # Clear a previous run
  #
  DELETE FROM SPORTS_AHL_TEAMS_ROSTERS WHERE season = v_season AND the_date = v_date;

  #
  # Stage 0: Tie any player_ids that have yet to be linked
  #
  UPDATE SPORTS_AHL_TRANSACTIONS AS TRANSACTION
  JOIN SPORTS_AHL_PLAYERS_IMPORT AS IMPORT
    ON (IMPORT.remote_id = TRANSACTION.remote_id)
  SET TRANSACTION.player_id = IMPORT.player_id
  WHERE TRANSACTION.season = v_season
  AND   TRANSACTION.the_date = v_date
  AND   TRANSACTION.player_id IS NULL;

  #
  # Stage 1: Start from the previous day
  #
  SELECT MAX(the_date) INTO v_prev_date
  FROM SPORTS_AHL_TEAMS_ROSTERS
  WHERE season = v_season
  AND   the_date < v_date;

  INSERT INTO SPORTS_AHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, capt_status, player_status)
    SELECT season, v_date AS the_date, team_id, player_id, jersey, pos, capt_status, player_status
    FROM SPORTS_AHL_TEAMS_ROSTERS
    WHERE season = v_season
    AND   the_date = v_prev_date;

  #
  # Stage 2: Apply changes from the transactions
  #
  INSERT INTO SPORTS_AHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, capt_status, player_status)
    SELECT season, the_date, team_id, player_id, NULL AS jersey, pos, NULL AS capt_status, IF(type = 'add', 'active', 'na') AS player_status
    FROM SPORTS_AHL_TRANSACTIONS
    WHERE season = v_season
    AND   the_date = v_date
    AND   type <> 'both'
  ON DUPLICATE KEY UPDATE player_status = VALUES(player_status);

  #
  # Stage 3: Record transactions as processed
  #
  UPDATE SPORTS_AHL_TRANSACTIONS
  SET processed = 1
  WHERE season = v_season
  AND   the_date = v_date;

  ##
  ## Game specific calcs
  ##
  DROP TEMPORARY TABLE IF EXISTS tmp_days_games;
  CREATE TEMPORARY TABLE tmp_days_games (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    game_id SMALLINT UNSIGNED,
    game_date DATE,
    PRIMARY KEY (season, game_type, game_id)
  ) ENGINE = MEMORY
    SELECT season, game_type, game_id, game_date
    FROM SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    AND   game_date = v_date;

  #
  # Stage 4: New additions from lineups (not in transactions)
  #
  INSERT INTO SPORTS_AHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, capt_status, player_status)
    SELECT tmp_days_games.season, tmp_days_games.game_date, LINEUP.team_id, LINEUP.player_id, LINEUP.jersey, LINEUP.pos, LINEUP.capt_status, 'active' AS player_status
    FROM tmp_days_games
    JOIN SPORTS_AHL_GAME_LINEUP AS LINEUP
      ON (LINEUP.season = tmp_days_games.season
      AND LINEUP.game_type = tmp_days_games.game_type
      AND LINEUP.game_id = tmp_days_games.game_id)
    LEFT JOIN SPORTS_AHL_TEAMS_ROSTERS AS ROSTER
      ON (ROSTER.season = tmp_days_games.season
      AND ROSTER.the_date = tmp_days_games.game_date
      AND ROSTER.team_id = LINEUP.team_id
      AND ROSTER.player_id = LINEUP.player_id)
    WHERE ROSTER.season IS NULL
  ON DUPLICATE KEY UPDATE team_id = VALUES(team_id),
                          jersey = VALUES(jersey),
                          pos = VALUES(pos),
                          capt_status = VALUES(capt_status),
                          player_status = VALUES(player_status);

  #
  # Stage 5: Captains / Alternates
  #  Taking care not to over-write teams whose games did not list any (which we assume was an error and not indicative of no team captains)
  #
  # First, games on this day with C/A set
  DROP TEMPORARY TABLE IF EXISTS tmp_days_games_capt;
  CREATE TEMPORARY TABLE tmp_days_games_capt (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    game_id SMALLINT UNSIGNED,
    game_date DATE,
    team_id VARCHAR(3),
    PRIMARY KEY (season, game_type, game_id, team_id)
  ) ENGINE = MEMORY
    SELECT DISTINCT tmp_days_games.season, tmp_days_games.game_type, tmp_days_games.game_id, tmp_days_games.game_date, LINEUP.team_id
    FROM tmp_days_games
    JOIN SPORTS_AHL_GAME_LINEUP AS LINEUP
      ON (LINEUP.season = tmp_days_games.season
      AND LINEUP.game_type = tmp_days_games.game_type
      AND LINEUP.game_id = tmp_days_games.game_id
      AND LINEUP.capt_status IS NOT NULL);

  # Then update teams in this list
  UPDATE tmp_days_games_capt AS SCHEDULE
  JOIN SPORTS_AHL_TEAMS_ROSTERS AS ROSTER
    ON (ROSTER.season = SCHEDULE.season
    AND ROSTER.the_date = SCHEDULE.game_date
    AND ROSTER.team_id = SCHEDULE.team_id)
  LEFT JOIN SPORTS_AHL_GAME_LINEUP AS LINEUP
    ON (LINEUP.season = SCHEDULE.season
    AND LINEUP.game_type = SCHEDULE.game_type
    AND LINEUP.game_id = SCHEDULE.game_id
    AND LINEUP.team_id = SCHEDULE.team_id
    AND LINEUP.player_id = ROSTER.player_id)
  SET ROSTER.capt_status = LINEUP.capt_status;

  #
  # Stage 6: Missing jerseys
  #
  UPDATE tmp_days_games AS SCHEDULE
  JOIN SPORTS_AHL_TEAMS_ROSTERS AS ROSTER
    ON (ROSTER.season = SCHEDULE.season
    AND ROSTER.the_date = SCHEDULE.game_date
    AND ROSTER.jersey IS NULL)
  JOIN SPORTS_AHL_GAME_LINEUP AS LINEUP
    ON (LINEUP.season = SCHEDULE.season
    AND LINEUP.game_type = SCHEDULE.game_type
    AND LINEUP.game_id = SCHEDULE.game_id
    AND LINEUP.team_id = ROSTER.team_id
    AND LINEUP.player_id = ROSTER.player_id)
  SET ROSTER.jersey = LINEUP.jersey;

END $$

DELIMITER ;

