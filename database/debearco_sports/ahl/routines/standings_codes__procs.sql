##
##
##
DROP PROCEDURE IF EXISTS `ahl_standings_codes_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_codes_setup`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Prepare AHL standing code processing'
BEGIN

  DECLARE v_season_finale DATE;
  # Determine which standings are final (so we can more accurately process the codes)
  SELECT MAX(game_date) INTO v_season_finale FROM SPORTS_AHL_SCHEDULE WHERE season = v_season AND game_type = 'regular';
  UPDATE tmp_standings SET _tb_final = (the_date = v_season_finale);

END $$

DELIMITER ;

##
## Process status codes
##
DROP PROCEDURE IF EXISTS `ahl_standings_codes_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_codes_process`(
  v_season SMALLINT UNSIGNED,
  v_code CHAR(1),
  v_col VARCHAR(6),
  v_cutoff_pos TINYINT UNSIGNED
)
    COMMENT 'Process AHL standing codes'
BEGIN

  DECLARE v_sort_col VARCHAR(7);
  DECLARE v_cmp_col VARCHAR(17);

  # Pre-2015, when each team played same number of games, initial sort was points
  IF v_season < 2015 THEN
    SET v_sort_col := 'pts';
    SET v_cmp_col := v_sort_col;

  # 2015 onwards, where each team may not play the same number of games, initial sort was points percentage
  ELSE
    SET v_sort_col := 'pts_pct';
    SET v_cmp_col := '_tb_worst_pts_pct';

  END IF;

  # Simplify our query by generalising our position and join cols
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = IF("', v_col, '" = "league", _tb_league_id, IF("', v_col, '" = "conf", _tb_conf_id, _tb_div_id)),
                                              _tb_pos = IF("', v_col, '" = "league", pos_league, IF("', v_col, '" = "conf", pos_conf, pos_div));'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  # Process - Guesstimate
  CALL _exec(CONCAT('
    INSERT INTO tmp_standings (season, the_date, team_id, status_code)
      SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
             IF(tmp_standings_cpB.team_id IS NULL, "', v_code, '", tmp_standings_cpA.status_code) AS status_code
      FROM tmp_standings_cpA
      LEFT JOIN tmp_standings_cpB
        ON (tmp_standings_cpB.season = tmp_standings_cpA.season
        AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
        AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
        AND tmp_standings_cpB._tb_pos > ', v_cutoff_pos, '
        AND (tmp_standings_cpB.max_', v_sort_col, ' > tmp_standings_cpA.', v_cmp_col, '
          OR tmp_standings_cpB.max_', v_sort_col, ' = tmp_standings_cpA.', v_cmp_col, '
           AND tmp_standings_cpB._tb_max_reg_ot_wins > tmp_standings_cpA.reg_ot_wins))
      WHERE (tmp_standings_cpA.wins + tmp_standings_cpA.loss + tmp_standings_cpA.ot_loss + tmp_standings_cpA.so_loss) > 10
      AND   tmp_standings_cpA._tb_final = 0
      AND   tmp_standings_cpA._tb_pos <= ', v_cutoff_pos, '
      GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
    ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);'));

  # Process - Final
  INSERT INTO tmp_standings (season, the_date, team_id, status_code)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           v_code AS status_code
    FROM tmp_standings_cpA
    WHERE tmp_standings_cpA._tb_final = 1
    AND   tmp_standings_cpA._tb_pos <= v_cutoff_pos
  ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);

END $$

DELIMITER ;

