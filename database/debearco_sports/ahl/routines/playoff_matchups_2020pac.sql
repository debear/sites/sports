## ################################# ##
## 2020/21 Pacific Division Matchups ##
## ################################# ##

##
## Play-in Round 1
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_2020_playin_rnd1`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_2020_playin_rnd1`(
  IN v_season SMALLINT UNSIGNED,
  IN v_series_date DATE,
  IN v_conf_id TINYINT UNSIGNED
)
    COMMENT 'AHL playoff 2020/21 Pacific Division first play-in matchup calcs'
BEGIN

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, '11', v_conf_id, 4, v_conf_id, 7, 0, 0, 0),
    (v_season, v_series_date, '12', v_conf_id, 5, v_conf_id, 6, 0, 0, 0),
    (v_season, v_series_date, '31', v_conf_id, 1, v_conf_id, 0, 0, 0, 0),
    (v_season, v_series_date, '32', v_conf_id, 2, v_conf_id, 3, 0, 0, 0);

END $$

DELIMITER ;

##
## Play-in Round 2
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_2020_playin_rnd2`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_2020_playin_rnd2`(
  IN v_season SMALLINT UNSIGNED,
  IN v_series_date DATE,
  IN v_conf_id TINYINT UNSIGNED
)
    COMMENT 'AHL playoff 2020/21 Pacific Division second play-in matchup calcs'
BEGIN

  DECLARE v_playin_hi TINYINT UNSIGNED;
  DECLARE v_playin_lo TINYINT UNSIGNED;
  SELECT pos_conf INTO v_playin_hi FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf > 3 ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_playin_lo FROM tmp_seeds WHERE conf_id = v_conf_id AND pos_conf > 3 ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, '21', v_conf_id, v_playin_hi, v_conf_id, v_playin_lo, 0, 0, 0),
    (v_season, v_series_date, '31', v_conf_id, 1, v_conf_id, 0, 0, 0, 0),
    (v_season, v_series_date, '32', v_conf_id, 2, v_conf_id, 3, 0, 0, 0);

END $$

DELIMITER ;

##
## Semi Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_2020_semi`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_2020_semi`(
  IN v_season SMALLINT UNSIGNED,
  IN v_series_date DATE,
  IN v_conf_id TINYINT UNSIGNED
)
    COMMENT 'AHL playoff 2020/21 Pacific Division semi-final matchup calcs'
BEGIN

  DECLARE v_seed_prelim TINYINT UNSIGNED;
  SELECT pos_conf INTO v_seed_prelim FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, '31', v_conf_id, 1, v_conf_id, v_seed_prelim, 0, 0, 0),
    (v_season, v_series_date, '32', v_conf_id, 2, v_conf_id, 3, 0, 0, 0);

END $$

DELIMITER ;

##
## Final
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_2020_final`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_2020_final`(
  IN v_season SMALLINT UNSIGNED,
  IN v_series_date DATE,
  IN v_conf_id TINYINT UNSIGNED
)
    COMMENT 'AHL playoff 2020/21 Pacific Division final matchup calcs'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_AHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games, complete) VALUES
    (v_season, v_series_date, '41', v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0, 0);

END $$

DELIMITER ;
