##
## Determine positions for a particular position type
##
DROP PROCEDURE IF EXISTS `ahl_standings_pos`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_pos`(
  v_season SMALLINT UNSIGNED,
  v_col VARCHAR(6)
)
    COMMENT 'Calculate AHL standing positions'
BEGIN

  #
  # Stage 0: Simplify our query by generalising our position and join cols
  #
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = IF("', v_col, '" = "league", _tb_league_id, IF("', v_col, '" = "conf", _tb_conf_id, _tb_div_id));'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  #
  # Stage 1: Basic sort
  #
  CALL ahl_standings_ties_sort(v_season, v_col);

  #
  # Stage 2: Tie-breaks...
  #

  # From 2015-16 to 2020-21, and from 2022-23, when teams did not necessarily play same number of games, initial
  #  sort is points percentage, so secondary sort should be points
  IF v_season BETWEEN 2015 AND 2020 OR v_season >= 2022 THEN
    CALL ahl_standings_ties_identify();
    IF ahl_standings_ties_num() > 0 THEN
      CALL ahl_standings_ties_pts();
      CALL ahl_standings_ties_break();
    END IF;
  END IF;

  # In 2021-22, relative win numbers were used
  IF v_season = 2021 THEN
    # Regulation Win Pct
    CALL ahl_standings_ties_identify();
    IF ahl_standings_ties_num() > 0 THEN
      CALL ahl_standings_ties_pct_winreg();
      CALL ahl_standings_ties_break();
    END IF;

    # Regulation/OT Win Pct
    CALL ahl_standings_ties_identify();
    IF ahl_standings_ties_num() > 0 THEN
      CALL ahl_standings_ties_pct_winrow();
      CALL ahl_standings_ties_break();
    END IF;

    # Any Win Pct
    CALL ahl_standings_ties_identify();
    IF ahl_standings_ties_num() > 0 THEN
      CALL ahl_standings_ties_pct_win();
      CALL ahl_standings_ties_break();
    END IF;

  # Other than then, it uses absolute numbers
  ELSE
    # Reg/OT Wins
    CALL ahl_standings_ties_identify();
    IF ahl_standings_ties_num() > 0 THEN
      CALL ahl_standings_ties_regotwin();
      CALL ahl_standings_ties_break();
    END IF;
  END IF;

  # Points in season series (we will run as point pct for compatibility between 2 and 3+ team ties)
  CALL ahl_standings_ties_identify();
  IF ahl_standings_ties_num() > 0 THEN
    CALL ahl_standings_ties_series_ptspct();
    CALL ahl_standings_ties_break();
  END IF;

  # Goal Differential
  CALL ahl_standings_ties_identify();
  IF ahl_standings_ties_num() > 0 THEN
    CALL ahl_standings_ties_goal_diff();
    CALL ahl_standings_ties_break();
  END IF;

  # Goals Scored (2 team tie) and Differential (3+ team tie) in season series
  CALL ahl_standings_ties_identify();
  IF ahl_standings_ties_num() > 0 THEN
    CALL ahl_standings_ties_series_scoring();
    CALL ahl_standings_ties_break();
  END IF;

  # Intra-conference points percentage
  CALL ahl_standings_ties_identify();
  IF ahl_standings_ties_num() > 0 THEN
    CALL ahl_standings_ties_intra_conf();
    CALL ahl_standings_ties_break();
  END IF;

  # Aribtrary final (guaranteed) sort
  CALL ahl_standings_ties_identify();
  IF ahl_standings_ties_num() > 0 THEN
    CALL ahl_standings_ties_guaranteed();
    CALL ahl_standings_ties_break();
  END IF;

  #
  # Stage Z: Propogate back to standings table
  #
  CALL _exec(CONCAT('UPDATE tmp_standings SET pos_', v_col, ' = _tb_pos;'));

END $$

DELIMITER ;

