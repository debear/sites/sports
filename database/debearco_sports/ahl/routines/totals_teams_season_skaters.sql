##
## Team season totals
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams_season_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_season_skaters`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate team season totals for AHL team skaters'
BEGIN

  ##
  ## Combine daily figures into a single season
  ##
  INSERT INTO SPORTS_AHL_TEAMS_SEASON_SKATERS (season, season_type, team_id, stat_dir, gp, goals, assists, plus_minus, pims, pp_goals, pp_assists, pp_opps, pk_goals, pk_kills, pk_opps, sh_goals, sh_assists, shots, so_goals, so_shots)
    SELECT season,
           game_type AS season_type,
           team_id,
           stat_dir,
           SUM(IFNULL(gp, 0)) AS gp,
           SUM(IFNULL(goals, 0)) AS goals,
           SUM(IFNULL(assists, 0)) AS assists,
           SUM(IFNULL(plus_minus, 0)) AS plus_minus,
           SUM(IFNULL(pims, 0)) AS pims,
           SUM(IFNULL(pp_goals, 0)) AS pp_goals,
           SUM(IFNULL(pp_assists, 0)) AS pp_assists,
           SUM(IFNULL(pp_opps, 0)) AS pp_opps,
           SUM(IFNULL(pk_goals, 0)) AS pk_goals,
           SUM(IFNULL(pk_kills, 0)) AS pk_kills,
           SUM(IFNULL(pk_opps, 0)) AS pk_opps,
           SUM(IFNULL(sh_goals, 0)) AS sh_goals,
           SUM(IFNULL(sh_assists, 0)) AS sh_assists,
           SUM(IFNULL(shots, 0)) AS shots,
           SUM(IFNULL(so_goals, 0)) AS so_goals,
           SUM(IFNULL(so_shots, 0)) AS so_shots
    FROM SPORTS_AHL_TEAMS_GAME_SKATERS
    WHERE season = v_season
    AND   game_type = v_game_type
    GROUP BY season,
             game_type,
             team_id,
             stat_dir
    ORDER BY season, game_type, team_id, stat_dir
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          plus_minus = VALUES(plus_minus),
                          pims = VALUES(pims),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          pp_opps = VALUES(pp_opps),
                          pk_goals = VALUES(pk_goals),
                          pk_kills = VALUES(pk_kills),
                          pk_opps = VALUES(pk_opps),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          shots = VALUES(shots),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots);

END $$

DELIMITER ;

