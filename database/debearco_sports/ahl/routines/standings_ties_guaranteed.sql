##
## A final, arbitrary, sorting to guarantee broken tie
##
DROP PROCEDURE IF EXISTS `ahl_standings_ties_guaranteed`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_ties_guaranteed`()
    COMMENT 'Attempt to break AHL standing ties on something guaranteed'
BEGIN

  INSERT INTO tmp_standings (season, the_date, team_id, _tb_pos_adj)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           COUNT(tmp_standings_cpB.team_id) AS _tb_pos_adj
    FROM tmp_standings_cpA
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos = tmp_standings_cpA._tb_pos
      AND STRCMP(tmp_standings_cpB.team_id, tmp_standings_cpA.team_id) = -1)
    WHERE tmp_standings_cpA._tb_is_tied = 1
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
  ON DUPLICATE KEY UPDATE _tb_pos_adj = VALUES(_tb_pos_adj);

END $$

DELIMITER ;

