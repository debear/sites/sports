##
## Process rosters using available info
##
DROP PROCEDURE IF EXISTS `ahl_rosters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_rosters`()
    COMMENT 'Update AHL team rosters'
BEGIN

  DECLARE v_date DATE;
  DECLARE v_sched_start_date DATE;
  DECLARE v_sched_end_date DATE;
  DECLARE v_trans_start_date DATE;

  # Determine date range - either first schedule or first transaction
  SELECT MIN(the_date), MAX(the_date) INTO v_sched_start_date, v_sched_end_date FROM tmp_date_list;
  SELECT MIN(the_date) INTO v_trans_start_date FROM SPORTS_AHL_TRANSACTIONS WHERE season = fn_ahl_season_from_date(v_sched_start_date) AND processed = 0;
  SELECT IF(v_sched_start_date > v_trans_start_date, v_trans_start_date, v_sched_start_date) INTO v_date;

  # And process for each date in this range
  WHILE v_date <= v_sched_end_date DO
    CALL ahl_rosters_process(v_date);
    SELECT DATE_ADD(v_date, INTERVAL 1 DAY) INTO v_date;
  END WHILE;

  ALTER TABLE SPORTS_AHL_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;

END $$

DELIMITER ;

##
## Initial roster processing
##
DROP PROCEDURE IF EXISTS `ahl_rosters_initial`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_rosters_initial`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Update AHL team rosters after the initial loading'
BEGIN

  DECLARE v_date DATE;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  DECLARE cur_date CURSOR FOR
    SELECT DISTINCT(the_date) FROM SPORTS_AHL_TRANSACTIONS WHERE season = v_season AND processed = 0 ORDER BY the_date;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Loop through
  OPEN cur_date;
  loop_date: LOOP

    FETCH cur_date INTO v_date;
    IF v_done = 1 THEN LEAVE loop_date; END IF;

    CALL ahl_rosters_process(v_date);

  END LOOP loop_date;
  CLOSE cur_date;

END $$

DELIMITER ;

