##
## Backdate AHL rosters
##
DROP PROCEDURE IF EXISTS `ahl_rosters_backdated`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_rosters_backdated` (
  v_season SMALLINT UNSIGNED,
  v_date DATE
)
    COMMENT 'Create initial AHL rosters from teams first games'
BEGIN

  DECLARE v_game_id SMALLINT UNSIGNED;
  DECLARE v_team_id VARCHAR(3);
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  DECLARE cur_team CURSOR FOR
    SELECT game_id, team_id FROM tmp_first_games ORDER BY team_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Get the first games
  CALL ahl_rosters_backdated_first_games(v_season);

  # Loop through
  OPEN cur_team;
  loop_team: LOOP

    FETCH cur_team INTO v_game_id, v_team_id;
    IF v_done = 1 THEN LEAVE loop_team; END IF;

    CALL ahl_rosters_backdated_team(v_season, v_date, v_team_id, v_game_id);

  END LOOP loop_team;
  CLOSE cur_team;

END $$

DELIMITER ;

# Determine first games
DROP PROCEDURE IF EXISTS `ahl_rosters_backdated_first_games`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_rosters_backdated_first_games` (
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Determine AHL teams first games'
BEGIN

  # Determine each teams first games
  DROP TEMPORARY TABLE IF EXISTS tmp_first_games;
  CREATE TEMPORARY TABLE tmp_first_games (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular','playoff'),
    game_id SMALLINT UNSIGNED,
    game_date DATE,
    team_id VARCHAR(3),
    game_num TINYINT UNSIGNED,
    opp_id VARCHAR(3),
    venue ENUM('home','visitor'),
    PRIMARY KEY (season, game_type, game_id, team_id),
    INDEX for_join (season, game_type, team_id, game_date) USING BTREE
  ) ENGINE = MEMORY;
  # Home teams
  INSERT INTO tmp_first_games (season, game_type, game_id, game_date, team_id, game_num, opp_id, venue)
    SELECT season, game_type, game_id, game_date, home, 0 AS game_num, visitor, 'home' AS venue
    FROM SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular';
  # Visitors
  INSERT INTO tmp_first_games (season, game_type, game_id, game_date, team_id, game_num, opp_id, venue)
    SELECT season, game_type, game_id, game_date, visitor, 0 AS game_num, home, 'visitor' AS venue
    FROM SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular';

  # Get sequence numbers
  CALL _duplicate_tmp_table('tmp_first_games', 'tmp_first_games_cpA');
  CALL _duplicate_tmp_table('tmp_first_games', 'tmp_first_games_cpB');
  INSERT INTO tmp_first_games (season, game_type, game_id, game_date, team_id, game_num)
    SELECT tmp_first_games_cpA.season, tmp_first_games_cpA.game_type, tmp_first_games_cpA.game_id, tmp_first_games_cpA.game_date, tmp_first_games_cpA.team_id,
           COUNT(tmp_first_games_cpB.game_id) + 1 AS game_num
    FROM tmp_first_games_cpA
    LEFT JOIN tmp_first_games_cpB
      ON (tmp_first_games_cpB.season = tmp_first_games_cpA.season
      AND tmp_first_games_cpB.game_type = tmp_first_games_cpA.game_type
      AND tmp_first_games_cpB.team_id = tmp_first_games_cpA.team_id
      AND tmp_first_games_cpB.game_date < tmp_first_games_cpA.game_date)
    GROUP BY tmp_first_games_cpA.season, tmp_first_games_cpA.game_type, tmp_first_games_cpA.game_id, tmp_first_games_cpA.team_id
  ON DUPLICATE KEY UPDATE game_num = VALUES(game_num);

  # Only keep the first games
  DELETE FROM tmp_first_games WHERE game_num > 1;

END $$

DELIMITER ;

# Process a single team
DROP PROCEDURE IF EXISTS `ahl_rosters_backdated_team`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_rosters_backdated_team` (
  v_season SMALLINT UNSIGNED,
  v_date DATE,
  v_team_id VARCHAR(3),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Determine AHL teams first lineup'
BEGIN

  DECLARE v_date_str CHAR(8);

  # Get roster
  DECLARE v_fname VARCHAR(50);
  DECLARE v_sname VARCHAR(50);
  DECLARE v_pos VARCHAR(3);
  DECLARE v_remote_id SMALLINT UNSIGNED;
  DECLARE v_player_id SMALLINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  DECLARE cur_player CURSOR FOR
    SELECT PLAYER.first_name, PLAYER.surname, LINEUP.pos, IMPORT.remote_id, IMPORT.player_id
    FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
    JOIN SPORTS_AHL_PLAYERS AS PLAYER
      ON (PLAYER.player_id = LINEUP.player_id)
    JOIN SPORTS_AHL_PLAYERS_IMPORT AS IMPORT
      ON (IMPORT.player_id = LINEUP.player_id)
    WHERE LINEUP.season = v_season
    AND   LINEUP.game_type = 'regular'
    AND   LINEUP.game_id = v_game_id
    AND   LINEUP.team_id = v_team_id
    ORDER BY PLAYER.surname, PLAYER.first_name;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  SET v_date_str := REPLACE(v_date, '-', '');

  # Header
  SELECT '#' AS head;
  SELECT CONCAT('# ', v_team_id) AS head;
  SELECT '#' AS head;
  SELECT '' AS head_sep;

  # Loop through
  OPEN cur_player;
  loop_player: LOOP

    FETCH cur_player INTO v_fname, v_sname, v_pos, v_remote_id, v_player_id;
    IF v_done = 1 THEN LEAVE loop_player; END IF;

    SELECT CONCAT('# ', v_fname, ' ', v_sname, ' (', v_pos, ') (Remote: ', v_remote_id, ') :: Added') AS pl_cmnt;
    SELECT CONCAT('SELECT player_id INTO @player_', v_date_str, '_', v_remote_id, ' FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = ', v_remote_id, ';') AS pl_id;
    SELECT CONCAT('INSERT INTO SPORTS_AHL_TRANSACTIONS (season, the_date, remote_id, player_id, team_id, pos, type, detail) VALUES (\'', v_season, '\', \'', v_date, '\', \'', v_remote_id, '\', @player_', v_date_str, '_', v_remote_id, ', \'', v_team_id, '\', \'', v_pos, '\', \'add\', \'Added\') ON DUPLICATE KEY UPDATE type = IF(type = VALUES(type), VALUES(type), \'both\');') AS pl_ins;
    SELECT '' AS pl_sep;

  END LOOP loop_player;
  CLOSE cur_player;

END $$

DELIMITER ;

