##
## Season Series Point Percentage
##
DROP PROCEDURE IF EXISTS `ahl_standings_ties_series_ptspct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_ties_series_ptspct`()
    COMMENT 'Attempt to break AHL standing ties on Series Pts %age'
BEGIN

  # Determine common game results
  CALL ahl_standings_ties_common();
  CALL ahl_standings_ties_series_ptspct_process();

  # Process
  INSERT INTO tmp_standings (season, the_date, team_id, _tb_pos_adj)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           COUNT(tmp_standings_cpB.team_id) AS _tb_pos_adj
    FROM tmp_standings_cpA
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos = tmp_standings_cpA._tb_pos
      AND tmp_standings_cpB._tb_series_pts_pct > tmp_standings_cpA._tb_series_pts_pct)
    WHERE tmp_standings_cpA._tb_is_tied = 1
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
  ON DUPLICATE KEY UPDATE _tb_pos_adj = VALUES(_tb_pos_adj);

END $$

DELIMITER ;

#
# Process the common game list to determine each teams point percentage
#
DROP PROCEDURE IF EXISTS `ahl_standings_ties_series_ptspct_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_ties_series_ptspct_process`()
    COMMENT 'Process AHL standing ties to determine Series Pts %age'
BEGIN

  UPDATE tmp_standings
  JOIN tmp_standings_common_sched
    ON (tmp_standings_common_sched.season = tmp_standings.season
    AND tmp_standings_common_sched.the_date = tmp_standings.the_date
    AND tmp_standings_common_sched.team_id = tmp_standings.team_id)
  SET tmp_standings._tb_series_pts_pct = tmp_standings_common_sched.sum_pts_pct;

END $$

DELIMITER ;

