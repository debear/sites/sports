#
# Initial standings
#
DROP PROCEDURE IF EXISTS `ahl_standings_initial`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_standings_initial`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Create the initial AHL standings for a season'
BEGIN

  # Declare our vars
  DECLARE v_date DATE;

  # Get meta out
  SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) INTO v_date FROM SPORTS_AHL_SCHEDULE WHERE season = v_season;

  # Get the list of teams
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    city VARCHAR(20),
    franchise VARCHAR(20),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id),
    INDEX by_def (team_id, conf_id, div_id)
  ) ENGINE = MEMORY
    SELECT TEAM_DIV.team_id, TEAM.city, TEAM.franchise,
           DIVN.parent_id AS conf_id,
           DIVN.grouping_id AS div_id
    FROM SPORTS_AHL_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_AHL_TEAMS AS TEAM
      ON (TEAM.team_id = TEAM_DIV.team_id)
    JOIN SPORTS_AHL_GROUPINGS AS DIVN
      ON (DIVN.grouping_id = TEAM_DIV.grouping_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099)
    ORDER BY TEAM_DIV.team_id;
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # And their max number of games played
  DROP TEMPORARY TABLE IF EXISTS tmp_teams_max_gp;
  CREATE TEMPORARY TABLE tmp_teams_max_gp (
    team_id VARCHAR(3),
    max_gp TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT home AS team_id, 2 * COUNT(*) AS max_gp
    FROM SPORTS_AHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'regular'
    GROUP BY home;

  # Store our table
  DELETE FROM SPORTS_AHL_STANDINGS WHERE season = v_season;
  INSERT INTO SPORTS_AHL_STANDINGS (season, the_date, team_id, wins, loss, ot_loss, so_loss, pts, pts_pct, goals_for, goals_against, home_wins, home_loss, home_ot_loss, home_so_loss, visitor_wins, visitor_loss, visitor_ot_loss, visitor_so_loss, recent_wins, recent_loss, recent_ot_loss, recent_so_loss, pos_league, pos_conf, pos_div, max_pts, max_pts_pct, reg_ot_wins, status_code)
    SELECT v_season, v_date, tmp_teams.team_id, 0, 0, 0, 0, 0, 0.000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 * tmp_teams_max_gp.max_gp, 1.000, 0, NULL
    FROM tmp_teams
    JOIN tmp_teams_max_gp
      ON (tmp_teams_max_gp.team_id = tmp_teams.team_id);

  # Sort league
  INSERT INTO SPORTS_AHL_STANDINGS (season, the_date, team_id, pos_league)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_league = VALUES(pos_league);

  # Sort conference
  INSERT INTO SPORTS_AHL_STANDINGS (season, the_date, team_id, pos_conf)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.conf_id = ME.conf_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_conf = VALUES(pos_conf);

  # Sort division
  INSERT INTO SPORTS_AHL_STANDINGS (season, the_date, team_id, pos_div)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.conf_id = ME.conf_id
      AND OTHER.div_id = ME.div_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_div = VALUES(pos_div);

  ALTER TABLE SPORTS_AHL_STANDINGS ORDER BY season, the_date, team_id;

END $$

DELIMITER ;

