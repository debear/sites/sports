 ##
## Player season totals
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_skaters`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for AHL skaters'
BEGIN

  ##
  ## Combine daily figures into a single season
  ##
  INSERT INTO SPORTS_AHL_PLAYERS_SEASON_SKATERS (season, season_type, player_id, team_id, gp, starts, goals, assists, plus_minus, pims, pp_goals, pp_assists, sh_goals, sh_assists, gw_goals, shots, so_goals, so_shots)
    SELECT season,
           game_type AS season_type,
           player_id,
           team_id,
           SUM(IFNULL(gp, 0)) AS gp,
           SUM(IFNULL(start, 0)) AS starts,
           SUM(IFNULL(goals, 0)) AS goals,
           SUM(IFNULL(assists, 0)) AS assists,
           SUM(IFNULL(plus_minus, 0)) AS plus_minus,
           SUM(IFNULL(pims, 0)) AS pims,
           SUM(IFNULL(pp_goals, 0)) AS pp_goals,
           SUM(IFNULL(pp_assists, 0)) AS pp_assists,
           SUM(IFNULL(sh_goals, 0)) AS sh_goals,
           SUM(IFNULL(sh_assists, 0)) AS sh_assists,
           SUM(IFNULL(gw_goals, 0)) AS gw_goals,
           SUM(IFNULL(shots, 0)) AS shots,
           SUM(IFNULL(so_goals, 0)) AS so_goals,
           SUM(IFNULL(so_shots, 0)) AS so_shots
    FROM SPORTS_AHL_PLAYERS_GAME_SKATERS
    WHERE season = v_season
    AND   game_type = v_game_type
    GROUP BY season,
             game_type,
             player_id,
             team_id
    ORDER BY player_id, season, MIN(game_id)
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          starts = VALUES(starts),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          plus_minus= VALUES(plus_minus),
                          pims = VALUES(pims),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          gw_goals = VALUES(gw_goals),
                          shots = VALUES(shots),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots);

END $$

DELIMITER ;

