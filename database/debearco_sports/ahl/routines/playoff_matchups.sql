##
## Playoff matchups
##  Need to process all four rounds, as this generates the round codes used in game_id allocation
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Determine AHL playoff matchups, allocating round_codes'
proc: BEGIN

  DECLARE v_round_first TINYINT UNSIGNED DEFAULT 1;
  DECLARE v_round_last TINYINT UNSIGNED DEFAULT 4;
  DECLARE v_round_proc TINYINT UNSIGNED;
  DECLARE v_series_date DATE;
  DECLARE v_is_pre_playoffs TINYINT UNSIGNED;

  # Identify the rounds to loop through
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_round_num TINYINT UNSIGNED;
  DECLARE cur_rounds CURSOR FOR
    SELECT round_num
    FROM tmp_series_proc
    WHERE proc_create = 1
    ORDER BY round_num;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Does not apply to 2019/20 when no playoffs were held
  IF v_season = 2019 THEN
    LEAVE proc;
  END IF;

  # From 2021/22 onwards, the first round has round_code zero (as there are now five rounds)
  IF v_season >= 2021 THEN
    SET v_round_first = 0;
  END IF;

  # Create some (empty) temporary tables we'll need later
  DROP TEMPORARY TABLE IF EXISTS tmp_series_rounds_stage0;
  CREATE TEMPORARY TABLE tmp_series_rounds_stage0 (
    round_code CHAR(2),
    last_date DATE,
    started TINYINT UNSIGNED,
    PRIMARY KEY (round_code)
  ) ENGINE = MEMORY;
  DROP TEMPORARY TABLE IF EXISTS tmp_series_rounds;
  CREATE TEMPORARY TABLE tmp_series_rounds (
    round_num TINYINT UNSIGNED,
    last_date DATE,
    series_still_tbc TINYINT UNSIGNED,
    started TINYINT UNSIGNED,
    complete TINYINT UNSIGNED,
    PRIMARY KEY (round_num)
  ) ENGINE = MEMORY;

  # In pre-playoff calcs (before a game has been played), we are always (and only) processing the first round
  SELECT COUNT(*) = 0 INTO v_is_pre_playoffs
  FROM SPORTS_AHL_SCHEDULE
  WHERE season = v_season
  AND   game_type = 'playoff'
  AND   IFNULL(status, 'NULL') NOT IN ('NULL','PPD','CNC');

  IF v_is_pre_playoffs = 1 THEN
    SELECT DATE_ADD(MAX(game_date), INTERVAL 1 DAY) INTO v_series_date FROM SPORTS_AHL_SCHEDULE WHERE season = v_season AND game_type = 'regular';
    CALL ahl_playoff_matchups_round(v_season, v_round_first, v_series_date);
    LEAVE proc;
  END IF;

  # If we have started the playoffs, determine the round(s) which we need to process
  # - Last series date we have previously processed for each round
  INSERT INTO tmp_series_rounds_stage0
    SELECT round_code, MAX(the_date) AS last_date, (MAX(higher_games) + MAX(lower_games)) > 0 AS started, MAX(complete) AS complete
    FROM SPORTS_AHL_PLAYOFF_SERIES
    WHERE season = v_season
    GROUP BY round_code;
  # - Determine if each round has been completed
  INSERT INTO tmp_series_rounds
    SELECT SUBSTRING(LPAD(ROUNDS.round_code, 2, '0'), 1, 1) AS round_num,
           ROUNDS.last_date,
           SUM(ROUNDS.started = 1 AND ROUNDS.complete = 0) > 0 AS series_still_tbc,
           MAX(ROUNDS.started) AS started,
           MIN(SERIES.complete) AS complete
    FROM tmp_series_rounds_stage0 AS ROUNDS
    JOIN SPORTS_AHL_PLAYOFF_SERIES AS SERIES
      ON (SERIES.season = v_season
      AND SERIES.round_code = ROUNDS.round_code
      AND SERIES.the_date = ROUNDS.last_date)
    GROUP BY round_num;
  # - Seed our base table to identify the processable rounds
  DROP TEMPORARY TABLE IF EXISTS tmp_series_proc;
  CREATE TEMPORARY TABLE tmp_series_proc (
    round_num TINYINT UNSIGNED,
    proc_create TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (round_num)
  ) ENGINE = MEMORY;
  SET v_round_proc = v_round_first;
  WHILE v_round_proc <= v_round_last DO
    INSERT INTO tmp_series_proc (round_num) VALUES (v_round_proc);
    SET v_round_proc = v_round_proc + 1;
  END WHILE;
  # - Now use this to calculate if any rounds need creating
  CALL _duplicate_tmp_table('tmp_series_proc', 'tmp_series_proc_cp');
  CALL _duplicate_tmp_table('tmp_series_rounds', 'tmp_series_rounds_cp');
  INSERT INTO tmp_series_proc (round_num, proc_create)
    SELECT tmp_base.round_num,
           (tmp_last.complete = 1) AND (tmp_round.round_num IS NULL OR tmp_round.series_still_tbc = 1) AS proc_create
    FROM tmp_series_proc_cp AS tmp_base
    LEFT JOIN tmp_series_rounds AS tmp_round
      ON (tmp_round.round_num = tmp_base.round_num)
    LEFT JOIN tmp_series_rounds_cp AS tmp_last
      ON (CAST(tmp_last.round_num AS SIGNED) = CAST(tmp_base.round_num AS SIGNED) - 1)
    GROUP BY tmp_base.round_num
    HAVING proc_create IS NOT NULL
  ON DUPLICATE KEY UPDATE proc_create = VALUES(proc_create);

  # Process the rounds
  SET v_done := 0; # Reset the continue handler tracker
  OPEN cur_rounds;
  loop_rounds: LOOP

    FETCH cur_rounds INTO v_round_num;
    IF v_done = 1 THEN LEAVE loop_rounds; END IF;
    SELECT DATE_ADD(MAX(last_date), INTERVAL 1 DAY) INTO v_series_date FROM tmp_series_rounds WHERE round_num <= v_round_num;
    CALL ahl_playoff_matchups_round(v_season, v_round_num, v_series_date);

  END LOOP loop_rounds;
  CLOSE cur_rounds;

END $$

DELIMITER ;

# Process the series for a specific round
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_round`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_round`(
  v_season SMALLINT UNSIGNED,
  v_round_num TINYINT UNSIGNED,
  v_series_date DATE
)
    COMMENT 'Determine AHL playoff matchups within a specific round'
proc: BEGIN

  # Process for determining the conferences to loop through
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_conf_id TINYINT UNSIGNED;
  DECLARE v_counter TINYINT UNSIGNED DEFAULT 0;
  DECLARE cur_conf CURSOR FOR
    SELECT DISTINCT conf_id FROM tmp_div_list;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # Prep
  CALL ahl_playoff_matchups_setup(v_season, v_round_num, v_series_date);
  IF v_season >= 2021 THEN
    CALL ahl_playoff_matchups_divisional_exp21_setup();
  END IF;

  # 2020/21 Pacific Division handling
  IF v_season = 2020 THEN
    SET v_conf_id := 59; # Conference we have attached the Pacific Division
    # Per-round processing
    IF v_round_num = 1 THEN
      # Play-in Round 1, and middle Semi-Final
      CALL ahl_playoff_matchups_2020_playin_rnd1(v_season, v_series_date, v_conf_id);
    ELSEIF v_round_num = 2 THEN
      # Play-in Round 2, and middle Semi-Final
      CALL ahl_playoff_matchups_2020_playin_rnd2(v_season, v_series_date, v_conf_id);
    ELSEIF v_round_num = 3 THEN
      # Semi-Finals
      CALL ahl_playoff_matchups_2020_semi(v_season, v_series_date, v_conf_id);
    ELSE
      # Final
      CALL ahl_playoff_matchups_2020_final(v_season, v_series_date, v_conf_id);
    END IF;

    # Final processing
    ALTER TABLE SPORTS_AHL_PLAYOFF_SERIES ORDER BY season, the_date, round_code;
    LEAVE proc;
  END IF;

  # Conference-based matchups follow one of two different bracket types, depending on the season
  IF v_round_num < 4 THEN

    # Loop through the conferences
    SET v_done := 0; # Reset the continue handler tracker
    OPEN cur_conf;
    loop_conf: LOOP

      FETCH cur_conf INTO v_conf_id;
      IF v_done = 1 THEN LEAVE loop_conf; END IF;

      # First two rounds are where the brackets occurred
      IF v_round_num < 3 THEN
        # Bracket 1: Divisional until end of 2010/11 season
        IF v_season <= 2010 THEN
          # Check for XO
          CALL ahl_playoff_matchups_divisional_xo();

          # Divisional Semi Final
          IF v_round_num = 1 THEN
            CALL ahl_playoff_matchups_divisional_semi(v_season, v_series_date, v_conf_id, v_counter);

          # Divisional Final
          ELSEIF v_round_num = 2 THEN
            CALL ahl_playoff_matchups_divisional_final(v_season, v_series_date, v_conf_id, v_counter);
          END IF;

        # Bracket 2: Conference from 2011/12 until 2014/15 season
        ELSEIF v_season <= 2014 THEN
          # Conference Quarter Final
          IF v_round_num = 1 THEN
            CALL ahl_playoff_matchups_conference_quarter(v_season, v_series_date, v_conf_id, v_counter);

          # Conference Semi Final
          ELSEIF v_round_num = 2 THEN
            CALL ahl_playoff_matchups_conference_semi(v_season, v_series_date, v_conf_id, v_counter);
          END IF;

        # Bracket 3: 16 team Divisional (by pts_pct) until 2020/21 season
        ELSEIF v_season <= 2020 THEN
          # Check for divisional XO (which was abolished in the 2016/17 season)
          IF v_season = 2015 THEN
            CALL ahl_playoff_matchups_divisional_xo();
          END IF;

          # Divisional Semi Final
          IF v_round_num = 1 THEN
            CALL ahl_playoff_matchups_divisional_semi(v_season, v_series_date, v_conf_id, v_counter);

          # Divisional Final
          ELSEIF v_round_num = 2 THEN
            CALL ahl_playoff_matchups_divisional_final(v_season, v_series_date, v_conf_id, v_counter);
          END IF;

        # Bracket 4: Divisional (by pts_pct), bar bottom two teams in each division, from 2021/22 season
        ELSE
          # First Round matchups
          IF v_round_num = 0 THEN
            CALL ahl_playoff_matchups_divisional_exp21_first(v_season, v_series_date, v_conf_id, v_counter);

          # Divisional Semi Final
          ELSEIF v_round_num = 1 THEN
            CALL ahl_playoff_matchups_divisional_exp21_semi(v_season, v_series_date, v_conf_id, v_counter);

          # Divisional Final
          ELSEIF v_round_num = 2 THEN
            CALL ahl_playoff_matchups_divisional_exp21_final(v_season, v_series_date, v_conf_id, v_counter);
          END IF;

        END IF;

      # Conference Final is common
      ELSEIF v_round_num = 3 THEN
        CALL ahl_playoff_matchups_conference_final(v_season, v_series_date, v_conf_id, v_counter);
      END IF;

    END LOOP loop_conf;
    CLOSE cur_conf;

  # Calder Cup Final is a fixed method
  ELSE
    CALL ahl_playoff_matchups_cup_final(v_season, v_series_date, v_counter);
  END IF;

  # Prune rows we just added for series that were started before processing this round
  DELETE SERIES.*
  FROM SPORTS_AHL_PLAYOFF_SERIES AS SERIES
  JOIN tmp_series_rounds_stage0 AS PREUPDATE
    ON (PREUPDATE.round_code = SERIES.round_code
    AND PREUPDATE.started = 1)
  WHERE SERIES.season = v_season
  AND   SERIES.the_date = v_series_date;

  # Final ordering
  ALTER TABLE SPORTS_AHL_PLAYOFF_SERIES ORDER BY season, the_date, round_code;

END $$

DELIMITER ;

##
## Setup
##
DROP PROCEDURE IF EXISTS `ahl_playoff_matchups_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_playoff_matchups_setup`(
  v_season SMALLINT UNSIGNED,
  v_round_num TINYINT UNSIGNED,
  v_series_date DATE
)
    COMMENT 'Prepare for our AHL playoff matchup calcs'
BEGIN

  DECLARE v_round_first TINYINT UNSIGNED DEFAULT 1;
  DECLARE v_round_started TINYINT UNSIGNED;
  DECLARE v_standing_date DATE;

  IF v_season >= 2021 THEN
    SET v_round_first = 0;
  END IF;

  # There are scenarios where we could be processing mid-round (such as First Round > Divisional Semi-Finals), so determine if this applies
  SELECT started INTO v_round_started
  FROM tmp_series_rounds
  WHERE round_num = v_round_num;

  # Clear a previous run (if the series has started, from the processing date)
  DELETE FROM SPORTS_AHL_PLAYOFF_SERIES
  WHERE season = v_season
  AND   SUBSTRING(LPAD(round_code, 2, '0'), 1, 1) >= v_round_num
  AND   (v_round_started = 0 OR the_date >= v_series_date);

  #
  # Div order standardisation
  #
  DROP TEMPORARY TABLE IF EXISTS tmp_div_list;
  CREATE TEMPORARY TABLE tmp_div_list (
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    div_order TINYINT UNSIGNED,
    div_order_raw TINYINT UNSIGNED,
    div_size TINYINT UNSIGNED,
    PRIMARY KEY (div_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_AHL_GROUPINGS.parent_id AS conf_id,
           SPORTS_AHL_GROUPINGS.grouping_id AS div_id,
           0 AS div_order,
           SPORTS_AHL_GROUPINGS.order AS div_order_raw,
           COUNT(SPORTS_AHL_TEAMS_GROUPINGS.team_id) AS div_size
    FROM SPORTS_AHL_TEAMS_GROUPINGS
    JOIN SPORTS_AHL_GROUPINGS
      ON (SPORTS_AHL_GROUPINGS.grouping_id = SPORTS_AHL_TEAMS_GROUPINGS.grouping_id)
    WHERE v_season BETWEEN SPORTS_AHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_AHL_TEAMS_GROUPINGS.season_to, 2099)
    GROUP BY SPORTS_AHL_TEAMS_GROUPINGS.grouping_id;

  # Convert our raw (spaced out) orders to a sequential list
  CALL _duplicate_tmp_table('tmp_div_list', 'tmp_div_list_cpA');
  CALL _duplicate_tmp_table('tmp_div_list', 'tmp_div_list_cpB');
  INSERT INTO tmp_div_list (conf_id, div_id, div_order)
    SELECT tmp_div_list_cpA.conf_id, tmp_div_list_cpA.div_id, COUNT(tmp_div_list_cpB.div_id) + 1
    FROM tmp_div_list_cpA
    LEFT JOIN tmp_div_list_cpB
      ON (tmp_div_list_cpB.conf_id = tmp_div_list_cpA.conf_id
      AND tmp_div_list_cpB.div_order_raw < tmp_div_list_cpA.div_order_raw)
    GROUP BY tmp_div_list_cpA.div_id
  ON DUPLICATE KEY UPDATE div_order = VALUES(div_order);

  # In 2020/21, only the Pacific Division held playoffs
  IF v_season = 2020 THEN
    DELETE FROM tmp_div_list WHERE div_id <> 64;
  END IF;

  #
  # Build our temp table of seedings
  #
  SELECT DATE(MAX(the_date)) INTO v_standing_date FROM SPORTS_AHL_STANDINGS WHERE season = v_season;
  DROP TEMPORARY TABLE IF EXISTS tmp_seeds;
  CREATE TEMPORARY TABLE tmp_seeds (
    team_id VARCHAR(3),
    pts TINYINT UNSIGNED,
    pts_pct DECIMAL(4,3) UNSIGNED,
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    seed TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_div TINYINT UNSIGNED,
    status_code VARCHAR(1),
    reseed TINYINT UNSIGNED,
    div_xo ENUM('larger', 'smaller', 'na'),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY;

  IF v_season <> 2020 THEN
    INSERT INTO tmp_seeds (team_id, pts, pts_pct, conf_id, div_id, seed, pos_league, pos_conf, pos_div, status_code, reseed, div_xo)
      SELECT STANDINGS.team_id, STANDINGS.pts, STANDINGS.pts_pct,
             DIVN.parent_id AS conf_id, DIVN.grouping_id AS div_id, SEEDS.seed,
             STANDINGS.pos_league, STANDINGS.pos_conf, STANDINGS.pos_div,
             STANDINGS.status_code,
             0 AS reseed, 'na' AS div_xo
      FROM SPORTS_AHL_TEAMS_GROUPINGS AS TEAM_DIVN
      JOIN SPORTS_AHL_GROUPINGS AS DIVN
        ON (DIVN.grouping_id = TEAM_DIVN.grouping_id)
      JOIN SPORTS_AHL_STANDINGS AS STANDINGS
        ON (STANDINGS.season = v_season
        AND STANDINGS.the_date = v_standing_date
        AND STANDINGS.team_id = TEAM_DIVN.team_id)
      LEFT JOIN SPORTS_AHL_PLAYOFF_SEEDS AS SEEDS
        ON (SEEDS.season = v_season
        AND SEEDS.conf_id = DIVN.parent_id
        AND SEEDS.team_id = TEAM_DIVN.team_id)
      WHERE v_season BETWEEN TEAM_DIVN.season_from AND IFNULL(TEAM_DIVN.season_to, 2099);

  # In 2020/21, only the Pacific Division held playoffs
  ELSE
    INSERT INTO tmp_seeds (team_id, pts, pts_pct, conf_id, div_id, seed, pos_league, pos_conf, pos_div, status_code, reseed, div_xo)
      SELECT STANDINGS.team_id, STANDINGS.pts, STANDINGS.pts_pct,
             DIVN.parent_id AS conf_id, DIVN.grouping_id AS div_id, SEEDS.seed,
             STANDINGS.pos_league, STANDINGS.pos_conf, STANDINGS.pos_div,
             IF(STANDINGS.pos_div = 1, 'y', 'x') AS status_code,
             0 AS reseed, 'na' AS div_xo
      FROM SPORTS_AHL_TEAMS_GROUPINGS AS TEAM_DIVN
      JOIN SPORTS_AHL_GROUPINGS AS DIVN
        ON (DIVN.grouping_id = TEAM_DIVN.grouping_id)
      JOIN SPORTS_AHL_STANDINGS AS STANDINGS
        ON (STANDINGS.season = v_season
        AND STANDINGS.the_date = v_standing_date
        AND STANDINGS.team_id = TEAM_DIVN.team_id)
      LEFT JOIN SPORTS_AHL_PLAYOFF_SEEDS AS SEEDS
        ON (SEEDS.season = v_season
        AND SEEDS.conf_id = DIVN.parent_id
        AND SEEDS.team_id = TEAM_DIVN.team_id)
      WHERE TEAM_DIVN.grouping_id = 64;
  END IF;

  # Re-seed pos_conf to ensure 1..8 (given division format, it could be 1..6,8-9 if team in 7th is better than 8/9 but in "wrong" division)
  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpA');
  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpB');
  INSERT INTO tmp_seeds (team_id, pos_conf)
    SELECT tmp_seeds_cpA.team_id, COUNT(tmp_seeds_cpB.team_id) + 1 AS pos_conf
    FROM tmp_seeds_cpA
    LEFT JOIN tmp_seeds_cpB
      ON (tmp_seeds_cpB.conf_id = tmp_seeds_cpA.conf_id
      AND tmp_seeds_cpB.pos_conf < tmp_seeds_cpA.pos_conf)
    GROUP BY tmp_seeds_cpA.team_id
  ON DUPLICATE KEY UPDATE pos_conf = VALUES(pos_conf);

  # Adjust to only include teams to have progressed
  IF IFNULL(v_round_num, v_round_first) > v_round_first THEN
    # Delete those who did not make the first round
    DELETE FROM tmp_seeds WHERE status_code IS NULL;

    # Who hasn't progressed this far?
    DROP TEMPORARY TABLE IF EXISTS tmp_playoff_losers;
    CREATE TEMPORARY TABLE tmp_playoff_losers (
      conf_id TINYINT UNSIGNED,
      pos_conf TINYINT UNSIGNED,
      PRIMARY KEY (conf_id, pos_conf)
    ) ENGINE = MEMORY
      SELECT IF(higher_games < lower_games, higher_conf_id, lower_conf_id) AS conf_id,
             IF(higher_games < lower_games, higher_seed, lower_seed) AS pos_conf
      FROM SPORTS_AHL_PLAYOFF_SERIES
      WHERE season = v_season
      AND   SUBSTRING(LPAD(round_code, 2, '0'), 1, 1) < v_round_num
      AND   complete = 1
      GROUP BY conf_id, pos_conf;

    # Remove from tmp_seeds
    DELETE tmp_seeds.*
    FROM tmp_playoff_losers
    JOIN tmp_seeds
      ON (tmp_seeds.conf_id = tmp_playoff_losers.conf_id
      AND tmp_seeds.pos_conf = tmp_playoff_losers.pos_conf);
  END IF;

END $$

DELIMITER ;
