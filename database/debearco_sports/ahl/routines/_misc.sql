##
## Misc AHL calcs
##

#
# Determine the league_id for the AHL
#
DROP FUNCTION IF EXISTS `fn_ahl_league_id`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `fn_ahl_league_id`() RETURNS TINYINT UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine the AHL league_id'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_league_id TINYINT UNSIGNED;

  SELECT MAX(season) INTO v_season FROM SPORTS_AHL_SCHEDULE;
  SELECT CONF.parent_id INTO v_league_id
  FROM SPORTS_AHL_TEAMS_GROUPINGS AS TEAM
  JOIN SPORTS_AHL_GROUPINGS AS DIVISION
    ON (DIVISION.grouping_id = TEAM.grouping_id)
  JOIN SPORTS_AHL_GROUPINGS AS CONF
    ON (CONF.grouping_id = DIVISION.parent_id)
  WHERE v_season BETWEEN TEAM.season_from AND IFNULL(TEAM.season_to, 2099)
  LIMIT 1;

  RETURN v_league_id;

END $$

DELIMITER ;

#
# Determine a season from a date
#
DROP FUNCTION IF EXISTS `fn_ahl_season_from_date`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `fn_ahl_season_from_date`(
  v_date DATE
) RETURNS YEAR
    DETERMINISTIC
    COMMENT 'Determine an AHL season from a date'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;

  SET v_season := YEAR(v_date);
  # Adjust for end of season being in a different calendar year
  IF MONTH(v_date) < 7 THEN
    SET v_season := v_season - 1;
  END IF;

  RETURN v_season;

END $$

DELIMITER ;

#
# Identify series date range
#
DROP PROCEDURE IF EXISTS `ahl_sequential_date_range`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_sequential_date_range` (
  v_season SMALLINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Create temp table of date ranges for AHL calcs'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_date_range;
  CREATE TEMPORARY TABLE tmp_date_range (
    season SMALLINT UNSIGNED,
    the_date DATE,
    PRIMARY KEY (season, the_date)
  ) ENGINE = MEMORY;

  WHILE v_start_date <= v_end_date DO
    INSERT INTO tmp_date_range (season, the_date) VALUES (v_season, v_start_date);
    SELECT DATE_ADD(v_start_date, INTERVAL 1 DAY) INTO v_start_date;
  END WHILE;

END $$

DELIMITER ;

#
# Identify numeric (tinyint) ranges
#
DROP PROCEDURE IF EXISTS `ahl_sequential_tinyint_range`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_sequential_tinyint_range` (
  v_start TINYINT UNSIGNED,
  v_end TINYINT UNSIGNED
)
    COMMENT 'Create temp table of TINYINT UNSIGNED ranges for AHL calcs'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_num_range;
  CREATE TEMPORARY TABLE tmp_num_range (
    num TINYINT UNSIGNED,
    PRIMARY KEY (num)
  ) ENGINE = MEMORY;

  WHILE v_start <= v_end DO
    INSERT INTO tmp_num_range (num) VALUES (v_start);
    SET v_start = v_start + 1;
  END WHILE;

END $$

DELIMITER ;
