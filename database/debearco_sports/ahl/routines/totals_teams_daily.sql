##
## Team season totals
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams`()
    COMMENT 'Calculate daily game totals for AHL teams'
BEGIN

  CALL `ahl_totals_teams_daily_skaters`();
  CALL `ahl_totals_teams_daily_goalies`();

END $$

DELIMITER ;

##
## Team season totals table maintenance
##
DROP PROCEDURE IF EXISTS `ahl_totals_teams_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_teams_order`()
    COMMENT 'Order the AHL team total tables'
BEGIN

  ALTER TABLE SPORTS_AHL_TEAMS_GAME_SKATERS ORDER BY season, team_id, game_type, game_id, stat_dir;
  ALTER TABLE SPORTS_AHL_TEAMS_GAME_GOALIES ORDER BY season, team_id, game_type, game_id, stat_dir;

END $$

DELIMITER ;

