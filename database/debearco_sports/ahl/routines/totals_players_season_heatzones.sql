##
## Heatzones by player
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_heatzones`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_heatzones`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Calculate heatzones for AHL players'
BEGIN

  # Prep
  CALL `ahl_totals_players_season_heatzones_setup`(v_season, v_season_type);

  # Run
  CALL `ahl_totals_players_season_heatzones_skaters`(v_season, v_season_type);
  CALL `ahl_totals_players_season_heatzones_goalies`(v_season, v_season_type);

  # Tidy
  CALL `ahl_totals_players_season_heatzones_tidy`();

END $$

DELIMITER ;

##
## Run for skaters
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_heatzones_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_heatzones_skaters`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Calculate heatzones for AHL skaters'
BEGIN

  # Gather totals per-player
  DROP TEMPORARY TABLE IF EXISTS `tmp_totals_skaters`;
  CREATE TEMPORARY TABLE `tmp_totals_skaters` (
    `skater` SMALLINT UNSIGNED NOT NULL,
    `heatzone` TINYINT UNSIGNED NOT NULL,
    `num_shots` SMALLINT UNSIGNED,
    `char_shots` CHAR(1),
    `num_goals` SMALLINT UNSIGNED,
    `char_goals` CHAR(1),
    PRIMARY KEY (`skater`, `heatzone`)
  ) SELECT `skater`, `heatzone`,
           SUM(`type` IN ('shot', 'goal')) AS `num_shots`, '' AS `char_shots`,
           SUM(`type` = 'goal') AS `num_goals`, '' AS `char_goals`
    FROM `tmp_raw_events`
    WHERE `skater` IS NOT NULL
    GROUP BY `skater`, `heatzone`;

  # Some per-player calcs
  DROP TEMPORARY TABLE IF EXISTS `tmp_skaters`;
  CREATE TEMPORARY TABLE `tmp_skaters` (
    `skater` SMALLINT UNSIGNED NOT NULL,
    `tot_shots` SMALLINT UNSIGNED,
    `tot_goals` SMALLINT UNSIGNED,
    PRIMARY KEY (`skater`)
  ) ENGINE = MEMORY
    SELECT `skater`,
           SUM(`num_shots`) AS `tot_shots`,
           SUM(`num_goals`) AS `tot_goals`
    FROM `tmp_totals_skaters`
    GROUP BY `skater`;

  # Determine the value as a percentage and convert to a HEX char
  UPDATE `tmp_totals_skaters`
  JOIN `tmp_skaters` USING (`skater`)
  SET `char_shots` = IF(`tot_shots`, CHAR(128 + ROUND((`num_shots` / `tot_shots`) * 99)), ' '),
      `char_goals` = IF(`tot_goals`, CHAR(128 + ROUND((`num_goals` / `tot_goals`) * 99)), ' ');

  # Main insert
  INSERT INTO `SPORTS_AHL_PLAYERS_SEASON_SKATERS_HEATMAP` (`season`, `season_type`, `player_id`, `shots`, `goals`)
    SELECT v_season AS `season`, v_season_type AS `season_type`, `tmp_skaters`.`skater` AS `player_id`,
           COMPRESS(GROUP_CONCAT(IFNULL(`tmp_totals_skaters`.`char_shots`, ' ') ORDER BY `tmp_num_range`.`num` SEPARATOR '')) AS `shots`,
           COMPRESS(GROUP_CONCAT(IFNULL(`tmp_totals_skaters`.`char_goals`, ' ') ORDER BY `tmp_num_range`.`num` SEPARATOR '')) AS `goals`
    FROM `tmp_skaters`
    JOIN `tmp_num_range`
      ON (1 = 1)
    LEFT JOIN `tmp_totals_skaters`
      ON (`tmp_totals_skaters`.`skater` = `tmp_skaters`.`skater`
      AND `tmp_totals_skaters`.`heatzone` = `tmp_num_range`.`num`)
    GROUP BY `tmp_skaters`.`skater`
  ON DUPLICATE KEY UPDATE `shots` = VALUES(`shots`),
                          `goals` = VALUES(`goals`);

END $$

DELIMITER ;

##
## Run for goalies
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_heatzones_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_heatzones_goalies`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Calculate heatzones for AHL goalies'
BEGIN

  # Gather totals per-player
  DROP TEMPORARY TABLE IF EXISTS `tmp_totals_goalies`;
  CREATE TEMPORARY TABLE `tmp_totals_goalies` (
    `goalie` SMALLINT UNSIGNED NOT NULL,
    `heatzone` TINYINT UNSIGNED NOT NULL,
    -- `num_shots` SMALLINT UNSIGNED,
    -- `char_shots` CHAR(1),
    `num_saves` SMALLINT UNSIGNED,
    `char_saves` CHAR(1),
    -- `num_goals` SMALLINT UNSIGNED,
    -- `char_goals` CHAR(1),
    PRIMARY KEY (`goalie`, `heatzone`)
  ) SELECT `goalie`, `heatzone`,
           -- SUM(`type` IN ('shot', 'goal')) AS `num_shots`, '' AS `char_shots`,
           SUM(`type` = 'shot') AS `num_saves`, '' AS `char_saves` -- ,
           -- SUM(`type` = 'goal') AS `num_goals`, '' AS `char_goals`
    FROM `tmp_raw_events`
    WHERE `goalie` IS NOT NULL
    GROUP BY `goalie`, `heatzone`;

  # Some per-player calcs
  DROP TEMPORARY TABLE IF EXISTS `tmp_goalies`;
  CREATE TEMPORARY TABLE `tmp_goalies` (
    `goalie` SMALLINT UNSIGNED NOT NULL,
    -- `tot_shots` SMALLINT UNSIGNED,
    `tot_saves` SMALLINT UNSIGNED,
    -- `tot_goals` SMALLINT UNSIGNED,
    PRIMARY KEY (`goalie`)
  ) ENGINE = MEMORY
    SELECT `goalie`,
           -- SUM(`num_shots`) AS `tot_shots`,
           SUM(`num_saves`) AS `tot_saves` -- ,
           -- SUM(`num_goals`) AS `tot_goals`
    FROM `tmp_totals_goalies`
    GROUP BY `goalie`;

  # Determine the value as a percentage and convert to a HEX char
  UPDATE `tmp_totals_goalies`
  JOIN `tmp_goalies` USING (`goalie`)
  SET -- `char_shots` = IF(`tot_shots`, CHAR(128 + ROUND((`num_shots` / `tot_shots`) * 99)), ' '),
      `char_saves` = IF(`tot_saves`, CHAR(128 + ROUND((`num_saves` / `tot_saves`) * 99)), ' '); -- ,
      -- `char_goals` = IF(`tot_goals`, CHAR(128 + ROUND((`num_goals` / `tot_goals`) * 99)), ' ');

  # Main insert
  #INSERT INTO `SPORTS_AHL_PLAYERS_SEASON_GOALIES_HEATMAP` (`season`, `season_type`, `player_id`, `shots`, `saves`, `goals`)
  INSERT INTO `SPORTS_AHL_PLAYERS_SEASON_GOALIES_HEATMAP` (`season`, `season_type`, `player_id`, `saves`)
    SELECT v_season AS `season`, v_season_type AS `season_type`, `tmp_goalies`.`goalie` AS `player_id`,
           -- COMPRESS(GROUP_CONCAT(IFNULL(`tmp_totals_goalies`.`char_shots`, ' ') ORDER BY `tmp_num_range`.`num` SEPARATOR '')) AS `shots`,
           COMPRESS(GROUP_CONCAT(IFNULL(`tmp_totals_goalies`.`char_saves`, ' ') ORDER BY `tmp_num_range`.`num` SEPARATOR '')) AS `saves` -- ,
           -- COMPRESS(GROUP_CONCAT(IFNULL(`tmp_totals_goalies`.`char_goals`, ' ') ORDER BY `tmp_num_range`.`num` SEPARATOR '')) AS `goals`
    FROM `tmp_goalies`
    JOIN `tmp_num_range`
      ON (1 = 1)
    LEFT JOIN `tmp_totals_goalies`
      ON (`tmp_totals_goalies`.`goalie` = `tmp_goalies`.`goalie`
      AND `tmp_totals_goalies`.`heatzone` = `tmp_num_range`.`num`)
    GROUP BY `tmp_goalies`.`goalie`
  ON DUPLICATE KEY UPDATE -- `shots` = VALUES(`shots`),
                          `saves` = VALUES(`saves`); -- ,
                          -- `goals` = VALUES(`goals`);

END $$

DELIMITER ;

##
## Base setup
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_heatzones_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_heatzones_setup`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Setup for calculating heatzones for AHL players'
BEGIN

  CALL `ahl_sequential_tinyint_range`(1, 56);

  DROP TEMPORARY TABLE IF EXISTS `tmp_raw_events`;
  CREATE TEMPORARY TABLE `tmp_raw_events` (
    `game_id` SMALLINT UNSIGNED NOT NULL,
    `event_id` SMALLINT UNSIGNED NOT NULL,
    `type` ENUM('shot', 'goal', 'hit', 'penalty'),
    `heatzone` TINYINT UNSIGNED,
    `skater` SMALLINT UNSIGNED,
    `goalie` SMALLINT UNSIGNED,
    PRIMARY KEY (`game_id`, `event_id`),
    KEY `by_skater` (`skater`),
    KEY `by_goalie` (`goalie`)
  );

  #
  # Shots
  #
  INSERT INTO `tmp_raw_events`
    SELECT `STAT`.`game_id`, `STAT`.`event_id`, 'shot' AS `type`, `STAT`.`heatzone`,
           `SKATER`.`player_id` AS `skater`, `GOALIE`.`player_id` AS `goalie`
    FROM `SPORTS_AHL_GAME_EVENT_SHOT` AS `STAT`
    JOIN `SPORTS_AHL_GAME_EVENT` AS `EVENT`
      ON (`EVENT`.`season` = `STAT`.`season`
      AND `EVENT`.`game_type` = `STAT`.`game_type`
      AND `EVENT`.`game_id` = `STAT`.`game_id`
      AND `EVENT`.`event_id` = `STAT`.`event_id`)
    JOIN `SPORTS_AHL_GAME_LINEUP` AS `SKATER`
      ON (`SKATER`.`season` = `STAT`.`season`
      AND `SKATER`.`game_type` = `STAT`.`game_type`
      AND `SKATER`.`game_id` = `STAT`.`game_id`
      AND `SKATER`.`team_id` = `STAT`.`by_team_id`
      AND `SKATER`.`jersey` = `STAT`.`by_jersey`)
    LEFT JOIN `SPORTS_AHL_GAME_LINEUP` AS `GOALIE`
      ON (`GOALIE`.`season` = `STAT`.`season`
      AND `GOALIE`.`game_type` = `STAT`.`game_type`
      AND `GOALIE`.`game_id` = `STAT`.`game_id`
      AND `GOALIE`.`team_id` = `STAT`.`on_team_id`
      AND `GOALIE`.`jersey` = `STAT`.`on_jersey`)
    WHERE `STAT`.`season` = v_season
    AND   `STAT`.`game_type` = v_season_type
    AND   `STAT`.`coord_x` IS NOT NULL
    AND   `STAT`.`coord_y` IS NOT NULL;

  #
  # Goals
  #
  INSERT INTO `tmp_raw_events`
    SELECT `STAT`.`game_id`, `STAT`.`event_id`, 'goal' AS `type`, `STAT`.`heatzone`,
           `SKATER`.`player_id` AS `skater`, NULL AS `goalie` -- , `GOALIE`.`player_id` AS `goalie`
    FROM `SPORTS_AHL_GAME_EVENT_GOAL` AS `STAT`
    JOIN `SPORTS_AHL_GAME_EVENT` AS `EVENT`
      ON (`EVENT`.`season` = `STAT`.`season`
      AND `EVENT`.`game_type` = `STAT`.`game_type`
      AND `EVENT`.`game_id` = `STAT`.`game_id`
      AND `EVENT`.`event_id` = `STAT`.`event_id`)
    JOIN `SPORTS_AHL_GAME_LINEUP` AS `SKATER`
      ON (`SKATER`.`season` = `STAT`.`season`
      AND `SKATER`.`game_type` = `STAT`.`game_type`
      AND `SKATER`.`game_id` = `STAT`.`game_id`
      AND `SKATER`.`team_id` = `STAT`.`by_team_id`
      AND `SKATER`.`jersey` = `STAT`.`scorer`)
    -- LEFT JOIN `SPORTS_AHL_GAME_LINEUP` AS `GOALIE`
    --   ON (`GOALIE`.`season` = `STAT`.`season`
    --   AND `GOALIE`.`game_type` = `STAT`.`game_type`
    --   AND `GOALIE`.`game_id` = `STAT`.`game_id`
    --   AND `GOALIE`.`team_id` = `STAT`.`on_team_id`
    --   AND `GOALIE`.`jersey` = `STAT`.`on_jersey`)
    WHERE `STAT`.`season` = v_season
    AND   `STAT`.`game_type` = v_season_type
    AND   `STAT`.`coord_x` IS NOT NULL
    AND   `STAT`.`coord_y` IS NOT NULL;

END $$

DELIMITER ;

##
## Table Maintenance
##
DROP PROCEDURE IF EXISTS `ahl_totals_players_season_heatzones_tidy`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `ahl_totals_players_season_heatzones_tidy`()
    COMMENT 'Tidy the heatzone for AHL player tables'
BEGIN

  ALTER TABLE `SPORTS_AHL_PLAYERS_SEASON_GOALIES_HEATMAP` ORDER BY `season`, `season_type`, `player_id`;
  ALTER TABLE `SPORTS_AHL_PLAYERS_SEASON_SKATERS_HEATMAP` ORDER BY `season`, `season_type`, `player_id`;

END $$

DELIMITER ;
