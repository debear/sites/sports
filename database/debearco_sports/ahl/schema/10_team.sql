CREATE TABLE `SPORTS_AHL_TEAMS` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `city` VARCHAR(40) COLLATE latin1_general_ci NOT NULL,
  `city_abbrev` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `franchise` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `founded` YEAR DEFAULT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TEAMS_NAMING` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `alt_team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `alt_city` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  `alt_franchise` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TEAMS_GROUPINGS` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `grouping_id` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TEAMS_ARENAS` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `building_id` TINYINT(3) UNSIGNED NOT NULL,
  `arena` VARCHAR(55) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`team_id`,`season_from`,`building_id`),
  KEY `team_season` (`team_id`,`season_from`,`season_to`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TEAMS_HISTORY_TITLES` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `league_id` TINYINT(3) UNSIGNED NOT NULL,
  `div_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `conf_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `league_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `playoff_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`team_id`,`league_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TEAMS_HISTORY_REGULAR` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `wins` TINYINT(1) UNSIGNED DEFAULT 0,
  `loss` TINYINT(1) UNSIGNED DEFAULT 0,
  `ties` TINYINT(1) UNSIGNED DEFAULT NULL,
  `ot_loss` TINYINT(1) UNSIGNED DEFAULT NULL,
  `so_loss` TINYINT(1) UNSIGNED DEFAULT NULL,
  `pts` TINYINT(1) UNSIGNED DEFAULT 0,
  `pos` TINYINT(1) UNSIGNED DEFAULT NULL,
  `league_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TEAMS_HISTORY_PLAYOFF_SUMMARY` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `wildcard` TINYINT(1) UNSIGNED DEFAULT 0,
  `div_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  `conf_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  `playoff_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`team_id`,`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TEAMS_HISTORY_PLAYOFF` (
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `round` TINYINT(3) UNSIGNED NOT NULL,
  `round_code` enum('PRE','1R','CQF','DSF','QF','CSF','DF','SF','CF','F','TCF','CCF') COLLATE latin1_general_ci NOT NULL,
  `opp_team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `opp_team_alt` VARCHAR(90) COLLATE latin1_general_ci DEFAULT NULL,
  `for` TINYINT(3) UNSIGNED DEFAULT 0,
  `against` TINYINT(3) UNSIGNED DEFAULT 0,
  `info` ENUM('goals','double-elimination') COLLATE latin1_general_cs DEFAULT NULL,
  PRIMARY KEY (`season`,`team_id`,`round`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
