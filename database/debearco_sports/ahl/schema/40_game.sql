CREATE TABLE `SPORTS_AHL_GAME_LINEUP` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pos` ENUM('C','D','F','G','LW','RW') COLLATE latin1_general_ci DEFAULT NULL,
  `capt_status` ENUM('C','A') COLLATE latin1_general_ci DEFAULT NULL,
  `started` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_OFFICIALS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `official_type` ENUM('referee','linesman') COLLATE latin1_general_ci NOT NULL,
  `official_id` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`official_type`,`official_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_PERIODS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `period` TINYINT(3) UNSIGNED NOT NULL,
  `goals` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `shots` TINYINT(3) UNSIGNED DEFAULT NULL,
  `pims` SMALLINT(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`period`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_PP_STATS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `pp_type` ENUM('pp','pk') COLLATE latin1_general_ci NOT NULL,
  `pp_goals` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `pp_opps` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`pp_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_THREE_STARS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `star` TINYINT(1) UNSIGNED NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`star`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
