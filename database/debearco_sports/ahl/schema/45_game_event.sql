# Full list of plays - not uploaded
CREATE TABLE `SPORTS_AHL_GAME_EVENT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period` TINYINT(3) UNSIGNED NOT NULL,
  `event_time` TIME NOT NULL,
  `event_type` ENUM('?','GOAL','PENALTY','SHOT','SHOOTOUT') COLLATE latin1_general_ci NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `play` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

# Boxscore plays only - uploaded
CREATE TABLE `SPORTS_AHL_GAME_EVENT_BOXSCORE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period` TINYINT(3) UNSIGNED NOT NULL,
  `event_time` TIME NOT NULL,
  `event_type` ENUM('GOAL','PENALTY','SHOOTOUT') COLLATE latin1_general_ci NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `play` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_EVENT_GOAL` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `goal_num` TINYINT(3) UNSIGNED DEFAULT NULL,
  `scorer` TINYINT(3) UNSIGNED NOT NULL,
  `assist_1` TINYINT(3) UNSIGNED DEFAULT NULL,
  `assist_2` TINYINT(3) UNSIGNED DEFAULT NULL,
  `goal_type` ENUM('EV','PP','SH') COLLATE latin1_general_ci NOT NULL DEFAULT 'EV',
  `goalie_status` ENUM('EN','WG') COLLATE latin1_general_ci DEFAULT NULL,
  `penalty_shot` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `coord_x` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `coord_y` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `heatzone` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_scoring` (`season`,`game_type`,`game_id`,`by_team_id`,`scorer`,`assist_1`,`assist_2`),
  KEY `by_goal_type` (`season`,`game_type`,`game_id`,`goal_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_EVENT_PENALTY` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `served_by` TINYINT(3) UNSIGNED DEFAULT NULL,
  `type` ENUM('Abuse of officials','Aggressor','Bench minor','Boarding','Butt-ending','Charging','Check to the head','Checking from behind','Clipping','Cross-checking','Delay of game','Diving','Elbowing','Fighting','Game misconduct','Goaltender interference','Gross misconduct','Handling the puck','Head-butting','High-sticking','Holding','Holding the stick','Hooking','Illegal equipment','Illegal lineup','Illegal stick','Instigator','Interference','Kicking','Kneeing','Leaving the crease','Match penalty','Misconduct','Roughing','Slashing','Slew-footing','Spearing','Third man in','Throwing equipment','Too many men','Tripping','Unsportsmanlike conduct') COLLATE latin1_general_ci DEFAULT NULL,
  `pims` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_offender` (`season`,`game_type`,`game_id`,`team_id`,`jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_EVENT_SHOT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `by_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `on_team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `on_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `penalty_shot` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `coord_x` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `coord_y` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `heatzone` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_shooter` (`season`,`game_type`,`game_id`,`by_team_id`,`by_jersey`),
  KEY `by_goalie` (`season`,`game_type`,`game_id`,`on_team_id`,`on_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_EVENT_SHOOTOUT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `by_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `on_team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `on_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `status` ENUM('goal','no_goal') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_shooter` (`season`,`game_type`,`game_id`,`by_team_id`,`by_jersey`),
  KEY `by_goalie` (`season`,`game_type`,`game_id`,`on_team_id`,`on_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GAME_EVENT_ONICE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `player_1` TINYINT(3) UNSIGNED NOT NULL,
  `player_2` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_3` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_4` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_5` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_6` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`,`team_id`),
  KEY `by_full` (`season`,`game_type`,`game_id`,`event_id`,`team_id`,`player_1`,`player_2`,`player_3`,`player_4`,`player_5`,`player_6`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
