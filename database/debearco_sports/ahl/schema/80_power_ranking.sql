CREATE TABLE `SPORTS_AHL_POWER_RANKINGS_WEEKS` (
  `season` YEAR NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `calc_date` DATE NOT NULL,
  `date_from` DATE NOT NULL,
  `date_to` DATE NOT NULL,
  `when_run` DATETIME DEFAULT NULL,
  PRIMARY KEY (`season`,`week`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_POWER_RANKINGS` (
  `season` YEAR NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `rank` TINYINT(3) UNSIGNED DEFAULT NULL,
  `score` DECIMAL(5,3) DEFAULT NULL,
  `point_pct` DECIMAL(5,3) DEFAULT NULL,
  `scoring` DECIMAL(5,3) DEFAULT NULL,
  `special_teams` DECIMAL(5,3) DEFAULT NULL,
  `str_sched` DECIMAL(5,3) DEFAULT NULL,
  `comment` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`week`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
