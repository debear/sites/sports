CREATE TABLE `SPORTS_AHL_SCHEDULE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `game_num` MEDIUMINT(8) UNSIGNED NOT NULL,
  `remote_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL,
  `home` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `visitor` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `game_date` DATE NOT NULL,
  `game_time` TIME NOT NULL,
  `home_score` TINYINT(1) UNSIGNED DEFAULT NULL,
  `visitor_score` TINYINT(1) UNSIGNED DEFAULT NULL,
  `status` ENUM('F','OT','2OT','3OT','4OT','5OT','SO','PPD','CNC') COLLATE latin1_general_ci DEFAULT NULL,
  `attendance` SMALLINT(4) UNSIGNED DEFAULT NULL,
  `home_goalie` TINYINT(3) UNSIGNED DEFAULT NULL,
  `visitor_goalie` TINYINT(3) UNSIGNED DEFAULT NULL,
  `alt_venue` VARCHAR(150) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`),
  KEY `by_status` (`season`,`game_type`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_SCHEDULE_DATES` (
  `season` YEAR NOT NULL,
  `date` DATE NOT NULL,
  `num_games` TINYINT(3) UNSIGNED NOT NULL,
  `date_prev` DATE DEFAULT NULL,
  `date_next` DATE DEFAULT NULL,
  PRIMARY KEY (`season`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_SCHEDULE_MATCHUPS` (
  `season` YEAR NOT NULL,
  `team_idA` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `team_idB` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`team_idA`,`team_idB`,`game_type`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
