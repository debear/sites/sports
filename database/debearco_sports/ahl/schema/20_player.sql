CREATE TABLE `SPORTS_AHL_PLAYERS` (
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `dob` DATE DEFAULT NULL,
  `birthplace` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `birthplace_country` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `height` TINYINT(3) UNSIGNED DEFAULT NULL,
  `weight` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `shoots_catches` ENUM('Left','Right') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_PLAYERS_IMPORT` (
  `player_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `remote_id` SMALLINT(5) UNSIGNED NOT NULL,
  `profile_imported` DATETIME DEFAULT NULL,
  PRIMARY KEY (`player_id`),
  UNIQUE KEY `remote_id` (`remote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_PLAYERS_AWARDS` (
  `season` YEAR NOT NULL,
  `award_id` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `player_name` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `team_name` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `pos` ENUM('C','LW','RW','D','G') COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `FAUX_PRIMARY` (`season`,`award_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
