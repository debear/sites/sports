CREATE TABLE `SPORTS_AHL_POSITIONS` (
  `pos_code` ENUM('C','D','F','G','LW','RW') COLLATE latin1_general_ci NOT NULL,
  `pos_long` VARCHAR(15) COLLATE latin1_general_ci NOT NULL,
  `posgroup_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `type` ENUM('skaters','goaltenders') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pos_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_POSITIONS_GROUPS` (
  `posgroup_id` TINYINT(3) UNSIGNED NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`posgroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_STATUS` (
  `type` ENUM('player') COLLATE latin1_general_ci NOT NULL,
  `code` ENUM('active','ir','na') COLLATE latin1_general_ci NOT NULL,
  `disp_code` CHAR(4) COLLATE latin1_general_ci DEFAULT NULL,
  `disp_name` VARCHAR(25) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`type`,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_OFFICIALS` (
  `season` YEAR NOT NULL,
  `official_id` TINYINT(3) UNSIGNED NOT NULL,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`season`,`official_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_GROUPINGS` (
  `grouping_id` TINYINT(1) UNSIGNED NOT NULL,
  `parent_id` TINYINT(1) UNSIGNED DEFAULT NULL,
  `type` ENUM('league','conf','div') COLLATE latin1_general_ci NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `name_full` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `icon` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`grouping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_AWARDS` (
  `award_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `description` VARCHAR(250) COLLATE latin1_general_ci DEFAULT NULL,
  `disp_order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`award_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
