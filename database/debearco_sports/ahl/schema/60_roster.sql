CREATE TABLE `SPORTS_AHL_TEAMS_ROSTERS` (
  `season` YEAR NOT NULL,
  `the_date` DATE NOT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `pos` ENUM('C','D','F','G','LW','RW') COLLATE latin1_general_ci DEFAULT NULL,
  `capt_status` ENUM('C','A') COLLATE latin1_general_ci DEFAULT NULL,
  `player_status` ENUM('active','na') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`the_date`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_AHL_TRANSACTIONS` (
  `season` YEAR NOT NULL,
  `the_date` DATE NOT NULL,
  `remote_id` SMALLINT(5) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `team_id` CHAR(3) COLLATE latin1_general_ci NOT NULL,
  `pos` ENUM('C','D','F','G','LW','RW') COLLATE latin1_general_ci DEFAULT NULL,
  `type` ENUM('add','del','both') COLLATE latin1_general_ci NOT NULL,
  `detail` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `processed` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`the_date`,`remote_id`,`team_id`),
  KEY `by_player` (`season`,`the_date`,`player_id`),
  KEY `by_team_daily` (`season`,`the_date`,`team_id`),
  KEY `by_team_season` (`season`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
