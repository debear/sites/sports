#!/bin/bash
# Reload database routines/views

# Config
here=$(realpath $(dirname $0))
db=$(basename $here)

# Load the display helpers
source $here/../../tests/_scripts/helpers.sh

# Local shorthands
function report_error() {
  exit_code="$1"
  if [ $exit_code -eq 0 ]; then
    echo -n '[ '
    display_green -n 'Done'
    echo ' ]'
  else
    echo -n '[ '
    display_red -n 'Error'
    echo ' ]'
  fi
}

# Parse CLI arguments
sect=
proc=
for arg; do
  # What is this argument?
  case "$arg" in
    # Procedure type
    routines|views)
      proc="${proc} $arg"
      ;;

    # Other, consider as code segregation / section
    *)
      if [ -e "$here/$arg" ]; then
        sect="${sect} $arg"
      else
        echo "* Unknown section / directory: '$arg'" >&2
      fi
      ;;
  esac
done

# Remove all existing?
if [ -z "$sect" ]; then
  display_section 'Pruning existing:'
  # Routines (Procedures / Functions)
  if [ -z "$proc" ] || [ $(echo "$proc" | grep -Fc ' routines') -gt 0 ]; then
    display_info -n "  - routines: "
    echo "SELECT CONCAT('DROP ', ROUTINE_TYPE, ' \`', ROUTINE_NAME, '\`;') AS the_sql FROM ROUTINES WHERE ROUTINE_SCHEMA = '$db';" | mysql -s information_schema | mysql -s $db
    report_error $?
  fi
  # Views
  if [ -z "$proc" ] || [ $(echo "$proc" | grep -Fc ' views') -gt 0 ]; then
    display_info -n '  - views: '
    echo "SELECT CONCAT('DROP VIEW \`', TABLE_NAME, '\`;') AS the_sql FROM VIEWS WHERE TABLE_SCHEMA = '$db';" | mysql -s information_schema | mysql -s $db
    report_error $?
  fi

  echo
fi

# Formulate arguments to use in our file search pattern
if [ -z "$sect" ]; then
  sect=$(find $here -mindepth 1 -maxdepth 1 -type d -printf '%f ')
fi
if [ -z "$proc" ]; then
  proc='routines views'
fi

# As we'll use temporary files, create our location
tmpdir="/tmp/debear/db-resync/$db-$$"
mkdir -p $tmpdir

# Run...
tot_okay=0; tot_error=0
for s in $sect; do
  display_section "$s:"
  for p in $proc; do
    display_info -n "  - $p: "
    files=$(find $here/$s/$p/*.sql -type f 2>/dev/null)
    if [ ! -z "$files" ]; then
      num_tot=0; num_okay=0; num_error=0
      for sql in $files; do
        tmpfile="$tmpdir/$s-$p-$(basename $(echo "$sql"))"
        mysql $db <$sql >$tmpfile.out 2>$tmpfile.err
        # Check for error / success
        if [ $? -eq 0 ]; then
          num_okay=$(( $num_okay + 1 ))
          tot_okay=$(( $tot_okay + 1 ))
        else
          num_error=$(( $num_error + 1 ))
          tot_error=$(( $tot_error + 1 ))
        fi
        num_tot=$(( $num_tot + 1 ))
      done
      # Report on success / failure
      if [ $num_error -eq 0 ]; then
        echo -n '[ '
        display_green -n 'Done'
        echo " ] ($num_okay of $num_tot imported successfully)"
      else
        echo -n '[ '
        display_red -n 'Error'
        echo " ] ($num_error of $num_tot failed)"
        for e in $(find $tmpdir -name "$s-$p-*.err" -type f -size +1c); do
          f=$(basename "$e" | sed -r "s/^$s-$p-(.+)\.err/\1/g")
          display_red -n "    - $f: "
          cat $e
        done
      fi
    else
      echo -n '[ '
      display_yellow -n 'Skipped'
      echo ' ]'
    fi
  done
done
rm -rf $tmpdir

# Exit appropriately
echo
if [ $tot_error -gt 0 ]; then
  display_error "$tot_error file(s) failed to import"
  exit 1
elif [ $tot_okay -gt 0 ]; then
  display_success 'All files imported successfully'
else
  display_warning 'No files found to be imported'
fi
