#
# Convert a numeric value into its ordinal string
#
DROP FUNCTION IF EXISTS `display_ordinal_number`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `display_ordinal_number`(
  v_num SMALLINT UNSIGNED
) RETURNS VARCHAR(8)
    DETERMINISTIC
    COMMENT 'Return a number''s ordinal suffix'
BEGIN

  DECLARE v_ordinal CHAR(2);

  IF v_num % 100 BETWEEN 11 AND 13 THEN
    SET v_ordinal = 'th';
  ELSEIF v_num % 10 = 1 THEN
    SET v_ordinal = 'st';
  ELSEIF v_num % 10 = 2 THEN
    SET v_ordinal = 'nd';
  ELSEIF v_num % 10 = 3 THEN
    SET v_ordinal = 'rd';
  ELSE
    SET v_ordinal = 'th';
  END IF;

  RETURN CONCAT(v_num, v_ordinal);

END $$

DELIMITER ;
