#
# Convert the HTML character (&...;) in to string equivalent
#
DROP FUNCTION IF EXISTS `fn_basic_html_comparison`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `fn_basic_html_comparison`( v_string TEXT ) RETURNS TEXT CHARSET latin1 COLLATE latin1_general_ci
    DETERMINISTIC
    COMMENT 'Manual HTML decode for string comparison'
BEGIN

    DECLARE my_while TINYINT(1) DEFAULT 0;
    DECLARE my_amp_pos TINYINT(1) DEFAULT 0;
    DECLARE my_comma_pos TINYINT(1) DEFAULT 0;
    DECLARE my_code VARCHAR(10);
    DECLARE my_char CHAR(1);

    -- If there are no ampersands, there is no point in continuing!
    IF v_string NOT REGEXP '&[A-Za-z]+;' THEN
      RETURN v_string;
    END IF;

    -- Okay, so we need to find all instances of these codes and convert them to the letter
    work_loop: WHILE my_while < 30 DO
      -- Look for the start of the sequence
      SET my_amp_pos = LOCATE('&', v_string, my_amp_pos + 1);
      IF my_amp_pos = 0 THEN
        LEAVE work_loop;
      END IF;

      -- Then the end
      SET my_comma_pos = LOCATE(';', v_string, my_amp_pos);
      IF my_comma_pos = 0 THEN
        LEAVE work_loop;
      END IF;

      -- Check this matches the format, and isn't just part of our text
      SET my_code = SUBSTRING(v_string, my_amp_pos, my_comma_pos - my_amp_pos + 1);
      IF my_code NOT REGEXP '&[A-Za-z]+;' THEN
        LEAVE work_loop;
      END IF;

      -- All good, so perform the replace
      SET my_char = SUBSTRING(v_string, my_amp_pos + 1, 1);
      SET v_string = REPLACE(v_string, my_code, my_char);

      -- Increment our 'safety' counter
      SET my_while = my_while + 1;
    END WHILE;

    RETURN v_string;

END $$

DELIMITER ;
