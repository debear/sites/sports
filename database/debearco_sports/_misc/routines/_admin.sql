##
## Internal procedures
##
#
# Run
#
DROP PROCEDURE IF EXISTS `_exec`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `_exec`(
  v_sql TEXT
)
    COMMENT 'Execute an arbitrary SQL statement'
BEGIN

  SET @sql = v_sql; # PREPARE must come from an @... variable, not one we declare
  PREPARE sth FROM @sql;
  EXECUTE sth;
  DEALLOCATE PREPARE sth;

END $$

DELIMITER ;

#
# Copy a temporary a table, both structure and data
#
DROP PROCEDURE IF EXISTS `_duplicate_tmp_table`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `_duplicate_tmp_table`(
  v_tbl_base VARCHAR(100),
  v_tbl_copy VARCHAR(100)
)
    COMMENT 'Duplicate a temporary table'
BEGIN

  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS ', v_tbl_copy, ';'));
  CALL _exec(CONCAT('CREATE TEMPORARY TABLE ', v_tbl_copy, ' LIKE ', v_tbl_base, ';'));
  CALL _exec(CONCAT('INSERT INTO ', v_tbl_copy, ' SELECT * FROM ', v_tbl_base, ';'));

END $$

DELIMITER ;
