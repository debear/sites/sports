CREATE TABLE `SPORTS_COMMON_USER_FAVOURITES` (
  `sport` VARCHAR(10) COLLATE latin1_general_ci NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `type` VARCHAR(15) COLLATE latin1_general_ci NOT NULL,
  `char_id` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `int_id` INT(11) DEFAULT NULL,
  UNIQUE KEY `FAUX_PRIMARY` (`sport`,`user_id`,`type`,`char_id`,`int_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
