CREATE TABLE `SPORTS_COMMON_MAJORS_ODDS` (
  `sport` ENUM('mlb','nfl','nhl','ahl') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT UNSIGNED NOT NULL,
  `home_line` DECIMAL(4,2) UNSIGNED DEFAULT NULL,
  `visitor_line` DECIMAL(4,2) UNSIGNED DEFAULT NULL,
  `home_spread` DECIMAL(3,1) SIGNED DEFAULT NULL,
  `visitor_spread` DECIMAL(3,1) SIGNED DEFAULT NULL,
  `over_under` DECIMAL(3,1) UNSIGNED DEFAULT NULL,
  `last_synced` DATETIME NOT NULL DEFAULT '2001-01-01 00:00:00',
  PRIMARY KEY (`sport`,`season`,`game_type`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
