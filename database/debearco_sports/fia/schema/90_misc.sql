CREATE TABLE `SPORTS_FIA_STATS` (
  `stat_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `section` ENUM('quali','race') COLLATE latin1_general_ci NOT NULL,
  `name_short` VARCHAR(40) COLLATE latin1_general_ci NOT NULL,
  `name_full` VARCHAR(200) COLLATE latin1_general_ci NOT NULL,
  `best_desc` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `type` ENUM('abs','avg','pct','delta') COLLATE latin1_general_ci NOT NULL,
  `disp_order` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
