CREATE TABLE `SPORTS_FIA_RACES` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `name_full` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `name_short` VARCHAR(30) COLLATE latin1_general_ci NOT NULL,
  `flag` CHAR(6) COLLATE latin1_general_ci DEFAULT NULL,
  `circuit` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `location` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `timezone` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `quirks` SET('SPRINT_RACE','SPRINT_SHOOTOUT') COLLATE latin1_general_ci DEFAULT NULL,
  `race_time` DATETIME NOT NULL,
  `race2_time` DATETIME DEFAULT NULL,
  `qual_time` DATETIME NOT NULL,
  `round_order` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `race_laps_total` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `lap_length` DECIMAL(4,3) UNSIGNED NOT NULL DEFAULT 0.000,
  `race_laps_completed` TINYINT(3) UNSIGNED DEFAULT NULL,
  `race2_laps_total` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `race2_laps_completed` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`round`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_RACE_DRIVERS` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `car_no` TINYINT(3) UNSIGNED NOT NULL,
  `driver_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` TINYINT(3) UNSIGNED NOT NULL,
  `driver_code` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`round`,`car_no`),
  KEY `by_driver` (`season`,`series`,`driver_id`),
  KEY `by_team` (`season`,`series`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_RACE_QUALI` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `session` ENUM('q1','q2','q3','sq1','sq2','sq3') NOT NULL,
  `pos` TINYINT(1) UNSIGNED DEFAULT 0,
  `car_no` TINYINT(1) UNSIGNED NOT NULL,
  `q_time` TIME DEFAULT NULL,
  `q_time_ms` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `gap_time` TIME DEFAULT NULL,
  `gap_time_ms` SMALLINT(5) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`round`,`session`,`car_no`),
  UNIQUE KEY `by_car` (`season`,`series`,`round`,`car_no`,`session`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_RACE_GRID` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `race` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `grid_pos` TINYINT(1) UNSIGNED NOT NULL,
  `car_no` TINYINT(3) UNSIGNED NOT NULL,
  `qual_pos` TINYINT(1) UNSIGNED DEFAULT NULL,
  `notes` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`round`,`race`,`grid_pos`),
  UNIQUE KEY `by_car` (`season`,`series`,`round`,`car_no`,`race`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_RACE_RESULT` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `race` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `pos` TINYINT(1) UNSIGNED NOT NULL,
  `result` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `car_no` TINYINT(3) UNSIGNED NOT NULL,
  `laps` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `race_time` TIME DEFAULT NULL,
  `race_time_ms` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `gap_time` TIME DEFAULT NULL,
  `gap_time_ms` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `pts` DECIMAL(3,1) UNSIGNED DEFAULT NULL,
  `notes` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`round`,`race`,`pos`),
  UNIQUE KEY `by_car` (`season`,`series`,`round`,`car_no`,`race`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_RACE_FASTEST_LAP` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `race` TINYINT(1) UNSIGNED NOT NULL,
  `lap_time` TIME NOT NULL,
  `lap_time_ms` SMALLINT(3) UNSIGNED NOT NULL,
  `lap_num` TINYINT(2) UNSIGNED NOT NULL,
  `car_no` TINYINT(2) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`series`,`round`,`race`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_RACE_LEADERS` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `race` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `lap_from` TINYINT(3) UNSIGNED NOT NULL,
  `lap_to` TINYINT(3) UNSIGNED NOT NULL,
  `car_no` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`series`,`round`,`race`,`lap_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
