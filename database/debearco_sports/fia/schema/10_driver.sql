CREATE TABLE `SPORTS_FIA_DRIVERS` (
  `driver_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(25) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(25) COLLATE latin1_general_ci NOT NULL,
  `birthplace` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `birthplace_country` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `nationality` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `dob` DATE DEFAULT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_DRIVER_HISTORY` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `driver_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pos` TINYINT(1) UNSIGNED DEFAULT 0,
  `pts` DECIMAL(4,1) UNSIGNED DEFAULT 0.0,
  `num_poles` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `num_wins` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `num_podiums` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `num_fastest_laps` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`series`,`driver_id`),
  KEY `by_driver` (`driver_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_DRIVER_HISTORY_PROGRESS` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `driver_id` SMALLINT(5) UNSIGNED NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `race` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `pos` TINYINT(1) UNSIGNED DEFAULT 0,
  `pts` DECIMAL(4,1) UNSIGNED DEFAULT 0.0,
  PRIMARY KEY (`season`,`series`,`driver_id`,`round`,`race`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_DRIVER_GP_HISTORY` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `driver_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` TINYINT(3) UNSIGNED NOT NULL,
  `gp_flag` CHAR(6) COLLATE latin1_general_ci DEFAULT NULL,
  `qual` TINYINT(1) UNSIGNED DEFAULT NULL,
  `pos` TINYINT(1) UNSIGNED DEFAULT NULL,
  `result` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `pts` DECIMAL(3,1) UNSIGNED DEFAULT NULL,
  `fastest_lap` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`round`,`driver_id`),
  KEY `by_driver_season` (`season`,`driver_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_DRIVER_STATS` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `driver_id` SMALLINT(5) UNSIGNED NOT NULL,
  `stat_id` TINYINT(3) UNSIGNED NOT NULL,
  `value` DECIMAL(7,2) DEFAULT 0.00,
  `extra` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `pos` TINYINT(3) UNSIGNED DEFAULT 0,
  `pos_tied` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`series`,`driver_id`,`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
