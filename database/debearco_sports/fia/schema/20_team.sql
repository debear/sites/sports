CREATE TABLE `SPORTS_FIA_TEAMS` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `team_id` TINYINT(1) UNSIGNED NOT NULL,
  `team_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `team_country` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  `official_name` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `base` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `base_country` CHAR(3) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_TEAM_HISTORY` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `team_id` TINYINT(1) UNSIGNED NOT NULL,
  `pos` TINYINT(1) UNSIGNED DEFAULT 0,
  `pts` DECIMAL(4,1) UNSIGNED DEFAULT 0.0,
  `num_poles` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `num_wins` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `num_podiums` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `num_fastest_laps` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `notes` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_TEAM_HISTORY_PROGRESS` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `team_id` TINYINT(1) UNSIGNED NOT NULL,
  `round` TINYINT(1) UNSIGNED NOT NULL,
  `race` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `pos` TINYINT(1) UNSIGNED DEFAULT 0,
  `pts` DECIMAL(4,1) UNSIGNED DEFAULT 0.0,
  `notes` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`series`,`team_id`,`round`,`race`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_FIA_TEAM_STATS` (
  `season` YEAR NOT NULL,
  `series` ENUM('f1','gp2','f2','gp3','f3','fe') COLLATE latin1_general_ci NOT NULL,
  `team_id` TINYINT(3) UNSIGNED NOT NULL,
  `stat_id` TINYINT(3) UNSIGNED NOT NULL,
  `value` DECIMAL(7,2) DEFAULT 0.00,
  `extra` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `pos` TINYINT(3) UNSIGNED DEFAULT 0,
  `pos_tied` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`series`,`team_id`,`stat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
