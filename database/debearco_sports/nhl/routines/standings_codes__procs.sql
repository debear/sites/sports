##
##
##
DROP PROCEDURE IF EXISTS `nhl_standings_codes_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_codes_setup`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Prepare NHL standing code processing'
BEGIN

  DECLARE v_season_finale DATE;
  # Determine which standings are final (so we can more accurately process the codes)
  SELECT MAX(game_date) INTO v_season_finale FROM SPORTS_NHL_SCHEDULE WHERE season = v_season AND game_type = 'regular';
  UPDATE tmp_standings SET _tb_final = (the_date = v_season_finale);

END $$

DELIMITER ;

##
## Process status codes
##
DROP PROCEDURE IF EXISTS `nhl_standings_codes_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_codes_process`(
  v_code CHAR(1),
  v_col VARCHAR(6),
  v_cutoff_pos TINYINT UNSIGNED
)
    COMMENT 'Process NHL standing codes'
BEGIN

  # Simplify our query by generalising our position and join cols
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = IF("', v_col, '" = "league", _tb_league_id, IF("', v_col, '" = "conf", _tb_conf_id, _tb_div_id)),
                                              _tb_pos = IF("', v_col, '" = "league", pos_league, IF("', v_col, '" = "conf", pos_conf, pos_div));'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  # Process - Guesstimate
  INSERT INTO tmp_standings (season, the_date, team_id, status_code)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           IF(tmp_standings_cpB.team_id IS NULL, v_code, tmp_standings_cpA.status_code) AS status_code
    FROM tmp_standings_cpA
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos > v_cutoff_pos
      AND (tmp_standings_cpB.max_pts > tmp_standings_cpA.pts
        OR tmp_standings_cpB.max_pts = tmp_standings_cpA.pts
         AND tmp_standings_cpB._tb_max_reg_ot_wins > tmp_standings_cpA.reg_ot_wins))
    WHERE tmp_standings_cpA._tb_final = 0
    AND   tmp_standings_cpA._tb_pos <= v_cutoff_pos
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
  ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);

  # Process - Final
  INSERT INTO tmp_standings (season, the_date, team_id, status_code)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           v_code AS status_code
    FROM tmp_standings_cpA
    WHERE tmp_standings_cpA._tb_final = 1
    AND   tmp_standings_cpA._tb_pos <= v_cutoff_pos
  ON DUPLICATE KEY UPDATE status_code = VALUES(status_code);

END $$

DELIMITER ;
