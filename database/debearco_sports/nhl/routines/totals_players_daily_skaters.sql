##
## Player daily game totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_daily_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_daily_skaters`()
    COMMENT 'Calculate daily game totals for NHL skaters'
BEGIN

  ##
  ## Games Played / Time on Ice
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, gp, start, toi, shifts)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           '1' AS gp,
           SPORTS_NHL_GAME_LINEUP.started AS start,
           tmp_game_players.toi,
           tmp_game_players.shifts
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          start = VALUES(start),
                          toi = VALUES(toi),
                          shifts = VALUES(shifts);

  ##
  ## Goals / Assists (EV, PP, SH -> *NOT* GW)
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, goals, assists, pp_goals, pp_assists, sh_goals, sh_assists)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT
                 IF(SPORTS_NHL_GAME_EVENT_GOAL.scorer = SPORTS_NHL_GAME_LINEUP.jersey,
                    SPORTS_NHL_GAME_EVENT_GOAL.event_id, NULL)) AS goals,
           COUNT(DISTINCT
                 IF(SPORTS_NHL_GAME_EVENT_GOAL.assist_1 = SPORTS_NHL_GAME_LINEUP.jersey
                    OR SPORTS_NHL_GAME_EVENT_GOAL.assist_2 = SPORTS_NHL_GAME_LINEUP.jersey,
                    SPORTS_NHL_GAME_EVENT_GOAL.event_id, NULL)) AS assists,
           COUNT(DISTINCT
                 IF(SPORTS_NHL_GAME_EVENT_GOAL.goal_type = 'PP'
                    AND SPORTS_NHL_GAME_EVENT_GOAL.scorer = SPORTS_NHL_GAME_LINEUP.jersey,
                    SPORTS_NHL_GAME_EVENT_GOAL.event_id, NULL)) AS pp_goals,
           COUNT(DISTINCT
                 IF(SPORTS_NHL_GAME_EVENT_GOAL.goal_type = 'PP'
                    AND (SPORTS_NHL_GAME_EVENT_GOAL.assist_1 = SPORTS_NHL_GAME_LINEUP.jersey
                      OR SPORTS_NHL_GAME_EVENT_GOAL.assist_2 = SPORTS_NHL_GAME_LINEUP.jersey),
                    SPORTS_NHL_GAME_EVENT_GOAL.event_id, NULL)) AS pp_assists,
           COUNT(DISTINCT
                 IF(SPORTS_NHL_GAME_EVENT_GOAL.goal_type = 'SH'
                    AND SPORTS_NHL_GAME_EVENT_GOAL.scorer = SPORTS_NHL_GAME_LINEUP.jersey,
                    SPORTS_NHL_GAME_EVENT_GOAL.event_id, NULL)) AS sh_goals,
           COUNT(DISTINCT
                 IF(SPORTS_NHL_GAME_EVENT_GOAL.goal_type = 'SH'
                    AND (SPORTS_NHL_GAME_EVENT_GOAL.assist_1 = SPORTS_NHL_GAME_LINEUP.jersey
                      OR SPORTS_NHL_GAME_EVENT_GOAL.assist_2 = SPORTS_NHL_GAME_LINEUP.jersey),
                    SPORTS_NHL_GAME_EVENT_GOAL.event_id, NULL)) AS sh_assists
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_GOAL
      ON (SPORTS_NHL_GAME_EVENT_GOAL.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_GOAL.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_GOAL.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_GOAL.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND (SPORTS_NHL_GAME_EVENT_GOAL.scorer = SPORTS_NHL_GAME_LINEUP.jersey
        OR SPORTS_NHL_GAME_EVENT_GOAL.assist_1 = SPORTS_NHL_GAME_LINEUP.jersey
        OR SPORTS_NHL_GAME_EVENT_GOAL.assist_2 = SPORTS_NHL_GAME_LINEUP.jersey))
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE goals = VALUES(goals),
                          assists = VALUES(assists),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists);

  ##
  ## Game Winners
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, gw_goals)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           IF(GW_GOAL.event_id IS NOT NULL, 1, 0) AS gw_goals
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_SCHEDULE.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_SCHEDULE.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND ((SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_GAME_LINEUP.team_id
        AND SPORTS_NHL_SCHEDULE.home_score > SPORTS_NHL_SCHEDULE.visitor_score)
        OR (SPORTS_NHL_SCHEDULE.visitor = SPORTS_NHL_GAME_LINEUP.team_id
        AND SPORTS_NHL_SCHEDULE.visitor_score > SPORTS_NHL_SCHEDULE.home_score)))
    LEFT JOIN SPORTS_NHL_GAME_EVENT_GOAL AS GW_GOAL
      ON (GW_GOAL.season = SPORTS_NHL_GAME_LINEUP.season
      AND GW_GOAL.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND GW_GOAL.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND GW_GOAL.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND GW_GOAL.scorer = SPORTS_NHL_GAME_LINEUP.jersey
      AND GW_GOAL.goal_num = IF(SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_GAME_LINEUP.team_id,
                                    SPORTS_NHL_SCHEDULE.visitor_score, SPORTS_NHL_SCHEDULE.home_score) + 1)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE gw_goals = VALUES(gw_goals);

  ##
  ## Plus / Minus
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, plus_minus)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           SUM(IF(SPORTS_NHL_GAME_EVENT_ONICE.event_id IS NULL,
                  0,
                  IF(SPORTS_NHL_GAME_EVENT_ONICE.team_id IS NOT NULL,
                     IF(PLUS_MINUS.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id, 1, -1),
                     0))) AS plus_minus
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_GOAL AS PLUS_MINUS
      ON (PLUS_MINUS.season = SPORTS_NHL_GAME_LINEUP.season
      AND PLUS_MINUS.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND PLUS_MINUS.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND PLUS_MINUS.goal_type <> 'PP')
    LEFT JOIN SPORTS_NHL_GAME_EVENT_ONICE
      ON (SPORTS_NHL_GAME_EVENT_ONICE.season = PLUS_MINUS.season
      AND SPORTS_NHL_GAME_EVENT_ONICE.game_type = PLUS_MINUS.game_type
      AND SPORTS_NHL_GAME_EVENT_ONICE.game_id = PLUS_MINUS.game_id
      AND SPORTS_NHL_GAME_EVENT_ONICE.event_id = PLUS_MINUS.event_id
      AND SPORTS_NHL_GAME_EVENT_ONICE.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND (SPORTS_NHL_GAME_EVENT_ONICE.player_1 = SPORTS_NHL_GAME_LINEUP.jersey
        OR SPORTS_NHL_GAME_EVENT_ONICE.player_2 = SPORTS_NHL_GAME_LINEUP.jersey
        OR SPORTS_NHL_GAME_EVENT_ONICE.player_3 = SPORTS_NHL_GAME_LINEUP.jersey
        OR SPORTS_NHL_GAME_EVENT_ONICE.player_4 = SPORTS_NHL_GAME_LINEUP.jersey
        OR SPORTS_NHL_GAME_EVENT_ONICE.player_5 = SPORTS_NHL_GAME_LINEUP.jersey
        OR SPORTS_NHL_GAME_EVENT_ONICE.player_6 = SPORTS_NHL_GAME_LINEUP.jersey))
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE plus_minus = VALUES(plus_minus);

  ##
  ## PIMs / Pens Taken
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, pims, pens_taken)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           IF(SPORTS_NHL_GAME_EVENT_PENALTY.event_id IS NULL,
              0,
              SUM(SPORTS_NHL_GAME_EVENT_PENALTY.pims)) AS pims,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_PENALTY.event_id) AS pens_taken
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_PENALTY
      ON (SPORTS_NHL_GAME_EVENT_PENALTY.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE pims = VALUES(pims),
                          pens_taken = VALUES(pens_taken);

  ##
  ## Pens Drawn
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, pens_drawn)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_PENALTY.event_id) AS pens_drawn
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_PENALTY
      ON (SPORTS_NHL_GAME_EVENT_PENALTY.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.drawn_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.drawn_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE pens_drawn = VALUES(pens_drawn);

  ##
  ## Shots
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, shots)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_SHOT.event_id)
            + COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_GOAL.event_id) AS shots
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_SHOT
      ON (SPORTS_NHL_GAME_EVENT_SHOT.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_SHOT.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_SHOT.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_SHOT.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_SHOT.by_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_GOAL
      ON (SPORTS_NHL_GAME_EVENT_GOAL.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_GOAL.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_GOAL.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_GOAL.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_GOAL.scorer = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE shots = VALUES(shots);

  ##
  ## Missed Shots
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, missed_shots)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_MISSEDSHOT.event_id) AS missed_shots
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_MISSEDSHOT
      ON (SPORTS_NHL_GAME_EVENT_MISSEDSHOT.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_MISSEDSHOT.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_MISSEDSHOT.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_MISSEDSHOT.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_MISSEDSHOT.by_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE missed_shots = VALUES(missed_shots);

  ##
  ## Blocked Shots
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, blocked_shots, shots_blocked)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT BLOCKS.event_id) AS blocked_shots,
           COUNT(DISTINCT SHOTS_BLOCKED.event_id) AS shots_blocked
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_BLOCKEDSHOT AS BLOCKS
      ON (BLOCKS.season = SPORTS_NHL_GAME_LINEUP.season
      AND BLOCKS.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND BLOCKS.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND BLOCKS.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND BLOCKS.by_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_BLOCKEDSHOT AS SHOTS_BLOCKED
      ON (SHOTS_BLOCKED.season = SPORTS_NHL_GAME_LINEUP.season
      AND SHOTS_BLOCKED.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SHOTS_BLOCKED.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SHOTS_BLOCKED.on_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SHOTS_BLOCKED.on_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE blocked_shots = VALUES(blocked_shots),
                          shots_blocked = VALUES(shots_blocked);

  ##
  ## Hits
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, hits, times_hit)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT HITS.event_id) AS hits,
           COUNT(DISTINCT TIMES_HIT.event_id) AS times_hit
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_HIT AS HITS
      ON (HITS.season = SPORTS_NHL_GAME_LINEUP.season
      AND HITS.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND HITS.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND HITS.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND HITS.by_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_HIT AS TIMES_HIT
      ON (TIMES_HIT.season = SPORTS_NHL_GAME_LINEUP.season
      AND TIMES_HIT.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND TIMES_HIT.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND TIMES_HIT.on_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND TIMES_HIT.on_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE hits = VALUES(hits),
                          times_hit = VALUES(times_hit);

  ##
  ## Faceoffs
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, fo_wins, fo_loss)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           SUM(IF(SPORTS_NHL_GAME_EVENT_FACEOFF.winner = SPORTS_NHL_GAME_LINEUP.team_id,
                  1, 0)) AS fo_wins,
           SUM(IF(SPORTS_NHL_GAME_EVENT_FACEOFF.winner = SPORTS_NHL_GAME_LINEUP.team_id,
                  0, 1)) AS fo_loss
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_SCHEDULE.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_SCHEDULE.game_id = SPORTS_NHL_GAME_LINEUP.game_id)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_FACEOFF
      ON (SPORTS_NHL_GAME_EVENT_FACEOFF.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_FACEOFF.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_FACEOFF.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND IF(SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_GAME_LINEUP.team_id,
             SPORTS_NHL_GAME_EVENT_FACEOFF.home,
             SPORTS_NHL_GAME_EVENT_FACEOFF.visitor) = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    AND   SPORTS_NHL_GAME_EVENT_FACEOFF.event_id IS NOT NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE fo_wins = VALUES(fo_wins),
                          fo_loss = VALUES(fo_loss);

  ##
  ## Shootout
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, so_goals, so_shots)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           IF(SPORTS_NHL_GAME_EVENT_SHOOTOUT.status = 'goal', 1, 0) AS so_goals,
           IF(SPORTS_NHL_GAME_EVENT_SHOOTOUT.event_id IS NOT NULL, 1, 0) AS so_shots
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_SHOOTOUT
      ON (SPORTS_NHL_GAME_EVENT_SHOOTOUT.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.by_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots);

  ##
  ## Giveaways
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, giveaways)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_GIVEAWAY.event_id) AS giveaways
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_GIVEAWAY
      ON (SPORTS_NHL_GAME_EVENT_GIVEAWAY.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_GIVEAWAY.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_GIVEAWAY.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_GIVEAWAY.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_GIVEAWAY.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE giveaways = VALUES(giveaways);

  ##
  ## Takeaways
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, takeaways)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_TAKEAWAY.event_id) AS takeaways
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_TAKEAWAY
      ON (SPORTS_NHL_GAME_EVENT_TAKEAWAY.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_TAKEAWAY.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_TAKEAWAY.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_TAKEAWAY.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_TAKEAWAY.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE takeaways = VALUES(takeaways);

END $$

DELIMITER ;

