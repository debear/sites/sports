#
# Initial standings
#
DROP PROCEDURE IF EXISTS `nhl_standings_initial`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_initial`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Create the initial NHL standings for a season'
BEGIN

  # Declare our vars
  DECLARE v_date DATE;
  DECLARE v_max_gp TINYINT UNSIGNED;

  # Get meta out
  SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) INTO v_date FROM SPORTS_NHL_SCHEDULE WHERE season = v_season;
  SELECT 2 * COUNT(*) INTO v_max_gp FROM SPORTS_NHL_SCHEDULE WHERE season = v_season AND game_type = 'regular' GROUP BY home ORDER BY COUNT(*) DESC LIMIT 1;

  # Get the list of teams
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    city VARCHAR(20),
    franchise VARCHAR(20),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id),
    INDEX by_def (team_id, conf_id, div_id)
  ) ENGINE = MEMORY
    SELECT TEAM_DIV.team_id, TEAM.city, TEAM.franchise,
           DIVN.parent_id AS conf_id,
           DIVN.grouping_id AS div_id
    FROM SPORTS_NHL_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_NHL_TEAMS AS TEAM
      ON (TEAM.team_id = TEAM_DIV.team_id)
    JOIN SPORTS_NHL_GROUPINGS AS DIVN
      ON (DIVN.grouping_id = TEAM_DIV.grouping_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099)
    ORDER BY TEAM_DIV.team_id;
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # Store our table
  DELETE FROM SPORTS_NHL_STANDINGS WHERE season = v_season;
  INSERT INTO SPORTS_NHL_STANDINGS (season, the_date, team_id, wins, loss, ot_loss, pts, goals_for, goals_against, home_wins, home_loss, home_ot_loss, visitor_wins, visitor_loss, visitor_ot_loss, recent_wins, recent_loss, recent_ot_loss, pos_league, pos_conf, pos_div, max_pts, reg_ot_wins, status_code)
    SELECT v_season, v_date, team_id, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 * v_max_gp, 0, NULL
    FROM tmp_teams;

  # Sort league
  INSERT INTO SPORTS_NHL_STANDINGS (season, the_date, team_id, pos_league)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_league = VALUES(pos_league);

  # Sort conference
  INSERT INTO SPORTS_NHL_STANDINGS (season, the_date, team_id, pos_conf)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.conf_id = ME.conf_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_conf = VALUES(pos_conf);

  # Sort division
  INSERT INTO SPORTS_NHL_STANDINGS (season, the_date, team_id, pos_div)
    SELECT v_season, v_date, ME.team_id,
           1 + COUNT(DISTINCT OTHER.team_id)
    FROM tmp_teams AS ME
    LEFT JOIN tmp_teams_cp AS OTHER
      ON (OTHER.team_id <> ME.team_id
      AND OTHER.conf_id = ME.conf_id
      AND OTHER.div_id = ME.div_id
      AND (STRCMP(OTHER.city, ME.city) = -1
          OR (STRCMP(OTHER.city, ME.city) = 0 AND STRCMP(OTHER.franchise, ME.franchise) = -1)))
    GROUP BY ME.team_id
  ON DUPLICATE KEY UPDATE pos_div = VALUES(pos_div);

  ALTER TABLE SPORTS_NHL_STANDINGS ORDER BY season, the_date, team_id;

END $$

DELIMITER ;
