##
## Team daily game totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_teams_daily_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_daily_skaters`()
    COMMENT 'Calculate daily game totals for NHL team skaters'
BEGIN

  ##
  ## Combine player totals for the teams
  ##
  INSERT INTO SPORTS_NHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, gp, goals, assists, plus_minus, pims, pens_taken, pens_drawn, pp_goals, pp_assists, sh_goals, sh_assists, shots, missed_shots, blocked_shots, shots_blocked, hits, times_hit, fo_wins, fo_loss, so_goals, so_shots, giveaways, takeaways)
    SELECT SPORTS_NHL_PLAYERS_GAME_SKATERS.season,
           SPORTS_NHL_PLAYERS_GAME_SKATERS.team_id,
           SPORTS_NHL_PLAYERS_GAME_SKATERS.game_type,
           SPORTS_NHL_PLAYERS_GAME_SKATERS.game_id,
           'for' AS stat_dir,
           1 AS gp,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.goals) AS goals,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.assists) AS assists,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.plus_minus) AS plus_minus,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.pims) AS pims,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.pens_taken) AS pens_taken,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.pens_drawn) AS pens_drawn,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.pp_goals) AS pp_goals,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.pp_assists) AS pp_assists,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.sh_goals) AS sh_goals,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.sh_assists) AS sh_assists,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.shots) AS shots,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.missed_shots) AS missed_shots,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.blocked_shots) AS blocked_shots,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.shots_blocked) AS shots_blocked,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.hits) AS hits,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.times_hit) AS times_hit,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.fo_wins) AS fo_wins,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.fo_loss) AS fo_loss,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.so_goals) AS so_goals,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.so_shots) AS so_shots,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.giveaways) AS giveaways,
           SUM(SPORTS_NHL_PLAYERS_GAME_SKATERS.takeaways) AS takeaways
    FROM tmp_game_list
    JOIN SPORTS_NHL_PLAYERS_GAME_SKATERS
      ON (SPORTS_NHL_PLAYERS_GAME_SKATERS.season = tmp_game_list.season
      AND SPORTS_NHL_PLAYERS_GAME_SKATERS.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_PLAYERS_GAME_SKATERS.game_id = tmp_game_list.game_id)
    GROUP BY SPORTS_NHL_PLAYERS_GAME_SKATERS.season,
             SPORTS_NHL_PLAYERS_GAME_SKATERS.team_id,
             SPORTS_NHL_PLAYERS_GAME_SKATERS.game_type,
             SPORTS_NHL_PLAYERS_GAME_SKATERS.game_id
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          plus_minus = VALUES(plus_minus),
                          pims = VALUES(pims),
                          pens_taken = VALUES(pens_taken),
                          pens_drawn = VALUES(pens_drawn),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          shots = VALUES(shots),
                          missed_shots = VALUES(missed_shots),
                          blocked_shots = VALUES(blocked_shots),
                          shots_blocked = VALUES(shots_blocked),
                          hits = VALUES(hits),
                          times_hit = VALUES(times_hit),
                          fo_wins = VALUES(fo_wins),
                          fo_loss = VALUES(fo_loss),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots),
                          giveaways = VALUES(giveaways),
                          takeaways = VALUES(takeaways);

  ##
  ## Goalie scoring / pens
  ##
  INSERT INTO SPORTS_NHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, gp, goals, assists, pims, pens_taken, pens_drawn)
    SELECT SPORTS_NHL_PLAYERS_GAME_GOALIES.season,
           SPORTS_NHL_PLAYERS_GAME_GOALIES.team_id,
           SPORTS_NHL_PLAYERS_GAME_GOALIES.game_type,
           SPORTS_NHL_PLAYERS_GAME_GOALIES.game_id,
           'for' AS stat_dir,
           1 AS gp,
           SUM(SPORTS_NHL_PLAYERS_GAME_GOALIES.goals) AS goals,
           SUM(SPORTS_NHL_PLAYERS_GAME_GOALIES.assists) AS assists,
           SUM(SPORTS_NHL_PLAYERS_GAME_GOALIES.pims) AS pims,
           SUM(SPORTS_NHL_PLAYERS_GAME_GOALIES.pens_taken) AS pens_taken,
           SUM(SPORTS_NHL_PLAYERS_GAME_GOALIES.pens_drawn) AS pens_drawn
    FROM tmp_game_list
    JOIN SPORTS_NHL_PLAYERS_GAME_GOALIES
      ON (SPORTS_NHL_PLAYERS_GAME_GOALIES.season = tmp_game_list.season
      AND SPORTS_NHL_PLAYERS_GAME_GOALIES.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_PLAYERS_GAME_GOALIES.game_id = tmp_game_list.game_id)
    GROUP BY SPORTS_NHL_PLAYERS_GAME_GOALIES.season,
             SPORTS_NHL_PLAYERS_GAME_GOALIES.team_id,
             SPORTS_NHL_PLAYERS_GAME_GOALIES.game_type,
             SPORTS_NHL_PLAYERS_GAME_GOALIES.game_id
  ON DUPLICATE KEY UPDATE goals = goals + VALUES(goals),
                          assists = assists + VALUES(assists),
                          pims = pims + VALUES(pims),
                          pens_taken = pens_taken + VALUES(pens_taken),
                          pens_drawn = pens_drawn + VALUES(pens_drawn);

  ##
  ## Other composite stats
  ##
  INSERT INTO SPORTS_NHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, pp_opps, pk_goals, pk_kills, pk_opps)
    SELECT PP.season,
           PP.team_id,
           PP.game_type,
           PP.game_id,
           'for' AS stat_dir,
           PP.pp_opps,
           PK.pp_goals AS pk_goals,
           PK.pp_opps - PK.pp_goals AS pk_opps,
           PK.pp_opps AS pk_opps
    FROM tmp_game_list
    JOIN SPORTS_NHL_GAME_PP_STATS AS PP
      ON (PP.season = tmp_game_list.season
      AND PP.game_type = tmp_game_list.game_type
      AND PP.game_id = tmp_game_list.game_id
      AND PP.pp_type = 'pp')
    JOIN SPORTS_NHL_GAME_PP_STATS AS PK
      ON (PK.season = tmp_game_list.season
      AND PK.game_type = tmp_game_list.game_type
      AND PK.game_id = tmp_game_list.game_id
      AND PK.team_id = PP.team_id
      AND PK.pp_type = 'pk')
    GROUP BY PP.season,
             PP.team_id,
             PP.game_type,
             PP.game_id
  ON DUPLICATE KEY UPDATE pp_opps = VALUES(pp_opps),
                          pk_goals = VALUES(pk_goals),
                          pk_kills = VALUES(pk_kills),
                          pk_opps = VALUES(pk_opps);

  ##
  ## And get oppositions totals for these...
  ##
  INSERT INTO SPORTS_NHL_TEAMS_GAME_SKATERS (season, team_id, game_type, game_id, stat_dir, gp, goals, assists, plus_minus, pims, pens_taken, pens_drawn, pp_goals, pp_assists, pp_opps, pk_goals, pk_kills, pk_opps, sh_goals, sh_assists, shots, missed_shots, blocked_shots, shots_blocked, hits, times_hit, fo_wins, fo_loss, so_goals, so_shots, giveaways, takeaways)
    SELECT ME.season, ME.team_id, ME.game_type, ME.game_id,
           'against' AS stat_dir,
           THEM.gp,
           THEM.goals,
           THEM.assists,
           THEM.plus_minus,
           THEM.pims,
           THEM.pens_taken,
           THEM.pens_drawn,
           THEM.pp_goals,
           THEM.pp_assists,
           THEM.pp_opps,
           THEM.pk_goals,
           THEM.pk_kills,
           THEM.pk_opps,
           THEM.sh_goals,
           THEM.sh_assists,
           THEM.shots,
           THEM.missed_shots,
           THEM.blocked_shots,
           THEM.shots_blocked,
           THEM.hits,
           THEM.times_hit,
           THEM.fo_wins,
           THEM.fo_loss,
           THEM.so_goals,
           THEM.so_shots,
           THEM.giveaways,
           THEM.takeaways
    FROM tmp_game_list
    JOIN SPORTS_NHL_TEAMS_GAME_SKATERS AS ME
      ON (ME.season = tmp_game_list.season
      AND ME.game_type = tmp_game_list.game_type
      AND ME.game_id = tmp_game_list.game_id
      AND ME.stat_dir = 'for')
    JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = ME.season
      AND SPORTS_NHL_SCHEDULE.game_type = ME.game_type
      AND SPORTS_NHL_SCHEDULE.game_id = ME.game_id)
    JOIN SPORTS_NHL_TEAMS_GAME_SKATERS AS THEM
      ON (THEM.season = SPORTS_NHL_SCHEDULE.season
      AND THEM.team_id = IF(ME.team_id = SPORTS_NHL_SCHEDULE.home, SPORTS_NHL_SCHEDULE.visitor, SPORTS_NHL_SCHEDULE.home)
      AND THEM.game_type = SPORTS_NHL_SCHEDULE.game_type
      AND THEM.game_id = SPORTS_NHL_SCHEDULE.game_id
      AND THEM.stat_dir = 'for')
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          plus_minus = VALUES(plus_minus),
                          pims = VALUES(pims),
                          pens_taken = VALUES(pens_taken),
                          pens_drawn = VALUES(pens_drawn),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          pp_opps = VALUES(pp_opps),
                          pk_goals = VALUES(pk_goals),
                          pk_kills = VALUES(pk_kills),
                          pk_opps = VALUES(pk_opps),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          shots = VALUES(shots),
                          missed_shots = VALUES(missed_shots),
                          blocked_shots = VALUES(blocked_shots),
                          shots_blocked = VALUES(shots_blocked),
                          hits = VALUES(hits),
                          times_hit = VALUES(times_hit),
                          fo_wins= VALUES(fo_wins),
                          fo_loss = VALUES(fo_loss),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots),
                          giveaways = VALUES(giveaways),
                          takeaways = VALUES(takeaways);

END $$

DELIMITER ;

