##
## Playoff seeds
##
DROP PROCEDURE IF EXISTS `nhl_playoff_seeds`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_seeds`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate NHL playoff seeds'
BEGIN

  DECLARE v_standing_date DATE;
  DECLARE v_outstanding SMALLINT UNSIGNED;

  SELECT MAX(the_date) INTO v_standing_date FROM SPORTS_NHL_STANDINGS WHERE season = v_season;

  # Clear previous run
  DELETE FROM SPORTS_NHL_PLAYOFF_SEEDS WHERE season = v_season;

  # Determine if in-season or after the season was finished
  SELECT COUNT(*) INTO v_outstanding FROM SPORTS_NHL_SCHEDULE WHERE season = v_season AND game_type = 'regular' AND status IS NULL;

  # Calculate - in-season
  IF v_outstanding > 0 THEN
    # Calculate by hand
    CALL nhl_playoff_seeds_div_wc(v_season, v_standing_date);

  # Calculate - after season
  ELSE
    INSERT INTO SPORTS_NHL_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
      SELECT SPORTS_NHL_STANDINGS.season,
             SPORTS_NHL_GROUPINGS.parent_id AS conf_id,
             SPORTS_NHL_STANDINGS.pos_conf AS seed,
             SPORTS_NHL_STANDINGS.team_id,
             SPORTS_NHL_STANDINGS.pos_div,
             SPORTS_NHL_STANDINGS.pos_conf,
             SPORTS_NHL_STANDINGS.pos_league
      FROM SPORTS_NHL_STANDINGS
      JOIN SPORTS_NHL_TEAMS_GROUPINGS
        ON (SPORTS_NHL_TEAMS_GROUPINGS.team_id = SPORTS_NHL_STANDINGS.team_id
        AND v_season BETWEEN SPORTS_NHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_NHL_TEAMS_GROUPINGS.season_to, 2099))
      JOIN SPORTS_NHL_GROUPINGS
        ON (SPORTS_NHL_GROUPINGS.grouping_id = SPORTS_NHL_TEAMS_GROUPINGS.grouping_id)
      WHERE SPORTS_NHL_STANDINGS.season = v_season
      AND   SPORTS_NHL_STANDINGS.the_date = v_standing_date
      AND   SPORTS_NHL_STANDINGS.status_code IS NOT NULL;
  END IF;

  # Final ordering
  ALTER TABLE SPORTS_NHL_PLAYOFF_SEEDS ORDER BY season, conf_id, seed;

END $$

DELIMITER ;

##
## In-season calcs using Divisional format with wild cards
##
DROP PROCEDURE IF EXISTS `nhl_playoff_seeds_div_wc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_seeds_div_wc`(
  v_season SMALLINT UNSIGNED,
  v_standing_date DATE
)
    COMMENT 'Calculate NHL playoff seeds: divisional with wildcards'
BEGIN

  # Get
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    season SMALLINT UNSIGNED,
    conf_id TINYINT UNSIGNED,
    seed TINYINT UNSIGNED,
    team_id CHAR(3),
    pos_div TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    PRIMARY KEY (season, conf_id, team_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_NHL_STANDINGS.season,
           SPORTS_NHL_GROUPINGS.parent_id AS conf_id,
           0 AS seed,
           SPORTS_NHL_STANDINGS.team_id,
           SPORTS_NHL_STANDINGS.pos_div,
           SPORTS_NHL_STANDINGS.pos_conf,
           SPORTS_NHL_STANDINGS.pos_league
    FROM SPORTS_NHL_STANDINGS
    JOIN SPORTS_NHL_TEAMS_GROUPINGS
      ON (SPORTS_NHL_TEAMS_GROUPINGS.team_id = SPORTS_NHL_STANDINGS.team_id
      AND v_season BETWEEN SPORTS_NHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_NHL_TEAMS_GROUPINGS.season_to, 2099))
    JOIN SPORTS_NHL_GROUPINGS
      ON (SPORTS_NHL_GROUPINGS.grouping_id = SPORTS_NHL_TEAMS_GROUPINGS.grouping_id)
    WHERE SPORTS_NHL_STANDINGS.season = v_season
    AND   SPORTS_NHL_STANDINGS.the_date = v_standing_date
    AND   SPORTS_NHL_STANDINGS.pos_conf <= 8;

  # Sort
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cpA');
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cpB');
  INSERT INTO tmp_teams (season, conf_id, team_id, seed)
    SELECT tmp_teams_cpA.season, tmp_teams_cpA.conf_id, tmp_teams_cpA.team_id,
           COUNT(tmp_teams_cpB.team_id) + 1 AS seed
    FROM tmp_teams_cpA
    LEFT JOIN tmp_teams_cpB
      ON (tmp_teams_cpB.season = tmp_teams_cpA.season
      AND tmp_teams_cpB.conf_id = tmp_teams_cpA.conf_id
      AND tmp_teams_cpB.pos_conf < tmp_teams_cpA.pos_conf)
    GROUP BY tmp_teams_cpA.season, tmp_teams_cpA.conf_id, tmp_teams_cpA.team_id
  ON DUPLICATE KEY UPDATE seed = VALUES(seed);

  INSERT INTO SPORTS_NHL_PLAYOFF_SEEDS (season, conf_id, seed, team_id, pos_div, pos_conf, pos_league)
    SELECT * FROM tmp_teams;

END $$

DELIMITER ;
