##
## Player totals sorted
##
DROP PROCEDURE IF EXISTS `nhl_totals_teams_season_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for NHL teams'
BEGIN

  # Broken down by individual routines
  CALL nhl_totals_teams_season_sort_skaters(v_season, v_season_type);
  CALL nhl_totals_teams_season_sort_skaters_advanced(v_season, v_season_type);
  CALL nhl_totals_teams_season_sort_goalies(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_teams_season_sort_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season_sort_skaters`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort skater season totals for NHL teams'
BEGIN

  DELETE FROM SPORTS_NHL_TEAMS_SEASON_SKATERS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nhl_totals_teams_sort__tmp(v_season, v_season_type, 'SKATERS',
                                  'goals SMALLINT UNSIGNED,
                                   assists SMALLINT UNSIGNED,
                                   points SMALLINT UNSIGNED,
                                   plus_minus SMALLINT SIGNED,
                                   pims SMALLINT UNSIGNED,
                                   pens_taken SMALLINT UNSIGNED,
                                   pens_drawn SMALLINT UNSIGNED,
                                   pp_goals TINYINT UNSIGNED,
                                   pp_assists TINYINT UNSIGNED,
                                   pp_points SMALLINT UNSIGNED,
                                   pp_opps SMALLINT UNSIGNED,
                                   pp_pct DECIMAL(6,5),
                                   pk_goals TINYINT UNSIGNED,
                                   pk_kills SMALLINT UNSIGNED,
                                   pk_opps SMALLINT UNSIGNED,
                                   pk_pct DECIMAL(6,5),
                                   sh_goals TINYINT UNSIGNED,
                                   sh_assists TINYINT UNSIGNED,
                                   sh_points SMALLINT UNSIGNED,
                                   shots SMALLINT UNSIGNED,
                                   shot_pct DECIMAL(6,5),
                                   missed_shots SMALLINT UNSIGNED,
                                   blocked_shots SMALLINT UNSIGNED,
                                   shots_blocked SMALLINT UNSIGNED,
                                   hits SMALLINT UNSIGNED,
                                   times_hit SMALLINT UNSIGNED,
                                   fo_wins SMALLINT UNSIGNED,
                                   fo_loss SMALLINT UNSIGNED,
                                   fo_pct DECIMAL(6,5),
                                   so_goals TINYINT UNSIGNED,
                                   so_shots TINYINT UNSIGNED,
                                   so_pct DECIMAL(6,5),
                                   giveaways SMALLINT UNSIGNED,
                                   takeaways SMALLINT UNSIGNED',
                                  'goals,
                                   assists,
                                   goals + assists AS points,
                                   plus_minus,
                                   pims,
                                   pens_taken,
                                   pens_drawn,
                                   pp_goals,
                                   pp_assists,
                                   pp_goals + pp_assists AS pp_points,
                                   pp_opps,
                                   IF(pp_opps > 0, pp_goals / pp_opps, 0) AS pp_pct,
                                   pk_goals,
                                   pk_kills,
                                   pk_opps,
                                   IF(pk_opps > 0, pk_kills / pk_opps, 0) AS pk_pct,
                                   sh_goals,
                                   sh_assists,
                                   sh_goals + sh_assists AS sh_points,
                                   shots,
                                   IF(shots > 0, goals / shots, 0) AS shot_pct,
                                   missed_shots,
                                   blocked_shots,
                                   shots_blocked,
                                   hits,
                                   times_hit,
                                   fo_wins,
                                   fo_loss,
                                   IF((fo_wins + fo_loss) > 0, fo_wins / (fo_wins + fo_loss), 0) AS fo_pct,
                                   so_goals,
                                   so_shots,
                                   IF(so_shots > 0, so_goals / so_shots, 0) AS so_pct,
                                   giveaways,
                                   takeaways');

  # Calcs
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'goals', 'B.goals > A.goals');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'assists', 'B.assists > A.assists');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'points', 'B.points > A.points');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'plus_minus', 'B.plus_minus > A.plus_minus');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pims', 'B.pims > A.pims');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pens_taken', 'B.pens_taken > A.pens_taken');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pens_drawn', 'B.pens_drawn > A.pens_drawn');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_goals', 'B.pp_goals > A.pp_goals');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_assists', 'B.pp_assists > A.pp_assists');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_points', 'B.pp_points > A.pp_points');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_opps', 'B.pp_opps > A.pp_opps');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_pct', 'B.pp_pct > A.pp_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_goals', 'B.pk_goals < A.pk_goals');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_kills', 'B.pk_kills > A.pk_kills');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_opps', 'B.pk_opps < A.pk_opps');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'pk_pct', 'B.pk_pct > A.pk_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_goals', 'B.sh_goals > A.sh_goals');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_assists', 'B.sh_assists > A.sh_assists');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_points', 'B.sh_points > A.sh_points');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'shots', 'B.shots > A.shots');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'shot_pct', 'B.shot_pct > A.shot_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'missed_shots', 'B.missed_shots > A.missed_shots');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'blocked_shots', 'B.blocked_shots > A.blocked_shots');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'shots_blocked', 'B.shots_blocked > A.shots_blocked');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'hits', 'B.hits > A.hits');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'times_hit', 'B.times_hit > A.times_hit');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'fo_wins', 'B.fo_wins > A.fo_wins');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'fo_loss', 'B.fo_loss > A.fo_loss');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'fo_pct', 'B.fo_pct > A.fo_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'so_goals', 'B.so_goals > A.so_goals');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'so_shots', 'B.so_shots > A.so_shots');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'so_pct', 'B.so_pct > A.so_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'giveaways', 'B.giveaways > A.giveaways');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS', 'takeaways', 'B.takeaways > A.takeaways');

  # Order
  ALTER TABLE SPORTS_NHL_TEAMS_SEASON_SKATERS_SORTED ORDER BY season, season_type, team_id, stat_dir;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_teams_season_sort_skaters_advanced`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season_sort_skaters_advanced`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort advanced skater season totals for NHL teams'
BEGIN

  DELETE FROM SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nhl_totals_teams_sort__tmp(v_season, v_season_type, 'SKATERS_ADVANCED',
                                  'corsi_cf SMALLINT(5) UNSIGNED,
                                   corsi_ca SMALLINT(5) UNSIGNED,
                                   corsi_pct DECIMAL(4,1) UNSIGNED,
                                   fenwick_ff SMALLINT(5) UNSIGNED,
                                   fenwick_fa SMALLINT(5) UNSIGNED,
                                   fenwick_pct DECIMAL(4,1) UNSIGNED,
                                   pdo_sh_gf SMALLINT(5) UNSIGNED,
                                   pdo_sh_sog SMALLINT(5) UNSIGNED,
                                   pdo_sh DECIMAL(4,1) UNSIGNED,
                                   pdo_sv_sv SMALLINT(5) UNSIGNED,
                                   pdo_sv_sog SMALLINT(5) UNSIGNED,
                                   pdo_sv DECIMAL(4,1) UNSIGNED,
                                   pdo DECIMAL(4,1) UNSIGNED',
                                  'corsi_cf,
                                   corsi_ca,
                                   corsi_pct,
                                   fenwick_ff,
                                   fenwick_fa,
                                   fenwick_pct,
                                   pdo_sh_gf,
                                   pdo_sh_sog,
                                   pdo_sh,
                                   pdo_sv_sv,
                                   pdo_sv_sog,
                                   pdo_sv,
                                   pdo');

  # Calcs
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_cf', 'B.corsi_cf > A.corsi_cf');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_ca', 'B.corsi_ca < A.corsi_ca');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_pct', 'B.corsi_pct > A.corsi_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_ff', 'B.fenwick_ff > A.fenwick_ff');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_fa', 'B.fenwick_fa < A.fenwick_fa');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_pct', 'B.fenwick_pct > A.fenwick_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sh_gf', 'B.pdo_sh_gf > A.pdo_sh_gf');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sh_sog', 'B.pdo_sh_sog > A.pdo_sh_sog');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sh', 'B.pdo_sh > A.pdo_sh');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sv_sv', 'B.pdo_sv_sv > A.pdo_sv_sv');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sv_sog', 'B.pdo_sv_sog > A.pdo_sv_sog');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sv', 'B.pdo_sv > A.pdo_sv');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo', 'B.pdo > A.pdo');

  # Order
  ALTER TABLE SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED_SORTED ORDER BY season, season_type, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_teams_season_sort_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season_sort_goalies`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort goalie season totals for NHL teams'
BEGIN

  DELETE FROM SPORTS_NHL_TEAMS_SEASON_GOALIES_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nhl_totals_teams_sort__tmp(v_season, v_season_type, 'GOALIES',
                                  'win TINYINT UNSIGNED,
                                   loss TINYINT UNSIGNED,
                                   ot_loss TINYINT UNSIGNED,
                                   so_loss TINYINT UNSIGNED,
                                   goals_against SMALLINT UNSIGNED,
                                   shots_against SMALLINT UNSIGNED,
                                   saves SMALLINT UNSIGNED,
                                   save_pct DECIMAL(6,5),
                                   goals_against_avg DECIMAL(8,5),
                                   shutout TINYINT UNSIGNED,
                                   minutes_played MEDIUMINT UNSIGNED,
                                   en_goals_against TINYINT UNSIGNED,
                                   so_goals_against TINYINT UNSIGNED,
                                   so_shots_against TINYINT UNSIGNED,
                                   so_saves TINYINT UNSIGNED,
                                   so_save_pct DECIMAL(6,5),
                                   goals SMALLINT UNSIGNED,
                                   assists SMALLINT UNSIGNED,
                                   points SMALLINT UNSIGNED,
                                   pims SMALLINT UNSIGNED',
                                  'win,
                                   loss,
                                   ot_loss,
                                   so_loss,
                                   goals_against,
                                   shots_against,
                                   shots_against - goals_against AS saves,
                                   IF(shots_against > 0, (shots_against - goals_against) / shots_against, 0) AS save_pct,
                                   IF(minutes_played > 0, (goals_against * 3600) / minutes_played, 0) AS goals_against_avg,
                                   shutout AS shutout,
                                   minutes_played AS minutes_played,
                                   en_goals_against AS en_goals_against,
                                   so_goals_against AS so_goals_against,
                                   so_shots_against AS so_shots_against,
                                   so_shots_against - so_goals_against AS so_saves,
                                   IF(so_shots_against > 0, (so_shots_against - so_goals_against) / so_shots_against, 0) AS so_save_pct,
                                   goals,
                                   assists,
                                   goals + assists AS points,
                                   pims');

  # Calcs
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'win', 'B.win > A.win');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'loss', 'B.loss > A.loss');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'ot_loss', 'B.ot_loss > A.ot_loss');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_loss', 'B.so_loss > A.so_loss');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against', 'B.goals_against < A.goals_against');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'shots_against', 'B.shots_against > A.shots_against');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'saves', 'B.saves > A.saves');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'save_pct', 'B.save_pct > A.save_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_avg', 'B.goals_against_avg < A.goals_against_avg');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'shutout', 'B.shutout > A.shutout');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'minutes_played', 'B.minutes_played > A.minutes_played');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'en_goals_against', 'B.en_goals_against > A.en_goals_against');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_goals_against', 'B.so_goals_against > A.so_goals_against');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_shots_against', 'B.so_shots_against > A.so_shots_against');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_saves', 'B.so_saves > A.so_saves');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'so_save_pct', 'B.so_save_pct > A.so_save_pct');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'goals', 'B.goals > A.goals');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'assists', 'B.assists > A.assists');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'points', 'B.points > A.points');
  CALL nhl_totals_teams_sort__calc(v_season, v_season_type, 'GOALIES', 'pims', 'B.pims > A.pims');

  # Order
  ALTER TABLE SPORTS_NHL_TEAMS_SEASON_GOALIES_SORTED ORDER BY season, season_type, team_id, stat_dir;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_teams_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table ENUM('SKATERS', 'SKATERS_ADVANCED', 'GOALIES'),
  v_cols VARCHAR(2176),
  v_calcs VARCHAR(2176)
)
    COMMENT 'Create temporary table for NHL team season stat sort'
BEGIN

  DECLARE v_inc_stat_dir TINYINT(1) UNSIGNED;
  SET v_inc_stat_dir := v_table IN ('SKATERS', 'GOALIES');

  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_TEAMS_SEASON_', v_table));
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_NHL_TEAMS_SEASON_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      team_id VARCHAR(3) NOT NULL,
      ', IF(v_inc_stat_dir, 'stat_dir ENUM(\'for\', \'against\') NOT NULL,', ''), '
      ', v_cols, ',
      PRIMARY KEY (season, season_type, team_id', IF(v_inc_stat_dir, ', stat_dir', ''), '),
      KEY by_stat_dit (season, season_type', IF(v_inc_stat_dir, ', stat_dir', ''), ')
    ) ENGINE = MEMORY
      SELECT season, season_type, team_id', IF(v_inc_stat_dir, ', stat_dir', ''), ', ', v_calcs, '
      FROM SPORTS_NHL_TEAMS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '";'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_NHL_TEAMS_SEASON_', v_table), CONCAT('tmp_SPORTS_NHL_TEAMS_SEASON_', v_table, '_cp'));

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_teams_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table ENUM('SKATERS', 'SKATERS_ADVANCED', 'GOALIES'),
  v_col VARCHAR(25),
  v_calc VARCHAR(1024)
)
    COMMENT 'Sort season totals for NHL teams'
BEGIN

  DECLARE v_inc_stat_dir TINYINT(1) UNSIGNED;
  SET v_inc_stat_dir := v_table IN ('SKATERS', 'GOALIES');

  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_NHL_TEAMS_SEASON_', v_table, '_SORTED (season, season_type, team_id', IF(v_inc_stat_dir, ', stat_dir', ''), ', `', v_col, '`)
      SELECT A.season, A.season_type, A.team_id', IF(v_inc_stat_dir, ', A.stat_dir', ''), ',
             COUNT(B.team_id) + 1 AS `', v_col, '`
      FROM tmp_SPORTS_NHL_TEAMS_SEASON_', v_table, ' AS A
      LEFT JOIN tmp_SPORTS_NHL_TEAMS_SEASON_', v_table, '_cp AS B
        ON (B.season = A.season
        AND B.season_type = A.season_type
        ', IF(v_inc_stat_dir, 'AND B.stat_dir = A.stat_dir', ''), '
        AND ', v_calc, ')
      WHERE A.season = ', v_season, '
      AND   A.season_type = "', v_season_type, '"
      GROUP BY A.season, A.season_type, A.team_id', IF(v_inc_stat_dir, ', A.stat_dir', ''), '
    ON DUPLICATE KEY UPDATE `', v_col, '` = VALUES(`', v_col, '`);'));

END $$

DELIMITER ;

