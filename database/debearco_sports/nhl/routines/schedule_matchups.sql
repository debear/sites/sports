#
# Instances of scheduled matchups between two teams
#
DROP PROCEDURE IF EXISTS `nhl_schedule_matchups`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_schedule_matchups`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Establish which teams meet when during the regular season'
BEGIN

  # Clear old version
  DELETE FROM SPORTS_NHL_SCHEDULE_MATCHUPS WHERE season = v_season AND game_type = 'regular';

  # Determine the teams
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT TEAM_DIV.team_id, DIVISION.parent_id AS conf_id, DIVISION.grouping_id AS div_id
    FROM SPORTS_NHL_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_NHL_GROUPINGS AS DIVISION
      ON (DIVISION.grouping_id = TEAM_DIV.grouping_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099);
  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # The possible matchups
  #  (Only storing a single version, with the teams ordered alphabetically)
  DROP TEMPORARY TABLE IF EXISTS tmp_teams_matchups;
  CREATE TEMPORARY TABLE tmp_teams_matchups (
    team_idA VARCHAR(3),
    team_idB VARCHAR(3),
    PRIMARY KEY (team_idA, team_idB)
  ) ENGINE = MEMORY
    SELECT tmp_teams.team_id AS team_idA,
           tmp_teams_cp.team_id AS team_idB
    FROM tmp_teams
    JOIN tmp_teams_cp
      ON (STRCMP(tmp_teams_cp.team_id, tmp_teams.team_id) = 1);

  # And their games
  INSERT INTO SPORTS_NHL_SCHEDULE_MATCHUPS (season, team_idA, team_idB, game_type, game_id)
    SELECT SPORTS_NHL_SCHEDULE.season, tmp_teams_matchups.team_idA, tmp_teams_matchups.team_idB,
           SPORTS_NHL_SCHEDULE.game_type, SPORTS_NHL_SCHEDULE.game_id
    FROM tmp_teams_matchups
    JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = v_season
      AND SPORTS_NHL_SCHEDULE.game_type = 'regular'
      AND ((SPORTS_NHL_SCHEDULE.home = tmp_teams_matchups.team_idA AND SPORTS_NHL_SCHEDULE.visitor = tmp_teams_matchups.team_idB)
        OR (SPORTS_NHL_SCHEDULE.home = tmp_teams_matchups.team_idB AND SPORTS_NHL_SCHEDULE.visitor = tmp_teams_matchups.team_idA)));

  # Re-order
  ALTER TABLE SPORTS_NHL_SCHEDULE_MATCHUPS ORDER BY season, team_idA, team_idB, game_type, game_id;

END $$

DELIMITER ;

