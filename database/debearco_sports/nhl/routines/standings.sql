##
## Calculate the standings
##
DROP PROCEDURE IF EXISTS `nhl_standings`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings`(
  v_recent_games TINYINT UNSIGNED
)
    COMMENT 'Calculate NHL standings'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_max_gp TINYINT UNSIGNED;

  # Setup our run
  CALL nhl_standings_setup(v_season, v_max_gp);

  # Basic standing details
  CALL nhl_standings_calcs(v_recent_games, v_max_gp);

  # Calculate positions
  CALL nhl_standings_pos('div');
  CALL nhl_standings_pos('conf');
  CALL nhl_standings_pos('league');

  # Status Codes
  CALL nhl_standings_codes(v_season);

  # Store in the final table...
  CALL nhl_standings_store();

  # Tidy the table up...
  CALL nhl_standings_tidy();

  # Copy in to the season archive table
  CALL nhl_standings_history();

END $$

DELIMITER ;
