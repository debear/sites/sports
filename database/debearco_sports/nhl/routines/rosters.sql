##
## Team rosters
##
DROP PROCEDURE IF EXISTS `nhl_rosters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_rosters`()
    COMMENT 'Calculate NHL team rosters'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_date DATE;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  # Cursor
  DECLARE cur_date CURSOR FOR
    SELECT the_date FROM tmp_date_list ORDER BY the_date;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  SELECT DISTINCT season INTO v_season FROM tmp_date_list;

  # Clear previous calculations
  DELETE SPORTS_NHL_TEAMS_ROSTERS.*
  FROM tmp_date_list
  JOIN SPORTS_NHL_TEAMS_ROSTERS
    ON (SPORTS_NHL_TEAMS_ROSTERS.season = tmp_date_list.season
    AND SPORTS_NHL_TEAMS_ROSTERS.the_date = tmp_date_list.the_date);

  # Loop through each of the days and process individually
  OPEN cur_date;
  loop_date: LOOP

    FETCH cur_date INTO v_date;
    IF v_done = 1 THEN LEAVE loop_date; END IF;

    CALL nhl_rosters_calc(v_season, v_date);

  END LOOP loop_date;
  CLOSE cur_date;

  # Final ordering
  ALTER TABLE SPORTS_NHL_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_rosters_calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_rosters_calc`(
  v_season SMALLINT UNSIGNED,
  v_date DATE
)
    COMMENT 'Calculate daily NHL team rosters'
BEGIN

  DECLARE v_copy_date DATE;

  # Create from the previous day
  SELECT MAX(the_date) INTO v_copy_date FROM SPORTS_NHL_TEAMS_ROSTERS WHERE season = v_season AND the_date < v_date;
  INSERT INTO SPORTS_NHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, capt_status, player_status)
    SELECT season, v_date, team_id, player_id, jersey, pos, capt_status, player_status
    FROM SPORTS_NHL_TEAMS_ROSTERS
    WHERE season = v_season
    AND   the_date = v_copy_date;

  # Calculate the appropriate game for each date
  DROP TEMPORARY TABLE IF EXISTS tmp_nhl_game;
  CREATE TEMPORARY TABLE tmp_nhl_game (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    game_type ENUM('regular','playoff'),
    game_id SMALLINT(5) UNSIGNED,
    PRIMARY KEY(season, the_date, team_id),
    INDEX active_roster (season, game_type, game_id, team_id)
  ) ENGINE = MEMORY
    SELECT v_season AS season,
           v_date AS the_date,
           SPORTS_NHL_TEAMS_GROUPINGS.team_id,
           SUBSTRING(MAX(CONCAT(SPORTS_NHL_SCHEDULE.game_date, ';', SPORTS_NHL_SCHEDULE.game_type)), 12) AS game_type,
           SUBSTRING(MAX(CONCAT(SPORTS_NHL_SCHEDULE.game_date, ';', SPORTS_NHL_SCHEDULE.game_id)), 12) AS game_id
    FROM SPORTS_NHL_TEAMS_GROUPINGS
    LEFT JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = v_season
      AND (SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_TEAMS_GROUPINGS.team_id
        OR SPORTS_NHL_SCHEDULE.visitor = SPORTS_NHL_TEAMS_GROUPINGS.team_id)
      AND SPORTS_NHL_SCHEDULE.game_date = v_date)
    WHERE v_season BETWEEN SPORTS_NHL_TEAMS_GROUPINGS.season_from AND IFNULL(SPORTS_NHL_TEAMS_GROUPINGS.season_to, 2099)
    GROUP BY SPORTS_NHL_TEAMS_GROUPINGS.team_id;

  # Unified active rosters (combination of those in the lineup and scratches)
  DROP TEMPORARY TABLE IF EXISTS tmp_nhl_active_rosters;
  CREATE TEMPORARY TABLE tmp_nhl_active_rosters (
    season SMALLINT UNSIGNED,
    the_date DATE,
    game_type ENUM('regular','playoff'),
    game_id SMALLINT(3) UNSIGNED,
    team_id VARCHAR(3),
    player_id SMALLINT(5) UNSIGNED,
    jersey TINYINT(3) UNSIGNED,
    pos VARCHAR(3) NULL,
    capt_status ENUM('C','A') NULL,
    player_status ENUM('active','ir','na') NULL,
    PRIMARY KEY (season, the_date, game_type, game_id, team_id, player_id),
    INDEX player (season, player_id),
    INDEX team (team_id, game_type, game_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           v_date AS the_date,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.jersey,
           SPORTS_NHL_GAME_LINEUP.pos,
           SPORTS_NHL_GAME_LINEUP.capt_status,
           'active' AS player_status
    FROM tmp_nhl_game
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_nhl_game.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_nhl_game.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_nhl_game.game_id
      AND SPORTS_NHL_GAME_LINEUP.team_id = tmp_nhl_game.team_id);
  # Then scratches
  INSERT IGNORE INTO tmp_nhl_active_rosters (season, the_date, game_type, game_id, team_id, player_id, jersey, pos, capt_status, player_status)
    SELECT SPORTS_NHL_GAME_SCRATCHES.season,
           v_date AS the_date,
           SPORTS_NHL_GAME_SCRATCHES.game_type,
           SPORTS_NHL_GAME_SCRATCHES.game_id,
           SPORTS_NHL_GAME_SCRATCHES.team_id,
           SPORTS_NHL_GAME_SCRATCHES.player_id,
           SPORTS_NHL_GAME_SCRATCHES.jersey,
           SPORTS_NHL_GAME_SCRATCHES.pos,
           NULL AS capt_status,
           'active' AS player_status
    FROM tmp_nhl_game
    JOIN SPORTS_NHL_GAME_SCRATCHES
      ON (SPORTS_NHL_GAME_SCRATCHES.season = tmp_nhl_game.season
      AND SPORTS_NHL_GAME_SCRATCHES.game_type = tmp_nhl_game.game_type
      AND SPORTS_NHL_GAME_SCRATCHES.game_id = tmp_nhl_game.game_id
      AND SPORTS_NHL_GAME_SCRATCHES.team_id = tmp_nhl_game.team_id);

  # And then update according to active rosters from the previous game
  INSERT INTO SPORTS_NHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, capt_status, player_status)
    SELECT SPORTS_NHL_TEAMS_ROSTERS.season,
           SPORTS_NHL_TEAMS_ROSTERS.the_date,
           IF(tmp_nhl_active_rosters.player_id IS NOT NULL,
              tmp_nhl_active_rosters.team_id,
              SPORTS_NHL_TEAMS_ROSTERS.team_id) AS team_id,
           SPORTS_NHL_TEAMS_ROSTERS.player_id,
           IF(tmp_nhl_active_rosters.player_id IS NOT NULL,
              tmp_nhl_active_rosters.jersey,
              SPORTS_NHL_TEAMS_ROSTERS.jersey) AS jersey,
           IF(tmp_nhl_active_rosters.player_id IS NOT NULL,
              tmp_nhl_active_rosters.pos,
              SPORTS_NHL_TEAMS_ROSTERS.pos) AS pos,
           IF(tmp_nhl_active_rosters.player_id IS NOT NULL,
              tmp_nhl_active_rosters.capt_status,
              IF(tmp_nhl_game.game_id IS NULL,
                 SPORTS_NHL_TEAMS_ROSTERS.capt_status, NULL)) AS capt_status,
           IF(tmp_nhl_active_rosters.player_id IS NOT NULL,
              'active',
              IF(tmp_nhl_game.game_id IS NULL,
              SPORTS_NHL_TEAMS_ROSTERS.player_status, 'na')) AS player_status
    FROM SPORTS_NHL_TEAMS_ROSTERS
    LEFT JOIN tmp_nhl_active_rosters
      ON (tmp_nhl_active_rosters.season = SPORTS_NHL_TEAMS_ROSTERS.season
      AND tmp_nhl_active_rosters.the_date = SPORTS_NHL_TEAMS_ROSTERS.the_date
      AND tmp_nhl_active_rosters.player_id = SPORTS_NHL_TEAMS_ROSTERS.player_id)
    LEFT JOIN tmp_nhl_game
      ON (tmp_nhl_game.season = SPORTS_NHL_TEAMS_ROSTERS.season
      AND tmp_nhl_game.the_date = SPORTS_NHL_TEAMS_ROSTERS.the_date
      AND tmp_nhl_game.team_id = SPORTS_NHL_TEAMS_ROSTERS.team_id)
    WHERE SPORTS_NHL_TEAMS_ROSTERS.season = v_season
    AND   SPORTS_NHL_TEAMS_ROSTERS.the_date = v_date
  ON DUPLICATE KEY UPDATE team_id       = VALUES(team_id),
                          jersey        = VALUES(jersey),
                          capt_status   = VALUES(capt_status),
                          player_status = VALUES(player_status);

  # Add any new players
  INSERT INTO SPORTS_NHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, capt_status, player_status)
    SELECT tmp_nhl_active_rosters.season,
           tmp_nhl_active_rosters.the_date,
           tmp_nhl_active_rosters.team_id,
           tmp_nhl_active_rosters.player_id,
           tmp_nhl_active_rosters.jersey,
           tmp_nhl_active_rosters.pos,
           tmp_nhl_active_rosters.capt_status,
           tmp_nhl_active_rosters.player_status
    FROM tmp_nhl_active_rosters
    LEFT JOIN SPORTS_NHL_TEAMS_ROSTERS
      ON (SPORTS_NHL_TEAMS_ROSTERS.season = tmp_nhl_active_rosters.season
      AND SPORTS_NHL_TEAMS_ROSTERS.the_date = tmp_nhl_active_rosters.the_date
      AND SPORTS_NHL_TEAMS_ROSTERS.player_id = tmp_nhl_active_rosters.player_id)
    WHERE tmp_nhl_active_rosters.season = v_season
    AND   tmp_nhl_active_rosters.the_date = v_date
    AND   SPORTS_NHL_TEAMS_ROSTERS.player_id IS NULL
  ON DUPLICATE KEY UPDATE team_id       = VALUES(team_id),
                          jersey        = VALUES(jersey),
                          capt_status   = VALUES(capt_status),
                          player_status = VALUES(player_status);

END $$

DELIMITER ;

