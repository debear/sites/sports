##
## Playoff Round Robin
##
DROP PROCEDURE IF EXISTS `nhl_playoff_round_robin`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_round_robin`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate NHL playoff round robin standings'
BEGIN

  # Basic Info
  DROP TEMPORARY TABLE IF EXISTS tmp_round_robin;
  CREATE TEMPORARY TABLE tmp_round_robin LIKE SPORTS_NHL_PLAYOFF_ROUND_ROBIN;
  ALTER TABLE tmp_round_robin ADD COLUMN is_tied TINYINT UNSIGNED AFTER seed;
  INSERT INTO tmp_round_robin
    SELECT ROUND_ROBIN.season, ROUND_ROBIN.conf_id, ROUND_ROBIN.team_id, ROUND_ROBIN.pos_conf, ROUND_ROBIN.pos_league,
           0 AS seed, 0 AS is_tied,
           SUM((SCHEDULE.home = ROUND_ROBIN.team_id AND SCHEDULE.home_score > SCHEDULE.visitor_score) OR (SCHEDULE.visitor = ROUND_ROBIN.team_id AND SCHEDULE.visitor_score > SCHEDULE.home_score)) AS wins,
           SUM((SCHEDULE.home = ROUND_ROBIN.team_id AND SCHEDULE.status = 'F' AND SCHEDULE.home_score < SCHEDULE.visitor_score) OR (SCHEDULE.visitor = ROUND_ROBIN.team_id AND SCHEDULE.status = 'F' AND SCHEDULE.visitor_score < SCHEDULE.home_score)) AS loss,
           SUM((SCHEDULE.home = ROUND_ROBIN.team_id AND SCHEDULE.status <> 'F' AND SCHEDULE.home_score < SCHEDULE.visitor_score) OR (SCHEDULE.visitor = ROUND_ROBIN.team_id AND SCHEDULE.status <> 'F' AND SCHEDULE.visitor_score < SCHEDULE.home_score)) AS ot_loss,
           0 AS pts,
           SUM(IF(SCHEDULE.home = ROUND_ROBIN.team_id, SCHEDULE.home_score, SCHEDULE.visitor_score)) AS goals_for,
           SUM(IF(SCHEDULE.home = ROUND_ROBIN.team_id, SCHEDULE.visitor_score, SCHEDULE.home_score)) AS goals_against
    FROM SPORTS_NHL_PLAYOFF_ROUND_ROBIN AS ROUND_ROBIN
    JOIN SPORTS_NHL_SCHEDULE AS SCHEDULE
      ON (SCHEDULE.season = ROUND_ROBIN.season
      AND SCHEDULE.game_type = 'playoff'
      AND SCHEDULE.game_id < 20
      AND ROUND_ROBIN.team_id IN (SCHEDULE.home, SCHEDULE.visitor)
      AND SCHEDULE.status IS NOT NULL)
    WHERE ROUND_ROBIN.season = v_season
    GROUP BY ROUND_ROBIN.season, ROUND_ROBIN.conf_id, ROUND_ROBIN.team_id;
  UPDATE tmp_round_robin SET pts = (2 * wins) + ot_loss;

  # Rank
  CALL _duplicate_tmp_table('tmp_round_robin', 'tmp_round_robin_cpA');
  CALL _duplicate_tmp_table('tmp_round_robin', 'tmp_round_robin_cpB');
  INSERT INTO tmp_round_robin (season, conf_id, team_id, pos_conf, pos_league, seed, is_tied, wins, loss, ot_loss, pts, goals_for, goals_against)
    SELECT cpA.season, cpA.conf_id, cpA.team_id,
           cpA.pos_conf, cpA.pos_league,
           COUNT(DISTINCT IF(cpB.pts > cpA.pts, cpB.team_id, NULL)) + 1 AS seed,
           COUNT(DISTINCT IF(cpB.pts = cpA.pts, cpB.team_id, NULL)) > 0 AS is_tied,
           cpA.wins, cpA.loss, cpA.ot_loss, cpA.pts, cpA.goals_for, cpA.goals_against
    FROM tmp_round_robin_cpA AS cpA
    LEFT JOIN tmp_round_robin_cpB AS cpB
      ON (cpB.season = cpA.season
      AND cpB.conf_id = cpA.conf_id
      AND cpB.team_id <> cpA.team_id
      AND cpB.pts >= cpA.pts)
    GROUP BY cpA.season, cpA.conf_id, cpA.team_id
  ON DUPLICATE KEY UPDATE seed = VALUES(seed), is_tied = VALUES(is_tied);

  # Break Ties
  CALL _duplicate_tmp_table('tmp_round_robin', 'tmp_round_robin_cpA');
  CALL _duplicate_tmp_table('tmp_round_robin', 'tmp_round_robin_cpB');
  INSERT INTO tmp_round_robin (season, conf_id, team_id, pos_conf, pos_league, seed, is_tied, wins, loss, ot_loss, pts, goals_for, goals_against)
    SELECT cpA.season, cpA.conf_id, cpA.team_id,
           cpA.pos_conf, cpA.pos_league,
           cpA.seed + COUNT(DISTINCT cpB.team_id) AS seed, 0 AS is_tied,
           cpA.wins, cpA.loss, cpA.ot_loss, cpA.pts, cpA.goals_for, cpA.goals_against
    FROM tmp_round_robin_cpA AS cpA
    LEFT JOIN tmp_round_robin_cpB AS cpB
      ON (cpB.season = cpA.season
      AND cpB.conf_id = cpA.conf_id
      AND cpB.seed = cpA.seed
      AND cpB.pos_league < cpA.pos_league)
    WHERE cpA.is_tied = 1
    GROUP BY cpA.season, cpA.conf_id, cpA.team_id
  ON DUPLICATE KEY UPDATE seed = VALUES(seed);

  # Add back to the main table
  INSERT INTO SPORTS_NHL_PLAYOFF_ROUND_ROBIN (season, conf_id, team_id, pos_conf, pos_league, seed, wins, loss, ot_loss, pts, goals_for, goals_against)
    SELECT season, conf_id, team_id, pos_conf, pos_league, seed, wins, loss, ot_loss, pts, goals_for, goals_against
    FROM tmp_round_robin
  ON DUPLICATE KEY UPDATE seed = VALUES(seed),
                          wins = VALUES(wins),
                          loss = VALUES(loss),
                          ot_loss = VALUES(ot_loss),
                          pts = VALUES(pts),
                          goals_for = VALUES(goals_for),
                          goals_against = VALUES(goals_against);

  ALTER TABLE SPORTS_NHL_PLAYOFF_ROUND_ROBIN ORDER BY season, conf_id, team_id;

END $$

DELIMITER ;
