##
## Player season totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_season_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season_skaters`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for NHL skaters'
BEGIN

  ##
  ## Combine daily figures into a single season
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_SEASON_SKATERS (season, season_type, player_id, team_id, gp, starts, goals, assists, plus_minus, pims, pens_taken, pens_drawn, pp_goals, pp_assists, sh_goals, sh_assists, gw_goals, shots, missed_shots, blocked_shots, shots_blocked, hits, times_hit, fo_wins, fo_loss, so_goals, so_shots, giveaways, takeaways, atoi)
    SELECT season,
           game_type AS season_type,
           player_id,
           team_id,
           SUM(IFNULL(gp, 0)) AS gp,
           SUM(IFNULL(start, 0)) AS starts,
           SUM(IFNULL(goals, 0)) AS goals,
           SUM(IFNULL(assists, 0)) AS assists,
           SUM(IFNULL(plus_minus, 0)) AS plus_minus,
           SUM(IFNULL(pims, 0)) AS pims,
           SUM(IFNULL(pens_taken, 0)) AS pens_taken,
           SUM(IFNULL(pens_drawn, 0)) AS pens_drawn,
           SUM(IFNULL(pp_goals, 0)) AS pp_goals,
           SUM(IFNULL(pp_assists, 0)) AS pp_assists,
           SUM(IFNULL(sh_goals, 0)) AS sh_goals,
           SUM(IFNULL(sh_assists, 0)) AS sh_assists,
           SUM(IFNULL(gw_goals, 0)) AS gw_goals,
           SUM(IFNULL(shots, 0)) AS shots,
           SUM(IFNULL(missed_shots, 0)) AS missed_shots,
           SUM(IFNULL(blocked_shots, 0)) AS blocked_shots,
           SUM(IFNULL(shots_blocked, 0)) AS shots_blocked,
           SUM(IFNULL(hits, 0)) AS hits,
           SUM(IFNULL(times_hit, 0)) AS times_hit,
           SUM(IFNULL(fo_wins, 0)) AS fo_wins,
           SUM(IFNULL(fo_loss, 0)) AS fo_loss,
           SUM(IFNULL(so_goals, 0)) AS so_goals,
           SUM(IFNULL(so_shots, 0)) AS so_shots,
           SUM(IFNULL(giveaways, 0)) AS giveaways,
           SUM(IFNULL(takeaways, 0)) AS takeaways,
           SEC_TO_TIME(AVG(TIME_TO_SEC(toi))) AS atoi
    FROM SPORTS_NHL_PLAYERS_GAME_SKATERS
    WHERE season = v_season
    AND   game_type = v_game_type
    GROUP BY season, game_type, player_id, team_id
    ORDER BY player_id, season, MIN(game_id)
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          starts = VALUES(starts),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          plus_minus= VALUES(plus_minus),
                          pims = VALUES(pims),
                          pens_taken = VALUES(pens_taken),
                          pens_drawn = VALUES(pens_drawn),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          gw_goals = VALUES(gw_goals),
                          shots = VALUES(shots),
                          missed_shots = VALUES(missed_shots),
                          blocked_shots = VALUES(blocked_shots),
                          shots_blocked = VALUES(shots_blocked),
                          hits = VALUES(hits),
                          times_hit = VALUES(times_hit),
                          fo_wins= VALUES(fo_wins),
                          fo_loss = VALUES(fo_loss),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots),
                          giveaways = VALUES(giveaways),
                          takeaways = VALUES(takeaways),
                          atoi = VALUES(atoi);

END $$

DELIMITER ;

