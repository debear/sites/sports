##
## Player season totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_season`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for NHL players'
BEGIN

  CALL `nhl_totals_players_season_skaters`(v_season, v_game_type);
  CALL `nhl_totals_players_season_goalies`(v_season, v_game_type);
  CALL `nhl_totals_players_season_heatzones`(v_season, v_game_type);
  CALL `nhl_totals_players_season_advanced`(v_season, v_game_type);

END $$

DELIMITER ;

##
## Player season total table maintenance
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_season_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season_order`()
    COMMENT 'Order the NHL player total tables'
BEGIN

  ALTER TABLE SPORTS_NHL_PLAYERS_SEASON_SKATERS ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED ORDER BY season, season_type, player_id, team_id;
  ALTER TABLE SPORTS_NHL_PLAYERS_SEASON_GOALIES ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

