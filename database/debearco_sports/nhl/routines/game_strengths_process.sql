##
## Determine the per-second on-ice numbers
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths_process`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Determine the on-ice numbers for NHL game strength calcs'
BEGIN

  DECLARE v_periods_count TINYINT UNSIGNED DEFAULT 0;
  DECLARE v_periods_num  TINYINT UNSIGNED;
  DECLARE v_num_invalid SMALLINT UNSIGNED DEFAULT 1; # To get us in to the loop later
  DECLARE v_game_home CHAR(3);
  DECLARE v_game_visitor CHAR(3);

  CALL _duplicate_tmp_table('tmp_times', 'tmp_times_cp');

  # Determine who the home/visitor teams are
  SELECT `home`, `visitor`
         INTO v_game_home, v_game_visitor
  FROM `SPORTS_NHL_SCHEDULE`
  WHERE `season` = v_season
  AND   `game_type` = v_game_type
  AND   `game_id` = v_game_id;

  # First the home team
  INSERT INTO `tmp_times` (`period`, `time_num`, `time_fmt`, `home_skaters`, `home_goalie`)
    SELECT `tmp_times_cp`.`period`,
           `tmp_times_cp`.`time_num`,
           `tmp_times_cp`.`time_fmt`,
           IF(`tmp_faceoffs`.`event_id` IS NOT NULL,
              `tmp_faceoffs`.`home_skaters`,
              SUM(1 - `tmp_onice`.`is_goalie`)
                - IF(`tmp_times_cp`.`is_period_end`, 0,
                    ((SUM(`tmp_onice`.`shift_start` = `tmp_times_cp`.`time_fmt`)
                        + SUM(`tmp_onice`.`shift_end` = `tmp_times_cp`.`time_fmt`)) / 2))) AS `home_skaters`,
           IF(`tmp_faceoffs`.`event_id` IS NOT NULL,
              `tmp_faceoffs`.`home_goalie`,
              SUM(`tmp_onice`.`is_goalie`)
                - IF(`tmp_times_cp`.`is_period_end`, 0,
                    (SUM(IF(`tmp_onice`.`is_goalie` = 1, `tmp_onice`.`shift_start` = `tmp_times_cp`.`time_fmt`, 0)) / 2))) AS `home_goalie`
    FROM `tmp_times_cp`
    LEFT JOIN `tmp_faceoffs`
      ON (`tmp_faceoffs`.`period` = `tmp_times_cp`.`period`
      AND `tmp_faceoffs`.`event_time` = `tmp_times_cp`.`time_fmt`)
    JOIN `tmp_onice`
      ON (`tmp_onice`.`team_id` = v_game_home
      AND `tmp_onice`.`period` = `tmp_times_cp`.`period`
      AND `tmp_times_cp`.`time_fmt` BETWEEN `tmp_onice`.`shift_end` AND `tmp_onice`.`shift_start`)
    GROUP BY `tmp_times_cp`.`period`, `tmp_times_cp`.`time_num`
  ON DUPLICATE KEY UPDATE `home_skaters` = VALUES (`home_skaters`),
                          `home_goalie` = VALUES (`home_goalie`);

  # Then the visitors
  INSERT INTO `tmp_times` (`period`, `time_num`, `time_fmt`, `visitor_skaters`, `visitor_goalie`)
    SELECT `tmp_times_cp`.`period`,
           `tmp_times_cp`.`time_num`,
           `tmp_times_cp`.`time_fmt`,
           IF(`tmp_faceoffs`.`event_id` IS NOT NULL,
              `tmp_faceoffs`.`visitor_skaters`,
              SUM(1 - `tmp_onice`.`is_goalie`)
                - IF(`tmp_times_cp`.`is_period_end`, 0,
                    ((SUM(`tmp_onice`.`shift_start` = `tmp_times_cp`.`time_fmt`)
                        + SUM(`tmp_onice`.`shift_end` = `tmp_times_cp`.`time_fmt`)) / 2))) AS `visitor_skaters`,
           IF(`tmp_faceoffs`.`event_id` IS NOT NULL,
              `tmp_faceoffs`.`visitor_goalie`,
              SUM(`tmp_onice`.`is_goalie`)
                - IF(`tmp_times_cp`.`is_period_end`, 0,
                    (SUM(IF(`tmp_onice`.`is_goalie` = 1, `tmp_onice`.`shift_start` = `tmp_times_cp`.`time_fmt`, 0)) / 2))) AS `visitor_goalie`
    FROM `tmp_times_cp`
    LEFT JOIN `tmp_faceoffs`
      ON (`tmp_faceoffs`.`period` = `tmp_times_cp`.`period`
      AND `tmp_faceoffs`.`event_time` = `tmp_times_cp`.`time_fmt`)
    JOIN `tmp_onice`
      ON (`tmp_onice`.`team_id` = v_game_visitor
      AND `tmp_onice`.`period` = `tmp_times_cp`.`period`
      AND `tmp_times_cp`.`time_fmt` BETWEEN `tmp_onice`.`shift_end` AND `tmp_onice`.`shift_start`)
    GROUP BY `tmp_times_cp`.`period`, `tmp_times_cp`.`time_num`
  ON DUPLICATE KEY UPDATE `visitor_skaters` = VALUES (`visitor_skaters`),
                          `visitor_goalie` = VALUES (`visitor_goalie`);

  # Data Fix: We sometimes have the wrong numbers of on-ice players, so assume continuation of the last (valid) row
  ALTER TABLE `tmp_times`
    ADD COLUMN `home_valid` TINYINT UNSIGNED DEFAULT 0,
    ADD COLUMN `visitor_valid` TINYINT UNSIGNED DEFAULT 0;

  # Run up to once per-period (or until no more fixes required)
  SELECT CASE `status`
           WHEN 'OT' THEN 4
           WHEN 'SO' THEN 4
           WHEN '2OT' THEN 5
           WHEN '3OT' THEN 6
           WHEN '4OT' THEN 7
           ELSE 3
         END INTO v_periods_num
  FROM `SPORTS_NHL_SCHEDULE`
  WHERE `season` = v_season
  AND   `game_type` = v_game_type
  AND   `game_id` = v_game_id;

  WHILE v_num_invalid AND (v_periods_count <= v_periods_num) DO
    SELECT SUM((1 - `home_valid`) + (1 -`visitor_valid`)) INTO v_num_invalid FROM `tmp_times`;
    IF v_num_invalid THEN
      CALL nhl_game_onice_strengths_fix();
    END IF;

    SET v_periods_count = v_periods_count + 1;
  END WHILE;

  ALTER TABLE `tmp_times`
    DROP COLUMN `home_valid`,
    DROP COLUMN `visitor_valid`;
END $$

DELIMITER ;

##
## Determine per-game periods of team strengths
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths_fix`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths_fix`()
    COMMENT 'Fix data issues with the on-ice strength periods for NHL games'
BEGIN

  UPDATE `tmp_times`
  SET `home_valid` = (`home_skaters` + `home_goalie`) >= 4,
      `visitor_valid` = (`visitor_skaters` + `visitor_goalie`) >= 4;

  # Start of game
  UPDATE `tmp_times`
  SET `home_skaters` = 5, `home_goalie` = 1, `home_valid` = 1
  WHERE `period` = 1
  AND   `time_num` = 1200
  AND   `home_valid` = 0;
  UPDATE `tmp_times`
  SET `visitor_skaters` = 5, `visitor_goalie` = 1, `visitor_valid` = 1
  WHERE `period` = 1
  AND   `time_num` = 1200
  AND   `visitor_valid` = 0;

  # Start of period
  CALL _duplicate_tmp_table('tmp_times', 'tmp_times_cpA');
  CALL _duplicate_tmp_table('tmp_times', 'tmp_times_cpB');
  INSERT INTO `tmp_times` (`period`, `time_num`, `home_skaters`, `home_goalie`)
    SELECT `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`,
           `tmp_times_cpB`.`home_skaters`, `tmp_times_cpB`.`home_goalie`
    FROM `tmp_times_cpA`
    JOIN `tmp_times_cpB`
      ON (`tmp_times_cpB`.`period` = (`tmp_times_cpA`.`period` - 1)
      AND `tmp_times_cpB`.`home_valid` = 1
      AND `tmp_times_cpB`.`time_num` = 0)
    WHERE `tmp_times_cpA`.`home_valid` = 0
    GROUP BY `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`
  ON DUPLICATE KEY UPDATE `home_skaters` = VALUES(`home_skaters`),
                          `home_goalie` = VALUES(`home_goalie`);

  INSERT INTO `tmp_times` (`period`, `time_num`, `visitor_skaters`, `visitor_goalie`)
    SELECT `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`,
           `tmp_times_cpB`.`visitor_skaters`, `tmp_times_cpB`.`visitor_goalie`
    FROM `tmp_times_cpA`
    JOIN `tmp_times_cpB`
      ON (`tmp_times_cpB`.`period` = (`tmp_times_cpA`.`period` - 1)
      AND `tmp_times_cpB`.`visitor_valid` = 1
      AND `tmp_times_cpB`.`time_num` = 0)
    WHERE `tmp_times_cpA`.`visitor_valid` = 0
    GROUP BY `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`
  ON DUPLICATE KEY UPDATE `visitor_skaters` = VALUES(`visitor_skaters`),
                          `visitor_goalie` = VALUES(`visitor_goalie`);

  UPDATE `tmp_times`
  SET `home_valid` = (`home_skaters` + `home_goalie`) >= 4,
      `visitor_valid` = (`visitor_skaters` + `visitor_goalie`) >= 4;

  # Same period
  CALL _duplicate_tmp_table('tmp_times', 'tmp_times_cpA');
  CALL _duplicate_tmp_table('tmp_times', 'tmp_times_cpB');
  INSERT INTO `tmp_times` (`period`, `time_num`, `home_skaters`, `home_goalie`)
    SELECT `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`,
           `tmp_times_cpB`.`home_skaters`, `tmp_times_cpB`.`home_goalie`
    FROM `tmp_times_cpA`
    JOIN `tmp_times_cpB`
      ON (`tmp_times_cpB`.`period` = `tmp_times_cpA`.`period`
      AND `tmp_times_cpB`.`home_valid` = 1
      AND `tmp_times_cpB`.`time_num` > `tmp_times_cpA`.`time_num`)
    WHERE `tmp_times_cpA`.`home_valid` = 0
    GROUP BY `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`
  ON DUPLICATE KEY UPDATE `home_skaters` = VALUES(`home_skaters`),
                          `home_goalie` = VALUES(`home_goalie`);

  INSERT INTO `tmp_times` (`period`, `time_num`, `visitor_skaters`, `visitor_goalie`)
    SELECT `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`,
           `tmp_times_cpB`.`visitor_skaters`, `tmp_times_cpB`.`visitor_goalie`
    FROM `tmp_times_cpA`
    JOIN `tmp_times_cpB`
      ON (`tmp_times_cpB`.`period` = `tmp_times_cpA`.`period`
      AND `tmp_times_cpB`.`visitor_valid` = 1
      AND `tmp_times_cpB`.`time_num` > `tmp_times_cpA`.`time_num`)
    WHERE `tmp_times_cpA`.`visitor_valid` = 0
    GROUP BY `tmp_times_cpA`.`period`, `tmp_times_cpA`.`time_num`
  ON DUPLICATE KEY UPDATE `visitor_skaters` = VALUES(`visitor_skaters`),
                          `visitor_goalie` = VALUES(`visitor_goalie`);

  UPDATE `tmp_times`
  SET `home_valid` = (`home_skaters` + `home_goalie`) >= 4,
      `visitor_valid` = (`visitor_skaters` + `visitor_goalie`) >= 4;

END $$

DELIMITER ;

##
## Determine per-game periods of team strengths
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths_group`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths_group`()
    COMMENT 'Group the on-ice strength periods for NHL games'
BEGIN

  CALL _duplicate_tmp_table('tmp_times', 'tmp_times_cp');
  DROP TEMPORARY TABLE IF EXISTS `tmp_times_grouped`;
  CREATE TEMPORARY TABLE `tmp_times_grouped` LIKE `tmp_times`;
  ALTER TABLE `tmp_times_grouped`
    ADD COLUMN `boundary_start` TINYINT UNSIGNED,
    ADD COLUMN `boundary_end` TINYINT UNSIGNED;
  INSERT INTO `tmp_times_grouped`
    SELECT *, 0 AS `boundary_start`, 0 AS `boundary_end`
    FROM `tmp_times`;

  # Compare each timestamp with the previous second to find a difference in strength
  INSERT INTO `tmp_times_grouped` (`period`, `time_num`, `boundary_start`)
    SELECT `A`.`period`, `A`.`time_num`,
           `A`.`home_skaters` <> `B`.`home_skaters`
            OR `A`.`home_goalie` <> `B`.`home_goalie`
            OR `A`.`visitor_skaters` <> `B`.`visitor_skaters`
            OR `A`.`visitor_goalie` <> `B`.`visitor_goalie` AS `boundary_start`
    FROM `tmp_times` AS `A`
    JOIN `tmp_times_cp` AS `B`
      ON (`A`.`period` = `B`.`period`
      AND `A`.`time_num` = `B`.`time_num` - 1)
    WHERE `B`.`time_num` > 1
  ON DUPLICATE KEY UPDATE `boundary_start` = IF(`boundary_start` = 0, VALUES(`boundary_start`), `boundary_start`);

  INSERT INTO `tmp_times_grouped` (`period`, `time_num`, `boundary_end`)
    SELECT `A`.`period`, `A`.`time_num`,
           `A`.`home_skaters` <> `B`.`home_skaters`
            OR `A`.`home_goalie` <> `B`.`home_goalie`
            OR `A`.`visitor_skaters` <> `B`.`visitor_skaters`
            OR `A`.`visitor_goalie` <> `B`.`visitor_goalie` AS `boundary_end`
    FROM `tmp_times` AS `A`
    JOIN `tmp_times_cp` AS `B`
      ON (`A`.`period` = `B`.`period`
      AND `A`.`time_num` = `B`.`time_num` + 1)
    WHERE `A`.`time_num` > 1
  ON DUPLICATE KEY UPDATE `boundary_end` = IF(`boundary_end` = 0, VALUES(`boundary_end`), `boundary_end`);

  # Period Start / End are automatic boundaries
  INSERT INTO `tmp_times_grouped` (`period`, `time_num`, `boundary_start`)
    SELECT `period`, MAX(`time_num`) AS `time_num`, 1 AS `boundary_start`
    FROM `tmp_times`
    GROUP BY `period`
  ON DUPLICATE KEY UPDATE `boundary_start` = VALUES(`boundary_start`);

  INSERT INTO `tmp_times_grouped` (`period`, `time_num`, `boundary_end`)
    SELECT `period`, MIN(`time_num`) AS `time_num`, 1 AS `boundary_end`
    FROM `tmp_times`
    GROUP BY `period`
  ON DUPLICATE KEY UPDATE `boundary_end` = VALUES(`boundary_end`);

END $$

DELIMITER ;
