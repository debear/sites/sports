##
## Base NHL standings
##
DROP PROCEDURE IF EXISTS `nhl_standings_calcs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_calcs`(
  v_recent_games TINYINT UNSIGNED,
  v_max_gp TINYINT UNSIGNED
)
    COMMENT 'NHL standings calculations'
BEGIN

  # Standings columns
  CALL nhl_standings_calcs_main(v_max_gp);
  CALL nhl_standings_calcs_shootout(v_max_gp);
  CALL nhl_standings_calcs_div();
  CALL nhl_standings_calcs_recent(v_recent_games);
  CALL nhl_standings_calcs_misc();

  # Tie-break facilitators
  CALL nhl_standings_calcs_tiebreak();

END $$

DELIMITER ;
