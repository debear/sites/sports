##
## Player daily game totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_players`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players`()
    COMMENT 'Calculate daily game totals for NHL players'
BEGIN

  CALL `nhl_totals_players_daily__prepare`();
  CALL `nhl_totals_players_daily_skaters`();
  CALL `nhl_totals_players_daily_goalies`();
  CALL `nhl_totals_daily_advanced`();

END $$

DELIMITER ;

##
##
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_daily__prepare`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_daily__prepare`()
    COMMENT 'Calcs to run before processing daily NHL player game totals'
BEGIN

  # Players who played in our games
  DROP TEMPORARY TABLE IF EXISTS tmp_game_players;
  CREATE TEMPORARY TABLE tmp_game_players (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id VARCHAR(3),
    jersey TINYINT UNSIGNED,
    toi TIME,
    shifts TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, game_id, team_id, jersey)
  ) ENGINE = MEMORY
    SELECT SPORTS_NHL_GAME_TOI.season, SPORTS_NHL_GAME_TOI.game_type, SPORTS_NHL_GAME_TOI.game_id,
           SPORTS_NHL_GAME_TOI.team_id, SPORTS_NHL_GAME_TOI.jersey,
           SEC_TO_TIME(SUM(TIME_TO_SEC(SPORTS_NHL_GAME_TOI.shift_start) - TIME_TO_SEC(SPORTS_NHL_GAME_TOI.shift_end))) AS toi,
           COUNT(DISTINCT SPORTS_NHL_GAME_TOI.shift_num) AS shifts
    FROM tmp_game_list
    JOIN SPORTS_NHL_GAME_TOI
      ON (SPORTS_NHL_GAME_TOI.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_TOI.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_TOI.game_id = tmp_game_list.game_id)
    GROUP BY SPORTS_NHL_GAME_TOI.season, SPORTS_NHL_GAME_TOI.game_type, SPORTS_NHL_GAME_TOI.game_id,
             SPORTS_NHL_GAME_TOI.team_id, SPORTS_NHL_GAME_TOI.jersey;
  CALL _duplicate_tmp_table('tmp_game_players', 'tmp_game_players_cp');

END $$

DELIMITER ;

##
## Player season total table maintenance
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_order`()
    COMMENT 'Order the NHL player daily game total tables'
BEGIN

  ALTER TABLE SPORTS_NHL_PLAYERS_GAME_SKATERS ORDER BY season, player_id, game_type, game_id;
  ALTER TABLE SPORTS_NHL_PLAYERS_GAME_GOALIES ORDER BY season, player_id, game_type, team_id;

  # Advanced stats (includes team calcs, given build process)
  ALTER TABLE SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED ORDER BY season, player_id, game_type, game_id;
  ALTER TABLE SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED ORDER BY season, team_id, game_type, game_id;

END $$

DELIMITER ;

