##
## Determine per-game periods of team strengths
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths_identify`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths_identify`()
    COMMENT 'Group the on-ice strength periods for NHL games'
BEGIN

  # Determine start/end groups
  DELETE FROM `tmp_times_grouped` WHERE `boundary_start` = 0 AND `boundary_end` = 0;
  CALL _duplicate_tmp_table('tmp_times_grouped', 'tmp_times_grouped_cp');

  DROP TEMPORARY TABLE IF EXISTS `tmp_times_groups`;
  CREATE TEMPORARY TABLE `tmp_times_groups` LIKE `tmp_times_grouped`;
  ALTER TABLE `tmp_times_groups` ADD COLUMN `boundary_num` SMALLINT UNSIGNED;

  INSERT INTO `tmp_times_groups`
    SELECT `tmp_times_grouped`.*,
           COUNT(`tmp_times_grouped_cp`.`period`) + 1 AS `boundary_num`
    FROM `tmp_times_grouped`
    LEFT JOIN `tmp_times_grouped_cp`
      ON (`tmp_times_grouped_cp`.`period` < `tmp_times_grouped`.`period`
       OR (`tmp_times_grouped_cp`.`period` = `tmp_times_grouped`.`period`
       AND `tmp_times_grouped_cp`.`time_num` < `tmp_times_grouped`.`time_num`))
    GROUP BY `tmp_times_grouped`.`period`, `tmp_times_grouped`.`time_num`
  ON DUPLICATE KEY UPDATE `boundary_num` = VALUES(`boundary_num`);

  # Then tie together
  CALL _duplicate_tmp_table('tmp_times_groups', 'tmp_times_groups_cpA');
  CALL _duplicate_tmp_table('tmp_times_groups', 'tmp_times_groups_cpB');
  ALTER TABLE `tmp_times_groups` ADD COLUMN `boundary_end_num` SMALLINT UNSIGNED;
  INSERT INTO `tmp_times_groups` (`period`, `time_num`, `boundary_end_num`)
    SELECT `tmp_times_groups_cpA`.`period`, `tmp_times_groups_cpA`.`time_num`,
           `tmp_times_groups_cpB`.`boundary_num` AS `boundary_end_num`
    FROM `tmp_times_groups_cpA`
    JOIN `tmp_times_groups_cpB`
      ON (`tmp_times_groups_cpB`.`period` = `tmp_times_groups_cpA`.`period`
      AND `tmp_times_groups_cpB`.`time_num` <= `tmp_times_groups_cpA`.`time_num`
      AND `tmp_times_groups_cpB`.`boundary_end` = 1)
    WHERE `tmp_times_groups_cpA`.`boundary_start` = 1
  ON DUPLICATE KEY UPDATE `boundary_end_num` = VALUES(`boundary_end_num`);

  # Determine sequential ID
  CALL _duplicate_tmp_table('tmp_times_groups', 'tmp_times_groups_cpA');
  CALL _duplicate_tmp_table('tmp_times_groups', 'tmp_times_groups_cpB');
  ALTER TABLE `tmp_times_groups` ADD COLUMN `instance_id` TINYINT UNSIGNED;
  INSERT INTO `tmp_times_groups`
    SELECT `tmp_times_groups_cpA`.*,
           COUNT(`tmp_times_groups_cpB`.`boundary_num`) + 1 AS `instance_id`
    FROM `tmp_times_groups_cpA`
    LEFT JOIN `tmp_times_groups_cpB`
      ON (`tmp_times_groups_cpB`.`boundary_start` = 1
      AND `tmp_times_groups_cpB`.`boundary_num` < `tmp_times_groups_cpA`.`boundary_num`)
    WHERE `tmp_times_groups_cpA`.`boundary_start` = 1
    GROUP BY `tmp_times_groups_cpA`.`period`, `tmp_times_groups_cpA`.`time_num`
  ON DUPLICATE KEY UPDATE `instance_id` = VALUES(`instance_id`);

  # Then the various strength options
  ALTER TABLE `tmp_times_groups`
    DROP COLUMN `is_period_end`,
    DROP COLUMN `boundary_start`,
    DROP COLUMN `boundary_end`,
    ADD COLUMN `home_num` TINYINT UNSIGNED AFTER `home_goalie`,
    ADD COLUMN `visitor_num` TINYINT UNSIGNED AFTER `visitor_goalie`,
    ADD COLUMN `team_id` ENUM('home','visitor'),
    ADD COLUMN `strength` ENUM('5-on-5','5-on-4','4-on-4','5-on-3','4-on-3','3-on-3'),
    ADD COLUMN `goalies` ENUM('home-visitor','home-empty','empty-visitor','empty-empty');
  UPDATE `tmp_times_groups`
  SET `home_num` = LEAST(`home_skaters` + `home_goalie`, 6),
      `visitor_num` = LEAST(`visitor_skaters` + `visitor_goalie`, 6);
  UPDATE `tmp_times_groups`
  SET `team_id` = IF(`home_num` > `visitor_num`, 'home',
                     IF(`home_num` < `visitor_num`, 'visitor', NULL)),
      `strength` = CONCAT((IF(`home_num` > `visitor_num`, `home_num`, `visitor_num`) - 1),
                          '-on-',
                          (IF(`home_num` <= `visitor_num`, `home_num`, `visitor_num`) - 1)),
      `goalies` = CONCAT(IF(`home_goalie`, 'home', 'empty'),
                         '-',
                         IF(`visitor_goalie`, 'visitor', 'empty'));

END $$

DELIMITER ;

##
## Store the per-game periods of team strengths
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths_store`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Store the on-ice strength periods for NHL games'
BEGIN

  DELETE FROM `SPORTS_NHL_GAME_STRENGTHS`
  WHERE `season` = v_season
  AND   `game_type` = v_game_type
  AND   `game_id` = v_game_id;

  CALL _duplicate_tmp_table('tmp_times_groups', 'tmp_times_groups_cp');
  INSERT INTO `SPORTS_NHL_GAME_STRENGTHS`
    SELECT v_season AS `season`, v_game_type AS `game_type`, v_game_id AS `game_id`,
           `START`.`instance_id`, `START`.`period`,
           `START`.`time_fmt` AS `time_start`,
           `END`.`time_fmt` AS `time_end`,
           `START`.`team_id`, `START`.`strength`, `START`.`goalies`
    FROM `tmp_times_groups` AS `START`
    JOIN `tmp_times_groups_cp` AS `END`
      ON (`END`.`boundary_num` = `START`.`boundary_end_num`)
    WHERE `START`.`instance_id` IS NOT NULL
    ORDER BY `START`.`instance_id`;

  ALTER TABLE `SPORTS_NHL_GAME_STRENGTHS` ORDER BY `season`, `game_type`, `game_id`;

END $$

DELIMITER ;
