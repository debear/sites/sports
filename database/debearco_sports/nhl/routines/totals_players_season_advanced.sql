##
## Player season advanced stat calcs
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_season_advanced`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season_advanced`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season game advanced stats for NHL players'
BEGIN

  ##
  ## Combine daily figures into a single season
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED (season, season_type, player_id, team_id,
      corsi_cf, corsi_ca, corsi_pct, corsi_off_cf, corsi_off_ca, corsi_off_pct, corsi_rel,
      fenwick_ff, fenwick_fa, fenwick_pct, fenwick_off_ff, fenwick_off_fa, fenwick_off_pct, fenwick_rel,
      pdo_sh_gf, pdo_sh_sog, pdo_sh, pdo_sv_sv, pdo_sv_sog, pdo_sv, pdo,
      zs_off_num, zs_off_pct, zs_def_num, zs_def_pct)
    SELECT season,
           game_type AS season_type,
           player_id,
           team_id,
           SUM(IFNULL(corsi_cf, 0)) AS corsi_cf,
           SUM(IFNULL(corsi_ca, 0)) AS corsi_ca,
           NULL AS corsi_pct,
           SUM(IFNULL(corsi_off_cf, 0)) AS corsi_off_cf,
           SUM(IFNULL(corsi_off_ca, 0)) AS corsi_off_ca,
           NULL AS corsi_off_pct,
           NULL AS corsi_rel,
           SUM(IFNULL(fenwick_ff, 0)) AS fenwick_ff,
           SUM(IFNULL(fenwick_fa, 0)) AS fenwick_fa,
           NULL AS fenwick_pct,
           SUM(IFNULL(fenwick_off_ff, 0)) AS fenwick_off_ff,
           SUM(IFNULL(fenwick_off_fa, 0)) AS fenwick_off_fa,
           NULL AS fenwick_off_pct,
           NULL AS fenwick_rel,
           SUM(IFNULL(pdo_sh_gf, 0)) AS pdo_sh_gf,
           SUM(IFNULL(pdo_sh_sog, 0)) AS pdo_sh_sog,
           NULL AS pdo_sh,
           SUM(IFNULL(pdo_sv_sv, 0)) AS pdo_sv_sv,
           SUM(IFNULL(pdo_sv_sog, 0)) AS pdo_sv_sog,
           NULL AS pdo_sv,
           NULL AS pdo,
           SUM(IFNULL(zs_off_num, 0)) AS zs_off_num,
           NULL AS zs_off_pct,
           SUM(IFNULL(zs_def_num, 0)) AS zs_def_num,
           NULL AS zs_def_pct
    FROM SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED
    WHERE season = v_season
    AND   game_type = v_game_type
    GROUP BY season, game_type, player_id, team_id
    ORDER BY player_id, season, MIN(game_id)
  ON DUPLICATE KEY UPDATE corsi_cf = VALUES(corsi_cf),
                          corsi_ca = VALUES(corsi_ca),
                          corsi_pct = VALUES(corsi_pct),
                          corsi_off_cf = VALUES(corsi_off_cf),
                          corsi_off_ca = VALUES(corsi_off_ca),
                          corsi_off_pct = VALUES(corsi_off_pct),
                          corsi_rel = VALUES(corsi_rel),
                          fenwick_ff = VALUES(fenwick_ff),
                          fenwick_fa = VALUES(fenwick_fa),
                          fenwick_pct = VALUES(fenwick_pct),
                          fenwick_off_ff = VALUES(fenwick_off_ff),
                          fenwick_off_fa = VALUES(fenwick_off_fa),
                          fenwick_off_pct = VALUES(fenwick_off_pct),
                          fenwick_rel = VALUES(fenwick_rel),
                          pdo_sh_gf = VALUES(pdo_sh_gf),
                          pdo_sh_sog = VALUES(pdo_sh_sog),
                          pdo_sh = VALUES(pdo_sh),
                          pdo_sv_sv = VALUES(pdo_sv_sv),
                          pdo_sv_sog = VALUES(pdo_sv_sog),
                          pdo_sv = VALUES(pdo_sv),
                          pdo = VALUES(pdo),
                          zs_off_num = VALUES(zs_off_num),
                          zs_off_pct = VALUES(zs_off_pct),
                          zs_def_num = VALUES(zs_def_num),
                          zs_def_pct = VALUES(zs_def_pct);

  # Then build the percentages / relatives
  UPDATE SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED
  SET corsi_pct = IF(corsi_cf + corsi_ca = 0, NULL,
        (100 * CAST(corsi_cf AS SIGNED)) / (CAST(corsi_cf AS SIGNED) + CAST(corsi_ca AS SIGNED))),
      corsi_off_pct = IF(corsi_off_cf + corsi_off_ca = 0, NULL,
        (100 * CAST(corsi_off_cf AS SIGNED)) / (CAST(corsi_off_cf AS SIGNED) + CAST(corsi_off_ca AS SIGNED))),
      corsi_rel = IF((corsi_cf + corsi_ca) = 0 OR (corsi_off_cf + corsi_off_ca) = 0, NULL,
        ((100 * CAST(corsi_cf AS SIGNED)) / (CAST(corsi_cf AS SIGNED) + CAST(corsi_ca AS SIGNED))) - ((100 * CAST(corsi_off_cf AS SIGNED)) / (CAST(corsi_off_cf AS SIGNED) + CAST(corsi_off_ca AS SIGNED)))),
      fenwick_pct = IF(fenwick_ff + fenwick_fa = 0, NULL,
        (100 * CAST(fenwick_ff AS SIGNED)) / (CAST(fenwick_ff AS SIGNED) + CAST(fenwick_fa AS SIGNED))),
      fenwick_off_pct = IF(fenwick_off_ff + fenwick_off_fa = 0, NULL,
        (100 * CAST(fenwick_off_ff AS SIGNED)) / (CAST(fenwick_off_ff AS SIGNED) + CAST(fenwick_off_fa AS SIGNED))),
      fenwick_rel = IF((fenwick_ff + fenwick_fa) = 0 OR (fenwick_off_ff + fenwick_off_fa) = 0, NULL,
        ((100 * CAST(fenwick_ff AS SIGNED)) / (CAST(fenwick_ff AS SIGNED) + CAST(fenwick_fa AS SIGNED))) - ((100 * CAST(fenwick_off_ff AS SIGNED)) / (CAST(fenwick_off_ff AS SIGNED) + CAST(fenwick_off_fa AS SIGNED)))),
      pdo_sh = IF(pdo_sh_sog = 0, NULL,
        (100 * CAST(pdo_sh_gf AS SIGNED)) / CAST(pdo_sh_sog AS SIGNED)),
      pdo_sv = IF(pdo_sv_sog = 0, NULL,
        (100 * CAST(pdo_sv_sv AS SIGNED)) / CAST(pdo_sv_sog AS SIGNED)),
      pdo = IF(pdo_sh_sog = 0 OR pdo_sv_sog = 0, NULL,
        ((100 * CAST(pdo_sh_gf AS SIGNED)) / CAST(pdo_sh_sog AS SIGNED)) + ((100 * CAST(pdo_sv_sv AS SIGNED)) / CAST(pdo_sv_sog AS SIGNED))),
      zs_off_pct = IF(zs_off_num + zs_def_num = 0, NULL,
        (100 * CAST(zs_off_num AS SIGNED)) / (CAST(zs_off_num AS SIGNED) + CAST(zs_def_num AS SIGNED))),
      zs_def_pct = IF(zs_off_num + zs_def_num = 0, NULL,
        (100 * CAST(zs_def_num AS SIGNED)) / (CAST(zs_off_num AS SIGNED) + CAST(zs_def_num AS SIGNED)))
  WHERE season = v_season
  AND   season_type = v_game_type;

END $$

DELIMITER ;
