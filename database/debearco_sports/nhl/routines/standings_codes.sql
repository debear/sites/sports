##
## Status codes
##
DROP PROCEDURE IF EXISTS `nhl_standings_codes`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_codes`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Calculate status codes for NHL standings'
BEGIN

  #
  # Run some pre-processing calcs
  #
  CALL nhl_standings_codes_setup(v_season);

  #
  # Individual processes
  #
  # Playoff Spot
  # Up to the end of 2012/13, Conference format, Top 8 in conference
  IF v_season <= 2011 THEN
    CALL nhl_standings_codes_process('x', 'conf', 8);

  # In 2020/21, purely Divisional
  ELSEIF v_season = 2020 THEN
    CALL nhl_standings_codes_process('x', 'div', 4);

  # From 2013/14 onwards, Division format, Top 3 in division, plus next two wildcards
  ELSE
    CALL nhl_standings_codes_process('w', 'conf', 8);
    CALL nhl_standings_codes_process('x', 'div', 3);

  END IF;

  # Division Winner
  CALL nhl_standings_codes_process('y', 'div', 1);

  # Home-Ice
  CALL nhl_standings_codes_process('z', 'conf', 1);

  # Presidents Trophy
  CALL nhl_standings_codes_process('*', 'league', 1);

END $$

DELIMITER ;
