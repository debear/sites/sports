##
## Team season totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_teams_season`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season totals for NHL teams'
BEGIN

  CALL `nhl_totals_teams_season_skaters`(v_season, v_game_type);
  CALL `nhl_totals_teams_season_goalies`(v_season, v_game_type);
  CALL `nhl_totals_teams_season_advanced`(v_season, v_game_type);

END $$

DELIMITER ;

##
## Team season totals table maintenance
##
DROP PROCEDURE IF EXISTS `nhl_totals_teams_season_order`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season_order`()
    COMMENT 'Order the NHL team total tables'
BEGIN

  ALTER TABLE SPORTS_NHL_TEAMS_SEASON_SKATERS ORDER BY season, season_type, team_id, stat_dir;
  ALTER TABLE SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED ORDER BY season, season_type, team_id;
  ALTER TABLE SPORTS_NHL_TEAMS_SEASON_GOALIES ORDER BY season, season_type, team_id, stat_dir;

END $$

DELIMITER ;

