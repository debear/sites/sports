##
## Determine positions for a particular position type
##
DROP PROCEDURE IF EXISTS `nhl_standings_pos`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_pos`(
  v_col VARCHAR(6)
)
    COMMENT 'Calculate NHL standing positions'
BEGIN

  #
  # Stage 0: Simplify our query by generalising our position and join cols
  #
  CALL _exec(CONCAT('UPDATE tmp_standings SET _tb_join = IF("', v_col, '" = "league", _tb_league_id, IF("', v_col, '" = "conf", _tb_conf_id, _tb_div_id));'));
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

  #
  # Stage 1: Basic sort
  #
  CALL nhl_standings_ties_sort(v_col);

  #
  # Stage 2: Tie-breaks...
  #

  # GP
  CALL nhl_standings_ties_identify();
  IF nhl_standings_ties_num() > 0 THEN
    CALL nhl_standings_ties_gp();
    CALL nhl_standings_ties_break();
  END IF;

  # Reg/OT Wins
  CALL nhl_standings_ties_identify();
  IF nhl_standings_ties_num() > 0 THEN
    CALL nhl_standings_ties_regwin();
    CALL nhl_standings_ties_break();
  END IF;

  # Reg/OT Wins
  CALL nhl_standings_ties_identify();
  IF nhl_standings_ties_num() > 0 THEN
    CALL nhl_standings_ties_regotwin();
    CALL nhl_standings_ties_break();
  END IF;

  # Points in season series (we will run as point pct for compatibility between 2 and 3+ team ties)
  CALL nhl_standings_ties_identify();
  IF nhl_standings_ties_num() > 0 THEN
    CALL nhl_standings_ties_series_ptspct();
    CALL nhl_standings_ties_break();
  END IF;

  # Goal Differential
  CALL nhl_standings_ties_identify();
  IF nhl_standings_ties_num() > 0 THEN
    CALL nhl_standings_ties_goal_diff();
    CALL nhl_standings_ties_break();
  END IF;

  # Aribtrary final (guaranteed) sort
  CALL nhl_standings_ties_identify();
  IF nhl_standings_ties_num() > 0 THEN
    CALL nhl_standings_ties_guaranteed();
    CALL nhl_standings_ties_break();
  END IF;

  #
  # Stage Z: Propogate back to standings table
  #
  CALL _exec(CONCAT('UPDATE tmp_standings SET pos_', v_col, ' = _tb_pos;'));

END $$

DELIMITER ;
