##
## Player totals sorted
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_season_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season_sort`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort season totals for NHL players'
BEGIN

  # Broken down by individual routines
  CALL nhl_totals_players_season_sort_skaters(v_season, v_season_type);
  CALL nhl_totals_players_season_sort_skaters_advanced(v_season, v_season_type);
  CALL nhl_totals_players_season_sort_goalies(v_season, v_season_type);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_players_season_sort_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season_sort_skaters`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort skater season totals for NHL players'
BEGIN

  DELETE FROM SPORTS_NHL_PLAYERS_SEASON_SKATERS_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nhl_totals_players_sort__tmp(v_season, v_season_type, 'SKATERS',
                                    'goals TINYINT UNSIGNED,
                                     assists TINYINT UNSIGNED,
                                     points TINYINT UNSIGNED,
                                     plus_minus TINYINT SIGNED,
                                     pims SMALLINT UNSIGNED,
                                     pens_taken TINYINT UNSIGNED,
                                     pens_drawn TINYINT UNSIGNED,
                                     pp_goals TINYINT UNSIGNED,
                                     pp_assists TINYINT UNSIGNED,
                                     pp_points TINYINT UNSIGNED,
                                     sh_goals TINYINT UNSIGNED,
                                     sh_assists TINYINT UNSIGNED,
                                     sh_points TINYINT UNSIGNED,
                                     gw_goals TINYINT UNSIGNED,
                                     shots SMALLINT UNSIGNED,
                                     shot_pct DECIMAL(6,5),
                                     missed_shots SMALLINT UNSIGNED,
                                     blocked_shots SMALLINT UNSIGNED,
                                     shots_blocked SMALLINT UNSIGNED,
                                     hits SMALLINT UNSIGNED,
                                     times_hit SMALLINT UNSIGNED,
                                     fo_wins SMALLINT UNSIGNED,
                                     fo_loss SMALLINT UNSIGNED,
                                     fo_pct DECIMAL(6,5),
                                     so_goals TINYINT UNSIGNED,
                                     so_shots TINYINT UNSIGNED,
                                     so_pct DECIMAL(6,5),
                                     giveaways SMALLINT UNSIGNED,
                                     takeaways SMALLINT UNSIGNED,
                                     atoi TIME,
                                     qual_shot TINYINT UNSIGNED,
                                     qual_fo TINYINT UNSIGNED',
                                    'SUM(goals) AS goals,
                                     SUM(assists) AS assists,
                                     SUM(goals) + SUM(assists) AS points,
                                     SUM(plus_minus) AS plus_minus,
                                     SUM(pims) AS pims,
                                     SUM(pens_taken) AS pens_taken,
                                     SUM(pens_drawn) AS pens_drawn,
                                     SUM(pp_goals) AS pp_goals,
                                     SUM(pp_assists) AS pp_assists,
                                     SUM(pp_goals) + SUM(pp_assists) AS pp_points,
                                     SUM(sh_goals) AS sh_goals,
                                     SUM(sh_assists) AS sh_assists,
                                     SUM(sh_goals) + SUM(sh_assists) AS sh_points,
                                     SUM(gw_goals) AS gw_goals,
                                     SUM(shots) AS shots,
                                     IF(SUM(shots) > 0, SUM(goals) / SUM(shots), 0) AS shot_pct,
                                     SUM(missed_shots) AS missed_shots,
                                     SUM(blocked_shots) AS blocked_shots,
                                     SUM(shots_blocked) AS shots_blocked,
                                     SUM(hits) AS hits,
                                     SUM(times_hit) AS times_hit,
                                     SUM(fo_wins) AS fo_wins,
                                     SUM(fo_loss) AS fo_loss,
                                     IF((SUM(fo_wins) + SUM(fo_loss)) > 0, SUM(fo_wins) / (SUM(fo_wins) + SUM(fo_loss)), 0) AS fo_pct,
                                     SUM(so_goals) AS so_goals,
                                     SUM(so_shots) AS so_shots,
                                     IF(SUM(so_shots) > 0, SUM(so_goals) / SUM(so_shots), 0) AS so_pct,
                                     SUM(giveaways) AS giveaways,
                                     SUM(takeaways) AS takeaways,
                                     SEC_TO_TIME(IF(gp,SUM(TIME_TO_SEC(atoi) * gp) / gp,0)) AS atoi,
                                     0 AS qual_shot,
                                     0 AS qual_fo');

  # Shooting and Faceoff sorting qualification
  CALL nhl_totals_players_sort__tmp_qual_skaters(v_season, v_season_type);

  # Calcs
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'points', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'plus_minus', 'TINYINT SIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pims', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pens_taken', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pens_drawn', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'pp_points', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'sh_points', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'gw_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'shots', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'shot_pct', 'DECIMAL(6,5)', '>', 'qual_shot');
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'shot_pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'missed_shots', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'blocked_shots', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'shots_blocked', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'hits', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'times_hit', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'fo_wins', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'fo_loss', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'fo_pct', 'DECIMAL(6,5)', '>', 'qual_fo');
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'fo_pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'so_goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'so_shots', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'so_pct', 'DECIMAL(6,5)', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'giveaways', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'takeaways', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS', 'atoi', 'TIME', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NHL_PLAYERS_SEASON_SKATERS_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_players_season_sort_skaters_advanced`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season_sort_skaters_advanced`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort advanced skater season totals for NHL players'
BEGIN

  DELETE FROM SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge
  CALL nhl_totals_players_sort__tmp(v_season, v_season_type, 'SKATERS_ADVANCED',
                                    'corsi_cf SMALLINT(5) UNSIGNED,
                                     corsi_ca SMALLINT(5) UNSIGNED,
                                     corsi_pct DECIMAL(4,1) UNSIGNED,
                                     corsi_off_cf SMALLINT(5) UNSIGNED,
                                     corsi_off_ca SMALLINT(5) UNSIGNED,
                                     corsi_off_pct DECIMAL(4,1) UNSIGNED,
                                     corsi_rel DECIMAL(4,1) SIGNED,
                                     fenwick_ff SMALLINT(5) UNSIGNED,
                                     fenwick_fa SMALLINT(5) UNSIGNED,
                                     fenwick_pct DECIMAL(4,1) UNSIGNED,
                                     fenwick_off_ff SMALLINT(5) UNSIGNED,
                                     fenwick_off_fa SMALLINT(5) UNSIGNED,
                                     fenwick_off_pct DECIMAL(4,1) UNSIGNED,
                                     fenwick_rel DECIMAL(4,1) SIGNED,
                                     pdo_sh_gf SMALLINT(5) UNSIGNED,
                                     pdo_sh_sog SMALLINT(5) UNSIGNED,
                                     pdo_sh DECIMAL(4,1) UNSIGNED,
                                     pdo_sv_sv SMALLINT(5) UNSIGNED,
                                     pdo_sv_sog SMALLINT(5) UNSIGNED,
                                     pdo_sv DECIMAL(4,1) UNSIGNED,
                                     pdo DECIMAL(4,1) UNSIGNED,
                                     zs_off_num SMALLINT(5) UNSIGNED,
                                     zs_off_pct DECIMAL(4,1) UNSIGNED,
                                     zs_def_num SMALLINT(5) UNSIGNED,
                                     zs_def_pct DECIMAL(4,1) UNSIGNED',
                                    'SUM(corsi_cf) AS corsi_cf,
                                     SUM(corsi_ca) AS corsi_ca,
                                     (100 * SUM(corsi_cf)) / (SUM(corsi_cf) + SUM(corsi_ca)) AS corsi_pct,
                                     SUM(corsi_off_cf) AS corsi_off_cf,
                                     SUM(corsi_off_ca) AS corsi_off_ca,
                                     (100 * SUM(corsi_off_cf)) / (SUM(corsi_off_cf) + SUM(corsi_off_ca)) AS corsi_off_pct,
                                     (100 * SUM(corsi_cf)) / (SUM(corsi_cf) + SUM(corsi_ca))
                                      - (100 * SUM(corsi_off_cf)) / (SUM(corsi_off_cf) + SUM(corsi_off_ca)) AS corsi_rel,
                                     SUM(fenwick_ff) AS fenwick_ff,
                                     SUM(fenwick_fa) AS fenwick_fa,
                                     (100 * SUM(fenwick_ff)) / (SUM(fenwick_ff) + SUM(fenwick_fa)) AS fenwick_pct,
                                     SUM(fenwick_off_ff) AS fenwick_off_ff,
                                     SUM(fenwick_off_fa) AS fenwick_off_fa,
                                     (100 * SUM(fenwick_off_ff)) / (SUM(fenwick_off_ff) + SUM(fenwick_off_fa)) AS fenwick_off_pct,
                                     (100 * SUM(fenwick_ff)) / (SUM(fenwick_ff) + SUM(fenwick_fa))
                                      - (100 * SUM(fenwick_off_ff)) / (SUM(fenwick_off_ff) + SUM(fenwick_off_fa)) AS fenwick_rel,
                                     SUM(pdo_sh_gf) AS pdo_sh_gf,
                                     SUM(pdo_sh_sog) AS pdo_sh_sog,
                                     (100 * SUM(pdo_sh_gf)) / SUM(pdo_sh_sog) AS pdo_sh,
                                     SUM(pdo_sv_sv) AS pdo_sv_sv,
                                     SUM(pdo_sv_sog) AS pdo_sv_sog,
                                     (100 * SUM(pdo_sv_sv)) / SUM(pdo_sv_sog) AS pdo_sv,
                                     (100 * SUM(pdo_sh_gf)) / SUM(pdo_sh_sog) + (100 * SUM(pdo_sv_sv)) / SUM(pdo_sv_sog) AS pdo,
                                     SUM(zs_off_num) AS zs_off_num,
                                     (100 * SUM(zs_off_num)) / (SUM(zs_off_num) + SUM(zs_def_num)) AS zs_off_pct,
                                     SUM(zs_def_num) AS zs_def_num,
                                     (100 * SUM(zs_def_num)) / (SUM(zs_off_num) + SUM(zs_def_num)) AS zs_def_pct');

  # Calcs
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_cf', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_ca', 'SMALLINT(5) UNSIGNED', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_pct', 'DECIMAL(4,1) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_off_cf', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_off_ca', 'SMALLINT(5) UNSIGNED', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_off_pct', 'DECIMAL(4,1) UNSIGNED', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'corsi_rel', 'DECIMAL(4,1) SIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_ff', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_fa', 'SMALLINT(5) UNSIGNED', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_pct', 'DECIMAL(4,1) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_off_ff', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_off_fa', 'SMALLINT(5) UNSIGNED', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_off_pct', 'DECIMAL(4,1) UNSIGNED', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'fenwick_rel', 'DECIMAL(4,1) SIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sh_gf', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sh_sog', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sh', 'DECIMAL(4,1) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sv_sv', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sv_sog', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo_sv', 'DECIMAL(4,1) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'pdo', 'DECIMAL(4,1) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'zs_off_num', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'zs_off_pct', 'DECIMAL(4,1) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'zs_def_num', 'SMALLINT(5) UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'SKATERS_ADVANCED', 'zs_def_pct', 'DECIMAL(4,1) UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_players_season_sort_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_season_sort_goalies`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Sort goalie season totals for NHL players'
BEGIN

  DELETE FROM SPORTS_NHL_PLAYERS_SEASON_GOALIES_SORTED WHERE season = v_season AND season_type = v_season_type;

  # Merge the data
  CALL nhl_totals_players_sort__tmp(v_season, v_season_type, 'GOALIES',
                                    'gp TINYINT UNSIGNED,
                                     starts TINYINT UNSIGNED,
                                     win TINYINT UNSIGNED,
                                     loss TINYINT UNSIGNED,
                                     ot_loss TINYINT UNSIGNED,
                                     so_loss TINYINT UNSIGNED,
                                     goals_against TINYINT UNSIGNED,
                                     shots_against SMALLINT UNSIGNED,
                                     saves SMALLINT UNSIGNED,
                                     save_pct DECIMAL(6,5),
                                     goals_against_avg DECIMAL(8,5),
                                     shutout TINYINT UNSIGNED,
                                     minutes_played MEDIUMINT UNSIGNED,
                                     en_goals_against TINYINT UNSIGNED,
                                     so_goals_against TINYINT UNSIGNED,
                                     so_shots_against TINYINT UNSIGNED,
                                     so_saves TINYINT UNSIGNED,
                                     so_save_pct DECIMAL(6,5),
                                     goals TINYINT UNSIGNED,
                                     assists TINYINT UNSIGNED,
                                     points TINYINT UNSIGNED,
                                     pims TINYINT UNSIGNED,
                                     pens_taken TINYINT UNSIGNED,
                                     pens_drawn TINYINT UNSIGNED,
                                     qual TINYINT UNSIGNED',
                                    'SUM(gp) AS gp,
                                     SUM(starts) AS starts,
                                     SUM(win) AS win,
                                     SUM(loss) AS loss,
                                     SUM(ot_loss) AS ot_loss,
                                     SUM(so_loss) AS so_loss,
                                     SUM(goals_against) AS goals_against,
                                     SUM(shots_against) AS shots_against,
                                     SUM(shots_against) - SUM(goals_against) AS saves,
                                     IF(SUM(shots_against) > 0, (SUM(shots_against) - SUM(goals_against)) / SUM(shots_against), 0) AS save_pct,
                                     IF(SUM(minutes_played) > 0, (SUM(goals_against) * 3600) / SUM(minutes_played), 0) AS goals_against_avg,
                                     SUM(shutout) AS shutout,
                                     SUM(minutes_played) AS minutes_played,
                                     SUM(en_goals_against) AS en_goals_against,
                                     SUM(so_goals_against) AS so_goals_against,
                                     SUM(so_shots_against) AS so_shots_against,
                                     SUM(so_shots_against) - SUM(so_goals_against) AS so_saves,
                                     IF(SUM(so_shots_against) > 0, (SUM(so_shots_against) - SUM(so_goals_against)) / SUM(so_shots_against), 0) AS so_save_pct,
                                     SUM(goals) AS goals,
                                     SUM(assists) AS assists,
                                     SUM(goals) + SUM(assists) AS points,
                                     SUM(pims) AS pims,
                                     SUM(pens_taken) AS pens_taken,
                                     SUM(pens_drawn) AS pens_drawn,
                                     0 AS qual');

  # Games Played Qualification
  CALL nhl_totals_players_sort__tmp_qual_goalies(v_season, v_season_type);

  # Calcs
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'gp', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'starts', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'win', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'loss', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'ot_loss', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_loss', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against', 'TINYINT UNSIGNED', '<', 'qual');
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_all', 'TINYINT UNSIGNED', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'shots_against', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'saves', 'SMALLINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'save_pct', 'DECIMAL(6,5)', '>', 'qual');
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'save_pct_all', 'DECIMAL(6,5)', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_avg', 'DECIMAL(8,5)', '<', 'qual');
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals_against_avg_all', 'DECIMAL(8,5)', '<', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'shutout', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'minutes_played', 'MEDIUMINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'en_goals_against', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_goals_against', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_shots_against', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_saves', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'so_save_pct', 'DECIMAL(6,5)', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'goals', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'assists', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'points', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'pims', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'pens_taken', 'TINYINT UNSIGNED', '>', NULL);
  CALL nhl_totals_players_sort__calc(v_season, v_season_type, 'GOALIES', 'pens_drawn', 'TINYINT UNSIGNED', '>', NULL);

  # Order
  ALTER TABLE SPORTS_NHL_PLAYERS_SEASON_GOALIES_SORTED ORDER BY season, season_type, player_id, team_id;

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_players_sort__tmp`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_sort__tmp`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table ENUM('SKATERS', 'SKATERS_ADVANCED', 'GOALIES'),
  v_cols VARCHAR(2048),
  v_calcs VARCHAR(2560)
)
    COMMENT 'Create temporary table for NHL player season stat sort'
BEGIN

  DECLARE v_inc_league TINYINT(1) UNSIGNED;
  SET v_inc_league := v_table IN ('SKATERS', 'GOALIES');

  # Get the stats
  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table));
  # When first creating the table, get player total - one row per player
  CALL _exec(CONCAT(
   'CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, ' (
      season SMALLINT UNSIGNED NOT NULL,
      season_type ENUM(\'regular\', \'playoff\') NOT NULL,
      player_id SMALLINT UNSIGNED NOT NULL,
      team_id VARCHAR(4),
      ', v_cols, ',
      PRIMARY KEY (season, season_type, player_id, team_id)
    ) ENGINE = MEMORY
      SELECT season, season_type, player_id, \'_NHL\' AS team_id, ', v_calcs, '
      FROM SPORTS_NHL_PLAYERS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '"
      ', IF(v_inc_league, 'AND   team_league IS NULL', ''), '
      GROUP BY season, season_type, player_id;'));
  CALL _exec(CONCAT(
   'INSERT INTO tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '
      SELECT season, season_type, player_id, team_id, ', v_calcs, '
      FROM SPORTS_NHL_PLAYERS_SEASON_', v_table, '
      WHERE season = ', v_season, '
      AND   season_type = "', v_season_type, '"
      ', IF(v_inc_league, 'AND   team_league IS NULL', ''), '
      GROUP BY season, season_type, player_id, team_id;'));
  # But then also get team_id grouped stats for each player

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_players_sort__calc`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_sort__calc`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff'),
  v_table ENUM('SKATERS', 'SKATERS_ADVANCED', 'GOALIES'),
  v_col_sort VARCHAR(25),
  v_col_def VARCHAR(50),
  v_sort_dir VARCHAR(5),
  v_qual  VARCHAR(10)
)
    COMMENT 'Sort season totals for NHL players'
BEGIN

  DECLARE v_col_data VARCHAR(25);
  DECLARE v_col_qual_flag VARCHAR(25) DEFAULT NULL;

  # Data column (which may not be v_col_sort)
  IF SUBSTRING(v_col_sort, -4) = '_all' THEN
    SET v_col_data = LEFT(v_col_sort, LENGTH(v_col_sort) - 4);
  ELSE
    SET v_col_data = v_col_sort;
  END IF;

  # Storing the qual flag?
  IF v_qual IS NOT NULL THEN
    SET v_col_qual_flag = CONCAT(v_col_data, '_qual');
  END IF;

  # Determine unique values for this column for sorting
  CALL _exec(CONCAT('DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT;'));
  CALL _exec(CONCAT(
    'CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT (
       team_id VARCHAR(4),
       `', v_col_data, '` ', v_col_def, ',
       ', IF(v_qual IS NOT NULL, CONCAT('`', v_qual, '` TINYINT UNSIGNED,'), ''), '
       num_players SMALLINT UNSIGNED,
       value_order SMALLINT UNSIGNED,
       UNIQUE KEY (team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ')
     ) ENGINE = MEMORY
       SELECT team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', COUNT(player_id) AS num_players, 9999 AS value_order
       FROM tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '
       GROUP BY team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ';'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT_cpA'));
  CALL _duplicate_tmp_table(CONCAT('tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT'), CONCAT('tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT_cpB'));

  # Sort these values
  CALL _exec(CONCAT(
    'INSERT INTO tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT (team_id, `', v_col_data, '`', IF(v_qual IS NOT NULL, CONCAT(', `', v_qual, '`'), ''), ', value_order)
       SELECT A.team_id, A.', v_col_data, ', ', IF(v_qual IS NOT NULL, CONCAT('A.', v_qual, ', '), ''), 'IFNULL(SUM(B.num_players), 0) + 1 AS value_order
       FROM tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT_cpA AS A
       LEFT JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT_cpB AS B
         ON (B.team_id = A.team_id
         AND (', IF(v_qual IS NOT NULL,
                    CONCAT('B.', v_qual, ' > A.', v_qual, ' OR (B.', v_qual, ' = A.', v_qual, ' AND '), ''),
              'B.', v_col_data, ' ', v_sort_dir, ' A.', v_col_data,
              IF (v_qual IS NOT NULL, ')', ''), '))
       GROUP BY A.team_id, A.', v_col_data, IF(v_qual IS NOT NULL, CONCAT(', A.', v_qual), ''), '
     ON DUPLICATE KEY UPDATE value_order = VALUES(value_order);'));

  # Update sorting for the players
  CALL _exec(CONCAT(
   'INSERT INTO SPORTS_NHL_PLAYERS_SEASON_', v_table, '_SORTED (season, season_type, player_id, team_id, `', v_col_sort, '`', IF(v_col_qual_flag IS NOT NULL, CONCAT(', `', v_col_qual_flag, '`'), ''), ')
      SELECT PLAYER.season, PLAYER.season_type, PLAYER.player_id, PLAYER.team_id,
             STAT.value_order AS `', v_col_sort, '`',
             IF(v_col_qual_flag IS NOT NULL, CONCAT(', STAT.', v_qual, ' AS `', v_col_qual_flag, '`'), ''), '
      FROM tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, ' AS PLAYER
      JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_', v_table, '_STAT AS STAT
        ON (STAT.team_id = PLAYER.team_id
        AND STAT.', v_col_data, ' = PLAYER.', v_col_data,
            IF(v_qual IS NOT NULL,
               CONCAT(' AND STAT.', v_qual, '= PLAYER.', v_qual), ''), ')
      WHERE PLAYER.season = ', v_season, '
      AND   PLAYER.season_type = "', v_season_type, '"
    ON DUPLICATE KEY UPDATE `', v_col_sort, '` = VALUES(`', v_col_sort, '`)',
                            IF(v_col_qual_flag IS NOT NULL, CONCAT(', `', v_col_qual_flag, '` = VALUES(`', v_col_qual_flag, '`)'), ''), ';'));

END $$

DELIMITER ;

