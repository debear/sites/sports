##
## Daily advanced stat calcs
##
DROP PROCEDURE IF EXISTS `nhl_totals_daily_advanced`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_daily_advanced`()
    COMMENT 'Calculate daily game advanced stats for the NHL'
BEGIN

  # Some initial setup calcs
  CALL nhl_totals_daily_advanced_setup();

  # Individual stats
  CALL nhl_totals_daily_advanced_corsi();
  CALL nhl_totals_daily_advanced_fenwick();
  CALL nhl_totals_daily_advanced_pdo();
  CALL nhl_totals_daily_advanced_zone_starts();

  # Finally store
  CALL nhl_totals_daily_advanced_store();

END $$

DELIMITER ;

##
## Setup
##
DROP PROCEDURE IF EXISTS `nhl_totals_daily_advanced_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_daily_advanced_setup`()
    COMMENT 'Setup process for daily game advanced stats for the NHL'
BEGIN

  DECLARE v_player_index TINYINT UNSIGNED;
  DECLARE v_team_index TINYINT UNSIGNED;

  # Reduce the rosters table
  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_rosters;
  CREATE TEMPORARY TABLE tmp_player_advanced_rosters LIKE SPORTS_NHL_GAME_LINEUP;
  INSERT INTO tmp_player_advanced_rosters
    SELECT SPORTS_NHL_GAME_LINEUP.*
    FROM tmp_game_list
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos <> 'G');

  # Our base event listpdo
  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_events_base;
  CREATE TEMPORARY TABLE tmp_player_advanced_events_base (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    event_id SMALLINT UNSIGNED,
    event_period TINYINT UNSIGNED,
    event_time TIME,
    event_team_id CHAR(3),
    event_type ENUM('GOAL', 'SHOT', 'MISSEDSHOT', 'BLOCKEDSHOT', 'FACEOFF'),
    zone ENUM('off', 'neu', 'def'),
    is_corsi TINYINT(1) UNSIGNED,
    is_fenwick TINYINT(1) UNSIGNED,
    is_pdo TINYINT(1) UNSIGNED,
    is_zone_start TINYINT(1) UNSIGNED,
    num_home TINYINT(1) UNSIGNED,
    num_visitor TINYINT(1) UNSIGNED,
    is_x_counter TINYINT(1) UNSIGNED,
    PRIMARY KEY (season, game_type, game_id, event_id)
  ) SELECT SPORTS_NHL_GAME_EVENT.season,
           SPORTS_NHL_GAME_EVENT.game_type,
           SPORTS_NHL_GAME_EVENT.game_id,
           SPORTS_NHL_GAME_EVENT.event_id,
           SPORTS_NHL_GAME_EVENT.period AS event_period,
           SPORTS_NHL_GAME_EVENT.event_time,
           # Blocked Shots are recorded by the team who blocked the shot than attempted it
           IF(SPORTS_NHL_GAME_EVENT.event_type <> 'BLOCKEDSHOT',
              SPORTS_NHL_GAME_EVENT.team_id,
              IF(SPORTS_NHL_GAME_EVENT.team_id = SPORTS_NHL_SCHEDULE.home,
                 SPORTS_NHL_SCHEDULE.visitor,
                 SPORTS_NHL_SCHEDULE.home)) AS event_team_id,
           SPORTS_NHL_GAME_EVENT.event_type,
           SPORTS_NHL_GAME_EVENT.zone,
           SPORTS_NHL_GAME_EVENT.event_type IN ('GOAL', 'SHOT', 'MISSEDSHOT', 'BLOCKEDSHOT') AS is_corsi,
           SPORTS_NHL_GAME_EVENT.event_type IN ('GOAL', 'SHOT', 'MISSEDSHOT') AS is_fenwick,
           SPORTS_NHL_GAME_EVENT.event_type IN ('GOAL', 'SHOT') AS is_pdo,
           SPORTS_NHL_GAME_EVENT.event_type = 'FACEOFF' AND SPORTS_NHL_GAME_EVENT.zone IN ('off', 'def') AS is_zone_start,
           CASE
             WHEN ONICE_HOME.player_5 IS NULL THEN 3
             WHEN ONICE_HOME.player_6 IS NULL THEN 4
             ELSE 5
           END AS num_home,
           CASE
             WHEN ONICE_VISITOR.player_5 IS NULL THEN 3
             WHEN ONICE_VISITOR.player_6 IS NULL THEN 4
             ELSE 5
           END AS num_visitor,
           0 AS is_x_counter
    FROM tmp_game_list
    JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = tmp_game_list.season
      AND SPORTS_NHL_SCHEDULE.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_SCHEDULE.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_EVENT
      ON (SPORTS_NHL_GAME_EVENT.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_EVENT.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_EVENT.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_EVENT.event_type IN ('GOAL', 'SHOT', 'MISSEDSHOT', 'BLOCKEDSHOT', 'FACEOFF'))
    JOIN SPORTS_NHL_GAME_EVENT_ONICE AS ONICE_HOME
      ON (ONICE_HOME.season = SPORTS_NHL_GAME_EVENT.season
      AND ONICE_HOME.game_type = SPORTS_NHL_GAME_EVENT.game_type
      AND ONICE_HOME.game_id = SPORTS_NHL_GAME_EVENT.game_id
      AND ONICE_HOME.event_id = SPORTS_NHL_GAME_EVENT.event_id
      AND ONICE_HOME.team_id = SPORTS_NHL_SCHEDULE.home)
    JOIN SPORTS_NHL_GAME_EVENT_ONICE AS ONICE_VISITOR
      ON (ONICE_VISITOR.season = SPORTS_NHL_GAME_EVENT.season
      AND ONICE_VISITOR.game_type = SPORTS_NHL_GAME_EVENT.game_type
      AND ONICE_VISITOR.game_id = SPORTS_NHL_GAME_EVENT.game_id
      AND ONICE_VISITOR.event_id = SPORTS_NHL_GAME_EVENT.event_id
      AND ONICE_VISITOR.team_id = SPORTS_NHL_SCHEDULE.visitor)
    ORDER BY SPORTS_NHL_GAME_EVENT.season,
             SPORTS_NHL_GAME_EVENT.game_type,
             SPORTS_NHL_GAME_EVENT.game_id,
             SPORTS_NHL_GAME_EVENT.event_id;

  # Pruning events that do not apply
  DELETE FROM tmp_player_advanced_events_base WHERE num_home <> num_visitor;
  UPDATE tmp_player_advanced_events_base SET is_x_counter = is_corsi + is_fenwick + is_pdo + is_zone_start;
  DELETE FROM tmp_player_advanced_events_base WHERE is_x_counter = 0;
  ALTER TABLE tmp_player_advanced_events_base
    DROP COLUMN num_home,
    DROP COLUMN num_visitor,
    DROP COLUMN is_x_counter;

  # Calculate per-team shot/goal totals
  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_byteam;
  CREATE TEMPORARY TABLE tmp_player_advanced_byteam (
    season SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    gf TINYINT UNSIGNED,
    ga TINYINT UNSIGNED,
    corsi_cf TINYINT UNSIGNED,
    corsi_ca TINYINT UNSIGNED,
    fenwick_ff TINYINT UNSIGNED,
    fenwick_fa TINYINT UNSIGNED,
    pdo_sf TINYINT UNSIGNED,
    pdo_sa TINYINT UNSIGNED,
    PRIMARY KEY (season, game_type, game_id, team_id)
  ) SELECT season, game_type, game_id, event_team_id AS team_id,
           SUM(event_type = 'GOAL') AS gf, 0 AS ga,
           SUM(event_type IN ('GOAL', 'SHOT', 'MISSEDSHOT', 'BLOCKEDSHOT')) AS corsi_cf, 0 AS corsi_ca,
           SUM(event_type IN ('GOAL', 'SHOT', 'MISSEDSHOT')) AS fenwick_ff, 0 AS fenwick_fa,
           SUM(event_type IN ('GOAL', 'SHOT')) AS pdo_sf, 0 AS pdo_sa
    FROM tmp_player_advanced_events_base
    WHERE event_type IN ('GOAL', 'SHOT', 'MISSEDSHOT', 'BLOCKEDSHOT')
    GROUP BY season, game_type, game_id, event_team_id;
  # Part 2: Against
  CALL _duplicate_tmp_table('tmp_player_advanced_byteam', 'tmp_player_advanced_byteam_cp');
  UPDATE tmp_player_advanced_byteam
  JOIN tmp_player_advanced_byteam_cp
    ON (tmp_player_advanced_byteam_cp.season = tmp_player_advanced_byteam.season
    AND tmp_player_advanced_byteam_cp.game_type = tmp_player_advanced_byteam.game_type
    AND tmp_player_advanced_byteam_cp.game_id = tmp_player_advanced_byteam.game_id
    AND tmp_player_advanced_byteam_cp.team_id <> tmp_player_advanced_byteam.team_id)
  SET tmp_player_advanced_byteam.ga = tmp_player_advanced_byteam_cp.gf,
      tmp_player_advanced_byteam.corsi_ca = tmp_player_advanced_byteam_cp.corsi_cf,
      tmp_player_advanced_byteam.fenwick_fa = tmp_player_advanced_byteam_cp.fenwick_ff,
      tmp_player_advanced_byteam.pdo_sa = tmp_player_advanced_byteam_cp.pdo_sf;

  # Prepare adding the players
  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_events;
  CREATE TEMPORARY TABLE tmp_player_advanced_events LIKE tmp_player_advanced_events_base;
  ALTER TABLE tmp_player_advanced_events
    ADD COLUMN player_id SMALLINT UNSIGNED,
    ADD COLUMN player_team_id CHAR(3),
    ADD COLUMN jersey TINYINT(2) UNSIGNED,
    ADD COLUMN is_team TINYINT(1) UNSIGNED,
    DROP PRIMARY KEY,
    ADD UNIQUE KEY (season, game_type, game_id, event_id, player_id);

  # Add the players
  SET v_team_index := 0;
  WHILE v_team_index <= 1 DO
    SET v_player_index := 1;
    WHILE v_player_index <= 6 DO
      CALL _exec(CONCAT('INSERT INTO tmp_player_advanced_events
      SELECT tmp_player_advanced_events_base.*,
             tmp_player_advanced_rosters.player_id,
             tmp_player_advanced_rosters.team_id AS player_team_id,
             tmp_player_advanced_rosters.jersey,
             ', (1 - v_team_index), ' AS is_team
      FROM tmp_player_advanced_events_base
      JOIN SPORTS_NHL_GAME_EVENT_ONICE
        ON (SPORTS_NHL_GAME_EVENT_ONICE.season = tmp_player_advanced_events_base.season
        AND SPORTS_NHL_GAME_EVENT_ONICE.game_type = tmp_player_advanced_events_base.game_type
        AND SPORTS_NHL_GAME_EVENT_ONICE.game_id = tmp_player_advanced_events_base.game_id
        AND SPORTS_NHL_GAME_EVENT_ONICE.event_id = tmp_player_advanced_events_base.event_id
        AND SPORTS_NHL_GAME_EVENT_ONICE.team_id ', IF(v_team_index, '<>', '='), ' tmp_player_advanced_events_base.event_team_id)
      JOIN tmp_player_advanced_rosters
        ON (tmp_player_advanced_rosters.season = SPORTS_NHL_GAME_EVENT_ONICE.season
        AND tmp_player_advanced_rosters.game_type = SPORTS_NHL_GAME_EVENT_ONICE.game_type
        AND tmp_player_advanced_rosters.game_id = SPORTS_NHL_GAME_EVENT_ONICE.game_id
        AND tmp_player_advanced_rosters.team_id = SPORTS_NHL_GAME_EVENT_ONICE.team_id
        AND tmp_player_advanced_rosters.jersey = SPORTS_NHL_GAME_EVENT_ONICE.player_', v_player_index , ');'));
      SET v_player_index = v_player_index + 1;
    END WHILE;
    SET v_team_index = v_team_index + 1;
  END WHILE;

  # Faceoffs also need to be at the start of a shift for players, so prune those that aren't
  DELETE tmp_player_advanced_events.*
  FROM tmp_player_advanced_events
  LEFT JOIN SPORTS_NHL_GAME_TOI
    ON (SPORTS_NHL_GAME_TOI.season = tmp_player_advanced_events.season
    AND SPORTS_NHL_GAME_TOI.game_type = tmp_player_advanced_events.game_type
    AND SPORTS_NHL_GAME_TOI.game_id = tmp_player_advanced_events.game_id
    AND SPORTS_NHL_GAME_TOI.team_id = tmp_player_advanced_events.player_team_id
    AND SPORTS_NHL_GAME_TOI.jersey = tmp_player_advanced_events.jersey
    AND SPORTS_NHL_GAME_TOI.period = tmp_player_advanced_events.event_period
    AND SPORTS_NHL_GAME_TOI.shift_start = tmp_player_advanced_events.event_time)
  WHERE tmp_player_advanced_events.event_type = 'FACEOFF'
  AND   SPORTS_NHL_GAME_TOI.shift_num IS NULL;

  # Flip around the zones for records where is_team = 0 (zone is relative to the event's team_id, not the player's)
  UPDATE tmp_player_advanced_events
  SET zone = CASE zone
    WHEN 'off' THEN 'def'
    WHEN 'def' THEN 'off'
    ELSE zone
  END
  WHERE is_team = 0;

END $$

DELIMITER ;

##
## Corsi calcs
##
DROP PROCEDURE IF EXISTS `nhl_totals_daily_advanced_corsi`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_daily_advanced_corsi`()
    COMMENT 'Corsi calcs for daily game advanced stats for the NHL'
BEGIN

  # Build our on-ice numbers
  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_corsi;
  CREATE TEMPORARY TABLE tmp_player_advanced_corsi (
    season SMALLINT UNSIGNED,
    player_id SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    cf TINYINT UNSIGNED,
    ca TINYINT UNSIGNED,
    pct DECIMAL(4,1),
    off_cf TINYINT UNSIGNED,
    off_ca TINYINT UNSIGNED,
    off_pct DECIMAL(4,1),
    rel DECIMAL(4,1),
    PRIMARY KEY (season, player_id, game_type, game_id),
    INDEX by_game_team (season, game_type, game_id, team_id)
  ) SELECT season, player_id, game_type, game_id, player_team_id AS team_id,
           SUM(event_team_id = player_team_id) AS cf,
           SUM(event_team_id <> player_team_id) AS ca,
           NULL AS pct,
           NULL AS off_cf,
           NULL AS off_ca,
           NULL AS off_pct,
           NULL AS rel
    FROM tmp_player_advanced_events
    WHERE is_corsi
    GROUP BY season, game_type, game_id, player_id;

  # Off-ice numbers
  UPDATE tmp_player_advanced_corsi
  JOIN tmp_player_advanced_byteam
    ON (tmp_player_advanced_byteam.season = tmp_player_advanced_corsi.season
    AND tmp_player_advanced_byteam.game_type = tmp_player_advanced_corsi.game_type
    AND tmp_player_advanced_byteam.game_id = tmp_player_advanced_corsi.game_id
    AND tmp_player_advanced_byteam.team_id = tmp_player_advanced_corsi.team_id)
  SET tmp_player_advanced_corsi.off_cf = tmp_player_advanced_byteam.corsi_cf - tmp_player_advanced_corsi.cf,
      tmp_player_advanced_corsi.off_ca = tmp_player_advanced_byteam.corsi_ca - tmp_player_advanced_corsi.ca;

  # Percentages
  UPDATE tmp_player_advanced_corsi
  SET pct = IF(cf + ca, (100 * cf) / (cf + ca), NULL),
      off_pct = IF(off_cf + off_ca, (100 * off_cf) / (off_cf + off_ca), NULL);

  # Relative
  UPDATE tmp_player_advanced_corsi
  SET rel = pct - off_pct
  WHERE pct IS NOT NULL
  AND   off_pct IS NOT NULL;

END $$

DELIMITER ;

##
## Fenwick calcs
##
DROP PROCEDURE IF EXISTS `nhl_totals_daily_advanced_fenwick`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_daily_advanced_fenwick`()
    COMMENT 'Fenwick calcs for daily game advanced stats for the NHL'
BEGIN

  # Build our on-ice numbers
  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_fenwick;
  CREATE TEMPORARY TABLE tmp_player_advanced_fenwick (
    season SMALLINT UNSIGNED,
    player_id SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    ff TINYINT UNSIGNED,
    fa TINYINT UNSIGNED,
    pct DECIMAL(4,1),
    off_ff TINYINT UNSIGNED,
    off_fa TINYINT UNSIGNED,
    off_pct DECIMAL(4,1),
    rel DECIMAL(4,1),
    PRIMARY KEY (season, player_id, game_type, game_id),
    INDEX by_game_team (season, game_type, game_id, team_id)
  ) SELECT season, player_id, game_type, game_id, player_team_id AS team_id,
           SUM(event_team_id = player_team_id) AS ff,
           SUM(event_team_id <> player_team_id) AS fa,
           NULL AS pct,
           NULL AS off_ff,
           NULL AS off_fa,
           NULL AS off_pct,
           NULL AS rel
    FROM tmp_player_advanced_events
    WHERE is_fenwick
    GROUP BY season, game_type, game_id, player_id;

  # Off-ice numbers
  UPDATE tmp_player_advanced_fenwick
  JOIN tmp_player_advanced_byteam
    ON (tmp_player_advanced_byteam.season = tmp_player_advanced_fenwick.season
    AND tmp_player_advanced_byteam.game_type = tmp_player_advanced_fenwick.game_type
    AND tmp_player_advanced_byteam.game_id = tmp_player_advanced_fenwick.game_id
    AND tmp_player_advanced_byteam.team_id = tmp_player_advanced_fenwick.team_id)
  SET tmp_player_advanced_fenwick.off_ff = tmp_player_advanced_byteam.fenwick_ff - tmp_player_advanced_fenwick.ff,
      tmp_player_advanced_fenwick.off_fa = tmp_player_advanced_byteam.fenwick_fa - tmp_player_advanced_fenwick.fa;

  # Percentages
  UPDATE tmp_player_advanced_fenwick
  SET pct = IF(ff + fa, (100 * ff) / (ff + fa), NULL),
      off_pct = IF(off_ff + off_fa, (100 * off_ff) / (off_ff + off_fa), NULL);

  # Relative
  UPDATE tmp_player_advanced_fenwick
  SET rel = pct - off_pct
  WHERE pct IS NOT NULL
  AND   off_pct IS NOT NULL;

END $$

DELIMITER ;

##
## PDO calcs
##
DROP PROCEDURE IF EXISTS `nhl_totals_daily_advanced_pdo`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_daily_advanced_pdo`()
    COMMENT 'PDO calcs for daily game advanced stats for the NHL'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_pdo;
  CREATE TEMPORARY TABLE tmp_player_advanced_pdo (
    season SMALLINT UNSIGNED,
    player_id SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    sh_gf TINYINT UNSIGNED,
    sh_sog TINYINT UNSIGNED,
    sh DECIMAL(4,1) UNSIGNED,
    sv_sv TINYINT UNSIGNED,
    sv_sog TINYINT UNSIGNED,
    sv DECIMAL(4,1) UNSIGNED,
    pdo DECIMAL(4,1),
    PRIMARY KEY (season, player_id, game_type, game_id)
  ) SELECT season, player_id, game_type, game_id, player_team_id AS team_id,
           SUM(is_team = 1 AND event_type = 'GOAL') AS sh_gf,
           SUM(is_team = 1) AS sh_sog,
           NULL AS sh,
           SUM(is_team = 0 AND event_type <> 'GOAL') AS sv_sv,
           SUM(is_team = 0) AS sv_sog,
           NULL AS sv,
           NULL AS pdo
    FROM tmp_player_advanced_events
    WHERE is_pdo
    GROUP BY season, game_type, game_id, player_id;

  # Percentages
  UPDATE tmp_player_advanced_pdo
  SET sh = IF(sh_sog, 100 * (sh_gf / sh_sog), NULL),
      sv = IF(sv_sog, 100 * (sv_sv / sv_sog), NULL);

  # Combined total
  UPDATE tmp_player_advanced_pdo
  SET pdo = sh + sv
  WHERE sh IS NOT NULL
  AND   sv IS NOT NULL;

END $$

DELIMITER ;

##
## Zone start calcs
##
DROP PROCEDURE IF EXISTS `nhl_totals_daily_advanced_zone_starts`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_daily_advanced_zone_starts`()
    COMMENT 'Zone start calcs for daily game advanced stats for the NHL'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_player_advanced_zone;
  CREATE TEMPORARY TABLE tmp_player_advanced_zone (
    season SMALLINT UNSIGNED,
    player_id SMALLINT UNSIGNED,
    game_type ENUM('regular', 'playoff'),
    game_id SMALLINT UNSIGNED,
    team_id CHAR(3),
    off_num TINYINT UNSIGNED,
    off_pct DECIMAL(4,1),
    def_num TINYINT UNSIGNED,
    def_pct DECIMAL(4,1),
    PRIMARY KEY (season, player_id, game_type, game_id)
  ) SELECT season, player_id, game_type, game_id, player_team_id AS team_id,
           SUM(zone = 'off') AS off_num,
           NULL AS off_pct,
           SUM(zone = 'def') AS def_num,
           NULL AS def_pct
    FROM tmp_player_advanced_events
    WHERE is_zone_start
    GROUP BY season, game_type, game_id, player_id;

  # Percentages
  UPDATE tmp_player_advanced_zone
  SET off_pct = IF(off_num + def_num, (100 * off_num) / (off_num + def_num), NULL),
      def_pct = IF(off_num + def_num, (100 * def_num) / (off_num + def_num), NULL);

END $$

DELIMITER ;

##
## Store the results
##
DROP PROCEDURE IF EXISTS `nhl_totals_daily_advanced_store`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_daily_advanced_store`()
    COMMENT 'Store the daily game advanced stats for the NHL'
BEGIN

  # Players
  DELETE SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED.*
  FROM tmp_game_list
  JOIN SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED
    ON (SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED.season = tmp_game_list.season
    AND SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED.game_type = tmp_game_list.game_type
    AND SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED.game_id = tmp_game_list.game_id);

  INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED (season, player_id, game_type, game_id, team_id,
      corsi_cf, corsi_ca, corsi_pct, corsi_off_cf, corsi_off_ca, corsi_off_pct, corsi_rel,
      fenwick_ff, fenwick_fa, fenwick_pct, fenwick_off_ff, fenwick_off_fa, fenwick_off_pct, fenwick_rel,
      pdo_sh_gf, pdo_sh_sog, pdo_sh, pdo_sv_sv, pdo_sv_sog, pdo_sv, pdo,
      zs_off_num, zs_off_pct, zs_def_num, zs_def_pct)
    SELECT roster.season, roster.player_id, roster.game_type, roster.game_id, roster.team_id,
           corsi.cf, corsi.ca, corsi.pct, corsi.off_cf, corsi.off_ca, corsi.off_pct, corsi.rel,
           fenwick.ff, fenwick.fa, fenwick.pct, fenwick.off_ff, fenwick.off_fa, fenwick.off_pct, fenwick.rel,
           pdo.sh_gf, pdo.sh_sog, pdo.sh, pdo.sv_sv, pdo.sv_sog, pdo.sv, pdo.pdo,
           zs.off_num, zs.off_pct, zs.def_num, zs.def_pct
    FROM tmp_player_advanced_rosters AS roster
    JOIN tmp_player_advanced_corsi AS corsi
      ON (corsi.season = roster.season
      AND corsi.player_id = roster.player_id
      AND corsi.game_type = roster.game_type
      AND corsi.game_id = roster.game_id)
    JOIN tmp_player_advanced_fenwick AS fenwick
      ON (fenwick.season = roster.season
      AND fenwick.player_id = roster.player_id
      AND fenwick.game_type = roster.game_type
      AND fenwick.game_id = roster.game_id)
    JOIN tmp_player_advanced_pdo AS pdo
      ON (pdo.season = roster.season
      AND pdo.player_id = roster.player_id
      AND pdo.game_type = roster.game_type
      AND pdo.game_id = roster.game_id)
    JOIN tmp_player_advanced_zone AS zs
      ON (zs.season = roster.season
      AND zs.player_id = roster.player_id
      AND zs.game_type = roster.game_type
      AND zs.game_id = roster.game_id);

  # Teams
  DELETE SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED.*
  FROM tmp_game_list
  JOIN SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED
    ON (SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED.season = tmp_game_list.season
    AND SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED.game_type = tmp_game_list.game_type
    AND SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED.game_id = tmp_game_list.game_id);

  INSERT INTO SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED (season, team_id, game_type, game_id,
      corsi_cf, corsi_ca, corsi_pct,
      fenwick_ff, fenwick_fa, fenwick_pct,
      pdo_sh_gf, pdo_sh_sog, pdo_sh, pdo_sv_sv, pdo_sv_sog, pdo_sv, pdo)
    SELECT season, team_id, game_type, game_id,
           corsi_cf, corsi_ca,
           IF(corsi_cf + corsi_ca, (100 * corsi_cf) / (corsi_cf + corsi_ca), NULL) AS corsi_pct,
           fenwick_ff, fenwick_fa,
           IF(fenwick_ff + fenwick_fa, (100 * fenwick_ff) / (fenwick_ff + fenwick_fa), NULL) AS fenwick_pct,
           gf AS pdo_sh_gf, pdo_sf AS pdo_sh_sog,
           IF(pdo_sf, (100 * gf) / pdo_sf, NULL) AS pdo_sh,
           pdo_sa - ga AS pdo_sv_sv, pdo_sa AS pdo_sv_sog,
           IF(pdo_sa, (100 * (pdo_sa - ga)) / pdo_sa, NULL) AS pdo_sv,
           IF(pdo_sf AND pdo_sa, ((100 * gf) / pdo_sf) + ((100 * (pdo_sa - ga)) / pdo_sa), NULL) AS pdo
    FROM tmp_player_advanced_byteam;

END $$

DELIMITER ;
