##
## Team season advanced stat calcs
##
DROP PROCEDURE IF EXISTS `nhl_totals_teams_season_advanced`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season_advanced`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate season game advanced stats for NHL teams'
BEGIN

  ##
  ## Combine daily figures into a single season
  ##
  INSERT INTO SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED (season, season_type, team_id,
      corsi_cf, corsi_ca, corsi_pct,
      fenwick_ff, fenwick_fa, fenwick_pct,
      pdo_sh_gf, pdo_sh_sog, pdo_sh, pdo_sv_sv, pdo_sv_sog, pdo_sv, pdo)
    SELECT season,
           game_type AS season_type,
           team_id,
           SUM(IFNULL(corsi_cf, 0)) AS corsi_cf,
           SUM(IFNULL(corsi_ca, 0)) AS corsi_ca,
           NULL AS corsi_pct,
           SUM(IFNULL(fenwick_ff, 0)) AS fenwick_ff,
           SUM(IFNULL(fenwick_fa, 0)) AS fenwick_fa,
           NULL AS fenwick_pct,
           SUM(IFNULL(pdo_sh_gf, 0)) AS pdo_sh_gf,
           SUM(IFNULL(pdo_sh_sog, 0)) AS pdo_sh_sog,
           NULL AS pdo_sh,
           SUM(IFNULL(pdo_sv_sv, 0)) AS pdo_sv_sv,
           SUM(IFNULL(pdo_sv_sog, 0)) AS pdo_sv_sog,
           NULL AS pdo_sv,
           NULL AS pdo
    FROM SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED
    WHERE season = v_season
    AND   game_type = v_game_type
    GROUP BY season, game_type, team_id
    ORDER BY season, game_type, team_id
  ON DUPLICATE KEY UPDATE corsi_cf = VALUES(corsi_cf),
                          corsi_ca = VALUES(corsi_ca),
                          corsi_pct = VALUES(corsi_pct),
                          fenwick_ff = VALUES(fenwick_ff),
                          fenwick_fa = VALUES(fenwick_fa),
                          fenwick_pct = VALUES(fenwick_pct),
                          pdo_sh_gf = VALUES(pdo_sh_gf),
                          pdo_sh_sog = VALUES(pdo_sh_sog),
                          pdo_sh = VALUES(pdo_sh),
                          pdo_sv_sv = VALUES(pdo_sv_sv),
                          pdo_sv_sog = VALUES(pdo_sv_sog),
                          pdo_sv = VALUES(pdo_sv),
                          pdo = VALUES(pdo);

  # Then build the percentages
  UPDATE SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED
  SET corsi_pct = IF(corsi_cf + corsi_ca, (100 * corsi_cf) / (corsi_cf + corsi_ca), NULL),
      fenwick_pct = IF(fenwick_ff + fenwick_fa, (100 * fenwick_ff) / (fenwick_ff + fenwick_fa), NULL),
      pdo_sh = IF(pdo_sh_sog, (100 * pdo_sh_gf) / pdo_sh_sog, NULL),
      pdo_sv = IF(pdo_sv_sog, (100 * pdo_sv_sv) / pdo_sv_sog, NULL),
      pdo = IF(pdo_sh_sog AND pdo_sv_sog, ((100 * pdo_sh_gf) / pdo_sh_sog) + ((100 * pdo_sv_sv) / pdo_sv_sog), NULL)
  WHERE season = v_season
  AND   season_type = v_game_type;

END $$

DELIMITER ;
