##
## Player sorting qualification logic
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_sort__tmp_qual_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_sort__tmp_qual_skaters`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NHL skater sorting'
BEGIN

  DECLARE v_rounds TINYINT UNSIGNED;

  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_QUAL;
  CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_QUAL (
    season SMALLINT UNSIGNED,
    season_type ENUM('regular','playoff'),
    player_id SMALLINT UNSIGNED,
    qual_shot TINYINT UNSIGNED DEFAULT 0,
    qual_fo TINYINT UNSIGNED DEFAULT 0,
    PRIMARY KEY (season, season_type, player_id)
  ) ENGINE = MEMORY;

  # Determine Team GP
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_teamGP;
  CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_teamGP (
    team_id VARCHAR(3),
    num TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, COUNT(DISTINCT game_id) AS num
    FROM SPORTS_NHL_PLAYERS_GAME_SKATERS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY team_id;

  # Shooting Pct
  #  Regular Season
  #   - Min 1 shot per Team GP
  IF v_season_type = 'regular' THEN
    # And calculate for each player
    INSERT INTO tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_QUAL (season, season_type, player_id, qual_shot)
      SELECT STAT.season, STAT.season_type, STAT.player_id,
             SUM(STAT.shots) >= MIN(TEAM_GP.num) AS qual_shot
      FROM SPORTS_NHL_PLAYERS_SEASON_SKATERS AS STAT
      LEFT JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_teamGP AS TEAM_GP
        ON (TEAM_GP.team_id = STAT.team_id)
      WHERE STAT.season = v_season
      AND   STAT.season_type = v_season_type
      AND   STAT.team_league IS NULL
      GROUP BY STAT.season, STAT.season_type, STAT.player_id
    ON DUPLICATE KEY UPDATE qual_shot = VALUES(qual_shot);

  #  Playoffs
  #   - Min 1 shot per Round, + 1 for SCF
  ELSE
    # Determine the number of rounds played
    SELECT COUNT(DISTINCT FLOOR(game_id / 100)) INTO v_rounds
    FROM SPORTS_NHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = v_season_type
    AND   status IS NOT NULL;

    IF v_rounds = 4 THEN
      SET v_rounds = 5;
    END IF;

    INSERT INTO tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_QUAL (season, season_type, player_id, qual_shot)
      SELECT STAT.season, STAT.season_type, STAT.player_id,
             SUM(STAT.shots) >= MIN(TEAM_GP.num) AS qual_shot
      FROM SPORTS_NHL_PLAYERS_SEASON_SKATERS AS STAT
      LEFT JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_teamGP AS TEAM_GP
        ON (TEAM_GP.team_id = STAT.team_id)
      WHERE STAT.season = v_season
      AND   STAT.season_type = v_season_type
      AND   STAT.team_league IS NULL
      GROUP BY STAT.season, STAT.season_type, STAT.player_id
    ON DUPLICATE KEY UPDATE qual_shot = VALUES(qual_shot);

  END IF;

  # Faceoff Pct
  #  Regular Season + Playoffs
  #   - Min 15% of Teams FO
  # Determine Team Total FO
  DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_teamFO;
  CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_teamFO (
    team_id VARCHAR(3),
    num SMALLINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id, SUM(fo_wins + fo_loss) AS num
    FROM SPORTS_NHL_PLAYERS_GAME_SKATERS
    WHERE season = v_season
    AND   game_type = v_season_type
    GROUP BY team_id;

  # And calculate for each player
  INSERT INTO tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_QUAL (season, season_type, player_id, qual_fo)
    SELECT STAT.season, STAT.season_type, STAT.player_id,
           IF(IFNULL(MIN(TEAM_FO.num), 0) = 0, 0,
              ((SUM(STAT.fo_wins + fo_loss) / MIN(TEAM_FO.num)) >= 0.15)) AS qual_fo
    FROM SPORTS_NHL_PLAYERS_SEASON_SKATERS AS STAT
    LEFT JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_teamFO AS TEAM_FO
      ON (TEAM_FO.team_id = STAT.team_id)
    WHERE STAT.season = v_season
    AND   STAT.season_type = v_season_type
    AND   STAT.team_league IS NULL
    GROUP BY STAT.season, STAT.season_type, STAT.player_id
  ON DUPLICATE KEY UPDATE qual_fo = VALUES(qual_fo);

  #
  # Write back
  #
  UPDATE tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS_QUAL AS QUAL
  JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_SKATERS AS STAT
    ON (STAT.season = QUAL.season
    AND STAT.season_type = QUAL.season_type
    AND STAT.player_id = QUAL.player_id)
  SET STAT.qual_shot = IF(STAT.team_id = '_NHL', QUAL.qual_shot, 1), # Players always qualify for team grouped lists
      STAT.qual_fo = IF(STAT.team_id = '_NHL', QUAL.qual_fo, 1);

END $$

DELIMITER ;

DROP PROCEDURE IF EXISTS `nhl_totals_players_sort__tmp_qual_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_sort__tmp_qual_goalies`(
  v_season SMALLINT UNSIGNED,
  v_season_type ENUM('regular','playoff')
)
    COMMENT 'Determine qualifying criteria for NHL goalie sorting'
BEGIN

  DECLARE v_rounds TINYINT UNSIGNED;

  # - Regular Season
  #   - Min 25 GP in a season
  #   -  == Rate of 3 GP per 10 Team Games
  #   -     Or, GP >= 1 if Team GP < 10 (To ensure we have people on the list in first 10 games)
  IF v_season_type = 'regular' THEN
    # Determine Team GP
    DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_teamGP;
    CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_teamGP (
      team_id VARCHAR(3),
      num TINYINT UNSIGNED,
      PRIMARY KEY (team_id)
    ) ENGINE = MEMORY
      SELECT team_id, COUNT(DISTINCT game_id) AS num
      FROM SPORTS_NHL_PLAYERS_GAME_GOALIES
      WHERE season = v_season
      AND   game_type = v_season_type
      GROUP BY team_id;

    # And calculate for each player
    DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_QUAL;
    CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_QUAL (
      season SMALLINT UNSIGNED,
      season_type ENUM('regular','playoff'),
      player_id SMALLINT UNSIGNED,
      qual TINYINT UNSIGNED,
      PRIMARY KEY (season, season_type, player_id)
    ) ENGINE = MEMORY
      SELECT STAT.season, STAT.season_type, STAT.player_id,
             IF(IFNULL(MIN(TEAM_GP.num), 0) = 0, 0,
                IF(MIN(TEAM_GP.num) < 10, SUM(STAT.gp) >= 1,
                  ((SUM(STAT.gp) / MIN(TEAM_GP.num)) >= 0.3))) AS qual
      FROM SPORTS_NHL_PLAYERS_SEASON_GOALIES AS STAT
      LEFT JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_teamGP AS TEAM_GP
        ON (TEAM_GP.team_id = STAT.team_id)
      WHERE STAT.season = v_season
      AND   STAT.season_type = v_season_type
      AND   STAT.team_league IS NULL
      GROUP BY STAT.season, STAT.season_type, STAT.player_id;

  # - Playoff
  #   - Min 420 Min in a season
  #   -  == Rate of 105 Min per Round (except Round 1 to ensure they appear after Game 1)
  #   -     (105 Min = 6300 Sec)
  ELSE
    # Determine the number of rounds played
    SELECT COUNT(DISTINCT FLOOR(game_id / 100)) INTO v_rounds
    FROM SPORTS_NHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = v_season_type
    AND   status IS NOT NULL;

    IF v_rounds = 1 THEN
      SET v_rounds = 0;
    END IF;

    DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_QUAL;
    CREATE TEMPORARY TABLE tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_QUAL (
      season SMALLINT UNSIGNED,
      season_type ENUM('regular','playoff'),
      player_id SMALLINT UNSIGNED,
      qual TINYINT UNSIGNED,
      PRIMARY KEY (season, season_type, player_id)
    ) ENGINE = MEMORY
      SELECT season, season_type, player_id,
             SUM(minutes_played) >= (v_rounds * 6300) AS qual
      FROM SPORTS_NHL_PLAYERS_SEASON_GOALIES
      WHERE season = v_season
      AND   season_type = v_season_type
      AND   team_league IS NULL
      GROUP BY season, season_type, player_id;

  END IF;

  #
  # Write back
  #
  UPDATE tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES_QUAL AS QUAL
  JOIN tmp_SPORTS_NHL_PLAYERS_SEASON_GOALIES AS STAT
    ON (STAT.season = QUAL.season
    AND STAT.season_type = QUAL.season_type
    AND STAT.player_id = QUAL.player_id)
  SET STAT.qual = IF(STAT.team_id = '_NHL', QUAL.qual, 1); # Players always qualify for team grouped lists

END $$

DELIMITER ;

