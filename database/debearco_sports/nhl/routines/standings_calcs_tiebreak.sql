##
## Standings info for tie-breaking
##
# See: http://www.nhl.com/ice/standings.htm
DROP PROCEDURE IF EXISTS `nhl_standings_calcs_tiebreak`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_calcs_tiebreak`()
    COMMENT 'Tie-break data in NHL standings'
BEGIN

  # Note: We can only deal with absolutes at this stage, as "points in season series" tie-break needs to know the teams involved, which we do not at this stage

  # League / Conf / Div IDs
  UPDATE tmp_standings
  JOIN tmp_teams
    ON (tmp_teams.team_id = tmp_standings.team_id)
  SET tmp_standings._tb_league_id = tmp_teams.league_id,
      tmp_standings._tb_conf_id = tmp_teams.conf_id,
      tmp_standings._tb_div_id = tmp_teams.div_id;

  # Games Played / Goal Differential
  UPDATE tmp_standings SET _tb_gp = wins + loss + ot_loss, _tb_goal_diff = CAST(goals_for AS SIGNED) - CAST(goals_against AS SIGNED);

END $$

DELIMITER ;
