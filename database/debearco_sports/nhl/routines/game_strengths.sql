##
## Determine per-game periods of team strengths
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Calculate on-ice strength periods for NHL games'
BEGIN

  # Build the standard periods
  CALL nhl_game_onice_strengths_setup(v_season, v_game_type, v_game_id);

  # Process the on ice numbers
  CALL nhl_game_onice_strengths_process(v_season, v_game_type, v_game_id);

  # Process the on ice numbers
  CALL nhl_game_onice_strengths_group();
  CALL nhl_game_onice_strengths_identify();

  # And write to disk
  CALL nhl_game_onice_strengths_store(v_season, v_game_type, v_game_id);

END $$

DELIMITER ;
