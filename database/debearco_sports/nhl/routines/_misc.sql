##
## Misc NHL calcs
##

#
# Determine a season from a date
#
DROP FUNCTION IF EXISTS `_fn_nhl_season_from_date`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `_fn_nhl_season_from_date`(
  v_date DATE
) RETURNS YEAR
    DETERMINISTIC
    COMMENT 'Determine an NHL season from a date'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;

  SET v_season := YEAR(v_date);
  # Adjust for end of season being in a different calendar year
  IF MONTH(v_date) < 7 THEN
    SET v_season := v_season - 1;
  END IF;

  RETURN v_season;

END $$

DELIMITER ;

#
# Identify series date range
#
DROP PROCEDURE IF EXISTS `nhl_sequential_date_range`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_sequential_date_range` (
  v_season SMALLINT UNSIGNED,
  v_start_date DATE,
  v_end_date DATE
)
    COMMENT 'Create temp table of date ranges for NHL calcs'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_date_range;
  CREATE TEMPORARY TABLE tmp_date_range (
    season SMALLINT UNSIGNED,
    the_date DATE,
    PRIMARY KEY (season, the_date)
  ) ENGINE = MEMORY;

  WHILE v_start_date <= v_end_date DO
    INSERT INTO tmp_date_range (season, the_date) VALUES (v_season, v_start_date);
    SELECT DATE_ADD(v_start_date, INTERVAL 1 DAY) INTO v_start_date;
  END WHILE;

END $$

DELIMITER ;

#
# Identify numeric (tinyint) ranges
#
DROP PROCEDURE IF EXISTS `nhl_sequential_tinyint_range`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_sequential_tinyint_range` (
  v_start TINYINT UNSIGNED,
  v_end TINYINT UNSIGNED
)
    COMMENT 'Create temp table of TINYINT UNSIGNED ranges for NHL calcs'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_num_range;
  CREATE TEMPORARY TABLE tmp_num_range (
    num TINYINT UNSIGNED,
    PRIMARY KEY (num)
  ) ENGINE = MEMORY;

  WHILE v_start <= v_end DO
    INSERT INTO tmp_num_range (num) VALUES (v_start);
    SET v_start = v_start + 1;
  END WHILE;

END $$

DELIMITER ;
