##
## Individual build components
##
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build`()
    COMMENT 'Calculate the NHL power ranks component parts'
BEGIN

  # Temporary table our intermediate results will be stored in
  DROP TEMPORARY TABLE IF EXISTS tmp_stats;
  CREATE TEMPORARY TABLE tmp_stats (
    team_id VARCHAR(3),
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT team_id
    FROM tmp_teams;

  # Point Percentage
  CALL nhl_power_ranks_build_ptpct(10);

  # Scoring / Defence
  CALL nhl_power_ranks_build_scoring(8);

  # Special Teams
  CALL nhl_power_ranks_build_spectm(7);

  # Faceoffs
  CALL nhl_power_ranks_build_faceoffs(4);

  # Strength of Schedule
  CALL nhl_power_ranks_build_strsched(3);

END $$

DELIMITER ;

#
# Point %age
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_ptpct`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_ptpct`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NHL power ranks point percentages'
BEGIN

  # Build the data
  CALL nhl_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              SUM(IF(tmp_sched.result IN ('row', 'sow'), 2, IF(tmp_sched.result IN ('otl', 'sol'), 1, 0))) / (COUNT(tmp_sched.game_id) * 2)) AS tot_ov,
           IF(SUM(is_ft) = 0, 0,
              SUM(IF(is_ft, IF(tmp_sched.result IN ('row', 'sow'), 2, IF(tmp_sched.result IN ('otl', 'sol'), 1, 0)), 0)) / (SUM(tmp_sched.is_ft) * 2)) AS tot_ft,
           IF(SUM(is_wk) = 0, 0,
              SUM(IF(is_wk, IF(tmp_sched.result IN ('row', 'sow'), 2, IF(tmp_sched.result IN ('otl', 'sol'), 1, 0)), 0)) / (SUM(tmp_sched.is_wk) * 2)) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL nhl_power_ranks_build_process('pt_pct', 0, v_weight);

END $$

DELIMITER ;

#
# Scoring / Defence
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_scoring`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_scoring`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NHL power ranks scoring'
BEGIN

  # Build the data
  CALL nhl_power_ranks_build_schema('SMALLINT SIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              SUM(IF(tmp_sched.team_id = SCHED.home,
                     CAST(SCHED.home_score AS SIGNED) - CAST(SCHED.visitor_score AS SIGNED),
                     CAST(SCHED.visitor_score AS SIGNED) - CAST(SCHED.home_score AS SIGNED)))) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              SUM(IF(tmp_sched.is_ft,
                     IF(tmp_sched.team_id = SCHED.home,
                        CAST(SCHED.home_score AS SIGNED) - CAST(SCHED.visitor_score AS SIGNED),
                        CAST(SCHED.visitor_score AS SIGNED) - CAST(SCHED.home_score AS SIGNED)),
                     0))) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              SUM(IF(tmp_sched.is_wk,
                     IF(tmp_sched.team_id = SCHED.home,
                        CAST(SCHED.home_score AS SIGNED) - CAST(SCHED.visitor_score AS SIGNED),
                        CAST(SCHED.visitor_score AS SIGNED) - CAST(SCHED.home_score AS SIGNED)),
                     0))) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_NHL_SCHEDULE AS SCHED
      ON (SCHED.season = tmp_sched.season
      AND SCHED.game_type = 'regular'
      AND SCHED.game_id = tmp_sched.game_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL nhl_power_ranks_build_process('scoring', 0, v_weight);

END $$

DELIMITER ;

#
# Special Teams
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_spectm`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_spectm`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NHL power ranks power plays'
BEGIN

  # Build the data
  CALL nhl_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL, 0,
              SUM(IF(IFNULL(PP.pp_opps, 0) > 0, PP.pp_goals / PP.pp_opps, 0) + IF(IFNULL(PK.pp_opps, 0) > 0, (PK.pp_opps - PK.pp_goals) / PK.pp_opps, 0))) AS tot_ov,
           IF(SUM(tmp_sched.is_ft) = 0, 0,
              SUM(IF(tmp_sched.is_ft, IF(IFNULL(PP.pp_opps, 0) > 0, PP.pp_goals / PP.pp_opps, 0) + IF(IFNULL(PK.pp_opps, 0) > 0, (PK.pp_opps - PK.pp_goals) / PK.pp_opps, 0), 0))) AS tot_ft,
           IF(SUM(tmp_sched.is_wk) = 0, 0,
              SUM(IF(tmp_sched.is_wk, IF(IFNULL(PP.pp_opps, 0) > 0, PP.pp_goals / PP.pp_opps, 0) + IF(IFNULL(PK.pp_opps, 0) > 0, (PK.pp_opps - PK.pp_goals) / PK.pp_opps, 0), 0))) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_NHL_GAME_PP_STATS AS PP
      ON (PP.season = tmp_sched.season
      AND PP.game_type = 'regular'
      AND PP.game_id = tmp_sched.game_id
      AND PP.team_id = tmp_teams.team_id
      AND PP.pp_type = 'pp')
    LEFT JOIN SPORTS_NHL_GAME_PP_STATS AS PK
      ON (PK.season = tmp_sched.season
      AND PK.game_type = 'regular'
      AND PK.game_id = tmp_sched.game_id
      AND PK.team_id = tmp_teams.team_id
      AND PK.pp_type = 'pk')
    GROUP BY tmp_teams.team_id;

  # Process
  CALL nhl_power_ranks_build_process('special_teams', 0, v_weight);

END $$

DELIMITER ;

#
# Faceoffs
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_faceoffs`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_faceoffs`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NHL power ranks faceoffs'
BEGIN

  # Build the data
  CALL nhl_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL OR EVENT.game_id IS NULL, 0,
              SUM(EVENT.winner = tmp_teams.team_id) / SUM(EVENT.game_id)) AS tot_ov,
           IF(SUM(tmp_sched.is_ft AND EVENT.game_id IS NOT NULL) = 0, 0,
              SUM(IF(tmp_sched.is_ft, EVENT.winner = tmp_teams.team_id, 0)) / SUM(tmp_sched.is_ft AND EVENT.game_id IS NOT NULL)) AS tot_ft,
           IF(SUM(tmp_sched.is_wk AND EVENT.game_id IS NOT NULL) = 0, 0,
              SUM(IF(tmp_sched.is_wk, EVENT.winner = tmp_teams.team_id, 0)) / SUM(tmp_sched.is_wk AND EVENT.game_id IS NOT NULL)) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_FACEOFF AS EVENT
      ON (EVENT.season = tmp_sched.season
      AND EVENT.game_type = 'regular'
      AND EVENT.game_id = tmp_sched.game_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL nhl_power_ranks_build_process('faceoffs', 0, v_weight);

END $$

DELIMITER ;

#
# Strength of Schedule
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_strsched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_strsched`(
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Calculate the NHL power ranks strength of schedule'
BEGIN

  # Build the data
  CALL nhl_power_ranks_build_schema('DECIMAL(9,6) UNSIGNED');
  INSERT INTO tmp_data (team_id, tot_ov, tot_ft, tot_wk)
    SELECT tmp_teams.team_id,
           IF(tmp_sched.season IS NULL OR (STANDING.wins + STANDING.loss + STANDING.ot_loss) = 0, 0,
              SUM(STANDING.pts) / SUM(2 * (STANDING.wins + STANDING.loss + STANDING.ot_loss))) AS tot_ov,
           IF(SUM(is_ft) = 0 OR (STANDING.wins + STANDING.loss + STANDING.ot_loss) = 0, 0,
              SUM(IF(is_ft, STANDING.pts, 0)) / SUM(IF(is_ft, 2 * (STANDING.wins + STANDING.loss + STANDING.ot_loss), 0))) AS tot_ft,
           IF(SUM(is_wk) = 0 OR (STANDING.wins + STANDING.loss + STANDING.ot_loss) = 0, 0,
              SUM(IF(is_wk, STANDING.pts, 0)) / SUM(IF(is_wk, 2 * (STANDING.wins + STANDING.loss + STANDING.ot_loss), 0))) AS tot_wk
    FROM tmp_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_teams.team_id)
    LEFT JOIN SPORTS_NHL_STANDINGS AS STANDING
      ON (STANDING.season = tmp_sched.season
      AND STANDING.the_date = tmp_sched.pre_standing_date
      AND STANDING.team_id = tmp_sched.opp_id)
    GROUP BY tmp_teams.team_id;

  # Process
  CALL nhl_power_ranks_build_process('str_sched', 0, v_weight);

END $$

DELIMITER ;

