##
## Basic sorting
##
DROP PROCEDURE IF EXISTS `nhl_standings_ties_sort`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_ties_sort`(
  v_col VARCHAR(6)
)
    COMMENT 'Initial NHL standing calcs'
BEGIN

  # Note: Slightly different query if Conf comparison, as opposed to Div / League
  INSERT INTO tmp_standings (season, the_date, team_id, _tb_pos)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           COUNT(tmp_standings_cpB.team_id) + 1 AS _tb_pos
    FROM tmp_standings_cpA
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND IF(v_col <> 'conf',
             tmp_standings_cpB.pts > tmp_standings_cpA.pts,
             (tmp_standings_cpB.pos_div = 1 AND tmp_standings_cpA.pos_div > 1
               OR (tmp_standings_cpB.pos_div = 1 AND tmp_standings_cpA.pos_div = 1 AND tmp_standings_cpB.pts > tmp_standings_cpA.pts)
               OR (tmp_standings_cpB.pos_div > 1 AND tmp_standings_cpA.pos_div > 1 AND tmp_standings_cpB.pts > tmp_standings_cpA.pts))))
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id
  ON DUPLICATE KEY UPDATE _tb_pos = VALUES(_tb_pos);

  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

END $$

DELIMITER ;

##
## Identify which teams are tied
##
DROP PROCEDURE IF EXISTS `nhl_standings_ties_identify`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_ties_identify`()
    COMMENT 'Identify NHL standing ties'
BEGIN

  INSERT INTO tmp_standings (season, the_date, team_id, _tb_is_tied)
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id,
           tmp_standings_cpB.team_id IS NOT NULL AS _tb_is_tied
    FROM tmp_standings_cpA
    LEFT JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos = tmp_standings_cpA._tb_pos
      AND tmp_standings_cpB.team_id <> tmp_standings_cpA.team_id)
  ON DUPLICATE KEY UPDATE _tb_is_tied = VALUES(_tb_is_tied);

  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

END $$

DELIMITER ;

##
## Determine how many ties are remaining
##
DROP FUNCTION IF EXISTS `nhl_standings_ties_num`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` FUNCTION `nhl_standings_ties_num`() RETURNS SMALLINT UNSIGNED
    DETERMINISTIC
    COMMENT 'Determine remaining intermediate NHL standing ties'
BEGIN

  DECLARE v_num SMALLINT UNSIGNED;
  SELECT SUM(_tb_is_tied = 1) INTO v_num FROM tmp_standings;
  RETURN v_num;

END $$

DELIMITER ;

##
## Break ties by updating their resolved positions
##
DROP PROCEDURE IF EXISTS `nhl_standings_ties_break`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_ties_break`()
    COMMENT 'Identify NHL broken standing ties'
BEGIN

  UPDATE tmp_standings
  SET _tb_pos = _tb_pos + _tb_pos_adj
  WHERE _tb_is_tied = 1;

  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpA');
  CALL _duplicate_tmp_table('tmp_standings', 'tmp_standings_cpB');

END $$

DELIMITER ;

##
## Determine common games for our tied teams
##
DROP PROCEDURE IF EXISTS `nhl_standings_ties_common`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_ties_common`()
    COMMENT 'Identify NHL standing tie common games'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_standings_common_teams;
  CREATE TEMPORARY TABLE tmp_standings_common_teams (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    opp_team_id VARCHAR(3),
    PRIMARY KEY (season, the_date, team_id, opp_team_id)
  ) ENGINE = MEMORY
    SELECT tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id, tmp_standings_cpB.team_id AS opp_team_id
    FROM tmp_standings_cpA
    JOIN tmp_standings_cpB
      ON (tmp_standings_cpB.season = tmp_standings_cpA.season
      AND tmp_standings_cpB.the_date = tmp_standings_cpA.the_date
      AND tmp_standings_cpB._tb_join = tmp_standings_cpA._tb_join
      AND tmp_standings_cpB._tb_pos = tmp_standings_cpA._tb_pos
      AND tmp_standings_cpB.team_id <> tmp_standings_cpA.team_id)
    WHERE tmp_standings_cpA._tb_is_tied = 1
    GROUP BY tmp_standings_cpA.season, tmp_standings_cpA.the_date, tmp_standings_cpA.team_id;

  DROP TEMPORARY TABLE IF EXISTS tmp_standings_common_sched;
  CREATE TEMPORARY TABLE tmp_standings_common_sched (
    season SMALLINT UNSIGNED,
    the_date DATE,
    team_id VARCHAR(3),
    num_opp TINYINT UNSIGNED,
    sum_gp TINYINT UNSIGNED,
    sum_pts_pct DECIMAL(5,4),
    sum_gf SMALLINT SIGNED,
    sum_gd SMALLINT SIGNED,
    PRIMARY KEY (season, the_date, team_id)
  ) ENGINE = MEMORY
    SELECT tmp_standings_common_teams.season, tmp_standings_common_teams.the_date, tmp_standings_common_teams.team_id,
           COUNT(DISTINCT tmp_standings_common_teams.opp_team_id) AS num_opp,
           IFNULL(COUNT(tmp_sched.game_id), 0) AS sum_gp,
           IF(tmp_sched.game_id IS NULL, 0,
              SUM(tmp_sched.pts) / COUNT(tmp_sched.game_id)) AS sum_pts_pct,
           IFNULL(SUM(tmp_sched.goals_for), 0) AS sum_gf,
           IFNULL(SUM(CAST(tmp_sched.goals_for AS SIGNED) - CAST(tmp_sched.goals_against AS SIGNED)), 0) AS sum_gd
    FROM tmp_standings_common_teams
    LEFT JOIN tmp_sched
      ON (tmp_sched.team_id = tmp_standings_common_teams.team_id
      AND tmp_sched.opp_id = tmp_standings_common_teams.team_id
      AND tmp_sched.game_date <= tmp_standings_common_teams.the_date)
    GROUP BY tmp_standings_common_teams.season, tmp_standings_common_teams.the_date, tmp_standings_common_teams.team_id;

END $$

DELIMITER ;
