##
## Player daily game totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_players_daily_goalies`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_players_daily_goalies`()
    COMMENT 'Calculate daily game totals for NHL goalies'
BEGIN

  ##
  ## Games Played / Started / Records
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, gp, start, win, loss, ot_loss, so_loss, shutout)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           1 AS gp,
           SPORTS_NHL_GAME_LINEUP.started AS start,
           IF((SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.home_score > SPORTS_NHL_SCHEDULE.visitor_score
               AND SPORTS_NHL_SCHEDULE.home_goalie = SPORTS_NHL_GAME_LINEUP.jersey)
                OR (SPORTS_NHL_SCHEDULE.visitor = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.visitor_score > SPORTS_NHL_SCHEDULE.home_score
               AND SPORTS_NHL_SCHEDULE.visitor_goalie = SPORTS_NHL_GAME_LINEUP.jersey),
                 1, 0) AS win,
           IF((SPORTS_NHL_SCHEDULE.game_type = 'playoff'
                OR SPORTS_NHL_SCHEDULE.status = 'F')
              AND ((SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.home_score < SPORTS_NHL_SCHEDULE.visitor_score
               AND SPORTS_NHL_SCHEDULE.home_goalie = SPORTS_NHL_GAME_LINEUP.jersey)
                OR (SPORTS_NHL_SCHEDULE.visitor = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.visitor_score < SPORTS_NHL_SCHEDULE.home_score
               AND SPORTS_NHL_SCHEDULE.visitor_goalie = SPORTS_NHL_GAME_LINEUP.jersey)),
                 1, 0) AS loss,
           IF(SPORTS_NHL_SCHEDULE.game_type = 'regular'
              AND SPORTS_NHL_SCHEDULE.status = 'OT'
              AND ((SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.home_score < SPORTS_NHL_SCHEDULE.visitor_score
               AND SPORTS_NHL_SCHEDULE.home_goalie = SPORTS_NHL_GAME_LINEUP.jersey)
                OR (SPORTS_NHL_SCHEDULE.visitor = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.visitor_score < SPORTS_NHL_SCHEDULE.home_score
               AND SPORTS_NHL_SCHEDULE.visitor_goalie = SPORTS_NHL_GAME_LINEUP.jersey)),
                 1, 0) AS ot_loss,
           IF(SPORTS_NHL_SCHEDULE.game_type = 'regular'
              AND SPORTS_NHL_SCHEDULE.status = 'SO'
              AND ((SPORTS_NHL_SCHEDULE.home = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.home_score < SPORTS_NHL_SCHEDULE.visitor_score
               AND SPORTS_NHL_SCHEDULE.home_goalie = SPORTS_NHL_GAME_LINEUP.jersey)
                OR (SPORTS_NHL_SCHEDULE.visitor = SPORTS_NHL_GAME_LINEUP.team_id
               AND SPORTS_NHL_SCHEDULE.visitor_score < SPORTS_NHL_SCHEDULE.home_score
               AND SPORTS_NHL_SCHEDULE.visitor_goalie = SPORTS_NHL_GAME_LINEUP.jersey)),
                 1, 0) AS so_loss,
           0 AS shutout
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos = 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_SCHEDULE.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_SCHEDULE.game_id = SPORTS_NHL_GAME_LINEUP.game_id)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          start = VALUES(start),
                          win = VALUES(win),
                          loss = VALUES(loss),
                          ot_loss = VALUES(ot_loss),
                          so_loss = VALUES(so_loss),
                          shutout = VALUES(shutout);

  ##
  ## Goals / Shots Against
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, goals_against, shots_against, en_goals_against)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT IF(SPORTS_NHL_GAME_EVENT_GOAL.goal_type <> 'EN',
                             SPORTS_NHL_GAME_EVENT_GOAL.event_id,
                             NULL)) AS goals_against,
           COUNT(DISTINCT IF(SPORTS_NHL_GAME_EVENT_GOAL.goal_type <> 'EN',
                             SPORTS_NHL_GAME_EVENT_GOAL.event_id,
                             NULL))
             + COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_SHOT.event_id) AS shots_against,
           NULL AS en_goals_against
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos = 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_GOAL
      ON (SPORTS_NHL_GAME_EVENT_GOAL.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_GOAL.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_GOAL.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_GOAL.on_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_GOAL.on_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_SHOT
      ON (SPORTS_NHL_GAME_EVENT_SHOT.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_SHOT.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_SHOT.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_SHOT.on_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_SHOT.on_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE goals_against = VALUES(goals_against),
                          shots_against = VALUES(shots_against),
                          en_goals_against = VALUES(en_goals_against);

  ##
  ## Shutout?  (We may be overriding what we set above)
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, shutout)
    SELECT ME.season,
           ME.player_id,
           ME.game_type,
           ME.game_id,
           ME.team_id,
           1 AS shutout
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP AS ME
      ON (ME.season = tmp_game_list.season
      AND ME.game_type = tmp_game_list.game_type
      AND ME.game_id = tmp_game_list.game_id
      AND ME.pos = 'G')
    JOIN tmp_game_players AS MY_TOI
      ON (MY_TOI.season = ME.season
      AND MY_TOI.game_type = ME.game_type
      AND MY_TOI.game_id = ME.game_id
      AND MY_TOI.team_id = ME.team_id
      AND MY_TOI.jersey = ME.jersey)
    LEFT JOIN SPORTS_NHL_GAME_LINEUP AS OTHER_GOALIE
      ON (OTHER_GOALIE.season = ME.season
      AND OTHER_GOALIE.game_type = ME.game_type
      AND OTHER_GOALIE.game_id = ME.game_id
      AND OTHER_GOALIE.team_id = ME.team_id
      AND OTHER_GOALIE.pos = ME.pos)
    LEFT JOIN tmp_game_players_cp AS OTHER_GOALIE_TOI
      ON (OTHER_GOALIE_TOI.season = OTHER_GOALIE.season
      AND OTHER_GOALIE_TOI.game_type = OTHER_GOALIE.game_type
      AND OTHER_GOALIE_TOI.game_id = OTHER_GOALIE.game_id
      AND OTHER_GOALIE_TOI.team_id = OTHER_GOALIE.team_id
      AND OTHER_GOALIE_TOI.jersey = OTHER_GOALIE.jersey)
    LEFT JOIN SPORTS_NHL_SCHEDULE
      ON (SPORTS_NHL_SCHEDULE.season = ME.season
      AND SPORTS_NHL_SCHEDULE.game_type = ME.game_type
      AND SPORTS_NHL_SCHEDULE.game_id = ME.game_id)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    AND   OTHER_GOALIE_TOI.jersey IS NULL
    AND   ((SPORTS_NHL_SCHEDULE.home = ME.team_id
        AND SPORTS_NHL_SCHEDULE.home_score > SPORTS_NHL_SCHEDULE.visitor_score
        AND SPORTS_NHL_SCHEDULE.home_goalie = ME.jersey)
        OR (SPORTS_NHL_SCHEDULE.visitor = ME.team_id
        AND SPORTS_NHL_SCHEDULE.visitor_score > SPORTS_NHL_SCHEDULE.home_score
        AND SPORTS_NHL_SCHEDULE.visitor_goalie = ME.jersey))
    AND   IF(SPORTS_NHL_SCHEDULE.home = ME.team_id,
             SPORTS_NHL_SCHEDULE.visitor_score,
             SPORTS_NHL_SCHEDULE.home_score) = 0
    GROUP BY ME.season,
             ME.game_type,
             ME.game_id,
             ME.player_id
  ON DUPLICATE KEY UPDATE shutout = VALUES(shutout);

  ##
  ## Minutes Played
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, minutes_played)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           tmp_game_players.toi AS minutes_played
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos = 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE minutes_played = VALUES(minutes_played);

  ##
  ## Shootouts
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, so_goals_against, so_shots_against)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT IF(SPORTS_NHL_GAME_EVENT_SHOOTOUT.status = 'goal',
                             SPORTS_NHL_GAME_EVENT_SHOOTOUT.event_id,
                             NULL)) AS so_goals_against,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_SHOOTOUT.event_id) AS so_shots_against
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos = 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_SHOOTOUT
      ON (SPORTS_NHL_GAME_EVENT_SHOOTOUT.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.on_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_SHOOTOUT.on_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE so_goals_against = VALUES(so_goals_against),
                          so_shots_against = VALUES(so_shots_against);

  ##
  ## Scoring
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, goals, assists)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT
                 IF(GOALS_FOR.scorer = SPORTS_NHL_GAME_LINEUP.jersey,
                    GOALS_FOR.event_id, NULL)) AS goals,
           COUNT(DISTINCT
                 IF(GOALS_FOR.assist_1 = SPORTS_NHL_GAME_LINEUP.jersey
                    OR GOALS_FOR.assist_2 = SPORTS_NHL_GAME_LINEUP.jersey,
                    GOALS_FOR.event_id, NULL)) AS assists
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos = 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_GOAL AS GOALS_FOR
      ON (GOALS_FOR.season = SPORTS_NHL_GAME_LINEUP.season
      AND GOALS_FOR.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND GOALS_FOR.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND GOALS_FOR.by_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND (GOALS_FOR.scorer = SPORTS_NHL_GAME_LINEUP.jersey
        OR GOALS_FOR.assist_1 = SPORTS_NHL_GAME_LINEUP.jersey
        OR GOALS_FOR.assist_2 = SPORTS_NHL_GAME_LINEUP.jersey))
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE goals = VALUES(goals),
                          assists = VALUES(assists);

  ##
  ## PIMs / Pens Taken
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, pims, pens_taken)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           SUM(IF(SPORTS_NHL_GAME_EVENT_PENALTY.pims IS NOT NULL, SPORTS_NHL_GAME_EVENT_PENALTY.pims, 0)) AS pims,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_PENALTY.event_id) AS pens_taken
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos = 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_PENALTY
      ON (SPORTS_NHL_GAME_EVENT_PENALTY.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE pims = VALUES(pims),
                          pens_taken = VALUES(pens_taken);

  ##
  ## Pens Drawn
  ##
  INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, pens_drawn)
    SELECT SPORTS_NHL_GAME_LINEUP.season,
           SPORTS_NHL_GAME_LINEUP.player_id,
           SPORTS_NHL_GAME_LINEUP.game_type,
           SPORTS_NHL_GAME_LINEUP.game_id,
           SPORTS_NHL_GAME_LINEUP.team_id,
           COUNT(DISTINCT SPORTS_NHL_GAME_EVENT_PENALTY.event_id) AS pens_drawn
    FROM tmp_game_list
    LEFT JOIN SPORTS_NHL_GAME_SUMMARISED
      ON (SPORTS_NHL_GAME_SUMMARISED.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_SUMMARISED.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_SUMMARISED.game_id = tmp_game_list.game_id)
    JOIN SPORTS_NHL_GAME_LINEUP
      ON (SPORTS_NHL_GAME_LINEUP.season = tmp_game_list.season
      AND SPORTS_NHL_GAME_LINEUP.game_type = tmp_game_list.game_type
      AND SPORTS_NHL_GAME_LINEUP.game_id = tmp_game_list.game_id
      AND SPORTS_NHL_GAME_LINEUP.pos = 'G')
    JOIN tmp_game_players
      ON (tmp_game_players.season = SPORTS_NHL_GAME_LINEUP.season
      AND tmp_game_players.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND tmp_game_players.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND tmp_game_players.team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND tmp_game_players.jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    LEFT JOIN SPORTS_NHL_GAME_EVENT_PENALTY
      ON (SPORTS_NHL_GAME_EVENT_PENALTY.season = SPORTS_NHL_GAME_LINEUP.season
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_type = SPORTS_NHL_GAME_LINEUP.game_type
      AND SPORTS_NHL_GAME_EVENT_PENALTY.game_id = SPORTS_NHL_GAME_LINEUP.game_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.drawn_team_id = SPORTS_NHL_GAME_LINEUP.team_id
      AND SPORTS_NHL_GAME_EVENT_PENALTY.drawn_jersey = SPORTS_NHL_GAME_LINEUP.jersey)
    WHERE SPORTS_NHL_GAME_SUMMARISED.season IS NULL
    GROUP BY SPORTS_NHL_GAME_LINEUP.season,
             SPORTS_NHL_GAME_LINEUP.game_type,
             SPORTS_NHL_GAME_LINEUP.game_id,
             SPORTS_NHL_GAME_LINEUP.player_id
  ON DUPLICATE KEY UPDATE pens_drawn = VALUES(pens_drawn);

END $$

DELIMITER ;

