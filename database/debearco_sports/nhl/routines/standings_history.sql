##
## Copy in to team history
##
DROP PROCEDURE IF EXISTS `nhl_standings_history`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_standings_history`()
    COMMENT 'Copy NHL team standings to the history table'
BEGIN

  DECLARE v_season SMALLINT UNSIGNED;
  DECLARE v_the_date DATE;
  DECLARE v_season_complete TINYINT UNSIGNED;

  SELECT DISTINCT season INTO v_season
  FROM tmp_date_list;

  SELECT MAX(the_date) INTO v_the_date
  FROM SPORTS_NHL_STANDINGS
  WHERE season = v_season;

  SELECT SUM(status IS NULL) > 0 INTO v_season_complete
  FROM SPORTS_NHL_SCHEDULE
  WHERE season = v_season
  AND   game_type = 'regular';

  INSERT INTO SPORTS_NHL_TEAMS_HISTORY_REGULAR (season, team_id, season_half, wins, loss, ties, ot_loss, pts, pos, league_champ)
    SELECT STANDINGS.season, STANDINGS.team_id, 0 AS season_half,
           STANDINGS.wins, STANDINGS.loss, NULL AS ties, STANDINGS.ot_loss, STANDINGS.pts,
           STANDINGS.pos_div AS pos,
           (STANDINGS.pos_league = 1 AND v_season_complete = 1) AS league_champ
    FROM SPORTS_NHL_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_NHL_STANDINGS AS STANDINGS
      ON (STANDINGS.season = v_season
      AND STANDINGS.the_date = v_the_date
      AND STANDINGS.team_id = TEAM_DIV.team_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099)
  ON DUPLICATE KEY UPDATE wins = VALUES(wins),
                          loss = VALUES(loss),
                          ot_loss = VALUES(ot_loss),
                          pts = VALUES(pts),
                          pos = VALUES(pos),
                          league_champ = VALUES(league_champ);

  ALTER TABLE SPORTS_NHL_TEAMS_HISTORY_REGULAR ORDER BY team_id, season;

END $$

DELIMITER ;
