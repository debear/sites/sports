##
## Playoff matchups (before official series confirmation, i.e., round_code is guessed)
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups`(
  v_season SMALLINT UNSIGNED
)
    COMMENT 'Guesstimate NHL playoff matchups, before official confirmation'
proc: BEGIN

  DECLARE v_round_num TINYINT UNSIGNED;
  DECLARE v_series_date DATE;
  DECLARE v_round_rows TINYINT UNSIGNED;
  DECLARE v_round_started TINYINT UNSIGNED;
  DECLARE v_round_complete TINYINT UNSIGNED;

  DECLARE v_counter TINYINT UNSIGNED DEFAULT 0;

  # Identify the conferences to loop through
  DECLARE v_conf_id TINYINT UNSIGNED;
  DECLARE v_done TINYINT UNSIGNED DEFAULT 0;

  DECLARE cur_conf CURSOR FOR
    SELECT DISTINCT DIVISION.parent_id AS conf_id
    FROM SPORTS_NHL_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_NHL_GROUPINGS AS DIVISION
      ON (DIVISION.grouping_id = TEAM_DIV.grouping_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099);
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_done = 1;

  # We will skip this for the 2019 Playoffs, in favour of a manual approach
  IF v_season = 2019 THEN
    LEAVE proc;
  END IF;

  # Determine the round to process
  DROP TEMPORARY TABLE IF EXISTS tmp_series_rounds_stage0;
  CREATE TEMPORARY TABLE tmp_series_rounds_stage0 (
    round_num TINYINT UNSIGNED,
    last_date DATE,
    num_rows TINYINT UNSIGNED,
    started TINYINT UNSIGNED,
    complete TINYINT UNSIGNED,
    PRIMARY KEY (round_num)
  ) ENGINE = MEMORY
    SELECT SUBSTRING(round_code, 1, 1) AS round_num, MAX(the_date) AS last_date, COUNT(DISTINCT the_date) AS num_rows, NULL AS started, NULL AS complete
    FROM SPORTS_NHL_PLAYOFF_SERIES
    WHERE season = v_season
    GROUP BY round_num;
  DROP TEMPORARY TABLE IF EXISTS tmp_series_rounds;
  CREATE TEMPORARY TABLE tmp_series_rounds LIKE tmp_series_rounds_stage0;
  INSERT INTO tmp_series_rounds
    SELECT ROUNDS.round_num, ROUNDS.last_date, ROUNDS.num_rows, MAX(SERIES.higher_games + SERIES.lower_games) AS started, MIN(4 IN (SERIES.higher_games, SERIES.lower_games)) AS complete
    FROM tmp_series_rounds_stage0 AS ROUNDS
    JOIN SPORTS_NHL_PLAYOFF_SERIES AS SERIES
      ON (SERIES.season = v_season
      AND SUBSTRING(SERIES.round_code, 1, 1) = ROUNDS.round_num
      AND SERIES.the_date = ROUNDS.last_date)
    GROUP BY ROUNDS.round_num;

  # Shortcut our series info
  DROP TEMPORARY TABLE IF EXISTS tmp_series;
  CREATE TEMPORARY TABLE tmp_series LIKE SPORTS_NHL_PLAYOFF_SERIES;
  ALTER TABLE tmp_series ENGINE = MEMORY,
                         ADD COLUMN round_num TINYINT UNSIGNED,
                         ADD COLUMN complete TINYINT UNSIGNED,
                         ADD COLUMN winner_conf_id TINYINT UNSIGNED,
                         ADD COLUMN winner_seed TINYINT UNSIGNED,
                         ADD COLUMN loser_conf_id TINYINT UNSIGNED,
                         ADD COLUMN loser_seed TINYINT UNSIGNED;
  INSERT INTO tmp_series
    SELECT SERIES.*,
          SUBSTRING(SERIES.round_code, 1, 1) AS round_num,
          (4 IN (SERIES.higher_games, SERIES.lower_games)) AS complete,
          IF(SERIES.higher_games = 4, SERIES.higher_conf_id,
             IF(SERIES.lower_games = 4, SERIES.lower_conf_id, 0)) AS winner_conf_id,
          IF(SERIES.higher_games = 4, SERIES.higher_seed,
             IF(SERIES.lower_games = 4, SERIES.lower_seed, 0)) AS winner_seed,
          IF(SERIES.higher_games = 4, SERIES.lower_conf_id,
             IF(SERIES.lower_games = 4, SERIES.higher_conf_id, 0)) AS loser_conf_id,
          IF(SERIES.higher_games = 4, SERIES.lower_seed,
             IF(SERIES.lower_games = 4, SERIES.higher_seed, 0)) AS loser_seed
    FROM tmp_series_rounds
    JOIN SPORTS_NHL_PLAYOFF_SERIES AS SERIES
      ON (SERIES.season = v_season
      AND SERIES.the_date = tmp_series_rounds.last_date
      AND SUBSTRING(SERIES.round_code, 1, 1) = tmp_series_rounds.round_num);

  # Get the current round info out
  SELECT round_num, last_date, num_rows, started, complete
    INTO v_round_num, v_series_date, v_round_rows, v_round_started, v_round_complete
  FROM tmp_series_rounds
  ORDER BY last_date DESC
  LIMIT 1;

  # If no matches (start of playoffs...), base from end of season
  IF v_done OR (v_round_num = 1 AND v_round_started = 0) THEN
    # In some scenarios we may already have the start of the playoff schedule, so prioritise that date
    SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) INTO v_series_date
    FROM SPORTS_NHL_SCHEDULE
    WHERE season = v_season
    AND   game_type = 'playoff';
    # But if not, fall back to the day-after-regular-season
    IF v_series_date IS NULL THEN
      SELECT DATE_ADD(MAX(game_date), INTERVAL 1 DAY) INTO v_series_date
      FROM SPORTS_NHL_SCHEDULE
      WHERE season = v_season
      AND   game_type = 'regular';
    END IF;
    # Ensure non-null values
    SET v_round_num := 1;
    SET v_round_complete := 0;
    # Re-set our CONTINUE HANDLER checker
    SET v_done := 0;

  # We'll only process after completed round - if current round is not, do not continue
  ELSEIF v_round_complete = 0 AND (v_round_num > 1 OR v_round_rows > 1) THEN
    LEAVE proc;

  # Ensure the round and series date are correct
  ELSE
    # Get the initial series date
    IF v_round_num = 1 AND v_round_complete = 0 THEN
      # In some scenarios we may already have the start of the playoff schedule, so prioritise that date
      SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) INTO v_series_date
      FROM SPORTS_NHL_SCHEDULE
      WHERE season = v_season
      AND   game_type = 'playoff';
      # But if not, fall back to the day-after-regular-season
      IF v_series_date IS NULL THEN
        SELECT DATE_ADD(MAX(game_date), INTERVAL 1 DAY) INTO v_series_date
        FROM SPORTS_NHL_SCHEDULE
        WHERE season = v_season
        AND   game_type = 'regular';
      END IF;
    # Between rounds, the initial series date is the day after the last game of the previous series
    ELSE
      SET v_series_date := DATE_ADD(v_series_date, INTERVAL 1 DAY);
    END IF;

    # If the last round was completed, move on to the next round
    IF v_round_complete = 1 THEN
      SET v_round_num := (v_round_num + 1);
    END IF;

    # Catch end of playoff scenario
    IF v_round_num > 4 THEN
      LEAVE proc;
    END IF;
  END IF;

  # Clear previous run, as well as any temporary calcs
  DELETE FROM SPORTS_NHL_PLAYOFF_SERIES WHERE season = v_season AND SUBSTRING(round_code, 1, 1) = v_round_num;
  DELETE FROM SPORTS_NHL_PLAYOFF_SERIES
  WHERE season = v_season
  AND   (SUBSTRING(round_code, 1, 1) = v_round_num
      OR round_code IN ('1', '2', '3', '4', '5', '6', '7', '8',
                        '26', '27', '28', '29',
                        '38', '39',
                        '49'));

  # Seed the remaining teams
  IF v_season <= 2012 THEN
    CALL nhl_playoff_matchups_seed_conference(v_season, v_round_num);
  ELSE
    CALL nhl_playoff_matchups_seed_divisional(v_season, v_round_num);
  END IF;

  OPEN cur_conf;
  loop_conf: LOOP

    FETCH cur_conf INTO v_conf_id;
    IF v_done = 1 THEN LEAVE loop_conf; END IF;

    # Conf Quarter / Div Semi
    IF v_round_num = 1 THEN
      # Calculate, the version of which depends on if we are before or after the 2013/14 realignment
      IF v_season <= 2012 THEN
        CALL nhl_playoff_matchups_conference_quarter(v_season, v_series_date, v_conf_id, v_counter);
      ELSEIF v_season = 2020 THEN
        # There was no wildcard in 2020/21, the four teams were purely divisional
        CALL nhl_playoff_matchups_division_semi(v_season, v_series_date, v_conf_id, v_counter);
      ELSE
        CALL nhl_playoff_matchups_division_semi_wildcard(v_season, v_series_date, v_conf_id, v_counter);
      END IF;

    # Conf Semi / Div Final
    ELSEIF v_round_num = 2 THEN
      # Calculate, the version of which depends on if we are before or after the 2013/14 realignment
      IF v_season <= 2012 THEN
        CALL nhl_playoff_matchups_conference_semi(v_season, v_series_date, v_conf_id, v_counter);
      ELSE
        CALL nhl_playoff_matchups_division_final(v_season, v_series_date, v_conf_id, v_counter);
      END IF;

    # Conf Final
    ELSEIF v_round_num = 3 THEN
      CALL nhl_playoff_matchups_conference_final(v_season, v_series_date, v_conf_id, v_counter);

    # Stanley Cup - only run once...
    ELSEIF v_counter = 0 THEN
      CALL nhl_playoff_matchups_stanley_cup(v_season, v_series_date, v_counter);

    END IF;

  END LOOP loop_conf;
  CLOSE cur_conf;

  # Final ordering
  ALTER TABLE SPORTS_NHL_PLAYOFF_SERIES ORDER BY season, the_date, round_code;

END $$

DELIMITER ;

##
## Divison Semis (No Wildcard)
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_division_semi`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_division_semi`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup calcs for division semi-finals'
BEGIN

  DECLARE v_div_id TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  #
  # First Division
  #
  SELECT div_id INTO v_div_id FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY div_order LIMIT 1;

  # Matchup 1: Division Winner/Leader v 4th
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 4;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);
  # Matchup 2: Division 2nd v 3rd
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 2;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 3;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  #
  # Second Division
  #
  SELECT div_id INTO v_div_id FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY div_order DESC LIMIT 1;

  # Matchup 3: Division 2nd v 3rd
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 2;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 3;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 3 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  # Matchup 4: Division Winner/Leader v 4th
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 4;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 4 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  # Increment the counter
  SET v_counter = v_counter + 4;

END $$

DELIMITER ;

##
## Divison Semis (with Wildcard)
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_division_semi_wildcard`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_division_semi_wildcard`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup calcs for division semi-finals'
BEGIN

  DECLARE v_div_id TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  #
  # First Division
  #
  SELECT div_id INTO v_div_id FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY div_order LIMIT 1;

  # Matchup 1: Division Winner/Leader v Wildcard
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 1;
  IF v_seed_a = 1 THEN
    SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND qual = 'wc' ORDER BY pos_conf DESC LIMIT 1;
  ELSE
    SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND qual = 'wc' ORDER BY pos_conf ASC LIMIT 1;
  END IF;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);
  # Matchup 2: Division 2nd v 3rd
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 2;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 3;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  #
  # Second Division
  #
  SELECT div_id INTO v_div_id FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY div_order DESC LIMIT 1;

  # Matchup 3: Division 2nd v 3rd
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 2;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 3;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 3 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  # Matchup 4: Division Winner/Leader v Wildcard
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND div_id = v_div_id AND pos_div = 1;
  IF v_seed_a = 1 THEN
    SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND qual = 'wc' ORDER BY pos_conf DESC LIMIT 1;
  ELSE
    SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND qual = 'wc' ORDER BY pos_conf ASC LIMIT 1;
  END IF;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 4 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  # Increment the counter
  SET v_counter = v_counter + 4;

END $$

DELIMITER ;

##
## Division Final
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_division_final`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_division_final`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup calcs for division finals'
BEGIN

  DECLARE v_div_a TINYINT UNSIGNED;
  DECLARE v_div_b TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  # Determine the order of the two divs and reseed
  SELECT reseed_div_id INTO v_div_a FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 1 ORDER BY div_order ASC LIMIT 1;
  SELECT reseed_div_id INTO v_div_b FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 1 ORDER BY div_order DESC LIMIT 1;

  # First Div Matchup
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed_div_id = v_div_a AND reseed = 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed_div_id = v_div_a AND reseed = 2;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(25 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  # Second Div Matchup
  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed_div_id = v_div_b AND reseed = 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed_div_id = v_div_b AND reseed = 2;
  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(25 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);

  # Increment the counter
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

##
## Conference Quarters
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_conference_quarter`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_conference_quarter`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup calcs for conference quarter-finals'
BEGIN

  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(v_counter + 1 AS CHAR(2)), v_conf_id, 1, v_conf_id, 8, 0, 0),
    (v_season, v_series_date, CAST(v_counter + 2 AS CHAR(2)), v_conf_id, 2, v_conf_id, 7, 0, 0),
    (v_season, v_series_date, CAST(v_counter + 3 AS CHAR(2)), v_conf_id, 3, v_conf_id, 6, 0, 0),
    (v_season, v_series_date, CAST(v_counter + 4 AS CHAR(2)), v_conf_id, 4, v_conf_id, 5, 0, 0);
  SET v_counter = v_counter + 4;

END $$

DELIMITER ;

##
## Conference Semi Final
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_conference_semi`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_conference_semi`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup calcs for conference semi-finals'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;
  DECLARE v_seed_c TINYINT UNSIGNED;
  DECLARE v_seed_d TINYINT UNSIGNED;

  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 2;
  SELECT pos_conf INTO v_seed_c FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 3;
  SELECT pos_conf INTO v_seed_d FROM tmp_seeds WHERE conf_id = v_conf_id AND reseed = 4;

  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(25 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_d, 0, 0),
    (v_season, v_series_date, CAST(25 + v_counter + 2 AS CHAR(2)), v_conf_id, v_seed_b, v_conf_id, v_seed_c, 0, 0);
  SET v_counter = v_counter + 2;

END $$

DELIMITER ;

##
## Conference Final
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_conference_final`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_conference_final`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  IN    v_conf_id TINYINT UNSIGNED,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup calcs for conference finals'
BEGIN

  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT pos_conf INTO v_seed_a FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf  ASC LIMIT 1;
  SELECT pos_conf INTO v_seed_b FROM tmp_seeds WHERE conf_id = v_conf_id ORDER BY pos_conf DESC LIMIT 1;

  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, CAST(37 + v_counter + 1 AS CHAR(2)), v_conf_id, v_seed_a, v_conf_id, v_seed_b, 0, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;

##
## Stanley Cup Final
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_stanley_cup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_stanley_cup`(
  IN    v_season SMALLINT UNSIGNED,
  IN    v_series_date DATE,
  INOUT v_counter TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup calcs for Stanley Cup final'
BEGIN

  DECLARE v_conf_a TINYINT UNSIGNED;
  DECLARE v_seed_a TINYINT UNSIGNED;
  DECLARE v_conf_b TINYINT UNSIGNED;
  DECLARE v_seed_b TINYINT UNSIGNED;

  SELECT conf_id, pos_conf INTO v_conf_a, v_seed_a FROM tmp_seeds ORDER BY pos_league  ASC LIMIT 1;
  SELECT conf_id, pos_conf INTO v_conf_b, v_seed_b FROM tmp_seeds ORDER BY pos_league DESC LIMIT 1;

  INSERT INTO SPORTS_NHL_PLAYOFF_SERIES (season, the_date, round_code, higher_conf_id, higher_seed, lower_conf_id, lower_seed, higher_games, lower_games) VALUES
    (v_season, v_series_date, '41', v_conf_a, v_seed_a, v_conf_b, v_seed_b, 0, 0);
  SET v_counter = v_counter + 1;

END $$

DELIMITER ;

##
## Seeding Calcs
##

#
# Conference-based
#
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_seed_conference`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_seed_conference`(
  v_season SMALLINT UNSIGNED,
  v_round_num TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup conference seeding'
BEGIN

  DECLARE v_standing_date DATE;
  SELECT DATE(MAX(the_date)) INTO v_standing_date FROM SPORTS_NHL_STANDINGS WHERE season = v_season;

  # Build our temp table of details
  DROP TEMPORARY TABLE IF EXISTS tmp_seeds;
  CREATE TEMPORARY TABLE tmp_seeds (
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    pos_div TINYINT UNSIGNED,
    team_id VARCHAR(3),
    reseed TINYINT UNSIGNED,
    PRIMARY KEY (conf_id, pos_conf)
  ) ENGINE = MEMORY
    SELECT DIVN.parent_id AS conf_id, DIVN.grouping_id AS div_id, STANDINGS.pos_conf, STANDINGS.pos_league, STANDINGS.pos_div, STANDINGS.team_id, STANDINGS.pos_conf AS reseed
    FROM SPORTS_NHL_TEAMS_GROUPINGS AS TEAM_DIVN
    JOIN SPORTS_NHL_GROUPINGS AS DIVN
      ON (DIVN.grouping_id = TEAM_DIVN.grouping_id)
    JOIN SPORTS_NHL_STANDINGS AS STANDINGS
      ON (STANDINGS.season = v_season
      AND STANDINGS.the_date = v_standing_date
      AND STANDINGS.team_id = TEAM_DIVN.team_id
      AND STANDINGS.pos_conf <= 8)
    WHERE v_season BETWEEN TEAM_DIVN.season_from AND IFNULL(TEAM_DIVN.season_to, 2099);

  # Ensure only winners progress, if processing beyond the first round
  IF v_round_num > 1 THEN
    DELETE tmp_seeds.*
    FROM tmp_seeds
    JOIN tmp_series
      ON (tmp_series.season = v_season
      AND tmp_series.round_num < v_round_num
      AND tmp_series.complete = 1
      AND tmp_series.loser_conf_id = tmp_seeds.conf_id
      AND tmp_series.loser_seed = tmp_seeds.pos_conf);
  END IF;

  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpA');
  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpB');
  INSERT INTO tmp_seeds (conf_id, pos_conf, reseed)
    SELECT tmp_seeds_cpA.conf_id, tmp_seeds_cpA.pos_conf, COUNT(tmp_seeds_cpB.team_id) + 1
    FROM tmp_seeds_cpA
    LEFT JOIN tmp_seeds_cpB
      ON (tmp_seeds_cpB.conf_id = tmp_seeds_cpA.conf_id
      AND tmp_seeds_cpB.pos_conf < tmp_seeds_cpA.pos_conf)
    GROUP BY tmp_seeds_cpA.team_id
  ON DUPLICATE KEY UPDATE reseed = VALUES(reseed);

END $$

DELIMITER ;

##
## Division-based
##
DROP PROCEDURE IF EXISTS `nhl_playoff_matchups_seed_divisional`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_playoff_matchups_seed_divisional`(
  v_season SMALLINT UNSIGNED,
  v_round_num TINYINT UNSIGNED
)
    COMMENT 'NHL playoff matchup divisional seeding'
BEGIN

  DECLARE v_standing_date DATE;
  SELECT DATE(MAX(the_date)) INTO v_standing_date FROM SPORTS_NHL_STANDINGS WHERE season = v_season;

  # Build our temp table of details
  DROP TEMPORARY TABLE IF EXISTS tmp_seeds;
  CREATE TEMPORARY TABLE tmp_seeds (
    conf_id TINYINT UNSIGNED,
    pos_conf TINYINT UNSIGNED,
    pos_league TINYINT UNSIGNED,
    pos_div TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    div_order TINYINT UNSIGNED,
    qual ENUM('divw', 'divq', 'wc'),
    team_id VARCHAR(3),
    reseed_div_id TINYINT UNSIGNED,
    reseed TINYINT UNSIGNED,
    PRIMARY KEY (conf_id, pos_conf)
  ) ENGINE = MEMORY
    SELECT DIVN.parent_id AS conf_id, STANDINGS.pos_conf, STANDINGS.pos_league, STANDINGS.pos_div, DIVN.grouping_id AS div_id, DIVN.order AS div_order,
           IF(STANDINGS.status_code IN ('y', 'z', '*') OR STANDINGS.pos_div = 1, 'divw',
              IF(STANDINGS.status_code = 'x' OR STANDINGS.pos_div IN (2,3), 'divq', 'wc')) AS qual,
           STANDINGS.team_id, DIVN.grouping_id AS reseed_div_id, IF(STANDINGS.pos_div < 4, STANDINGS.pos_div, 4) AS reseed
    FROM SPORTS_NHL_TEAMS_GROUPINGS AS TEAM_DIVN
    JOIN SPORTS_NHL_GROUPINGS AS DIVN
      ON (DIVN.grouping_id = TEAM_DIVN.grouping_id)
    JOIN SPORTS_NHL_STANDINGS AS STANDINGS
      ON (STANDINGS.season = v_season
      AND STANDINGS.the_date = v_standing_date
      AND STANDINGS.team_id = TEAM_DIVN.team_id
      AND (STANDINGS.status_code IS NOT NULL
        OR STANDINGS.pos_div < 4
        OR (STANDINGS.pos_div < 6 AND STANDINGS.pos_conf < 9)))
    WHERE v_season BETWEEN TEAM_DIVN.season_from AND IFNULL(TEAM_DIVN.season_to, 2099);

  # Update division of the (reseeded) 4th seeds (the wildcards) as being relative to the division winners
  # (Though the 2020/21 season was purely divisional)
  IF v_season <> 2020 THEN
    CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpA');
    CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpB');
    CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpC');
    # Higher Div Winner / Lower Wildcard
    INSERT INTO tmp_seeds (conf_id, pos_conf, reseed_div_id)
      SELECT tmp_seeds_cpA.conf_id, tmp_seeds_cpA.pos_conf, tmp_seeds_cpB.reseed_div_id
      FROM tmp_seeds_cpA
      JOIN tmp_seeds_cpB
        ON (tmp_seeds_cpB.conf_id = tmp_seeds_cpA.conf_id
        AND tmp_seeds_cpB.pos_conf = 1)
      JOIN tmp_seeds_cpC
        ON (tmp_seeds_cpC.conf_id = tmp_seeds_cpA.conf_id
        AND tmp_seeds_cpC.pos_conf < tmp_seeds_cpA.pos_conf
        AND tmp_seeds_cpC.reseed = tmp_seeds_cpA.reseed)
      WHERE tmp_seeds_cpA.reseed = 4
    ON DUPLICATE KEY UPDATE reseed_div_id = VALUES(reseed_div_id);
    # Lower Div Winner / higher Wildcard
    INSERT INTO tmp_seeds (conf_id, pos_conf, reseed_div_id)
      SELECT tmp_seeds_cpA.conf_id, tmp_seeds_cpA.pos_conf, tmp_seeds_cpB.reseed_div_id
      FROM tmp_seeds_cpA
      JOIN tmp_seeds_cpB
        ON (tmp_seeds_cpB.conf_id = tmp_seeds_cpA.conf_id
        AND tmp_seeds_cpB.pos_conf = 2)
      JOIN tmp_seeds_cpC
        ON (tmp_seeds_cpC.conf_id = tmp_seeds_cpA.conf_id
        AND tmp_seeds_cpC.pos_conf > tmp_seeds_cpA.pos_conf
        AND tmp_seeds_cpC.reseed = tmp_seeds_cpA.reseed)
      WHERE tmp_seeds_cpA.reseed = 4
    ON DUPLICATE KEY UPDATE reseed_div_id = VALUES(reseed_div_id);
  END IF;

  # Ensure only winners progress, if processing beyond the first round
  IF v_round_num > 1 THEN
    DELETE tmp_seeds.*
    FROM tmp_seeds
    JOIN tmp_series
      ON (tmp_series.season = v_season
      AND tmp_series.round_num < v_round_num
      AND tmp_series.complete = 1
      AND tmp_series.loser_conf_id = tmp_seeds.conf_id
      AND tmp_series.loser_seed = tmp_seeds.pos_conf);
  END IF;

  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpA');
  CALL _duplicate_tmp_table('tmp_seeds', 'tmp_seeds_cpB');
  INSERT INTO tmp_seeds (conf_id, pos_conf, reseed)
    SELECT tmp_seeds_cpA.conf_id, tmp_seeds_cpA.pos_conf, COUNT(tmp_seeds_cpB.team_id) + 1
    FROM tmp_seeds_cpA
    LEFT JOIN tmp_seeds_cpB
      ON (tmp_seeds_cpB.conf_id = tmp_seeds_cpA.conf_id
      AND tmp_seeds_cpB.reseed_div_id = tmp_seeds_cpA.reseed_div_id
      AND tmp_seeds_cpB.reseed < tmp_seeds_cpA.reseed)
    GROUP BY tmp_seeds_cpA.conf_id, tmp_seeds_cpA.pos_conf
  ON DUPLICATE KEY UPDATE reseed = VALUES(reseed);

END $$

DELIMITER ;
