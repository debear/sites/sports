##
## Do some setup work
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths_setup`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths_setup`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Prepare the table we will use for NHL game strength calcs'
BEGIN

  DECLARE v_game_status CHAR(5);
  DECLARE v_end_period TINYINT UNSIGNED;
  DECLARE v_end_time SMALLINT UNSIGNED; # Converted to seconds...

  # Build our temporary tables
  DROP TEMPORARY TABLE IF EXISTS `tmp_times`;
  CREATE TEMPORARY TABLE `tmp_times` (
    `period` TINYINT UNSIGNED,
    `time_num` SMALLINT UNSIGNED,
    `time_fmt` TIME,
    `is_period_end` TINYINT UNSIGNED,
    `home_skaters` TINYINT UNSIGNED,
    `home_goalie` TINYINT UNSIGNED,
    `visitor_skaters` TINYINT UNSIGNED,
    `visitor_goalie` TINYINT UNSIGNED,
    PRIMARY KEY (`period`, `time_num`)
  ) ENGINE = MEMORY;

  ##
  ## Some initial data gathering
  ##

  DROP TEMPORARY TABLE IF EXISTS `tmp_onice`;
  CREATE TEMPORARY TABLE `tmp_onice` (
    `team_id` CHAR(3),
    `jersey` TINYINT UNSIGNED,
    `is_goalie` TINYINT UNSIGNED,
    `shift_num` TINYINT UNSIGNED,
    `period` TINYINT UNSIGNED,
    `shift_start` TIME,
    `shift_end` TIME,
    PRIMARY KEY (`team_id`, `jersey`, `shift_num`),
    KEY `for_lookup` (`team_id`, `period`, `shift_start`, `shift_end`)
  ) ENGINE = MEMORY
    SELECT `TOI`.`team_id`, `TOI`.`jersey`, (`LINEUP`.`pos` = 'G') AS `is_goalie`,
           `TOI`.`shift_num`, `TOI`.`period`, `TOI`.`shift_start`, `TOI`.`shift_end`
    FROM `SPORTS_NHL_GAME_TOI` AS `TOI`
    LEFT JOIN `SPORTS_NHL_GAME_LINEUP` AS `LINEUP`
      ON (`LINEUP`.`season` = `TOI`.`season`
      AND `LINEUP`.`game_type` = `TOI`.`game_type`
      AND `LINEUP`.`game_id` = `TOI`.`game_id`
      AND `LINEUP`.`team_id` = `TOI`.`team_id`
      AND `LINEUP`.`jersey` = `TOI`.`jersey`)
    WHERE `TOI`.`season` = v_season
    AND   `TOI`.`game_type` = v_game_type
    AND   `TOI`.`game_id` = v_game_id;

  DROP TEMPORARY TABLE IF EXISTS `tmp_faceoffs`;
  CREATE TEMPORARY TABLE `tmp_faceoffs` (
    `event_id` SMALLINT UNSIGNED,
    `period` TINYINT UNSIGNED,
    `event_time` TIME,
    `home_skaters` TINYINT UNSIGNED,
    `home_goalie` TINYINT UNSIGNED,
    `visitor_skaters` TINYINT UNSIGNED,
    `visitor_goalie` TINYINT UNSIGNED,
    PRIMARY KEY (`event_id`, `period`, `event_time`),
    KEY `for_lookup` (`period`, `event_time`)
  ) ENGINE = MEMORY
    SELECT `EVENT`.`event_id`, `EVENT`.`period`, `EVENT`.`event_time`,
           SUM(IF(`ONICE`.`team_id` <> `SCHED`.`home`, 0,
                  (IFNULL(`LINEUP_PL1`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL2`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL3`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL4`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL5`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL6`.`pos`, 'G') <> 'G'))) AS `home_skaters`,
           SUM(IF(`ONICE`.`team_id` <> `SCHED`.`home`, 0,
                  (IFNULL(`LINEUP_PL1`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL2`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL3`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL4`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL5`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL6`.`pos`, 'SK') = 'G'))) AS `home_goalie`,
           SUM(IF(`ONICE`.`team_id` <> `SCHED`.`visitor`, 0,
                  (IFNULL(`LINEUP_PL1`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL2`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL3`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL4`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL5`.`pos`, 'G') <> 'G')
                  + (IFNULL(`LINEUP_PL6`.`pos`, 'G') <> 'G'))) AS `visitor_skaters`,
           SUM(IF(`ONICE`.`team_id` <> `SCHED`.`visitor`, 0,
                  (IFNULL(`LINEUP_PL1`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL2`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL3`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL4`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL5`.`pos`, 'SK') = 'G')
                  + (IFNULL(`LINEUP_PL6`.`pos`, 'SK') = 'G'))) AS `visitor_goalie`
    FROM `SPORTS_NHL_SCHEDULE` AS `SCHED`
    JOIN `SPORTS_NHL_GAME_EVENT` AS `EVENT`
      ON (`EVENT`.`season` = `SCHED`.`season`
      AND `EVENT`.`game_type` = `SCHED`.`game_type`
      AND `EVENT`.`game_id` = `SCHED`.`game_id`
      AND `EVENT`.`event_type` = 'FACEOFF')
    JOIN `SPORTS_NHL_GAME_EVENT_ONICE` AS `ONICE`
      ON (`ONICE`.`season` = `EVENT`.`season`
      AND `ONICE`.`game_type` = `EVENT`.`game_type`
      AND `ONICE`.`game_id` = `EVENT`.`game_id`
      AND `ONICE`.`event_id` = `EVENT`.`event_id`)
    LEFT JOIN `SPORTS_NHL_GAME_LINEUP` AS `LINEUP_PL1`
      ON (`LINEUP_PL1`.`season` = `ONICE`.`season`
      AND `LINEUP_PL1`.`game_type` = `ONICE`.`game_type`
      AND `LINEUP_PL1`.`game_id` = `ONICE`.`game_id`
      AND `LINEUP_PL1`.`team_id` = `ONICE`.`team_id`
      AND `LINEUP_PL1`.`jersey` = `ONICE`.`player_1`)
    LEFT JOIN `SPORTS_NHL_GAME_LINEUP` AS `LINEUP_PL2`
      ON (`LINEUP_PL2`.`season` = `ONICE`.`season`
      AND `LINEUP_PL2`.`game_type` = `ONICE`.`game_type`
      AND `LINEUP_PL2`.`game_id` = `ONICE`.`game_id`
      AND `LINEUP_PL2`.`team_id` = `ONICE`.`team_id`
      AND `LINEUP_PL2`.`jersey` = `ONICE`.`player_2`)
    LEFT JOIN `SPORTS_NHL_GAME_LINEUP` AS `LINEUP_PL3`
      ON (`LINEUP_PL3`.`season` = `ONICE`.`season`
      AND `LINEUP_PL3`.`game_type` = `ONICE`.`game_type`
      AND `LINEUP_PL3`.`game_id` = `ONICE`.`game_id`
      AND `LINEUP_PL3`.`team_id` = `ONICE`.`team_id`
      AND `LINEUP_PL3`.`jersey` = `ONICE`.`player_3`)
    LEFT JOIN `SPORTS_NHL_GAME_LINEUP` AS `LINEUP_PL4`
      ON (`LINEUP_PL4`.`season` = `ONICE`.`season`
      AND `LINEUP_PL4`.`game_type` = `ONICE`.`game_type`
      AND `LINEUP_PL4`.`game_id` = `ONICE`.`game_id`
      AND `LINEUP_PL4`.`team_id` = `ONICE`.`team_id`
      AND `LINEUP_PL4`.`jersey` = `ONICE`.`player_4`)
    LEFT JOIN `SPORTS_NHL_GAME_LINEUP` AS `LINEUP_PL5`
      ON (`LINEUP_PL5`.`season` = `ONICE`.`season`
      AND `LINEUP_PL5`.`game_type` = `ONICE`.`game_type`
      AND `LINEUP_PL5`.`game_id` = `ONICE`.`game_id`
      AND `LINEUP_PL5`.`team_id` = `ONICE`.`team_id`
      AND `LINEUP_PL5`.`jersey` = `ONICE`.`player_5`)
    LEFT JOIN `SPORTS_NHL_GAME_LINEUP` AS `LINEUP_PL6`
      ON (`LINEUP_PL6`.`season` = `ONICE`.`season`
      AND `LINEUP_PL6`.`game_type` = `ONICE`.`game_type`
      AND `LINEUP_PL6`.`game_id` = `ONICE`.`game_id`
      AND `LINEUP_PL6`.`team_id` = `ONICE`.`team_id`
      AND `LINEUP_PL6`.`jersey` = `ONICE`.`player_6`)
    WHERE `SCHED`.`season` = v_season
    AND   `SCHED`.`game_type` = v_game_type
    AND   `SCHED`.`game_id` = v_game_id
    GROUP BY `EVENT`.`season`, `EVENT`.`game_type`, `EVENT`.`game_id`, `EVENT`.`event_id`;

  # Determine how long the game lasted
  SELECT `status`
         INTO v_game_status
  FROM `SPORTS_NHL_SCHEDULE`
  WHERE `season` = v_season
  AND   `game_type` = v_game_type
  AND   `game_id` = v_game_id;

  # And when the last goal occurred
  SELECT `period`, TIME_TO_SEC(`event_time`)
         INTO v_end_period, v_end_time
  FROM `SPORTS_NHL_GAME_EVENT`
  WHERE `season` = v_season
  AND   `game_type` = v_game_type
  AND   `game_id` = v_game_id
  AND   `event_type` = 'GOAL'
  ORDER BY `period` DESC, `event_time` ASC # Time stored 20:00..0:00
  LIMIT 1;

  ##
  ## Prepare the tables with appropriate per-second times
  ##

  CALL nhl_game_onice_strengths_prepare(1, 1200, 0); # 1200secs = 20min * 60secs
  CALL nhl_game_onice_strengths_prepare(2, 1200, 0); # 1200secs = 20min * 60secs
  CALL nhl_game_onice_strengths_prepare(3, 1200, 0); # 1200secs = 20min * 60secs
  # Then do we need any OT periods too?
  IF v_game_status IN ('OT', 'SO', '2OT', '3OT', '4OT') THEN
    # How long does the period last?
    IF v_game_status <> 'OT' THEN
      # Full length of time, which depends if we're regular season or playoffs
      IF v_game_type = 'regular' THEN
        CALL nhl_game_onice_strengths_prepare(4, 300, 0); # 300secs = 5min * 60secs
      ELSE
        CALL nhl_game_onice_strengths_prepare(4, 1200, 0); # 1200secs = 20min * 60secs
      END IF;
    ELSE
      # The game ended in this period
      IF v_game_type = 'regular' THEN
        CALL nhl_game_onice_strengths_prepare(4, 300, v_end_time); # 300secs = 5min * 60secs
      ELSE
        CALL nhl_game_onice_strengths_prepare(4, 1200, v_end_time); # 1200secs = 20min * 60secs
      END IF;
    END IF;
  END IF;
  # 2OT?
  IF v_game_status IN ('2OT', '3OT', '4OT') THEN
    IF v_game_status <> '2OT' THEN
      CALL nhl_game_onice_strengths_prepare(5, 1200, 0); # 1200secs = 20min * 60secs
    ELSE
      CALL nhl_game_onice_strengths_prepare(5, 1200, v_end_time);
    END IF;
  END IF;
  # 3OT?
  IF v_game_status IN ('3OT', '4OT') THEN
    IF v_game_status <> '3OT' THEN
      CALL nhl_game_onice_strengths_prepare(6, 1200, 0); # 1200secs = 20min * 60secs
    ELSE
      CALL nhl_game_onice_strengths_prepare(6, 1200, v_end_time);
    END IF;
  END IF;
  # 4OT? (which we assume to be the final OT period)
  IF v_game_status IN ('4OT') THEN
    CALL nhl_game_onice_strengths_prepare(7, 1200, v_end_time);
  END IF;

END $$

DELIMITER ;

##
## Insert the time-based data
##
DROP PROCEDURE IF EXISTS `nhl_game_onice_strengths_prepare`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_onice_strengths_prepare`(
  v_period TINYINT UNSIGNED,
  v_p_start SMALLINT UNSIGNED,
  v_p_end SMALLINT UNSIGNED
)
    COMMENT 'Prepare the table we will use for NHL game strength calcs'
BEGIN

  # SIGNED because we could allow it to go to -1, and our range comfortably fits inside a signed SMALLINT
  DECLARE v_p_time SMALLINT SIGNED;

  SET v_p_time = v_p_start;

  WHILE v_p_time >= v_p_end DO
    INSERT INTO `tmp_times` (`period`, `time_num`, `time_fmt`, `is_period_end`, `home_skaters`, `home_goalie`, `visitor_skaters`, `visitor_goalie`)
      VALUES (v_period, v_p_time, SEC_TO_TIME(v_p_time), v_p_time = v_p_end, 0, 0, 0, 0);

    SET v_p_time = v_p_time - 1;
  END WHILE;

END $$

DELIMITER ;
