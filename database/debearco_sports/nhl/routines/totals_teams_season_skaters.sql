##
## Team season totals
##
DROP PROCEDURE IF EXISTS `nhl_totals_teams_season_skaters`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_totals_teams_season_skaters`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff')
)
    COMMENT 'Calculate team season totals for NHL team skaters'
BEGIN

  ##
  ## Combine daily figures into a single season
  ##
  INSERT INTO SPORTS_NHL_TEAMS_SEASON_SKATERS (season, season_type, team_id, stat_dir, gp, goals, assists, plus_minus, pims, pens_taken, pens_drawn, pp_goals, pp_assists, pp_opps, pk_goals, pk_kills, pk_opps, sh_goals, sh_assists, shots, missed_shots, blocked_shots, shots_blocked, hits, times_hit, fo_wins, fo_loss, so_goals, so_shots, giveaways, takeaways)
    SELECT season,
           game_type AS season_type,
           team_id,
           stat_dir,
           SUM(IFNULL(gp, 0)) AS gp,
           SUM(IFNULL(goals, 0)) AS goals,
           SUM(IFNULL(assists, 0)) AS assists,
           SUM(IFNULL(plus_minus, 0)) AS plus_minus,
           SUM(IFNULL(pims, 0)) AS pims,
           SUM(IFNULL(pens_taken, 0)) AS pens_taken,
           SUM(IFNULL(pens_drawn, 0)) AS pens_drawn,
           SUM(IFNULL(pp_goals, 0)) AS pp_goals,
           SUM(IFNULL(pp_assists, 0)) AS pp_assists,
           SUM(IFNULL(pp_opps, 0)) AS pp_opps,
           SUM(IFNULL(pk_goals, 0)) AS pk_goals,
           SUM(IFNULL(pk_kills, 0)) AS pk_kills,
           SUM(IFNULL(pk_opps, 0)) AS pk_opps,
           SUM(IFNULL(sh_goals, 0)) AS sh_goals,
           SUM(IFNULL(sh_assists, 0)) AS sh_assists,
           SUM(IFNULL(shots, 0)) AS shots,
           SUM(IFNULL(missed_shots, 0)) AS missed_shots,
           SUM(IFNULL(blocked_shots, 0)) AS blocked_shots,
           SUM(IFNULL(shots_blocked, 0)) AS shots_blocked,
           SUM(IFNULL(hits, 0)) AS hits,
           SUM(IFNULL(times_hit, 0)) AS times_hit,
           SUM(IFNULL(fo_wins, 0)) AS fo_wins,
           SUM(IFNULL(fo_loss, 0)) AS fo_loss,
           SUM(IFNULL(so_goals, 0)) AS so_goals,
           SUM(IFNULL(so_shots, 0)) AS so_shots,
           SUM(IFNULL(giveaways, 0)) AS giveaways,
           SUM(IFNULL(takeaways, 0)) AS takeaways
    FROM SPORTS_NHL_TEAMS_GAME_SKATERS
    WHERE season = v_season
    AND   game_type = v_game_type
    GROUP BY season, game_type, team_id, stat_dir
    ORDER BY season, game_type, team_id, stat_dir
  ON DUPLICATE KEY UPDATE gp = VALUES(gp),
                          goals = VALUES(goals),
                          assists = VALUES(assists),
                          pims = VALUES(pims),
                          plus_minus = VALUES(plus_minus),
                          pens_taken = VALUES(pens_taken),
                          pens_drawn = VALUES(pens_drawn),
                          pp_goals = VALUES(pp_goals),
                          pp_assists = VALUES(pp_assists),
                          pp_opps = VALUES(pp_opps),
                          pk_goals = VALUES(pk_goals),
                          pk_kills = VALUES(pk_kills),
                          pk_opps = VALUES(pk_opps),
                          sh_goals = VALUES(sh_goals),
                          sh_assists = VALUES(sh_assists),
                          shots = VALUES(shots),
                          missed_shots = VALUES(missed_shots),
                          blocked_shots = VALUES(blocked_shots),
                          shots_blocked = VALUES(shots_blocked),
                          hits = VALUES(hits),
                          times_hit = VALUES(times_hit),
                          fo_wins= VALUES(fo_wins),
                          fo_loss = VALUES(fo_loss),
                          so_goals = VALUES(so_goals),
                          so_shots = VALUES(so_shots),
                          giveaways = VALUES(giveaways),
                          takeaways = VALUES(takeaways);

END $$

DELIMITER ;

