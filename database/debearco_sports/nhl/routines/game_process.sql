
##
## Determine per-game periods of team strengths
##
DROP PROCEDURE IF EXISTS `nhl_game_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_game_process`(
  v_season SMALLINT UNSIGNED,
  v_game_type ENUM('regular','playoff'),
  v_game_id SMALLINT UNSIGNED
)
    COMMENT 'Post-process individual NHL games'
BEGIN

  # Reduce our play-by-play to prod uploading boxscore events
  INSERT INTO SPORTS_NHL_GAME_EVENT_BOXSCORE (`season`, `game_type`, `game_id`, `event_id`, `period`, `event_time`, `event_type`, `team_id`, `play`)
    SELECT `season`, `game_type`, `game_id`, `event_id`, `period`, `event_time`, `event_type`, `team_id`, `play`
    FROM SPORTS_NHL_GAME_EVENT
    WHERE `season` = v_season
    AND   `game_type` = v_game_type
    AND   `game_id` = v_game_id
    AND   `event_type` IN ('GOAL','PENALTY','SHOOTOUT');

  # Reduce our event co-ordinates to prod uploading lists by-type
  INSERT INTO SPORTS_NHL_GAME_EVENT_COORDS (`season`, `game_type`, `game_id`, `event_type`, `events`)
    SELECT `season`, `game_type`, `game_id`,
          IF(`event_type` IN ('BLOCKEDSHOT','MISSEDSHOT','SHOT'), 'SHOT', `event_type`) AS `event_type`,
          COMPRESS(CONCAT('[', GROUP_CONCAT(CONCAT('[', `period`, ',', `coord_x`, ',', `coord_y`, ',\"', `team_id`, '\"]') ORDER BY `event_id`), ']')) AS `events`
    FROM SPORTS_NHL_GAME_EVENT
    WHERE `season` = v_season
    AND   `game_type` = v_game_type
    AND   `game_id` = v_game_id
    AND   `event_type` IN ('GOAL','PENALTY','SHOOTOUT','BLOCKEDSHOT','MISSEDSHOT','SHOT','HIT')
    AND   `coord_x` IS NOT NULL
    AND   `team_id` IS NOT NULL
    GROUP BY `season`, `game_type`, `game_id`, IF(`event_type` IN ('BLOCKEDSHOT','MISSEDSHOT','SHOT'), 'SHOT', `event_type`);

  # Calculate on-ice strength periods
  CALL nhl_game_onice_strengths(v_season, v_game_type, v_game_id);

END $$

DELIMITER ;
