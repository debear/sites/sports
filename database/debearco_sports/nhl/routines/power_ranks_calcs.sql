#
# Team schedules
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_sched`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_sched`(
  v_season SMALLINT UNSIGNED,
  v_calc_date DATE
)
    COMMENT 'Calculate the NHL power ranks team schedules'
BEGIN

  # First, shorten the team list
  DROP TEMPORARY TABLE IF EXISTS tmp_teams;
  CREATE TEMPORARY TABLE tmp_teams (
    team_id VARCHAR(3),
    conf_id TINYINT UNSIGNED,
    div_id TINYINT UNSIGNED,
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY
    SELECT TEAM_DIV.team_id, DIVISION.parent_id AS conf_id, DIVISION.grouping_id AS div_id
    FROM SPORTS_NHL_TEAMS_GROUPINGS AS TEAM_DIV
    JOIN SPORTS_NHL_GROUPINGS AS DIVISION
      ON (DIVISION.grouping_id = TEAM_DIV.grouping_id)
    WHERE v_season BETWEEN TEAM_DIV.season_from AND IFNULL(TEAM_DIV.season_to, 2099);

  CALL _duplicate_tmp_table('tmp_teams', 'tmp_teams_cp');

  # Then build...
  DROP TEMPORARY TABLE IF EXISTS tmp_sched;
  CREATE TEMPORARY TABLE tmp_sched (
    season SMALLINT UNSIGNED,
    game_id SMALLINT(5),
    team_id VARCHAR(3),
    opp_id VARCHAR(3),
    result ENUM('row','sow','l','otl','sol'),
    pre_standing_date DATE,
    is_ft TINYINT UNSIGNED,
    is_wk TINYINT UNSIGNED,
    PRIMARY KEY (season, game_id, team_id),
    INDEX by_team (team_id),
    INDEX by_game_opp (season, game_id, opp_id)
  ) ENGINE = MEMORY
    SELECT SCHED.season, SCHED.game_id,
           tmp_teams.team_id,
           IF(tmp_teams.team_id = SCHED.home, SCHED.visitor, SCHED.home) AS opp_id,
           CASE SCHED.status
             WHEN 'F' THEN
               IF((tmp_teams.team_id = SCHED.home AND SCHED.home_score > SCHED.visitor_score)
                  OR (tmp_teams.team_id = SCHED.visitor AND SCHED.home_score < SCHED.visitor_score),
                  'row', 'l')
             WHEN 'OT' THEN
               IF((tmp_teams.team_id = SCHED.home AND SCHED.home_score > SCHED.visitor_score)
                  OR (tmp_teams.team_id = SCHED.visitor AND SCHED.home_score < SCHED.visitor_score),
                  'row', 'otl')
             WHEN 'SO' THEN
               IF((tmp_teams.team_id = SCHED.home AND SCHED.home_score > SCHED.visitor_score)
                  OR (tmp_teams.team_id = SCHED.visitor AND SCHED.home_score < SCHED.visitor_score),
                  'sow', 'sol')
           END AS result,
           DATE_SUB(SCHED.game_date, INTERVAL 1 DAY) AS pre_standing_date,
           SCHED.game_date >= DATE_SUB(v_calc_date, INTERVAL 2 WEEK) AS is_ft,
           SCHED.game_date >= DATE_SUB(v_calc_date, INTERVAL 1 WEEK) AS is_wk
    FROM tmp_teams
    JOIN SPORTS_NHL_SCHEDULE AS SCHED
      ON (SCHED.season = v_season
      AND SCHED.game_date <= v_calc_date
      AND IFNULL(SCHED.status, 'NULL') NOT IN ('NULL','PPD','CNC')
      AND (SCHED.home = tmp_teams.team_id
           OR SCHED.visitor = tmp_teams.team_id));

END $$

DELIMITER ;

#
# Create the table to store the raw data for a stat
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_schema`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_schema`(
  v_type VARCHAR(30)
)
    COMMENT 'Create the table for processing NHL power rank data'
BEGIN

  DROP TEMPORARY TABLE IF EXISTS tmp_data;
  CALL _exec(CONCAT('CREATE TEMPORARY TABLE tmp_data (
    team_id VARCHAR(3),
    tot_ov ', v_type, ',
    tot_ft ', v_type, ',
    tot_wk ', v_type, ',
    PRIMARY KEY (team_id)
  ) ENGINE = MEMORY;'));

END $$

DELIMITER ;

#
# Process a stat
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_process`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_process`(
  v_col VARCHAR(15),
  v_asc TINYINT UNSIGNED,
  v_weight TINYINT UNSIGNED
)
    COMMENT 'Process and individual NHL power rank'
BEGIN

  # Add to the table
  CALL _exec(CONCAT('ALTER TABLE tmp_stats ADD COLUMN ', v_col, '_ov DECIMAL(5,3) SIGNED,
                                           ADD COLUMN ', v_col, '_ft DECIMAL(5,3) SIGNED,
                                           ADD COLUMN ', v_col, '_wk DECIMAL(5,3) SIGNED;'));

  # Process the individual parts: overall, recent, division
  CALL nhl_power_ranks_build_process_part(v_col, v_asc, 'ov');
  CALL nhl_power_ranks_build_process_part(v_col, v_asc, 'ft');
  CALL nhl_power_ranks_build_process_part(v_col, v_asc, 'wk');

  # And add to the final table
  CALL _exec(CONCAT('ALTER TABLE tmp_ranks ADD COLUMN ', v_col, ' DECIMAL(5,3) SIGNED;'));

  # Overall total accounts for 60% of our score
  # Last fortnight total accounts for 25% of our score
  # Last week total accounts for 15% of our score
  CALL _exec(CONCAT('UPDATE tmp_ranks
  JOIN tmp_stats
    ON (tmp_stats.team_id = tmp_ranks.team_id)
  SET tmp_ranks.', v_col, ' = (tmp_stats.', v_col, '_ov * ', v_weight, ' * 0.60)
                            + (tmp_stats.', v_col, '_ft * ', v_weight, ' * 0.25)
                            + (tmp_stats.', v_col, '_wk * ', v_weight, ' * 0.15);'));
  CALL _exec(CONCAT('UPDATE tmp_ranks SET score = score + ', v_col, ';'));

END $$

DELIMITER ;

#
# Process a stat part
#
DROP PROCEDURE IF EXISTS `nhl_power_ranks_build_process_part`;

DELIMITER $$
CREATE DEFINER=`debearco_sysadmn`@`localhost` PROCEDURE `nhl_power_ranks_build_process_part`(
  v_col VARCHAR(15),
  v_asc TINYINT UNSIGNED,
  v_part ENUM('ov','ft','wk')
)
    COMMENT 'Process and individual NHL power rank part'
BEGIN

  # Declare vars
  DECLARE v_median FLOAT;
  DECLARE v_std_dev FLOAT;

  # Calculate the median and standard deviations of the data set (using temporary vars to get info out of _exec)
  CALL _exec(CONCAT('SELECT AVG(IFNULL(tot_', v_part, ', 0)), STDDEV_POP(IFNULL(tot_', v_part, ', 0))
         INTO @v_median, @v_std_dev
  FROM tmp_data;'));
  SELECT @v_median, @v_std_dev INTO v_median, v_std_dev;

  # And score each team based on multiples of the standard deviation from the mean
  CALL _exec(CONCAT('UPDATE tmp_stats
  JOIN tmp_data
    ON (tmp_data.team_id = tmp_stats.team_id)
  SET tmp_stats.', v_col, '_', v_part, ' = IF(', v_asc, ' = 0,
        CAST(IFNULL(tmp_data.tot_', v_part, ', 0) AS DECIMAL(11,7)) - CAST(', v_median, ' AS DECIMAL(11,7)),
        CAST(', v_median, ' AS DECIMAL(11,7)) - CAST(IFNULL(tmp_data.tot_', v_part, ', 0) AS DECIMAL(11,7))) / ', v_std_dev, ';'));

END $$

DELIMITER ;

