CREATE TABLE `SPORTS_NHL_TEAMS_ROSTERS` (
  `season` YEAR NOT NULL,
  `the_date` DATE NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `pos` ENUM('C','D','G','L','R') COLLATE latin1_general_ci NOT NULL,
  `capt_status` ENUM('C','A') COLLATE latin1_general_ci DEFAULT NULL,
  `player_status` ENUM('active','ir','na') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`the_date`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
