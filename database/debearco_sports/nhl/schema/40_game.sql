CREATE TABLE `SPORTS_NHL_GAME_LINEUP` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pos` ENUM('C','D','G','L','R') COLLATE latin1_general_ci NOT NULL,
  `capt_status` ENUM('C','A') COLLATE latin1_general_ci DEFAULT NULL,
  `started` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_SCRATCHES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `pos` ENUM('C','D','G','L','R') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_LINEUP_ISSUES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `player_name` VARCHAR(150) COLLATE latin1_general_ci NOT NULL,
  `pos` ENUM('C','D','G','L','R') COLLATE latin1_general_ci NOT NULL,
  `capt_status` ENUM('C','A') COLLATE latin1_general_ci DEFAULT NULL,
  `started` TINYINT(1) UNSIGNED DEFAULT NULL,
  `problem` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`,`player_name`),
  KEY `by_player` (`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_SCRATCHES_ISSUES` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `player_name` VARCHAR(150) COLLATE latin1_general_ci NOT NULL,
  `pos` ENUM('C','D','G','L','R') COLLATE latin1_general_ci DEFAULT NULL,
  `problem` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`,`player_name`),
  KEY `by_player` (`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_OFFICIALS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `official_type` ENUM('referee','linesman') COLLATE latin1_general_ci NOT NULL,
  `official_id` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`official_type`,`official_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_STRENGTHS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(4) UNSIGNED NOT NULL,
  `instance_id` TINYINT(2) UNSIGNED NOT NULL,
  `period` TINYINT(1) UNSIGNED NOT NULL,
  `time_start` TIME NOT NULL,
  `time_end` TIME NOT NULL,
  `team_id` ENUM('home','visitor') COLLATE latin1_general_ci DEFAULT NULL,
  `strength` ENUM('5-on-5','5-on-4','4-on-4','5-on-3','4-on-3','3-on-3') COLLATE latin1_general_ci NOT NULL,
  `goalies` ENUM('home-visitor','home-empty','empty-visitor','empty-empty') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`instance_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_PP_STATS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `pp_type` ENUM('pp','pk') COLLATE latin1_general_ci NOT NULL,
  `pp_goals` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `pp_opps` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`pp_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_THREE_STARS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `star` TINYINT(1) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`star`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_TOI` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  `shift_num` TINYINT(3) UNSIGNED NOT NULL,
  `period` TINYINT(3) UNSIGNED NOT NULL,
  `shift_start` TIME NOT NULL,
  `shift_end` TIME NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`team_id`,`jersey`,`shift_num`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_SUMMARISED` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
