CREATE TABLE `SPORTS_NHL_POWER_RANKINGS_WEEKS` (
  `season` YEAR NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `calc_date` DATE NOT NULL,
  `date_from` DATE NOT NULL,
  `date_to` DATE NOT NULL,
  `when_run` DATETIME DEFAULT NULL,
  PRIMARY KEY (`season`,`week`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_POWER_RANKINGS` (
  `season` YEAR NOT NULL,
  `week` TINYINT(3) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `rank` TINYINT(3) UNSIGNED DEFAULT NULL,
  `score` DECIMAL(5,3) DEFAULT NULL,
  `point_pct` DECIMAL(5,3) DEFAULT NULL,
  `scoring` DECIMAL(5,3) DEFAULT NULL,
  `special_teams` DECIMAL(5,3) DEFAULT NULL,
  `faceoffs` DECIMAL(5,3) DEFAULT NULL,
  `str_sched` DECIMAL(5,3) DEFAULT NULL,
  `comment` text COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`week`,`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
