CREATE TABLE `SPORTS_NHL_PLAYERS` (
  `player_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `dob` DATE DEFAULT NULL,
  `birthplace` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `birthplace_country` CHAR(2) COLLATE latin1_general_ci DEFAULT NULL,
  `height` TINYINT(3) UNSIGNED DEFAULT NULL,
  `weight` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `shoots_catches` ENUM('Left','Right') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_PLAYERS_IMPORT` (
  `player_id` SMALLINT(5) UNSIGNED NOT NULL,
  `remote_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `profile_imported` DATETIME DEFAULT NULL,
  PRIMARY KEY (`player_id`),
  UNIQUE KEY `remote_id` (`remote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_PLAYERS_AWARDS` (
  `season` YEAR NOT NULL,
  `award_id` TINYINT(3) UNSIGNED NOT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `player_name` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BKA','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','HMT','KC','LA','MIN','MNS','MTL','MTM','NEW','NJ','NSH','NYA','NYI','NYR','OSE','OTS','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `pos` ENUM('C','LW','RW','D','G') COLLATE latin1_general_ci DEFAULT NULL,
  UNIQUE KEY `FAUX_PRIMARY` (`season`,`award_id`,`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
