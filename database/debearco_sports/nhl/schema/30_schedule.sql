CREATE TABLE `SPORTS_NHL_SCHEDULE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `home` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `visitor` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `game_date` DATE NOT NULL,
  `game_time` TIME DEFAULT NULL,
  `home_score` TINYINT(1) UNSIGNED DEFAULT NULL,
  `visitor_score` TINYINT(1) UNSIGNED DEFAULT NULL,
  `status` ENUM('F','OT','2OT','3OT','4OT','5OT','SO','PPD','CNC') COLLATE latin1_general_ci DEFAULT NULL,
  `home_goalie` TINYINT(3) UNSIGNED DEFAULT NULL,
  `visitor_goalie` TINYINT(3) UNSIGNED DEFAULT NULL,
  `attendance` MEDIUMINT(8) UNSIGNED DEFAULT NULL,
  `three_star_sel` VARCHAR(200) COLLATE latin1_general_ci DEFAULT NULL,
  `alt_venue` VARCHAR(150) COLLATE latin1_general_ci DEFAULT NULL,
  `lineup_linked` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`),
  KEY `by_status` (`season`,`game_type`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_SCHEDULE_DATES` (
  `season` YEAR NOT NULL,
  `date` DATE NOT NULL,
  `num_games` TINYINT(3) UNSIGNED NOT NULL,
  `date_prev` DATE DEFAULT NULL,
  `date_next` DATE DEFAULT NULL,
  PRIMARY KEY (`season`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_SCHEDULE_MATCHUPS` (
  `season` YEAR NOT NULL,
  `team_idA` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `team_idB` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`team_idA`,`team_idB`,`game_type`,`game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
