# Full list of plays - not uploaded
CREATE TABLE `SPORTS_NHL_GAME_EVENT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period` TINYINT(3) UNSIGNED NOT NULL,
  `event_time` TIME NOT NULL,
  `event_type` ENUM('?','BLOCKEDSHOT','CHALLENGE','FACEOFF','GIVEAWAY','GOAL','HIT','MISSEDSHOT','PENALTY','SHOOTOUT','SHOT','STOPPAGE','TAKEAWAY','TIMEOUT') COLLATE latin1_general_ci NOT NULL,
  `zone` ENUM('off','def','neu') COLLATE latin1_general_ci DEFAULT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `coord_x` TINYINT(3) DEFAULT NULL,
  `coord_y` TINYINT(3) DEFAULT NULL,
  `heatzone` TINYINT(3) UNSIGNED DEFAULT NULL,
  `play` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_BLOCKEDSHOT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `by_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `on_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `on_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `shot_type` ENUM('Backhand','Bat','Between Legs','Deflected','Over Net','Penalty Shot','Poke','Slap','Snap','Tip-In','Wide of Net','Wrap-around','Wrist') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_blocker` (`season`,`game_type`,`game_id`,`by_team_id`,`by_jersey`),
  KEY `by_shooter` (`season`,`game_type`,`game_id`,`on_team_id`,`on_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_CHALLENGE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `challenge` ENUM('Goal Interference','Interference On Goalie','Missed Stoppage','Offside') COLLATE latin1_general_ci DEFAULT NULL,
  `outcome` ENUM('Goal','Goal Overturned') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_FACEOFF` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `home` TINYINT(3) UNSIGNED DEFAULT NULL,
  `visitor` TINYINT(3) UNSIGNED DEFAULT NULL,
  `winner` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_players` (`season`,`game_type`,`game_id`,`home`,`visitor`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_GIVEAWAY` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_player` (`season`,`game_type`,`game_id`,`team_id`,`jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_GOAL` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `goal_num` TINYINT(3) UNSIGNED DEFAULT NULL,
  `scorer` TINYINT(3) UNSIGNED DEFAULT NULL,
  `assist_1` TINYINT(3) UNSIGNED DEFAULT NULL,
  `assist_2` TINYINT(3) UNSIGNED DEFAULT NULL,
  `on_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `on_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `shot_type` ENUM('Backhand','Bat','Between Legs','Deflected','Over Net','Penalty Shot','Poke','Slap','Snap','Tip-In','Wide of Net','Wrap-around','Wrist') COLLATE latin1_general_ci DEFAULT NULL,
  `shot_distance` TINYINT(3) UNSIGNED DEFAULT NULL,
  `goal_type` ENUM('EV','PP','SH') COLLATE latin1_general_ci NOT NULL,
  `goalie_status` ENUM('EN','WG') COLLATE latin1_general_ci DEFAULT NULL,
  `penalty_shot` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_scoring` (`season`,`game_type`,`game_id`,`by_team_id`,`scorer`,`assist_1`,`assist_2`),
  KEY `by_goal_type` (`season`,`game_type`,`game_id`,`goal_type`),
  KEY `by_goalie` (`season`,`game_type`,`game_id`,`on_team_id`,`on_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_HIT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `by_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `on_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `on_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_hitter` (`season`,`game_type`,`game_id`,`by_team_id`,`by_jersey`),
  KEY `by_hittee` (`season`,`game_type`,`game_id`,`on_team_id`,`on_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_MISSEDSHOT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `by_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `shot_type` ENUM('Backhand','Bat','Between Legs','Deflected','Over Net','Penalty Shot','Poke','Slap','Snap','Tip-In','Wide of Net','Wrap-around','Wrist') COLLATE latin1_general_ci DEFAULT NULL,
  `shot_distance` TINYINT(3) UNSIGNED DEFAULT NULL,
  `miss_reason` ENUM('Goalpost','Hit Crossbar','Over Net','Wide of Net') COLLATE latin1_general_ci DEFAULT NULL,
  `penalty_shot` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_shooter` (`season`,`game_type`,`game_id`,`by_team_id`,`by_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_PENALTY` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `served_by` TINYINT(3) UNSIGNED DEFAULT NULL,
  `type` ENUM('Abuse of Officials','Abusive Language','Bench','Boarding','Broken Stick','Butt Ending','Charging','Checking From Behind','Clipping','Closing Hand on Puck','Cross Checking','Delay of Game','Diving','Elbowing','Embellishment','Face-Off Violation','Fighting','Game Misconduct','Goaltender Interference','Head Butting','Hi-Sticking','Holding','Holding the Stick','Hooking','Illegal Check to Head','Illegal Equipment','Illegal Play by Goalie','Illegal Stick','Instigator','Interference','Kneeing','Leaving the Crease','Misconduct','Puck Over Glass','Roughing','Slashing','Smothering Puck in Crease','Spearing','Throwing Stick','Too Many Men on the Ice','Tripping','Unsportsmanlike Conduct') COLLATE latin1_general_ci DEFAULT NULL,
  `pims` TINYINT(3) UNSIGNED DEFAULT NULL,
  `drawn_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `drawn_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_offender` (`season`,`game_type`,`game_id`,`team_id`,`jersey`),
  KEY `by_drawn` (`season`,`game_type`,`game_id`,`drawn_team_id`,`drawn_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_SHOOTOUT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `by_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `on_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `on_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `shot_type` ENUM('Backhand','Bat','Between Legs','Deflected','Over Net','Penalty Shot','Poke','Slap','Snap','Tip-In','Wide of Net','Wrap-around','Wrist') COLLATE latin1_general_ci DEFAULT NULL,
  `shot_distance` TINYINT(3) UNSIGNED DEFAULT NULL,
  `status` ENUM('goal','save','miss','goalpost') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_shooter` (`season`,`game_type`,`game_id`,`by_team_id`,`by_jersey`),
  KEY `by_goalie` (`season`,`game_type`,`game_id`,`on_team_id`,`on_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_SHOT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `by_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `by_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `on_team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `on_jersey` TINYINT(3) UNSIGNED DEFAULT NULL,
  `shot_type` ENUM('Backhand','Bat','Between Legs','Deflected','Over Net','Penalty Shot','Poke','Slap','Snap','Tip-In','Wide of Net','Wrap-around','Wrist') COLLATE latin1_general_ci DEFAULT NULL,
  `shot_distance` TINYINT(3) UNSIGNED DEFAULT NULL,
  `penalty_shot` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_shooter` (`season`,`game_type`,`game_id`,`by_team_id`,`by_jersey`),
  KEY `by_goalie` (`season`,`game_type`,`game_id`,`on_team_id`,`on_jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_STOPPAGE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `reason` SET('Challenge (Home) - Goal Interference','Challenge (Home) - Offside','Challenge (Visitor) - Goal Interference','Challenge (Visitor) - Offside','Challenge (League) - Goal Interference','Challenge (League) - Offside','Clock Problem','Goalie Stopped','Hand Pass','High Stick','Ice Problem','Icing','Net Off','Objects On Ice','Official Injury','Offside','Player Equipment','Player Injury','Premature Substitution','Puck Frozen','Puck In Benches','Puck In Crowd','Puck In Netting','Referee Or Linesman','Rink Repair','Switch Sides','TV Timeout','Video Review') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_TAKEAWAY` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `jersey` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`),
  KEY `by_player` (`season`,`game_type`,`game_id`,`team_id`,`jersey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_TIMEOUT` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_UNKNOWN` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period` TINYINT(3) UNSIGNED NOT NULL,
  `event_time` TIME NOT NULL,
  `event_type` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `play` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_ONICE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `player_1` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_2` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_3` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_4` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_5` TINYINT(3) UNSIGNED DEFAULT NULL,
  `player_6` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`,`team_id`),
  KEY `by_full` (`season`,`game_type`,`game_id`,`event_id`,`team_id`,`player_1`,`player_2`,`player_3`,`player_4`,`player_5`,`player_6`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

# Boxscore plays only - uploaded
CREATE TABLE `SPORTS_NHL_GAME_EVENT_BOXSCORE` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_id` SMALLINT(5) UNSIGNED NOT NULL,
  `period` TINYINT(3) UNSIGNED NOT NULL,
  `event_time` TIME NOT NULL,
  `event_type` ENUM('GOAL','PENALTY','SHOOTOUT') COLLATE latin1_general_ci NOT NULL,
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `play` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GAME_EVENT_COORDS` (
  `season` YEAR NOT NULL,
  `game_type` ENUM('regular','playoff') COLLATE latin1_general_ci NOT NULL,
  `game_id` SMALLINT(5) UNSIGNED NOT NULL,
  `event_type` ENUM('GOAL','PENALTY','SHOOTOUT','SHOT','HIT') COLLATE latin1_general_ci NOT NULL,
  `events` VARBINARY(5000),
  PRIMARY KEY (`season`,`game_type`,`game_id`,`event_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
