CREATE TABLE `SPORTS_NHL_POSITIONS` (
  `pos_code` ENUM('C','D','G','L','R') COLLATE latin1_general_ci NOT NULL,
  `pos_short` ENUM('C','D','G','LW','RW') COLLATE latin1_general_ci NOT NULL,
  `pos_long` VARCHAR(15) COLLATE latin1_general_ci NOT NULL,
  `posgroup_id` TINYINT(3) UNSIGNED DEFAULT NULL,
  `type` ENUM('skaters','goaltenders') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`pos_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_POSITIONS_GROUPS` (
  `posgroup_id` TINYINT(3) UNSIGNED NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`posgroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_STATUS` (
  `type` ENUM('player') COLLATE latin1_general_ci NOT NULL,
  `code` ENUM('active','ir','na') COLLATE latin1_general_ci NOT NULL,
  `disp_code` ENUM('IR','NA') COLLATE latin1_general_ci DEFAULT NULL,
  `disp_name` VARCHAR(25) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`type`,`code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_OFFICIALS` (
  `season` YEAR NOT NULL,
  `official_id` TINYINT(3) UNSIGNED NOT NULL,
  `first_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `surname` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`season`,`official_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_GROUPINGS` (
  `grouping_id` TINYINT(1) UNSIGNED NOT NULL,
  `parent_id` TINYINT(1) UNSIGNED DEFAULT NULL,
  `type` ENUM('league','conf','div') COLLATE latin1_general_ci NOT NULL,
  `name` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  `name_full` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  `order` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `icon` VARCHAR(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`grouping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_DRAFT` (
  `season` YEAR NOT NULL,
  `round` TINYINT(3) UNSIGNED NOT NULL,
  `round_pick` TINYINT(2) UNSIGNED NOT NULL,
  `pick` SMALLINT(5) UNSIGNED NOT NULL,
  `team_id` ENUM('ANA','ARI','ATF','ATL','BOS','BUF','CAR','CBN','CGY','CHI','CLB','COL','COR','CSE','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NJ','NSH','NYI','NYR','OSE','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `team_id_orig` ENUM('ANA','ARI','ATF','ATL','BOS','BUF','CAR','CBN','CGY','CHI','CLB','COL','COR','CSE','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NJ','NSH','NYI','NYR','OSE','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `team_id_via` VARCHAR(15) COLLATE latin1_general_ci DEFAULT NULL,
  `remote_id` MEDIUMINT(8) UNSIGNED DEFAULT NULL,
  `player_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `player` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `pos` ENUM('C','D','G','LW','RW','F') COLLATE latin1_general_ci DEFAULT NULL,
  `country` CHAR(2) COLLATE latin1_general_ci DEFAULT NULL,
  `height` TINYINT(3) UNSIGNED DEFAULT NULL,
  `weight` TINYINT(3) UNSIGNED DEFAULT NULL,
  `junior_league` VARCHAR(30) COLLATE latin1_general_ci DEFAULT NULL,
  `junior_team` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`pick`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_AWARDS` (
  `award_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE latin1_general_ci NOT NULL,
  `description` VARCHAR(250) COLLATE latin1_general_ci NOT NULL,
  `disp_order` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`award_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
