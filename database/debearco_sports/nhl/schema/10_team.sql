CREATE TABLE `SPORTS_NHL_TEAMS` (
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BKA','BOS','BUF','CAR','CBN','CGY','CHI','CLB','COL','COR','CSE','DAL','DET','EDM','FLA','HFD','HMT','KC','LA','MIN','MNS','MTL','MTM','NEW','NJ','NSH','NYA','NYI','NYR','OSE','OTS','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `city` VARCHAR(40) COLLATE latin1_general_ci NOT NULL,
  `franchise` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `founded` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_TEAMS_NAMING` (
  `team_id` ENUM('ANA','ARI','ATL','BOS','BUF','CAR','CBN','CGY','CHI','CLB','COL','DAL','DET','EDM','FLA','LA','MIN','MTL','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `alt_team_id` ENUM('ALB','ATF','ATL','COR','CSE','HFD','KC','MNS','NEW','OSE','PHO','QUE','WIN') COLLATE latin1_general_ci DEFAULT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `alt_city` VARCHAR(40) COLLATE latin1_general_ci DEFAULT NULL,
  `alt_franchise` VARCHAR(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_TEAMS_GROUPINGS` (
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NEW','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `grouping_id` TINYINT(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`team_id`,`season_from`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_TEAMS_ARENAS` (
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NEW','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `season_from` YEAR NOT NULL,
  `season_to` YEAR DEFAULT NULL,
  `building_id` TINYINT(3) UNSIGNED NOT NULL,
  `arena` VARCHAR(55) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`team_id`,`season_from`,`building_id`),
  KEY `team_season` (`team_id`,`season_from`,`season_to`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_TEAMS_HISTORY_TITLES` (
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NEW','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `league_id` TINYINT(3) UNSIGNED NOT NULL,
  `div_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `conf_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `league_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  `playoff_champ` TINYINT(3) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`team_id`,`league_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_TEAMS_HISTORY_REGULAR` (
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NEW','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `season_half` TINYINT(1) UNSIGNED NOT NULL,
  `wins` TINYINT(2) UNSIGNED DEFAULT 0,
  `loss` TINYINT(2) UNSIGNED DEFAULT 0,
  `ties` TINYINT(2) UNSIGNED DEFAULT NULL,
  `ot_loss` TINYINT(2) UNSIGNED DEFAULT NULL,
  `pts` TINYINT(3) UNSIGNED DEFAULT 0,
  `pos` TINYINT(1) UNSIGNED DEFAULT NULL,
  `league_champ` TINYINT(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`team_id`,`season`,`season_half`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_TEAMS_HISTORY_PLAYOFF_SUMMARY` (
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NEW','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `league_id` TINYINT(1) UNSIGNED DEFAULT 0,
  `wildcard` TINYINT(1) UNSIGNED DEFAULT 0,
  `div_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  `conf_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  `league_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  `playoff_champ` TINYINT(1) UNSIGNED DEFAULT 0,
  PRIMARY KEY (`team_id`,`season`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `SPORTS_NHL_TEAMS_HISTORY_PLAYOFF` (
  `team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NEW','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci NOT NULL,
  `season` YEAR NOT NULL,
  `league_id` TINYINT(3) UNSIGNED DEFAULT 0,
  `round` TINYINT(3) UNSIGNED NOT NULL,
  `round_code` ENUM('PRE','CQF','DSF','QF','CSF','DF','SF','LF','CF','F','SCF') COLLATE latin1_general_ci DEFAULT NULL,
  `opp_team_id` ENUM('ALB','ANA','ARI','ATF','ATL','BOS','BUF','CAR','CGY','CHI','CLB','COL','COR','DAL','DET','EDM','FLA','HFD','KC','LA','MIN','MNS','MTL','NEW','NJ','NSH','NYI','NYR','OTT','PHI','PHO','PIT','QUE','SEA','SJ','STL','TB','TOR','UTA','VAN','VGK','WIN','WPG','WSH') COLLATE latin1_general_ci DEFAULT NULL,
  `opp_team_alt` VARCHAR(90) COLLATE latin1_general_ci DEFAULT NULL,
  `for` TINYINT(3) UNSIGNED DEFAULT 0,
  `against` TINYINT(3) UNSIGNED DEFAULT 0,
  `info` ENUM('Total Goals') COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`season`,`team_id`,`round`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
