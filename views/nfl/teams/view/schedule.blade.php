@php
    // Render the markup in a variable we can process later
    $page = (string)View::make('sports.nfl.teams.view.home.schedule', [
        'sport' => $sport,
        'schedule' => $team->schedule,
        'css' => 'detailed',
    ]);
@endphp
@json([
    'sched' => $team->scheduleJson(),
    'page' => $page,
])
