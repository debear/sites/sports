@php
    $last_week = 0;
    $i = 0;
    // We could have passed in a schedule, but if not ensure we use the recent schedule
    if (!isset($schedule)) {
        $schedule = $team->recentSchedule;
    }
@endphp
<dl class="inline_list {!! $sport !!}_box schedule {!! $css ?? '' !!} clearfix">
    <dt class="row-head title">{!! FrameworkConfig::get('debear.setup.season.viewing') !!} Schedule</dt>
    @foreach ($schedule as $game)
        {{-- Bye Week? --}}
        @if ($game->isRegular() && ($game->week != $last_week + 1))
            <dt class="row-head week">Week {!! $last_week + 1 !!}</dt>
                <dd class="row-{!! ++$i % 2 !!} bye">Bye Week</dd>
        @endif

        {{-- The scheduled game --}}
        @php
            $row_css = 'row-' . (++$i % 2);
            $last_week = $game->week;
        @endphp
        <dt class="row-head week">{!! $game->isRegular() ? "Week {$game->week}" : $game->title_short !!}</dt>
            <dd class="{!! $row_css !!} opp {!! $game->{$game->team_opp}->logoRightCSS('narrow_small') !!}">{!! $game->team_venue !!}</dd>
            <dd class="status {!! $row_css !!}">
                <span class="team-name"><a class="hidden-t hidden-d" href="{!! $game->{$game->team_opp}->link !!}"><span class="hidden">{!! $game->{$game->team_opp}->city !!} </span>{!! $game->{$game->team_opp}->franchise !!}</a></span>
                @if ($game->isComplete())
                    <span class="{!! $game->resultCSS() !!}">{!! $game->resultCode() !!} {!! $game->resultScore() !!}</span>
                    <a class="no_underline icon_game_boxscore" href="{!! $game->link !!}"></a>
                @else
                    {!! $game->isAbandoned() ? $game->status_disp : $game->start_time !!}
                @endif
            </dd>
    @endforeach
</dl>
