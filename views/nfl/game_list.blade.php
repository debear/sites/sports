@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

@if (\DeBear\Helpers\Sports\Common\ManageViews::getMode() != 'body')
    <h1>{!! $title !!}</h1>

    @include('sports.major_league.schedule.weekly.game_weeks')
@endif

<div class="game_week_content">
    @php
        $dates = $schedule->unique('game_date');
    @endphp
    @foreach ($dates as $date)
        <h3>{!! (new \Carbon\Carbon($date))->format('l jS F') !!}</h3>
        <ul class="grid inline_list">
            @foreach ($schedule->where('game_date', $date) as $game)
                <li class="{!! $grid !!} grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                    <ul class="inline_list nfl_box game_list {!! $list_view !!} clearfix">
                        <li class="team visitor {!! $game->visitor->rowCSS() !!}">
                            <span class="icon {!! $game->visitor->logoCSS() !!}">{!! $game->visitor->franchise !!}</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home {!! $game->home->rowCSS() !!}">
                            <span class="icon {!! $game->home->logoCSS() !!}">{!! $game->home->franchise !!}</span>
                        </li>
                        <li class="info">{!! $game->start_time !!}, {!! $game->venue !!}</li>
                        <li class="list visitor">@include("sports.nfl.$list_view.list", ['players' => $data->where('team_id', $game->visitor_id)])</li>
                        <li class="list home">@include("sports.nfl.$list_view.list", ['players' => $data->where('team_id', $game->home_id)])</li>
                    </ul>
                </li>
            @endforeach
        </ul>
    @endforeach
</div>

@endsection
