@php
    $num_periods = $game->num_periods;
@endphp
<dl class="periods clearfix">
    {{-- Header --}}
    @for ($i = 1; $i <= $num_periods; $i++)
        <dt class="period head">{!! $game->periodName($i) !!}</dt>
    @endfor
    <dt class="total head">Tot</dt>
    {{-- Visitor --}}
    <dd class="team {!! $game->visitor->logoCSS() !!}"></dd>
    @for ($i = 1; $i <= $num_periods; $i++)
        <dd class="period">{!! $game->visitor->scoring->where('period', $i)->score ?? 0 !!}</dd>
    @endfor
    <dt class="total head">{!! $game->visitor_score !!}</dt>
    {{-- Home --}}
    <dd class="team {!! $game->home->logoCSS() !!}"></dd>
    @for ($i = 1; $i <= $num_periods; $i++)
        <dd class="period">{!! $game->home->scoring->where('period', $i)->score ?? 0 !!}</dd>
    @endfor
    <dt class="total head">{!! $game->home_score !!}</dt>
</dl>
