@php
    // Build our column and group details
    $groups = [];
    $total_columns = 0;
    foreach (FrameworkConfig::get("debear.setup.players.groups.{$data->stat_group}.detailed") as $group) {
        $groups[$group] = \DeBear\Helpers\Sports\Namespaces::createObject("PlayerSeason{$group}")->getSeasonStatMeta();
        $total_columns += count($groups[$group]);
    }
@endphp
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season scrolltable-season-{!! $data->stat_group !!}" data-default="0" data-total="{!! $total_columns !!}" data-current="gp">
    <main>
        {{-- Header --}}
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        {{-- Seasons --}}
        @foreach ($data as $season)
            @php
                $row_css = 'row-' . ($loop->iteration % 2);
            @endphp
            <row>
                <cell class="row-head season">{!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($season->season) !!}</cell>
                <cell class="row-head season-type">{!! ucfirst($season->season_type) !!}</cell>
                <cell class="{!! $row_css !!} team"><span class="{!! \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($season->team_id) !!}">{!! $season->team_id !!}</span></cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top>
                {{-- Groups --}}
                @foreach (array_keys($groups) as $label)
                    <cell class="row-head group stats-{!! strtolower($label) !!}">{!! $label !!}</cell>
                @endforeach
                {{-- Stats by Group --}}
                <cell class="row-head misc gp" title="Games Played">GP</cell>
                <cell class="row-head misc gs" title="Games Started">GS</cell>
                @foreach ($groups as $label => $group)
                    @php
                        $ref = strtolower($label);
                    @endphp
                    @foreach ($group as $col => $def)
                        <cell class="row-head {!! $ref !!} {!! $col !!}" title="{!! $def['f'] !!}">{!! $def['s'] !!}</cell>
                    @endforeach
                @endforeach
            </top>
            {{-- Stats --}}
            @foreach ($data as $season)
                @php
                    $row_css = 'row-' . ($loop->iteration % 2);
                    $season->prepareStats();
                @endphp
                <row>
                    <cell class="{!! $row_css !!} misc gp">{!! $season->format('misc_gp') !!}</cell>
                    <cell class="{!! $row_css !!} misc gs">{!! $season->format('misc_gs') !!}</cell>
                    @foreach ($groups as $label => $group)
                        @php
                            $ref = strtolower($label);
                        @endphp
                        @foreach (array_keys($group) as $col)
                            <cell class="{!! $row_css !!} {!! $ref !!} {!! $col !!}">{!! $season->format("{$ref}_{$col}") !!}</cell>
                        @endforeach
                    @endforeach
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>
