@php
    // Build our column and group details
    $groups = [];
    $total_columns = 0;
    foreach (FrameworkConfig::get("debear.setup.players.groups.{$data->stat_group}.detailed") as $group) {
        $groups[$group] = \DeBear\Helpers\Sports\Namespaces::createObject("PlayerGame{$group}")->getGameStatMeta();
        $total_columns += count($groups[$group]);
    }
@endphp
<h3 class="games scrolltable-game-{!! $data->stat_group !!} row-head">{!! $data->season !!} Game-by-Game</h3>
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game scrolltable-game-{!! $data->stat_group !!}" data-default="0" data-total="{!! $total_columns !!}" data-current="gs">
    <main>
        {{-- Header --}}
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        {{-- Games --}}
        @foreach ($data as $game)
            @php
                $row_css = 'row-' . ($loop->iteration % 2);
                $is_home = ($game->team_id == $game->schedule->home_id);
                $game->schedule->team_type = ($is_home ? 'home' : 'visitor');
                $game->schedule->team_opp = ($is_home ? 'visitor' : 'home');
                $opp = "{$game->schedule->team_opp}_id";
                $opp_css = \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($game->schedule->$opp);
                if ($game->schedule->isRegular()) {
                    $date = "Week {$game->week}";
                } else {
                    $po_weeks = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'));
                    $date = FrameworkConfig::get('debear.setup.playoffs.rounds.' . $po_weeks[$game->week - 1] . '.short');
                }
            @endphp
            <row>
                <cell class="row-head date">{!! $date !!}</cell>
                <cell class="{!! $row_css !!} opp">
                    {!! $is_home ? 'v' : '@' !!}<span class="{!! $opp_css !!}">{!! $game->schedule->$opp !!}</span>
                </cell>
                <cell class="{!! $row_css !!} score">
                    <span class="{!! $game->schedule->resultCSS() !!}">{!! $game->schedule->resultCode() !!} {!! $game->schedule->resultScore() !!}</span>
                    <a class="icon_game_boxscore" href="{!! $game->schedule->link !!}"></a>
                </cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top>
                {{-- Groups --}}
                @foreach (array_keys($groups) as $label)
                    <cell class="row-head group stats-{!! strtolower($label) !!}">{!! $label !!}</cell>
                @endforeach
                {{-- Stats by Group --}}
                <cell class="row-head misc gs" title="Started Game">GS</cell>
                @foreach ($groups as $label => $group)
                    @php
                        $ref = strtolower($label);
                    @endphp
                    @foreach ($group as $col => $def)
                        <cell class="row-head {!! $ref !!} {!! $col !!}" title="{!! $def['f'] !!}">{!! $def['s'] !!}</cell>
                    @endforeach
                @endforeach
            </top>
            {{-- Stats --}}
            @foreach ($data as $game)
                @php
                    $row_css = 'row-' . ($loop->iteration % 2);
                    $game->prepareStats();
                @endphp
                <row>
                    <cell class="{!! $row_css !!} misc gs">{!! $game->format('misc_gs') !!}</cell>
                    @foreach ($groups as $label => $group)
                        @php
                            $ref = strtolower($label);
                        @endphp
                        @foreach (array_keys($group) as $col)
                            <cell class="{!! $row_css !!} {!! $ref !!} {!! $col !!}">{!! $game->format("{$ref}_{$col}") !!}</cell>
                        @endforeach
                    @endforeach
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>
