<dl class="{!! $sport !!}_box injuries clearfix">
    <dt class="row-head">Injury History</dt>
    @foreach ($data as $row)
        @php
            if ($row->game_type == 'regular') {
                $week = "Week {$row->week}";
            } else {
                $rounds = FrameworkConfig::get('debear.setup.playoffs.rounds');
                $po_codes = array_keys($rounds);
                $week = $rounds[$po_codes[$row->week - 1]]['short'];
            }
        @endphp
        <dt class="week">{!! $week !!}</dt>
            <dd class="status"><abbrev title="{!! $row->status_long !!}">{!! $row->status_short !!}</abbrev></dd>
            <dd class="injury">{!! $row->injury !!}</dd>
    @endforeach
</dl>
