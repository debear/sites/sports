{{-- Stat switcher --}}
@php
    $opt = [];
    foreach (array_unique(array_filter([$player->group, $player->group_core ?? '', 'defense', 'returns'])) as $alt_group) {
        $opt[] = "data-$alt_group=\"" . ($player->careerStats->sum("can_$alt_group") ? 'true' : 'false') . '"';
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="groups" {!! join(' ', $opt) !!}></div>
{{-- Table --}}
@include("sports.$sport.players.view.widgets.seasons", ['data' => $player->careerStats])
