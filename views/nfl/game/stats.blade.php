@include('sports.major_league.game.stats', ['stats' => [
    // Yards
    'Yards' => [
        [
            'label' => 'Total',
            'visitor' => $game->visitor->stats->yards_total,
            'home' => $game->home->stats->yards_total,
        ],
        [
            'label' => 'Passing',
            'visitor' => $game->visitor->stats->yards_pass,
            'home' => $game->home->stats->yards_pass,
        ],
        [
            'label' => 'Rushing',
            'visitor' => $game->visitor->stats->yards_rush,
            'home' => $game->home->stats->yards_rush,
        ],
        [
            'label' => 'Special Teams',
            'visitor' => $game->visitor->stats->yards_spctm,
            'home' => $game->home->stats->yards_spctm,
        ],
    ],

    // 1st Downs
    '1st Downs' => [
        [
            'label' => '1st Downs',
            'visitor' => $game->visitor->stats->downs_1st,
            'home' => $game->home->stats->downs_1st,
        ],
        [
            'label' => 'Passing',
            'visitor' => $game->visitor->stats->downs_1st_pass,
            'home' => $game->home->stats->downs_1st_pass,
        ],
        [
            'label' => 'Rushing',
            'visitor' => $game->visitor->stats->downs_1st_rush,
            'home' => $game->home->stats->downs_1st_rush,
        ],
        [
            'label' => 'Penalty',
            'visitor' => $game->visitor->stats->downs_1st_pen,
            'home' => $game->home->stats->downs_1st_pen,
        ],
    ],

    // Conversions
    'Conversions' => [
        [
            'label' => '3rd Down',
            'visitor' => $game->visitor->stats->downs_3rd_cnv,
            'home' => $game->home->stats->downs_3rd_cnv,
            'visitor_label' => $game->visitor->stats->downs_3rd_cnv . ' / ' . $game->visitor->stats->downs_3rd_num,
            'home_label' => $game->home->stats->downs_3rd_cnv . ' / ' . $game->home->stats->downs_3rd_num,
        ],
        [
            'label' => '4th Down',
            'visitor' => $game->visitor->stats->downs_4th_cnv,
            'home' => $game->home->stats->downs_4th_cnv,
            'visitor_label' => $game->visitor->stats->downs_4th_cnv . ' / ' . $game->visitor->stats->downs_4th_num,
            'home_label' => $game->home->stats->downs_4th_cnv . ' / ' . $game->home->stats->downs_4th_num,
        ],
        [
            'label' => 'Red Zone',
            'visitor' => $game->visitor->stats->rz_cnv,
            'home' => $game->home->stats->rz_cnv,
            'visitor_label' => $game->visitor->stats->rz_cnv . ' / ' . $game->visitor->stats->rz_num,
            'home_label' => $game->home->stats->rz_cnv . ' / ' . $game->home->stats->rz_num,
        ],
        [
            'label' => 'Goal-to-Go',
            'visitor' => $game->visitor->stats->goal_cnv,
            'home' => $game->home->stats->goal_cnv,
            'visitor_label' => $game->visitor->stats->goal_cnv . ' / ' . $game->visitor->stats->goal_num,
            'home_label' => $game->home->stats->goal_cnv . ' / ' . $game->home->stats->goal_num,
        ],
    ],

    // Penalties
    'Penalties' => [
        [
            'label' => 'Yards Lost',
            'visitor' => $game->visitor->stats->pens_yards,
            'home' => $game->home->stats->pens_yards,
        ],
        [
            'label' => 'Number',
            'visitor' => $game->visitor->stats->pens_num,
            'home' => $game->home->stats->pens_num,
        ],
    ],

    // Turnovers
    'Turnovers' => [
        [
            'label' => 'Total',
            'visitor' => $game->visitor->stats->to_tot,
            'home' => $game->home->stats->to_tot,
        ],
        [
            'label' => 'Interceptions',
            'visitor' => $game->visitor->stats->to_int,
            'home' => $game->home->stats->to_int,
        ],
        [
            'label' => 'Fumbles',
            'visitor' => $game->visitor->stats->to_fumb,
            'home' => $game->home->stats->to_fumb,
        ],
        [
            'label' => 'Fumbles Lost',
            'visitor' => $game->visitor->stats->to_fumb_lost,
            'home' => $game->home->stats->to_fumb_lost,
        ],
    ],

    // Miscellaneous
    'Miscellaneous' => [
        [
            'label' => 'Time of Possession',
            'visitor' => $game->visitor->stats->time_of_poss_sec,
            'home' => $game->home->stats->time_of_poss_sec,
            'visitor_label' => substr($game->visitor->stats->time_of_poss, 3),
            'home_label' => substr($game->home->stats->time_of_poss, 3),
        ],
    ],
]])
