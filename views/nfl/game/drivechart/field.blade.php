<ul class="inline_list marks">
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"><div class="extra_point visitor"></div></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">1</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="digit">1</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">2</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="digit">2</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">3</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="digit">3</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">4</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="digit">4</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid {!! $game->fieldLogo() !!}"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="digit">5</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="digit">5</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="digit">4</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">4</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="digit">3</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">3</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="digit">2</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">2</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num"><span class="digit">0</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num right"><span class="digit">1</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            <li class="num right"><span class="arrow">&lt;</span><span class="digit">1</span></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="num"><span class="digit">0</span><span class="arrow">&gt;</span></li>
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
    <li class="block last">
        <ul class="inline_list hash_top">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
        <div class="hash_mid">
            <ul class="inline_list hash_mid">
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
                <li class="mid"><div class="extra_point home"></div></li>
                <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
            </ul>
        </div>
        <ul class="inline_list hash_bot">
            <li class="hash"></li><li class="hash"></li><li class="hash"></li><li class="hash"></li>
        </ul>
    </li>
</ul>
