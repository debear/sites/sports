@php
    $pct_ratio = 8;
    $drive_bars = [];
@endphp
<div class="field-drive-chart hidden-m hidden-t">
    <ul class="inline_list pitch num_{!! $game->drives->count() !!} clearfix">
        {{-- Visiting Endzone --}}
        <li class="endzone visitor">
            <span class="{!! $game->visitor->logoCSS('small') !!} {!! $game->visitor->logoRightCSS('small') !!}">{!! strtoupper($game->visitor->franchise) !!}</span>
        </li>
        <li class="drives">
            {{-- Field Markings --}}
            @include('sports.nfl.game.drivechart.field')
            <ul class="inline_list drives">
                {{-- Individual Drives --}}
                @foreach ($game->drives as $drive)
                    @php
                        $is_home = ($drive->team_id == $game->home_id);
                        $team = ($is_home ? $game->home : $game->visitor);
                        /* Placement and Width */
                        $start = (isset($drive->start_pos_half) && $drive->start_pos_half == $drive->team_id ? $drive->start_pos_yd : 50 + (50 - $drive->start_pos_yd)) * $pct_ratio;
                        $len = ($drive->yards_net * $pct_ratio);
                        if ($drive->yards_net < 0) {
                            // Negative adjustments
                            $len = abs($len) - $pct_ratio; // Remove the end of dive arrow
                            $start -= $len;
                        } elseif (!$drive->yards_net) {
                            // No yardage - make sure we have something visual
                            $len = 1;
                        } else {
                            // Remove the end of dive arrow
                            $len = $len - $pct_ratio;
                        }
                        // Flip for the home team (starts from the right)
                        if ($is_home) { $start = 800 - $len - $start; }
                        // Store for our <style tag later
                        $drive_bars[$loop->index] = [
                            'left' => "{$start}px",
                            'width' => "{$len}px",
                        ];

                        /* Determine trailing arrow direction */
                        $arrow_left = (($is_home && $drive->yards_net >= 0) || (!$is_home && $drive->yards_net < 0));
                    @endphp
                    <li class="drive drive-{!! $loop->index !!} arrow_{!! $arrow_left ? 'left' : 'right' !!}">
                        <div class="bar {!! $team->rowCSS() !!}"></div>
                        <div class="summary hidden">
                            <span class="{!! $team->logoCSS() !!}">{!! $team->franchise !!}</span>:
                            {!! Format::pluralise($drive->num_plays, ' play') !!},
                            {!! Format::pluralise($drive->yards_net, ' yard', ['format-number' => true]) !!};
                            Time of Possession: {!! ltrim(substr($drive->time_of_poss, 3), '0') !!};
                            Result: {!! $drive->end_result !!}
                        </div>
                    </li>
                @endforeach
            </ul>
        </li>
        {{-- Home Endzone --}}
        <li class="endzone home">
            <span class="{!! $game->home->logoCSS('small') !!} {!! $game->home->logoRightCSS('small') !!}">{!! strtoupper($game->home->franchise) !!}</span>
        </li>
    </ul>
</div>
<style {!! $nonce !!}>
    @foreach ($drive_bars as $key => $bar)
        .field-drive-chart .drive-{!! $key !!} .bar { left: {!! $bar['left'] !!}; width: {!! $bar['width'] !!}; }
    @endforeach
</style>
