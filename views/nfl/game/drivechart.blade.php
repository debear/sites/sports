{{-- Visual chart --}}
@include('sports.nfl.game.drivechart.visual')

{{-- Tabular list --}}
@foreach (['visitor', 'home'] as $team)
    <div class="{!! $sport !!}_box team-drive-chart {!! $team !!} clearfix">
        <div class="row-title {!! $game->$team->rowCSS() !!}"><span class="{!! $game->$team->logoCSS() !!} icon">{!! $game->$team->name !!}</span></div>
        <div class="inner">
            <dl>
                <dt class="row-head num">#</dt>
                <dt class="row-head start_time">Start Time</dt>
                <dt class="row-head top">Time</dt>
                <dt class="row-head obtained">Obtained</dt>
                <dt class="row-head start_pos">Start Pos</dt>
                <dt class="row-head end_pos">End Pos</dt>
                <dt class="row-head plays">Play</dt>
                <dt class="row-head yards">Yds</dt>
                <dt class="row-head result">Result</dt>

                @foreach ($game->$team->drives as $drive)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <dt class="row-head num">{!! $loop->iteration !!}</dt>
                    <dd class="{!! $row_css !!} start_time">Q{!! $drive->start_quarter !!} {!! substr($drive->start_time, 3) !!}</dd>
                    <dd class="{!! $row_css !!} top">{!! ltrim(substr($drive->time_of_poss, 3), '0') !!}</dd>
                    <dd class="{!! $row_css !!} obtained">{!! $drive->obtained !!}</dd>
                    <dd class="{!! $row_css !!} start_pos">{!! $drive->start_pos !!}</dd>
                    <dd class="{!! $row_css !!} end_pos">{!! $drive->end_pos !!}</dd>
                    <dd class="{!! $row_css !!} plays">{!! $drive->num_plays !!}</dd>
                    <dd class="{!! $row_css !!} yards">{!! $drive->yards_net !!}</dd>
                    <dd class="{!! $row_css !!} result">{!! $drive->end_result !!}</dd>
                @endforeach
            </dl>
        </div>
    </div>
@endforeach
