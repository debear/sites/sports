{{-- Pre-calc the section heights --}}
@php
    $sect_rows = ['home' => [], 'visitor' => []];
    foreach (['passing', 'passing', 'rushing', 'receiving', 'kicking', 'punting', 'kick_ret', 'punt_ret', 'fumbles', 'def_tckl'] as $sect) {
        foreach (['home', 'visitor'] as $team) {
            $sect_rows[$team][$sect] = $game->$team->$sect->count();
        }
    }
    $alt_team = ['home' => 'visitor', 'visitor' => 'home'];
@endphp

{{-- Render --}}
@foreach (['visitor', 'home'] as $team)
    <ul class="inline_list {!! $sport !!} boxscore clearfix">
        <li class="{!! $game->$team->rowCSS() !!} team"><span class="{!! $game->$team->logoCSS() !!} icon">{!! $game->$team->name !!}</span></li>

        {{-- Passing --}}
        @php
            $passers = $game->$team->passing;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['passing'] - $sect_rows[$team]['passing']);
        @endphp
        <li class="cell section passer {!! $game->$team->rowCSS() !!}">Passing</li>
        <li class="static passer {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($passers as $passer)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $passer->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll passer {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat_wide {!! $game->$team->rowCSS() !!}">Atts</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Yds</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">TD</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Int</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Long</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Sack</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">Rating</li>

                @foreach ($passers as $passer)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat_wide {!! $row_css !!}">{!! $passer->cmp !!} / {!! $passer->atts !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $passer->yards !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $passer->td !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $passer->int !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $passer->long !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $passer->sacked !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! $passer->rating !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Rushing --}}
        @php
            $rushers = $game->$team->rushing;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['rushing'] - $sect_rows[$team]['rushing']);
        @endphp
        <li class="cell section rusher {!! $game->$team->rowCSS() !!}">Rushing</li>
        <li class="static rusher {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($rushers as $rusher)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $rusher->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll rusher {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">Atts</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Yds</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">TD</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Long</li>

                @foreach ($rushers as $rusher)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $rusher->atts !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $rusher->yards !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $rusher->td !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $rusher->long !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Receiving --}}
        @php
            $receivers = $game->$team->receiving;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['receiving'] - $sect_rows[$team]['receiving']);
        @endphp
        <li class="cell section receiver {!! $game->$team->rowCSS() !!}">Receiving</li>
        <li class="static receiver {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($receivers as $receiver)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $receiver->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll receiver {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">Tgts</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Rec</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Yds</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">TD</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Long</li>

                @foreach ($receivers as $receiver)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $receiver->targets !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $receiver->recept !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $receiver->yards !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $receiver->td !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $receiver->long !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Kicking --}}
        @php
            $kickers = $game->$team->kicking;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['kicking'] - $sect_rows[$team]['kicking']);
        @endphp
        <li class="cell section kicker {!! $game->$team->rowCSS() !!}">Kicking</li>
        <li class="static kicker {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($kickers as $kicker)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $kicker->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll kicker {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">FG</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">XP</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">50+</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">40-49</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">30-39</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">20-29</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">0-19</li>

                @foreach ($kickers as $kicker)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $kicker->fg_made !!} / {!! $kicker->fg_att !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kicker->xp_made !!} / {!! $kicker->xp_att !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kicker->fg_o50 !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kicker->fg_u50 !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kicker->fg_u40 !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kicker->fg_u30 !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kicker->fg_u20 !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Punting --}}
        @php
            $punters = $game->$team->punting;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['punting'] - $sect_rows[$team]['punting']);
        @endphp
        <li class="cell section punter {!! $game->$team->rowCSS() !!}">Punting</li>
        <li class="static punter {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($punters as $punter)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $punter->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll punter {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">Num</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Yds</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Avg</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Net</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Long</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">TB</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">In 20</li>

                @foreach ($punters as $punter)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $punter->num !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punter->yards !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punter->num ? sprintf('%0.01f', $punter->yards / $punter->num) : '&ndash;' !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punter->net !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punter->long !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punter->tb !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punter->inside20 !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Kickoff Returns --}}
        @php
            $kick_rets = $game->$team->kick_ret;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['kick_ret'] - $sect_rows[$team]['kick_ret']);
        @endphp
        <li class="cell section kick_ret {!! $game->$team->rowCSS() !!}">Kickoff Returns</li>
        <li class="static kick_ret {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($kick_rets as $kick_ret)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $kick_ret->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll kick_ret {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">Num</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Yds</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Avg</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Long</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">TD</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">FC</li>

                @foreach ($kick_rets as $kick_ret)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $kick_ret->num !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kick_ret->yards !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kick_ret->num ? sprintf('%0.01f', $kick_ret->yards / $kick_ret->num) : '&ndash;' !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kick_ret->long !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kick_ret->td !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $kick_ret->fair_catch !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Punt Returns --}}
        @php
            $punt_rets = $game->$team->punt_ret;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['punt_ret'] - $sect_rows[$team]['punt_ret']);
        @endphp
        <li class="cell section punt_ret {!! $game->$team->rowCSS() !!}">Punt Returns</li>
        <li class="static punt_ret {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($punt_rets as $punt_ret)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $punt_ret->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll punt_ret {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">Num</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Yds</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Avg</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Long</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">TD</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">FC</li>

                @foreach ($punt_rets as $punt_ret)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $punt_ret->num !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punt_ret->yards !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punt_ret->num ? sprintf('%0.01f', $punt_ret->yards / $punt_ret->num) : '&ndash;' !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punt_ret->long !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punt_ret->td !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $punt_ret->fair_catch !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Fumbles --}}
        @php
            $fumbling = $game->$team->fumbles;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['fumbles'] - $sect_rows[$team]['fumbles']);
        @endphp
        <li class="cell section fumbles {!! $game->$team->rowCSS() !!}">Fumbles</li>
        <li class="static fumbles {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($fumbling as $fumbles)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $fumbles->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll fumbles {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">Num</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Lost</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Force</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Rec</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">Rec TD</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">OOB</li>

                @foreach ($fumbling as $fumbles)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $fumbles->num_fumbled !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $fumbles->num_lost !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $fumbles->num_forced !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $fumbles->rec_own + $fumbles->rec_opp !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}{ t($fumbles->rec_own_td + $fumbles->rec_opp_td !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $fumbles->oob !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Defense --}}
        @php
            $tackles = $game->$team->def_tckl;
            $row_pad = 'row-pad-' . max(0, $sect_rows[$alt_team[$team]]['def_tckl'] - $sect_rows[$team]['def_tckl']);
        @endphp
        <li class="cell section defense {!! $game->$team->rowCSS() !!}">Defense</li>
        <li class="static defense {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell jersey dbl-height {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head dbl-height player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($tackles as $def_tckl)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $def_tckl->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll defense {!! $row_pad !!}">
            <ul class="inline_list">
                <li class="cell cell-head cell-first dbl-height stat_wide {!! $game->$team->rowCSS() !!}">Tackles</li>
                <li class="cell cell-head stat dbl-height {!! $game->$team->rowCSS() !!}">Sack</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Sack Yds</li>
                <li class="cell cell-head stat dbl-height {!! $game->$team->rowCSS() !!}">TFL</li>
                <li class="cell cell-head stat dbl-height {!! $game->$team->rowCSS() !!}">Hit</li>
                <li class="cell cell-head stat dbl-height {!! $game->$team->rowCSS() !!}">PD</li>
                <li class="cell cell-head stat dbl-height {!! $game->$team->rowCSS() !!}">Int</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Int Yds</li>
                <li class="cell cell-head stat dbl-height {!! $game->$team->rowCSS() !!}">Int TD</li>

                @foreach ($tackles as $def_tckl)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                        $def_pass = $game->$team->def_pass->where('jersey', $def_tckl->jersey);
                    @endphp
                    <li class="cell cell-first stat_wide {!! $row_css !!}">{!! $def_tckl->tckl !!} - {!! $def_tckl->asst !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_tckl->sacks !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_tckl->sacks_yards !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_tckl->tfl !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_tckl->qb_hit !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_pass->pd ?? '&ndash;' !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_pass->int ?? '&ndash;' !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_pass->int_yards ?? '&ndash;' !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $def_pass->int_td ?? '&ndash;' !!}</li>
                @endforeach
            </ul>
        </li>

    </ul>
@endforeach

{{-- Officials --}}
@php
    $off_pos = [
        'r' => 'Referee',
        'u' => 'Umpire',
        'hl' => 'Head Linesman',
        'lj' => 'Line Judge',
        'sj' => 'Side Judge',
        'fj' => 'Field Judge',
        'bj' => 'Back Judge',
        'ro' => 'Replay Official',
    ];
@endphp
<ul class="inline_list {!! $sport !!}_box officials clearfix">
    <li class="row-head">Officials</li>

    @foreach ($off_pos as $key => $label)
        @php
            $official = $game->officials->where('official_type', $key);
        @endphp
        <li class="row-0">{!! $label !!}:</li>
        <li class="row-1">{!! $official->official_num ? "#{$official->official_num} " : '' !!}{!! $official?->official?->name ?? '<em>Unknown Official</em>' !!}</li>
    @endforeach
</ul>
