<dl class="{!! $sport !!}_box playlist scoring clearfix">
    <dt class="row-head type">Scoring</dt>
    @for($quarter = 1; $quarter <= $game->num_periods; $quarter++)
        <dt class="row-head period">{!! $game->periodNameFull($quarter) !!}</dt>
        @forelse ($game->plays->where('quarter', $quarter) as $play)
            @php
                $row_css = 'row-' . ($loop->index % 2);
                $team = ($play->team_id == $game->home_id ? $game->home : $game->visitor);
            @endphp
            <dd class="{!! $row_css !!} {!! $team->logoCSS() !!} time">{!! substr($play->time, 3) !!}</dd>
            <dd class="{!! $row_css !!} play">
                <div class="score"><span class="visitor {!! $game->visitor->logoCSS() !!}">{!! $play->visitor_score !!}</span> &ndash; <span class="home {!! $game->home->logoRightCSS() !!}">{!! $play->home_score !!}</span></div>
                {!! $play->play !!}
            </dd>
        @empty
            <dd class="row-0 none"><em>No Scoring</em></dd>
        @endforelse
    @endfor
</dl>
