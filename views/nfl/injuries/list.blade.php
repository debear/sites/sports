<ul class="inline_list">
    @if (!$players->isset())
        <li class="row-0 na">No Injuries Reported</li>
    @else
        @php
            $last_status = '';
        @endphp
        @foreach ($players as $player)
            @php
                $row_css = 'row-' . ($loop->index % 2);
            @endphp
            @if ($player->status_long != $last_status)
                <li class="head status">{!! $player->status_long !!}</li>
            @endif
            <li class="{!! $row_css !!} pos">{!! $player->pos !!}</li>
            <li class="{!! $row_css !!} player"><a href="{!! $player->player->link !!}">{!! $player->player->name !!}</a><span class="injury">{!! $player->injury !!}</span></li>
            @php
                $last_status = $player->status_long;
            @endphp
        @endforeach
    @endif
</ul>
