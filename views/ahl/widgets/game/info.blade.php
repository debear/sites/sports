<li class="info">
    {{-- Three Stars --}}
    @if ($game->three_stars->isset())
        <dl class="three_stars">
            <dt>Three Stars</dt>
            @foreach ($game->three_stars as $star)
                @php
                    $star_team = ($star->team_id == $game->home_id ? $game->home : $game->visitor);
                @endphp
                <dd class="icon icon_game_star{!! $star->star !!}">
                    <span class="icon {!! $star_team->logoCSS() !!}"><a href="{!! $star->link !!}">{!! $star->name_short !!}</a></span>
                </dd>
            @endforeach
        </dl>
    @endif
    {{-- Goalies of Record --}}
    <dl class="goalies">
        @php
            if ($game->home_score > $game->visitor_score) {
                $goalie_winning = $game->home_goalie;
                $team_win = $game->home;
                $goalie_losing = $game->visitor_goalie;
                $team_loss = $game->visitor;
            } elseif ($game->home_score < $game->visitor_score) {
                $goalie_winning = $game->visitor_goalie;
                $team_win = $game->visitor;
                $goalie_losing = $game->home_goalie;
                $team_loss = $game->home;
            }
        @endphp
        @isset ($goalie_winning)
            <dt>Winning Goalie:</dt>
                <dd class="icon_right {!! $team_win->logoRightCSS() !!}">
                    <a href="{!! $goalie_winning->link !!}">{!! $goalie_winning->name_short !!}</a></dd>
        @endisset
        @isset ($goalie_losing)
            <dt>Losing Goalie:</dt>
                <dd class="icon_right {!! $team_loss->logoRightCSS() !!}">
                    <a href="{!! $goalie_losing->link !!}">{!! $goalie_losing->name_short !!}</a></dd>
        @endisset
    </dl>
</li>
