@php
    $matchups_round_max = 2;
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1><span class="hidden-m">{!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($season) !!}</span> Pacific Divsion Playoffs</h1>

<ul class="inline_list bracket bracket-v2020 clearfix">
    @include('sports.major_league.playoffs.bracket.conf', ['conf' => $conferences, 'include_all' => true, 'left' => true])
    {{-- Pacific Division Playoff Logo --}}
    <li class="pac">
        <div class="large playoffs-ahl-large-0000 playoffs-ahl-large-{!! FrameworkConfig::get('debear.setup.season.viewing') !!}"></div>
    </li>
</ul>

@endsection
