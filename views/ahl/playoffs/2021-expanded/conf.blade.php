@php
    // The matchups for this conference
    $matchups = $bracket->where('higher_conf_id', $conf->grouping_id);
    $bracket_rounds = $bracket_grouped[$conf->grouping_id];
    // Reverse display when viewing the right hand conference
    $reverse = (isset($right) && $right);
    if ($reverse) {
        $bracket_rounds = array_reverse($bracket_rounds, true);
    }
    // How many total series?
    $series_num_conf = count($bracket_grouped[$conf->grouping_id]);
    $series_num_tot = (2 * $series_num_conf) + 1;
    $series_num_div = $series_num_conf - 1;
    $num_rounds = count($config['series']);
    // Appropriate seeds for conf/div titles
    $seeds_conf = $seeds->where('conf_id', $conf->grouping_id);
    $obj_conf = $seeds_conf->where('seed', 1)->team->conference;
    // Details of the divisions in this conference
    $initial_matchups = $matchups->where('round_num', 1);
    $obj_div_t = $seeds_conf->where('seed', $initial_matchups->first()->higher_seed)->team->division;
    $obj_div_b = $seeds_conf->where('seed', $initial_matchups->last()->higher_seed)->team->division;
@endphp

<li class="grid-{!! $series_num_conf !!}-{!! $series_num_tot !!} rounds-{!! $num_rounds !!} {!! !$reverse ? 'conf-left' : 'conf-right' !!}">
    {{-- Header --}}
    @if ($obj_conf->name)
        <h3 class="conf {!! $obj_conf->rowCSS() !!} {!! $obj_conf->logoCSS('narrow-small') !!} {!! $obj_conf->logoRightCSS('narrow-small') !!}">
            <span class="full">{!! $obj_conf->name_full !!}</span>
            <span class="short">{!! $obj_conf->name !!}</span>
        </h3>
    @endif

    {{-- Top Division --}}
    <h3 class="div-top grid-{!! $series_num_div !!}-{!! $series_num_conf !!} {!! $obj_conf->rowCSS() !!}">
        <span class="full">{!! $obj_div_t->name_full !!}</span>
        <span class="short">{!! $obj_div_t->name !!}</span>
    </h3>

    {{-- Bracket --}}
    <ul class="inline_list clearfix">
        @foreach ($bracket_rounds as $round => $list)
            @if (is_array($list))
                <li class="grid-1-{!! $series_num_conf !!} round-{!! $round !!}">
                    {{-- Grouped divisionally up to Division Final --}}
                    @foreach (array_keys($div_list[$conf->grouping_id]) as $div_id)
                        @php
                            $round_matchups = ($list[$div_id] ?? []);
                            $heights = $div_sizes["{$conf->grouping_id}:$div_id"];
                        @endphp
                        @if ($heights['me'] < $heights['opp'])
                            <div class="filler"></div>
                        @endif
                        <div class="num-{!! !is_array($round_matchups) ? $round_matchups->count() : count($round_matchups) !!}-{!! $heights['me'] !!}">
                            @foreach ($round_matchups as $m)
                                @include('sports.major_league.playoffs.bracket.matchup')
                            @endforeach
                        </div>
                        @if ($heights['me'] < $heights['opp'])
                            <div class="filler"></div>
                        @endif
                    @endforeach
                </li>
            @else
                <li class="grid-1-{!! $series_num_conf !!} round-{!! $round !!} div-{!! $div_sizes['top'] !!}-{!! $div_sizes['bot'] !!}">
                    {{-- Conference Final --}}
                    @includeWhen($list->count(), 'sports.major_league.playoffs.bracket.matchup', ['m' => $list])
                </li>
            @endif
        @endforeach
    </ul>

    {{-- Bottom Division --}}
    <h3 class="div-bot grid-{!! $series_num_div !!}-{!! $series_num_conf !!} {!! $obj_conf->rowCSS() !!}">
        <span class="full">{!! $obj_div_b->name_full !!}</span>
        <span class="short">{!! $obj_div_b->name !!}</span>
    </h3>
</li>
