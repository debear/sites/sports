@php
    // Group the matchups and determine each size
    $bracket_grouped = array_fill_keys(
        $conferences->unique('grouping_id'),
        array_fill_keys(array_slice(array_keys($config['series']), 0, -1), [])
    );
    $div_ref = [];
    $div_list = array_fill_keys($conferences->unique('grouping_id'), []);
    foreach ($bracket->whereIn('round_num', array_slice(array_keys($config['series']), 0, -2)) as $series) {
        $c = $series->higher->conf_id;
        $d = $series->higher->div_id;
        $div_ref["$c:$d"] = true;
        if (!isset($bracket_grouped[$c][$series->round_num][$d])) {
            $bracket_grouped[$c][$series->round_num][$d] = [];
        }
        $bracket_grouped[$c][$series->round_num][$d][] = $bracket->where('round_code', $series->round_code);
        // For the ordered list of divisions, rely on the DSF (round 1) when we know there is info for each division available
        if ($series->round_num == 1) {
            $div_list[$c][$d] = true;
        }
    }
    // Conference Final is a single matchup
    foreach ($conferences as $conf) {
        $r = max(array_keys($bracket_grouped[$conf->grouping_id]));
        $bracket_grouped[$conf->grouping_id][$r] = $bracket->where('higher_conf_id', $conf->grouping_id)->where('round_num', $r);
    }
    // Determine the max sizes
    $max_by_div = [];
    $max_by_conf = array_fill_keys($conferences->unique('grouping_id'), 0);
    foreach (array_keys($div_ref) as $ref) {
        list($c, $d) = explode(':', $ref);
        $first = (isset($bracket_grouped[$c][0][$d]) ? count($bracket_grouped[$c][0][$d]) : 0);
        $dsf = (isset($bracket_grouped[$c][1][$d]) ? count($bracket_grouped[$c][1][$d]) : 0);
        $max_by_div[$ref] = max($first, $dsf);
        $max_by_conf[$c] += $max_by_div[$ref];
    }
    $matchups_round_max = max($max_by_conf);
    // Then the "opposite" division sizes (for filler calcs)
    $div_sizes = [];
    list($confA, $confB) = array_keys($div_list);
    list($confA_divA, $confA_divB) = array_keys($div_list[$confA]);
    list($confB_divA, $confB_divB) = array_keys($div_list[$confB]);
    $div_sizes["$confA:$confA_divA"] = ['me' => $max_by_div["$confA:$confA_divA"], 'opp' => $max_by_div["$confB:$confB_divA"]];
    $div_sizes["$confA:$confA_divB"] = ['me' => $max_by_div["$confA:$confA_divB"], 'opp' => $max_by_div["$confB:$confB_divB"]];
    $div_sizes["$confB:$confB_divA"] = ['me' => $max_by_div["$confB:$confB_divA"], 'opp' => $max_by_div["$confA:$confA_divA"]];
    $div_sizes["$confB:$confB_divB"] = ['me' => $max_by_div["$confB:$confB_divB"], 'opp' => $max_by_div["$confA:$confA_divB"]];
    $div_sizes['top'] = max($max_by_div["$confA:$confA_divA"], $max_by_div["$confB:$confB_divA"]);
    $div_sizes['bot'] = max($max_by_div["$confA:$confA_divB"], $max_by_div["$confB:$confB_divB"]);
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! $title !!}</h1>

<ul class="inline_list bracket bracket-v2021 clearfix">
    {{-- Misc header info? --}}
    @isset ($config['info'])
        <li class="notice">
            <div class="box info icon_info"><span>{!! $config['info'] !!}</span></div>
        </li>
    @endisset
    {{-- First Conference --}}
    @include('sports.ahl.playoffs.2021-expanded.conf', ['conf' => $conferences->first(), 'left' => true])
    {{-- Championship Round --}}
    @include('sports.major_league.playoffs.bracket.champ')
    {{-- Second Conference --}}
    @include('sports.ahl.playoffs.2021-expanded.conf', ['conf' => $conferences->last(), 'right' => true])
</ul>

@endsection
