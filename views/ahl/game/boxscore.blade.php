@foreach (['visitor', 'home'] as $team)
    <ul class="inline_list {!! $sport !!}_box boxscore clearfix">
        <li class="{!! $game->$team->rowCSS() !!} team"><span class="{!! $game->$team->logoCSS() !!} icon">{!! $game->$team->name !!}</span></li>

        {{-- Skaters --}}
        @php
            $skaters = $game->$team->skaters->sortBy('name_sort');
        @endphp
        <li class="cell section skater {!! $game->$team->rowCSS() !!}">Skaters</li>
        <li class="static skater">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($skaters as $skater)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $skater->player_id);
                        $skater->pos = $lineup->pos;
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a>{!! isset($lineup->capt_status) ? " ({$lineup->capt_status})" : '' !!}</li>
                @endforeach
            </ul>
        </li>
        <li class="scroll skater">
            <ul class="inline_list">
                <li class="cell cell-head cell-first pos {!! $game->$team->rowCSS() !!}">Pos</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">G</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">A</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">+/-</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">PIM</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">SOG</li>

                @foreach ($skaters as $skater)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first pos {!! $row_css !!}">{!! $skater->pos !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $skater->goals !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $skater->assists !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $skater->plusminus !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $skater->pims !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $skater->shots !!}</li>
                @endforeach
            </ul>
        </li>

        {{-- Goalies --}}
        @php
            $goalies = $game->$team->goalies->sortByDesc(['start', 'gp']);
        @endphp
        <li class="cell section goalie {!! $game->$team->rowCSS() !!}">Goaltenders</li>
        <li class="static goalie">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($goalies as $goalie)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $goalie->player_id);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $row_css !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll goalie">
            <ul class="inline_list">
                <li class="cell cell-head cell-first decision {!! $game->$team->rowCSS() !!}">Decision</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">TOI</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">SA</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">GA</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">SV</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">SV%</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">GAA</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">PIM</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">Pts</li>

                @foreach ($goalies as $goalie)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first decision {!! $goalie->decisionCSS() ?? $row_css !!}">{!! $goalie->decision !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! $goalie->time_on_ice !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $goalie->shots_against !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $goalie->goals_against !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $goalie->saves !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! $goalie->save_pct !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! $goalie->goals_against_avg !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $goalie->pims !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $goalie->pts !!}</li>
                @endforeach
            </ul>
        </li>
    </ul>
@endforeach

{{-- Three Stars --}}
<ul class="inline_list {!! $sport !!}_box three_stars clearfix">
    <li class="row-head">Three Stars</li>
    @isset($game->three_star_sel)
        <li class="sel">Selected by {!! $game->three_star_sel !!}.</li>
    @endisset
    @foreach ($game->three_stars as $star)
        @php
            $star_team = ($star->team_id == $game->home_id ? $game->home : $game->visitor);
            $row_css = 'row-' . ($loop->index % 2);
        @endphp
        <li class="star icon icon_game_star{!! $star->star !!}">
            <div class="{!! $row_css !!} name icon {!! $star_team->logoCSS() !!}"><a href="{!! $star->link !!}"><span class="hidden-m">{!! $star->name !!}</span><span class="hidden-t hidden-d">{!! $star->name_short !!}</span></a></div>
            {!! $star->image() !!}
        </li>
    @endforeach
</ul>

{{-- Officials --}}
<ul class="inline_list {!! $sport !!}_box officials clearfix">
    <li class="row-head">Officials</li>

    <li class="row-0 referee">Referees:</li>
    <li class="row-0 linesman">Linesmen:</li>

    <li class="row-1 referee">
        @foreach ($game->officials->where('official_type', 'referee')->sortBy('official_id') as $official)
            <p>#{!! $official->official_id !!} {!! $official->official->name !!}</p>
        @endforeach
    </li>
    <li class="row-1 linesman">
        @foreach ($game->officials->where('official_type', 'linesman')->sortBy('official_id') as $official)
            <p>#{!! $official->official_id !!} {!! $official->official->name !!}</p>
        @endforeach
    </li>
</ul>
