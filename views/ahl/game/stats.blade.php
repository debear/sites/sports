@include('sports.major_league.game.stats', ['stats' => [[
    // Shot Attempts.
    [
        'label' => 'Shots',
        'visitor' => $game->visitor->stats->shots,
        'home' => $game->home->stats->shots,
    ],

    // Special Teams.
    [
        'label' => 'Power Play',
        'visitor' => $game->visitor->stats->ppg,
        'home' => $game->home->stats->ppg,
        'visitor_label' => "{$game->visitor->stats->ppg} / {$game->visitor->stats->ppa}",
        'home_label' => "{$game->home->stats->ppg} / {$game->home->stats->ppa}",
    ],
    [
        'label' => 'SHG',
        'visitor' => $game->visitor->stats->shg,
        'home' => $game->home->stats->shg,
    ],
    // Penalties.
    [
        'label' => 'PIMs',
        'visitor' => $game->visitor->stats->pims,
        'home' => $game->home->stats->pims,
    ]
]]])
