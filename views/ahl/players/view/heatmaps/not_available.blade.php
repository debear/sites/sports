@php
    $opt = [];
    foreach (FrameworkConfig::get("debear.setup.stats.details.player.{$player->group}.heatmaps") as $type) {
        $opt[] = "data-$type=\"true\"";
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="heatmaps" {!! join(' ', $opt) !!}></div>
<fieldset class="info icon_info unavailable">
    Heatmaps are only available from {!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle(FrameworkConfig::get('debear.setup.players.heatmap_start')) !!} onwards
</fieldset>
