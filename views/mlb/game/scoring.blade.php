@php
    $half_disp = ['top' => 'Top', 'bot' => 'Bottom'];
@endphp
<dl class="{!! $sport !!}_box playlist scoring clearfix">
    @for ($inning = 1; $inning <= $game->innings; $inning++)
        @foreach (['top', 'bot'] as $half)
            @php
                $plays = $game->plays->where('inning_ref', "$inning-$half");
                if (!$plays->count()) {
                    continue;
                }
                $batter_team = ($half == 'top' ? 'visitor' : 'home');
                $pitcher_team = ($half == 'top' ? 'home' : 'visitor');
            @endphp
            <dt class="row-head period">{!! $half_disp[$half] !!} {!! Format::ordinal($inning) !!}</dt>
            @foreach ($plays as $play)
                @php
                    $row_css = 'row-' . ($loop->index % 2);
                    // Batter / Pitcher?
                    $batter = $game->$batter_team->lineup->where('jersey', $play->batter)->player;
                    $mugshot = isset($batter) ? $batter->image('pbp') : null;
                    // WinProb calcs
                    if ($play->home_winprob > 50) {
                        $winprob = '<span class="' . $game->home->logoRightCSS() . '">' . $play->home_winprob . '%</span>';
                    } elseif ($play->visitor_winprob > 50) {
                        $winprob = '<span class="' . $game->visitor->logoRightCSS() . '">' . $play->visitor_winprob . '%</span>';
                    } elseif (isset($play->home_winprob)) {
                        $winprob = $play->home_winprob . '%';
                    } else {
                        $winprob = null;
                    }
                @endphp
                @isset($mugshot)
                    <dd class="mugshot mugshot-pbp">{!! $mugshot !!}</dd>
                @endisset
                <dd class="{!! $row_css !!} play {!! isset($mugshot) ? 'with' : 'no' !!}-mugshot" data-play="{!! $play->play_id !!}">
                    <ul class="inline_list">
                        @isset($winprob)
                            <li class="winprob">{!! $winprob !!}</li>
                        @endisset
                        <li class="score">
                            <span class="visitor {!! $game->visitor->logoCSS() !!}">{!! $play->visitor_score !!}</span> &ndash; <span class="home {!! $game->home->logoRightCSS() !!}">{!! $play->home_score !!}</span>
                        </li>
                        <li class="state">
                            <dl>
                                <dt class="outs">Outs:</dt>
                                    <dd class="cell first icon_pbp_mlb_out {!! $play->out < 1 ? 'dimmed' : '' !!}"></dd>
                                    <dd class="cell last icon_pbp_mlb_out {!! $play->out < 2 ? 'dimmed' : '' !!}"></dd>
                                <dt class="runners">Baserunners:</dt>
                                    <dd class="cell first last icon_pbp_mlb_bases_{!! $play->ob_3b !!}{!! $play->ob_2b !!}{!! $play->ob_1b !!}"></dd>
                            </dl>
                        </li>
                        <li class="play">{!! $play->description !!}</li>
                    </ul>
                </dd>
            @endforeach
        @endforeach
    @endfor
</dl>
