@php
    // Determine the list of players this affects.
    $players = array_fill_keys(array_keys($stats), []);
    foreach ($game->$team->$list as $row) {
        foreach ($stats as $key => $col) {
            if ($row->$key) {
                $row_num = '';
                if ($row->$key > 1) {
                    $row_num = " {$row->$key}";
                }
                if (isset($col['as_y2d']) && $col['as_y2d']) {
                    $y2d_key = "{$key}_y2d";
                    if ($row->$y2d_key) {
                        $row_num .= " ({$row->$y2d_key})";
                    }
                }
                $players[$key][] = '<a href="' . $row->player->link . '">' . $row->player->name_short . '</a>' . $row_num;
            }
        }
    }
    // Reduce to only those relevant categories
    $players = array_filter($players);
@endphp
@if (count($players))
    <li class="cell section batting {!! $game->$team->rowCSS() !!}">{!! $title !!}</li>
    @foreach ($players as $key => $list)
        <li class="row-{!! $loop->index % 2 !!} row-summary">
            <strong>{!! $stats[$key]['label'] !!}:</strong> {!! join(', ', $list) !!}
        </li>
    @endforeach
@endif
