@php
    // Determine the list of players this affects.
    $info = array_fill_keys(array_keys($stats), []);
    foreach ($sources as $source => $cols) {
        foreach ($cols as $col) {
            $col_parts = explode(',', $col);
            foreach (['visitor', 'home'] as $team) {
                foreach ($game->$team->$source as $row) {
                    $row_parts = [];
                    foreach ($col_parts as $part) {
                        $row_parts[] = $row->$part;
                    }
                    // Include this player?
                    if (count(array_filter($row_parts))) {
                        $info[$col][] = '<a href="' . $row->player->link . '">' . $row->player->name_short . '</a> ' . join('&ndash;', $row_parts);
                    }
                }
            }
        }
    }
    // Then the more ad hoc fields
    // - Umpires
    foreach ($game->officials as $official) {
        $info['umpires'][] = $official->umpire_pos . ': ' . $official->official->name;
    }
    // - Weather
    if ($game->weather_temp) {
        $info['weather'][] = $game->weather_temp . '&deg;F';
    }
    if ($game->weather_cond) {
        $info['weather'][] = $game->weather_cond;
    }
    if ($game->weather_wind_speed) {
        $info['wind'][] = $game->weather_wind_speed . 'mph';
    }
    if ($game->weather_wind_dir) {
        $info['wind'][] = $game->weather_wind_dir;
    }
    // - Attendance
    if ($game->attendance) {
        $info['att'][] = number_format($game->attendance);
    }
    // Reduce to only those relevant categories
    $info = array_filter($info);
@endphp
<ul class="inline_list {!! $sport !!}_box game_info clearfix">
    <li class="row-head">Game Info</li>
    @foreach ($info as $key => $list)
        <li class="row row-{!! $loop->index % 2 !!}">
            <strong>{!! $stats[$key]['label'] !!}:</strong> {!! join(', ', $list) !!}
        </li>
    @endforeach
</ul>
