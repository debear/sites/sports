@php
    $dom_id = 'winprob';
    $chart = $game->winProbabilityChart($dom_id);
@endphp
<div class="chart" id="{!! $dom_id !!}"></div>
<script {!! HTTP::buildCSPNonceTag() !!}>
    @foreach ($chart['extraJS'] as $name => $var)
        var {!! $name !!} = {!! json_encode($var) !!};
        @php
            unset($chart['extraJS'][$name]);
        @endphp
    @endforeach
    var chart = new Highcharts.Chart({!! Highcharts::decode($chart) !!});
</script>
