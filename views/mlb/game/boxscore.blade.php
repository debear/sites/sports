@foreach (['visitor', 'home'] as $team)
    <ul class="inline_list {!! $sport !!}_box boxscore clearfix">
        <li class="{!! $game->$team->rowCSS() !!} team"><span class="{!! $game->$team->logoCSS() !!} icon">{!! $game->$team->name !!}</span></li>

        {{-- Batters --}}
        @php
            $batters = $game->$team->batting->sortBy('order');
            $notes = [];
            $note_prefix_ph = 'a'; $note_prefix_pr = '1';
            $num_batters = $batters->count();
        @endphp
        <li class="cell section batter {!! $game->$team->rowCSS() !!}">Batters</li>
        <li class="static batter">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($batters as $batter)
                    @php
                        $lineup = $game->$team->lineup->where('player_id', $batter->player_id);
                        $batter->pos = $lineup->pos;
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                        $is_starter = !($batter->order % 100);
                        // Position list
                        $pos_list = join('&ndash;', array_filter([$lineup->pos, $lineup->pos_alt1, $lineup->pos_alt2, $lineup->pos_alt3, $lineup->pos_alt4, $lineup->pos_alt5, $lineup->pos_alt6, $lineup->pos_alt7, $lineup->pos_alt8]));
                        // Prefix for non-starter?
                        $prefix = '';
                        if (!$is_starter && isset($batter->note)) {
                            // Pinch Runner or Hitter?
                            $prefix = '<span class="prefix">' . ($lineup->pos_alt1 == 'PR' ? $note_prefix_pr++ : $note_prefix_ph++) . '</span>';
                            $notes[] = "$prefix{$batter->note}";
                        }
                        // We'll also grab the batter zones and split batting average for our data attribute
                        $batter_extras = [];
                        foreach ($game->$team->batter_zones->where('player_id', $batter->player_id) as $zone) {
                            // Zones
                            $batter_extras[] = 'data-zone-' . $zone->batter_hand . '-vs-' . $zone->pitcher_hand . '="' . $zone->heatzones . '"';
                            // Batting average
                            $split_key = 'avg_v' . $zone->pitcher_hand . 'HP';
                            $batter_extras[] = 'data-avg-vs-' . $zone->pitcher_hand . '="' . ($zone->$split_key ?? 'undef') . '"';
                        }
                    @endphp
                    <li class="cell jersey {!! $is_starter ? 'starter' : 'non-starter' !!} {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name {!! $is_starter ? 'starter' : 'non-starter' !!} {!! $row_css !!}" data-player="{!! $batter->team_id !!}-{!! $batter->jersey !!}" data-mugshot="{{ $player->image('medium') }}" data-stands="{!! $player->stands !!}" {!! join(' ', $batter_extras) !!}>{!! $prefix !!}<a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a><span class="pos">{!! $pos_list !!}</span></li>
                @endforeach
            </ul>
        </li>
        <li class="scroll batter">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat_wide {!! $game->$team->rowCSS() !!}">H/AB</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">R</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">HR</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">RBI</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">BB</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">K</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">Avg</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">OBP</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">OPS</li>

                @foreach ($batters as $batter)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat_wide {!! $row_css !!}">{!! $batter->h_ab !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $batter->r !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $batter->hr !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $batter->rbi !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $batter->bb !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $batter->k !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! ltrim(sprintf('%.03f', $batter->avg_y2d), '0') !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! ltrim(sprintf('%.03f', $batter->obp_y2d), '0') !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! ltrim(sprintf('%.03f', $batter->ops_y2d), '0') !!}</li>
                @endforeach
            </ul>
        </li>
        {{-- Notes? --}}
        @foreach ($notes as $note)
            <li class="note row-{!! ($num_batters + $loop->index) % 2 !!}">{!! $note !!}</li>
        @endforeach

        {{-- Batter Summary --}}
        @include('sports.mlb.game.boxscore.team_summary', ['title' => 'Batting', 'class' => 'batting', 'list' => 'batting', 'stats' => [
            '2b' => ['label' => '2B', 'as_y2d' => true],
            '3b' => ['label' => '3B', 'as_y2d' => true],
            'hr' => ['label' => 'HR', 'as_y2d' => true],
            'tb' => ['label' => 'TB'],
            'rbi' => ['label' => 'RBI', 'as_y2d' => true],
            'lob' => ['label' => 'LOB'],
            'sac_fly' => ['label' => 'Sac Fly'],
            'sac_hit' => ['label' => 'Sac Hit'],
            'gidp' => ['label' => 'GIDP'],
        ]])

        {{-- Baserunning Summary --}}
        @include('sports.mlb.game.boxscore.team_summary', ['title' => 'Baserunning', 'class' => 'baserunning', 'list' => 'batting', 'stats' => [
            'sb' => ['label' => 'SB', 'as_y2d' => true],
            'cs' => ['label' => 'CS', 'as_y2d' => true],
            'po' => ['label' => 'PO'],
        ]])

        {{-- Fielder Summary --}}
        @include('sports.mlb.game.boxscore.team_summary', ['title' => 'Fielding', 'class' => 'fielding', 'list' => 'fielding', 'stats' => [
            'e' => ['label' => 'E', 'as_y2d' => true],
        ]])

        {{-- Pitchers --}}
        @php
            $pitchers = $game->$team->pitchers->sortBy('from_outs');
        @endphp
        <li class="cell section pitcher {!! $game->$team->rowCSS() !!}">Pitchers</li>
        <li class="static pitcher">
            <ul class="inline_list">
                <li class="cell jersey {!! $game->$team->rowCSS() !!}">#</li>
                <li class="cell cell-head player {!! $game->$team->rowCSS() !!}">Player</li>

                @foreach ($pitchers as $pitcher)
                    @php
                        $lineup = $game->$team->lineup->where('jersey', $pitcher->jersey);
                        $player = $lineup->player;
                        $row_css = 'row-' . ($loop->index % 2);
                        $decision = $game->$team->pitching->where('player_id', $lineup->player_id)->decision;
                    @endphp
                    <li class="cell jersey {!! $game->$team->rowCSS() !!}">{!! $lineup->jersey !!}</li>
                    <li class="cell name starter {!! $row_css !!}" data-player="{!! $pitcher->team_id !!}-{!! $pitcher->jersey !!}" data-mugshot="{{ $player->image('medium') }}" data-throws="{!! $player->throws !!}"><a href="{!! $player->link !!}"><span class="name-short">{!! $player->name_short !!}</span><span class="name-full">{!! $player->name !!}</span></a>{!! $decision ? '<span class="decision">(' . $decision . ')</span>' : '' !!}</li>
                @endforeach
            </ul>
        </li>
        <li class="scroll pitcher">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat {!! $game->$team->rowCSS() !!}">IP</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">H</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">R</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">ER</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">BB</li>
                <li class="cell cell-head stat {!! $game->$team->rowCSS() !!}">K</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">ERA</li>
                <li class="cell cell-head stat_wide {!! $game->$team->rowCSS() !!}">WHIP</li>

                @foreach ($pitchers as $pitcher)
                    @php
                        $lineup = $game->$team->lineup->where('jersey', $pitcher->jersey);
                        $pitcher = $game->$team->pitching->where('player_id', $lineup->player_id);
                        $row_css = 'row-' . ($loop->index % 2);
                    @endphp
                    <li class="cell cell-first stat {!! $row_css !!}">{!! $pitcher->ip !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $pitcher->h !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $pitcher->r !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $pitcher->er !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $pitcher->bb !!}</li>
                    <li class="cell stat {!! $row_css !!}">{!! $pitcher->k !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! sprintf('%0.02f', $pitcher->era_y2d) !!}</li>
                    <li class="cell stat_wide {!! $row_css !!}">{!! sprintf('%0.02f', $pitcher->whip_y2d) !!}</li>
                @endforeach
            </ul>
        </li>
    </ul>
@endforeach

{{-- Game Info --}}
@include('sports.mlb.game.boxscore.game_summary', ['stats' => [
    'game_score' => ['label' => 'Game Scores'],
    'ibb' => ['label' => 'IBB'],
    'wp' => ['label' => 'WP'],
    'hbp' => ['label' => 'HBP'],
    'bk' => ['label' => 'Balk'],
    'pt,s' => ['label' => 'Pitches-Strikes'],
    'go,fo' => ['label' => 'Groundouts-Flyouts'],
    'bf' => ['label' => 'Batters Faced'],
    'ir,ira' => ['label' => 'Inherited Runners-Scored'],
    'umpires' => ['label' => 'Umpires'],
    'weather' => ['label' => 'Weather'],
    'wind' => ['label' => 'Wind'],
    'att' => ['label' => 'Attendance'],
], 'sources' => [
    'batting' => ['ibb', 'wp'],
    'pitching' => ['game_score', 'wp', 'bk', 'pt,s', 'go,fo', 'bf', 'ir,ira'],
]])
