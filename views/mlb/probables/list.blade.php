@php
    $announced = ($players->isset());
@endphp
<ul class="inline_list probable">
    <li class="name row-0">
        @if ($announced)
            <a href="{!! $players->player->link !!}">{!! $players->player->name !!}</a> ({!! $players->player->throws !!}HP)
        @else
            <em>To Be Announced</em>
        @endif
    </li>
    <li class="mugshot mugshot-medium">
        @if ($announced)
            {!! $players->player->image('medium') !!}
        @else
            {!! \DeBear\ORM\Sports\MajorLeague\Player::silhouette('medium') !!}
        @endif
    </li>
    @if ($announced)
        @if ($players->stats->isset())
            <li class="stats row-1">
                {!! sprintf('%d&ndash;%d, %0.02f ERA, %0.03f WHIP',
                    $players->stats->w, $players->stats->l,
                    $players->stats->era, $players->stats->whip) !!}
            </li>
        @else
            <li class="stats row-1">
                <em>First appearance {!! $game->isRegular() ? 'this season' : 'in the playoffs' !!}</em>
            </li>
        @endif
    @endif
    <li class="notes">
        @isset ($players->notes)
            <strong>MLB.com's Notes:</strong> {!! $players->notes !!}
        @endisset
    </li>
    @if ($announced && $players->catcher->isset())
        <li class="catcher">
            <strong>{!! $players->catcher_rating !!} Catcher:</strong> <a href="{!! $players->catcher->link !!}">{!! $players->catcher->name !!}</a>
        </li>
    @endif
</ul>
