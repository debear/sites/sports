@php
    $recent = $award->recent;
    $by_pos = ($recent->count() > 1);
    $detailed = isset($recent->player);
    $season = \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($recent->season ?? $award->season);
@endphp

<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="{!! $sport !!}_box award {!! $by_pos ? 'multi' : 'single' !!} clearfix">
        {{-- Award Details --}}
        <h3 class="row-head">{!! $award->name !!}</h3>

        {{-- Description --}}
        <p class="descrip">{!! $award->description !!}</p>

        @if (!$by_pos)
            {{-- Mugshot --}}
            <div class="mugshot-small">
                @if ($detailed)
                    {!! $recent->player->image($size) !!}
                @else
                    {!! \DeBear\ORM\Sports\MajorLeague\Player::silhouette($size) !!}
                @endif
            </div>

            {{-- Last Winner --}}
            <p class="winner">
                <strong>{!! $season !!} Winner:</strong>
                @if ($detailed)
                    @if ($recent->team->isset())
                        <span class="{!! $recent->team->logoCSS() !!}">
                    @endif
                    <a href="{!! $recent->player->link !!}">{!! $recent->player->name !!}</a>
                    @if ($recent->team->isset())
                        </span>
                    @endif
                @elseif ($recent->isset())
                    {!! $recent->player_name !!}
                @else
                    <em>Not Awarded</em>
                @endif

                @isset ($recent->pos)
                    ({!! $recent->pos !!})
                @endisset
            </p>
        @else
            {{-- Multiple Winners --}}
            <dl class="winners">
                <dt>{!! $season !!} Winners:</dt>
                @foreach ($recent as $winner)
                    <dt class="pos">{!! $winner->pos !!}</dt>
                        <dd>
                            @if ($winner->team->isset())
                                <span class="{!! $winner->team->logoCSS() !!}">
                            @endif
                            <a href="{!! $winner->player->link !!}">{!! $winner->player->name !!}</a>
                            @if ($winner->team->isset())
                                </span>
                            @endif
                        </dd>
                @endforeach
            </dl>
        @endif

        {{-- Link --}}
        <p class="link"><a class="all-time" data-link="{!! $award->link !!}">All-Time Winners</a></p>
    </fieldset>
</li>
