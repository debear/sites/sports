<ul class="inline_list">
    @if (!$players->isset())
        <li class="row-0 na">Not Yet Announced</li>
    @else
        @foreach ($players as $player)
            @php
                $row_css = 'row-' . ($loop->index % 2);
            @endphp
            <li class="{!! $row_css !!} head spot {!! !$player->spot ? 'dbl-width' : '' !!}">{!! $player->spot ?: 'SP' !!}</li>
            @if ($player->spot)
                <li class="{!! $row_css !!} pos">{!! $player->pos !!}</li>
            @endif
            <li class="{!! $row_css !!} name"><a href="{!! $player->player->link !!}">{!! $player->player->name !!}</a> ({!! $player->spot ? $player->player->stands : "{$player->player->throws}HP" !!})</li>
        @endforeach
    @endif
</ul>
