@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! $draft_title !!}</h1>

@include('skeleton.widgets.subnav')

@foreach (array_keys($subnav['list']) as $t)
    <div class="subnav-{!! $t !!} {!! $t != 'amateur' ? 'hidden' : '' !!} draft clearfix">
        @php
            $opt = [];
            foreach ($draft->where('draft_type', $t)->unique('round_descrip') as $code) {
                $round = $draft_rounds->where('code', $code);
                $opt[] = [
                    'id' => $code,
                    'label' => ($round->isset() ? $round->name : "Round $code"),
                ];
            }
            print (new Dropdown("draft-$t", $opt, [
                'value' => 1,
                'select' => false,
            ]))->render();
        @endphp
        <dl class="round clearfix"></dl>
    </div>
@endforeach
<div class="draft-data hidden">@json($draft->toJson())</div>

@endsection
