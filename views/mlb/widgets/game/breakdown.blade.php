@php
    $num_innings = $game->innings;
    $innings = [];
    $ellipsis = false; // That we've added the ellipsis
    $last = false;
    for ($inn = 1; $inn <= $num_innings; $inn++) {
        $col = sprintf('inn%02d', $inn);
        if ($inn < 10 || $game->visitor->scoring->$col || $game->home->scoring->$col) {
            $css = '';
            if ($inn > 10 && ($inn != ($last + 1)) && !$ellipsis) {
                $ellipsis = true;
                $css = 'ellipsis';
            }
            $innings[] = [
                'css' => $css,
                'num' => $inn,
                'col' => $col,
            ];
            $last = $inn;
        }
    }
@endphp
<dl class="periods clearfix">
    {{-- Header --}}
    @foreach ($innings as $inn)
        <dt class="period head {!! $inn['css'] !!}">{!! $inn['num'] !!}</dt>
    @endforeach
    <dt class="total head">R</dt>
    <dt class="total head">H</dt>
    <dt class="total head">E</dt>
    {{-- Visitor --}}
    <dd class="team {!! $game->visitor->logoCSS() !!}"></dd>
    @foreach ($innings as $inn)
        <dd class="period {!! $inn['css'] !!} row_0">{!! $game->visitor->scoring->{$inn['col']} ?? 'X' !!}</dd>
    @endforeach
    <dt class="total head">{!! $game->visitor->scoring->runs !!}</dt>
    <dt class="total head">{!! $game->visitor->scoring->hits !!}</dt>
    <dt class="total head">{!! $game->visitor->scoring->errors !!}</dt>
    {{-- Home --}}
    <dd class="team {!! $game->home->logoCSS() !!}"></dd>
    @foreach ($innings as $inn)
        <dd class="period {!! $inn['css'] !!} row_0">{!! $game->home->scoring->{$inn['col']} ?? 'X' !!}</dd>
    @endforeach
    <dt class="total head">{!! $game->home->scoring->runs !!}</dt>
    <dt class="total head">{!! $game->home->scoring->hits !!}</dt>
    <dt class="total head">{!! $game->home->scoring->errors !!}</dt>
</dl>
