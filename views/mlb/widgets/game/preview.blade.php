<li class="probables">
    <dl>
        <dt>Probable Pitchers:</dt>
            <dd class="{!! $game->visitor->logoCSS() !!}">
                @php
                    $probable = $game->visitor->probable;
                @endphp
                @if ($probable->isset())
                    <a href="{!! $probable->link !!}">{!! $probable->name_short !!}</a>
                    @isset ($probable->whip)
                        ({!! sprintf('%d-%d, %0.02f ERA, %0.02f WHIP', $probable->w, $probable->l, $probable->era, $probable->whip) !!})
                    @else
                        (<em>First Appearance</em>)
                    @endisset
                @else
                    <em>Not Yet Announced</em>
                @endif
            <dd class="{!! $game->home->logoCSS() !!}">
                @php
                    $probable = $game->home->probable;
                @endphp
                @if ($probable->isset())
                    <a href="{!! $probable->link !!}">{!! $probable->name_short !!}</a>
                    @isset ($probable->whip)
                        ({!! sprintf('%d-%d, %0.02f ERA, %0.02f WHIP', $probable->w, $probable->l, $probable->era, $probable->whip) !!})
                    @else
                        (<em>First Appearance</em>)
                    @endisset
                @else
                    <em>Not Yet Announced</em>
                @endif
    </dl>
</li>
