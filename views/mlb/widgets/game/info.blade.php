@php
    $hr_loops = [
        $game->visitor_id => [
            'css' => 'visitor',
            'team_css' => $game->visitor->logoCSS(),
            'players' => $game->home_runs->where('team_id', $game->visitor_id),
        ],
        $game->home_id => [
            'css' => 'home',
            'team_css' => $game->home->logoCSS(),
            'players' => $game->home_runs->where('team_id', $game->home_id),
        ],
    ];
@endphp
<li class="info clearfix">
    <dl class="pitchers clearfix">
        <dt>W:</dt>
            <dd>
                <a href="{!! $game->winning_pitcher->link !!}">{!! $game->winning_pitcher->name_short !!}</a>
                ({!! $game->winning_pitcher->w !!}&ndash;{!! $game->winning_pitcher->l !!})
            </dd>
        <dt>L:</dt>
            <dd>
                <a href="{!! $game->losing_pitcher->link !!}">{!! $game->losing_pitcher->name_short !!}</a>
                ({!! $game->losing_pitcher->w !!}&ndash;{!! $game->losing_pitcher->l !!})
            </dd>
        {{-- Any Save? --}}
        @if ($game->saving_pitcher->isset())
            <dt>SV:</dt>
                <dd>
                    <a href="{!! $game->saving_pitcher->link !!}">{!! $game->saving_pitcher->name_short !!}</a>
                    ({!! $game->saving_pitcher->sv !!})
                </dd>
        @endif
    </dl>
    <dl class="home_runs clearfix">
        <dt>Home Runs:</dt>
            @foreach ($hr_loops as $team_id => $hr)
                <dd class="team {!! $hr['css'] !!} {!! $hr['team_css'] !!}"></dd>
                <dd class="list {!! $hr['css'] !!}">
                    @forelse ($hr['players'] as $player)
                        @php
                            $num = join(', ', range($player->hr_y2d - $player->hr + 1, $player->hr_y2d));
                            $comma = (!$loop->last ? ',' : '');
                        @endphp
                        <span><a href="{!! $player->link !!}">{!! $player->name_short !!}</a> ({!! $num !!})</span>{!! $comma !!}
                    @empty
                        <em>None</em>
                    @endforelse
                </dd>
            @endforeach
    </dl>
</li>
