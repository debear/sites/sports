{{-- Stat switcher --}}
@php
    $opt = [];
    foreach (array_keys(FrameworkConfig::get('debear.setup.stats.details.player')) as $alt_group) {
        $opt[] = "data-$alt_group=\"" . ($player->situationalStats->sum("can_$alt_group") ? 'true' : 'false') . '"';
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="groups" {!! join(' ', $opt) !!}></div>
{{-- Table --}}
@php
    $all_types = $player->situationalStats->getSituationalStatTypes();
    $split_types = array_intersect(
        $player->situationalStats->unique('split_type'),
        array_keys($all_types),
    );
@endphp
@foreach ($split_types as $split)
    @include('sports.mlb.players.view.widgets.splits', [
        'type' => 'situational',
        'data' => $player->situationalStats->where('split_type', $split),
    ])
@endforeach
