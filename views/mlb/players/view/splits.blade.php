{{-- Stat switcher --}}
@php
    $opt = [];
    foreach (array_keys(FrameworkConfig::get('debear.setup.stats.details.player')) as $alt_group) {
        $opt[] = "data-$alt_group=\"" . ($player->splitStats->sum("can_$alt_group") ? 'true' : 'false') . '"';
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="groups" {!! join(' ', $opt) !!}></div>
{{-- Table --}}
@php
    $all_types = $player->splitStats->getSplitStatTypes();
    $split_types = array_intersect(
        $player->splitStats->unique('split_type'),
        array_keys($all_types),
    );
@endphp
@foreach ($split_types as $split)
    @php
        $split_data = $player->splitStats->where('split_type', $split);
        // Additional sorting?
        if ($split == 'opponent') {
            $split_data = $split_data->sort(function ($a, $b) {
                return $a['team']->franchise <=> $b['team']->franchise;
            });
        } elseif ($split == 'stadium') {
            $split_data = $split_data->sort(function ($a, $b) {
                return $a['venue']->stadium <=> $b['venue']->stadium;
            });
        }
    @endphp
    @include('sports.mlb.players.view.widgets.splits', [
        'type' => 'splits',
        'data' => $split_data,
    ])
@endforeach
