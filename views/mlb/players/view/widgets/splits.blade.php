@php
    $method = "get{$type}StatMeta";
    $columns = $data->$method();
@endphp
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits scrolltable-{!! $type !!}-{!! $data->stat_group !!} scrolltable-{!! $type !!}-{!! $data->split_type !!}" data-default="0" data-total="{!! count($columns) !!}" data-current="{!! array_key_first($columns) !!}">
    <main>
        {{-- Header --}}
        <top>
            <cell class="{!! $title['css'] ?? 'row-head' !!} split">
                @isset($title)
                    {!! $title['label'] !!}
                @else
                    {!! $all_types[$data->split_type] !!}
                @endisset
            </cell>
        </top>
        {{-- Splits --}}
        @foreach ($data as $row)
            @php
                $row_css = 'row-' . ($loop->iteration % 2);
            @endphp
            <row>
                <cell class="{!! $row_css !!} label">
                    @if ($type == 'battervspitcher')
                        <a href="{!! $row->player->link !!}">{!! $row->player->name !!}</a>
                    @elseif ($data->split_type == 'opponent')
                        <span class="{!! \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($row->split_label) !!} icon">{!! $row->team->franchise !!}</span>
                    @elseif ($data->split_type == 'stadium')
                        <span title="{!! $data->venue->stadium !!}">{!! $data->venue->stadium !!}</span>
                    @else
                        {!! $row->split_label !!}
                    @endif
                </cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top>
                @foreach ($columns as $col => $def)
                    <cell class="row-head {!! $col !!}" title="{!! $def['f'] !!}">{!! $def['s'] !!}</cell>
                @endforeach
            </top>
            {{-- Stats --}}
            @foreach ($data as $row)
                @php
                    $row_css = 'row-' . ($loop->iteration % 2);
                @endphp
                <row>
                    @foreach (array_keys($columns) as $col)
                        <cell class="{!! $row_css !!} {!! $col !!}">{!! $row->format($col) !!}</cell>
                    @endforeach
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>
