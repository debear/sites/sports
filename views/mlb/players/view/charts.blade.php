@php
    $charts = $player->zones->where('is_sel', 1);
    // Do we have an overriding nonce to apply from the parent tab?
    if (session()->has("ajax.players.{$player->player_id}.charts")) {
        FrameworkConfig::set(['debear.headers.csp.nonce' => session("ajax.players.{$player->player_id}.charts")]);
    }
    $nonce = HTTP::buildCSPNonceTag();
@endphp
{{-- Stat switcher --}}
@php
    $opt = [
        'CMB-CMB' => 'data-CMB-CMB="false"',
        'L-CMB' => 'data-L-CMB="false"',
        'R-CMB' => 'data-R-CMB="false"',
        'CMB-L' => 'data-CMB-L="false"',
        'L-L' => 'data-L-L="false"',
        'R-L' => 'data-R-L="false"',
        'CMB-R' => 'data-CMB-R="false"',
        'L-R' => 'data-L-R="false"',
        'R-R' => 'data-R-R="false"',
    ];
    foreach ($player->zones as $z) {
        $k = "{$z->batter_hand}-{$z->pitcher_hand}";
        $opt[$k] = "data-$k=\"true\"";
        if ($z->is_sel) {
            $opt['default'] = "data-default=\"$k\"";
        }
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="hands" {!! join(' ', $opt) !!}></div>
{{-- Strikezone --}}
<dl class="{!! $sport !!}_box heatzones clearfix">
    <dt class="row-head">Zone Charts</dt>
    <dd class="strikezone">
        <div class="z o t l"><span>{!! $charts->heatzone_otl_avg !!}</span></div>
        <div class="z o t r"><span>{!! $charts->heatzone_otr_avg !!}</span></div>
        <div class="z o b l"><span>{!! $charts->heatzone_obl_avg !!}</span></div>
        <div class="z o b r"><span>{!! $charts->heatzone_obr_avg !!}</span></div>
        <div class="z i t l"><span>{!! $charts->heatzone_itl_avg !!}</span></div>
        <div class="z i t c"><span>{!! $charts->heatzone_itc_avg !!}</span></div>
        <div class="z i t r"><span>{!! $charts->heatzone_itr_avg !!}</span></div>
        <div class="z i m l"><span>{!! $charts->heatzone_iml_avg !!}</span></div>
        <div class="z i m c"><span>{!! $charts->heatzone_imc_avg !!}</span></div>
        <div class="z i m r"><span>{!! $charts->heatzone_imr_avg !!}</span></div>
        <div class="z i b l"><span>{!! $charts->heatzone_ibl_avg !!}</span></div>
        <div class="z i b c"><span>{!! $charts->heatzone_ibc_avg !!}</span></div>
        <div class="z i b r"><span>{!! $charts->heatzone_ibr_avg !!}</span></div>
    </dd>
</dl>
{{-- Spray Chart --}}
<dl class="{!! $sport !!}_box spray-chart clearfix">
    <dt class="row-head">Spray Charts</dt>
    <dd class="num">
        <div class="l">{!! $charts->spray_l_pct !!}</div>
        <div class="l-c">{!! $charts->spray_lc_pct !!}</div>
        <div class="c">{!! $charts->spray_c_pct !!}</div>
        <div class="c-r">{!! $charts->spray_cr_pct !!}</div>
        <div class="r">{!! $charts->spray_r_pct !!}</div>
    </dd>
    <dd class="field">
        <div class="l"></div>
        <div class="l-c"></div>
        <div class="c"></div>
        <div class="c-r"></div>
        <div class="r"></div>
    </dd>
</dl>
{{-- Dynamic styling --}}
<style {!! $nonce !!}>
    .heatzones .z.o.t.l { background-color: {!! $charts->heatzone_otl_colour !!}; }
    .heatzones .z.o.t.r { background-color: {!! $charts->heatzone_otr_colour !!}; }
    .heatzones .z.o.b.l { background-color: {!! $charts->heatzone_obl_colour !!}; }
    .heatzones .z.o.b.r { background-color: {!! $charts->heatzone_obr_colour !!}; }
    .heatzones .z.i.t.l { background-color: {!! $charts->heatzone_itl_colour !!}; }
    .heatzones .z.i.t.c { background-color: {!! $charts->heatzone_itc_colour !!}; }
    .heatzones .z.i.t.r { background-color: {!! $charts->heatzone_itr_colour !!}; }
    .heatzones .z.i.m.l { background-color: {!! $charts->heatzone_iml_colour !!}; }
    .heatzones .z.i.m.c { background-color: {!! $charts->heatzone_imc_colour !!}; }
    .heatzones .z.i.m.r { background-color: {!! $charts->heatzone_imr_colour !!}; }
    .heatzones .z.i.b.l { background-color: {!! $charts->heatzone_ibl_colour !!}; }
    .heatzones .z.i.b.c { background-color: {!! $charts->heatzone_ibc_colour !!}; }
    .heatzones .z.i.b.r { background-color: {!! $charts->heatzone_ibr_colour !!}; }

    .spray-chart .field .l { background-color: {!! $charts->spray_l_colour !!}; }
    .spray-chart .field .l-c { background-color: {!! $charts->spray_lc_colour !!}; }
    .spray-chart .field .c { background-color: {!! $charts->spray_c_colour !!}; }
    .spray-chart .field .c-r { background-color: {!! $charts->spray_cr_colour !!}; }
    .spray-chart .field .r { background-color: {!! $charts->spray_r_colour !!}; }
</style>
