@php
    $dom_id = 'repertoire';
    $chart = $data->repertoireChart($dom_id);
    // Velocity difference?
    $fastball_diff = '';
    if (isset($data->fastball_diff)) {
        if ($data->fastball_diff > 0) {
            $icon = 'pos_up';
            $text = '+' . $data->fastball_diff . 'mph';
        } elseif ($data->fastball_diff < 0) {
            $icon = 'pos_down';
            $text = $data->fastball_diff . 'mph';
        } else {
            $icon = 'pos_nc';
            $text = 'No change';
        }
        $fastball_diff = ' &ndash; <span class="icon icon_' . $icon . '">' . $text . ' since ' . ($data->season - 1) . '</span>';
    }
@endphp
<dl class="{!! $sport !!}_box repertoire clearfix">
    <dt class="row-head">Pitch Repertoire</dt>
    <dt class="fastball">Fastball Velocity:</dt>
        <dd class="fastball">{!! $data->fastball_avg !!}mph{!! $fastball_diff !!}</dt>
        <dd class="chart" id="{!! $dom_id !!}"></dd>
</dl>
<script {!! HTTP::buildCSPNonceTag() !!}>
    var chart = new Highcharts.Chart({!! Highcharts::decode($chart) !!});
</script>
