{{-- Stat switcher --}}
@php
    $opt = [];
    foreach (array_keys(FrameworkConfig::get('debear.setup.stats.details.player')) as $alt_group) {
        $opt[] = "data-$alt_group=\"" . ($player->batterVsPitcher->sum("can_$alt_group") ? 'true' : 'false') . '"';
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="groups" {!! join(' ', $opt) !!}></div>
{{-- Table --}}
@php
    $team_ids = array_filter($player->batterVsPitcher->unique('team_id'));
    sort($team_ids);
@endphp
@foreach ($team_ids as $team_id)
    @php
        $data = $player->batterVsPitcher->where('team_id', $team_id);
    @endphp
    @include('sports.mlb.players.view.widgets.splits', [
        'type' => 'battervspitcher',
        'data' => $data,
        'title' => [
            'css' => $data->team->rowCSS(),
            'label' => '<span class="icon ' . $data->team->logoCSS() . '">' . $data->team->name . '</span>',
        ],
    ])
@endforeach
