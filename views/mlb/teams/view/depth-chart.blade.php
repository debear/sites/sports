@if ($team->depth_chart->count())
    <div class="depth_chart">
        {{-- Diamond - adapted from http://cssdeck.com/labs/baseball-field --}}
        <ul class="inline_list diamond">
            <li class="field mowed-grass"></li>
            <li class="infield"></li>
            <li class="infield-grass mowed-grass"></li>
            <li class="batting-circle"></li>
            <li class="first base"></li>
            <li class="second base"></li>
            <li class="third base"></li>
            <li class="home-plate"></li>
            <li class="base-lines"></li>
            <li class="pitchers-mound"></li>
            <li class="pitchers-rubber"></li>
            <li class="first circle"></li>
            <li class="second circle"></li>
            <li class="third circle"></li>
            <li class="right batters-box"></li>
            <li class="left batters-box"></li>
        </ul>
        {{-- Players --}}
        <div class="blocks">
            @includeWhen($team->depth_chart->where('pos', 'C')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Catcher', 'code' => 'C'])
            @includeWhen($team->depth_chart->where('pos', '1B')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'First Base', 'code' => '1B'])
            @includeWhen($team->depth_chart->where('pos', '2B')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Second Base', 'code' => '2B'])
            @includeWhen($team->depth_chart->where('pos', 'SS')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Shortstop', 'code' => 'SS'])
            @includeWhen($team->depth_chart->where('pos', '3B')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Third Base', 'code' => '3B'])
            @includeWhen($team->depth_chart->where('pos', 'LF')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Left Field', 'code' => 'LF'])
            @includeWhen($team->depth_chart->where('pos', 'CF')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Center Field', 'code' => 'CF'])
            @includeWhen($team->depth_chart->where('pos', 'RF')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Right Field', 'code' => 'RF'])
            @includeWhen($team->depth_chart->where('pos', 'DH')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Designated Hitter', 'code' => 'DH'])
            @includeWhen($team->depth_chart->where('pos', 'SP')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Rotation', 'code' => 'SP'])
            @includeWhen($team->depth_chart->where('pos', 'RP')->count(), 'sports.mlb.teams.view.depth-chart.block', ['title' => 'Bullpen', 'code' => 'RP'])
        </div>
    </div>

@else
    {{-- Preseason --}}
    <fieldset class="info icon_info preseason">
        Depth chart will be available shortly before the start of the season.
    </fieldset>
@endif
