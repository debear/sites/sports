<div class="block grid-tp-1-2 grid-tl-1-3">
    <dl class="{!! $sport !!}_box pos-{!! $code !!} clearfix">
        <dt class="row-head">{!! $title !!}</dt>
        @foreach ($team->depth_chart->where('pos', $code) as $depth)
            {{-- Mugshot of "primary" player --}}
            @if ($loop->first)
                <dd class="mugshot-small">{!! $depth->player->image('small') !!}</dd>
            @endif
            {{-- List --}}
            <dd class="row-{!! $loop->iteration % 2 !!} player">
                {!! $loop->iteration !!}. <a href="{!! $depth->player->link !!}"><span class="hidden-t hidden-d">{!! $depth->player->name !!}</span><span class="hidden-m">{!! $depth->player->name_short !!}</span></a>
            </dd>
        @endforeach
    </dl>
</div>
