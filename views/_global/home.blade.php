@extends('skeleton.layouts.html')

@section('content')

<h1>Welcome to {!! FrameworkConfig::get('debear.names.site') !!}!</h1>

<ul class="inline_list home clearfix">
    <li class="news-cell">
        @include('sports._global.home.news')
    </li>
    <li class="events-cell">
        @include('sports._global.home.events')
    </li>
</ul>

@endsection
