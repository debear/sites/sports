@php
    Resources::addCSS('_global/sitemap.css');
    Resources::addCSS('_global/logos_zoom.css');
    Resources::addCSS('_common/box_colours.css');
    // Pre-process the sitemap into our split sections
    $sitemap = Sitemap::parseSubsites($sitemap);
@endphp
@extends('skeleton.layouts.html')

@section('content')

<h1>Find your way round {!! FrameworkConfig::get('debear.names.site') !!}!</h1>

{{-- Individual subsites --}}
<ul class="inline_list grid">
    @foreach ($sitemap['subsites'] as $subsite)
        @php
            $sport = ltrim($subsite['url'], '/');
        @endphp
        <li class="subsite grid-cell grid-1-2 grid-tp-1-1 grid-m-1-1">
            <fieldset class="hover {!! FrameworkConfig::get("debear.subsites.$sport.setup.box") !!}">
                <a class="no_underline" href="{!! $subsite['url'] !!}">
                    <span class="logo logo_sports_{!! $sport !!}_zoom"></span>
                    <span class="label">{!! $subsite['label'] !!}</span>
                </a>
            </fieldset>
        </li>
    @endforeach
</ul>

{{-- Other links (inside the standard page toggle system) --}}
<div id="sitemap">
    <h2>Other Links</h2>
    <dl class="inline_list clearfix other">
        @foreach ($sitemap['admin'] as $admin)
            <dt><a href="{!! $admin['url'] !!}">{!! $admin['label'] !!}</a></dt>
                @if ($admin['title'])
                    <dd>{!! $admin['title'] !!}</dd>
                @endif
        @endforeach
    </dl>
</div>

@endsection
