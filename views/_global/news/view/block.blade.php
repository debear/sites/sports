{{-- All handled through our scroller widget --}}
@php
    $scroller = [
        'list' => $block,
        'default' => 1,
        'interval' => 5000,
        'item-view' => 'sports._global.news.view.article',
        'item-variable' => 'article',
        'prev-next' => 'mini',
        'resp-widths' => [0 => 1, 769 => 2],
    ];
@endphp
@include('skeleton.widgets.scroller')

<style {!! HTTP::buildCSPNonceTag() !!}>
    @foreach ($block as $article)
        .article-{!! $article->uniqueReference() !!} {
            background-image: url('{!! $article->image() !!}');
        }
    @endforeach
</style>
