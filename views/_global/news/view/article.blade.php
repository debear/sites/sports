@php
    $a_attrib = 'href="' . $article->link . '" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window"';
@endphp
<article class="grid-cell {!! $article_css ?? '' !!}">
    @if (isset($inc_sports_logo) && $inc_sports_logo)
        <sport class="sport icon_{!! $article->app !!}_icon"></sport>
    @endif
    <h4><a {!! $a_attrib !!}>{!! $article->headline() !!}</a></h4>
    <thumbnail class="article-{!! $article->uniqueReference() !!}"><a {!! $a_attrib !!}></a></thumbnail>
    <summary>{!! $article->summary() !!}</summary>
    <publisher>{!! $article->publisher() !!}</publisher>
    <time>{!! $article->timeRelative() !!}</time>
    <a class="more" {!! $a_attrib !!}>Read Article &raquo;</a>
</article>
