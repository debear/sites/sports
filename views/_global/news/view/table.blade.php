@php
    $article_css = 'grid-cell box section';
@endphp
<ul class="inline_list articles grid">
    @foreach($articles as $article)
        <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            @include('sports._global.news.view.article')
        </li>
    @endforeach
</ul>

{{-- Thumbnail images --}}
<style {!! HTTP::buildCSPNonceTag() !!}>
    @foreach ($articles as $article)
        .article-{!! $article->uniqueReference() !!} {
            background-image: url('{!! $article->image() !!}');
        }
    @endforeach
</style>

{{-- Pagination list --}}
<pagination class="clearfix">
    {!! Pagination::render($num_articles, $per_page, '', $page) !!}
</pagination>
