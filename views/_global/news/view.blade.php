@extends('skeleton.layouts.html')

@section('content')

<h1>Latest {!! FrameworkConfig::get('debear.names.section') !!} News</h1>

@include('sports._global.news.view.block')
<articles>
    @include('sports._global.news.view.table', ['articles' => $list])
</articles>

@endsection
