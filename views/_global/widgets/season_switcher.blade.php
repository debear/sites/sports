@php
    // Determine
    $season_min = FrameworkConfig::get('debear.setup.switcher.min') ?? FrameworkConfig::get('debear.setup.season.min');
    $season_max = FrameworkConfig::get('debear.setup.switcher.max') ?? FrameworkConfig::get('debear.setup.season.max');
    $season_fmt = FrameworkConfig::get('debear.setup.switcher.format') ?? true;
    // Render
    Resources::addCSS('_global/widgets/season_switcher.css');
    Resources::addJS('_global/widgets/season_switcher.js');
    $opt = [];
    foreach (range($season_max, $season_min, -1) as $s) {
        $opt[] = [
            'id' => $s,
            'label' => ($season_fmt ? (\DeBear\Helpers\Sports\Controller::seasonTitle($s) ?? $s) : $s),
        ];
    }
    $dropdown = new Dropdown('switcher-season', $opt, [
        'value' => FrameworkConfig::get('debear.setup.season.viewing'),
        'select' => false,
    ]);
@endphp
@extends('skeleton.layouts.html')

@section('content')

<div class="season_switcher">
    {!! $dropdown->render() !!}
</div>
<div class="season_switcher_content">
    @yield('switcher_content')
</div>

@endsection
