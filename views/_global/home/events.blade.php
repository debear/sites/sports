<dl class="events">
    <dt class="row-head">Yesterday</dt>
    @foreach ($events['yesterday'] as $event)
        @include("sports._global.home.events.{$event->template}")
    @endforeach
    <dt class="row-head">Today</dt>
    @foreach ($events['today'] as $event)
        @include("sports._global.home.events.{$event->template}")
    @endforeach
</dl>
