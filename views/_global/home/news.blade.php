{{-- Articles using the individual article widget from the news page --}}
@php
    $article_css = 'grid-cell box section';
    $inc_sports_logo = true;
@endphp
<articles>
    <ul class="inline_list articles grid">
        @foreach($articles as $article)
            <li class="grid-cell grid-1-2 grid-t-1-1 grid-m-1-1">
                @include('sports._global.news.view.article')
            </li>
        @endforeach
    </ul>
</articles>
{{-- Thumbnail images --}}
<style {!! HTTP::buildCSPNonceTag() !!}>
    @foreach ($articles as $article)
        .article-{!! $article->uniqueReference() !!} {
            background-image: url('{!! $article->image() !!}');
        }
    @endforeach
</style>
