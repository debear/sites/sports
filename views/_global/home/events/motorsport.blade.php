{{-- Requires some config manipulation --}}
@php
    $orig = [
        'debear.datetime' => FrameworkConfig::get('debear.datetime'),
        'debear.section.endpoint' => FrameworkConfig::get('debear.section.endpoint'),
        'debear.section.code' => FrameworkConfig::get('debear.section.code'),
        'debear.links.races' => FrameworkConfig::get('debear.links.races'),
    ];
    FrameworkConfig::set([
        'debear.datetime' => FrameworkConfig::get("debear.subsites.{$event->template_icon}.datetime"),
        'debear.section.endpoint' => $event->template_icon,
        'debear.section.code' => $event->template_icon,
        'debear.links.races' => FrameworkConfig::get("debear.subsites.{$event->template_icon}.links.races"),
    ]);
@endphp
<dt class="row-head-track">{!! FrameworkConfig::get("debear.subsites.{$event->template_icon}.names.section") !!}</dt>
    @foreach ($event as $race)
        <dd class="motorsport clearfix">
            <ul class="inline_list race">
                <li class="row-0-track race">{!! $race->nameHomepage() !!}</li>
                @if ($race->template_type == 'qual' && $race->isComplete())
                    {{-- Qualifying result --}}
                    <li class="row-head-track res">Qualifying Result</li>
                    @foreach ($race->getGridFront() as $grid)
                        <li class="row-head-track pos">{!! $grid->grid_pos !!}</li>
                        <li class="podium-{!! $grid->grid_pos !!} participant {!! !isset($grid->raceParticipant->team) ? 'no-team' : '' !!}">
                            <span class="flag_right flag16_right_{!! $grid->raceParticipant->participant->nationality !!}"><a href="{!! $grid->raceParticipant->participant->link() !!}">{!! $grid->raceParticipant->participant->name !!}</a></span>
                        </li>
                        @isset($grid->raceParticipant->team)
                            <li class="podium-{!! $grid->grid_pos !!} team" title="{!! $grid->raceParticipant->team->name !!}">{!! $grid->raceParticipant->team->name !!}</li>
                        @endisset
                    @endforeach
                    <li class="row-1-track link"><a href="{!! $race->link() !!}#quali">View Qualifying Result</a></li>
                @elseif ($race->isComplete())
                    {{-- Race result --}}
                    <li class="row-head-track res">Result</li>
                    @foreach ($race->getPodium($race->template_type == 'race' ? 1 : 2) as $result)
                        <li class="row-head-track pos">{!! $result->pos !!}</li>
                        <li class="podium-{!! $result->pos !!} participant {!! !isset($result->raceParticipant->team) ? 'no-team' : '' !!}">
                            <span class="flag_right flag16_right_{!! $result->raceParticipant->participant->nationality !!}"><a href="{!! $result->raceParticipant->participant->link() !!}">{!! $result->raceParticipant->participant->name !!}</a></span>
                        </li>
                        @isset($result->raceParticipant->team)
                            <li class="podium-{!! $result->pos !!} team" title="{!! $result->raceParticipant->team->name !!}">{!! $result->raceParticipant->team->name !!}</li>
                        @endisset
                    @endforeach
                    <li class="row-1-track link"><a href="{!! $race->link() !!}{!! $race->template_type == 'race2' ? '#result2' : '' !!}">View Full Result</a></li>
                @elseif ($race->isCancelled($race->template_type == 'race' ? 1 : 2))
                    <li class="row-1-track status">{!! rtrim(FrameworkConfig::get('debear.links.races.label'), 's') !!} Cancelled</li>
                @endif
            </ul>
        </dd>
    @endforeach
{{-- Revert the config manipulation --}}
@php
    FrameworkConfig::set($orig);
@endphp
