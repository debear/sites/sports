{{-- Requires some config manipulation --}}
@php
    $orig = [
        'debear.datetime' => FrameworkConfig::get('debear.datetime'),
        'debear.links.schedule' => FrameworkConfig::get('debear.links.schedule'),
        'debear.section.endpoint' => FrameworkConfig::get('debear.section.endpoint'),
        'debear.section.code' => FrameworkConfig::get('debear.section.code'),
        'debear.setup.playoffs' => FrameworkConfig::get('debear.setup.playoffs'),
    ];
    FrameworkConfig::set([
        'debear.datetime' => FrameworkConfig::get("debear.subsites.{$event->template_icon}.datetime"),
        'debear.links.schedule' => FrameworkConfig::get("debear.subsites.{$event->template_icon}.links.schedule"),
        'debear.section.endpoint' => $event->template_icon,
        'debear.section.code' => $event->template_icon,
        'debear.setup.playoffs' => FrameworkConfig::get("debear.subsites.{$event->template_icon}.setup.playoffs"),
    ]);
    // Also custom CSS for team logos.
    Resources::addCSS("{$event->template_icon}/teams/tiny.css");
    $css_type = FrameworkConfig::get("debear.subsites.{$event->template_icon}.setup.box");
@endphp
<dt class="row-head-{!! $css_type !!}">{!! FrameworkConfig::get("debear.subsites.{$event->template_icon}.names.section") !!}</dt>
    @foreach ($event as $game)
        @php
            $series_summary = ($game->isPlayoff() ? $game->series_summary : '');
        @endphp
        <dd class="major_league row-{!! $loop->iteration % 2 !!}-{!! $css_type !!} {!! $series_summary ? 'dbl-height' : '' !!}">
            <div class="visitor">
                <span class="{!! $game->visitor->logoCSS() !!} icon">
                    <span class="hidden-m hidden-tp">{!! $game->visitor_id !!}</span>
                    <span class="hidden-d hidden-tl"><span class="hidden-m">{!! $game->visitor->city !!} </span>{!! $game->visitor->franchise !!}</span>
                </span>
                @if ($game->isComplete())
                    <span class="score {!! $game->visitor_score > $game->home_score ? 'winner' : ($game->visitor_score < $game->home_score ? 'loser' : 'tied') !!}">{!! $game->visitor_score !!}</span>
                @endif
            </div>
            <div class="home">
                <span class="{!! $game->home->logoCSS() !!} icon">
                    <span class="hidden-m hidden-tp">{!! $game->home_id !!}</span>
                    <span class="hidden-d hidden-tl"><span class="hidden-m">{!! $game->home->city !!} </span>{!! $game->home->franchise !!}</span>
                </span>
                @if ($game->isComplete())
                    <span class="score {!! $game->visitor_score < $game->home_score ? 'winner' : ($game->visitor_score > $game->home_score ? 'loser' : 'tied') !!}">{!! $game->home_score !!}</span>
                @endif
            </div>
            <div class="status">
                @if ($game->isComplete())
                    {!! $game->status !!}
                    <a class="icon_game_boxscore" href="{!! $game->link !!}"></a>
                @elseif ($game->isAbandoned())
                    <span class="abandoned">{!! $game->status !!}</span>
                @else
                    {!! $game->start_time !!}
                @endif
            </div>
            @if ($series_summary)
                <div class="series">{!! $series_summary !!}</div>
            @endif
        </dd>
    @endforeach
{{-- Revert the config manipulation --}}
@php
    FrameworkConfig::set($orig);
@endphp
