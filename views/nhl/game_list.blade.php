@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! $title !!}</h1>

{{-- Dates/Calendar --}}
@include('sports.major_league.schedule.daily.calendar')

{{-- Lineups --}}
@php
    $active_games = $schedule->whereNotIn('status', ['PPD', 'CNC']);
    $not_played = $schedule->whereIn('status', ['PPD', 'CNC']);
@endphp
<div class="game_week_content">
    <ul class="grid inline_list">
        @foreach($active_games as $game)
            <li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <ul class="inline_list nhl_box game_list {!! $list_view !!} clearfix">
                    <li class="info head">{!! $game->start_time !!}, {!! $game->venue !!}</li>
                    @if ($game->isPlayoff())
                        <li class="info">{!! $game->title !!}</li>
                    @endif
                    <li class="team visitor {!! $game->visitor->rowCSS() !!}">
                        <span class="icon {!! $game->visitor->logoCSS() !!}">{!! $game->visitor->franchise !!}</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home {!! $game->home->rowCSS() !!}">
                        <span class="icon {!! $game->home->logoCSS() !!}">{!! $game->home->franchise !!}</span>
                    </li>
                    <li class="list visitor">@include("sports.nhl.$list_view.list", ['players' => $data->where('game_team_ref', "{$game->game_type}-{$game->game_id}-{$game->visitor_id}")])</li>
                    <li class="list home">@include("sports.nhl.$list_view.list", ['players' => $data->where('game_team_ref', "{$game->game_type}-{$game->game_id}-{$game->home_id}")])</li>
                </ul>
            </li>
        @endforeach
    </ul>

    {{-- PPD / CNC --}}
    @if ($not_played->isset())
        <ul class="grid inline_list">
            @foreach($not_played as $game)
                <li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                    <ul class="inline_list nhl_box game_list not_played {!! $list_view !!} clearfix">
                        <li class="info head">{!! $game->status_full !!}</li>
                        @if ($game->isPlayoff())
                            <li class="info">{!! $game->title !!}</li>
                        @endif
                        <li class="team visitor {!! $game->visitor->rowCSS() !!}">
                            <span class="icon {!! $game->visitor->logoCSS() !!}">{!! $game->visitor->franchise !!}</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home {!! $game->home->rowCSS() !!}">
                            <span class="icon {!! $game->home->logoCSS() !!}">{!! $game->home->franchise !!}</span>
                        </li>
                    </ul>
                </li>
            @endforeach
        </ul>
    @endif
</div>

@endsection
