@php
    $type_map = [
        'GOAL' => 'far fa-lightbulb',
        'SHOOTOUT' => 'fa fa-map-marker-alt',
        'SHOT' => 'fa fa-map-marker',
        'PENALTY' => 'far fa-dizzy',
        'HIT' => 'fa fa-gavel',
    ];
    $event_id = 0;
    $event_icons = [];
@endphp

{{-- Events --}}
<ul class="inline_list events">
    {{-- Build-A-Rink --}}
    <li class="rink">
        <div class="left goal"></div>
        <div class="left goalline"></div>
        <div class="left crease"></div>
        <div class="left faceoff top"></div>
        <div class="left faceoff bot"></div>
        <div class="left blueline"></div>
        <div class="left dot top"></div>
        <div class="left dot bot"></div>
        <div class="centre faceoff">
            <div class="{!! $game->home->logoCSS('medium') !!}"></div>
        </div>
        <div class="redline"></div>
        <div class="right dot top"></div>
        <div class="right dot bot"></div>
        <div class="right blueline"></div>
        <div class="right faceoff top"></div>
        <div class="right faceoff bot"></div>
        <div class="right crease"></div>
        <div class="right goalline"></div>
        <div class="right goal"></div>
    </li>
    {{-- The list --}}
    @foreach ($game->events as $by_type)
        @foreach (json_decode($by_type->events ?? '[]') as $event)
            @php
                $event_icons[++$event_id] = [
                    'left' => sprintf('%0.05f%%', ($event[1] + 100) / 2),
                    'top' => sprintf('%0.05f%%', ($event[2] + 42) * (100 / 84)),
                ];
            @endphp
            <li class="event {!! $type_map[$by_type->event_type] !!} text_{!! $sport !!}-{!! $event[3] !!}" data-id="{!! $event_id !!}" data-period="{!! $event[0] !!}" data-type="{!! $by_type->event_type !!}" data-team_id="{!! $event[3] !!}"></li>
        @endforeach
    @endforeach
</ul>
<style {!! $nonce !!}>
    @foreach ($event_icons as $key => $icon)
        .events .event[data-id="{!! $key !!}"] { left: calc({!! $icon['left'] !!} - 9px); top: calc({!! $icon['top'] !!} - 9px); }
    @endforeach
</style>

{{-- Filters --}}
<dl class="events_filter {!! $sport !!}_box clearfix">
    <dt class="row-head sect">Filter Events</dt>
    <dt class="row-0 type">By Period:</dt>
        <dd class="row-1">
            @for ($period = 1; $period <= $game->num_periods; $period++)
                <div>
                    <input type="checkbox" id="filter_period_{!! $period !!}" class="event_filter checkbox" checked="checked" data-evtype="period" data-evvalue="{!! $period !!}">
                    <label for="filter_period_{!! $period !!}">{!! $game->periodNameMid($period) !!}</label>
                </div>
            @endfor
        </dd>
    <dt class="row-0 type">By Team:</dt>
        <dd class="row-1">
            <div>
                <input type="checkbox" id="filter_team_visitor" class="event_filter checkbox" checked="checked" data-evtype="team_id" data-evvalue="{!! $game->visitor_id !!}">
                <label for="filter_team_visitor"><span class="{!! $game->visitor->logoCSS() !!}">{!! $game->visitor_id !!}</span></label>
            </div>
            <div>
                <input type="checkbox" id="filter_team_home" class="event_filter checkbox" checked="checked" data-evtype="team_id" data-evvalue="{!! $game->home_id !!}">
                <label for="filter_team_home"><span class="{!! $game->home->logoCSS() !!}">{!! $game->home_id !!}</span></label>
            </div>
        </dd>
    <dt class="row-0 type">By Type:</dt>
        <dd class="row-1">
            <div>
                <input type="checkbox" id="filter_type_goal" class="event_filter checkbox" checked="checked" data-evtype="type" data-evvalue="GOAL">
                <label for="filter_type_goal"><span class="{!! $type_map['GOAL'] !!}"></span>Goal</label>
            </div>
            @if ($game->hasShootout())
                <div>
                    <input type="checkbox" id="filter_type_shootout" class="event_filter checkbox" checked="checked" data-evtype="type" data-evvalue="SHOOTOUT">
                    <label for="filter_type_shootout"><span class="{!! $type_map['SHOOTOUT'] !!}"></span>Shootout</label>
                </div>
            @endif
            <div>
                <input type="checkbox" id="filter_type_shot" class="event_filter checkbox" checked="checked" data-evtype="type" data-evvalue="SHOT">
                <label for="filter_type_shot"><span class="{!! $type_map['SHOT'] !!}"></span>Shot</label>
            </div>
            <div>
                <input type="checkbox" id="filter_type_penalty" class="event_filter checkbox" checked="checked" data-evtype="type" data-evvalue="PENALTY">
                <label for="filter_type_penalty"><span class="{!! $type_map['PENALTY'] !!}"></span>Penalty</label>
            </div>
            <div>
                <input type="checkbox" id="filter_type_hit" class="event_filter checkbox" checked="checked" data-evtype="type" data-evvalue="HIT">
                <label for="filter_type_hit"><span class="{!! $type_map['HIT'] !!}"></span>Hit</label>
            </div>
        </dd>
</dl>
