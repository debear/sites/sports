{{-- Goals --}}
@php
    $goals = $game->plays->whereIn('event_type', ['GOAL', 'SHOOTOUT']);
    $is_regular = $game->isRegular();
@endphp
<dl class="{!! $sport !!}_box playlist scoring goals clearfix">
    <dt class="row-head type">Goals</dt>
    @for($period = 1; $period <= $game->num_periods; $period++)
        @php
            if ($period == 1 || ($is_regular && $period == 5)) {
                // Reset for the Shootout
                $scores = ['visitor' => 0, 'home' => 0];
            }
        @endphp
        <dt class="row-head period">{!! $game->periodNameFull($period) !!}</dt>
        @forelse ($goals->where('period', $period) as $goal)
            @php
                $row_css = 'row-' . ($loop->index % 2);
                $is_home = ($goal->team_id == $game->home_id);
                $team = ($is_home ? $game->home : $game->visitor);
                $is_goal = (strpos($goal->play, 'scored') !== false);
                if (!$game->isRegular() || $period != 5 || $is_goal) {
                    $scores[$is_home ? 'home' : 'visitor']++;
                }
            @endphp
            <dd class="{!! $row_css !!} {!! $team->logoCSS() !!} time">
                @if ($goal->event_type == 'GOAL')
                    {!! $goal->time !!}
                @else
                    <span class="icon_pbp_hockey_shootout_{!! $is_goal ? 'goal' : 'miss' !!}"></span>
                @endif
            </dd>
            <dd class="{!! $row_css !!} play">
                <div class="score"><span class="visitor {!! $game->visitor->logoCSS() !!}">{!! $scores['visitor'] !!}</span> &ndash; <span class="home {!! $game->home->logoRightCSS() !!}">{!! $scores['home'] !!}</span></div>
                {!! $goal->play !!}
            </dd>
        @empty
            <dd class="row-0 none"><em>No Scoring</em></dd>
        @endforelse
    @endfor
</dl>

{{-- Penalties --}}
@php
    $penalties = $game->plays->where('event_type', 'PENALTY');
@endphp
<dl class="{!! $sport !!} playlist scoring penalties clearfix">
    <dt class="row-head type">Penalties</dt>
    @for($period = 1; $period <= $game->num_periods; $period++)
        <dt class="row-head period">{!! $game->periodNameFull($period) !!}</dt>
        @forelse ($penalties->where('period', $period) as $penalty)
            @php
                $row_css = 'row-' . ($loop->index % 2);
                $team = ($penalty->team_id == $game->home_id ? $game->home : $game->visitor);
            @endphp
            <dd class="{!! $row_css !!} {!! $team->logoCSS() !!} time">{!! $penalty->time !!}</dd>
            <dd class="{!! $row_css !!} play">{!! $penalty->play !!}</dd>
        @empty
            <dd class="row-0 none"><em>No Penalties</em></dd>
        @endforelse
    @endfor
</dl>
