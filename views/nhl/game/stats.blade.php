@include('sports.major_league.game.stats', ['stats' => [
    // Shot Attempts
    'Shots on Goal' => [
        [
            'label' => 'On Goal',
            'visitor' => $game->visitor->stats->shots,
            'home' => $game->home->stats->shots,
        ],
        [
            'label' => 'Missed Net',
            'visitor' => $game->visitor->stats->missed_shots,
            'home' => $game->home->stats->missed_shots,
        ],
        [
            'label' => 'Blocked',
            'visitor' => $game->visitor->stats->blocked_shots,
            'home' => $game->home->stats->blocked_shots,
        ],
    ],

    // Special Teams
    'Special Teams' => [
        [
            'label' => 'Power Play',
            'visitor' => $game->visitor->stats->ppg,
            'home' => $game->home->stats->ppg,
            'visitor_label' => "{$game->visitor->stats->ppg} / {$game->visitor->stats->ppa}",
            'home_label' => "{$game->home->stats->ppg} / {$game->home->stats->ppa}",
        ],
        [
            'label' => 'SHG',
            'visitor' => $game->visitor->stats->shg,
            'home' => $game->home->stats->shg,
        ],
    ],

    // Penalties
    'Penalties' => [
        [
            'label' => 'Taken',
            'visitor' => $game->visitor->stats->pen_num,
            'home' => $game->home->stats->pen_num,
        ],
        [
            'label' => 'PIMs',
            'visitor' => $game->visitor->stats->pims,
            'home' => $game->home->stats->pims,
        ],
    ],

    // Miscellaneous
    'Miscellaneous' => [
        [
            'label' => 'Hits',
            'visitor' => $game->visitor->stats->hits,
            'home' => $game->home->stats->hits,
        ],
        [
            'label' => 'Giveaways',
            'visitor' => $game->visitor->stats->giveaways,
            'home' => $game->home->stats->giveaways,
        ],
        [
            'label' => 'Takeaways',
            'visitor' => $game->visitor->stats->takeaways,
            'home' => $game->home->stats->takeaways,
        ],
        [
            'label' => 'Faceoffs',
            'visitor' => $game->visitor->stats->faceoffs,
            'home' => $game->home->stats->faceoffs,
        ],
    ],
]])
