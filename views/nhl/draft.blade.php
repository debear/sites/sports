@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! $draft_title !!}</h1>

@include('skeleton.widgets.subnav')

@foreach ($draft->unique('round') as $r)
    @php
        $inc_overall = ($r != 1);
        $round = $draft->where('round', $r);
    @endphp
    <dl class="inline_list subnav-round-{!! $r !!} {!! $r > 1 ? 'hidden' : '' !!} round clearfix">
        <dt class="row-head pick">Pick</dt>
        <dt class="row-head team">Team</dt>
        <dt class="row-head name">Player</dt>
        <dt class="row-head height">Height</dt>
        <dt class="row-head weight">Weight</dt>
        <dt class="row-head junior">Junior</dt>
        <dt class="row-head note">Notes</dt>
        @foreach ($round as $pick)
            @php
                $row_css = 'row-' . ($loop->index % 2);
                $valid = isset($pick->pos);
                if ($valid) {
                    $name = "<span class=\"flag16_{$pick->country}\">{$pick->player}</span> ({$pick->pos})";
                } else {
                    $name = ($pick->player == 'Void' ? 'Selection Forfeited' : 'Invalid Claim');
                }
                $team_chain = $pick->team_chain;
                $team_chain_alt = strip_tags($team_chain);
            @endphp
            <dd class="row-head {!! $inc_overall ? 'pick-rnd' : 'pick' !!}">{!! $pick->round_pick !!}</dd>
            @if ($inc_overall)
                <dd class="{!! $row_css !!} pick-ov">{!! $pick->pick !!}</dd>
            @endif
            <dd class="{!! $row_css !!} team"><span class="{!! \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($pick->team_id) !!}">{!! $pick->team_id !!}</span></dd>
            <dd class="{!! $row_css !!} name">{!! $name !!}</dd>
            <dd class="{!! $row_css !!} height">{!! $valid ? $pick->height_ft_in : '&ndash;' !!}</dd>
            <dd class="{!! $row_css !!} weight">{!! $valid ? "{$pick->weight}lb" : '&ndash;' !!}</dd>
            <dd class="{!! $row_css !!} junior {!! !$team_chain ? 'no-note' : '' !!} {!! !$valid ? 'no-junior' : '' !!}">{!! $valid ? ucwords($pick->junior_team) . (isset($pick->junior_league) ? " ({$pick->junior_league})" : '') : '&ndash;' !!}</dd>
            <dd class="{!! $row_css !!} note {!! !$team_chain ? 'none' : '" title="' . $team_chain_alt !!}">{!! $team_chain !!}</dd>
        @endforeach
    </dl>
@endforeach

@endsection
