<ul class="inline_list">
    @if (!$players->isset())
        <li class="row-0 na">Not Yet Announced</li>
    @else
        @foreach ($players as $player)
            @php
                $row_css = 'row-' . ($loop->index % 2);
            @endphp
            <li class="{!! $row_css !!} head pos">{!! $player->pos !!}</li>
            <li class="{!! $row_css !!} name"><a href="{!! $player->player->link !!}">{!! $player->player->name !!}</a></li>
        @endforeach
    @endif
</ul>
