@php
    $num_periods = $game->num_periods;
    $periods_raw = range(1, $num_periods);
    if ($game->game_type == 'playoff' && $num_periods > 4) {
        // Condense multiple playoff OTs
        $periods_raw = array_merge(range(1, 3), [$num_periods]);
    }
    $periods = [];
    foreach ($periods_raw as $period) {
        $periods[] = [
            'num' => $period,
            'text' => $game->periodName($period),
        ];
    }
@endphp
<dl class="periods clearfix">
    {{-- Header --}}
    @foreach ($periods as $p)
        <dt class="period head period-{!! $p['text'] !!}">{!! $p['text'] !!}</dt>
    @endforeach
    <dt class="total head">Tot</dt>
    {{-- Visitor --}}
    <dd class="team {!! $game->visitor->logoCSS() !!}"></dd>
    @foreach ($periods as $p)
        <dd class="period period-{!! $p['text'] !!} row_0">
            @if ($p['text'] != 'SO')
                {{-- Regular Period --}}
                {!! $game->visitor->scoring->where('period', $p['num'])->score ?? 0 !!}
            @else
                {{-- Shootout --}}
                {!! intval($game->visitor_score > $game->home_score) !!}
                <span>({!! $game->visitor->shootout->score ?? 0 !!})</span>
            @endif
        </dd>
    @endforeach
    <dt class="total head">{!! $game->visitor_score !!}</dt>
    {{-- Home --}}
    <dd class="team {!! $game->home->logoCSS() !!}"></dd>
    @foreach ($periods as $p)
        <dd class="period period-{!! $p['text'] !!} row_0">
            @if ($p['text'] != 'SO')
                {{-- Regular Period --}}
                {!! $game->home->scoring->where('period', $p['num'])->score ?? 0 !!}
            @else
                {{-- Shootout --}}
                {!! intval($game->home_score > $game->visitor_score) !!}
                <span>({!! $game->home->shootout->score ?? 0 !!})</span>
            @endif
        </dd>
    @endforeach
    <dt class="total head">{!! $game->home_score !!}</dt>
</dl>
