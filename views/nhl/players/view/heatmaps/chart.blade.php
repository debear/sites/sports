@php
    $zones = $player->heatmap->zones_css;
    // Do we have an overriding nonce to apply from the parent tab?
    if (session()->has("ajax.players.{$player->player_id}.heatmaps")) {
        FrameworkConfig::set(['debear.headers.csp.nonce' => session("ajax.players.{$player->player_id}.heatmaps")]);
    }
    $nonce = HTTP::buildCSPNonceTag();
@endphp
{{-- Stat switcher --}}
@php
    $opt = [];
    foreach (FrameworkConfig::get("debear.setup.stats.details.player.{$player->group}.heatmaps") as $type) {
        $has = "has_$type";
        $opt[] = "data-$type=\"" . ($player->heatmap->$has ? 'true' : 'false') . "\"";
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="heatmaps" {!! join(' ', $opt) !!}></div>
<div class="heatmaps-charts">
    {{-- Heatmap Rink --}}
    <ul class="inline_list heatmaps clearfix">
        @for ($i = 0; $i < count($zones); $i++)
            <li class="zone zcol-{!! ($i % 8) + 1 !!} zrow-{!! floor($i / 8) + 1 !!}"></li>
        @endfor
        <li class="rink"></li>
    </ul>
    {{-- Heatmap --}}
    <ul class="inline_list heatmaps clearfix">
        @foreach ($zones as $i => $zone)
            <li class="zcell event-{!! $i !!} zcol-{!! ($i % 8) + 1 !!}"></li>
        @endforeach
    </ul>
</div>
<style {!! $nonce !!}>
    @foreach ($zones as $i => $zone)
        @if ($zone != '#ffffff')
            .heatmaps-charts .zcell.event-{!! $i !!} { background-color: {!! $zone !!}; }
        @endif
    @endforeach
</style>
