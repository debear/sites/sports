@includeWhen(FrameworkConfig::get('debear.setup.season.viewing') >= FrameworkConfig::get('debear.setup.players.heatmap_start'), 'sports.nhl.players.view.heatmaps.chart')
@includeWhen(FrameworkConfig::get('debear.setup.season.viewing') < FrameworkConfig::get('debear.setup.players.heatmap_start'), 'sports.nhl.players.view.heatmaps.not_available')
