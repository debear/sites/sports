@php
    $grid_css = 'grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1';
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

@if (\DeBear\Helpers\Sports\Common\ManageViews::getMode() != 'body')
    <h1>Scores &amp; Schedule</h1>

    @include('sports.major_league.schedule.weekly.game_weeks')
@endif

<div class="game_week_content">
    {{-- Teams on a Bye Week --}}
    @includeWhen($bye_weeks->isset(), 'sports.major_league.schedule.weekly.byes')

    {{-- Favourite? --}}
    @php
        $favourites = $schedule->where('is_favourite', 1);
        $others = $schedule->where('is_favourite', 0);
    @endphp
    @if ($favourites->count())
        <h3>My Teams</h3>
        @include('sports.major_league.schedule.components.games', [
            'list' => $favourites,
            'grid' => $grid_css,
        ])
    @endif

    {{-- Games --}}
    @php
        $dates = $others->unique('game_date');
    @endphp
    @foreach ($dates as $date)
        <h3>{!! (new \Carbon\Carbon($date))->format('l jS F') !!}</h3>
        @include('sports.major_league.schedule.components.games', [
            'list' => $others->where('game_date', $date),
            'grid' => $grid_css,
        ])
    @endforeach
</div>

@endsection
