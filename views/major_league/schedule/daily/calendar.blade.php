@php
    Resources::addCSS('major_league/widgets/calendar.css');
    Resources::addJS('major_league/widgets/calendar.js');

    $fmt_d = 'l jS F';
    $fmt_m = 'D jS M';
@endphp
<div class="game_dates">
    {{-- Previous Day --}}
    @isset($game_date->date_prev)
        <span class="prev">
            &laquo; <a class="date" href="{!! $game_date->$link_method('prev') !!}" data-date="{!! $game_date->linkDate('prev') !!}"><span class="hidden-m">{!! $game_date->date_prev->format($fmt_d) !!}</span><span class="hidden-t hidden-d">{!! $game_date->date_prev->format($fmt_m) !!}</span></a>
        </span>
    @endisset
    {{-- Current Day --}}
    <span class="curr" data-link="{!! $game_date->$link_method() !!}" data-date="{!! $game_date->linkDate() !!}">
        <span class="date hidden-m">{!! $game_date->date->format($fmt_d) !!}</span>
        <span class="date hidden-t hidden-d">{!! $game_date->date->format($fmt_m) !!}</span>
        @include('skeleton.widgets.calendar')
    </span>
    {{-- Next Day --}}
    @isset($game_date->date_next)
        <span class="next">
            <a class="date" href="{!! $game_date->$link_method('next') !!}" data-date="{!! $game_date->linkDate('next') !!}"><span class="hidden-m">{!! $game_date->date_next->format($fmt_d) !!}</span><span class="hidden-t hidden-d">{!! $game_date->date_next->format($fmt_m) !!}</span></a> &raquo;
        </span>
    @endisset
</div>
