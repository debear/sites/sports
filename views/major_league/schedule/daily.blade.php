@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>Scores &amp; Schedule</h1>

{{-- Dates/Calendar --}}
@include('sports.major_league.schedule.daily.calendar')

{{-- Games --}}
@php
    $grid = 'grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1';
    // Split into groups
    $favourites = $schedule->where('is_favourite', 1);
    $others = $schedule->whereNot('is_favourite', 1);
    $preview = $others->whereNull('status');
    $complete = $others->whereNotNull('status');
    $result = $complete->whereNotIn('status', ['PPD', 'CNC', 'SSP']);
    $abandoned = $complete->whereIn('status', ['PPD', 'CNC', 'SSP']);
    // Section titles?
    $title_my = $title_other = '';
    if ($favourites->count() && $others->count()) {
        $title_my = 'My Teams';
        $title_other = 'Other Teams';
    }
@endphp
<h3>{!! $title_my !!}</h3>
@includeWhen($favourites->isset(), 'sports.major_league.schedule.components.games', ['list' => $favourites])
<h3>{!! $title_other !!}</h3>
@includeWhen($preview->isset(), 'sports.major_league.schedule.components.games', ['list' => $preview])
@includeWhen($result->isset(), 'sports.major_league.schedule.components.games', ['list' => $result])
@includeWhen($abandoned->isset(), 'sports.major_league.schedule.components.games', ['list' => $abandoned])

@endsection
