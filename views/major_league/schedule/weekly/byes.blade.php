<dl class="bye_weeks">
    <dt>Bye Weeks:</dt>
    @foreach ($bye_weeks as $bye)
        <dd class="{!! $bye->team->logoCSS() !!}">
            <span class="hidden-t hidden-d">{!! $bye->team->team_id !!}</span>
            <span class="hidden-m">{!! $bye->team->franchise !!}</span>
        </dd>
    @endforeach
</dl>
