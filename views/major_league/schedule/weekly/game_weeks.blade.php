@php
    $link_method = ($link_method ?? 'linkFull');
    $curr_week = $game_date->linkDate();
    // Which playoff weeks apply?
    $playoffs = [];
    $po_week = 0;
    $po_weeks = $game_date_list->where('game_type', '=', 'playoff');
    foreach (FrameworkConfig::get('debear.setup.playoffs.rounds') as $info) {
        $week = $po_weeks->where('week', ++$po_week);
        if (!$week->isset()) {
            // Not reached this stage in the playoffs yet, so no point in carrying on
            break;
        }
        $playoffs[] = [
            'name' => $info['short'],
            'date' => $week->linkDate(),
            'link' => $week->$link_method(),
        ];
    }
@endphp
<dl class="game_weeks hidden-m hidden-t">
    <dt class="regular_season">Regular Season:</dt>
        @foreach ($game_date_list->where('game_type', 'regular') as $week)
            <dd {!! $week->linkDate() == $curr_week ? 'class="current"' : '' !!}><a href="{!! $week->$link_method() !!}" data-date="{!! $week->linkDate() !!}">{!! $week->week !!}</a></dd>
        @endforeach
    @if ($playoffs)
        <dt class="playoffs">Playoffs:</dt>
            @foreach ($playoffs as $week)
                <dd {!! $week['date'] == $curr_week ? 'class="current"' : '' !!}><a href="{!! $week['link'] !!}" data-date="{!! $week['date'] !!}">{!! $week['name'] !!}</a></dd>
            @endforeach
    @endif
</dl>
@include('skeleton.widgets.subnav')
