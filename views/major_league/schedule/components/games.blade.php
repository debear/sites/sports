<ul class="games grid inline_list">
    @foreach ($list as $game)
        <li class="game-cell {!! $grid !!}">
            @include('sports.major_league.widgets.game')
        </li>
    @endforeach
</ul>
