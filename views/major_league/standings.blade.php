@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! $title !!}</h1>

@include('skeleton.widgets.subnav')

<div class="standings-wrapper">
    <ul class="inline_list standings-groups clearfix">
        @foreach ($groupings as $conf)
            <li class="by-div subnav-div clearfix">
                @isset ($conf->name_full)
                    <h3 class="{!! $conf->rowCSS() !!} {!! $conf->logoCSS('narrow-small') !!} {!! $conf->logoRightCSS('narrow-small') !!}">{!! $conf->name_full !!}</h3>
                @endisset
                @foreach ($conf->divs as $div)
                    @include('sports.major_league.standings.table', [
                    'list' => $div,
                ])
                @endforeach
            </li>
            @if ($render_conf)
                <li class="by-conf subnav-conf clearfix">
                    @include('sports.major_league.standings.table', [
                        'list' => $conf,
                    ])
                </li>
            @endif
        @endforeach
    </ul>
</div>
@include('sports.major_league.standings.key')

@endsection
