@extends('skeleton.layouts.html')

@section('content')

<h1>{!! $season_title !!} {!! FrameworkConfig::get('debear.names.section') !!} Stat Leaders</h1>

@if ($is_preseason)
    <fieldset class="info icon_info preseason">
        The {!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle(FrameworkConfig::get('debear.setup.season.viewing')) !!} statistical leaders will appear once the season has started.
    </fieldset>
@endif

{{-- Leaders --}}
<ul class="inline_list grid leaders">
    @foreach($leaders as $col => $stat)
        @php
            $tie_finders = array_fill_keys($stat->unique('pos'), false);
            $config = FrameworkConfig::get("debear.setup.stats.summary.$col");
            $is_player = ($config['type'] == 'player');
            $grid = ($is_player
                ? 'grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1'
                : 'grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1');
            // Form the page URL
            $random = $stat->random();
            $url = "/$sport/stats/{$config['type']}s/{$config['class']}?season={$random->season}&type={$random->season_type}&stat={$config['column']}";
            // Preserve the initial link for later use.
            if (!isset($teams_url)) {
                $teams_url = str_replace('?', '?team_id=&', $url);
            }
        @endphp
        <li class="stat-{!! $col !!} type-{!! $config['type'] !!} {!! $grid !!}">
            <dl class="{!! $sport !!}_box stat clearfix">
                <dt class="row-head name">{!! $config['type'] == 'team' ? 'Team ' : '' !!}{!! $config['name'] !!}</dt>
                @if ($is_player)
                    <dd class="mugshot mugshot-medium">{!! $stat->where('pos', 1)->player->image('medium') !!}</dd>
                @endif
                @foreach ($stat->slice(0, $max_num) as $row)
                    @php
                        $is_tied = $tie_finders[$row->pos];
                        $tie_finders[$row->pos] = true;
                        $row_css = 'row-' . ($loop->index % 2);
                        // Name or Tied?
                        if ($loop->iteration < $max_num || $stat->count() == $max_num) {
                            // A specific player / team
                            if ($is_player) {
                                // Build the player's team logo CSS
                                $team_list = explode(',', $row->team_id);
                                $teams = '';
                                foreach ($team_list as $team_id) {
                                    $teams .= '<span class="icon ' . DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($team_id) . '">';
                                }
                                // Prepend to the player's name
                                $name = "$teams<a href=\"{$row->player->link}\">{$row->player->name}</a>" . str_repeat('</span>', count($team_list));
                            } else {
                                // Just a team name
                                $name = '<span class="icon ' . DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($row->team_id) . '"><a href="' . $row->team->link . '">' . $row->team->franchise . '</a></span>';
                            }
                        } else {
                            // There are multiple ties remaining on the last line
                            $name = '<em>' . ($stat->count() - $loop->iteration + 1) . ($is_tied ? ' More' : '') . ' ' . ucfirst(ucfirst($config['type'])) . 's Tied</em>';
                        }
                    @endphp
                    <dt class="row-head pos">{!! !$is_tied ? $row->pos : '=' !!}</dt>
                        <dd class="{!! $row_css !!} entity">
                            {!! $name !!}
                            <span class="stat">{!! $row->stat_value !!}{!! $config['unit'] ?? '' !!}</span>
                        </dd>
                @endforeach
                <dd class="row-1 link"><a href="{!! $url !!}">View Full Stats</a></dd>
            </dl>
        </li>
    @endforeach
</ul>

{{-- Team Links --}}
@php
    $split_point = (int)round($all_teams->count() / 2);
@endphp
<dl class="{!! $sport !!}_box teams clearfix">
    <dt class="row-head">Sortable Stats by Team</dt>
    @foreach ($all_teams as $team)
        <dd {!! $loop->index == $split_point ? 'class="split-point"' : '' !!}><a class="no_underline {!! $team->logoCSS() !!}" href="{!! str_replace('team_id=&', "team_id={$team->team_id}&", $teams_url) !!}"></a></dd>
    @endforeach
</dl>

@endsection
