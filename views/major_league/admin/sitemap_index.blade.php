@php
    print '<'.'?xml version="1.0" encoding="UTF-8"?'.'>'; // Quirky to satisfy linting
@endphp
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>{!! Request::url() !!}/main</loc>
   </sitemap>
   <sitemap>
      <loc>{!! Request::url() !!}/players</loc>
   </sitemap>
   @for ($s = FrameworkConfig::get('debear.setup.season.min'); $s <= FrameworkConfig::get('debear.setup.season.max'); $s++)
    <sitemap>
      <loc>{!! Request::url() !!}/{!! $s !!}</loc>
    </sitemap>
   @endfor
</sitemapindex>
