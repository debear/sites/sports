@php
    $grid_css = 'grid-1-5 grid-tl-1-4 grid-tp-1-3 grid-m-1-2';
    $playoff_sizes_map = array_flip($playoff_sizes);
    $league = $groupings->where('name', strtoupper($sport));
@endphp
@extends('skeleton.layouts.html')

@section('content')

<h1>{!! FrameworkConfig::get('debear.names.section') !!} Logos</h1>

@include('skeleton.widgets.subnav')

{{-- Team/Groupings by size --}}
@foreach (array_keys($sizes) as $size)
    <ul class="inline_list logos grid subnav-{!! $size !!} {!! $size != $subnav['default'] ? 'hidden' : '' !!}">
        {{-- Teams --}}
        @foreach ($teams as $team)
            <li class="{!! $grid_css !!}">
                <dl>
                    <dt class="{!! $team->rowCSS() !!} {!! $team->logoCSS() !!}">{!! $team->name !!}</dt>
                        <dd class="{!! $team->logoCSS($size) !!}"></dd>
                </dl>
            </li>
        @endforeach
        {{-- Groupings --}}
        @foreach ($groupings as $grouping)
            <li class="{!! $grid_css !!}">
                <dl>
                    <dt class="{!! $grouping->rowCSS() !!} {!! $grouping->logoCSS() !!}">{!! $grouping->name_full !!}</dt>
                        <dd class="{!! $grouping->logoCSS($size) !!}"></dd>
                </dl>
            </li>
        @endforeach
        {{-- Playoffs --}}
        @isset ($playoff_sizes_map[$size])
            @foreach ($playoffs as $season)
                <li class="{!! $grid_css !!}">
                    <dl>
                        <dt class="{!! $league->rowCSS() !!} {!! $league->logoCSS() !!}">{!! ucfirst($season) !!} Playoffs</dt>
                            <dd class="playoffs-{!! $sport !!}-{!! $size !!} playoffs-{!! $sport !!}-{!! $size !!}-{!! $season !!}"></dd>
                    </dl>
                </li>
            @endforeach
        @endisset
    </ul>
@endforeach

@endsection
