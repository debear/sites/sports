<div class="key toggle-parent">
    <div>[ <a class="toggle-link" data-alt="Hide Key">Show Key</a> ]</div>
    <dl class="toggle-target hidden clearfix" data-toggle="hidden">
        <dt class="key_head row_league-{!! strtoupper(FrameworkConfig::get('debear.section.code')) !!}">Key</dt>
        @foreach ($key as $code => $label)
            @php
                $row_css = ('row-' . ($loop->iteration % 2));
            @endphp
            <dt class="{!! $row_css !!}">{!! $code !!}</dt>
                <dd class="{!! $row_css !!}">{!! $label !!}</dd>
        @endforeach
    </dl>
</div>
