@php
    $is_conf = ($list->type == 'conf');
    $head_css = $conf->rowCSS(); // Always taken from the conference
    $head_css_sec = ($is_conf ? $conf->logoCSS('narrow-small') : 'team');
    $teams = $standings->whereIn('team_id', $list->teams->unique('team_id'))->sortBy('pos_' . $list->type);
    // Header content depends on a few scenarios
    if (!$is_conf) {
        // Full Division name
        $head_content = $list->name_full;
    } elseif ($conf->icon) {
        // No content, if Conference has a logo
        $head_content = '';
    } else {
        // Short Conference name
        $head_content = $list->name;
    }
@endphp

{{-- Fixed --}}
<dl class="standings-fixed standings {!! $list->type !!}">
    {{-- Header --}}
    <dt class="{!! $head_css !!} {!! $head_css_sec !!} head">{!! $head_content !!}</dt>
    {{-- Teams --}}
    @foreach ($teams as $team_stand)
        @php
            $team = $list->teams->where('team_id', $team_stand->team_id);
            $row_css = 'row-' . ($loop->iteration % 2);
        @endphp
        <dd class="{!! $row_css !!} team">
            <span class="icon {!! $team->logoCSS() !!}">
                <a href="{!! $team->link !!}" {!! $is_conf ? 'class="hidden-m hidden-t"' : '' !!}>{!! $is_conf ? $team->team_id : $team->name !!}</a>
                @if ($is_conf)
                    <a href="{!! $team->link !!}" class="hidden-d">{!! $team->name !!}</a>
                @endif
            </span>
            @isset($team_stand->status_code)
                <sup>{!! $team_stand->status_code !!}</sup>
            @endisset
        </dd>
    @endforeach
</dl>

{{-- Scrollable --}}
<div class="standings-scroll">
    <dl class="standings {!! $list->type !!}">
        {{-- Header --}}
        @foreach ($columns[$list->type] as $code => $col)
            <dt class="{!! $col['label'] ? $head_css : '' !!} {!! $loop->first ? 'col-first' : '' !!} col {!! $code !!}  {!! $col['css'] !!}">{!! $col['label'] !!}</dt>
        @endforeach
        {{-- Teams --}}
        @foreach ($teams as $team_stand)
            @php
                $row_css = 'row-' . ($loop->iteration % 2);
            @endphp
            @foreach ($columns[$list->type] as $code => $col)
                <dd class="{!! $row_css !!} {!! $loop->first ? 'col-first' : '' !!} col {!! $code !!} {!! $col['css'] !!}">{!! $team_stand->format($col['col']) !!}</dd>
            @endforeach
        @endforeach
    </dl>
</div>
