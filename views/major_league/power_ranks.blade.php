@extends('skeleton.layouts.html')

@section('content')

<h1>Power Ranks</h1>

@isset($ranks->teams)
    @includeWhen($weeks->count() > 1, 'sports.major_league.power_ranks.switcher')

    <dl class="power_ranks ranks-{!! $ranks->week_code !!}">
        {{-- Dated time period? --}}
        @isset($ranks->date_to)
            <dd class="dates">Covering games up to {!! $ranks->date_to->format('jS F') !!}</dd>
        @endisset
        {{-- Ranks of teams --}}
        @php
            // Determine the max number of games played in this week (across all teams)
            $max_gp = 0;
            foreach ($ranks->teams as $row) {
                $max_gp = max($max_gp, count($row->games));
            }
            $respond_css = ($max_gp > 4 ? 'hidden-m hidden-t' : '');
        @endphp
        @foreach ($ranks->teams as $row)
            @include('sports.major_league.power_ranks.row')
        @endforeach
    </dl>
@else
    @include('sports.major_league.power_ranks.preseason')
@endisset

@endsection
