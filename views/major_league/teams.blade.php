@extends('skeleton.layouts.html')

@section('content')

<h1>Teams</h1>

<div class="clearfix">
    @foreach ($groupings as $conf)
        <dl class="teams clearfix">
            @isset ($conf->name_full)
                <dt class="{!! $conf->rowCSS() !!} {!! $conf->logoCSS('narrow-small') !!} {!! $conf->logoRightCSS('narrow-small') !!} conf">{!! $conf->name_full !!}</dt>
            @endisset
            @foreach ($conf->divs as $div)
                <dt class="{!! $conf->rowCSS() !!} div">{!! $div->name_full !!}</dt>
                @foreach ($div->teams as $team)
                    @php
                        $row_css = 'row-' . ($loop->index % 2);
                        $link_base = $team->link;
                    @endphp
                    <dd class="{!! $row_css !!} {!! $team->logoCSS('small') !!} logo"></dd>
                    <dd class="{!! $row_css !!} name">{!! $team->name !!}</dd>
                    <dd class="{!! $row_css !!} info">
                        <ul class="inline_list">
                            <li><a href="{!! $link_base !!}">Home</a></li><li><a href="{!! $link_base !!}#schedule">Schedule</a></li><li><a href="{!! $link_base !!}#roster">Roster</a></li>
                        </ul>
                    </dd>
                @endforeach
            @endforeach
        </dl>
    @endforeach
</div>

@endsection
