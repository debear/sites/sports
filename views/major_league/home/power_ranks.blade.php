<fieldset class="{!! $sport !!}_box">
    <h3 class="row-head">Latest Power Ranks</h3>
    <dl class="power_ranks clearfix">
        {{-- Dated time period? --}}
        @isset($power_ranks->date_to)
            <dd class="dates">Games up to {!! $power_ranks->date_to->format('jS F') !!}</dd>
        @endisset
        @foreach($power_ranks->teams->slice(0, 5) as $team)
            <dt class="row-head">{!! $team->rank !!}</dt>
                <dd class="team row-{!! $loop->iteration % 2 !!} {!! $team->team->logoCSS('narrow_small') !!}"><a href="{!! $team->team->link !!}">{!! $team->team->franchise !!}</a></dd>
        @endforeach
        <dd class="link"><a href="/{!! $sport !!}/power-ranks">Full Power Ranks</a></dd>
    </dl>
</fieldset>
