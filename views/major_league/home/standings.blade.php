@php
    $columns = ['list' => [], 'labels' => 0];
    $raw_col = \DeBear\Helpers\Sports\Namespaces::callStatic('Standings', 'parseColumns', [FrameworkConfig::get('debear.setup.season.viewing')]);
    foreach ($raw_col['conf'] as $key => $col) {
        if (!isset($col['css']) || $col['css'] != 'hidden-d') {
            $columns['list'][$key] = $col;
            $columns['labels'] += (int)($col['label'] != '');
        }
    }
    $hide_columns = ($columns['labels'] < 2);
@endphp
<fieldset class="{!! $sport !!}_box">
    <h3 class="row-head">Standings</h3>
    <dl class="standings toggle-target clearfix">
        @foreach ($groupings as $conf)
            <dt class="conf {!! $conf->rowCSS() !!} {!! $loop->iteration == 1 ? 'sel' : 'unsel' !!}" data-id="{!! $conf->grouping_id !!}">{!! $conf->name !!}</dt>
        @endforeach
        @foreach ($groupings as $conf)
            <dd class="conf tab-{!! $conf->grouping_id !!} {!! $loop->iteration > 1 ? 'hidden' : '' !!}">
                <dl>
                    @foreach ($conf->divs as $div)
                        <dt class="{!! $conf->rowCSS() !!}">{!! $div->name !!}</dt>
                        @php
                            $div_stand = $standings->whereIn('team_id', $div->teams->unique('team_id'))->sortBy('pos_div');
                        @endphp
                        @foreach ($div_stand as $stand)
                            @php
                                $team = $div->teams->where('team_id', $stand->team_id);
                                $row_css = 'row-' . ($loop->iteration % 2);
                            @endphp
                            <dd class="{!! $row_css !!} {!! $team->logoCSS() !!}">
                                <a href="{!! $team->link !!}">{!! $team->team_id !!}</a>
                                @isset($stand->status_code)
                                    <sup>{!! $stand->status_code !!}</sup>
                                @endisset
                                @foreach (array_reverse($columns['list']) as $col)
                                    <div class="col">{!! $stand->format($col['col']) !!}</div>
                                @endforeach
                            </dd>
                        @endforeach
                    @endforeach
                </dl>
            </dd>
        @endforeach
    </dl>
</fieldset>
