@php
    $config = \DeBear\Helpers\Sports\Namespaces::callStatic('PlayoffSeries', 'parseConfig', [FrameworkConfig::get('debear.setup.season.viewing')]);
    $all_rounds = $bracket->unique('round_num');
    $grid = count($all_rounds);
    // Determine the current round to focus on
    $focus_round = max($all_rounds);
    foreach ($all_rounds as $round) {
        if ($bracket->where('round_num', $round)->where('complete', '0')->count() && $round < $focus_round) {
            $focus_round = $round;
        }
    }
@endphp
<fieldset class="{!! $sport !!}_box">
    <h3 class="row-head">Playoffs</h3>
    <dl class="bracket toggle-target grid clearfix">
        @if (count($all_rounds) > 1)
            @foreach ($all_rounds as $round)
                <dt class="tab grid-cell grid-1-{!! $grid !!} {!! $round != $focus_round ? 'row-0 unsel' : 'row-head sel' !!}" data-id="{!! $round !!}" title="{!! $config['series'][$round]['full'] !!}">{!! $config['series'][$round]['short'] !!}</dt>
            @endforeach
        @endif
        @foreach ($all_rounds as $round)
            @php
                $series = $bracket->where('round_num', $round);
                $is_single = ($series->count() == 1);
                if (!$is_single) {
                    $split = $series->count() / 2;
                    $halves = [
                        'l' => $series->slice(0, $split),
                        'r' => $series->slice($split),
                    ];
                } else {
                    $halves = [
                        'r' => $series,
                    ];
                }
            @endphp
            <dd class="half grid-1-1 tab-{!! $round !!} {!! $round != $focus_round ? 'hidden' : '' !!}">
                <ul class="inline_list grid">
                    @foreach ($halves as $c => $conf)
                        @php
                            $logo_higher = ($c == 'l' || $is_single ? 'logoCSS' : 'logoRightCSS');
                            $logo_lower = ($c == 'l' ? 'logoCSS' : 'logoRightCSS');
                        @endphp
                        <li class="{!! !$is_single ? 'grid-cell grid-1-2' : '' !!} series-{!! !$is_single ? $c : 'single' !!}">
                            @foreach ($conf as $series)
                                @php
                                    // Account for placeholder teams in a series
                                    $higher_team = ($series->higher->team_id ? '<a href="' . $series->higher->link . '">' . $series->higher->team_id . '</a>' : '<em>TBD</em>');
                                    $lower_team = ($series->lower->team_id ? '<a href="' . $series->lower->link . '">' . $series->lower->team_id . '</a>' : '<em>TBD</em>');
                                    // Did a team win?
                                    $winner = $loser = 'no-one';
                                    if (isset($series->game_id)) {
                                        // By single game score.
                                        $winner = ($series->higher_score > $series->lower_score ? 'higher' : 'lower');
                                        $loser = ($series->higher_score < $series->lower_score ? 'higher' : 'lower');
                                    } elseif ($series->higher_games == $config['series'][$series->round_num]['wins']) {
                                        // Higher, by games.
                                        $winner = 'higher';
                                        $loser = 'lower';
                                    } elseif ($series->lower_games == $config['series'][$series->round_num]['wins']) {
                                        // Lower, by games.
                                        $winner = 'lower';
                                        $loser = 'higher';
                                    }
                                @endphp
                                <div class="higher {!! $winner == 'higher' ? 'winner' : ($loser == 'higher' ? 'loser' : '') !!} row-0">
                                    <span class="team {!! $series->higher->team_id ? $series->higher->$logo_higher('narrow_small') : '' !!}">{!! $higher_team !!}</span>
                                    <span class="score">{!! $series->higher_score ?? $series->higher_games !!}</span>
                                </div>
                                <div class="lower {!! $winner == 'lower' ? 'winner' : ($loser == 'lower' ? 'loser' : '') !!} row-1">
                                    <span class="team {!! $series->lower->team_id ? $series->lower->$logo_lower('narrow_small') : '' !!}">{!! $lower_team !!}</span>
                                    <span class="score">{!! $series->lower_score ?? $series->lower_games !!}</span>
                                </div>
                            @endforeach
                        </li>
                    @endforeach
                </ul>
            </dd>
        @endforeach
    </dl>
</fieldset>
