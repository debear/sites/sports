@php
    $article_img = [];
@endphp
@foreach ($news as $article)
    @php
        $a_attrib = 'href="' . $article->link . '" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window"';
        $article_img[$article->uniqueReference()] = $article->image();
    @endphp
    <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <article class="{!! $sport !!}_box">
            <h4><a {!! $a_attrib !!}>{!! $article->headline() !!}</a></h4>
            <publisher>{!! $article->publisher() !!}</publisher>
            <time>{!! $article->timeRelative() !!}</time>
            <thumbnail class="article-{!! $article->uniqueReference() !!}"><a {!! $a_attrib !!}></a></thumbnail>
            <summary>{!! $article->summary() !!}</summary>
            <a class="more" {!! $a_attrib !!}>Read Article &raquo;</a>
        </article>
    </li>
@endforeach
<style {!! HTTP::buildCSPNonceTag() !!}>
    @foreach ($article_img as $ref => $img)
        .article-{!! $ref !!} {
            background-image: url('{!! $img !!}');
        }
    @endforeach
</style>
