<fieldset class="{!! $sport !!}_box">
    <h3 class="row-head">Leaders</h3>
    <dl class="leaders clearfix">
        @foreach ($leaders as $c => $stat)
            <dd class="mugshot-small">{!! $stat->player->image('small') !!}</dd>
            <dt class="row-head">{!! FrameworkConfig::get("debear.setup.stats.summary.$c.name") !!}</dt>
            <dd class="name"><span class="icon {!! \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($stat->team_id) !!}"><a href="{!! $stat->player->link !!}">{!! $stat->player->name_short !!}</a></span></dd>
            <dd class="value">{!! $stat->stat_value !!}{!! FrameworkConfig::get("debear.setup.stats.summary.$c.unit") ?? '' !!}</dd>
        @endforeach
    </dl>
</fieldset>
