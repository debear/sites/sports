@extends('skeleton.layouts.html')

@section('content')

@php
    $nonce = HTTP::buildCSPNonceTag();

    // Determine W/L/T for each team in completed games
    $home_res = $visitor_res = '';
    if ($game->isComplete()) {
        if ($game->visitor_score > $game->home_score) {
            // Visitor win.
            $visitor_res = 'win';
            $home_res = 'loss';
        } elseif ($game->visitor_score < $game->home_score) {
            // Home win.
            $home_res = 'win';
            $visitor_res = 'loss';
        } else {
            // Tie!
            $home_res = $visitor_res = 'tie';
        }
    }
@endphp
<ul class="inline_list header">
    <li class="team visitor row_{!! $sport !!}-{!! $game->visitor_id !!}">
        <span class="{!! $game->visitor->logoRightCSS('small') !!}">
            <span class="hidden-m"><span class="hidden-tp">{!! $game->visitor->city !!} </span>{!! $game->visitor->franchise !!}</span>
            <span class="hidden-t hidden-d">{!! $game->visitor_id !!}</span>
        </span>
    </li>
    <li class="score visitor {!! $visitor_res !!}">{!! $game->visitor_score !!}</li>
    <li class="sep">&ndash;</li>
    <li class="score home {!! $home_res !!}">{!! $game->home_score !!}</li>
    <li class="team home row_{!! $sport !!}-{!! $game->home_id !!}">
        <span class="{!! $game->home->logoCSS('small') !!}">
            <span class="hidden-m"><span class="hidden-tp">{!! $game->home->city !!} </span>{!! $game->home->franchise !!}</span>
            <span class="hidden-t hidden-d">{!! $game->home_id !!}</span>
        </span>
    </li>
</ul>

{{-- Header --}}
<ul class="inline_list {!! $sport !!}_box game summary">
    <li class="row-head info">
        <span class="status">{!! $game->status_full !!}</span>
        <span class="venue">{!! $game->venue !!}</span>
    </li>
    <li class="title">
        {!! join(', ', array_filter([
            $game->title,
            $game->date_full,
        ])) !!}
    </li>
    <li class="breakdown">
        @include("sports.$sport.widgets.game.breakdown")
    </li>
    @if ($game->attendance)
        <li class="row-0 att">Attendance: {!! number_format($game->attendance) !!}</li>
    @endif
    @if ($summary = $game->series_summary)
        <li class="summary">{!! $summary !!}</li>
    @endif
</ul>

{{-- Tabs --}}
@include('skeleton.widgets.subnav')
@foreach (array_keys($subnav['list']) as $tab)
    <div class="subnav-{!! $tab !!} {!! $tab != $subnav['default'] ? 'hidden' : '' !!} clearfix">
        @includeFirst(["sports.$sport.game.$tab", "sports.major_league.game.$tab"])
    </div>
@endforeach

@endsection
