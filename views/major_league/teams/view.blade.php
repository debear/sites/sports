@extends('skeleton.layouts.html')

@section('content')

{{-- Header --}}
@include('sports.major_league.teams.view.header')

{{-- Tabs --}}
@if($team->currentStandings->isset())
    @include('skeleton.widgets.subnav')
    @foreach (array_keys($subnav['list']) as $tab)
        <div class="subnav-{!! $tab !!} subnav-tab {!! $tab != $subnav['default'] ? 'hidden' : '' !!} clearfix">
            @if ($loop->first)
                @includeFirst(["sports.$sport.teams.view.$tab", "sports.major_league.teams.view.$tab"])
            @elseif (FrameworkConfig::get("debear.setup.teams.subnav.$tab.dynamic"))
                @php
                    $dropdown = new Dropdown("season-{$tab}", $seasons, [
                        'value' => FrameworkConfig::get('debear.setup.season.viewing'),
                        'select' => false,
                    ]);
                @endphp
                <div class="tab-dropdown">{!! $dropdown->render() !!}</div>
                <div class="tab-content"></div>
                @include('sports.major_league.teams.view.widgets.loading')
            @else
                @include('sports.major_league.teams.view.widgets.loading')
            @endif
        </div>
    @endforeach
@else
    <fieldset class="info icon_info new-team">
        The {!! $team->franchise !!} are new to the {!! FrameworkConfig::get('debear.names.section') !!} and will play their first game soon!
    </fieldset>
@endif

@endsection
