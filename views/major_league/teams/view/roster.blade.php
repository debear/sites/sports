@if ($team->roster->count())
    @php
        $posgroups = \DeBear\Helpers\Sports\Namespaces::callStatic('PositionGroups', 'load');
    @endphp
    <dl class="roster">
        @foreach ($posgroups as $posgroup)
            @php
                $pos_players = $team->roster->whereIn('pos', $posgroup->positions->unique('pos_code'));
                if (!$pos_players->isset()) {
                    continue;
                }
            @endphp
            <dt class="row-head">{!! $posgroup->name !!}</dt>
            @foreach ($pos_players as $row)
                @php
                    $player = $row->player;
                    $pos = $posgroup->positions->where('pos_code', $row->pos);
                    $row_css = 'row-' . ($loop->index % 2);
                    $status_raw = ($row->player_status ?? 'active');
                    $status = FrameworkConfig::get("debear.setup.players.status.$status_raw");
                @endphp
                <dd class="{!! $row_css !!} mugshot mugshot-tiny">{!! $player->image('tiny') !!}</dd>
                <dd class="{!! $row_css !!} jersey">{!! isset($row->jersey) ? "#{$row->jersey}" : '' !!}</dd>
                <dd class="{!! $row_css !!} name">
                    <a href="{!! $player->link !!}">{!! $player->name !!}</a>
                    @isset ($status)
                        <abbrev class="{!! $status_raw !!}" title="{!! $status['long'] !!}">{!! $status['short'] !!}</abbrev>
                    @endisset
                    @isset ($row->capt_status)
                        <strong>{!! $row->capt_status !!}</strong>
                    @endisset
                </dd>
                <dd class="{!! $row_css !!} pos">{!! $pos->pos_short ?? $pos->pos_code !!}</dd>
                <dd class="{!! $row_css !!} height">{!! $player->height_fmt !!}</dd>
                <dd class="{!! $row_css !!} weight">{!! $player->weight_fmt !!}</dd>
                <dd class="{!! $row_css !!} dob">{!! $player->dob_fmt !!}</dd>
                <dd class="{!! $row_css !!} birthplace">{!! $player->birthplace_fmt !!}</dd>
            @endforeach
        @endforeach
    </dl>
@else
    {{-- Preseason --}}
    <fieldset class="info icon_info preseason">
        Rosters will be available shortly before the start of the season.
    </fieldset>
@endif
