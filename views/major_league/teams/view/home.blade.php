@foreach (['schedule', 'standings', 'stats', 'leaders'] as $view)
    @includeFirst(["sports.$sport.teams.view.home.$view", "sports.major_league.teams.view.home.$view"])
@endforeach
