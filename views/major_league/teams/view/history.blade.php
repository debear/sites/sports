@php
    $row_count = 0;
    $last_league_id = -1;
    $last_team_id = $team->team_id;
    $seasons = $team->history->seasons->regular->unique('season');
    $grouping = $team->history->groupings;
@endphp
<dl class="history clearfix">
    @foreach ($seasons as $s)
        @php
            $team_seasons = $team->history->seasons->regular->where('season', $s);
            $playoffs = $team->history->seasons->playoff->where('season', $s);
            $row_css = 'row-' . (++$row_count % 2);
            // New grouping info?
            if ($grouping->season_from > $s) {
                $grouping = $team->history->groupings->where('season_to', $s);
            }
        @endphp
        {{-- League summary? --}}
        @if ($grouping->grouping->league_id != $last_league_id)
            @php
                $last_league_id = $grouping->grouping->league_id;
            @endphp
            <dt class="league row-head {!! $grouping->grouping->league->rowCSS() !!}">
                <span class="icon {!! $grouping->grouping->league->logoCSS() !!}">{!! $grouping->grouping->league->name_full !!}</span>
            </dt>
            @include('sports.major_league.teams.view.history.titles')
        @endif
        {{-- Old team name? --}}
        @php
            $prev_name = $team->history->naming->where('season_to', $s);
        @endphp
        @if ($prev_name->isset())
            <dt class="row-head aka {!! $prev_name->rowCSS() !!}">Known as <span class="{!! $prev_name->logoCSS() !!}">{!! $prev_name->city !!} {!! $prev_name->franchise !!}</span></dt>
            @if ($prev_name->alt_team_id != $last_team_id)
                @php
                    $last_team_id = $prev_name->alt_team_id;
                @endphp
                @include('sports.major_league.teams.view.history.titles')
            @endif
        @endif
        {{-- Regular Season --}}
        <dt class="row-head parts-{!! $team_seasons->count() !!} season">{!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($s) !!}</dt>
        @foreach ($team_seasons as $season)
            <dd class="{!! $row_css !!} record">{!! $season->record !!}, {!! Format::ordinal($season->pos) !!} {!! $grouping->grouping->name !!}</dd>
        @endforeach
        {{-- Playoffs --}}
        @if ($playoffs->count())
            <dd class="{!! $row_css !!} playoffs">
                {{-- Matchups --}}
                <ul class="inline_list matchups clearfix">
                    @foreach ($playoffs as $p)
                        <li class="{!! $p->textCSS() !!}">
                            <span class="hidden-m">{!! FrameworkConfig::get("debear.setup.playoffs.codes.{$p->round_code}.full") !!}:</span>
                            <span class="hidden-t hidden-d">{!! FrameworkConfig::get("debear.setup.playoffs.codes.{$p->round_code}.short") !!}:</span>
                            {!! $p->result !!} {!! $p->for !!}&ndash;{!! $p->against !!}{!! isset($p->info) ? " ({$p->info})" : '' !!}
                            @isset ($p->opp_team_id)
                                v <span class="icon {!! \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($p->opp_team_id) !!}">{!! $p->opp_team_alt !!}</span>
                            @else
                                v {!! $p->opp_team_alt !!}
                            @endisset
                        </li>
                    @endforeach
                </ul>
            </dd>
        @endif
    @endforeach
</dl>
