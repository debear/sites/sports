{{-- Name --}}
<h1 class="{!! $team->logoRightCSS('narrow_large') !!}">{!! $team->name_responsive !!}</h1>
{{-- Favourite team? --}}
@if (DeBear\Models\Skeleton\User::object()->isLoggedIn())
    <div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="{!! $favourite->isset() ? 'rmv' : 'add' !!}">
            <span class="icons {!! $favourite->icon() !!} {!! $favourite->hover() !!}"></span>
            <span class="text">{!! $favourite->isset() ? 'Remove from favourites' : 'Add to favourites' !!}</span>
        </button>
    </div>
@endif
<dl class="inline_list header clearfix">
    {{-- Standings --}}
    @if($team->currentStandings->isset())
        <dt class="standing">{!! Format::ordinal($team->currentStandings->pos_div) !!}, {!! $team->division->name_full !!}:</dt>
            <dd class="standing">{!! $team->currentStandings->record . (isset($team->currentStandings->pts) ? ', ' . Format::pluralise($team->currentStandings->pts, 'pt') : '') !!}</dd>
    @endif
    {{-- Last Game --}}
    @php
        $last_game = $team->recentSchedule->whereNotNull('status')->last();
    @endphp
    @if($last_game->isset())
        @php
            $opp = $last_game->{$last_game->team_opp};
        @endphp
        <dt class="prev game">Last Game:</dt>
            <dd class="prev game">
                {!! $last_game->title_short ?: $last_game->date_short !!}
                @if ($last_game->isComplete())
                    <span class="{!! $last_game->resultCSS() !!}">{!! $last_game->resultCode() !!} {!! $last_game->resultScore() !!}</span>
                @else
                    <span class="result-tie">{!! $last_game->status !!}</span>
                @endif
                {!! $last_game->team_venue !!}
                <span class="{!! $opp->logoCSS() !!} icon">{!! $opp->team_id !!}</span>
            </dd>
    @endif
    {{-- Next Game --}}
    @php
        $next_game = $team->recentSchedule->whereNull('status')->first()
    @endphp
    @if($next_game->isset())
        @php
            $opp = $next_game->{$next_game->team_opp};
        @endphp
        <dt class="next game">Next Game:</dt>
            <dd class="next game">
                {!! $next_game->title_short ?: $next_game->date_short !!}
                {!! $next_game->team_venue !!}
                <span class="{!! $opp->logoCSS() !!} icon">{!! $opp->team_id !!}</span>
            </dd>
    @endif
</dl>
