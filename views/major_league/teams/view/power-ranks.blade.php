@php
    if ($team->power_ranks->count() > 1) {
        // Get the chart data
        $chart = $team->powerRanksChart('chart-power-ranks');
        $ret = [
            'chart' => [],
            'extraJS' => [],
        ];
        foreach ($chart['extraJS'] as $name => $var) {
            $ret['extraJS'][$name] = $var;
        }
        unset($chart['extraJS']);
        $ret['chart'] = $chart;
    } else {
        // Preseason
        $ret = false;
    }
@endphp
@json($ret)
