@php
    // Build the list (also to determine if we have anything to display)
    $stats = array_filter(FrameworkConfig::get('debear.setup.stats.summary'), function ($a) {
        return $a['type'] == 'team';
    });
    $team_stats = [];
    foreach ($stats as $key => $stat) {
        if (!isset($team->stats->{$stat['class']}->sort->{$stat['column']})) {
            continue;
        }
        $team_stats[$key] = [
            'name' => ($stat['name_short'] ?? $stat['name']),
            'rank' => Format::ordinal($team->stats->{$stat['class']}->sort->{$stat['column']}),
            'value' => $team->stats->{$stat['class']}->stat->format($stat['column']) . (isset($stat['unit']) ? "<unit>{$stat['unit']}</unit>" : ''),
        ];
    }
@endphp
@if (count($team_stats))
    <ul class="inline_list stats clearfix">
        <li class="row-head title">Team Stats</li>
        @foreach ($team_stats as $stat)
            <li class="cell {!! $sport !!}_box">
                <dl class="clearfix">
                    <dt class="row-head">{!! $stat['name'] !!}</dt>
                        <dd class="rank">{!! $stat['rank'] !!}</dd>
                        <dd class="value"><span>{!! $stat['value'] !!}</span></dd>
                </dl>
            </li>
        @endforeach
    </ul>
@endif
