@php
    $columns = ['list' => [], 'labels' => 0];
    $raw_col = \DeBear\Helpers\Sports\Namespaces::callStatic('Standings', 'parseColumns', [FrameworkConfig::get('debear.setup.season.viewing')]);
    foreach ($raw_col['conf'] as $key => $col) {
        if (!isset($col['css']) || $col['css'] != 'hidden-d') {
            $columns['list'][$key] = $col;
            $columns['labels'] += (int)($col['label'] != '');
        }
    }
    $hide_columns = ($columns['labels'] < 2);
@endphp
<ul class="inline_list standings {!! $sport !!}_box clearfix">
    {{-- Division name --}}
    <li class="div {!! $team->conference->rowCSS() !!} {!! $team->conference->logoRightCSS('narrow-small') !!}">{!! $team->division->name_full !!}</li>

    {{-- Column headers? --}}
    @if (!$hide_columns)
        <li class="header">
            @foreach (array_reverse($columns['list']) as $col)
                <div class="col {!! !$col['label'] ? 'col-empty' : '' !!} {!! $team->conference->rowCSS() !!}">{!! $col['label'] !!}</div>
            @endforeach
        </li>
    @endif

    {{-- Team rows --}}
    @foreach ($team->divStandings as $stand)
        @php
            if ($stand->team_id == $team->team_id) {
                $row_css = $team->rowCSS();
            } else {
                $row_css = 'row-' . ($loop->iteration % 2);
            }
        @endphp
        <li class="{!! $row_css !!} team">
            <span class="{!! $stand->team->logoCSS() !!}">
                <a href="{!! $stand->team->link !!}">{!! $stand->team->franchise !!}</a>
            </span>
            @isset($stand->status_code)
                <sup>{!! $stand->status_code !!}</sup>
            @endisset
            @foreach (array_reverse($columns['list']) as $col)
                <div class="{!! $row_css !!} col">{!! $stand->format($col['col']) !!}</div>
            @endforeach
        </li>
    @endforeach
</ul>
