<ul class="inline_list recent clearfix">
    @foreach($team->recentSchedule as $game)
        <li class="{!! $sport !!}_box">
            @if ($game->isComplete())
                <a href="{!! $game->link !!}">
            @endif
            <dl>
                <dt class="row-head">{!! $game->title_short ?: $game->date_short !!}</dt>
                    <dd class="opp {!! $game->{$game->team_opp}->logoRightCSS('narrow_small') !!}">
                        {!! $game->team_venue !!}
                    </dd>
                    @if ($game->isComplete())
                        <dd class="result row-0 {!! $game->resultCSS() !!}">{!! $game->resultCode() !!} {!! $game->resultScore() !!}</dd>
                    @else
                        <dd class="status row-0">{!! $game->isAbandoned() ? $game->status_disp : $game->start_time !!}</dd>
                    @endif
            </dl>
            @if ($game->isComplete())
                </a>
            @endif
        </li>
    @endforeach
</ul>
