@php
    // Build the list (also to determine if we have anything to display)
    $stats = array_filter(FrameworkConfig::get('debear.setup.stats.summary'), function ($a) {
        return $a['type'] == 'player';
    });
    $team_leaders = [];
    foreach ($stats as $key => $stat) {
        $player_id = $team->leaders->{$stat['class']}->sort->where($stat['column'], 1)->player_id;
        if (!isset($player_id)) {
            continue;
        }
        $player = $team->leaders->{$stat['class']}->stat->where('player_id', $player_id);
        $team_leaders[$key] = [
            'name' => ($stat['name_short'] ?? $stat['name']),
            'player' => $player->player,
            'value' => $player->format($stat['column']) . (isset($stat['unit']) ? "<unit>{$stat['unit']}</unit>" : ''),
        ];
    }
@endphp
@if (count($team_leaders))
    <ul class="inline_list leaders clearfix">
        <li class="row-head title">Team Leaders</li>
        @foreach ($team_leaders as $key => $leader)
            <li class="cell {!! $key !!} {!! $sport !!}_box">
                <dl>
                    <dt class="row-head">{!! $leader['name'] !!}</dt>
                        <dd class="name"><a href="{!! $leader['player']->link !!}">{!! $leader['player']->name_short !!}</a></dd>
                        <dd class="mugshot mugshot-small">{!! $leader['player']->image('small') !!}</dd>
                        <dd class="value"><span>{!! $leader['value'] !!}</span></dd>
                </dl>
            </li>
        @endforeach
    </ul>
@endif
