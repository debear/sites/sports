@php
    if ($team->standings->count() > 1) {
        // Get the chart data
        $chart = $team->standingsChart('chart-standings');
        $ret = [
            'chart' => [],
            'extraJS' => [],
        ];
        foreach ($chart['extraJS'] as $name => $var) {
            $ret['extraJS'][$name] = $var;
        }
        unset($chart['extraJS']);
        $ret['chart'] = $chart;
    } else {
        // Preseason
        $ret = false;
    }
@endphp
@json($ret)
