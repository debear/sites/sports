@php
    $league_trophy = (FrameworkConfig::get("debear.setup.playoffs.league-trophies.$last_league_id") ?? 'League Title');
    $playoff_trophy = (FrameworkConfig::get("debear.setup.playoffs.playoff-trophies.$last_league_id") ?? 'Playoff Championship');
    $titles = $team->history->titles
        ->where('league_id', $last_league_id)
        ->where('team_id', $team_seasons->team_id);
    $titles_fmt = array_filter([
        isset($titles->div_champ) ? Format::pluralise($titles->div_champ, ' Division Title') : false,
        isset($titles->conf_champ) ? Format::pluralise($titles->conf_champ, ' Conference Title') : false,
        isset($titles->league_champ) ? Format::pluralise($titles->league_champ, " $league_trophy") : false,
        isset($titles->playoff_champ) ? Format::pluralise($titles->playoff_champ, " $playoff_trophy") : false,
    ]);
@endphp
@if (count($titles_fmt))
    <dd class="league titles row-head {!! $grouping->grouping->league->rowCSS() !!}"><span>{!! join('</span>; <span>', preg_replace('/s{2,}$/', 's', $titles_fmt)) !!}</span></dd>
@endif
