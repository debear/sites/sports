{{-- Comparisons --}}
<dl class="{!! $sport !!}_box comparison clearfix">
    <dt class="row-head">Team Stats</dt>
    <dt class="team visitor {!! $game->visitor->rowCSS() !!} {!! $game->visitor->logoCSS() !!}">{!! $game->visitor->name_responsive !!}</dt>
    <dt class="team home {!! $game->home->rowCSS() !!} {!! $game->home->logoCSS() !!}">{!! $game->home->name_responsive !!}</dt>

    @foreach ($stats as $title => $section)
        @if (!is_int($title))
            <dt class="row-0 section">{!! $title !!}</dt>
        @endif

        @foreach ($section as $stat)
            @include('sports.major_league.game.stats.row', $stat)
        @endforeach
    @endforeach
</dl>
<style {!! $nonce !!}>
    @foreach (InternalCache::object()->get('game-bar-widths') as $key => $bar)
        .subnav-stats .stat-{!! $key !!} .bars .visitor { width: {!! $bar['visitor'] !!}; }
        .subnav-stats .stat-{!! $key !!} .bars .home { width: {!! $bar['home'] !!}; }
    @endforeach
</style>
