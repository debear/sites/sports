<dd class="row-0 label">{!! $label !!}</dd>
@php
    $total = $visitor + $home;
    if (!$total) {
        $visitor = $home = 1;
        $total = 2;
    }
    // Calculate the styling for the bar styling
    $key = Strings::codify("$title $label");
    InternalCache::object()->append('game-bar-widths', [$key => [
        'visitor' => sprintf('%0.02f%%', (100 * $visitor) / $total),
        'home' => sprintf('%0.02f%%', (100 * $home) / $total),
    ]]);
@endphp
<dd class="row-1 stat stat-{!! $key !!}">
    <span class="visitor">{!! $visitor_label ?? $visitor !!}</span>
    <div class="bar visitor {!! $game->visitor->rowCSS() !!}"></div>
    <div class="bars">
        <div class="visitor {!! $game->visitor->rowCSS() !!}"></div>
        <div class="home {!! $game->home->rowCSS() !!}"></div>
        <div class="gap {!! $game->home->rowCSS() !!}"></div>
    </div>
    <div class="bar home {!! $game->home->rowCSS() !!}"></div>
    <span class="home">{!! $home_label ?? $home !!}</span>
</dd>
