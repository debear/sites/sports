{{-- Managed by per-game_type views --}}
@php
    $is_regular = ($game->game_type == 'regular');
@endphp
<ul class="inline_list {!! $sport !!}_box series clearfix">
    <li class="row-head title">{!! $is_regular ? 'Season Series' : $game->series_title !!}</li>
    @foreach($game->matchups as $i => $matchup)
        @php
            $row_css = 'row-' . ($i % 2);
            $winner = false;
            if ($matchup->isComplete() && $matchup->home_score != $matchup->visitor_score) {
                $winner = ($matchup->home_score > $matchup->visitor_score ? $matchup->home_id : $matchup->visitor_id);
            }
        @endphp
        <li class="row-head date">{!! $is_regular ? $matchup->date_short : 'Game ' . substr($matchup->game_id, -1) !!}</li>
        <li class="{!! $row_css !!} team visitor"><span class="{!! $matchup->visitor->logoCSS() !!}">{!! $matchup->visitor_id !!}</span></li>
        <li class="{!! $row_css !!} score visitor{!! $winner ? ($winner == $matchup->visitor_id ? ' win' : ' loss') : '' !!}">{!! $matchup->visitor_score !!}</li>
        <li class="{!! $row_css !!} at">@</li>
        <li class="{!! $row_css !!} score home{!! $winner ? ($winner == $matchup->home_id ? ' win' : ' loss') : '' !!}">{!! $matchup->home_score !!}</li>
        <li class="{!! $row_css !!} team home"><span class="{!! $matchup->home->logoCSS() !!}">{!! $matchup->home_id !!}</span></li>
        <li class="{!! $row_css !!} info">{!! !$matchup->isPreview() ? $matchup->status_full : '<em>' . ($is_regular ? $matchup->start_time : $matchup->date_short) . '</em>' !!}</li>
        <li class="{!! $row_css !!} links">
            @if($matchup->isComplete())
                @foreach (FrameworkConfig::get('debear.setup.game.tabs') as $link)
                    <a class="icon_game_{!! $link['icon'] !!}" href="{!! $matchup->link !!}{!! $link['link'] ? "#{$link['link']}" : '' !!}"></a>
                @endforeach
            @endif
        </li>
    @endforeach
</ul>
