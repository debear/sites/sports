@php
    $size = 'small';
@endphp
@extends('skeleton.layouts.html')

@section('content')

<h1>{!! FrameworkConfig::get('debear.names.section') !!} Award Winners</h1>

<ul class="inline_list grid">
    @foreach ($awards as $award)
        {{-- Potentially sport-specific rendering --}}
        @includeFirst(["sports.$sport.awards.row", 'sports.major_league.awards.row'])
    @endforeach
</ul>

{{-- All-Time History Modal --}}
<modal class="all-time hidden">
    <cover></cover>
    <list>
        <close class="row-head">[<a class="modal-close">Close</a>]</close>
        <h3 class="row-head"></h3>
        <fieldset class="{!! $sport !!}_box"></fieldset>
    </list>
</modal>

@endsection
