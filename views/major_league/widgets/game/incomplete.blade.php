<ul class="inline_list {!! FrameworkConfig::get('debear.section.code') !!}_box game incomplete">
    <li class="status head"><strong>{!! $game->status_full !!}</strong></li>
    @if ($game->title)
        <li class="title">{!! $game->title !!}</li>
    @endif
    <li class="team visitor">
        <a href="{!! $game->visitor->link !!}" class="{!! $game->visitor->logoCSS('narrow_small') !!}">{!! $game->visitor->franchise !!}</a>
        @isset($game->visitor->standings)
            <span class="standings">({!! $game->visitor->standings->record !!})</span>
        @endisset
    </li>
    <li class="team home">
        <a href="{!! $game->home->link !!}" class="{!! $game->home->logoCSS('narrow_small') !!}">{!! $game->home->franchise !!}</a>
        @isset($game->home->standings)
            <span class="standings">({!! $game->home->standings->record !!})</span>
        @endisset
    </li>
    <li class="venue head">{!! $game->venue !!}</li>
</ul>
