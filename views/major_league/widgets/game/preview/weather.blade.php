<li class="weather line-with-icon">
    <dl>
        <dt>Forecast:</dt>
        @if ($venue->isDomed())
            <dd class="symbol icon_weather_dome">Game is in a domed stadium</dd>
        @else
            <dd class="symbol icon_weather_{!! $game->weather->symbol !!}">{!! $game->weather->description !!}</dd>
            <dd class="temp">{!! (int)$game->weather->tempF !!} &deg;F</dd>
        @endif
        @if ($game->weather->wind_speed)
            <dd class="wind">{!! (int)$game->weather->wind_speed !!}mph {!! $game->weather->wind_dir !!}</dd>
        @endif
    </dl>
</li>
