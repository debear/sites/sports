<li class="odds line-with-icon">
    <dl>
        <dt>Odds:</dt>
        {{-- Money Line --}}
        @if (FrameworkConfig::get('debear.setup.odds.display.moneyline'))
            <dd class="moneyline icon team-{!! FrameworkConfig::get('debear.section.code') !!}-{!! $game->visitor_id !!}">{!! $game->odds->visitor_line_us !!}</dd>
            <dd class="moneyline icon team-{!! FrameworkConfig::get('debear.section.code') !!}-{!! $game->home_id !!}">{!! $game->odds->home_line_us !!}</dd>
        @endif

        {{-- Spread --}}
        @if (FrameworkConfig::get('debear.setup.odds.display.spread'))
            @if (FrameworkConfig::get('debear.setup.odds.display.moneyline'))<dt class="over-under">Spread:</dt>@endif
                <dd class="spread icon team-{!! FrameworkConfig::get('debear.section.code') !!}-{!! $game->odds->home_spread < 0 ? $game->home_id : $game->visitor_id !!}">{!! min($game->odds->home_spread, $game->odds->visitor_spread) !!}</dd>
        @endif

        {{-- Over/Under --}}
        @if (FrameworkConfig::get('debear.setup.odds.display.over_under'))
            <dt class="over-under">O/U:</dt>
                <dd class="over-under">{!! $game->odds->over_under !!}</dd>
        @endif
    </dl>
</li>
