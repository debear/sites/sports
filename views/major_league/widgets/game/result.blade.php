@php
    $game_link = $game->link;
    // Determine W/L/T for each team in completed games
    $home_res = $visitor_res = '';
    if ($game->isComplete()) {
        if ($game->visitor_score > $game->home_score) {
            // Visitor win.
            $visitor_res = 'win';
            $home_res = 'loss';
        } elseif ($game->visitor_score < $game->home_score) {
            // Home win.
            $home_res = 'win';
            $visitor_res = 'loss';
        } else {
            // Tie!
            $home_res = $visitor_res = 'tie';
        }
    }
@endphp
<ul class="inline_list {!! FrameworkConfig::get('debear.section.code') !!}_box game result">
    {{-- Summary --}}
    <li class="status head">
        <strong>{!! $game->status_full !!}</strong>
        @if ($game->attendance)
            <span class="att">Attendance: {!! number_format($game->attendance) !!}</span>
        @endif
    </li>
    @if ($game->title)
        <li class="title">{!! $game->title !!}</li>
    @endif
    {{-- Final Score --}}
    <li class="team visitor {!! $visitor_res !!}">
        <span class="score">{!! $game->visitor_score !!}</span>
        <a href="{!! $game->visitor->link !!}" class="{!! $game->visitor->logoCSS('narrow_small') !!}">{!! $game->visitor->franchise !!}</a>
        @isset($game->visitor->standings)
            <span class="standings">({!! $game->visitor->standings->record !!})</span>
        @endisset
    </li>
    <li class="team home {!! $home_res !!}">
        <span class="score">{!! $game->home_score !!}</span>
        <a href="{!! $game->home->link !!}" class="{!! $game->home->logoCSS('narrow_small') !!}">{!! $game->home->franchise !!}</a>
        @isset($game->home->standings)
            <span class="standings">({!! $game->home->standings->record !!})</span>
        @endisset
    </li>
    {{-- Breakdown --}}
    <li class="breakdown">
        @include("sports.$sport.widgets.game.breakdown")
    </li>
    {{-- Playoff Series --}}
    @if ($game->isPlayoff() && $game->series_summary)
        <li class="playoff">
            {!! $game->series_summary !!}
        </li>
    @endif
    {{-- Footer --}}
    <li class="venue head">{!! $game->venue !!}</li>
    {{-- Extra Info --}}
    @includeIf("sports.$sport.widgets.game.info")
    {{-- Links --}}
    <li class="links">
        <ul class="inline_list">
            @foreach (array_reverse(FrameworkConfig::get('debear.setup.game.tabs')) as $link)
                <li>
                    <a class="icon_game_{!! $link['icon'] !!}" href="{!! $game_link !!}{!! $link['link'] ? "#{$link['link']}" : '' !!}"><em>{!! is_string($link['label']) ? $link['label'] : $link['label'][$game->game_type] !!}</em></a>
                </li>
            @endforeach
        </ul>
    </li>
</ul>
