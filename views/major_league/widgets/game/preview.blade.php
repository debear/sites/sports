@php
    $venue = ($game?->alt_venue_model ?? $game->home->currentVenue);
@endphp
<ul class="inline_list {!! FrameworkConfig::get('debear.section.code') !!}_box game preview">
    <li class="status head">{!! $game->start_time !!}</li>
    @if ($game->title)
        <li class="title">{!! $game->title !!}</li>
    @endif
    <li class="team visitor">
        <a href="{!! $game->visitor->link !!}" class="{!! $game->visitor->logoCSS('narrow_small') !!}">{!! $game->visitor->franchise !!}</a>
        @isset($game->visitor->standings)
            <span class="standings">({!! $game->visitor->standings->record !!})</span>
        @endisset
    </li>
    <li class="team home">
        <a href="{!! $game->home->link !!}" class="{!! $game->home->logoCSS('narrow_small') !!}">{!! $game->home->franchise !!}</a>
        @isset($game->home->standings)
            <span class="standings">({!! $game->home->standings->record !!})</span>
        @endisset
    </li>
    {{-- Playoff Series --}}
    @if ($game->isPlayoff() && $game->series_summary)
        <li class="playoff">
            {!! $game->series_summary !!}
        </li>
    @endif
    {{-- Weather Info --}}
    @includeWhen(($game?->weather?->isset() ?? false) || $venue->isDomed(), "sports.major_league.widgets.game.preview.weather")
    {{-- Game Odds --}}
    @includeWhen($game?->odds?->isset() ?? false, "sports.major_league.widgets.game.preview.odds")
    {{-- Location --}}
    <li class="venue head">{!! $game->venue !!}</li>
    {{-- Preview Info --}}
    @includeIf("sports.$sport.widgets.game.preview")
</ul>
