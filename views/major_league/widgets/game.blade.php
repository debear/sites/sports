{{-- Game Preview --}}
@includeWhen($game->isPreview(), "sports.major_league.widgets.game.preview")
{{-- Game Abandoned --}}
@includeWhen($game->isAbandoned(), "sports.major_league.widgets.game.incomplete")
{{-- Game Result --}}
@includeWhen($game->isComplete(), "sports.major_league.widgets.game.result")
