@php
    // Determine W/L/T for each team in completed games
    $home_res = $visitor_res = '';
    if ($game->isComplete()) {
        if ($game->visitor_score > $game->home_score) {
            // Visitor win.
            $visitor_res = 'win';
            $home_res = 'loss';
        } elseif ($game->visitor_score < $game->home_score) {
            // Home win.
            $home_res = 'win';
            $visitor_res = 'loss';
        } else {
            // Tie!
            $home_res = $visitor_res = 'tie';
        }
    }
@endphp
<dd>
    <div class="team visitor row-1 icon {!! $game->visitor->logoCSS() !!} {!! $visitor_res !!}">
        {!! $game->visitor_id !!}
        @if ($game->isComplete())
            <span class="score">{!! $game->visitor_score !!}</span>
        @endif
    </div>
    <div class="team home row-0 icon {!! $game->home->logoCSS() !!} {!! $home_res !!}">
        {!! $game->home_id !!}
        @if ($game->isComplete())
            <span class="score">{!! $game->home_score !!}</span>
        @endif
    </div>
    <div class="info row-1">
        @if ($game->isComplete())
            {{-- Game Links --}}
            <div class="link"><a href="{!! $game->link !!}" class="icon_right_game_boxscore no_underline">Boxscore</a></div>
            <div class="status">{!! $game->status_full !!}</div>
        @elseif ($game->isAbandoned())
            {{-- Abandoned Info --}}
            <em>{!! $game->status_full !!}</em>
        @else
            {{-- Status Info --}}
            {!! $game->start_time !!}
        @endif
    </div>
</dd>
