@php
    $game_date = \DeBear\Helpers\Sports\Namespaces::callStatic('ScheduleDate', 'getLatest');
    $schedule = \DeBear\Helpers\Sports\Namespaces::callStatic('Schedule', 'loadByDate', [$game_date, ['order-played-last' => true]]);
@endphp
<div class="schedule_header">
    <dl class="num-{!! $schedule->count() !!}">
        <dt class="row-head date">{!! $game_date->name_short !!}</dt>
        @each('sports.major_league.widgets.game_header.game', $schedule, 'game')
    </dl>
</div>
