@extends('skeleton.layouts.html')

@section('content')

{{-- Header and Filters --}}
<h1>{!! isset($opt['team_id']) ? $all_teams->where('team_id', $opt['team_id'])->name_responsive : FrameworkConfig::get('debear.names.section') !!} {!! $config['name'] !!} Leaders</h1>
@include('sports.major_league.stats.filters')

{{-- Table --}}
@php
    $name_col = ($type == 'player' ? 'name_short' : 'franchise');
    $url_base = '?';
    if (isset($opt['team_id'])) {
        $url_base .= "team_id={$opt['team_id']}&";
    }
    $url_base .= "season=$season&type=$season_type";
    $inc_stat_groups = isset($columns[$column]['g']);
    $default_pos = array_search($column, array_keys($columns)) + 1;
@endphp
<scrolltable class="scroll-x scrolltable-{!! $type !!}-{!! $class !!}" data-default="{!! $default_pos !!}" data-total="{!! count($columns) !!}" data-current="{!! $column !!}">
    <mobile-select>
        <current class="row-head">{!! $columns[$column]['f'] !!}</current>
        <select>
            @foreach ($columns as $col => $labels)
                <option value="{!! $col !!}" {!! $col == $column ? ' selected="selected"' : '' !!}>{!! $labels['f'] !!}</option>
            @endforeach
        </select>
    </mobile-select>
    <main>
        {{-- Header --}}
        <top {!! $inc_stat_groups ? 'class="stats-grouped"' : '' !!}>
            <cell class="row-head group">{!! ucfirst($type) . 's' !!}</cell>
        </top>
        {{-- Entities --}}
        @php
            $pos_cache = [];
        @endphp
        @foreach ($stats as $row)
            <row>
                @php
                    // Form the position
                    $pos = $row->stat_pos;
                    if (!isset($pos_cache[$pos]) || !$pos_cache[$pos]) {
                        $pos_cache[$pos] = true;
                    } else {
                        $pos = '=';
                    }
                    // Team logos
                    $team_list = explode(',', $row->team_id);
                    $teams = [];
                    foreach ($team_list as $team_id) {
                        $teams[] = '<span class="icon ' . DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($team_id) . '">';
                    }
                @endphp
                <cell class="row-head pos sort_order">{!! $pos !!}</cell>
                <cell class="row-{!! $loop->iteration % 2 !!} entity">{!! join($teams) !!}<a href="{!! $row->$type->link !!}">{!! $row->$type->$name_col !!}</a>{!! str_repeat('</span>', count($teams)) !!}</cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top {!! $inc_stat_groups ? 'class="stats-grouped"' : '' !!}>
                @php
                    // Build the components
                    $header = [
                        'parsed' => 0,
                        'groups' => [],
                        'names' => [],
                        'sorts' => [],
                    ];
                    foreach ($columns as $col => $labels) {
                        $is_first = (!$header['parsed'] ? 'first' : '');
                        $header['parsed']++;
                        // Grouped?
                        if (isset($labels['g']) && !isset($header['groups'][$labels['g']])) {
                            $header['groups'][$labels['g']] = '<cell class="row-head stat-group group-' . Strings::codify($labels['g']) . '">' . $labels['g'] . '</cell>';
                        }
                        // Name
                        $header['names'][] = '<cell class="row-head ' . $is_first . ' col-' . $col . ' name"><abbr title="' . $labels['f'] . '">' . $labels['s'] . '</abbr></cell>';
                        // Sort
                        $cell_css = ($col != $column ? ' unsel' : '_disabled sel');
                        $header['sorts'][] = '<cell class="row-head ' . $is_first . ' col-' . $col . ' sort">'
                            . '<a href="' . $url_base . '&stat=' . $col . '" class="no_underline icon_sort_up' . $cell_css . '"></a>'
                            . '</cell>';
                    }
                @endphp
                {{-- Display Groups? --}}
                {!! join($header['groups']) !!}
                {{-- Display Names --}}
                {!! join($header['names']) !!}
                {{-- Display Sorts --}}
                {!! join($header['sorts']) !!}
            </top>
            {{-- Entities --}}
            @foreach ($stats as $stat)
                @php
                    $row_css = 'row-' . ($loop->iteration % 2);
                @endphp
                <row>
                    @foreach (array_keys($columns) as $col)
                        @php
                            // Non-qualified player?
                            $qual = true;
                            if (isset($qual_stats[$col])) {
                                $col_qual = "qual_$col";
                                $qual = (bool)$stat->$col_qual;
                            }
                        @endphp
                        <cell class="{!! $col != $column ? $row_css : 'row-head' !!} {!! !$qual ? 'non-qual' : '' !!} stat col-{!! $col !!} {!! $col != $column ? 'hidden-m' : '' !!}">{!! $stat->format($col) !!}</cell>
                    @endforeach
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>

{{-- Qualifying Stats info label? --}}
@if (count($qual_stats))
    <div class="{!! $sport !!}_box non-qual">Indicates a player who does not qualify as a statistical leader</div>
@endif

{{-- Pagination --}}
@if ($stats->totalRows() > $stats->count())
    <pagination class="clearfix">
        {!! Pagination::render($stats->totalRows(), $opt['per_page'], "$url_base&stat=$column&page=", $opt['page']) !!}
    </pagination>
@endif

@endsection
