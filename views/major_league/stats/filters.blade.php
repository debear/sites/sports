<ul class="inline_list filters">
    <li class="toggle"><span class="brace">[ </span><a class="filter-show_hide show"><span>Show</span> Filters</a><span class="brace"> ]</span></li>
    <li class="panel row-1 hidden">
        <dl class="all clearfix">
            {{-- Team / League --}}
            @if ($type == 'player')
                <dt class="cat">Team:</dt>
                    @php
                        $opt_team = [[
                            'id' => '',
                            'label' => 'All ' . FrameworkConfig::get('debear.names.section'),
                        ]];
                        foreach ($all_teams as $t) {
                            $opt_team[] = [
                                'id' => $t->team_id,
                                'label' => '<span class="icon ' . $t->logoCSS() . '">' . $t->team_id . '</span>',
                            ];
                        }
                        $drop_team = new Dropdown('filter_team', $opt_team, [
                            'value' => $opt['team_id'] ?? '',
                            'select' => false,
                        ]);
                    @endphp
                    <dd class="opt-team">{!! $drop_team->render() !!}</dd>
            @endif
            {{-- Season --}}
            <dt class="cat">Season:</dt>
                @php
                    $opt_season = [];
                    foreach ($seasons as $s) {
                        $opt_season[] = [
                            'id' => "{$s->season}::{$s->season_type}",
                            'label' => \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($s->season) . ' ' . ($s->season_type == 'regular' ? 'Regular Season' : 'Playoffs'),
                        ];
                    }
                    $drop_season = new Dropdown('filter_season', $opt_season, [
                        'value' => "$season::$season_type",
                        'select' => false,
                    ]);
                @endphp
                <dd class="opt-season">{!! $drop_season->render() !!}</dd>
            {{-- Stat Category --}}
            <dt class="cat">Category:</dt>
                @php
                    $opt_cat = [];
                    foreach (FrameworkConfig::get('debear.setup.stats.details') as $cat_type => $cat_groups) {
                        foreach ($cat_groups as $cat_class => $cat_info) {
                            $opt_cat[] = [
                                'id' => "$cat_type::$cat_class",
                                'label' => ($cat_type == 'player' ? 'Individual' : 'Team') . ' ' . $cat_info['name'],
                            ];
                        }
                    }
                    $drop_cat = new Dropdown('filter_category', $opt_cat, [
                        'value' => "$type::$class",
                        'select' => false,
                    ]);
                @endphp
                <dd class="opt-category">{!! $drop_cat->render() !!}</dd>
            <dd class="btn"><button class="btn_green btn_small filter-load">Load</button></dd>
        </dl>
    </li>
</ul>
