@php
    // Setup the switcher
    $opt = [];
    foreach ($weeks as $week) {
        $opt[] = [
            'id' => $week->week_code,
            'attrib' => [
                'href' => $week->season . '/' . $week->week_code,
            ],
            'label' => $week->name,
        ];
    }
    $dropdown = new Dropdown('switcher', $opt, [
        'value' => $ranks->week_code,
        'select' => false,
    ]);
@endphp
{!! $dropdown->render() !!}
