@php
    $row_css = 'row-' . ($row->rank % 2);
@endphp
<dt class="{!! $row_css !!} rank">{!! $row->rank !!}</dt>
    {{-- Change since previous week --}}
    @isset($row->last_week)
        @php
            $diff = $row->last_week - $row->rank;
            $dir = (!$diff ? 'nc' : ($diff > 0 ? 'up' : 'down'));
        @endphp
        <dd class="{!! $row_css !!} change">
            <span class="hidden-m"><strong>Last Week:</strong> {!! $row->last_week !!}</span>
            <span class="icon_pos_{!! $dir !!}">{!! $diff ? abs($diff) : 'NC' !!}</span>
        </dd>
    @endisset
    {{-- Team --}}
    <dd class="{!! $row_css !!} team">
        <span class="logo hidden-m team-{!! FrameworkConfig::get('debear.section.code') !!}-narrow_large-{!! $row->team_id !!}"></span>
        <span class="logo hidden-t hidden-d team-{!! FrameworkConfig::get('debear.section.code') !!}-narrow_small-{!! $row->team_id !!}"></span>
        <h3><a href="{!! $row->team->link !!}">{!! $row->team->name !!}</a></h3>
    </dd>
    {{-- Games played --}}
    <dd class="{!! $row_css !!} schedule">
        <dl>
            <dt>This week:</dt>
                @forelse($row->games as $game)
                    <dd class="game">
                        <span class="score result-{!! $game->res !!}">
                            {!! substr(ucfirst($game->res), 0, 1) !!}
                            @if ($respond_css)
                                <span class="{!! $respond_css !!}">
                            @endif
                            {!! join(' &ndash; ', [
                                $game->res == 'win' ? $game->score_for : $game->score_agst,
                                $game->res == 'win' ? $game->score_agst : $game->score_for,
                            ]) !!}
                            @if ($respond_css)
                                </span>
                            @endif
                        </span>
                        {!! $game->venue !!}
                        <span class="opp icon team-{!! FrameworkConfig::get('debear.section.code') !!}-{!! $game->opp !!}">{!! $game->opp !!}</span>
                        @if ($game->status)
                            <span class="status {!! $respond_css !!}">({!! $game->status !!})</span>
                        @endif
                    </dd>
                @empty
                    <dd class="none">None</dd>
                @endforelse
        </dl>
    </dd>
    {{-- Any comments? --}}
    @isset($row->comment)
        <dd class="{!! $row_css !!} comment"><stong>Our Take:</stong> {!! $row->comment !!}</dd>
    @endisset
