@php
    $higher = $seeds->where('seed_ref', $series->higher->seed_ref);
    $lower = $seeds->where('seed_ref', $series->lower->seed_ref);
@endphp
@extends('skeleton.layouts.popup')

@section('content')

<h2 class="row-head">
    <span class="{!! $higher->team->logoCSS() !!}">{!! $higher->team->franchise !!}</span> <sup>{!! $higher->seed_full !!}</sup>
    -vs-
    <span class="{!! $lower->team->logoCSS() !!}">{!! $lower->team->franchise !!}</span> <sup>{!! $lower->seed_full !!}</sup>
</h2>

<dl class="games">
    @foreach($series->schedule as $game)
        @include('sports.major_league.playoffs.summary.game')
    @endforeach
    {{-- Series Summary --}}
    <dd class="summary row-1">
        {!! $series->series_summary !!}
    </dd>
</dl>

@endsection
