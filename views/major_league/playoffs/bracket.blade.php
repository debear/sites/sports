@php
    $conf_l = $conferences->first();
    $conf_r = $conferences->last();
    // Calculate the maximum matchups per-conference
    $matchups_round_max = 0;
    foreach (array_keys($config['series']) as $s) {
        $round_num = ($bracket->where('round_num', $s)->count() / 2);
        if ($round_num > $matchups_round_max) {
            $matchups_round_max = $round_num;
        }
    }
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! $title !!}</h1>

<ul class="inline_list bracket {!! isset($round_robin) ? 'with-round-robin' : '' !!} clearfix">
    {{-- Misc header info? --}}
    @isset ($config['info'])
        <li class="notice">
            <div class="box info icon_info"><span>{!! $config['info'] !!}</span></div>
        </li>
    @endisset
    {{-- First Conference --}}
    @include('sports.major_league.playoffs.bracket.conf', ['conf' => $conf_l, 'left' => true])
    {{-- Championship Round --}}
    @include('sports.major_league.playoffs.bracket.champ')
    {{-- Second Conference --}}
    @include('sports.major_league.playoffs.bracket.conf', ['conf' => $conf_r, 'right' => true])
    {{-- Round Robin? --}}
    @isset($round_robin)
        @include('sports.major_league.playoffs.bracket.round_robin', ['conf' => $conf_l, 'left' => true])
        @include('sports.major_league.playoffs.bracket.round_robin', ['conf' => $conf_r, 'right' => true])
    @endisset
</ul>

@endsection
