<dt class="row-head">
    Game {!! $loop->iteration !!}
    @if(!$game->isPreview())
        <span class="status">{!! $game->status_full !!}</span>
    @endif
</dt>
    {{-- Date/Time --}}
    <dd class="game_date row-1">
        <span class="date">{!! $game->date_short !!}</span>
        <span class="time">{!! $game->start_time !!}</span>
    </dd>
    {{-- Game Summary --}}
    <dd class="game clearfix">
        <ul class="inline_list">
            <li class="visitor {!! $game->isComplete() ? ($game->visitor_score > $game->home_score ? 'winner' : 'loser') : '' !!} {!! $game->visitor->logoCSS('small') !!}">
                @isset($game->visitor_score)
                    <span class="score">{!! $game->visitor_score !!}</span>
                @endisset
            </li>
            <li class="at">@</li>
            <li class="home {!! $game->isComplete() ? ($game->visitor_score > $game->home_score ? 'loser' : 'winner') : '' !!} {!! $game->home->logoRightCSS('small') !!}">
                @isset($game->home_score)
                    <span class="score">{!! $game->home_score !!}</span>
                @endisset
            </li>
            @if ($game->isComplete())
                <li class="links">
                    @php
                        $game_link = $game->link;
                    @endphp
                    @foreach (FrameworkConfig::get('debear.setup.game.tabs') as $link)
                        <a class="icon_game_{!! $link['icon'] !!} link-{!! count(FrameworkConfig::get('debear.setup.game.tabs')) - $loop->iteration !!}" href="{!! $game_link !!}{!! $link['link'] ? "#{$link['link']}" : '' !!}"><em>{!! is_string($link['label']) ? $link['label'] : $link['label'][$game->game_type] !!}</em></a>
                    @endforeach
                </li>
            @endif
        </ul>
    </dd>
    {{-- Game Info --}}
    <dd class="info clearfix">
        <ul class="inline_list">
            @if ($game->isComplete())
                @include("sports.$sport.widgets.game.info")
            @elseif ($game->isPreview())
                @includeIf("sports.$sport.widgets.game.preview")
            @endif
        </ul>
    </dd>
