@php
    $higher = $seeds->where('seed_ref', "{$m->higher_conf_id}:{$m->higher_seed}");
    $lower = $seeds->where('seed_ref', "{$m->lower_conf_id}:{$m->lower_seed}");

    // Did a team win?
    $winner = $loser = 'no-one';
    if (isset($m->game_id)) {
        // By single game score.
        $winner = ($m->higher_score > $m->lower_score ? 'higher' : 'lower');
        $loser = ($m->higher_score < $m->lower_score ? 'higher' : 'lower');
    } elseif ($m->higher_games == $config['series'][$m->round_num]['wins']) {
        // Higher, by games.
        $winner = 'higher';
        $loser = 'lower';
    } elseif ($m->lower_games == $config['series'][$m->round_num]['wins']) {
        // Lower, by games.
        $winner = 'lower';
        $loser = 'higher';
    }
    // Hoverable?
    $hoverable = ($m->games_sched || isset($m->game_id));
@endphp
<fieldset class="matchup {!! $hoverable ? 'hover' : '' !!} {!! FrameworkConfig::get('debear.setup.box') !!}">
    <div class="team {!! $higher->team->logoCSS() !!} {!! $winner == 'higher' ? 'winner' : ($loser == 'higher' ? 'loser' : '') !!}">
        <span class="team">{!! $higher->team_id !!} <sup>{!! $higher->seed_full !!}</sup></span>
        <span class="score">{!! $m->higher_score ?? $m->higher_games !!}</span>
    </div>
    <div class="team {!! $lower?->team?->logoCSS() ?? '' !!} {!! $winner == 'lower' ? 'winner' : ($loser == 'lower' ? 'loser' : '') !!}">
        <span class="team">{!! $lower->team_id ?? '<em>TBD</em>' !!} <sup>{!! $lower->seed_full !!}</sup></span>
        <span class="score">{!! $m->lower_score ?? $m->lower_games !!}</span>
    </div>
    @if ($hoverable)
        <div class="link row-head">
            @isset($m->game_id)
                <span class="icon_game_boxscore" data-link="{!! $m->game->link !!}">Boxscore</span>
            @else
                <span class="icon_playoffs" data-link="{!! $m->link !!}" data-modal="series">Summary</span>
            @endisset
        </div>
    @endif
</fieldset>
