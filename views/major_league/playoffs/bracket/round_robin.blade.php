@php
    $sport_endpoint = FrameworkConfig::get('debear.section.entrypoint');
    $sport_code = FrameworkConfig::get('debear.section.code');
    $seeds_conf = $seeds->where('conf_id', $conf->grouping_id);
    $obj_conf = $seeds_conf->where('seed', 1)->team->conference;
    $is_left = (isset($left) && $left);
    $all_teams = $round_robin->where('conf_id', $conf->grouping_id);
@endphp
<li class="round_robin conf-{!! $is_left ? 'left' : 'right' !!}">
    <h3 class="{!! $obj_conf->rowCSS() !!}">Round Robin</h3>
    <dl>
        <dt class="conf {!! $obj_conf->rowCSS() !!} {!! $obj_conf->logoCSS('narrow-small') !!}"></dt>
        @include("sports.$sport_code.widgets.playoffs.round_robin.head")
        @foreach ($all_teams as $team)
            @php
                $row_css = 'row-' . ($loop->iteration % 2);
                $team_info = $seeds_conf->where('seed', $team->seed)->team;
            @endphp
            <dt class="seed {!! $obj_conf->rowCSS() !!}">{!! $team->seed !!}</dt>
                <dd class="{!! $row_css !!} team {!! $team_info->logoCSS() !!}">
                    <span class="abbrev">{!! $team->team_id !!}</span>
                    <span class="name">{!! $team_info->name !!}</span>
                    <sup>{!! $team->pos_conf !!}</sup>
                </dd>
                @include("sports.$sport_code.widgets.playoffs.round_robin.team")
        @endforeach
        <dd class="link row-{!! ($all_teams->count() + 1) % 2 !!}"><a href="/{!! $sport_endpoint !!}/playoffs/{!! FrameworkConfig::get('debear.setup.season.viewing') !!}/round-robin-{!! $conf->grouping_id !!}" data-modal="series">Schedule &amp; Results</a></dd>
    </dl>
</li>
