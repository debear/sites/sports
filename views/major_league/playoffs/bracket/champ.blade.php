@php
    $round_nums = array_keys($config['series']);
    $num_rounds = count($round_nums);
    $m = $bracket->where('round_num', max($round_nums));
    // Championship header?
    if ($config['divisional']) {
        $champ_header = 'champ-div';
    } elseif ($seeds->where('has_bye', 1)->count()) {
        $champ_header = 'champ-byes';
    } else {
        $champ_header = 'champ';
    }
@endphp
<li class="{!! $champ_header !!} rounds-{!! $num_rounds !!} grid-1-{!! (2 * count($config['series'])) - 1 !!} num-1-{!! $matchups_round_max !!}">
    @if ($m->isset())
        <h3 class="row-head">{!! $champ_title !!}</h3>
        @include('sports.major_league.playoffs.bracket.matchup')
    @endif
    {{-- Championship/Playoff Logo --}}
    <div class="logo {!! !$m->isset() ? 'no-matchup' : '' !!}">
        <div class="large playoffs-{!! FrameworkConfig::get('debear.section.code') !!}-large-0000 playoffs-{!! FrameworkConfig::get('debear.section.code') !!}-large-{!! FrameworkConfig::get('debear.setup.season.viewing') !!}"></div>
        @isset($round_robin)
            <div class="medium playoffs-{!! FrameworkConfig::get('debear.section.code') !!}-medium-0000 playoffs-{!! FrameworkConfig::get('debear.section.code') !!}-medium-{!! FrameworkConfig::get('debear.setup.season.viewing') !!}"></div>
        @endisset
    </div>
</li>
