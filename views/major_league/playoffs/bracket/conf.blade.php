@php
    // The matchups for this conference
    $matchups = $bracket->where('higher_conf_id', $conf->grouping_id);
    // Reverse display when viewing the right hand conference
    $reverse = (isset($right) && $right);
    // Order the series
    $round_nums = array_keys($config['series']);
    $num_rounds = count($round_nums);
    $series = (!isset($include_all) ? array_slice($round_nums, 0, -1) : $round_nums);
    if ($reverse) {
        $series = array_reverse($series);
    }
    // How many total series?
    $series_num_conf = count($series);
    $series_num_tot = (!isset($include_all) ? (2 * $series_num_conf) + 1 : $series_num_conf);
    // Determine the teams with a bye
    $seeds_conf = $seeds->where('conf_id', $conf->grouping_id);
    $byes = $seeds_conf->where('has_bye', 1);
    // Appropriate seeds for conf/div titles
    $obj_conf = $seeds_conf->where('seed', 1)->team->conference;
    if ($config['divisional']) {
        $initial_matchups = $matchups->where('round_num', 1);
        $obj_div_t = $seeds_conf->where('seed', $initial_matchups->first()->higher_seed)->team->division;
        $obj_div_b = $seeds_conf->where('seed', $initial_matchups->last()->higher_seed)->team->division;
        $series_num_div = $series_num_conf - 1;
    }
@endphp

<li class="grid-{!! $series_num_conf !!}-{!! $series_num_tot !!} rounds-{!! $num_rounds !!} {!! !$reverse ? 'conf-left' : 'conf-right' !!}">
    {{-- Header --}}
    @if ($obj_conf->name)
        <h3 class="conf {!! $obj_conf->rowCSS() !!} {!! $obj_conf->logoCSS('narrow-small') !!} {!! $obj_conf->logoRightCSS('narrow-small') !!}">
            <span class="full">{!! $obj_conf->name_full !!}</span>
            <span class="short">{!! $obj_conf->name !!}</span>
        </h3>
    @endif

    {{-- Division? --}}
    @if ($config['divisional'])
        <h3 class="div-top grid-{!! $series_num_div !!}-{!! $series_num_conf !!} {!! $obj_conf->rowCSS() !!}">
            <span class="full">{!! $obj_div_t->name_full !!}</span>
            <span class="short">{!! $obj_div_t->name !!}</span>
        </h3>
    @endif

    {{-- Byes? --}}
    @if ($byes->isset())
        <div class="byes">
            <dl>
                <dt>Byes:</dt>
                    @foreach ($byes as $bye)
                        <dd class="icon {!! $bye->team->logoCSS() !!}">
                            <span class="full">{!! $bye->team->franchise !!}</span>
                            <span class="short">{!! $bye->team_id !!}</span>
                            <sup>{!! $bye->seed_full !!}</sup>
                        </dd>
                    @endforeach
            </dl>
        </div>
    @endif

    {{-- Bracket --}}
    <ul class="inline_list clearfix">
        @foreach ($series as $s)
            @php
                $series_matchups = $matchups->where('round_num', $s);
            @endphp
            <li class="grid-1-{!! $series_num_conf !!} num-{!! $series_matchups->count() !!}-{!! $matchups_round_max !!}">
                @foreach ($series_matchups as $m)
                    @include('sports.major_league.playoffs.bracket.matchup')
                @endforeach
            </li>
        @endforeach
    </ul>

    {{-- Division? --}}
    @if ($config['divisional'] && $obj_div_t->grouping_id != $obj_div_b->grouping_id)
        <h3 class="div-bot grid-{!! $series_num_div !!}-{!! $series_num_conf !!} {!! $obj_conf->rowCSS() !!}">
            <span class="full">{!! $obj_div_b->name_full !!}</span>
            <span class="short">{!! $obj_div_b->name !!}</span>
        </h3>
    @endif
</li>
