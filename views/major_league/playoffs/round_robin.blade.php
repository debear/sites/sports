@extends('skeleton.layouts.popup')

@section('content')

<h2 class="conf {!! $conf->rowCSS() !!} {!! $conf->logoCSS('small') !!} {!! $conf->logoRightCSS('small') !!}">{!! $conf->name_full !!}</h2>

<dl class="games">
    @foreach($schedule as $game)
        @include('sports.major_league.playoffs.summary.game')
    @endforeach
</dl>

@endsection
