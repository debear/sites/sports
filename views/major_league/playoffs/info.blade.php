@php
    // Some grammar rules.
    if (substr($champ_title, -1) == 's') {
        $champ_title = "the $champ_title";
    }
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! $title !!}</h1>

<fieldset class="info icon_info preseason">
    {!! $config['info'] !!}
</fieldset>

@endsection
