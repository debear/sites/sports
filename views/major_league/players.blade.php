@extends('skeleton.layouts.html')

@section('content')

<h1>{!! FrameworkConfig::get('debear.names.section') !!} Players</h1>

<ul class="inline_list wrapper clearfix">
    {{-- By Team --}}
    <li class="{!! $sport !!}_box ele teams clearfix">
        <h3 class="row-head">By Team</h3>
        @foreach ($groupings as $conf)
            <dl class="conf">
                @isset ($conf->name_full)
                    <dt class="{!! $conf->rowCSS() !!} {!! $conf->logoCSS('narrow-small') !!}">{!! $conf->name !!}</dt>
                @endisset
                @foreach ($conf->teams as $team)
                    <dd class="row-{!! $loop->index % 2 !!} {!! $team->logoCSS() !!} icon"><a href="{!! $team->link !!}#roster">{!! $team->name !!}</a></dd>
                @endforeach
            </dl>
        @endforeach
    </li>

    {{-- Search --}}
    <li class="ele search">
        <dl class="{!! $sport !!}_box clearfix">
            <dt class="row-head">Search By Name</dt>
            <dt class="row-1">Player&#39;s Name:</dt>
                <dd class="row-0 name"><input type="text" class="textbox"></dd>
                <dd class="row-0 button"><button class="btn_small btn_green">Search</button></dd>
                <dd class="results hidden"></dd>
        </dl>
    </li>

    {{-- Birthdays --}}
    @if ($birthdays->isset())
        @php
            $year = substr(Time::object()->getCurDate(), 0, 4);
        @endphp
        <li class="{!! $sport !!}_box ele birthdays">
            <h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>
            <div>
                <ul class="inline_list">
                    @foreach ($birthdays as $bday)
                        <li class="row-{!! $loop->index % 2 !!}"><a href="{!! $bday->link !!}">{!! $bday->name !!}</a> ({!! $year - substr($bday->dob, 0, 4) !!})</li>
                    @endforeach
                </ul>
            </div>
        </li>
    @endif
</ul>

@endsection
