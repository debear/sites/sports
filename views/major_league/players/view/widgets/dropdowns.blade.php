@php
    $dropdowns = [];
    // Season-by-season dropdown?
    if (FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.season") || FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.career")) {
        $season_types = ['regular' => 'Regular Season', 'playoff' => 'Playoffs'];
        $applicable_types = array_fill_keys(array_keys($season_types), false);
        $opt = [];
        foreach ($player->seasons as $s) {
            if (strpos($s, ':') !== false) {
                list ($y, $t) = explode(':', $s);
                $applicable_types[$t] = true;
                $opt[] = [
                    'id' => $s,
                    'label' => \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($y) . " {$season_types[$t]}",
                ];
            } else {
                $opt[] = [
                    'id' => $s,
                    'label' => \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($s),
                ];
            }
            if (!isset($default)) {
                $default = $s;
            }
        }
        // Prepending a career option?
        if (FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.career")) {
            foreach (array_reverse($season_types) as $type => $label) {
                if (!$applicable_types[$type]) {
                    continue;
                }
                array_unshift($opt, [
                    'id' => "0000:$type",
                    'label' => "Career $label"
                ]);
            }
        }
        // Build the dropdown
        $dropdowns['seasons'] = new Dropdown("season-$tab", $opt, [
            'value' => $default,
            'select' => false,
        ]);
    }
    // Stat group dropdown?
    if (FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.stat_group")) {
        $opt = [];
        foreach (FrameworkConfig::get('debear.setup.stats.details.player') as $group => $group_config) {
            $opt[] = [
                'id' => $group,
                'label' => $group_config['name'],
            ];
        }
        $dropdowns['groups'] = new Dropdown("group-$tab", $opt, [
            'value' => $player->group,
            'select' => false,
        ]);
    }
    // NFL-specific: Pos group dropdown? (Replaces the stat_group dropdown with one of the same name)
    if (FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.pos_group")) {
        $opt = [];
        foreach (array_unique([$player->group, 'defense', 'returns']) as $group) {
            $opt[] = [
                'id' => $group,
                'label' => FrameworkConfig::get("debear.setup.players.groups.$group.name"),
            ];
        }
        $dropdowns['groups'] = new Dropdown("group-$tab", $opt, [
            'value' => $player->group,
            'select' => false,
        ]);
    }
    // MLB-specific: Batter/Pitcher Hands
    if (FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.hands")) {
        $dropdowns['hands'] = new Dropdown("hands-$tab", [
            ['id' => 'CMB-CMB', 'label' => 'All v All'],
            ['id' => 'L-CMB', 'label' => 'LHB v All'],
            ['id' => 'R-CMB', 'label' => 'RHB v All'],
            ['id' => 'CMB-L', 'label' => 'All v LHP'],
            ['id' => 'L-L', 'label' => 'LHB v LHP'],
            ['id' => 'R-L', 'label' => 'RHB v LHP'],
            ['id' => 'CMB-R', 'label' => 'All v RHP'],
            ['id' => 'L-R', 'label' => 'LHB v RHP'],
            ['id' => 'R-R', 'label' => 'RHB v RHP'],
        ], [
            'value' => 'CMB-CMB',
            'select' => false,
        ]);
    }
    // AHL/NHL-specific: Heatmap Types
    if (FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.heatmaps")) {
        $opt = [];
        foreach (FrameworkConfig::get("debear.setup.stats.details.player.{$player->group}.heatmaps") as $type) {
            $opt[] = ['id' => $type, 'label' => ucfirst($type)];
        }
        $dropdowns['heatmaps'] = new Dropdown("heatmap-$tab", $opt, [
            'value' => $opt[0]['id'],
            'select' => false,
        ]);
    }
@endphp
<div class="tab-dropdown" data-num="{!! count($dropdowns) !!}">
    @foreach($dropdowns as $k => $d)
        <div class="{!! $k !!}" data-default="{!! $d->getValue() !!}">{!! $d->render() !!}</div>
    @endforeach
</div>
<div class="tab-content"></div>
@include('sports.major_league.players.view.widgets.loading')
