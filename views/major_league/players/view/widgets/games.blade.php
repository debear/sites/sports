@php
    $columns = $data->getGameStatMeta();
@endphp
@if ($recent)
    <h3 class="games scrolltable-game-{!! $data->stat_group !!} row-head">Last {!! Format::pluralise($data->count(), ' Game') !!}</h3>
@endif
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game scrolltable-game-{!! $data->stat_group !!}" data-default="0" data-total="{!! count($columns) !!}" data-current="{!! array_key_first($columns) !!}">
    <main>
        {{-- Header --}}
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        {{-- Games --}}
        @foreach ($data as $game)
            @php
                $row_css = 'row-' . ($loop->iteration % 2);
                $is_home = ($game->team_id == $game->schedule->home_id);
                $game->schedule->team_type = ($is_home ? 'home' : 'visitor');
                $game->schedule->team_opp = ($is_home ? 'visitor' : 'home');
                $opp = "{$game->schedule->team_opp}_id";
                $opp_css = \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($game->schedule->$opp);
            @endphp
            <row>
                <cell class="row-head date">{!! $game->schedule->date_short !!}</cell>
                <cell class="{!! $row_css !!} opp">
                    {!! $is_home ? 'v' : '@' !!}<span class="{!! $opp_css !!}">{!! $game->schedule->$opp !!}</span>
                </cell>
                <cell class="{!! $row_css !!} score">
                    <span class="{!! $game->schedule->resultCSS() !!}">{!! $game->schedule->resultCode() !!} {!! $game->schedule->resultScore() !!}</span>
                    <a class="icon_game_boxscore" href="{!! $game->schedule->link !!}"></a>
                </cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top>
                @foreach ($columns as $col => $def)
                    <cell class="row-head {!! $col !!}" title="{!! $def['f'] !!}">{!! $def['s'] !!}</cell>
                @endforeach
            </top>
            {{-- Stats --}}
            @foreach ($data as $game)
                @php
                    $row_css = 'row-' . ($loop->iteration % 2);
                @endphp
                <row>
                    @foreach (array_keys($columns) as $col)
                        <cell class="{!! $row_css !!} {!! $col !!}">{!! $game->format($col) !!}</cell>
                    @endforeach
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>
