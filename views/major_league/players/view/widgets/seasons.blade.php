@php
    $columns = $data->getSeasonStatMeta();
@endphp
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season scrolltable-season-{!! $data->stat_group !!}" data-default="0" data-total="{!! count($columns) !!}" data-current="{!! array_key_first($columns) !!}">
    <main>
        {{-- Header --}}
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        {{-- Seasons --}}
        @foreach ($data as $season)
            @php
                $row_css = 'row-' . ($loop->iteration % 2);
            @endphp
            <row>
                <cell class="row-head season">{!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($season->season) !!}</cell>
                <cell class="row-head season-type">{!! ucfirst($season->season_type) !!}</cell>
                <cell class="{!! $row_css !!} team"><span class="{!! \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($season->team_id) !!}">{!! $season->team_id !!}</span></cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top>
                @foreach ($columns as $col => $def)
                    <cell class="row-head {!! $col !!}" title="{!! $def['f'] !!}">{!! $def['s'] !!}</cell>
                @endforeach
            </top>
            {{-- Stats --}}
            @foreach ($data as $season)
                @php
                    $row_css = 'row-' . ($loop->iteration % 2);
                @endphp
                <row>
                    @foreach (array_keys($columns) as $col)
                        <cell class="{!! $row_css !!} {!! $col !!}">{!! $season->format($col) !!}</cell>
                    @endforeach
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>
