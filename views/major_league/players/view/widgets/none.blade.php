<fieldset class="info icon_info not-played">
    <span class="hidden-m">{!! $player->name !!} has</span><span class="hidden-t hidden-d">Has</span> not yet appeared in an {!! FrameworkConfig::get('debear.names.section') !!} game.
</fieldset>
