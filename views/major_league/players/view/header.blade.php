{{-- Name, Number, Pos, Team --}}
@php
    $header = [$player->name];
    // Roster Status?
    if ($player->lastRoster->is_latest) {
        $status_raw = ($player->lastRoster->player_status ?? 'active');
    } elseif ($player->lastRoster->season == FrameworkConfig::get('debear.setup.season.viewing')) {
        $status_raw = 'na';
    }
    if (isset($status_raw) && $status_raw != 'active') {
        $status = FrameworkConfig::get("debear.setup.players.status.$status_raw");
        $header[] = "<abbrev class=\"$status_raw\" title=\"{$status['long']}\">{$status['short']}</abbrev>";
    }
    // Jersey?
    if (isset($player->lastRoster->jersey)) {
        $header[] = "<span class=\"hidden-m jersey\">#{$player->lastRoster->jersey}</span>";
    }
    // Position?
    if (isset($player->lastRoster->pos)) {
        $header[] = "<span class=\"hidden-m pos\">{$player->lastRoster->pos}</span>";
    }
    // Team logo?
    $css = [];
    if (isset($player->lastRoster->team)) {
        $css[] = $player->lastRoster->team->logoRightCSS('narrow_large');
    }
@endphp
<h1 class="{!! join($css) !!}">{!! join($header) !!}</h1>
<div class="header clearfix">
    {{-- Mugshot --}}
    {!! $player->image('medium') !!}
    {{-- Stat Summary --}}
    @isset($player->statSummary)
        <ul class="inline_list summary">
            <li class="row-head">{!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($player->getLatestSeason()) !!} Regular Season:</li>
            @foreach ($player->statSummary as $stat)
                <li class="stat grid-1-{!! count($player->statSummary) !!}">
                    <dl class="{!! $sport !!}_box">
                        <dt class="row-head">{!! $stat['label'] !!}</dt>
                            <dd class="row-1 value">{!! $stat['value'] !!}</dd>
                            <dd class="row-0 rank">{!! $stat['rank'] !!}</dd>
                    </dl>
                </li>
            @endforeach
        </ul>
    @endisset
    {{-- Details --}}
    <dl class="details">
        @isset ($player->pronunciation)
            <dt class="pronounced">Pronounced:</dt>
                <dd class="pronounced">{!! $player->pronunciation !!}</dd>
        @endisset
        <dt class="height">Height:</dt>
            <dd class="height">{!! $player->height_fmt ?: '<em>Unknown</em>' !!}</dd>
        <dt class="weight">Weight:</dt>
            <dd class="weight">{!! $player->weight_fmt ?: '<em>Unknown</em>' !!}</dd>
        @if (FrameworkConfig::get('debear.setup.players.header_custom') !== null)
            <dt class="custom">{!! FrameworkConfig::get('debear.setup.players.header_custom') !!}:</dt>
                <dd class="custom">{!! $player->header_custom ?: '<em>Unknown</em>' !!}</dd>
        @endif
        <dt class="dob">Born:</dt>
            <dd class="dob">{!! $player->dob_fmt ?: '<em>Unknown</em>' !!}</dd>
        <dt class="pob">Place of Birth:</dt>
            <dd class="pob">{!! $player->birthplace_fmt ?: '<em>Unknown</em>' !!}</dd>
        @if (isset($player->draft) && $player->draft->isset())
            <dt class="draft">Drafted:</dt>
                <dd class="draft">{!! $player->draft->summary !!}</dd>
        @endif
        @isset ($player->high_school)
            <dt class="high_school">High School:</dt>
                <dd class="high_school">{!! $player->high_school !!}</dd>
        @endisset
        @isset ($player->college)
            <dt class="college">College:</dt>
                <dd class="college">{!! $player->college !!}</dd>
        @endisset
    </dl>
    {{-- Not Played Banner --}}
    @if (!$player->careerGP)
        <fieldset class="info icon_info not-played">
            <span class="hidden-m">{!! $player->name !!} has</span><span class="hidden-t hidden-d">Has</span> not yet appeared in an {!! FrameworkConfig::get('debear.names.section') !!} game.
        </fieldset>
    @endif
</div>
