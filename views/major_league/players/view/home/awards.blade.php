@php
    $found = [];
@endphp
<dl class="{!! $sport !!}_box awards clearfix">
    <dt class="row-head">Award History</dt>
    @foreach ($data as $row)
        @php
            $class = $season = '';
            if (!isset($found[$row->season]) || !$found[$row->season]) {
                $found[$row->season] = true;
                $class = (isset($row->team_id) ? \DeBear\ORM\Sports\MajorLeague\Team::buildLogoCSS($row->team_id) : "misc-$sport-" . strtoupper($sport));
                $season = \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($row->season);
            }
        @endphp
        <dt class="{!! $class !!}">{!! $season !!}</dt>
            <dd>{!! $row->name . (isset($row->pos) ? " ({$row->pos})" : '') !!}</dd>
    @endforeach
</dl>
