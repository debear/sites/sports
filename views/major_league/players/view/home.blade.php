{{-- Season Stats --}}
@includeFirst(["sports.$sport.players.view.widgets.seasons", 'sports.major_league.players.view.widgets.seasons'], [
    'data' => $player->seasonStats,
])
{{-- Recent Games --}}
@includeFirst(["sports.$sport.players.view.widgets.games", 'sports.major_league.players.view.widgets.games'], [
    'data' => $player->recentGames,
    'recent' => true,
])
{{-- Awards --}}
@includeWhen($player->awards->count(), 'sports.major_league.players.view.home.awards', [
    'data' => $player->awards,
])
{{-- NFL Specific: Injury News (though could be sport agnostic if info available) --}}
@includeWhen(isset($player->injuryLatest) && $player->injuryLatest->count(), 'sports.nfl.players.view.home.injuries', [
    'data' => $player->injuryLatest,
])
{{-- MLB Specific: Repertoire --}}
@includeWhen(isset($player->repertoire) && $player->repertoire->count(), 'sports.mlb.players.view.home.repertoire', [
    'data' => $player->repertoire,
])
