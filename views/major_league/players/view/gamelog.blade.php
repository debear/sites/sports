{{-- Stat switcher --}}
@php
    $opt = [];
    foreach (array_keys(FrameworkConfig::get('debear.setup.stats.details.player')) as $alt_group) {
        $opt[] = "data-$alt_group=\"" . ($player->seasonGames->sum("can_$alt_group") ? 'true' : 'false') . '"';
    }
@endphp
<div class="hidden dropdown-filters" data-dropdown="groups" {!! join(' ', $opt) !!}></div>
{{-- Table --}}
@includeFirst(["sports.$sport.players.view.widgets.games", 'sports.major_league.players.view.widgets.games'], [
    'data' => $player->seasonGames,
    'recent' => false,
])
