@extends('skeleton.layouts.html')

@section('content')

{{-- Header --}}
@include('sports.major_league.players.view.header')

{{-- Detailed Tabs, if stats available --}}
@if($player->careerGP)
    @include('skeleton.widgets.subnav')
    @foreach (array_keys($subnav['list']) as $tab)
        <div class="subnav-{!! $tab !!} subnav-tab {!! $tab != $subnav['default'] ? 'hidden' : '' !!} clearfix">
            @if ($loop->first)
                @includeFirst(["sports.$sport.players.view.$tab", "sports.major_league.players.view.$tab"])
            @elseif (FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns"))
                @include('sports.major_league.players.view.widgets.dropdowns')
            @else
                @include('sports.major_league.players.view.widgets.loading')
            @endif
        </div>
    @endforeach
@endif

@endsection
