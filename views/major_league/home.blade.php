@extends('skeleton.layouts.html')

@section('content')

<ul class="inline_list grid home">
    <li class="grid-cell grid-3-4 grid-t-1-1 grid-m-1-1">
        {{-- News --}}
        <ul class="inline_list grid">
            @include('sports.major_league.home.news')
        </ul>
    </li>

    <li class="grid-cell grid-1-4 grid-t-1-1 grid-m-1-1 clearfix">
        {{-- Standings / Playoffs --}}
        @includeWhen(!isset($bracket), 'sports.major_league.home.standings')
        @includeWhen(isset($bracket), 'sports.major_league.home.bracket')

        {{-- Stat Leaders --}}
        @includeWhen(count(array_filter($leaders)) > 0, 'sports.major_league.home.leaders')

        {{-- Power Ranks --}}
        @includeWhen(isset($power_ranks->teams), 'sports.major_league.home.power_ranks')
    </li>
</ul>

@endsection
