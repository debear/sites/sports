@php
    $recent = $award->recent;
    $detailed = isset($recent->player);
@endphp

<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="{!! $sport !!}_box award clearfix">
        {{-- Award Details --}}
        <h3 class="row-head">{!! $award->name !!}</h3>

        {{-- Mugshot --}}
        <div class="mugshot-small">
            @if ($detailed)
                {!! $recent->player->image($size) !!}
            @else
                {!! \DeBear\ORM\Sports\MajorLeague\Player::silhouette($size) !!}
            @endif
        </div>

        {{-- Description --}}
        <p class="descrip">{!! $award->description !!}</p>

        {{-- Last Winner --}}
        <p class="winner">
            <strong>{!! \DeBear\Helpers\Sports\MajorLeague\Controller::seasonTitle($recent->season ?? $award->season) !!} Winner:</strong>
            @if ($detailed)
                @if ($recent->team->isset())
                    <span class="{!! $recent->team->logoCSS() !!}">
                @endif
                <a href="{!! $recent->player->link !!}">{!! $recent->player->name !!}</a>
                @if ($recent->team->isset())
                    </span>
                @endif
            @elseif ($recent->isset())
                {!! $recent->player_name !!}
            @else
                <em>Not Awarded</em>
            @endif

            @isset ($recent->pos)
                ({!! $recent->pos !!})
            @endisset
        </p>

        {{-- Link --}}
        <p class="link"><a class="all-time" data-link="{!! $award->link !!}">All-Time Winners</a></p>
    </fieldset>
</li>
