<ul class="inline_list grid">
    @foreach($meetings_all->where('completed_heats', '>', 0)->sortBy('round_order') as $meeting)
        <li class="grid-cell grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1">
            <fieldset class="section meeting">
                <h3 class="row_head flag flag48_{!! $meeting->flag() !!}">{!! $meeting->name_full !!}</h3>
                <dl class="inline_list participants">
                    @php
                        $round = $meeting->round;
                        $race_part = $team->list->filter(function ($rider) use ($round, $team) {
                            return isset($rider['race_list'][$round])
                                && isset($team->roles[$rider['race_list'][$round]]);
                        })->sort(function ($a, $b) use ($round) {
                            return $a['race_list'][$round] <=> $b['race_list'][$round];
                        });
                        $last_type = '';
                    @endphp
                    @foreach ($race_part as $rider)
                        @php
                            $result = $rider->results->where('round', $round);
                            $rider_type = $team->roles[$result->bib_no];
                        @endphp
                        @if ($rider_type != $last_type)
                        <dt class="type">{!! $rider_type !!}</dt>
                        @endif
                            @php
                                $last_type = $rider_type;
                                $row_css = 'row_' . ($loop->iteration % 2);
                                $row_sec = ($loop->index ? 'secondary' : '');
                            @endphp
                            <dt class="num row_head {!! $row_sec !!}">#{!! $result->bib_no !!}</dt>
                            <dd class="participant {!! $row_css !!} {!! $row_sec !!} {!! !isset($result->pos) ? 'no-' : '' !!}score">{!! $rider->nameLinkFlag() !!}</dd>
                            @isset($result->pos)
                                <dd class="pts {!! $row_css !!} {!! $row_sec !!}">{!! $result->formatResult() !!}</dd>
                            @endisset
                    @endforeach
                </dl>
            </fieldset>
        </li>
    @endforeach
</ul>
