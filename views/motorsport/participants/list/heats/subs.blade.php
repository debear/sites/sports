g<ul class="inline_list grid">
    @foreach($team->list as $rider)
        <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <fieldset class="section subs profile">
                <h3 class="row_head">{!! $rider->nameFlag() !!}</h3>
                @if ($titles = $rider->championships())
                    <h4 class="champs podium-1">{!! count($titles) !!}x World Champion ({!! Format::groupRanges($titles) !!})</h4>
                @endif
                {!! $rider->image() !!}
                <h4>Meetings Ridden</h4>
                <ul class="inline_list">
                    @php
                        $meetings = $meetings_all->whereIn('round_order', $rider->round_list)->sortBy('round_order');
                    @endphp
                    @foreach ($meetings as $meeting)
                        @php
                            $result = $rider->results->where('round', $meeting->round);
                        @endphp
                        @if (!$result->is_wildcard && !$result->is_track_reserve)
                            <li>
                                {!! $meeting->nameShortLinkFlag() !!} &ndash; {!! $result->formatResult() !!}
                            </li>
                        @endif
                    @endforeach
                </ul>
                <a class="link" href="{!! $rider->link() !!}">Rider Profile &raquo;</a>
            </fieldset>
        </li>
    @endforeach
</ul>
