<ul class="inline_list grid">
    @foreach($team->list as $rider)
        <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <fieldset class="section perm profile">
                <div class="row_head num">{!! $rider->ranking !!}</div>
                <h3 class="row_head">{!! $rider->nameFlag() !!}</h3>
                @if ($titles = $rider->championships())
                    <h4 class="champs podium-1">{!! count($titles) !!}x World Champion ({!! Format::groupRanges($titles) !!})</h4>
                @endif
                {!! $rider->image() !!}
                <dl class="bio inline_list">
                    @if ($born = $rider->born)
                        <dt>Born:</dt>
                            <dd class="born clear" title="{!! strip_tags($born) !!}">{!! $born !!}</dd>
                    @endif
                    <dt>Experience:</dt>
                        <dd>{!! $rider->experience() !!} season</dd>
                    <dt>Career Victories:</dt>
                        <dd>{!! $rider->gpVictories() !!}</dd>
                    <dt>Career Podiums:</dt>
                        <dd>{!! $rider->gpPodiums() !!}</dd>
                    <dt>Career Points:</dt>
                        <dd>{!! $rider->gpPoints() !!}</dd>
                </dl>
                <a class="link" href="{!! $rider->link() !!}">Rider Profile &raquo;</a>
            </fieldset>
        </li>
    @endforeach
</ul>
