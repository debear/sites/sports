@php
    $h_tag = ($loop->first ? 'h1' : 'h2');
    $meetings_all = \DeBear\Helpers\Sports\Motorsport\Header::getCalendar();
@endphp
{{-- Palm any detail off to the specific template --}}
@if ($team->list->count())
    <{!! $h_tag !!}>{!! $team->name !!}</{!! $h_tag !!}>
    @include('sports.motorsport.participants.list.heats.' . $team->type)
@endif
