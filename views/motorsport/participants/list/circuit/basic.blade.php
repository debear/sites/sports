@if ($loop->first)
    <h1>{!! $title !!}</h1>
    <ul class="inline_list grid">
@endif

<li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="section">
        <h2>{!! $team->team_name !!}</h2>
        @foreach ($team->riders as $participant)
            <ul class="inline_list clearfix rider profile">
                <li class="row_head num">{!! $participant->bike_no !!}</li>
                <li class="row_head h3">{!! $participant->nameFlag() !!}</li>
                @if ($titles = $participant->championships())
                    <li class="h4 champs podium-1">{!! count($titles) !!}x World Champion ({!! Format::groupRanges($titles) !!})</li>
                @endif
                <li class="mugshot mugshot-large">
                    {!! $participant->image() !!}
                </li>
                <li class="bio">
                    <dl class="inline_list">
                        @if ($born = $participant->born)
                            <dt>Born:</dt>
                                <dd class="born clear" title="{!! strip_tags($born) !!}">{!! $born !!}</dd>
                        @endif
                        <dt>Experience:</dt>
                            <dd>{!! $participant->experience() !!} season</dd>
                        <dt>Career Victories:</dt>
                            <dd>{!! $participant->gpVictories() !!}</dd>
                        <dt>Career Podiums:</dt>
                            <dd>{!! $participant->gpPodiums() !!}</dd>
                        <dt>Career Pole Positions:</dt>
                            <dd>{!! $participant->gpPoles() !!}</dd>
                        <dt>Career Fastest Laps:</dt>
                            <dd>{!! $participant->gpFastestLaps() !!}</dd>
                        <dt>Career Points:</dt>
                            <dd>{!! $participant->gpPoints() !!}</dd>
                    </dl>
                </li>
                <li class="link">
                    <a href="{!! $participant->link() !!}">Rider Profile &raquo;</a>
                </li>
            </ul>
        @endforeach
    </fieldset>
</li>

@if ($loop->last)
    </ul>
@endif
