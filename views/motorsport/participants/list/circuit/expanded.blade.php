@if ($loop->first)
    <h1>{!! $title !!}</h1>
    <ul class="inline_list grid">
@endif

<li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="section profile">
        <div class="pos head {!! $team->pos() < 4 ? 'podium-' . $team->pos() : 'row_head' !!}">{!! $team->displayPos() !!}</div>
        <h3 class="row_head">{!! $team->nameFlag() !!}</h3>
        <div class="pts head row_head">{!! $team->displayPtsFull() !!}</div>
        @if ($titles = $team->championships())
            <h4 class="champs podium-1">{!! count($titles) !!}x Constructors&#39; Champion ({!! Format::groupRanges($titles) !!})</h4>
        @endif
        {!! $team->image(['size' => 'small']) !!}
        <dl class="bio inline_list">
            <dt>Team Base:</dt>
                <dd>{!! $team->base_location !!}</dd>
            <dt>Victories:</dt>
                <dd>{!! $team->gpVictories() !!}</dd>
            <dt>Podiums:</dt>
                <dd>{!! $team->gpPodiums() !!}</dd>
            <dt>Pole Positions:</dt>
                <dd>{!! $team->gpPoles() !!}</dd>
            <dt>Fastest Laps:</dt>
                <dd>{!! $team->gpFastestLaps() !!}</dd>
        </dl>
        <dl class="inline_list clearfix participants">
            @foreach ($team->drivers as $participant)
                @php
                    $row_css = 'row_' . ($loop->iteration % 2);
                @endphp
                <dt class="row_head num secondary">{!! $participant->car_no !!}</dt>
                <dd class="{!! $row_css !!} participant score secondary">{!! $participant->nameLinkFlag() !!}</dd>
                <dd class="{!! $row_css !!} pts secondary">{!! Format::pluralise($participant->pts, ' pt') !!}</dd>
            @endforeach
        </dl>
        <a class="link" href="{!! $team->link() !!}">Team Profile &raquo;</a>
    </fieldset>
</li>

@if ($loop->last)
    </ul>
@endif
