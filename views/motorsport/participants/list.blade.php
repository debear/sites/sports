@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

{{-- More explicit than an @each, so our include can access $loop --}}
@foreach($teams as $team)
    @include("sports.motorsport.participants.list.$type")
@endforeach

@endsection
