@extends('skeleton.layouts.html')

@section('content')

@include('sports.motorsport.participants.view.header')

@include('skeleton.widgets.subnav')

@include('sports.motorsport.participants.view.season')
@include('sports.motorsport.participants.view.stats')
@include('sports.motorsport.participants.view.history')

@endsection
