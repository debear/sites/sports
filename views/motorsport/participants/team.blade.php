@extends('skeleton.layouts.html')

@section('content')

@include('sports.motorsport.participants.team.header')

@include('skeleton.widgets.subnav')

@include('sports.motorsport.participants.team.season')
@include('sports.motorsport.participants.team.stats')
@include('sports.motorsport.participants.team.history')

@endsection
