{{-- The stats --}}
@unless (!$team->seasonStats->count())
    <ul class="subnav-stats inline_list clearfix hidden">
        @foreach($team->seasonStats as $stat)
            @php
                $row_css = $stat->posCSS($loop->index);
            @endphp
            <li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <dl class="stat">
                    <dt class="{!! $row_css !!}">{!! $stat->name_full !!}:</dt>
                        <dd class="value {!! $row_css !!} {!! $stat->valueCSS() !!}">{!! $stat->rendered() !!}</dd>
                        <dd class="rank {!! $row_css !!}">{!! $stat->displayPos() !!}</dd>
                </dl>
            </li>
        @endforeach
    </ul>
{{-- Season not yet started --}}
@else
    <fieldset class="subnav-stats no-stats info icon icon_info hidden">The {!! $season_viewing !!} season has not yet started.</fieldset>
@endif
