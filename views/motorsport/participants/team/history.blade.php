@php
    $sorted = $team->history;
    $current = $sorted->first();
    $display = $sorted->where('season', $season_viewing);
    $row_count = 0;
@endphp
<dl class="subnav-history hidden clearfix">
    <dt class="row_head season">Year</dt>
    <dt class="row_head name">Team Name</dt>
    <dt class="row_head result">Result</dt>

    {{-- Team no longer exists? --}}
    @if ($current->season != FrameworkConfig::get('debear.setup.season.default'))
        <dd class="row_head full">
            Team no longer participating in {!! FrameworkConfig::get('debear.names.section') !!}
        </dd>
    @endif

    {{-- List --}}
    @foreach ($sorted as $history)
        @php
            // Changes since last iteration
            if (isset($last)) {
                // Missing season(s)?
                $loop_diff = ($last['season'] - $history->season);
                if ($loop_diff > 1) {
                    $row_count++;
                    $row_css = 'row_' . ($row_count % 2);
                    if ($loop_diff == 2) {
                        // One season missed
                        $seasons = 'in ' . ($last['season'] - 1);
                    } elseif ($loop_diff == 3) {
                        // Two seasons missed
                        $seasons = 'in ' . ($history->season + 1) . ' or ' . ($last['season'] - 1);
                    } else {
                        // Three+ seasons missed
                        $seasons = 'between ' . ($history->season + 1) . ' and ' . ($last['season'] - 1);
                    }
                    print "<dd class=\"$row_css full\">Did not compete $seasons</dd>";
                }
                // Team name switch?
                if ($last['name'] != $history->team_name) {
                    $row_count++;
                    print "<dt class=\"row_head full\">Team transferred to {$last['name']} from {$history->team_name}</dd>";
                }
            }
            $last = [
                'season' => $history->season,
                'name' => $history->team_name,
            ];

            // Cell CSS
            $row_count++;
            $row_css = ($history->pos && ($history->pos < 4) ? "podium-{$history->pos}" : 'row_' . ($row_count % 2));
            // Current (incomplete) season?
            $in_progress = ($history->season == FrameworkConfig::get('debear.setup.season.default'));
            if ($in_progress) {
                $row_css .= ' current';
                $current_progress = true;
            }
            if ($history->season == $season_viewing) {
                $row_css .= ' display';
            }
            // Team name needs a flag?
            $flag = ($history->team_country != $display->team_country);
            // Format the season
            $season_cell = $history->season;
            if (!$history->isHistorical()) {
                $season_cell = '<a href="/' . $history->sectionEndpoint() . '/standings' . (!$in_progress ? "/{$history->season}" : '') . "\">$season_cell</a>";
            }
        @endphp
        <dd class="{!! $row_css !!} season">
            {{-- Season --}}
            {!! $season_cell !!}
            {{-- Flag season in progress --}}
            @if ($in_progress)
                <span class="in-progress"></span>
            @endif
        </dd>
        <dd class="{!! $row_css !!} name">
            {{-- Open the flag --}}
            @if ($flag)
                <span class="flag_right flag16_right_{!! $history->team_country !!}">
            @endif
                {{-- Official name --}}
                {!! $history->official_name !!}
            {{-- Close the flag --}}
            @if ($flag)
                </span>
            @endif
        </dd>
        <dd class="{!! $row_css !!} pts">
            {!! $history->displayPtsFull() !!}
        </dd>
        <dd class="{!! $row_css !!} pos">
            {!! $history->displayPos() !!}
        </dd>
        @isset($history->notes)
            <dd class="{!! $row_css !!} notes">
                {!! $history->notes !!}
            </dd>
        @endisset
    @endforeach

    @isset($current_progress)
        <dd class="key">
            <span class="in-progress"></span> &ndash; Season in progress
        </dd>
    @endisset
</dl>
