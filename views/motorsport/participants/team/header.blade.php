@php
    // Setup the switcher
    if ($season == $season_viewing) {
        $opt = [];
        foreach ($standings as $standteam) {
            $label = [
                'pos' => Format::ordinal($standteam->pos()),
                'name' => $standteam->nameFlag(),
                'pts' => $standteam->displayPtsFull(),
            ];
            $opt[] = [
                'id' => $standteam->id,
                'attrib' => [
                    'code' => $standteam->name_codified,
                    'season' => $season_viewing,
                    'label-text' => strip_tags(join(' &ndash; ', $label)),
                ],
                'label' => '<div class="team ' . $standteam->standingsCurr->css() . '">'
                    . '<span class="pos">' . $label['pos'] . '</span>'
                    . '<span class="pts">' . $label['pts'] . '</span>'
                    . $label['name']
                    . '</div>',
            ];
        }
        $dropdown = new Dropdown('switcher', $opt, [
            'value' => $team->id,
            'select' => false,
            'classes' => ['switcher'],
        ]);
    }
@endphp

<div class="individual-summary">
    {{-- Logo --}}
    {!! $team->image(['season' => $season_viewing]) !!}

    {{-- Name --}}
    <h1>{!! $team->nameFlag() !!}</h1>

    {{-- Team Switcher --}}
    @isset($dropdown)
        {!! $dropdown->render() !!}
    @endisset

    {{-- Summary --}}
    <ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Team Base:</strong>
                {!! $team->base_location !!}
            </span>

            <span>
                <strong>Constructors&#39; Titles:</strong>
                @if (count($team->championships()))
                    <abbrev title="{!! Format::groupRanges($team->championships()) !!}">
                @endif
                {!! count($team->championships()) !!}
                @if (count($team->championships()))
                    </abbrev>
                @endif
            </span>
        </li>

        <li class="standings">
            <span>
                <strong>{!! $season_viewing !!} Position:</strong>
                {!! $team->displayPos() !!}
            </span>

            <span>
                <strong>{!! $season_viewing !!} Points:</strong>
                {!! $team->displayPtsFull() !!}
            </span>
        </li>

        <li class="stats">
            <span>
                <strong>{!! $season_viewing !!} Wins:</strong>
                {!! $team->gpVictories() !!}
            </span>

            <span>
                <strong>{!! $season_viewing !!} Podiums:</strong>
                {!! $team->gpPodiums() !!}
            </span>

            <span>
                <strong>{!! $season_viewing !!} Pole Positions:</strong>
                {!! $team->gpPoles() !!}
            </span>

            <span>
                <strong>{!! $season_viewing !!} Fastest Laps:</strong>
                {!! $team->gpFastestLaps() !!}
            </span>
        </li>
    </ul>
</div>
