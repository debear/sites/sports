{{-- The chart --}}
@php
    // The chart
    $dom_id = 'season_chart';
    $chart = $team->progressChart($dom_id, count($standings));
    $js_obj = $chart['js-obj'];
    unset($chart['js-obj']);
@endphp
<div class="subnav-season chart" id="{!! $dom_id !!}"></div>

<script {!! HTTP::buildCSPNonceTag() !!}>
    var chart = new Highcharts.Chart({!! Highcharts::decode($chart) !!});
    @foreach ($js_obj as $key => $val)
        var {!! $key !!} = @json($val);
    @endforeach
</script>
