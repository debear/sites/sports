@php
    // Setup the switcher
    if ($season == $season_viewing) {
        $opt = [];
        foreach ($standings as $standpart) {
            $label = [
                'pos' => Format::ordinal($standpart->pos()),
                'name' => $standpart->nameFlag(),
                'pts' => $standpart->displayPtsFull(),
            ];
            $opt[] = [
                'id' => $standpart->id,
                'attrib' => [
                    'href' => $standpart->name_codified . '-' . $standpart->id . '/' . $season,
                    'label-text' => strip_tags(join(' &ndash; ', $label)),
                ],
                'label' => '<div class="participant ' . $standpart->standingsCurr->css() . '">'
                    . '<span class="pos">' . $label['pos'] . '</span>'
                    . '<span class="pts">' . $label['pts'] . '</span>'
                    . $label['name']
                    . '</div>',
            ];
        }
        $dropdown = new Dropdown('switcher', $opt, [
            'value' => $participant->id,
            'select' => false,
            'classes' => ['switcher'],
        ]);
    }
@endphp

<div class="individual-summary">
    {{-- Logo --}}
    {!! $participant->image(['season' => $season_viewing]) !!}

    {{-- Name --}}
    <h1>{!! $participant->nameFlag() !!}</h1>

    {{-- Participant Switcher --}}
    @isset($dropdown)
        {!! $dropdown->render() !!}
    @endisset

    {{-- Summary --}}
    <ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Born:</strong>
                {!! $participant->getBornAttribute() !!}
            </span>

            <span>
                <strong>Championships:</strong>
                @if (count($participant->championships()))
                    <abbrev title="{!! Format::groupRanges($participant->championships()) !!}">
                @endif
                {!! count($participant->championships()) !!}
                @if (count($participant->championships()))
                    </abbrev>
                @endif
            </span>

            <span>
                <strong>Experience:</strong>
                {!! $participant->experience() !!} Season
            </span>
        </li>

        <li class="standings">
            <span>
                <strong>{!! $season_viewing !!} Position:</strong>
                {!! $participant->standingsCurr->displayPos() !!}
            </span>

            <span>
                <strong>{!! $season_viewing !!} Points:</strong>
                {!! $participant->standingsCurr->displayPtsFull() !!}
            </span>
        </li>

        @include("sports.motorsport.participants.view.header.$type")
    </ul>
</div>
