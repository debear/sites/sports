@php
    $is_circuit = (FrameworkConfig::get('debear.setup.type') == 'circuit');
    $is_broken_down = (FrameworkConfig::get('debear.setup.participants.broken-down'));
    $is_inactive = !$participant->history->where('season', FrameworkConfig::get('debear.setup.season.default'))->count();
    $row_count = 0;
    $season_gaps = [];

    // Some pre-processing
    $max_races = 0;
    $races = $results = [];
    foreach ($participant->history as $history) {
        // Pre-cache the races.
        $races[$history->season_key] = [];
        if ($history->is_detailed) {
            // Via a calendar.
            foreach ($history->calendar as $race) {
                $has_sprint_race = $race->quirk('sprint_race');
                $first_race = ($has_sprint_race ? 2 : 1); // Skip the sprint race.
                $last_race = (isset($race->race2_time) ? 2 : 1);
                for ($i = $first_race; $i <= $last_race; $i++) {
                    $race_key = "{$race->round}-$i";
                    $races[$history->season_key][$race_key] = [
                        'round' => $race->round,
                        'race' => $i,
                        'flag' => $race->flag(),
                        'link' => $race->canLink() ? $race->link($i) : false,
                        'race_obj' => $race->getCurrent(),
                        'has_sprint_race' => $has_sprint_race,
                    ];
                }
            }
        } else {
            // Via the results list.
            foreach ($history->results as $res) {
                $race_key = "{$res->round}-1";
                $races[$history->season_key][$race_key] = [
                    'round' => $res->round,
                    'race' => 1,
                    'flag' => $res->flag(),
                    'link' => false,
                    'race_obj' => null,
                    'has_sprint_race' => false,
                ];
            }
        }
        // And then the results by team.
        foreach ($history->teams as $team) {
            foreach ($team->results as $result) {
                $race_key = $result->round . '-' . ($result->race ?? 1);
                if (!isset($races[$history->season_key][$race_key])) {
                    continue;
                }
                $results[$result->full_key] = [
                    'css' => $result->css($races[$history->season_key][$race_key]['race_obj']),
                    'grid' => isset($result->grid_pos) ? 'data-grid="' . $result->displayGrid() . '"' : '',
                    'pos' => $result->pos,
                    'pts' => $result->displayPts(),
                    'res' => $result->displayResult(),
                ];
                // Include points from the sprint race (if there was one)
                if ($races[$history->season_key][$race_key]['has_sprint_race']) {
                    $race1_key = preg_replace('/-2$/', '-1', $race_key);
                    $race1_pts = $team->results->where('race_key', $race1_key)->displayPts();
                    if (is_numeric($race1_pts)) {
                        $results[$result->full_key]['pts'] = (int)$results[$result->full_key]['pts'] + $race1_pts;
                    }
                }
            }
        }
        // Determine the maximum races in a season.
        $hist_races = count($races[$history->season_key]);
        if ($hist_races > $max_races) {
            $max_races = $hist_races;
        }
    }
@endphp
<tables class="subnav-history num-{!! $max_races !!} {!! $is_broken_down ? 'with' : 'no' !!}-group type-{!! FrameworkConfig::get('debear.setup.type') !!} hidden clearfix">
    {{-- Main Info --}}
    <main class="clearfix">
        {{-- Participant no longer active? --}}
        @if ($is_inactive)
            <div class="row_head full no-participate">
                {!! Format::singularise(ucfirst(FrameworkConfig::get('debear.setup.participants.url'))) !!} no longer active in {!! FrameworkConfig::get('debear.names.sport') !!}
            </div>
        @endif

        {{-- Seasons/Teams --}}
        @foreach ($participant->history as $history)
            @php
                // Some CSS classes
                $first_row_css = ($loop->first ? 'first-row' : '');
                $height_css = 'num-' . $history->teams->count();
                // Changes since last iteration
                if (isset($last)) {
                    // Missing season(s)?
                    $loop_diff = ($last - $history->season);
                    if ($loop_diff > 1) {
                        $row_count++;
                        $dnc_css = 'row_' . ($row_count % 2);
                        if ($loop_diff == 2) {
                            // One season missed
                            $seasons = 'in ' . ($last - 1);
                        } elseif ($loop_diff == 3) {
                            // Two seasons missed
                            $seasons = 'in ' . ($history->season + 1) . ' or ' . ($last - 1);
                        } else {
                            // Three+ seasons missed
                            $seasons = 'between ' . ($history->season + 1) . ' and ' . ($last - 1);
                        }
                        print "<div class=\"$dnc_css full\">Did not compete $seasons</div>";
                        $season_gaps[$history->reference] = true;
                    }
                }
                $last = $history->season;
                // Cell CSS
                $row_count++;
                $row_css[$history->reference] = ($history->pos && ($history->pos < 4) ? "podium-{$history->pos}" : 'row_' . ($row_count % 2));
                // Current (incomplete) season?
                $extra_css = [];
                $in_progress = ($history->season == FrameworkConfig::get('debear.setup.season.default'));
                if ($in_progress) {
                    $extra_css[] = 'current';
                    $current_progress = true;
                }
                if (($history->season == $season_viewing) && $history->is_relevant) {
                    $extra_css[] = 'display';
                }
                $extra_css = join(' ', $extra_css);
                // Format the season
                $season_cell = $history->season;
                if (!$history->isHistorical()) {
                    $season_cell = '<a href="/' . $history->sectionEndpoint() . '/standings' . (!$in_progress ? "/{$history->season}" : '') . "\">$season_cell</a>";
                }
            @endphp
            {{-- Season Summary --}}
            <div class="row_head {!! $first_row_css !!} {!! $extra_css !!} {!! $height_css !!} season">
                {{-- Season --}}
                {!! $season_cell !!}
                {{-- Flag season in progress --}}
                @if ($in_progress)
                    <span class="in-progress"></span>
                @endif
            </div>
            @if ($is_broken_down)
                <div class="row_head {!! $first_row_css !!} {!! $height_css !!} group">
                    {!! $history->group !!}
                </div>
            @endif
                <div class="{!! $row_css[$history->reference] !!} {!! $first_row_css !!} {!! $extra_css !!} {!! $height_css !!} pts">
                    {!! $history->displayPtsFull() !!}
                </div>
                <div class="{!! $row_css[$history->reference] !!} {!! $first_row_css !!} {!! $extra_css !!} {!! $height_css !!} pos">
                    {!! $history->displayPos() !!}
                </div>

            {{-- Results by Team --}}
            @foreach ($history->teams as $team)
                @php
                    $name = '';
                    $team_css = [];
                    if ($team->name) {
                        $team_css[] = $row_css[$history->reference];
                        $name = $team->nameLinkFlag();
                    }
                    if ($loop->parent->first && $loop->first) {
                        $team_css[] = 'team-first';
                    }
                @endphp
                @if ($name || count($races[$history->season_key]))
                    <div class="team {!! join(' ', $team_css) !!}">
                        {!! $name !!}
                    </div>
                @endif
            @endforeach
        @endforeach
    </main>

    {{-- Race Results --}}
    <races class="clearfix">
        <racelist>
            {{-- Participant no longer active? --}}
            @if ($is_inactive)
                <div class="gap"></div>
            @endif
            @foreach ($participant->history as $history)
                @php
                    $first_row_css = ($loop->first ? 'first-row' : '');
                    if (isset($season_gaps[$history->reference]) && $season_gaps[$history->reference]) {
                        print '<div class="gap"></div>';
                    }
                @endphp
                {{-- Races Header --}}
                @foreach ($races[$history->season_key] as $race)
                    @php
                        $first_race_css = ($loop->first ? 'first-race' : '');
                    @endphp
                    <div class="row_head {!! $first_row_css !!} {!! $first_race_css !!} race-cell flag16_{!! $race['flag'] !!} {!! $race['link'] ? 'with-link' : '' !!}">
                        @if ($race['link'])
                            <a class="no_underline" href="{!! $race['link'] !!}"></a>
                        @endif
                    </div>
                @endforeach
                {{-- Extra Race Padding --}}
                @if (count($races[$history->season_key]))
                    @for ($pad = count($races[$history->season_key]) + 1; $pad <= $max_races; $pad++)
                        <div class="{!! $first_row_css !!} race-cell race-pad res-no_race"></div>
                    @endfor
                @endif

                {{-- Results by Team --}}
                @if (count($races[$history->season_key]))
                    @foreach ($history->teams as $team)
                        @foreach ($races[$history->season_key] as $race)
                            @php
                                $first_race_css = ($loop->first ? 'first-race' : '');
                                $key = "{$team->team_key}-{$race['round']}-{$race['race']}";
                                if (isset($results[$key])) {
                                    $result = $results[$key];
                                } else {
                                    $result = [
                                        'css' => (isset($race['race_obj']) && ($race['race_obj']->canLink() || $race['race_obj']->isCancelled($race['race_obj']->quirk('sprint_race') ? 2 : 1) ) ? 'res-no_race' : 'res-not_yet_raced'),
                                        'grid' => '',
                                        'pos' => '',
                                        'pts' => '',
                                        'res' => '',
                                    ];
                                }
                            @endphp
                            <div class="race-cell {!! $first_race_css !!} {!! $result['css'] !!}" data-pos="{!! $result['pos'] !!}" data-pts="{!! $result['pts'] !!}" data-res="{!! $result['res'] !!}" {!! $result['grid'] ?? '' !!}>{!! $result['pts'] !!}</div>
                        @endforeach
                        {{-- Extra Race Padding --}}
                        @for ($pad = count($races[$history->season_key]) + 1; $pad <= $max_races; $pad++)
                            <div class="race-cell race-pad res-no_race"></div>
                        @endfor
                    @endforeach
                @endif
            @endforeach
        </racelist>
    </races>

    {{-- Toggles / Key --}}
    <div class="key-handler">
        @include('sports.motorsport.standings.view.key')
    </div>
    @isset($current_progress)
        <div class="key">
            <span class="in-progress"></span> &ndash; Season in progress
        </div>
    @endisset
</tables>
