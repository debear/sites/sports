@php
    // The chart
    $dom_id = 'season_chart';
    $charts = $participant->progressChart($dom_id, $max_pos);
@endphp
<div class="subnav-season">
    {{-- Season switcher? --}}
    @if (sizeof($charts) > 1)
        @php
            $dropdown = new Dropdown('switcher-progress', array_keys($charts), [
                'value' => $season_viewing,
                'select' => false,
                'classes' => ['switcher-progress'],
            ]);
        @endphp
        <div class="switcher">
            {!! $dropdown->render() !!}
        </div>
    @endif
    {{-- The chart --}}
    <div class="chart" id="{!! $dom_id !!}"></div>
</div>

<script {!! HTTP::buildCSPNonceTag() !!}>
    // Data for each season
    var chartHistory = {
        @foreach ($charts as $s => $chart)
            {!! $s !!}: {!! Highcharts::decode($chart) !!}{!! !$loop->last ? ',' : '' !!}
        @endforeach
    };
    // Then the chart
    var chart = new Highcharts.Chart(chartHistory[{!! $season_viewing !!}]);
</script>
