@php
    $stats = $participant->parseStats();
    $seasons = array_keys($stats['history']);
@endphp
<div class="subnav-stats hidden">
    {{-- Season switcher? --}}
    @if (sizeof($seasons) > 1)
        @php
            $dropdown = new Dropdown('switcher-stats', $seasons, [
                'value' => $season_viewing,
                'select' => false,
                'classes' => ['switcher-stats'],
            ]);
        @endphp
        <div class="switcher">
            {!! $dropdown->render() !!}
        </div>
    @endif
    {{-- The stats --}}
    <ul class="inline_list num_{!! count($stats['history']) !!} clearfix">
        {{-- Actual list --}}
        @php
            $no_stats = !count($stats['history'][$season_viewing]);
            $season_default = (($season_viewing < FrameworkConfig::get('debear.setup.season.default')) || !$stats['info']['preseason'] || $stats['info']['no-history']
                ? $season_viewing
                : max(array_keys(array_filter($stats['history']))));
        @endphp
        @foreach($stats['stats'] as $s)
            @php
                $stat = $stats['history'][$season_default][$s['stat_id']];
            @endphp
            <li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 {!! $no_stats ? 'hidden' : '' !!}">
                <dl data-id="{!! $s['stat_id'] !!}" class="stat">
                    <dt class="cell {!! $stat['pos_css'] !!}" data-css="{!! $stat['pos_css'] !!}">{!! $s['name_full'] !!}:</dt>
                        <dd class="cell value {!! $stat['pos_css'] !!} {!! $stat['value_css'] !!}" data-css="{!! $stat['pos_css'] !!} {!! $stat['value_css'] !!}">{!! $stat['value'] !!}</dd>
                        <dd class="cell rank {!! $stat['pos_css'] !!}" data-css="{!! $stat['pos_css'] !!}">{!! $stat['pos'] !!}</dd>
                </dl>
            </li>
        @endforeach
        {{-- No stats available --}}
        @if ($stats['info']['preseason'])
            <li class="preseason grid-1-1 {!! !$stats['info']['no-history'] && ($season_viewing != FrameworkConfig::get('debear.setup.season.default')) ? 'hidden' : '' !!}">
                <fieldset class="no-stats info icon icon_info">The {!! FrameworkConfig::get('debear.setup.season.default') !!} season has not yet started.</fieldset>
            </li>
        @endif
    </ul>
</div>
<script {!! HTTP::buildCSPNonceTag() !!}>
    var statHistory = @json($stats['history']);
</script>
