<li class="stats">
    <span>
        <strong>Career Wins:</strong>
        {!! $participant->gpVictories() !!}
    </span>

    <span>
        <strong>Career Podiums:</strong>
        {!! $participant->gpPodiums() !!}
    </span>

    <span>
        <strong>Career Points:</strong>
        {!! $participant->gpPoints() !!}
    </span>
</li>
