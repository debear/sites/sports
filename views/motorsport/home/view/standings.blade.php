<fieldset class="section grid-1-1 {!! isset($teams) ? 'grid-t-1-2' : '' !!} standings">
    <h3 class="row_head">{!! $name !!}s&#39; Standings</h3>

    <dl>
        @foreach ($list as $item)
            @php
                $row_css = $item->standingsCurr->css();
            @endphp
            <dt class="{!! $row_css !!} pos">{!! $item->standingsCurr->pos !!}</dt>
                <dd class="{!! $row_css !!} name">{!! $item->nameLinkFlag() !!}</dd>
                <dd class="{!! $row_css !!} pts">{!! $item->standingsCurr->displayPtsFull() !!}</dd>
        @endforeach

        {{-- Link to full standings? --}}
        @if ($more)
            <dd class="more row_{!! ($list->count() + 1) % 2 !!}">
                <a href="/{!! FrameworkConfig::get('debear.section.endpoint') !!}/standings{!! $header_season ? "/$header_season" : '' !!}">View Full Standings</a>
            </dd>
        @endif
    </dl>
</fieldset>
