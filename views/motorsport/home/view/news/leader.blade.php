<li class="image {!! $loop->index % 2 ? 'image-right' : 'image-left' !!} article-{!! $article->uniqueReference() !!}"></li>
<li class="{!! $row_css !!} headline">
    <a href="{!! $article->link !!}" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">{!! $article->headline() !!}</a>
</li>
<li class="{!! $row_css !!} info">
    <span class="pub">{!! $article->publisher() !!}</span>
    <span class="time">{!! $article->timeRelative() !!}</span>
</li>
<li class="{!! $row_css !!} summary">
    {!! $article->summary() !!}
</li>
