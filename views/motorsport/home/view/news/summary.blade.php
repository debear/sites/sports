<li class="{!! $row_css !!} headline-only">
    <a href="{!! $article->link !!}" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">{!! $article->headline() !!}</a>
    &ndash; {!! $article->timeRelative() !!}
    <em>({!! $article->publisher() !!})</em>
</li>
