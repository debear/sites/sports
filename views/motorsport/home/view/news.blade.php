@php
    $article_img = [];
@endphp
<fieldset class="section news">
    <h3 class="row_head">Latest News</h3>

    <ul class="inline_list">
        @foreach ($news as $article)
            @php
                $row_css = 'row_' . ($loop->index % 2);
                if ($loop->index < 3) {
                    $article_img[$article->uniqueReference()] = $article->image();
                }
            @endphp
            @includeWhen($loop->index < 3, 'sports.motorsport.home.view.news.leader', compact('row_css'))
            @includeWhen($loop->index >= 3, 'sports.motorsport.home.view.news.summary', compact('row_css'))
        @endforeach

        <li class="more row_{!! $news->count() % 2 !!}">
            <a href="/{!! FrameworkConfig::get('debear.section.endpoint') !!}/news{!! $header_season ? "?header=$header_season" : '' !!}">View the Latest News</a>
        </li>
    </ul>
</fieldset>

<style {!! HTTP::buildCSPNonceTag() !!}>
    @foreach ($article_img as $css => $img)
        .article-{!! $css !!} {
            background-image: url('{!! $img !!}');
        }
    @endforeach
</style>
