@php
    $race_name = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
    $season_disp = ($race->season != FrameworkConfig::get('debear.setup.season.viewing') ? "{$race->season} " : '');
    $has_sprint_race = $race->quirk('sprint_race');
    $inc_race2 = isset($race->race2_time);
@endphp
<fieldset class="section race-result grid-1-2 grid-tp-1-1 grid-m-1-1">
    <h3 class="row_head">{!! $prev_type !!} {!! $race_name !!}: {!! $season_disp . $race->nameFlag() !!}</h3>

    <dl>
        {{-- Awkward exception to handle: was the race cancelled? --}}
        @if ($race->isCancelled())
            <dd class="cancelled">
                <div class="box info icon icon_info">The {!! $race->season !!} {!! strtolower($race_name) !!} was cancelled.</div>
            </dd>

        {{-- No, display the result --}}
        @else
            @includeWhen(!$has_sprint_race, 'sports.motorsport.home.view.race.result-item', ['race_num' => 1])
            @includeWhen($inc_race2 || $has_sprint_race, 'sports.motorsport.home.view.race.result-item', ['race_num' => 2])

            <dd class="result more row_1">
                <a href="{!! $race->link() !!}">Full {!! $race_name !!} Result</a>
            </dd>
        @endif
    </dl>
</fieldset>
