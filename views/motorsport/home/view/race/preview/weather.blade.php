<dd class="weather icon_weather_{!! $weather->symbol !!}">
    {!! $weather->summary !!}, {!! (int)$weather->temp !!} &deg;C / {!! (int)$weather->tempF !!} &deg;F
    @isset($weather->precipitation)
        <span class="precip">, {!! (int)(100 * $weather->precipitation) !!}% chance {!! $weather->symbol == 'snow' ? 'snow' : 'rain' !!}</span>
    @endisset
</dd>
