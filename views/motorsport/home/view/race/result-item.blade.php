@php
    $title = ($inc_race2 && !$has_sprint_race ? "$race_name $race_num " : '') . 'Podium';
    $results = $race->results->where('race', $race_num);
    $winner = $results->first();
    $podium = $results->whereIn('pos', [1, 2, 3]);
    $inc_team = FrameworkConfig::get('debear.setup.teams.display');
@endphp

{{-- Winner Mugshot --}}
<dd class="mugshot mugshot-small">
    {!! $winner->raceParticipant->participant->image(['season' => $race->season, 'size' => 'small']) !!}
</dd>

{{-- The Podium --}}
<dt class="row_head">{!! $title !!}:</dt>
    @foreach ($podium as $p)
        @php
            $row_css = 'podium-' . $p->pos($race_num);
        @endphp
        <dt class="{!! $row_css !!} pos">
            {!! $p->pos($race_num) !!}
        </dt>
            <dd class="{!! $row_css !!} participant {!! !$inc_team ? 'no-team' : '' !!}">
                {!! $p->raceParticipant->participant->nameLinkFlag() !!}
            </dd>
            @if ($inc_team)
                <dd class="{!! $row_css !!} team">
                    {!! $p->raceParticipant->team->nameLinkFlag() !!}
                </dd>
            @endif
    @endforeach
