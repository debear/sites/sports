@php
    $race_name = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
    $season_disp = ($race->season != FrameworkConfig::get('debear.setup.season.viewing') ? "{$race->season} " : '');
    $inc_race2 = isset($race->race2_time);
@endphp
<fieldset class="section race-preview grid-1-2 grid-tp-1-1 grid-m-1-1">
    <h3 class="row_head">Next {!! $race_name !!}: {!! $season_disp !!}{!! $race->nameFlag() !!}</h3>

    <dl>
        {{-- Track --}}
        <dd class="venue">{!! $race->venue !!}</dd>

        {{-- Qualifying? --}}
        @isset($race->qual_time)
            <dt>Qualifying:</dt>
                <dd class="time">{!! $race->qual_start_time !!}</dd>
                @includeWhen($race->forecast->quali->isset(), 'sports.motorsport.home.view.race.preview.weather', ['weather' => $race->forecast->quali])
        @endisset

        {{-- Race --}}
        <dt>{!! $race_name !!}{!! $inc_race2 ? ' 1' : '' !!}:</dt>
            <dd class="time">{!! $race->race_start_time !!}</dd>
            @includeWhen($race->forecast->race->isset(), 'sports.motorsport.home.view.race.preview.weather', ['weather' => $race->forecast->race])

        {{-- Race 2? --}}
        @if ($inc_race2)
            <dt>{!! $race_name !!} 2:</dt>
                <dd class="time">{!! $race->race2_start_time !!}</dd>
                @includeWhen($race->forecast->race2->isset(), 'sports.motorsport.home.view.race.preview.weather', ['weather' => $race->forecast->race2])
        @endif
    </dl>
</fieldset>
