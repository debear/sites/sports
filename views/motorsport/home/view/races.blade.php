<div class="races grid grid-1-1">
    {{-- Left --}}
    @include('sports.motorsport.home.view.race.result', ['prev_type' => $races['right']->isComplete() ? 'Previous' : 'Last', 'race' => $races['left']])

    {{-- Right --}}
    @includeWhen(!$races['right']->isComplete(), 'sports.motorsport.home.view.race.preview', ['race' => $races['right']])
    @includeWhen($races['right']->isComplete(), 'sports.motorsport.home.view.race.result', ['prev_type' => 'Last', 'race' => $races['right']])
</div>
