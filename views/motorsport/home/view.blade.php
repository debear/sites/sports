@extends('skeleton.layouts.html')

@section('content')

<h1>Welcome to {!! FrameworkConfig::get('debear.names.site') !!} {!! FrameworkConfig::get('debear.names.section') !!}</h1>

<div class="grid">
    {{-- Left Column --}}
    <div class="col-left grid-3-4 grid-t-1-1 grid-m-1-1">
        {{-- Races --}}
        @include('sports.motorsport.home.view.races')

        {{-- News --}}
        @includeWhen(isset($news), 'sports.motorsport.home.view.news')
    </div>

    {{-- Right Column --}}
    <div class="col-right grid grid-1-4 grid-t-1-1 grid-m-1-1">
        {{-- Standings --}}
        @include('sports.motorsport.home.view.standings', ['name' => rtrim(ucfirst(FrameworkConfig::get('debear.setup.participants.url')), 's'), 'list' => $participants->forPage(1, 10), 'more' => true])
        @includeWhen(isset($teams), 'sports.motorsport.home.view.standings', ['name' => 'Constructor', 'list' => $teams, 'more' => false])
    </div>
</div>

@endsection
