@php
    $num_heat_groups = ($expected_heats / 4);
    $num_main_draw = ($race->setupRace->regulars + $race->setupRace->wildcards);
    $num_riders = $num_main_draw + $race->setupRace->track_reserves;
@endphp
<fieldset class="section result-grid subnav-grid">
    <dl>
        {{-- Header --}}
        <dt class="row_head table-cell table-clear rider">Rider</dt>
        <dt class="row_head table-cell result">Result</dt>
        @for ($heat_group = 1; $heat_group <= $num_heat_groups; $heat_group++)
            <dt class="row_head table-cell heats"><span class="lead">Heat </span>{!! $heat_group !!}</dt>
        @endfor
        @for ($heat = 1; $heat <= $expected_heats; $heat++)
            <dt class="row_head table-cell heat">{!! $heat !!}</dt>
        @endfor

        {{-- Riders --}}
        @for ($draw = 1; $draw <= $num_riders; $draw++)
            @php
                $row_css = 'row_' . ($draw % 2);
                $rider = $race->results->where('draw', $draw);

                // Result formatting
                $res_pos = (isset($rider->main_pos) ? Format::ordinal($rider->main_pos) : '');
                $res_pts = (isset($rider->main_pts) ? Format::pluralise($rider->main_pts, 'pt') : '');
            @endphp
            <dt class="row_head table-cell table-clear draw">{!! $draw !!}</dt>
            <dd class="{!! $row_css !!} table-cell bib_no">{!! $rider->bib_no !!}</dd>
            <dd class="{!! $row_css !!} table-cell table-name name">{!! $rider->raceParticipant->participant->nameLinkFlag() !!}</dd>
            <dd class="{!! $row_css !!} table-cell res-pts">{!! $res_pts !!}</dd>
            <dd class="{!! $row_css !!} table-cell res-pos">{!! $res_pos !!}</dd>

            @for ($heat = 1; $heat <= $expected_heats; $heat++)
                @php
                    $heat_res = $race->heats->where('rider_ref', "main::$heat::{$rider->bib_no}");
                    $cell_res = (isset($heat_res->pts) ? $heat_res->pts : (isset($heat_res->result) ? $heat_res->result : ''));
                    $cell_css = (isset($heat_res->gate) ? 'gate-' . $heat_res->gate : "$row_css no-ride");
                @endphp
                <dd class="{!! $cell_css !!} table-cell heat">{!! $cell_res !!}</dd>
            @endfor

            {{-- Separator? --}}
            @if ($draw == $num_main_draw)
                <dt class="row_head table-cell table-sep sep"></dt>
            @endif
        @endfor
    </dl>
</fieldset>
