@foreach ($heat_results[$heat_ref]->slice(0, $heat['qual']) as $qual)
    <dd class="mugshot table-cell hidden-m {!! $loop->index ? 'hidden-tl' : '' !!}">{!! $qual->raceParticipant->participant->image(['size' => 'small']) !!}</dd>
@endforeach
@foreach ($heat_results[$heat_ref] as $rider)
    <dt class="row_head table-cell pos">{!! is_numeric($rider->result) ? Format::ordinal($rider->result) : $rider->result !!}</dt>
    <dd class="gate-{!! $rider->gate !!} table-cell gate">{!! $gate_codes[$rider->gate] !!}</dd>
    <dd class="row_{!! $loop->index % 2 !!} table-cell bib_no">{!! $rider->bib_no !!}</dd>
    <dd class="row_{!! $loop->index % 2 !!} table-cell table-name rider">{!! $rider->raceParticipant->participant->nameLinkFlag() !!}</dd>
    @if ($loop->iteration == $heat['qual'])
        <dd class="row_head table-cell table-sep"></dd>
    @endif
@endforeach
