@php
    $stages = $race->knockoutHeats();
    $results = $race->sortedResult();
@endphp
<ul class="inline_list grid result-knockout subnav-finals hidden">
    @foreach ($stages as $heat)
        @php
            $heat_ref = "{$heat['heat_type']}::{$heat['heat']}";
            if (!isset($heat_results[$heat_ref])) {
                // Skip incomplete heats
                continue;
            }
            // Was the heat raced?
            $heat_raced = $heat_results[$heat_ref]->where('result', 1)->count();
        @endphp
        <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 heat-knockout-{!! $heat['heat_type'] !!} {!! !$heat_raced ? 'draw-only' : '' !!}">
            <fieldset class="section">
                <dl class="heat heat-knockout">
                    <dt class="row_head heat-name table-cell">{!! $heat['title'] !!}</dt>
                        @include('sports.motorsport.races.view.heats.knockout-' . ($heat_raced ? 'result' : 'draw'))
                </dl>
            </fieldset>
        </li>
    @endforeach

    {{-- Final Podium --}}
    <li class="grid-cell grid-1-2 grid-tp-1-1 podium">
        <fieldset class="section hidden-m">
            @include('sports.motorsport.races.view.common.podium', ['race_num' => 1, 'podium_subnav' => 'finals hidden'])
        </fieldset>
    </li>
</ul>
