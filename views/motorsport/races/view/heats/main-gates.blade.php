{{-- Gate Results --}}
    {{-- List opened in main-standings --}}
    <li class="grid-cell grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section heat-gates">
            <dl>
                <dt class="row_head table-title">Results by Gate</dt>
                <dt class="row_head table-cell res res-1">1st</dt>
                <dt class="row_head table-cell res res-2">2nd</dt>
                <dt class="row_head table-cell res res-3">3rd</dt>
                <dt class="row_head table-cell res res-4">4th</dt>
                @for ($gate = 1; $gate <= 4; $gate++)
                    <dt class="gate-{!! $gate !!} table-cell table-clear gate">{!! $gate_codes[$gate] !!}</dt>
                        @for ($res = 1; $res <= 4; $res++)
                            <dd class="{!! $res == 4 ? "gate-$gate" : "podium-$res" !!} table-cell res">{!! $stats_gates[$gate][$res] !!}</dd>
                        @endfor
                @endfor
            </dl>
        </fieldset>
    </li>
</ul>
