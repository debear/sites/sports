{{-- Standings --}}
@php
    if ($race->setupRace->semi_finals) {
        $cutoff = ($race->setupRace->semi_finals * 4);
    } else {
        $cutoff = (4 - $race->setupRace->last_chance_qualifiers) + (4 * $race->setupRace->last_chance_qualifiers);
    }
@endphp
<ul class="inline_list grid result-info subnav-list hidden">
    <li class="grid-cell grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section heat-standings">
            <dl>
                <dt class="row_head table-title">Standings after {!! $summary['heats'] !!} heats</dt>
                <dt class="row_head table-cell table-clear rider">Rider</dt>
                <dt class="row_head table-cell stat">Rides</dt>
                <dt class="row_head table-cell stat">Wins</dt>
                <dt class="row_head table-cell stat hidden-m">2nd</dt>
                <dt class="row_head table-cell stat hidden-m">3rd</dt>
                <dt class="row_head table-cell stat hidden-m">4th</dt>
                <dt class="row_head table-cell result">Result</dt>
                @foreach ($standings_main as $rider)
                    @php
                        $row_css = 'row_' . ($loop->index % 2);
                    @endphp
                    <dt class="row_head table-cell table-clear pos">{!! $rider->main_pos !!}</dt>
                        <dd class="{!! $row_css !!} table-cell bib_no">{!! $rider->bib_no !!}</dd>
                        <dd class="{!! $row_css !!} table-cell table-name rider">{!! $rider->raceParticipant->participant->nameLinkFlag() !!}</dd>
                        <dd class="{!! $row_css !!} table-cell stat">{!! $rider->stats->rides !!}</dd>
                        <dd class="podium-1 table-cell stat">{!! $rider->stats->num_1st !!}</dd>
                        <dd class="podium-2 table-cell stat hidden-m">{!! $rider->stats->num_2nd !!}</dd>
                        <dd class="podium-3 table-cell stat hidden-m">{!! $rider->stats->num_3rd !!}</dd>
                        <dd class="{!! $row_css !!} table-cell stat hidden-m">{!! $rider->stats->num_4th !!}</dd>
                        <dd class="row_head table-cell result">{!! Format::pluralise($rider->main_pts, 'pt') !!}</dd>

                    {{-- Separator? --}}
                    @if ($rider->main_pos == $cutoff)
                        <dd class="row_head table-cell table-sep"></dd>
                    @endif
                @endforeach
            </dl>
        </fieldset>
    </li>
    {{-- List closed in main-gates --}}
