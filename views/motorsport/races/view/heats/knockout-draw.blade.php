@foreach ($heat_results[$heat_ref] as $rider)
    <dt class="row_head table-cell pos">&ndash;</dt>
    <dd class="gate-{!! $rider->gate !!} table-cell gate">{!! $gate_codes[$rider->gate] !!}</dd>
    <dd class="row_{!! $loop->index % 2 !!} table-cell bib_no">{!! $rider->bib_no !!}</dd>
    <dd class="row_{!! $loop->index % 2 !!} table-cell table-name rider">{!! $rider->raceParticipant->participant->nameLinkFlag() !!}</dd>
@endforeach
