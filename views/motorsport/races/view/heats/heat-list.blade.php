{{-- Heats --}}
<ul class="inline_list grid result-{!! $tab !!} subnav-{!! $tab !!} heat-list-{!! $expected_heats !!} hidden">
    @for ($heat = 1; $heat <= $expected_heats; $heat++)
        @php
            // Get the riders in this heat, filter per-gate and get out the winner
            $heat_ref = "$type::$heat";
            if (!isset($heat_results[$heat_ref])) {
                // Skip incomplete heats
                continue;
            }
            $heat_winner = $heat_results[$heat_ref]->first()->raceParticipant->participant;
            $title = str_replace('${heat}', ($expected_heats > 1 ? " $heat" : ''), $title_tmpl);
        @endphp
        <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <fieldset class="section">
                <dl class="heat heat-list num-{!! $heat_results[$heat_ref]->count() !!}">
                    <dt class="row_head table-cell heat-name">{!! $title !!}</dt>
                        {{-- Mugshot --}}
                        <dd class="winner table-cell">{!! $heat_winner->image(['size' => 'small']) !!}</dd>
                        {{-- By Gate --}}
                        @foreach ($heat_results[$heat_ref] as $rider)
                            @php
                                $cmpl = is_numeric($rider->result);
                                $res = ($cmpl ? Format::ordinal($rider->result) : $rider->result);
                                $pts = ($cmpl ? Format::pluralise($rider->pts, 'pt') : $rider->pts);
                            @endphp
                            <dd class="gate gate-{!! $rider->gate !!} table-cell">{!! $gate_codes[$rider->gate] !!}</dd>
                            <dd class="gate-{!! $rider->gate !!} table-cell bib_no">{!! $rider->bib_no !!}</dd>
                            <dd class="gate-{!! $rider->gate !!} table-cell table-name rider">{!! $rider->raceParticipant->participant->nameLinkFlag() !!}</dd>
                            <dd class="gate-{!! $rider->gate !!} table-cell result">{!! $res !!}</dd>
                            <dd class="gate-{!! $rider->gate !!} table-cell pts">{!! $pts !!}</dd>
                        @endforeach
                </dl>
            </fieldset>
        </li>
    @endfor
</ul>
