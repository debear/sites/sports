@php
    $subnav_key = ($race_num == 1 ? '' : $race_num);
@endphp
<ul class="inline_list subnav-grid{!! $subnav_key !!} result-grid hidden size-{!! FrameworkConfig::get('debear.setup.grid.num-per-row') !!}">
    <li class="start-finish"></li>
    @foreach ($race->sortedGrid($race_num) as $race_grid)
        <li class="ind">
            <ul class="inline_list">
                <li class="slot"><div></div></li>
                <li class="mugshot">{!! $race_grid->raceParticipant->participant->image() !!}</li>
                <li class="{!! $race_grid->css() !!} pos">{!! $race_grid->pos() !!}</li>
                <li class="num">{!! $race_grid->raceParticipant->raceNumber() !!}</li>
                <li class="participant">{!! $race_grid->raceParticipant->participant->nameLinkFlag() !!}</li>
                <li class="row_1 extra">
                    {!! $has_quali_session && (!$has_sprint_race || $race_num == 1) ? '<div>' . $race_grid->formatResult() . '</div>' : '' !!}
                    {!! $race_grid->raceParticipant->team->nameLinkFlag() !!}
                </li>
            </ul>
        </li>
    @endforeach
</ul>
