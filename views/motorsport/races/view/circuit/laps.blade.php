@php
    $subnav_key = ($race_num == 1 ? '' : $race_num);

    // The chart
    $dom_id = 'lap_leaders' . ($race_num > 1 ? $race_num : '');
    $chart = $race->getLapLeadersChart($dom_id, $race_num);

    // Mugshot getters
    $pole = $race->getPolePosition($race_num);
    $winner = $race->getWinner($race_num);
@endphp
<dl class="subnav-laps{!! $subnav_key !!} result-laps hidden clearfix">
    <dd class="chart" id="{!! $dom_id !!}"></dd>

    {{-- Headings --}}
    <dt class="row_head pole">Pole Position</dt>
    <dt class="row_head winner">Race Winner</dt>

    {{-- Mugshots --}}
    <dd class="mugshot pole">{!! $pole->raceParticipant->participant->image() !!}</dd>
    <dd class="mugshot winner">{!! $winner->raceParticipant->participant->image() !!}</dd>
</dl>

<script {!! HTTP::buildCSPNonceTag() !!}>
    var chart = new Highcharts.Chart({!! Highcharts::decode($chart) !!});
</script>
