@php
    $subnav_key = ($quali_num == 1 ? '' : $quali_num);
    $num_sessions = DeBear\Helpers\Sports\Namespaces::callStatic('RaceQuali', 'numSessions');
    $team_link_css = FrameworkConfig::get('debear.setup.teams.history') ? 'with-link' : 'no-link';
@endphp
<ul class="inline_list sort-grid subnav-quali{!! $subnav_key !!} result-quali num-{!! $num_sessions !!} hidden cleafix ">
    {{-- Header --}}
    <li class="head">
        <dl>
            <dt class="row_head table-cell table-clear pos">Pos</dt>
            <dt class="row_head table-cell participant">{!! ucfirst(FrameworkConfig::get('debear.setup.participants.url')) !!}</dt>
            <dt class="row_head table-cell team">Team</dt>
            {{-- Session Text --}}
            @for ($i = 1; $i <= $num_sessions; $i++)
                <dt class="row_head table-cell session">Q{!! $i !!}</dt>
            @endfor
            <dt class="row_head table-cell grid-pos">Grid</dt>
            {{-- Session Sort --}}
            @for ($i = 1; $i <= $num_sessions; $i++)
                <dt class="row_head table-cell session-sort unsel session" data-col="q{!! $i !!}"><span class="icon_scroll_down"></span></dt>
            @endfor
            <dt class="row_head table-cell session-sort sel grid-pos" data-col="grid"><span class="icon_scroll_down"></span></dt>
        </dl>
    </li>

    {{-- Participants --}}
    @foreach ($race->sortedGrid(1) as $race_grid)
        @php
            // When there's a sprint shootout (i.e., a second instance), get only the appropriate sessions from the list
            $results = $race_grid->qualifying;
            if ($has_sprint_shootout) {
                $method = ($quali_num == 1 ? 'whereIn' : 'whereNotIn');
                $results = $results->$method('session', FrameworkConfig::get('debear.setup.quali.sprint'));
            }
            // Process
            $row_css = $race_grid->css();
            $times = [];
            $sort = ['data-sort-grid="' . $race_grid->pos() . '"'];
            for ($i = 1; $i <= $num_sessions; $i++) {
                $session = $results->where('session_num', $i);
                $isset = $session->count();
                $times[$i] = [
                    'pos' => $isset ? $session->pos : 99,
                    'abs' => $isset ? $session->time_abs : '',
                    'rel' => $isset ? $session->time_rel : '',
                ];
                // 100+ to keep those who did not qualify below those who did
                $sort[] = 'data-sort-q' . $i . '="' . ($isset ? $session->pos : (100 + $race_grid->pos())) . '"';
            }
        @endphp
        <li class="row" {!! join(' ', $sort) !!}>
            <dl>
                <dt class="row_head table-cell table-clear pos">{!! $race_grid->pos() !!}</dt>
                    <dd class="{!! $row_css !!} table-cell num">{!! $race_grid->raceParticipant->raceNumber() !!}</dd>
                    <dd class="{!! $row_css !!} table-cell table-name part-name">{!! $race_grid->raceParticipant->participant->nameLinkFlag() !!}</dd>
                    <dd class="{!! $row_css !!} table-cell table-name team {!! $team_link_css !!}">{!! $race_grid->raceParticipant->team->nameLinkFlag() !!}</dd>
                    @for ($i = 1; $i <= $num_sessions; $i++)
                        <dd class="{!! $times[$i]['abs'] ? $row_css : '' !!} table-cell session q{!! $i !!}-abs q{!! $i !!}-p{!! $times[$i]['pos'] !!}">{!! $times[$i]['abs'] !!}</dd>
                        @if ($times[$i]['pos'] > 1)
                            <dd class="{!! $times[$i]['rel'] ? $row_css : '' !!} table-cell session q{!! $i !!}-rel q{!! $i !!}-p{!! $times[$i]['pos'] !!}">{!! $times[$i]['rel'] !!}</dd>
                        @endif
                    @endfor
                    <dd class="{!! $row_css !!} table-cell pos">{!! $race_grid->pos() !!}</dd>
                    @if ($race_grid->hadPenalty() || $race_grid->notes)
                        <dd class="{!! $row_css !!} table-cell info">
                            <span class="icon_info" title="{!! $race_grid->notes ?: 'Demoted due to penalties' !!}"></span>
                        </dd>
                    @endif
            </dl>
        </li>
    @endforeach
</ul>
