@php
    $results = $race->sortedResult($race_num);
    $subnav_key = ($race_num == 1 ? '' : $race_num);
    $team_link_css = FrameworkConfig::get('debear.setup.teams.history') ? 'with-link' : 'no-link';
    $is_default = ($race_num == ($has_sprint_race ? 2 : 1));
@endphp
<ul class="inline_list grid result subnav-result{!! $subnav_key !!} {!! !$is_default ? 'hidden' : '' !!}">
    <li class="grid-cell grid-1-2 grid-tl-1-1 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section">
            <dl>
                {{-- Header --}}
                <dt class="row_head table-cell table-clear res-pos">Pos</dt>
                <dt class="row_head table-cell participant">{!! ucfirst(FrameworkConfig::get('debear.setup.participants.url')) !!}</dt>
                <dt class="row_head table-cell team">Team</dt>
                <dt class="row_head table-cell hidden-m laps">Laps</dt>
                <dt class="row_head table-cell hidden-m time">Race Time</dt>
                <dt class="row_head table-cell pts">Points</dt>

                {{-- Result --}}
                @foreach ($results as $result)
                    @php
                        $row_css = 'row_' . ($loop->index % 2);
                    @endphp
                    <dt class="{!! $result->css($race) !!} table-cell table-clear res-pos">{!! $result->result !!}</dt>
                    <dd class="{!! $row_css !!} table-cell num">{!! $result->raceParticipant->raceNumber() !!}</dd>
                    <dd class="{!! $row_css !!} table-cell table-name part-name">{!! $result->raceParticipant->participant->nameLinkFlag() !!}</dd>
                    <dd class="{!! $row_css !!} table-cell table-name team {!! $team_link_css !!}">{!! $result->raceParticipant->team->nameLinkFlag() !!}</dd>
                    <dd class="{!! $row_css !!} table-cell hidden-m laps">{!! $result->laps !!}</dd>
                    <dd class="{!! $row_css !!} table-cell hidden-m time">{!! $result->formatResult() !!}</dd>
                    @isset ($result->pts)
                        <dd class="{!! $row_css !!} table-cell pts">{!! Format::pluralise($result->pts, 'pt') !!}</dd>
                    @endisset
                @endforeach

                {{-- Fastest Lap --}}
                @php
                    $fl = $race->getFastestLap($race_num);
                    $row_css = 'row_' . (count($results) % 2);
                @endphp
                @if (isset($fl) && $fl->isset())
                    <dt class="row_head table-cell table-clear fl-title">Fastest Lap</dt>
                        <dd class="{!! $row_css !!} table-cell num">{!! $fl->raceParticipant->raceNumber() !!}</dd>
                        <dd class="{!! $row_css !!} table-cell table-name part-name">{!! $fl->raceParticipant->participant->nameLinkFlag() !!}</dd>
                        <dd class="{!! $row_css !!} table-cell table-name hidden-m team {!! $team_link_css !!}">{!! $fl->raceParticipant->team->nameLinkFlag() !!}</dd>
                        <dd class="{!! $row_css !!} table-cell fl-time">{!! $fl->time !!}</dd>
                        <dd class="{!! $row_css !!} table-cell fl-num">Lap {!! $fl->lap_num !!}</dd>
                @endif
            </dl>
        </fieldset>
    </li>

    {{-- Final Podium --}}
    <li class="grid-cell grid-1-2 grid-tl-1-1 grid-tp-1-1 podium">
        <fieldset class="section hidden-m">
            @include('sports.motorsport.races.view.common.podium', ['podium_subnav' => "result$subnav_key" . (!$is_default ? ' hidden' : '')])
        </fieldset>
    </li>
</ul>
