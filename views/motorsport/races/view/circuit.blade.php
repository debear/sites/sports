@php
    $has_quali_session = $race->qualifying->count();
    $has_sprint_race = $race->quirk('sprint_race');
    $has_sprint_shootout = $race->quirk('sprint_shootout');
@endphp
{{-- Qualifying --}}
@includeWhen($has_quali_session, 'sports.motorsport.races.view.circuit.quali', ['quali_num' => 1])

{{-- Grid --}}
@include('sports.motorsport.races.view.circuit.grid', ['race_num' => 1])

{{-- Result --}}
@include('sports.motorsport.races.view.circuit.result', ['race_num' => 1])

{{-- Lap Leaders --}}
@includeWhen(isset($subnav['list']['laps']), 'sports.motorsport.races.view.circuit.laps', ['race_num' => 1])

{{-- Race 2? --}}
@isset($race->race2_time)
    @includeWhen($has_quali_session && $has_sprint_shootout, 'sports.motorsport.races.view.circuit.quali', ['quali_num' => 2])
    @include('sports.motorsport.races.view.circuit.grid', ['race_num' => 2])
    @include('sports.motorsport.races.view.circuit.result', ['race_num' => 2])
    @include('sports.motorsport.races.view.circuit.laps', ['race_num' => 2])
@endisset

{{-- Championship Standings --}}
@include('sports.motorsport.races.view.common.champ')
