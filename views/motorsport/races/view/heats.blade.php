@php
    $gate_codes = ['', 'R', 'B', 'W', 'Y'];
    $expected_heats = $race->getNumExpectedHeats();
    $heat_results = $race->getGroupedHeatResults();
    $standings_main = $race->getStandingsMain();
    $stats_gates = $race->getMeetingGateStats();
@endphp

{{-- Quali Sprint --}}
@includeWhen($summary['quali_sprint'], 'sports.motorsport.races.view.heats.heat-list', [
    'tab' => 'sprint',
    'type' => 'sprint',
    'expected_heats' => count($race->heats->where('heat_type', 'sprint')->unique('heat')),
    'title_tmpl' => 'Qualifying Sprint${heat}',
])

{{-- Grid --}}
@include('sports.motorsport.races.view.heats.main-grid')

{{-- List --}}
@include('sports.motorsport.races.view.heats.heat-list', ['tab' => 'list', 'type' => 'main', 'title_tmpl' => 'Heat${heat}'])
@include('sports.motorsport.races.view.heats.main-standings')
@include('sports.motorsport.races.view.heats.main-gates')

{{-- Knockout --}}
@includeWhen($summary['inc_ko'], 'sports.motorsport.races.view.heats.knockout')

{{-- Championship Standings --}}
@include('sports.motorsport.races.view.common.champ')
