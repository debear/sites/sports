@php
    $inc_team = FrameworkConfig::get('debear.setup.teams.history');
    $first_round = ($race->round_order == 1);
@endphp
<ul class="inline_list grid subnav-champ hidden standings {!! $first_round ? 'first-round' : '' !!} {!! $inc_team ? 'inc' : 'no' !!}-team">
    <li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        @include('sports.motorsport.races.view.common.champ.table', ['type' => 'participant', 'title' => rtrim(ucfirst(FrameworkConfig::get('debear.setup.participants.url')), 's')])
    </li>
    @if ($inc_team)
        <li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            @include('sports.motorsport.races.view.common.champ.table', ['type' => 'team', 'title' => 'Constructor'])
        </li>
    @endif
</ul>
