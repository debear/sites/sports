@php
    $method = 'get' . ucfirst($type) . 'Standings';
    $standings = $race->$method();
    $separator = DeBear\Helpers\Sports\Motorsport\Setup::standingsSeparator();
@endphp
<fieldset class="section">
    <dl>
        {{-- Header --}}
        <dt class="row_head table-cell table-title">{!! $title !!}s&#39; Standings</dt>

        {{-- Standings --}}
        @foreach ($standings as $row)
            <dt class="row_head table-cell table-clear pos {!! !$first_round && !$row->pos_diff ? 'no-change' : '' !!}">{!! $row->pos !!}</dt>
            @if ($row->pos_diff)
                <dd class="{!! $row->pos_diff > 0 ? 'down' : 'up' !!} table-cell change">
                    <span class="icon_arrow_{!! $row->pos_diff > 0 ? 'down' : 'up' !!}">{!! abs($row->pos_diff) !!}</span>
                </dd>
            @endif
            <dd class="{!! $row->css !!} table-cell table-name participant">{!! $row->$type->nameLinkFlag() !!}</dd>
            @if (!$first_round)
                <dd class="{!! $row->pts_diff ? $row->css : '' !!} table-cell pts">{!! $row->pts_diff ? '+' . Format::pluralise($row->pts_diff, 'pt') : '' !!}</dd>
            @endif
            <dd class="{!! $row->css !!} table-cell pts">{!! Format::pluralise($row->pts, 'pt') !!}</dd>

            {{-- Standings separator? --}}
            @if ($row->pos == $separator)
                <dd class="row_head table-cell table-sep"></dd>
            @endif
        @endforeach
    </dl>
</fieldset>
