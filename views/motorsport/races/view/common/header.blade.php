@php
    $all = \DeBear\Helpers\Sports\Motorsport\Header::getCalendar();
    $race_name = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
    $num_rounds = count($all);
    $num_info_mobile = 1;
    $info_message = $race->infoMessage();
@endphp
<headers>
    <h1 class="hidden-m hidden-tp flag128_{!! $race->flag() !!}">{!! $race->name_full !!}</h1>
    <h1 class="hidden-d hidden-tl flag48_{!! $race->flag() !!}">{!! $race->name_full !!}</h1>
</headers>
<ul class="info">
    {{-- Race info --}}
    <li class="race">
        @if ($race->round_order > 1)
            @php
                $prev = $all->where('round_order', $race->round_order - 1);
                $method = ($prev->canLink() ? 'nameShortLinkFlag' : 'nameShortFlag');
            @endphp
            <span class="race-prev">&laquo; {!! $prev->$method() !!}</span>
        @endif
        <span class="race-num"><strong>{!! $race_name !!} {!! $race->round_order !!} of {!! $num_rounds !!}</strong></span>
        @if ($race->round_order < $num_rounds)
            @php
                $next = $all->where('round_order', $race->round_order + 1);
                $method = ($next->canLink() ? 'nameShortLinkFlag' : 'nameShortFlag');
            @endphp
            <span class="race-next">{!! $next->$method() !!} &raquo;</span>
        @endif
    </li>
    {{-- Per Sport Details --}}
    <li class="info-1">
        @php
            $info = 1;
        @endphp
        @foreach ($race->headerInfo() as $label => $value)
            @php
                if ($value == '-') {
                    print '</li><li class="info-' . (++$info) . ($info > $num_info_mobile ? ' hidden-m' : '') . '">';
                    continue;
                }
                $class = (!is_numeric($label) ? Strings::codify($label) : 'item-' . $loop->index);
            @endphp
            <span class="{!! $class !!}">
                @if (!is_numeric($label))
                    <strong>{!! trim($label) !!}:</strong>
                @endif
                {!! $value !!}
            </span>
        @endforeach
    </li>
    {{-- An info message? --}}
    @if ($info_message)
        <li class="message">
            <div class="box info icon icon_info">{!! $info_message !!}</div>
        </li>
    @endif
</ul>
