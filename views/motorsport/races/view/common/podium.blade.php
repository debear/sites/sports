@php
    $podiums = [2, 1, 3];
@endphp
<dl class="grid subnav-{!! $podium_subnav !!} podium">
    <dt class="grid-cell grid-1-1 row_head">Podium</dt>
    @foreach ($podiums as $pos)
        @php
            $podium = $results->where('pos', $pos)->raceParticipant->participant;
        @endphp
        <dd class="grid-cell grid-1-3 step-{!! $pos !!}">
            <ul class="inline_list">
                <li class="flag flag64_{!! $podium->nationality !!}"></li>
                <li class="mugshot">{!! $podium->image(['size' => 'small']) !!}</li>
                <li class="step name podium-{!! $pos !!}">{!! $podium->name !!}</li>
                <li class="step pos podium-{!! $pos !!}">{!! $pos !!}</li>
            </ul>
        </dd>
    @endforeach
</dl>
