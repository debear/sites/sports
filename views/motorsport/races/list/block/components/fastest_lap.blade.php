@php
    if (!isset($row_css)) {
        $row_css = 'podium-1';
    }
@endphp
<dd class="cell {!! $row_css !!} lap">
    Lap {!! $fl->lap_num !!}
</dd>
<dd class="cell {!! $row_css !!} name fastest-lap">
    {!! $fl->raceParticipant->participant->nameLinkFlag() !!}
</dd>
@if (isset($fl->raceParticipant->team))
    <dd class="cell {!! $row_css !!} team">
        {!! $fl->raceParticipant->team->nameLinkFlag() !!}
    </dd>
@endif
<dd class="cell {!! $row_css !!} time">
    {!! $fl->time !!}
</dd>
