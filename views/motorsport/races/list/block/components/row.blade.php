@php
    // Ensure we have a row-level CSS class
    if (!isset($row_css)) {
        $row_css = 'podium-' . $result->pos();
    }
    // Adapting the participant name field?
    $name_css = [];
    if (isset($no_pos) && $no_pos) {
        $name_css[] = 'no-pos';
    }
    if (!isset($result->raceParticipant->team)) {
        $name_css[] = 'no-team';
    }
@endphp

@if (!isset($no_pos) || !$no_pos)
    <dd class="cell {!! $row_css !!} pos">
        {!! $result->pos() !!}
    </dd>
@endif

<dd class="cell {!! $row_css !!} name {!! join(' ', $name_css) !!}">
    {!! $result->raceParticipant->participant->nameLinkFlag() !!}
</dd>

@if (isset($result->raceParticipant->team))
    <dd class="cell {!! $row_css !!} team {!! FrameworkConfig::get('debear.setup.teams.history') ? 'with' : 'no' !!}-link">
        {!! $result->raceParticipant->team->nameLinkFlag() !!}
    </dd>
@endif

<dd class="cell {!! $row_css !!} result">
    {!! $result->formatResult() !!}
</dd>
