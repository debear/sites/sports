@php
    $podium = $race->getPodium($race_num);
    if ($race_num == 1 || $has_sprint_race) {
        $race_title = '';
        $race_name_link = $race_name;
    } else {
        $race_title = $race_name_link = "$race_name $race_num";
    }
    if (FrameworkConfig::get('debear.setup.type') == 'circuit') {
        $fl = $race->getFastestLap($race_num);
        // What sort of grid do we need?
        if (isset($race->qual_time)) {
            if ($race_num == 1) {
                // The regular fastest-lap-times
                $quali_title = "$race_title Qualifying";
                $grid = $race->getGridFront($race_num);
            } elseif ($has_sprint_race) {
                // A sprint race
                $quali_title = "Sprint Race";
                $grid = $race->getPodium(1);
            }
        }
    }
@endphp

{{-- Race Podium --}}
<dt class="full-width section row_head">{!! $race_title !!} Podium</dt>
    @foreach($podium as $result)
        @include('sports.motorsport.races.list.block.components.row')
    @endforeach

{{-- Is there a qualifying component? --}}
@if (isset($grid) && $grid->isset())
    <dt class="full-width section row_head">{!! $quali_title !!}</dt>
        @foreach($grid as $result)
            @include('sports.motorsport.races.list.block.components.row')
        @endforeach
@endisset

{{-- A fastest lap? --}}
@if (isset($fl) && $fl->isset())
    <dt class="full-width section row_head">{!! $race_title !!} Fastest Lap</dt>
        @include('sports.motorsport.races.list.block.components.fastest_lap', [
            'row_css' => 'row_1',
        ])
@endif

{{-- Link --}}
<dd class="full-width row_0 race-link">
    <a href="{!! $race->link($race_num) !!}">&raquo; Full {!! $race_name_link !!} Coverage</a>
</dd>
