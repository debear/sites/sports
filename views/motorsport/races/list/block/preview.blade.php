{{-- Get the equivalent race from the previous year (which there may not be...) --}}
@php
    $show_prev = ($race->season > FrameworkConfig::get('debear.setup.season.min'));
    $race_prev = $races_prev->where('flag', $race->flag);
    if (!isset($race_prev) || !$race_prev->isComplete()) {
        unset($race_prev);
    }
    // Which race from last year are we displaying?
    if ($show_prev && isset($race_prev)) {
        $race_num = $race_prev->previewRaceNum();
        $podium = $race_prev->getPodium($race_num);
        if (FrameworkConfig::get('debear.setup.type') == 'circuit') {
            $pole = $race_prev->getPolePosition($race_num);
            $fl = $race_prev->getFastestLap($race_num);
        }
    }
@endphp

@if (isset($race->qual_time))
    <dd class="row_0 full-width"><strong>Qualifying Starts:</strong> {!! $race->qual_start_time !!}</dd>
@endif
<dd class="row_1 full-width"><strong>{!! $race_name !!} Starts:</strong> {!! $race->race_start_time !!}</dd>
@if (isset($race->race2_time))
    <dd class="row_0 full-width"><strong>{!! $race_name !!} 2 Starts:</strong> {!! $race->race2_start_time !!}</dd>
@endif

{{-- Previous Race Podium --}}
@if ($show_prev)
    <dt class="full-width section row_head">{!! $race->season - 1 !!} Podium</dt>
        @if (isset($race_prev))
            @foreach($podium as $result)
                @include('sports.motorsport.races.list.block.components.row')
            @endforeach
        @else
            <dd class="not_held full-width">No {!! $race->name_full !!} was held in {!! $race->season - 1 !!}.</dd>
        @endif
@endif

{{-- Is there a qualifying component? --}}
@if ($show_prev && isset($pole) && $pole->isset())
    <dt class="full-width section row_head">{!! $race->season - 1 !!} Pole Position</dt>
        @include('sports.motorsport.races.list.block.components.row', [
            'result' => $pole,
            'no_pos' => true,
            'row_css' => 'row_0',
        ])
@endif

{{-- A fastest lap too? --}}
@if ($show_prev && isset($fl) && $fl->isset())
    <dt class="full-width section row_head">{!! $race->season - 1 !!} Fastest Lap</dt>
        @include('sports.motorsport.races.list.block.components.fastest_lap', [
            'row_css' => 'row_1',
        ])
@endif
