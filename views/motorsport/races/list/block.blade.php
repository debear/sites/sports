<li class="grid-cell">
    <fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                @if ($race->isFirst() && !$race->isComplete() && !$race->isCancelled())
                    First {!! $race_name !!}
                @elseif (!$race->isComplete() && !$race->isCancelled())
                    Next {!! $race_name !!}
                @elseif (!$race->isLast())
                    Previous {!! $race_name !!}
                @else
                    Last {!! $race_name !!}
                @endif
                &ndash;
                {!! $race->nameFlag() !!}
            </dt>

            {{-- Display some relevant data --}}
            @if ($race->isCancelled())
                @include('sports.motorsport.races.list.block.components.cancelled')
            @elseif (!$race->isComplete())
                @include('sports.motorsport.races.list.block.preview')
            @elseif ($race->quirk('sprint_race'))
                @include('sports.motorsport.races.list.block.result', ['has_sprint_race' => true, 'race_num' => 2])
            @else
                @include('sports.motorsport.races.list.block.result', ['has_sprint_race' => false, 'race_num' => 1])
                @isset ($race->race2_time)
                    @include('sports.motorsport.races.list.block.result', ['has_sprint_race' => false, 'race_num' => 2])
                @endisset
            @endif
        </dl>
    </fieldset>
</li>
