{{-- Get the equivalent race from the previous year (which there may not be...) --}}
@php
    $race_prev = $races_prev->where('flag', $race->flag);
    if (isset($race_prev) && !$race_prev->isComplete()) {
        unset($race_prev);
    }
    if (isset($race_prev)) {
        $race_num = $race_prev->previewRaceNum();
        $podium = $race_prev->getPodium($race_num);
        if (FrameworkConfig::get('debear.setup.type') == 'circuit') {
            $pole = $race_prev->getPolePosition($race_num);
            $fl = $race_prev->getFastestLap($race_num);
        }
    }
    // Our race titles
    $race_title = (!$has_sprint_race ? $race_name : "Sprint $race_name");
    $race2_title = (!$has_sprint_race ? "$race_name 2" : "$race_name");
@endphp

{{-- Qualifying --}}
@isset($race->qual_time)
    <dd class="full-width row_0"><strong>Qualifying Starts:</strong> {!! $race->qual_start_time !!}</dd>
@endisset
{{-- Race --}}
<dd class="full-width row_1"><strong>{!! $race_title !!} Starts:</strong> {!! $race->race_start_time !!}</dd>
{{-- Race 2 --}}
@isset($race->race2_time)
    <dd class="full-width row_0"><strong>{!! $race2_title !!} Starts:</strong> {!! $race->race2_start_time !!}</dd>
@endisset

{{-- Previous Race --}}
@isset($race_prev)
    @if ($race_prev->isComplete($race_num))
        {{-- Winner --}}
        <dt class="full-width section row_head">{!! $race_prev->season !!} Podium</dt>
            @foreach($podium as $result)
                @include('sports.motorsport.races.list.table.components.row')
            @endforeach
        {{-- Pole Position --}}
        @if (isset($pole) && $pole->isset())
            <dt class="full-width section row_head">{!! $race_prev->season !!} Pole Position</dt>
                @include('sports.motorsport.races.list.table.components.row', [
                    'result' => $pole,
                    'no_pos' => true,
                    'row_css' => 'row_0',
                ])
        @endisset
        {{-- Fastest Lap --}}
        @if (isset($fl) && $fl->isset())
            <dt class="full-width section row_head">{!! $race_prev->season !!} Fastest Lap</dt>
                @include('sports.motorsport.races.list.table.components.fastest_lap', [
                    'row_css' => 'row_1',
                ])
        @endisset
    @else
        {{-- Race was cancelled --}}
        @include('sports.motorsport.races.list.table.components.cancelled', ['race' => $race_prev])
    @endif
@else
    {{-- No race was held the previous year --}}
    <dd class="not_held full-width">No {!! $race->name_full !!} was held in {!! $race->season - 1 !!}.</dd>
@endisset
