@php
    if (!isset($row_css)) {
        $row_css = 'podium-1';
    }
@endphp
<dd class="cell {!! $row_css !!} name no-pos">
    {!! $fl->raceParticipant->participant->nameLinkFlag() !!}
</dd>
@if (isset($fl->raceParticipant->team))
    <dd class="cell {!! $row_css !!} team">
        {!! $fl->raceParticipant->team->nameLinkFlag() !!}
    </dd>
@endif
