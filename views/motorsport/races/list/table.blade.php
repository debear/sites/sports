@php
    $has_sprint_race = $race->quirk('sprint_race');
    $first_race = ($has_sprint_race || (isset($race->race2_time) && !$race->isCancelled(2) && $race->isComplete(2))) ? 2 : 1;
    $last_race = ($has_sprint_race ? 2 : 1);
@endphp
@for ($num = $first_race; $num >= $last_race; $num--)
    <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">{!! $race->round_order !!}</dt>
                <dt class="row_head head title-row">
                    <name>{!! $race->name_full !!}</name>
                    <info>{!! $race->venue !!}</info>
                </dt>
                <dd class="flag flag48_{!! $race->flag() !!} title-row"></dd>

                {{-- Display some relevant data --}}
                @if ($race->isCancelled())
                    @include('sports.motorsport.races.list.table.components.cancelled')
                @elseif (!$race->isComplete())
                    @include('sports.motorsport.races.list.table.preview')
                @else
                    @include('sports.motorsport.races.list.table.result', ['race_num' => $num])
                @endif
            </dl>
        </fieldset>
    </li>
@endfor
