@php
    $race_name = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1>{!! FrameworkConfig::get('debear.setup.season.viewing') !!} Season Schedule</h1>

<ul class="inline_list block grid">
    {{-- Previous Race Block --}}
    @include('sports.motorsport.races.list.block', ['race' => $race_last])
    {{-- Next Race Block --}}
    @include('sports.motorsport.races.list.block', ['race' => $race_next])
</ul>

{{-- Upcoming Races --}}
@if ($iNumUpcoming = sizeof($races_upc))
    <h2 class="row_head">Upcoming {!! $race_name !!}{!! $iNumUpcoming > 1 ? 's' : '' !!}</h2>
    <ul class="inline_list list upcoming grid">
        {{-- @foreach to allow $races_prev to propagate --}}
        @foreach ($races_upc as $race)
            @include('sports.motorsport.races.list.table')
        @endforeach
    </ul>
@endif
{{-- Completed Races --}}
@if ($iNumCompleted = sizeof($races_cmpl))
    {{-- Display a heading to separate sections... if we're displaying both --}}
    @if (sizeof($races_upc))
        <h2 class="row_head">Completed {!! $race_name !!}{!! $iNumCompleted > 1 ? 's' : '' !!}</h2>
    @endif
    <ul class="inline_list list completed grid">
        @foreach ($races_cmpl as $race)
            @include('sports.motorsport.races.list.table')
        @endforeach
    </ul>
@endif

@endsection
