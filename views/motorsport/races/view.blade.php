@extends('skeleton.layouts.html')

@section('content')

@include('sports.motorsport.races.view.common.header')

@include('skeleton.widgets.subnav')

@include("sports.motorsport.races.view.$type")

@endsection
