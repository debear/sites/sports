@php
    $num_races = \DeBear\Helpers\Sports\Motorsport\Header::getNumRaces();
    $short_summary = ($num_races >= 22); // If there's 22 races or more, we need to shorten the summary
@endphp
<ul class="inline_list nav_calendar num_{!! $num_races !!}">
    @foreach($races as $race)
        @php
            $sprint_race = $race->quirk('sprint_race');
        @endphp
        {{-- Always a "Race 1" --}}
        @include('sports.motorsport.widgets.calendar.item', ['race_num' => (!$sprint_race ? 1 : 2), 'short_summary' => $short_summary])
        {{-- Is there a "Race 2"? --}}
        @if (isset($race->race2_time) && !$sprint_race)
            @include('sports.motorsport.widgets.calendar.item', ['race_num' => 2, 'short_summary' => $short_summary])
        @endif
    @endforeach
</ul>
