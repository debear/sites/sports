@php
    $url = ($race->canLink() ? $race->link($race_num) : false);
    $tag = ($url ? 'a' : 'flag');
    $focusing_on = \DeBear\Helpers\Sports\Motorsport\Header::getRaceFocus();
    $dim = ($focusing_on ? ($race->round != $focusing_on) : !$url);
@endphp
<li>
    <{!! $tag !!} {!! $url ? 'href="' . $url . '"' : '' !!} class="flag {!! $race->flag() !!} {!! $dim ? 'dim' : '' !!}"></{!! $tag !!}>
    <info>{!! $race->summary($race_num, $short_summary) !!}</info>
</li>
