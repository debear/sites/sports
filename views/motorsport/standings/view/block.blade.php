@php
    $is_constructor = ($type == 'constructors');
    $separator = \DeBear\Helpers\Sports\Motorsport\Setup::standingsSeparator();
    $race_obj = [];
    foreach ($races->pluck('round') as $id => $round) {
        $race_obj[$round] = $races->getRow($id);
    }
    $result_loops = [];
@endphp
<standings class="clearfix {!! $type !!}">
    {{-- Main Info --}}
    <main>
        <flags>
            <type class="row_head">{!! $title !!}</type>
            <total class="row_head {!! !$is_constructor ? 'sel sortable' : '' !!}" data-round="0">Total</total>
        </flags>
        @foreach ($list as $row)
            @php
                $standings = $row->standingsCurr;
                // First pass, so do some setup work
                $row_css = $standings->css();
                $result_loops[$loop->index] = [
                    'css' => $row_css,
                    'loops' => [],
                ];
                $trophies = $row->results->getTrophyCounts();

                // Determine what we're looping over (single participant or team of participants?)
                if ($is_constructor) {
                    // Team, so determine the appropriate participants and create a loop for each
                    $result_loops[$loop->index]['loops'] = $row->getParticipantLoops();
                } else {
                    // Participant, so a single loop
                    $result_loops[$loop->index]['loops'] = [$row->results];
                }
            @endphp
            <row class="rows-{!! count($result_loops[$loop->index]['loops']) !!} {!! $is_constructor ? 'split' : '' !!}" data-id="{!! $row->id !!}" data-pos="{!! $standings->pos !!}">
                <pos class="full {!! $row_css !!}">{!! $standings->pos !!}</pos>
                <name class="full {!! $row_css !!}">
                    {!! $row->nameLinkFlag() !!}
                    {!! $row->extraInfo() !!}
                    @if (count($trophies))
                        <trophies>
                            @foreach ($trophies as $key => $num)
                                <span class="icon_right_standings_trophy{!! $key !!}">{!! $num > 1 ? $num . 'x' : '' !!}</span>
                            @endforeach
                        </trophies>
                    @endif
                </name>
                <total class="full {!! $row_css !!}">{!! $standings->displayPtsFull() !!}</total>
                @foreach ($result_loops[$loop->index]['loops'] as $ref => $results)
                    @if (is_string($ref))
                        <row>
                            <participant class="{!! $result_loops[$loop->parent->index]['css'] !!}">{!! $ref !!}</participant>
                        </row>
                    @endif
                @endforeach
            </row>

            {{-- Standings separator? --}}
            @if ($standings->pos == $separator)
                <separator data-id="sep" data-pos="{!! $separator + 0.5 !!}"></separator>
            @endif
        @endforeach
    </main>

    {{-- Race Results --}}
    <races>
        <racelist>
            <flags>
                @foreach ($races as $race)
                    @php
                        $sprint_race = $race->quirk('sprint_race');
                    @endphp
                    <race class="row_head flag16_{!! $race->flag() !!} {!! !$is_constructor && $race->isComplete(1) ? 'sortable' : '' !!}" data-round="{!! $race->round !!}-{!! !$sprint_race ? 1 : 2 !!}"></race>
                    @if (!$sprint_race && isset($race->race2_time))
                        <race class="row_head flag16_{!! $race->flag() !!} {!! !$is_constructor && $race->isComplete(2) ? 'sortable' : '' !!}" data-round="{!! $race->round !!}-2"></race>
                    @endif
                @endforeach
            </flags>

            @foreach ($list as $row)
                @php
                    $standings = $row->standingsCurr;
                @endphp
                <row class="rows-{!! count($result_loops[$loop->index]['loops']) !!} outer" data-id="{!! $row->id !!}" data-pos="{!! $standings->pos !!}">
                    @foreach ($result_loops[$loop->index]['loops'] as $ref => $results)
                        @if (is_string($ref))
                            <row>
                        @endif
                        @foreach ($races as $race)
                            @php
                                $has_sprint_race = $race->quirk('sprint_race');
                                $first_race = ($has_sprint_race ? 2 : 1); // Skip the sprint race.
                                $last_race = (isset($race->race2_time) ? 2 : 1);
                            @endphp
                            @for ($race_num = $first_race; $race_num <= $last_race; $race_num++)
                                @php
                                    $result = $results->where('race_key', "{$race->round}-$race_num");
                                    // Do we have a position?
                                    $pos = $result->pos();
                                    // Summarise the points if there's a sprint race
                                    $pts = $result->displayPts();
                                    if ($has_sprint_race) {
                                        $race1_pts = $results->where('race_key', "{$race->round}-1")->displayPts();
                                        if (is_numeric($race1_pts)) {
                                            $pts = (int)$pts + $race1_pts;
                                        }
                                    }
                                    // Determine the grid data attribute (which depends on the race type)
                                    $grid = ($is_circuit ? 'data-grid="' . $result->grid_pos . '"' : '');
                                @endphp
                                <race class="race-cell {!! $result->css($race_obj[$race->round]) !!}" data-round="{!! $pos ? "{$result->round}-$race_num" : '' !!}" data-pos="{!! $pos !!}" data-pts="{!! $pts !!}" data-res="{!! $result->displayResult() !!}" {!! $grid !!}>{!! $pts !!}</race>
                            @endfor
                        @endforeach
                        @if (is_string($ref))
                            </row>
                        @endif
                    @endforeach
                </row>

                {{-- Standings separator? --}}
                @if ($standings->pos == $separator)
                    <separator data-id="sep" data-pos="{!! $separator + 0.5 !!}"></separator>
                @endif
            @endforeach
        </racelist>
    </races>
</standings>
