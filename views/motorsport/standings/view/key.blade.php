{{-- Colour key --}}
<dl class="toggles display_key">
    <dt>Key:</dt>
        <dd class="show unsel" data-show="1">Show</dd>
        <dd class="hide sel" data-show="0">Hide</dd>
        <dd class="list hidden">
            <ul class="results_key inline_list">
                <li class="res-pos_1">Winner</li>
                <li class="res-pos_2">Runner-Up</li>
                <li class="res-pos_3">Third</li>
                @if ($is_circuit)
                    <li class="res-pts_finish">Points Finish</li>
                    <li class="res-non_pts_finish">Non-Points Finish</li>
                    <li class="res-not_clsfd">Not Classified</li>
                @else
                    <li class="res-pts_final">Finalist</li>
                    <li class="res-pts_semi">Semi-Finalist</li>
                    <li class="res-pts_finish">Points Scored</li>
                    <li class="res-nc">Not Classified</li>
                @endif
                <li class="res-dsq">Disqualified</li>
                @if ($is_circuit)
                    <li class="res-dns">Did Not Start</li>
                @endif
                <li class="res-not_yet_raced">Race To Be Held</li>
                <li class="res-no_race">Did Not Race</li>
                @if ($is_circuit)
                    <li class="res-fast_lap">Fastest Lap (FL)</li>
                @endif
            </ul>
        </dd>
</dl>

{{-- Data Toggle --}}
<dl class="toggles display_field">
    <dt>Display:</dt>
        <dd data-attrib="pts" class="sel">Points Scored</dd>
        <dd data-attrib="res" class="unsel">{!! Format::singularise(FrameworkConfig::get('debear.links.races.label')) !!} Result</dd>
        @if ($is_circuit)
            <dd data-attrib="grid" class="unsel">Grid Pos</dd>
        @endif
</dl>
