@php
    $inc_teams = isset($teams);
    $is_circuit = (FrameworkConfig::get('debear.setup.type') == 'circuit');
    $participant_title = rtrim(FrameworkConfig::get('debear.setup.participants.url'), 's');
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1><span class="hidden-tp hidden-m">{!! FrameworkConfig::get('debear.setup.season.viewing') !!}</span> <span class="hidden-m">{!! FrameworkConfig::get('debear.names.section') !!}</span> Championship Standings</h1>

<tables class="key-handler races-{!! \DeBear\Helpers\Sports\Motorsport\Header::getNumRaces() !!}">
    {{-- Participants --}}
    @if ($inc_teams)
        <h2>{!! ucfirst($participant_title) !!}s&#39; Standings</h2>
    @endif
    @include('sports.motorsport.standings.view.block', [
        'type' => 'participants',
        'title' => ucfirst($participant_title),
        'list' => $participants,
    ])

    {{-- The key --}}
    @include('sports.motorsport.standings.view.key')

    {{-- Teams? --}}
    @if ($inc_teams)
        <h2>Constructors&#39; Standings</h2>
        @include('sports.motorsport.standings.view.block', [
            'type' => 'constructors',
            'title' => 'Constructor',
            'list' => $teams,
        ])
    @endisset
</tables>

@endsection
