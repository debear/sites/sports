@php
    $default_stat_id = FrameworkConfig::get('debear.setup.stats.default');
    $stats_meta = $stats_meta->whereIn('stat_id', $stats[array_key_last($stats)]->unique('stat_id'));
    $total_stats = $stats_meta->count();
    $default_pos = array_search($default_stat_id, array_values($stats_meta->pluck('stat_id'))) + 1;
    $row_attrib = [];
@endphp
<scrolltable class="scroll-x scrolltable-{!! $type !!} stats_{!! $total_stats !!}" data-default="{!! $default_pos !!}" data-total="{!! $total_stats !!}" data-current="{!! $default_stat_id !!}">
    <mobile-select>
        <current class="row_head">{!! $stats_meta->getRow($default_stat_id)->name_full !!}</current>
        <select>
            @php
                $last_section = '';
                foreach ($stats_meta as $stat) {
                    if ($stat->section != $last_section) {
                        if ($last_section) {
                            print '</optgroup>';
                        }
                        print '<optgroup label="' . FrameworkConfig::get('debear.setup.stats.groups.' . $stat->section) . '">';
                    }
                    $last_section = $stat->section;
                    print '<option value="' . $stat->stat_id . '"'
                        . ($stat->stat_id == $default_stat_id ? ' selected="selected"' : '')
                        . '>' . $stat->name_full . '</option>';
                }
                print '</optgroup>';
            @endphp
        </select>
    </mobile-select>
    <main>
        {{-- Header --}}
        <top>
            <cell class="row_head group">{!! $name !!}</cell>
        </top>
        {{-- Entities --}}
        @php
            $pos_cache = [];
        @endphp
        @foreach ($stats as $row)
            @php
                // Build up the data attributes
                $id = $row->first()->entity_id;
                $row_attrib[$id] = [];
                foreach ($row as $stat) {
                    $row_attrib[$id][] = 'data-sort-' . $stat->stat_id . '="' . $stat->pos . ':' . intval($stat->renderable()) . '"';
                };
                $stat_row = $row->where('stat_id', $default_stat_id);
            @endphp
            <row {!! join(' ', $row_attrib[$id]) !!}>
                @php
                    $pos = $stat_row->pos;
                    if (!isset($pos_cache[$pos]) || !$pos_cache[$pos]) {
                        $pos_cache[$pos] = true;
                    } else {
                        $pos = '=';
                    }
                @endphp
                <cell class="row_head pos sort_order">{!! $pos !!}</cell>
                <cell class="row_{!! $loop->iteration % 2 !!} entity">{!! $stat_row->entity->nameLinkFlag() !!}</cell>
            </row>
        @endforeach
    </main>
    <data>
        <datalist>
            {{-- Header --}}
            <top>
                @php
                    // Build the components
                    $header = [
                        'groups' => [],
                        'names' => [],
                        'sorts' => [],
                    ];
                    $last_section = '';
                    foreach ($stats_meta as $stat) {
                        $is_first = (!$last_section ? 'first' : '');
                        // Group?
                        if ($stat->section != $last_section) {
                            $header['groups'][$stat->section] = [
                                'count' => 1,
                                'cell' => '<cell class="row_head ' . $is_first . ' group stats_{count}">' . FrameworkConfig::get('debear.setup.stats.groups.' . $stat->section) . '</cell>'
                            ];
                        } else {
                            $header['groups'][$stat->section]['count']++;
                        }
                        $last_section = $stat->section;
                        // Name
                        $header['names'][] = '<cell class="row_head ' . $is_first . ' name"><abbr title="' . $stat->name_full . '">' . $stat->name_short . '</abbr></cell>';
                        // Sort
                        $up_type = ($stat->stat_id != $default_stat_id ? ' unsel' : '_disabled sel');
                        $down_type = ' unsel';
                        $header['sorts'][] = '<cell class="row_head ' . $is_first . ' sort" data-sort="' . $stat->stat_id . '">'
                            . '<span class="icon_sort_up' . $up_type . '" data-dir="asc"></span>'
                            . '<span class="icon_sort_down' . $down_type . '" data-dir="desc"></span>'
                            . '</cell>';
                    }
                @endphp
                {{-- Display Groups --}}
                @foreach ($header['groups'] as $group)
                    {!! str_replace('{count}', $group['count'], $group['cell']) !!}
                @endforeach
                {{-- Display Names --}}
                {!! join($header['names']) !!}
                {{-- Display Sorts --}}
                {!! join($header['sorts']) !!}
            </top>
            {{-- Entities --}}
            @foreach ($stats as $row)
                @php
                    $row_css = 'row_' . ($loop->iteration % 2);
                @endphp
                <row {!! join(' ', $row_attrib[$row->first()->entity_id]) !!}>
                    @foreach ($row->sortBy('disp_order') as $stat)
                        <cell class="{!! $row_css !!} {!! $stat->valueCSS() !!} {!! $stat->stat_id != $default_stat_id ? 'hidden-m' : '' !!}" data-field="{!! $stat->stat_id !!}">{!! $stat->renderedTabular() !!}</cell>
                    @endforeach
                </row>
            @endforeach
        </datalist>
    </data>
</scrolltable>
