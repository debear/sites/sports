@php
    $inc_teams = isset($teams);
@endphp
@extends(\DeBear\Helpers\Sports\Common\ManageViews::getExtends())

@section(\DeBear\Helpers\Sports\Common\ManageViews::getSection())

<h1><span class="hidden-m">{!! FrameworkConfig::get('debear.setup.season.viewing') !!}</span> Stat Leaders</h1>

@if (count($subnav['list']) > 1)
    @include('skeleton.widgets.subnav')
@endif

<div class="stats">
    @foreach ($subnav['list'] as $key => $name)
        <div class="subnav-{!! $key !!} stats-table {!! !$loop->first ? 'hidden' : '' !!} clearfix">
            @includeWhen(count(${$key}) > 0, 'sports.motorsport.stats.view.block', ['stats' => $$key, 'type' => Format::singularise($key)])
            @includeWhen(!count(${$key}), 'sports.motorsport.stats.view.no-stats')
        </div>
    @endforeach
</div>

@endsection
