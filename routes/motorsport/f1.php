<?php

/**
 * Routes for the F1 site
 */

Route::prefix('f1')->group(function () {
    /* The homepage */
    Route::get('/', 'Sports\Motorsport@index');

    /* News */
    Route::get('/news', 'Sports\Common\News@index');
    // Legacy migration (which has always been season agnostic).
    Route::get('/{season}/news', function ($season) {
        return redirect("/f1/news", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/news-{season}', function ($season) {
        return redirect("/f1/news", 308);
    })->where('season', '20[0-3]\d');

    /* Calendar */
    Route::get('/races/{season?}/{mode?}', 'Sports\Motorsport\Races@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/races', function ($season) {
        return redirect("/f1/races/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/races-{season}', function ($season) {
        return redirect("/f1/races/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Race */
    Route::get('/races/{season}-{race}', 'Sports\Motorsport\Races@show')
        ->where('season', '20[0-3]\d');
    // Legacy migration (which handles the redirect/404 for us).
    Route::get('/{season}/race-{round}', 'Sports\Motorsport\Races@legacy')
        ->where('season', '20[0-3]\d')
        ->where('round', '\d{1,2}');
    Route::get('/race-{season}-{round}', 'Sports\Motorsport\Races@legacy')
        ->where('season', '20[0-3]\d')
        ->where('round', '\d{1,2}');
    Route::get('/race-{round}', 'Sports\Motorsport\Races@legacyCurrentSeason')
        ->where('round', '\d{1,2}');

    /* Standings */
    Route::get('/standings/{season?}/{mode?}', 'Sports\Motorsport\Standings@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/standings', function ($season) {
        return redirect("/f1/standings/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/standings-{season}', function ($season) {
        return redirect("/f1/standings/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Stat Leaders */
    Route::get('/stats/{season?}/{mode?}', 'Sports\Motorsport\Stats@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/stats', function ($season) {
        return redirect("/f1/stats/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/stats-{season}', function ($season) {
        return redirect("/f1/stats/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Team List */
    Route::get('/teams/{season?}/{mode?}', 'Sports\Motorsport\Participants@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/teams', function ($season) {
        return redirect("/f1/teams/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/teams-{season}', function ($season) {
        return redirect("/f1/teams/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Team */
    Route::get('/teams/{team}/{season?}', 'Sports\Motorsport\Teams@show')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/team-{team_id}', 'Sports\Motorsport\Teams@showLegacy')
        ->where('season', '20[0-3]\d')
        ->where('team_id', '\d{1,2}');
    Route::get('/team-{season}-{team_id}', 'Sports\Motorsport\Teams@showLegacy')
        ->where('season', '20[0-3]\d')
        ->where('team_id', '\d{1,2}');
    Route::get('/team-{team_id}', 'Sports\Motorsport\Teams@showLegacyCurrentSeason')
        ->where('team_id', '\d{1,2}');

    /* Driver */
    Route::get('/drivers/{participant_name}-{participant_id}/{season?}', 'Sports\Motorsport\Participants@show')
        ->where('participant_name', '[a-z\-]+')
        ->where('participant_id', '\d{1,3}')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/driver-{participant_id}', 'Sports\Motorsport\Participants@showLegacy')
        ->where('season', '20[0-3]\d')
        ->where('participant_id', '\d{1,3}');
    Route::get('/driver-{participant_id}', 'Sports\Motorsport\Participants@showLegacyCurrentSeason')
        ->where('participant_id', '\d{1,3}');

    /* Sitemap */
    Route::get('/sitemap/{season?}', 'Sports\Motorsport\SiteTools@sitemap')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/sitemap', function ($season) {
        return redirect("/f1/sitemap/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/sitemap-{season}', function ($season) {
        return redirect("/f1/sitemap/$season", 308);
    })->where('season', '20[0-3]\d');
    // XML version.
    Route::get('/sitemap.xml', 'Sports\Motorsport\SiteTools@sitemapXml');

    /* Site Tools */
    Route::get('/manifest.json', 'Skeleton\SiteTools@manifest');

    /* Ensure the 404 appears correctly */
    Route::fallback(
        function () {
            DeBear\Helpers\Sports\Motorsport\Header::setup();
            return response(view('errors.404'), 404);
        }
    );
});
