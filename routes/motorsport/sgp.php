<?php

/**
 * Routes for the Speedway Grand Prix site
 */

Route::prefix('sgp')->group(function () {
    /* The homepage */
    Route::get('/', 'Sports\Motorsport@index');

    /* Calendar */
    Route::get('/meetings/{season?}/{mode?}', 'Sports\Motorsport\Races@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/meetings', function ($season) {
        return redirect("/sgp/meetings/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Meeting */
    Route::get('/meetings/{season}-{race}', 'Sports\Motorsport\Races@show')
        ->where('season', '20[0-3]\d');
    // Legacy migration (which handles the redirect/404 for us).
    Route::get('/{season}/meeting-{round}', 'Sports\Motorsport\Races@legacy')
        ->where('season', '20[0-3]\d')
        ->where('round', '\d{1,2}');
    Route::get('/meeting-{season}-{round}', 'Sports\Motorsport\Races@legacy')
        ->where('season', '20[0-3]\d')
        ->where('round', '\d{1,2}');
    Route::get('/meeting-{round}', 'Sports\Motorsport\Races@legacyCurrentSeason')
        ->where('round', '\d{1,2}');

    /* Standings */
    Route::get('/standings/{season?}/{mode?}', 'Sports\Motorsport\Standings@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/standings', function ($season) {
        return redirect("/sgp/standings/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/standings-{season}', function ($season) {
        return redirect("/sgp/standings/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Stat Leaders */
    Route::get('/stats/{season?}/{mode?}', 'Sports\Motorsport\Stats@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/stats', function ($season) {
        return redirect("/sgp/stats/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/stats-{season}', function ($season) {
        return redirect("/sgp/stats/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Rider List */
    Route::get('/riders/{season?}/{mode?}', 'Sports\Motorsport\Participants@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/riders', function ($season) {
        return redirect("/sgp/riders/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/riders-{season}', function ($season) {
        return redirect("/sgp/riders/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Rider */
    Route::get('/riders/{participant_name}-{participant_id}/{season?}', 'Sports\Motorsport\Participants@show')
        ->where('participant_name', '[a-z\-]+')
        ->where('participant_id', '\d{1,3}')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/rider-{participant_id}', 'Sports\Motorsport\Participants@showLegacy')
        ->where('season', '20[0-3]\d')
        ->where('participant_id', '\d{1,3}');
    Route::get('/rider-{participant_id}', 'Sports\Motorsport\Participants@showLegacyCurrentSeason')
        ->where('participant_id', '\d{1,3}');

    /* Sitemap */
    Route::get('/sitemap/{season?}', 'Sports\Motorsport\SiteTools@sitemap')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/sitemap', function ($season) {
        return redirect("/sgp/sitemap/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/sitemap-{season}', function ($season) {
        return redirect("/sgp/sitemap/$season", 308);
    })->where('season', '20[0-3]\d');
    // XML version.
    Route::get('/sitemap.xml', 'Sports\Motorsport\SiteTools@sitemapXml');

    /* Site Tools */
    Route::get('/manifest.json', 'Skeleton\SiteTools@manifest');

    /* Ensure the 404 appears correctly */
    Route::fallback(
        function () {
            DeBear\Helpers\Sports\Motorsport\Header::setup();
            return response(view('errors.404'), 404);
        }
    );
});
