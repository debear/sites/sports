<?php

/**
 * Routes for the MLB site
 */

Route::prefix('mlb')->group(function () {
    /* The homepage */
    Route::get('/', 'Sports\MajorLeague@index');
    // Legacy migration.
    Route::get('/index', function () {
        return redirect("/mlb", 308);
    });
    Route::get('/{season}', function ($season) {
        return redirect("/mlb", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/{season}/index', function ($season) {
        return redirect("/mlb", 308);
    })->where('season', '20[0-3]\d');

    /* Schedule */
    Route::get('/schedule/{date?}/{mode?}', 'Sports\MajorLeague\Schedule@index')
        ->where('date', '20[0-3]\d[01]\d[0-3]\d')
        ->where('mode', '(body|switcher)');
    Route::get('/schedule/{season}/body', 'Sports\MajorLeague\Schedule@seasonSwitcher')
        ->where('season', '20[0-3]\d');
    Route::get('/schedule/{season}/calendar', 'Sports\MajorLeague\Schedule@calendarExceptions')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/schedule', 'Sports\MajorLeague\Schedule@legacyIndex')
        ->where('season', '20[0-3]\d');
    Route::get('/schedule-{date}', function ($date) {
        return redirect("/mlb/schedule/$date", 308);
    })->where('date', '20[0-3]\d[01]\d[0-3]\d');
    Route::get('/{season}/schedule-{date}', function ($season, $date) {
        return redirect("/mlb/schedule/$date", 308);
    })->where(['season' => '20[0-3]\d', 'date' => '20[0-3]\d[01]\d[0-3]\d']);

    /* Probables */
    Route::get('/probables/{date?}/{mode?}', 'Sports\MLB\Probables@index')
        ->where('date', '20[0-3]\d[01]\d[0-3]\d')
        ->where('mode', '(body|switcher)');
    Route::get('/probables/{season}/body', 'Sports\MLB\Probables@seasonSwitcher')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/probables', function ($season) {
        return redirect("/mlb/probables", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/probables-{date}', function ($date) {
        return redirect("/mlb/probables/$date", 308);
    })->where('date', '20[0-3]\d[01]\d[0-3]\d');
    Route::get('/{season}/probables-{date}', function ($season, $date) {
        return redirect("/mlb/probables/$date", 308);
    })->where(['season' => '20[0-3]\d', 'date' => '20[0-3]\d[01]\d[0-3]\d']);

    /* Starting Lineups */
    Route::get('/lineups/{date?}/{mode?}', 'Sports\MajorLeague\StartingLineups@index')
        ->where('date', '20[0-3]\d[01]\d[0-3]\d')
        ->where('mode', '(body|switcher)');
    Route::get('/lineups/{season}/body', 'Sports\MajorLeague\StartingLineups@seasonSwitcher')
        ->where('season', '20[0-3]\d');

    /* Game */
    Route::get('/schedule/{date}/{away}-at-{home}-{game_type}{game_id}', 'Sports\MajorLeague\Schedule@show')
        ->where('date', '20[0-3]\d[01]\d[0-3]\d')
        ->where('away', '[a-z0-9\-]+')
        ->where('home', '[a-z0-9\-]+')
        ->where('game_type', '[rp]')
        ->where('game_id', '\d{1,4}');
    // Legacy migration.
    Route::get('/game-{game_type}{game_id}', function ($game_type, $game_id) {
        return redirect("/mlb/schedule", 308);
    })->where(['game_type' => '[rp]', 'game_id' => '\d+']);
    Route::get('/{season}/game-{game_type}{game_id}', 'Sports\MajorLeague\Schedule@legacyShow')
        ->where('season', '20[0-3]\d')
        ->where('game_type', '[rp]')
        ->where('game_id', '\d+');
    Route::get('/game-{game_type}{game_id}-{detail}', function ($game_type, $game_id, $detail) {
        return redirect("/mlb/schedule", 308);
    })->where(['game_type' => '[rp]', 'game_id' => '\d+', 'detail' => '[a-z]+']);
    Route::get('/{season}/game-{game_type}{game_id}-{detail}', 'Sports\MajorLeague\Schedule@legacyShow')
        ->where('season', '20[0-3]\d')
        ->where('game_type', '[rp]')
        ->where('game_id', '\d+')
        ->where('detail', '[a-z]+');

    /* Playoffs */
    Route::get('/playoffs/{season?}/{mode?}', 'Sports\MajorLeague\Playoffs@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    Route::get('/playoffs/{season}/{higher}-{lower}-{code}', 'Sports\MajorLeague\Playoffs@show')
        ->where('season', '20[0-3]\d')
        ->where('higher', '[^\-]+')
        ->where('lower', '[^\-]+')
        ->where('code', '\d{2}');
    // Legacy migration.
    Route::get('/{season}/playoffs', function ($season) {
        return redirect("/mlb/playoffs/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/playoffs/{code}', function ($code) {
        return redirect("/mlb/playoffs", 308);
    })->where(['season' => '20[0-3]\d', 'code' => '\d{2}']);
    Route::get('/{season}/playoffs/{code}', 'Sports\MajorLeague\Playoffs@legacy')
        ->where('season', '20[0-3]\d')
        ->where('code', '\d{2}');

    /* Standings */
    Route::get('/standings/{season?}/{mode?}', 'Sports\MajorLeague\Standings@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/standings', function ($season) {
        return redirect("/mlb/standings/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/standings/{type}', function ($type) {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        return redirect("/mlb/standings/$season", 308);
    })->where('type', 'expanded|league');
    Route::get('/{season}/standings/{type}', function ($season, $type) {
        return redirect("/mlb/standings/$season", 308);
    })->where(['season' => '20[0-3]\d', 'type' => 'expanded|league']);

    /* News */
    Route::get('/news', 'Sports\Common\News@index');
    // Legacy migration.
    Route::get('/{season}/news', function ($season) {
        return redirect("/mlb/news", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/news-articles', function () {
        return redirect("/mlb/news", 308);
    });
    Route::get('/{season}/news-articles', function ($season) {
        return redirect("/mlb/news", 308);
    })->where('season', '20[0-3]\d');

    /* Stats */
    Route::get('/stats', 'Sports\MajorLeague\Stats@index');
    Route::get('/stats/{type}s/{class}', 'Sports\MajorLeague\Stats@show')
        ->where('type', 'player|team');
    // Legacy migration.
    Route::get('/{season}/stats', function ($season) {
        return redirect("/mlb/stats", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/stats-{type}-{category}', 'Sports\MajorLeague\Stats@legacyRedirectNoSeason')
        ->where(['type' => 'ind|team', 'category' => '[a-z_]+']);
    Route::get('/{season}/stats-{type}-{category}', 'Sports\MajorLeague\Stats@legacyRedirectWithSeason')
        ->where(['season' => '20[0-3]\d', 'type' => 'ind|team', 'category' => '[a-z_]+']);
    // Legacy team stat migration.
    Route::get('/stats-{team_id}', 'Sports\MajorLeague\Stats@legacyTeamNoSeason')
        ->where('team_id', '[A-Z]{2,3}');
    Route::get('/stats-{team_id}-{category}', 'Sports\MajorLeague\Stats@legacyTeamNoSeason')
        ->where(['team_id' => '[A-Z]{2,3}', 'category' => '[a-z_]+']);
    Route::get('/{season}/stats-{team_id}', 'Sports\MajorLeague\Stats@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}']);
    Route::get('/{season}/stats-{team_id}-{category}', 'Sports\MajorLeague\Stats@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}', 'category' => '[a-z_]+']);

    /* Teams */
    Route::get('/teams', 'Sports\MajorLeague\Teams@index');
    Route::get('/teams/{name}', 'Sports\MajorLeague\Teams@show')
        ->where('name', '[a-z0-9\-]+');
    Route::get('/teams/{name}/{tab}/{season?}', 'Sports\MajorLeague\Teams@tab')
        ->where('name', '[a-z0-9\-]+')
        ->where('tab', '[a-z\-]+')
        ->where('season', '20[0-3]\d');
    // Favourites.
    Route::post('/teams/{name}/favourite', 'Sports\MajorLeague\TeamFavourites@store')
        ->where('name', '[a-z0-9\-]+')
        ->middleware('auth:user');
    Route::delete('/teams/{name}/favourite', 'Sports\MajorLeague\TeamFavourites@destroy')
        ->where('name', '[a-z0-9\-]+')
        ->middleware('auth:user');
    // Legacy migration.
    Route::get('/{season}/teams', function ($season) {
        return redirect("/mlb/teams", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/team-{team_id}', 'Sports\MajorLeague\Teams@legacyTeamNoSeason')
        ->where('team_id', '[A-Z]{2,3}');
    Route::get('/team-{team_id}-{page}', 'Sports\MajorLeague\Teams@legacyTeamNoSeason')
        ->where(['team_id' => '[A-Z]{2,3}', 'page' => '[a-z]+']);
    Route::get('/{season}/team-{team_id}', 'Sports\MajorLeague\Teams@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}']);
    Route::get('/{season}/team-{team_id}-{page}', 'Sports\MajorLeague\Teams@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}', 'page' => '[a-z]+']);

    /* Players */
    Route::get('/players', 'Sports\MajorLeague\Players@index');
    Route::get('/players/search/{term}', 'Sports\MajorLeague\Players@search');
    Route::get('/players/{name}-{player_id}', 'Sports\MajorLeague\Players@show')
        ->where('name', '[a-z\-]+')
        ->where('player_id', '\d+');
    Route::get('/players/{name}-{player_id}/{tab}', 'Sports\MajorLeague\Players@tab')
        ->where('name', '[a-z\-]+')
        ->where('player_id', '\d+')
        ->where('tab', '[a-z\-]+');
    // Legacy migration.
    Route::get('/{season}/players', function ($season) {
        return redirect('/mlb/players', 308);
    })->where('season', '20[0-3]\d');
    Route::get('/player-{player_id}', 'Sports\MajorLeague\Players@legacy')
        ->where('player_id', '\d+');
    Route::get('/player-{player_id}-{detail}', 'Sports\MajorLeague\Players@legacy')
        ->where('player_id', '\d+')
        ->where('detail', '[a-z]+');
    Route::get('/{season}/player-{player_id}', 'Sports\MajorLeague\Players@legacy')
        ->where('season', '20[0-3]\d')
        ->where('player_id', '\d+');
    Route::get('/{season}/player-{player_id}-{detail}', 'Sports\MajorLeague\Players@legacy')
        ->where('season', '20[0-3]\d')
        ->where('player_id', '\d+')
        ->where('detail', '[a-z]+');

    /* Draft */
    Route::get('/draft/{season?}/{mode?}', 'Sports\MLB\Drafts@index')
        ->where('season', '(19|20)\d{2}')
        ->where('mode', 'body');

    /* Awards */
    Route::get('/awards', 'Sports\MajorLeague\Awards@index');
    Route::get('/awards/{award_name}-{award_id}', 'Sports\MajorLeague\Awards@show')
        ->where('award_name', '[a-z\-]+')
        ->where('award_id', '\d+');

    /* Power Ranks */
    Route::get('/power-ranks/{season?}/{week?}', 'Sports\MajorLeague\PowerRanks@index')
        ->where('season', '20[0-3]\d')
        ->where('week', '\d+');
    // Legacy migration.
    Route::get('/{season}/power-ranks', function ($season) {
        return redirect("/mlb/power-ranks/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/power-ranks-{week}', function ($week) {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        return redirect("/mlb/power-ranks/$season/$week", 308);
    })->where('week', '\d+');
    Route::get('/{season}/power-ranks-{week}', function ($season, $week) {
        return redirect("/mlb/power-ranks/$season/$week", 308);
    })->where(['season' => '20[0-3]\d', 'week' => '\d+']);

    /* Admin */
    Route::get('/manage/logos', 'Sports\MajorLeague\Admin@logos')
        ->middleware('auth:user:admin');

    /* Sitemap */
    Route::get('/sitemap', 'Sports\MajorLeague\SiteTools@sitemap');
    Route::get('/sitemap.xml', 'Sports\MajorLeague\SiteTools@sitemapXml');
    Route::get('/sitemap.xml/{section}', 'Sports\MajorLeague\SiteTools@sitemapXmlSection')
        ->where('section', 'main|players|20[0-3]\d');

    /* Site Tools */
    Route::get('/manifest.json', 'Skeleton\SiteTools@manifest');
});
