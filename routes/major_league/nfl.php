<?php

/**
 * Routes for the NFL site
 */

Route::prefix('nfl')->group(function () {
    /* The homepage */
    Route::get('/', 'Sports\MajorLeague@index');
    // Legacy migration.
    Route::get('/index', function () {
        return redirect("/nfl", 308);
    });
    Route::get('/{season}', function ($season) {
        return redirect("/nfl", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/{season}/index', function ($season) {
        return redirect("/nfl", 308);
    })->where('season', '20[0-3]\d');

    /* Schedule */
    Route::get('/schedule/{week?}/{mode?}', 'Sports\MajorLeague\Schedule@index')
        ->where('week', '20[0-3]\d-(week-\d+|[a-z]+)')
        ->where('mode', '(body|switcher)');
    Route::get('/schedule/{season}/body', 'Sports\MajorLeague\Schedule@seasonSwitcher')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/schedule', 'Sports\MajorLeague\Schedule@legacyIndex')
        ->where('season', '20[0-3]\d');
    Route::get('/schedule-{week}', function ($week) {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        return redirect("/nfl/schedule/$season-week-$week", 308);
    })->where('week', '1?\d');
    Route::get('/schedule-{round}', function ($round) {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        return redirect("/nfl/schedule/$season-$round", 308);
    })->where('round', '[a-z]+');
    Route::get('/{season}/schedule-{week}', function ($season, $week) {
        return redirect("/nfl/schedule/$season-week-$week", 308);
    })->where(['season' => '20[0-3]\d', 'week' => '1?\d']);
    Route::get('/{season}/schedule-{round}', function ($season, $round) {
        return redirect("/nfl/schedule/$season-$round", 308);
    })->where(['season' => '20[0-3]\d', 'round' => '[a-z]+']);

    /* Inactives */
    Route::get('/inactives/{week?}/{mode?}', 'Sports\NFL\Inactives@index')
        ->where('week', '20[0-3]\d-(week-\d+|[a-z]+)')
        ->where('mode', '(body|switcher)');
    Route::get('/inactives/{season}/body', 'Sports\NFL\Inactives@seasonSwitcher')
        ->where('season', '20[0-3]\d');

    /* Injuries */
    Route::get('/injuries/{week?}/{mode?}', 'Sports\NFL\Injuries@index')
        ->where('week', '20[0-3]\d-(week-\d+|[a-z]+)')
        ->where('mode', '(body|switcher)');
    Route::get('/injuries/{season}/body', 'Sports\NFL\Injuries@seasonSwitcher')
        ->where('season', '20[0-3]\d');

    /* Game */
    Route::get('/schedule/{week}/{away}-at-{home}-{game_type}{game_id}', 'Sports\MajorLeague\Schedule@show')
        ->where('week', '20[0-3]\d-(week-\d+|[a-z]+)')
        ->where('away', '[a-z0-9\-]+')
        ->where('home', '[a-z0-9\-]+')
        ->where('game_type', '[rp]')
        ->where('game_id', '\d{1,4}');
    // Legacy migration.
    Route::get('/game-{game_type}{game_id}', function ($game_type, $game_id) {
        return redirect("/nfl/schedule", 308);
    })->where(['game_type' => '[rp]', 'game_id' => '\d+']);
    Route::get('/{season}/game-{game_type}{game_id}', 'Sports\MajorLeague\Schedule@legacyShow')
        ->where('season', '20[0-3]\d')
        ->where('game_type', '[rp]')
        ->where('game_id', '\d+');
    Route::get('/game-{game_type}{game_id}-{detail}', function ($game_type, $game_id, $detail) {
        return redirect("/nfl/schedule", 308);
    })->where(['game_type' => '[rp]', 'game_id' => '\d+', 'detail' => '[a-z]+']);
    Route::get('/{season}/game-{game_type}{game_id}-{detail}', 'Sports\MajorLeague\Schedule@legacyShow')
        ->where('season', '20[0-3]\d')
        ->where('game_type', '[rp]')
        ->where('game_id', '\d+')
        ->where('detail', '[a-z]+');

    /* Playoffs */
    Route::get('/playoffs/{season?}/{mode?}', 'Sports\MajorLeague\Playoffs@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/playoffs', function ($season) {
        return redirect("/nfl/playoffs/$season", 308);
    })->where('season', '20[0-3]\d');

    /* Standings */
    Route::get('/standings/{season?}/{mode?}', 'Sports\MajorLeague\Standings@index')
        ->where('season', '20[0-3]\d')
        ->where('mode', 'body');
    // Legacy migration.
    Route::get('/{season}/standings', function ($season) {
        return redirect("/nfl/standings/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/standings/{type}', function ($type) {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        return redirect("/nfl/standings/$season", 308);
    })->where('type', 'expanded|league');
    Route::get('/{season}/standings/{type}', function ($season, $type) {
        return redirect("/nfl/standings/$season", 308);
    })->where(['season' => '20[0-3]\d', 'type' => 'expanded|league']);

    /* News */
    Route::get('/news', 'Sports\Common\News@index');
    // Legacy migration.
    Route::get('/{season}/news', function ($season) {
        return redirect("/nfl/news", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/news-articles', function () {
        return redirect("/nfl/news", 308);
    });
    Route::get('/{season}/news-articles', function ($season) {
        return redirect("/nfl/news", 308);
    })->where('season', '20[0-3]\d');

    /* Stats */
    Route::get('/stats', 'Sports\MajorLeague\Stats@index');
    Route::get('/stats/{type}s/{class}', 'Sports\MajorLeague\Stats@show')
        ->where('type', 'player|team');
    // Legacy migration.
    Route::get('/{season}/stats', function ($season) {
        return redirect("/nfl/stats", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/stats-{type}-{category}', 'Sports\MajorLeague\Stats@legacyRedirectNoSeason')
        ->where(['type' => 'ind|team', 'category' => '[a-z_]+']);
    Route::get('/{season}/stats-{type}-{category}', 'Sports\MajorLeague\Stats@legacyRedirectWithSeason')
        ->where(['season' => '20[0-3]\d', 'type' => 'ind|team', 'category' => '[a-z_]+']);
    // Legacy team stat migration.
    Route::get('/stats-{team_id}', 'Sports\MajorLeague\Stats@legacyTeamNoSeason')
        ->where('team_id', '[A-Z]{2,3}');
    Route::get('/stats-{team_id}-{category}', 'Sports\MajorLeague\Stats@legacyTeamNoSeason')
        ->where(['team_id' => '[A-Z]{2,3}', 'category' => '[a-z_]+']);
    Route::get('/{season}/stats-{team_id}', 'Sports\MajorLeague\Stats@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}']);
    Route::get('/{season}/stats-{team_id}-{category}', 'Sports\MajorLeague\Stats@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}', 'category' => '[a-z_]+']);

    /* Teams */
    Route::get('/teams', 'Sports\MajorLeague\Teams@index');
    Route::get('/teams/{name}', 'Sports\MajorLeague\Teams@show')
        ->where('name', '[a-z0-9\-]+');
    Route::get('/teams/{name}/{tab}/{season?}', 'Sports\MajorLeague\Teams@tab')
        ->where('name', '[a-z0-9\-]+')
        ->where('tab', '[a-z\-]+')
        ->where('season', '20[0-3]\d');
    // Favourites.
    Route::post('/teams/{name}/favourite', 'Sports\MajorLeague\TeamFavourites@store')
        ->where('name', '[a-z0-9\-]+')
        ->middleware('auth:user');
    Route::delete('/teams/{name}/favourite', 'Sports\MajorLeague\TeamFavourites@destroy')
        ->where('name', '[a-z0-9\-]+')
        ->middleware('auth:user');
    // Legacy migration.
    Route::get('/{season}/teams', function ($season) {
        return redirect("/nfl/teams", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/team-{team_id}', 'Sports\MajorLeague\Teams@legacyTeamNoSeason')
        ->where('team_id', '[A-Z]{2,3}');
    Route::get('/team-{team_id}-{page}', 'Sports\MajorLeague\Teams@legacyTeamNoSeason')
        ->where(['team_id' => '[A-Z]{2,3}', 'page' => '[a-z]+']);
    Route::get('/{season}/team-{team_id}', 'Sports\MajorLeague\Teams@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}']);
    Route::get('/{season}/team-{team_id}-{page}', 'Sports\MajorLeague\Teams@legacyTeamWithSeason')
        ->where(['season' => '20[0-3]\d', 'team_id' => '[A-Z]{2,3}', 'page' => '[a-z]+']);

    /* Players */
    Route::get('/players', 'Sports\MajorLeague\Players@index');
    Route::get('/players/search/{term}', 'Sports\MajorLeague\Players@search');
    Route::get('/players/{name}-{player_id}', 'Sports\MajorLeague\Players@show')
        ->where('name', '[a-z\-]+')
        ->where('player_id', '\d+');
    Route::get('/players/{name}-{player_id}/{tab}', 'Sports\MajorLeague\Players@tab')
        ->where('name', '[a-z\-]+')
        ->where('player_id', '\d+')
        ->where('tab', '[a-z\-]+');
    // Legacy migration.
    Route::get('/{season}/players', function ($season) {
        return redirect('/nfl/players', 308);
    })->where('season', '20[0-3]\d');
    Route::get('/player-{player_id}', 'Sports\MajorLeague\Players@legacy')
        ->where('player_id', '\d+');
    Route::get('/player-{player_id}-{detail}', 'Sports\MajorLeague\Players@legacy')
        ->where('player_id', '\d+')
        ->where('detail', '[a-z]+');
    Route::get('/{season}/player-{player_id}', 'Sports\MajorLeague\Players@legacy')
        ->where('season', '20[0-3]\d')
        ->where('player_id', '\d+');
    Route::get('/{season}/player-{player_id}-{detail}', 'Sports\MajorLeague\Players@legacy')
        ->where('season', '20[0-3]\d')
        ->where('player_id', '\d+')
        ->where('detail', '[a-z]+');

    /* Awards */
    Route::get('/awards', 'Sports\MajorLeague\Awards@index');
    Route::get('/awards/{award_name}-{award_id}', 'Sports\MajorLeague\Awards@show')
        ->where('award_name', '[a-z\-]+')
        ->where('award_id', '\d+');

    /* Power Ranks */
    Route::get('/power-ranks/{season?}/{week?}', 'Sports\MajorLeague\PowerRanks@index')
        ->where('season', '20[0-3]\d');
    // Legacy migration.
    Route::get('/{season}/power-ranks', function ($season) {
        return redirect("/nfl/power-ranks/$season", 308);
    })->where('season', '20[0-3]\d');
    Route::get('/power-ranks-{type_code}{week}', function ($type_code, $week) {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        if ($type_code == 'r') {
            $week = "week-$week";
        } else {
            $po_codes = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'));
            $week = $po_codes[$week - 1];
        }
        return redirect("/nfl/power-ranks/$season/$week", 308);
    })->where(['type_code' => '[rp]', 'week' => '\d+']);
    Route::get('/{season}/power-ranks-{type_code}{week}', function ($season, $type_code, $week) {
        if ($type_code == 'r') {
            $week = "week-$week";
        } else {
            $po_codes = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'));
            $week = $po_codes[$week - 1];
        }
        return redirect("/nfl/power-ranks/$season/$week", 308);
    })->where(['season' => '20[0-3]\d', 'type_code' => '[rp]', 'week' => '\d+']);

    /* Admin */
    Route::get('/manage/logos', 'Sports\MajorLeague\Admin@logos')
        ->middleware('auth:user:admin');

    /* Sitemap */
    Route::get('/sitemap', 'Sports\MajorLeague\SiteTools@sitemap');
    Route::get('/sitemap.xml', 'Sports\MajorLeague\SiteTools@sitemapXml');
    Route::get('/sitemap.xml/{section}', 'Sports\MajorLeague\SiteTools@sitemapXmlSection')
        ->where('section', 'main|players|20[0-3]\d');

    /* Site Tools */
    Route::get('/manifest.json', 'Skeleton\SiteTools@manifest');
});
