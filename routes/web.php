<?php

/**
 * Routes for the Sports sub-domain
 */

// Sport-agnostic: Home.
Route::get('/', 'Sports\Home@index');
Route::redirect('/html/', '/', 308);
Route::redirect('/html/index', '/', 308);
Route::redirect('/html/index.php', '/', 308);

// And then our sport-specific pages.
foreach (FrameworkConfig::get('debear.sports.subsites') as $site => $site_config) {
    require "{$site_config['group']}/$site.php";
}
