## Build v3.0.2181 - 6781be9c - 2025-02-28 - Hotfix: AHL Integration
* 6781be9c: Add a new penalty type mapping (_Thierry Draper_)

## Build v3.0.2180 - ef5e802d - 2025-02-28 - Hotfix: NHL Integration
* ef5e802d: Add a new missed shot location mapping (_Thierry Draper_)
* f59c9f8f: Fix a missing NHL game event shot_type ENUM standardisation (_Thierry Draper_)

## Build v3.0.2178 - f3e64fff - 2025-02-26 - Improvement: Subsite Endpoints
* f3e64fff: Make use of the new section endpoint/config code split (_Thierry Draper_)

## Build v3.0.2177 - 5b92625b - 2025-02-24 - SysAdmin: News Article Syncing
* 5b92625b: Force pruning of news articles, on sync from remote, to account for the ID rebasing (_Thierry Draper_)

## Build v3.0.2176 - 26c6103c - 2025-02-22 - SoftDep: February 2025
* 26c6103c: Update PHP.ini locations to the new docker image location (_Thierry Draper_)
* 560f1312: Resolve team_id issues within MLB Batter-vs-Pitcher tables (_Thierry Draper_)
* 30d36da7: Upgrade Highcharts to v12 (_Thierry Draper_)
* a7db3ab9: Migrate config getting to the framework Config class (_Thierry Draper_)
* 91e80629: Auto: NPM Update for February 2025 (_Thierry Draper_)
* 66805b91: Auto: Composer Update for February 2025 (_Thierry Draper_)

## Build v3.0.2172 - b884b317 - 2025-02-10 - Hotfix: NFL Integration
* b884b317: Update another NFL roster table mapping (_Thierry Draper_)
* f1039041: Generalise gamebook roster split customisation (_Thierry Draper_)
* 343405cf: Improve the NFL injury mapping, in particular for length (_Thierry Draper_)
* e8291666: Add a test-mode flag for debugging NFL injury issues (_Thierry Draper_)

## Build v3.0.2168 - 489756bc - 2025-02-09 - Hotfix: Lockfile Stamping
* 489756bc: Fix lockfile stamping when integration scripts silently abort (_Thierry Draper_)

## Build v3.0.2167 - f1a64307 - 2025-02-09 - Setup: MLB 2025 season
* f1a64307: Apply a minor update to the URL used to load MLB schedules (_Thierry Draper_)
* 2219e982: Update internal files for the MLB 2025 season (_Thierry Draper_)
* 5474d687: Handle the Athletics relocation within the schema and integration scripts (_Thierry Draper_)

## Build v3.0.2164 - a0cb78d0 - 2025-02-06 - Setup: F1 2025 season
* a0cb78d0: Update internal files for the F1 2025 season (_Thierry Draper_)

## Build v3.0.2163 - 92127752 - 2025-01-28 - Setup: SGP 2025 season
* 92127752: Apply integration updates for the new SGP knockout format (_Thierry Draper_)
* 559e14aa: Make Motorsport stats dynamic per-season given the new SGP knockout format (_Thierry Draper_)
* da540690: Apply SGP meeting changes following the new format introduced in 2025 (_Thierry Draper_)
* 78dd6fb6: Re-jig SGP setup files as per the standard (_Thierry Draper_)
* accbe180: Update internal files for the SGP 2025 season (_Thierry Draper_)

## Build v3.0.2158 - 0f2b43d5 - 2025-01-17 - SoftDep: January 2025
* (Auto-commits only)

## Build v3.0.2158 - 778b8df1 - 2024-12-28 - Hotfix: MLB Draft Positions
* 778b8df1: Fix parsing of MLB draft player positions (_Thierry Draper_)

## Build v3.0.2157 - 124c1030 - 2024-12-23 - Hotfix: F1 Integration
* 124c1030: Correct the F1 race start times for the Las Vegas Grand Prix (_Thierry Draper_)
* 1e57c96c: Tweak F1 integration section titles (_Thierry Draper_)
* f673b677: Fix parsing of F1 fastest lap driver (_Thierry Draper_)
* d34b31c8: Handle non-latin1 characters in the integration for a database-stored F1 race name (_Thierry Draper_)
* acfd94b5: Fix how we identify F1 race numbers on Sprint weekends (_Thierry Draper_)

## Build v3.0.2152 - a3e0bfe3 - 2024-12-17 - SysAdmin: Remove Exemplar Cronjobs
* a3e0bfe3: Remove the no-longer maintained exemplar Sports crontab (_Thierry Draper_)

## Build v3.0.2151 - 25663181 - 2024-12-17 - Hotfix: SGP Perl Linting
* 25663181: Apply a minor SGP perl linting fix following Fedora 41 upgrade (_Thierry Draper_)

## Build v3.0.2150 - 895b200e - 2024-12-17 - Hotfix: F1 Mobile Result Table
* 895b200e: Fix the F1 race result mobile table (_Thierry Draper_)

## Build v3.0.2149 - af36adaa - 2024-12-17 - Hotfix: Page Indexing Fixes
* af36adaa: Fix MLB player name imports, which could create UTF-8 incompatible URLs (_Thierry Draper_)
* 6f3c49a6: Fix NHL player mapping via name only, which could create UTF-8 incompatible URLs (_Thierry Draper_)
* dcb92dc5: Fix the missing sport section in the motorsport participant links (_Thierry Draper_)
* acd7636e: Fix handling of the motorsport season navigation link appender (_Thierry Draper_)
* 8a522170: Fix the game team naming query to be season aware (_Thierry Draper_)

## Build v3.0.2144 - 4fa28fbb - 2024-12-15 - SoftDep: December 2024
* (Auto-commits only)

## Build v3.0.2144 - ead600e6 - 2024-11-27 - Hotfix: MLB Integration
* ead600e6: Include player import mapping in info synced between machines (_Thierry Draper_)
* 345cea6a: MLB standing code calc fixes (_Thierry Draper_)
* 0d7c6c2f: Streamline the "import all" logic in MLB game updating (_Thierry Draper_)

## Build v3.0.2141 - 5ee3d747 - 2024-11-27 - Hotfix: NFL Integration
* 5ee3d747: Include team depth charts in player info synced between machines (_Thierry Draper_)
* b0a63d80: Raw NFL game position column needs to handle wider string (_Thierry Draper_)
* b846ca21: NFL roster parsing mapping update for gameday inactive equivalents (_Thierry Draper_)
* c283d7dd: Additional pruning of secondary info from injury detail (_Thierry Draper_)
* 8141c5ab: Include (potential) fixing of raw gamebook as standard within its loading method (_Thierry Draper_)
* e5c0821f: Streamline the "import all" logic in NFL game updating (_Thierry Draper_)

## Build v3.0.2135 - 9773a439 - 2024-11-27 - Hotfix: NHL Integration
* 9773a439: Minor secondary database tidying (_Thierry Draper_)
* 039a0fa4: Standardise the shot type ENUM (_Thierry Draper_)
* 4740a5c4: Handle new challenge type (_Thierry Draper_)
* 87c26b47: Check the correct location for an existing initial NHL schedule run (_Thierry Draper_)
* ad15af51: Better handling of UTF-8 encoding within roster parsing (_Thierry Draper_)
* df1ccbd3: Expand the NHL country code to ISO code mapping (_Thierry Draper_)
* 2c271bf7: MySQL 8.0 compatible NHL stat calculation fix (_Thierry Draper_)
* e4c0197f: Streamline the "import all" logic in NHL game updating (_Thierry Draper_)

## Build v3.0.2127 - 13074ab9 - 2024-11-27 - Hotfix: AHL Integration
* 13074ab9: Latest AHL playoff matchup fixes (_Thierry Draper_)
* bfee50be: Streamline the "import all" logic in AHL game updating (_Thierry Draper_)

## Build v3.0.2125 - 1391fc64 - 2024-11-27 - Improvement: Player Mugshot Syncing
* 1391fc64: Mugshot process logging improvements (_Thierry Draper_)
* ef77ff13: Log when each player profile was imported within the AHL (_Thierry Draper_)
* 7d5d9daa: Remove exit code from semi-acceptable AHL mugshot parsing issues (_Thierry Draper_)
* 1409364d: NFL mugshot URL improvements, now choosing the league roster file of the team roster files (_Thierry Draper_)

## Build v3.0.2121 - c35f0507 - 2024-11-16 - SoftDep: November 2024
* (Auto-commits only)

## Build v3.0.2121 - 09e5fbe3 - 2024-10-29 - Hotfix: NFL Alternate Venue Loading
* 09e5fbe3: Include alternate venues in standard NFL schedule loading (_Thierry Draper_)

## Build v3.0.2120 - 9cb93007 - 2024-10-21 - SoftDep: October 2024
* 9cb93007: Move away from the apparently non-deterministic, and now broken, array-based orWhere clause (_Thierry Draper_)
* 52eff0a6: Auto: NPM Update for October 2024 (_Thierry Draper_)
* 5b9d1eb1: Auto: Composer Update for October 2024 (_Thierry Draper_)

## Build v3.0.2119 - b9673c66 - 2024-10-05 - Hotfix: Major League Schedule Favourites
* b9673c66: Fix rendering of non-favourite teams on the major league schedules (_Thierry Draper_)

## Build v3.0.2118 - 698ec803 - 2024-10-04 - Hotfix: SGP Server Sync
* 698ec803: Add the (now potentially dynamic) rider ranking table to the data synced between servers (_Thierry Draper_)

## Build v3.0.2117 - 57869c30 - 2024-10-04 - Feature: Major League Game Odds
* 57869c30: Render the game odds on the schedule preview widget (_Thierry Draper_)
* bbfbff64: Load the game odds on game load (_Thierry Draper_)
* 5dec943c: Add the objects for a new Major League odds process (_Thierry Draper_)

## Build v3.0.2114 - 5b574381 - 2024-09-29 - Hotfix: NFL Integration
* 5b574381: Tweak the roster parsing display (_Thierry Draper_)
* 6fa960ef: Fix the roster player parsing regexp (_Thierry Draper_)
* 0d912883: Extend the NFL player table roster status mapping (_Thierry Draper_)
* 551c8756: Extend the player name match to a fifth character (_Thierry Draper_)
* 097c91a2: Extend the NFL injury calcs to handle a Tuesday report run (_Thierry Draper_)
* 9ac7c78d: Update the NFL integration for the variable upstream URL (_Thierry Draper_)

## Build v3.0.2108 - 74d2235c - 2024-09-29 - Hotfix: F1 Integration
* 74d2235c: Update the F1 integration regexes following recent updates (_Thierry Draper_)

## Build v3.0.2107 - a8baa454 - 2024-09-29 - Hotfix: SGP Integration
* a8baa454: Apply some minor SGP integration tidying (_Thierry Draper_)
* b958f157: Include an internal visualisation of the SGP meeting scorecard within the parsed results (_Thierry Draper_)
* 7a3b5667: Add the ad hoc ability for the SGP integration to correct main position results (_Thierry Draper_)
* a06dcc29: Fix a MySQL 8.0 table re-use when calculating standings (_Thierry Draper_)

## Build v3.0.2103 - 1d861a9b - 2024-09-15 - SoftDep: September 2024
* (Auto-commits only)

## Build v3.0.2103 - 33cfa667 - 2024-09-15 - SysAdmin: CI Tweaks
* 33cfa667: Catch Phan run errors that could cause cyclical self-calling (_Thierry Draper_)
* 44176020: Fix first run setup for downloading seed data (_Thierry Draper_)
* cb9a1d88: Apply some minor CI script tidying and improvements (_Thierry Draper_)

## Build v3.0.2100 - e4fcd026 - 2024-09-01 - Hotfix: Multiple Mini Fixes
* e4fcd026: Fix a MySQL 8.0 temporary table reuse for pitcher repertoires that was allowed under MariaDB (_Thierry Draper_)
* 93ea7f6b: Fix parsing F1 qualifying results if a driver did not register a time in any session (_Thierry Draper_)

## Build v3.0.2098 - d5f07683 - 2024-09-01 - Setup: AHL 2024/25 season
* d5f07683: Update internal files for the AHL 2024/25 season (_Thierry Draper_)

## Build v3.0.2097 - c257972c - 2024-09-01 - Setup: NHL 2024/25 season
* c257972c: Restore local caching of downloaded schedules (_Thierry Draper_)
* 06ecd60d: Update internal files for the NHL 2024/25 season (_Thierry Draper_)

## Build v3.0.2095 - d5fe407e - 2024-08-19 - SoftDep: August 2024
* d5fe407e: Resolve reported instance of propagated type mismatch from an input argument in the Major League stats page (_Thierry Draper_)
* da936e97: Fix warnings for PHPs upcoming implicit nullable param deprecation (_Thierry Draper_)
* e7cdce31: Auto: NPM Update for August 2024 (_Thierry Draper_)
* 0e930c3d: Auto: Composer Update for August 2024 (_Thierry Draper_)

## Build v3.0.2093 - 34bd8272 - 2024-08-13 - Setup: NFL 2024 season
* 34bd8272: Handle a new alias for the LA Rams team ID (_Thierry Draper_)
* 224c17e4: Update internal files for the NFL 2024 season (_Thierry Draper_)

## Build v3.0.2091 - d035088b - 2024-08-13 - Improvement: MySQL DDL Migration
* d035088b: Fix some additional instances where GROUPING is considered a keyword (_Thierry Draper_)
* 137cd653: Migrate the season variables in the stored procedures to SMALLINTs, because YEAR does not work in MySQL (_Thierry Draper_)
* 4b62a6f3: Migrate our YEAR column schema definition to the deprecated non-width version (_Thierry Draper_)

## Build v3.0.2088 - 4588e2d3 - 2024-08-13 - Improvement: MySQL Zero Dates
* 4588e2d3: Remove a possible MySQL zerodate instance when setting up Major League power ranking weeks (_Thierry Draper_)

## Build v3.0.2087 - 6161a239 - 2024-08-09 - SysAdmin: MariaDB to MySQL Migration
* 6161a239: Fix some implied empty database definitions in MySQL JOINs that were considered acceptable in MariaDB (_Thierry Draper_)
* f7623f57: Refine problematic NULL column definitions within MySQL (_Thierry Draper_)
* 25e0df51: Fix the GROUP BY clauses in F1 setup due to the ONLY_FULL_GROUP_BY sql_mode (_Thierry Draper_)
* d70e1b78: Handle the way MySQL considers rank as a keyword (_Thierry Draper_)
* 34ac106b: Handle the way MySQL considers GROUPING as a keyword (_Thierry Draper_)
* 72057c61: Migrate our CI from MariaDB to MySQL (_Thierry Draper_)

## Build v3.0.2081 - 50f8c7de - 2024-07-17 - Hotfix: SAST Reporting
* 50f8c7de: Fix the SAST report download sort ordering (_Thierry Draper_)

## Build v3.0.2080 - 9a2eb701 - 2024-07-17 - Feature: CI Server Sync Job
* 9a2eb701: Add a new CI lint script for our individual sport syncs (_Thierry Draper_)
* 4daa4f2f: Include the server sync database to our CI job list (_Thierry Draper_)

## Build v3.0.2078 - 35d64da4 - 2024-07-17 - Hotfix: F1 Integration
* 35d64da4: Fix the F1 setup lap config for sprint weekends (_Thierry Draper_)
* 3d41c461: Apply minor F1 regexp integration fixes (_Thierry Draper_)
* 9e0b2ed4: Handle F1 sprint race time parsing, which complete without an hour part (_Thierry Draper_)
* e3c354d2: Improve F1 integration processing validation (_Thierry Draper_)
* 77dcf213: Remove the need to pass race name into the F1 processing script (_Thierry Draper_)

## Build v3.0.2073 - 21e8d39f - 2024-07-17 - Hotfix: SGP Integration
* 21e8d39f: Include integration changes for the SGP Sprint race (_Thierry Draper_)
* f42f3b79: Apply upstream SGP integration data fixes (_Thierry Draper_)
* d40e2a99: Improve the SGP integration fix handling (_Thierry Draper_)
* 85bb2ee4: Restore the maximum daily cache for the downloaded season integration file (_Thierry Draper_)
* 70141dba: Restore the dynamic buildId determination (_Thierry Draper_)
* 41892948: Make some minor SGP integration logging improvements (_Thierry Draper_)

## Build v3.0.2067 - bbf9720b - 2024-07-17 - Improvement: NHL Utah Hockey Club
* bbf9720b: Add the NHL integration codes for Utah Hockey Club (_Thierry Draper_)
* 68f74a6a: Expand NHL schema to include the new Utah Hockey Club (_Thierry Draper_)

## Build v3.0.2065 - 09b79389 - 2024-07-16 - SoftDep: July 2024
* (Auto-commits only)

## Build v3.0.2065 - 60417fe3 - 2024-06-28 - Hotfix: AHL Integration
* 60417fe3: Fix propagation of previous rosters in AHL roster processing (_Thierry Draper_)
* e5ba8f5e: Fix in-season AHL playoff matchup calcs when rounds overlap (_Thierry Draper_)

## Build v3.0.2063 - 6662bbba - 2024-06-28 - Hotfix: NHL Integration
* 6662bbba: Expand the lineup jersey fixing to include roster jersey fixing too (_Thierry Draper_)
* 26943b75: Include a new goal shot type mapping (_Thierry Draper_)
* 22055d42: Process play zone info for correct parsing (_Thierry Draper_)
* 0b3b5139: Resolve the play period to the most applicable source in the data (_Thierry Draper_)
* 6365ee4d: Handle Swiss player country codes wich do not match the ISO 3166-1 alpha-3 standard (_Thierry Draper_)

## Build v3.0.2058 - 61f63ca8 - 2024-06-23 - Hotfix: New Player View
* 61f63ca8: Fix the season loading calc for players yet to play (_Thierry Draper_)

## Build v3.0.2057 - 2483957b - 2024-06-21 - Hotfix: Reported Errors
* 2483957b: Distinguish seasons for stat loading loading where a player may be on a roster in a given season, but not play or accrue stats (_Thierry Draper_)
* c8b00694: Ensure table name uniqueness on primary-v-secondary player stat calcs (_Thierry Draper_)
* f336ca7c: Ensure NFL player group calcs handles players with no secondary info (_Thierry Draper_)
* 98052828: Ensure MLB player heatzone chard loader processes batter/picher combos correctly (_Thierry Draper_)
* ff988231: Fix a division-by-zero scenario missed previously (_Thierry Draper_)
* 9f42bfcf: Auto: Upgrade puppeteer-core to the latest security patched version (_Thierry Draper_)
* 0ab8174b: Fix the local CI job parser to exclude comments after the allow_failure flag (_Thierry Draper_)

## Build v3.0.2051 - 8ceb4ef1 - 2024-06-18 - SoftDep: June 2024
* (Auto-commits only)

## Build v3.0.2051 - da9f112e - 2024-06-17 - Hotfix: CI Downloads
* da9f112e: Fix the request sent to determine what CI data files need downloading (_Thierry Draper_)
* c8eaed84: Improve the location of the CI download trust store cert (_Thierry Draper_)

## Build v3.0.2049 - 5a05a5aa - 2024-06-02 - Feature: Major League Season Series
* 5a05a5aa: Propagate the the client-side the potential per-game_type tab labels (_Thierry Draper_)
* 90f26562: Render as a new tab the other games in the season series between two teams (_Thierry Draper_)
* 2c18c3d5: Split the game title calculation into separate series name and, possible, game number accessors (_Thierry Draper_)
* 9b3e6248: Determine the other matchups between, within the same game_type, for AHL, NHL and MLB games (_Thierry Draper_)

## Build v3.0.2045 - 846daf4d - 2024-06-02 - Hotfix: SGP Quali Heat Display
* 846daf4d: Fix how the (single) SGP Quali Heat displays on mobile (_Thierry Draper_)

## Build v3.0.2044 - c4304a71 - 2024-06-02 - Hotfix: Reported Bug Redux
* c4304a71: Fix a missing instance of ORM filters being called with a null / missing data value (_Thierry Draper_)
* b7dfa91d: Update the heatcharts div-by-zero checks to consider floating point values (_Thierry Draper_)

## Build v3.0.2042 - 40f28a04 - 2024-05-22 - Hotfix: Reported Bugs
* 40f28a04: Improve stat column aliasing to remove database errors for missing / invalid column names (_Thierry Draper_)
* d4cb4147: Fix a minor issue with NFL player group parsing in a looped context (_Thierry Draper_)
* e00feedf: Resolve issues with heatcharts due to incomplete division-by-zero checks (_Thierry Draper_)
* 4af93729: Remove errors caused by ORM filters being called with a null / missing data value (_Thierry Draper_)

## Build v3.0.2038 - c17b6dbc - 2024-05-18 - SoftDep: May 2024
* (Auto-commits only)

## Build v3.0.2038 - aecb5071 - 2024-05-13 - Security: CI Jobs
* aecb5071: Modifications following output of the initial SAST artifacts (_Thierry Draper_)
* b3387e0e: Switch our localci SAST exclusion to the new emulator (_Thierry Draper_)
* e91acbd5: Implement a local SAST emulator script using the GitLab docker images (_Thierry Draper_)
* 1731acc1: Implement an initial SAST reporting tool (_Thierry Draper_)
* 270f3daf: Fix a CSS attribute that has a preferred shorthand to what is specified (_Thierry Draper_)
* ca5e96cd: Migrate our only/except job logic to the preferred rules format (_Thierry Draper_)
* 18477f76: Incorporate the GitLab managed SAST pipeline job (_Thierry Draper_)
* 7875ebb3: Add some native tool dependency vulnerability scanning within our CI (_Thierry Draper_)

## Build v3.0.2030 - 81dd3f37 - 2024-04-27 - Hotfix: MLB Fixes
* 81dd3f37: Enforce a numerical binary value when filtering BvP output (_Thierry Draper_)
* e55394ef: Fix Power Ranking calcs for quirks after the first week of the season (_Thierry Draper_)

## Build v3.0.2028 - 61d44c63 - 2024-04-27 - Improvement: CI Downloads
* 61d44c63: Switch CI downloads from wget to curl (_Thierry Draper_)

## Build v3.0.2027 - f5ca921d - 2024-04-24 - Improvement: JavaScript Formatting
* f5ca921d: Apply some minor eslint hint tweaks to the JavaScript (_Thierry Draper_)
* f19d737c: Disable the eslint unused disabled rule checked whilst linting, as it conflicts with the standards check (_Thierry Draper_)
* 2ac3c499: Migrate to eslints flat config format (_Thierry Draper_)

## Build v3.0.2024 - fea8a11f - 2024-04-21 - SoftDep: April 2024
* (Auto-commits only)

## Build v3.0.2024 - f001f0a2 - 2024-04-18 - Improvement: CI ECMAScript Version
* f001f0a2: Update the targeted version of ECMAScript in CI to v13 / 2022 (_Thierry Draper_)

## Build v3.0.2023 - a5e1215c - 2024-03-30 - Hotfix: MLB Two-Way Player Schema
* a5e1215c: Update the ENUM of a missed table with the Two-Way Player code (_Thierry Draper_)

## Build v3.0.2022 - f8655dc1 - 2024-03-24 - Improvement: MLB Two-Way Player Status
* f8655dc1: Allow the MLB roster downloader to preserve the initial rosters on day of download (_Thierry Draper_)
* 6eda5c8f: Include an as-hitter/pitcher dropdown selector on the per-player Batter-vs-Pitcher tab for Two-Way players (_Thierry Draper_)
* befd271a: Include the new Two-Way players in the pitcher repertoire calcs (_Thierry Draper_)
* 6644788c: Parse and store Two-Way players as such, and no longer as pseudo-Pitchers (_Thierry Draper_)

## Build v3.0.2018 - d944d834 - 2024-03-17 - SoftDep: Laravel 11 and March 2024 Update
* d944d834: Upgrade dependencies to PHPUnit 11 (_Thierry Draper_)
* 2a2d8a89: Tweak the way we parse an Eloquent query based on the expression re-write in Laravel 10 (_Thierry Draper_)
* 3c04ad04: Switch use of the now-removed $dates property in Eloquent to its $casts equivalent (_Thierry Draper_)
* 04afb38b: Upgrade dependencies to PHPUnit 10 (_Thierry Draper_)

## Build v3.0.2014 - 86cc4eee - 2024-03-16 - Improvement: F1 Processing
* 86cc4eee: Include the Fantasy motorsport selection status as part of the F1/SGP sporting sync (_Thierry Draper_)
* 5c44659b: No longer download the F1 news from live when uploading results (_Thierry Draper_)
* 420339f9: Use the correct exit code to represent silent errors (_Thierry Draper_)
* c2b956b6: Auto-create the full F1 results path for downloading files in a new F1 season (_Thierry Draper_)

## Build v3.0.2010 - 66bec94e - 2024-03-06 - Hotfix: NHL Starting Lineups
* 66bec94e: Improve the NHL starting lineup loading, including reducing the timeframe (_Thierry Draper_)

## Build v3.0.2009 - f4bdad55 - 2024-03-02 - Setup: SGP 2024 season
* f4bdad55: Render the Quali Sprint result on the meeting page (_Thierry Draper_)
* 520d30b9: Update internal files for the SGP 2024 season (_Thierry Draper_)
* a0f7b3d1: Add the SGP quirks model to the meeting list to account for the new 2024 Quali Sprint (_Thierry Draper_)

## Build v3.0.2006 - b95ad8a5 - 2024-02-25 - Hotfix: F1 Race Weather
* b95ad8a5: Make the F1 race locations compatible with the OpenWeatherMap searches (_Thierry Draper_)

## Build v3.0.2005 - 98f48473 - 2024-02-17 - SoftDep: February 2024
* (Auto-commits only)

## Build v3.0.2005 - 2b8b912d - 2024-02-17 - Hotfix: NHL Integration
* 2b8b912d: Implement bespoke NHL game roster fixes (_Thierry Draper_)
* 900e202b: Extend the NHL goal shot type list based on upstream changes (_Thierry Draper_)
* 10a08043: Minor upstream label tweaks for NHL penalty standardisation (_Thierry Draper_)
* 78903d31: Tidy the NHL integration version calcs, given the scenarios in which it is called (_Thierry Draper_)

## Build v3.0.2001 - 920173ab - 2024-02-17 - Hotfix: AHL Integration
* 920173ab: Minor upstream label tweaks for AHL penalty standardisation (_Thierry Draper_)

## Build v3.0.2000 - ad0d8a0d - 2024-02-17 - Hotfix: NFL Integration
* ad0d8a0d: Minor upstream label tweaks for NFL injuries and roster groups (_Thierry Draper_)
* 96512d6c: Correct the multi-line player name fix within the NFL gamebook (_Thierry Draper_)

## Build v3.0.1998 - f5c6006e - 2024-02-17 - Setup: F1 2024 Confirmed Times
* f5c6006e: Confirm F1 2024 start times for quali, race and sprints (_Thierry Draper_)

## Build v3.0.1997 - 32503496 - 2024-02-12 - Setup: F1 2024 season
* 32503496: Fix the motorsport standings which can now exceed the page width even in the desktop viewport (_Thierry Draper_)
* d56ffbfc: Update internal files for the F1 2024 season (_Thierry Draper_)

## Build v3.0.1995 - 6420550a - 2024-02-12 - Setup: MLB 2024 season
* 6420550a: Update internal files for the MLB 2024 season (_Thierry Draper_)

## Build v3.0.1994 - 78dec587 - 2024-01-27 - Hotfix: Participant History Table
* 78dec587: Fix the participant history table width, to exclude count of sprint races (_Thierry Draper_)

## Build v3.0.1993 - 0ca0289d - 2024-01-27 - Hotfix: Motorsport Stats Switcher
* 0ca0289d: Fix the CSS selector used by the participant stats switcher (_Thierry Draper_)

## Build v3.0.1992 - 7455dca9 - 2024-01-27 - Improvement: Refactoring JavaScript
* 7455dca9: Make use of the event attaching instantiation improvements (_Thierry Draper_)
* 2fb5f2b5: Apply the latest round of micro-optimisations and small improvements (_Thierry Draper_)
* e078c944: Revert PHPCS "linting" enforced JavaScript standards for curly brace positioning (_Thierry Draper_)
* 43926a81: Replace the JavaScript linter with something less enforcing of (clunky) standards (_Thierry Draper_)
* 307a0f71: Move to greater use of the arrow functions (_Thierry Draper_)
* f7e9da52: Make better use of template literals in string expressions (_Thierry Draper_)
* 893180be: Move away from the deprecated centralised DOM querySelector parent node argument (_Thierry Draper_)
* 112a2cfb: Replace use of the deprecated Input shorthands that can be achieved simply in other existing methods (_Thierry Draper_)
* eef5c76b: Replace use of the deprecated DOM.get* methods in favour of the generalised query selector (_Thierry Draper_)
* 97eecbc1: Update unit tests following server-side to client-side JavaScript event changes in the skeleton (_Thierry Draper_)

## Build v3.0.1982 - dffc5870 - 2024-01-19 - Improvement: JavaScript Null Operators
* dffc5870: Make use of the optional chaining JS operator to simplify the code (_Thierry Draper_)
* 7b157308: Make use of the nullish coalescing operator to simplify the code (_Thierry Draper_)
* bcf9c2e6: Update eslints base ECMAScript version to allow for null-handling simplification (_Thierry Draper_)

## Build v3.0.1979 - 77c4c713 - 2024-01-14 - SoftDep: January 2024
* (Auto-commits only)

## Build v3.0.1979 - 34ce28a4 - 2023-12-21 - Hotfix: Missing SGP server_sync changes
* 34ce28a4: Make the appropriate SGP server_sync changes for the new integration process (_Thierry Draper_)

## Build v3.0.1978 - 38c51847 - 2023-12-21 - Feature: SGP Integration
* 38c51847: Remove the use of official motorsport race/meeting name in favour of the formulaic full version (_Thierry Draper_)
* 651c4fa3: Prune the SGP admin interface that has been replaced by the integration (_Thierry Draper_)
* 50cc7fd8: Move our rider status calcs from bib_no to explicitly set (_Thierry Draper_)
* e6899394: Create an integration process with the official SGP site (_Thierry Draper_)

## Build v3.0.1974 - 0c51daf5 - 2023-12-17 - SoftDep: December 2023
* (Auto-commits only)

## Build v3.0.1974 - e95a6907 - 2023-12-06 - Hotfix: AHL Integration
* e95a6907: AHL script version standardisation (_Thierry Draper_)
* 2660ed21: AHL penalty parsing tweak (_Thierry Draper_)

## Build v3.0.1972 - 13d721e4 - 2023-12-06 - Improvement: NFL Gamebooks
* 13d721e4: Apply some more minor roster parsing fixes (_Thierry Draper_)
* c6bc4329: Remove the raw gamebook exception, in place for the generic multi-line name fix (_Thierry Draper_)
* 41bc7623: NFL Gamebook download improvements (_Thierry Draper_)

## Build v3.0.1969 - 656b8e95 - 2023-12-03 - Improvement: Silent Error Codes
* 656b8e95: Flip the silent error code from the too-generic 1 to a more appropriate range (_Thierry Draper_)

## Build v3.0.1968 - 58461fa3 - 2023-11-30 - Hotfix: Estimated NFL Standing Codes
* 58461fa3: Fix estimated NFL standing code calculations (_Thierry Draper_)

## Build v3.0.1967 - 64980c46 - 2023-11-30 - Hotfix: Major League Weather
* 64980c46: Identify domed NFL/MLB games to enure consistent schedule page game box sizing (_Thierry Draper_)

## Build v3.0.1966 - 4efbd0a5 - 2023-11-27 - CI: Standardised CSS Standards
* 4efbd0a5: Apply our standard 120-char width for CSS standards rather than the default 80 (_Thierry Draper_)

## Build v3.0.1965 - faacd7cc - 2023-11-27 - SysAdmin: Automated F1 Integration
* faacd7cc: Migrate the F1 integration to a fully automated process, with improved validation as a result (_Thierry Draper_)

## Build v3.0.1964 - 446afffc - 2023-11-18 - Composer: November 2023
* (Auto-commits only)

## Build v3.0.1964 - 22d538a7 - 2023-11-17 - Hotfix: NFL Integration
* 22d538a7: Minor NFL integration fixes (_Thierry Draper_)
* f1174a31: NFL integration fixes when multiple players identified with same name/pos combo (_Thierry Draper_)
* b9269f70: Assist NFL roster parsing when unknown status table identified (_Thierry Draper_)
* 249792cf: Prevent NFL roster parsing from proceeding in our daily integration if left in per-team debug mode (_Thierry Draper_)
* 16361d1d: Introduce an NFL gamepook position fixer helper method (_Thierry Draper_)
* 3690598d: Introduce data fixes to the raw NFL gamebook text (_Thierry Draper_)
* 194b1134: Update the NFL gamebook download URL to the upstream 2023 upstream (_Thierry Draper_)
* 6d92a583: Minor clarification of error count in puppeteer downloads (_Thierry Draper_)

## Build v3.0.1956 - 09f94440 - 2023-11-17 - Hotfix: MLB Roster Codes
* 09f94440: Minor clarification on raw MLB roster codes (_Thierry Draper_)

## Build v3.0.1955 - 22e36cd6 - 2023-11-17 - Improvement: NHL Integration
* 22e36cd6: NHL integration switch from statsapi to api-web (_Thierry Draper_)
* 93750ff4: NHL play-by-play fixes from the start of 2023/24 (_Thierry Draper_)
* de73f926: NHL integration fix for gender-neutral officials (_Thierry Draper_)
* 91e9dd96: Expanded NHL shot ENUM (_Thierry Draper_)

## Build v3.0.1951 - 43360967 - 2023-11-17 - Improvement: AHL integration
* 43360967: Generic AHL penalty parsing play-by-play fix (_Thierry Draper_)
* add1aea4: Process AHL transactions to a roster status when players are called up or down (_Thierry Draper_)
* e413228d: Re-work AHL playoff matchup calcs to account for overlapping rounds (_Thierry Draper_)

## Build v3.0.1948 - 099dd734 - 2023-10-18 - Improvement: 2023/24 NHL Mugshots
* 099dd734: Reflect the fact NHL mugshots are now PNGs, not JPGs (_Thierry Draper_)
* a1569eaa: Download NHL player mugshots from the new location (_Thierry Draper_)

## Build v3.0.1946 - bfcc01c7 - 2023-10-14 - Improvement: Application Timezones
* bfcc01c7: Improve return clauses of legacy controllers (_Thierry Draper_)
* 9b6079a6: Explicitly separate database timezone from the application (_Thierry Draper_)

## Build v3.0.1944 - 7bbdf52b - 2023-10-14 - Composer: October 2023
* (Auto-commits only)

## Build v3.0.1944 - b7b2153d - 2023-09-30 - Improvement: Generalised AHL and NHL integration scripts
* b7b2153d: Apply fixes as part of the global automated script improvements (_Thierry Draper_)
* 8293822e: Remove the now multi-repo NHL and AHL automated integration wrappers (_Thierry Draper_)

## Build v3.0.1942 - 888d2cc0 - 2023-09-30 - Hotfix: NFL Integration
* 888d2cc0: Retrofit addition of protocol to the meta images (_Thierry Draper_)
* dad7e14b: Implement a new 2023 version of the NFL GameCenter puppeteer script (_Thierry Draper_)
* 55a30604: NFL player name parsing now needs to go to the fourth-level of first name mapping (_Thierry Draper_)
* 80dcd69f: Map new NFL roster table sections (_Thierry Draper_)
* f9daa90d: Map NFL player positions using our mapping table whilst roster parsing (_Thierry Draper_)

## Build v3.0.1937 - 063e1d48 - 2023-09-18 - Composer: September 2023
* (Auto-commits only)

## Build v3.0.1937 - 37aef71d - 2023-09-18 - Hotfix: F1 Sprint Race Integration
* 37aef71d: F1 sprint race integration fixes (_Thierry Draper_)

## Build v3.0.1936 - 2b58771c - 2023-09-18 - Hotfix: MLB StatsAPI Integration
* 2b58771c: Move MLB player and roster parsing to the StatsAPI (_Thierry Draper_)

## Build v3.0.1935 - a7ad2d37 - 2023-09-02 - Decomm: Major League Play-by-Play
* a7ad2d37: Move MLB win probability entries to their own subset table (_Thierry Draper_)
* 2e743bce: Move NHL game event coordinates to their own subset table (_Thierry Draper_)
* 40541c15: Move NHL boxscore plays to their own subset table (_Thierry Draper_)
* 77aa4bbd: Move AHL boxscore plays to their own subset table (_Thierry Draper_)
* b24d171a: Move MLB scoring plays to its own subset table (_Thierry Draper_)
* a1a0585b: Remove the pitch location tab from the MLB game pages (_Thierry Draper_)
* 51d6510b: Remove the play-by-play tabs from the MLB and NHL game pages (_Thierry Draper_)

## Build v3.0.1928 - eeb774c4 - 2023-08-30 - Improvement: CI SymLink Conversion
* eeb774c4: Switch to the new symlink-to-dir CI converter (_Thierry Draper_)
* 30d066de: Switch to the new OG/Twitter meta image path definitions (_Thierry Draper_)

## Build v3.0.1926 - 81bcbe19 - 2023-08-08 - Decomm: MotoGP
* 81bcbe19: Consider the MotoGP decomm worthy of a minor version bump (_Thierry Draper_)
* de5eb174: Reflect the MotoGP decomm in our README (_Thierry Draper_)
* 1ec9a197: Update/Re-factor the CI to take in to account coverage previously applied under the now-removed MotoGP tests (_Thierry Draper_)
* ea6b3450: Remove MotoGP / 2 / 3 / E specific CI tests (_Thierry Draper_)
* ac0158d2: Remove MotoGP / 2 / 3 / E models (_Thierry Draper_)
* e0028378: Remove MotoGP / 2 / 3 / E specific images and styling (_Thierry Draper_)
* facdfabb: Remove MotoGP / 2 / 3 / E integration scripts and supporting processes and data (_Thierry Draper_)
* 9fb85232: Remove MotoGP / 2 / 3 / E schema (_Thierry Draper_)
* f245cf80: Remove MotoGP / 2 / 3 / E config and routes (_Thierry Draper_)

## Build v3.0.1917 - c70a6ca7 - 2023-08-05 - Composer: August 2023
* (Auto-commits only)

## Build v3.0.1917 - 702addac - 2023-07-31 - Setup: AHL 2023/24 season
* 702addac: Update internal files for the AHL 2023/24 season (_Thierry Draper_)

## Build v3.0.1916 - 414607d5 - 2023-07-31 - Setup: NHL 2023/24 season
* 414607d5: Update internal files for the NHL 2023/24 season (_Thierry Draper_)

## Build v3.0.1915 - 406f462c - 2023-07-31 - Setup: NFL 2023 season
* 406f462c: Update internal files for the NFL 2023 season (_Thierry Draper_)

## Build v3.0.1914 - fea0f7ee - 2023-07-15 - Composer: July 2023
* (Auto-commits only)

## Build v3.0.1914 - 773f028b - 2023-07-15 - Hotfix: Data Integrations
* 773f028b: Handle MLB player split calcs when an alternate stadium has been renamed (_Thierry Draper_)
* 9efc8775: Add a new rule the custom MLB team_id mapping (_Thierry Draper_)
* fcaaf5a1: Fix MLBs hit location calcs for when info is missing (_Thierry Draper_)
* ffc4af2d: Add to the ENUMs, new MLB pitch types being identified in 2023 (_Thierry Draper_)
* 76fa1808: Add logic for MLBs new 2023 pitch clock violation rules (_Thierry Draper_)
* 1e4025a1: Switch how the MLB automatic runner in extra innings logic is defined (_Thierry Draper_)
* 2717f61a: Switch MLB draft integration to the new statsapi subdomain, with 2023s additional rookie incentive round (_Thierry Draper_)
* facb4781: Apply MotoGP integration changes for 2023 Sprint weekend format (_Thierry Draper_)
* e69153aa: Move away from the NHL integrations use of INSERT IGNORE, which can renders empty ENUM values (_Thierry Draper_)
* eb2411e9: Fix, syntactically, the NHL playoff matchup calcs (_Thierry Draper_)
* 6a802c6a: Expand NHL goalie Goals Against calcs to SMALLINT, as more than 255 has now been recorded (_Thierry Draper_)
* 53bbfaf0: Re-work the NHL play-by-play event co-ordinate cacher (_Thierry Draper_)
* 45fb4afc: Fix NHL play-by-plays with a new penality parser and some shot fixes (_Thierry Draper_)
* e9cb4cc2: Fix, syntactically, the AHL playoff matchup calcs (_Thierry Draper_)
* 27c4ae44: Add a new AHL penalty type parser (_Thierry Draper_)

## Build v3.0.1899 - 1c0f5d0f - 2023-07-13 - Hotfix: Motorsport Participant History
* 1c0f5d0f: Correctly render Cancelled races on the participant Career History tab (_Thierry Draper_)

## Build v3.0.1898 - d35966ce - 2023-06-28 - Hotfix: Major League page fixes
* d35966ce: Replace deprecated use of ${var} with {$var} (_Thierry Draper_)
* f6c795ad: Fix MLB Probable pitcher stat calcs (_Thierry Draper_)
* 31263dc3: Move the AHL-specific playoff bracket CSS to a centralised, always loaded, file (_Thierry Draper_)

## Build v3.0.1895 - 44fd30e9 - 2023-06-17 - Composer: June 2023
* (Auto-commits only)

## Build v3.0.1895 - 720b4e44 - 2023-05-14 - Improvement: Highcharts v11
* 720b4e44: Switch Highcharts innvocation to the new v11 format (_Thierry Draper_)
* d44cabca: Switch from Highcharts v10 to v11 (_Thierry Draper_)

## Build v3.0.1893 - 0a06fbff - 2023-05-14 - Composer: May 2023
* (Auto-commits only)

## Build v3.0.1893 - 1892c0f3 - 2023-05-02 - SysAdmin: Deprecations
* 1892c0f3: Replace deprecated use of ${var} with {$var} (_Thierry Draper_)
* ed7a121f: Replace use of fgrep with grep given it is being flagged overtly as obsolete (_Thierry Draper_)

## Build v3.0.1891 - e061e5f6 - 2023-04-30 - Feature: F1 Sprint Shootout
* e061e5f6: Correctly parse the F1 lap leaders (_Thierry Draper_)
* 7085735b: Display the results of the new F1 Sprint Shootout (_Thierry Draper_)
* 27608389: Process the new F1 Sprint Shootout results (_Thierry Draper_)

## Build v3.0.1888 - b9375efa - 2023-04-15 - Composer: April 2023
* (Auto-commits only)

## Build v3.0.1888 - 63c3304d - 2023-03-11 - Composer: March 2023
* 63c3304d: Fix the puppeteer-core version dependency to ensure the absolute latest version is used, given how far behind we have been (_Thierry Draper_)
* c8e74552: Auto: NPM Update for March 2023 (_Thierry Draper_)
* 5d4a2a0a: Auto: Composer Update for March 2023 (_Thierry Draper_)

## Build v3.0.1887 - ad584f65 - 2023-03-01 - Improvement: CSS Standards to Prettier
* ad584f65: Fix CSS standards according to the new Prettier rules (_Thierry Draper_)
* 910acf8e: Switch the deprecated Stylelint CSS standards to Prettier (_Thierry Draper_)

## Build v3.0.1885 - 53d49531 - 2023-02-28 - Setup: MLB 2023 season
* 53d49531: Setup: MLB 2023 season (_Thierry Draper_)

## Build v3.0.1884 - be397a0b - 2023-02-28 - Hotfix: Data Integrations
* be397a0b: Switch to the generic and prettified database resync script (_Thierry Draper_)
* d9e7bb1c: Fix in-season NFL playoff matchup processing (_Thierry Draper_)
* 4e27f8b6: Handle NFL games that are postponed or cancelled from their scheduled week (_Thierry Draper_)
* c479b6b0: Improve NFL roster parsing, as a combination of team and NFL.com pages, along with storing depth charts (_Thierry Draper_)
* 25ba8bf1: Improve handling of nth-generation surnames in NFL player name suffix parsing (_Thierry Draper_)
* 445359c6: Sync NHL advanced player stats in the server-sync script (_Thierry Draper_)
* 1aa084ca: Correctly parse NHL missed shots with incomplete shot location information (_Thierry Draper_)
* 5a106cf0: Improve validation and parsing of NHL penalties (_Thierry Draper_)
* b0738304: Start generically parse goalie name character encoding (_Thierry Draper_)
* 6d3cc84c: Generically parse Roughing penalties in AHL integration (_Thierry Draper_)

## Build v3.0.1874 - 74c9971d - 2023-02-12 - Composer: February 2023
* (Auto-commits only)

## Build v3.0.1874 - 660b5011 - 2023-02-05 - Setup: 2023 F1, MotoGP and SGP Setup
* 660b5011: Standardise the motorsport terminology from Quali(fying) Race to Sprint Race (_Thierry Draper_)
* e9c8c7fb: Setup SGP 2023 (_Thierry Draper_)
* b9172c90: Add logic for rendering MotoGP sprint races as per F1 logic (_Thierry Draper_)
* a7fb8bf1: Setup MotoGP 2023 (_Thierry Draper_)
* 12be7bbd: Automate calculation of motorsport summary headers (_Thierry Draper_)
* e5c4f07e: Setup F1 2023 (_Thierry Draper_)

## Build v3.0.1868 - 564933b8 - 2023-01-31 - Improvement: SGP Unit Test updates
* 564933b8: Update the passing around of data in unit test requests following skeleton improvements (_Thierry Draper_)

## Build v3.0.1867 - 134f61bf - 2023-01-15 - Composer: January 2023
* (Auto-commits only)

## Build v3.0.1867 - 03dcef67 - 2023-01-08 - SysAdmin: CI Docker Images from Container Registry
* 03dcef67: Switch the CI jobs to images from our Container Registry (_Thierry Draper_)

## Build v3.0.1866 - fd9859fd - 2023-01-05 - Improvement: MySQL Linting
* fd9859fd: Switch the CI MariaDB collation to latin1_general (_Thierry Draper_)
* 86ba3db2: Archive the setup scripts for historical motorsport seasons (_Thierry Draper_)
* 2db5d38f: Include the CI job for linting setup scripts (_Thierry Draper_)

## Build v3.0.1863 - d3a6c50a - 2022-12-18 - Composer: December 2022
* (Auto-commits only)

## Build v3.0.1863 - 532dba44 - 2022-11-19 - Composer: November 2022
* (Auto-commits only)

## Build v3.0.1863 - 1a099dd5 - 2022-11-14 - Hotfix: Data Integrations
* 1a099dd5: Version the NFL puppeteer scripts (_Thierry Draper_)
* 9cc4e02f: Update NFL game roster parsing for 2022, along with the standardised data fixing pattern for extremes (_Thierry Draper_)
* dbac0dd6: Apply a retry logic to downloading the NFL.com gamebook for a game (_Thierry Draper_)
* 84f04b04: Remove the Cookies popup during the NFL.com gamecenter puppeteer processing (_Thierry Draper_)
* 3b6de5c1: Download and parse NFL rosters from a combination of the NFL.com and team website pages (_Thierry Draper_)
* 407bb227: Fix the NFL roster loading script to find the correct initial file for comparison (_Thierry Draper_)
* 88c4b50c: Include a general Utility position for the 2022 MLB Player Awards (_Thierry Draper_)
* 8f5943aa: Introduce an AHL roster processing script for rosters not built up from transactions (_Thierry Draper_)
* 3528eeea: Correctly parse NHL shootout attempts recorded as a Failed Attempt (_Thierry Draper_)
* 73d2af2e: Exclude comment lines when analysing MotoGP race output for errors (_Thierry Draper_)
* 8762e0b9: Standardise SQL in F1 Lap Leader calcs (_Thierry Draper_)

## Build v3.0.1852 - bf583cf2 - 2022-11-01 - Hotfix: SGP mobile site logo width
* bf583cf2: Fix the SGP mobile site logo width (_Thierry Draper_)

## Build v3.0.1851 - 882882e2 - 2022-11-01 - Hotfix: Secondary ORM failsafes
* 882882e2: Ensure ORM secondary loading queries include a failsafe for empty where/orWhere loops (_Thierry Draper_)

## Build v3.0.1850 - acffb161 - 2022-10-16 - Composer: October 2022
* (Auto-commits only)

## Build v3.0.1850 - 6e3a5aa0 - 2022-10-06 - Hotfix: MLB Playoffs
* 6e3a5aa0: Allow a TBD team on the Major League homepage when a playoff bracket is only partially complete (_Thierry Draper_)
* d6f192ff: Fix date-based Major League playoff calculations whilst still in-season (_Thierry Draper_)

## Build v3.0.1848 - e7e4f0de - 2022-09-18 - Composer: September 2022
* e7e4f0de: Resolve PHPMD variable name violation following the dependency update (_Thierry Draper_)
* a283a455: Auto: NPM Update for September 2022 (_Thierry Draper_)
* d0d235ad: Auto: Composer Update for September 2022 (_Thierry Draper_)

## Build v3.0.1847 - f790f950 - 2022-09-17 - Improvement: Mugshot/Logo file path methods
* f790f950: Split out on-disk mugshot/logo path determination in to their own methods (_Thierry Draper_)

## Build v3.0.1846 - c2def982 - 2022-09-16 - Hotfix: Major League data fixes
* c2def982: Fix MLB stat loading for players moving teams mid-season (_Thierry Draper_)
* 206ddead: Correctly display player stats for Major League players who start the season missing games (_Thierry Draper_)

## Build v3.0.1844 - e2859aa3 - 2022-09-14 - Hotfix: Major League Tweaks
* e2859aa3: Install wget as a CI environment dependency (_Thierry Draper_)
* b5fb8d57: Standardise MLB pitch type definitions on MLB game pages (_Thierry Draper_)
* af885d2e: Ensure the team scores fit in the space provided for a Major League result (_Thierry Draper_)
* 555bff74: Ensure spacing between the NFL game boxscores for each team (_Thierry Draper_)
* 9a4902d0: Ensure more than just the first play in an MLB winprob chart half inning has the correct label (_Thierry Draper_)

## Build v3.0.1839 - 927b6cb9 - 2022-08-14 - Composer: August 2022
* (Auto-commits only)

## Build v3.0.1839 - 58e0033c - 2022-08-05 - Hotfix: Database input encoding
* 58e0033c: Update the CI test for previously double-encoded MLB pitch-list input (_Thierry Draper_)
* 6ed3261f: Encode NFL player names when new players are imported from rosters (_Thierry Draper_)

## Build v3.0.1837 - ea2dd161 - 2022-08-03 - Setup: AHL 2022/23 season
* ea2dd161: Update AHL playoff calcs, where the Pacific Division in 2022/23 excludes bottom 3, not 2, following expansion (_Thierry Draper_)
* 39a71369: Update AHL standings calcs to use Pts, not Pts %age as initial tie-breaker again from 2022/23 (_Thierry Draper_)
* 56d1ca38: Update configuration for the 2022/23 AHL season, including new integration team IDs (_Thierry Draper_)

## Build v3.0.1834 - 2c32ade1 - 2022-07-31 - Setup: NFL 2022 season
* 2c32ade1: Update internal files for the NFL 2022 season (_Thierry Draper_)
* ab80f5aa: Guesstimate NFL game dates when parsing the schedule when no games have a date that week (_Thierry Draper_)
* e70d0aed: Fix NFL schedule downloading to get through the cookie management popup (_Thierry Draper_)

## Build v3.0.1831 - 7ae76fea - 2022-07-31 - Setup: NHL 2022/23 season
* 7ae76fea: Update the previously missed MLB 2022 server_sync script (_Thierry Draper_)
* 0e0ee9e4: Update internal files for the NHL 2022/23 season (_Thierry Draper_)

## Build v3.0.1829 - ba5ce38d - 2022-07-30 - Improvement: Fantasy helpers
* ba5ce38d: Switch the SGP Publishing database name getter to the new standardised method (_Thierry Draper_)
* dc14b71f: Add a pseudo-NHL injury code for Fantasy entities (_Thierry Draper_)
* ba5fcb15: Move NFL Injury status from word to code (_Thierry Draper_)

## Build v3.0.1826 - 853a9fcf - 2022-07-21 - Hotfix: MLB Draft integration
* 853a9fcf: Update the MLB Draft Round integration field mapping (_Thierry Draper_)

## Build v3.0.1825 - 4e922202 - 2022-07-19 - SysAdmin: Backport for PHP 8.0
* 4e922202: Update the composer file to allow PHP 8.0, as well as the previous 8.1 (_Thierry Draper_)

## Build v3.0.1824 - 721dca0f - 2022-07-16 - Composer: July 2022
* (Auto-commits only)

## Build v3.0.1824 - c6470ca2 - 2022-07-16 - Improvement: Mugshot / Silhouette standardisation
* c6470ca2: Increase the scope of PHPMDs TooManyMethods whitelist (_Thierry Draper_)
* 8c44a643: Standardise Mugshot / Silhouette URL generation for Fantasy game use (_Thierry Draper_)

## Build v3.0.1822 - cd61111a - 2022-07-15 - Hotfix: MotoGP integration updates
* cd61111a: Make minor MotoGP integration tidy-ups (_Thierry Draper_)
* 23ac5415: Silently pass on MotoGP timing sessions that did not take place (_Thierry Draper_)

## Build v3.0.1820 - cba99b17 - 2022-06-19 - Composer: June 2022
* (Auto-commits only)

## Build v3.0.1820 - 04878d44 - 2022-06-08 - SysAdmin: Revert to native Blade data displaying
* 04878d44: Bump the memory allocation for the PHPUnit CI job (_Thierry Draper_)
* bc263eac: Fix SGP races where the Torun Grand Prix was incorrectly encoded (_Thierry Draper_)
* 6855927f: Update the unit tests following the encoding of data at-source (_Thierry Draper_)
* e3f922a8: Re-factor our Blade template variable echoing to use the appropriate native Laravel mechanism (_Thierry Draper_)
* 8ab08be0: Move common DeBear helper classes to the alias list for use in Blade templates (_Thierry Draper_)
* 0c28fed8: Revert use of @print and @config in Blade templates with the native Laravel method (_Thierry Draper_)
* 76eabd42: Restore Blade linting using the new dependency (_Thierry Draper_)

## Build v3.0.1813 - d4ef3e84 - 2022-05-31 - Improvement: Policy checks in middleware
* d4ef3e84: Move Major League favourite team management policy checking from controller to the middleware (_Thierry Draper_)
* f648a604: Move Major League admin page policy checking from controller to the middleware (_Thierry Draper_)
* b937139e: Move SGP Race Admin policy checking from controller to the middleware (_Thierry Draper_)

## Build v3.0.1810 - 1a9efa23 - 2022-05-30 - CI: Micro-optimisation to Global Test
* 1a9efa23: Apply a micro-optimisation to the PHP unit tests (_Thierry Draper_)

## Build v3.0.1809 - 34750ca6 - 2022-05-27 - Improvement: Highcharts v10
* 34750ca6: Switch from HighCharts v9 to v10 (_Thierry Draper_)

## Build v3.0.1808 - 4d59c71e - 2022-05-27 - SysAdmin: Major League database table optimisation
* 4d59c71e: Optimise some of the larger Major League database tables (_Thierry Draper_)

## Build v3.0.1807 - 55401ae4 - 2022-05-27 - Improvement: InternalCache SGP race admin processing
* 55401ae4: Fix how the InternalCache object is used within SGP Race Admin (_Thierry Draper_)

## Build v3.0.1806 - 59d47b50 - 2022-05-27 - CI: PHP Code Coverage streamlining
* 59d47b50: Streamline the PHP Code Coverage output, and consider output less than 100% as a failure (_Thierry Draper_)

## Build v3.0.1805 - 9e778380 - 2022-05-26 - SysAdmin: Major League mugshot compression
* 9e778380: Add image compression as part of the Major League mugshot processing (_Thierry Draper_)

## Build v3.0.1804 - 30526118 - 2022-05-26 - CI: Report PHP Unit code coverage on master
* 30526118: Propagate the PHP Unit code coverage report to the deploy stage for its result to be tagged against the master branch (_Thierry Draper_)

## Build v3.0.1803 - b761e0a5 - 2022-05-22 - Improvement: Unit Tests as part of Code Coverage
* b761e0a5: Switch Code Coverage from using XDebug to PCOV (_Thierry Draper_)
* fd9e96bb: Add the GitLab code coverage regexp pattern, previously configured within the UI (_Thierry Draper_)
* 13dfb54b: Refine our Unit Tests according to localci tweaks made to the skeleton (_Thierry Draper_)
* 2920f0f0: Include appropriate PHPUnit code in our code coverage tests (_Thierry Draper_)

## Build v3.0.1799 - 76c4d213 - 2022-05-22 - Improvement: Laravel 9
* 76c4d213: Updates to function signature changes following the Laravel 9 upgrade (_Thierry Draper_)
* 0a66496e: Remove Blade linting that is no longer available following the Laravel 9 upgrade (_Thierry Draper_)

## Build v3.0.1797 - cbce1bb4 - 2022-05-22 - Hotfix: Unit Tests affected by skeleton changes
* cbce1bb4: Updated Unit Test expectations following dropdown changes in the skeleton (_Thierry Draper_)

## Build v3.0.1796 - 4021859d - 2022-05-22 - Hotfix: SGP Integration
* 4021859d: Fix SGP meeting admin rider tie-breaking on rider ranking (_Thierry Draper_)

## Build v3.0.1795 - 947b6f81 - 2022-05-22 - Hotfix: MLB Integration
* 947b6f81: Re-introduce the MLB game parsing duplicate name check (_Thierry Draper_)
* 3f84ab3a: MLB fix for parsing the list of pitchers used in a game (_Thierry Draper_)
* c048a60a: Correctly parse the MLB Two-Way Player designation (_Thierry Draper_)

## Build v3.0.1792 - 49f128f5 - 2022-05-22 - Hotfix: Motorsport Gap-to-Leader Time
* 49f128f5: Correctly render a motorsport gap-to-leader time, whether sub-second or over a minute (_Thierry Draper_)

## Build v3.0.1791 - 66c54356 - 2022-05-22 - Improvement: MLB 2022
* 66c54356: Move the MLB player download script to an older URL, not requiring the fname-sname-remote_id pattern (_Thierry Draper_)
* 1009ceca: MLB 2022 playoff format changes (_Thierry Draper_)
* 12bac8e3: MLB 2022 setup (_Thierry Draper_)

## Build v3.0.1788 - 852006c1 - 2022-05-22 - Hotfix: MotoGP Integration
* 852006c1: Integration changes for MotoGP following upstream changes (_Thierry Draper_)

## Build v3.0.1787 - 97d24cff - 2022-05-22 - Hotfix: F1 Integration
* 97d24cff: F1 race name display fix in the integration script for non-ASCII chars (_Thierry Draper_)
* 81d59b52: F1 Lap Leader name decoding fix (_Thierry Draper_)
* f87a2aff: F1 race data parsing fix (_Thierry Draper_)

## Build v3.0.1784 - d36114d7 - 2022-05-22 - Hotfix: MLB pitcher sort
* d36114d7: MLB pitcher fix for sorting players on GO/FO ratio (_Thierry Draper_)

## Build v3.0.1783 - 43dc3ea7 - 2022-05-22 - Hotfix: NFL Integration
* 43dc3ea7: Include the NFL game data fix helper logic missed in the original commit (_Thierry Draper_)
* 37dcf658: NFL standings and playoff processing logic for the new 2021 playoff format (_Thierry Draper_)
* dacb59e6: Fix NFL player dedupe logic, in case matching fields like height and weight are NULL (_Thierry Draper_)
* 0aafdfca: Updated NFL schedule processing following upstream changes (_Thierry Draper_)
* ac9182a3: Move the NFL puppeteer logic to a non-game specific location (_Thierry Draper_)

## Build v3.0.1778 - b39a5f7d - 2022-05-22 - Hotfix: NHL Integration
* b39a5f7d: NHL game parsing fix for officials table (_Thierry Draper_)

## Build v3.0.1777 - 48d8cc8f - 2022-05-22 - Hotfix: AHL Integration
* 48d8cc8f: Fix processing and rendering of the new AHL DQF playoff round (_Thierry Draper_)
* 7a005fca: AHL game status parsing fix (_Thierry Draper_)
* 7e171b74: AHL game data encoding loading fix (_Thierry Draper_)
* 95f2a1fa: Confirm an AHL game goalie position, which is no longer implied in case of EBUGs (_Thierry Draper_)
* d1c35027: Add AHL integration game data fix logic (_Thierry Draper_)
* f2d1ca5f: Use up-to-date AHL standings tie-breaker logic following the switch to Pts Pct (_Thierry Draper_)

## Build v3.0.1771 - 13716aa3 - 2022-05-21 - Improvement: 2022 F1, MotoGP and SGP setup
* 13716aa3: SGP 2022 setup (_Thierry Draper_)
* 964a830d: MotoGP 2022 setup (_Thierry Draper_)
* fac3c541: Updated Speedway Grand Prix logos (_Thierry Draper_)
* f96bf0ba: F1 2022 setup (_Thierry Draper_)

## Build v3.0.1767 - 4d2dfd10 - 2022-05-21 - Hotfix: Motorsport Race List
* 4d2dfd10: Fix Motorsport race list view where a completed race did not feature a Fastest Lap (_Thierry Draper_)

## Build v3.0.1766 - 8f6df53c - 2022-05-21 - Hotfix: Major League pages
* 8f6df53c: Redirect users to the main Major League Team/Player page if a tab is accessed directly (_Thierry Draper_)
* dbef64f9: Render a loading state on Major League team tabs (_Thierry Draper_)
* 10fa236b: Correctly sort NFL Passer stat pages by Passer Rating (_Thierry Draper_)
* 0cd67a37: Allow Major League player tabs from loading when season applicable, but empty DB query returned (_Thierry Draper_)

## Build v3.0.1762 - f3b5fc84 - 2022-05-21 - SysAdmin: PHP 8
* f3b5fc84: Auto: NPM Update for April 2022 (_Thierry Draper_)
* 4cbc8b92: Auto: Composer Update for April 2022 (_Thierry Draper_)
* 35621d32: Auto: NPM Update for March 2022 (_Thierry Draper_)
* a2fe772c: Auto: Composer Update for March 2022 (_Thierry Draper_)
* 2cf3ce01: Auto: NPM Update for February 2022 (_Thierry Draper_)
* 8674af91: Auto: Composer Update for February 2022 (_Thierry Draper_)
* 063abe64: Use a new stylelint rule modifier to prevent recent false negatives (_Thierry Draper_)
* eb3bdc08: Auto: NPM Update for January 2022 (_Thierry Draper_)
* 7dde7c79: Auto: Composer Update for January 2022 (_Thierry Draper_)
* 9e200df7: Auto: NPM Update for December 2021 (_Thierry Draper_)
* 6d6fdcc2: Auto: Composer Update for December 2021 (_Thierry Draper_)
* 5bcd72e5: Add a new CI step to lint Blade templates (_Thierry Draper_)
* 73916ca9: Move away from our Arrays::hasIndex wrapper (_Thierry Draper_)
* f4929524: Improve null handling, including PHP 8s nullsafe operator (_Thierry Draper_)
* a5636128: Implement PHP 8s mixed return types (_Thierry Draper_)
* e6501785: Implement PHP 8s new union param/return types (_Thierry Draper_)
* 6a74c046: Include a repo-specific version of the Phan PHP Standards CI job (_Thierry Draper_)
* d5eb1b2a: Apply the final fixes suggested by Phan (_Thierry Draper_)
* e7c7c465: Apply several type-hinting related fixes (_Thierry Draper_)
* 14230322: Remove unused imported classes (_Thierry Draper_)
* 3291764e: Switch from Laravel's on-the-fly class facades at the root level to their actual full path (_Thierry Draper_)
* 6bfb0610: Fix code coverage fallout from PHP 8 migration (_Thierry Draper_)
* 373c354e: Include mixed class method paramter type hints from PHP 8 (_Thierry Draper_)

## Build v3.0.1749 - dfe72eff - 2021-11-13 - Composer: November 2021
* (Auto-commits only)

## Build v3.0.1749 - af07861d - 2021-11-12 - CI: CSS Property Standards
* af07861d: Update CSS property rules to reflect the new alphabetical standards (_Thierry Draper_)
* cdf73f6c: Add new stylelint standards test for alphabetical CSS property ordering (_Thierry Draper_)

## Build v3.0.1747 - d86d3875 - 2021-10-30 - Improvement: Use generalised config/datetime test methods
* d86d3875: Make use of the new skeleton config/datetime unit test tweak method (_Thierry Draper_)
* bff3d020: Switch to the skeletons config/datetime unit test tweak method (_Thierry Draper_)
* 3133cb63: Use correct JavaScript variable camel case variables for Highcharts data variables (_Thierry Draper_)

## Build v3.0.1744 - 26d558f9 - 2021-10-29 - Hotfix: Data integrations
* 26d558f9: Remove a recently deprecated stylelint rule (_Thierry Draper_)
* 391eed63: Remove from committed code the AHL transaction data fixes (_Thierry Draper_)
* 9c8e167e: Generalise AHL game roster field HTML escaping (_Thierry Draper_)
* 493ec9df: Fix AHL playoff matchup calcs for the new preliminary round (_Thierry Draper_)
* ec14c09f: Parse NHL play-by-play for the 2021/22 upstream tweaks (_Thierry Draper_)
* 55718f42: Silently fail NHL game lineup parsing when a roster file was not downloaded (e.g., returned a 404) (_Thierry Draper_)
* e47a74b6: Correctly handle skipping errors when files may be missing but acceptable (_Thierry Draper_)
* be22c760: Use the non-legacy location when parsing NHL player mugshots (_Thierry Draper_)
* 3e4d0973: Fix MLB playoff series LDS calcs when not all teams are known (_Thierry Draper_)
* 2bf8566f: Try and match raw gamebook position when matching unknown NFL players (_Thierry Draper_)
* 57d75035: Include NFL game stats in the files to verify before game parsing (_Thierry Draper_)
* 1c852d91: Latest NFL Roster integration fix, including new Practice Squad status determination (_Thierry Draper_)

## Build v3.0.1732 - 1990554b - 2021-10-16 - Composer: October 2021
* (Auto-commits only)

## Build v3.0.1732 - 3cff22fd - 2021-10-11 - Hotfix: SGP Championship calcs
* 3cff22fd: Include SGP template calcs in our PHP CI jobs (_Thierry Draper_)
* cd541af8: Correctly process tie-breakers for SGP championship calculations (_Thierry Draper_)
* e37d5148: Correct SGP Race Admin result rider name summary styling (_Thierry Draper_)

## Build v3.0.1729 - 57f28463 - 2021-09-29 - Improvement: AHL 2021/22
* 57f28463: Launching AHL's 2021/22 season (_Thierry Draper_)

## Build v3.0.1728 - fff8db95 - 2021-09-29 - Feature: New AHL playoff format from 2021/22
* fff8db95: AHL playoff bracket changes for the new 2021/22 playoff format (_Thierry Draper_)
* 0bebb1f8: AHL playoff series calcs for the new 2021/22 playoff format (_Thierry Draper_)
* 2fb5c58d: AHL playoff seed calcs for the new 2021/22 playoff format (_Thierry Draper_)
* 7450d2b1: AHL standing code calcs for the new 2021/22 playoff format (_Thierry Draper_)

## Build v3.0.1724 - 347f011e - 2021-09-29 - Improvement: 2021 NFL Integration
* 347f011e: Include Field Goal Return for Touchdown as an NFL scoring play (_Thierry Draper_)
* 3065b814: Standardise NFL game parsing data fixes to include how Gamebook fixes are stored and loaded (_Thierry Draper_)
* b962eba1: Auto: NPM Update for September 2021 (_Thierry Draper_)
* 7f5be10d: Fix NFL Roster displays due to missing get-last-roster query limit (_Thierry Draper_)
* 6f87d06c: Updated NFL Game integration to reflect changes to the data source (_Thierry Draper_)
* fe878bd8: Script for resyncing NFL remote IDs from v1 to v2 of the integration model (_Thierry Draper_)
* 4fcb5eb4: Updated NFL Injuries integration to reflect changes to the data source (_Thierry Draper_)
* 07de28b4: Updated NFL Mugshot integration to reflect changes to the data source (_Thierry Draper_)
* 4b242bbc: Updated NFL Roster/Player integration to reflect changes to the data source (_Thierry Draper_)

## Build v3.0.1716 - 957509ff - 2021-09-18 - Composer: September 2021
* (Auto-commits only)

## Build v3.0.1716 - 7be758f8 - 2021-09-16 - Improvement: Skeleton Sitemap Subsite Parser
* 7be758f8: Use the Sitemap subsite parser now in the skeleton (_Thierry Draper_)

## Build v3.0.1715 - 655d3339 - 2021-09-14 - Improvement: NHL 2021/22
* 655d3339: Launching NHL's 2021/22 season (_Thierry Draper_)

## Build v3.0.1714 - 60d2f630 - 2021-09-04 - Hotfix: MLB Integration Data Fixes
* 60d2f630: Move the MLB integration data fixes into the _data dir (_Thierry Draper_)

## Build v3.0.1713 - e415947e - 2021-09-04 - Hotfix: F1 Half Points
* e415947e: Correctly render 23 race motorsport headers (_Thierry Draper_)
* 56c47c48: Standardise argument use of the pluralisation Format method (_Thierry Draper_)
* 1da98bdb: Correctly render half-points on Motorsport pages (_Thierry Draper_)
* ca2b16e0: Handle F1 integration when short race w/no Fastest Lap (like 2021 Belgian Grand Prix) (_Thierry Draper_)

## Build v3.0.1709 - fed63283 - 2021-08-24 - Improvement: Pre-processing CSS/JS minification
* fed63283: A post-deploy script for pre-merging per-subsite CSS/JS (_Thierry Draper_)
* 5c7148b4: Re-jig common/global CSS to facilitate automated post-deploy merges (_Thierry Draper_)

## Build v3.0.1707 - 808b186c - 2021-08-15 - Hotfix: Major League stat tables in pre-season
* 808b186c: Correctly validate the Major League stats table season argument when passed a season in pre-season (_Thierry Draper_)

## Build v3.0.1706 - f92685ee - 2021-08-13 - Hotfix: MLB AB Result Mapping
* f92685ee: Add new MLB integration At Bat result mapping (_Thierry Draper_)

## Build v3.0.1705 - 8393f2e7 - 2021-08-13 - Composer: August 2021
* 8393f2e7: Make necessary tooltip changes following upgrade to HighCharts v9.1.2 (_Thierry Draper_)
* fd40679a: Auto: Composer Update for August 2021 (_Thierry Draper_)

## Build v3.0.1704 - c9281a94 - 2021-08-13 - Hotfix: Timezone Management
* c9281a94: Add missing .env values to the PHPUnit test XML spec (_Thierry Draper_)
* c43c1c95: Correctly manage timezones within the unit test date manipulation (_Thierry Draper_)
* 431944e2: Make the required Motorsport Race tweak following removal of the ConvertTimezone class (_Thierry Draper_)
* 9b6e9620: Standardise the timezone of event times listed on the global homepage (_Thierry Draper_)

## Build v3.0.1700 - 74f1911e - 2021-08-09 - Improvement: NFL changes for 2021
* 74f1911e: Correctly filter out missing stat leaders on the Major League homepage (_Thierry Draper_)
* 08cf7513: Launching NFL's 2021 season (_Thierry Draper_)
* 2d173cb6: Update NFL integration to handle 18 week regular seasons from 2021 (_Thierry Draper_)

## Build v3.0.1697 - cbdf682e - 2021-08-07 - Hotfix: SGP Race Admin Redux
* cbdf682e: Fix SGP semi-final rider ranking (_Thierry Draper_)

## Build v3.0.1696 - c68aa7fa - 2021-08-07 - Hotfix: MLB Win Probability Integration
* c68aa7fa: Correctly handling errors in MLB win probability data when integrating (_Thierry Draper_)

## Build v3.0.1695 - 94fc3c1e - 2021-08-07 - Hotfix: SGP Race Admin
* 94fc3c1e: Reset the number of completed SGP heats (in case of partial error) (_Thierry Draper_)
* 7c2eb057: Fix SGP participant saving when no changes were actually made (_Thierry Draper_)
* 2f9d15e8: Standardise the unset SGP draft dates (_Thierry Draper_)

## Build v3.0.1692 - 5b2d3e77 - 2021-08-04 - Hotfix: Historical AHL and NHL Player Heatmaps
* 5b2d3e77: Ensure historical AHL/NHL player heatmaps do not load with a 404 (_Thierry Draper_)

## Build v3.0.1691 - e1522759 - 2021-08-03 - Hotfix: Major League homepage images
* e1522759: Correctly render Major League homepage article images (_Thierry Draper_)

## Build v3.0.1690 - f907d703 - 2021-08-03 - Improvement: Error page handling
* f907d703: Make corresponding tweaks to config fix when rendering error pages (_Thierry Draper_)

## Build v3.0.1689 - f0d3d682 - 2021-08-02 - Hotfix: Post launch fixes
* f0d3d682: Add a subtle watermark on the homepage articles to indicate the sport (_Thierry Draper_)
* 78b7c07d: Standardise Home/News page box margins with remaining pages (_Thierry Draper_)
* fa36ae98: Remove dead symlinks to Major League custom playoff sprites, missed in earlier clear up (_Thierry Draper_)
* d1546601: Fix default MLB pitcher stat table calcs (_Thierry Draper_)
* 17052e5f: Redirect legacy Major League stats requests to their updated counterparts (_Thierry Draper_)
* ca41f1f9: Enable image lazy loading to reduce 503s on mugshot-heavy pages (_Thierry Draper_)
* 0d957ae7: Add a link to the main Sports homepage on the subsite mobile nav (_Thierry Draper_)
* a13f447b: Fix the team hover attributes on F1 driver career history tab (_Thierry Draper_)
* 32e6e08d: Fix some remaining F1 sprint qualifying calcs and display issues (_Thierry Draper_)
* 1546b7e3: Fix incorrect CSP implementation for inline styles with nonces (_Thierry Draper_)
* 80fb3a49: Fix CSP issues with loading of Major League team Highcharts objects via AJAX (_Thierry Draper_)
* 16713fca: Ensure AHL/NHL/MLB schedule dates are pruned correctly at the end of the playoffs (_Thierry Draper_)
* 9454f6a3: Fix Major League game stats table team header styling (_Thierry Draper_)
* 2161ab00: Fix NHL Draft parsing and display when multiple teams traded a selection (_Thierry Draper_)

## Build v3.0.1675 - d1847e89 - 2021-07-27 - Launching Laravel - [![Version: 3.0](https://img.shields.io/badge/Version-3.0-brightgreen.svg)](https://gitlab.com/debear/sports/tree/v3.0)


## Build v2.2.1094 - 7291eeb4 - 2018-12-13
* Launching MotoE, which involves some header image tweaks for sub-10 race seasons
* CRT / Open MotoGP entries no longer relevant after the end of the 2015 season
* Re-include MotoGP weather info on the race page
* Fix cancelled motorsport races from being accessed via URL (even if currently not linked within the site)
* Latest MLB team logos and colours (new Miami logo) and 2018 playoff banner
* Latest NFL player matching fixes
* MLB data fix for players whose position is not available within the player page JSON blob
* NFL integration fix to hadle the updated gamecenter page
* Minor fixes to the NHL integration roster scripts
* Minor NFL officials terminology fix
* NFL mugshot resync fix for the possibility of ' within player URLs
* Minor MLB play-by-play integration fixes
* Config for launching NHL 2018/19 season
* Config and tweaks for launching AHL 2018/19 season
* MotoGP fix to account for the cancelled 2018 British GP, which includes template changes too
* Another F1 integration tweak thanks to Wikipedia
* Minor MLB integration tweaks that have built up over the season
* Minor tweaks for the launch of the 2018 season (inluding the launch of the 2018 season...!)
* Minor fix to MLB game parsing due to a 2018 play-by-play with dodgy data
* Removal of the 'hosted dev' environment on production

## Build v2.2.1080 - f2ef0c7e - 2018-09-11
* MotoGP fix to account for the cancelled 2018 British GP, which includes template changes too
* Another F1 integration tweak thanks to Wikipedia
* Minor MLB integration tweaks that have built up over the season
* Minor tweaks for the launch of the 2018 season (inluding the launch of the 2018 season...!)
* Minor fix to MLB game parsing due to a 2018 play-by-play with dodgy data
* Removal of the 'hosted dev' environment on production
* Major League integration re-jig to standardise the downloader to use a LWP based downloader with better fail management from a single function, rather than cobbling together individual wget instances

## Build v2.2.1071 - b201d26f - 2018-06-10
* _Changelog truncated for initial build_