SET @sync_app := 'sports_data';

#
# Combined
#
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'All Sports Data', 'dev,data', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, 'Sports > Data > AHL', 1, 'script'),
                        (@sync_app, 2, 'Sports > Data > NHL', 2, 'script'),
                        (@sync_app, 3, 'Sports > Data > NFL', 3, 'script'),
                        (@sync_app, 4, 'Sports > Data > MLB', 4, 'script'),
                        (@sync_app, 5, 'Sports > Data > F1', 5, 'script');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 1, '/var/www/debear/bin/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app, '_ahl __REMOTE__')),
                               (@sync_app, 2, '/var/www/debear/bin/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app, '_nhl __REMOTE__')),
                               (@sync_app, 3, '/var/www/debear/bin/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app, '_nfl __REMOTE__')),
                               (@sync_app, 4, '/var/www/debear/bin/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app, '_mlb __REMOTE__')),
                               (@sync_app, 5, '/var/www/debear/bin/git-admin/server-sync', CONCAT('__DIR__ ', @sync_app, '_f1 __REMOTE__'));

# AHL
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (CONCAT(@sync_app, '_ahl'), 'AHL Sports Data', 'dev,data', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (CONCAT(@sync_app, '_ahl'), 1, 'Sports > Data > AHL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (CONCAT(@sync_app, '_ahl'), 1, '/var/www/debear/sites/sports/data/ahl', NULL, NULL);

# NHL
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (CONCAT(@sync_app, '_nhl'), 'NHL Sports Data', 'dev,data', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (CONCAT(@sync_app, '_nhl'), 1, 'Sports > Data > NHL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (CONCAT(@sync_app, '_nhl'), 1, '/var/www/debear/sites/sports/data/nhl', NULL, NULL);

# NFL
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (CONCAT(@sync_app, '_nfl'), 'NFL Sports Data', 'dev,data', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (CONCAT(@sync_app, '_nfl'), 1, 'Sports > Data > NFL', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (CONCAT(@sync_app, '_nfl'), 1, '/var/www/debear/sites/sports/data/nfl', NULL, NULL);

# MLB
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (CONCAT(@sync_app, '_mlb'), 'MLB Sports Data', 'dev,data', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (CONCAT(@sync_app, '_mlb'), 1, 'Sports > Data > MLB', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (CONCAT(@sync_app, '_mlb'), 1, '/var/www/debear/sites/sports/data/mlb', NULL, NULL);

# F1
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (CONCAT(@sync_app, '_f1'), 'F1 Sports Data', 'dev,data', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (CONCAT(@sync_app, '_f1'), 1, 'Sports > Data > F1', 1, 'file');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (CONCAT(@sync_app, '_f1'), 1, '/var/www/debear/sites/sports/data/f1', NULL, NULL);
