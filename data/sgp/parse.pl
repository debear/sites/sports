#!/usr/bin/perl -w
# Parse the meeting results

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Sort::Naturally;
use HTML::Entities;
use Encode;
use DBI;

# Validate the arguments
#  - there needs to be two: season, round
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the parse meeting script.\n";
  exit 98;
}
our ($season, $round) = @ARGV;

# Get config vars
our %config;
foreach ('config.pl', 'parse/main_pos.pl', 'parse/standings.pl', 'parse/fixes.pl', 'parse/validation.pl', 'parse/visualisation.pl') {
  my $config = abs_path(__DIR__ . "/$_");
  require $config;
}

print "##\n## Parsing meeting results for $season round $round\n##\n\n";
print STDERR "# Parsing Meeting $season // $round\n";

# Verify its size (to ensure a game was played!)
my $results_file = sprintf('%s/_data/%d/round_%02d.json.gz', $config{'base_dir'}, $season, $round);
my @stat = stat $results_file;
if (!defined($stat[7]) || $stat[7] <= 32) {
  print STDERR "No meeting info available\n";
  exit 10;
}
our $results = load_json($results_file);
$results = $$results{'pageProps'}{'round'};

# If any rider fixes are required, apply them now
my $fix_file = "$config{'base_dir'}/_data/fixes/$season.pl";
require $fix_file
  if -e $fix_file;

# Validate riders all have a 'numbers' attribute in the heats, and there are no duplicates (e.g., 2023-10)
my $invalid = validate_rider_bib_no($results);
if (@$invalid) {
  print STDERR "Meeting data rider bib number validation failed:\n- " . join("\n- ", @$invalid) . "\n";
  exit 1;
}

# Load meeting config info
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT SETUP.gates_version, SETUP.draw_version, SETUP.regulars + SETUP.wildcards AS meeting_riders, SETUP.track_reserves,
  GROUP_CONCAT(IF(SETUP_KO.heat = 1, SETUP_KO.main_pos, NULL) ORDER BY SETUP_KO.sel_pos) AS draw_ko1,
  GROUP_CONCAT(IF(SETUP_KO.heat = 2, SETUP_KO.main_pos, NULL) ORDER BY SETUP_KO.sel_pos) AS draw_ko2
FROM SPORTS_SGP_SETUP_RACES AS SETUP
JOIN SPORTS_SGP_SETUP_KNOCKOUT_DRAW AS SETUP_KO
  ON (SETUP_KO.version = SETUP.gates_version)
WHERE ? BETWEEN SETUP.season_from AND SETUP.season_to
GROUP BY SETUP.season_from;';
my $sth = $dbh->prepare($sql);
$sth->execute($season);
$config{'meeting_setup'} = $sth->fetchrow_hashref;
$config{'meeting_setup'}{'draw_ko1'} = [ split ',', $config{'meeting_setup'}{'draw_ko1'} ];
$config{'meeting_setup'}{'draw_ko2'} = [ split ',', $config{'meeting_setup'}{'draw_ko2'} ];

# Determine default draw positions for the track reserves
for (my $i = 0; $i < $config{'meeting_setup'}{'track_reserves'}; $i++) {
  my $b = $config{'meeting_setup'}{'meeting_riders'} + $i + 1;
  $config{'default_draw'}{$b} = $b;
}

# The mapping for heats 1-4 to draw
$sql = 'SELECT heat, CASE gate WHEN 1 THEN "r" WHEN 2 THEN "b" WHEN 3 THEN "w" WHEN 4 THEN "y" END AS gate, draw
FROM SPORTS_SGP_SETUP_GATES
WHERE version = ?
AND   heat < 5;';
$sth = $dbh->prepare($sql);
$sth->execute($config{'meeting_setup'}{'gates_version'});
while (my $row = $sth->fetchrow_hashref) {
  $config{'heats_draw'}{$$row{'heat'}} = { }
    if !defined($config{'heats_draw'}{$$row{'heat'}});
  $config{'heats_draw'}{$$row{'heat'}}{$$row{'gate'}} = $$row{'draw'};
}

# Setup the rider info from the database
$sql = 'SELECT RACE_RIDER.bib_no, CONCAT(RIDER.first_name, " ", RIDER.surname) AS rider_name,
       IF(RIDER_RANKING.ranking IS NOT NULL, RIDER_RANKING.ranking,
          CASE RACE_RIDER.status
            WHEN "wc" THEN 16
            WHEN "trsv" THEN RACE_RIDER.bib_no
            ELSE NULL
          END) AS ranking
FROM SPORTS_SGP_RACE_RIDERS AS RACE_RIDER
LEFT JOIN SPORTS_SPEEDWAY_RIDERS AS RIDER
  ON (RIDER.rider_id = RACE_RIDER.rider_id)
LEFT JOIN SPORTS_SGP_RIDER_RANKING AS RIDER_RANKING
  ON (RIDER_RANKING.season = RACE_RIDER.season
  AND RIDER_RANKING.rider_id = RACE_RIDER.rider_id)
WHERE RACE_RIDER.season = ?
AND   RACE_RIDER.round = ?
ORDER BY ranking;';
$sth = $dbh->prepare($sql);
$sth->execute($season, $round);
my %riders = ( );
while (my $row = $sth->fetchrow_hashref) {
  $riders{$$row{'bib_no'}} = {
    'name_full' => Encode::encode('UTF-8', decode_entities($$row{'rider_name'})),
    'draw' => defined($config{'default_draw'}{$$row{'bib_no'}}) ? $config{'default_draw'}{$$row{'bib_no'}} : undef,
    'pos' => undef,
    'pts' => undef,
    'main_pos' => undef,
    'main_pts' => undef,
    'sprint_pts' => undef,
    'heats' => 0,
    'by_heat' => { },
    'by_pos' => { 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0 },
    'ranking' => $$row{'ranking'},
  };
}

# Ensure our rider list adquately maps the heat and result list
$invalid = validate_rider_mapping(\%riders, $results);
if (@$invalid) {
  print STDERR "Rider mapping validation failed:\n- " . join("\n- ", @$invalid) . "\n";
  exit 1;
}

#
# Prune a previous run
#
print "#\n# Prune Previous Run\n#\n";
foreach my $tbl ('SPORTS_SGP_RACE_HEATS', 'SPORTS_SGP_RACE_RESULT') {
  print "DELETE FROM $tbl WHERE season = '$season' AND round = '$round';\n";
}

#
# Run through the heats in the JSON (which is out-of-order)
#
my %heats = (
  'sprint' => { },
  'main' => { },
  'ko' => { },
);
my %tot_res = (
  'sprint' => 0,
  'main' => 0,
  'ko' => 0,
);
foreach my $heat (@{$$results{'races'}}) {
  # Skip entries that do not relate to heats
  next if !defined($$heat{'tags'}[1]{'title'}) || $$heat{'tags'}[1]{'title'} !~ m/^(sprint|heat|$config{'ko_heat_code'}|final)\d+$/;

  # Some tag parsing to determine what we're processing
  my $heat_ref = $$heat{'tags'}[1]{'title'};
  $heat_ref = 'gf1' if $heat_ref eq 'final1';
  $heat_ref =~ s/^heat//;
  my $heat_type = ($heat_ref =~ m/^sprint/ ? 'sprint' : ($heat_ref =~ m/^\d+$/ ? 'main' : 'ko'));

  # The results for this heat
  my @heat_res = ( );
  my @res = @{$$heat{'results'}};
  foreach my $res (@{$res[$#res]{'rankings'}}) {
    my $gate = defined($$res{'color'}{'title'}) ? lc substr($$res{'color'}{'title'}, 0, 1) : '?';
    my $rank = !defined($$res{'status'}{'shortTitle'}) ? $$res{'rank'} : 5; # A rider with a status is always classified 5th
    my $result = defined($$res{'status'}{'shortTitle'}) ? $config{'heat_status'}{$$res{'status'}{'shortTitle'}} : $$res{'rank'};
    $result = 'r' if !defined($result) && !$$res{'time'}; # Data fix where a rider retired with an unknown status
    $result = 't' if $result eq 'x' && $$res{'rank'} > 4;
    push @heat_res, {
      'rank' => $rank,
      'bib_no' => int($$res{'entry'}{'number'}),
      'pts' => defined($$res{'points'}) ? int($$res{'points'}) : undef,
      'gate' => $gate,
      'status' => defined($$res{'status'}{'shortTitle'}) ? $$res{'status'}{'shortTitle'} : undef,
      'result' => $result,
    };
    $tot_res{$heat_type}++;

    # For sprint races, we need to track the additional championship points earned
    $riders{$$res{'entry'}{'number'}}{'sprint_pts'} += int($$res{'points'})
      if $heat_type eq 'sprint';

    # Add to the rider's draw info, if not previously set
    $riders{$$res{'entry'}{'number'}}{'draw'} = $config{'heats_draw'}{$heat_ref}{$gate}
      if ($heat_type eq 'main' && $heat_ref =~ m/^(1|2|3|4)$/ && $$res{'entry'}{'number'} !~ m/^(17|18)$/);

    # Results by (main draw) heat
    $riders{$$res{'entry'}{'number'}}{'by_heat'}{$heat_ref} = {
      'gate' => $gate,
      'res' => defined($$res{'points'}) ? int($$res{'points'}) : $result,
    } if ($heat_type eq 'main');

    # For main heat results, add to the main_heats total
    $riders{$$res{'entry'}{'number'}}{'main_pts'} += int($$res{'points'})
      if ($heat_type eq 'main' && defined($$res{'points'}));

    # For knockout heats, we need to calculate our own points value
    $heat_res[$#heat_res]{'pts'} = (4 - $$res{'rank'})
      if ($heat_type eq 'ko' && $rank =~ m/^(1|2|3)$/);

    # Cache the per-heat positions for tiebreakers
    $riders{$$res{'entry'}{'number'}}{'heats'}++
      if $heat_type ne 'sprint';
    $riders{$$res{'entry'}{'number'}}{'by_pos'}{$rank}++
      if $heat_type eq 'main';
  }

  # Add to our list
  $heats{$heat_type}{$heat_ref} = \@heat_res;
}

# Now the meeting results
foreach my $rider (@{$$results{'results'}[0]{'rankings'}}) {
  next if !$riders{$$rider{'entry'}{'number'}}{'heats'};
  $riders{$$rider{'entry'}{'number'}}{'pos'} = $$rider{'rank'};
  $riders{$$rider{'entry'}{'number'}}{'pts'} = $$rider{'points'};
  # If appropriate, include the sprint points which are not included in the upstream standings point totals
  $riders{$$rider{'entry'}{'number'}}{'pts'} += $riders{$$rider{'entry'}{'number'}}{'sprint_pts'}
    if defined($riders{$$rider{'entry'}{'number'}}{'sprint_pts'});
}

# Sort the main heat results to get the intermediate standings and then validate our workings
set_main_pos(\%riders, $heats{'main'});
fix_main_pos(\%riders) if defined &fix_main_pos;
$invalid = validate_main_pos(\%riders, $heats{'ko'});
if (@$invalid) {
  print STDERR "The main_pos calculations do not seem plausible:\n- " . join("\n- ", @$invalid) . "\n";
  exit 1;
}

#
# A visual scorecard of the event for debugging purposes
#
visualise_meeting(\%riders, \%heats);

#
# Sprint
#
if (%{$heats{'sprint'}}) {
  print "\n#\n# Sprint\n#\n";
  print "INSERT INTO SPORTS_SGP_RACE_HEATS (season, round, heat_type, heat, bib_no, gate, result, pts) VALUES\n";
  my $c = 0;
  foreach my $heat (sort keys %{$heats{'sprint'}}) {
    my ($heat_num) = ($heat =~ m/^sprint(\d+)$/gsi);
    print "  # Sprint $heat_num\n";
    foreach my $res (@{$heats{'sprint'}{$heat}}) {
      my $pts = (defined($$res{'pts'}) && !defined($$res{'status'}) ? "'$$res{'pts'}'" : 'NULL');
      print "  ('$season', '$round', 'sprint', '$heat_num', '$$res{'bib_no'}', '$config{'gates'}{$$res{'gate'}}', '$$res{'result'}', $pts)";
      print '' . (++$c != $tot_res{'sprint'} ? ',' : ';') . "\n";
    }
  }
}

#
# Main Heats
#
print "\n#\n# Main Heats\n#\n";

# Default gates
my @rider_map = ( );
foreach my $bib_no (nsort keys %riders) {
  push @rider_map, "WHEN $riders{$bib_no}{'draw'} THEN $bib_no";
}
print "# Default gates\n";
print "INSERT INTO SPORTS_SGP_RACE_HEATS (season, round, heat_type, heat, gate, bib_no)
  SELECT '$season' AS season, '$round' AS round, 'main' AS heat_type, heat, gate,
         CASE draw " . join(" ", @rider_map) . " END AS bib_no
  FROM SPORTS_SGP_SETUP_GATES
  WHERE version = '$config{'meeting_setup'}{'gates_version'}'
  ORDER BY draw, heat;\n";

# Meeting results
print "\n# Heat results\n";
my $tot_heats = 0;
print "INSERT INTO SPORTS_SGP_RACE_HEATS (season, round, heat_type, heat, bib_no, gate, result, pts) VALUES\n";
my $c = 0;
foreach my $heat_num (nsort keys %{$heats{'main'}}) {
  print "  # Heat $heat_num\n";
  $tot_heats++;
  foreach my $res (@{$heats{'main'}{$heat_num}}) {
    my $pts = (defined($$res{'pts'}) && !defined($$res{'status'}) ? "'$$res{'pts'}'" : 'NULL');
    print "  ('$season', '$round', 'main', '$heat_num', '$$res{'bib_no'}', '$config{'gates'}{$$res{'gate'}}', '$$res{'result'}', $pts)";
    print '' . (++$c != $tot_res{'main'} ? ',' : '') . "\n";
  }
}
print "ON DUPLICATE KEY UPDATE result = VALUES(result), pts = VALUES(pts);\n";

#
# Semi Final / Grand Final
#
print "\n#\n# Knockouts\n#\n";
print "INSERT INTO SPORTS_SGP_RACE_HEATS (season, round, heat_type, heat, bib_no, gate, result, pts) VALUES\n";
$c = 0;
foreach my $heat ("$config{'ko_heat_code'}1", "$config{'ko_heat_code'}2", 'gf1') {
  my ($heat_type, $heat_num) = ($heat =~ m/^(\D+)(\d+)$/gsi);
  print "  # " . uc($heat) . "\n";
  $tot_heats++;
  foreach my $res (@{$heats{'ko'}{$heat}}) {
    my $pts = (defined($$res{'pts'}) && !defined($$res{'status'}) ? "'$$res{'pts'}'" : 'NULL');
    print "  ('$season', '$round', '$heat_type', '$heat_num', '$$res{'bib_no'}', '$config{'gates'}{$$res{'gate'}}', '$$res{'result'}', $pts)";
    print '' . (++$c != $tot_res{'ko'} ? ',' : ';') . "\n";
  }
}

#
# Meeting Results
#
print "\n#\n# Meeting Results\n#\n";
print "INSERT INTO SPORTS_SGP_RACE_RESULT (season, round, bib_no, draw, pos, pts, main_pos, main_pts) VALUES\n";
$c = 0; my $num_riders = scalar keys %riders;
foreach my $bib_no (nsort keys %riders) {
  my %rider = %{$riders{$bib_no}};
  print "  ('$season', '$round', '$bib_no', '$rider{'draw'}'";
  print ", " . (defined($rider{'pos'}) ? "'$rider{'pos'}'" : 'NULL');
  print ", " . (defined($rider{'pts'}) ? "'$rider{'pts'}'" : 'NULL');
  print ", " . (defined($rider{'main_pos'}) ? "'$rider{'main_pos'}'" : 'NULL');
  print ", " . (defined($rider{'main_pts'}) ? "'$rider{'main_pts'}'" : 'NULL');
  print ')' . (++$c != scalar keys %riders ? ',' : ';') . "\n";
}

#
# Confirm the meeting has been processed
#
print "\n#\n# Meeting Processed\n#\n";
print "UPDATE SPORTS_SGP_RACES SET completed_heats = '$tot_heats' WHERE season = '$season' AND round = '$round';\n";

#
# Championship Standings
#
print "\n#\n# Championship Standings\n#\n";
foreach my $sql (build_championship_standings($season, $round)) {
  print "$sql\n";
}
