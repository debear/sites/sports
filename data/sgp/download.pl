#!/usr/bin/perl -w
# Get the data for a single meeting from the official SPG website

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Path qw(make_path);
use JSON;

# Validate the arguments
#  - there needs to be three: season, round, round_order
if (@ARGV < 3) {
  print STDERR "Insufficient arguments to the download meeting script.\n";
  exit 98;
}
our ($season, $round, $round_order) = @ARGV;

# Get config vars
our %config;
foreach ('config.pl', '../_global/download.pl', '../_global/log.pl') {
  my $config = abs_path(__DIR__ . "/$_");
  require $config;
}

# Build our target paths
my $season_dir = sprintf('%s/_data/%s', $config{'base_dir'}, $season);
my $ids_file = "$season_dir/ids.json.gz";
my $season_file = "$season_dir/meetings.json.gz";
my $meeting_file = sprintf('%s/round_%02d.json.gz', $season_dir, $round);
make_path($season_dir);

# Run...
print "# Downloading $season, round $round: ";

# Skip if already done
if (-e $meeting_file) {
  print "[ Skipped ]\n";
  exit;
}

# First we need to determine the current buildId
my $homepage = download('https://www.fimspeedway.com/');
my ($build_id) = ($homepage =~ m/"buildId":"([^\"]+)"/gsi);
download_failed("Unable to identify the buildId when attempting to download data for round $round", 98)
  if !defined($build_id);

# Next we get the list of meetings for this season (once per season)
download("https://www.fimspeedway.com/_next/data/$build_id/en/sgp/results.json?championship=sgp", $ids_file, {'gzip' => 1})
  if ! -e $ids_file;
my $remote_ids = load_file($ids_file); # At this stage, quicker to identify via text search than parsing the full JSON
my ($championship_id) = ($remote_ids =~ m/{"id":"(\d+)","title":"SGP","slug":"sgp","__typename":"Championship"}/gsi);
download_failed("Unable to identify the championshipId when attempting to download data for round $round", 98)
  if !defined($championship_id);
my ($season_id) = ($remote_ids =~ m/{"id":"(\d+)","slug":"$season","title":"$season","__typename":"Season"}/gsi);
download_failed("Unable to identify the seasonId when attempting to download data for round $round", 98)
  if !defined($season_id);

# Use this to get the full list of meetings for the season
download("https://www.fimspeedway.com/api/results", $season_file, {
  'http_method' => 'POST',
  'headers' => { 'Content-Type' => 'application/json' },
  'request_body' => "{\"championshipId\":\"$championship_id\",\"seasonId\":\"$season_id\"}",
  'skip-today' => 1,
  'gzip' => 1,
});

# Finally, the main meeting download
my $slug;
my $meetings = load_json($season_file);
foreach my $r (@{$$meetings{'rounds'}}) {
  $slug = $$r{'slug'}
    if $$r{'seasonOffset'} == $round_order;
}
download_failed("Unable to identify the meeting slug when attempting to download data for round $round", 98)
  if !defined($slug);
download("https://www.fimspeedway.com/_next/data/$build_id/en/results/$slug.json?slug=$slug", $meeting_file, {'skip-today' => 1, 'gzip' => 1});

# Confirm download success upon completion
print "[ Done ]\n";

sub download_failed {
  my ($err, $exit_code) = @_;
  print STDERR "$err\n";
  print "[ Failed ]\n";
  exit ($exit_code // 20);
}
