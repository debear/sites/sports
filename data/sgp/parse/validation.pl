#!/usr/bin/perl -w
# Data validation methods

use strict;
use Sort::Naturally;

# Ensure all riders have a set and unique bib_no
sub validate_rider_bib_no {
  my ($results) = @_;
  my %invalid = ( 'missing' => { }, 'usage' => { } );
  our %config;

  # Build our list of entries to check
  my @tests = ( );
  # - Heat Results
  foreach my $heat (@{$$results{'races'}}) {
    # Skip entries that do not relate to heats
    next if !defined($$heat{'tags'}[1]{'title'})
      || $$heat{'tags'}[1]{'title'} !~ m/^(heat|$config{'ko_heat_code'}|final)\d+$/;
    # Merge the entries for this heat
    my @res = @{$$heat{'results'}};
    push @tests, @{$$heat{'results'}[$#res]{'rankings'}};
  }
  # - Rider Rankings
  push @tests, @{$$results{'results'}[0]{'rankings'}};

  # Now perform the validation checks
  foreach my $test (@tests) {
    # Missing?
    if (!defined($$test{'entry'}{'number'})) {
      $invalid{'missing'}{$$test{'entry'}{'object'}{'id'}} = 1;
    # Multiple riders with same bib_no?
    } else {
      $invalid{'usage'}{$$test{'entry'}{'number'}} = { }
        if !defined($invalid{'usage'}{$$test{'entry'}{'number'}});
      $invalid{'usage'}{$$test{'entry'}{'number'}}{$$test{'entry'}{'object'}{'id'}} = 1;
    }
  }

  # Finally, check the output to determine if any errors exist
  my @invalid = ( );
  # - Missing?
  foreach my $remote_id (nsort keys %{$invalid{'missing'}}) {
    push @invalid, "Rider $remote_id does not have a Bib number";
  }
  # - Multiple riders with same bib_no?
  foreach my $bib_no (nsort keys %{$invalid{'usage'}}) {
    my @remote_ids = nsort keys %{$invalid{'usage'}{$bib_no}};
    next if @remote_ids == 1;
    push @invalid, "Bib number $bib_no has been assigned to multiple riders (" . join(', ', @remote_ids) . ")";
  }

  # Return whatever anomalies we have found
  return \@invalid;
}

# Ensure all riders are correctly mapped from meeting data to rider list
sub validate_rider_mapping {
  my ($riders, $results) = @_;
  our %config;
  my %invalid = ( ); # Hash of Bib => Heat/Res invalid instances

  # Check the heat results
  foreach my $heat (@{$$results{'races'}}) {
    # Skip entries that do not relate to heats
    next if !defined($$heat{'tags'}[1]{'title'})
      || $$heat{'tags'}[1]{'title'} !~ m/^(heat|$config{'ko_heat_code'}|final)\d+$/;
    # Generate a visual heat reference
    my $heat_ref = $$heat{'tags'}[1]{'title'};
    $heat_ref = 'gf1' if $heat_ref eq 'final1';
    $heat_ref =~ s/^heat//;
    # Loop through the riders
    my @res = @{$$heat{'results'}};
    foreach my $res (@{$$heat{'results'}[$#res]{'rankings'}}) {
      my $bib_no = $$res{'entry'}{'number'};
      # Skip if this rider is known
      next if defined($$riders{$bib_no});
      # Invalid instance found, add to our list
      @{$invalid{$bib_no}} = ( )
        if !defined($invalid{$bib_no});
      push @{$invalid{$bib_no}}, $heat_ref;
    }
  }
  # Then the final standings
  foreach my $res (@{$$results{'results'}[0]{'rankings'}}) {
    my $bib_no = $$res{'entry'}{'number'};
    # Skip if this rider is known
    next if defined($$riders{$bib_no});
    # Invalid instance found, add to our list
    @{$invalid{$bib_no}} = ( )
      if !defined($invalid{$bib_no});
    push @{$invalid{$bib_no}}, 'Res';
  }

  # Convert these to our output
  my @invalid = ( );
  foreach my $bib_no (nsort keys %invalid) {
    push @invalid, "Found unmapped rider with bib $bib_no in the meeting data (" . join(', ', nsort @{$invalid{$bib_no}}) . ")";
  }
  return \@invalid;
}

# Ensure the main_pos calculations are plausible
sub validate_main_pos {
  my ($riders, $ko_heats) = @_;
  our %config;
  my @invalid = ( );

  # Positions 9+ are the same as the final pos
  foreach my $bib_no (nsort keys %$riders) {
    push @invalid, "Rider $bib_no did not qualify for the semi-finals but pos ($$riders{$bib_no}{'pos'}) does not match main_pos ($$riders{$bib_no}{'main_pos'})"
      if defined($$riders{$bib_no}{'pos'}) && $$riders{$bib_no}{'pos'} > 8 && $$riders{$bib_no}{'pos'} != $$riders{$bib_no}{'main_pos'};
  }

  # Semi Finalists / Last-Chance Qualifiers (same logic, mapping defined within DB)
  # Positions 1, 4, 6, 7 qualified for SF1 / 3, 6, 8, 9 qualified for LCQ1
  my $heat_code = $config{'ko_heat_code'} . '1';
  validate_main_pos_ko_heat(uc $heat_code, $riders, $$ko_heats{$heat_code}, $config{'meeting_setup'}{'draw_ko1'}, \@invalid);
  # Positions 2, 3, 5, 8 qualified for SF2 / 3, 5, 7, 10 qualified for LCQ2
  $heat_code = $config{'ko_heat_code'} . '2';
  validate_main_pos_ko_heat(uc $heat_code, $riders, $$ko_heats{$heat_code}, $config{'meeting_setup'}{'draw_ko2'}, \@invalid);

  # Return whatever anomalies we have found
  return \@invalid;
}
sub validate_main_pos_ko_heat {
  my ($heat_ref, $riders, $heat, $main_pos, $invalid) = @_;
  foreach my $bib_no (nsort map { $_->{'bib_no'} } @$heat) {
    push @$invalid, "Rider $bib_no rode in $heat_ref but main_pos ($$riders{$bib_no}{'main_pos'}) was not set in the expected range (" . join(', ', @$main_pos) . ")"
      if !grep(/^$$riders{$bib_no}{'main_pos'}$/, @$main_pos);
  }
}

# Return true to pacify the compiler
1;
