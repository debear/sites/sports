#!/usr/bin/perl -w
# Helper method for generating championship standings

use strict;

sub build_championship_standings {
  my ($season, $round) = @_;
  our %config;

  # Build our per-position clauses and queries
  my @calc_col = ( );
  my @calc_val = ( );
  my @by_pos = ( );
  for (my $i = 1; $i <= $config{'meeting_setup'}{'meeting_riders'}; $i++) {
    # Stat pre-calc.
    push @calc_col, "num_pos$i TINYINT(3) UNSIGNED";
    push @calc_val, "SUM(IFNULL(SPORTS_SGP_RACE_RESULT.pos, 0) = $i) AS num_pos$i";
    # Tiebreaking calcs.
    push @by_pos, "UPDATE tmp_rider_pts
SET pos = pos + IFNULL((SELECT COUNT(*)
                        FROM tmp_rider_pts2
                        WHERE tmp_rider_pts2.pos = tmp_rider_pts.pos
                        AND   tmp_rider_pts2.is_tied = tmp_rider_pts.is_tied
                        AND   tmp_rider_pts2.num_pos$i > tmp_rider_pts.num_pos$i), 0),
    is_tied = (SELECT COUNT(*)
              FROM tmp_rider_pts3
              WHERE tmp_rider_pts3.pos = tmp_rider_pts.pos
              AND   tmp_rider_pts3.is_tied = tmp_rider_pts.is_tied
              AND   tmp_rider_pts3.num_pos$i = tmp_rider_pts.num_pos$i
              AND   tmp_rider_pts3.rider_id <> tmp_rider_pts.rider_id)
WHERE is_tied = 1;";
    push @by_pos, 'TRUNCATE TABLE tmp_rider_pts2;';
    push @by_pos, 'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;';
    push @by_pos, 'TRUNCATE TABLE tmp_rider_pts3;';
    push @by_pos, 'INSERT INTO tmp_rider_pts3 SELECT * FROM tmp_rider_pts;';
    push @by_pos, ''; # Visual separator
  }

  # And now return the relevant SQL statements
  return (
    # .
    "SELECT round_order INTO \@v_round_order
FROM SPORTS_SGP_RACES
WHERE season = '$season'
AND   round = '$round';",

    '', # Get point totals.
    'DROP TEMPORARY TABLE IF EXISTS tmp_rider_pts;',
    'CREATE TEMPORARY TABLE tmp_rider_pts (
  rider_id INT(11) UNSIGNED NOT NULL,
  pts INT(11) UNSIGNED NOT NULL,
  pos INT(11) UNSIGNED,
  num_wins TINYINT(3) UNSIGNED,
  num_podiums TINYINT(3) UNSIGNED,
  ' . join(', ', @calc_col) . ',
  is_tied TINYINT(1) UNSIGNED,
  took_part INT(11) UNSIGNED,
  ranking TINYINT(1) UNSIGNED,
  first_event CHAR(4),
  PRIMARY KEY (rider_id)
) SELECT SPORTS_SGP_RACE_RIDERS.rider_id,
          SUM(IFNULL(SPORTS_SGP_RACE_RESULT.pts, 0)) AS pts,
          SUM(IFNULL(SPORTS_SGP_RACE_RESULT.pos, 0) = 1) AS num_wins,
          SUM(IFNULL(SPORTS_SGP_RACE_RESULT.pos, 0) IN (1, 2, 3)) AS num_podiums,
          ' . join(', ', @calc_val) . ',
          NULL AS pos, 0 AS is_tied,
          0 AS took_part,
          IFNULL(SPORTS_SGP_RIDER_RANKING.ranking, MIN(SPORTS_SGP_RACE_RIDERS.bib_no)) AS ranking,
          MIN(CONCAT(LPAD(SPORTS_SGP_RACES.round_order, 2, "0"), '
          . "LPAD(SPORTS_SGP_RACE_RIDERS.bib_no, 2, \"0\"))) AS first_event
  FROM SPORTS_SGP_RACES
  JOIN SPORTS_SGP_RACE_RIDERS
    ON (SPORTS_SGP_RACE_RIDERS.season = SPORTS_SGP_RACES.season
    AND SPORTS_SGP_RACE_RIDERS.round = SPORTS_SGP_RACES.round)
  LEFT JOIN SPORTS_SGP_RIDER_RANKING
    ON (SPORTS_SGP_RIDER_RANKING.season = SPORTS_SGP_RACE_RIDERS.season
    AND SPORTS_SGP_RIDER_RANKING.rider_id = SPORTS_SGP_RACE_RIDERS.rider_id)
  LEFT JOIN SPORTS_SGP_RACE_RESULT
    ON (SPORTS_SGP_RACE_RESULT.season = SPORTS_SGP_RACE_RIDERS.season
    AND SPORTS_SGP_RACE_RESULT.round = SPORTS_SGP_RACE_RIDERS.round
    AND SPORTS_SGP_RACE_RESULT.bib_no = SPORTS_SGP_RACE_RIDERS.bib_no)
  WHERE SPORTS_SGP_RACES.season = '$season'
  AND   SPORTS_SGP_RACES.round_order <= \@v_round_order
  GROUP BY SPORTS_SGP_RACE_RIDERS.rider_id;",

    '', # Set the took_part field (separately due to JOIN affecting pts calc).
    "UPDATE tmp_rider_pts
JOIN SPORTS_SGP_RACES
  ON (SPORTS_SGP_RACES.season = '$season'
  AND SPORTS_SGP_RACES.round_order <= \@v_round_order)
JOIN SPORTS_SGP_RACE_RIDERS
  ON (SPORTS_SGP_RACE_RIDERS.season = SPORTS_SGP_RACES.season
  AND SPORTS_SGP_RACE_RIDERS.round = SPORTS_SGP_RACES.round
  AND SPORTS_SGP_RACE_RIDERS.rider_id = tmp_rider_pts.rider_id)
JOIN SPORTS_SGP_RACE_HEATS
  ON (SPORTS_SGP_RACE_HEATS.season = SPORTS_SGP_RACE_RIDERS.season
  AND SPORTS_SGP_RACE_HEATS.round = SPORTS_SGP_RACE_RIDERS.round
  AND SPORTS_SGP_RACE_HEATS.bib_no = SPORTS_SGP_RACE_RIDERS.bib_no)
SET tmp_rider_pts.took_part = 1;",
    'DROP TEMPORARY TABLE IF EXISTS tmp_rider_pts2;',
    'CREATE TEMPORARY TABLE tmp_rider_pts2 LIKE tmp_rider_pts;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',
    'CREATE TEMPORARY TABLE tmp_rider_pts3 LIKE tmp_rider_pts;',
    'INSERT INTO tmp_rider_pts3 SELECT * FROM tmp_rider_pts;',

    '', # Rank.
    'UPDATE tmp_rider_pts
SET pos = 1 + IFNULL((SELECT COUNT(*) FROM tmp_rider_pts2 WHERE tmp_rider_pts2.pts > tmp_rider_pts.pts), 0);',
    'TRUNCATE TABLE tmp_rider_pts2;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',

    '', # Identify ties.
    'UPDATE tmp_rider_pts
JOIN tmp_rider_pts2
  ON (tmp_rider_pts.pts = tmp_rider_pts2.pts
  AND tmp_rider_pts.rider_id <> tmp_rider_pts2.rider_id)
SET tmp_rider_pts.is_tied = 1;',
    'TRUNCATE TABLE tmp_rider_pts2;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',

    '', # Break first on number wins, second, etc.
    ), @by_pos, (

    '', # Next tiebreak is by those who took part in heats.
    'UPDATE tmp_rider_pts
SET pos = pos + IFNULL((SELECT COUNT(*)
                        FROM tmp_rider_pts2
                        WHERE tmp_rider_pts2.pos = tmp_rider_pts.pos
                        AND   tmp_rider_pts2.is_tied = tmp_rider_pts.is_tied
                        AND   tmp_rider_pts2.took_part > tmp_rider_pts.took_part), 0),
    is_tied = 0
WHERE is_tied = 1;',
    'TRUNCATE TABLE tmp_rider_pts2;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',

    '', # Then break remaining ties by ranking.
    'UPDATE tmp_rider_pts
JOIN tmp_rider_pts2
  ON (tmp_rider_pts.pos = tmp_rider_pts2.pos
  AND tmp_rider_pts.rider_id <> tmp_rider_pts2.rider_id)
SET tmp_rider_pts.is_tied = 1;',
    'TRUNCATE TABLE tmp_rider_pts2;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',

    'UPDATE tmp_rider_pts
SET pos = pos + IFNULL((SELECT COUNT(*)
                        FROM tmp_rider_pts2
                        WHERE tmp_rider_pts2.pos = tmp_rider_pts.pos
                        AND   tmp_rider_pts2.is_tied = tmp_rider_pts.is_tied
                        AND   tmp_rider_pts2.ranking < tmp_rider_pts.ranking), 0),
    is_tied = 0
WHERE is_tied = 1;',
    'TRUNCATE TABLE tmp_rider_pts2;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',

    '', # Then break any remaining ties by the rider's first appearance.
    'UPDATE tmp_rider_pts
JOIN tmp_rider_pts2
  ON (tmp_rider_pts.pos = tmp_rider_pts2.pos
  AND tmp_rider_pts.rider_id <> tmp_rider_pts2.rider_id)
SET tmp_rider_pts.is_tied = 1;',
    'TRUNCATE TABLE tmp_rider_pts2;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',

    'UPDATE tmp_rider_pts
SET pos = pos + IFNULL((SELECT COUNT(*)
                        FROM tmp_rider_pts2
                        WHERE tmp_rider_pts2.pos = tmp_rider_pts.pos
                        AND   tmp_rider_pts2.is_tied = tmp_rider_pts.is_tied
                        AND   tmp_rider_pts2.first_event < tmp_rider_pts.first_event), 0),
    is_tied = 0
WHERE is_tied = 1;',
    'TRUNCATE TABLE tmp_rider_pts2;',
    'INSERT INTO tmp_rider_pts2 SELECT * FROM tmp_rider_pts;',

    '', # Update the progress table.
    "DELETE FROM SPORTS_SGP_RIDER_HISTORY_PROGRESS WHERE season = '$season' AND round = '$round';",
    "INSERT INTO SPORTS_SGP_RIDER_HISTORY_PROGRESS (season, rider_id, round, pos, pts)
  SELECT '$season', rider_id, '$round', pos, pts
  FROM tmp_rider_pts;",
    'ALTER TABLE SPORTS_SGP_RIDER_HISTORY_PROGRESS ORDER BY season, rider_id, round;',

    '', # Update the main standings table.
    "DELETE FROM SPORTS_SGP_RIDER_HISTORY WHERE season = '$season';",
    "INSERT INTO SPORTS_SGP_RIDER_HISTORY (season, rider_id, pos, pts, pos_raceoff, num_wins, num_podiums)
  SELECT '$season' , rider_id, pos, pts, NULL AS pos_raceoff, num_wins, num_podiums
  FROM tmp_rider_pts;",
    'ALTER TABLE SPORTS_SGP_RIDER_HISTORY ORDER BY season, rider_id;',
  );
}

# Return true to pacify the compiler
1;
