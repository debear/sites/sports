#!/usr/bin/perl -w
# Helper method for parsing the main heat results into the main_pos attribute

use strict;
use Sort::Naturally;
use List::Util qw(sum);

sub set_main_pos {
  my ($riders, $heats) = @_;

  # Determine the riders to be sorted
  my @sorting = ( );
  foreach my $bib_no (keys %$riders) {
    push @sorting, int($bib_no) if defined($$riders{$bib_no}{'pos'});
  }

  # Primarily we sort by points scored
  my @sorted_pts = set_main_pos_sort_pts($riders, \@sorting);
  for (my $i_pts = 0; $i_pts < @sorted_pts; $i_pts++) {
    next if @{$sorted_pts[$i_pts]{'riders'}} == 1; # Skip where no tie exists

    # First tie breaker: heat wins
    my @sorted_pos1 = set_main_pos_sort_pos($riders, $sorted_pts[$i_pts]{'riders'}, 1);
    for (my $i_pos1 = 0; $i_pos1 < @sorted_pos1; $i_pos1++) {
      next if @{$sorted_pos1[$i_pos1]{'riders'}} == 1; # Skip where no tie exists

      # Second tie breaker: heat runner ups
      my @sorted_pos2 = set_main_pos_sort_pos($riders, $sorted_pos1[$i_pos1]{'riders'}, 2);
      for (my $i_pos2 = 0; $i_pos2 < @sorted_pos2; $i_pos2++) {
        next if @{$sorted_pos2[$i_pos2]{'riders'}} == 1; # Skip where no tie exists

        # Third tie breaker: heat thirds
        my @sorted_pos3 = set_main_pos_sort_pos($riders, $sorted_pos2[$i_pos2]{'riders'}, 3);
        for (my $i_pos3 = 0; $i_pos3 < @sorted_pos3; $i_pos3++) {
          next if @{$sorted_pos3[$i_pos3]{'riders'}} == 1; # Skip where no tie exists

          # Fourth tie breaker: heat last
          my @sorted_pos4 = set_main_pos_sort_pos($riders, $sorted_pos3[$i_pos3]{'riders'}, 4);
          for (my $i_pos4 = 0; $i_pos4 < @sorted_pos4; $i_pos4++) {
            next if @{$sorted_pos4[$i_pos4]{'riders'}} == 1; # Skip where no tie exists

            # Fifth tie breaker: heat unclassified
            my @sorted_pos5 = set_main_pos_sort_pos($riders, $sorted_pos4[$i_pos4]{'riders'}, 5);
            for (my $i_pos5 = 0; $i_pos5 < @sorted_pos5; $i_pos5++) {
              next if @{$sorted_pos5[$i_pos5]{'riders'}} == 1; # Skip where no tie exists

              # Sixth tie breaker: rider head-to-head
              my @sorted_hth = set_main_pos_sort_h2h($heats, $sorted_pos5[$i_pos5]{'riders'});
              for (my $i_hth = 0; $i_hth < @sorted_hth; $i_hth++) {
                next if @{$sorted_hth[$i_hth]{'riders'}} == 1; # Skip where no tie exists

                # Final tie breaker: rider ranking
                my @sorted_ranking = set_main_pos_sort_rank($riders, $sorted_hth[$i_hth]{'riders'});
                splice(@sorted_hth, $i_hth, 1, @sorted_ranking);
              }
              splice(@sorted_pos5, $i_pos5, 1, @sorted_hth);
            }
            splice(@sorted_pos4, $i_pos4, 1, @sorted_pos5);
          }
          splice(@sorted_pos3, $i_pos3, 1, @sorted_pos4);
        }
        splice(@sorted_pos2, $i_pos2, 1, @sorted_pos3);
      }
      splice(@sorted_pos1, $i_pos1, 1, @sorted_pos2);
    }
    splice(@sorted_pts, $i_pts, 1, @sorted_pos1);
  }

  # Write this back to our rider list
  my @sorted = map { $_->{'riders'}[0] } @sorted_pts;
  for (my $i = 0; $i < @sorted; $i++) {
    $$riders{$sorted[$i]}{'main_pos'} = ($i + 1);
  }
}

sub set_main_pos_sort_pts {
  my ($riders, $sorting) = @_;
  my %to_sort = ( );
  foreach my $r (@$sorting) {
    $to_sort{$$riders{$r}{'main_pts'}} = {
      'value' => $$riders{$r}{'main_pts'},
      'riders' => [ ],
    } if !defined($to_sort{$$riders{$r}{'main_pts'}});
    push @{$to_sort{$$riders{$r}{'main_pts'}}{'riders'}}, $r;
  }
  return reverse sort { $$a{'value'} <=> $$b{'value'} } values %to_sort; # DESC sort - more points is better
}

sub set_main_pos_sort_pos {
  my ($riders, $sorting, $pos_num) = @_;
  my %to_sort = ( );
  foreach my $r (@$sorting) {
    $to_sort{$$riders{$r}{'by_pos'}{$pos_num}} = {
      'value' => $$riders{$r}{'by_pos'}{$pos_num},
      'riders' => [ ],
    } if !defined($to_sort{$$riders{$r}{'by_pos'}{$pos_num}});
    push @{$to_sort{$$riders{$r}{'by_pos'}{$pos_num}}{'riders'}}, $r;
  }
  return reverse sort { $$a{'value'} <=> $$b{'value'} } values %to_sort; # DESC sort - more placings is better
}

sub set_main_pos_sort_h2h {
  my ($heats, $sorting) = @_;
  @$sorting = nsort @$sorting; # Ensure a consistent order

  # Prepare our scoring cache
  my %hth = ( );
  foreach my $r (@$sorting) {
    $hth{$r} = 0;
  }

  # Parse the heats and identify matches
  foreach my $heat_num (keys %$heats) {
    my @heat_riders = nsort map { $_->{'bib_no'} } @{$$heats{$heat_num}};
    # Determine if multiple riders we are parsing were involved, as this heat should be parsed
    my %union = my %isect = ();
    foreach my $r (@$sorting, @heat_riders) { $union{$r}++ && $isect{$r}++ }
    my @isect = keys %isect;
    next if @isect < 2;
    # Determine the order the riders completed this heat
    my @heat_res = ( );
    foreach my $bib_no (@isect) {
      push @heat_res, {
        'bib_no' => $bib_no,
        'rank' => sum map { $_->{'bib_no'} == $bib_no ? $_->{'rank'} : 0 } @{$$heats{$heat_num}},
      };
    }
    @heat_res = sort { $$a{'rank'} <=> $$b{'rank'} } @heat_res;
    # And now apply this ranking (in the form of "riders beaten") to our scoring cache
    for (my $i = 0; $i < @heat_res; $i++) {
      $hth{$heat_res[$i]{'bib_no'}} += @heat_res - $i - 1;
    }
  }

  # Now perform the sorting action
  my %to_sort = ( );
  foreach my $r (keys %hth) {
    $to_sort{$hth{$r}} = {
      'value' => $hth{$r},
      'riders' => [ ],
    } if !defined($to_sort{$hth{$r}});
    push @{$to_sort{$hth{$r}}{'riders'}}, int($r);
  }
  return reverse sort { $$a{'value'} <=> $$b{'value'} } values %to_sort; # DESC sort - more points is better
}

sub set_main_pos_sort_rank {
  my ($riders, $sorting) = @_;
  my %to_sort = ( );
  foreach my $r (@$sorting) {
    $to_sort{$$riders{$r}{'ranking'}} = {
      'value' => $$riders{$r}{'ranking'},
      'riders' => [ ],
    } if !defined($to_sort{$$riders{$r}{'ranking'}});
    push @{$to_sort{$$riders{$r}{'ranking'}}{'riders'}}, $r;
  }
  return sort { $$a{'value'} <=> $$b{'value'} } values %to_sort; # ASC sort - lower ranking has higher priority
}

# Return true to pacify the compiler
1;
