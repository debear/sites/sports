#!/usr/bin/perl -w
# Helper methods for fixing input data

use strict;
our %config;

# Use an externally defined bib_no in all instances we can find a rider in the meeting
sub fix_rider_bib_no {
  my ($results, $remote_id, $new_bib_no) = @_;
  print "# Fixing remote rider $remote_id to have bib number $new_bib_no\n";
  # Rider Rankings
  for (my $res_num = 0; $res_num < @{$$results{'results'}[0]{'rankings'}}; $res_num++) {
    $$results{'results'}[0]{'rankings'}[$res_num]{'entry'}{'number'} = $new_bib_no
      if $$results{'results'}[0]{'rankings'}[$res_num]{'entry'}{'object'}{'id'} == $remote_id;
  }
  # Heat Results
  for (my $heat_num = 0; $heat_num < @{$$results{'races'}}; $heat_num++) {
    # Skip entries that do not relate to heats
    next if !defined($$results{'races'}[$heat_num]{'tags'}[1]{'title'})
      || $$results{'races'}[$heat_num]{'tags'}[1]{'title'} !~ m/^(heat|$config{'ko_heat_code'}|final)\d+$/;

    my @res = @{$$results{'races'}[$heat_num]{'results'}};
    for (my $res_num = 0; $res_num < @{$$results{'races'}[$heat_num]{'results'}[$#res]{'rankings'}}; $res_num++) {
      $$results{'races'}[$heat_num]{'results'}[$#res]{'rankings'}[$res_num]{'entry'}{'number'} = $new_bib_no
        if $$results{'races'}[$heat_num]{'results'}[$#res]{'rankings'}[$res_num]{'entry'}{'object'}{'id'} == $remote_id;
    }
  }
}

# Use an externally defined draw position should we not be able to determine one from the heat results
sub fix_rider_draw {
  my ($bib_no, $draw) = @_;
  print "# Fixing rider with bib $bib_no to have draw number $draw\n";
  $config{'default_draw'}{$bib_no} = $draw;
}

# Correct a missing gate reference
sub fix_heat_gate {
  my ($results, $heat_ref, $bib_no, $gate) = @_;
  for (my $heat_num = 0; $heat_num < @{$$results{'races'}}; $heat_num++) {
    next if !defined($$results{'races'}[$heat_num]{'tags'}[1]{'title'})
      || $$results{'races'}[$heat_num]{'tags'}[1]{'title'} ne $heat_ref;
    my @res = @{$$results{'races'}[$heat_num]{'results'}};
    for (my $res_num = 0; $res_num < @{$$results{'races'}[$heat_num]{'results'}[$#res]{'rankings'}}; $res_num++) {
      next if $$results{'races'}[$heat_num]{'results'}[$#res]{'rankings'}[$res_num]{'entry'}{'number'} != $bib_no;
      next if defined($$results{'races'}[$heat_num]{'results'}[$#res]{'rankings'}[$res_num]{'color'});
      print "# Fixing rider with bib $bib_no gate to $gate in $heat_ref\n";
      %{$$results{'races'}[$heat_num]{'results'}[$#res]{'rankings'}[$res_num]{'color'}} = (
        'title' => $gate,
        '__typename' => 'Color',
      );
    }
  }
}

# Return true to pacify the compiler
1;
