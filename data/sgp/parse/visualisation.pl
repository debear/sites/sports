#!/usr/bin/perl -w
# Data visualisation methods

use strict;
use POSIX qw(floor);

# Ensure all riders have a set and unique bib_no
sub visualise_meeting {
  my ($riders, $heats) = @_;
  our %config;
  print "\n#\n# Meeting Scorecard\n#\n";

  # On first pass, we need to determine the table width based on rider names
  $config{'visualise_maxlen'} = 0;
  foreach my $bib_no (keys %$riders) {
    my $strlen = length(Encode::decode_utf8($$riders{$bib_no}{'name_full'}));
    $config{'visualise_maxlen'} = $strlen if $strlen > $config{'visualise_maxlen'};
  }

  # Sprint?
  visualise_meeting_sprint($riders, $$heats{'sprint'}) if %{$$heats{'sprint'}};

  # Main Heats
  visualise_meeting_heats($riders, $$heats{'main'});

  # Knockouts
  visualise_meeting_knockout($riders, $$heats{'ko'});
}

sub visualise_meeting_sprint {
  my ($riders, $sprints) = @_;
  print "\n";
  foreach my $sprint (sort keys %$sprints) {
    my ($sprint_num) = ($sprint =~ m/^sprint(\d+)$/gsi);
    visualise_meeting_heat("Sprint $sprint_num", $riders, $$sprints{$sprint});
  }
}

sub visualise_meeting_heats {
  my ($riders, $main) = @_;
  our %config;

  my $seperator = '+----+-' . ('-' x ($config{'visualise_maxlen'} + 5)) . '-+'
    . ('----+' x $config{'main_heats'}) . '-------+------+';

  # Header row
  print "\n# $seperator\n";
  print '# | Dr | ' . sprintf('%-' . ($config{'visualise_maxlen'} + 5) . 's', 'Rider') . ' |';
  for (my $heat_num = 1; $heat_num <= $config{'main_heats'}; $heat_num++) {
    printf(' %2d |', $heat_num);
  }
  print "  Pts  | Pos  |\n";
  print "# $seperator\n";
  # Draw / Riders
  foreach my $bib_no (sort { $$riders{$a}{'draw'} <=> $$riders{$b}{'draw'} } keys %$riders) {
    my %rider = %{$$riders{$bib_no}};

    # Rider info
    my $draw = sprintf('%2d', $rider{'draw'});
    my $bib_no_vis = sprintf("%4s", "#$bib_no");
    my $rider = $rider{'name_full'};
    my $strpad = $config{'visualise_maxlen'} - length(Encode::decode_utf8($rider));
    $rider .= (' ' x $strpad);
    print "# | $draw | $bib_no_vis $rider |";

    # Heats
    for (my $heat_num = 1; $heat_num <= $config{'main_heats'}; $heat_num++) {
      print ' ' . (defined($rider{'by_heat'}{$heat_num}) ? $rider{'by_heat'}{$heat_num}{'gate'} . $rider{'by_heat'}{$heat_num}{'res'} : '  ') . ' |';
    }

    # Point totals
    if (defined($rider{'main_pts'})) {
      printf(' %2dpt%s |', $rider{'main_pts'}, $rider{'main_pts'} != 1 ? 's' : ' ');
    } else {
      print '   -   |';
    }
    # Position
    if (defined($rider{'main_pos'})) {
      printf(' %2d%s |', $rider{'main_pos'},
        $rider{'main_pos'} == 1 ? 'st'
          : ($rider{'main_pos'} == 2 ? 'nd'
            : ($rider{'main_pos'} == 1 ? 'rd' : 'th')));
    } else {
      print '  NR  |';
    }

    print "\n";
  }
  print "# $seperator\n";
}

sub visualise_meeting_knockout {
  my ($riders, $ko) = @_;
  our %config;
  print "\n";

  # Some data conversions
  foreach my $heat ("$config{'ko_heat_code'}1", "$config{'ko_heat_code'}2", 'gf1') {
    foreach my $res (@{$$ko{$heat}}) {
      $$riders{$$res{'bib_no'}}{'ko_heat'} = { } if !defined($$riders{$$res{'bib_no'}}{'ko_heat'});
      $$riders{$$res{'bib_no'}}{'ko_heat'}{$heat} = $res;
    }
  }

  # Header row
  my $sep_top = '+-------' . ('-' x ($config{'visualise_maxlen'} + 5)) . '-+' . ('----+' x 3);
  my $sep_mid_bot = '+-----+-' . ('-' x ($config{'visualise_maxlen'} + 5)) . '-+' . ('----+' x 3);
  print "# $sep_top\n";
  my $heat_char = uc(substr($config{'ko_heat_code'}, 0, 1));
  print "# | Knockout Heats " . (' ' x ($config{'visualise_maxlen'} - 4)) . " | ${heat_char}1 | ${heat_char}2 | GF |\n";
  print "# $sep_mid_bot\n";

  # Top 8 from the meeting
  my %top8 = grep { defined($$riders{$_}{'pos'}) && $$riders{$_}{'pos'} <= $config{'ko_riders'} } keys %$riders;
  foreach my $bib_no (sort { $$riders{$a}{'pos'} <=> $$riders{$b}{'pos'} } %top8) {
    my %rider = %{$$riders{$bib_no}};

    # Position and rider info
    my $bib_no_vis = sprintf("%4s", "#$bib_no");
    my $rider = $rider{'name_full'};
    my $strpad = $config{'visualise_maxlen'} - length(Encode::decode_utf8($rider));
    printf('# | %d%s | %s %s%s |', $rider{'pos'},
      $rider{'pos'} == 1 ? 'st'
        : ($rider{'pos'} == 2 ? 'nd'
          : ($rider{'pos'} == 1 ? 'rd' : 'th')), $bib_no_vis, $rider, ' ' x $strpad);

    # Heats
    foreach my $heat ("$config{'ko_heat_code'}1", "$config{'ko_heat_code'}2", 'gf1') {
      print ' ' . (defined($rider{'ko_heat'}{$heat}) ? $rider{'ko_heat'}{$heat}{'gate'} . $rider{'ko_heat'}{$heat}{'result'} : '  ') . ' |';
    }
    print "\n";
  }
  print "# $sep_mid_bot\n";
}

sub visualise_meeting_heat {
  my ($title, $riders, $heat_res) = @_;
  our %config;

  # Render the table
  my $sep_mid = '+-----+-' . ('-' x ($config{'visualise_maxlen'} + 5)) . '-+------+-----+';
  my $asd = 24 - length($title);
  my $tit_pad_l = floor(($config{'visualise_maxlen'} + $asd) / 2);
  my $tit_pad_r = $config{'visualise_maxlen'} + $asd - $tit_pad_l;
  print '# +-' . ('-' x ($config{'visualise_maxlen'} + 24)) . "-+\n";
  print '# | ' . (' ' x $tit_pad_l) . $title . (' ' x $tit_pad_r) . " |\n";
  print "# $sep_mid\n";
  print '# | Pos | Rider' . (' ' x $config{'visualise_maxlen'}) . " | Gate | Pts |\n";
  print "# $sep_mid\n";
  foreach my $res (@$heat_res) {
    # Extra info
    my $bib_no = sprintf('%4s', "#$$res{'bib_no'}");
    my $rider = $$riders{$$res{'bib_no'}}{'name_full'};
    my $strpad = $config{'visualise_maxlen'} - length(Encode::decode_utf8($rider));
    $rider .= (' ' x $strpad);
    my $pts = (defined($$res{'pts'}) && !defined($$res{'status'}) ? $$res{'pts'} : '-');
    # Render
    print "# |  $$res{'result'}  | $bib_no $rider |  $$res{'gate'}   |  $pts  |\n";
  }
  print "# $sep_mid\n";
}

# Return true to pacify the compiler
1;
