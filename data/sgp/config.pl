#!/usr/bin/perl -w
# Perl config file for get_latest_changes with all DB, dir info, etc

use Dir::Self;
use MIME::Base64;
use JSON;

our %config;
$config{'base_dir'} = __DIR__;
$config{'server_sync'} = 'sports_sgp';

# Database details
$config{'db_admin'} = 'debearco_admin';
$config{'db_name'} = 'debearco_sports';
$config{'db_user'} = $config{'db_name'};
# Password
my $db_pass_file = $config{'base_dir'}; $db_pass_file =~ s@/sites/.+$@/etc/passwd/web/db@;
$config{'db_pass'} = do { local(@ARGV, $/) = $db_pass_file; <> }; chomp($config{'db_pass'});
$config{'db_pass'} = decode_base64($config{'db_pass'});

# Draw mappings
$config{'default_draw'} = $config{'heats_draw'} = { };

# Misc
$config{'main_heats'} = 20;
$config{'ko_riders'} = 8;

# Mapping of Gate code to number
$config{'gates'} = {
  '?' => 0,
  'r' => 1,
  'b' => 2,
  'w' => 3,
  'y' => 4,
};

# Heat statuses
$config{'heat_status'} = {
  'R' => 'r', '5' => 'r', # Treat '5' as an alias
  'M' => 'm',
  'T' => 't',
  'F' => 'f',
  'EX' => 'x',
};

# Knockout config
our $season;
$config{'ko_has_sf'} = ($season <= 2024);
$config{'ko_has_lcq'} = !$config{'ko_has_sf'};
$config{'ko_heat_code'} = ($config{'ko_has_sf'} ? 'sf' : 'lcq');

# File loading helpers
sub load_file {
  my ($file) = @_;

  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = (); # Garbage collect...

  return $contents;
}
sub load_json {
  return decode_json(load_file($_[0]));
}

# Return true to pacify the compiler
1;
