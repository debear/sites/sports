#!/usr/bin/perl -w
# Download, import and process team rosters from theahl.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'rosters';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my @time = localtime();
my $date = time2date(time());
my $season = ($time[4] > 3 ? 1900 : 1899) + $time[5];
my $season_dir = season_dir($season);
my $season_type;

# Identify date to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sth = $dbh->prepare('SELECT MIN(game_date) AS first_game FROM SPORTS_AHL_SCHEDULE WHERE season = ?;');
$sth->execute($season);
my ($first_game) = $sth->fetchrow_array;
$first_game = '2099-12-31';

# If we're processing before the start of the season, we use a special 'date' argument
if ($date le $first_game) { # <= because we're running a day behind, so on first_game we're actually processing the day before
  $date = 'initial' ;
  $season_type = 'regular';
} else {
  # Determine the season type
  my $st_sth = $dbh->prepare('SELECT game_type FROM SPORTS_AHL_SCHEDULE WHERE season = ? AND game_date >= ? ORDER BY game_date LIMIT 1;');
  $st_sth->execute($season, $date);
  ($season_type) = $st_sth->fetchrow_array;
  undef $st_sth if defined($st_sth);
}

undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);

## Inform user
print "\nAcquiring data from $season // $season_type // $date\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Download, if we haven't done so already (handled in-script)
#
print '=> Download: ';
command("$config{'base_dir'}/rosters/download.pl $season_dir $season_type $date",
        "$config{'log_dir'}/$logs{'roster_d'}.log",
        "$config{'log_dir'}/$logs{'roster_d'}.err");
done(9);

# If only downloading, go no further
end_script() if download_only();

#
# Parse
#
print '=> Parse: ';
command("$config{'base_dir'}/rosters/parse.pl $season_dir $season_type $date",
        "$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_p'}.err");
done(12);

#
# Import into the database
#
print '=> Importing Rosters: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_i'}.log",
        "$config{'log_dir'}/$logs{'roster_i'}.err");
done(0);

# Next group of scripts are the first section of post-processing
$config{'exit_status'} = 'post-process-1';

#
# Call player import script to update player details
#
print '=> Process Players: ';
command("$config{'base_dir'}/transactions/import.pl $season",
        "$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_p'}.err");
done(2);

print '=> Importing Players: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_i'}.log",
        "$config{'log_dir'}/$logs{'players_i'}.err");
done(0);

# Next group of scripts are the second section of post-processing
$config{'exit_status'} = 'post-process-2';

#
# Team rosters
#
print '=> Process Rosters: ';
command("echo \"CALL ahl_rosters_initial('$season');\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'transactions_p'}.log",
        "$config{'log_dir'}/$logs{'transactions_p'}.err");
done(2);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'roster_d'       => '01_roster_download',
    'roster_p'       => '02_roster_parse',
    'roster_i'       => '03_roster_import',
    'players_p'      => '04_players_process',
    'players_i'      => '05_players_import',
    'transactions_p' => '06_transactions_process',
  );
}
