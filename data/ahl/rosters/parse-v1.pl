#!/usr/bin/perl -w
# Loop through and parse team rosters, and any new players found

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use DBI;

# Validate the arguments
#  - there needs to be at least two: a season and a season_type
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 2 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $season_type, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;
my $reimport = grep(/\-\-reimport/, @ARGV);

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing rosters for '$season'-'$season_type' // '$date' // '$reimport'\n#\n";

# Now we've set the data_dir, change $game_type and $date to be the versions we want to store in the database
if ($date eq 'initial') {
  # Connect to the database
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

  # Run our query
  my $sth = $dbh->prepare('SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) FROM SPORTS_AHL_SCHEDULE WHERE season = ? AND game_type = ?;');
  $sth->execute($season, $season_type);
  ($date) = $sth->fetchrow_array;
  undef $sth if defined($sth);

  # Disconnect from the database
  $dbh->disconnect if defined($dbh);
}

# Get the list of teams
my $err = 0;
my @list = glob "$config{'data_dir'}/*.htm" or $err = 1;
if ($err) {
  print STDERR "No roster files found?\n";
  exit 20;
}

# Clear a previous run
print "# Clearing previous instance\n";
print "DELETE FROM SPORTS_AHL_TEAMS_ROSTERS WHERE season = '$season' AND the_date = '$date';\n\n";

# Loop through the teams
my %dupe = ( );
foreach my $file (@list) {
  # Get the team_id from the filename
  my ($team_id) = ($file =~ m/$config{'data_dir'}\/([A-Z]+)\.htm/si);
  print "#\n# $team_id ($file)\n#\n\n";

  # Load the file
  my $roster;
  open FILE, "<$file";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);

  # Get the players
  my $pattern = '<tr>\s*<td>(\d+)<\/td>\s*<td><A HREF=\'.*?\?id=(\d+)\' class=\'content\'>(.*?)<\/a><\/td>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<td>.*?<\/td>\s*<td>.*?<\/td>\s*<\/tr>';
  my @matches = ($roster =~ m/$pattern/gsi);
  while (my ($jersey, $remote_id, $name, $pos, $shoots_catches, $ht, $wt, $dob, $birthplace) = splice(@matches, 0, 9)) {
    # Dupe check...
    next if defined($dupe{$remote_id});

    # Split the name
    my @name = split(' ', $name);
    my $fname = convert_text(shift @name);
    my $sname = convert_text(join(' ', @name));
    trim(\$fname); trim(\$sname);

    # Convert height
    $ht =~ s/&nbsp;/ /;
    trim(\$ht);
    if ($ht =~ /^\d+[\-']\d+"?$/) {
      my ($ht_ft, $ht_in) = ($ht =~ m/^(\d+)[\-'](\d+)"?$/);
      $ht = ($ht_ft * 12) + $ht_in;
    } else {
      $ht = 'NULL';
    }

    # Convert weight
    $wt =~ s/&nbsp;/ /;
    trim(\$wt);
    if ($wt eq '') {
      $wt = 'NULL';
    }

    # Process the date of birth
    $dob =~ s/&nbsp;/ /;
    trim(\$dob);
    if ($dob ne '') {
      my ($dob_m, $dob_d, $dob_y) = ($dob =~ m/^(.{3}) (\d+), (\d{4})$/);
      $dob = sprintf('\'%04d-%02d-%02d\'', $dob_y, $config{'map'}{'months'}{$dob_m}, $dob_d);
    } else {
      $dob = 'NULL';
    }

    # And then the place of birth
    my ($birthplace_country) = convert_birthplace(\$birthplace);

    # Convert Shoots/Catches
    $shoots_catches =~ s/&nbsp;/ /;
    trim(\$shoots_catches);
    if ($shoots_catches ne '') {
      $shoots_catches = '\'' . ($shoots_catches eq 'L' ? 'Left' : 'Right') . '\'';
    } else {
      $shoots_catches = 'NULL';
    }

    # Output the SQL
    print "# $fname $sname, #$jersey ($remote_id)\n";
    # - Player
    print "INSERT IGNORE INTO SPORTS_AHL_PLAYERS_IMPORT (player_id, remote_id, profile_imported) VALUES (NULL, $remote_id, NOW()) ON DUPLICATE KEY UPDATE profile_imported = VALUES(profile_imported);\n";
    print "SELECT \@player_$remote_id := player_id FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = $remote_id;\n"; # Get the ID again, or the new auto_incremented value
    print "INSERT INTO SPORTS_AHL_PLAYERS (player_id, first_name, surname, dob, birthplace, birthplace_country, height, weight, shoots_catches) VALUES (\@player_$remote_id, '$fname', '$sname', $dob, $birthplace, $birthplace_country, $ht, $wt, $shoots_catches) ON DUPLICATE KEY UPDATE height = VALUES(height), weight = VALUES(weight), shoots_catches = VALUES(shoots_catches);\n";
    # - Roster
    print "INSERT INTO SPORTS_AHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos) VALUES ('$season', '$date', '$team_id', \@player_$remote_id, '$jersey', '$pos');\n\n";
    $dupe{$remote_id} = 1;
  }
}

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_AHL_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;\n";

# Return true to pacify the compiler
1;
