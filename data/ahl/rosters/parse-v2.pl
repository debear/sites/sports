#!/usr/bin/perl -w
# Loop through and parse team rosters as new transactions

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use DBI;
use JSON;

# Validate the arguments
#  - there needs to be at least two: a season and a season_type
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 2 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $season_type, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

print "#\n# Parsing rosters for '$season'-'$season_type' // '$date' ('$date_roster')\n#\n";

# This version is only meant to be run at the start of the season...
if ($date ne 'initial') {
  print STDERR "v2 of this script is only intended to run in 'initial' mode, as the transaction list deals with day-to-day variances\n";
  exit 10;
}
my $date_var = $date;
$date_var =~ s/-//g;

# Get the list of teams
my $err = 0;
my @list = glob "$config{'data_dir'}/*.json.gz" or $err = 1;
if ($err) {
  print STDERR "No roster files found?\n";
  exit 20;
}

# Some characters in player names (used in SQL comments only) need UTF-8 recognition
binmode STDOUT, ":utf8";

# Clear a previous run
print "# Clearing previous instance\n";
print "DELETE FROM SPORTS_AHL_TRANSACTIONS WHERE season = '$season' AND the_date = '$date_roster';\n\n";

# Loop through the teams
my %dupe = ( );
foreach my $file (@list) {
  # Get the team_id from the filename
  my ($team_id) = ($file =~ m/$config{'data_dir'}\/([A-Z]+)\.json\.gz/si);
  print "#\n# $team_id ($file)\n#\n\n";

  # Load the file
  my $roster;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);
  $roster =~ s/^\(//; $roster =~ s/\)$//;
  $roster = decode_json($roster);

  foreach my $s (@{$$roster{'roster'}[0]{'sections'}}) {
    next if $$s{'title'} eq 'Team Personnel'; # Not all sections apply...
    print "## $$s{'title'}\n\n";
    foreach my $p (@{$$s{'data'}}) {
      $p = $$p{'row'};
      print "# $$p{'name'}, #$$p{'tp_jersey_number'} ($$p{'position'}, ID: $$p{'player_id'})\n";

      my $player_var = "\@player_" . $date_var . $$p{'player_id'};
      print "SELECT player_id INTO $player_var FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = '$$p{'player_id'}';\n";
      print "INSERT INTO SPORTS_AHL_TRANSACTIONS (season, the_date, remote_id, player_id, team_id, pos, type, detail)
               VALUES ('$season', '$date_roster', '$$p{'player_id'}', $player_var, '$team_id', '$$p{'position'}', 'add', 'Added')
             ON DUPLICATE KEY UPDATE type = IF(type = VALUES(type), VALUES(type), 'both');\n\n";
    }
  }
}

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_AHL_TRANSACTIONS ORDER BY season, the_date, remote_id;\n";

# Return true to pacify the compiler
1;
