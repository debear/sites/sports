#!/usr/bin/perl -w
# Get player info for players on current theahl.com rosters

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use File::Path;
use DBI;

# Validate the arguments
#  - there needs to be at least two: a season and a season_type
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 2 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $season_type, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# v2 is only meant to be run at the start of the season...
if ($season >= 2016 && $date ne 'initial') {
  print STDERR "v2 of this script is only intended to run in 'initial' mode, as the transaction list deals with day-to-day variances\n";
  exit 20;
}

$config{'debug'} = 0;

print "#\n# Downloading rosters for '$season'-'$season_type' // '$date'\n#\n";

# Create our data directory
mkpath($config{'data_dir'})
  if ! -e $config{'data_dir'};

my $rem_season = convert_season($season, $season_type);

# Get the teams from the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sth = $dbh->prepare('SELECT TEAM.team_id, TEAM.city, TEAM.franchise FROM SPORTS_AHL_TEAMS_GROUPINGS AS DIVN JOIN SPORTS_AHL_TEAMS AS TEAM ON (TEAM.team_id = DIVN.team_id) WHERE ? BETWEEN DIVN.season_from AND IFNULL(DIVN.season_to, 2099) ORDER BY DIVN.team_id;');
$sth->execute($season);
my $teams = $sth->fetchall_arrayref;

while (my $team = splice(@$teams, 0, 1)) {
  my ($team_id, $team_city, $team_franchise) = @$team;
  print "# $team_city $team_franchise ($team_id)\n";

  my $rem_team_id = convert_team_id($team_id);
  my $url; my $ext;
  if ($season < 2016) {
    $url = "http://theahl.com/stats/roster.php?step=&sub=&season_id=$rem_season&division_id=0&tournament_id=0&team_id=$rem_team_id";
    $ext = 'htm';
  } else {
    $url = "https://lscluster.hockeytech.com/feed/index.php?feed=statviewfeed&view=roster&team_id=$rem_team_id&season_id=$rem_season&key=$config{'app_key'}&client_code=ahl&site_id=1&league_id=4&lang=en";
    $ext = 'json';
  }
  my $local = "$config{'data_dir'}/$team_id.$ext.gz";
  print "## From: $url\n## To: $local\n";
  download($url, $local, {'skip-today' => 1, 'gzip' => 1, 'debug' => $config{'debug'}});
  print "\n";
}

# Disconnect from the database
undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);
