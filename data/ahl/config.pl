#!/usr/bin/perl -w
# Perl config file with all DB, dir info, etc

use MIME::Base64;
use HTML::Entities;
use Encode;
use DBI;

our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

$config{'sport'} = 'ahl';
$config{'base_dir'} .= '/' . $config{'sport'}; # Append the AHL dir
$config{'cdn_dir'} .= '/' . $config{'sport'}; # Append the AHL dir
$config{'num_recent'} = 10; # The number of games in our recent game calcs

# From 2016-17, website needs an app_key
$config{'app_key'} = '50c2cd9b5e18e390';

# Maps
$config{'map'} = {
  'team_id' => { 'ABB' => 379,   # Abbotsford Heat
                 'ABC' => 440,   # Abbotsford Canucks
                 'ADK' => 400,   # Adirondack Flames
                 'ADP' => 313,   # Adirondack Phantoms
                 'ALB' => 369,   # Albany Devils
                 'ALR' => 318,   # Albany River Rats
                 'BAK' => 402,   # Bakersfield Condors
                 'BEL' => 413,   # Bellville Senators
                 'BID' => 414,   # Binghamton Devils
                 'BIN' => 315,   # Binghamton Senators
                 'BRI' => 317,   # Bridgeport Sound Tigers
                 'CGY' => 444,   # Calgary Wranglers
                 'CHA' => 384,   # Charlotte Checkers
                 'CHI' => 330,   # Chicago Wolves
                 'CLE' => 325,   # Cleveland Barons
                 'CLM' => 373,   # Cleveland Monsters
                 'COL' => 419,   # Colorado Eagles
                 'CON' => 307,   # Connecticut Whale
                 'CV'  => 445,   # Coachella Valley Firebirds
                 'GR'  => 328,   # Grand Rapids Griffins
                 'HAM' => 326,   # Hamilton Bulldogs
                 'HFD' => 307,   # Hartford Wolf Pack
                 'HER' => 319,   # Hershey Bears
                 'HOU' => 329,   # Houston Aeros
                 'HSK' => 437,   # Henderson Silver Knights
                 'IAC' => 375,   # Iowa Chops
                 'IAS' => 336,   # Iowa Stars
                 'IAW' => 389,   # Iowa Wild
                 'LAV' => 415,   # Laval Rocket
                 'LE'  => 373,   # Lake Erie Monsters
                 'LV'  => 313,   # Lehigh Valley Phantoms
                 'LWD' => 369,   # Lowell Devils
                 'LWM' => 311,   # Lowell Lock Monsters
                 'MCH' => 344,   # Manchester Monarchs
                 'MTB' => 321,   # Manitoba Moose
                 'MIL' => 327,   # Milwaukee Admirals
                 'NOR' => 314,   # Norfolk Admirals
                 'OKC' => 383,   # Oklahoma City Barons
                 'OMA' => 337,   # Omaha Ak-Sar-Ben Knights
                 'ONT' => 403,   # Ontario Reign
                 'PEO' => 334,   # Peoria Rivermen
                 'PHI' => 313,   # Philadelphia Phantoms
                 'POR' => 310,   # Portland Pirates
                 'PRO' => 309,   # Providence Bruins
                 'QC'  => 371,   # Quad City Flames
                 'RCH' => 323,   # Rochester Americans
                 'RFD' => 372,   # Rockford IceHogs
                 'SA'  => 331,   # San Antonio Rampage
                 'SD'  => 404,   # San Diego Gulls
                 'SJ'  => 405,   # San Jose Barracuda
                 'SJI' => 387,   # St. John's IceCaps
                 'SPR' => 312,   # Springfield Falcons
                 'SPT' => 411,   # Springfield Thunderbirds
                 'STJ' => 387,   # St. John's IceCaps
                 'STO' => 406,   # Stockton Heat
                 'SYR' => 324,   # Syracuse Crunch
                 'TEX' => 380,   # Texas Stars
                 'TOR' => 335,   # Toronto Marlies
                 'TUC' => 412,   # Tucson Roadrunners
                 'UTI' => 390,   # Utica Comets (pre-2020/21)
                 'UTC' => 390,   # Utica Comets (from 2021/22)
                 'WBS' => 316,   # Wilkes-Barre/Scranton Penguins
                 'WOR' => 370 }, # Worcester Sharks
  'seasons' => { 2005 => { 'regular' => 1,  'playoff' => 7 },
                 2006 => { 'regular' => 8,  'playoff' => 10 },
                 2007 => { 'regular' => 12, 'playoff' => 15 },
                 2008 => { 'regular' => 16, 'playoff' => 29 },
                 2009 => { 'regular' => 30, 'playoff' => 33 },
                 2010 => { 'regular' => 34, 'playoff' => 36 },
                 2011 => { 'regular' => 37, 'playoff' => 39 },
                 2012 => { 'regular' => 40, 'playoff' => 42 },
                 2013 => { 'regular' => 43, 'playoff' => 47 },
                 2014 => { 'regular' => 48, 'playoff' => 50 },
                 2015 => { 'regular' => 51, 'playoff' => 53 },
                 2016 => { 'regular' => 54, 'playoff' => 56 },
                 2017 => { 'regular' => 57, 'playoff' => 60 },
                 2018 => { 'regular' => 61, 'playoff' => 64 },
                 2019 => { 'regular' => 65, 'playoff' => undef },
                 2020 => { 'regular' => 68, 'playoff' => 72 },
                 2021 => { 'regular' => 73, 'playoff' => 76 },
                 2022 => { 'regular' => 77, 'playoff' => 80 },
                 2023 => { 'regular' => 81, 'playoff' => 84 },
                 2024 => { 'regular' => 86, 'playoff' => 89 } },
  'months' => { 'Jan' => 1, 'Feb' =>  2, 'Mar' =>  3, 'Apr' =>  4,
                'May' => 5, 'Jun' =>  6, 'Jul' =>  7, 'Aug' =>  8,
                'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12 },
  'ca_provinces' => { 'AB' => 1, 'BC' => 1, 'MB' => 1, 'NB' => 1, 'NL'  => 1,
                      'NS' => 1, 'NT' => 1, 'NU' => 1, 'ON' => 1, 'PEI' => 1,
                      'QC' => 1, 'SK' => 1, 'YT' => 1 },
};

# Check as to whether we parse/import the data or just download it
sub download_only {
  return defined($config{'download_only'}) && $config{'download_only'};
}

# Get info for a given game
sub get_game_info {
  my @db_args = @_;

  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT SPORTS_AHL_SCHEDULE.game_id, SPORTS_AHL_SCHEDULE.remote_id,
                    LOWER(VISITOR.franchise) AS visitor,
                    LOWER(VISITOR.city) AS visitor_city,
                    SPORTS_AHL_SCHEDULE.visitor AS visitor_id,
                    LOWER(HOME.franchise) AS home,
                    LOWER(HOME.city) AS home_city,
                    SPORTS_AHL_SCHEDULE.home AS home_id
             FROM SPORTS_AHL_SCHEDULE
             JOIN SPORTS_AHL_TEAMS AS VISITOR
               ON (VISITOR.team_id = SPORTS_AHL_SCHEDULE.visitor)
             JOIN SPORTS_AHL_TEAMS AS HOME
               ON (HOME.team_id = SPORTS_AHL_SCHEDULE.home)
             WHERE SPORTS_AHL_SCHEDULE.season = ?
             AND   SPORTS_AHL_SCHEDULE.game_type = ?
             AND   SPORTS_AHL_SCHEDULE.game_id = ?;';
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, @db_args[0..2]);
  $dbh->disconnect if defined($dbh);
  return $$ret[0];
}

#
# Convert a single year in to the dir data is stored
#
sub season_dir {
  return sprintf('%04d-%02d', $_[0], ($_[0] + 1) % 100);
}

#
# Convert a team_id in to an eliteleague.co.uk ID
#
sub convert_team_id {
  my ($team_id) = @_;
  return $config{'map'}{'team_id'}{$team_id};
}

#
# Map a remote (numeric) team_id to our local (three-char) team_id
#
sub convert_team_remote_id {
  my ($season, $remote_id) = @_;

  # Season variations of remote_team_id
  if (!defined($config{'map'}{'team_remote_id'})) {
    # Copy (by value)
    %{$config{'map'}{'team_remote_id'}} = ();
    foreach my $team_id (keys %{$config{'map'}{'team_id'}}) {
      # Season checks where there are duplicate IDs
      $map_remote_id = $config{'map'}{'team_id'}{$team_id};
      my $current = 1; # Assume so, unless one of the following rules hits...

      # 307 - Whale / Wolf Pack
      if ($map_remote_id == 307) {
        $current = ($team_id eq 'CON' && $season >= 2010 && $season <= 2012)  # Whale
                || ($team_id eq 'HFD' && ($season < 2010 || $season > 2012)); # Wolf Pack

      # 313 - Phantoms
      } elsif ($map_remote_id == 313) {
        $current = ($team_id eq 'PHI' && $season < 2009)  # Philadelphia
                || ($team_id eq 'ADP' && $season >= 2009 && $season <= 2013) # Adirondack
                || ($team_id eq 'LV'  && $season > 2013); # Lehigh Valley

      # 369 - Devils
      } elsif ($map_remote_id == 369) {
        $current = ($team_id eq 'ALB' && $season >= 2010) # Albany
                || ($team_id eq 'LWD' && $season < 2010); # Lowell

      # 387 - Ice Caps
      } elsif ($map_remote_id == 387) {
        $current = ($team_id eq 'SJI' && $season >= 2015) # MTL affiliation
                || ($team_id eq 'STJ' && $season < 2015); # WPG affiliation

      # 373 - Monsters
      } elsif ($map_remote_id == 373) {
        $current = ($team_id eq 'CLM' && $season >= 2016) # Cleveland
                || ($team_id eq 'LE' && $season < 2016);  # Lake Erie

      # 390 - Comets
      } elsif ($map_remote_id == 390) {
        $current = ($team_id eq 'UTC' && $season >= 2021) # NJ affiliation
                || ($team_id eq 'UTI' && $season < 2021); # VAN affiliation
      }

      # Perform copy...?
      $config{'map'}{'team_remote_id'}{$map_remote_id} = $team_id
        if $current;
    }
  }

  return $config{'map'}{'team_remote_id'}{$remote_id};
}

#
# Convert a season/season_type in to an eliteleague.co.uk ID
#
sub convert_season {
  my ($season, $season_type) = @_;
  return $config{'map'}{'seasons'}{$season}{$season_type};
}

#
# Convert an ID to a summary of the playoff series
#
sub playoff_summary {
  my ($season, $game_id) = @_;
  my $series;
  my $inc_series_code = ($game_id < 100 || $game_id !~ /^4/);

  # First Round
  if ($game_id < 100) {
    $series = '1R';
  # Calder Cup
  } elsif ($game_id =~ /^4/) {
    $series = 'CCF';
  # Conference Final
  } elsif ($game_id =~ /^3/) {
    $series = 'CF';
  # Divisional-based
  } elsif ($season < 2011 || $season > 2014) {
    $series = 'D' . ($game_id =~ /^1/ ? 'S' : '') . 'F';
  # Conference-based
  } else {
    $series = 'C' . ($game_id =~ /^1/ ? 'Q' : 'S') . 'F';
  }

  return $series . ($inc_series_code ? ' Series ' . chr(64 + substr(sprintf("%03d", $game_id), 1, 1)) : '') . ', Game ' . substr($game_id, -1);
}

#
# Convert a player's birthplace
#
sub convert_birthplace {
  my ($birthplace) = @_;
  my $birthplace_country = 'NULL';

  trim($birthplace);
  if ($$birthplace ne '') {
    # Place / Country
    if ($birthplace =~ m/,/) {
      # Split to determine state/province/country
      my ($bp_l, $bp_r) = ($$birthplace =~ m/^(.+),\s*?([^,]+)$/);
      trim (\$bp_l); trim (\$bp_r);

      # State/Province?
      if ($bp_r =~ m/^[A-Z]{2,3}$/) {
        $$birthplace = "$bp_l, $bp_r";
        $birthplace_country = (defined($config{'map'}{'ca_provinces'}{$bp_r}) ? 'Canada' : 'United States');

      # Country
      } else {
        $$birthplace = $bp_l;
        $birthplace_country = $bp_r;
      }

      $$birthplace = "'" . convert_text($$birthplace) . "'";
      $birthplace_country = "'" . convert_text($birthplace_country) . "'";

    # Place only
    } else {
      $$birthplace = "'" . convert_text($$birthplace) . "'";
    }

  } else {
    $$birthplace = 'NULL';
  }

  return $birthplace_country;
}

# Return true to pacify the compiler
1;
