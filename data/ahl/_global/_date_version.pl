#!/usr/bin/perl -w
# Determine which version of the script to run and run it

use File::Basename;

my $script = $0;
my $section = basename(dirname($script));

# Date calcs
my @time = localtime();
my $date = sprintf('%04d-%02d', $time[5]+1900, $time[4]+1);
my $v;
if ($date lt '2016-08') {
    $v = 1;
} elsif (($date lt '2017-08') || ($section ne 'games')) {
    $v = 2;
} else {
    $v = 3;
}

$script =~ s/\.pl/-v$v.pl/;
require $script;
