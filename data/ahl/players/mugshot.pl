#!/usr/bin/perl -w
# Process and copy the player's mugshot

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use POSIX;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player mugshot script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Processing mugshot for (remote) player '$remote_id'\n#\n\n";

# Determine the player_id for this player - either existing value if already imported, or inserted if not
# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
benchmark_stamp('Connect to Database');

# Run our query
my $sth = $dbh->prepare('SELECT player_id FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = ?;');
$sth->execute($remote_id);
my ($player_id) = $sth->fetchrow_array;
undef $sth if defined($sth);
benchmark_stamp('Determine player_id');

if (!defined($player_id)) {
  print STDERR "Unable to import mugshot for remote '$remote_id': player not already imported?\n";
  exit 98;
}

# Declare variables
my $img_src = "$config{'data_dir'}/mugshot.jpg";
my $img_dst = "$config{'cdn_dir'}/players/$player_id.png";
my $resync = grep(/\-\-resync/, @ARGV);

# Validate...
if (! -e $config{'data_dir'}) {
  print STDERR "# Unable to parse player $player_id\n";
  exit 10;
} elsif ( ! -e $img_src ) {
  print "# No source mugshot found for player $player_id\n";
  exit;
} elsif ( -e $img_dst && !$resync ) {
  print "# Destination mugshot already found for player $player_id\n";
  exit;
} elsif ( -e $img_dst ) {
  # A slightly more complicated validation: the source file needs to be newer
  my @stat_src = stat $img_src;
  my @stat_dst = stat $img_dst;
  if ($stat_dst[9] > $stat_src[9]) { # 9 = mtime
    print "# Destination mugshot file newer than source file for player $player_id\n";
    exit;
  }
}
benchmark_stamp('Validate image');

# Any cropping required?
my $preimg = '';
my $proc = '';
my $d = `identify $img_src | grep -Po '^.*?(\\d+x\\d+) '`;
my ($w, $h) = ($d =~ m/(\d+)x(\d+)/);

if ($h >= 200) {
  # Calculate height at 80%
  my $h2 = ($w > $h ? ceil($h * 0.8) : $h);
  # Determine width of resulting height down to 150px
  my $w2 = $w / ($h2 / 150);
  # Calculate required offset for final 100x150 image
  my $o = floor(($w2 - 100) / 2);
  # Our resulting argument...
  $proc = "-crop ${w}x${h2}+0+0 -resize ${w2}x150 -crop 100x150+$o+0";
} elsif ($h >= 150) {
  # From the top
  my $o = floor(($w - 100) / 2);
  $proc = "-crop 100x150+$o+0";
} elsif ($h < 150) {
  # Centre a shorter image
  $preimg = '-background none';
  $proc = '-gravity center -extent 100x150';
}
benchmark_stamp('Pre-process');

# Convert and then compress
my $cmd = "convert $preimg $img_src -quiet $proc $img_dst";
print "# Converting: $cmd\n";
`$cmd`;

$cmd = "image-compress-png --quiet $img_dst";
print "# Compressing: $cmd\n";
`$cmd`;

my @stat = stat $img_dst;
if ($stat[7]) { # 7 = size
  print "# Player mugshot for $player_id converted\n";
} else {
  unlink $img_dst;
  print "# Attempted to convert player mugshot for $player_id, but removed due to error in conversion process\n";
}
benchmark_stamp('Convert');

