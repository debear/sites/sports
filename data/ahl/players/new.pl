#!/usr/bin/perl -w
# Import new players identified in the game scripts

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Path;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

print "#\n# Importing new players\n#\n\n";

# Get the list of players to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT IMPORT.remote_id, IMPORT.player_id
FROM SPORTS_AHL_PLAYERS_IMPORT AS IMPORT
LEFT JOIN SPORTS_AHL_PLAYERS AS PLAYER
  ON (PLAYER.player_id = IMPORT.player_id)
WHERE PLAYER.player_id IS NULL
ORDER BY IMPORT.remote_id;';
my $players = $dbh->selectall_arrayref($sql, { Slice => {} });
$dbh->disconnect if defined($dbh);

# If none found, no need to continue
if (!@{$players}) {
  print "# Skipping, no players found.\n";
  exit;
}

# Loop through them...
foreach my $p (@{$players}) {
  print "# Remote: $$p{'remote_id'}, Imported: $$p{'player_id'}\n";
  print `$config{'base_dir'}/players/_import.pl $$p{'remote_id'}` . "\n";
}

