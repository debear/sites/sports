#!/usr/bin/perl -w
# Get player info for a specific player on theahl.com
#  Note: We still have to download the profile page even if we only want the mugshot, as the mugshot URL is stored in the profile

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Path;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player download script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;
my $force = grep(/\-\-force/, @ARGV);

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
benchmark_stamp('Config Loaded');

print "#\n# Downloading (remote) player '$remote_id'\n#\n";

# If it exists (and we're not in force mode), do not continue
if ( -e $config{'data_dir'} && !$force ) {
  print "# Skipping, previously downloaded...\n\n";
  exit;
}

# Create our data directory
mkpath($config{'data_dir'})
  if ! -e $config{'data_dir'};
benchmark_stamp('Created data_dir');

# Download the profile
my $profile = "$config{'data_dir'}/profile.htm";
my $url = "http://theahl.com/stats/player.php?id=$remote_id";
print "# Profile\n# From: $url\n# To: $profile\n\n";
download($url, $profile);
benchmark_stamp('Downloaded Profile');

# Then load to determine the mugshot URL
print "# Mugshot\n";
my $mugshot = "$config{'data_dir'}/mugshot.jpg";
my $img_url = `grep -Po '<img src="http:\\/\\/cluster\\.leaguestat\\.com\\/download\\.php\\?client_code\\=ahl\\&file_path=media\\/.*?\\.jpg\\&amp;w=120"' $profile | grep -Po 'http.*?w=120'`;
benchmark_stamp('Determined Mugshot URL');

if ($img_url) {
  chomp($img_url);
  print "# From: $img_url\n# To: $mugshot\n\n";
  download($img_url, $mugshot);
  benchmark_stamp('Downloaded Mugshot');
} else {
  print "# Not found...\n\n";
}

# Return true to pacify the compiler
1;
