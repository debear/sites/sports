#!/usr/bin/perl -w
# Get player info for a specific player on theahl.com
#  Note: We still have to download the profile page even if we only want the mugshot, as the mugshot URL is stored in the profile

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Path;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player download script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;
my $force = grep(/\-\-force/, @ARGV);

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
benchmark_stamp('Config Loaded');

print "#\n# Downloading (remote) player '$remote_id'\n#\n";

# If it exists (and we're not in force mode), do not continue
if ( -e $config{'data_dir'} && !$force ) {
  print "# Skipping, previously downloaded...\n\n";
  exit;
}

# Create our data directory
mkpath($config{'data_dir'})
  if ! -e $config{'data_dir'};
benchmark_stamp('Created data_dir');

# Need to calculate the season
my @time = localtime();
my $season = ($time[4] > 7 ? 1900 : 1899) + $time[5];
my $remote_season = convert_season($season, 'regular');

# Download the profile
my $profile = "$config{'data_dir'}/profile.htm.gz";
my $url = "https://lscluster.hockeytech.com/feed/index.php?feed=statviewfeed&view=player&player_id=$remote_id&season_id=$remote_season&site_id=1&key=$config{'app_key'}&client_code=ahl&league_id=&lang=en&statsType=standard";
print "# Profile\n# From: $url\n# To: $profile\n\n";
download($url, $profile, {'skip-today' => 1, 'if-modified-since' => 1, 'gzip' => 1});
benchmark_stamp('Downloaded Profile');

# Then load to determine the mugshot URL
print "# Mugshot\n";
my $mugshot = "$config{'data_dir'}/mugshot.jpg";
my $player;
open FILE, "gzip -dc $profile |";
my @contents = <FILE>;
close FILE;
$player = join('', @contents);
my ($img_url) = ($player =~ m/"profileImage":"([^"]+)"/);
$img_url =~ s/\\\//\//g
  if defined($img_url);
benchmark_stamp('Determined Mugshot URL');

if ($img_url) {
  chomp($img_url);
  $img_url =~ s/\\\\/\\/g;
  print "# From: $img_url\n# To: $mugshot\n\n";
  download($img_url, $mugshot, {'skip-today' => 1, 'if-modified-since' => 1});
  benchmark_stamp('Downloaded Mugshot');
} else {
  print "# Not found...\n\n";
}

# Return true to pacify the compiler
1;
