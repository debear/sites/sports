#!/usr/bin/perl -w
# Parse an individual player's details from theahl.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Determine the player_id for this player - either existing value if already imported, or inserted if not
# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
benchmark_stamp('Connect to Database');

# Run our query
my $sth = $dbh->prepare('SELECT player_id FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = ?;');
$sth->execute($remote_id);
my ($player_id) = $sth->fetchrow_array;
undef $sth if defined($sth);
benchmark_stamp('Determine player_id from existing');

my $new = 0;
if (!defined($player_id)) {
  # Insert
  $sth = $dbh->prepare('INSERT INTO SPORTS_AHL_PLAYERS_IMPORT (player_id, remote_id, profile_imported) VALUES (NULL, ?, NOW()) ON DUPLICATE KEY UPDATE profile_imported = VALUES(profile_imported);');
  $sth->execute($remote_id);
  # Get the ID...
  $player_id = $sth->{mysql_insertid};
  undef $sth if defined($sth);
  $new = 1;
  benchmark_stamp('Determine player_id as new record');
}

# Disconnect from the database
$dbh->disconnect if defined($dbh);
benchmark_stamp('Disconnect from database');
print "#\n# Parseing (remote) player '$remote_id' (Imported ID: '$player_id'" . ($new ? ' - New' : '') . ")\n#\n";

# Load the file
my $file = "$config{'data_dir'}/profile.htm";
my $player;
open FILE, "<$file";
my @contents = <FILE>;
close FILE;
$player = join('', @contents);
benchmark_stamp('Load file');

# Name
my ($name) = ($player =~ m/<td>Name:<\/td>\s*<td>(.*?)<\/td>/gsi);
my @name = split(' ', $name);
my $fname = convert_text(shift @name);
my $sname = convert_text(join(' ', @name));
trim(\$fname); trim(\$sname);
# Strip initial from surname component (e.g., Firstname (I.) Surname)
$sname =~ s/^\([A-Z]+\.\) //;
benchmark_stamp('Parse: Player Name');

# Height
my ($ht) = ($player =~ m/<td>Height:<\/td>\s*<td>(.*?)<\/td>/gsi);
$ht = '' if !defined($ht);
$ht =~ s/&nbsp;/ /;
trim(\$ht);
if ($ht =~ /^\d+[\-']\d+"?$/) {
  my ($ht_ft, $ht_in) = ($ht =~ m/^(\d+)[\-'](\d+)"?$/);
  $ht = ($ht_ft * 12) + $ht_in;
} else {
  $ht = 'NULL';
}
benchmark_stamp('Parse: Height');

# Weight
my ($wt) = ($player =~ m/<td>Weight:<\/td>\s*<td>(.*?)<\/td>/gsi);
$wt = '' if !defined($wt);
$wt =~ s/&nbsp;/ /;
trim(\$wt);
if ($wt eq '') {
  $wt = 'NULL';
}
benchmark_stamp('Parse: Weight');

# Date of Birth
my ($dob) = ($player =~ m/<td>Birthdate:<\/td>\s*<td>(.*?)<\/td>/gsi);
$dob = '' if !defined($dob);
$dob =~ s/&nbsp;/ /;
trim(\$dob);
if ($dob ne '') {
  $dob = '\'' . $dob . '\'';
} else {
  $dob = 'NULL';
}
benchmark_stamp('Parse: Date of Birth');

# Place of Birth
my ($birthplace) = ($player =~ m/<td>Birth place:<\/td>\s*<td>(.*?)<\/td>/gsi);
$birthplace = '' if !defined($birthplace);
my ($birthplace_country) = convert_birthplace(\$birthplace);
benchmark_stamp('Parse: Place of Birth');

# Shoots / Catches
my ($shoots_catches) = ($player =~ m/<td>(?:Shoots|Catches):<\/td>\s*<td>(.*?)<\/td>/gsi);
if (defined($shoots_catches)) {
  $shoots_catches = '\'' . ($shoots_catches eq 'L' ? 'Left' : 'Right') . '\'';
} else {
  $shoots_catches = 'NULL';
}
benchmark_stamp('Parse: Shoots/Catches');

print "INSERT INTO SPORTS_AHL_PLAYERS (player_id, first_name, surname, dob, birthplace, birthplace_country, height, weight, shoots_catches) VALUES ('$player_id', '$fname', '$sname', $dob, $birthplace, $birthplace_country, $ht, $wt, $shoots_catches) ON DUPLICATE KEY UPDATE height = VALUES(height), weight = VALUES(weight), shoots_catches = VALUES(shoots_catches);\n\n";

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_AHL_PLAYERS ORDER BY player_id;\n";
print "ALTER TABLE SPORTS_AHL_PLAYERS_IMPORT ORDER BY player_id;\n" if $new;
print "\n";

# Return true to pacify the compiler
1;
