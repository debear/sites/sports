#!/usr/bin/perl -w
# Parse an individual player's details from theahl.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Determine the player_id for this player - either existing value if already imported, or inserted if not
# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
benchmark_stamp('Connect to Database');

# Run our query
my $sth = $dbh->prepare('SELECT player_id FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = ?;');
$sth->execute($remote_id);
my ($player_id) = $sth->fetchrow_array;
undef $sth if defined($sth);
benchmark_stamp('Determine player_id from existing');

my $new = 0;
if (!defined($player_id)) {
  # Insert
  $sth = $dbh->prepare('INSERT INTO SPORTS_AHL_PLAYERS_IMPORT (player_id, remote_id, profile_imported) VALUES (NULL, ?, NOW()) ON DUPLICATE KEY UPDATE profile_imported = VALUES(profile_imported);');
  $sth->execute($remote_id);
  # Get the ID...
  $player_id = $sth->{mysql_insertid};
  undef $sth if defined($sth);
  $new = 1;
  benchmark_stamp('Determine player_id as new record');
}

# Disconnect from the database
$dbh->disconnect if defined($dbh);
benchmark_stamp('Disconnect from database');
print "#\n# Parseing (remote) player '$remote_id' (Imported ID: '$player_id'" . ($new ? ' - New' : '') . ")\n#\n";

# Load the file
my $file = "$config{'data_dir'}/profile.htm.gz";
my $player;
open FILE, "gzip -dc $file |";
my @contents = <FILE>;
close FILE;
$player = join('', @contents);
benchmark_stamp('Load file');
$player =~ s/^\(//; $player =~ s/\)$//;
$player = decode_json($player);
benchmark_stamp('Decoded JSON');

# Fix: Sometimes the height and weight are backwards...
if ($$player{'info'}{'height'} =~ m/^\d+$/ && $$player{'info'}{'weight'} =~ m/^\d+[\-\']\d+$/) {
  print "# (Data Fix: Height/Weight in wrong fields.\n";
  my $tmp_weight = $$player{'info'}{'height'};
  $$player{'info'}{'height'} = $$player{'info'}{'weight'};
  $$player{'info'}{'weight'} = $tmp_weight;
}
# Fix: accidental UTF-16 encoding
for my $field ('firstName', 'lastName') {
  $$player{'info'}{$field} =~ s/\x{2019}/'/g;
}

# Name
my $fname = convert_text($$player{'info'}{'firstName'});
my $sname = convert_text($$player{'info'}{'lastName'});
trim(\$fname); trim(\$sname);
benchmark_stamp('Parse: Player Name');

# Height
my $ht = 'NULL';
my ($ht_ft, $ht_in) = ($$player{'info'}{'height'} =~ m/^(\d+)[\-\'](\d+)$/);
if (defined($ht_ft)) {
  $ht = ($ht_ft * 12) + $ht_in;
}
benchmark_stamp('Parse: Height');

# Weight
my $wt = $$player{'info'}{'weight'};
if (!defined($wt) || !$wt || ($wt !~ /^\d+$/)) {
  $wt = 'NULL';
}
benchmark_stamp('Parse: Weight');

# Date of Birth
my $dob = $$player{'info'}{'birthDate'};
if ($dob) {
  $dob = '\'' . $dob . '\'';
} else {
  $dob = 'NULL';
}
benchmark_stamp('Parse: Date of Birth');

# Place of Birth
my ($birthplace) = $$player{'info'}{'birthPlace'};
$birthplace = '' if !defined($birthplace);
my ($birthplace_country) = convert_birthplace(\$birthplace);
benchmark_stamp('Parse: Place of Birth');

# Shoots / Catches
my $type = $$player{'info'}{'playerType'};
my $field = ($type eq 'skater' ? 'shoots' : 'catches');
my $shoots_catches = $$player{'info'}{$field};
if (defined($shoots_catches)) {
  $shoots_catches = '\'' . ($shoots_catches eq 'L' ? 'Left' : 'Right') . '\'';
} else {
  $shoots_catches = 'NULL';
}
benchmark_stamp('Parse: Shoots/Catches');

print "INSERT INTO SPORTS_AHL_PLAYERS (player_id, first_name, surname, dob, birthplace, birthplace_country, height, weight, shoots_catches) VALUES ('$player_id', '$fname', '$sname', $dob, $birthplace, $birthplace_country, $ht, $wt, $shoots_catches) ON DUPLICATE KEY UPDATE height = VALUES(height), weight = VALUES(weight), shoots_catches = VALUES(shoots_catches);\n\n";

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_AHL_PLAYERS ORDER BY player_id;\n";
print "ALTER TABLE SPORTS_AHL_PLAYERS_IMPORT ORDER BY player_id;\n" if $new;
print "\n";

# Return true to pacify the compiler
1;
