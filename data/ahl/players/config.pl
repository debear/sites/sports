#!/usr/bin/perl -w
# Perl config file

our (%config, $remote_id);
$config{'data_dir'} = "$config{'base_dir'}/_data/players/$remote_id";

# Return true to pacify the compiler
1;
