#!/usr/bin/perl -w
# Load through the various games and generate the SQL to import them in to the database

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse roster script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
my $common = abs_path(__DIR__ . '/parse-v3/common.pl');
require $common;

# Load our component files
foreach my $f ( 'rosters', 'play_by_play', 'misc' ) {
  my $g = abs_path(__DIR__ . '/parse-v3/' . $f . '.pl');
  require $g;
}

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);
our $game_concat = "$season_dir//$game_type//$game_id";

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

##
## Process the game
##
# Load the data
print "\n## Loading data for $season // $game_type // $game_id // $$game_info{'remote_id'}\n";
print STDERR "# Loading $game_type game" . ($game_type eq 'playoff' ? ' (' . playoff_summary($season, $game_id) . ')' : '') . ": $season " . ucfirst($game_type) . ", Game $game_id\n";
our ($summary_file, $summary) = load_file($season_dir, $game_type, $$game_info{'remote_id'}, 'summary');
our ($pbp_file, $pbp) = load_file($season_dir, $game_type, $$game_info{'remote_id'}, 'play_by_play');

# Ensure we got a file....
if (!defined($summary)) {
  print STDERR "Missing game data ($season :: $game_type :: $game_id)\n";
  exit 10;
}
print "# Summary File: $summary_file\n";
print "# Play-by-Play File: $pbp_file\n\n";

# Any data fixes to load?
my $fix_file = "$config{'base_dir'}/_data/fixes/games/$season_dir.pl";
if (-e $fix_file) {
  print "#\n# Loading potential data fixes from $fix_file\n#\n";
  require $fix_file;
  print "\n";
}

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

##
## Reset a previous run
##
print "# Clear a previous run\n";
clear_rosters()      if check_disp(1);
clear_play_by_play() if check_disp(2);
clear_misc()         if check_disp(3);
print "\n";

# Run the parts
parse_misc_process(); # Always needed...
parse_rosters()      if check_disp(1);
parse_play_by_play() if check_disp(2);
parse_misc_display() if check_disp(3);

# Return true to pacify the compiler
1;
