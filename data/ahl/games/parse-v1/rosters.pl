#!/usr/bin/perl -w
use POSIX;

#
# Rosters / Player Summary
#
sub parse_rosters {
  our (%config, $game_info, $summary);
  print "##\n## Roster / Player Summary\n##\n\n";
  # Info for sharing...
  our %rosters = ( 'by_team' => { $$game_info{'visitor_id'} => { },
                                  $$game_info{'home_id'} => { } },
                   'by_remote' => { } );

  my @loop = ( { 'code' => 'visitors', 'team_id' => $$game_info{'visitor_id'} },
               { 'code' => 'home', 'team_id' => $$game_info{'home_id'} } );
  foreach my $loop (@loop) {
    print "## $$loop{'team_id'}\n";
    my ($list) = ($summary =~ m/(<table[^>]*>\s*<tr[^>]*><td[^>]*><b[^>]*>$$loop{'code'}: .*?<\/b><\/td><\/tr>.*?<\/table>\s*(?:<br>)?\s*<table[^>]*>.*?<\/table>)/gsi);
    ($list) = ($summary =~ m/(<table[^>]*>\s*<tr[^>]*><td[^>]*><b[^>]*>$$loop{'code'}: .*?<\/b><\/td><\/tr>.*?<\/table>)/gsi) if !defined($list); # Fallback...

    # Skaters
    print "# - Skaters\n\n";
    my ($list_skaters) = ($list =~ m/<table[^>]*>(.*?)<\/table>\s*<table[^>]*>.*?<\/table>/gsi);
    ($list_skaters) = ($list =~ m/<table[^>]*>(.*?)<tr[^>]*>\s*<td[^>]*>&nbsp;<\/td>\s*<td[^>]*>&nbsp;<\/td>\s*<td[^>]*>&nbsp;Goalies.*?<\/table>/gsi) if !defined($list_skaters); # Fallback...

    my @skaters = ($list_skaters =~ m/<tr class="light">\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>\D*?(\d*?)\D*?<\/td>\s*<td>&nbsp;(<i>)?<a href='[^']*?player\.php\?id=(\d+)'>(.*?)<\/a>(?:<\/i>)?<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(\d*?)<\/td>\s*<td[^>]*>(\d*?)<\/td>\s*(<td[^>]*>[-\+]?\d*?<\/td>)?\s*(<td[^>]*>\d*?<\/td>)?\s*<td[^>]*>(\d*?)<\/td>\s*<\/tr>/gsi);
    while (my ($capt, $jersey, $starter, $player_id, $name, $pos, $goals, $assists, $plus_minus, $shots, $pims) = splice(@skaters, 0, 11)) {
      print "# $name #$jersey\n";

      # Exclusion: duplicate ID(s) in the remote database we need to manage
      if (defined($config{'map'}{'duplicate_players'}{$player_id})) {
        $player_id = $config{'map'}{'duplicate_players'}{$player_id};
        # Has this excluded player already been processed / will be processed?
        $skip = defined($rosters{'by_remote'}{$player_id});
        for (my $i = 0; !$skip && $i < (@skaters / 11); $i++) {
          $skip = ($skaters[($i * 11) + 3] == $player_id);
        }
        if ($skip) {
          print "# - Skipping, an excluded player who appears in this game file multiple times\n";
          next;
        }
      }

      # Convert undef stats to 0, rather than NULL
      $goals = 0 if !defined($goals);
      $assists = 0 if !defined($assists);
      $pims = 0 if !defined($pims);

      # Process
      $rosters{'by_team'}{$$loop{'team_id'}}{$jersey} = $player_id;
      $rosters{'by_remote'}{$player_id} = $jersey;
      $rosters{'by_name_sf'}{$$loop{'team_id'}}{$name} = $jersey;
      my ($s,$f) = split(', ', $name); $rosters{'by_name_fs'}{$$loop{'team_id'}}{"$f $s"} = $jersey;
      $var = get_player_id($$loop{'team_id'}, $jersey, $player_id);

      $capt =~ s/&nbsp;//; check_for_null(\$capt);
      $starter = (defined($starter) ? 1 : 0);
      ($plus_minus) = ($plus_minus =~ m/>(.*?)</) if defined($plus_minus);
      $plus_minus = 0 if !defined($plus_minus) || $plus_minus eq '';
      ($shots) = ($shots =~ m/>(.*?)</) if defined($shots);
      $shots = 0 if !defined($shots) || $shots eq '';

      print "INSERT INTO SPORTS_AHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started) VALUES ('$season', '$game_type', '$game_id', '$$loop{'team_id'}', '$jersey', $var, '$pos', $capt, '$starter');\n";
      print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, gp, start, goals, assists, plus_minus, pims, shots) VALUES ('$season', $var, '$game_type', '$game_id', '$$loop{'team_id'}', '1', '$starter', '$goals', '$assists', '$plus_minus', '$pims', '$shots');\n\n";
    }

    # Goalies
    print "# - Goalies\n";
    my ($list_goalies) = ($list =~ m/<table[^>]*>.*?<\/table>\s*<table[^>]*>(.*?)<\/table>/gsi);
    ($list_goalies) = ($list =~ m/<table[^>]*>.*?<tr[^>]*>.*?(Goalies.*?)<\/table>/gsi) if !defined($list_goalies); # Fallback...
    my %team = ( 'all' => [ ], 'zero_ga' => [ ] );
    my @goalies = ($list_goalies =~ m/<tr class="light">\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>\D*?(\d*?)\D*?<\/td>\s*<td>&nbsp;(<i>)?<a href='[^']*?player\.php\?id=(\d+)'>(.*?)<\/a>[^<]*?(?:<\/i>)?\s*<\/td>(?:\s*<td><\/td>\s*)?\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<\/tr>/gsi);
    while (my ($capt, $jersey, $starter, $player_id, $name, $goals_against, $mins_played, $shots_against, $saves, $pims) = splice(@goalies, 0, 10)) {
      print "\n# $name #$jersey\n";

      # Exclusion: duplicate ID(s) in the remote database we need to manage
      if (defined($config{'map'}{'duplicate_players'}{$player_id})) {
        $player_id = $config{'map'}{'duplicate_players'}{$player_id};
        # Has this excluded player already been processed / will be processed?
        $skip = defined($rosters{'by_remote'}{$player_id});
        for (my $i = 0; !$skip && $i < (@goalies / 10); $i++) {
          $skip = ($goalies[($i * 10) + 3] == $player_id);
        }
        if ($skip) {
          print "# - Skipping, an excluded player who appears in this game file multiple times\n";
          next;
        }
      }

      # Process
      $rosters{'by_team'}{$$loop{'team_id'}}{$jersey} = $player_id;
      $rosters{'by_remote'}{$player_id} = $jersey;
      $rosters{'by_name_sf'}{$$loop{'team_id'}}{$name} = $jersey;
      my ($s,$f) = split(', ', $name); $rosters{'by_name_fs'}{$$loop{'team_id'}}{"$f $s"} = $jersey;
      my $var = get_player_id($$loop{'team_id'}, $jersey, $player_id);

      $capt =~ s/&nbsp;//; check_for_null(\$capt);
      # Fallback: Get the starter from boxscore?
      my $team_misc = ($$loop{'code'} eq 'visitors' ? 'visitor' : 'home');
      $starter = (defined($starter) || (defined($misc{'goalies'}{'first'}{'player'}{$team_misc}) && $s eq $misc{'goalies'}{'first'}{'player'}{$team_misc}) ? 1 : 0);

      print "INSERT INTO SPORTS_AHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started) VALUES ('$season', '$game_type', '$game_id', '$$loop{'team_id'}', '$jersey', $var, 'G', $capt, '$starter');\n";

      # Skip game goalie row if player didn't play
      next if $mins_played eq '-';

      # Was this the goalie of record for a tied game?
      if (defined($misc{'goalies'}{'last'})) {
        $misc{'status'}{'goalie_' . $team_misc} = $jersey if $s eq $misc{'goalies'}{'last'}{'player'}{$team_misc} && $misc{'scoring'}{'winner'} eq '';
      }

      # Record
      my $w   = (defined($misc{'status'}{'goalie_w'}) && $misc{'scoring'}{'winner'} eq $$loop{'team_id'} && $misc{'status'}{'goalie_w'} eq $jersey) ? 1 : 0;
      my $l   = (defined($misc{'status'}{'goalie_l'}) && $misc{'scoring'}{'loser'}  eq $$loop{'team_id'} && $misc{'status'}{'goalie_l'} eq $jersey && ($misc{'status'}{'status'} eq 'F'  || $game_type eq 'playoff')) ? 1 : 0;
      my $t   = (!defined($misc{'status'}{'goalie_w'}) && !defined($misc{'status'}{'goalie_l'}) && defined($misc{'status'}{'goalie_' . $team_misc}) && $jersey eq $misc{'status'}{'goalie_' . $team_misc}) ? 1 : 0;
      my $otl = (defined($misc{'status'}{'goalie_l'}) && $misc{'scoring'}{'loser'}  eq $$loop{'team_id'} && $misc{'status'}{'goalie_l'} eq $jersey &&  $misc{'status'}{'status'} eq 'OT' && $game_type eq 'regular')  ? 1 : 0;
      my $sol = (defined($misc{'status'}{'goalie_l'}) && $misc{'scoring'}{'loser'}  eq $$loop{'team_id'} && $misc{'status'}{'goalie_l'} eq $jersey &&  $misc{'status'}{'status'} eq 'SO' && $game_type eq 'regular')  ? 1 : 0;
      # MM:SS Time to SQL time
      my ($mins, $sec) = ($mins_played =~ m/^(\d{3}|[6-9]\d):(\d{2})/);
      if (defined($mins)) {
        my $hrs = floor($mins / 60);
        $mins = $mins % 60;
        $mins_played = sprintf('%02d:%02d:%02d', $hrs, $mins, $sec);
      } else {
        $mins_played = '00:' . $mins_played;
      }
      # Other calcs...
      my $goals_against = $shots_against - $saves;

      # SQL
      print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, gp, start, win, loss, ot_loss, so_loss, goals_against, shots_against, minutes_played, pims) VALUES ('$season', $var, '$game_type', '$game_id', '$$loop{'team_id'}', '1', '$starter', '$w', '$l', '$otl', '$sol', '$goals_against', '$shots_against', '$mins_played', '$pims');\n";

      # Add to lists for determining shutouts
      push @{$team{'all'}},     $var;
      push @{$team{'zero_ga'}}, $var if !$goals_against;
    }
    print "\n";

    # Shutout?
    print "# Record shutout for '$team{'zero_ga'}[0]'\nUPDATE SPORTS_AHL_PLAYERS_GAME_GOALIES SET shutout = 1 WHERE season = '$season' AND game_type = '$game_type' AND game_id = '$game_id' AND team_id = '$$loop{'team_id'}' AND player_id = $team{'zero_ga'}[0];\n\n"
      if (scalar(@{$team{'all'}}) == 1 && scalar(@{$team{'zero_ga'}}) == 1);
  }
}

# SQL to get the player_id
sub get_player_id {
  our (%config);
  my ($team_id, $jersey, $player_id) = @_;
  my $var = "\@player_${team_id}_${jersey}";

  print "SET $var := (SELECT player_id FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = '$player_id');\n";
  print "INSERT IGNORE INTO SPORTS_AHL_PLAYERS_IMPORT (player_id, remote_id, profile_imported) VALUES (\@player_$jersey, '$player_id', NOW()) ON DUPLICATE KEY UPDATE profile_imported = VALUES(profile_imported);\n";
  print "SET $var := (SELECT IFNULL($var, LAST_INSERT_ID()));\n\n";

  return $var;
}

# Return true to pacify the compiler
1;
