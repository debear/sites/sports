#!/usr/bin/perl -w
#
# Scoring
#
sub parse_scoring {
  our (%config, %misc);
  print "##\n## Scoring\n##\n\n";

  parse_scoring_combined();

  # Tidy ups: Special Team, Goalie and Period scoring
  my $gw_num = ($misc{'scoring'}{'loser'} ne '' ? $misc{'scoring'}{$misc{'scoring'}{'loser'}} + 1 : -1); # Tied game has no GWG...
  print "# Tidy special teams\n";
  print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, pp_goals, pp_assists, sh_goals, sh_assists, gw_goals)
  SELECT LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id,
         SUM(GOAL.goal_type = 'PP' AND GOAL.scorer = LINEUP.jersey) AS pp_goals,
         SUM(GOAL.goal_type = 'PP' AND (GOAL.assist_1 = LINEUP.jersey OR GOAL.assist_2 = LINEUP.jersey)) AS pp_assists,
         SUM(GOAL.goal_type = 'SH' AND GOAL.scorer = LINEUP.jersey) AS sh_goals,
         SUM(GOAL.goal_type = 'SH' AND (GOAL.assist_1 = LINEUP.jersey OR GOAL.assist_2 = LINEUP.jersey)) AS sh_assists,
         SUM(GOAL.goal_num = '$gw_num' AND GOAL.scorer = LINEUP.jersey) AS gw_goals
  FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
  LEFT JOIN SPORTS_AHL_GAME_EVENT_GOAL AS GOAL
    ON (GOAL.season = LINEUP.season
    AND GOAL.game_type = LINEUP.game_type
    AND GOAL.game_id = LINEUP.game_id
    AND GOAL.by_team_id = LINEUP.team_id)
  WHERE LINEUP.season = '$season'
  AND   LINEUP.game_type = '$game_type'
  AND   LINEUP.game_id = '$game_id'
  AND   LINEUP.pos <> 'G'
  GROUP BY LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id
ON DUPLICATE KEY UPDATE pp_goals = VALUES (pp_goals),
                        pp_assists = VALUES (pp_assists),
                        sh_goals = VALUES (sh_goals),
                        sh_assists = VALUES (sh_assists),
                        gw_goals = VALUES (gw_goals);\n\n";

  print "# Tidy goalie scoring\n";
  print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, goals, assists)
  SELECT LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id,
         IFNULL(SUM(GOAL.scorer = LINEUP.jersey), 0) AS goals,
         IFNULL(SUM(GOAL.assist_1 = LINEUP.jersey OR GOAL.assist_2 = LINEUP.jersey), 0) AS assists
  FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
  JOIN SPORTS_AHL_PLAYERS_GAME_GOALIES AS GOALIE
    ON (GOALIE.season = LINEUP.season
    AND GOALIE.game_type = LINEUP.game_type
    AND GOALIE.game_id = LINEUP.game_id
    AND GOALIE.team_id = LINEUP.team_id
    AND GOALIE.player_id = LINEUP.player_id)
  LEFT JOIN SPORTS_AHL_GAME_EVENT_GOAL AS GOAL
    ON (GOAL.season = LINEUP.season
    AND GOAL.game_type = LINEUP.game_type
    AND GOAL.game_id = LINEUP.game_id
    AND GOAL.by_team_id = LINEUP.team_id)
  WHERE LINEUP.season = '$season'
  AND   LINEUP.game_type = '$game_type'
  AND   LINEUP.game_id = '$game_id'
  AND   LINEUP.pos = 'G'
  GROUP BY LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id
ON DUPLICATE KEY UPDATE goals = VALUES(goals),
                        assists = VALUES(assists);\n\n";

  print "# Tidy period scoring\n";
  print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, goals)
  SELECT season, game_type, game_id, team_id, period, COUNT(event_id) AS goals
  FROM SPORTS_AHL_GAME_EVENT
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   game_id = '$game_id'
  AND   event_type = 'GOAL'
  GROUP BY team_id, period
ON DUPLICATE KEY UPDATE goals = VALUES(goals);\n\n";
}

#
# Combined score list
#
sub parse_scoring_combined {
  our (%config, $game_info, $summary);
  my ($scoring) = ($summary =~ m/<table width="100%" border="0"  cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="2"><b class="content-w">Scoring<\/b><\/td><\/tr>(.*?)<\/table><\/td><td valign="top"/gsi);
  my @goals = ($scoring =~ m/<tr class="light"><td><i>(.*?)<\/i>\. (.{1,3}) <a href='player\.php\?id=(\d+)'>.*?<\/a>, \(\d+\)\s*\(?(?:<a href='player\.php\?id=(\d+)'>.*?<\/a>)?,?\s*(?:<a href='player\.php\?id=(\d+)'>.*?<\/a>)?\)?, (\d{1,2}:\d{2})\s*(\([^\)]+\))?\s*<\/td><td align='right'>\s*<a onMouseOver="overlib\('.*?(<table.*?<\/table>)'.*?<\/td><\/tr>/gsi);

  my %counter = ( );
  while (my ($period, $team_id, $scorer, $assist_1, $assist_2, $time, $info, $plus_minus) = splice(@goals, 0, 8)) {
    # Player ID checks
    $scorer = $config{'map'}{'duplicate_players'}{$scorer} if defined($config{'map'}{'duplicate_players'}{$scorer});
    $assist_1 = $config{'map'}{'duplicate_players'}{$assist_1} if defined($assist_1) && defined($config{'map'}{'duplicate_players'}{$assist_1});
    $assist_2 = $config{'map'}{'duplicate_players'}{$assist_2} if defined($assist_2) && defined($config{'map'}{'duplicate_players'}{$assist_2});

    # Determine player and team
    my $scorer_jersey = $rosters{'by_remote'}{$scorer};
    $team_id = (defined($rosters{'by_team'}{$$game_info{'visitor_id'}}{$scorer_jersey}) && ($rosters{'by_team'}{$$game_info{'visitor_id'}}{$scorer_jersey} == $scorer) ? $$game_info{'visitor_id'} : $$game_info{'home_id'});

    # Pre-process the plus/minus
    my ($plus, $minus) = ($plus_minus =~ m/<td class=light valign=top><table[^>]*>(.*?)<\/table><\/td><td class=light valign=top><table[^>]*>(.*?)<\/table>/gsi);
    my @plus = ($plus =~ m/<td nowrap>(\d+)\s.*?<\/td>/gsi);
    my @minus = ($minus =~ m/<td nowrap>(\d+)\s.*?<\/td>/gsi);

    # Event processing...
    convert_period(\$period);
    $misc{'event_id'}++;
    $counter{$team_id}++;

    # Info Conversion
    ($scorer, $assist_1, $assist_2) = parse_scoring__players($scorer, $assist_1, $assist_2);
    my ($goal_type, $goalie_status, $penalty_shot) = parse_scoring__info($info);

    # Play
    my $play = '';
    $play .= 'Power Play ' if $goal_type eq 'PP';
    $play .= 'Shorthanded ' if $goal_type eq 'SH';
    $play =~ s/ $/, / if $penalty_shot && $play ne ''; # Formatting...
    $play .= 'Penalty Shot ' if $penalty_shot;
    $play .= 'Empty Net ' if $goalie_status eq 'EN';
    $play .= 'Extra Skater ' if $goalie_status eq 'WG';
    $play .= 'Goal, scored by {{' . $team_id . '_' . $scorer . '}}';
    $play .= ' assisted by {{' . $team_id . '_' . $assist_1 . '}}' if $assist_1 ne 'NULL';
    $play .= ' and {{' . $team_id . '_' . $assist_2 . '}}' if $assist_2 ne 'NULL';
    $play =~ s/'(\d+)'/$1/g; # Fix column quoting

    # Output as SQL
    print "# $time, P:$period ($team_id)\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$period', '00:$time', 'GOAL', '$team_id', '$play');\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT_GOAL (season, game_type, game_id, event_id, by_team_id, goal_num, scorer, assist_1, assist_2, goal_type, goalie_status, penalty_shot) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$team_id', '$counter{$team_id}', '$scorer', $assist_1, $assist_2, '$goal_type', $goalie_status, '$penalty_shot');\n";

    # Plus/Minus SQL
    my $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
    my %loop = ( $team_id => \@plus, $opp_team_id => \@minus ); my $i = 0;
    while (my ($pm_team_id, $players) = each %loop) {
      print "# " . ($i++ == 0 ? 'Plus' : 'Minus') . "\n";

      # Skip if no matching players found
      if (!defined($$players[0])) {
        print "#  - Skipping...\n";
        next;
      }

      # Process
      for (my $j = 0; $j < 6; $j++) {
        check_for_null(\$$players[$j]);
      }
      print "INSERT INTO SPORTS_AHL_GAME_EVENT_ONICE (season, game_type, game_id, event_id, team_id, player_1, player_2, player_3, player_4, player_5, player_6) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$pm_team_id', $$players[0], $$players[1], $$players[2], $$players[3], $$players[4], $$players[5]);\n";
    }
    print "\n";
  }
}

#
# Split score list
#
sub parse_scoring_split {
  our (%config, $game_info, $summary);
  my ($scoring_visitor, $scoring_home) = ($summary =~ m/<tr>\s*<td width="50%" valign="top">\s*<table width="100%" border="0"  cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="2"><b class="content-w">.*? Scoring<\/b><\/td><\/tr>\s*(.*?)\s*<\/table><\/td>\s*<td width="50%" valign="top">\s*<table width="100%" border="0"  cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="2"><b class="content-w">.*? Scoring<\/b><\/td><\/tr>\s*(.*?)\s*<\/table><\/td>\s*<\/tr>/gsi);

  my @loop = ( { 'team_id' => $$game_info{'visitor_id'}, 'list' => $scoring_visitor },
               { 'team_id' => $$game_info{'home_id'}, 'list' => $scoring_home } );
  foreach my $l (@loop) {
    my @goals = ($$l{'list'} =~ m/<tr class="light"><td><i>(.*?)<\/i>\.\s*<a href='player\.php\?id=(\d+)'>.*?<\/a>, \(\d+\)\s*\(?(?:<a href='player\.php\?id=(\d+)'>.*?<\/a>)?,?\s*(?:<a href='player\.php\?id=(\d+)'>.*?<\/a>)?\)?, (\d{1,2}:\d{2})\s*(\([^\)]+\))?\s*<\/td><td align='right'>/gsi);
    my $goal_num = 0;
    while (my ($period, $scorer, $assist_1, $assist_2, $time, $info) = splice(@goals, 0, 6)) {
      # Player ID checks
      $scorer = $config{'map'}{'duplicate_players'}{$scorer} if defined($config{'map'}{'duplicate_players'}{$scorer});
      $assist_1 = $config{'map'}{'duplicate_players'}{$assist_1} if defined($assist_1) && defined($config{'map'}{'duplicate_players'}{$assist_1});
      $assist_2 = $config{'map'}{'duplicate_players'}{$assist_2} if defined($assist_2) && defined($config{'map'}{'duplicate_players'}{$assist_2});

      # Event processing...
      convert_period(\$period);
      $misc{'event_id'}++;
      $goal_num++;

      # Info Conversion
      ($scorer, $assist_1, $assist_2) = parse_scoring__players($scorer, $assist_1, $assist_2);
      my ($goal_type, $goalie_status, $penalty_shot) = parse_scoring__info($info);

      # Play
      my $play = '';
      $play .= 'Power Play ' if $goal_type eq 'PP';
      $play .= 'Shorthanded ' if $goal_type eq 'SH';
      $play =~ s/ $/, / if $penalty_shot && $play ne ''; # Formatting...
      $play .= 'Penalty Shot ' if $penalty_shot;
      $play .= 'Empty Net ' if $goalie_status eq 'EN';
      $play .= 'Extra Skater ' if $goalie_status eq 'WG';
      $play .= 'Goal, scored by {{' . $$l{'team_id'} . '_' . $scorer . '}}';
      $play .= ' assisted by {{' . $$l{'team_id'} . '_' . $assist_1 . '}}' if $assist_1 ne 'NULL';
      $play .= ' and {{' . $$l{'team_id'} . '_' . $assist_2 . '}}' if $assist_2 ne 'NULL';
      $play =~ s/'(\d+)'/$1/g; # Fix column quoting

      # Output as SQL
      print "# $time, P:$period ($$l{'team_id'})\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$period', '00:$time', 'GOAL', '$$l{'team_id'}', '$play');\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT_GOAL (season, game_type, game_id, event_id, by_team_id, goal_num, scorer, assist_1, assist_2, goal_type, goalie_status, penalty_shot) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$$l{'team_id'}', '$goal_num', '$scorer', $assist_1, $assist_2, '$goal_type', $goalie_status, '$penalty_shot');\n\n";
    }
  }
}

#
# Helpers
#
# remote_id => jersey
sub parse_scoring__players {
  our (%rosters);
  my ($scorer, $assist_1, $assist_2) = @_;

  $scorer = $rosters{'by_remote'}{$scorer};
  # Assists are optional...
  $assist_1 = $rosters{'by_remote'}{$assist_1} if defined($assist_1);
  check_for_null(\$assist_1);
  $assist_2 = $rosters{'by_remote'}{$assist_2} if defined($assist_2);
  check_for_null(\$assist_2);

  # Return
  return ($scorer, $assist_1, $assist_2);
}

# Goal info => details
sub parse_scoring__info {
  my ($info) = @_;
  $info = '' if !defined($info);

  # Goal type
  my $goal_type = 'EV';
  $goal_type = 'PP' if $info =~ m/PP/;
  $goal_type = 'SH' if $info =~ m/SH/;

  # Goalie status
  my $goalie_status;
  $goalie_status = 'EN' if $info =~ m/EN/;
  #$goalie_status = 'WG'; # Can we determine this?
  check_for_null(\$goalie_status);

  # Penalty Shot?
  my $penalty_shot = ($info =~ m/PS/) ? 1 : 0;

  # Return...
  return ($goal_type, $goalie_status, $penalty_shot);
}


# Return true to pacify the compiler
1;
