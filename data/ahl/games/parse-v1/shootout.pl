#!/usr/bin/perl -w
#
# Shootout
#
sub parse_shootout {
  our (%config, $game_info, %misc, $summary);
  print "##\n## Shootout\n##\n";

  # Only continue if the game went to a shootout...
  if ($misc{'status'}{'status'} ne 'SO') {
    print "# Skipping: Game did not go to a shootout...\n\n";
    return;
  }
  print "\n";

  # Determine who shot first, as well as the shooter lists
  my ($so_first, $visitor, $so_visitor, $home, $so_home) = ($summary =~ m/<tr><td colspan="2">(.*?) shoots first for each round\.<\/td><\/tr>\s*<tr>\s*<td width="50%" valign="top">\s*<table width="100%" border="0"  cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="3"><b class="content-w">(.*?) Shootout<\/b><\/td><\/tr>(.*?)<tr><td><\/td><td><b>Total:<\/b><\/td><td><b>\d+<\/b><\/td><\/tr>\s*<\/table>\s*<\/td><td width="50%" valign="top">\s*<table width="100%" border="0"  cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="3"><b class="content-w">(.*?) Shootout<\/b><\/td><\/tr>(.*?)<tr><td><\/td><td><b>Total:<\/b><\/td><td><b>\d+<\/b><\/td><\/tr>\s*<\/table>/gsi);

  # Parse shooters
  my %shooters = ( $$game_info{'visitor_id'} => [ ], $$game_info{'home_id'} => [ ] );
  my @loop = ( { 'team_id' => $$game_info{'visitor_id'}, 'shooters' => $so_visitor },
               { 'team_id' => $$game_info{'home_id'}, 'shooters' => $so_home } );
  foreach my $s ( @loop ) {
    my @p = $$s{'shooters'} =~ m/<td align="right" size="5">(\d+)<\/td><td>.*?<\/td><td>(.*?)<\/td>/gsi;
    while (my ($jersey, $result) = splice(@p, 0, 2)) {
      push @{$shooters{$$s{'team_id'}}}, { 'player' => $jersey, 'result' => $result };
    }
  }

  # Goalies (of record as other info missing...?)
  my %so_goalies = ( $$game_info{'visitor_id'} => ($misc{'scoring'}{'winner'} eq $$game_info{'visitor_id'} ? $misc{'status'}{'goalie_w'} : $misc{'status'}{'goalie_l'}),
                     $$game_info{'home_id'}    => ($misc{'scoring'}{'winner'} eq $$game_info{'home_id'}    ? $misc{'status'}{'goalie_w'} : $misc{'status'}{'goalie_l'}) );

  # Determine who started first
  my $team_id = ($so_first eq $visitor ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
  # Though the official record could be wrong.... if the other team has more rows then surely they started first??
  my $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
  if ( @{$shooters{$team_id}} < @{$shooters{$opp_team_id}} ) {
    $team_id = $opp_team_id;
  }
  # Now display...
  while (@{$shooters{$$game_info{'visitor_id'}}} || @{$shooters{$$game_info{'home_id'}}}) {
    # Skip if no rows (avoiding errors...)
    next if !@{$shooters{$team_id}};

    # Get and process convert
    my ($p) = splice(@{$shooters{$team_id}}, 0, 1);
    $misc{'event_id'}++;
    my $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
    my $opp_goalie = $so_goalies{$opp_team_id};
    check_for_null(\$opp_goalie);
    my $status = ($$p{'result'} eq 'Goal' ? 'goal' : 'no_goal');

    # Play
    my $play = "{{${team_id}_$$p{'player'}}} ";
    $play .= 'scored' if $status eq 'goal';
    $play .= 'missed' if $status eq 'no_goal';

    # Display
    print "# $team_id #$$p{'player'}: $$p{'result'}\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', 5, '00:00:00', 'SHOOTOUT', '$team_id', '$play');\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT_SHOOTOUT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, status) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$team_id', '$$p{'player'}', '$opp_team_id', $opp_goalie, '$status');\n\n";

    # Switch team for next loop...
    $team_id = $opp_team_id;
  }


  # Tidy ups: Player/Goalie summary
  @loop = ( { 'type' => 'shooters', 'tbl' => 'SKATERS', 'agst' => '', 'col' => 'by', 'pos' => '<>' },
            { 'type' => 'goalies', 'tbl' => 'GOALIES', 'agst' => '_against', 'col' => 'on', 'pos' => '=' } );
  foreach my $l (@loop) {
    print "# Tidy $$l{'type'}\n";
    print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_$$l{'tbl'} (season, player_id, game_type, game_id, so_goals$$l{'agst'}, so_shots$$l{'agst'})
  SELECT LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id,
         SUM(SHOOTOUT.$$l{'col'}_jersey = LINEUP.jersey AND SHOOTOUT.status = 'goal') AS so_goals$$l{'agst'},
         SUM(SHOOTOUT.$$l{'col'}_jersey = LINEUP.jersey) AS so_shots$$l{'agst'}
  FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
  JOIN SPORTS_AHL_PLAYERS_GAME_$$l{'tbl'} AS PLAYER
    ON (PLAYER.season = LINEUP.season
    AND PLAYER.game_type = LINEUP.game_type
    AND PLAYER.game_id = LINEUP.game_id
    AND PLAYER.team_id = LINEUP.team_id
    AND PLAYER.player_id = LINEUP.player_id)
  LEFT JOIN SPORTS_AHL_GAME_EVENT_SHOOTOUT AS SHOOTOUT
    ON (SHOOTOUT.season = LINEUP.season
    AND SHOOTOUT.game_type = LINEUP.game_type
    AND SHOOTOUT.game_id = LINEUP.game_id
    AND SHOOTOUT.$$l{'col'}_team_id = LINEUP.team_id)
  WHERE LINEUP.season = '$season'
  AND   LINEUP.game_type = '$game_type'
  AND   LINEUP.game_id = '$game_id'
  AND   LINEUP.pos $$l{'pos'} 'G'
  GROUP BY LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id
ON DUPLICATE KEY UPDATE so_goals$$l{'agst'} = VALUES(so_goals$$l{'agst'}),
                        so_shots$$l{'agst'} = VALUES(so_shots$$l{'agst'});\n\n";
  }
}

# Return true to pacify the compiler
1;
