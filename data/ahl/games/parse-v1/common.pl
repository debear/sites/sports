#!/usr/bin/perl -w
# Common routines for game processing

use List::Util qw( max );

# Validate and load a file
sub load_file {
  our (%config);
  my ($season_dir, $game_type, $game_id, $filename, $args) = @_;

  # Validate
  my $file = sprintf('%s/_data/%s/games/%s/%d/%s.htm',
                     $config{'base_dir'},
                     $season_dir,
                     $game_type,
                     $game_id,
                     $filename);
  if (! -e $file) {
    print STDERR "File '$file' does not exist, unable to continue.\n";
    if (!$$args{'no_exit_on_error'}) {
      exit;
    } else {
      return ($file, undef);
    }
  }

  # Load the file
  my $contents;
  open FILE, "<$file";
  my @contents = <FILE>;
  close FILE;
  $contents = join('', @contents);
  @contents = (); # Garbage collect...

  return ($file, $contents);
}

# Clear a previous run
sub clear_previous {
  my ($season, $game_type, $game_id, @tables) = @_;

  print "\n## Resetting a previous data load...\n";
  foreach my $table (@tables) {
    print "DELETE FROM `$table` WHERE `season` = '$season' AND `game_type` = '$game_type' AND `game_id` = '$game_id';\n";
  }
}

# Order by game
sub reorder_tables {
  foreach my $table (@_) {
    print "ALTER TABLE `$table` ORDER BY `season`, `game_type`, `game_id`;\n";
  }
}

# Convert undefined values to 0
sub convert_undef_zero {
  my ($txt) = @_;
  $$txt = 0 if !defined($$txt);
}

# Convert a period in to a period number (e.g., 1st = 1; OT = 4; SO = 5; 2OT = 5, etc...)
sub convert_period {
  my ($p) = @_;
  trim($p);

  # Remove any ordinal value
  $$p =~ s/^(\d+)\s*(?:st|nd|rd|th)$/$1/; # This could also catch 3OT, so need to be careful....

  # If numeric, no need to continue
  return if $$p =~ m/^\d+$/;

  # OT => 4
  if ($$p eq 'OT') {
    $$p = 4;
    return;
  }

  # SO => 5
  if ($$p eq 'SO') {
    $$p = 5;
    return;
  }

  # Ordinal OT (convert for below...)
  $$p =~ s/^(\d)(?:st|nd|rd|th)\s*OT$/OT$1/;

  # Convert xOT into 3+x...
  my $n;
  ($n) = ($$p =~ m/^(\d+)\s*OT/) if ($$p =~ m/^\d+\s*OT/);
  ($n) = ($$p =~ m/^OT\s*(\d+)$/) if ($$p =~ m/^OT\s*\d+$/);
  $$p = 3 + $n;
}

# Convert a penalty description from the boxscore's into ours
sub parse_penalty_type {
  my ($descrip) = @_;

  $$descrip =~ s/^\s+//;
  $$descrip =~ s/\s+$//;

  # Strip trailing postfixes
  $$descrip =~ s/\s*-\s*Minor$//;
  $$descrip =~ s/\s*-\s*Major$//;
  $$descrip =~ s/\s*-\s*Match$//;
  $$descrip =~ s/\s*-\s*Misconduct$//;
  $$descrip =~ s/\s*-\s*Playing$//;

  # Strip some common prefixes that are accounted for in other areas of the penalty
  $$descrip =~ s/^Bench minor\s*-\s*//;
  $$descrip =~ s/^Double minor\s*-\s*//;
  $$descrip =~ s/^Game misconduct\s*-\s*//;
  $$descrip =~ s/^Major\s*-\s*//;
  $$descrip =~ s/^Match\s*-\s*//;
  $$descrip =~ s/^Misconduct\s*-\s*//;

  # Tweak some wording
  $$descrip = 'Delay of game' if $$descrip =~ /^Delay of game -/i;
  $$descrip = 'Unsportsmanlike conduct' if $$descrip =~ /^Unsportsmanlike conduct -/i;
  $$descrip = 'Spearing' if $$descrip eq 'Spearing (attempt)';
  $$descrip = 'Unnecessary stoppage' if $$descrip eq 'Goaltender - unnecessary stoppage';

  # One final re-trim
  $$descrip =~ s/^\s+//;
  $$descrip =~ s/\s+$//;

  return $$descrip;
}

# Return true to pacify the compiler
1;
