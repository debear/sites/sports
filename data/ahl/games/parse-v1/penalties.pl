#!/usr/bin/perl -w
#
# Penalties
#
sub parse_penalties {
  our (%config, $season, $game_type, $game_id);
  print "##\n## Penalties\n##\n\n";

  parse_penalties_combined();

  print "# Tidy period penalties\n";
  print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, pims)
  SELECT EVENT.season, EVENT.game_type, EVENT.game_id, EVENT.team_id, EVENT.period, IFNULL(SUM(PENALTY.pims), 0) AS pims
  FROM SPORTS_AHL_GAME_EVENT AS EVENT
  LEFT JOIN SPORTS_AHL_GAME_EVENT_PENALTY AS PENALTY
    ON (PENALTY.season = EVENT.season
    AND PENALTY.game_type = EVENT.game_type
    AND PENALTY.game_id = EVENT.game_id
    AND PENALTY.event_id = EVENT.event_id)
  WHERE EVENT.season = '$season'
  AND   EVENT.game_type = '$game_type'
  AND   EVENT.game_id = '$game_id'
  AND   EVENT.event_type = 'PENALTY'
  GROUP BY EVENT.team_id, EVENT.period
ON DUPLICATE KEY UPDATE pims = VALUES(pims);\n\n";
}

#
# Combined penalty list
#
sub parse_penalties_combined {
  our (%config, $game_info, $summary);
  my ($pens) = ($summary =~ m/<table width="100%" border="0"  cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="2"><b class="content-w">Penalties<\/b><\/td><\/tr>\s*(.*?)<\/table>/gsi);
  my @pens = ($pens =~ m/<tr class="light"><td><i>([^<]+)<\/i>\s*-\s*.{2,3}\s*(Served By)?\s*<a href='player.php\?id=(\d+)'>.*?<\/a>,\s*(\d{1,2}:\d{2})\s*-\s*(.*?),.*?\D(\d+)\s*min[^<]*?<\/td><\/tr>/gsi);
  while (my ($period, $served, $player_id, $time, $type, $pims) = splice(@pens, 0, 6)) {
    # Determine player and team
    $player_id = $config{'map'}{'duplicate_players'}{$player_id} if defined($config{'map'}{'duplicate_players'}{$player_id});
    my $jersey = $rosters{'by_remote'}{$player_id};
    my $team_id = (defined($rosters{'by_team'}{$$game_info{'visitor_id'}}{$jersey}) && ($rosters{'by_team'}{$$game_info{'visitor_id'}}{$jersey} == $player_id) ? $$game_info{'visitor_id'} : $$game_info{'home_id'});

    # Event processing...
    $misc{'event_id'}++;
    convert_period(\$period);

    # Who was it against and who served it?
    my $bench = defined($served);
    my $served = $jersey;
    check_for_null(\$served);
    my $player = ($bench ? 0 : $jersey);
    check_for_null(\$player);

    # Convert type
    parse_penalty_type(\$type);
    check_for_null(\$type);

    # Play
    my $play = '';
    $play = 'Bench ' if $player eq "'0'";
    if ($pims == 2) {
      $play .= 'Minor Penalty';
    } elsif ($pims == 4) {
      $play .= 'Double-Minor';
    } elsif ($pims == 5) {
      $play .= 'Major Penalty';
    } else {
      $play .= 'Penalty';
    }
    $play .= " on {{${team_id}_${player}}}" if $player ne "'0'";
    $play .= " for $type";
    $play .= " (Served by {{${team_id}_$served}})" if $served ne $player;
    $play =~ s/'//g; # Formatting

    print "# $time P:$period ($team_id" . ($bench ? ' (BN)' : '') . " #$jersey, $pims for $type)\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$period', '00:$time', 'PENALTY', '$team_id', '$play');\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT_PENALTY (season, game_type, game_id, event_id, team_id, jersey, served_by, type, pims) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$team_id', $player, $served, $type, '$pims');\n\n";
  }
}

#
# Split penalty list
#
sub parse_penalties_split {
  our (%config, $game_info, $summary);
  my ($pens_visitor, $pens_home) = ($summary =~ m/<tr>\s*<td width="50%" valign="top">\s*<table width="100%" border="0" cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="2"><b class="content-w">.*? Penalties<\/b><\/td><\/tr>\s*(.*?)\s*<\/table><\/td>\s*<td width="50%" valign="top">\s*<table width="100%" border="0" cellspacing="0" cellpadding="1">\s*<tr class="content-w"><td class="dark" align="center" colspan="2"><b class="content-w">.*? Penalties<\/b><\/td><\/tr>\s*(.*?)\s*<\/table><\/td><\/tr>/gsi);

  my @loop = ( { 'team_id' => $$game_info{'visitor_id'}, 'list' => $pens_visitor },
               { 'team_id' => $$game_info{'home_id'}, 'list' => $pens_home } );
  foreach my $l (@loop) {
    my @pens = ($$l{'list'} =~ m/<tr class="light"><td><i>(.*?)<\/i>\s*-\s*(Served By)?\s*<a href='player\.php\?id=(\d*)'>.*?<\/a>,\s*(\d{1,2}:\d{2})\s*-\s*(.*?),\s*(\d+) min[^<]*?<\/td><\/tr>/gsi);
    while (my ($period, $served, $player_id, $time, $type, $pims) = splice(@pens, 0, 6)) {
      # Determine player and team
      $player_id = $config{'map'}{'duplicate_players'}{$player_id} if defined($config{'map'}{'duplicate_players'}{$player_id});
      my $jersey = $rosters{'by_remote'}{$player_id};

      # Event processing...
      $misc{'event_id'}++;
      convert_period(\$period);

      # Who was it against and who served it?
      my $bench = defined($served);
      my $served = $jersey;
      check_for_null(\$served);
      my $player = ($bench ? 0 : $jersey);
      check_for_null(\$player);
      $jersey = '??' if !defined($jersey); # Fix for our comment

      # Convert type
      parse_penalty_type(\$type);
      check_for_null(\$type);

      # Play
      my $play = '';
      $play = 'Bench ' if !$player eq "'0'";
      if ($pims == 2) {
        $play .= 'Minor Penalty';
      } elsif ($pims == 4) {
        $play .= 'Double-Minor';
      } elsif ($pims == 5) {
        $play .= 'Major Penalty';
      } else {
        $play .= 'Penalty';
      }
      $play .= " on {{$$l{'team_id'}_${player}}}" if $player ne "'0'";
      $play .= " for $type";
      $play .= " (Served by {{$$l{'team_id'}_$served}})" if $served ne $player;
      $play =~ s/'//g; # Formatting

      print "# $time P:$period ($$l{'team_id'}" . ($bench ? ' (BN)' : '') . " #$jersey, $pims for $type)\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$period', '00:$time', 'PENALTY', '$$l{'team_id'}', '$play');\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT_PENALTY (season, game_type, game_id, event_id, team_id, jersey, served_by, type, pims) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$$l{'team_id'}', $player, $served, $type, '$pims');\n\n";
    }
  }
}

# Return true to pacify the compiler
1;
