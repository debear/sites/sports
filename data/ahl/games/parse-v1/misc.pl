#!/usr/bin/perl -w
#
# Misc Game Info (Pre-processing)
#
sub parse_misc_process {
  our (%config, $game_info, $summary);
  # Flag for potential error identification
  print "##\n## Misc Info (Processing)\n##\n##\n\n";
  our %misc; # Info we want to carry around the script

  # Shots by Period
  print "# Shots by Period\n";
  my ($visitor_shots, $home_shots) = ($summary =~ m/<table[^>]*>\s*<tr class="dark"><td class="content-w"><b>SHOTS<\/b><\/td>\s*.*?<\/tr>\s*<tr class="light"><td class="content" nowrap>.*?<\/td>(.*?)<td align="center" class="content"><b>\d+<\/b><\/td>\s*<\/tr>\s*<tr class="light"><td class="content" nowrap>.*?<\/td>(.*?)<td align="center" class="content"><b>\d+<\/b><\/td>\s*<\/tr>\s*<\/table>/gsi);
  $misc{'shots'} = { $$game_info{'visitor_id'} => [ ($visitor_shots =~ m/>(\d+)</gsi) ],
                     $$game_info{'home_id'}    => [ ($home_shots =~ m/>(\d+)</gsi) ]     };

  # Special Teams
  print "# Special Teams\n";
  my ($visitor_ppg, $visitor_ppopp, $home_ppg, $home_ppopp) = ($summary =~ m/<tr>\s*<td class="light" align="center">.*?<\/td>\s*<td class="light" align="center">(\d+) \/ (\d+)<\/td>/gsi);
  $misc{'special_teams'} = { $$game_info{'visitor_id'} => { 'pp' => { 'goals' => $visitor_ppg, 'opp' => $visitor_ppopp },
                                                            'pk' => { 'goals' => $home_ppg,    'opp' => $home_ppopp    } },
                             $$game_info{'home_id'}    => { 'pp' => { 'goals' => $home_ppg,    'opp' => $home_ppopp    },
                                                            'pk' => { 'goals' => $visitor_ppg, 'opp' => $visitor_ppopp } } };

  ## Next block getter...
  %{$misc{'status'}} = ();
  my ($detail_att, $detail_stars, $detail_offic) = ($summary =~ m/<table width="100%" border="0"  cellspacing="0" cellpadding="1">\s*<tr class="content-w">\s*<td class="dark" align="center" width="33%"><b class="content-w">Arena<\/b><\/td>.*?<\/tr>\s*<tr>\s*<td class="light" valign="top">(.*?)<\/td>\s*<td><img src="[^"]*" alt="" width="1" height="1" border="0"><\/td>\s*<td class="light" valign="top">(.*?)<\/td>\s*<td><img src="[^"]*" alt="" width="1" height="1" border="0"><\/td>\s*<td class="light" valign="top">(.*?)<\/td>.*?<\/table>/gsi);

  # Three Stars / Man of the Match
  print "# Three Stars / Man of the Match\n";
  @{$misc{'stars_raw'}} = ($detail_stars =~ m/(\d)\. <i>(.*?)<\/i> - (\d+)\s*(.*?)<br/gsi);

  # Officials
  print "# Officials\n";
  $misc{'officials'} = {};
  foreach my $offic (split(/<br>/i, $detail_offic)) {
    my ($offic, $offic_jersey, $offic_type) = ($offic =~ m/^(.+) \((\d+)\), (.+)$/gsi);
    next if !defined($offic); # Skip incomplete info

    $offic = convert_text($offic); trim(\$offic);
    $offic_type = convert_text($offic_type); trim(\$offic_type);
    my ($offic_fname, $offic_sname) = ($offic =~ m/^([^ ]+) (.+)$/gsi);
    $offic_type = lc $offic_type;

    $misc{'officials'}{$offic_type} = {} if !defined($misc{'officials'}{$offic_type});
    $misc{'officials'}{$offic_type}{$offic_jersey} = { 'fname' => $offic_fname, 'sname' => $offic_sname };
  }

  # Scores
  print "# Scores\n";
  my ($visitor_score, $home_score) = ($summary =~ m/<table[^>]*>\s*<tr class="dark"><td class="content-w"><b>SCORING<\/b><\/td>\s*.*?<\/tr>\s*<tr class="light"><td class="content" nowrap>.*?<\/td>.*?<td align="center" class="content"><b>(\d+)<\/b><\/td>\s*<\/tr>\s*<tr class="light"><td class="content" nowrap>.*?<\/td>.*?<td align="center" class="content"><b>(\d+)<\/b><\/td>\s*<\/tr>\s*<\/table>/gsi);
  $misc{'scoring'} = { $$game_info{'visitor_id'} => $visitor_score,
                       $$game_info{'home_id'}    => $home_score,
                       'winner'                  => ($visitor_score > $home_score ? $$game_info{'visitor_id'} : ($visitor_score < $home_score ? $$game_info{'home_id'} : '')),
                       'loser'                   => ($visitor_score < $home_score ? $$game_info{'visitor_id'} : ($visitor_score > $home_score ? $$game_info{'home_id'} : ''))  };

  # Determine starting/ending goalies?
  # Get the goalie list from the boxscore, as it includes time on/off ice
  my ($goalies) = ($boxscore =~ m/<!-- start goalies section -->(.*?)<!-- end goalie section -->/gsi);
  if (defined($goalies)) {
    my @goalies = ($goalies =~ m/<tr[^>]*><td[^>]*>(.{2,3})\s*-\s*([^\(]+) \(\d{1,2}:\d{1,2}\)[^<]*<\/td>.*?<td[^>]*>([^\/]+)\/(\d{1,2}:\d{2})<\/td><td[^>]*>([^\/]+)\/(\d{1,2}:\d{2})<\/td><\/tr>/gsi);
    $misc{'goalies'}{'first'} = { 'off_ice' => { 'visitor' => '9:59:59', 'home' => '9:59:59' },
                                  'player' => { 'visitor' => '0', 'home' => '0' },
                                  'visitor_team_id' => '' };
    $misc{'goalies'}{'last'} = { 'off_ice' => { 'visitor' => '0:00:00', 'home' => '0:00:00' },
                                 'player' => { 'visitor' => '0', 'home' => '0' },
                                 'visitor_team_id' => '' };
    while (my ($team_id, $sname, $start_period, $start_time, $end_period, $end_time) = splice(@goalies, 0, 6)) {
      # Home or Visitor?
      if ($misc{'goalies'}{'last'}{'visitor_team_id'} eq '') {
        $misc{'goalies'}{'last'}{'visitor_team_id'} = $team_id
      }
      my $team = ($misc{'goalies'}{'last'}{'visitor_team_id'} eq $team_id ? 'visitor' : 'home');

      # Convert the periods to a numeric value (for comparisons...)
      convert_period(\$start_period);
      convert_period(\$end_period);

      # Compare this team's goalies - first on ice
      $start_time = '0' . $start_time if length($start_time) == 4;
      my $cmp = "$start_period:$start_time";
      if ($cmp lt $misc{'goalies'}{'first'}{'off_ice'}{$team}) {
        $misc{'goalies'}{'first'}{'off_ice'}{$team} = $cmp;
        $misc{'goalies'}{'first'}{'player'}{$team} = $sname; # We'll need to convert this when the roster has been processed
      }

      # Compare this team's goalies - last off ice
      $cmp = "$end_period:$end_time";
      if ($cmp gt $misc{'goalies'}{'last'}{'off_ice'}{$team}) {
        $misc{'goalies'}{'last'}{'off_ice'}{$team} = $cmp;
        $misc{'goalies'}{'last'}{'player'}{$team} = $sname; # We'll need to convert this when the roster has been processed
      }
    }
  }

  # Goalies of Record
  print "# Goalies of Record\n";
  %{$misc{'status'}} = ( 'goalie_w' => ($boxscore =~ m/<tr><td class="r">(\d+)<\/td><td class="l">[^<]*? \(W\)<\/td><td class="r">\d+:\d{2}<\/td>/gsi),
                         'goalie_l' => ($boxscore =~ m/<tr><td class="r">(\d+)<\/td><td class="l">[^<]*? \((?:L|OT|OTL|SL|SOL)\)<\/td><td class="r">\d+:\d{2}<\/td>/gsi) );
  # If this failed, we need to try a different tact... (Elite League version)
  if (!defined($misc{'status'}{'goalie_l'})) {
    # Get the player names
    my ($alt_goalie_w) = ($boxscore =~ m/<tr><td align="left">.{2,3} - ([^\(]*?) \(\d{1,2}:\d{2}\) \(W\)<\/td>/gsi);
    my ($alt_goalie_l) = ($boxscore =~ m/<tr><td align="left">.{2,3} - ([^\(]*?) \(\d{1,2}:\d{2}\) \((?:L|OT|OTL|SL|SOL)\)<\/td>/gsi);
    # Then convert to jersey, via the surname
    my ($visitor_players, $home_players) = ($boxscore =~ m/<table width="100%" border="1"  cellspacing="0" cellpadding="0">\s*<tr bgcolor="#000080"><td align="center" colspan="3"><font color="#FFFFFF"><b>.*? LINEUP<\/b><\/font><\/td><\/tr>\s*<tr><td align="center">Pos<\/td><td align="right">No\.&nbsp;<\/td><td width="70%">&nbsp;Name<\/td><\/tr>(.*?)<\/table>\s*<\/td>.*?<table width="100%" border="1"  cellspacing="0" cellpadding="0">\s*<tr bgcolor="#000080"><td align="center" colspan="3"><font color="#FFFFFF"><b>.*? LINEUP<\/b><\/font><\/td><\/tr>\s*<tr><td align="center">Pos<\/td><td align="right">No\.&nbsp;<\/td><td width="70%">&nbsp;Name<\/td><\/tr>(.*?)<\/table>\s*<\/td>/gsi);
    my %game_goalies = ( );
    @{$game_goalies{'visitor'}} = ($visitor_players =~ m/<tr><td align="center">G[CA]?<\/td><td align="right">(\d+)&nbsp;<\/td><td>&nbsp;(?:<B>)?([^,]+), .*?(?:<\/B>)?<\/td><\/tr>/gsi);
    @{$game_goalies{'home'}} = ($home_players =~ m/<tr><td align="center">G[CA]?<\/td><td align="right">(\d+)&nbsp;<\/td><td>&nbsp;(?:<B>)?([^,]+), .*?(?:<\/B>)?<\/td><\/tr>/gsi);

    my @loop = ( { 'key' => 'goalie_w', 'value' => $alt_goalie_w, 'list' => ($misc{'scoring'}{'winner'} eq $$game_info{'visitor_id'} ? 'visitor' : 'home') },
                 { 'key' => 'goalie_l', 'value' => $alt_goalie_l, 'list' => ($misc{'scoring'}{'winner'} eq $$game_info{'visitor_id'} ? 'home' : 'visitor') } );
    foreach my $l (@loop) {
      while (my ($jersey, $sname) = splice(@{$game_goalies{$$l{'list'}}}, 0, 2)) {
        $misc{'status'}{$$l{'key'}} = $jersey if defined($$l{'value'}) && $sname eq $$l{'value'};
      }
    }
  }

  # If *this* failed, this is (probably) a tied Elite League game
  if ($misc{'status'}{'goalie_w'} eq 'goalie_l') {
    # No winner, so remove the goalie_w => goalie_l element
    print "#   Tied game...?\n";
    delete($misc{'status'}{'goalie_w'});

  # Convert Win/Loss -> Home/Visitor
  } else {
    $misc{'status'}{'goalie_home'} = ($misc{'scoring'}{'winner'} eq $$game_info{'home_id'} ? $misc{'status'}{'goalie_w'} : $misc{'status'}{'goalie_l'});
    $misc{'status'}{'goalie_visitor'} = ($misc{'scoring'}{'winner'} eq $$game_info{'visitor_id'} ? $misc{'status'}{'goalie_w'} : $misc{'status'}{'goalie_l'});
  }

  # Game Status
  print "# Game Status\n";
  my $status = 'F';
  if ($summary =~ m/<tr class="dark"><td class="content-w"><b>SCORING<\/b><\/td>.*?<font color="#ffffff"><b>SO<\/td>/gsi) {
    $status = 'SO';
  } elsif ($summary =~ m/<b>\d?OT<\/td>/) {
    ($status) = ($summary =~ m/<tr class="dark"><td class="content-w"><b>SCORING<\/b><\/td>.*?<font color="#ffffff"><b>(\d?OT)<\/td>\s*<td align="center" class="content-w"><font color="#ffffff"><b>Total&nbsp;<\/td>/gsi);
  }
  $misc{'status'}{'status'} = $status;

  # Attendance
  print "# Attendance\n";
  my ($attendance) = ($detail_att =~ m/Attendance: (\d+)\D/gsi);
  $misc{'status'}{'attendance'} = $attendance;

  print "\n";
}

#
# Misc Game Info (Display)
#
sub parse_misc_display {
  our (%config, $game_info, %misc);
  print "##\n## Misc Info (Display)\n##\n##\n\n";

  # Shots by Period
  print "# Shots by Period\n";
  while (my ($team_id, $shots) = each %{$misc{'shots'}}) {
    for (my $i = 0; $i < @$shots; $i++) {
      my $period = $i+1;
      print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, shots) VALUES ('$season', '$game_type', '$game_id', '$team_id', '$period', '$$shots[$i]') ON DUPLICATE KEY UPDATE shots = VALUES(shots);\n";
    }
  }
  print "\n";

  # Special Teams
  print "# Special Teams\n";
  while (my ($team_id, $stats) = each %{$misc{'special_teams'}}) {
    while (my ($type, $type_stats) = each %$stats) {
      print "INSERT INTO SPORTS_AHL_GAME_PP_STATS (season, game_type, game_id, team_id, pp_type, pp_goals, pp_opps) VALUES ('$season', '$game_type', '$game_id', '$team_id', '$type', '$$type_stats{'goals'}', '$$type_stats{'opp'}');\n";
    }
  }
  print "\n";

  # Three Stars
  print "# Three Stars\n";
  my @stars = @{$misc{'stars_raw'}};
  while (my ($star, $star_team_id, $star_jersey, $star_name) = splice(@stars, 0, 4)) {
    # Convert team_id from their version to ours
    $star_team_id = (defined($rosters{'by_name_fs'}{$$game_info{'visitor_id'}}{$star_name}) && $rosters{'by_name_fs'}{$$game_info{'visitor_id'}}{$star_name} eq $star_jersey ? $$game_info{'visitor_id'} : $$game_info{'home_id'});

    print "INSERT INTO SPORTS_AHL_GAME_THREE_STARS (season, game_type, game_id, star, team_id, jersey) VALUES ('$season', '$game_type', '$game_id', '$star', '$star_team_id', '$star_jersey');\n";
    $misc{'stars'}{$star} = { 'team_id' => $star_team_id, 'jersey' => $star_jersey };
  }
  print "\n";

  # Officials
  print "# Officials\n";
  foreach my $offic_type (reverse sort keys %{$misc{'officials'}}) {
    foreach my $offic_jersey (sort {$a <=> $b} keys %{$misc{'officials'}{$offic_type}}) {
      print "# $misc{'officials'}{$offic_type}{$offic_jersey}{'fname'} $misc{'officials'}{$offic_type}{$offic_jersey}{'sname'}, #$offic_jersey ($offic_type)\n";
      print "INSERT IGNORE INTO SPORTS_AHL_OFFICIALS (season, official_id, first_name, surname) VALUES ('$season', '$offic_jersey', '$misc{'officials'}{$offic_type}{$offic_jersey}{'fname'}', '$misc{'officials'}{$offic_type}{$offic_jersey}{'sname'}');\n";
      print "INSERT INTO SPORTS_AHL_GAME_OFFICIALS (season, game_type, game_id, official_type, official_id) VALUES ('$season', '$game_type', '$game_id', '$offic_type', '$offic_jersey');\n";
    }
  }
  print "\n";

  # Scores
  print "# Scores\n";
  print "# -> Visitor: ${$misc{'scoring'}}{$$game_info{'visitor_id'}}\n";
  print "# -> Home: ${$misc{'scoring'}}{$$game_info{'home_id'}}\n";

  # Goalies of Record
  print "# Goalies of Record\n";
  my $goalie_home = $misc{'status'}{'goalie_home'};
  my $goalie_visitor = $misc{'status'}{'goalie_visitor'};
  check_for_null(\$goalie_home);
  print "# -> Home: " . (defined($misc{'status'}{'goalie_home'}) ? $misc{'status'}{'goalie_home'} : 'Unknown') . "\n";
  check_for_null(\$goalie_visitor);
  print "# -> Visitor: " . (defined($misc{'status'}{'goalie_visitor'}) ? $misc{'status'}{'goalie_visitor'} : 'Unknown') . "\n";

  # Game Status
  print "# Game Status\n";
  my $status = $misc{'status'}{'status'};
  check_for_null(\$status);
  print "# -> $misc{'status'}{'status'}\n";

  # Attendance
  print "# Attendance\n";
  my $attendance = $misc{'status'}{'attendance'};
  check_for_null(\$attendance);
  print "# -> ";
  if (defined($misc{'status'}{'attendance'})) {
    print $misc{'status'}{'attendance'};
  } else {
    print "Unknown";
  }
  print "\n";

  # Process these updates
  print "UPDATE SPORTS_AHL_SCHEDULE SET home_score = '${$misc{'scoring'}}{$$game_info{'home_id'}}', visitor_score = '${$misc{'scoring'}}{$$game_info{'visitor_id'}}', status = $status, attendance = $attendance, home_goalie = $goalie_home, visitor_goalie = $goalie_visitor WHERE season = '$season' AND game_type = '$game_type' AND game_id = '$game_id';\n\n";
}

# Return true to pacify the compiler
1;
