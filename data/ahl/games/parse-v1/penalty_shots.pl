#!/usr/bin/perl -w
#
# Penalties
#
sub parse_penalty_shots {
  our ($game_info, $summary);
  print "##\n## Penalty Shots (misses only...)\n##\n";

  # Get
  my ($ps_visitor, $ps_home) = ($summary =~ m/<tr class="content-w"><td class="dark" align="center" colspan="2"><b class="content-w">Penalty Shots<\/b><\/font><\/td>\s*<\/tr>.*?<tr><td width="50%" valign="top">\s*<table border="0" cellpadding="2" cellspacing="0" width="100%">\s*<tr  class="light"><td class="light" valign="top" width="5">#<\/td><td><b>Shooter<\/b><\/td><td  width="5"><b>#<\/b><\/td><td><b>Goaltender<\/b><\/td><td  width="15"><b>Period<\/b><\/td><td class="light"><b>Time<\/b><\/td><td class="light"><b>Goal\?<\/b><\/td><td class="light"><b>Rule<\/b><\/td><\/tr>(.*?)<\/table>\s*<\/td>\s*<td width="50%" valign="top">\s*<table border="0" cellpadding="2" cellspacing="0" width="100%">\s*<tr valign="top" class="light"><td width="5">#<\/td><td><b>Shooter<\/b><\/td><td width="5"><b>\#<\/b><\/td><td><b>Goaltender<\/b><\/td><td width="15"><b>Period<\/b><\/td><td><b>Time<\/b><\/td><td><b>Goal\?<\/b><\/td><td><b>Rule<\/b><\/td><\/tr>(.*?)<\/table>\s*<\/td>\s*<\/tr>/gsi);

  # Process
  my @ps = ( );
  my @loop = ( { 'team_id' => $$game_info{'visitor_id'}, 'ps' => $ps_visitor },
               { 'team_id' => $$game_info{'home_id'}, 'ps' => $ps_home } );
  foreach my $l (@loop) {
    # Skip empty matches
    next if !defined($$l{'ps'}) || $$l{'ps'} =~ m/^\s*$/;

    # Get the list
    my @list = ($$l{'ps'} =~ m/<tr valign="top"  class="light"><td width="5" class="r" valign="top">(\d+)<\/td><td>.*?<\/td><td>(\d+)<\/td><td>.*?<\/td><td>(.*?)<\/td><td nowrap>(\d{1,2}:\d{2})<\/td><td>(Yes|No)<\/td><td nowrap>.*?<\/td><\/tr>/gsi);
    while (my @s = splice(@list, 0, 5)) {
      next if $s[4] eq 'Yes'; # Only after misses.....
      push @ps, { 'shooter' => $s[0],
                  'goalie' => $s[1],
                  'period' => $s[2],
                  'time' => $s[3],
                  'team_id' => $$l{'team_id'}};
    }
  }

  if (!@ps) {
    print "# Skipping: No missed penalty shots...\n\n";
    return;
  }
  print "\n";

  foreach my $s (@ps) {
    my $opp_team_id = ($$s{'team_id'} eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
    convert_period(\$$s{'period'});
    $misc{'event_id'}++;

    print "# $$s{'time'} $$s{'period'}: $$s{'team_id'} #$$s{'shooter'} on $opp_team_id #$$s{'goalie'}\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$$s{'period'}', '00:$$s{'time'}', 'SHOT', '$$s{'team_id'}');\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT_SHOT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, penalty_shot) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$$s{'team_id'}', '$$s{'shooter'}', '$opp_team_id', '$$s{'goalie'}', '1');\n\n";
  }
}

# Return true to pacify the compiler
1;
