#!/usr/bin/perl -w
# Tidy up the database tables after a game import and processing run

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
my $common = abs_path(__DIR__ . '/parse-v3/common.pl');
require $common;

##
## Reset a previous run
##
print "##\n## Table Maintenance\n##\n";
reorder_tables(@{$config{'tables'}});
print "##\n## Table Maintenance Complete\n##\n\n";

