#!/usr/bin/perl -w

# Clear the previous run
sub clear_play_by_play {
  clear_previous(
    'SPORTS_AHL_GAME_EVENT',
    'SPORTS_AHL_GAME_EVENT_GOAL',
    'SPORTS_AHL_GAME_EVENT_ONICE',
    'SPORTS_AHL_GAME_EVENT_PENALTY',
    'SPORTS_AHL_GAME_EVENT_SHOT',
    'SPORTS_AHL_GAME_EVENT_SHOOTOUT',
    'SPORTS_AHL_GAME_EVENT_BOXSCORE',
  );
}

#
# Play-by-Play
#
sub parse_play_by_play {
  our (%config, $pbp);
  print "##\n## Play-by-Play\n##\n";

  our $event_id = 0;
  my %type_map = (
    'goal' => 'GOAL',
    'penalty' => 'PENALTY',
    'shot' => 'SHOT',
    'penaltyshot' => 'SHOT',
    'shootout' => 'SHOOTOUT',
  );
  our %goal_counts = ();

  # Process the individual events
  foreach my $play (@$pbp) {
    # Skip certain events
    next if !defined($type_map{$$play{'event'}});
    $event_id++;

    # Parse the play
    my $parse_method = "parse_play_by_play_$$play{'event'}";
    my ($comment, $team_id, $detail_sql, $pbp, $extra_sql) = $parse_method->($$play{'details'});
    next if !defined($comment); # Some plays need to be skipped

    # Event headline info
    $$play{'details'}{'period'}{'id'} = 5 if $$play{'event'} eq 'shootout';
    my $time = parse_play_by_play_time($$play{'details'}{'period'}{'id'}, $$play{'details'}{'time'});
    print "\n# P$$play{'details'}{'period'}{'id'} $time :: $type_map{$$play{'event'}} :: $comment\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$event_id', '$$play{'details'}{'period'}{'id'}', '00:$time', '$type_map{$$play{'event'}}', '$team_id', '$pbp');\n";
    print "$detail_sql\n";
    print join("\n", @$extra_sql) . "\n" if defined($extra_sql);
  }

  # Identify and merge the boxscore events
  print "INSERT INTO SPORTS_AHL_GAME_EVENT_BOXSCORE (`season`, `game_type`, `game_id`, `event_id`, `period`, `event_time`, `event_type`, `team_id`, `play`)
  SELECT `season`, `game_type`, `game_id`, `event_id`, `period`, `event_time`, `event_type`, `team_id`, `play`
  FROM SPORTS_AHL_GAME_EVENT
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  AND   `game_id` = '$game_id'
  AND   `event_type` IN ('GOAL','PENALTY','SHOOTOUT');\n";

  # Now tidy and propagate some the totals
  tidy_play_by_play_goals();
  tidy_play_by_play_penalties();
  tidy_play_by_play_shootout();
}

sub parse_play_by_play_goal {
  my ($play) = @_;
  # Teams
  my $team_id = convert_team_remote_id($season, $$play{'team'}{'id'});
  my $opp_team_id = ($team_id eq $$game_info{'home_id'} ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
  if (!defined($goal_counts{$team_id})) {
    $goal_counts{$team_id} = 1;
  } else {
    $goal_counts{$team_id}++;
  }
  # Players
  my $scorer = $$play{'scoredBy'}{'jerseyNumber'};
  my $assist_1 = defined($$play{'assists'}[0]) ? $$play{'assists'}[0]{'jerseyNumber'} : 'NULL';
  my $assist_2 = defined($$play{'assists'}[1]) ? $$play{'assists'}[1]{'jerseyNumber'} : 'NULL';
  # Flags
  my $goal_type = 'EV';
  $goal_type = 'PP' if $$play{'properties'}{'isPowerPlay'};
  $goal_type = 'SH' if $$play{'properties'}{'isShortHanded'};
  my $goalie_status = ($$play{'properties'}{'isEmptyNet'} ? 'EN' : 'NULL');
  my $penalty_shot = int($$play{'properties'}{'isPenaltyShot'});
  # Shot Location
  my $coord_x = defined($$play{'xLocation'}) ? $$play{'xLocation'} : 'NULL';
  my $coord_y = defined($$play{'yLocation'}) ? $$play{'yLocation'} : 'NULL';
  my $heatzone = parse_play_by_play_heatzone($coord_x, $coord_y);
  # Comment
  my $comment = "$team_id #$scorer (A: #$assist_1, #$assist_2) (Num:$goal_counts{$team_id} / Type:$goal_type / G:$goalie_status / PS:$penalty_shot) ($coord_x, $coord_y)";
  # Full SQL
  $goalie_status = "'$goalie_status'" if $goalie_status ne 'NULL';
  $assist_1 = "'$assist_1'" if $assist_1 ne 'NULL';
  $assist_2 = "'$assist_2'" if $assist_2 ne 'NULL';
  $coord_x = "'$coord_x'" if $coord_x ne 'NULL';
  $coord_y = "'$coord_y'" if $coord_y ne 'NULL';
  $heatzone = "'$heatzone'" if $heatzone ne 'NULL';
  my $detail_sql = "INSERT INTO SPORTS_AHL_GAME_EVENT_GOAL (season, game_type, game_id, event_id, by_team_id, goal_num, scorer, assist_1, assist_2, goal_type, goalie_status, penalty_shot, coord_x, coord_y, heatzone) VALUES ('$season', '$game_type', '$game_id', '$event_id', '$team_id', '$goal_counts{$team_id}', '$scorer', $assist_1, $assist_2, '$goal_type', $goalie_status, '$penalty_shot', $coord_x, $coord_y, $heatzone);";
  # Plus/Minus
  my @extra_sql = ();
  my @plus = (); my @minus = ();
  for (my $i = 0; $i < 6; $i++) {
    $plus[$i] = (defined($$play{'plus_players'}[$i]{'jerseyNumber'}) ? "'$$play{'plus_players'}[$i]{'jerseyNumber'}'" : 'NULL');
    $minus[$i] = (defined($$play{'minus_players'}[$i]{'jerseyNumber'}) ? "'$$play{'minus_players'}[$i]{'jerseyNumber'}'" : 'NULL');
  }
  push @extra_sql, "INSERT INTO SPORTS_AHL_GAME_EVENT_ONICE (season, game_type, game_id, event_id, team_id, player_1, player_2, player_3, player_4, player_5, player_6) VALUES ('$season', '$game_type', '$game_id', '$event_id', '$team_id', $plus[0], $plus[1], $plus[2], $plus[3], $plus[4], $plus[5]);"
    if $plus[0] ne 'NULL';
  push @extra_sql, "INSERT INTO SPORTS_AHL_GAME_EVENT_ONICE (season, game_type, game_id, event_id, team_id, player_1, player_2, player_3, player_4, player_5, player_6) VALUES ('$season', '$game_type', '$game_id', '$event_id', '$opp_team_id', $minus[0], $minus[1], $minus[2], $minus[3], $minus[4], $minus[5]);"
    if $minus[0] ne 'NULL';
  # Play-by-Play
  my $pbp = '';
  $pbp .= 'Power Play ' if $goal_type eq 'PP';
  $pbp .= 'Shorthanded ' if $goal_type eq 'SH';
  $pbp =~ s/ $/, / if $penalty_shot && $pbp ne ''; # Formatting...
  $pbp .= 'Penalty Shot ' if $penalty_shot;
  $pbp .= 'Empty Net ' if $goalie_status eq '\'EN\'';
  $pbp .= 'Extra Skater ' if $goalie_status eq '\'WG\'';
  $pbp .= 'Goal, scored by {{' . $team_id . '_' . $scorer . '}}';
  $pbp .= ' assisted by {{' . $team_id . '_' . $assist_1 . '}}' if $assist_1 ne 'NULL';
  $pbp .= ' and {{' . $team_id . '_' . $assist_2 . '}}' if $assist_2 ne 'NULL';
  $pbp =~ s/'(\d+)'/$1/g; # Fix column quoting
  return ($comment, $team_id, $detail_sql, $pbp, \@extra_sql);
}

sub parse_play_by_play_penalty {
  my ($play) = @_;
  # Some penalties do not apply
  return (undef) if $$play{'againstTeam'}{'id'} eq '0';
  # Team
  my $team_id = convert_team_remote_id($season, $$play{'againstTeam'}{'id'});
  # Players
  my $taken_by = defined($$play{'takenBy'}{'jerseyNumber'}) ? $$play{'takenBy'}{'jerseyNumber'} : 0; # This would be a Bench Penalty
  my $served_by = defined($$play{'servedBy'}{'jerseyNumber'}) ? $$play{'servedBy'}{'jerseyNumber'} : 'NULL';
  my $bench = ($taken_by eq 0);
  # Type / PIMs
  my $type_raw = $$play{'description'};
  my $type = parse_penalty_type($type_raw);
  my $pims = $$play{'minutes'}; $pims =~ s/\.00$//;
  # Comment
  my $comment = "$team_id" . ($bench ? ' (BN)' : '') . " #$taken_by" . ($taken_by ne $served_by ? ", served by #$served_by" : '') . ", $pims for $type; Raw: $type_raw)";
  # Full SQL
  $served_by = "'$served_by'" if $served_by ne 'NULL';
  my $detail_sql = "INSERT INTO SPORTS_AHL_GAME_EVENT_PENALTY (season, game_type, game_id, event_id, team_id, jersey, served_by, type, pims) VALUES ('$season', '$game_type', '$game_id', '$event_id', '$team_id', '$taken_by', $served_by, '$type', '$pims');";
  # Play-by-Play
  my $pbp = '';
  $pbp = 'Bench ' if $taken_by eq "'0'";
  if ($pims == 2) {
    $pbp .= 'Minor Penalty';
  } elsif ($pims == 4) {
    $pbp .= 'Double-Minor';
  } elsif ($pims == 5) {
    $pbp .= 'Major Penalty';
  } else {
    $pbp .= 'Penalty';
  }
  $pbp .= " on {{${team_id}_${taken_by}}}" if $taken_by ne "'0'";
  $pbp .= " for $type";
  $pbp .= " (Served by {{${team_id}_$served_by}})" if $served_by !~ /^(NULL|'$taken_by')$/;
  $pbp =~ s/'//g; # Formatting
  return ($comment, $team_id, $detail_sql, $pbp);
}

sub parse_play_by_play_shot {
  my ($play) = @_;
  $$play{'isPenaltyShot'} = 0 if !defined($$play{'isPenaltyShot'});
  # Skipping shots that result in goals (we parse the goal)
  return (undef) if $$play{'isGoal'};
  # Teams
  my $team_id = convert_team_remote_id($season, $$play{'shooterTeamId'});
  my $opp_team_id = ($team_id eq $$game_info{'home_id'} ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
  # Players
  my $shooter = $$play{'shooter'}{'jerseyNumber'};
  my $goalie = defined($$play{'goalie'}{'jerseyNumber'}) ? $$play{'goalie'}{'jerseyNumber'} : 'NULL';
  # Shot Location
  my $coord_x = defined($$play{'xLocation'}) ? $$play{'xLocation'} : 'NULL';
  my $coord_y = defined($$play{'yLocation'}) ? $$play{'yLocation'} : 'NULL';
  my $heatzone = parse_play_by_play_heatzone($coord_x, $coord_y);
  # Comment
  my $comment = "$team_id #$shooter on $opp_team_id #$goalie ($coord_x, $coord_y)";
  $comment .= " ** PENALTY SHOT **" if $$play{'isPenaltyShot'};
  # Full SQL
  $goalie = "'$goalie'" if $goalie ne 'NULL';
  $coord_x = "'$coord_x'" if $coord_x ne 'NULL';
  $coord_y = "'$coord_y'" if $coord_y ne 'NULL';
  $heatzone = "'$heatzone'" if $heatzone ne 'NULL';
  my $detail_sql = "INSERT INTO SPORTS_AHL_GAME_EVENT_SHOT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, penalty_shot, coord_x, coord_y, heatzone) VALUES ('$season', '$game_type', '$game_id', '$event_id', '$team_id', '$shooter', '$opp_team_id', $goalie, '$$play{'isPenaltyShot'}', $coord_x, $coord_y, $heatzone);";
  # Play-by-Play
  my $pbp = ($$play{'isPenaltyShot'} ? 'Penalty Shot Miss' : 'Shot on Goal') . " by {{${team_id}_$shooter}}";
  return ($comment, $team_id, $detail_sql, $pbp);
}

sub parse_play_by_play_penaltyshot {
  my ($play) = @_;
  # Process as if it's a shot, but flag that it's a penalty shot
  $$play{'isPenaltyShot'} = 1;
  $$play{'shooterTeamId'} = $$play{'shooter_team'}{'id'}; # Data fix...
  return parse_play_by_play_shot($play);
}

sub parse_play_by_play_shootout {
  my ($play) = @_;
  # Teams
  my $team_id = convert_team_remote_id($season, $$play{'shooterTeam'}{'id'});
  my $opp_team_id = ($team_id eq $$game_info{'home_id'} ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
  # Players
  my $shooter = $$play{'shooter'}{'jerseyNumber'};
  my $goalie = defined($$play{'goalie'}{'jerseyNumber'}) ? $$play{'goalie'}{'jerseyNumber'} : 'NULL';
  # Result
  my $status = ($$play{'isGoal'} ? 'goal' : 'no_goal');
  # Comment
  my $comment = "$team_id #$shooter on $opp_team_id #$goalie :: " . ($$play{'isGoal'} ? 'GOAL' : 'No Goal');
  # Full SQL
  $goalie = "'$goalie'" if $goalie ne 'NULL';
  my $detail_sql = "INSERT INTO SPORTS_AHL_GAME_EVENT_SHOOTOUT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, status) VALUES ('$season', '$game_type', '$game_id', '$event_id', '$team_id', '$shooter', '$opp_team_id', $goalie, '$status');";
  # Play-by-Play
  my $pbp = "{{${team_id}_$shooter}} ";
  $pbp .= 'scored' if $status eq 'goal';
  $pbp .= 'missed' if $status eq 'no_goal';
  return ($comment, $team_id, $detail_sql, $pbp);
}

sub parse_play_by_play_time {
  my ($period, $time) = @_;
  # Shootout?
  if ($game_type eq 'regular' && $period == 5) {
    return '00:00';
  }
  # No, regular play
  my ($time_m, $time_s) = split(':', $time);
  my $s = ($time_m * 60) + $time_s;
  my $max = ($game_type eq 'playoff' || $period != 4 ? 1200 : 300); # 20min regulation / playoff OT, 5min regular OT
  my $left = $max - $s;
  return sprintf('%02d:%02d', int($left / 60), $left % 60);
}

sub parse_play_by_play_heatzone {
  my ($coord_x, $coord_y) = @_;
  # Skip incomplete data
  return 'NULL' if ($coord_x eq 'NULL' || $coord_y eq 'NULL');
  # Map from (0,0) -> (600,300) to (-100,-42) -> (100,42) and manipulate
  my $map_x = ($coord_x - 300) / 3;
  my $map_y = ($coord_y - 150) / (150 / 42);
  return (floor(($map_y + 45) / 13) * 8) + floor(abs($map_x) / 12.5) + 1;
}

sub tidy_play_by_play_goals {
  # Tidy ups: Special Team and Period scoring
  my $gw_num = ($misc{'scoring'}{'loser'} ne '' ? $misc{'scoring'}{$misc{'scoring'}{'loser'}} + 1 : -1); # Tied game has no GWG...
  print "\n# Tidy special teams\n";
  print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, pp_goals, pp_assists, sh_goals, sh_assists, gw_goals)
  SELECT LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id,
         SUM(GOAL.goal_type = 'PP' AND GOAL.scorer = LINEUP.jersey) AS pp_goals,
         SUM(GOAL.goal_type = 'PP' AND (GOAL.assist_1 = LINEUP.jersey OR GOAL.assist_2 = LINEUP.jersey)) AS pp_assists,
         SUM(GOAL.goal_type = 'SH' AND GOAL.scorer = LINEUP.jersey) AS sh_goals,
         SUM(GOAL.goal_type = 'SH' AND (GOAL.assist_1 = LINEUP.jersey OR GOAL.assist_2 = LINEUP.jersey)) AS sh_assists,
         SUM(GOAL.goal_num = '$gw_num' AND GOAL.scorer = LINEUP.jersey) AS gw_goals
  FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
  LEFT JOIN SPORTS_AHL_GAME_EVENT_GOAL AS GOAL
    ON (GOAL.season = LINEUP.season
    AND GOAL.game_type = LINEUP.game_type
    AND GOAL.game_id = LINEUP.game_id
    AND GOAL.by_team_id = LINEUP.team_id)
  WHERE LINEUP.season = '$season'
  AND   LINEUP.game_type = '$game_type'
  AND   LINEUP.game_id = '$game_id'
  AND   LINEUP.pos <> 'G'
  GROUP BY LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id
ON DUPLICATE KEY UPDATE pp_goals = VALUES (pp_goals),
                        pp_assists = VALUES (pp_assists),
                        sh_goals = VALUES (sh_goals),
                        sh_assists = VALUES (sh_assists),
                        gw_goals = VALUES (gw_goals);\n";

  print "\n# Tidy period scoring\n";
  print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, goals)
  SELECT season, game_type, game_id, team_id, period, COUNT(event_id) AS goals
  FROM SPORTS_AHL_GAME_EVENT
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   game_id = '$game_id'
  AND   event_type = 'GOAL'
  GROUP BY team_id, period
ON DUPLICATE KEY UPDATE goals = VALUES(goals);\n";
}

sub tidy_play_by_play_penalties {
  print "\n# Tidy period penalties\n";
  print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, pims)
  SELECT EVENT.season, EVENT.game_type, EVENT.game_id, EVENT.team_id, EVENT.period, IFNULL(SUM(PENALTY.pims), 0) AS pims
  FROM SPORTS_AHL_GAME_EVENT AS EVENT
  LEFT JOIN SPORTS_AHL_GAME_EVENT_PENALTY AS PENALTY
    ON (PENALTY.season = EVENT.season
    AND PENALTY.game_type = EVENT.game_type
    AND PENALTY.game_id = EVENT.game_id
    AND PENALTY.event_id = EVENT.event_id)
  WHERE EVENT.season = '$season'
  AND   EVENT.game_type = '$game_type'
  AND   EVENT.game_id = '$game_id'
  AND   EVENT.event_type = 'PENALTY'
  GROUP BY EVENT.team_id, EVENT.period
ON DUPLICATE KEY UPDATE pims = VALUES(pims);\n";
}

sub tidy_play_by_play_shootout {
  my @loop = (
    { 'type' => 'shooters', 'tbl' => 'SKATERS', 'agst' => '', 'col' => 'by', 'pos' => '<>' },
    { 'type' => 'goalies', 'tbl' => 'GOALIES', 'agst' => '_against', 'col' => 'on', 'pos' => '=' }
  );
  foreach my $l (@loop) {
    print "\n# Tidy shootout $$l{'type'}\n";
    print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_$$l{'tbl'} (season, player_id, game_type, game_id, so_goals$$l{'agst'}, so_shots$$l{'agst'})
  SELECT LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id,
         SUM(SHOOTOUT.$$l{'col'}_jersey = LINEUP.jersey AND SHOOTOUT.status = 'goal') AS so_goals$$l{'agst'},
         SUM(SHOOTOUT.$$l{'col'}_jersey = LINEUP.jersey) AS so_shots$$l{'agst'}
  FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
  JOIN SPORTS_AHL_PLAYERS_GAME_$$l{'tbl'} AS PLAYER
    ON (PLAYER.season = LINEUP.season
    AND PLAYER.game_type = LINEUP.game_type
    AND PLAYER.game_id = LINEUP.game_id
    AND PLAYER.team_id = LINEUP.team_id
    AND PLAYER.player_id = LINEUP.player_id)
  LEFT JOIN SPORTS_AHL_GAME_EVENT_SHOOTOUT AS SHOOTOUT
    ON (SHOOTOUT.season = LINEUP.season
    AND SHOOTOUT.game_type = LINEUP.game_type
    AND SHOOTOUT.game_id = LINEUP.game_id
    AND SHOOTOUT.$$l{'col'}_team_id = LINEUP.team_id)
  WHERE LINEUP.season = '$season'
  AND   LINEUP.game_type = '$game_type'
  AND   LINEUP.game_id = '$game_id'
  AND   LINEUP.pos $$l{'pos'} 'G'
  GROUP BY LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id
ON DUPLICATE KEY UPDATE so_goals$$l{'agst'} = VALUES(so_goals$$l{'agst'}),
                        so_shots$$l{'agst'} = VALUES(so_shots$$l{'agst'});\n";
  }
}

# Return true to pacify the compiler
1;
