#!/usr/bin/perl -w
# Common routines for game processing

use List::Util qw( max );
use JSON;

# Validate and load a file
sub load_file {
  our (%config);
  my ($season_dir, $game_type, $game_id, $filename, $args) = @_;

  # Validate
  my $file = sprintf('%s/_data/%s/games/%s/%d/%s.json.gz',
                     $config{'base_dir'},
                     $season_dir,
                     $game_type,
                     $game_id,
                     $filename);
  if (! -e $file) {
    print STDERR "File '$file' does not exist, unable to continue.\n";
    if (!$$args{'no_exit_on_error'}) {
      exit;
    } else {
      return ($file, undef);
    }
  }

  # Load the file
  my $contents;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  $contents = join('', @contents);
  @contents = (); # Garbage collect...
  $contents =~ s/^\(//; $contents =~ s/\)$//;
  $contents =~ s/\\u2019/'/g;
  $contents = decode_json($contents);

  return ($file, $contents);
}

# Clear a previous run
sub clear_previous {
  our ($season, $game_type, $game_id);
  foreach my $table (@_) {
    print "DELETE FROM `$table` WHERE `season` = '$season' AND `game_type` = '$game_type' AND `game_id` = '$game_id';\n";
  }
}

# Order by game
sub reorder_tables {
  foreach my $table (@_) {
    print "ALTER TABLE `$table` ORDER BY `season`, `game_type`, `game_id`;\n";
  }
}

# Convert undefined values to 0
sub convert_undef_zero {
  my ($txt) = @_;
  $$txt = 0 if !defined($$txt);
}

# Convert a period in to a period number (e.g., 1st = 1; OT = 4; SO = 5; 2OT = 5, etc...)
sub convert_period {
  my ($p) = @_;
  trim($p);

  # Remove any ordinal value
  $$p =~ s/^(\d+)\s*(?:st|nd|rd|th)$/$1/; # This could also catch 3OT, so need to be careful....

  # If numeric, no need to continue
  return if $$p =~ m/^\d+$/;

  # OT => 4
  if ($$p eq 'OT') {
    $$p = 4;
    return;
  }

  # SO => 5
  if ($$p eq 'SO') {
    $$p = 5;
    return;
  }

  # Ordinal OT (convert for below...)
  $$p =~ s/^(\d)(?:st|nd|rd|th)\s*OT$/OT$1/;

  # Convert xOT into 3+x...
  my $n;
  ($n) = ($$p =~ m/^(\d+)\s*OT/) if ($$p =~ m/^\d+\s*OT/);
  ($n) = ($$p =~ m/^OT\s*(\d+)$/) if ($$p =~ m/^OT\s*\d+$/);
  $$p = 3 + $n;
}

# Convert a penalty description from the boxscore's into ours
sub parse_penalty_type {
  my ($descrip) = @_;

  my $descrip_orig = $descrip;
  $descrip =~ s/^\s+//;
  $descrip =~ s/\s+$//;

  # Strip trailing postfixes
  $descrip =~ s/\s*-\s*Minor$//i;
  $descrip =~ s/\s*-\s*Major$//i;
  $descrip =~ s/\s*-\s*Match$//i;
  $descrip =~ s/\s*-\s*Misconduct$//i;
  $descrip =~ s/\s*-\s*Playing$//i;

  # Strip some common prefixes and suffixes that are accounted for in other areas of the penalty
  for my $class ('Bench minor', 'Double minor', 'Game misconduct', 'Major', 'Match', 'Misconduct', 'Penalty Shot') {
    $descrip =~ s/^$class\s*-\s*//i;
    $descrip =~ s/\s*-\s*$class$//i;
  }

  # Tweak some wording
  $descrip = 'Delay of game' if $descrip =~ /^Delay of game -/i;
  $descrip = 'Unsportsmanlike conduct' if $descrip =~ /^Unsportsmanlike conduct -/i;
  $descrip = 'Spearing' if $descrip eq 'Spearing (attempt)';
  $descrip = 'Unnecessary stoppage' if $descrip eq 'Goaltender - unnecessary stoppage';
  $descrip = 'Illegal stick' if $descrip eq 'Playing with opponent\'s stick';
  $descrip = 'Check to the head' if $descrip =~ /Illegal check to the head/i;

  # One final re-trim
  $descrip =~ s/^\s+//;
  $descrip =~ s/\s+$//;

  # Then standardise
  my $ret;
  # Our "direct" matches - the ENUM fields in the table
  my @direct = (
    'Abuse of officials',
    'Aggressor',
    'Bench minor',
    'Boarding',
    'Butt-ending',
    'Charging',
    'Check to the head',
    'Checking from behind',
    'Clipping',
    'Cross-checking',
    'Delay of game',
    'Diving',
    'Elbowing',
    'Fighting',
    'Game misconduct',
    'Goaltender interference',
    'Gross misconduct',
    'Handling the puck',
    'Head-butting',
    'High-sticking',
    'Holding',
    'Hooking',
    'Holding the stick',
    'Illegal equipment',
    'Illegal lineup',
    'Illegal stick',
    'Interference',
    'Kicking',
    'Kneeing',
    'Leaving the crease',
    'Misconduct',
    'Roughing',
    'Slashing',
    'Slew-footing',
    'Spearing',
    'Third man in',
    'Throwing equipment',
    'Too many men',
    'Tripping',
    'Unsportsmanlike conduct',
  );

  # Direct match?
  if (grep(/^$descrip$/, @direct)) {
    $ret = $descrip;

  # No, so map...
  } elsif ($descrip =~ /abuse of official/i) {
    $ret = 'Abuse of officials';

  } elsif ($descrip eq 'Abusive language'
           || $descrip =~ /leaving the bench/i) {
    $ret = 'Bench minor';

  } elsif ($descrip =~ /^butt-en/i) {
    $ret = 'Butt-ending';

  } elsif ($descrip =~ /^charg/i) {
    $ret = 'Charging';

  } elsif ($descrip eq 'Checking to the head') {
    $ret = 'Check to the head';

  } elsif ($descrip =~ /^delay of game/i
           || $descrip =~ /^coach/i
           || $descrip eq 'Disputing decision'
           || $descrip eq 'Goalkeeper'
           || $descrip eq 'Incorrect starting lineup'
           || $descrip =~ /^objects on ice/i
           || $descrip eq 'Unnecessary stoppage'
           || $descrip =~ /^covering puck/i) {
    $ret = 'Delay of game';

  } elsif ($descrip =~ /^diving/i) {
    $ret = 'Diving';

  } elsif ($descrip =~ /^fighting/i) {
    $ret = 'Fighting';

  } elsif ($descrip eq 'Leaving penalty bench'
           || $descrip eq 'Persisting a fight') {
    $ret = 'Game misconduct';

  } elsif ($descrip eq 'Closing hand on puck'
           || $descrip eq 'Picking up the puck') {
    $ret = 'Handling the puck';

  } elsif ($descrip =~ /^head-butt/i) {
    $ret = 'Head-butting';

  } elsif ($descrip =~ /^high-stick/i) {
    $ret = 'High-sticking';

  } elsif ($descrip =~ /^hold/i) {
    $ret = 'Holding';

  } elsif ($descrip =~ /^hook/i) {
    $ret = 'Hooking';

  } elsif ($descrip eq 'Elbow pad'
           || $descrip eq 'Jersey'
           || $descrip eq 'Visor') {
    $ret = 'Illegal equipment';

  } elsif ($descrip =~ /^illegal stick/i
           || $descrip eq 'Broken stick') {
    $ret = 'Illegal stick';

  } elsif ($descrip =~ /^instigat/i) {
    $ret = 'Instigator';

  } elsif ($descrip =~ /^interference/i) {
    $ret = 'Interference';

  } elsif ($descrip eq 'Goaltender - beyond center') {
    $ret = 'Leaving the crease';

  } elsif ($descrip eq 'Match') {
    $ret = 'Match penalty';

  } elsif ($descrip eq 'Continuing altercation'
           || $descrip eq 'Returning to ice/bench'
           || $descrip eq 'Secondary altercation'
           || $descrip eq 'Third major'
           || $descrip eq 'Third major/second fight') {
    $ret = 'Misconduct';

  } elsif ($descrip =~ /^roughing/i
           || $descrip eq 'Attempt/Deliberate injury') {
    $ret = 'Roughing';

  } elsif ($descrip =~ /^slash/i) {
    $ret = 'Slashing';

  } elsif ($descrip =~ /^throw/i) {
    $ret = 'Throwing equipment';

  } elsif ($descrip eq 'Illegal substitution') {
    $ret = 'Too many men';

  } elsif ($descrip =~ /^tripping/i) {
    $ret = 'Tripping';

  # Unknown?
  } else {
    print STDERR "Unknown penalty type '$descrip' (Originally: '$descrip_orig')\n";
    $ret = '*UNKNOWN*';
  }

  $descrip = $ret;
  return $descrip;
}

# Return true to pacify the compiler
1;
