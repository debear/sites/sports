#!/usr/bin/perl -w
use POSIX;

# Clear the previous run
sub clear_rosters {
  clear_previous(
    'SPORTS_AHL_GAME_LINEUP',
    'SPORTS_AHL_PLAYERS_GAME_SKATERS',
    'SPORTS_AHL_PLAYERS_GAME_GOALIES'
  );
}

#
# Rosters / Player Summary
#
sub parse_rosters {
  our (%config, $game_info, %misc, $summary);
  print "##\n## Roster / Player Summary\n##\n\n";
  # Info for sharing...
  our %rosters = ( 'by_team' => { $$game_info{'visitor_id'} => { },
                                  $$game_info{'home_id'} => { } },
                   'by_remote' => { } );

  my @loop = ( { 'code' => 'visitingTeam', 'team_id' => $$game_info{'visitor_id'} },
               { 'code' => 'homeTeam', 'team_id' => $$game_info{'home_id'} } );
  foreach my $loop (@loop) {
    print "## $$loop{'team_id'}\n";

    # Skaters
    print "# - Skaters\n\n";
    foreach my $skater (@{$$summary{$$loop{'code'}}{'skaters'}}) {
      # Some data fixes
      foreach my $field ('firstName', 'lastName') {
        $$skater{'info'}{$field} = Encode::encode('UTF-8', $$skater{'info'}{$field});
      }
      # Display debug info
      print "# $$skater{'info'}{'firstName'} $$skater{'info'}{'lastName'} ($$skater{'info'}{'position'}, #$$skater{'info'}{'jerseyNumber'})\n";
      my $player_id = $$skater{'info'}{'id'};

      # Exclusion: duplicate ID(s) in the remote database we need to manage
      if (defined($config{'map'}{'duplicate_players'}{$player_id})) {
        $player_id = $config{'map'}{'duplicate_players'}{$player_id};
        # Has this excluded player already been processed / will be processed?
        $skip = defined($rosters{'by_remote'}{$player_id});
        for (my $i = 0; !$skip && $i < (@{$$summary{$$loop{'code'}}{'skaters'}} / 11); $i++) {
          $skip = ($$summary{$$loop{'code'}}{'skaters'}[($i * 11) + 3] == $player_id);
        }
        if ($skip) {
          print "# - Skipping, an excluded player who appears in this game file multiple times\n";
          next;
        }
      }

      # Misc info
      my $capt = $$skater{'status'}; check_for_null(\$capt);
      my $starter = (defined($$skater{'starting'}) && $$skater{'starting'} ? 1 : 0);

      # Get stats, converting undef to 0 rather than NULL
      my $goals = $$skater{'stats'}{'goals'}; convert_undef_zero(\$goals);
      my $assists = $$skater{'stats'}{'assists'}; convert_undef_zero(\$assists);
      my $pims = $$skater{'stats'}{'penaltyMinutes'}; convert_undef_zero(\$pims);
      my $plus_minus = $$skater{'stats'}{'plusMinus'}; convert_undef_zero(\$plus_minus);
      my $shots = $$skater{'stats'}{'shots'}; convert_undef_zero(\$shots);
      my $hits = $$skater{'stats'}{'hits'}; convert_undef_zero(\$hits);

      # Process
      $rosters{'by_team'}{$$loop{'team_id'}}{$$skater{'info'}{'jerseyNumber'}} = $player_id;
      $rosters{'by_remote'}{$player_id} = $$skater{'info'}{'jerseyNumber'};
      $rosters{'by_name_fs'}{$$loop{'team_id'}}{"$$skater{'info'}{'firstName'} $$skater{'info'}{'lastName'}"} = $$skater{'info'}{'jerseyNumber'};
      $rosters{'by_name_sf'}{$$loop{'team_id'}}{"$$skater{'info'}{'lastName'} $$skater{'info'}{'firstName'}"} = $$skater{'info'}{'jerseyNumber'};
      $var = get_player_id($$loop{'team_id'}, $$skater{'info'}{'jerseyNumber'}, $player_id);

      print "INSERT INTO SPORTS_AHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started) VALUES ('$season', '$game_type', '$game_id', '$$loop{'team_id'}', '$$skater{'info'}{'jerseyNumber'}', $var, '$$skater{'info'}{'position'}', $capt, '$starter');\n";
      print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, gp, start, goals, assists, plus_minus, pims, shots) VALUES ('$season', $var, '$game_type', '$game_id', '$$loop{'team_id'}', '1', '$starter', '$goals', '$assists', '$plus_minus', '$pims', '$shots');\n\n";
    }

    # Goalies
    print "# - Goalies\n";
    my %team = ( 'all' => [ ], 'zero_ga' => [ ] );
    foreach my $goalie (@{$$summary{$$loop{'code'}}{'goalies'}}) {
      # Some data fixes
      foreach my $field ('firstName', 'lastName') {
        $$goalie{'info'}{$field} = Encode::encode('UTF-8', $$goalie{'info'}{$field});
      }
      # Implied position (in case of a skater as an EBUG)
      $$goalie{'info'}{'position'} = 'G';
      # Display debug info
      print "# $$goalie{'info'}{'firstName'} $$goalie{'info'}{'lastName'} ($$goalie{'info'}{'position'}, #$$goalie{'info'}{'jerseyNumber'})\n";
      my $player_id = $$goalie{'info'}{'id'};

      # Exclusion: duplicate ID(s) in the remote database we need to manage
      if (defined($config{'map'}{'duplicate_players'}{$player_id})) {
        $player_id = $config{'map'}{'duplicate_players'}{$player_id};
        # Has this excluded player already been processed / will be processed?
        $skip = defined($rosters{'by_remote'}{$player_id});
        for (my $i = 0; !$skip && $i < (@{$$summary{$$loop{'code'}}{'goalies'}} / 11); $i++) {
          $skip = ($$summary{$$loop{'code'}}{'goalies'}[($i * 11) + 3] == $player_id);
        }
        if ($skip) {
          print "# - Skipping, an excluded player who appears in this game file multiple times\n";
          next;
        }
      }

      # Misc info
      my $capt = $$goalie{'status'}; check_for_null(\$capt);
      my $starter = (defined($$goalie{'starting'}) && $$goalie{'starting'} ? 1 : 0);

      # Get stats, converting undef to 0 rather than NULL
      my $goals = $$goalie{'stats'}{'goals'}; convert_undef_zero(\$goals);
      my $assists = $$goalie{'stats'}{'assists'}; convert_undef_zero(\$assists);
      my $pims = $$goalie{'stats'}{'penaltyMinutes'}; convert_undef_zero(\$pims);
      my $plus_minus = $$goalie{'stats'}{'plusMinus'}; convert_undef_zero(\$plus_minus);
      my $mins_played = $$goalie{'stats'}{'timeOnIce'}; # Processed later, so do not convert (yet)
      my $shots_against = $$goalie{'stats'}{'shotsAgainst'}; convert_undef_zero(\$shots_against);
      my $goals_against = $$goalie{'stats'}{'goalsAgainst'}; convert_undef_zero(\$goals_against);
      my $saves = $$goalie{'stats'}{'saves'}; convert_undef_zero(\$saves);

      # Process
      $rosters{'by_team'}{$$loop{'team_id'}}{$$goalie{'info'}{'jerseyNumber'}} = $player_id;
      $rosters{'by_remote'}{$player_id} = $$goalie{'info'}{'jerseyNumber'};
      $rosters{'by_name_fs'}{$$loop{'team_id'}}{"$$goalie{'info'}{'firstName'} $$goalie{'info'}{'lastName'}"} = $$goalie{'info'}{'jerseyNumber'};
      $rosters{'by_name_sf'}{$$loop{'team_id'}}{"$$goalie{'info'}{'lastName'} $$goalie{'info'}{'firstName'}"} = $$goalie{'info'}{'jerseyNumber'};
      my $var = get_player_id($$loop{'team_id'}, $$goalie{'info'}{'jerseyNumber'}, $player_id);

      print "INSERT INTO SPORTS_AHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started) VALUES ('$season', '$game_type', '$game_id', '$$loop{'team_id'}', '$$goalie{'info'}{'jerseyNumber'}', $var, '$$goalie{'info'}{'position'}', $capt, '$starter');\n";

      # Skip game goalie row if player didn't play
      next if !defined($mins_played) || !$mins_played;

      # Record
      my $w   = (defined($misc{'status'}{'goalie_w'}) && $misc{'scoring'}{'winner'} eq $$loop{'team_id'} && $misc{'status'}{'goalie_w'} eq $$goalie{'info'}{'jerseyNumber'}) ? 1 : 0;
      my $l   = (defined($misc{'status'}{'goalie_l'}) && $misc{'scoring'}{'loser'}  eq $$loop{'team_id'} && $misc{'status'}{'goalie_l'} eq $$goalie{'info'}{'jerseyNumber'} && ($misc{'status'}{'status'} eq 'F'  || $game_type eq 'playoff')) ? 1 : 0;
      my $otl = (defined($misc{'status'}{'goalie_l'}) && $misc{'scoring'}{'loser'}  eq $$loop{'team_id'} && $misc{'status'}{'goalie_l'} eq $$goalie{'info'}{'jerseyNumber'} &&  $misc{'status'}{'status'} eq 'OT' && $game_type eq 'regular')  ? 1 : 0;
      my $sol = (defined($misc{'status'}{'goalie_l'}) && $misc{'scoring'}{'loser'}  eq $$loop{'team_id'} && $misc{'status'}{'goalie_l'} eq $$goalie{'info'}{'jerseyNumber'} &&  $misc{'status'}{'status'} eq 'SO' && $game_type eq 'regular')  ? 1 : 0;
      # MM:SS Time to SQL time
      my ($mins, $sec) = ($mins_played =~ m/^(\d{1,3}):(\d{2})/);
      if (defined($mins)) {
        my $hrs = floor($mins / 60);
        $mins = $mins % 60;
        $mins_played = sprintf('%02d:%02d:%02d', $hrs, $mins, $sec);
      } else {
        $mins_played = '00:' . $mins_played;
      }

      # SQL
      print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, gp, start, win, loss, ot_loss, so_loss, goals_against, shots_against, minutes_played, pims, goals, assists) VALUES ('$season', $var, '$game_type', '$game_id', '$$loop{'team_id'}', '1', '$starter', '$w', '$l', '$otl', '$sol', '$goals_against', '$shots_against', '$mins_played', '$pims', '$goals', '$assists');\n\n";

      # Add to lists for determining shutouts
      push @{$team{'all'}},     $var;
      push @{$team{'zero_ga'}}, $var if !$goals_against;
    }
    print "\n";

    # Shutout?
    print "# Record shutout for '$team{'zero_ga'}[0]'\nUPDATE SPORTS_AHL_PLAYERS_GAME_GOALIES SET shutout = 1 WHERE season = '$season' AND game_type = '$game_type' AND game_id = '$game_id' AND team_id = '$$loop{'team_id'}' AND player_id = $team{'zero_ga'}[0];\n\n"
      if (scalar(@{$team{'all'}}) == 1 && scalar(@{$team{'zero_ga'}}) == 1);
  }
}

# SQL to get the player_id
sub get_player_id {
  our (%config);
  my ($team_id, $jersey, $player_id) = @_;
  my $var = "\@player_${team_id}_${jersey}";

  print "SET $var := (SELECT player_id FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = '$player_id');\n";
  print "INSERT IGNORE INTO SPORTS_AHL_PLAYERS_IMPORT (player_id, remote_id, profile_imported) VALUES (\@player_$jersey, '$player_id', NOW());\n";
  print "SET $var := (SELECT IFNULL($var, LAST_INSERT_ID()));\n\n";

  return $var;
}

# Return true to pacify the compiler
1;
