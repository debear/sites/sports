#!/usr/bin/perl -w
# Get game data for a single game from theahl.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get game script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Declare variables
my ($season_dir, $game_type, $game_id) = @ARGV;
my $season = substr($season_dir, 0, 4);
my $season_full = sprintf('%04d%04d', $season, $season + 1);

my $game_info = get_game_info(@ARGV);
my $local = sprintf('%s/_data/%s/games/%s/%s.json.gz', $config{'base_dir'}, $season_dir, $game_type, $$game_info{'remote_id'});

# Run...
print "# Downloading $game_type game $game_id // $$game_info{'remote_id'} " . ($game_type eq 'playoff' ? ' (' . playoff_summary($season, $game_id) . ')' : '') . ": $$game_info{'visitor_id'} @ $$game_info{'home_id'}: ";

# Skip if already done...
if (-e $local) {
  print "[ Skipped ]\n";
  exit;
}

# Make sure the game dir exists
download("https://lscluster.hockeytech.com/feed/index.php?feed=statviewfeed&view=gameSummary&game_id=$$game_info{'remote_id'}&key=$config{'app_key'}&site_id=1&client_code=ahl&lang=en&league_id=", $local, {'gzip' => 1});
print "[ Done ]\n";

# Return true to pacify the compiler
1;
