#!/usr/bin/perl -w
# Game processing specific configuration

# Duplicate player_id's in remote system
$config{'map'}{'duplicate_players'} = {
  2118 => 4313,
  2401 => 5606,
  4712 => 5759,
  5440 => 5755,
  5444 => 5683,
  5699 => 5527,
};

# Tables we operate on
@{$config{'tables'}} = (
  'SPORTS_AHL_GAME_EVENT',
  'SPORTS_AHL_GAME_EVENT_GOAL',
  'SPORTS_AHL_GAME_EVENT_ONICE',
  'SPORTS_AHL_GAME_EVENT_PENALTY',
  'SPORTS_AHL_GAME_EVENT_SHOOTOUT',
  'SPORTS_AHL_GAME_EVENT_SHOT',
  'SPORTS_AHL_GAME_LINEUP',
  'SPORTS_AHL_GAME_OFFICIALS',
  'SPORTS_AHL_GAME_PERIODS',
  'SPORTS_AHL_GAME_PP_STATS',
  'SPORTS_AHL_GAME_THREE_STARS',
  'SPORTS_AHL_PLAYERS_GAME_SKATERS',
  'SPORTS_AHL_PLAYERS_GAME_GOALIES',
);

# Return true to pacify the compiler
1;
