#!/usr/bin/perl -w
# Load through the various games and generate the SQL to import them in to the database

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse roster script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
my $common = abs_path(__DIR__ . '/parse-v2/common.pl');
require $common;

# Load our component files
foreach my $f ( 'rosters', 'scoring', 'shootout', 'penalties', 'penalty_shots', 'misc' ) {
  my $g = abs_path(__DIR__ . '/parse-v2/' . $f . '.pl');
  require $g;
}

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

##
## Reset a previous run
##
clear_previous($season, $game_type, $game_id, @{$config{'tables'}});

##
## Process the game
##
# Load the data
print "\n## Loading data for $season // $game_type // $game_id // $$game_info{'remote_id'}\n";
print STDERR "# Loading $game_type game" . ($game_type eq 'playoff' ? ' (' . playoff_summary($season, $game_id) . ')' : '') . ": $season " . ucfirst($game_type) . ", Game $game_id\n";
our ($summary_file, $summary) = load_file($season_dir, $game_type, $$game_info{'remote_id'});

# Ensure we got a file....
if (!defined($summary)) {
  print STDERR "Missing game data ($season :: $game_type :: $game_id)\n";
  exit 10;
}

print "# Summary File:  $summary_file\n";

# Run the parts
$config{'disp'} = -1; # Section to display: -1 = All, 0 = None, 1-X = appropriate section
parse_misc_process(); # Always needed...
parse_rosters()       if check_disp(1);
parse_scoring()       if check_disp(2);
parse_shootout()      if check_disp(3);
parse_penalties()     if check_disp(4);
parse_penalty_shots() if check_disp(5);
parse_misc_display()  if check_disp(6);

# Return true to pacify the compiler
1;
