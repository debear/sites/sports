#!/usr/bin/perl -w
#
# Penalties
#
sub parse_penalty_shots {
  our (%config, $game_info, %misc, $summary);
  print "##\n## Penalty Shots (misses only...)\n##\n";

  # Process
  my @ps = ( );
  my @loop = ( { 'team_id' => $$game_info{'visitor_id'}, 'ps' => 'visitingTeam' },
               { 'team_id' => $$game_info{'home_id'}, 'ps' => 'homeTeam' } );
  foreach my $l (@loop) {
    # Skip empty matches
    next if !defined($$summary{'penaltyShots'}{$$l{'ps'}}) || !scalar(@{$$summary{'penaltyShots'}{$$l{'ps'}}});

    foreach my $ps (@{$$summary{'penaltyShots'}{$$l{'ps'}}}) {
      next if $$ps{'isGoal'}; # Only after misses.....
      my $period = $$ps{'period'}{'shortName'}; convert_period(\$period);
      push @ps, { 'shooter' => $$ps{'shooter'}{'jerseyNumber'},
                  'goalie' => $$ps{'goalie'}{'jerseyNumber'},
                  'period' => $period,
                  'time' => convert_event_time($$ps{'time'}),
                  'team_id' => $$l{'team_id'}};
    }
    if (!@ps) {
      print "# Skipping: No missed penalty shots...\n\n";
      return;
    }
    print "\n";

    foreach my $s (@ps) {
      my $opp_team_id = ($$s{'team_id'} eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
      $misc{'event_id'}++;

      print "# $$s{'time'} $$s{'period'}: $$s{'team_id'} #$$s{'shooter'} on $opp_team_id #$$s{'goalie'}\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$$s{'period'}', '00:$$s{'time'}', 'SHOT', '$$s{'team_id'}');\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT_SHOT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, penalty_shot) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$$s{'team_id'}', '$$s{'shooter'}', '$opp_team_id', '$$s{'goalie'}', '1');\n\n";
    }
  }
}

# Return true to pacify the compiler
1;
