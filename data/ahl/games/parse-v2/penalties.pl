#!/usr/bin/perl -w
#
# Penalties
#
sub parse_penalties {
  our (%config, %misc, $summary);
  print "##\n## Penalties\n##\n\n";

  foreach my $pe (@{$$summary{'periods'}}) {
    foreach my $p (@{$$pe{'penalties'}}) {
      # Split the JSON into component parts
      my $period = $$p{'period'};
      $period = $$period{'id'}
        if ref($period) eq 'HASH';
      my $team_id = convert_team_remote_id($season, $$p{'againstTeam'}{'id'});
      my $jersey = $$p{'takenBy'}{'jerseyNumber'};
      my $served = $$p{'servedBy'}{'jerseyNumber'};
      my $time = convert_event_time($$p{'time'});
      my $type = $$p{'description'};
      my $type_raw = $type;
      my $pims = $$p{'minutes'};

      # Event processing...
      $misc{'event_id'}++;

      # Who was it against and who served it?
      my $bench = !defined($jersey);
      my $player = ($bench ? 0 : $jersey);
      convert_undef_zero(\$jersey); # For debugging...
      check_for_null(\$served);
      check_for_null(\$player);

      # Convert type
      parse_penalty_type(\$type);
      check_for_null(\$type);
      check_for_null(\$type_raw);

      # Play
      my $play = '';
      $play = 'Bench ' if $player eq "'0'";
      if ($pims == 2) {
        $play .= 'Minor Penalty';
      } elsif ($pims == 4) {
        $play .= 'Double-Minor';
      } elsif ($pims == 5) {
        $play .= 'Major Penalty';
      } else {
        $play .= 'Penalty';
      }
      $play .= " on {{${team_id}_${player}}}" if $player ne "'0'";
      $play .= " for $type";
      $play .= " (Served by {{${team_id}_$served}})" if $served ne $player;
      $play =~ s/'//g; # Formatting

      print "# $time P:$period ($team_id" . ($bench ? ' (BN)' : '') . " #$jersey, $pims for $type; Raw: $type_raw)\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$period', '00:$time', 'PENALTY', '$team_id', '$play');\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT_PENALTY (season, game_type, game_id, event_id, team_id, jersey, served_by, type, pims) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$team_id', $player, $served, $type, '$pims');\n\n";
    }
  }

  print "# Tidy period penalties\n";
  print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, pims)
  SELECT EVENT.season, EVENT.game_type, EVENT.game_id, EVENT.team_id, EVENT.period, IFNULL(SUM(PENALTY.pims), 0) AS pims
  FROM SPORTS_AHL_GAME_EVENT AS EVENT
  LEFT JOIN SPORTS_AHL_GAME_EVENT_PENALTY AS PENALTY
    ON (PENALTY.season = EVENT.season
    AND PENALTY.game_type = EVENT.game_type
    AND PENALTY.game_id = EVENT.game_id
    AND PENALTY.event_id = EVENT.event_id)
  WHERE EVENT.season = '$season'
  AND   EVENT.game_type = '$game_type'
  AND   EVENT.game_id = '$game_id'
  AND   EVENT.event_type = 'PENALTY'
  GROUP BY EVENT.team_id, EVENT.period
ON DUPLICATE KEY UPDATE pims = VALUES(pims);\n\n";
}

# Return true to pacify the compiler
1;
