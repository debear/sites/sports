#!/usr/bin/perl -w
#
# Misc Game Info (Pre-processing)
#
sub parse_misc_process {
  our ($game_info, %summary);
  # Flag for potential error identification
  print "##\n## Misc Info (Processing)\n##\n##\n\n";
  # Info we want to carry around the script
  our %misc = (
    'status' => {},
  );

  # Game Status
  print "# Game Status\n";
  $misc{'status'}{'status'} = 'F';
  # OT / SO ?
  $raw_status = $$summary{'details'}{'status'};
  $raw_status =~ s/^Unofficial //i;
  $misc{'status'}{'status'} = substr($raw_status, 6)
    if ($raw_status !~ /Final$/) && ($raw_status !~ / Forfeit$/i);
  # Fix: Summary periods may (incorrectly?) store extra info
  @{$$summary{'periods'}} = @{$$summary{'periods'}}[0..2]
    if ($misc{'status'}{'status'} eq 'F') && @{$$summary{'periods'}} > 3;

  # Shots by Period
  print "# Shots by Period\n";
  $misc{'shots'} = { $$game_info{'visitor_id'} => [ 0, 0, 0 ],
                     $$game_info{'home_id'}    => [ 0, 0, 0 ] };
  my $i = 0;
  foreach my $p (@{$$summary{'periods'}}) {
    # Shots
    $misc{'shots'}{$$game_info{'visitor_id'}}[$i] = $$p{'stats'}{'visitingShots'};
    $misc{'shots'}{$$game_info{'home_id'}}[$i] = $$p{'stats'}{'homeShots'};
    $i++;
  }

  # Special Teams
  print "# Special Teams\n";
  $misc{'special_teams'} = { $$game_info{'visitor_id'} => { 'pp' => { 'goals' => $$summary{'visitingTeam'}{'stats'}{'powerPlayGoals'},
                                                                      'opp' => $$summary{'visitingTeam'}{'stats'}{'powerPlayOpportunities'} },
                                                            'pk' => { 'goals' => $$summary{'homeTeam'}{'stats'}{'powerPlayGoals'},
                                                                      'opp' => $$summary{'homeTeam'}{'stats'}{'powerPlayOpportunities'} } },
                             $$game_info{'home_id'}    => { 'pp' => { 'goals' => $$summary{'homeTeam'}{'stats'}{'powerPlayGoals'},
                                                                      'opp' => $$summary{'homeTeam'}{'stats'}{'powerPlayOpportunities'} },
                                                            'pk' => { 'goals' => $$summary{'visitingTeam'}{'stats'}{'powerPlayGoals'},
                                                                      'opp' => $$summary{'visitingTeam'}{'stats'}{'powerPlayOpportunities'} } } };

  # Three Stars
  print "# Three Stars\n";
  @{$misc{'stars_raw'}} = ( );
  foreach my $s (@{$$summary{'mostValuablePlayers'}}) {
    push @{$misc{'stars_raw'}}, { 'team_id' => convert_team_remote_id($season, $$s{'team'}{'id'}), 'jersey' => $$s{'player'}{'info'}{'jerseyNumber'}, 'remote_id' => $$s{'player'}{'info'}{'id'} };
  }

  # Officials
  print "# Officials\n";
  $misc{'officials'} = {};
  my %offic_types = ( 'referees' => 'referee', 'linesmen' => 'linesman' );
  foreach my $k (keys %offic_types) {
    my $offic_type = $offic_types{$k};
    foreach my $o (@{$$summary{$k}}) {
      $misc{'officials'}{$offic_type} = {} if !defined($misc{'officials'}{$offic_type});
      $misc{'officials'}{$offic_type}{$$o{'jerseyNumber'}} = { 'fname' => $$o{'firstName'}, 'sname' => $$o{'lastName'} };
    }
  }

  # Scores
  print "# Scores\n";
  my $visitor_score = $$summary{'visitingTeam'}{'stats'}{'goals'};
  my $home_score = $$summary{'homeTeam'}{'stats'}{'goals'};
  $misc{'scoring'} = { $$game_info{'visitor_id'} => $visitor_score,
                       $$game_info{'home_id'}    => $home_score,
                       'winner'                  => ($visitor_score > $home_score ? $$game_info{'visitor_id'} : ($visitor_score < $home_score ? $$game_info{'home_id'} : '')),
                       'loser'                   => ($visitor_score < $home_score ? $$game_info{'visitor_id'} : ($visitor_score > $home_score ? $$game_info{'home_id'} : ''))  };

  # Goalies of Record
  # - Note: it turns out the jerseyNumber field in the goalie log can be inaccurate, so get the jersey from the player ID and corresponding player
  print "# Goalies of Record\n";
  $misc{'status'}{'goalie_w'} = $misc{'status'}{'goalie_l'} = undef;
  foreach my $key ( 'visitingTeam', 'homeTeam' ) {
    my $goalies = $$summary{$key}{'goalies'};
    foreach my $g (@{$$summary{$key}{'goalieLog'}}) {
      if (defined($$g{'result'})) {
        if ($$g{'result'} =~ m/[WL]$/) {
          my $var = 'goalie_' . lc(substr($$g{'result'}, -1));
          foreach my $goalie (@$goalies) {
            $misc{'status'}{$var} = $$goalie{'info'}{'jerseyNumber'} if $$goalie{'info'}{'id'} eq $$g{'info'}{'id'};
          }
        }
      }
    }
  }
  # Convert Win/Loss -> Home/Visitor
  $misc{'status'}{'goalie_home'} = ($misc{'scoring'}{'winner'} eq $$game_info{'home_id'} ? $misc{'status'}{'goalie_w'} : $misc{'status'}{'goalie_l'});
  $misc{'status'}{'goalie_visitor'} = ($misc{'scoring'}{'winner'} eq $$game_info{'visitor_id'} ? $misc{'status'}{'goalie_w'} : $misc{'status'}{'goalie_l'});

  # Attendance
  print "# Attendance\n";
  $misc{'status'}{'attendance'} = $$summary{'details'}{'attendance'};

  print "\n";
}

#
# Misc Game Info (Display)
#
sub parse_misc_display() {
  our (%config, $game_info, %misc);
  print "##\n## Misc Info (Display)\n##\n##\n\n";

  # Shots by Period
  print "# Shots by Period\n";
  while (my ($team_id, $shots) = each %{$misc{'shots'}}) {
    for (my $i = 0; $i < @$shots; $i++) {
      my $period = $i+1;
      print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, shots) VALUES ('$season', '$game_type', '$game_id', '$team_id', '$period', '$$shots[$i]') ON DUPLICATE KEY UPDATE shots = VALUES(shots);\n";
    }
  }
  print "\n";

  # Special Teams
  print "# Special Teams\n";
  while (my ($team_id, $stats) = each %{$misc{'special_teams'}}) {
    while (my ($type, $type_stats) = each %$stats) {
      print "INSERT INTO SPORTS_AHL_GAME_PP_STATS (season, game_type, game_id, team_id, pp_type, pp_goals, pp_opps) VALUES ('$season', '$game_type', '$game_id', '$team_id', '$type', '$$type_stats{'goals'}', '$$type_stats{'opp'}');\n";
    }
  }
  print "\n";

  # Three Stars
  print "# Three Stars\n";
  my $star_num = 1;
  foreach my $star (@{$misc{'stars_raw'}}) {
    # Convert team_id from their version to ours
    print "INSERT INTO SPORTS_AHL_GAME_THREE_STARS (season, game_type, game_id, star, team_id, jersey) VALUES ('$season', '$game_type', '$game_id', '$star_num', '$$star{'team_id'}', '$$star{'jersey'}');\n";
    $misc{'stars'}{$star_num} = { 'team_id' => $$star{'team_id'}, 'jersey' => $$star{'jersey'} };
    $star_num++;
  }
  print "\n";

  # Officials
  print "# Officials\n";
  foreach my $offic_type (reverse sort keys %{$misc{'officials'}}) {
    foreach my $offic_jersey (sort {$a <=> $b} keys %{$misc{'officials'}{$offic_type}}) {
      print "# $misc{'officials'}{$offic_type}{$offic_jersey}{'fname'} $misc{'officials'}{$offic_type}{$offic_jersey}{'sname'}, #$offic_jersey ($offic_type)\n";
      print "INSERT IGNORE INTO SPORTS_AHL_OFFICIALS (season, official_id, first_name, surname) VALUES ('$season', '$offic_jersey', '" . convert_text($misc{'officials'}{$offic_type}{$offic_jersey}{'fname'}) . "', '" . convert_text($misc{'officials'}{$offic_type}{$offic_jersey}{'sname'}) . "');\n";
      print "INSERT INTO SPORTS_AHL_GAME_OFFICIALS (season, game_type, game_id, official_type, official_id) VALUES ('$season', '$game_type', '$game_id', '$offic_type', '$offic_jersey');\n";
    }
  }
  print "\n";

  # Scores
  print "# Scores\n";
  print "# -> Visitor: ${$misc{'scoring'}}{$$game_info{'visitor_id'}}\n";
  print "# -> Home: ${$misc{'scoring'}}{$$game_info{'home_id'}}\n";

  # Goalies of Record
  print "# Goalies of Record\n";
  my $goalie_home = $misc{'status'}{'goalie_home'};
  my $goalie_visitor = $misc{'status'}{'goalie_visitor'};
  check_for_null(\$goalie_home);
  print "# -> Home: " . (defined($misc{'status'}{'goalie_home'}) ? $misc{'status'}{'goalie_home'} : 'Unknown') . "\n";
  check_for_null(\$goalie_visitor);
  print "# -> Visitor: " . (defined($misc{'status'}{'goalie_visitor'}) ? $misc{'status'}{'goalie_visitor'} : 'Unknown') . "\n";

  # Game Status
  print "# Game Status\n";
  my $status = $misc{'status'}{'status'};
  check_for_null(\$status);
  print "# -> $misc{'status'}{'status'}\n";

  # Attendance
  print "# Attendance\n";
  my $attendance = $misc{'status'}{'attendance'};
  check_for_null(\$attendance);
  print "# -> ";
  if (defined($misc{'status'}{'attendance'})) {
    print $misc{'status'}{'attendance'};
  } else {
    print "Unknown";
  }
  print "\n";

  # Process these updates
  print "UPDATE SPORTS_AHL_SCHEDULE SET home_score = '${$misc{'scoring'}}{$$game_info{'home_id'}}', visitor_score = '${$misc{'scoring'}}{$$game_info{'visitor_id'}}', status = $status, attendance = $attendance, home_goalie = $goalie_home, visitor_goalie = $goalie_visitor WHERE season = '$season' AND game_type = '$game_type' AND game_id = '$game_id';\n\n";
}

# Return true to pacify the compiler
1;
