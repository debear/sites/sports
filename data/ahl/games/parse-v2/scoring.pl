#!/usr/bin/perl -w
#
# Scoring
#
sub parse_scoring {
  our (%config, $game_info, %misc, $summary);
  print "##\n## Scoring\n##\n\n";

  foreach my $p (@{$$summary{'periods'}}) {
    foreach my $g (@{$$p{'goals'}}) {
      # Split the JSON into component parts
      my $period = $$g{'period'};
      $period = $$period{'id'}
        if ref($period) eq 'HASH';
      my $team_id = convert_team_remote_id($season, $$g{'team'}{'id'});
      my $scorer = $$g{'scoredBy'}{'jerseyNumber'};
      my $assist_1 = $$g{'assists'}[0]{'jerseyNumber'}; check_for_null(\$assist_1);
      my $assist_2 = $$g{'assists'}[1]{'jerseyNumber'}; check_for_null(\$assist_2);
      my $time = convert_event_time($$g{'time'});

      # Event processing...
      $misc{'event_id'}++;
      $counter{$team_id}++;

      # Goal type
      my $goal_type = 'EV';
      $goal_type = 'PP' if $$g{'properties'}{'isPowerPlay'};
      $goal_type = 'SH' if $$g{'properties'}{'isShortHanded'};

      # Goalie status
      my $goalie_status;
      $goalie_status = 'EN' if $$g{'properties'}{'isEmptyNet'};
      #$goalie_status = 'WG'; # Can we determine this?
      check_for_null(\$goalie_status);

      # Penalty Shot?
      my $penalty_shot = ($$g{'properties'}{'isPenaltyShot'} ? 1 : 0);

      # Play
      my $play = '';
      $play .= 'Power Play ' if $goal_type eq 'PP';
      $play .= 'Shorthanded ' if $goal_type eq 'SH';
      $play =~ s/ $/, / if $penalty_shot && $play ne ''; # Formatting...
      $play .= 'Penalty Shot ' if $penalty_shot;
      $play .= 'Empty Net ' if $goalie_status eq '\'EN\'';
      $play .= 'Extra Skater ' if $goalie_status eq '\'WG\'';
      $play .= 'Goal, scored by {{' . $team_id . '_' . $scorer . '}}';
      $play .= ' assisted by {{' . $team_id . '_' . $assist_1 . '}}' if $assist_1 ne 'NULL';
      $play .= ' and {{' . $team_id . '_' . $assist_2 . '}}' if $assist_2 ne 'NULL';
      $play =~ s/'(\d+)'/$1/g; # Fix column quoting

      # Output as SQL
      print "# $time, P:$period ($team_id)\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$period', '00:$time', 'GOAL', '$team_id', '$play');\n";
      print "INSERT INTO SPORTS_AHL_GAME_EVENT_GOAL (season, game_type, game_id, event_id, by_team_id, goal_num, scorer, assist_1, assist_2, goal_type, goalie_status, penalty_shot) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$team_id', '$counter{$team_id}', '$scorer', $assist_1, $assist_2, '$goal_type', $goalie_status, '$penalty_shot');\n";

      # Plus/Minus SQL
      my $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
      my %loop = ( $team_id => 'plus_players', $opp_team_id => 'minus_players' ); my $i = 0;
      while (my ($pm_team_id, $loop_key) = each %loop) {
        print "# $loop_key ($pm_team_id)\n";
        my $players = $$g{$loop_key};

        # Skip if no matching players found
        if (!defined($$players[0])) {
          print "#  - Skipping...\n";
          next;
        }

        # Process
        for (my $i = 0; $i < 6; $i++) {
          check_for_null(\$$players[$i]{'jerseyNumber'});
        }
        print "INSERT INTO SPORTS_AHL_GAME_EVENT_ONICE (season, game_type, game_id, event_id, team_id, player_1, player_2, player_3, player_4, player_5, player_6) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$pm_team_id', $$players[0]{'jerseyNumber'}, $$players[1]{'jerseyNumber'}, $$players[2]{'jerseyNumber'}, $$players[3]{'jerseyNumber'}, $$players[4]{'jerseyNumber'}, $$players[5]{'jerseyNumber'});\n";
      }
      print "\n";
    }
  }

  # Tidy ups: Special Team and Period scoring
  my $gw_num = ($misc{'scoring'}{'loser'} ne '' ? $misc{'scoring'}{$misc{'scoring'}{'loser'}} + 1 : -1); # Tied game has no GWG...
  print "# Tidy special teams\n";
  print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, pp_goals, pp_assists, sh_goals, sh_assists, gw_goals)
  SELECT LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id,
         SUM(GOAL.goal_type = 'PP' AND GOAL.scorer = LINEUP.jersey) AS pp_goals,
         SUM(GOAL.goal_type = 'PP' AND (GOAL.assist_1 = LINEUP.jersey OR GOAL.assist_2 = LINEUP.jersey)) AS pp_assists,
         SUM(GOAL.goal_type = 'SH' AND GOAL.scorer = LINEUP.jersey) AS sh_goals,
         SUM(GOAL.goal_type = 'SH' AND (GOAL.assist_1 = LINEUP.jersey OR GOAL.assist_2 = LINEUP.jersey)) AS sh_assists,
         SUM(GOAL.goal_num = '$gw_num' AND GOAL.scorer = LINEUP.jersey) AS gw_goals
  FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
  LEFT JOIN SPORTS_AHL_GAME_EVENT_GOAL AS GOAL
    ON (GOAL.season = LINEUP.season
    AND GOAL.game_type = LINEUP.game_type
    AND GOAL.game_id = LINEUP.game_id
    AND GOAL.by_team_id = LINEUP.team_id)
  WHERE LINEUP.season = '$season'
  AND   LINEUP.game_type = '$game_type'
  AND   LINEUP.game_id = '$game_id'
  AND   LINEUP.pos <> 'G'
  GROUP BY LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id
ON DUPLICATE KEY UPDATE pp_goals = VALUES (pp_goals),
                        pp_assists = VALUES (pp_assists),
                        sh_goals = VALUES (sh_goals),
                        sh_assists = VALUES (sh_assists),
                        gw_goals = VALUES (gw_goals);\n\n";

  print "# Tidy period scoring\n";
  print "INSERT INTO SPORTS_AHL_GAME_PERIODS (season, game_type, game_id, team_id, period, goals)
  SELECT season, game_type, game_id, team_id, period, COUNT(event_id) AS goals
  FROM SPORTS_AHL_GAME_EVENT
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   game_id = '$game_id'
  AND   event_type = 'GOAL'
  GROUP BY team_id, period
ON DUPLICATE KEY UPDATE goals = VALUES(goals);\n\n";
}

# Return true to pacify the compiler
1;
