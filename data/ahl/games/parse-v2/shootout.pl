#!/usr/bin/perl -w
#
# Shootout
#
sub parse_shootout {
  our (%config, $game_info, %misc, $summary);
  print "##\n## Shootout\n##\n";

  # Only continue if the game went to a shootout...
  if (!$$summary{'hasShootout'}) {
    print "# Skipping: Game did not go to a shootout...\n\n";
    return;
  }
  print "\n";

  # Parse shooters
  my %shooters = ( $$game_info{'visitor_id'} => [ ], $$game_info{'home_id'} => [ ] );
  my @loop = ( { 'team_id' => $$game_info{'visitor_id'}, 'shooters' => 'visitingTeamShots' },
               { 'team_id' => $$game_info{'home_id'}, 'shooters' => 'homeTeamShots' } );
  foreach my $s (@loop) {
    foreach my $p (@{$$summary{'shootoutDetails'}{$$s{'shooters'}}}) {
      push @{$shooters{$$s{'team_id'}}}, { 'shooter' => $$p{'shooter'}{'jerseyNumber'}, 'goalie' => $$p{'goalie'}{'jerseyNumber'}, 'goal' => $$p{'isGoal'} };
    }
  }

  # Safety catch, given our subsequent while loop...
  if (!@{$shooters{$$game_info{'visitor_id'}}} || !@{$shooters{$$game_info{'home_id'}}}) {
    print STDERR "Game went to a shootout, but no shooters found.\n";
    return;
  }

  # Now display...
  if (@{$shooters{$$game_info{'visitor_id'}}} < @{$shooters{$$game_info{'home_id'}}}) {
    $team_id = $$game_info{'home_id'};
  } else {
    $team_id = $$game_info{'visitor_id'}; # As we don't know who started first, assume visitor
  }
  while (@{$shooters{$$game_info{'visitor_id'}}} || @{$shooters{$$game_info{'home_id'}}}) {
    my $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});

    # Skip if no rows (avoiding errors...)
    if (!@{$shooters{$team_id}}) {
      $team_id = $opp_team_id;
      next;
    }

    # Get and process convert
    my ($p) = splice(@{$shooters{$team_id}}, 0, 1);
    $misc{'event_id'}++;
    my $shooter = $$p{'shooter'};
    my $goalie = $$p{'goalie'}; check_for_null(\$goalie);
    my $status = ($$p{'goal'} ? 'goal' : 'no_goal');

    # Play
    my $play = "{{${team_id}_$shooter}} ";
    $play .= 'scored' if $status eq 'goal';
    $play .= 'missed' if $status eq 'no_goal';

    # Display
    print "# $team_id #$shooter: $status\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, team_id, play) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', 5, '00:00:00', 'SHOOTOUT', '$team_id', '$play');\n";
    print "INSERT INTO SPORTS_AHL_GAME_EVENT_SHOOTOUT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, status) VALUES ('$season', '$game_type', '$game_id', '$misc{'event_id'}', '$team_id', '$shooter', '$opp_team_id', $goalie, '$status');\n\n";

    # Switch team for next loop...
    $team_id = $opp_team_id;
  }

  # Tidy ups: Player/Goalie summary
  @loop = ( { 'type' => 'shooters', 'tbl' => 'SKATERS', 'agst' => '', 'col' => 'by', 'pos' => '<>' },
            { 'type' => 'goalies', 'tbl' => 'GOALIES', 'agst' => '_against', 'col' => 'on', 'pos' => '=' } );
  foreach my $l (@loop) {
    print "# Tidy $$l{'type'}\n";
    print "INSERT INTO SPORTS_AHL_PLAYERS_GAME_$$l{'tbl'} (season, player_id, game_type, game_id, so_goals$$l{'agst'}, so_shots$$l{'agst'})
  SELECT LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id,
         SUM(SHOOTOUT.$$l{'col'}_jersey = LINEUP.jersey AND SHOOTOUT.status = 'goal') AS so_goals$$l{'agst'},
         SUM(SHOOTOUT.$$l{'col'}_jersey = LINEUP.jersey) AS so_shots$$l{'agst'}
  FROM SPORTS_AHL_GAME_LINEUP AS LINEUP
  JOIN SPORTS_AHL_PLAYERS_GAME_$$l{'tbl'} AS PLAYER
    ON (PLAYER.season = LINEUP.season
    AND PLAYER.game_type = LINEUP.game_type
    AND PLAYER.game_id = LINEUP.game_id
    AND PLAYER.team_id = LINEUP.team_id
    AND PLAYER.player_id = LINEUP.player_id)
  LEFT JOIN SPORTS_AHL_GAME_EVENT_SHOOTOUT AS SHOOTOUT
    ON (SHOOTOUT.season = LINEUP.season
    AND SHOOTOUT.game_type = LINEUP.game_type
    AND SHOOTOUT.game_id = LINEUP.game_id
    AND SHOOTOUT.$$l{'col'}_team_id = LINEUP.team_id)
  WHERE LINEUP.season = '$season'
  AND   LINEUP.game_type = '$game_type'
  AND   LINEUP.game_id = '$game_id'
  AND   LINEUP.pos $$l{'pos'} 'G'
  GROUP BY LINEUP.season, LINEUP.player_id, LINEUP.game_type, LINEUP.game_id
ON DUPLICATE KEY UPDATE so_goals$$l{'agst'} = VALUES(so_goals$$l{'agst'}),
                        so_shots$$l{'agst'} = VALUES(so_shots$$l{'agst'});\n\n";
  }
}

# Return true to pacify the compiler
1;
