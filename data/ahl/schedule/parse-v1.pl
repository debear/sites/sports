#!/usr/bin/perl -w
# Parse the schedule for a season from theahl.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use List::Util qw(max);

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $mode, $dated_file) = parse_args();
$config{'data_dir'} = $config{'base_dir'} . '/_data';
$config{'base_dir'} = get_base_dir();

# Load alternate venue details
my %alt_venues;
my $alt_venues_file = "$config{'data_dir'}/schedule/${season_dir}_alt_venues.csv";
if (-e $alt_venues_file) {
  print "# Loading alternate venues from $alt_venues_file\n";
  my $alt_venues = load_file($alt_venues_file);
  foreach my $alt_venue (split(/\n/, $alt_venues)) {
    # Get details
    my ($date, $home, $visitor, $venue) = split(/,/, $alt_venue);
    # Add to list
    %{$alt_venues{$date}} = ( ) if !defined($alt_venues{$date});
    $alt_venues{$date}{"$home-$visitor"} = $venue;
  }
}

# Determine info we currently have in the database (to keep existing IDs consistent)
my %id_maps = ( 'by_fixture' => {}, 'by_remote' => {}, 'num_prev' => { 'regular' => 0, 'playoff' => 0 } );
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT game_type, game_id, game_num, remote_id, home, visitor
           FROM SPORTS_AHL_SCHEDULE
           WHERE season = ?;';
my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
foreach my $game (@$ret) {
  $id_maps{'num_prev'}{$$game{'game_type'}}++;
  $$game{'fixture'} = $$game{'visitor'} . '@' . $$game{'home'};

  # Game IDs
  $id_maps{'by_fixture'}{$$game{'game_type'}} = ( ) if !defined($id_maps{'by_fixture'}{$$game{'game_type'}});
  $id_maps{'by_fixture'}{$$game{'game_type'}}{$$game{'fixture'}} = ( ) if !defined($id_maps{'by_fixture'}{$$game{'game_type'}}{$$game{'fixture'}});
  $id_maps{'by_fixture'}{$$game{'game_type'}}{$$game{'fixture'}}{$$game{'game_num'}} = $$game{'game_id'};

  # Remote IDs
  $id_maps{'by_remote'}{$$game{'game_id'}} = $$game{'remote_id'} if $$game{'remote_id'};
}

# If we have playoff games, get the pre-determined round codes
my %playoff_codes = ( );
if (grep /^$mode$/, ('playoff', 'both')) {
  $sql = 'SELECT DISTINCT SERIES.round_code, HIGHER.team_id AS higher_seed, LOWER.team_id AS lower_seed
          FROM SPORTS_AHL_PLAYOFF_SERIES AS SERIES
          JOIN SPORTS_AHL_PLAYOFF_SEEDS AS HIGHER
            ON (HIGHER.season = SERIES.season
            AND HIGHER.conf_id = SERIES.higher_conf_id
            AND HIGHER.seed = SERIES.higher_seed)
          JOIN SPORTS_AHL_PLAYOFF_SEEDS AS LOWER
            ON (LOWER.season = SERIES.season
            AND LOWER.conf_id = SERIES.lower_conf_id
            AND LOWER.seed = SERIES.lower_seed)
          WHERE SERIES.season = ?';
  $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
  foreach my $series (@$ret) {
    # Write two versions: H:L and L:H to facilitate search
    $playoff_codes{$$series{'lower_seed'} . ':' . $$series{'higher_seed'}} = $$series{'round_code'};
    $playoff_codes{$$series{'higher_seed'} . ':' . $$series{'lower_seed'}} = $$series{'round_code'};
  }
}

$dbh->disconnect if defined($dbh);

# Get and loop through the list of files
my %rem_map = ();
my @sched_list = ();
foreach my $game_type ('regular', 'playoff') {
  # Ignore?
  next if !grep /^$mode$/, ($game_type, 'both');

  # What remote code do we use?
  my $season_remote = convert_season($season, $game_type);

  # Import
  my $filename = "$config{'base_dir'}$dated_file/$game_type.htm";
  my $html = load_file($filename);

  # Get the list of future games
  my $pattern = '<tr>\s*<td class="ls-schedule ls-gamenum">(\d+)<\/td>\s*<td class="ls-schedule ls-date">\s*[a-z]+, ([a-z]+) (\d+), (\d{4})\s*<\/td>\s*<td class="ls-schedule ls-time">(\d+):(\d+) ([ap]m) ([a-z])[a-z]+<\/td>\s*<td class="ls-schedule ls-visiting">(?:<b>)?<a href=".*?team_id=(\d+)">.*?<\/a>(?:<\/b>)?<\/td>\s*<td class="ls-schedule ls-visiting-score">.*?<\/td>\s*<td class="ls-schedule ls-AT">at&nbsp;&nbsp;<\/td>\s*<td class="ls-schedule ls-home">(?:<b>)?<a href=".*?team_id=(\d+)">.*?<\/a>(?:<\/b>)?<\/td>\s*<td class="ls-schedule ls-home-score">.*?<\/td>\s*<td class="ls-schedule ls-venue">(.*?)<\/td>\s*<td class="ls-schedule ls-ticketlinks">.*?<\/td>\s*<td class="ls-schedule ls-gamelinks">(.*?)<\/td>\s*<td class="ls-schedule ls-gamerecap">\s*<\/td>\s*<\/tr>';

  # Process
  my @matches = ($html =~ m/$pattern/gsi);

  # First pass, determining info and possible game_id's
  my @sched = ();
  my $new_id = 0; # ID of any new game we have to process
  while (my @match = splice(@matches, 0, 12)) {
    # Skip uncompleted / moved games
    next if $match[10] =~ m/Postponed/;

    # Process
    my %game = ('raw' => \@match);

    # Determine home/visitor
    $game{'home'} = convert_team_remote_id($season, $match[9]);
    $game{'visitor'} = convert_team_remote_id($season, $match[8]);

    # Convert times to Eastern
    $match[4] -= 2  if $match[7] eq 'N';
    $match[5] += 30 if $match[7] eq 'N';
    $match[4] += 1  if $match[7] eq 'C';
    $match[4] += 3  if $match[7] eq 'P';

    if ($match[5] >= 60) {
      $match[5] -= 60;
      $match[4]++;
    }

    # Game date / time
    $game{'game_date'} = sprintf('%04d-%02d-%02d', $match[3], $config{'map'}{'months'}{$match[1]}, $match[2]);
    $game{'game_time'} = sprintf('%02d:%02d:00', $match[4] + ($match[6] eq 'pm' && $match[4] < 12 ? 12 : 0), $match[5]);

    # Game Num
    $game{'game_num'} = $match[0];

    # Internal ID
    $game{'fixture'} = $game{'visitor'} . '@' . $game{'home'};
    $game{'game_id'} = $id_maps{'by_fixture'}{$game_type}{$game{'fixture'}}{$game{'game_num'}};
    undef $id_maps{'by_fixture'}{$game_type}{$game{'fixture'}}{$game{'game_num'}};
    $new_id = max($new_id, $game{'game_id'}) if defined($game{'game_id'});
    # Create a playoff Internal ID based on the round if we haven't already
    if (!defined($game{'game_id'}) && $game_type eq 'playoff') {
      # Skip games for future playoff rounds that we haven't determined a round code for
      next if (!defined($playoff_codes{$game{'home'} . ':' . $game{'visitor'}}));
      my $round_code = $playoff_codes{$game{'home'} . ':' . $game{'visitor'}};
      $game{'game_id'} = ($round_code * 10) + $game{'game_num'};
    }

    # Remote ID
    if ($match[11]) {
      my ($rem_id) = ($match[11] =~ m/game_id=(\d+)"/gsi);
      $game{'remote_id'} = $rem_id;
    }

    # Alternate Venue?
    $game{'alt_venue'} = $alt_venues{$game{'game_date'}}{$game{'home'} . '-' . $game{'visitor'}} if defined($alt_venues{$game{'game_date'}}{$game{'home'} . '-' . $game{'visitor'}});

    push @sched, \%game;
  }

  # Second pass, tying together
  foreach my $game (@sched) {
    # Determine the game_id
    my $game_id = $$game{'game_id'};
    # An (unclaimed) previously scheduled game?
    if (!defined($game_id)) {
      my @dates = sort keys %{$id_maps{'by_fixture'}{$game_type}{$$game{'fixture'}}};
      if (@dates) {
        my $gd = shift @dates;
        $game_id = $id_maps{'by_fixture'}{$game_type}{$$game{'fixture'}}{$gd};
        undef $id_maps{'by_fixture'}{$game_type}{$$game{'fixture'}}{$gd};
      }
    }
    # A new game...
    $game_id = (++$new_id) if !defined($game_id);

    # Is there a remote_id?
    my $remote_id;
    if (defined($$game{'remote_id'})) {
      $remote_id = "'$$game{'remote_id'}'";
    } elsif (defined($id_maps{'by_remote'}{$game_id})) {
      $remote_id = "'$id_maps{'by_remote'}{$game_id}'";
    } else {
      $remote_id = 'NULL';
    }

    # Is there an alternate venue?
    if (defined($$game{'alt_venue'})) {
      $$game{'alt_venue'} = "'$$game{'alt_venue'}'";
    } else {
      $$game{'alt_venue'} = 'NULL';
    }

    # Now write as SQL
    push (@sched_list, "'$season', '$game_type', '$game_id', '$$game{'game_num'}', $remote_id, '$$game{'home'}', '$$game{'visitor'}', '$$game{'game_date'}', '$$game{'game_time'}', NULL, NULL, NULL, NULL, $$game{'alt_venue'}");
  }
}

# Skip if no matches
if ( !@sched_list) {
  exit;
}

# Now output as SQL
print "INSERT INTO `SPORTS_AHL_SCHEDULE` (`season`, `game_type`, `game_id`, `game_num`, `remote_id`, `home`, `visitor`, `game_date`, `game_time`, `home_score`, `visitor_score`, `status`, `attendance`, `alt_venue`) VALUES\n(" . join("),\n(", @sched_list) . ")\nON DUPLICATE KEY UPDATE `remote_id` = VALUES(`remote_id`), `game_date` = VALUES(`game_date`), `game_time` = VALUES(`game_time`), `alt_venue` = VALUES(`alt_venue`);\n";
print "ALTER TABLE `SPORTS_AHL_SCHEDULE` ORDER BY `season`, `game_type`, `game_id`;\n";

# If regular season, some extra calcs if creating for the first time
if (grep(/^$mode$/, ('regular', 'both')) && !$id_maps{'num_prev'}{'regular'}) {
  print "\n";

  # Team v Team matchups
  print "# Team v Team matchups\n";
  print "CALL ahl_schedule_matchups('$season');\n\n";

  # Power ranking weeks
  print "# Power rank dates\n";
  print "CALL ahl_power_ranks_dates('$season');\n\n";

  # Initial standings
  print "# Initial standings\n";
  print "CALL ahl_standings_initial('$season');\n";
}

# Return true to pacify the compiler
1;
