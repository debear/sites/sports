#!/usr/bin/perl -w
# Get the schedule for a season from theahl.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $mode, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir();

# Get the vars to pass to the web request
my @requests = ( );
push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

my $file_ext = ($season < 2016 ? 'htm' : 'json') . '.gz';

# Get the list
foreach my $game_type (@requests) {
  # What remote code do we use?
  my $season_remote = convert_season($season, $game_type);

  # Where are we saving this file
  my $sched = "$config{'base_dir'}/$game_type/$dated_file.$file_ext";
  # Skip if already downloaded
  next if -e $sched;

  # Otherwise, form the URL
  my $url;
  if ($season < 2016) {
    $url = "http://theahl.com/stats/schedule.php?view=season&team_id=-1&season_id=$season_remote&home_away=&division_id=";
  } else {
    $url = "https://lscluster.hockeytech.com/feed/index.php?feed=statviewfeed&view=schedule&team=-1&season=$season_remote&month=-1&location=homeaway&key=$config{'app_key'}&client_code=ahl&site_id=1&league_id=4&division_id=-1&lang=en";
  }

  # And download
  print "# Downloading $game_type schedule in $season_dir\n#  From: $url\n#  To: $sched\n";
  download($url, $sched, {'gzip' => 1});
}
