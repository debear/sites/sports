#!/usr/bin/perl -w
# Common code for the schedule downloading and parsing

#
# Convert command line args in to something we can process
#
sub parse_args {
  my $season = ''; my $season_dir = ''; my $mode = ''; my $dated_file = 'initial';
  foreach my $arg (@ARGV) {
    if ($arg eq '--regular') {
      $mode = ($mode eq '' ? 'regular' : 'both');
    } elsif ($arg eq '--playoff') {
      $mode = ($mode eq '' ? 'playoff' : 'both');
    } elsif ($season_dir eq '') {
      $season_dir = $arg;
      $season = substr($season_dir, 0, 4);
    } elsif ($arg =~ /^\d{4}-\d{2}-\d{2}$/ && $dated_file eq 'initial') {
      $dated_file = $arg;
    } else {
      print '# Unknown option "' . $arg . '"' . "\n";
    }
  }

  # Must have at least season and mode
  if ($season eq '' || $mode eq '') {
    print STDERR "Invalid arguments.\n";
    exit;
  }

  return ($season, $season_dir, $mode, $dated_file);
}

#
# Where are the files downloaded
#
sub get_base_dir {
  our (%config, $season_dir);
  return sprintf('%s/_data/%s/schedules', $config{'base_dir'}, $season_dir);
}

#
# Load a file
#
sub load_file {
  my ($filename) = @_;
  my $open_cmd = (substr($filename, -3) eq '.gz' ? "gzip -dc $filename |" : "<$filename");

  open FILE, $open_cmd;
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ();

  return $contents;
}

# Return true to pacify the compiler
1;
