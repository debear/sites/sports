#!/usr/bin/perl -w
# Download and import transactions from theahl.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'transactions';
my %logs = get_log_def();

# Guesstimate the season
my @time = localtime();
my $date = time2date(time());
my $season_guess = ($time[4] > 7 ? 1900 : 1899) + $time[5];

# Unless passed in a special argument, we're to ask the user how to operate
my $season;
my @mode = ();

# Determine for ourselves
if ($config{'is_unattended'}) {
  # Season is our guessed season
  $season = $season_guess;
  # Get the crossover point from the database as to whether we're regular season or playoffs
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $info = $dbh->selectrow_hashref('SELECT MAX(game_date) AS max_reg FROM SPORTS_AHL_SCHEDULE WHERE season = ? AND game_type = "regular";', { Slice => {} }, $season);
  $dbh->disconnect if defined($dbh);
  if (!defined($info)) {
    print STDERR "Error: Unable to run for $season because schedule has not yet been loaded?\n";
    exit;
  }
  # Up to this date is regular season, anything after it is playoffs
  push @mode, (!defined($$info{'max_reg'}) || $date le $$info{'max_reg'} ? 'regular' : 'playoff');

# Passed in
} elsif (($season) = grep(/^20\d{2}$/, @ARGV)) {
  push @mode, 'regular' if grep(/^--regular$/, @ARGV);
  push @mode, 'playoff' if grep(/^--playoff$/, @ARGV);
  if (!@mode) {
    print STDERR "Error: No valid modes passed in.\n";
    exit;
  }

# Get from the user
} else {
  # Confirm the season to use
  $season = question_user("Please enter a season in YYYY format [$season_guess]", '\d{4}', $season_guess);

  # Which game types?
  my $q = question_user("Regular season? [Yn]", '[yn]', 'y');
  push @mode, 'regular' if $q eq 'y';
  $q = question_user("Playoffs? [yN]", '[yn]', 'n');
  push @mode, 'playoff' if $q eq 'y';

  # Ensure something has been entered
  if (!@mode) {
    print STDERR "Error: Please select at least one of regular season or playoff.\n";
    exit;
  }
  print "\n";
}

# Tidy internal variables after being determined above
my $season_dir = season_dir($season);
my $m = join(' ', @mode);

# Inform user
print "Acquiring data from $season, $m game types\n";
print 'Run started at ' . `date` . "\n";

# Idenfity the log file
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

# Loop through regular/playoffs separately
foreach my $mode (@mode) {
  # Determine date argument
  my $date_arg = '';
  my $date_ext = '';
  $date_ext = '.htm.gz' if $season < 2016;
  $date_ext = '.json.gz' if $season == 2016;
  $date_arg = $date
    if (-e "$config{'base_dir'}/_data/$season_dir/transactions/$mode/initial$date_ext");
  print '=> ' . ucfirst($mode) . " ($date_arg):\n";

  # Download, if we haven't done so already (handled in-script)
  print '  -> Download: ';
  command("$config{'base_dir'}/transactions/download.pl $season_dir $mode $date_arg",
          "$config{'log_dir'}/$logs{'trans_d'}.log",
          "$config{'log_dir'}/$logs{'trans_d'}.err");
  done(9);

  # Parse (unless download only...)
  if (!download_only()) {
    print '  -> Parse: ';
    command("$config{'base_dir'}/transactions/parse.pl $season_dir $mode $date_arg",
            "$config{'log_dir'}/$logs{'trans_p'}.sql",
            "$config{'log_dir'}/$logs{'trans_p'}.err");
    done(12);
  }
}

# If only downloading, go no further
end_script() if download_only();

# Import Transactions
print '=> Import Transactions: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'trans_p'}.sql",
        "$config{'log_dir'}/$logs{'trans_i'}.log",
        "$config{'log_dir'}/$logs{'trans_i'}.err");
done(0);

# Next group of scripts are the first section of post-processing
$config{'exit_status'} = 'post-process-1';

# Process
print '=> New Players: ';
command("$config{'base_dir'}/transactions/import.pl $season_dir",
        "$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_p'}.err");
done(8);

# Import Processed
print '=> Import Players: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_i'}.log",
        "$config{'log_dir'}/$logs{'players_i'}.err");
done(5);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'trans_d'   => '01_trans_download',
    'trans_p'   => '02_trans_parse',
    'trans_i'   => '03_trans_import',
    'players_p' => '04_players_process',
    'players_i' => '05_players_import',
  );
}
