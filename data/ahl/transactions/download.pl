#!/usr/bin/perl -w
# Get transaction lists from theahl.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use File::Path;

# Validate the arguments
#  - there needs to be at least two: a season and a season_type
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 2 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get transaction script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $season_type, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Downloading transactions for '$season' // '$date'\n#\n";

# Create our data directory
mkpath($config{'data_dir'})
  if ! -e $config{'data_dir'};

my $rem_season = convert_season($season, $season_type);
my $ext = 'htm';
my $url;
if ($season < 2016) {
  $url = "http://theahl.com/stats/transactions.php?showFull=1&f_season_id=$rem_season&f_team_id=0";
} else {
  $url = "https://lscluster.hockeytech.com/feed/index.php?feed=statviewfeed&view=transactions&team_id=-1&season_id=$rem_season&site_id=1&key=$config{'app_key'}&client_code=ahl&league_id=&lang=en";
  $ext = 'json';
}

# First pass: main download
my $per_page = 200;
my $local = download_file($url . ($season >= 2017 ? "&first=0&limit=$per_page" : ''), $date, $ext, $season >= 2017 ? 1 : undef);

# 2017 onwards, the results could be across multiple pages
if ($season >= 2017) {
  my $content = load_file($local);
  # Get the total number of results
  my ($total_results) = ($content =~ m/{"num_results":"?(\d+)"?}/);
  # Pagination...
  if ($total_results > $per_page) {
    # Initial reduction of the total for our first page
    $total_results -= $per_page;
    my $page = 1;
    # Then loop through remaining pages
    do {
      $page++; # Move on to the next page
      $local = download_file($url . "&first=" . (($page - 1) * $per_page) . "&limit=$per_page", $date, $ext, $page);
      $total_results -= $per_page;
    } while ($total_results > 0);
  }
}

# Download method
sub download_file {
  my ($url, $date, $ext, $page) = @_;
  my $local = "$config{'data_dir'}/$date" . (defined($page) ? sprintf('/pg%02d', $page) : '') . ".$ext.gz";

  # Display and process
  print "## From $url\n## To $local\n";
  download($url, $local, {'skip-today' => 1, 'gzip' => 1});
  print "\n";

  return $local;
}
