#!/usr/bin/perl -w
# Determine which version of the script to run and run it

# Date calcs
my @time = localtime();
my $date = sprintf('%04d-%02d', $time[5]+1900, $time[4]+1);
my $v;
if ($date lt '2016-08') {
  $v = 1;
} elsif ($date lt '2017-09') {
  $v = 2;
} else {
  $v = 3;
}

my $script = $0;
$script =~ s/\.pl/-v$v.pl/;
require $script;
