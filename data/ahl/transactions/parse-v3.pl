#!/usr/bin/perl -w
# Loop through and parse AHL transactions

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be at least two: a season and a season_type
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 2 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the transaction parsing script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $season_type, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_data/fixes/transactions.pl');
require $config;
my $dir = "$config{'data_dir'}/$date";

print "#\n# Parsing transactions for '$season'-'$season_type' // '$date'\n#\n\n";

# Any bespoke transactions to be added?
my $custom_file = "$dir/custom.sql";
print "# Custom Transactions ($custom_file)\n" . load_file($custom_file) . "#\n# Transaction Logs\n#\n\n" if -e $custom_file;

# Loop through all the files in this run
my @files = glob("$dir/*.json.gz");
foreach my $file (@files) {
  print "# Loading File: $file\n\n";

  # Load the file, convert to JSON
  my $contents = load_file($file);
  $contents =~ s/^\(//; $contents =~ s/\)$//; # As it's designed for JSONP, convert to regular JSON

  # Zero'th pass: markup fixes...
  $contents =~ s/"transaction":"(Retired)"/"transaction":"DEL $1"/gsi; # Retired players (DEL)
  $contents =~ s/"transaction":"(Suspended by Team)"/"transaction":"DEL $1"/gsi; # Suspended players (DEL)
  $contents =~ s/"transaction":"(Suspension lifted)"/"transaction":"ADD $1"/gsi; # Suspended players (ADD)

  # Get the players
  my %matches = ( );
  # First pass, get the players.... considering there could be multiple instances of a player per day
  my $pattern = '\{"prop":\{"player_name":\{"playerLink":"(\d+)"[^}]*},"team_city":\{"teamLink":"(\d+)"}},"row":\{"transaction_date":"(\d{4}-\d{2}-\d{2})","player_name":"([^"]+)","team_city":"([^"]+)","transaction_type":"(ADD|DEL)","transaction":"([^"]+)"}}';
  my @matches = ($contents =~ m/$pattern/gsi);
  while (my @match = splice(@matches, 0, 7)) {
    # Some validation...
    next if ($season == 2009 && ($match[2] eq '2008-11-27' || $match[2] eq '2009-02-19' || $match[2] eq '2009-03-16'));

    # Continue
    %{$matches{$match[2]}} = () if !defined($matches{$match[2]});

    # Add or update... but only update if the detail is greater in this subsequent row
    if (!defined($matches{$match[2]}{$match[0]})) {
      $matches{$match[2]}{$match[0]} = \@match;
    } else {
      $matches{$match[2]}{$match[0]}[6] = $match[6]
        if $matches{$match[2]}{$match[0]}[6] =~ m/^\s*$/;
    }
  }

  # Second pass, produce the SQL...
  foreach my $date_raw (sort keys %matches) {
    foreach my $remote_id (sort keys %{$matches{$date_raw}}) {
      my @match = @{$matches{$date_raw}{$remote_id}};

      # Date fix?
      my $date = date_fix($season, $date_raw, $remote_id, $match[3]);
      # Team fix?
      $match[1] = team_fix($date, $remote_id, $match[3]) if $match[1] eq '';
      $match[1] = convert_team_remote_id($season, $match[1]);

      # Convert detail
      trim(\$match[6]); $match[6] = convert_text($match[6]);
      $match[6] = ($match[6] ne '' ? "'$match[6]'" : 'NULL');

      # Transaction type...
      $match[7]  = lc $match[5]; # Database version is all lower case
      $match[8] = ucfirst $match[7]; # But title case our display...

      # Determine the player's position
      ($match[9]) = ($match[3] =~ m/\(([^\)]+)\)/gsi);

      my $player_var = "\@player_$date$remote_id";
      $player_var =~ s/-//g;

      print "# $match[3] :: $match[4] // $match[8] // $match[6]\n";
      print "SELECT player_id INTO $player_var FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = '$remote_id';\n";
      print "INSERT INTO SPORTS_AHL_TRANSACTIONS (season, the_date, remote_id, player_id, team_id, pos, type, detail)
               VALUES ('$season', '$date', '$remote_id', $player_var, '$match[1]', '$match[9]', '$match[7]', $match[6])
             ON DUPLICATE KEY UPDATE type = IF(type = VALUES(type), VALUES(type), 'both');\n\n";
    }
  }
}

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_AHL_TRANSACTIONS ORDER BY season, the_date, remote_id;\n";

# Return true to pacify the compiler
1;
