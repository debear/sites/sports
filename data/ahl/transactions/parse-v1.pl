#!/usr/bin/perl -w
# Loop through and parse AHL transactions

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be at least two: a season and a season_type
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 2 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the transaction parsing script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $season_type, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
my $file = "$config{'data_dir'}/$date.htm";

print "#\n# Parsing transactions for '$season'-'$season_type' // '$date'\n#\n\n";

# Any bespoke transactions to be added?
my $custom_file = $file; $custom_file =~ s/\.htm$/\.sql/;
print "# Custom Transactions ($custom_file)\n" . load_file($custom_file) . "#\n# Transaction Logs\n#\n\n" if -e $custom_file;

# Load the file
my $contents = load_file($file);

# Get the players
my %matches = ( );
# Zero'th pass: markup fixes...
$contents =~ s/<td class="dataRow" nowrap width="50%">(Retired)<\/td>/<td class="dataRow" nowrap width="50%">DEL $1<\/td>/gsi; # Retired players (DEL)
$contents =~ s/<td class="dataRow" nowrap width="50%">(Suspended by Team)<\/td>/<td class="dataRow" nowrap width="50%">DEL $1<\/td>/gsi; # Suspended players (DEL)
$contents =~ s/<td class="dataRow" nowrap width="50%">(Suspension lifted)<\/td>/<td class="dataRow" nowrap width="50%">ADD $1<\/td>/gsi; # Suspended players (ADD)
# First pass, get the players.... considering there could be multiple instances of a player per day
my $pattern = '<tr>\s*<td class="dataRow" nowrap>(\d+)\/(\d+)\/(\d+)<\/td>\s*<td class="dataRow" nowrap><a href="\/stats\/player\.php\?id=(\d+)">(.*?) \((.{0,2})\)<\/a><\/td>\s*<td class="dataRow" nowrap>(.*?)<\/td>\s*<td class="dataRow" nowrap width="50%">(ADD|DEL)(?:ed|eted)?(.*?)<\/td>\s*<\/tr>';
my @matches = ($contents =~ m/$pattern/gsi);
while (my @match = splice(@matches, 0, 9)) {
  my $d = "$match[2]-$match[0]-$match[1]";
  # Some validation...
  next if ($season == 2009 && ($d eq '2008-11-27' || $d eq '2009-02-19' || $d eq '2009-03-16'));

  # Continue
  %{$matches{$d}} = () if !defined($matches{$d});

  # Add or update... but only update if the detail is greater in this subsequent row
  if (!defined($matches{$d}{$match[3]})) {
    $matches{$d}{$match[3]} = \@match;
  } else {
    $matches{$d}{$match[3]}[8] = $match[8]
      if $matches{$d}{$match[3]}[8] =~ m/^\s*$/;
  }
}

# Second pass, produce the SQL...
foreach my $date (sort keys %matches) {
  foreach my $remote_id (sort keys %{$matches{$date}}) {
    my @match = @{$matches{$date}{$remote_id}};

    # Date fix?
    ($match[2], $match[0], $match[1], $date) = date_fix($season, $date, $remote_id) if $date eq '0000-00-00';
    # Team fix?
    ($match[6]) = team_fix($season, $date, $remote_id) if $match[6] eq '';

    # Convert team name
    trim(\$match[6]); $match[6] = convert_text($match[6]);

    # Convert detail
    trim(\$match[8]); $match[8] = convert_text($match[8]);
    $match[8] = ($match[8] ne '' ? "'$match[8]'" : 'NULL');

    # Transaction type...
    $match[9]  = lc $match[7]; # Database version is all lower case
    $match[10] = ucfirst $match[9]; # But title case our display...

    my $team_var = "\@team_$match[2]$match[0]$match[1]_$remote_id";
    my $player_var = "\@player_$match[2]$match[0]$match[1]$remote_id";

    print "# $match[4] ($match[5]) ($remote_id) :: $match[6] // $match[10] // $match[8]\n";
    print "SELECT DIVN.team_id INTO $team_var FROM SPORTS_AHL_TEAMS_GROUPINGS AS DIVN JOIN SPORTS_AHL_TEAMS AS TEAM ON (TEAM.team_id = DIVN.team_id AND IFNULL(TEAM.city_abbrev, TEAM.city) = '$match[6]') WHERE $season BETWEEN DIVN.season_from AND IFNULL(DIVN.season_to, 2099);\n";
    print "SELECT player_id INTO $player_var FROM SPORTS_AHL_PLAYERS_IMPORT WHERE remote_id = '$remote_id';\n";
    print "INSERT INTO SPORTS_AHL_TRANSACTIONS (season, the_date, remote_id, player_id, team_id, pos, type, detail) VALUES ('$season', '$date', '$remote_id', $player_var, $team_var, '$match[5]', '$match[9]', $match[8]) ON DUPLICATE KEY UPDATE type = IF(type = VALUES(type), VALUES(type), 'both');\n\n";
  }
}

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_AHL_TRANSACTIONS ORDER BY season, the_date, remote_id;\n";

##
## Useful functions
##
# Correct blank dates
sub date_fix {
  my ($season, $date, $remote_id) = @_;

  if ($season == 2007 && $remote_id == 1984) {
    $date = '2007-11-18';
  } elsif ($season == 2009 && $remote_id == 2011) {
    $date = '2009-10-01';
  } elsif ($season == 2012 && $remote_id == 4945) {
    $date = '2013-04-21';
  }

  my @match = split('-', $date);
  return ($match[0], $match[1], $match[2], $date);
}

# Correct blank teams
sub team_fix {
  my ($season, $date, $remote_id) = @_;

  if ($season == 2009 && $date eq '2009-10-01' && $remote_id == 3215) {
    return 'Rockford';

  } elsif ($season == 2012 && $date eq '2012-10-11' &&
            (   $remote_id == 570  || $remote_id == 1183 || $remote_id == 3540
             || $remote_id == 3607 || $remote_id == 3754 || $remote_id == 3760
             || $remote_id == 3898 || $remote_id == 4008 || $remote_id == 4020
             || $remote_id == 4290 || $remote_id == 4302 || $remote_id == 4356
             || $remote_id == 4529 || $remote_id == 4659 || $remote_id == 4662)) {
    return 'Adirondack';

  } elsif ($season == 2012 && $date eq '2012-10-11' && $remote_id == 4465) {
    return 'Texas';
  }

  # Unknown...?
  return '';
}

# Return true to pacify the compiler
1;
