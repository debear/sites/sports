#!/usr/bin/perl -w
# Process transaction AHL data

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use DateTime;

# Validate the arguments
#  - there needs to be at least one: a season
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the transaction player importing script.\n";
  exit 98;
}

# Declare variables
our ($season_dir) = @ARGV;

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Importing new players from transactions for '$season'\n#\n";

# Determine the players to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $list = $dbh->selectall_arrayref('SELECT remote_id, GROUP_CONCAT(DISTINCT team_id SEPARATOR "/") AS team_ids FROM SPORTS_AHL_TRANSACTIONS WHERE season = ? AND IFNULL(player_id, 0) = 0 AND processed = 0 GROUP BY remote_id ORDER BY remote_id;', { Slice => {} }, $season);
$dbh->disconnect if defined($dbh);
foreach my $trans (@$list) {
  print "# Importing new player, $$trans{'remote_id'}, $$trans{'team_ids'}\n";
  print `$config{'base_dir'}/players/_import.pl $$trans{'remote_id'}`;
}

