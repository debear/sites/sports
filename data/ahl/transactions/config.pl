#!/usr/bin/perl -w
# Perl config file

our $season_dir; our $date;
our $season = substr($season_dir, 0, 4);
$config{'data_dir'} = "$config{'base_dir'}/_data/$season_dir/transactions/$season_type"
  if defined($season_type);

#
# Load a file
#
sub load_file {
  my ($filename) = @_;
  my $open_cmd = (substr($filename, -3) eq '.gz' ? "gzip -dc $filename |" : "<$filename");

  open FILE, $open_cmd;
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ();

  return $contents;
}

# Return true to pacify the compiler
1;
