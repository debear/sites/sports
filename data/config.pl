#!/usr/bin/perl -w
# Global perl config and methods for data scripts

use File::Path;
use Dir::Self;
use open qw( :std :encoding(UTF-8) );

our %config;
$config{'domain'} = 'debear.uk';
$config{'base_dir'} = abs_path(__DIR__);
$config{'htdocs_dir'} = abs_path(__DIR__ . '/../htdocs');
$config{'cdn_dir'} = abs_path(__DIR__ . '/../../cdn/htdocs/sports');

# Database details
$config{'db_admin'} = 'debearco_admin';
$config{'db_common'} = 'debearco_common';
$config{'db_name'} = 'debearco_sports';
$config{'db_user'} = $config{'db_name'};
# Password
my $db_pass_file = $config{'base_dir'}; $db_pass_file =~ s@/sites/.+$@/etc/passwd/web/db@;
$config{'db_pass'} = do { local(@ARGV, $/) = $db_pass_file; <> }; chomp($config{'db_pass'});
$config{'db_pass'} = decode_base64($config{'db_pass'});

# Exit statuses
%{$config{'exit_codes'}} = (
  'okay' => 0,
  # Pre-requisite checks
  'failed-prereq' => 98,
  'failed-prereq-silent' => 97, # Same as above, but does not trigger a warning
  # Generic error
  'unknown' => 99,
  # Pre-processing
  'pre-flight' => 10,
  # Core part of script
  'core' => 20,
  # Arbitrarily grouped sections of post-processing
  'post-process-1' => 31,
  'post-process-2' => 32,
  'post-process-3' => 33,
  'post-process-4' => 34,
  'post-process-5' => 35,
);

# Load global methods
$config{'global_rel'} = '../../'; # Relative path to the _global directory
my $global_dir = abs_path($config{'base_dir'} . '/_global');
for my $inc ('benchmark.pl', 'flow.pl', 'log.pl', 'download.pl', 'strings.pl') {
  require "$global_dir/$inc";
}

# Return true to pacify the compiler
1;
