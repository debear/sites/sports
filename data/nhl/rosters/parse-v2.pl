#!/usr/bin/perl -w
# Loop through and parse team rosters, and any new players found

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use JSON;

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though:  date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;
my $reimport = grep(/\-\-reimport/, @ARGV);

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

print "#\n# Parsing rosters for '$season' // '$date' ('$date_roster') // '$reimport'\n#\n";

# Get the list of teams
my $err = 0;
my @list = glob "$config{'data_dir'}/*.json.gz" or $err = 1;
if ($err) {
  print STDERR "No roster files found?\n";
  exit 10;
}

# Clear a previous run
print "# Clearing previous instance\n";
print "DELETE FROM SPORTS_NHL_TEAMS_ROSTERS WHERE season = '$season' AND the_date = '$date_roster';\n\n";

# Prepare our database queries
my $sel_sth = $dbh->prepare('SELECT player_id FROM SPORTS_NHL_PLAYERS_IMPORT WHERE remote_id = ?;');
my $new_sth = $dbh->prepare('SELECT MAX(player_id) + 1 AS new_player_id FROM SPORTS_NHL_PLAYERS;');
my $ins_sth = $dbh->prepare('INSERT INTO SPORTS_NHL_PLAYERS_IMPORT (player_id, remote_id) VALUES (?, ?);');
my $imp_sth = $dbh->prepare('UPDATE SPORTS_NHL_PLAYERS_IMPORT SET profile_imported = NULL WHERE player_id = ?;');
my $ply_sth = $dbh->prepare('INSERT INTO SPORTS_NHL_PLAYERS (player_id, first_name, surname) VALUES (?, ?, ?);');

# Loop through the teams
foreach my $file (@list) {
  # Get the team_id from the filename
  my ($team_id) = ($file =~ m/$config{'data_dir'}\/([A-Z]+)\.json\.gz/si);
  print "#\n# $team_id ($file)\n#\n\n";

  # Load the file
  my $roster;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);
  $roster = decode_json($roster);

  foreach my $player (@{$$roster{'teams'}[0]{'roster'}{'roster'}}) {
    my $remote_id = $$player{'person'}{'id'};
    my $fname = $$player{'person'}{'firstName'};
    my $sname = $$player{'person'}{'lastName'};
    my $jersey = $$player{'person'}{'primaryNumber'};
    my $pos = $$player{'person'}{'primaryPosition'}{'code'};
    my $pos_full = $$player{'person'}{'primaryPosition'}{'abbreviation'};
    my $roster_status = $$player{'person'}{'rosterStatus'};
    my $roster_active = $$player{'person'}{'active'};

    # Get the player_id from the database
    $sel_sth->execute($remote_id);
    my ($player_id) = $sel_sth->fetchrow_array;

    # If no existing player record found, store the fields we have from the roster page and create
    if (!defined($player_id)) {
      # Get the new player ID
      $new_sth->execute;
      ($player_id) = $new_sth->fetchrow_array;

      # Store the record in the import table
      $ins_sth->execute($player_id, $remote_id);

      # Create the player record
      $ply_sth->execute($player_id, $fname, $sname);

    # If re-importing rosters (i.e., including existing players), remove the flag in the IMPORT table so we re-sync non-NHL data (as well as mugshots, etc)
    } elsif ($reimport) {
      $imp_sth->execute($player_id);
    }

    # Status?
    my $status = 'active';
    if ($roster_status eq 'I') {
      $status = 'ir';
    } elsif ($roster_status eq 'N' || !$roster_active) {
      $status = 'na';
    }

    # Jersey correction
    $jersey = 0 if (!defined($jersey) || !$jersey);

    # Output the SQL
    print "# $fname $sname ($pos_full, $remote_id)\n";
    print "INSERT INTO SPORTS_NHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status) VALUES ('$season', '$date_roster', '$team_id', '$player_id', '$jersey', '$pos', '$status');\n\n";
  }
}

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_NHL_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;\n";

# Disconnect from the database
undef $sel_sth if defined($sel_sth);
undef $new_sth if defined($new_sth);
undef $ins_sth if defined($ins_sth);
undef $imp_sth if defined($imp_sth);
undef $ply_sth if defined($ply_sth);
$dbh->disconnect if defined($dbh);
