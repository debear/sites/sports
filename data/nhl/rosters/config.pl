#!/usr/bin/perl -w
# Perl config file

our $season_dir; our $date;
our $season = substr($season_dir, 0, 4);
$config{'data_dir'} = "$config{'base_dir'}/_data/$season_dir/rosters/$date";

# Determine the date we're processing
sub determine_db_date {
  my ($dbh, $season, $date) = @_;
  my $sth;
  if ($date eq 'initial') {
    $sth = $dbh->prepare('SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) AS first_date FROM SPORTS_NHL_SCHEDULE WHERE season = ?;');
    $sth->execute($season);
  } else {
    $sth = $dbh->prepare('SELECT DATE_SUB(?, INTERVAL 1 DAY) AS roster_date;');
    $sth->execute($date);
  }
  ($date) = $sth->fetchrow_array;
  return $date;
}

# Return true to pacify the compiler
1;
