#!/usr/bin/perl -w
# Loop through and parse team rosters, and any new players found

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use DBI;

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though:  date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;
my $reimport = grep(/\-\-reimport/, @ARGV);

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing rosters for '$season' // '$date' // '$reimport'\n#\n";

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Now we've set the data_dir, change $game_type and $date to be the versions we want to store in the database
if ($date eq 'initial') {
  my $sth = $dbh->prepare('SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) FROM SPORTS_NHL_SCHEDULE WHERE season = ?;');
  $sth->execute($season);
  ($date) = $sth->fetchrow_array;
  undef $sth if defined($sth);
}

# Get the list of teams
my $err = 0;
my @list = glob "$config{'data_dir'}/*.htm" or $err = 1;
if ($err) {
  print STDERR "No roster files found?\n";
  exit 10;
}

# Clear a previous run
print "# Clearing previous instance\n";
print "DELETE FROM SPORTS_NHL_TEAMS_ROSTERS WHERE season = '$season' AND the_date = '$date';\n\n";

# Prepare our database queries
my $sel_sth = $dbh->prepare('SELECT player_id FROM SPORTS_NHL_PLAYERS_IMPORT WHERE remote_id = ?;');
my $new_sth = $dbh->prepare('SELECT MAX(player_id) + 1 AS new_player_id FROM SPORTS_NHL_PLAYERS;');
my $ins_sth = $dbh->prepare('INSERT INTO SPORTS_NHL_PLAYERS_IMPORT (player_id, remote_id) VALUES (?, ?);');
my $imp_sth = $dbh->prepare('UPDATE SPORTS_NHL_PLAYERS_IMPORT SET profile_imported = NULL WHERE player_id = ?;');
my $ply_sth = $dbh->prepare('INSERT INTO SPORTS_NHL_PLAYERS (player_id, first_name, surname) VALUES (?, ?, ?);');

# Loop through the teams
foreach my $file (@list) {
  # Get the team_id from the filename
  my ($team_id) = ($file =~ m/$config{'data_dir'}\/([A-Z]+)\.htm/si);
  print "#\n# $team_id ($file)\n#\n\n";

  # Load the file
  my $roster;
  open FILE, "<$file";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);

  # Section of players (F, D, G)
  my $pattern = '<div class="moduleHeader" style="margin: [^"]*">(.*?)<\/div>\s*<table class="data" cellpadding="0" cellspacing="0">(.*?)<\/table>';
  my @sect = ($roster =~ m/$pattern/gsi);

  while (my ($pos_sect, $list) = splice(@sect, 0, 2)) {
    print "# $pos_sect\n\n";
    $pos_sect = substr($pos_sect, 0, 1);

    # Get the list of players out (**jersey number required**)
    my $pattern = '<tr[^>]*>\s*<td[^>]*>\s*<span class="sweaterNo">(\d+)<\/span>\s*<\/td>\s*<td[^>]*>\s*<nobr><a href="\/club\/player\.htm\?id=(\d+)">(.*?)<\/a>\s*.*?<span style="font-weight: bold;">"?(.?)"?</span>\s*.*?\s*<\/td>\s*(<td[^>]*>[A-Z]<\/td>)?.*?\s*<\/tr>';
    my @matches = ($list =~ m/$pattern/gsi);

    # Display each player, creating where required
    while (my ($jersey, $remote_id, $name, $capt, $pos) = splice(@matches, 0, 5)) {
      # Process the captain status in to a database version
      $capt = (defined($capt) && $capt ne '' ? "'$capt'" : 'NULL');

      # Get their position
      if (defined($pos)) {
        # From the table
        ($pos) = ($pos =~ m/>([A-Z])</gsi);

      } elsif ($pos_sect ne 'F') {
        # From the table group
        $pos = $pos_sect;

      } else {
        # Fallback to the player page
        my $profile = download("http://www.nhl.com/ice/player.htm?id=$remote_id");
        $pattern = '<span style="color: #666;">(.)[^<]+<\/span><\/div>';
        ($pos) = ($profile =~ m/$pattern/gsi);
      }

      # Get the player_id from the database
      $sel_sth->execute($remote_id);
      my ($player_id) = $sel_sth->fetchrow_array;

      # If no existing player record found, store the fields we have from the roster page and create
      if (!defined($player_id)) {
        # Get the new player ID
        $new_sth->execute;
        ($player_id) = $new_sth->fetchrow_array;

        # Store the record in the import table
        $ins_sth->execute($player_id, $remote_id);

        # Create the player record
        my @name = split(' ', $name);
        my $fname = convert_text(shift @name);
        my $sname = convert_text(join(' ', @name));
        $ply_sth->execute($player_id, $fname, $sname);

      # If re-importing rosters (i.e., including existing players), remove the flag in the IMPORT table so we re-sync non-NHL data (as well as mugshots, etc)
      } elsif ($reimport) {
        $imp_sth->execute($player_id);
      }

      # Output the SQL
      print "# $name, #$jersey ($remote_id)\n";
      print "INSERT INTO SPORTS_NHL_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, capt_status, player_status) VALUES ('$season', '$date', '$team_id', '$player_id', '$jersey', '$pos', $capt, 'active');\n";
    }
    print "\n";
  }
}

print "#\n# Tidy\n#\n";
print "ALTER TABLE SPORTS_NHL_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;\n";

# Disconnect from the database
undef $sel_sth if defined($sel_sth);
undef $new_sth if defined($new_sth);
undef $ins_sth if defined($ins_sth);
undef $imp_sth if defined($imp_sth);
undef $ply_sth if defined($ply_sth);
$dbh->disconnect if defined($dbh);
