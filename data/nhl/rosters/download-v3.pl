#!/usr/bin/perl -w
# Get player info for players on current NHL.com rosters

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use File::Path;
use DBI;

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though:  date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season_dir, $date) = @ARGV;
our $season = substr($season_dir, 0, 4);
my $season_full = sprintf('%04d%04d', $season, $season + 1);
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Downloading rosters for '$season' // '$date'\n#\n";

# Create our data directory
mkpath($config{'data_dir'})
  if ! -e $config{'data_dir'};

# Get the teams from the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sth = $dbh->prepare('SELECT TEAM.team_id, TEAM.city, TEAM.franchise FROM SPORTS_NHL_TEAMS_GROUPINGS AS DIVN JOIN SPORTS_NHL_TEAMS AS TEAM ON (TEAM.team_id = DIVN.team_id) WHERE ? BETWEEN DIVN.season_from AND IFNULL(DIVN.season_to, 2099) ORDER BY DIVN.team_id;');
$sth->execute($season);
my $teams = $sth->fetchall_arrayref;

while (my $team = splice(@$teams, 0, 1)) {
  my ($team_id, $team_city, $team_franchise) = @$team;
  my $team_id_rem = convert_team_id_to_remote($team_id);
  print "# $team_city $team_franchise ($team_id, $team_id_rem)\n";

  my $url = "https://api-web.nhle.com/v1/roster/${team_id_rem}/${season_full}";
  my $local = "$config{'data_dir'}/$team_id.json.gz";
  print "## From: $url\n## To:   $local\n";
  download($url, $local, {'skip-today' => 1, 'gzip' => 1});
  print "\n";
}

# Disconnect from the database
undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);
