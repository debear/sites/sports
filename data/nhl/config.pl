#!/usr/bin/perl -w
# Perl config file with all DB, dir info, etc

use MIME::Base64;
use HTML::Entities;
use Encode;
use DBI;

our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

$config{'base_dir'} .= '/nhl'; # Append the NHL dir
$config{'data_dir'} = $config{'base_dir'} . '/_data';
$config{'statsapi_league_id'} = 133; # league_id of the NHL in the statsapi integration
$config{'statsapi_team_id_map'} = {
  # Active
   1 => 'NJ',   2 => 'NYI',  3 => 'NYR',  4 => 'PHI',  5 => 'PIT',
   6 => 'BOS',  7 => 'BUF',  8 => 'MTL',  9 => 'OTT', 10 => 'TOR',
  12 => 'CAR', 13 => 'FLA', 14 => 'TB',  15 => 'WSH', 16 => 'CHI',
  17 => 'DET', 18 => 'NSH', 19 => 'STL', 20 => 'CGY', 21 => 'COL',
  22 => 'EDM', 23 => 'VAN', 24 => 'ANA', 25 => 'DAL', 26 => 'LA',
  28 => 'SJ',  29 => 'CLB', 30 => 'MIN', 52 => 'WPG', 54 => 'VGK',
  55 => 'SEA', 59 => 'UTA',
  # Former franchises
  27 => 'PHO', 11 => 'ATL', 33 => 'WIN', 53 => 'ARI',
};
%{$config{'statsapi_team_id_inv'}} = reverse %{$config{'statsapi_team_id_map'}};
$config{'statsapi_team_id_rem'} = {
  # Active
   1 => 'NJD',  2 => 'NYI',  3 => 'NYR',  4 => 'PHI',  5 => 'PIT',
   6 => 'BOS',  7 => 'BUF',  8 => 'MTL',  9 => 'OTT', 10 => 'TOR',
  12 => 'CAR', 13 => 'FLA', 14 => 'TBL', 15 => 'WSH', 16 => 'CHI',
  17 => 'DET', 18 => 'NSH', 19 => 'STL', 20 => 'CGY', 21 => 'COL',
  22 => 'EDM', 23 => 'VAN', 24 => 'ANA', 25 => 'DAL', 26 => 'LAK',
  28 => 'SJS', 29 => 'CBJ', 30 => 'MIN', 52 => 'WPG', 54 => 'VGK',
  55 => 'SEA', 59 => 'UTA',
  # Former franchises
  27 => 'PHO', 11 => 'ATL', 33 => 'WIN', 53 => 'ARI',
};
$config{'timezone'} = 'US/Eastern';
# Start processing lineups half an hour before the opening face-off (to allow time for information to be posted)
$config{'lineups_time_to_start'} = 30;
# The number of games in our recent streak calcs
$config{'num_recent'} = 10;

# Check as to whether we parse/import the data or just download it
sub download_only {
  return defined($config{'download_only'}) && $config{'download_only'};
}

# Get info for a given game
sub get_game_info {
  my @db_args = @_;

  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT SPORTS_NHL_SCHEDULE.game_id,
                    LOWER(VISITOR.franchise) AS visitor,
                    LOWER(VISITOR.city) AS visitor_city,
                    SPORTS_NHL_SCHEDULE.visitor AS visitor_id,
                    LOWER(HOME.franchise) AS home,
                    LOWER(HOME.city) AS home_city,
                    SPORTS_NHL_SCHEDULE.home AS home_id
             FROM SPORTS_NHL_SCHEDULE
             JOIN SPORTS_NHL_TEAMS AS VISITOR
               ON (VISITOR.team_id = SPORTS_NHL_SCHEDULE.visitor)
             JOIN SPORTS_NHL_TEAMS AS HOME
               ON (HOME.team_id = SPORTS_NHL_SCHEDULE.home)
             WHERE SPORTS_NHL_SCHEDULE.season = ?
             AND   SPORTS_NHL_SCHEDULE.game_type = ?
             AND   SPORTS_NHL_SCHEDULE.game_id = ?;';
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, @db_args[0..2]);
  $dbh->disconnect if defined($dbh);
  return $$ret[0];
}

#
# Convert a single year in to the dir data is stored
#
sub season_dir {
  return sprintf('%04d-%02d', $_[0], ($_[0] + 1) % 100);
}

#
# Convert a game_type in to an NHL.com game code
#
sub convert_game_type {
  my ($game_type) = @_;
  return ($game_type eq 'regular' ? '02' : '03');
}

#
# Convert an ID to a summary of the playoff series
#
sub playoff_summary {
  my ($season, $game_id) = @_;
  my $series;

  if ($game_id =~ /^4/) {
    $series = 'SCF';
  } elsif ($season == 2020) {
    $series = 'ED' if $game_id =~ /^1[12]/ || $game_id =~ /^21/;
    $series = 'CD' if $game_id =~ /^1[34]/ || $game_id =~ /^22/;
    $series = 'WD' if $game_id =~ /^1[56]/ || $game_id =~ /^23/;
    $series = 'ND' if !defined($series);
    $series .= 'SF' if $game_id =~ /^1/;
    $series .= 'F' if $game_id =~ /^2/;
    # Round 3 is not Conference-level
    $series = 'SCSF' if $game_id =~ /^3/;
  } else {
    $series = 'EC' if $game_id =~ /^1[1-4]/ || $game_id =~ /^2[1-2]/ || $game_id =~ /^31/;
    $series = 'WC' if !defined($series);
    $series .= 'QF' if $game_id =~ /^1/;
    $series .= 'SF' if $game_id =~ /^2/;
    $series .= 'F' if $game_id =~ /^3/;
  }

  return $series . ' Game ' . substr($game_id, -1);
}

#
# Return NHL.com domain maps
#
sub get_team_domain_map {
  my ($season) = @_;
  my %teams = ( 'devils' => 'NJ', 'islanders' => 'NYI', 'rangers' => 'NYR', 'flyers' => 'PHI', 'penguins' => 'PIT', 'bruins' => 'BOS', 'sabres' => 'BUF', 'canadiens' => 'MTL', 'senators' => 'OTT', 'mapleleafs' => 'TOR', 'thrashers' => 'ATL', 'hurricanes' => 'CAR', 'panthers' => 'FLA', 'lightning' => 'TB', 'jets' => 'WPG', 'capitals' => 'WSH', 'blackhawks' => 'CHI', 'bluejackets' => 'CLB', 'redwings' => 'DET', 'predators' => 'NSH', 'blues' => 'STL', 'flames' => 'CGY', 'avalanche' => 'COL', 'oilers' => 'EDM', 'wild' => 'MIN', 'canucks' => 'VAN', 'ducks' => 'ANA', 'stars' => 'DAL', 'kings' => 'LA', 'coyotes' => 'ARI', 'sharks' => 'SJ', 'goldenknights' => 'VGK', 'kraken' => 'SEA', 'hockeyclub' => 'UTA' );

  # Before 2014/15, the Coyotes were known as the Phoenix Coyotes
  $teams{'coyotes'} = 'PHO' if $season < 2014;

  return \%teams;
}

#
# Convert a domain name to a team_id
#
sub convert_team_domain {
  my ($season, $domain) = @_;
  my $domains = get_team_domain_map($season);

  if (defined($$domains{$domain})) {
    return $$domains{$domain};
  } else {
    return $domain;
  }
}

#
# Convert a team_id from NHL.com's to ours
#
sub convert_team_id {
  my ($team_id) = @_;
  my %team_ids = (
    'AFM' => 'ATF', 'CBJ' => 'CLB', 'CLR' => 'COR', 'KCS' => 'KC',
    'LAK' => 'LA', 'L.A' => 'LA', 'NJD' => 'NJ', 'N.J' => 'NJ', 'OAK' => 'OSE',
    'PHX' => 'PHO', 'SJS' => 'SJ', 'S.J' => 'SJ', 'TBL' => 'TB', 'T.B' => 'TB',
  );

  # If there is a mapping, use that, otherwise use what we were given
  if (defined($team_ids{$team_id})) {
    return $team_ids{$team_id};
  } else {
    return $team_id;
  }
}

#
# Convert a numeric team_id from NHL.com's to ours
#
sub convert_numeric_team_id {
  my ($team_id) = @_;
  return ${$config{'statsapi_team_id_map'}}{$team_id};
}

sub convert_team_id_to_numeric {
  my ($team_id) = @_;
  return ${$config{'statsapi_team_id_inv'}}{$team_id};
}

sub convert_numeric_remote_id {
  my ($team_id) = @_;
  return ${$config{'statsapi_team_id_rem'}}{$team_id};
}

sub convert_team_id_to_remote {
  my ($team_id) = @_;
  return convert_numeric_remote_id(convert_team_id_to_numeric($team_id));
}

# Return true to pacify the compiler
1;
