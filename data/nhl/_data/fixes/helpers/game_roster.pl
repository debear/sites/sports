#!/usr/bin/perl -w
# Helper methods for gamebook fixes

our %rosters; # JSON per-team file
our $rosters; # HTML game file
our $game_info;

# Override the player's jersey within the team roster
sub fix_roster_jersey {
  my ($team_id, $player, $remote_id, $new, $old) = @_;
  # Determine the hash key for this team, but skip if they are not playing
  my $type = ($$game_info{'home_id'} eq $team_id ? 'home' : ($$game_info{'visitor_id'} eq $team_id ? 'away' : undef));
  return if !defined($type);
  # Now do the processing, if the player is found
  return if $rosters !~ /<td[^>]+>$old<\/td>\s+<td[^>]+>[^<]+<\/td>\s+<td[^>]+>$player<\/td>/i;
  $rosters =~ s/(<td[^>]+>)$old(<\/td>\s+<td[^>]+>[^<]+<\/td>\s+<td[^>]+>$player<\/td>)/$1$new$2/i;
  print "# Fixing roster sweaterNumber for $player (Remote: $remote_id) from '$old' to '$new'\n";
}

# Override the player's jersey within the game lineups
sub fix_lineup_jersey {
  my ($team_id, $player, $remote_id, $new, $old) = @_;
  # Determine the hash key for this team, but skip if they are not playing
  my $type = ($$game_info{'home_id'} eq $team_id ? 'home' : ($$game_info{'visitor_id'} eq $team_id ? 'away' : undef));
  return if !defined($type);
  # Now do the processing
  foreach my $cat (keys %{$rosters{$type}}) {
    foreach my $p (@{$rosters{$type}{$cat}}) {
      next if $$p{'id'} != $remote_id || $$p{'sweaterNumber'} != $old;
      print "# Fixing lineup sweaterNumber for $player (Remote: $remote_id) from '$old' to '$new'\n";
      $$p{'sweaterNumber'} = $new;
    }
  }
}

# Return true to satisfy the parser
return 1;
