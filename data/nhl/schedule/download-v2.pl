#!/usr/bin/perl -w
# Get the schedule for a season from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $season_full, $mode, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir();

# Get the vars to pass to the web request
my @requests = ( );
push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# Determine date ranges to scan
my %dates = ();
my $dates_file = "$config{'data_dir'}/schedule/${season_dir}_dates.csv";
open FILE, "<$dates_file";
my @dates = <FILE>;
close FILE;
foreach my $line (@dates) {
  chomp($line);
  my $c = substr($line, 7, 1); # Split char is the first one after the data column
  my ($t, $s, $f) = split(/$c/, $line);
  $dates{$t} = { 'start' => $s, 'finish' => $f };
}
@dates = ( ); # Homegrown garbage collect...

# Get the list
foreach my $game_type (@requests) {
  my $sched = "$config{'base_dir'}/$game_type/$dated_file.json.gz";

  # Skip if already downloaded
  next if -e $sched;

  # Form the URL and download
  my %d = %{$dates{$game_type}};
  my $url = "https://statsapi.web.nhl.com/api/v1/schedule?site=en_nhl&startDate=$d{'start'}&endDate=$d{'finish'}";
  download($url, $sched, {'gzip' => 1});
}
