#!/usr/bin/perl -w
# Parse the schedule for a season from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $season_full, $mode, $dated_file) = parse_args();
$config{'data_dir'} = $config{'base_dir'} . '/_data';
$config{'base_dir'} = get_base_dir();

# Conversion hashes
my $teams = get_team_domain_map($season);
my %months = ( 'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12 );

# Load in alternate venue info
my %alt_venues = ( );
my $alt_file = "$config{'data_dir'}/schedule/${season_dir}_alt_venues.csv";
if ( -e $alt_file ) {
  open FILE, "<$alt_file";
  my @alt = <FILE>;
  close FILE;

  foreach my $line (@alt) {
    chomp($line);
    my $c = substr($line, 10, 1); # Split char is the first one after the data column
    my ($d, $v, $h, $a) = split(/$c/, $line);
    $alt_venues{"$d-$v-$h"} = convert_text($a);
  }

  @alt = ( ); # Homegrown garbage collect...
}

# Get the list of Game IDs from our pre-downloaded recaps, as they're not referred to in the schedule file nor ordered sequentially
my %reg = ( );
if ($season >= 2011 && grep /^$mode$/, ('regular', 'both')) {
  my $num_games = num_games($season);
  for (my $i = 1; $i <= $num_games; $i++) {
    my $i_fmt = sprintf('%04d', $i);
    my $filename = "$config{'base_dir'}/regular/$i_fmt.htm";

    # Skip if no file
    next if !-e $filename;

    # Skip if empty file
    my @stat = stat $filename;
    next if $stat[7] < 1024;

    open FILE, "<$filename";
    my @html = <FILE>;
    close FILE;
    my $html = join('', @html);
    @html = ();

    # Game date
    my $pattern = 'var startDateOverride = jQuery.datepicker.parseDate\(\'mm\/dd\/yy\', \'(\d{2})\/(\d{2})\/(\d{4})\'\);';
    my @date = ($html =~ m/$pattern/gsi);
    my $date_key = "$date[2]-$date[0]-$date[1]";
    $reg{$date_key} = { } if (!defined($reg{$date_key}));

    # Opponents
    my $team_key;
    if ($season <= 2012) {
      $pattern = '"hometeam":{[^}]+"vhost":"([^"]+)"[^}]+}.*?"awayteam":{[^}]+"vhost":"([^"]+)"[^}]+}' if $season == 2011;
      $pattern = 'pageTrackerNHL._trackPageview\("nhl\/(.*?) @ ([^\/]+)\/' if $season == 2012;
      my @teams = ($html =~ m/$pattern/gsi);
      $teams[0] =~ s/ //g; $teams[1] =~ s/ //g;
      $team_key = $$teams{$teams[1]} . '@' . $$teams{$teams[0]} if $season == 2011;
      $team_key = $$teams{$teams[0]} . '@' . $$teams{$teams[1]} if $season == 2012;

    } else {
      $pattern = 'game_string: "([^@]+) @ ([^"]+)"';
      my @teams = ($html =~ m/$pattern/gsi);
      $team_key = convert_team_id($teams[0]) . '@' . convert_team_id($teams[1]);
    }
    $reg{$date_key}{$team_key} = $i;
  }
}

# Get and loop through the list of files
my @sched_list = ();
foreach my $game_type ('regular', 'playoff') {
  # Ignore?
  next if !grep /^$mode$/, ($game_type, 'both');

  # Import
  my $filename = "$config{'base_dir'}/$game_type$dated_file.htm";
  next if !-e $filename;

  open FILE, "<$filename";
  my @html = <FILE>;
  close FILE;
  my $html = join('', @html);
  @html = ();

  # Get the list of future games
  my $pattern;

  if ($season < 2011 && ($game_type eq 'regular' || $season != 2009)) {
    $pattern = '<div class="skedDataRow date">[^<]+(\w{3}) (\d{1,2}), (\d{4})[^<]+<\/div>.*?<a href="http://([^.]+).nhl.com">.*?<a href="http://([^.]+).nhl.com">.*?<a[^>]* id="sked-[23]-(\d+)-h"[^>]*>.*?<div class="skedDataRow time">(.*?)<\/div>\s*<div class="skedDataRow [\w]*network">';

  } else {
    $pattern = '<div style="display:block;" class="skedStartDateSite">[^<]*?(\w{3}) (\d{1,2}), (\d{4})[^<]*?<\/div>.*?<!-- Away --><a[^>]*?onclick="loadTeamSpotlight\(jQuery\(this\)\);".*?rel="(\w+)".*?>.*?<!-- Home --><a[^>]*?onclick="loadTeamSpotlight\(jQuery\(this\)\);".*?rel="(\w+)".*?>.*?<!-- Time --><div style="display:block;" class="skedStartTimeEST">(.*?)<\/div>(.*?)<\/tr>';
  }
  my @matches = ($html =~ m/$pattern/gsi);

  while (my @match = splice(@matches, 0, 7)) {
    my $other_info = $match[6];

    # Adapt a few fields
    # Teams
    $match[3] = ($season < 2011 && ($game_type eq 'regular' || $season != 2009) ? convert_team_domain($season, $match[3]) : convert_team_id($match[3]));
    $match[4] = ($season < 2011 && ($game_type eq 'regular' || $season != 2009) ? convert_team_domain($season, $match[4]) : convert_team_id($match[4]));
    # Game Time
    my $time_index = ($season < 2011 && ($game_type eq 'regular' || $season != 2009) ? 6 : 5);
    if ($match[$time_index] =~ /TBD/) {
      $match[6] = '19'; $match[7] = '00';
    } else {
      ($match[6], $match[7], $match[8]) = ($match[$time_index] =~ m/\s*(\d{1,2}):(\d{2})\s+([AP])M\s+ET\s*/si);
      $match[6] += 12 if ($match[6] < 12 && $match[8] eq 'P');
    }
    my $time_key = sprintf('%02d:%02d:00', $match[6], $match[7]);
    # Date
    $match[0] = $months{$match[0]};
    my $date_key = sprintf('%04d-%02d-%02d', $match[2], $match[0], $match[1]);
    my $team_key = "$match[3]\@$match[4]";

    # Game ID
    if ($season < 2011 && ($game_type eq 'regular' || $season != 2009)) {
      $match[9] = $match[5];
    } elsif ($game_type eq 'regular') {
      $match[9] = $reg{$date_key}{$team_key};
    } else {
      ($match[9]) = ($other_info =~ m/\/(?:ice|en)\/recap(?:\.htm)?\?id=\d{4}03(\d+)/si);
    }

    # Alternate venue?
    if (defined($alt_venues{"$date_key-$match[3]-$match[4]"})) {
      $match[10] = "'" . $alt_venues{"$date_key-$match[3]-$match[4]"} . "'";
    } else {
      $match[10] = 'NULL';
    }

    # Now write as SQL
    push (@sched_list, "'$season', '$game_type', '$match[9]', '$match[4]', '$match[3]', '$date_key', '$time_key', NULL, NULL, NULL, NULL, $match[10]");
  }
}

# Skip if no matches
if ( !@sched_list) {
  exit;
}

# Now output as SQL
print "INSERT INTO `SPORTS_NHL_SCHEDULE` (`season`, `game_type`, `game_id`, `home`, `visitor`, `game_date`, `game_time`, `home_score`, `visitor_score`, `status`, `attendance`, `alt_venue`) VALUES\n(" . join("),\n(", @sched_list) . ")\nON DUPLICATE KEY UPDATE `home` = VALUES(`home`), `visitor` = VALUES(`visitor`), `game_date` = VALUES(`game_date`), `game_time` = VALUES(`game_time`), `alt_venue` = VALUES(`alt_venue`);\n";
print "ALTER TABLE `SPORTS_NHL_SCHEDULE` ORDER BY `season`, `game_type`, `game_id`;\n";

# If regular season, some extra info
if (grep /^$mode$/, ('regular', 'both')) {
  print "\n";

  # Team v Team matchups
  print "# Team v Team matchups\n";
  print "CALL nhl_schedule_matchups('$season');\n\n";

  # Power ranking weeks
  print "# Power rank dates\n";
  print "CALL nhl_power_ranks_dates('$season');\n\n";

  # Initial standings
  print "# Initial standings\n";
  print "CALL nhl_standings_initial('$season');\n";
}
