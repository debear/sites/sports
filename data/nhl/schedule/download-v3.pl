#!/usr/bin/perl -w
# Get the schedule for a season from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $season_full, $mode, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir();

# Get the vars to pass to the web request
my @requests = ( );
push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# Determine date ranges to scan
my %dates = ();
my $dates_file = "$config{'data_dir'}/schedule/${season_dir}_dates.csv";
open FILE, "<$dates_file";
my @dates = <FILE>;
close FILE;
foreach my $line (@dates) {
  chomp($line);
  my $c = substr($line, 7, 1); # Split char is the first one after the data column
  my ($t, $s, $f) = split(/$c/, $line);
  $dates{$t} = { 'start' => $s, 'finish' => $f };
}
@dates = ( ); # Homegrown garbage collect...

# Get the list
foreach my $game_type (@requests) {
  my $dir = "$config{'base_dir'}/$game_type/$dated_file";
  next if -e $dir; # Skip if already downloaded
  mkdir $dir;

  # Determine the date range to process
  my $date_from = ($dated_file ne 'initial' && $dated_file gt $dates{$game_type}{'start'} ? $dated_file : $dates{$game_type}{'start'});
  my $date_to = $dates{$game_type}{'finish'};

  # Download each page
  print "$game_type // $date_from > $date_to\n";
  my $date = $date_from;
  my $finished = 0;
  for (my $pg = 0; defined($date) && $pg <= 35; $pg++) {
    # Get the paged schedule
    my $url = "https://api-web.nhle.com/v1/schedule/$date";
    my $local = sprintf('%s/pg%02d.json.gz', $dir, $pg);
    print "# Page $pg - From: $url To: $local\n";
    download($url, $local, {'skip-today' => 1, 'gzip' => 1});

    # Determine the next page to load (if possible)
    my $sched = load_file($local);
    $date = defined($$sched{'nextStartDate'}) ? $$sched{'nextStartDate'} : undef;
  }
  # If the catch-all was triggered, consider this an error
  if (defined($date)) {
    print STDERR "Schedule download catch-all triggered - failed download?\n";
  }
}
