#!/usr/bin/perl -w
# Get the schedule for a season from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $season_full, $mode, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir();

# Get the vars to pass to the web request
my @requests = ( );
push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# URL args
my @time = localtime();
my $season_diff = ($time[4] > 7 ? 1900 : 1899) + $time[5] - $season;

# Get extra info for regular season games
if ($season >= 2011 && grep /^$mode$/, ('regular', 'both')) {
  my $num_games = num_games($season);
  for (my $i = 1; $i <= $num_games; $i++) {
    my $i_fmt = sprintf('%04d', $i);
    my $recap = "$config{'base_dir'}/regular/$i_fmt.htm";

    # Skip if we've already downloaded this file
    next if -e $recap;

    # Get the (recap) file we can parse for the
    download("http://www.nhl.com/gamecenter/en/recap?id=${season}02${i_fmt}", $recap);
  }
}

# Get the list
foreach my $game_type (@requests) {
  my $sched = "$config{'base_dir'}/$game_type$dated_file.htm";

  # Skip if already downloaded
  next if -e $sched;

  # Otherwise, form the URL
  my $sel = substr(convert_game_type($game_type), 1);
  my $url;

  # Up to 2010/11 season
  if ($season < 2011) {
    $url = "http://www.nhl.com/ice/app?formids=PropertySelection%2CPropertySelection_0%2CPropertySelection_1%2CPropertySelection_2%2CPropertySelection_3&component=%24SimpleForm&page=schedulebyseason&service=direct&submitmode=&submitname=&PropertySelection=$season_diff&PropertySelection_0=$sel&PropertySelection_1=0&PropertySelection_2=0&PropertySelection_3=0";

  # From 2011/12 season onwards - no longers gives the game ID in the HTML
  } else {
    $url = "http://www.nhl.com/ice/schedulebyseason.htm?season=$season_full&gameType=$sel&team=&network=&venue=";
  }

  # And download
  download($url, $sched);
}
