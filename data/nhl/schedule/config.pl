#!/usr/bin/perl -w
# Common code for the schedule downloading and parsing

use JSON;

#
# Convert command line args in to something we can process
#
sub parse_args {
  my $season = ''; my $season_dir = ''; my $season_full = ''; my $mode = ''; my $dated_file = 'initial';
  foreach my $arg (@ARGV) {
    if ($arg eq '--regular') {
      $mode = ($mode eq '' ? 'regular' : 'both');
    } elsif ($arg eq '--playoff') {
      $mode = ($mode eq '' ? 'playoff' : 'both');
    } elsif ($season_dir eq '') {
      $season_dir = $arg;
      $season = substr($season_dir, 0, 4);
      $season_full = sprintf('%04d%04d', $season, $season + 1);
    } elsif ($arg =~ /^\d{4}-\d{2}-\d{2}$/ && $dated_file eq 'initial') {
      $dated_file = $arg;
    } else {
      print '# Unknown option "' . $arg . '"' . "\n";
    }
  }

  return ($season, $season_dir, $season_full, $mode, $dated_file);
}

#
# Where are the files downloaded
#
sub get_base_dir {
  our (%config, $season_dir);
  return sprintf('%s/_data/%s/schedules', $config{'base_dir'}, $season_dir);
}

#
# Identify number of games to be played in a season
#
sub num_games {
  my ($season) = @_;
  return $season != 2012 ? 1230 : 720;
}

#
# Load a file
#
sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $content = join('', @contents);
  @contents = ( );
  return decode_json($content);
}

# Return true to pacify the compiler
1;
