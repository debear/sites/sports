#!/usr/bin/perl -w
# Parse the schedule for a season from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Date::Manip;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $season_full, $mode, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir();

# If a (previously imported) game is postponed or cancelled, we process it slightly differently
my %ppd_list = ();
my %cnc_list = ();

# Get and loop through the list of files
my @sched_list = ();
foreach my $game_type ('regular', 'playoff') {
  # Ignore?
  next if !grep /^$mode$/, ($game_type, 'both');
  my $mode_flag = ($mode eq 'regular' ? 2 : 3);

  # Loop through all the files in this run
  foreach my $file (glob("$config{'base_dir'}/$game_type/$dated_file/*.json.gz")) {
    print "# Processing File: $file\n";
    my $schedule = load_file($file);

    foreach my $days (@{$$schedule{'gameWeek'}}) {
      foreach my $game (@{$$days{'games'}}) {
        # Skip complete games, or those from the opposite type that may have slipped in to our date range
        next if $$game{'gameType'} ne $mode_flag || ($$game{'gameState'} !~ /^(FUT)$/i);
        my $game_id = int(substr($$game{'id'}, 6));

        # Future games only, skipping uncompleted / moved games
        if ($$game{'gameScheduleState'} =~ m/^(PPD|SUSP)$/i) {
          # Postponed games are a special exception that we'll post-parse later
          $ppd_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"} = 1;
          next;
        }
        if ($$game{'gameScheduleState'} =~ m/^(CNCL)$/i) {
          # Cancelled games are another special exception that we'll post-parse later
          $cnc_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"} = 1;
          next;
        }

        # In some instances, we may have a rescheduled game that was previously stated as PPD or CNCL
        delete $ppd_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"}
          if defined($ppd_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"});
        delete $cnc_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"}
          if defined($cnc_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"});

        # Teams
        my $home = convert_numeric_team_id($$game{'homeTeam'}{'id'});
        my $visitor = convert_numeric_team_id($$game{'awayTeam'}{'id'});

        # Determine game time
        my $date_tz = Date_ConvTZ($$game{'startTimeUTC'}, 'UTC', 'US/Eastern');
        my $time = substr($date_tz, 8);

        # Alternate venue?
        my $alt_venue = ($$game{'neutralSite'} ? '"' . convert_text($$game{'venue'}{'default'}) . '"' : 'NULL');

        # Store
        push (@sched_list, "'$season', '$game_type', '$game_id', '$home', '$visitor', '$$days{'date'}', '$time', NULL, NULL, NULL, NULL, $alt_venue");
      }
    }
  }
}

# Store the parsed schedule
if (@sched_list) {
  print "\n";
  print "INSERT INTO `SPORTS_NHL_SCHEDULE` (`season`, `game_type`, `game_id`, `home`, `visitor`, `game_date`, `game_time`, `home_score`, `visitor_score`, `status`, `attendance`, `alt_venue`) VALUES\n  (" . join("),\n  (", @sched_list) . ")\nON DUPLICATE KEY UPDATE `home` = VALUES(`home`), `visitor` = VALUES(`visitor`), `game_date` = VALUES(`game_date`), `game_time` = VALUES(`game_time`), `status` = VALUES(`status`), `alt_venue` = VALUES(`alt_venue`);\n";
  print "UPDATE `SPORTS_NHL_SCHEDULE` SET status = 'PPD' WHERE `season` = '$season' AND ((" . join(') OR (', keys %ppd_list) . ")) AND status IS NULL;\n" if %ppd_list;
  print "UPDATE `SPORTS_NHL_SCHEDULE` SET status = 'CNC' WHERE `season` = '$season' AND ((" . join(') OR (', keys %cnc_list) . ")) AND status IS NULL;\n" if %cnc_list;
  print "ALTER TABLE `SPORTS_NHL_SCHEDULE` ORDER BY `season`, `game_type`, `game_id`;\n";
}

# Schedule dates
print "\n# Schedule dates\n";
print "CALL nhl_schedule_dates('$season');\n";

# If regular season, some extra calcs if creating for the first time
if (grep(/^$mode$/, ('regular', 'both')) && $dated_file eq 'initial') {
  print "\n";

  # Team v Team matchups
  print "# Team v Team matchups\n";
  print "CALL nhl_schedule_matchups('$season');\n\n";

  # Power ranking weeks
  print "# Power rank dates\n";
  print "CALL nhl_power_ranks_dates('$season');\n\n";

  # Initial standings
  print "# Initial standings\n";
  print "CALL nhl_standings_initial('$season');\n";
}
