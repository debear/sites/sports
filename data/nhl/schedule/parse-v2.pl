#!/usr/bin/perl -w
# Parse the schedule for a season from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Date::Manip;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $season_dir, $season_full, $mode, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir();

# Load in alternate venue info
my %alt_venues = ( );
my $alt_file = "$config{'data_dir'}/schedule/${season_dir}_alt_venues.csv";
if ( -e $alt_file ) {
  open FILE, "<$alt_file";
  my @alt = <FILE>;
  close FILE;

  foreach my $line (@alt) {
    chomp($line);
    my $c = substr($line, 10, 1); # Split char is the first one after the data column
    my ($d, $v, $h, $a) = split(/$c/, $line);
    $alt_venues{"$d-$v-$h"} = convert_text($a);
  }

  @alt = ( ); # Homegrown garbage collect...
}

# Get and loop through the list of files
my @sched_list = ();
my %ppd_list= ();
my $sched_cmpl = 0;
foreach my $game_type ('regular', 'playoff') {
  # Ignore?
  next if !grep /^$mode$/, ($game_type, 'both');
  my $mode_char = uc substr($game_type, 0, 1);

  # Import
  my $filename = "$config{'base_dir'}/$game_type/$dated_file.json.gz";
  next if !-e $filename;

  open FILE, "gzip -dc $filename |";
  my @html = <FILE>;
  close FILE;
  my $html = join('', @html);
  my %schedule = %{decode_json($html)};
  @html = ();

  foreach my $dates (@{$schedule{'dates'}}) {
    foreach my $game (@{$$dates{'games'}}) {
      # Skip opposite games that may have slipped in to our date range
      next if $$game{'gameType'} ne $mode_char;
      my $game_id = int(substr($$game{'gamePk'}, 6));

      # Future games only, skipping uncompleted / moved games
      if ($$game{'status'}{'detailedState'} =~ m/^(Postponed|Suspended)$/i) {
        # Postponed games are a special exception that we'll post-parse later
        $ppd_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"} = 1;
        next;
      }
      if ($$game{'status'}{'detailedState'} =~ /^(Final|Forfeit)$/i) {
        $sched_cmpl++;
        next;
      }

      # In some instances, we may have a rescheduled game that was previously stated as PPD
      delete $ppd_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"}
        if defined($ppd_list{"`game_type` = '$game_type' AND `game_id` = '$game_id'"});

      # Teams
      my $home = convert_numeric_team_id($$game{'teams'}{'home'}{'team'}{'id'});
      my $visitor = convert_numeric_team_id($$game{'teams'}{'away'}{'team'}{'id'});

      # Determine game time
      my $date_tz = Date_ConvTZ($$game{'gameDate'}, 'UTC', 'US/Eastern');
      my $time = substr($date_tz, 8);

      # Alternate venue?
      my $alt_venue;
      if (defined($alt_venues{"$$dates{'date'}-$visitor-$home"})) {
        $alt_venue = "'" . $alt_venues{"$$dates{'date'}-$visitor-$home"} . "'";
      } else {
        $alt_venue = 'NULL';
      }

      # Store
      push (@sched_list, "'$season', '$game_type', '$game_id', '$home', '$visitor', '$$dates{'date'}', '$time', NULL, NULL, NULL, NULL, $alt_venue");
    }
  }
}

# Store the parsed schedule
if (@sched_list) {
  print "INSERT INTO `SPORTS_NHL_SCHEDULE` (`season`, `game_type`, `game_id`, `home`, `visitor`, `game_date`, `game_time`, `home_score`, `visitor_score`, `status`, `attendance`, `alt_venue`) VALUES\n  (" . join("),\n  (", @sched_list) . ")\nON DUPLICATE KEY UPDATE `home` = VALUES(`home`), `visitor` = VALUES(`visitor`), `game_date` = VALUES(`game_date`), `game_time` = VALUES(`game_time`), `status` = VALUES(`status`), `alt_venue` = VALUES(`alt_venue`);\n";
  print "UPDATE `SPORTS_NHL_SCHEDULE` SET status = 'PPD' WHERE `season` = '$season' AND ((" . join(') OR (', keys %ppd_list) . ")) AND status IS NULL;\n" if %ppd_list;
  print "ALTER TABLE `SPORTS_NHL_SCHEDULE` ORDER BY `season`, `game_type`, `game_id`;\n";
}

# Schedule dates
print "\n# Schedule dates\n";
print "CALL nhl_schedule_dates('$season');\n\n";

# If regular season, some extra calcs if creating for the first time
if (grep(/^$mode$/, ('regular', 'both')) && !$sched_cmpl) {
  print "\n";

  # Team v Team matchups
  print "# Team v Team matchups\n";
  print "CALL nhl_schedule_matchups('$season');\n\n";

  # Power ranking weeks
  print "# Power rank dates\n";
  print "CALL nhl_power_ranks_dates('$season');\n\n";

  # Initial standings
  print "# Initial standings\n";
  print "CALL nhl_standings_initial('$season');\n";
}
