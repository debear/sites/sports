#!/usr/bin/perl -w
# Download, import and process the starting lineups from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'lineups';
$config{'log_inc_time'} = '%H%M';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my $date = time2date(time());
our $season = substr($date, 0, 4) - (substr($date, 5, 2) lt '08' ? 1 : 0);
my $season_dir = season_dir($season);

# Game-parsing-specific config
$config = abs_path(__DIR__ . '/games/config.pl');
require $config;

# Identify week to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT CONCAT(UPPER(SUBSTRING(game_type, 1, 1)), "-", LPAD(game_id, 4, "0")) AS game_ref, game_type, game_id, visitor, home
FROM SPORTS_NHL_SCHEDULE
WHERE season = ?
AND   status IS NULL
AND   CONCAT(game_date, " ", game_time) > CONVERT_TZ(NOW(), @@session.time_zone, "' . $config{'timezone'} . '")
AND   CONCAT(game_date, " ", game_time) <= DATE_ADD(CONVERT_TZ(NOW(), @@session.time_zone, "' . $config{'timezone'} . '"), INTERVAL ? MINUTE)
ORDER BY game_date, game_time, game_id;';
my $games = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $config{'lineups_time_to_start'});
$dbh->disconnect if defined($dbh);

# If nothing returned, not time to run this
if (!@$games) {
  print STDERR "No games starting within the next $config{'lineups_time_to_start'} minutes, exiting...\n";
  exit $config{'exit_codes'}{'failed-prereq-silent'};
}

## Inform user
print "\nAcquiring data as of 'now' +$config{'lineups_time_to_start'}mins\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

foreach my $game (@$games) {
  print "- $$game{'game_ref'}: $$game{'visitor'} @ $$game{'home'}\n";

  # Download
  print '  - Download: ';
  command("$config{'base_dir'}/games/download.pl $season_dir $$game{'game_type'} $$game{'game_id'} --lineup-only",
          "$config{'log_dir'}/$logs{'download'}.log",
          "$config{'log_dir'}/$logs{'download'}.err");
  print "[ Done ]\n";
  next if download_only();

  # Parse
  print '  - Parse:    ';
  command("$config{'base_dir'}/games/parse/rosters-v$config{'integration_version'}.pl $season_dir $$game{'game_type'} $$game{'game_id'} --quick",
          "$config{'log_dir'}/$logs{'parse'}.sql",
          "$config{'log_dir'}/$logs{'parse'}.err");
  print "[ Done ]\n";
}

# If only downloading, go no further
end_script() if download_only();

#
# Import into the database
#
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'import'}.log",
        "$config{'log_dir'}/$logs{'import'}.err");
done(2);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'parse'    => '02_parse',
    'import'   => '03_import',
  );
}
