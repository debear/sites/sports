#!/usr/bin/perl -w
# Load the info for an individual player

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";

# Validate...
if (! -e $config{'dir'}) {
  print STDERR "# Unable to parse player $player_id\n";
  exit 10;
}

# Load the files
my $profile = load_file('profile');

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

# Get the player name
my $pattern = '<title>(.*?)\,';
my ($name) = ($profile =~ m/$pattern/gsi);
my @name = split(' ', $name);
my $fname = convert_text(shift @name);
my $sname = convert_text(join(' ', @name));

print "#\n# Parsing player info for $player_id\n#\n\n";
my %season_list = ( );

# Bio
parse_bio() if check_disp(1);

# Draft
parse_draft() if check_disp(2);

# Career Stats (Regular season)
parse_career('regular') if check_disp(3);

# Career Stats (Playoffs)
parse_career('playoff') if check_disp(4);

# Player IDs
print "# Mark player as imported\nUPDATE SPORTS_NHL_PLAYERS_IMPORT SET profile_imported = NOW() WHERE player_id = '$player_id';\n\n";

print "#\n# Completed parsing player info for $player_id\n#\n";

# Load contents of a file
sub load_file {
  my ($fn) = @_;
  my $file = "$config{'dir'}/$fn.htm";
  open FILE, "<$file";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...

  return $contents;
}

# Create an import statement based on a given table name and a hash
sub create_insert {
  my ($tbl, $hash) = @_;
  my @cols = ( 'player_id' ); my @vals = ( $player_id ); my @updates = ( );
  foreach my $key (keys %$hash) {
    push @cols, $key;
    push @vals, defined($$hash{$key}) && $$hash{$key} ne '' ? '"' . $$hash{$key} . '"' : 'NULL';
    push @updates, "$key = VALUES($key)";
  }

  print 'INSERT INTO ' . $tbl . ' (' . join(', ', @cols) . ') VALUES (' . join(', ', @vals) . ') ON DUPLICATE KEY UPDATE ' . join(', ', @updates) . ';' . "\n\n";
}

##
## Player Bio
##
sub parse_bio {
  print "# Player Bio\n";
  my %info = ( );

  # Height
  $pattern = '<td colspan="1" rowspan="1" class="label">\s+HEIGHT:\s+<\/td><td colspan="1" rowspan="1">\s+([^<]+)\s+<\/td>';
  ($info{'height'}) = ($profile =~ m/$pattern/gsi);
  if (defined($info{'height'})) {
    my @tmp = split "'", $info{'height'};
    if(!defined($tmp[1])) {
      $tmp[1] = 0;
    } else {
      $tmp[1] =~ s/"//g;
    }
    $info{'height'} = (12 * $tmp[0]) + $tmp[1];
  }

  # Weight
  $pattern = '<td colspan="1" rowspan="1" class="label">\s+WEIGHT:\s+<\/td><td colspan="1" rowspan="1">\s+([^<]+)\s+<\/td>';
  ($info{'weight'}) = ($profile =~ m/$pattern/gsi);

  # Shoots / Catches
  $pattern = '<td colspan="1" rowspan="1" class="label">\s+(?:Shoots|Catches):\s+<\/td><td colspan="1" rowspan="1">\s+([^<]+)\s+<\/td>';
  ($info{'shoots_catches'}) = ($profile =~ m/$pattern/gsi);

  # Date of Birth
  my %months = ( 'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12 );
  $pattern = '<td colspan="1" rowspan="1" class="label">\s+BIRTHDATE:\s+<\/td><td colspan="1" rowspan="1">\s+(\w{3})\w* (\d{1,2}), (\d{4})[^>]+<\/td>';
  my @dob = ($profile =~ m/$pattern/gsi);
  $info{'dob'} = scalar @dob ? $dob[2] . '-' . $months{$dob[0]} . '-' . $dob[1] : undef;

  # Birthplace
  $pattern = '<td colspan="1" rowspan="1" class="label">\s+BIRTHPLACE:\s+<\/td><td colspan="1" rowspan="1">\s+(.*?), ([^,<]+)\s+<\/td>';
  ($info{'birthplace'}, $info{'birthplace_country'}) = ($profile =~ m/$pattern/gsi);
  $info{'birthplace'} = convert_text($info{'birthplace'})
    if defined $info{'birthplace'};
  $info{'birthplace_country'} = convert_text($info{'birthplace_country'})
    if defined $info{'birthplace_country'};

  create_insert('SPORTS_NHL_PLAYERS', \%info);
}

##
## Draft
##
sub parse_draft {
  print "# Draft details\n";

  $pattern = '<td colspan="1" rowspan="1" class="label">\s+DRAFTED:\s+<\/td><td colspan="1" rowspan="1">\s+(\w+)\s+\/ <a style="text-decoration: underline;" shape="rect" href="\/ice\/draftsearch\.htm\?team=\w+&amp;year=(\d+)">\d+ NHL Entry Draft<\/a><\/td>';
  my @draft_a = ($profile =~ m/$pattern/gsi);
  $pattern = '<td colspan="1" rowspan="1" class="label">\s+ROUND:\s+<\/td><td colspan="1" rowspan="1">\s*(\d+)(?:<[^>]+>)?\w+.*?\((\d+)(?:<[^>]+>)?\w+(?:<[^>]+>)? overall\).*?<\/td>';
  my @draft_b = ($profile =~ m/$pattern/gsi);

  return if !scalar @draft_a;

  # Team / Year
  my %draft = ( 'season'    => $draft_a[1],
                'round'     => $draft_b[0],
                'pick'      => $draft_b[1],
                'team_id'   => convert_team_id($draft_a[0]),
                'player'    => "$fname $sname" );
  create_insert('SPORTS_NHL_DRAFT', \%draft);
}

##
## Career Stats
##
sub parse_career {
  my ($type) = @_;
  print "# Career stats ($type)\n";

  # Position (for later)
  my $pattern = '<span style="color: #666;">([^>])[^>]*</span>';
  my ($pos) = ($profile =~ m/$pattern/gsi);

  # Get the list from the stats section
  my $h3_type = ($type eq 'regular' ? 'REGULAR SEASON' : 'PLAYOFF');
  $pattern = '<h3>CAREER ' . $h3_type . ' STATISTICS</h3><table class="data playerStats"><thead>.*?<\/thead>(.*?)<\/table>';
  my ($table) = ($profile =~ m/$pattern/gsi);

  # Get the individual season list
  # It's a coincidence that skater and goalie stats have the same number of columns!
  my $num_cols = 11;
  $pattern = '<tr[^>]*><td[^>]*>.*?(\d{4})-.*?<\/td><!-- team name --><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td>';
  # Extra fields not included in goalie playoff stats
  if ($pos ne 'G' || $type eq 'regular') {
    $num_cols += 2;
    $pattern .= '<td[^>]*>(.*?)<\/td><td[^>]*>(.*?)<\/td>';
  }
  $pattern .= '<\/tr>';
  my @data = ($table =~ m/$pattern/gsi);

  while (my @season = splice(@data, 0, $num_cols)) {
    # Strip any HTML and trim any whitespace, except team name
    for (my $i = 0; $i <= $#season; $i++) {
      $season[$i] = strip_and_trim($season[$i], $i != 1);
    }

    # Skip if no games played in this season during the playoffs
    next if $type eq 'playoff' && $season[2] eq '';

    # Convert the season from YYYY-YYYY (such as 2009-2010) into YYYY (2009)
    $season[0] = substr($season[0], 0 ,4);

    # Check the GP column
    $season[2] = 0 if $season[2] eq '';

    # We also want to ignore any recent NHL season
    #  - These can be calced manualy
    my $nhl_calc_season = ($season[1] =~ m/^<a/ && $season[0] > 2006);

    # Valid row?
    next if $nhl_calc_season;
    # Non-NHL team?
    next if $season[1] !~ m/^<a/;

    # Convert team name
    my %row = ( 'season_type' => $type );
      ($row{'team_id'}) = ($season[1] =~ m/t(?:ea)?m=(\w+)/gsi);
       $row{'team_id'} = convert_team_id($row{'team_id'});
    # Get other info
    $row{'season'} = $season[0];

    # Required stats are dependent on the position
    $row{'gp'} = $season[2];
    if ($pos eq 'G') {
      # Goalie
      my $extra_index = 0; # Account for two additional columns in regular season stats
      $row{'win'} = $season[3];
      $row{'loss'} = $season[4];
      if ( $type eq 'regular' ) {
        $season[5] =~ s/-//g;
        $row{'tie'} = $season[5];
        $row{'ot_loss'} = $season[6];
        $extra_index = 2;
      }
      $row{'shutout'} = $season[5 + $extra_index];
      $row{'goals_against'} = $season[6 + $extra_index];
      $season[7 + $extra_index] =~ s/,//g;
      $row{'shots_against'} = $season[7 + $extra_index];
      $season[10 + $extra_index] =~ s/,//g;
      $row{'minutes_played'} = ($season[10 + $extra_index] ne '' ? $season[10 + $extra_index] * 60 : 0);
      create_insert('SPORTS_NHL_PLAYERS_SEASON_GOALIES', \%row);

    } else {
      # Skater
      $row{'goals'} = $season[3];
      $row{'assists'} = $season[4];
      $row{'plus_minus'} = $season[6];
      $row{'pims'} = $season[7];
      $row{'pp_goals'} = $season[8];
      $row{'sh_goals'} = $season[9];
      $row{'gw_goals'} = $season[10];
      $row{'shots'} = $season[11];
      create_insert('SPORTS_NHL_PLAYERS_SEASON_SKATERS', \%row);
    }
  }
}

# Process a season row
sub strip_and_trim {
  my ($str, $strip_html) = @_;

  # Strip any HTML
  if ($strip_html) {
    $str =~ s/<[^>]+>//g;
  }

  # Trim
  $str =~ s/^\s+//;
  $str =~ s/\s+$//;

  return $str;
}
