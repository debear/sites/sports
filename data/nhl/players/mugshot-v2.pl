#!/usr/bin/perl -w
# Process and copy the player's mugshot

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to player mugshot script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";
my $ext = 'png'; # As of 2023-24, we handle PNGs (until then it was JPEGs)
my $img_src = "$config{'dir'}/mugshot.$ext";
my $img_dst = abs_path("$config{'base_dir'}/../..") . "/resources/images/nhl/players/$player_id.$ext";
my $resync = grep(/\-\-resync/, @ARGV);

# Validate...
if (! -e $config{'dir'}) {
  print STDERR "# Unable to parse player $player_id\n";
  exit 10;
} elsif ( ! -e $img_src ) {
  print "# No source mugshot found for player $player_id\n";
  exit;
} elsif ( -e $img_dst && !$resync ) {
  print "# Destination mugshot already found for player $player_id\n";
  exit;
} elsif ( -e $img_dst ) {
  # A slightly more complicated validation: the source file needs to be newer
  my @stat_src = stat $img_src;
  my @stat_dst = stat $img_dst;
  if ($stat_dst[9] > $stat_src[9]) { # 9 = mtime
    print "# Destination mugshot file newer than source file for player $player_id\n";
    exit;
  }
}

# Convert and then compress
my $cmd = "convert $img_src -quiet -resize 150x150 -crop 100x150+25+0 $img_dst";
print "# Converting: $cmd\n";
`$cmd`;

$cmd = "image-compress-$ext --quiet $img_dst";
print "# Compressing: $cmd\n";
`$cmd`;

my @stat = stat $img_dst;
if ($stat[7]) { # 7 = size
  print "# Player mugshot for $player_id converted\n";
} else {
  unlink $img_dst;
  print "# Attempted to convert player mugshot for $player_id, but removed due to error in copy process\n";
}
