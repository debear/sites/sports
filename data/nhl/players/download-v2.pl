#!/usr/bin/perl -w
# Get player info for a single player from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Encode;
use URI::Escape;

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the get player script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";

# Skip, if this player has already been downloaded (and we're not in --force mode)
print "# Download for $player_id / $remote_id: ";
my $profile = "$config{'dir'}/profile.json.gz";
my $dl = (!-e $profile || grep(/\-\-force/, @ARGV));
if (-e $profile && !grep(/\-\-force/, @ARGV)) {
  my @stat = stat $profile;
  $dl = (time2date($stat[9]) ne time2date(time())); # 9 = mtime
}
if (!$dl) {
  print "[ Skipped ]\n";
  exit;
}
mkdir $config{'dir'};

# Downloading mugshots only?
my $mugshots_only = grep(/\-\-mugshot\-only/, @ARGV);

# Profile
if (!$mugshots_only) {
  download("https://statsapi.web.nhl.com/api/v1/people/$remote_id?site=en_nhl&expand=person.stats&stats=yearByYear,yearByYearPlayoffs", "$config{'dir'}/profile.json.gz", {'skip-today' => 1, 'if-modified-since' => 1, 'gzip' => 1});
  # Remove the old HTML version
  my $profile_html = "$config{'dir'}/profile.htm";
  unlink $profile_html if -e $profile_html;
}

# Mugshot
download("https://cms.nhl.bamgrid.com/images/headshots/current/168x168/$remote_id\@2x.jpg", "$config{'dir'}/mugshot.jpg", {'skip-today' => 1, 'if-modified-since' => 1});
# But remove if empty file
my @stat = stat "$config{'dir'}/mugshot.jpg";
unlink "$config{'dir'}/mugshot.jpg" if !$stat[7]; # 7 = size

print "[ Done ]\n";
