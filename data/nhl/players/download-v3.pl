#!/usr/bin/perl -w
# Get player info for a single player from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Encode;
use URI::Escape;
use JSON;

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the get player script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Guesstimate the season
my @time = localtime();
my $season = ($time[4] > 7 ? 1900 : 1899) + $time[5];
my $season_full = sprintf('%04d%04d', $season, $season + 1);

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";

# Skip, if this player has already been downloaded (and we're not in --force mode)
print "# Download for $player_id / $remote_id: ";
my $profile_local = "$config{'dir'}/profile.json.gz";
my $dl = (!-e $profile_local || grep(/\-\-force/, @ARGV));
if (-e $profile_local && !grep(/\-\-force/, @ARGV)) {
  my @stat = stat $profile_local;
  $dl = (time2date($stat[9]) ne time2date(time())); # 9 = mtime
}
if (!$dl) {
  print "[ Skipped ]\n";
  exit;
}
mkdir $config{'dir'};

download("https://statsapi.web.nhl.com/api/v1/people/$remote_id?site=en_nhl&expand=person.stats&stats=yearByYear,yearByYearPlayoffs", $profile_local, {'skip-today' => 1, 'if-modified-since' => 1, 'gzip' => 1});

# Mugshot (requires knowing the curent team)
my $profile = load_json($profile_local);
my $remote_numeric_id = $$profile{'people'}[0]{'currentTeam'}{'id'};
if (!defined($remote_numeric_id) || !$remote_numeric_id) {
  print STDERR "Unable to determine the current team for player $player_id (Remote: $remote_id)\n";
  print "[ Error ]\n";
  exit 1;
}
my $remote_team_id = convert_numeric_remote_id($remote_numeric_id);
my $mugshot_local = "$config{'dir'}/mugshot.png";
download("https://assets.nhle.com/mugs/nhl/$season_full/$remote_team_id/$remote_id.png", $mugshot_local, {'skip-today' => 1, 'if-modified-since' => 1});
# But remove if empty file
my @stat = stat $mugshot_local;
unlink $mugshot_local if !$stat[7]; # 7 = size

print "[ Done ]\n";

# Load contents of a file
sub load_json {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...

  return decode_json($contents);
}
