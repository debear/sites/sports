#!/usr/bin/perl -w
# Load the info for an individual player

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Locale::Country;
use Switch;

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";

# Validate...
if (! -e $config{'dir'}) {
  print STDERR "# Unable to parse player $player_id\n";
  exit 10;
}

# Load the files
my $profile = load_file('profile');
$profile = decode_json($profile);

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

# Get the player name, for display

print "#\n# Parsing player info for $player_id\n#\n\n";
my %season_list = ( );

# Bio
parse_bio() if check_disp(1);

# Draft
parse_draft() if check_disp(2);

# Player IDs
print "# Mark player as imported\nUPDATE SPORTS_NHL_PLAYERS_IMPORT SET profile_imported = NOW() WHERE player_id = '$player_id';\n\n";

print "#\n# Completed parsing player info for $player_id\n#\n";

# Load contents of a file
sub load_file {
  my ($fn) = @_;
  my $file = "$config{'dir'}/$fn.json.gz";
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...

  return $contents;
}

# Create an import statement based on a given table name and a hash
sub create_insert {
  my ($tbl, $hash) = @_;
  my @cols = ( 'player_id' ); my @vals = ( $player_id ); my @updates = ( );
  foreach my $key (keys %$hash) {
    push @cols, $key;
    push @vals, defined($$hash{$key}) && $$hash{$key} ne '' ? '"' . $$hash{$key} . '"' : 'NULL';
    push @updates, "$key = VALUES($key)";
  }

  print 'INSERT INTO ' . $tbl . ' (' . join(', ', @cols) . ') VALUES (' . join(', ', @vals) . ') ON DUPLICATE KEY UPDATE ' . join(', ', @updates) . ';' . "\n\n";
}

##
## Player Bio
##
sub parse_bio {
  print "# Player Bio\n";
  my %info = ( );

  # Name (just in case)
  $info{'first_name'}  = defined($$profile{'firstName'}{'default'}) ? encode_entities($$profile{'firstName'}{'default'}) : undef;
  $info{'surname'}  = defined($$profile{'lastName'}{'default'}) ? encode_entities($$profile{'lastName'}{'default'}) : undef;

  # Height
  $info{'height'} = $$profile{'heightInInches'};
  # Weight
  $info{'weight'} = $$profile{'weightInPounds'};

  # Shoots / Catches
  $$profile{'shootsCatches'} = '' if !defined($$profile{'shootsCatches'});
  switch ($$profile{'shootsCatches'}) {
    case 'L' { $info{'shoots_catches'} = 'Left' }
    case 'R' { $info{'shoots_catches'} = 'Right' }
    else     { $info{'shoots_catches'} = undef }
  }

  # Date of Birth
  $info{'dob'} = $$profile{'birthDate'};

  # Birthplace
  $info{'birthplace'}  = $$profile{'birthCity'}{'default'};
  $info{'birthplace'} .= ', ' . $$profile{'birthStateProvince'}{'default'}
    if defined($$profile{'birthStateProvince'}{'default'}) && $$profile{'birthStateProvince'}{'default'} ne '';
  $info{'birthplace'} = encode_entities($info{'birthplace'})
    if defined $info{'birthplace'};
  $info{'birthplace_country'} = lc country_code2code(lc fix_country_code3($$profile{'birthCountry'}), LOCALE_CODE_ALPHA_3, LOCALE_CODE_ALPHA_2)
    if defined $$profile{'birthCountry'};

  create_insert('SPORTS_NHL_PLAYERS', \%info);
}

sub fix_country_code3 {
  my ($code3) = @_;
  return 'BGR' if uc $code3 eq 'BUL';
  return 'CHE' if uc $code3 eq 'SUI';
  return 'DEU' if uc $code3 eq 'GER';
  return 'DNK' if uc $code3 eq 'DEN';
  return 'LVA' if uc $code3 eq 'LAT';
  return 'SVN' if uc $code3 eq 'SLO';
  return $code3;
}

##
## Draft
##
sub parse_draft {
  print "# Draft record\n";
  print "UPDATE SPORTS_NHL_DRAFT SET player_id = '$player_id' WHERE remote_id = '$remote_id';\n\n";
}
