#!/usr/bin/perl -w
# Process and copy the player's mugshot

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Copy;

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to player mugshot script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";
my $img_src = "$config{'dir'}/mugshot.jpg";
my $img_dst = abs_path("$config{'base_dir'}/../..") . "/htdocs/images/nhl/players/$player_id.jpg";
my $resync = grep(/\-\-resync/, @ARGV);

# Validate...
if (! -e $config{'dir'}) {
  print STDERR "# Unable to parse player $player_id\n";
  exit 10;
} elsif ( ! -e $img_src ) {
  print "# No source mugshot found for player $player_id\n";
  exit;
} elsif ( -e $img_dst && !$resync ) {
  print "# Destination mugshot already found for player $player_id\n";
  exit;
}

copy($img_src, $img_dst);
my @stat = stat $img_dst;
if ($stat[7]) {
  print "# Player mugshot for $player_id converted\n";
} else {
  unlink $img_dst;
  print "# Attempted to convert player mugshot for $player_id, but removed due to error in copy process\n";
}
