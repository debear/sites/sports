#!/usr/bin/perl -w
# Load the info for an individual player

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Locale::Country;
use Switch;

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";

# Validate...
if (! -e $config{'dir'}) {
  print STDERR "# Unable to parse player $player_id\n";
  exit 10;
}

# Load the files
my $profile = load_file('profile');
$profile = decode_json($profile);
$profile = $$profile{'people'}[0];

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

# Get the player name, for display

print "#\n# Parsing player info for $player_id\n#\n\n";
my %season_list = ( );

# Bio
parse_bio() if check_disp(1);

# Draft
parse_draft() if check_disp(2);

# Career Stats (Regular season)
parse_career('regular') if check_disp(3);

# Career Stats (Playoffs)
parse_career('playoff') if check_disp(4);

# Player IDs
print "# Mark player as imported\nUPDATE SPORTS_NHL_PLAYERS_IMPORT SET profile_imported = NOW() WHERE player_id = '$player_id';\n\n";

print "#\n# Completed parsing player info for $player_id\n#\n";

# Load contents of a file
sub load_file {
  my ($fn) = @_;
  my $file = "$config{'dir'}/profile.json.gz";
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...

  return $contents;
}

# Create an import statement based on a given table name and a hash
sub create_insert {
  my ($tbl, $hash) = @_;
  my @cols = ( 'player_id' ); my @vals = ( $player_id ); my @updates = ( );
  foreach my $key (keys %$hash) {
    push @cols, $key;
    push @vals, defined($$hash{$key}) && $$hash{$key} ne '' ? '"' . $$hash{$key} . '"' : 'NULL';
    push @updates, "$key = VALUES($key)";
  }

  print 'INSERT INTO ' . $tbl . ' (' . join(', ', @cols) . ') VALUES (' . join(', ', @vals) . ') ON DUPLICATE KEY UPDATE ' . join(', ', @updates) . ';' . "\n\n";
}

##
## Player Bio
##
sub parse_bio {
  print "# Player Bio\n";
  my %info = ( );

  # Name (just in case)
  $info{'first_name'}  = defined($$profile{'firstName'}) ? encode_entities($$profile{'firstName'}) : undef;
  $info{'surname'}  = defined($$profile{'lastName'}) ? encode_entities($$profile{'lastName'}) : undef;

  # Height
  $info{'height'} = $$profile{'height'};
  if (defined($info{'height'})) {
    my @tmp = split "'", $info{'height'};
    if(!defined($tmp[1])) {
      $tmp[1] = 0;
    } else {
      $tmp[1] =~ s/"//g;
    }
    $info{'height'} = (12 * $tmp[0]) + $tmp[1];
  }

  # Weight
  $info{'weight'} = $$profile{'weight'};

  # Shoots / Catches
  $$profile{'shootsCatches'} = '' if !defined($$profile{'shootsCatches'});
  switch ($$profile{'shootsCatches'}) {
    case 'L' { $info{'shoots_catches'} = 'Left' }
    case 'R' { $info{'shoots_catches'} = 'Right' }
    else     { $info{'shoots_catches'} = undef }
  }

  # Date of Birth
  $info{'dob'} = $$profile{'birthDate'};

  # Birthplace
  $info{'birthplace'}  = $$profile{'birthCity'};
  $info{'birthplace'} .= ', ' . $$profile{'birthStateProvince'}
    if defined($$profile{'birthStateProvince'}) && $$profile{'birthStateProvince'} ne '';
  $info{'birthplace'} = encode_entities($info{'birthplace'})
    if defined $info{'birthplace'};
  $info{'birthplace_country'} = lc country_code2code(lc $$profile{'birthCountry'}, LOCALE_CODE_ALPHA_3, LOCALE_CODE_ALPHA_2)
    if defined $$profile{'birthCountry'};

  create_insert('SPORTS_NHL_PLAYERS', \%info);
}

##
## Draft
##
sub parse_draft {
  print "# Draft record\n";
  print "UPDATE SPORTS_NHL_DRAFT SET player_id = '$player_id' WHERE remote_id = '$remote_id';\n\n";
}

##
## Career Stats
##
sub parse_career {
  my ($type) = @_;
  print "# Career stats ($type)\n";

  # Required processing
  my %stat_map;
  # Goalie stats
  if ($$profile{'primaryPosition'}{'abbreviation'} eq 'G') {
    %stat_map = (
      'table' => 'SPORTS_NHL_PLAYERS_SEASON_GOALIES',
      'cols' => {
        'gp' => 'games',
        'starts' => 'gamesStarted',
        'win' => 'wins',
        'loss' => 'losses',
        'tie' => 'ties',
        'ot_loss' => 'ot',
        'goals_against' => 'goalsAgainst',
        'shots_against' => 'shotsAgainst',
        'shutout' => 'shutouts',
        'minutes_played' => [ 'toi', 'timeOnIce' ],
      }
    );

  # Skater stats
  } else {
    %stat_map = (
      'table' => 'SPORTS_NHL_PLAYERS_SEASON_SKATERS',
      'cols' => {
        'gp' => 'games',
        'goals' => 'goals',
        'assists' => 'assists',
        'plus_minus' => 'plusMinus',
        'pims' => 'penaltyMinutes',
        'pp_goals' => 'powerPlayGoals',
        'pp_assists' => [ 'sub', [ 'powerPlayPoints', 'powerPlayGoals' ] ],
        'sh_goals' => 'shortHandedGoals',
        'sh_assists' => [ 'sub', [ 'shortHandedPoints', 'shortHandedGoals' ] ],
        'gw_goals' => 'gameWinningGoals',
        'shots' => 'shots',
        'blocked_shots' => 'blocked',
        'hits' => 'hits',
        'atoi' => [ 'atoi', [ 'timeOnIce', 'games' ] ],
      }
    );
  }

  # Get the list from the stats section
  my %season_list = ( );
  my $search = ($type eq 'regular' ? 'yearByYear' : 'yearByYearPlayoffs');
  foreach my $stats (@{$$profile{'stats'}}) {
    next if $$stats{'type'}{'displayName'} ne $search;

    foreach my $season (@{$$stats{'splits'}}) {
      # Convert the season from YYYYYYYY (such as 20092010) into YYYY (2009)
      $$season{'season'} = substr($$season{'season'}, 0 ,4);

      # We also want to ignore any recent NHL season
      #  - These can be calced manually
      my $is_nhl = (defined($$season{'league'}{'id'}) && $$season{'league'}{'id'} == $config{'statsapi_league_id'});
      next if !$is_nhl || $$season{'season'} <= 2006;

      my %row = ( 'season_type' => $type );
      $row{'team_id'} = convert_numeric_team_id($$season{'team'}{'id'});
      $row{'season'} = $$season{'season'};

      # Loop through the stat cols
      foreach my $col (keys %{$stat_map{'cols'}}) {
        # Value copy
        if (ref($stat_map{'cols'}{$col}) ne 'ARRAY') {
          $row{$col} = $$season{'stat'}{$stat_map{'cols'}{$col}};
          next;
        }

        # Processing required
        my ($calc_type, $calc_fields) = @{$stat_map{'cols'}{$col}};
        if ($calc_type eq 'sub') {
          # A - B
          $row{$col} = $$season{'stat'}{$$calc_fields[0]} - $$season{'stat'}{$$calc_fields[1]}
            if defined($$season{'stat'}{$$calc_fields[0]});

        } elsif ($calc_type eq 'toi') {
          # Time on Ice
          if (defined($$season{'stat'}{$calc_fields}) && $$season{'stat'}{$calc_fields}) {
            my ($toi_m, $toi_s) = ($$season{'stat'}{$calc_fields} =~ m/^(\d+):(\d+)$/);
            $row{$col} = ($toi_m * 60) + $toi_s;
          }

        } elsif ($calc_type eq 'atoi') {
          # Average Time on Ice, from season totals
          if (defined($$season{'stat'}{$$calc_fields[0]}) && defined($$season{'stat'}{$$calc_fields[1]}) && $$season{'stat'}{$$calc_fields[1]}) {
            my ($toi_m, $toi_s) = ($$season{'stat'}{$$calc_fields[0]} =~ m/^(\d+):(\d+)$/);
            my $toi = ($toi_m * 60) + $toi_s;
            my $atoi = $toi / $$season{'stat'}{$$calc_fields[1]};
            $row{$col} = sprintf('%d:%02d', int($atoi / 60), $atoi % 60);
          }
        }
      }

      # Output the SQL
      create_insert($stat_map{'table'}, \%row);
    }
  }
}
