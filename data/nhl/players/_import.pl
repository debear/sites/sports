#!/usr/bin/perl -w
# Download and process players from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config{'dir'} = __DIR__;
$config{'download_only'} = 0; # Set to only download data, rather than parse and import it as well

# Get the suggested list of players to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT IMP.player_id, IMP.remote_id,
       PL.first_name, PL.surname
FROM SPORTS_NHL_PLAYERS_IMPORT AS IMP
JOIN SPORTS_NHL_PLAYERS AS PL
  ON (PL.player_id = IMP.player_id)
WHERE IMP.profile_imported IS NULL
ORDER BY IMP.player_id;';
my $player_list = $dbh->selectall_arrayref($sql, { Slice => {} });
$dbh->disconnect if defined($dbh);

# Quit if none found...
if (!@$player_list) {
  exit;
}

# Loop through the players, downloading and parsing the profile / career info
print "##\n## Importing players...\n##\n";
foreach my $player (@$player_list) {
  print "##\n## $$player{'player_id'}: $$player{'first_name'} $$player{'surname'} ($$player{'remote_id'})\n##\n\n";

  # Download
  print `$config{'dir'}/download.pl $$player{'player_id'} $$player{'remote_id'} 2>&1` . "\n";
  next if download_only();

  # Process
  print `$config{'dir'}/parse.pl $$player{'player_id'} $$player{'remote_id'}` . "\n";

  # Mugshot
  print `$config{'dir'}/mugshot.pl $$player{'player_id'} $$player{'remote_id'}` . "\n";
}

