#!/usr/bin/perl -w
# Get player info for a single player from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Encode;
use URI::Escape;

# Validate the arguments
#  - there needs to be two: player_id, remote_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the get player script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_id";

# Skip, if this player has already been downloaded (and we're not in --force mode)
print "# Download for $player_id / $remote_id: ";
my $dl = (!-e $config{'dir'} || grep(/\-\-force/, @ARGV));
if (-e $config{'dir'} && !grep(/\-\-force/, @ARGV)) {
  my @stat = stat "$config{'dir'}/profile.htm";
  $dl = (time2date($stat[9]) ne time2date(time())); # 9 = mtime
}
if (!$dl) {
  print "[ Skipped ]\n";
  exit;
}
mkdir $config{'dir'};

# Downloading mugshots only?
my $mugshots_only = grep(/\-\-mugshot\-only/, @ARGV);

# Profile
download("http://www.nhl.com/ice/player.htm?id=$remote_id", "$config{'dir'}/profile.htm", {'skip-today' => 1})
  if !$mugshots_only;

# Mugshot
download("http://cdn.nhl.com/photos/mugs/$remote_id.jpg", "$config{'dir'}/mugshot.jpg", {'skip-today' => 1});
# But remove if empty file
my @stat = stat "$config{'dir'}/mugshot.jpg";
unlink "$config{'dir'}/mugshot.jpg" if !$stat[7];

print "[ Done ]\n";
