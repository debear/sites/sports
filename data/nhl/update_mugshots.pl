#!/usr/bin/perl -w
# Download and process player mugshots from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Identify season (no need to ask, do it from dates)
my @time = localtime();
my $season = ($time[4] > 7 ? 1900 : 1899) + $time[5];

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

# Inform user
print "Processing mugshots for $season players\n";

# Get the list of players to update
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT IMP.player_id, IMP.remote_id,
       PL.first_name, PL.surname
FROM SPORTS_NHL_TEAMS_ROSTERS AS ROS
JOIN SPORTS_NHL_PLAYERS AS PL
  ON (PL.player_id = ROS.player_id)
JOIN SPORTS_NHL_PLAYERS_IMPORT AS IMP
  ON (IMP.player_id = ROS.player_id)
WHERE ROS.season = ?
GROUP BY IMP.remote_id
ORDER BY IMP.remote_id;';
my $player_list = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
$dbh->disconnect if defined($dbh);

# No need to continue (but not an error) if nothing found
if (!@$player_list) {
  print STDERR "- No players found. Aborting.\n";
  exit $config{'exit_codes'}{'failed-prereq-silent'};
}

# Setup logging
$config{'log_dir'} .= 'mugshots';
my %logs = get_log_def();
identify_log_file();
mkpath($config{'log_dir'});

# Run
print "\nRun started at " . `date` . "\n";
foreach my $player (@$player_list) {
  $$player{'disp_name'} = decode_text("$$player{'first_name'} $$player{'surname'}");
  print "- $$player{'player_id'}: $$player{'disp_name'} (Remote ID: $$player{'remote_id'})\n";

  # Download
  print '  - Download: ';
  command("$config{'base_dir'}/players/download.pl $$player{'player_id'} $$player{'remote_id'} --force --mugshot-only",
          "$config{'log_dir'}/$logs{'download'}.log",
          "$config{'log_dir'}/$logs{'download'}.err");
  print "[ Done ]\n";

  # Process
  print '  - Process:  ';
  command("$config{'base_dir'}/players/mugshot.pl $$player{'player_id'} $$player{'remote_id'} --resync",
          "$config{'log_dir'}/$logs{'process'}.log",
          "$config{'log_dir'}/$logs{'process'}.err");
  print "[ Done ]\n";
}

# Tidy up and end the script
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'process'  => '02_process',
  );
}
