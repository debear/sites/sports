#!/usr/bin/perl -w
# Download and import game data from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Validate args: cannot supply both unattended and complete
if ($config{'is_unattended'} && $config{'is_completing'}) {
  print STDERR ' - Unable to run script with both --unattended and --complete flags set.' . "\n";
  exit $config{'exit_codes'}{'pre-flight'};
}

# Define log details
$config{'log_dir'} .= 'games';
my %logs = get_log_def();

# Guesstimate the season
my @time = localtime();
my $season_guess = ($time[4] > 7 ? 1900 : 1899) + $time[5];

# Unless passed in a special argument, we're to ask the user how to operate
my $season;

# Determine for ourselves
if ($config{'is_unattended'}) {
  # Season is our guessed season
  $season = $season_guess;

# Get from the user
} else {
  $season = question_user("Please enter a season in YYYY format [$season_guess]", '\d{4}', $season_guess);
}

# Tidy internal variables after being determined above
my $season_dir = season_dir($season);

# Connect to the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Validate that we are not in the middle of another update, where data was processed but requires human intervention to complete
if (!$config{'is_completing'}) {
  my $sql = 'SELECT *
FROM SPORTS_NHL_SCHEDULE
WHERE season = ?
AND   game_date < CURDATE()
AND   status IS NOT NULL
AND   status NOT IN ("PPD", "CNC")
AND   IFNULL(lineup_linked, 0) = 0;';
  my $sth = $dbh->prepare($sql);
  $sth->execute($season);
  if ($sth->rows) {
    print STDERR ' - Unable to continue, manual intervention required to complete a previous run.' . "\n";
    print STDERR '   Please re-run with the --complete argument.' . "\n";
    undef $sth if defined($sth);
    $dbh->disconnect if defined($dbh);
    exit $config{'exit_codes'}{'pre-flight'};
  }
}

# Get the suggested list of games to import
my $sql = 'SELECT game_type, game_id, home, visitor,
       CONCAT(DATE_FORMAT(game_time, "%H:%i"), " ", DATE_FORMAT(game_date, "%D %b")) AS date_fmt
FROM SPORTS_NHL_SCHEDULE
WHERE season = ?
AND   game_date < CURDATE()
' . ($config{'is_completing'}
       ? 'AND   status IS NOT NULL AND status NOT IN ("PPD", "CNC") AND IFNULL(lineup_linked, 0) = 0' # Games that need the process completing via manual input
       : 'AND   status IS NULL') # Games we have not yet imported
  . '
ORDER BY game_date, game_time, game_id;';
my $game_list = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
$dbh->disconnect if defined($dbh);

# No need to continue (but not an error) if nothing found
if (!@$game_list) {
  print STDERR ' - There are no outstanding games to import.' . "\n";
  end_script();
}

# Process
my %list = ( 'all' => [ ], 'regular' => [ ], 'playoff' => [ ], 'process' => [ ], 'list' => [ ] );
my %game_ids = ( 'regular' => [ ], 'playoff' => [ ] );
foreach my $game (@$game_list) {
  $$game{'game_type_code'} = convert_game_type($$game{'game_id'});

  # Summarise for display
  $$game{'summary'} = 'Game ' . uc(substr($$game{'game_type'}, 0, 1)) . '-' . $$game{'game_id'} . ': ' . $$game{'visitor'} . ' @ ' . $$game{'home'} . ', ' . $$game{'date_fmt'};
  $$game{'summary'} .= ' - ' . playoff_summary($season, $$game{'game_id'})
    if ($$game{'game_type'} eq 'playoff');
  print $$game{'summary'} . "\n";

  # Store for info
  push @{$list{'all'}}, $game;
}

# Which do we want to include? If automated, yes, we do...
my $import_all = ($config{'is_unattended'} ? 'y' : question_user('Do you wish to import ALL these games? [Y/n/c]', '[ync]', 'Y'));

# Are we excluding specific games?
if ($import_all =~ m/^n$/i) {
  foreach my $game (@{$list{'all'}}) {
    my $import = question_user($$game{'summary'} . ' - Import? [Y/n]', '[yn]', 'Y');
    if ($import =~ m/^y$/i) {
      push @{$list{'process'}}, $game;
      push @{$list{$$game{'game_type'}}}, $game;
      push @{$game_ids{$$game{'game_type'}}}, $$game{'game_id'};
    }
  }

# No, so process the full list if not cancelling
} elsif ($import_all !~ m/^c$/i) {
  $list{'process'} = $list{'all'};
  foreach my $game (@{$list{'all'}}) {
    push @{$list{$$game{'game_type'}}}, $game;
    push @{$game_ids{$$game{'game_type'}}}, $$game{'game_id'};
  }
}

# Check again - if here, no rounds were explicitly selected
if (!@{$list{'process'}}) {
  print ' - No games were selected to be imported, exiting.' . "\n";
  exit $config{'exit_codes'}{'pre-flight'};
}

# Inform user
print "\nProcessing data from $season, " . @{$list{'process'}} . " game(s)\n";
print 'Run started at ' . `date` . "\n";

# Idenfity the log file
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Processing game data
#  - Only relevant if not 'completing' a previous run
#
if (!$config{'is_completing'}) {
  #
  # Parse each of the games
  #
  foreach my $game (@{$list{'process'}}) {
    print "- $$game{'summary'}\n";

    # Download, if we haven't done so already (handled in-script)
    print '  - Download: ';
    command("$config{'base_dir'}/games/download.pl $season_dir $$game{'game_type'} $$game{'game_id'}",
            "$config{'log_dir'}/$logs{'games_d'}.log",
            "$config{'log_dir'}/$logs{'games_d'}.err");
    print "[ Done ]\n";
    next if download_only();

    # Parse
    print '  - Parse:    ';
    command("$config{'base_dir'}/games/parse.pl $season_dir $$game{'game_type'} $$game{'game_id'}",
            "$config{'log_dir'}/$logs{'games_pa'}.sql",
            "$config{'log_dir'}/$logs{'games_pa'}.err");
    print "[ Done ]\n";
  }

  # If only downloading, go no further
  end_script() if download_only();

  #
  # Tidy the game tables
  #
  print '=> Tidy: ';
  command("$config{'base_dir'}/games/order.pl",
          "$config{'log_dir'}/$logs{'games_pa'}.sql",
          "$config{'log_dir'}/$logs{'games_pa'}.err");
  done(20);

  #
  # Import into the database
  #
  print '=> Importing Games: ';
  command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'games_pa'}.sql",
          "$config{'log_dir'}/$logs{'games_i'}.log",
          "$config{'log_dir'}/$logs{'games_i'}.err");
  done(9);
}

# Next group of scripts are the first section of post-processing
$config{'exit_status'} = 'post-process-1';

#
# Game-by-Game Post-Processing
#
if (!$config{'is_completing'}) {
  print '=> Post-Processing Games: ';
  foreach my $game (@{$list{'process'}}) {
    command("echo \"CALL nhl_game_process($season, '$$game{'game_type'}', '$$game{'game_id'}');\" | /usr/bin/mysql $config{'db_name'}",
            "$config{'log_dir'}/$logs{'games_pr'}.log",
            "$config{'log_dir'}/$logs{'games_pr'}.err");
  }
  done(3);
}

#
# Update players (lineups, scratches, stats)
#  - If unattended, flag here as we need to abort if player linkage required
#
print '=> Linking Players: ';
command("$config{'base_dir'}/games/lineups.pl" . ($config{'is_unattended'} ? ' --unattended' : ''),
        undef,
        "$config{'log_dir'}/$logs{'players_l'}.err");
done(9);

#
# Call player import script to update new players
#
print '=> New Players: ';
command("$config{'base_dir'}/players/_import.pl",
        "$config{'log_dir'}/$logs{'players_c'}.sql",
        "$config{'log_dir'}/$logs{'players_c'}.err");
done(13);

print '=> Importing Players: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'players_c'}.sql",
        "$config{'log_dir'}/$logs{'players_i'}.log",
        "$config{'log_dir'}/$logs{'players_i'}.err");
done(7);

# Next group of scripts are the second section of post-processing
$config{'exit_status'} = 'post-process-2';

#
# Form the tmp_date_list queries to prepend to our stored procedures
#
my %tmp_date_list = ();
$tmp_date_list{'full'} = "DROP TEMPORARY TABLE IF EXISTS tmp_date_list; CREATE TEMPORARY TABLE tmp_date_list (season YEAR NOT NULL, the_date DATE, PRIMARY KEY (season, the_date)) ENGINE = MEMORY SELECT season, game_date AS the_date FROM SPORTS_NHL_SCHEDULE WHERE season = '$season' AND ((game_type = 'regular' AND game_id IN ('" . join("', '", @{$game_ids{'regular'}}) . "')) OR (game_type = 'playoff' AND game_id IN ('" . join("', '", @{$game_ids{'playoff'}}) . "'))) GROUP BY season, the_date; ";
foreach my $game_type ( 'regular', 'playoff' ) {
  $tmp_date_list{$game_type} = "DROP TEMPORARY TABLE IF EXISTS tmp_date_list; CREATE TEMPORARY TABLE tmp_date_list (season YEAR NOT NULL, the_date DATE, PRIMARY KEY (season, the_date)) ENGINE = MEMORY SELECT season, game_date AS the_date FROM SPORTS_NHL_SCHEDULE WHERE season = '$season' AND game_type = '$game_type' AND game_id IN ('" . join("', '", @{$game_ids{$game_type}}) . "') GROUP BY season, the_date; ";
}

#
# And ditto tmp_game_list
#
my %tmp_game_list = ();
$tmp_game_list{'full'} = "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '$season', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY; ";
foreach my $game_type ( 'regular', 'playoff' ) {
  $tmp_game_list{'full'} .= "INSERT INTO tmp_game_list (game_type, game_id) VALUES ('$game_type', '" . join("'), ('$game_type', '", @{$game_ids{$game_type}}) . "'); "
    if @{$list{$game_type}};

  $tmp_game_list{$game_type} = "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '$season', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY; INSERT INTO tmp_game_list (game_type, game_id) VALUES ('$game_type', '" . join("'), ('$game_type', '", @{$game_ids{$game_type}}) . "'); ";
}

#
# Update player totals
#
# Game totals
print '=> Game Totals (Players): ';
command("echo \"$tmp_game_list{'full'}CALL nhl_totals_players();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err");
command("echo \"CALL nhl_totals_players_order();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err");
done(3);

# Season totals
print '=> Season Totals (Players): ';
foreach my $game_type ( 'regular', 'playoff' ) {
  next if !@{$list{$game_type}};
  command("echo \"CALL nhl_totals_players_season('$season', '$game_type');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'totals_pl'}.log",
          "$config{'log_dir'}/$logs{'totals_pl'}.err");
  command("echo \"CALL nhl_totals_players_season_sort('$season', '$game_type');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'totals_pl'}.log",
          "$config{'log_dir'}/$logs{'totals_pl'}.err");
}
command("echo \"CALL nhl_totals_players_season_order();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err");
done(1);

#
# Update team totals
#
print '=> Game Totals (Teams): ';
command("echo \"$tmp_game_list{'full'}CALL nhl_totals_teams();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err");
command("echo \"CALL nhl_totals_teams_order();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err");
done(5);

# Season totals
print '=> Season Totals (Teams): ';
foreach my $game_type ( 'regular', 'playoff' ) {
  next if !@{$list{$game_type}};
  command("echo \"CALL nhl_totals_teams_season('$season', '$game_type');\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err");
  command("echo \"CALL nhl_totals_teams_season_sort('$season', '$game_type');\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err");
}
command("echo \"CALL nhl_totals_teams_season_order();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err");
done(3);

#
# Team rosters
#
print '=> Updating team rosters: ';
command("echo \"$tmp_date_list{'full'}CALL nhl_rosters();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'rosters'}.log",
        "$config{'log_dir'}/$logs{'rosters'}.err");
done(3);

# Next group of scripts are the third section of post-processing
$config{'exit_status'} = 'post-process-3';

#
# Standings
#
if (@{$list{'regular'}}) {
  print '=> Standings: ';
  command("echo \"$tmp_date_list{'regular'}CALL nhl_standings('$config{'num_recent'}');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'standings'}.log",
          "$config{'log_dir'}/$logs{'standings'}.err");
  done(15);
}

#
# Playoff seeds / matchups
#
if (@{$list{'regular'}}) {
  print '=> Playoff Seeds: ';
  command("echo \"CALL nhl_playoff_seeds('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_seeds'}.log",
          "$config{'log_dir'}/$logs{'po_seeds'}.err");
  done(11);

  print '=> Playoff Matchups: ';
  command("echo \"CALL nhl_playoff_matchups('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_matchups'}.log",
          "$config{'log_dir'}/$logs{'po_matchups'}.err");
  done(8);
}

# Next group of scripts are the fourth section of post-processing
$config{'exit_status'} = 'post-process-4';

#
# Playoff series updates
#
if (@{$list{'playoff'}}) {
  print '=> Playoff Series: ';
  command("echo \"$tmp_game_list{'playoff'}CALL nhl_playoff_series('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_series'}.log",
          "$config{'log_dir'}/$logs{'po_series'}.err");
  done(10);

  print '=> Playoff Matchups: ';
  command("echo \"CALL nhl_playoff_matchups('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_matchups'}.log",
          "$config{'log_dir'}/$logs{'po_matchups'}.err");
  done(8);

  # Download latest version of the schedule
  print '=> Playoff Schedule:' . "\n";
  my @now = localtime(time());
  my $sched_date = sprintf('%04d-%02d-%02d', $now[5] + 1900, $now[4] + 1, $now[3]);
  print '   -> Download: ';
  command("$config{'base_dir'}/schedule/download.pl $season_dir --playoff $sched_date",
          "$config{'log_dir'}/$logs{'po_series_d'}.log",
          "$config{'log_dir'}/$logs{'po_series_d'}.err");
  done(13);

  # Parse
  print '   -> Parse: ';
  command("$config{'base_dir'}/schedule/parse.pl $season_dir --playoff $sched_date",
          "$config{'log_dir'}/$logs{'po_series_pa'}.sql",
          "$config{'log_dir'}/$logs{'po_series_pa'}.err");
  done(16);

  # Import
  print '   -> Import: ';
  command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'po_series_pa'}.sql",
          "$config{'log_dir'}/$logs{'po_series_i'}.log",
          "$config{'log_dir'}/$logs{'po_series_i'}.err");
  done(15);

  # Process
  print '   -> Process: ';
  command("echo \"CALL nhl_playoff_series_id_match('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_series_pr'}.log",
          "$config{'log_dir'}/$logs{'po_series_pr'}.err");
  done(14);

  # Histories
  print '=> Playoff History: ';
  command("echo \"CALL nhl_history_playoffs('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_history'}.log",
          "$config{'log_dir'}/$logs{'po_history'}.err");
  done(9);
}

#
# Power rankings?
#
if (@{$list{'regular'}}) {
  print '=> Power Ranks: ';
  command("echo \"CALL nhl_power_ranks('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'power_ranks'}.log",
          "$config{'log_dir'}/$logs{'power_ranks'}.err");
  done(13);
}

#
# Tidy up and end the script
#
end_script();

#
# Custom end script (if running in --complete mode)
#
sub end_script_custom {
  # No need to do anything if we're not completing a previous (automated) run
  return if !$config{'is_completing'};

  # Load the lockfile library
  my $lib = abs_path(__DIR__ . '/../_global/lockfile.pl');
  require $lib;

  # And run the 'finishing' method
  lockfile_finish('sports_nhl');
}


#
# Define the log file names
#
sub get_log_def {
  return (
    'games_d'      => '01_games_download',
    'games_pa'     => '02_games_parse',
    'games_i'      => '03_games_import',
    'games_pr'     => '04_games_process',
    'players_l'    => '05_players_link',
    'players_c'    => '06_players_create',
    'players_i'    => '07_players_import',
    'totals_pl'    => '08_totals_player',
    'totals_tm'    => '09_totals_team',
    'rosters'      => '10_rosters',
    'standings'    => '11_standings',
    'po_seeds'     => '12_playoff_seeds',
    'po_series'    => '13_playoff_series',
    'po_matchups'  => '14_playoff_matchups',
    'po_series_d'  => '15_schedule_download',
    'po_series_pa' => '16_schedule_parse',
    'po_series_i'  => '17_schedule_import',
    'po_series_pr' => '18_schedule_process',
    'po_history'   => '19_playoff_history',
    'power_ranks'  => '20_power_ranks',
  );
}
