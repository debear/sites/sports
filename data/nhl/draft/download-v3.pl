#!/usr/bin/perl -w
# Get the draft results from NHL.com, along with CapFriendly for traded selections

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be one: a season
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get draft script.\n";
  exit 98;
}

# Declare variables
our ($season_dir) = @ARGV;
our $season = substr($season_dir, 0, 2) . substr($season_dir, -2);
$season = 2000 if $season == 1900; # Boundary condition

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

print "#\n# Downloading draft results for '$season'\n#\n";
my $url = "https://records.nhl.com/site/api/draft?sort=[{%22property%22:%22overallPickNumber%22,%22direction%22:%22ASC%22}]&cayenneExp=%20draftYear%20=%20${season}&start=0";
my $local = "$config{'base_dir'}/_data/drafts/$season_dir.json.gz";
print "## From: $url\n## To:   $local\n";
download($url, $local, {'skip-today' => 1, 'gzip' => 1});
