#!/usr/bin/perl -w
our %config;

# Custom LOCALE_CODE_ALPHA_3 country codes we know won't be parsed by Locale::Country
%{$config{'custom_country'}} = (
  'twn' => 'tw',
  'den' => 'dk',
);

# Load contents of a file
sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...

  return $contents;
}

# Return true to pacify the compiler
1;
