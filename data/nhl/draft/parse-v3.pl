#!/usr/bin/perl -w
# Parse the draft results from NHL.com ready to import

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Locale::Country;

# Validate the arguments
#  - there needs to be one: a season
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the parse draft script.\n";
  exit 98;
}

# Declare variables
our ($season_dir) = @ARGV;
our $season = substr($season_dir, 0, 2) . substr($season_dir, -2);
$season = 2000 if $season == 1900; # Boundary condition

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate...
my $file = "$config{'base_dir'}/_data/drafts/$season_dir.json.gz";
if (! -e $file) {
  print STDERR "Unable to parse draft results for $season_dir\n";
  exit 10;
}

print "##\n##\n## Parsing Draft Results from $season_dir (stored with season $season)\n##\n##\n\n";
print STDERR "# Parsing Draft Results: $season_dir\n";

# Now process the selections
my %results = %{decode_json(load_file($file))};
my %rounds = ();
foreach my $sel (@{$results{'data'}}) {
  # Determine the relevant teams
  my $team_id = convert_numeric_team_id($$sel{'draftedByTeamId'});
  my $team_id_orig;
  my @team_id_via = ( );
  foreach my $team_part_raw (split('-', $$sel{'teamPickHistory'})) {
    my $team_part = convert_team_id($team_part_raw);
    if (!defined($team_id_orig)) {
      $team_id_orig = "'$team_part'";
    } elsif ($team_part ne $team_id) {
      push @team_id_via, $team_part;
    }
  }
  $team_id_orig = "'$team_id'" if !defined($team_id_orig);
  my $team_id_via = (@team_id_via ? "'" . join(',', @team_id_via) . "'" : 'NULL');

  # Parse in to selection components
  my $player = convert_text($$sel{'playerName'});
  my $remote_id = $$sel{'id'}; check_for_null(\$remote_id);
  # Position
  my $pos = $$sel{'position'};
  $pos .= 'W' if defined($pos) && $pos =~ /^[LR]$/;
  check_for_null(\$pos);
  # Country
  my $country = $$sel{'countryCode'};
  $country = lc country_code2code($country, LOCALE_CODE_ALPHA_3, LOCALE_CODE_ALPHA_2)
    if defined($country);
  check_for_null(\$country);
  # Height / Weight
  my $height = $$sel{'height'}; check_for_null(\$height);
  my $weight = $$sel{'weight'}; check_for_null(\$weight);
  # Junior League
  my $junior_league = (defined($$sel{'amateurLeague'}) && $$sel{'amateurLeague'} !~ m/^\s*$/) ? "'" . convert_text($$sel{'amateurLeague'}) . "'" : 'NULL';
  # Junior Team
  my $junior_team = (defined($$sel{'amateurTeam'}) && $$sel{'amateurTeam'} !~ m/^\s*$/) ? "'" . convert_text($$sel{'amateurTeam'}) . "'" : 'NULL';

  # Add to the per-round list
  @{$rounds{$$sel{'roundNumber'}}} = ( )
    if !defined($rounds{$$sel{'roundNumber'}});
  push @{$rounds{$$sel{'roundNumber'}}}, "($season, $$sel{'roundNumber'}, $$sel{'overallPickNumber'}, $$sel{'pickInRound'}, '$team_id', $team_id_orig, $team_id_via, $remote_id, NULL, '$player', $pos, $country, $height, $weight, $junior_league, $junior_team)";
}

# Render the results
print "# Clear a previous run\n";
print "DELETE FROM SPORTS_NHL_DRAFT WHERE season = $season;\n\n";
foreach my $r (sort keys %rounds) {
  print "# Round $r\n";
  print "INSERT INTO SPORTS_NHL_DRAFT (season, round, pick, round_pick, team_id, team_id_orig, team_id_via, remote_id, player_id, player, pos, country, height, weight, junior_league, junior_team) VALUES\n  "  . join(",\n  ", @{$rounds{$r}}) . ";\n\n";
}
print "# Table maintenance\n";
print "ALTER TABLE SPORTS_NHL_DRAFT ORDER BY season, round, pick;\n";
