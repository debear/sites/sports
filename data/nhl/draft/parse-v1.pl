#!/usr/bin/perl -w
# Parse the draft results from NHL.com ready to import

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Locale::Country;

# Validate the arguments
#  - there needs to be one: a season
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the parse draft script.\n";
  exit 98;
}

# Declare variables
our ($season_dir) = @ARGV;
our $season = substr($season_dir, 0, 2) . substr($season_dir, -2);
$season = 2000 if $season == 1900; # Boundary condition

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate...
my $file = "$config{'base_dir'}/_data/drafts/$season_dir.htm.gz";
if (! -e $file) {
  print STDERR "Unable to parse draft results for $season_dir\n";
  exit 10;
}

print "##\n##\n## Parsing Draft Results from $season_dir (stored with season $season)\n##\n##\n\n";
print STDERR "# Parsing Draft Results: $season_dir\n";

# Load the file and get the players
my $content = load_file($file);
my $pattern = '<tr><td>(\d+)<\/td><td>(\d+)<\/td><td class="primary-sort-column">(\d+)<\/td><td class="">([^<]*?)<\/td><td class="">([^<]*)</td><td class="">(.*?)<\/td><td class="">([^<]*?)<\/td><td class="">([^<]*)</td><td class="">([^<]*)<\/td><td class="">([^<]*)<\/td><td class="">([^<]*)<\/td><td class="">([^<]*)<\/td><\/tr>';
my @matches = ($content =~ m/$pattern/gsi);

# Then loop through the produce the SQL
my @picks = (); my $last_round;
while (my ($round, $pick_round, $pick_ov, $team_id, $team_id_via, $player, $pos, $country, $height, $weight, $junior_league, $junior_team) = splice(@matches, 0, 12)) {
  # Display a whole round?
  if (defined($last_round) && ($last_round != $round)) {
    print_round($last_round, \@picks);
    @picks = ();
  }

  #
  # Process this pick
  #
  # Team details around the pick
  $team_id = convert_team_id($team_id);
  my $team_id_orig = $team_id;
  if (defined($team_id_via) && $team_id_via !~ m/^\s*$/) {
    # v1: Original and Final
    if ($team_id_via =~ m/from /) {
      ($team_id_orig) = ($team_id_via =~ m/from (.*?)\)/);
      $team_id_orig = convert_team_id($team_id_orig);
      $team_id_via = 'NULL';

    # v2: Last 4 steps
    } else {
      my @team_id_vias = split('-', $team_id_via);
      # Strip the resulting team (we already have, and store in team_id)
      my ($rmv) = splice(@team_id_vias, -1, 1);
      # Strip the initial team (we store in team_id_orig)
      ($team_id_orig) = splice(@team_id_vias, 0, 1);
      $team_id_orig = convert_team_id($team_id_orig);
      # Then any remaining teams go in to our "via" list
      if (@team_id_vias) {
        for (my $i = 0; $i < @team_id_vias; $i++) {
          $team_id_vias[$i] = convert_team_id($team_id_vias[$i]);
        }
        $team_id_via = "'" . join(',', @team_id_vias) . "'";
      } else {
        $team_id_via = 'NULL';
      }
    }
  } else {
    $team_id_via = 'NULL';
  }
  # Player details
  my ($remote_id) = ($player =~ m/"\/ice\/player\.htm\?id=(\d+)"/gsi);
  check_for_null(\$remote_id);
  $player =~ s/(<.*?>)//g;
  trim(\$player);
  $player = convert_text($player);
  # Pos
  check_for_null(\$pos);
  # Country
  if (defined($country) && $country !~ m/^\s*$/) {
    # An exception list...
    $country = lc $country;
    if (defined($config{'custom_country'}{$country})) {
      $country = $config{'custom_country'}{$country};
    } else {
      $country = lc country_code2code($country, LOCALE_CODE_ALPHA_3, LOCALE_CODE_ALPHA_2);
    }
    $country = "'$country'";
  } else {
    $country = 'NULL';
  }
  # Height
  if (defined($height) && $height !~ m/^\s*$/) {
    my ($height_ft, $height_in) = ($height =~ m/^(\d+)' (\d+)"$/);
    $height = "'" . ((12 * $height_ft) + $height_in) . "'";
  } else {
    $height = 'NULL';
  }
  # Weight
  check_for_null(\$weight);
  # Junior Team
  if (defined($junior_league) && $junior_league !~ m/^\s*$/) {
    $junior_league = "'" . convert_text($junior_league) . "'";
  } else {
    $junior_league = 'NULL';
  }
  if (defined($junior_team) && $junior_team !~ m/^\s*$/) {
    $junior_team = "'" . convert_text($junior_team) . "'";
  } else {
    $junior_team = 'NULL';
  }

  #
  # Render
  #
  push @picks, "($season, $round, $pick_ov, $pick_round, '$team_id', '$team_id_orig', $team_id_via, $remote_id, NULL, '$player', $pos, $country, $height, $weight, $junior_league, $junior_team)";
  $last_round = $round;
}

# Output the last round
print_round($last_round, \@picks);
# Then tie the two IDs together
print "# Tie to imported players (if known at this stage)
UPDATE SPORTS_NHL_DRAFT
JOIN SPORTS_NHL_PLAYERS_IMPORT
  ON (SPORTS_NHL_PLAYERS_IMPORT.remote_id = SPORTS_NHL_DRAFT.remote_id)
SET SPORTS_NHL_DRAFT.player_id = SPORTS_NHL_PLAYERS_IMPORT.player_id
WHERE SPORTS_NHL_DRAFT.season = $season;\n\n";
# Table maintenance
print "# Table maintenance\nALTER TABLE SPORTS_NHL_DRAFT ORDER BY season, pick;\n";

# Display the SQL for a single round
sub print_round {
  my ($round, $list) = @_;
  print "# Round $round\nINSERT IGNORE INTO SPORTS_NHL_DRAFT (season, round, pick, round_pick, team_id, team_id_orig, team_id_via, remote_id, player_id, player, pos, country, height, weight, junior_league, junior_team) VALUES\n  "  . join(",\n  ", @$list) . ";\n\n";
}
