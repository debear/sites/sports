#!/usr/bin/perl -w
# Get the draft results from NHL.com, along with CapFriendly for traded selections

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be one: a season
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get draft script.\n";
  exit 98;
}

# Declare variables
our ($season_dir) = @ARGV;
our $season = substr($season_dir, 0, 2) . substr($season_dir, -2);
$season = 2000 if $season == 1900; # Boundary condition

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

print "#\n# Downloading draft results for '$season'\n#\n";
my $dir = "$config{'base_dir'}/_data/drafts/$season_dir";
make_path($dir);

my @files = (
  { 'url' => "https://statsapi.web.nhl.com/api/v1/draft/$season?hydrate=prospects,team", 'local' => 'results.json.gz' },
  { 'url' => "https://www.capfriendly.com/draft/$season", 'local' => 'alt.htm.gz' },
);
foreach my $dl (@files) {
  print "## From: $$dl{'url'}\n## To:   $dir/$$dl{'local'}\n";
  download($$dl{'url'}, "$dir/$$dl{'local'}", {'skip-today' => 1, 'gzip' => 1});
}
