#!/usr/bin/perl -w
# Parse the draft results from NHL.com ready to import

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Locale::Country;

# Validate the arguments
#  - there needs to be one: a season
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the parse draft script.\n";
  exit 98;
}

# Declare variables
our ($season_dir) = @ARGV;
our $season = substr($season_dir, 0, 2) . substr($season_dir, -2);
$season = 2000 if $season == 1900; # Boundary condition

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate...
my $dir = "$config{'base_dir'}/_data/drafts/$season_dir";
my $file = "$dir/results.json.gz";
if (! -e $file) {
  print STDERR "Unable to parse draft results for $season_dir\n";
  exit 10;
}

print "##\n##\n## Parsing Draft Results from $season_dir (stored with season $season)\n##\n##\n\n";
print STDERR "# Parsing Draft Results: $season_dir\n";

my %results = %{decode_json(load_file($file))};
my $alt = load_file("$dir/alt.htm.gz");

print "# Clear a previous run\n";
print "DELETE FROM SPORTS_NHL_DRAFT WHERE season = $season;\n\n";

# Parse the 'alt' file to establish traded pick info
our %cf_teams = ( );
my %traded = ();
foreach my $trade ($alt =~ m/<table class="draft_trade (.*?)<\/table>/gsi) {
  my @teams = ($trade =~ m/\/images\/logos\/(.*?).svg/gsi);
  my ($pick) = ($trade =~ m/<b>$season .*? round pick\*{0,} \(.*? - #(\d+) /gsi);
  # In some scenarios, the trade of this pick may not match because it was not transferred (e.g., 2020 #73)
  next if !defined($pick);
  @{$traded{$pick}} = ()
    if !defined $traded{$pick};
  # In some scenarios, a single team may trade the same pick (so be listed) twice if conditions were not met the first tim (e.g., 2021 #214)
  push @{$traded{$pick}}, convert_capfriendly_logo($teams[$#teams])
    if !@{$traded{$pick}} || ($teams[$#teams] ne $traded{$pick}[$#{$traded{$pick}}]);
}

print "# Convert Cap Friendly team names for traded selections\n";
foreach my $name (sort keys %cf_teams) {
  print "SELECT T.team_id INTO \@cf_$name
FROM SPORTS_NHL_TEAMS AS T
JOIN SPORTS_NHL_TEAMS_GROUPINGS AS TG
  ON (TG.team_id = T.team_id
  AND $season BETWEEN TG.season_from AND IFNULL(TG.season_to, 2099))
WHERE fn_basic_html_comparison(LOWER(REPLACE(CONCAT(T.city, ' ', T.franchise), ' ', '_'))) = '$name';\n";
  print "CALL _exec('SELECT IF(\@cf_$name IS NULL, \"Could not be matched\", CONCAT(\"# Result: \", \@cf_$name)) AS \"# Matching $name\";');\n\n";
}

# Now parse the selections themselves
foreach my $round (@{$results{'drafts'}[0]{'rounds'}}) {
  my @list = ();
  foreach my $sel (@{$$round{'picks'}}) {
    # Determine the relevant teams
    my $team_id = convert_team_id($$sel{'team'}{'abbreviation'});
    my $team_id_orig;
    my @team_id_via = ( );
    if (defined($traded{$$sel{'pickOverall'}})) {
      foreach my $team_alt (reverse(@{$traded{$$sel{'pickOverall'}}})) {
        if (!defined($team_id_orig)) {
          # First team is the original assigned team
          $team_id_orig = $team_alt;
        } else {
          # All others are intermediate teams
          push @team_id_via, $team_alt;
        }
      }
    }
    $team_id_orig = "'$team_id'"
      if !defined($team_id_orig);
    my $team_id_via = (@team_id_via ? 'CONCAT(' . join(', \',\', ', @team_id_via) . ')' : 'NULL');

    # Parse in to selection components
    my $player = convert_text($$sel{'prospect'}{'fullName'});
    my $remote_id = $$sel{'prospect'}{'nhlPlayerId'};
    check_for_null(\$remote_id);
    my $pos = $$sel{'prospect'}{'primaryPosition'}{'code'};
    $pos .= 'W' if defined($pos) && $pos =~ /^[LR]$/;
    check_for_null(\$pos);
    # Country
    my $country = $$sel{'prospect'}{'birthCountry'};
    $country = lc country_code2code($country, LOCALE_CODE_ALPHA_3, LOCALE_CODE_ALPHA_2)
      if defined($country);
    check_for_null(\$country);
    # Height / Weight
    my $height = $$sel{'prospect'}{'height'};
    if (defined($height)) {
      my ($h_ft, $h_in) = ($height =~ m/^(\d+)' (\d+)"$/);
      $height = (12 * $h_ft) + $h_in;
    }
    check_for_null(\$height);
    my $weight = $$sel{'prospect'}{'weight'};
    check_for_null(\$weight);
    # Junior League
    my $junior_league = $$sel{'prospect'}{'amateurLeague'}{'name'};
    if (defined($junior_league) && $junior_league !~ m/^\s*$/) {
      $junior_league = "'" . convert_text($junior_league) . "'";
    } else {
      $junior_league = 'NULL';
    }
    # Junior Team
    my $junior_team = $$sel{'prospect'}{'amateurTeam'}{'name'};
    if (defined($junior_team) && $junior_team !~ m/^\s*$/) {
      $junior_team = "'" . convert_text($junior_team) . "'";
    } else {
      $junior_team = 'NULL';
    }

    # Add to the list for display
    push @list, "($season, $$round{'roundNumber'}, $$sel{'pickOverall'}, $$sel{'pickInRound'}, '$team_id', $team_id_orig, $team_id_via, $remote_id, NULL, '$player', $pos, $country, $height, $weight, $junior_league, $junior_team)";
  }
  print "# Round $$round{'roundNumber'}\nINSERT INTO SPORTS_NHL_DRAFT (season, round, pick, round_pick, team_id, team_id_orig, team_id_via, remote_id, player_id, player, pos, country, height, weight, junior_league, junior_team) VALUES\n  "  . join(",\n  ", @list) . ";\n\n";
}

print "# Table maintenance\n";
print "ALTER TABLE SPORTS_NHL_DRAFT ORDER BY season, round, pick;\n";

# Convert a CapFriendly team logo to an internal team_id
sub convert_capfriendly_logo {
  my ($img) = @_;
  $img =~ s/[_\d]+$//g;
  $cf_teams{$img} = 1;
  return "\@cf_$img";
}
