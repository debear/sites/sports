#!/usr/bin/perl -w
# Read the game info and convert to SQL

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config{'script_dir'} = __DIR__;
$config{'debug'} = 0; # Display script to run, rather than actually running it...

# Local config
$config = $config{'script_dir'} . '/parse.config.pl';
require $config;

print "##\n##\n## Table Maintenance\n##\n##\n\n";

# Fudge the required args
my @time = localtime();
our $season = ($time[4] > 7 ? 1900 : 1899) + $time[5];
our $season_dir = sprintf('%04d-%02d', $season, ($season + 1) % 100);
our $game_type = 'regular';
our $game_id = 1;

# Load local config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Run
foreach my $script (@{$config{'scripts'}}) {
  run_script($script, '--order');
}

print "##\n## Table Maintenance Complete\n##\n\n";

