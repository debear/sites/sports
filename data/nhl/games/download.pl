#!/usr/bin/perl -w
# Get game data for a single game from NHL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use JSON;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get game script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);
my $game_type_code = convert_game_type($game_type);
my $game_id_fmted = sprintf('%04d', $game_id);
my $season_full = sprintf('%04d%04d', $season, $season + 1);

my $game_info = get_game_info(@ARGV);
my $dir = sprintf('%s/_data/%s/games/%s/%s', $config{'base_dir'}, $season_dir, $game_type, $game_id_fmted);

# Action flags
my $lineup_only = grep(/^--lineup-only$/, @ARGV);

# Downloading specific components only?
our %components = ();
if ($lineup_only) {
  $components{'rosters'} = 1;
  $components{'teams_home'} = 1;
  $components{'teams_visitor'} = 1;
}
$components{'_set'} = (scalar keys %components > 0);

# Load local config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Run...
print '# Downloading ' . ucfirst($game_type) . " Game $game_id" . ($game_type eq 'playoff' ? ' (' . playoff_summary($season, $game_id) . ')' : '') . ": $$game_info{'visitor_id'} @ $$game_info{'home_id'}: ";

# Skip if already done and we're not after specific component(s)
if (-e "$dir/play_by_play.htm.gz" && !$components{'_set'}) {
  print "[ Skipped ]\n";
  exit;
}

# Make sure the game dir exists
make_path($dir);

# v2 gamedata (that may be needed later)
if ($config{'integration_version'} == 2) {
  # Main game data
  download_file("https://statsapi.web.nhl.com/api/v1/game/${season}${game_type_code}${game_id_fmted}/content?site=en_nhl", 'content', 'json');
  download_file("https://statsapi.web.nhl.com/api/v1/game/${season}${game_type_code}${game_id_fmted}/feed/live?site=en_nhl", 'gamedata', 'json');

  my $teamIds = convert_team_id_to_numeric($$game_info{'visitor_id'}) . ',' . convert_team_id_to_numeric($$game_info{'home_id'});
  download_file("https://statsapi.web.nhl.com/api/v1/teams?site=en_nhl&teamId=$teamIds&expand=team.roster,roster.person", 'teams', 'json');

# v3 gamedata (that may be needed later)
} elsif ($config{'integration_version'} == 3) {
  download_file("https://api-web.nhle.com/v1/gamecenter/${season}${game_type_code}${game_id_fmted}/landing", 'summary', 'json');
  download_file("https://api-web.nhle.com/v1/gamecenter/${season}${game_type_code}${game_id_fmted}/boxscore", 'boxscore', 'json');
  download_file("https://api-web.nhle.com/v1/gamecenter/${season}${game_type_code}${game_id_fmted}/play-by-play", 'play_by_play', 'json');
  my $home_id = convert_team_id_to_remote($$game_info{'home_id'});
  download_file("https://api-web.nhle.com/v1/roster/${home_id}/${season_full}", 'teams_home', 'json');
  my $visitor_id = convert_team_id_to_remote($$game_info{'visitor_id'});
  download_file("https://api-web.nhle.com/v1/roster/${visitor_id}/${season_full}", 'teams_visitor', 'json');
}

# Player info
download_file("http://www.nhl.com/scores/htmlreports/${season_full}/TV${game_type_code}${game_id_fmted}.HTM", 'toi_visitor');
download_file("http://www.nhl.com/scores/htmlreports/${season_full}/TH${game_type_code}${game_id_fmted}.HTM", 'toi_home');
download_file("http://www.nhl.com/scores/htmlreports/${season_full}/RO${game_type_code}${game_id_fmted}.HTM", 'rosters');

# Game events
if ($config{'integration_version'} == 1) {
  download_file("http://www.nhl.com/gamecenter/en/boxscore?id=${season}${game_type_code}${game_id_fmted}", 'boxscore');
}
download_file("http://www.nhl.com/scores/htmlreports/${season_full}/PL${game_type_code}${game_id_fmted}.HTM", 'play_by_play');

# Game summary
download_file("http://www.nhl.com/scores/htmlreports/${season_full}/GS${game_type_code}${game_id_fmted}.HTM", 'game_summary');
download_file("http://www.nhl.com/scores/htmlreports/${season_full}/ES${game_type_code}${game_id_fmted}.HTM", 'event_summary');

print "[ Done ]\n";

# Perform a download
sub download_file {
  our %components;
  my ($url, $type, $ext) = @_;

  # Skip if this component is not to be downloaded
  my $defined_component = defined($components{$type});
  return
    if $components{'_set'} && (!$defined_component || !$components{$type});

  # Perform
  my $local = "$dir/$type." . (defined($ext) ? $ext : 'htm') . '.gz';
  download($url, $local, {'if-modified-since' => 1, 'gzip' => 1});
}
