#!/usr/bin/perl -w
# Load through the various games and generate the SQL to import them in to the database

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse game script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config = abs_path(__DIR__ . '/parse.config.pl');
require $config;

$config{'debug'} = 0; # Display script to run, rather than actually running it...

# Declare variables
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

# Load local config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "##\n##\n## Loading $game_type, Game $game_id" . ($game_type eq 'playoff' ? ' (' . playoff_summary($season, $game_id) . ')' : '') . "\n##\n##\n\n";
print STDERR "# Loading Game: $season " . ucfirst($game_type) . ", Game $game_id\n";

# Verify its size (to ensure a game was played!)
my $file = "$config{'base_dir'}/_data/$season_dir/games/$game_type/" . sprintf('%04d', $game_id) . '/play_by_play.htm.gz';
my @stat = stat $file;
if (!defined($stat[7]) || $stat[7] <= 256) {
  print STDERR "No game info available\n";
  exit 10;

# A check, to see if we have to use summarised data over the full play-by-play driven data
} elsif ($stat[7] <= 1000) {
  # Too small to contain the full stats, so switch the scripts
  @{$config{'scripts'}} = ( { 'script' => 'summarised', 'disp' => 'Summarised Data', 'versioned' => 1 } );
}

# Loop one: clear a previous run...
print "##\n## Reset\n##\n\n";
foreach my $script (@{$config{'scripts'}}) {
  run_script($script, '--reset');
}

# Loop two: run!
print "\n##\n## Run\n##\n\n";
foreach my $script (@{$config{'scripts'}}) {
  run_script($script);
}

print "##\n## Game $game_type, Game $game_id Done\n##\n\n";

