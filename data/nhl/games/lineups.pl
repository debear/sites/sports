#!/usr/bin/perl -w
# After the data has been loaded, update any problematic roster players that we couldn't find

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use DBI;
use JSON;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config{'is_unattended'} = grep(/^--unattended$/, @ARGV); # No user interaction expected

#
# Connect to the database
#
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql; my $sth; my $sub_sth; my $ret;

#
# Update both issue lists based on the information we have
#
print '... Updating on known information:' . "\n";
# First, LINEUP_ISSUES from itself
$sql = 'UPDATE SPORTS_NHL_GAME_LINEUP_ISSUES AS A
LEFT JOIN SPORTS_NHL_GAME_LINEUP_ISSUES AS B
  ON (A.player_name = B.player_name
  AND A.team_id = B.team_id
  AND A.jersey = B.jersey
  AND A.pos = B.pos)
SET A.player_id = B.player_id
WHERE A.player_id IS NULL
AND   B.player_id IS NOT NULL;';
print '...... LINEUP_ISSUES -> LINEUP_ISSUES: ';
$sth = $dbh->prepare($sql);
$ret = $sth->execute;
print $ret . ' changes made' . "\n";

# Next, SCRATCH_ISSUES from itself
$sql =~ s/SPORTS_NHL_GAME_LINEUP_ISSUES/SPORTS_NHL_GAME_SCRATCHES_ISSUES/g;
print '...... SCRATCHES_ISSUES -> SCRATCHES_ISSUES: ';
$sth = $dbh->prepare($sql);
$ret = $sth->execute;
print $ret . ' changes made' . "\n";

# Then LINEUP_ISSUES from SCRATCH_ISSUES
$sql =~ s/SPORTS_NHL_GAME_SCRATCHES_ISSUES AS A/SPORTS_NHL_GAME_LINEUP_ISSUES AS A/g;
print '...... SCRATCHES_ISSUES -> LINEUP_ISSUES: ';
$sth = $dbh->prepare($sql);
$ret = $sth->execute;
print $ret . ' changes made' . "\n";

# Finally SCRATCH_ISSUES from ROSTER_ISSUES
$sql =~ s/SPORTS_NHL_GAME_LINEUP_ISSUES AS A/SPORTS_NHL_GAME_SCRATCHES_ISSUES AS A/g;
$sql =~ s/SPORTS_NHL_GAME_SCRATCHES_ISSUES AS B/SPORTS_NHL_GAME_LINEUP_ISSUES AS B/g;
my $saved_sql = $sql;
print '...... LINEUP_ISSUES -> SCRATCHES_ISSUES: ';
$sth = $dbh->prepare($sql);
$ret = $sth->execute;
print $ret . ' changes made' . "\n";
print '... [ Done ]' . "\n";

#
# Prepare our queries for new players
#
our %remote_searches;
my $new_sth = $dbh->prepare('SELECT MAX(player_id) + 1 AS new_player_id FROM SPORTS_NHL_PLAYERS;');
my $ins_sth = $dbh->prepare('INSERT INTO SPORTS_NHL_PLAYERS_IMPORT (player_id, remote_id) VALUES (?, ?);');
my $ply_sth = $dbh->prepare('INSERT INTO SPORTS_NHL_PLAYERS (player_id, first_name, surname) VALUES (?, ?, ?);');

#
# Identify the players who we still haven't matched
#
print '... Identify remaining players:' . "\n";
$sql = 'SELECT player_name,
       team_id,
       jersey,
       pos,
       problem,
       COUNT(*) AS num_instances
FROM {TABLE}
WHERE player_id IS NULL
GROUP BY player_name,
         team_id,
         jersey,
         pos
ORDER BY num_instances DESC, player_name;';
my $match_sql = 'SELECT player_id, first_name, surname FROM SPORTS_NHL_PLAYERS WHERE surname LIKE ? ORDER BY surname, first_name;';
my $match_sth = $dbh->prepare($match_sql);
my @acceptable = ( 'I', 'Imported', 'R', 'Remote' );
my @tables = ( 'SPORTS_NHL_GAME_LINEUP_ISSUES', 'SPORTS_NHL_GAME_SCRATCHES_ISSUES' );

# Pass One: Verify (only if we are in unattended mode, as we will not have the required tty)
if ($config{'is_unattended'}) {
  # Determine
  my $links_rqd = 0;
  foreach my $tbl (@tables) {
    my $sub_sql = $sql;
    $sub_sql =~ s/{TABLE}/$tbl/g;
    $sth = $dbh->prepare($sub_sql);
    $sth->execute;
    $links_rqd += $sth->rows;
  }

  # Flag, if player(s) require linking
  if ($links_rqd) {
    my $s = ($links_rqd == 1 ? '' : 's');
    print STDERR "Unable to proceed with player linking: $links_rqd player$s require manual input and script has been run in unattended mode.\n";
    # Exit is in core part, so flag accordingly
    exit $config{'exit_codes'}{'core'};
  }
}

# Pass Two: Run
for (my $i = 0; $i < @tables; $i++) {
  my $sub_sql = $sql;
  $sub_sql =~ s/{TABLE}/$tables[$i]/g;
  $sth = $dbh->prepare($sub_sql);
  $sth->execute;

  $sub_sql = 'UPDATE ' . $tables[$i] . '
SET player_id = ?
WHERE player_name = ?
AND   team_id = ?
AND   jersey = ?
AND   pos = ?;';
  $sub_sth = $dbh->prepare($sub_sql);

  while (my $row = $sth->fetchrow_hashref) {
    print '    -> ' . decode_text($$row{'player_name'}) . ' (' . $$row{'team_id'} . ' #' . $$row{'jersey'} . ', ' . $$row{'pos'} . ') found with ' . $$row{'num_instances'} . ' instance(s) of "' . $$row{'problem'} . '".' . "\n";

    # Try and guess from our own database and NHL.com, using first 4 chars of surname
    guess_ids($$row{'player_name'});

    # Identify the type of ID
    print '       Please enter the ID type [(I)mported/(R)emote]: ';
    my $import_type = <STDIN>;
    chomp($import_type);
    while (!grep $_ eq $import_type, @acceptable) {
      print '        - "' . $import_type . '" is not a valid response.  Please try again: ';
      $import_type = <STDIN>;
      chomp($import_type);
    }

    # Now get the ID
    print '       Please now enter the ID: ';
    my $import_id = <STDIN>;
    chomp($import_id);
    while ($import_id !~ /^\d+$/) {
      print '        - "' . $import_id . '" is not a valid response.  Please try again: ';
      $import_id = <STDIN>;
      chomp($import_id);
    }
    print "\n";

    # If a new player, create appropriate database links
    if ($import_type =~ /^R/) {
      # If a remote player, mark for download
      my $remote_id = $import_id;
      $new_sth->execute;
      ($import_id) = $new_sth->fetchrow_array;
      $ins_sth->execute($import_id, $remote_id);

      # Create a rudimentary player record
      #  (Name already escaped when row created in SPORTS_NHL_GAME_%_ISSUES)
      my @name = split(' ', $$row{'player_name'});
      my $fname = ucfirst lc shift @name;
      my $sname = ucfirst lc join(' ', @name);
      $ply_sth->execute($import_id, $fname, $sname);
    }

    # Update
    $ret = $sub_sth->execute($import_id, $$row{'player_name'}, $$row{'team_id'}, $$row{'jersey'}, $$row{'pos'});
  }

  # Shortcut any changes from SPORTS_NHL_GAME_LINEUP_ISSUES into SPORTS_NHL_GAME_SCRATCHES_ISSUES
  if (!$i) {
    $sub_sth = $dbh->prepare($saved_sql);
    $ret = $sub_sth->execute;
  }
}
print '... [ Done ]' . "\n";

#
# Update LINEUP and SCRATCHES based on *_ISSUES
#
print '... Update LINEUP from LINEUP_ISSUES: ';
$sql = 'INSERT IGNORE INTO SPORTS_NHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started)
  SELECT season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started
  FROM SPORTS_NHL_GAME_LINEUP_ISSUES
  WHERE player_id IS NOT NULL;';
$sth = $dbh->prepare($sql);
$ret = $sth->execute;
print $ret . ' changes made [ Done ]' . "\n";

print '... Update SCRATCHES from SCRATCHES_ISSUES: ';
$sql =~ s/SPORTS_NHL_GAME_LINEUP/SPORTS_NHL_GAME_SCRATCHES/g;
$sql =~ s/, capt_status, started//g;
$sth = $dbh->prepare($sql);
$ret = $sth->execute;
print $ret . ' changes made [ Done ]' . "\n";

#
# Record
#
print '... Confirm lineup updates in SPORTS_NHL_SCHEDULE: ';
$sql = 'UPDATE SPORTS_NHL_SCHEDULE SET lineup_linked = 1 WHERE status IS NOT NULL AND status NOT IN ("PPD", "CNC") AND IFNULL(lineup_linked, 0) = 0;';
$sth = $dbh->prepare($sql);
$ret = $sth->execute;
print $ret . ' changes made [ Done ]' . "\n";

# Disconnect from the database
undef $sth if defined($sth);
undef $new_sth if defined($new_sth);
undef $ins_sth if defined($ins_sth);
undef $ply_sth if defined($ply_sth);
undef $match_sth if defined($match_sth);
undef $sub_sth if defined($sub_sth);
$dbh->disconnect if defined($dbh);

# Try and guess from our own database and NHL.com, using first 4 chars of surname
sub guess_ids {
  my ($player_name) = @_;
  my @name_split = split ' ', $player_name;
  my $sname = lc join(' ', splice(@name_split, 1));
  my $search = substr($sname, 0, 4);
  my $num = 0;

  # Imported (Local) Matches
  print '       Imported Matches:' . "\n";
  $match_sth->execute($search . '%');
  while (my $match_row = $match_sth->fetchrow_hashref) {
    print "        - $$match_row{'first_name'} $$match_row{'surname'}, $$match_row{'player_id'}\n";
    $num++;
  }
  print '        - None' . "\n"
    if !$num;

  # NHL.com (Remote) Matches
  $num = 0;
  my $sname_search = substr($sname, 0, 3);
  my $sname_uri = uri_escape($sname_search);
  print '       NHL.com Matches:' . "\n";
  $remote_searches{$sname} = download("https://search.d3.nhle.com/api/v1/search/player?culture=en-us&limit=50&q=$sname_uri%2A&active=true")
    if !defined($remote_searches{$sname});
  $remote_searches{$sname} = decode_json($remote_searches{$sname});

  foreach my $match (@{$remote_searches{$sname}}) {
    next if $$match{'name'} !~ m/ $sname_search/i;
    print "        - $$match{'playerId'} => $$match{'name'}\n";
    $num++;
  }
  print '        - None' . "\n"
    if !$num;

  print "\n";
}

