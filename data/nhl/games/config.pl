#!/usr/bin/perl -w
# Perl config file
use JSON;

# Determine the game integration version
our ($season, $game_type, $game_id, %config);
if ($season <= 2014 || ($season == 2015 && defined($game_type) && $game_type eq 'regular' && defined($game_id) && $game_id < 730 && $game_id !~ /^(704|716|720)$/)) {
  $config{'integration_version'} = 1;
} elsif ($season < 2023 || ($season == 2023 && defined($game_type) && $game_type eq 'regular' && defined($game_id) && $game_id < 180)) {
  $config{'integration_version'} = 2;
} else {
  $config{'integration_version'} = 3;
}

# Validate and load a file
sub load_file {
  my ($season_dir, $game_type, $game_id, $filename, $args) = @_;

  # Validate
  my $ext = (defined($args) && defined($$args{'ext'}) ? $$args{'ext'} : 'htm');
  my $file = sprintf('%s/_data/%s/games/%s/%04d/%s.%s.gz',
                     $config{'base_dir'},
                     $season_dir,
                     $game_type,
                     $game_id,
                     $filename,
                     $ext);
  if (! -e $file) {
    my $msg = "File '$file' does not exist, unable to continue.";
    if (!$$args{'no_exit_on_error'}) {
      print STDERR "$msg\n";
      exit 10;
    } else {
      print STDERR "# $msg\n";
      return ($file, undef);
    }
  }

  # Load the file
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = (); # Garbage collect...
  $contents = decode_json($contents) if $ext eq 'json' && !defined($$args{'no-decode'}); # If the content is JSON, decode it in to an object first
  $contents = Encode::decode('UTF-8', $contents) if ref($contents) =~ /^(|SCALAR)$/; # i.e., implicit scalar

  return ($file, $contents);
}

# Return true to pacify the compiler
1;
