#!/usr/bin/perl -w
# Config for the game parsing scripts

@{$config{'scripts'}} = ( { 'script' => 'rosters',      'disp' => 'Rosters',      'versioned' => 1 },
                          { 'script' => 'toi',          'disp' => 'Time on Ice',  'versioned' => 0 },
                          { 'script' => 'summary',      'disp' => 'Game Summary', 'versioned' => 0 },
                          { 'script' => 'play_by_play', 'disp' => 'Play by Play', 'versioned' => 1 } );

sub run_script {
  my ($script, $extra) = @_;

  my $v = ($$script{'versioned'} ? '-v' . $config{'integration_version'} : '');
  my $full_script = "$config{'base_dir'}/games/parse/$$script{'script'}$v.pl";
  our ($season_dir, $game_type, $game_id);
  foreach my $arg ($season_dir, $game_type, $game_id, $extra) {
    $full_script .= ' ' . $arg if defined($arg);
  }

  print "##\n## $$script{'disp'}\n##\n";
  print "# Running: \`$full_script\`" if $config{'debug'};
  print `$full_script` if !$config{'debug'};
  print "\n";
}

# Return true to pacify the compiler
1;
