#!/usr/bin/perl -w
# Load the player totals and game info from the NHL.com boxscore, rather than play-by-play, roster, etc, gamesheets

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse play by play totals script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate this is the right version
validate_integration_version(2);

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_EVENT', 'SPORTS_NHL_GAME_EVENT_GOAL', 'SPORTS_NHL_GAME_EVENT_PENALTY', 'SPORTS_NHL_GAME_EVENT_SHOOTOUT', 'SPORTS_NHL_GAME_LINEUP', 'SPORTS_NHL_GAME_LINEUP_ISSUES', 'SPORTS_NHL_GAME_OFFICIALS', 'SPORTS_NHL_GAME_PP_STATS', 'SPORTS_NHL_GAME_SCRATCHES', 'SPORTS_NHL_GAME_SCRATCHES_ISSUES', 'SPORTS_NHL_GAME_SUMMARISED', 'SPORTS_NHL_PLAYERS_GAME_GOALIES', 'SPORTS_NHL_PLAYERS_GAME_SKATERS' );

  clear_previous($season, $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  #reorder_tables(@tables) if $ARGV[3] eq '--order'; # Do not run for performance reasons
  exit;
}

# First check, does the file exist, and if it does, load it
my ($gamedata_file, $gamedata) = load_file($season_dir, $game_type, $game_id, 'gamedata', { 'ext' => 'json' });
my @all_plays = @{$$gamedata{'liveData'}{'plays'}{'allPlays'}};
my $contents;
print "# Loading summarised game data for $season // $game_type // $game_id\n\n";

# Rosters
parse_rosters() if check_disp(1);

# Scoring
parse_scoring() if check_disp(2);

# Shootout
parse_shootout() if check_disp(3);

# Penalties
parse_penalties() if check_disp(4);

# Officials
parse_officials() if check_disp(5);

# PP Stats
parse_stats_pp() if check_disp(6);

# Player Stats
parse_boxscore() if check_disp(7);

# Result
parse_result() if check_disp(8);

# Three Stars
parse_three_stars() if check_disp(9);

# Misc
parse_misc() if check_disp(10);

##
## Rosters (inc Scratches)
##
sub parse_rosters {
  print "##\n## Lineup\n##\n\n";

  # Vars for storing...
  our %rosters = ( );

  # Loop through the two teams
  foreach my $type ('away', 'home') {
    my $type_alt = ($type eq 'away' ? 'visitor' : $type);
    my $team_id = $$game_info{"${type_alt}_id"};
    print "#\n# " . ucfirst($type) . "\n#\n";

    # Get the info
    my @goalies = @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$type}{'goalies'}};
    my @skaters = @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$type}{'skaters'}};
    my @all = @skaters;
    push @all, @goalies;
    my @scratches = @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$type}{'scratches'}};
    my %scratches = map { $_ => 1 } @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$type}{'scratches'}};

    # Loop through the active players
    print "# Active\n";
    foreach my $id (@all) {
      # Skip those in the scratches list
      next if exists($scratches{$id});

      # Log info
      my %player = %{$$gamedata{'gameData'}{'players'}{"ID$id"}};
      my %info = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$type}{'players'}{"ID$id"}};
      my %stats = %{$info{'stats'}};
      %{$stats{'penalties'}} = ( 'taken' => 0, 'drawn' => 0 );
      %{$stats{'so'}} = ( 'goals' => 0, 'shots' => 0 );
      %{$rosters{$id}} = ( 'team_id' => $team_id, 'jersey' => $info{'jerseyNumber'}, 'stats' => \%stats );

      # Display
      print "\n# $player{'fullName'}, #$info{'jerseyNumber'} ($info{'position'}{'abbreviation'})\n";
      $player{'fullName'} = convert_text(uc $player{'fullName'});
      print "INSERT INTO SPORTS_NHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started)
  SELECT '$season', '$game_type', '$game_id', '$team_id', '$info{'jerseyNumber'}', player_id, '$info{'position'}{'code'}', NULL, '0'
  FROM SPORTS_NHL_PLAYERS_IMPORT
  WHERE remote_id = '$id';\n";
      print "INSERT INTO SPORTS_NHL_GAME_LINEUP_ISSUES (season, game_type, game_id, team_id, jersey, player_name, pos, capt_status, started, problem)
  SELECT '$season', '$game_type', '$game_id', '$team_id', '$info{'jerseyNumber'}', '$player{'fullName'}', '$info{'position'}{'code'}', NULL, '0', '0 matches' AS problem
  FROM SPORTS_NHL_SCHEDULE
  LEFT JOIN SPORTS_NHL_PLAYERS_IMPORT
    ON (remote_id = '$id')
  WHERE SPORTS_NHL_SCHEDULE.season = '$season'
  AND   SPORTS_NHL_SCHEDULE.game_type = '$game_type'
  AND   SPORTS_NHL_SCHEDULE.game_id = '$game_id'
  AND   SPORTS_NHL_PLAYERS_IMPORT.remote_id IS NULL;\n";
    }

    # Then the scratches...
    print "\n# Scratches\n";
    foreach my $id (@scratches) {
      # Log info
      my %player = %{$$gamedata{'gameData'}{'players'}{"ID$id"}};
      my %info = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$type}{'players'}{"ID$id"}};
      %{$rosters{$id}} = ( 'team_id' => $team_id, 'jersey' => $info{'jerseyNumber'} );

      # Display
      print "\n# $player{'fullName'}, #$info{'jerseyNumber'} ($info{'position'}{'abbreviation'})\n";
      $player{'fullName'} = convert_text(uc $player{'fullName'});
      print "INSERT INTO SPORTS_NHL_GAME_SCRATCHES (season, game_type, game_id, team_id, jersey, player_id, pos)
  SELECT '$season', '$game_type', '$game_id', '$team_id', '$info{'jerseyNumber'}', player_id, NULL
  FROM SPORTS_NHL_PLAYERS_IMPORT
  WHERE remote_id = '$id';\n";
      print "INSERT INTO SPORTS_NHL_GAME_SCRATCHES_ISSUES (season, game_type, game_id, team_id, jersey, player_name, pos, problem)
  SELECT '$season', '$game_type', '$game_id', '$team_id', '$info{'jerseyNumber'}', '$player{'fullName'}', NULL, '0 matches' AS problem
  FROM SPORTS_NHL_SCHEDULE
  LEFT JOIN SPORTS_NHL_PLAYERS_IMPORT
    ON (remote_id = '$id')
  WHERE SPORTS_NHL_SCHEDULE.season = '$season'
  AND   SPORTS_NHL_SCHEDULE.game_type = '$game_type'
  AND   SPORTS_NHL_SCHEDULE.game_id = '$game_id'
  AND   SPORTS_NHL_PLAYERS_IMPORT.remote_id IS NULL;\n";
    }
    print "\n";
  }
}

##
## Scoring
##
sub parse_scoring {
  print "##\n## Scoring\n##\n";

  our %rosters;
  my @scoring_plays = @{$$gamedata{'liveData'}{'plays'}{'scoringPlays'}};

  my @events = ( ); my @goals = ( );
  foreach my $event_id (@scoring_plays) {
    my %info = %{$all_plays[$event_id]};
    my $period = $info{'about'}{'period'};

    # Skip marked shootout goals
    next if $period == 5;

    # Time needs reversing (e.g., if marked as 05:12 we represent it as 14:48 - JSON has time from start of period, play-by-play version uses time left in period)
    my ($time_m, $time_s) = split /:/, $info{'about'}{'periodTime'};
    my $time = sprintf('%02d:%02d', 20 - $time_m - ($time_s ne '00' ? 1 : 0), $time_s ne '00' ? 60 - $time_s : 0);

    # Other event info
    my $team_id = ($info{'team'}{'id'} == $$gamedata{'gameData'}{'teams'}{'away'}{'id'} ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
    my $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
    my $team_type = ($team_id eq $$game_info{'visitor_id'} ? 'away' : 'home');
    my $goal_type = "'EV'";
    $goal_type = "'PP'" if $info{'result'}{'strength'}{'code'} eq 'PPG';
    $goal_type = "'SH'" if $info{'result'}{'strength'}{'code'} eq 'SHG';
    my $shot_type = 'NULL';
    $shot_type = "'" . convert_text($info{'result'}{'secondaryType'}) . "'" if defined($info{'result'}{'secondaryType'});
    # Things we won't know:
    my $en = 'NULL';
    my $ps = 'NULL';

    # Scorers
    my $g = 'NULL'; my $a1 = 'NULL'; my $a2 = 'NULL';
    foreach my $s (@{$info{'players'}}) {
      my $jersey = $rosters{$$s{'player'}{'id'}}{'jersey'};
      if ($g eq 'NULL') {
        $g = $jersey;
      } elsif ($a1 eq 'NULL') {
        $a1 = $jersey;
      } else {
        $a2 = $jersey;
      }
    }

    # Summarise
    my %extra = ();
    my $play = '';
    # Type
    $play .= 'Power Play '
      if $goal_type eq "'PP'";
    $play .= 'Shorthanded '
      if $goal_type eq "'SH'";
    # Scorer
    $play .= "Goal, scored by {{${team_id}_$g}}";
    # Assists
    push @{$extra{'assists'}}, "{{${team_id}_$a1}}"
      if $a1 ne 'NULL';
    push @{$extra{'assists'}}, "{{${team_id}_$a2}}"
      if $a2 ne 'NULL';
    $play .= ' assisted by ' . join(' and ', @{$extra{'assists'}})
      if ($extra{'assists'}) && @{$extra{'assists'}};

    # Store
    push @events, "('$season', '$game_type', '$game_id', '$event_id', '$period', '00:$time', 'GOAL', NULL, '$play')";
    push @goals, "('$season', '$game_type', '$game_id', '$event_id', '$team_id', '$info{'about'}{'goals'}{$team_type}', $g, $a1, $a2, '$opp_team_id', NULL, $shot_type, NULL, $goal_type, $en, $ps)";
  }

  print "INSERT INTO SPORTS_NHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, zone, play) VALUES " . join(', ', @events) . ";\n\n" if @events > 0;
  print "INSERT INTO SPORTS_NHL_GAME_EVENT_GOAL (season, game_type, game_id, event_id, by_team_id, goal_num, scorer, assist_1, assist_2, on_team_id, on_jersey, shot_type, shot_distance, goal_type, goalie_status, penalty_shot) VALUES " . join(', ', @goals) . ";\n\n" if @goals > 0;
}

##
## Shootout
##
sub parse_shootout {
  # Skip if the game didn't go to a shootout
  return if $$gamedata{'liveData'}{'linescore'}{'currentPeriod'} < 5;

  # Load
  print "##\n## Shootout\n##\n";
  our %rosters;
  my @events = ( ); my @shootout = ( );
  my @shootout_plays = @{$$gamedata{'liveData'}{'plays'}{'playsByPeriod'}[4]{'plays'}};
  foreach my $event_id (@shootout_plays) {
    my %info = %{$all_plays[$event_id]};

    # Status (helps us weed out the header/footer plays)
    my $status = '';
    $status = 'goal' if $info{'result'}{'eventTypeId'} eq 'GOAL';
    $status = 'save' if $info{'result'}{'eventTypeId'} eq 'SHOT';
    $status = 'goalpost' if $info{'result'}{'description'} =~ /Goalpost/i;
    $status = 'miss' if $info{'result'}{'eventTypeId'} eq 'MISSED_SHOT' && $status eq '';
    next if $status eq '';

    # Shooter / Goalie, identifying teams too
    my $scorer = 'NULL'; my $scorer_id; my $goalie = 'NULL';
    my $team_id; my $opp_team_id;
    foreach my $p (@{$info{'players'}}) {
      my %r = %{$rosters{$$p{'player'}{'id'}}};
      if ($$p{'playerType'} ne 'Goalie') {
        $scorer_id = $$p{'player'}{'id'};
        $scorer = $r{'jersey'};
        $team_id = $r{'team_id'};
        $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
        $rosters{$scorer_id}{'stats'}{'so'}{'shots'}++;
      } else {
        $goalie = $r{'jersey'};
      }
    }
    # Summarise
    my $play = '{{' . $team_id . '_' . $scorer . '}} ';
    $play .= 'scored' if $status eq 'goal';
    $play .= 'saved' if $status eq 'save';
    $play .= 'hit goal post' if $status eq 'goalpost';
    $play .= 'missed net' if $status eq 'miss';
    if ($status eq 'goal') {
      $rosters{$scorer_id}{'stats'}{'so'}{'goals'}++;
    }
    # Shot type
    my $shot_type = (defined($info{'result'}{'secondaryType'}) ? "'" . convert_text($info{'result'}{'secondaryType'}) . '"' : 'NULL');

    $scorer = "'$scorer'" if $scorer ne 'NULL';
    $goalie = "'$goalie'" if $goalie ne 'NULL';

    push @events, "('$season', '$game_type', '$game_id', '$event_id', '5', '00:00:00', 'SHOOTOUT', '$play')";
    push @shootout, "('$season', '$game_type', '$game_id', '$event_id, '$team_id', $scorer, '$opp_team_id', $goalie, $shot_type, NULL, '$status')";
  }

  print "INSERT INTO SPORTS_NHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, zone, play) VALUES " . join(', ', @events) . ";\n\n" if @events > 0;
  print "INSERT INTO SPORTS_NHL_GAME_EVENT_SHOOTOUT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, shot_type, shot_distance, status) VALUES " . join(', ', @shootout) . ";\n\n" if @shootout > 0;
}

##
## Penalties
##
sub parse_penalties {
  print "##\n## Penalties\n##\n";

  our %rosters;
  my @events = ( ); my @penalties = ( );
  my @penalty_plays = @{$$gamedata{'liveData'}{'plays'}{'penaltyPlays'}};
  foreach my $event_id (@penalty_plays) {
    my %info = %{$all_plays[$event_id]};
    my $period = $info{'about'}{'period'};

    # Time needs reversing (e.g., if marked as 05:12 we represent it as 14:48 - JSON has time from start of period, play-by-play version uses time left in period)
    my ($time_m, $time_s) = split /:/, $info{'about'}{'periodTime'};
    my $time = sprintf('%02d:%02d', 20 - $time_m - ($time_s ne '00' ? 1 : 0), $time_s ne '00' ? 60 - $time_s : 0);

    # Other event info
    my $team_id = ($info{'team'}{'id'} == $$gamedata{'gameData'}{'teams'}{'away'}{'id'} ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
    my $opp_team_id = ($team_id eq $$game_info{'visitor_id'} ? $$game_info{'home_id'} : $$game_info{'visitor_id'});

    # Player / Served By / Drawn By
    my $player = 'NULL'; my $served = 'NULL'; my $drawn = 'NULL';
    foreach my $p (@{$info{'players'}}) {
      my %r = %{$rosters{$$p{'player'}{'id'}}};
      if ($$p{'playerType'} eq 'PenaltyOn') {
        if ($info{'result'}{'penaltySeverity'} ne 'Bench Minor') {
          $rosters{$$p{'player'}{'id'}}{'stats'}{'penalties'}{'taken'}++;
          $player = $r{'jersey'};
          $served = $player;
        } else {
          $player = 0;
          $served = $r{'jersey'};
        }
      } elsif ($$p{'playerType'} eq 'DrewBy') {
        $rosters{$$p{'player'}{'id'}}{'stats'}{'penalties'}{'drawn'}++;
        $drawn = $r{'jersey'};
      }
    }

    # Reason, Duration
    my $reason = parse_penalty_type($info{'result'}{'secondaryType'});
    my $pims = $info{'result'}{'penaltyMinutes'};

    # Summarise
    my $play = '';
    if ($pims == 2) {
      $play .= 'Minor Penalty';
    } elsif ($pims == 4) {
      $play .= 'Double-Minor';
    } elsif ($pims == 5) {
      $play .= 'Major Penalty';
    } else {
      $play .= 'Penalty';
    }
    $play .= " on {{${team_id}_$player}} for $reason";
    $play .= ", drawn by {{${opp_team_id}_$drawn}}"
      if $drawn ne 'NULL';
    $play .= " (Served by {{${team_id}_$served}})"
      if $served != $player;

    # Store
    push @events, "('$season', '$game_type', '$game_id', '$event_id', '$period', '00:$time', 'PENALTY', NULL, '$play')";
    push @penalties, "('$season', '$game_type', '$game_id', '$event_id', '$team_id', $player, $served, '$reason', $pims, '$opp_team_id', $drawn)";
  }

  print "INSERT INTO SPORTS_NHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, zone, play) VALUES " . join(', ', @events) . ";\n\n" if @events > 0;
  print "INSERT INTO SPORTS_NHL_GAME_EVENT_PENALTY (season, game_type, game_id, event_id, team_id, jersey, served_by, type, pims, drawn_team_id, drawn_jersey) VALUES " . join(', ', @penalties) . ";\n\n" if @penalties > 0;
}

##
## Officials
##
sub parse_officials {
  print "##\n## Officials\n##\n";

  my @officials = @{$$gamedata{'liveData'}{'boxscore'}{'officials'}};
  my %list = ( 'referee' => [ ], 'linesman' => [ ] );
  foreach my $o (@officials) {
    push @{$list{lc $$o{'officialType'}}}, $$o{'official'}{'fullName'};
  }
  foreach my $type ('referee', 'linesman') {
    foreach my $official (@{$list{$type}}) {
      print "INSERT INTO SPORTS_NHL_GAME_OFFICIALS (season, game_type, game_id, official_type, official_id)
  SELECT '$season', '$game_type', '$game_id', '$type', official_id
  FROM SPORTS_NHL_OFFICIALS
  WHERE season = '$season'
  AND   LOWER(CONCAT(first_name, ' ', surname)) = '" . lc(convert_text($official)) . "';\n";
    }
  }
  print "\n";
}

##
## PP Stats
##
sub parse_stats_pp {
  print "##\n## Special Teams\n##\n";
  my @list;

  # Visitor PP / Home PK
  my %visitor_stats = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{'away'}{'teamStats'}{'teamSkaterStats'}};
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'visitor_id'}', 'pp', '$visitor_stats{'powerPlayGoals'}', '$visitor_stats{'powerPlayOpportunities'}')";
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'home_id'}', 'pk', '$visitor_stats{'powerPlayGoals'}', '$visitor_stats{'powerPlayOpportunities'}')";

  # Home PP / Visitor PK
  my %home_stats = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'teamStats'}{'teamSkaterStats'}};
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'home_id'}', 'pp', '$home_stats{'powerPlayGoals'}', '$home_stats{'powerPlayOpportunities'}')";
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'visitor_id'}', 'pk', '$home_stats{'powerPlayGoals'}', '$home_stats{'powerPlayOpportunities'}')";

  # Print
  print "INSERT INTO SPORTS_NHL_GAME_PP_STATS (season, game_type, game_id, team_id, pp_type, pp_goals, pp_opps) VALUES " . join(', ', @list) . ";\n" if @list > 0;
  print "\n";
}

##
## Player Stats
##
sub parse_boxscore {
  our %rosters;
  my @skaters = ( ); my @goalies = ( );

  print "##\n## Boxscore\n##\n";
  my %teams = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}};
  foreach my $type ('away', 'home') {
    my $team_id = ($type eq 'home' ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
    my %players = %{$teams{$type}{'players'}};
    foreach my $id (keys %players) {
      my %p = %{$players{$id}};
      $id = $p{'person'}{'id'};

      # Position?
      my $pos = $p{'position'}{'abbreviation'};
      next if $pos eq 'N/A'; # Players marked as N/A were scratched...

      # Get penalty counts
      my %pens = %{$rosters{$id}{'stats'}{'penalties'}};

      # Skater stats
      if ($pos ne 'G') {
        my %s   = %{$p{'stats'}{'skaterStats'}};
        my %so  = %{$rosters{$id}{'stats'}{'so'}};
        my $fol = $s{'faceoffTaken'} - $s{'faceOffWins'};
        push @skaters, "# $team_id, $p{'person'}{'fullName'} (#$p{'jerseyNumber'})
INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, gp, goals, assists, plus_minus, pims, pens_taken, pens_drawn, pp_goals, pp_assists, sh_goals, sh_assists, shots, blocked_shots, hits, fo_wins, fo_loss, so_goals, so_shots, giveaways, takeaways, toi)
  SELECT season, player_id, game_type, game_id, team_id, 1, $s{'goals'}, $s{'assists'}, $s{'plusMinus'}, $s{'penaltyMinutes'}, $pens{'taken'}, $pens{'drawn'}, $s{'powerPlayGoals'}, $s{'powerPlayAssists'}, $s{'shortHandedGoals'}, $s{'shortHandedAssists'}, $s{'shots'}, $s{'blocked'}, $s{'hits'}, $s{'faceOffWins'}, $fol, $so{'goals'}, $so{'shots'}, $s{'giveaways'}, $s{'takeaways'}, '$s{'timeOnIce'}'
  FROM SPORTS_NHL_GAME_LINEUP
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   game_id = '$game_id'
  AND   team_id = '$team_id'
  AND   jersey = '$p{'jerseyNumber'}';";

      # Goalie stats
      } else {
        my %s = %{$p{'stats'}{'goalieStats'}};
        my $w   = ($s{'decision'} eq 'W' ? 1 : 0);
        my $l   = ($s{'decision'} eq 'L' && $$gamedata{'liveData'}{'linescore'}{'currentPeriod'} == 3 ? 1 : 0);
        my $otl = ($s{'decision'} eq 'L' && $$gamedata{'liveData'}{'linescore'}{'currentPeriod'} == 4 ? 1 : 0);
        my $sol = ($s{'decision'} eq 'L' && $$gamedata{'liveData'}{'linescore'}{'currentPeriod'} == 5 ? 1 : 0);
        my $ga  = $s{'shots'} - $s{'saves'};
        push @goalies, "# $team_id, $p{'person'}{'fullName'} (#$p{'jerseyNumber'})
INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, gp, win, loss, ot_loss, so_loss, goals_against, shots_against, minutes_played, goals, assists, pims, pens_taken, pens_drawn)
  SELECT season, player_id, game_type, game_id, team_id, 1, $w, $l, $otl, $sol, $ga, $s{'shots'}, '$s{'timeOnIce'}', $s{'goals'}, $s{'assists'}, $s{'pim'}, $pens{'taken'}, $pens{'drawn'}
  FROM SPORTS_NHL_GAME_LINEUP
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   game_id = '$game_id'
  AND   team_id = '$team_id'
  AND   jersey = '$p{'jerseyNumber'}';";
      }
    }
  }

  # Display
  print "# Skater Stats\n";
  print join("\n\n", @skaters) . "\n\n" if @skaters;
  print "# Goalie Stats\n";
  print join("\n\n", @goalies) . "\n\n" if @goalies;
}

##
## Game result
##
sub parse_result {
  print "##\n## Game summary\n##\n";
  our %rosters;

  # Scores, Status
  my %current = %{$$gamedata{'liveData'}{'plays'}{'currentPlay'}};
  my $visitor = $current{'about'}{'goals'}{'away'};
  my $home = $current{'about'}{'goals'}{'home'};
  my $status = 'F';
  if ($$gamedata{'liveData'}{'linescore'}{'currentPeriod'} == 4) {
    $status = 'OT';
  } elsif ($$gamedata{'liveData'}{'linescore'}{'currentPeriod'} == 5) {
    $status = 'SO';
    # Increase score (which doesn't feature shootout score)
    if ($$gamedata{'liveData'}{'linescore'}{'shootoutInfo'}{'away'}{'scores'} > $$gamedata{'liveData'}{'linescore'}{'shootoutInfo'}{'home'}{'scores'}) {
      $visitor++;
    } else {
      $home++;
    }
  }

  # Winner/Loser
  my $winner = $rosters{$$gamedata{'liveData'}{'decisions'}{'winner'}{'id'}}{'jersey'};
  my $loser = $rosters{$$gamedata{'liveData'}{'decisions'}{'loser'}{'id'}}{'jersey'};
  my $visitor_goalie = ($visitor > $home ? $winner : $loser);
  my $home_goalie = ($visitor < $home ? $winner : $loser);

  print "UPDATE SPORTS_NHL_SCHEDULE SET home_score = '$home', visitor_score = '$visitor', status = '$status', home_goalie = $home_goalie, visitor_goalie = $visitor_goalie, attendance = NULL, three_star_sel = NULL WHERE season = '$season' AND game_type = '$game_type' AND game_id = '$game_id';\n\n";
}

##
## Three stars
##
sub parse_three_stars {
  print "##\n## Three Stars\n##\n";
  our %rosters;

  my %d = %{$$gamedata{'liveData'}{'decisions'}};
  my @list = ( );
  my @stars = ( { 'col' => 'firstStar', 'num' => 1 }, { 'col' => 'secondStar', 'num' => 2 }, { 'col' => 'thirdStar', 'num' => 3 } );
  foreach my $star (@stars) {
    my %r = %{$rosters{$d{$$star{'col'}}{'id'}}};
    push @list, "('$season', '$game_type', '$game_id', '$$star{'num'}', '$r{'team_id'}', '$r{'jersey'}')";
  }

  print "INSERT INTO SPORTS_NHL_GAME_THREE_STARS (season, game_type, game_id, star, team_id, jersey) VALUES " . join(', ', @list) . ";\n" if @list > 0;
  print "\n";
}

##
## Misc
##
sub parse_misc {
  print "##\n## Record this game is running off summarised info\n##\n";
  print "INSERT INTO SPORTS_NHL_GAME_SUMMARISED (season, game_type, game_id) VALUES ('$season', '$game_type', '$game_id');\n\n";
}

