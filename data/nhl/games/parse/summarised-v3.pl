#!/usr/bin/perl -w
# Load the player totals and game info from the NHL.com boxscore, rather than play-by-play, roster, etc, gamesheets

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse play by play totals script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate this is the right version
validate_integration_version(3);

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = (
    'SPORTS_NHL_GAME_EVENT',
    'SPORTS_NHL_GAME_EVENT_GOAL',
    'SPORTS_NHL_GAME_EVENT_PENALTY',
    'SPORTS_NHL_GAME_EVENT_SHOOTOUT',
    'SPORTS_NHL_GAME_LINEUP',
    'SPORTS_NHL_GAME_LINEUP_ISSUES',
    'SPORTS_NHL_GAME_OFFICIALS',
    'SPORTS_NHL_GAME_PP_STATS',
    'SPORTS_NHL_GAME_SCRATCHES',
    'SPORTS_NHL_GAME_SCRATCHES_ISSUES',
    'SPORTS_NHL_GAME_SUMMARISED',
    'SPORTS_NHL_PLAYERS_GAME_GOALIES',
    'SPORTS_NHL_PLAYERS_GAME_SKATERS'
  );

  clear_previous($season,
  $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  #reorder_tables(@tables) if $ARGV[3] eq '--order'; # Do not run for performance reasons
  exit;
}

# First check, does the file exist, and if it does, load it
my ($gamedata_file, $gamedata) = load_file($season_dir, $game_type, $game_id, 'summary', { 'ext' => 'json' });
my ($boxscore_file, $boxscore) = load_file($season_dir, $game_type, $game_id, 'boxscore', { 'ext' => 'json' });
print "# Loading summarised game data for $season // $game_type // $game_id\n\n";

# Rosters
parse_rosters() if check_disp(1);

# Scoring
parse_scoring() if check_disp(2);

# Shootout
parse_shootout() if check_disp(3);

# Penalties
parse_penalties() if check_disp(4);

# Officials
parse_officials() if check_disp(5);

# PP Stats
parse_stats_pp() if check_disp(6);

# Player Stats
parse_boxscore() if check_disp(7);

# Result
parse_result() if check_disp(8);

# Three Stars
parse_three_stars() if check_disp(9);

# Misc
parse_misc() if check_disp(10);

#
# The new v3 API does not feel sufficiently well known to be able to parse for this edge-case in game processing, so we will come back to it as-and-when required
#
print STDERR "Tech Debt: The NHL API is not sufficiently well understood to build the summarised script, an edge case in our processing.\n";
exit $config{'exit_codes'}{'failed-prereq'};

##
## Rosters (inc Scratches)
##
sub parse_rosters {
  print "##\n## Lineup\n##\n";
  print "\n";
}

##
## Scoring
##
sub parse_scoring {
  print "##\n## Scoring\n##\n";
  print "\n";
}

##
## Shootout
##
sub parse_shootout {
  # Skip if the game didn't go to a shootout
  return if !@{$$gamedata{'summary'}{'shootout'}};

  print "##\n## Shootout\n##\n";
  print "\n";
}

##
## Penalties
##
sub parse_penalties {
  print "##\n## Penalties\n##\n";
  print "\n";
}

##
## Officials
##
sub parse_officials {
  print "##\n## Officials\n##\n";
  print "\n";
}

##
## PP Stats
##
sub parse_stats_pp {
  print "##\n## Special Teams\n##\n";
  print "\n";
}

##
## Player Stats
##
sub parse_boxscore {
  print "##\n## Boxscore\n##\n";
  print "\n";
}

##
## Game result
##
sub parse_result {
  print "##\n## Game summary\n##\n";
  print "\n";
}

##
## Three stars
##
sub parse_three_stars {
  print "##\n## Three Stars\n##\n";
  print "\n";
}

##
## Misc
##
sub parse_misc {
  print "##\n## Record this game is running off summarised info\n##\n";
  #print "INSERT INTO SPORTS_NHL_GAME_SUMMARISED (season, game_type, game_id) VALUES ('$season', '$game_type', '$game_id');\n";
}
