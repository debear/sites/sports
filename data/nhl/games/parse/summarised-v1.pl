#!/usr/bin/perl -w
# Load the player totals and game info from the NHL.com boxscore, rather than play-by-play, roster, etc, gamesheets

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse play by play totals script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate this is the right version
validate_integration_version(1);

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_EVENT', 'SPORTS_NHL_GAME_EVENT_GOAL', 'SPORTS_NHL_GAME_EVENT_PENALTY', 'SPORTS_NHL_GAME_EVENT_SHOOTOUT', 'SPORTS_NHL_GAME_LINEUP', 'SPORTS_NHL_GAME_LINEUP_ISSUES', 'SPORTS_NHL_GAME_OFFICIALS', 'SPORTS_NHL_GAME_PP_STATS', 'SPORTS_NHL_GAME_SCRATCHES', 'SPORTS_NHL_GAME_SCRATCHES_ISSUES', 'SPORTS_NHL_GAME_SUMMARISED', 'SPORTS_NHL_PLAYERS_GAME_GOALIES', 'SPORTS_NHL_PLAYERS_GAME_SKATERS' );

  clear_previous($season, $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  #reorder_tables(@tables) if $ARGV[3] eq '--order'; # Do not run for performance reasons
  exit;
}

# First check, does the file exist, and if it does, load it
my ($file, $contents) = load_file($season_dir, $game_type, $game_id, 'boxscore');
print "# Loading summarised game data for $season // $game_type // $game_id\n\n";

# Rosters
parse_rosters() if check_disp(1);

# Scoring
parse_scoring() if check_disp(2);

# Shootout
parse_shootout() if check_disp(3);

# Penalties
parse_penalties() if check_disp(4);

# Officials
parse_officials() if check_disp(5);

# PP Stats
parse_stats_pp() if check_disp(6);

# Player Stats
parse_boxscore() if check_disp(7);

# Result
parse_result() if check_disp(8);

# Misc
parse_misc() if check_disp(9);

##
## Rosters (inc Scratches)
##
sub parse_rosters {
  # Vars for storing...
  our %rosters = ( );

  # Start with the active players...
  print "# Lineup\n";
  my ($active) = ($contents =~ m/<div class="primary">Stats<\/div>(.*?)<\/div><!-- \/wide col -->/gsi);
  my @active = ($active =~ m/<div class="primary tableHeader"><img title="([^"]+)".*? (\w+)<\/a><\/div><table class="stats"(.*?)<\/table>/gsi);
  while (my ($team, $type, $roster) = splice(@active, 0, 3)) {
    # Convert a team name to a team_id
    my $team_lc = lc $team;
    my $team_id = ($team_lc =~ m/$$game_info{'home'}$/ ? $$game_info{'home_id'} : $$game_info{'visitor_id'});

    # Get the player list
    my $cols = ($type eq 'skaters' ? 15 : 8);
    my $pattern = '<tr[^>]*><td[^>]*>(\d+)<\/td><td[^>]*>(?:<span[^>]*>)?<a[^>]*href="[^"]*id=(\d+)">([^<]*?)<\/a>(?:<\/span>\s*(.*?)\s*)?<\/td>';
    $pattern .= '<td[^>]*>([CLRD])<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>\d+<\/td><td[^>]*>(-?\d+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d+)</td><td[^>]*>(\d+)<\/td><td[^>]*>[^<]*<\/td><td[^>]*>[^<]*<\/td><td[^>]*>[^<]*<\/td><td[^>]*>(\d{1,2}:\d{2})<\/td>' if $type eq 'skaters';
    $pattern .= '<td[^>]*>[^>]*<\/td><td[^>]*>[^>]*<\/td><td[^>]*>[^>]*<\/td><td[^>]*>(\d+) - (\d+)<\/td><td[^>]*>[^>]*<\/td><td[^>]*>(\d+)<\/td><td[^>]*>(\d{1,2}:\d{2})<\/td>' if $type eq 'goaltenders';
    $pattern .= '<\/tr>';
    my @roster = ($roster =~ m/$pattern/gsi);

    print "\n# $team ($team_id) $type\n";
    while (my @player = splice(@roster, 0, $cols)) {
      # Goalie position is implied in the table, so add in if required...
      if ($type eq 'goaltenders') {
        splice(@player, 4, 0, 'G');
        if (defined($player[3]) && $player[3] =~ m/\(.\)/) {
          ($player[3]) = ($player[3] =~ m/\((\w)\)/);
        } else {
          $player[3] = '';
        }
        splice(@player, 5, 0, $player[3]);
      }

      # Log info
      my $len = ($type eq 'skaters' ? $cols - 1 : $cols + 1);
      %{$rosters{$player[1]}} = ( 'team_id' => $team_id, 'jersey' => $player[0], 'stats' => [ @player[4..$len] ] );

      # Display
      print "\n# $player[2], #$player[0] ($player[4])\n";
      $player[2] = convert_text(uc $player[2]);
      print "INSERT INTO SPORTS_NHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started)
  SELECT '$season', '$game_type', '$game_id', '$team_id', '$player[0]', player_id, '$player[4]', NULL, '0'
  FROM SPORTS_NHL_PLAYERS_IMPORT
  WHERE remote_id = '$player[1]';\n";
      print "INSERT INTO SPORTS_NHL_GAME_LINEUP_ISSUES (season, game_type, game_id, team_id, jersey, player_name, pos, capt_status, started, problem)
  SELECT '$season', '$game_type', '$game_id', '$team_id', '$player[0]', '$player[2]', '$player[4]', NULL, '0', '0 matches' AS problem
  FROM SPORTS_NHL_SCHEDULE
  LEFT JOIN SPORTS_NHL_PLAYERS_IMPORT
    ON (remote_id = '$player[1]')
  WHERE SPORTS_NHL_SCHEDULE.season = '$season'
  AND   SPORTS_NHL_SCHEDULE.game_type = '$game_type'
  AND   SPORTS_NHL_SCHEDULE.game_id = '$game_id'
  AND   SPORTS_NHL_PLAYERS_IMPORT.remote_id IS NULL;\n";
    }
  }

  # Then the scratches...
  print "\n# Scratches\n";
  my @inactive = ($contents =~ m/<ul><li class="sub">([\w\s]+) Scratches<\/li>(.*?)<\/ul>/gsi);
  while (my ($team, $players) = splice(@inactive, 0, 2)) {
    # Convert a team name to a team_id
    $team = lc $team;
    my $team_id = ($team =~ m/$$game_info{'home'}$/ ? $$game_info{'home_id'} : $$game_info{'visitor_id'});

    # Get the player list
    my @players = ($players =~ m/<a href="[^"]+id=(\d+)">(.*?)<\/a>/gsi);
    while (my ($remote_player_id, $name) = splice(@players, 0, 2)) {
      %{$rosters{$remote_player_id}} = ( 'team_id' => $team_id, 'jersey' => 0 );

      print "\n# $name ($team_id)\n";
      $name = convert_text(uc $name);
      print "INSERT INTO SPORTS_NHL_GAME_SCRATCHES (season, game_type, game_id, team_id, jersey, player_id, pos)
  SELECT '$season', '$game_type', '$game_id', '$team_id', 0, player_id, NULL
  FROM SPORTS_NHL_PLAYERS_IMPORT
  WHERE remote_id = '$remote_player_id';\n";
      print "INSERT INTO SPORTS_NHL_GAME_SCRATCHES_ISSUES (season, game_type, game_id, team_id, jersey, player_name, pos, problem)
  SELECT '$season', '$game_type', '$game_id', '$team_id', 0, '$name', NULL, '0 matches' AS problem
  FROM SPORTS_NHL_SCHEDULE
  LEFT JOIN SPORTS_NHL_PLAYERS_IMPORT
    ON (remote_id = '$remote_player_id')
  WHERE SPORTS_NHL_SCHEDULE.season = '$season'
  AND   SPORTS_NHL_SCHEDULE.game_type = '$game_type'
  AND   SPORTS_NHL_SCHEDULE.game_id = '$game_id'
  AND   SPORTS_NHL_PLAYERS_IMPORT.remote_id IS NULL;\n";
    }
  }

  print "\n";
}

##
## Scoring
##
sub parse_scoring {
  our %rosters;
  my ($list) = ($contents =~ m/<!-- scoring summary table --><table class="summary">(.*?)<\/table>/gsi);
  my @rows = ($list =~ m/(<tr.*?<\/tr>)/gsi);

  my @events = ( ); my @goals = ( ); my %goal_num = ( ); my $period = 0;
  foreach my $r (@rows) {
    # Period?
    if ($r =~ m/class="sub p\d"/gsi) {
      $period++;
      next;
    }

    # Goal Header
    my ($event_id, $time, $team_id, $type, $en, $ps) = ($r =~ m/class="ev(\d+)"[^>]*><td[^>]*class="time"[^>]*>(\d{1,2}\:\d{2})<\/td><td[^>]*class="team">([^<]+)<\/td>.*?-->\s+(PPG - |SHG - )?(EN -)?(PS -)?.+$/gsi);
    next if !defined($event_id);
    $team_id = convert_team_id($team_id);
    my $opp_team_id = ($team_id eq $$game_info{'home_id'} ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
    $goal_num{$team_id}++;
    if (defined($type)) {
      $type =~ s/ - //;
      $type = "'$type'";
    } else {
      $type = 'NULL';
    }
    if (defined($en)) {
      $en = "'EN'";
    } else {
      $en = 'NULL';
    }
    $ps = (defined($ps) ? 1 : 0);

    # Goals and Assists
    my ($g, $a1, $a2) = ($r =~ m/http:\/\/www\.nhl\.com\/ice\/player\.htm\?id=(\d+)/gsi);
    $g = $rosters{$g}{'jersey'};
    if (defined($a1)) {
      $a1 = $rosters{$a1}{'jersey'};
    } else {
      $a1 = 'NULL';
    }
    if (defined($a2)) {
      $a2 = $rosters{$a2}{'jersey'};
    } else {
      $a2 = 'NULL';
    }

    # Summarise
    my %extra = ();
    my $play = '';
    # Type
    push @{$extra{'type'}}, 'Empty Net'
      if $en ne 'NULL';
    push @{$extra{'type'}}, 'Power Play'
      if $type eq "'PP'";
    push @{$extra{'type'}}, 'Shorthanded'
      if $type eq "'SH'";
    push @{$extra{'type'}}, 'Penalty Shot'
      if $ps;
    $play .= join(', ', @{$extra{'type'}}) . ' '
      if defined($extra{'type'}) && @{$extra{'type'}};
    # Scorer
    $play .= "Goal, scored by {{${team_id}_$g}}";
    # Assists
    push @{$extra{'assists'}}, "{{${team_id}_$a1}}"
      if $a1 ne 'NULL';
    push @{$extra{'assists'}}, "{{${team_id}_$a2}}"
      if $a2 ne 'NULL';
    $play .= ' assisted by ' . join(' and ', @{$extra{'assists'}})
      if ($extra{'assists'}) && @{$extra{'assists'}};

    # Store
    push @events, "('$season', '$game_type', '$game_id', '$event_id', '$period', '00:$time', 'GOAL', NULL, '$play')";
    push @goals, "('$season', '$game_type', '$game_id', '$event_id', '$team_id', '$goal_num{$team_id}', $g, $a1, $a2, '$opp_team_id', NULL, NULL, NULL, $type, $en, $ps)";
  }

  print "# Scoring\n";
  print "INSERT INTO SPORTS_NHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, zone, play) VALUES " . join(', ', @events) . ";\n\n" if @events > 0;
  print "INSERT INTO SPORTS_NHL_GAME_EVENT_GOAL (season, game_type, game_id, event_id, by_team_id, goal_num, scorer, assist_1, assist_2, on_team_id, on_jersey, shot_type, shot_distance, goal_type, goalie_status, penalty_shot) VALUES " . join(', ', @goals) . ";\n\n" if @goals > 0;
}

##
## Shootout
##
sub parse_shootout {
  our %rosters;
  my ($header, $list) = ($contents =~ m/<table[^>]*><tbody><tr[^>]*><th[^>]*>Shootout<\/th><\/tr>(<tr.*?<\/tr>)(.*?)<\/table>/gsi);
  return if !defined($list);
  my @events = ( ); my @shootout = ( ); my $event_id = 1000; # Arbitrary high start point

  # Who went first?
  my ($first_team) = ($header =~ m/<tr[^>]*><th[^>]*>ROUND<\/th><th[^>]*>(.*?)<\/th>/gsi);
  my $first_team_id = ($first_team =~ m/$$game_info{'home'}$/gsi ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
  my $second_team_id = ($first_team_id eq $$game_info{'home_id'} ? $$game_info{'visitor_id'} : $$game_info{'home_id'});

  # And then the shooters...
  my $pattern = '<tr><td[^>]*>(\d+)<\/td><td[^>]*>(.*?)<\/td><td[^>]*><\/td><td[^>]*>(.*?)<\/td>';
  my @matches = ($list =~ m/$pattern/gsi);
  while (my ($round, $first, $second) = splice(@matches, 0, 3)) {
    # First shooter
    my ($first_status, $first_player_id) = ($first =~ m/<a class="[^"]*shootout([^"]+)" href="[^"]+id=(\d+)">/gsi);
    if (defined($first_player_id)) {
      $event_id++;
      $first_status = lc $first_status;

      # Summarise
      my $play = '{{' . $first_team_id . '_' . $rosters{$first_player_id}{'jersey'} . '}} ';
      $play .= 'scored' if $first_status eq 'goal';
      $play .= 'saved' if $first_status eq 'save';
      $play .= 'hit goal post' if $first_status eq 'goalpost';
      $play .= 'missed net' if $first_status eq 'miss';

      push @events, "('$season', '$game_type', '$game_id', '$event_id', '5', '00:00:00', 'SHOOTOUT', '$play')";
      push @shootout, "('$season', '$game_type', '$game_id', '$event_id', '$first_team_id', '$rosters{$first_player_id}{'jersey'}', '$second_team_id', NULL, NULL, NULL, '$first_status')";
    }

    # Second shooter
    my ($second_status, $second_player_id) = ($second =~ m/<a class="[^"]*shootout([^"]+)" href="[^"]+id=(\d+)">/gsi);
    if (defined($second_player_id)) {
      $event_id++;
      $second_status = lc $second_status;

      # Summarise
      my $play = '{{' . $second_team_id . '_' . $rosters{$second_player_id}{'jersey'} . '}} ';
      $play .= 'scored' if $second_status eq 'goal';
      $play .= 'saved' if $second_status eq 'save';
      $play .= 'hit goal post' if $second_status eq 'goalpost';
      $play .= 'missed net' if $second_status eq 'miss';

      push @events, "('$season', '$game_type', '$game_id', '$event_id', '5', '00:00:00', 'SHOOTOUT', '$play')";
      push @shootout, "('$season', '$game_type', '$game_id', '$event_id, '$second_team_id', '$rosters{$second_player_id}{'jersey'}', '$first_team_id', NULL, NULL, NULL, '$second_status')";
    }
  }

  print "# Shootout\n";
  print "INSERT INTO SPORTS_NHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, zone, play) VALUES " . join(', ', @events) . ";\n\n" if @events > 0;
  print "INSERT INTO SPORTS_NHL_GAME_EVENT_SHOOTOUT (season, game_type, game_id, event_id, by_team_id, by_jersey, on_team_id, on_jersey, shot_type, shot_distance, status) VALUES " . join(', ', @shootout) . ";\n\n" if @shootout > 0;
}

##
## Penalties
##
sub parse_penalties {
  our %rosters;
  my ($list) = ($contents =~ m/<!-- penalty details table --><table class="summary">(.*?)<\/table>/gsi);
  my @rows = ($list =~ m/<tr(.*?)<\/tr>/gsi);

  my @events = ( ); my @penalties = ( ); my $period = 0;
  foreach my $r (@rows) {
    # Period?
    if ($r =~ m/class="sub p\d"/gsi) {
      $period++;
      next;
    }

    # Penalty header
    my ($event_id, $time, $team_id) = ($r =~ m/class="ev(\d+)"[^>]*><td[^>]*>(\d{1,2}\:\d{2})<\/td><td[^>]*>([^<]+)<\/td>/gsi);
    next if !defined($event_id);
    $team_id = convert_team_id($team_id);

    # Player
    my ($remote_player_id, $name) = ($r =~ m/<!-- penalized player = served by player --><a[^>]*href="[^"]*id=(\d+)">(.*?)<\/a>/gsi);
    my $player = defined($remote_player_id) && defined($rosters{$remote_player_id}{'jersey'}) ? $rosters{$remote_player_id}{'jersey'} : 0;

    # Served by?
    my ($served_remote_player_id, $served_name) = ($r =~ m/<a[^>]*rel="playerServingPenaltyLinkData" href="[^"]*id=(\d+)">(.*?)<\/a>/gsi);
    my $served = defined($served_remote_player_id) && defined($rosters{$served_remote_player_id}{'jersey'}) ? $rosters{$served_remote_player_id}{'jersey'} : $player;

    # Drawn by
    my $opp_team_id = ($$game_info{'home_id'} eq $team_id ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
    my ($drawn_remote_player_id, $drawn_name) = ($r =~ m/against.*?<a[^>]*href="[^"]*id=(\d+)">(.*?)<\/a>/gsi);
    my $drawn;
    if (defined($drawn_remote_player_id) && defined($rosters{$drawn_remote_player_id})) {
      $drawn = $rosters{$drawn_remote_player_id}{'jersey'};
    } else {
      $drawn = 'NULL';
    }

    # Reason
    my $pims = 2;
    my $pattern = '<\/a>(?:<!--  served by another player -->)?(.*?)';
    $pattern .= (defined($drawn_remote_player_id) ? 'against' : '<\/td>');
    my ($reason) = ($r =~ m/$pattern/gsi);

    # ... ascertain PIM length...
    $pims = 4 if $reason =~ m/- double minor/;
    $pims = 5 if $reason =~ m/\(maj\)/;
    $pims = 10 if $reason =~ m/[Mm]isconduct/;

    # ... checking for random UTF-8 char at the front that trim can't strip
    trim(\$reason);
    my $c = substr($reason, 0, 1);
    $reason = substr($reason, 2) if ord(substr($reason, 0, 1)) > 165; # 165 is last (possibly valid) UTF-8 letter according to asciichart.com
    # ... and convert the reason to our standard format
    $reason = parse_penalty_type($reason);

    # Only known instance of a difference served_by is a bench penalty
    $player = 0 if $reason =~ m/ - bench/gsi;

    # Summarise
    my $play = '';
    if ($pims == 2) {
      $play .= 'Minor Penalty';
    } elsif ($pims == 4) {
      $play .= 'Double-Minor';
    } elsif ($pims == 5) {
      $play .= 'Major Penalty';
    } else {
      $play .= 'Penalty';
    }
    $play .= " on {{${team_id}_$player}} for $reason";
    $play .= ", drawn by {{${opp_team_id}_$drawn}}"
      if $drawn ne 'NULL';
    $play .= " (Served by {{${team_id}_$served}})"
      if $served != $player;

    # Store
    push @events, "('$season', '$game_type', '$game_id', '$event_id', '$period', '00:$time', 'PENALTY', NULL, '$play')";
    push @penalties, "('$season', '$game_type', '$game_id', '$event_id', '$team_id', $player, $served, '$reason', $pims, '$opp_team_id', $drawn)";
  }

  print "# Penalties\n";
  print "INSERT INTO SPORTS_NHL_GAME_EVENT (season, game_type, game_id, event_id, period, event_time, event_type, zone, play) VALUES " . join(', ', @events) . ";\n\n" if @events > 0;
  print "INSERT INTO SPORTS_NHL_GAME_EVENT_PENALTY (season, game_type, game_id, event_id, team_id, jersey, served_by, type, pims, drawn_team_id, drawn_jersey) VALUES " . join(', ', @penalties) . ";\n\n" if @penalties > 0;
}

##
## Officials
##
sub parse_officials {
  my ($officials) = ($contents =~ m/<ul><li[^>]*>Officials<\/li>(.*?)<\/ul>/gsi);
  my ($referees) = ($officials =~ m/<li>Referees: (.*?)<\/li>/gsi);
  my ($linesmen) = ($officials =~ m/<li>Linesmen: (.*?)<\/li>/gsi);

  my %officials;
  @{$officials{'referee'}} = split(', ', $referees)
    if defined($referees);
  @{$officials{'linesman'}} = split(', ', $linesmen)
    if defined($linesmen);

  print "# Game officials\n";
  foreach my $type ('referee', 'linesman') {
    foreach my $official (@{$officials{$type}}) {
      print "INSERT INTO SPORTS_NHL_GAME_OFFICIALS (season, game_type, game_id, official_type, official_id)
  SELECT '$season', '$game_type', '$game_id', '$type', official_id
  FROM SPORTS_NHL_OFFICIALS
  WHERE season = '$season'
  AND   LOWER(CONCAT(first_name, ' ', surname)) = '" . lc(convert_text($official)) . "';\n";
    }
  }
  print "\n";
}

##
## PP Stats
##
sub parse_stats_pp {
  my ($visitor_ppg, $visitor_ppo, $home_ppg, $home_ppo) = ($contents =~ m/<tr class="pp"><td[^>]*>Power Plays<\/td><td[^>]*class="aPP">(\d+)\/(\d+)<\/td><td[^>]*class="hPP">(\d+)\/(\d+)<\/td><\/tr>/gsi);
  my @list;

  # Visitor PP / Home PK
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'visitor_id'}', 'pp', '$visitor_ppg', '$visitor_ppo')";
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'home_id'}', 'pk', '$visitor_ppg', '$visitor_ppo')";

  # Home PP / Visitor PK
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'home_id'}', 'pp', '$home_ppg', '$home_ppo')";
  push @list, "('$season', '$game_type', '$game_id', '$$game_info{'visitor_id'}', 'pk', '$home_ppg', '$home_ppo')";

  # Print
  print "# Special Teams\n";
  print "INSERT INTO SPORTS_NHL_GAME_PP_STATS (season, game_type, game_id, team_id, pp_type, pp_goals, pp_opps) VALUES " . join(', ', @list) . ";\n" if @list > 0;
  print "\n";
}

##
## Player Stats
##
sub parse_boxscore {
  our %rosters;
  my @skaters = (); my @goalies = ();
  my ($status) = ($contents =~ m/<div class="boxData"><span class="prd">\s*(OT|SO)\s*<\/span><br\/>.*?<span style="display: none;" class="time">Final<\/span><\/div>/gsi);
  $status = '' if !defined($status);

  # Parse
  foreach my $remote_player_id (keys %rosters) {
    next if !defined($rosters{$remote_player_id}{'stats'});
    my @stats = @{$rosters{$remote_player_id}{'stats'}};

    # Skater
    if ($stats[0] ne 'G') {
      push @skaters, "INSERT INTO SPORTS_NHL_PLAYERS_GAME_SKATERS (season, player_id, game_type, game_id, team_id, gp, goals, assists, plus_minus, pims, shots, blocked_shots, hits, giveaways, takeaways, toi)
  SELECT season, player_id, game_type, game_id, team_id, 1, $stats[1], $stats[2], $stats[3], $stats[4], $stats[5], $stats[7], $stats[6], $stats[8], $stats[9], '00:$stats[10]'
  FROM SPORTS_NHL_GAME_LINEUP
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   game_id = '$game_id'
  AND   team_id = '$rosters{$remote_player_id}{'team_id'}'
  AND   jersey = '$rosters{$remote_player_id}{'jersey'}';";

    # Goalie
    } else {
      my $w = $stats[1] eq 'W' ? 1 : 0;
      my $l = $stats[1] eq 'L' && $status eq '' ? 1 : 0;
      my $otl = $stats[1] eq 'L' && $status eq 'OT' ? 1 : 0;
      my $sol = $stats[1] eq 'L' && $status eq 'SO' ? 1 : 0;
      my $ga = $stats[3] - $stats[2];
      push @goalies, "INSERT INTO SPORTS_NHL_PLAYERS_GAME_GOALIES (season, player_id, game_type, game_id, team_id, gp, win, loss, ot_loss, so_loss, goals_against, shots_against, minutes_played, pims)
  SELECT season, player_id, game_type, game_id, team_id, 1, $w, $l, $otl, $sol, $ga, $stats[3], '00:$stats[5]', $stats[4]
  FROM SPORTS_NHL_GAME_LINEUP
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   game_id = '$game_id'
  AND   team_id = '$rosters{$remote_player_id}{'team_id'}'
  AND   jersey = '$rosters{$remote_player_id}{'jersey'}';";
    }
  }

  # Display
  print "# Skater Stats\n";
  print join("\n\n", @skaters) . "\n\n" if @skaters;
  print "# Goalie Stats\n";
  print join("\n\n", @goalies) . "\n\n" if @goalies;
}

##
## Game result
##
sub parse_result {
  # Score
  my ($visitor) = ($contents =~ m/<div[^>]*class="score aw">(\d+)<\/div>/gsi);
  my ($home) = ($contents =~ m/<div[^>]*class="score hm">(\d+)<\/div>/gsi);
  my ($status) = ($contents =~ m/<div class="boxData"><span class="prd">\s*(OT|SO)\s*<\/span><br\/>.*?<span style="display: none;" class="time">Final<\/span><\/div>/gsi);
  $status = 'F' if !defined($status);

  # Goalies
  our %rosters; my %goalies = ( );
  foreach my $remote_player_id (keys %rosters) {
    next if !defined($rosters{$remote_player_id}{'stats'});
    if ($rosters{$remote_player_id}{'stats'}[0] eq 'G') {
      $goalies{'w'} = $rosters{$remote_player_id}{'jersey'} if $rosters{$remote_player_id}{'stats'}[1] eq 'W';
      $goalies{'l'} = $rosters{$remote_player_id}{'jersey'} if $rosters{$remote_player_id}{'stats'}[1] eq 'L';
    }
  }
  # Convert from W/L to Home/Visitor
  if ($home > $visitor) {
    $goalies{'home'} = $goalies{'w'};
    $goalies{'visitor'} = $goalies{'l'};
  } else {
    $goalies{'visitor'} = $goalies{'w'};
    $goalies{'home'} = $goalies{'l'};
  }

  print "# Game summary\n";
  print "UPDATE SPORTS_NHL_SCHEDULE SET home_score = '$home', visitor_score = '$visitor', status = '$status', home_goalie = $goalies{'home'}, visitor_goalie = $goalies{'visitor'}, attendance = NULL, three_star_sel = NULL WHERE season = '$season' AND game_type = '$game_type' AND game_id = '$game_id';\n\n";
}

##
## Misc
##
sub parse_misc {
  print "# Record this game is running off summarised info\n";
  print "INSERT INTO SPORTS_NHL_GAME_SUMMARISED (season, game_type, game_id) VALUES ('$season', '$game_type', '$game_id');\n\n";
}


