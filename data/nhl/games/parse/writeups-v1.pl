#!/usr/bin/perl -w
# Load the preview / recaps for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse play by play script.\n";
  exit 98;
}

# Note: Script deprecated after Mar-2017. TD.
print STDERR "ERROR: Attempting to run the deprecated writeups script for $ARGV[0] // $ARGV[1] // $ARGV[2].\n";

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

# Team conversion list of domain to IDs
my $teams = get_team_domain_map($season);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate this is the right version
if ($config{'integration_version'} != 1) {
  print STDERR "Wrong integration version called: I am v1, but this game requires v$config{'integration_version'}\n";
  exit 10;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_WRITEUP' );

  clear_previous($season, $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

# Parsing logic
my $logic;
my $backdated = ($season == 2009 && $game_type eq 'playoff');
if ($season < 2011 && !$backdated) {
  $logic = 1;

} elsif ($season < 2013 && !$backdated) {
  $logic = 2;

} else {
  $logic = 3;

}

# There are two types of file here, one for before (preview) and after (recap)
my @writeups = ( );
my @types = ( 'preview', 'recap' );
foreach my $type (@types) {
  # First check, does the file exist?
  my ($file, $contents) = load_file($season_dir, $game_type, $game_id, $type, ( 'no_exit_on_error' => 1 ) );
  print "# Loading $type: $file\n";
  my $pattern;

  # Headline
  my $headline;
  if ($logic == 1) {
    $pattern = '<div class="newsHeadline">(.*?)<\/div>';
    ($headline) = ($contents =~ m/$pattern/gsi);

  } elsif ($logic == 2 || $logic == 3) {
    $pattern = '<!-- headline, byline, etc --><h1[^>]*>\s*(\S.*?\S)\s*</h1>';
    ($headline) = ($contents =~ m/$pattern/gsi);
    ($headline) = ($contents =~ m/<span class="headline">(.*?)<\/span>/gsi)
      if !defined($headline);
  }
  if (!defined($headline)) {
    $headline = 'NULL';
  } else {
    trim(\$headline);
    $headline = "'" . convert_text($headline) . "'";
  }

  # Edited
  $pattern = '<div class="pubDateLocation">.*?(\d+)\.(\d+)\.(\d+) \/ (\d+)\:(\d+) (\w)M\s*<\/div>';
  my @edited = ($contents =~ m/$pattern/gsi);
  # Now modify into a MySQL timestamp
  my $edited = 'NULL';
  if (@edited) {
    $edited[3] += 12 if ($edited[5] eq 'P') && $edited[3] < 12;
    $edited = "'$edited[2]-$edited[0]-$edited[1] $edited[3]:$edited[4]:00'";
  }

  # Source
  if ($logic == 1) {
    $pattern = '<div class="newsByLine"[^>]*>\s*(\S.*?\S)\s*<\/div>';
  } elsif ($logic == 2 || $logic == 3) {
    $pattern = '<h3 class="newsByLine">\s*(\S.*?\S)\s*<\/h3>';
  }
  my ($source) = ($contents =~ m/$pattern/gsi);
  if (!defined($source)) {
    $source = 'NULL';
  } else {
    $source =~ s/<[^>]+>//g;
    $source =~ s/&nbsp;/ /g;
    $source =~ s/\s{2,}/ /g;
    $source =~ s/^\s+//g;
    $source = "'" . convert_text($source) . "'";
  }

  # Article
  my $article;
  if ($logic == 1) {
    ($article) = ($contents =~ m/<div id="newsBody">(.*?)<\/div>\s*<div class="newsTools">/gsi);

  } elsif ($logic == 2) {
    $pattern = '<div class="articleText group">\s*(?:<div>)?(.*?)(?:<\/div>)?';
    foreach my $pattern_end ( '<div style="float: right; margin-top: -\d+px;" class="newsTools">',
                              '</div><!-- gameContentAvailable -->' ) {
      ($article) = ($contents =~ m/$pattern$pattern_end/gsi);
      last if defined($article);
    }
    # Another method, if this failed...
    ($article) = ($contents =~ m/(<p>.+<\/p>)<div id="taboola-below-main-column"><\/div>/gsi)
      if !defined($article);

  } else {
    ($article) = ($contents =~ m/<script src="http:\/\/cdn\.nhle\.com\/projects\/ui-t5\/com\.nhl\.ui\.t5\.components\.share\/SocialShareBar\/dist\/js\/SocialShareBar\.min\.js\?v=[^"]*" type="text\/javascript"><\/script>.*?(<p>.*?)<div id="taboola-below-main-column"/gsi);

    ($article) = ($contents =~ m/<script src="http:\/\/cdn\.nhle\.com\/projects\/ui-t5\/com\.nhl\.ui\.t5\.components\.share\/SocialShareBar\/dist\/js\/SocialShareBar\.min\.js\?v=[^"]*" type="text\/javascript"><\/script>.*?(<p>.*?)<\/div><a shape="rect" name="comments"><\/a>/gsi)
      if !defined($article);

  }

  # Trim
  $article =~ s/^\s+//g;
  $article =~ s/\s+$//g;
  $article =~ s/\s{2,}/ /g;
  $article =~ s/^(?:<\/div>)+//gs;
  $article =~ s/<p>\s+/<p>/g;
  $article =~ s/\s+<\/p>/<\/p>/g;
  $article =~ s/<script.*?<\/script>//gs;
  $article =~ s/<span.*?<\/span>//gs;
  $article =~ s/<table.*?<\/table>//gs;
  $article =~ s/<div.*?<\/div>//gs;
  $article =~ s/<img.*?>//gs;

  # Modify the article to remove any NHL.com references to players or teams
  # First, teams
  $pattern = '(<a[^>]*?href="http:\/\/[^\.]+\.nhl\.com"[^>]*?>[^\<]+<\/a>)';
  my @team_references = ($article =~ m/$pattern/gsi);
  foreach my $team (@team_references) {
    # Identify the individual team
    $pattern = 'http://([^\.]+).nhl.com';
    my ($team_id) = ($team =~ m/$pattern/gsi);
    $article =~ s/$team/\{\{TEAM_$$teams{$team_id}\}\}/g;
  }

  # Now, players (slightly trickier as we need to use conversion codes)
  $pattern = '(<a href="http:\/\/www\.nhl\.com\/ice\/player\.htm\?id=\d+">[^\<]+<\/a>)';
  my @player_references = ($article =~ m/$pattern/gsi);
  my %player_list = ( ); # Used to get a unique list of players to modify
  foreach my $player (@player_references) {
    $pattern = 'http:\/\/www.nhl.com\/ice\/player.htm\?id=(\d+)"[^>]*>(.*?)<\/a>';
    my ($remote_player_id, $name) = ($player =~ m/$pattern/gsi);
    $player_list{$remote_player_id} = convert_text($name);
    $player =~ s/\?/\\?/g;
    $article =~ s/$player/\{\{PLAYER_REM_$remote_player_id\}\}/g;
  }

  # Remove other links...
  $article =~ s/<a[^>]*?>(.*?)<\/a>/$1/gsi;

  # Encode into HTML-safe text
  $article =~ s/\r//g;
  $article = convert_text($article);
  $article =~ s/\n/\\n/g;

  # Swap from NHL.com ID's to ours
  print "SET \@article_${season}_${game_type}_${game_id}_${type} := \"$article\";\n";
  my @player_list = keys %player_list;
  foreach my $player (@player_list) {
    print "SET \@rem_$player := IFNULL((SELECT CONCAT('{{PLAYER_', `player_id`, '}}') FROM `SPORTS_NHL_PLAYERS_IMPORT` WHERE `remote_id` = '$player'), '$player_list{$player}');\n";
    print "SELECT REPLACE(\@article_${season}_${game_type}_${game_id}_${type}, '{{PLAYER_REM_$player}}', \@rem_$player) INTO \@article_${season}_${game_type}_${game_id}_${type};\n";
  }

  # Now create
  print "\n";
  push @writeups, "('$season', '$game_type', '$game_id', '$type', $headline, $edited, $source, \@article_${season}_${game_type}_${game_id}_${type})";
}

# Output the SQL
print "INSERT INTO `SPORTS_NHL_GAME_WRITEUP` (`season`, `game_type`, `game_id`, `writeup_type`, `headline`, `edited`, `source`, `article`) VALUES " . join(', ', @writeups) . " ON DUPLICATE KEY UPDATE `headline` = VALUES(`headline`), `edited` = VALUES(`edited`), `source` = VALUES(`source`), `article` = VALUES (`article`);\n\n" if @writeups > 0;

