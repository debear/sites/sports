#!/usr/bin/perl -w
# Common routines for game processing

use List::Util qw( max );
use Text::Autoformat;

# Clear a previous run
sub clear_previous {
  my ($season, $game_type, $game_id, @tables) = @_;

  print "## Resetting a previous data load...\n";
  foreach my $table (@tables) {
    print "DELETE FROM `$table` WHERE `season` = '$season' AND `game_type` = '$game_type' AND `game_id` = '$game_id';\n";
  }
}

# Order by game
sub reorder_tables {
  my @tables = @_;

  print "## Table maintenance...\n";
  foreach my $table (@tables) {
    print "ALTER TABLE `$table` ORDER BY `season`, `game_type`, `game_id`;\n";
  }
}

# Validate the script is using the correct version of the integration it should be
sub validate_integration_version {
  my ($v) = @_;
  if ($config{'integration_version'} != $v) {
    print STDERR "Wrong integration version called: I am v$v, but this game requires v$config{'integration_version'}\n";
    exit 10;
  }
}

# Convert undefined values to 0
sub convert_undef_zero {
  my ($txt) = @_;
  $$txt = 0 if !defined($$txt);
}

# Convert a penalty description from NHL.com's play-by-play into ours
sub parse_penalty_type {
  my ($descrip) = @_;
  $descrip =~ s/^\s+//;
  $descrip =~ s/\s+$//;

  # Strip some common endings
  $descrip =~ s/\s*?-\s*?bench$//i;
  $descrip =~ s/\s*?-\s*?double minor$//i;
  $descrip =~ s/\s*?-\s*?misconduct$//i;
  $descrip =~ s/\s*?-\s*?game$//i;

  # Convert to 'highlight' case
  $descrip = autoformat($descrip, { case => 'highlight' });
  $descrip =~ s/^\s+//;
  $descrip =~ s/\s+$//;
  # Convert to lower case for eq comparison
  my $descrip_lc = lc $descrip;

  # Known values in our ENUM
  my @type_enum = (
    'Abuse of Officials', 'Abusive Language', 'Bench', 'Boarding', 'Broken Stick', 'Butt Ending', 'Charging',
    'Checking From Behind', 'Clipping', 'Closing Hand on Puck', 'Cross Checking', 'Delay of Game', 'Diving', 'Elbowing',
    'Embellishment', 'Face-Off Violation', 'Fighting', 'Game Misconduct', 'Goaltender Interference', 'Head Butting',
    'Hi-Sticking', 'Holding', 'Holding the Stick', 'Hooking', 'Illegal Check to Head', 'Illegal Equipment',
    'Illegal Play by Goalie', 'Illegal Stick', 'Instigator', 'Interference', 'Kneeing', 'Leaving the Crease',
    'Misconduct', 'Puck Over Glass', 'Roughing', 'Slashing', 'Smothering Puck in Crease', 'Spearing', 'Throwing Stick',
    'Too Many Men on the Ice', 'Tripping', 'Unsportsmanlike Conduct'
  );

  # Find and replace specific instances
  if ($descrip =~ / \(maj\)$/i) {
    $descrip =~ s/ \(maj\)$//gi;
    return $descrip;

  } elsif ($descrip_lc eq 'major'
        || $descrip_lc eq 'minor') {
    return undef;

  } elsif ($descrip_lc eq 'delaying the game'
        || $descrip =~ /^Delay Gm/
        || $descrip =~ /^Delay Game/
        || $descrip =~ /^Coach/
        || $descrip =~ /^Leaving/
        || $descrip_lc eq 'objects on ice') {
    return 'Delay of Game';

  } elsif ($descrip_lc eq 'delaying game-ill. play goalie') {
    return 'Illegal Play by Goalie';

  } elsif ($descrip_lc eq 'delaying game-puck over glass') {
    return 'Puck Over Glass';

  } elsif ($descrip_lc eq 'delaying game-smothering puck') {
    return 'Smothering Puck in Crease';

  } elsif ($descrip_lc eq 'too many men/ice'
        || $descrip_lc eq 'illegal substitution') {
    return 'Too Many Men on the Ice';

  } elsif ($descrip_lc eq 'interference on goalkeeper'
        || $descrip_lc eq 'interference - goalkeeper') {
    return 'Goaltender Interference';

  } elsif ($descrip_lc eq 'hi stick'
        || $descrip_lc eq 'high-sticking') {
    return 'Hi-sticking';

  } elsif ($descrip_lc eq 'match penalty') {
    return 'Game Misconduct';

  } elsif ($descrip_lc eq 'instigator - face shield'
        || $descrip_lc eq 'aggressor') {
    return 'Instigator';

  } elsif ($descrip_lc eq 'cross check'
        || $descrip_lc eq 'cross-checking') {
    return 'Cross Checking';

  } elsif ($descrip_lc eq 'concealing puck') {
    return 'Closing Hand on Puck';

  } elsif ($descrip_lc eq 'goalie leave crease'
        || $descrip_lc =~ /goalie partic.*?center/) {
    return 'Leaving the Crease';

  } elsif ($descrip =~ /^Puck Thrown F/) {
    return 'Illegal Play by Goalie';

  } elsif ($descrip_lc =~ /removing.*?opp.*?helmet/) {
    return 'Roughing';

  } elsif ($descrip_lc eq 'playing without a helmet') {
    return 'Illegal Equipment';

  } elsif ($descrip_lc eq 'throwing equipment') {
    return 'Unsportsmanlike Conduct';

  # A known value from our ENUM
  } elsif (grep(/^$descrip$/, @type_enum)) {
    return $descrip;

  # Strip duration in label
  } elsif ($descrip =~ /\(\d+ min\)/) {
    $descrip =~ s/ \(\d+ min\)//;
    return $descrip;

  # Otherwise use what we were given
  } else {
    print STDERR "Unknown Penalty Type '$descrip'\n";
    return $descrip;
  }
}

# Return true to pacify the compiler
1;
