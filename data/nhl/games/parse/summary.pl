#!/usr/bin/perl -w
# Load the Summary stats for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse summary script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_OFFICIALS', 'SPORTS_NHL_GAME_THREE_STARS', 'SPORTS_NHL_GAME_PP_STATS' );

  clear_previous($season, $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  print "ALTER TABLE `SPORTS_NHL_OFFICIALS` ORDER BY `season`, `official_id`;\n" if $ARGV[3] eq '--order'; # Ad hoc ordering...
  exit;
}

# First check, does the file exist, and if it does, load it
my ($file, $contents) = load_file($season_dir, $game_type, $game_id, 'game_summary');
print "# Loading summary for $season // $game_type // $game_id\n";

# Get the list of teams involved
my $pattern = '<img src="[^"]+logo[ac](?:std)?(\w{3})(?:fr)?\d*?\.gif" alt="([^"]+)" width="50" height="50" border="0"(?: \/)?>';
my @matches = ($contents =~ m/$pattern/gsi);
my %teams = ( );
$teams{'home'} = convert_team_id(uc($matches[2]));
$teams{'visitor'} = convert_team_id(uc($matches[0]));

# Attendance
$contents =~ s/&nbsp;/ /g;
$pattern = 'Attendance\s*([\d,]+)\s*(?:at)?(?:@)?\s*';
my ($attendance) = ($contents =~ m/$pattern/gsi);
if (!defined($attendance)) {
  $pattern =~ s/Attendance/Ass\.\/Att\./g;
  ($attendance) = ($contents =~ m/$pattern/gsi);
}

if (!defined($attendance)) {
  $attendance = 'NULL';
} else {
  $attendance =~ s/,//g;
  $attendance = "'$attendance'";
}

# Now get the individual scores
$pattern = '<td align="center" style="font-size: 12px;font-weight:bold">VISITOR<\/td>.*?<td align="center" style="font-size: 40px;font-weight:bold">(\d+)<\/td>';
my ($visitor_score) = ($contents =~ m/$pattern/gsi);
$pattern =~ s/VISITOR/HOME/g;
my ($home_score) = ($contents =~ m/$pattern/gsi);

# Did this game go to OT or the Shootout?
#  - Identified by period of last goal
$pattern = '<tr class="[^"]*Color">\s*<td align="center">' . ($visitor_score + $home_score) . '<\/td>\s*<td align="center">([0-9OTS]+)<\/td>\s*<td align="center">[0-9:]+<\/td>\s*<td align="center">\D+<\/td>';
my ($status) = ($contents =~ m/$pattern/gsi);
if (!defined($status)) {
  $status = 'SO';
}
# Convert a numeric period to 'F' (i.e., regulation)
if ($status =~ m/^\d+$/) {
  if ($game_type eq 'regular' || $status < 4) {
    $status = 'F';
  } else {
    $status = ($status > 4 ? $status-3 : '') . 'OT';
  }
}

# Winning / Losing goalies
# First the winner
$pattern = '<td align="[^"]+" class="[^"]+">(\d+)<\/td>\s*<td align="[^"]+" class="[^"]+">G<\/td>\s*<td class="[^"]+">[^(<]+\(W\)\s*<\/td>';
my ($winner) = ($contents =~ m/$pattern/gsi);
if (!defined($winner)) {
  $winner = 'NULL';
} else {
  $winner = "'$winner'";
}

# Then the loser
$pattern =~ s/W/(?:OT)?L?/g;
my ($loser) = ($contents =~ m/$pattern/gsi);
if (!defined($loser)) {
  $loser = 'NULL';
} else {
  $loser = "'$loser'";
}

# Do we now have anything?
if ($winner eq 'NULL' || $loser eq 'NULL') {
  # Probably not in this file, so let's try the event_summary instead
  my ($sub_file, $sub_contents) = load_file($season_dir, $game_type, $game_id, 'event_summary', ( 'no_exit_on_error' => 1 ) );

  # First the winner
  $pattern = '<td align="[^"]+" class="[^"]+">(\d+)<\/td>\s*<td align="[^"]+" class="[^"]+">G<\/td>\s*<td class="[^"]+">[^(<]+\(W\)\s*<\/td>';
  ($winner) = ($sub_contents =~ m/$pattern/gsi);
  if (!defined($winner)) {
    $winner = 'NULL';
  } else {
    $winner = "'$winner'";
  }

  # Then the loser
  $pattern =~ s/W/(?:OT)?L?/g;
  ($loser) = ($sub_contents =~ m/$pattern/gsi);
  if (!defined($loser)) {
    $loser = 'NULL';
  } else {
    $loser = "'$loser'";
  }
}
# Convert from W/L to Home/Visitor
my $home_goalie; my $visitor_goalie;
if ($home_score > $visitor_score) {
  $home_goalie = $winner;
  $visitor_goalie = $loser;
} else {
  $visitor_goalie = $winner;
  $home_goalie = $loser;
}

# 3 Star selection
$pattern = '<span class="sectionheading">.*?3 STARS<\/span> (?: Picked )?(?:Par\/)?By:([^<]+)</td>';
my ($picked_by) = ($contents =~ m/$pattern/gsi);
if (!defined($picked_by) || $picked_by =~ m/^\s*$/) {
  $picked_by = 'NULL';
} else {
  $picked_by =~ s/^\s+//g;
  $picked_by = "'" . convert_text($picked_by, 'UTF-8') . "'";
}

# Output the SQL for the above (all same table)
print "UPDATE `SPORTS_NHL_SCHEDULE` SET home_score = '$home_score', visitor_score = '$visitor_score', status = '$status', home_goalie = $home_goalie, visitor_goalie = $visitor_goalie, attendance = $attendance, three_star_sel = $picked_by WHERE season = '$season' AND game_type = '$game_type' AND game_id = '$game_id';\n\n";

# Referees and Linesmen
print "# Updating Referee's and Linesmen\n";
$pattern = '<table[^>]*>\s*<tr[^>]*>\s*<td[^>]*>Referee<\/td>\s*<td[^>]*>Lines[^>]*<\/td>\s*<\/tr>\s*<tr[^>]*>\s*<td[^>]*>\s*<table[^>]*>(.*?)<\/table>\s*<\/td>\s*<td[^>]*>\s*<table[^>]*>(.*?)<\/table>';
my @officials = ($contents =~ m/$pattern/gsi);
# Loop through the individual blocks
my @official_list = ( );
my @game_list = ( );
$pattern = '<td align="left">#(\d+)([^<]+)<\/td>'; # Pattern for individuals
for (my $t = 0; $t < 2; $t++) {
  my @ind = ($officials[$t] =~ m/$pattern/gsi);
  for (my $i = 0; $i < @ind; $i += 2) {
    trim(\$ind[$i+1]);
    my @tmp = split(' ', $ind[$i+1]);
    my ($fname) = convert_text(shift @tmp);
    my $sname = convert_text(join(' ', @tmp));
    push @official_list, "('$season', '$ind[$i]', '$fname', '$sname')";
    push @game_list, "('$season', '$game_type', '$game_id', '" . (!$t ? 'referee' : 'linesman') . "', '$ind[$i]')";
  }
}
print "INSERT IGNORE INTO `SPORTS_NHL_OFFICIALS` (`season`, `official_id`, `first_name`, `surname`) VALUES " . join(', ', @official_list) . ";\n";
print "INSERT INTO `SPORTS_NHL_GAME_OFFICIALS` (`season`, `game_type`, `game_id`, `official_type`, `official_id`) VALUES " . join(', ', @game_list) . ";\n\n";

# 3 Stars
print "# 3 Stars of the Game\n";
$pattern = '<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">\s*(?:<tr>.*?<\/tr>\s*)?<tr>\s*<td align="center">1.</td>\s*<td align="center">([^<]*)</td>\s*<td align="center">[^<]*</td>\s*<td align="left">(\d*)\s*[^<]*</td>\s*<\/tr>\s*<tr>\s*<td align="center">2.</td>\s*<td align="center">([^<]*)</td>\s*<td align="center">[^<]*</td>\s*<td align="left">(\d*)\s*[^<]*</td>\s*<\/tr>\s*<tr>\s*<td align="center">3.</td>\s*<td align="center">([^<]*)</td>\s*<td align="center">[^<]*</td>\s*<td align="left">(\d*)\s*[^<]*</td>\s*<\/tr>\s*<\/table>';
@matches = ($contents =~ m/$pattern/gsi);
my @list = ( );
for (my $i = 1; $i < 4; $i++) {
  if(defined($matches[2*($i-1)]) && $matches[2*($i-1)] ne '') {
    my $jersey = ($matches[(2*$i)-1] ? $matches[(2*$i)-1] : 0);
    push @list, "('$season', '$game_type', '$game_id', '$i', '" . convert_team_id($matches[2*($i-1)]) . "', '$jersey')";
  }
}
print "INSERT INTO `SPORTS_NHL_GAME_THREE_STARS` (`season`, `game_type`, `game_id`, `star`, `team_id`, `jersey`) VALUES " . join(', ', @list) . " ON DUPLICATE KEY UPDATE `team_id` = VALUES(`team_id`), `jersey` = VALUES(`jersey`);\n\n" if @list > 0;

# Power Play / Penalty Kill
print "# Special Teams\n";
$pattern = '<table border="0" cellpadding="0" cellspacing="0" width="100%">\s+<tr class="heading">\s+<td align="center" class="[^\"]+" width="25%">5v4<\/td>\s+<td align="center" class="[^"]+" width="25%">5v3<\/td>\s+<td align="center" class="[^"]+" width="25%">4v3<\/td>\s+<td align="center" class="[^"]+" width="25%">TOT<\/td>\s+<\/tr>\s+<tr class="oddColor">\s+<td align="center">[^<]+<\/td>\s+<td align="center">[^<]+<\/td>\s+<td align="center">[^<]+<\/td>\s+<td align="center">([^<]+)<\/td>\s+<\/tr>\s+<\/table>';
@matches = ($contents =~ m/$pattern/gsi);

# If nothing found, try and alternative route
if (@matches == 0) {
  $pattern = '<table cellpadding="0" cellspacing="0" border="0">\s+<td class="bold">.*?Power Plays \(Goals-Opp\.\/PPTime\)<\/td>\s+<td>.*?<\/td>\s+<\/table>\s+<\/td>\s+<td align="left" class="bold \+ oddColor \+ tborder" width="40%">(.*?)<\/td>\s+<\/tr>\s+<\/table>';
  @matches = ($contents =~ m/$pattern/gsi);
}

# Parse
@list = ( );
for (my $i = 0; $i < @matches; $i++) {
  $pattern = '^(\d+)\-(\d+)\/';
  my ($pp_goals, $pp_opps) = ($matches[$i] =~ m/$pattern/gsi);
  $pp_goals = 0 if !defined($pp_goals);
  $pp_opps = 0 if !defined($pp_opps);
  my $pp_team = (!$i ? 'visitor' : 'home');
  my $pk_team = (!$i ? 'home' : 'visitor');
  push @list, "('$season', '$game_type', '$game_id', '$teams{$pp_team}', 'pp', '$pp_goals', '$pp_opps')";
  push @list, "('$season', '$game_type', '$game_id', '$teams{$pk_team}', 'pk', '$pp_goals', '$pp_opps')";
}
print "INSERT INTO `SPORTS_NHL_GAME_PP_STATS` (`season`, `game_type`, `game_id`, `team_id`, `pp_type`, `pp_goals`, `pp_opps`) VALUES " . join(', ', @list) . " ON DUPLICATE KEY UPDATE `pp_goals` = VALUES(`pp_goals`), `pp_opps` = VALUES(`pp_opps`);\n\n" if @list > 0;

