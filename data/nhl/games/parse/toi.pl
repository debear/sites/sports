#!/usr/bin/perl -w
# Load the Time on Ice figures for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse time on ice script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_TOI' );

  clear_previous($season, $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  #reorder_tables(@tables) if $ARGV[3] eq '--order'; # Do not run for performance reasons
  exit;
}

print "# Loading time on ice for $season // $game_type // $game_id\n\n";

# There are two types of file here, one for each team
my @types = ( 'home', 'visitor' );
foreach my $type (@types) {
  my ($file, $contents) = load_file($season_dir, $game_type, $game_id, 'toi_' . $type, ( 'no_exit_on_error' => 1 ) );
  next if !defined($contents);
  print "# $$game_info{$type.'_id'} ($type)\n\n";

  # Which team are we looking at?
  my $pattern = '<img src="[^"]+logo[ac](?:std)?(\w{3})(?:fr)?\d*?\.gif" alt="([^"]+)" width="50" height="50" border="0"(?: \/)?>';
  my @matches = ($contents =~ m/$pattern/gsi);
  my $team_id = convert_team_id(uc($matches[$type eq $types[0] ? 2 : 0]));

  # Now look for the tables to get the values for each player
  $pattern = '<tr>\s*<td align="center" valign="top" class="playerHeading \+ border" colspan="8">(\d+) ([^,]+), ([^<]+)<\/td>\s*<\/tr>(.*?)<tr>\s*<td width="100%" class="spacer \+ bborder \+ lborder \+ rborder" colspan="8">&nbsp;<\/td>\s*<\/tr>';
  @matches = ($contents =~ m/$pattern/gsi);
  print STDERR "# No time on ice matches found.\n" if @matches == 0; # Consider this as a warning and not an error
  while (my @match = splice(@matches, 0, 4)) {
    print "# Loading data for $match[2] $match[1] (#$match[0])\n";

    # Now get the individual records
    $pattern = '<tr class="\s*\w+Color">\s*<td align="center" class="lborder \+ bborder">(\d+)<\/td>\s*<td align="center" class="lborder \+ bborder">([0-9OTS]+)<\/td>\s*<td align="center" class="lborder \+ bborder">([0-9:]+) \/ ([0-9:]+)<\/td>\s*<td align="center" class="lborder \+ bborder">([0-9:]+) \/ ([0-9:]+)<\/td>\s*<td align="center" class="lborder \+ bborder">([0-9:]+)<\/td>\s*<td align="center" class="lborder \+ bborder \+ rborder">([^<]+)<\/td>\s*<\/tr>';
    my @sub_matches = ($match[3] =~ m/$pattern/gsi);
    my @player_list = ( );
    my $last_period = '';
    my $last_shift_start = '';
    my $last_shift_end = '';
    my $seq_num = 0;
    while (my @sub_match = splice(@sub_matches, 0, 8)) {
      my $drop = 0;
      $sub_match[1] = '4' if ($sub_match[1] eq 'OT');

      # Tweak some funny scenarios in the times
      # -> shift_start OR shift_end entries the wrong way round (e.g., 0:00 / 20:00 instead of 20:00 / 0:00)
      if (time_to_sec($sub_match[3]) < time_to_sec($sub_match[5])) {
        # Do we swap the shift_start time or the shift_end?
        if (time_to_sec($sub_match[3]) < time_to_sec($sub_match[4])) {
          # Start
          $sub_match[3] = $sub_match[2];
        } else {
          # End
          $sub_match[5] = $sub_match[4];
        }
      }

      # -> Known period to be incorrect (e.g., Previously end of 3rd, now OT but recorded as 3rd)
      if ($last_period ne '' && $last_period eq $sub_match[1] && time_to_sec($last_shift_end) < time_to_sec($sub_match[5])) {
        $sub_match[1]++;
      }

      # -> Known time to be incorrect (OT starts at 5:00, not 20:00)
      if ($game_type eq 'regular' && $sub_match[1] eq '4' && time_to_sec($sub_match[3]) > 300) {
        $sub_match[3] = '5:00';
      }

      # -> Shift length of 0 seconds
      if (time_to_sec($sub_match[3]) == time_to_sec($sub_match[5])) {
        $drop = 1;
      }

      $seq_num += !$drop;
      push @player_list, "('$season', '$game_type', '$game_id', '$team_id', '$match[0]', '$seq_num', '$sub_match[1]', '00:$sub_match[3]', '00:$sub_match[5]')"
        if !$drop;

      # Store last shift info
      $last_period = $sub_match[1];
      $last_shift_start = $sub_match[3];
      $last_shift_end = $sub_match[5];
    }
    print 'INSERT INTO `SPORTS_NHL_GAME_TOI` (`season`, `game_type`, `game_id`, `team_id`, `jersey`, `shift_num`, `period`, `shift_start`, `shift_end`) VALUES ' . join(', ', @player_list) . ";\n\n";
  }
}

# Convert a time into total seconds
sub time_to_sec {
  my ($time) = @_;
  my @split = split(':', $time);
  return (60*$split[0]) + $split[1];
}

