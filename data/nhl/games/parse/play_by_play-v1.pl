#!/usr/bin/perl -w
# Load the Play-by-Play events for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse play by play script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate this is the right version
validate_integration_version(1);

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_EVENT', 'SPORTS_NHL_GAME_EVENT_BLOCKEDSHOT', 'SPORTS_NHL_GAME_EVENT_FACEOFF', 'SPORTS_NHL_GAME_EVENT_GIVEAWAY', 'SPORTS_NHL_GAME_EVENT_GOAL', 'SPORTS_NHL_GAME_EVENT_HIT', 'SPORTS_NHL_GAME_EVENT_MISSEDSHOT', 'SPORTS_NHL_GAME_EVENT_ONICE', 'SPORTS_NHL_GAME_EVENT_PENALTY', 'SPORTS_NHL_GAME_EVENT_SHOOTOUT', 'SPORTS_NHL_GAME_EVENT_SHOT', 'SPORTS_NHL_GAME_EVENT_STOPPAGE', 'SPORTS_NHL_GAME_EVENT_TAKEAWAY', 'SPORTS_NHL_GAME_EVENT_TIMEOUT' );

  clear_previous($season, $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  #reorder_tables(@tables) if $ARGV[3] eq '--order'; # Do not run for performance reasons
  exit;
}

# First check, does the file exist, and if it does, load it
my ($file, $contents) = load_file($season_dir, $game_type, $game_id, 'play_by_play');
print "# Loading play-by-play for $season // $game_type // $game_id\n";

# Get the list of involved teams
my $pattern = '<img src="[^"]+logo[ac](\w{3})(?:fr)?\.gif" alt="([^"]+)" width="50" height="50" border="0"(?: \/)?>';
my @matches = ($contents =~ m/$pattern/gsi);
my %teams = ( );
$teams{'home'} = convert_team_id(uc($matches[2]));
$teams{'visitor'} = convert_team_id(uc($matches[0]));
my %team_goals = ( );

# Lists containing the data we'll store
my @events = ( );
my @events_on_ice = ( );
my @events_faceoff = ( );
my @events_penalty = ( );
my @events_goal = ( );
my @events_stoppage = ( );
my @events_timeout = ( );
my @events_hit = ( );
my @events_giveaway = ( );
my @events_takeaway = ( );
my @events_blockedshot = ( );
my @events_missedshot = ( );
my @events_shot = ( );
my @events_challenge = ( );
my @events_shootout = ( );

my @events_unknown = ( );

# Get the events
$pattern = '<tr class="evenColor">\s*<td align="center" class="[^"]*">([^<]*)<\/td>\s*<td class="[^"]*" align="center">([^<]*)<\/td>\s*<td class="[^"]*" align="center">([^<]*)<\/td>\s*<td class="[^"]*" align="center">[^<]*<br(?: \/)?>([^<]*)<\/td>\s*<td class="[^"]*"(?: align="center")?>([^<]*)<\/td>\s*<td class="[^"]*">(.*?)<\/td>\s*<td class="[^"]*">(?:\s*<table border="0" cellpadding="0" cellspacing="0">\s*<tr>\s*)?((?:\s*<td align="center">\s*<table border="0" cellpadding="0" cellspacing="0">.*?<\/table>\s*<\/td>\s*(?:<td align="center">&nbsp;<\/td>\s*)?)*)(?:<\/tr>\s*<\/table>\s*)?(?:&nbsp;)?<\/td>\s*<td class="[^"]*">(?:\s*<table border="0" cellpadding="0" cellspacing="0">\s*<tr>\s*)?((?:\s*<td align="center">\s*<table border="0" cellpadding="0" cellspacing="0">.*?<\/table>\s*<\/td>\s*(?:<td align="center">&nbsp;<\/td>\s*)?)*)(?:<\/tr>\s*<\/table>\s*)?(?:&nbsp;)?<\/td>\s*<\/tr>';
@matches = ($contents =~ m/$pattern/gsi);
print STDERR "No play-by-play matches found.\n" if @matches == 0;
my $event_id = 0;
while (my @event = splice(@matches, 0, 8)) {
  # Ignore any shootout events (period == 5) and period start/end notices
  my $useful = 1;

  # Blank events (2013/regular/971 - Goal carried over in re-scheduled game)
  if ($event[1] eq '') {
    $event[1] = 1;
    $event[2] = 'EV';
    $event[3] = '20:00';
  }

  # Period / Game Start / Finish notes?
  my @ignore_codes = ('', 'PGSTR', 'PGEND', 'ANTHEM', 'PSTR', 'PEND', 'EISTR', 'EIEND', 'SOC', 'GEND', 'GOFF');
  $useful &= !grep(/^$event[4]$/, @ignore_codes);

  # Only shots in a shootout?
  $useful &= ($game_type eq 'playoff' || $event[1] != 5 || ($event[4] eq 'GOAL' || $event[4] eq 'SHOT' || $event[4] eq 'MISS'));

  if ($useful) {
    # Increment the event_id counter
    my $ignore = 0;
    $event_id++;

    # So what was it?
    my $event_type = '?';
    my $event_info = "('$season', '$game_type', '$game_id', '$event_id', ";
    my $event_zone = 'NULL';
    my $event_team_id; my $event_play;

    if ($game_type eq 'playoff' || $event[1] !~ m/^5$/) {
      # Which zone did it occur in?
      ($event_zone) = ($event[5] =~ m/(\w{3})\. Zone/gsi);

      # Regulation / Overtime event
      if ($event[4] eq 'FAC') {
        # A faceoff
        $event_type = 'FACEOFF';
        $event_info .= parse_faceoff($event[5], \%teams, $event_zone, \$event_team_id, \$event_play);
        push @events_faceoff, $event_info;

      } elsif ($event[4] eq 'PENL') {
        # A penalty
        $event_type = 'PENALTY';
        my $extra_event_info = '';
        ($ignore, $extra_event_info) = parse_penalty($event[5], $season, $game_id, $event_id, \%teams, \$event_team_id, \$event_play);
        if (!$ignore) {
          $event_info .= $extra_event_info;
          push @events_penalty, $event_info;
        }

      } elsif ($event[4] eq 'GOAL') {
        # A goal
        $event_type = 'GOAL';
        $event_info .= parse_goal($event[5], $event[2], \%teams, $event[7], $event[6], \%team_goals, \$event_team_id, \$event_play);
        push @events_goal, $event_info;

      } elsif ($event[4] eq 'STOP') {
        # A stoppage

        # Does this stoppage include a team's timeout?
        if ($event[5] =~ m/VISITOR TIMEOUT/gsi || $event[5] =~ m/HOME TIMEOUT/gsi) {
          # Strip from the current event, and create a new one
          my $team_timeout = '';
          my @tmp = split(',', $event[5]);
          foreach my $tmp (@tmp) {
            if (!defined($team_timeout) || $team_timeout eq '') {
              ($team_timeout) = ($tmp =~ m/^(\w+) TIMEOUT$/gsi);
            }
          }
          $event[5] =~ s/,?\w+ TIMEOUT//g;
          @tmp = ( $event[0], $event[1], $event[2], $event[3], 'TIMEOUT', lc $team_timeout, $event[6], $event[7] );
          splice @matches, 0, 0, @tmp;

        # Or is it a challenge (and we have no subsequent info)?
        } elsif ($event[5] =~ m/^Chlg/gsi) {
          # Check the next event to see if it's a CHL, and if so that it has detail
          if ($matches[4] ne 'CHL' || $matches[5] !~ m/^[a-z]/i) {
            my @chlg = ($event[5] =~ m/^CHLG ([A-Z]+)\s*?\-\s*?(.+)$/si);
            if (uc($chlg[0]) eq 'HM') {
              $chlg[0] = $teams{'home'};
            } elsif (uc($chlg[0]) eq 'VIS') {
              $chlg[0] = $teams{'visitor'};
            }
            my @tmp = ( $event[0], $event[1], $event[2], $event[3], 'CHL', "$chlg[0] Challenge - $chlg[1]", $event[6], $event[7] );
            splice @matches, 0, 0, @tmp;
          }
          # In both instances, we don't really want to count this as a stoppage
          $event[5] = '';
        }

        # Now parse, if there is anything to parse
        if ($event[5] ne '') {
          $event_type = 'STOPPAGE';
          $event_info .= parse_stoppage($event[5], \$event_team_id, \$event_play);
          push @events_stoppage, $event_info;
        } else {
          # Ignore, and move on
          $event_id--;
          $ignore = 1;
        }

      } elsif ($event[4] eq 'TIMEOUT') {
        # A team timeout
        $event_type = 'TIMEOUT';
        $event_info .= parse_timeout($event[5], \%teams, \$event_team_id, \$event_play);
        push @events_timeout, $event_info;

      } elsif ($event[4] eq 'HIT') {
        # A hit
        $event_type = 'HIT';
        $event_info .= parse_hit($event[5], \$event_team_id, \$event_play);
        push @events_hit, $event_info;

      } elsif ($event[4] eq 'GIVE') {
        # A giveaway
        $event_type = 'GIVEAWAY';
        $event_info .= parse_giveaway($event[5], 'Giveaway', \$event_team_id, \$event_play);
        push @events_giveaway, $event_info;

      } elsif ($event[4] eq 'TAKE') {
        # A takeaway
        $event_type = 'TAKEAWAY';
        $event_info .= parse_giveaway($event[5], 'Takeaway', \$event_team_id, \$event_play);
        push @events_takeaway, $event_info;

      } elsif ($event[4] eq 'BLOCK') {
        # A blocked shot
        $event_type = 'BLOCKEDSHOT';
        $event_info .= parse_blockedshot($event[5], \%teams, \$event_team_id, \$event_play);
        push @events_blockedshot, $event_info;

      } elsif ($event[4] eq 'MISS') {
        # A missed shot
        $event_type = 'MISSEDSHOT';
        $event_info .= parse_missedshot($event[5], \$event_team_id, \$event_play);
        push @events_missedshot, $event_info;

      } elsif ($event[4] eq 'SHOT') {
        # A shot
        $event_type = 'SHOT';
        $event_info .= parse_shot($event[5], \%teams, $event[7], $event[6], \$event_team_id, \$event_play);
        push @events_shot, $event_info;

      } elsif ($event[4] eq 'CHL') {
        # A coach's challenge
        my $parsed_info = parse_challenge($event[5], \$event_team_id, \$event_play);
        if (defined($parsed_info)) {
          $event_type = 'CHALLENGE';
          $event_info .= $parsed_info;
          push @events_challenge, $event_info;
        } else {
          $ignore = 1;
        }
      }

      # Convert the zone for storing
      if (!defined($event_zone)) {
        $event_zone = 'NULL';
      } else {
        $event_zone = "'" . lc($event_zone) . "'";
      }

    } else {
      # Shoot out
      $event_type = 'SHOOTOUT';
      $event_info .= parse_shootout($event[4], $event[5], \%teams, $event[7], $event[6], \$event_team_id, \$event_play);
      push @events_shootout, $event_info;
    }

    # Add, unless otherwise told not to
    if (!$ignore) {
      # Create the base event
      $event_play = 'NULL' if !defined($event_play);
      $event_team_id = 'NULL' if !defined($event_team_id);
      # Though store slightly differently if unknown (for future fixing)
      if ($event_type ne '?') {
        push @events, "('$season', '$game_type', '$game_id', '$event_id', '$event[1]', '00:$event[3]', '$event_type', $event_zone, $event_team_id, $event_play)";
      } else {
        # Store raw details for future debugging
        $event_type = convert_text($event[4]);
        $event_play = convert_text($event[5]);
        push @events_unknown, "('$season', '$game_type', '$game_id', '$event_id', '$event[1]', '00:$event[3]', '$event_type', '$event_play')";
      }

      # Add to the on-ice players
      for (my $i = 0; $i < 2; $i++) {
        $pattern = '<font style="cursor:hand;" title="[^"]+">(\d+)</font>';
        my @sub_matches = ($event[6+$i] =~ m/$pattern/gsi);
        my @team_on_ice = ( );
        foreach my $player (@sub_matches) { push @team_on_ice, "'$player'"; }
        while ($#team_on_ice < 5) { push @team_on_ice, 'NULL'; }

        # If more than six players listed, take just the last six
        if (@team_on_ice > 6) {
          @team_on_ice = splice(@team_on_ice, @team_on_ice - 6, 6);
        }

        push @events_on_ice, "('$season', '$game_type', '$game_id', '$event_id', '" . $teams{!$i ? 'visitor' : 'home'} . "', " . join(', ', @team_on_ice) . ')';
      }
    }
  }
}

# Now output the SQL
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT (`season`, `game_type`, `game_id`, `event_id`, `period`, `event_time`, `event_type`, `zone`, `team_id`, `play`) VALUES " . join(', ', @events) . ";\n\n" if check_disp(1) && @events > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_ONICE (`season`, `game_type`, `game_id`, `event_id`, `team_id`, `player_1`, `player_2`, `player_3`, `player_4`, `player_5`, `player_6`) VALUES " . join(', ', @events_on_ice) . ";\n\n" if check_disp(2) && @events_on_ice > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_FACEOFF (`season`, `game_type`, `game_id`, `event_id`, `home`, `visitor`, `winner`) VALUES " . join(', ', @events_faceoff) . ";\n\n" if check_disp(3) && @events_faceoff > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_PENALTY (`season`, `game_type`, `game_id`, `event_id`, `team_id`, `jersey`, `served_by`, `type`, `pims`, `drawn_team_id`, `drawn_jersey`) VALUES " . join(', ', @events_penalty) . ";\n\n" if check_disp(4) && @events_penalty > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_GOAL (`season`, `game_type`, `game_id`, `event_id`, `by_team_id`, `goal_num`, `scorer`, `assist_1`, `assist_2`, `on_team_id`, `on_jersey`, `shot_type`, `shot_distance`, `goal_type`, `goalie_status`, `penalty_shot`) VALUES " . join(', ', @events_goal) . ";\n\n" if check_disp(5) && @events_goal > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_STOPPAGE (`season`, `game_type`, `game_id`, `event_id`, `reason`) VALUES " . join(', ', @events_stoppage) . ";\n\n" if check_disp(6) && @events_stoppage > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_TIMEOUT (`season`, `game_type`, `game_id`, `event_id`, `team_id`) VALUES " . join(', ', @events_timeout) . ";\n\n" if check_disp(7) && @events_timeout > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_HIT (`season`, `game_type`, `game_id`, `event_id`, `by_team_id`, `by_jersey`, `on_team_id`, `on_jersey`) VALUES " . join(', ', @events_hit) . ";\n\n" if check_disp(8) && @events_hit > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_GIVEAWAY (`season`, `game_type`, `game_id`, `event_id`, `team_id`, `jersey`) VALUES " . join(', ', @events_giveaway) . ";\n\n" if check_disp(9) && @events_giveaway > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_TAKEAWAY (`season`, `game_type`, `game_id`, `event_id`, `team_id`, `jersey`) VALUES " . join(', ', @events_takeaway) . ";\n\n" if check_disp(10) && @events_takeaway > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_BLOCKEDSHOT (`season`, `game_type`, `game_id`, `event_id`, `by_team_id`, `by_jersey`, `on_team_id`, `on_jersey`, `shot_type`) VALUES " . join(', ', @events_blockedshot) . ";\n\n" if check_disp(11) && @events_blockedshot > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_MISSEDSHOT (`season`, `game_type`, `game_id`, `event_id`, `by_team_id`, `by_jersey`, `shot_type`, `shot_distance`, `miss_reason`, `penalty_shot`) VALUES " . join(', ', @events_missedshot) . ";\n\n" if check_disp(12) && @events_missedshot > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_SHOT (`season`, `game_type`, `game_id`, `event_id`, `by_team_id`, `by_jersey`, `on_team_id`, `on_jersey`, `shot_type`, `shot_distance`, `penalty_shot`) VALUES " . join(', ', @events_shot) . ";\n\n" if check_disp(13) && @events_shot > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_CHALLENGE (`season`, `game_type`, `game_id`, `event_id`, `team_id`, `challenge`, `outcome`) VALUES " . join(', ', @events_challenge) . ";\n\n" if check_disp(14) && @events_challenge > 0;
print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_SHOOTOUT (`season`, `game_type`, `game_id`, `event_id`, `by_team_id`, `by_jersey`, `on_team_id`, `on_jersey`, `shot_type`, `shot_distance`, `status`) VALUES " . join(', ', @events_shootout) . ";\n\n" if check_disp(15) && @events_shootout > 0;

print "INSERT IGNORE INTO SPORTS_NHL_GAME_EVENT_UNKNOWN (`season`, `game_type`, `game_id`, `event_id`, `period`, `event_time`, `event_type`, `play`) VALUES " . join(', ', @events_unknown) . ";\n\n" if check_disp(16) && @events_unknown > 0;

# Convert the raw play-by-play zone in to a text display value
sub format_pbp_zone {
  my ($event_zone) = @_;

  return 'unknown'
    if !defined($event_zone);

  if ($event_zone eq 'Neu') {
    return 'neutral';
  } elsif ($event_zone eq 'Off') {
    return 'offensive';
  } elsif ($event_zone eq 'Def') {
    return 'defensive';
  } else {
    return 'unknown';
  }
}

# Parse a face-off event into some SQL
sub parse_faceoff {
  my ($event, $teams, $event_zone, $event_team_id, $event_play) = @_;

  # Get details
  my $pattern = '([\w\.]{3})?(?: won)?.*?#(\d+).*?#(\d+)';
  my @matches = ($event =~ m/$pattern/gsi);
  if (!defined($matches[0]) || $matches[0] eq 'Def' || $matches[0] eq 'Off' || $matches[0] eq 'Neu') {
    $matches[0] = 'NULL';
  } else {
    $matches[0] = convert_team_id($matches[0]);
  }
  $$event_team_id = "'$matches[0]'";

  # Summarise
  if ($matches[0] ne 'NULL') {
    my $home_win = ($matches[0] eq $$game_info{'home_id'});
    my $opp_team = ($home_win ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
    $$event_play = '\'{{' . $matches[0] . '_' . ($home_win ? $matches[2] : $matches[1]) . '}} won ' . format_pbp_zone($event_zone) . ' zone faceoff against {{' . $opp_team . '_' . ($home_win ? $matches[1] : $matches[2]) . '}}\'';
    $matches[0] = "'$matches[0]'";
  }

  # Return
  return "'$matches[2]', '$matches[1]', " . $matches[0] . ")";
}

# Parse a penalty event into some SQL
sub parse_penalty {
  my ($event, $season, $game_id, $event_id, $teams, $event_team_id, $event_play) = @_;
  my $event_info = '';
  my $unknown_team = 0;

  # Basic penalty info
  my $pattern = '^([\w\.]{3})? ?(.*?)&nbsp;(.*?)\((\d+)? min\)';
  my @matches = ($event =~ m/$pattern/gsi);
  if (uc ($matches[0] . $matches[1]) eq 'TEAM') {
    # We don't know who had the penalty yet
    $matches[1] = '0';
    $unknown_team = 1;

  } elsif (uc $matches[1] eq 'TEAM') {
    $matches[1] = '0';
  } else {
    $pattern = '#(\d+) ';
    ($matches[1]) = ($matches[1] =~ m/$pattern/gsi);
  }

  if (!defined($matches[1])) {
    $matches[1] = 'NULL';
  } else {
    $matches[1] = "'$matches[1]'";
  }

  # How many PIMs?  If none, we're ignoring
  if (defined($matches[3]) && $matches[3] == 0) {
    return (1, '');
  }

  # Try and parse the penalty description
  $matches[2] = parse_penalty_type($matches[2]);

  # Served by a different player?
  $pattern = 'Served by: #(\d+)';
  my ($served_by) = ($event =~ m/$pattern/gsi);
  if (!defined($served_by)) {
    $served_by = $matches[1];
  } else {
    $served_by = "'$served_by'";
  }

  # If we don't know which team, then guess based on who served it
  if ($unknown_team) {
    $pattern .= ' ([^\,]+),?';
    my (undef, $served_name) = ($event =~ m/$pattern/gsi);

    # Now check who's on-ice
    print "SELECT `SPORTS_NHL_GAME_LINEUP`.`team_id` INTO \@penl_team_${event_id}
FROM `SPORTS_NHL_GAME_LINEUP`
LEFT JOIN `SPORTS_NHL_PLAYERS`
  ON (`SPORTS_NHL_PLAYERS`.`player_id` = `SPORTS_NHL_GAME_LINEUP`.`player_id`)
WHERE `SPORTS_NHL_GAME_LINEUP`.`season` = '$season'
AND   `SPORTS_NHL_GAME_LINEUP`.`game_id` = '$game_id'
AND   `SPORTS_NHL_GAME_LINEUP`.`jersey` = $served_by
AND   `SPORTS_NHL_PLAYERS`.`surname` LIKE '$served_name';\n";
    $matches[0] = '@penl_team_' . $event_id;
  }

  # Now form the SQL
  $matches[0] = ("'" . convert_team_id($matches[0]) . "'") if ($matches[0] !~ m/^\@/);
  $matches[2] = convert_text($matches[2]);
  for (my $j = 2; $j <= 3; $j++) {
    if (!defined($matches[$j]) || $matches[$j] eq '') {
      $matches[$j] = 'NULL';
    } else {
      $matches[$j] = "'$matches[$j]'";
    }
  }
  $event_info = "$matches[0], $matches[1], $served_by, $matches[2], $matches[3], ";
  $$event_team_id = $matches[0];

  # Was this penalty drawn by a specific player?
  $pattern = 'Drawn By: ([\w\.]{3}) #(\d+)';
  my @d_matches = ($event =~ m/$pattern/gsi);
  if (@d_matches) {
    $event_info .= "'" . convert_team_id($d_matches[0]) . "', $d_matches[1])";
  } else {
    # Unknown, so calculate the team
    my $drawn_team = (convert_team_id($matches[0]) eq $$teams{'home'} ? $$teams{'visitor'} : $$teams{'home'});
    $event_info .= "'$drawn_team', NULL)";
  }

  # Summarise
  $$event_play = '';
  $$event_play = 'Bench ' if $matches[1] eq "'0'";
  if ($matches[3] eq "'2'") {
    $$event_play .= 'Minor Penalty';
  } elsif ($matches[3] eq "'4'") {
    $$event_play .= 'Double-Minor';
  } elsif ($matches[3] eq "'5'") {
    $$event_play .= 'Major Penalty';
  } else {
    $$event_play .= 'Penalty';
  }
  $$event_play .= " on {{$matches[0]_$matches[1]}}" if $matches[1] ne "'0'";
  $$event_play .= " for $matches[2]";
  $$event_play .= ', drawn by {{' . convert_team_id($d_matches[0]) . "_$d_matches[1]}}"
    if @d_matches;
  $$event_play .= " (Served by {{$matches[0]_$served_by}})"
    if $served_by ne $matches[1];
  $$event_play =~ s/'//g;
  $$event_play = "'$$event_play'";

  return (0, $event_info);
}

# Parse a goal event into some SQL
sub parse_goal {
  my ($event, $event_strength, $teams, $home_onice, $visitor_onice, $team_goals, $event_team_id, $event_play) = @_;

  # Basic goal info
  my $pattern = '^([\w\.]{3}) #(\d+)[^,]+, ((?:Penalty Shot, )?)([^,]*),?[^,]*(?:, )?(\d+) ft';
  my @basic = ($event =~ m/$pattern/gsi);
  # Situation where we have no shot distance
  if (!@basic) {
    $pattern = substr($pattern, 0, -8);
    @basic = ($event =~ m/$pattern/gsi);
    $basic[3] = $basic[4] = undef;
  }

  $basic[0] = convert_team_id($basic[0]);
  $$event_team_id = "'$basic[0]'";
  if (!defined($basic[3]) || $basic[3] =~ m/ Zon/gsi) {
    $basic[3] = 'NULL';
  } else {
    $basic[3] = convert_text($basic[3]);
  }
  $basic[4] = 'NULL'
    if !defined($basic[4]);

  # Assists?
  $pattern = 'Assists?: #(\d+)[^;]+((?:;.*)?)';
  my @assists = ($event =~ m/$pattern/gsi);
  if (!defined($assists[0])) {
    # Unassissted goal
    @assists = ( 'NULL', 'NULL' );

  } elsif (!defined($assists[1]) || $assists[1] eq '') {
    # Single assist
    $assists[1] = 'NULL';

  } else {
    # Two assists
    $pattern = '#(\d+) ';
    ($assists[1]) = ($assists[1] =~ m/$pattern/gsi);
  }

  # Was a goalie pulled?
  my $goalie_status = 'NULL';
  my $on_team = 'NULL';
  my $on_jersey = 'NULL';

  # First, EN
  my $test_onice = ($basic[0] eq $$teams{'home'} ? $visitor_onice : $home_onice);
  if ($test_onice !~ m/<td align="center">G<\/td>/) {
    $goalie_status = "EN"
      if $test_onice ne '';
  } else {
    # There was a goalie, so identify who he was
    $pattern = '<font style="cursor:hand;" title="Goalie [^"]+">(\d+)<\/font>';
    $on_team = "'" . ($basic[0] eq $$teams{'home'} ? $$teams{'visitor'} : $$teams{'home'}) . "'";
    ($on_jersey) = ($test_onice =~ m/$pattern/gsi);
    $on_jersey = "'$on_jersey'";
  }

  # Next, WG
  if ($goalie_status eq 'NULL' && (!defined($basic[2]) || $basic[2] eq '')) {
    my $test_onice = ($basic[0] eq $$teams{'home'} ? $home_onice : $visitor_onice);
    $goalie_status = "WG"
      if $test_onice !~ m/<td align="center">G<\/td>/ && $test_onice ne '';
  }

  # How many goals have this team scored?
  $$team_goals{$basic[0]} = 0 if (!defined($$team_goals{$basic[0]}));
  my $goal_num = ++$$team_goals{$basic[0]};

  # Summarise
  my %extra = ();
  $$event_play = '\'';
  # Type
  push @{$extra{'type'}}, 'Empty Net'
    if $goalie_status eq 'EN';
  push @{$extra{'type'}}, 'Extra Skater'
    if $goalie_status eq 'WG';
  push @{$extra{'type'}}, 'Power Play'
    if $event_strength eq 'PP';
  push @{$extra{'type'}}, 'Shorthanded'
    if $event_strength eq 'SH';
  push @{$extra{'type'}}, 'Penalty Shot'
    if defined($basic[2]) && $basic[2] ne '';
  $$event_play .= join(', ', @{$extra{'type'}}) . ' '
    if defined($extra{'type'}) && @{$extra{'type'}};
  # Scorer
  $$event_play .= "Goal, scored by {{$basic[0]_$basic[1]}}";
  # Assists
  push @{$extra{'assists'}}, "{{$basic[0]_$assists[0]}}"
    if $assists[0] ne 'NULL';
  push @{$extra{'assists'}}, "{{$basic[0]_$assists[1]}}"
    if $assists[1] ne 'NULL';
  $$event_play .= ' assisted by ' . join(' and ', @{$extra{'assists'}})
    if ($extra{'assists'}) && @{$extra{'assists'}};
  # Shot
  $$event_play .= " ($basic[4]ft $basic[3])"
    if $basic[3] ne 'NULL';
  $$event_play .= "'";

  # Return
  return "'$basic[0]', '$goal_num', '$basic[1]', " . ($assists[0] ne 'NULL' ? "'$assists[0]'" : 'NULL') . ", " . ($assists[1] ne 'NULL' ? "'$assists[1]'" : 'NULL') . ", $on_team, $on_jersey, " . ($basic[3] ne 'NULL' ? "'$basic[3]'" : 'NULL') . ", " . ($basic[4] ne 'NULL' ? "'$basic[4]'" : 'NULL') . ", '$event_strength', " . ($goalie_status ne 'NULL' ? "'$goalie_status'" : 'NULL') . ", '" . (defined($basic[2]) && $basic[2] ne '' ? '1' : '0') . "')";
}

# Parse a stoppage event into some SQL
sub parse_stoppage {
  my ($event, $event_team_id, $event_play) = @_;

  # Format the text so it's a bit nicer
  my @tmp = split(',| ', $event);
  my @sorted_tmp = sort { length($b) cmp length($a) } @tmp;
  foreach my $tmp (@sorted_tmp) {
    if ($tmp ne 'TV') {
      my $new = ucfirst(lc($tmp));
      $event =~ s/$tmp/$new/g;
    }
  }
  $event =~ s/,/, /g;
  $event = convert_text($event);

  # Summarise
  $$event_play = "'Stoppage: $event'";

  return "'$event')";
}

# Parse a timeout event into some SQL
sub parse_timeout {
  my ($event, $teams, $event_team_id, $event_play) = @_;

  # Summarise
  $$event_team_id = "'$$teams{$event}'";
  $$event_play = "'Timeout taken by {{TEAM_$$teams{$event}}}'";

  # Return event SQL
  return "'" . $$teams{$event} . "')";
}

# Parse a hit event into some SQL
sub parse_hit {
  my ($event, $event_team_id, $event_play) = @_;

  # Who did the hit?
  my $pattern = '^([\w\.]{3}) #(\d+)?';
  my @hitter = ($event =~ m/$pattern/gsi);
  $hitter[0] = convert_team_id($hitter[0]);
  $$event_team_id = "'$hitter[0]'";
  my $event_info = "'" . $hitter[0] . "', " . (defined($hitter[1]) ? "'$hitter[1]'" : 'NULL') . ', ';

  # Who was hit?
  $pattern =~ s/\^/HIT /g;
  my @hittee = ($event =~ m/$pattern/gsi);
  if (defined($hittee[1])) {
    $hittee[0] = convert_team_id($hittee[0]);
    $event_info .= "'" . $hittee[0] . "', '$hittee[1]')";
  } else {
    $event_info .= 'NULL, NULL)';
  }

  # Summarise the play
  $$event_play = '\'Hit by ' . (defined($hitter[1]) ? "{{$hitter[0]_$hitter[1]}}" : 'unknown player');
  $$event_play .= " on {{$hittee[0]_$hittee[1]}}"
    if defined($hittee[1]);
  $$event_play .= '\'';

  return $event_info;
}

# Parse a giveaway event into some SQL
sub parse_giveaway {
  my ($event, $type, $event_team_id, $event_play) = @_;

  # Get the info
  my $pattern = '^([\w\.]{3}).*?#(\d+)';
  my @matches = ($event =~ m/$pattern/gsi);
  $matches[0] = convert_team_id($matches[0]);
  $$event_team_id = "'$matches[0]'";

  # Summarise
  $$event_play = "'$type by {{$matches[0]_$matches[1]}}'";

  # Return
  return "'" . convert_team_id($matches[0]) . "', '$matches[1]')";
}

# Parse a blocked shot event into some SQL
sub parse_blockedshot {
  my ($event, $teams, $event_team_id, $event_play) = @_;
  my $pattern = '^([\w\.]{3})?.*?#(\d+)?.*?BY\s*([\w\.]{3}) #(\d+)[^,]+,\s*([^,]+)?,?';
  my @matches = ($event =~ m/$pattern/gsi);
  if (!defined($matches[4]) || $matches[4] =~ m/Zone$/) {
    $matches[4] = 'NULL';
  } else {
    $matches[4] = convert_text($matches[4]);
  }

  # Handle unknown shooter
  $matches[2] = convert_team_id($matches[2]) if defined($matches[2]);
  $$event_team_id = "'$matches[2]'";
  if (defined($matches[0])) {
    $matches[0] = convert_team_id($matches[0]);
  } else {
    $matches[0] = ($matches[2] eq $$teams{'home'} ? $$teams{'visitor'} : $$teams{'home'});
  }
  $matches[1] = 'NULL'
    if !defined($matches[1]);

  # Summarise
  my @extra = ();
  $$event_play = "\'Blocked shot by {{$matches[2]_$matches[3]}}";
  push @extra, $matches[4]
    if $matches[4] ne 'NULL';
  push @extra, "Shot by {{$matches[0]_$matches[1]}}"
    if $matches[1] ne 'NULL';
  $$event_play .= ' (' . join(' ', @extra) . ')'
    if @extra;
  $$event_play .= '\'';

  # Return
  return  "'$matches[2]', '$matches[3]', '$matches[0]', " . ($matches[1] ne 'NULL' ? "'$matches[1]'" : 'NULL') . ", " . ($matches[4] ne 'NULL' ? "'$matches[4]'" : 'NULL') . ")";
}

# Parse a missed shot event into some SQL
sub parse_missedshot {
  my ($event, $event_team_id, $event_play) = @_;

  # Parse
  my $pattern = '^([\w\.]{3}) #(\d+)[^,]+, ((?:Penalty Shot, )?)([^,]+), ([^,]+)?,?[^,]+, (\d+) ft';
  my @matches = ($event =~ m/$pattern/gsi);
  $matches[0] = convert_team_id($matches[0]);
  $matches[3] = convert_text($matches[3]);
  if ($matches[4] =~ m/Zon$/) {
    $matches[4] = 'NULL';
  } else {
    $matches[4] = convert_text($matches[4]);
  }
  $$event_team_id = "'$matches[0]'";

  # Summarise
  $$event_play = '\'' . (defined($matches[2]) && $matches[2] ne '' ? 'Penalty Shot Miss' : 'Missed Shot') . " by {{$matches[0]_$matches[1]}} (" . ($matches[4] ne 'NULL' ? "$matches[4], " : 'NULL') . "$matches[5]ft" . ($matches[3] ne 'NULL' ? ' ' . $matches[3] : '') . ")'";

  # Return
  return "'$matches[0]', '$matches[1]', '$matches[3]', '$matches[5]', " . ($matches[4] ne 'NULL' ? "'$matches[4]'" : 'NULL') . ", '" . (defined($matches[2]) && $matches[2] ne '' ? '1' : '0') . "')";
}

# Parse a shot event into some SQL
sub parse_shot {
  my ($event, $teams, $home_onice, $visitor_onice, $event_team_id, $event_play) = @_;
  my $pattern = '^([\w\.]{3}).*?#?(\d+)[^,]+, ((?:Penalty Shot, )?)([^,]+)?,?[^,]*, (\d+) ft';
  my @matches = ($event =~ m/$pattern/gsi);
  $matches[0] = convert_team_id($matches[0]);
  if (!defined($matches[3]) || $matches[3] =~ m/Zon$/) {
    $matches[3] = 'NULL';
  } else {
    $matches[3] = convert_text($matches[3]);
  }
  $$event_team_id = "'$matches[0]'";

  # Who was the shot on?
  my $test_onice = $matches[0] eq $$teams{'home'} ? $visitor_onice : $home_onice;
  $pattern = '<font style="cursor:hand;" title="Goalie [^"]+">(\d+)<\/font>';
  my ($opp_goalie) = ($test_onice =~ m/$pattern/gsi);

  # Summarise
  $$event_play = '\'' . (defined($matches[2]) && $matches[2] ne '' ? 'Penalty Shot Miss' : 'Shot on Goal') . " by {{$matches[0]_$matches[1]}} ($matches[4]ft" . ($matches[3] ne 'NULL' ? ' ' . $matches[3] : '') . ")'";

  # Return
  return "'" . $matches[0] . "', '$matches[1]', '" . ($matches[0] eq $$teams{'home'} ? $$teams{'visitor'} : $$teams{'home'}) . "', '$opp_goalie', " . ($matches[3] eq 'NULL' ? $matches[3] : "'$matches[3]'") . ", '$matches[4]', '" . (defined($matches[2]) && $matches[2] ne '' ? '1' : '0') . "')";
}

# Parse a coach's challenge into some SQL
sub parse_challenge {
  my ($event, $event_team_id, $event_play) = @_;
  my $pattern = '^([^ ]+) Challenge( \- [^\-]+)?( \- Result: .*?)?$';
  my @matches = ($event =~ m/$pattern/gsi);

  # Skip if not a valid option
  return if (!@matches);
  $matches[1] =~ s/^ \-\s+//
    if defined($matches[1]);
  splice @matches, 1, 0, undef
    if $matches[1] =~ m/^Result:/i;
  $matches[2] =~ s/^(?: \- )?Result:\s+//i
    if defined($matches[2]);

  # Which team (or was it the league?)
  my $is_league = lc($matches[0]) eq 'league';
  my $team_id = convert_team_id($matches[0]);
  $$event_team_id = $is_league ? 'NULL' : "'$team_id'";

  # What and outcome
  my $challenge = defined($matches[1]) ? convert_text(title_case($matches[1])) : undef;
  my $outcome = defined($matches[2]) ? title_case($matches[2]) : undef;

  # Summarise
  $$event_play = "'" . ($is_league ? "League " : '') . "Challenge" . (defined($matches[1]) ? ": $challenge" : '') . "." . (defined($matches[2]) ? " Outcome: $outcome" : '') . "'";

  # Return
  check_for_null(\$challenge);
  check_for_null(\$outcome);
  return "$$event_team_id, $challenge, $outcome)";
}

# Parse a shootout event into some SQL
sub parse_shootout {
  my ($event_type, $event, $teams, $home_onice, $visitor_onice, $event_team_id, $event_play) = @_;
  my $pattern = '^([\w\.]{3}).*?#?(\d+)[^,]+, ([^,]+), ([^,]+)?[^,\d]*,?[^,\d]*,?\s*(\d+) ft';
  my @matches = ($event =~ m/$pattern/gsi);

  # Catch situation where no player info is stored
  if (!@matches) {
    $pattern = '^([\w\.]{3}).*?#.*?(\d+) ft';
    @matches = ($event =~ m/$pattern/gsi);
    splice(@matches, 1, 0, '', '', '');
  }
  # Catch situation where no shot attempt is actually made
  if (!defined($matches[0])) {
    $pattern = '^([\w\.]{3}).*?#?(\d+)[^,]+, (\d+) ft';
    @matches = ($event =~ m/$pattern/gsi);
    # Squeeze in the missing event info in the middle of the list
    splice(@matches, 2, 0, undef, undef);
  }

  # Check values
  $matches[0] = convert_team_id($matches[0]);
  $matches[1] = '0' if !defined($matches[1]) || $matches[1] eq '';
  $matches[2] = 'NULL' if !defined($matches[2]) || $matches[2] eq '';
  $matches[3] = '' if (!defined($matches[3]) || $matches[3] =~ m/Zone$/);
  $matches[3] = convert_text($matches[3]);
  $$event_team_id = "'$matches[0]'";

  # What was the end result?
  my $status = '';
  if ($event_type eq 'GOAL') {
    $status = 'goal';
  } elsif ($event_type eq 'SHOT') {
    $status = 'save';
  } elsif ($event_type eq 'MISS' && $matches[3] eq 'Goalpost') {
    $status = 'goalpost';
  } else {
    $status = 'miss';
  }

  # Who was the shot on?
  my $test_onice = $matches[0] eq $$teams{'home'} ? $visitor_onice : $home_onice;
  $pattern = '<font style="cursor:hand;" title="Goalie [^"]+">(\d+)<\/font>';
  my ($opp_goalie) = ($test_onice =~ m/$pattern/gsi);

  # Summarise
  $$event_play = "'{{$matches[0]_$matches[1]}} ";
  $$event_play .= 'scored' if $status eq 'goal';
  $$event_play .= 'saved' if $status eq 'save';
  $$event_play .= 'hit goal post' if $status eq 'goalpost';
  $$event_play .= 'missed net' if $status eq 'miss';
  $$event_play .= " ($matches[4]ft";
  $$event_play .= " $matches[2]" if $matches[2] ne 'NULL';
  $$event_play .= ")'";

  # Return
  $matches[2] = "'$matches[2]'" if $matches[2] ne 'NULL';
  return "'$matches[0]', '$matches[1]', '" . ($matches[0] eq $$teams{'home'} ? $$teams{'visitor'} : $$teams{'home'}) . "', '$opp_goalie', $matches[2], '$matches[4]', '$status')";
}
