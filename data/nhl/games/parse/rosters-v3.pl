#!/usr/bin/perl -w
# Load the team Rosters for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse roster script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate this is the right version
validate_integration_version(3);

# Flag checks
my $flag_reset = (defined($ARGV[3]) && $ARGV[3] eq '--reset');
my $flag_order = (defined($ARGV[3]) && $ARGV[3] eq '--order');
my $flag_quick = (defined($ARGV[3]) && $ARGV[3] eq '--quick');

# First check, does the file exist, and if it does, load it
my $roster_file; our $rosters;
($roster_file, $rosters) = load_file($season_dir, $game_type, $game_id, 'rosters', { 'no_exit_on_error' => $flag_quick } );
print "# Loading players for $season // $game_type // $game_id\n";

# If no rosters supplied in quick mode, flag.
if (!defined($rosters) && $flag_quick) {
  print "# No roster file available but we're running in 'quick' mode, so silently aborting.\n";
  exit;
}

# Then check we have some info to parse if we want the lineups
my @starters = ($rosters =~ /<td align="left" width="70%" class="[^"]*bold[^"]*">/g);
if (!@starters) {
  if ($flag_quick) {
    print "# No starters found but we're running in 'quick' mode, so silently aborting.\n";
  } else {
    print STDERR "No starters found, implying incomplete rosters file.\n";
  }
  exit;
}

# Special processing flags?
if ($flag_reset || $flag_order || $flag_quick) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_LINEUP', 'SPORTS_NHL_GAME_LINEUP_ISSUES', 'SPORTS_NHL_GAME_SCRATCHES', 'SPORTS_NHL_GAME_SCRATCHES_ISSUES' );

  clear_previous($season, $game_type, $game_id, @tables) if $flag_reset || $flag_quick;
  reorder_tables(@tables) if $flag_order;
  exit if !$flag_quick;
}

# Get the mappings from the rosters
our %rosters = ( );
foreach my $type ('home', 'away') {
  my $type_file = ($type eq 'away' ? 'visitor' : $type);
  ($roster_file, $rosters{$type}) = load_file($season_dir, $game_type, $game_id, "teams_${type_file}", { 'ext' => 'json' });
}
# If any roster fixes are required to match the gamebook (that is the fault of the roster lookup), apply them now
my $fix_file = "$config{'base_dir'}/_data/fixes/$season_dir/game_roster.pl";
require $fix_file
  if -e $fix_file;
# Now convert the team roster to the mapping
my %maps = ( );
foreach my $type ('home', 'away') {
  %{$maps{$type}} = ( 'jersey' => { }, 'name' => { } );
  foreach my $cat (keys %{$rosters{$type}}) {
    foreach my $p (@{$rosters{$type}{$cat}}) {
      my $fname = convert_text($$p{'firstName'}{'default'});
      my $sname = convert_text($$p{'lastName'}{'default'});
      my %h = (
        'jersey' => $$p{'sweaterNumber'},
        'id' => $$p{'id'},
        'name' => "$fname $sname",
        'fname' => $fname,
        'sname' => $sname,
        'pos' => $$p{'positionCode'}
      );
      $maps{$type}{'jersey'}{$h{'jersey'}} = \%h
        if defined($h{'jersey'});
      my $name = uc $h{'name'};
      $maps{$type}{'name'}{$name} = \%h;
    }
  }
}

# Get each team in turn
my $team_id_pattern = '<img src="[^"]+logo[ac](?:std)?(\w{3})(?:fr)?\d*?\.gif" alt="([^"]+)" width="50" height="50" border="0"(?: \/)?>';
my $pattern = '<td v?align="(?:top)?(?:center)?" width="50%"(?: class="border")?>\s*<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">(.*?)<\/table>\s*<\/td>';
my @matches = ($rosters =~ m/$pattern/gsi);
print STDERR "No roster matches found.\n" if @matches == 0;
print "\n## Lineups\n\n";
for (my $i = 0; $i < 2; $i++) {
  my $type = (!$i ? 'visitor' : 'home');
  print "# $$game_info{$type.'_id'} ($type)\n";

  # Which team are we looking at?
  my @sub_matches = ($rosters =~ m/$team_id_pattern/gsi);
  my $team_id = convert_team_id(uc($sub_matches[!$i ? 0 : 2]));

  $pattern = '<tr>\s*<td align="center" width="15%"(?: class=")?([^"]*)"?>(\d+)<\/td>\s*<td align="center" width="15%"(?: class=")?([^"]*)"?>([^>]+)<\/td>\s*<td align="left" width="70%" class="([^"]*)">([^>]+)</td>\s*<\/tr>';
  @sub_matches = ($matches[$i] =~ m/$pattern/gsi);

  while (my @player = splice(@sub_matches, 0, 6)) {
    # Captain's status?
    my ($capt_status) = ($player[5] =~ m/\((\w)\)$/gsi);
    check_for_null(\$capt_status);
    $player[5] =~ s/  \(\w\)$//g;
    print "\n# $player[5], #$player[1] ($player[3])\n";

    # Starter?
    my $starter = ($player[4] =~ m/bold/gsi);
    $starter = 0 if ($starter eq '');

    # Do we have a matching map for this player?
    $player[5] = convert_text($player[5]);
    my $map = get_player_map($type, $player[1], $player[5]);

    # Yes, use the remote_id
    if (defined($$map{'id'})) {
      get_player_id($$map{'id'}, $$map{'fname'}, $$map{'sname'});
      print "INSERT INTO SPORTS_NHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started)
  VALUES ('$season', '$game_type', '$game_id', '$team_id', '$player[1]', \@player_$$map{'id'}, '$player[3]', $capt_status, '$starter');\n";

    # No, check via their name
    } else {
      print "INSERT INTO SPORTS_NHL_GAME_LINEUP (season, game_type, game_id, team_id, jersey, player_id, pos, capt_status, started)
SELECT '$season', '$game_type', '$game_id', '$team_id', '$player[1]', player_id, '$player[3]', $capt_status, '$starter'
FROM SPORTS_NHL_PLAYERS
WHERE fn_basic_html_comparison(UPPER(CONCAT(first_name, ' ', surname))) = fn_basic_html_comparison('$player[5]')
HAVING COUNT(DISTINCT player_id) = 1;\n";
      print "INSERT INTO SPORTS_NHL_GAME_LINEUP_ISSUES (season, game_type, game_id, team_id, jersey, player_name, pos, capt_status, started, problem)
SELECT '$season', '$game_type', '$game_id', '$team_id', '$player[1]', '$player[5]', '$player[3]', $capt_status, '$starter', IF(player_id IS NULL, '0 matches', CONCAT(COUNT(DISTINCT player_id), ' matches found: ', GROUP_CONCAT(player_id SEPARATOR ', '))) AS problem
FROM SPORTS_NHL_SCHEDULE
LEFT JOIN SPORTS_NHL_PLAYERS
  ON (fn_basic_html_comparison(UPPER(CONCAT(first_name, ' ', surname))) = fn_basic_html_comparison('$player[5]'))
WHERE SPORTS_NHL_SCHEDULE.season = '$season'
AND   SPORTS_NHL_SCHEDULE.game_type = '$game_type'
AND   SPORTS_NHL_SCHEDULE.game_id = '$game_id'
HAVING COUNT(DISTINCT SPORTS_NHL_PLAYERS.player_id) <> 1;\n";
    }
  }

  print "\n";
}

# And now the scratches
print "## Scratches\n\n";
for (my $i = 0; $i < 2; $i++) {
  my $type = (!$i ? 'visitor' : 'home');
  print "# $$game_info{$type.'_id'} ($type)\n";

  if (@matches > ($i + 2)) {
    # Which team are we looking at?
    my @sub_matches = ($rosters =~ m/$team_id_pattern/gsi);
    my $team_id = convert_team_id(uc($sub_matches[!$i ? 0 : 2]));

    $pattern = '<tr>\s*<td align="center" width="15%"(?: class=")?([^"]*)"?>(\d+)<\/td>\s*<td align="center" width="15%"(?: class=")?([^"]*)"?>([^>]+)<\/td>\s*<td align="left" width="70%" class="([^"]*)">([^>]+)</td>\s*<\/tr>';
    @sub_matches = ($matches[2+$i] =~ m/$pattern/gsi);
    while (my @player = splice(@sub_matches, 0, 6)) {
      $player[5] =~ s/  \(\w\)$//g;
      print "\n# $player[5], #$player[1] ($player[3])\n";

      # Do we have a matching map for this player?
      $player[5] = convert_text($player[5]);
      my $map = get_player_map($type, $player[1], $player[5]);

      # Yes, use the remote_id
      if (defined($$map{'id'})) {
        get_player_id($$map{'id'}, $$map{'fname'}, $$map{'sname'});
        print "INSERT INTO SPORTS_NHL_GAME_SCRATCHES (season, game_type, game_id, team_id, jersey, player_id, pos)
  VALUES ('$season', '$game_type', '$game_id', '$team_id', '$player[1]', \@player_$$map{'id'}, '$player[3]');\n";

      # No, check via their name
      } else {
        print "INSERT INTO SPORTS_NHL_GAME_SCRATCHES (season, game_type, game_id, team_id, jersey, player_id, pos)
SELECT '$season', '$game_type', '$game_id', '$team_id', '$player[1]', player_id, '$player[3]'
FROM SPORTS_NHL_PLAYERS
WHERE fn_basic_html_comparison(UPPER(CONCAT(first_name, ' ', surname))) = fn_basic_html_comparison('$player[5]')
LIMIT 1;\n";
        print "INSERT INTO SPORTS_NHL_GAME_SCRATCHES_ISSUES (season, game_type, game_id, team_id, jersey, player_name, pos, problem)
SELECT '$season', '$game_type', '$game_id', '$team_id', '$player[1]', '$player[5]', '$player[3]', IF(player_id IS NULL, '0 matches', CONCAT(COUNT(DISTINCT player_id), ' matches found: ', GROUP_CONCAT(player_id SEPARATOR ', '))) AS problem
FROM SPORTS_NHL_SCHEDULE
LEFT JOIN SPORTS_NHL_PLAYERS
  ON (fn_basic_html_comparison(UPPER(CONCAT(first_name, ' ', surname))) = fn_basic_html_comparison('$player[5]'))
WHERE SPORTS_NHL_SCHEDULE.season = '$season'
AND   SPORTS_NHL_SCHEDULE.game_type = '$game_type'
AND   SPORTS_NHL_SCHEDULE.game_id = '$game_id'
HAVING COUNT(DISTINCT SPORTS_NHL_PLAYERS.player_id) <> 1;\n";
      }
    }
  }

  print "\n";
}

#
# Generate the SQL to identify or create a player by remote ID
#
sub get_player_id {
  my ($remote_id, $fname, $sname) = @_;

  # Remote ID link
  print "INSERT IGNORE SPORTS_NHL_PLAYERS_IMPORT (player_id, remote_id)
  SELECT IFNULL(IMPORT.player_id, MAX(PLAYERS.player_id) + 1), '$remote_id'
  FROM SPORTS_NHL_SCHEDULE AS SCHEDULE
  LEFT JOIN SPORTS_NHL_PLAYERS_IMPORT AS IMPORT
    ON (IMPORT.remote_id = '$remote_id')
  LEFT JOIN SPORTS_NHL_PLAYERS AS PLAYERS
    ON (IMPORT.remote_id IS NULL)
  WHERE SCHEDULE.season = '$season'
  AND   SCHEDULE.game_type = '$game_type'
  AND   SCHEDULE.game_id = '$game_id';\n";
  # Who was created / identify previous
  print "SELECT player_id INTO \@player_$remote_id
FROM SPORTS_NHL_PLAYERS_IMPORT
WHERE remote_id = '$remote_id';\n";
  # Create if new
  print "INSERT IGNORE INTO SPORTS_NHL_PLAYERS (player_id, first_name, surname) VALUES (\@player_$remote_id, '$fname', '$sname');\n\n";
}

#
# Identify a player from the mapping
#
sub get_player_map {
  my ($type, $jersey, $name) = @_;
  $type = 'away' if $type eq 'visitor';

  # First, by jersey
  if (defined($maps{$type}{'jersey'}{$jersey})) {
    return $maps{$type}{'jersey'}{$jersey};
  # Then, by name
  } elsif (defined($maps{$type}{'name'}{$name})) {
    return $maps{$type}{'name'}{$name};
  }

  # Nope
  return undef;
}
