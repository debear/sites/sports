#!/usr/bin/perl -w
# Load the preview / recaps for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the parse play by play script.\n";
  exit 98;
}

# Note: Script deprecated after Mar-2017. TD.
print STDERR "ERROR: Attempting to run the deprecated writeups script for $ARGV[0] // $ARGV[1] // $ARGV[2].\n";

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season_dir, $game_type, $game_id) = @ARGV;
our $season = substr($season_dir, 0, 4);

# Team conversion list of domain to IDs
my $teams = get_team_domain_map($season);

my $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'game_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $game_id)\n";
  exit 98;
}

# Load local game config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Validate this is the right version
if ($config{'integration_version'} != 2) {
  print STDERR "Wrong integration version called: I am v2, but this game requires v$config{'integration_version'}\n";
  exit 10;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NHL_GAME_WRITEUP' );

  clear_previous($season, $game_type, $game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

# Load the writeup files and parse the JSON
my ($json_file, $json_contents) = load_file($season_dir, $game_type, $game_id, 'content', { 'ext' => 'json' });

my @writeups = ( );
my @types = ( 'preview', 'recap' );
foreach my $type (@types) {
  # First check, does the file exist?
  my ($file, $contents) = load_file($season_dir, $game_type, $game_id, $type, ( 'no_exit_on_error' => 1 ) );
  print "# Loading $type: $file\n";

  # Headline - JSON
  my $headline = "'" . convert_text($$json_contents{'editorial'}{$type}{'items'}[0]{'headline'}) . "'";
  # Edited - JSON
  my $edited = $$json_contents{'editorial'}{$type}{'items'}[0]{'date'};
  $edited =~ s/[\+-][01]\d[0-5]\d$//g;
  $edited =~ s/T/ /g;
  $edited = "'" . convert_text($edited) . "'";
  # Source - JSON
  my @contributers = ( );
  foreach my $c (@{$$json_contents{'editorial'}{$type}{'items'}[0]{'contributor'}{'contributors'}}) {
    push @contributers, convert_text($$c{'name'});
  }
  my $source = "'" . join(', ', @contributers) . ', ' . $$json_contents{'editorial'}{$type}{'items'}[0]{'contributor'}{'source'} . "'";

  # Article - File
  my ($lead, $body) = ($contents =~ m/<div class="article-item__bottom">\s*?<div class="article-item__preview">(.*?)<\/div>\s*?<div class="article-item__body">(.*?)<\/div>/gsi);
  my $article = $lead . $body;
  # Process...
  $article =~ s/<o:p><\/o:p>//g;
  $article =~ s/<p><span class="token token-video"[^>]*?><\/p>//g;
  # Tokens?
  my @tokens = ($article =~ m/<span class="token token-playerCard" id="(token-[^"]+)">(.*?)<\/span>/gsi);
  my %player_list = ( ); # Used to get a unique list of players to modify
  while (my ($token, $name) = splice(@tokens, 0, 2)) {
    next if defined($player_list{$token});

    my $replace;
    if (defined($$json_contents{'editorial'}{$type}{'items'}[0]{'tokenData'}{$token}{'id'})) {
      my $remote_id = $$json_contents{'editorial'}{$type}{'items'}[0]{'tokenData'}{$token}{'id'};
      $player_list{$remote_id} = convert_text($name);
      $replace = '{{PLAYER_REM_' . $remote_id . '}}';
    } else {
      $replace = $name;
      $player_list{$token} = 1;
    }
    $article =~ s/<span class="token token-playerCard" id="$token">.*?<\/span>/$replace/g;
  }
  # Strip other tokens and links...
  $article =~ s/<span class="token[^>]+>(.*?)<\/span>/$1/g;
  $article =~ s/<a[^>]*?>(.*?)<\/a>/$1/gsi;
  # Trim
  $article =~ s/^\s+//g;
  $article =~ s/\s+$//g;
  $article =~ s/\s{2,}/ /g;
  $article =~ s/^(?:<\/div>)+//gs;
  $article =~ s/<p>\s+/<p>/g;
  $article =~ s/\s+<\/p>/<\/p>/g;
  $article =~ s/<script.*?<\/script>//gs;
  $article =~ s/<span.*?<\/span>//gs;
  $article =~ s/<table.*?<\/table>//gs;
  $article =~ s/<div.*?<\/div>//gs;
  $article =~ s/<img.*?>//gs;
  # Encode into HTML-safe text
  $article =~ s/\r//g;
  $article = convert_text($article);
  $article =~ s/\n/\\n/g;

  # Swap from NHL.com ID's to ours
  print "SET \@article_${season}_${game_type}_${game_id}_${type} := \"$article\";\n";
  my @player_list = keys %player_list;
  foreach my $player (@player_list) {
    next if $player_list{$player} eq '1';
    print "SET \@rem_$player := IFNULL((SELECT CONCAT('{{PLAYER_', `player_id`, '}}') FROM `SPORTS_NHL_PLAYERS_IMPORT` WHERE `remote_id` = '$player'), '$player_list{$player}');\n";
    print "SELECT REPLACE(\@article_${season}_${game_type}_${game_id}_${type}, '{{PLAYER_REM_$player}}', \@rem_$player) INTO \@article_${season}_${game_type}_${game_id}_${type};\n";
  }

  # Now create
  print "\n";
  push @writeups, "('$season', '$game_type', '$game_id', '$type', $headline, $edited, $source, \@article_${season}_${game_type}_${game_id}_${type})";
}

# Output the SQL
print "INSERT INTO `SPORTS_NHL_GAME_WRITEUP` (`season`, `game_type`, `game_id`, `writeup_type`, `headline`, `edited`, `source`, `article`) VALUES " . join(', ', @writeups) . " ON DUPLICATE KEY UPDATE `headline` = VALUES(`headline`), `edited` = VALUES(`edited`), `source` = VALUES(`source`), `article` = VALUES (`article`);\n\n" if @writeups > 0;

