#!/usr/bin/perl -w
# Download, import and process team rosters from NHL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'rosters';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my @time = localtime();
my $date = time2date(time());
my $season = ($time[4] > 3 ? 1900 : 1899) + $time[5];
my $season_dir = season_dir($season);

# Identify date to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT MIN(game_date) AS first_game
FROM SPORTS_NHL_SCHEDULE
WHERE season = ?;';
my $sth = $dbh->prepare($sql);
$sth->execute($season);
my ($first_game) = $sth->fetchrow_array;
$date = 'initial' if $date le $first_game; # <= because we're running a day behind, so on first_game we're actually processing the day before

undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);

## Inform user
print "\nAcquiring data from $season // $date\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Download, if we haven't done so already (handled in-script)
#
print '=> Download: ';
command("$config{'base_dir'}/rosters/download.pl $season_dir $date",
        "$config{'log_dir'}/$logs{'roster_d'}.log",
        "$config{'log_dir'}/$logs{'roster_d'}.err");
done(9);

# If only downloading, go no further
end_script() if download_only();

#
# Parse
#
print '=> Parse: ';
command("$config{'base_dir'}/rosters/parse.pl $season_dir $date --reimport",
        "$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_p'}.err");
done(12);

#
# Import into the database
#
print '=> Importing Rosters: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_i'}.log",
        "$config{'log_dir'}/$logs{'roster_i'}.err");
done(0);

# Next group of scripts are the first section of post-processing
$config{'exit_status'} = 'post-process-1';

#
# Call player import script to update player details
#
print '=> Process Players: ';
command("$config{'base_dir'}/players/_import.pl",
        "$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_p'}.err");
done(2);

print '=> Importing Players: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_i'}.log",
        "$config{'log_dir'}/$logs{'players_i'}.err");
done(0);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'roster_d'  => '01_roster_download',
    'roster_p'  => '02_roster_parse',
    'roster_i'  => '03_roster_import',
    'players_p' => '04_players_process',
    'players_i' => '05_players_import',
  );
}
