#!/usr/bin/perl -w
# Perl config file for get_latest_changes with all DB, dir info, etc

use Dir::Self;
use MIME::Base64;

our %config;
$config{'base_dir'} = __DIR__;
$config{'server_sync'} = 'sports_f1';

# Database details
$config{'db_admin'} = 'debearco_admin';
$config{'db_name'} = 'debearco_sports';
$config{'db_user'} = $config{'db_name'};
# Password
my $db_pass_file = $config{'base_dir'}; $db_pass_file =~ s@/sites/.+$@/etc/passwd/web/db@;
$config{'db_pass'} = do { local(@ARGV, $/) = $db_pass_file; <> }; chomp($config{'db_pass'});
$config{'db_pass'} = decode_base64($config{'db_pass'});

# Return true to pacify the compiler
1;
