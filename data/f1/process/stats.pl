#!/usr/bin/perl -w
# Calculate the post-race stats

foreach my $type ('driver', 'team') {
  my $mode_test = ($type eq 'driver' ? 10 : 11);
  my $typeU = uc $type;

  print "\n" . '# ' . ucfirst($type) . ' Stat Calcs' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print "\n" . '# Prune' . "\n" . 'DELETE FROM SPORTS_FIA_' . $typeU . '_STATS WHERE season = "' . $season . '" AND series = "f1";' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  print "\n" . '# Generate' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  print '# 1: Front Rows' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           1 AS stat, SUM(GRID.grid_pos IN (1, 2)) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_GRID AS GRID
      ON (GRID.season = DRIVERS.season
      AND GRID.series = DRIVERS.series
      AND GRID.round = DRIVERS.round
      AND GRID.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND GRID.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 2: Poles' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           2 AS stat, SUM(GRID.grid_pos = 1) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_GRID AS GRID
      ON (GRID.season = DRIVERS.season
      AND GRID.series = DRIVERS.series
      AND GRID.round = DRIVERS.round
      AND GRID.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND GRID.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 3: Avg Qual Pos' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           3 AS stat, AVG(GRID.qual_pos) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_GRID AS GRID
      ON (GRID.season = DRIVERS.season
      AND GRID.series = DRIVERS.series
      AND GRID.round = DRIVERS.round
      AND GRID.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND GRID.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 4: Avg Grid Pos' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           4 AS stat, AVG(GRID.grid_pos) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_GRID AS GRID
      ON (GRID.season = DRIVERS.season
      AND GRID.series = DRIVERS.series
      AND GRID.round = DRIVERS.round
      AND GRID.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND GRID.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 5: Out-Qual Teammate' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           5 AS stat, SUM(GRID.qual_pos < GRID_TM.qual_pos) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_GRID AS GRID
      ON (GRID.season = DRIVERS.season
      AND GRID.series = DRIVERS.series
      AND GRID.round = DRIVERS.round
      AND GRID.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND GRID.car_no = DRIVERS.car_no)
    LEFT JOIN SPORTS_FIA_RACE_DRIVERS AS DRIVERS_TM
      ON (DRIVERS_TM.season = DRIVERS.season
      AND DRIVERS_TM.series = DRIVERS.series
      AND DRIVERS_TM.round = DRIVERS.round
      AND DRIVERS_TM.team_id = DRIVERS.team_id
      AND DRIVERS_TM.driver_id <> DRIVERS.driver_id)
    LEFT JOIN SPORTS_FIA_RACE_GRID AS GRID_TM
      ON (GRID_TM.season = DRIVERS_TM.season
      AND GRID_TM.series = DRIVERS_TM.series
      AND GRID_TM.round = DRIVERS_TM.round
      AND GRID_TM.race = GRID.race
      AND GRID_TM.car_no = DRIVERS_TM.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if ($type eq 'driver') && ($print_mode == -1 || $print_mode == $mode_test);
  print '# 6: Total Laps' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           6 AS stat, SUM(RESULT.laps) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 7: Race Finishes' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           7 AS stat, SUM(RESULT.result REGEXP "^[0-9]+$") AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 8: Classified Finishes' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           8 AS stat, 100 * (SUM(RESULT.result REGEXP "^[0-9]+$") / SUM(RESULT.result IS NOT NULL)) AS value,
           CONCAT("(", SUM(RESULT.result REGEXP "^[0-9]+$"), " / ", SUM(RESULT.result IS NOT NULL), ")") AS extra,
           NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 9: Points Finishes' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           9 AS stat, 100 * (SUM(RESULT.pts IS NOT NULL) / SUM(RESULT.result IS NOT NULL)) AS value,
           CONCAT("(", SUM(RESULT.pts IS NOT NULL), " / ", SUM(RESULT.result IS NOT NULL), ")") AS extra,
           NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 10: Podiums' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           10 AS stat, SUM(RESULT.pos IN (1, 2, 3)) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 11: Wins' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           11 AS stat, SUM(RESULT.pos = 1) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 12: Laps Led' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           12 AS stat, IFNULL(SUM(LEADERS.lap_to - LEADERS.lap_from + 1), 0) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_LEADERS AS LEADERS
      ON (LEADERS.season = DRIVERS.season
      AND LEADERS.series = DRIVERS.series
      AND LEADERS.round = DRIVERS.round
      AND LEADERS.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND LEADERS.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 13: Fastest Laps' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           13 AS stat, SUM(FASTEST_LAP.lap_time IS NOT NULL) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_FASTEST_LAP AS FASTEST_LAP
      ON (FASTEST_LAP.season = DRIVERS.season
      AND FASTEST_LAP.series = DRIVERS.series
      AND FASTEST_LAP.round = DRIVERS.round
      AND FASTEST_LAP.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND FASTEST_LAP.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 14: Avg Pos' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           14 AS stat, AVG(RESULT.pos) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print '# 15: Beat Teammate' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           15 AS stat, SUM(RESULT.pos < RESULT_TM.pos) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND RESULT.car_no = DRIVERS.car_no)
    LEFT JOIN SPORTS_FIA_RACE_DRIVERS AS DRIVERS_TM
      ON (DRIVERS_TM.season = DRIVERS.season
      AND DRIVERS_TM.series = DRIVERS.series
      AND DRIVERS_TM.round = DRIVERS.round
      AND DRIVERS_TM.team_id = DRIVERS.team_id
      AND DRIVERS_TM.driver_id <> DRIVERS.driver_id)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT_TM
      ON (RESULT_TM.season = DRIVERS_TM.season
      AND RESULT_TM.series = DRIVERS_TM.series
      AND RESULT_TM.round = DRIVERS_TM.round
      AND RESULT_TM.race = RESULT.race
      AND RESULT_TM.car_no = DRIVERS_TM.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if ($type eq 'driver') && ($print_mode == -1 || $print_mode == $mode_test);
  print '# 16: Avg Gain' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS
    SELECT DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id,
           16 AS stat,
           AVG(
             IF(GRID.grid_pos IS NULL OR RESULT.pos IS NULL OR RESULT.pos NOT REGEXP "^[0-9]+$",
                NULL,
                CAST(GRID.grid_pos AS SIGNED) - CAST(RESULT.pos AS SIGNED))
           ) AS value,
           NULL AS extra, NULL AS pos, NULL AS pos_tied
    FROM SPORTS_FIA_RACE_DRIVERS AS DRIVERS
    JOIN SPORTS_FIA_RACES AS RACE
      ON (RACE.season = DRIVERS.season
      AND RACE.series = DRIVERS.series
      AND RACE.round = DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_GRID AS GRID
      ON (GRID.season = DRIVERS.season
      AND GRID.series = DRIVERS.series
      AND GRID.round = DRIVERS.round
      AND GRID.race >= IF(FIND_IN_SET("SPRINT_RACE", RACE.quirks), 2, 1)
      AND GRID.car_no = DRIVERS.car_no)
    LEFT JOIN SPORTS_FIA_RACE_RESULT AS RESULT
      ON (RESULT.season = DRIVERS.season
      AND RESULT.series = DRIVERS.series
      AND RESULT.round = DRIVERS.round
      AND RESULT.race = GRID.race
      AND RESULT.car_no = DRIVERS.car_no)
    WHERE DRIVERS.season = "' . $season . '"
    AND   DRIVERS.series = "f1"
    GROUP BY DRIVERS.season, DRIVERS.series, DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  print "\n" . '# Sort Positions' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS (season, series, ' . $type . '_id, stat_id, pos, pos_tied)
    SELECT A.season, A.series, A.' . $type . '_id, A.stat_id,
           COUNT(DISTINCT B.' . $type . '_id) + 1 AS pos, 0 AS pos_tied
    FROM SPORTS_FIA_' . $typeU . '_STATS AS A
    JOIN SPORTS_FIA_STATS AS STAT
      ON (STAT.stat_id = A.stat_id)
    LEFT JOIN SPORTS_FIA_' . $typeU . '_STATS AS B
      ON (B.season = A.season
      AND B.series = A.series
      AND B.stat_id = A.stat_id
      AND ((STAT.best_desc = 1 AND IFNULL(B.value, -99999.99) > IFNULL(A.value, -99999.99))
        OR (STAT.best_desc = 0 AND IFNULL(B.value, 99999.99) < IFNULL(A.value, 99999.99))))
    WHERE A.season = "' . $season . '"
    GROUP BY A.season, A.series, A.' . $type . '_id, A.stat_id
  ON DUPLICATE KEY UPDATE pos = VALUES(pos), pos_tied = VALUES(pos_tied);' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  print '# Ties' . "\n"
  . 'INSERT INTO SPORTS_FIA_' . $typeU . '_STATS (season, series, ' . $type . '_id, stat_id, pos, pos_tied)
    SELECT A.season, A.series, A.' . $type . '_id, A.stat_id, A.pos, 1 AS pos_tied
    FROM SPORTS_FIA_' . $typeU . '_STATS AS A
    JOIN SPORTS_FIA_' . $typeU . '_STATS AS B
      ON (B.season = A.season
      AND B.series = A.series
      AND B.stat_id = A.stat_id
      AND B.pos = A.pos
      AND B.' . $type . '_id <> A.' . $type . '_id)
    WHERE A.season = "' . $season . '"
    GROUP BY A.season, A.series, A.' . $type . '_id, A.stat_id
  ON DUPLICATE KEY UPDATE pos_tied = VALUES(pos_tied);' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  print '# Tidy' . "\n"
  . 'ALTER TABLE SPORTS_FIA_' . $typeU . '_STATS ORDER BY season, series, ' . $type . '_id, stat_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  benchmark_stamp("Stats: $type");
}

# Return true to pacify the compiler
1;
