#!/usr/bin/perl -w
# Calculate the post-race standings

our $race;
foreach my $type ('driver', 'team') {
  my $mode_test = ($type eq 'driver' ? 8 : 9);
  my $typeU = uc $type;

  print "\n" . '# ' . ucfirst($type) . ' Standings' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print "\n" . '# Required calcs' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'SET @round_order := (SELECT round_order FROM SPORTS_FIA_RACES WHERE season = "' . $season . '" AND series = "f1" AND round = "' . $round . '");' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print "\n" . '# Point calculations' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a (
    season YEAR NOT NULL,
    series CHAR(5),
    ' . $type . '_id INT(11) UNSIGNED NOT NULL,
    pos TINYINT(1) UNSIGNED,
    pts FLOAT UNSIGNED,
    num_poles TINYINT(3) UNSIGNED,
    num_wins TINYINT(3) UNSIGNED,
    num_podiums TINYINT(3) UNSIGNED,
    num_fastest_laps TINYINT(3) UNSIGNED,
    PRIMARY KEY (season, ' . $type . '_id)
  ) ENGINE = MEMORY
    SELECT SPORTS_FIA_RACE_DRIVERS.season, SPORTS_FIA_RACE_DRIVERS.series, SPORTS_FIA_RACE_DRIVERS.' . $type . '_id,
           0 AS pos,
           SUM(IFNULL(SPORTS_FIA_RACE_RESULT.pts, 0)) AS pts,
           SUM(IF(SPORTS_FIA_RACE_GRID.race >= IF(FIND_IN_SET("SPRINT_RACE", SPORTS_FIA_RACES.quirks), 2, 1), IFNULL(SPORTS_FIA_RACE_GRID.grid_pos, 0), 0) = 1) AS num_poles,
           SUM(IF(SPORTS_FIA_RACE_RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", SPORTS_FIA_RACES.quirks), 2, 1), IFNULL(SPORTS_FIA_RACE_RESULT.pos, 0), 0) = 1) AS num_wins,
           SUM(IF(SPORTS_FIA_RACE_RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", SPORTS_FIA_RACES.quirks), 2, 1), IFNULL(SPORTS_FIA_RACE_RESULT.pos, 0), 0) IN (1, 2, 3)) AS num_podiums,
           SUM(SPORTS_FIA_RACE_FASTEST_LAP.race >= IF(FIND_IN_SET("SPRINT_RACE", SPORTS_FIA_RACES.quirks), 2, 1) AND SPORTS_FIA_RACE_FASTEST_LAP.car_no IS NOT NULL) AS num_fastest_laps
    FROM SPORTS_FIA_RACES
    JOIN SPORTS_FIA_RACE_DRIVERS
      ON (SPORTS_FIA_RACE_DRIVERS.season = SPORTS_FIA_RACES.season
      AND SPORTS_FIA_RACE_DRIVERS.series = SPORTS_FIA_RACES.series
      AND SPORTS_FIA_RACE_DRIVERS.round = SPORTS_FIA_RACES.round)
    LEFT JOIN SPORTS_FIA_RACE_GRID
      ON (SPORTS_FIA_RACE_GRID.season = SPORTS_FIA_RACE_DRIVERS.season
      AND SPORTS_FIA_RACE_GRID.series = SPORTS_FIA_RACE_DRIVERS.series
      AND SPORTS_FIA_RACE_GRID.round = SPORTS_FIA_RACE_DRIVERS.round
      AND SPORTS_FIA_RACE_GRID.car_no = SPORTS_FIA_RACE_DRIVERS.car_no)
    LEFT JOIN SPORTS_FIA_RACE_RESULT
      ON (SPORTS_FIA_RACE_RESULT.season = SPORTS_FIA_RACE_DRIVERS.season
      AND SPORTS_FIA_RACE_RESULT.series = SPORTS_FIA_RACE_DRIVERS.series
      AND SPORTS_FIA_RACE_RESULT.round = SPORTS_FIA_RACE_DRIVERS.round
      AND SPORTS_FIA_RACE_RESULT.race = SPORTS_FIA_RACE_GRID.race
      AND SPORTS_FIA_RACE_RESULT.car_no = SPORTS_FIA_RACE_DRIVERS.car_no)
    LEFT JOIN SPORTS_FIA_RACE_FASTEST_LAP
      ON (SPORTS_FIA_RACE_FASTEST_LAP.season = SPORTS_FIA_RACE_DRIVERS.season
      AND SPORTS_FIA_RACE_FASTEST_LAP.series = SPORTS_FIA_RACE_DRIVERS.series
      AND SPORTS_FIA_RACE_FASTEST_LAP.round = SPORTS_FIA_RACE_DRIVERS.round
      AND SPORTS_FIA_RACE_FASTEST_LAP.race = SPORTS_FIA_RACE_GRID.race
      AND SPORTS_FIA_RACE_FASTEST_LAP.car_no = SPORTS_FIA_RACE_DRIVERS.car_no)
    WHERE SPORTS_FIA_RACES.season = "' . $season . '"
    AND   SPORTS_FIA_RACES.series = "f1"
    AND   SPORTS_FIA_RACES.round_order <= @round_order
    GROUP BY SPORTS_FIA_RACE_DRIVERS.season,
             SPORTS_FIA_RACE_DRIVERS.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  # Any penalties?
  foreach my $id (keys %{$penalties{$type}}) {
    my %info = %{$penalties{$type}{$id}};
    print "\n" . '# Championship Penalty; ID: ' . $id . ' - ' . $info{'pts'} . 'pt(s) - ' . $info{'notes'} . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
    print 'UPDATE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a
SET pts = pts - ' . $info{'pts'} . '
WHERE season = "' . $season . '"
AND   series = "f1"
AND   ' . $type . '_id = "' . $id . '";' . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
  }

  # Initial position calcs
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b SELECT * FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY
    SELECT tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.season,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.series,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.' . $type . '_id,
           IF(tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b.' . $type . '_id IS NULL, 1, COUNT(tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b.' . $type . '_id) + 1) AS pos,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.pts,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.num_poles,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.num_wins,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.num_podiums,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.num_fastest_laps
    FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a
    LEFT JOIN tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b
      ON (tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b.season = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.season
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b.series = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.series
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1b.pts > tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.pts)
    GROUP BY tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.season,
             tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.series,
             tmp_SPORTS_FIA_' . $typeU . '_HISTORY_stage1a.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  # Identify Ties
  print "\n" . '# Identify ties' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORYa;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORYa LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORYa SELECT * FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'ALTER TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties ADD COLUMN is_tied TINYINT(1) UNSIGNED,
                                                       ADD COLUMN tied_pos TINYINT(1) UNSIGNED;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties (season, series, ' . $type . '_id, pos, pts, num_poles, num_wins, num_podiums, num_fastest_laps, is_tied, tied_pos)
    SELECT tmp_SPORTS_FIA_' . $typeU . '_HISTORY.season,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.series,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.' . $type . '_id,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.pos,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.pts,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.num_poles,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.num_wins,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.num_podiums,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.num_fastest_laps,
           1 AS is_tied,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY.pos AS tied_pos
    FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY
    JOIN tmp_SPORTS_FIA_' . $typeU . '_HISTORYa
      ON (tmp_SPORTS_FIA_' . $typeU . '_HISTORY.season = tmp_SPORTS_FIA_' . $typeU . '_HISTORYa.season
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY.series = tmp_SPORTS_FIA_' . $typeU . '_HISTORYa.series
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY.pos = tmp_SPORTS_FIA_' . $typeU . '_HISTORYa.pos
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY.' . $type . '_id <> tmp_SPORTS_FIA_' . $typeU . '_HISTORYa.' . $type . '_id)
    GROUP BY tmp_SPORTS_FIA_' . $typeU . '_HISTORY.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  # Get final position info
  print "\n" . '# Get tiebreaker info' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1 LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'ALTER TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1 ADD COLUMN best_pos TINYINT(1) UNSIGNED, ADD COLUMN first_best_pos TINYINT(1) UNSIGNED, ADD COLUMN num_pos' . join(' TINYINT(1) UNSIGNED, ADD COLUMN num_pos', 1 .. $max_pos) . ' TINYINT(1) UNSIGNED, ADD COLUMN best_ret TINYINT(1) UNSIGNED, ADD COLUMN first_best_ret TINYINT(1) UNSIGNED;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1
    SELECT tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties.*,
           MIN(IFNULL(IF(CAST(SPORTS_FIA_RACE_RESULT.pos AS CHAR) = SPORTS_FIA_RACE_RESULT.result, SPORTS_FIA_RACE_RESULT.pos, NULL), 100)) AS best_pos,
           100 AS first_best_pos,' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  for (my $i = 1; $i <= $max_pos; $i++) {
    print '           SUM(IF(IFNULL(IF(CAST(SPORTS_FIA_RACE_RESULT.pos AS CHAR) = SPORTS_FIA_RACE_RESULT.result, SPORTS_FIA_RACE_RESULT.pos, NULL), 0) = ' . $i . ', 1, 0)) AS num_pos' . $i . ',' . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
  }
  print '           MIN(IFNULL(IF(CAST(SPORTS_FIA_RACE_RESULT.pos AS CHAR) <> SPORTS_FIA_RACE_RESULT.result, SPORTS_FIA_RACE_RESULT.pos, NULL), 100)) AS best_ret,
           100 AS first_best_ret
    FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties
    LEFT JOIN SPORTS_FIA_RACE_DRIVERS
      ON (SPORTS_FIA_RACE_DRIVERS.season = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties.season
      AND SPORTS_FIA_RACE_DRIVERS.series = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties.series
      AND SPORTS_FIA_RACE_DRIVERS.' . $type . '_id = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties.' . $type . '_id)
    LEFT JOIN SPORTS_FIA_RACES
      ON (SPORTS_FIA_RACES.season = SPORTS_FIA_RACE_DRIVERS.season
      AND SPORTS_FIA_RACES.series = SPORTS_FIA_RACE_DRIVERS.series
      AND SPORTS_FIA_RACES.round = SPORTS_FIA_RACE_DRIVERS.round)
    LEFT JOIN SPORTS_FIA_RACE_RESULT
      ON (SPORTS_FIA_RACE_RESULT.season = SPORTS_FIA_RACE_DRIVERS.season
      AND SPORTS_FIA_RACE_RESULT.series = SPORTS_FIA_RACE_DRIVERS.series
      AND SPORTS_FIA_RACE_RESULT.round = SPORTS_FIA_RACE_DRIVERS.round
      AND SPORTS_FIA_RACE_RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", SPORTS_FIA_RACES.quirks), 2, 1)
      AND SPORTS_FIA_RACE_RESULT.car_no = SPORTS_FIA_RACE_DRIVERS.car_no)
    GROUP BY tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties.season,
             tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties.series,
             tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties.' . $type . '_id;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  # Find the first round the best position happened
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a SELECT * FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1 (season, series, ' . $type . '_id, first_best_pos)
  SELECT tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.season,
         tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.series,
         tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.' . $type . '_id,
         IFNULL(MIN(SPORTS_FIA_RACES.round_order), 100) AS first_best_pos
  FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a
  LEFT JOIN SPORTS_FIA_RACE_DRIVERS
    ON (SPORTS_FIA_RACE_DRIVERS.season = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.season
    AND SPORTS_FIA_RACE_DRIVERS.series = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.series
    AND SPORTS_FIA_RACE_DRIVERS.' . $type . '_id = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.' . $type . '_id)
  LEFT JOIN SPORTS_FIA_RACES
    ON (SPORTS_FIA_RACES.season = SPORTS_FIA_RACE_DRIVERS.season
    AND SPORTS_FIA_RACES.series = SPORTS_FIA_RACE_DRIVERS.series
    AND SPORTS_FIA_RACES.round = SPORTS_FIA_RACE_DRIVERS.round)
  LEFT JOIN SPORTS_FIA_RACE_RESULT
    ON (SPORTS_FIA_RACE_RESULT.season = SPORTS_FIA_RACE_DRIVERS.season
    AND SPORTS_FIA_RACE_RESULT.series = SPORTS_FIA_RACE_DRIVERS.series
    AND SPORTS_FIA_RACE_RESULT.round = SPORTS_FIA_RACE_DRIVERS.round
    AND SPORTS_FIA_RACE_RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", SPORTS_FIA_RACES.quirks), 2, 1)
    AND SPORTS_FIA_RACE_RESULT.car_no = SPORTS_FIA_RACE_DRIVERS.car_no
    AND CAST(SPORTS_FIA_RACE_RESULT.pos AS CHAR) = SPORTS_FIA_RACE_RESULT.result
    AND SPORTS_FIA_RACE_RESULT.pos = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.best_pos)
  GROUP BY tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.season,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.series,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.' . $type . '_id
ON DUPLICATE KEY UPDATE first_best_pos = VALUES(first_best_pos);' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  # Find the first round the best retired position happened
  print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a SELECT * FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1 (season, series, ' . $type . '_id, first_best_ret)
  SELECT tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.season,
         tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.series,
         tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.' . $type . '_id,
         IFNULL(MIN(SPORTS_FIA_RACES.round_order), 100) AS first_best_ret
  FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a
  LEFT JOIN SPORTS_FIA_RACE_DRIVERS
    ON (SPORTS_FIA_RACE_DRIVERS.season = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.season
    AND SPORTS_FIA_RACE_DRIVERS.series = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.series
    AND SPORTS_FIA_RACE_DRIVERS.' . $type . '_id = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.' . $type . '_id)
  LEFT JOIN SPORTS_FIA_RACES
    ON (SPORTS_FIA_RACES.season = SPORTS_FIA_RACE_DRIVERS.season
    AND SPORTS_FIA_RACES.series = SPORTS_FIA_RACE_DRIVERS.series
    AND SPORTS_FIA_RACES.round = SPORTS_FIA_RACE_DRIVERS.round)
  LEFT JOIN SPORTS_FIA_RACE_RESULT
    ON (SPORTS_FIA_RACE_RESULT.season = SPORTS_FIA_RACE_DRIVERS.season
    AND SPORTS_FIA_RACE_RESULT.series = SPORTS_FIA_RACE_DRIVERS.series
    AND SPORTS_FIA_RACE_RESULT.round = SPORTS_FIA_RACE_DRIVERS.round
    AND SPORTS_FIA_RACE_RESULT.race >= IF(FIND_IN_SET("SPRINT_RACE", SPORTS_FIA_RACES.quirks), 2, 1)
    AND SPORTS_FIA_RACE_RESULT.car_no = SPORTS_FIA_RACE_DRIVERS.car_no
    AND CAST(SPORTS_FIA_RACE_RESULT.pos AS CHAR) <> SPORTS_FIA_RACE_RESULT.result
    AND SPORTS_FIA_RACE_RESULT.pos = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.best_ret)
  GROUP BY tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.season,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.series,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1a.' . $type . '_id
ON DUPLICATE KEY UPDATE first_best_ret = VALUES(first_best_ret);' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  # Now break them
  my $created = 0;
  foreach my $break ( 'best_pos', 1 .. $max_pos, 'first_best_pos', 'best_ret', 'first_best_ret' ) {
    $break = 'num_pos' . $break
      if $break =~ /^\d+$/;
    $created = 1
      if $break =~ /^num/;

    print "\n" . '# Break tie on ' . $break . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;

    foreach my $arg ( 'a', 'b' ) {
      print 'DROP TEMPORARY TABLE IF EXISTS tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2' . $arg . ';' . "\n"
        if ($print_mode == -1 || $print_mode == $mode_test) && !$created;
      print 'CREATE TEMPORARY TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2' . $arg . ' LIKE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1;' . "\n"
        if ($print_mode == -1 || $print_mode == $mode_test) && !$created;
      print 'TRUNCATE TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2' . $arg . ';' . "\n"
        if ($print_mode == -1 || $print_mode == $mode_test) && $created;
      print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2' . $arg . ' SELECT * FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1;' . "\n"
        if $print_mode == -1 || $print_mode == $mode_test;
    }

    print 'INSERT INTO tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1 (season, series, ' . $type . '_id, pos, is_tied)
    SELECT tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.season,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.series,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.' . $type . '_id,
           tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.pos
             + COUNT(IF(tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.' . $break . ' ' . ($break =~ /^num/ ? '<' : '>') . ' tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.' . $break . ',
                        tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.' . $type . '_id, NULL)) AS pos,
           COUNT(DISTINCT IF(tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.' . $break . ' = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.' . $break . ',
                             1, NULL)) AS is_tied
    FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a
    LEFT JOIN tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b
      ON (tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.season = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.season
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.series = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.series
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.' . $type . '_id <> tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.' . $type . '_id
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.pos = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.pos
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.is_tied = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.is_tied
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.' . $break . ' ' . ($break =~ /^num/ ? '<' : '>') . '= tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2b.' . $break . ')
    WHERE tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.is_tied = 1
    GROUP BY tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.season,
             tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.series,
             tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage2a.' . $type . '_id
ON DUPLICATE KEY UPDATE pos = VALUES(pos), is_tied = VALUES(is_tied);' . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
  }

  print "\n" . '# Update the broken ties' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'UPDATE tmp_SPORTS_FIA_' . $typeU . '_HISTORY
    JOIN tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1
      ON (tmp_SPORTS_FIA_' . $typeU . '_HISTORY.season = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1.season
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY.series = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1.series
      AND tmp_SPORTS_FIA_' . $typeU . '_HISTORY.' . $type . '_id = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1.' . $type . '_id)
    SET tmp_SPORTS_FIA_' . $typeU . '_HISTORY.pos = tmp_SPORTS_FIA_' . $typeU . '_HISTORY_ties_stage1.pos;' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  print "\n" . 'ALTER TABLE tmp_SPORTS_FIA_' . $typeU . '_HISTORY ADD COLUMN notes VARCHAR(255);' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  foreach my $id (keys %{$penalties{$type}}) {
    my %info = %{$penalties{$type}{$id}};
    $info{'notes'} =~ s/\s+$//g;
    print "\n" . '# Championship Penalty; ID: ' . $id . ' - ' . $info{'pts'} . 'pt(s) - ' . $info{'notes'} . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
    print 'UPDATE tmp_SPORTS_FIA_' . $typeU . '_HISTORY
SET notes = "' . $info{'notes'} . '"
WHERE season = "' . $season . '"
AND   series = "f1"
AND   ' . $type . '_id = "' . $id . '";' . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
  }

  #
  my $sql_notes = '';
  my $sql_notes_dku = '';
  if ($type eq 'team') {
    $sql_notes = ', notes';
    $sql_notes_dku = ', notes = VALUES(notes)'
  }

  print "\n" . '# Finally store' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO SPORTS_FIA_' . $typeU . '_HISTORY (season, series, ' . $type . '_id, pos, pts, num_poles, num_wins, num_podiums, num_fastest_laps' . $sql_notes . ')
    SELECT season, series, ' . $type . '_id, pos, pts, num_poles, num_wins, num_podiums, num_fastest_laps' . $sql_notes . '
    FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY
ON DUPLICATE KEY UPDATE pos = VALUES(pos),
                        pts = VALUES(pts),
                        num_poles = VALUES(num_poles),
                        num_wins = VALUES(num_wins),
                        num_podiums = VALUES(num_podiums),
                        num_fastest_laps = VALUES(num_fastest_laps)' . $sql_notes_dku . ';' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;

  print "\n" . '# And store again in our progress table' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'INSERT INTO SPORTS_FIA_' . $typeU . '_HISTORY_PROGRESS (season, series, ' . $type . '_id, round, race, pos, pts' . $sql_notes . ')
    SELECT season, series, ' . $type . '_id, "' . $round . '", "' . $race . '", pos, pts' . $sql_notes . '
    FROM tmp_SPORTS_FIA_' . $typeU . '_HISTORY
ON DUPLICATE KEY UPDATE pos = VALUES(pos), pts = VALUES(pts)' . $sql_notes_dku . ';' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  benchmark_stamp("Standings: $type");
}

# Return true to pacify the compiler
1;
