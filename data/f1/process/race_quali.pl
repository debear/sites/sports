#!/usr/bin/perl -w
# Process the qualifying session

#
# 2-part qualifying session?
#
our $season;
our $race;
our $two_part_quali;

#
# Extract tables
#
my $pattern;
my $pattern_src = $results;

# Qualifying - Standard 3 part session
($pattern_src) = ($results =~ m/<h3[^\n]*?>Qualifying.*?<\/h3>\s*(<table class="wikitable sortable".*?<\/table>)/gsi)
  if $season >= 2019;
$pattern = '<table[^>]*>\s*<tr[^>]*>\s*<th[^>]*>.*?Pos.*?</th>\s*<th[^>]*>.*?No.*?<\/th>\s*<th[^>]*>.*?(?:Driver|Name).*?<\/th>\s*<th[^>]*>.*?Constructor.*?<\/th>\s*<th[^>]*>.*?(?:Part |Q)1.*?<\/th>\s*<th[^>]*>.*?(?:Part |Q)2.*?<\/th>\s*<th[^>]*>.*?(?:Part |Q)3.*?<\/th>\s*<th[^>]*>.*?Grid.*?<\/th>\s*<\/tr>(.*?)<\/table>'
  if $season <= 2016;
$pattern = '<table[^>]*>(?:\s*<tbody>)?\s*<tr[^>]*>\s*<th[^>]*>.*?Pos.*?<\/th>\s*<th[^>]*>.*?No.*?<\/th>\s*<th[^>]*>.*?(?:Driver|Name).*?<\/th>\s*<th[^>]*>.*?Constructor.*?<\/th>\s*<th[^>]*>.*?Times.*?<\/th>\s*<th[^>]*>.*?Grid.*?<\/th>\s*<\/tr>\s*<tr>\s*<th[^>]*>.*?(?:Part |Q)1.*?<\/th>\s*<th[^>]*>.*?(?:Part |Q)2.*?<\/th>\s*<th[^>]*>.*?(?:Part |Q)3.*?<\/th>\s*<\/tr>(.*?)<\/table>'
  if $season >= 2017;
# Down to two parts?
$pattern = '<table[^>]*>\s*<tr[^>]*>\s*<th[^>]*>.*?Pos.*?</th>\s*<th[^>]*>.*?No.*?<\/th>\s*<th[^>]*>.*?(?:Driver|Name).*?<\/th>\s*<th[^>]*>.*?Constructor.*?<\/th>\s*<th[^>]*>.*?(?:Part |Q)1.*?<\/th>\s*<th[^>]*>.*?(?:Part |Q)2.*?<\/th>\s*<th[^>]*>.*?Grid.*?<\/th>\s*<\/tr>(.*?)<\/table>'
  if $two_part_quali;
my ($qual_table) = ($pattern_src =~ /$pattern/gsi);
benchmark_stamp('Get Qualification Table');

#
# Qualifying results
#
parse_qualifying($qual_table, 'q', ++$race, 1);

# Return true to pacify the compiler
1;
