#!/usr/bin/perl -w
# Process a race session

our $season;
our $round;

# Validate the configured drivers match those in the results
if ($print_mode == -1 || $print_mode == 7) {
  print "\n" . '## Validating results-to-driver setup' . "\n";
  # Returns a "comment" when mapped successfully, error message when not
  print "SELECT IF(RACE_DRIVER.driver_id IS NOT NULL,
  CONCAT('# Car #', RACE_RESULT.car_no, ' mapped to ', RACE_DRIVER.driver_code, ' (ID ', RACE_DRIVER.driver_id, ')'),
  CONCAT('Unable to map Car #', RACE_RESULT.car_no, ' to a driver')) AS mapping_result
FROM SPORTS_FIA_RACE_RESULT AS RACE_RESULT
LEFT JOIN SPORTS_FIA_RACE_DRIVERS AS RACE_DRIVER
  ON (RACE_DRIVER.season = RACE_RESULT.season
  AND RACE_DRIVER.series = RACE_RESULT.series
  AND RACE_DRIVER.round = RACE_RESULT.round
  AND RACE_DRIVER.car_no = RACE_RESULT.car_no)
WHERE RACE_RESULT.season = '$season'
AND   RACE_RESULT.series = 'f1'
AND   RACE_RESULT.round = '$round'
GROUP BY RACE_RESULT.car_no, RACE_DRIVER.driver_id
ORDER BY RACE_RESULT.car_no;\n";
}

# Return true to pacify the compiler
1;
