#!/usr/bin/perl -w
# Process a sprint shootout session

our $results;
our $race;
my $shootout_table;

# Qualifying - Standard 3 part session
my ($pattern_src) = ($results =~ m/<h3[^>]*?>[^\n]*?Sprint (?:shootout|qualifying)(?: classification)?[^\n]*?<\/h3>\s*(<table class="wikitable sortable".*?<\/table>)/gsi);
if (defined($pattern_src)) {
  my $pattern = '<table[^>]*>(?:\s*<tbody>)?\s*<tr[^>]*>\s*<th[^>]*>.*?Pos.*?<\/th>\s*<th[^>]*>.*?No.*?<\/th>\s*<th[^>]*>.*?(?:Driver|Name).*?<\/th>\s*<th[^>]*>.*?Constructor.*?<\/th>\s*<th[^>]*>.*?Times.*?<\/th>\s*<th[^>]*>.*?Grid.*?<\/th>\s*<\/tr>\s*<tr>\s*<th[^>]*>.*?SQ1.*?<\/th>\s*<th[^>]*>.*?SQ2.*?<\/th>\s*<th[^>]*>.*?SQ3.*?<\/th>\s*<\/tr>(.*?)<\/table>';
  ($shootout_table) = ($pattern_src =~ /$pattern/gsi);
  benchmark_stamp('Get Sprint Shootout Table');
}
if (defined($shootout_table)) {
  print "\n" . '## Sprint Shootout Detected' . "\n"
    if $print_mode == -1 || $print_mode == 3;
  parse_qualifying($shootout_table, 'sq', ++$race, 3);
}

# Return true to pacify the compiler
1;
