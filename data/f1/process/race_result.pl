#!/usr/bin/perl -w
# Process a race session

our $results;
our $season;
$pattern_src = $results;
$pattern_src =~ s/<span class="mw-editsection"><span class="mw-editsection-bracket">.*?<\/span><\/span>//gsi;
($pattern_src) = ($results =~ m/<h3[^\n]*?>Race[^<]*?<\/h3>\s*(<table class="wikitable sortable".*?<\/table>)/gsi)
  if $season >= 2024;
($pattern_src) = ($pattern_src =~ m/<h3[^>]*?><span[^>]*?>Race[^<]*?<\/span><\/h3>\s*(<table class="wikitable.*?<\/table>)/gsi)
  if $season >= 2019 && !defined($pattern_src);
$pattern = '<table[^>]*>(?:\s*<tbody>)?\s*<tr[^>]*>\s*<th[^>]*>.*?Pos.*?<\/th>\s*<th[^>]*>.*?No.*?<\/th>\s*<th[^>]*>.*?(?:Driver|Name).*?<\/th>\s*<th[^>]*>.*?Constructor.*?<\/th>\s*<th[^>]*>.*?Laps?.*?<\/th>\s*<th[^>]*>.*?Time\/Retired.*?<\/th>\s*<th[^>]*>.*?Grid.*?<\/th>\s*<th[^>]*>.*?Points.*?<\/th>\s*<\/tr>(.*?)<\/table>';
my ($race_table) = ($pattern_src =~ /$pattern/gsi);
benchmark_stamp('Get Race Table');

print "\n" . '## Main Grand Prix Processing' . "\n"
  if ($race > 1) && ($print_mode == -1 || $print_mode == 5); # Non-zero implies we are processing _after_ a Sprint Race
parse_race($race_table, $race, 5);

# Return true to pacify the compiler
1;
