#!/usr/bin/perl -w
# Process a qualifying sprint session

our $results;
our $race;
$pattern_src = $results;
$pattern_src =~ s/<span class="mw-editsection"><span class="mw-editsection-bracket">.*?<\/span><\/span>//gsi;
($pattern_src) = ($pattern_src =~ m/<h3><span[^>]*?>Sprint qualifying[^<]*?<\/span><\/h3>\s*(<table class="wikitable.*?<\/table>)/gsi)
  if $season >= 2019 && $season < 2022;
($pattern_src) = ($pattern_src =~ m/<h3[^>]*?>(?:<span[^>]*?>)?Sprint classification[^<]*?(?:<\/span>)?<\/h3>\s*(<table class="wikitable.*?<\/table>)/gsi)
  if $season >= 2022;
my $sprint_race_table;
if (defined($pattern_src)) {
  # First pattern
  $pattern = '<table[^>]*>(?:\s*<tbody>)?\s*<tr[^>]*>\s*<th[^>]*>.*?Pos.*?<\/th>\s*<th[^>]*>.*?No.*?<\/th>\s*<th[^>]*>.*?(?:Driver|Name).*?<\/th>\s*<th[^>]*>.*?Constructor.*?<\/th>\s*<th[^>]*>.*?Laps?.*?<\/th>\s*<th[^>]*>.*?Time\/Retired.*?<\/th>\s*<th[^>]*>.*?Grid.*?<\/th>\s*<th[^>]*>.*?Points.*?<\/th>\s*<th[^>]*>.*?Final.*?grid.*?<\/th>\s*<\/tr>(.*?)<\/table>'
    if $season < 2023;
  $pattern = '<table[^>]*>(?:\s*<tbody>)?\s*<tr[^>]*>\s*<th[^>]*>.*?Pos.*?<\/th>\s*<th[^>]*>.*?No.*?<\/th>\s*<th[^>]*>.*?(?:Driver|Name).*?<\/th>\s*<th[^>]*>.*?Constructor.*?<\/th>\s*<th[^>]*>.*?Laps?.*?<\/th>\s*<th[^>]*>.*?Time\/Retired.*?<\/th>\s*<th[^>]*>.*?Grid.*?<\/th>\s*<th[^>]*>.*?Points.*?<\/th>\s*<\/tr>(.*?)<\/table>'
    if $season >= 2023;
  ($sprint_race_table) = ($pattern_src =~ /$pattern/gsi);
  benchmark_stamp('Get Sprint Race Table');
}
if (defined($sprint_race_table)) {
  print "\n" . '## Sprint Qualifying / Sprint Race Detected' . "\n"
    if $print_mode == -1 || $print_mode == 3;
  # Strip out the "Final Grid" column for the standard race result parser
  my $sprint_race = $sprint_race_table;
  $sprint_race =~ s/(<tr class="sortbottom">.*?)(<\/tr>)/$1<td class="faux-final-grid"><\/td>$2/gsi;
  $sprint_race =~ s/<td[^>]*>\s*.*?\s*<\/td>\s*(<\/tr>)/$1/g
    if $season < 2023;
  parse_race($sprint_race, $race, 3);

  # Calculate the resulting Grand Prix grid (in seasons prior to the Sprint Shootout)
  if ($season < 2023) {
    my @grid = ( );
    my $last_qual = -1;
    $pattern = '<tr[^>]*>\s*<th[^>]*>.*?<\/th>\s*<td[^>]*>\s*(\d+)\s*<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>\s*(.*?)\s*<\/td>\s*<\/tr>';
    $sprint_race_table =~ s/<sup[^>]*>.*?<\/sup>//g;
    my @gp_grid = ($sprint_race_table =~ /$pattern/gsi);
    while (my ($car_no, $grid_pos) = splice(@gp_grid, 0, 2)) {
      push @grid, { 'car_no' => $car_no, 'pos' => $grid_pos }
        if !grep(/^$grid_pos$/, ( '', 'DNS', 'DNQ', '—' ))
            && ord($grid_pos) != 226; # chr(226) = —
      $last_qual = $grid_pos if $grid_pos =~ /^\d+$/ && $grid_pos > $last_qual;
    }
    # Tidy up any pit lane starters
    foreach my $driver (@grid) {
      my $grid_pos = $$driver{'pos'};
      $grid_pos =~ s/<[^>]+>//g;
      $$driver{'pos'} = ++$last_qual if $grid_pos =~ /^PL$|^Pit$/;
    }
    # Sort
    my @grid_sort = sort { $$a{'pos'} <=> $$b{'pos'} } @grid;
    benchmark_stamp('Parse Grand Prix Grid');

    # Grand Prix race grid
    print "\n" . '# Generating Grand Prix Grid' . "\n"
      if $print_mode == -1 || $print_mode == 3;
    print 'DELETE FROM SPORTS_FIA_RACE_GRID WHERE season = "' . $season . '" AND series = "f1" AND round = "' . $round . '" AND race = "2";' . "\n"
      if $print_mode == -1 || $print_mode == 3;
    foreach my $driver (@grid_sort) {
      print 'INSERT INTO SPORTS_FIA_RACE_GRID (season, series, round, race, grid_pos, car_no, notes) VALUES ("' . $season . '", "f1", "' . $round . '", "2", "' . $$driver{'pos'} . '", "' . $$driver{'car_no'} . '", NULL);' . "\n"
        if $print_mode == -1 || $print_mode == 3;
    }
    benchmark_stamp('Generate Grand Prix Grid');
  }
}

# Return true to pacify the compiler
1;
