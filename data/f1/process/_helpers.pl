#!/usr/bin/perl -w
# Helper methods for parsing the Formula One results

#
# Process the qualifying results
#
sub parse_qualifying {
  my ($qual_table, $prefix, $race_num, $grid_test) = @_;
  my $session_test = $grid_test + 1;
  my $session_label = ($prefix eq 'q' ? 'Qualifying' : 'Sprint Shootout');
  my $validation_prefix = ($prefix eq 'q' ? 'quali' : 'sprint');

  # Variables
  my @grid = ( );
  my %qual = ( );
  our @dnq = ( );
  my $last_qual = -1;
  our %validation;

  # Replace some common annoyances
  $qual_table =~ s/<b>(.*?)<\/b>/$1/g;
  $qual_table =~ s/<sup[^>]*>.*?<\/sup>//g;
  $qual_table =~ s/>\s+/>/g;
  $qual_table =~ s/\s+</</g;
  $qual_table =~ s/\N{U+2021}//g;
  $qual_table =~ s/<span style="display:none;?">\d+<\/span>//g;

  # Parse
  $pattern = '<tr[^>]*>\s*<th[^>]*>(.*?)<\/th>\s*<td[^>]*>(\d+)<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>\(?(.*?)\)?<\/td>\s*<\/tr>';
  $pattern = '<tr[^>]*>\s*<th[^>]*>(.*?)<\/th>\s*<td[^>]*>(\d+)<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>\(?(.*?)\)?<\/td>\s*<\/tr>' if $two_part_quali;
  $pattern =~ s/\(\\d\+\)/\(\\d\+\)<\\\/td>\\s\*<td\[\^>\]\*>\.\*\?/gsi
    if $qual_table =~ m/<span style="[^"]*visibility:hidden;[^"]*">Nat\.<\/span>/gsi;

  my @matches = ($qual_table =~ /$pattern/gsi);
  benchmark_stamp("Parse $session_label Table");
  my $last_pos = 0;
  my $splice = ($two_part_quali ? 5 : 6);
  while (my @driver = splice(@matches, 0, $splice)) {
    # Adding an extra value if two part quali?
    splice(@driver, 4, 0, '') if $two_part_quali;

    # If no position set explicitly, work it out from the last row
    $driver[0] = $last_pos+1 if $driver[0] !~ /^\d+$/;
    $last_pos = $driver[0];

    # Grid Pos
    push @grid, { 'car_no' => $driver[1], 'pos' => $driver[5] }
      if !grep(/^$driver[5]$/, ( '', 'DNS', 'DNQ', '—' ))
          && ord($driver[5]) != 226; # chr(226) = —
    push @dnq, $driver[1]
      if ($driver[5] eq 'DNQ');
    $last_qual = $driver[5] if $driver[5] =~ /^\d+$/ && $driver[5] > $last_qual;

    # Qual Sessions
    for (my $i = 1; $i < ($num_quali_sessions+1); $i++) {
      # Strip any HTML and specific "not relevant" tags
      $driver[$i+1] =~ s/<[^>]+>//g;
      $driver[$i+1] =~ s/n\/a//i;
      next if ($driver[$i+1] =~ /^\s*$/);

      # Convert time
      $driver[$i+1] = undef
        if ($driver[$i+1] !~ /\d[\:;\.]\d{2}[\.\:]\d{3}/);
      if (defined($driver[$i+1])) {
        my @time = ($driver[$i+1] =~ /(\d)[\:;\.](\d{2})[\.\:](\d{3})/);
        $driver[$i+1] = (60000 * $time[0]) + (1000 * $time[1]) + $time[2];
      }

      # Pop onto the list
      push @{$qual{$i}}, { 'car_no' => $driver[1], 'time' => $driver[$i+1] };
    }
  }
  benchmark_stamp("Process $session_label Table");

  # Tidy up any pit lane starters
  foreach my $driver (@grid) {
    my $grid_pos = $$driver{'pos'};
    $grid_pos =~ s/<[^>]+>//g;
    $$driver{'pos'} = ++$last_qual if $grid_pos =~ /^PL$|^Pit$/;
  }

  # Sort the results
  my @grid_sort = sort { $$a{'pos'} <=> $$b{'pos'} } @grid;
  my %qual_sort = ( );
  for (my $i = 1; $i < ($num_quali_sessions+1); $i++) {
    @{$qual_sort{$i}} = sort { (defined($$a{'time'}) ? $$a{'time'} : 180000) <=> (defined($$b{'time'}) ? $$b{'time'} : 180000) } @{$qual{$i}};
  }

  # Produce the Grid
  print "\n" . '# Generating Grid' . "\n"
    if $print_mode == -1 || $print_mode == $grid_test;
  print 'DELETE FROM SPORTS_FIA_RACE_GRID WHERE season = "' . $season . '" AND series = "f1" AND round = "' . $round . '" AND race = "' . $race_num . '";' . "\n"
    if $print_mode == -1 || $print_mode == $grid_test;
  foreach my $driver (@grid_sort) {
    print 'INSERT INTO SPORTS_FIA_RACE_GRID (season, series, round, race, grid_pos, car_no, notes) VALUES ("' . $season . '", "f1", "' . $round . '", "' . $race_num . '", "' . $$driver{'pos'} . '", "' . $$driver{'car_no'} . '", NULL);' . "\n"
      if $print_mode == -1 || $print_mode == $grid_test;
    $validation{"$validation_prefix-grid"} = @grid_sort;
  }
  benchmark_stamp("Generate $session_label Grid");

  # Produce the Qualifying Results
  for (my $i = 1; $i < ($num_quali_sessions+1); $i++) {
    my $leader = ''; my $pos = 1;
    print "\n" . '# Generating ' . $session_label . ' ' . $i . ' Results' . "\n"
      if $print_mode == -1 || $print_mode == $session_test;
    print 'DELETE FROM SPORTS_FIA_RACE_QUALI WHERE season = "' . $season . '" AND round = "' . $round . '" AND series = "f1" AND session = "' . $prefix . $i . '";' . "\n"
      if $print_mode == -1 || $print_mode == $session_test;
    foreach my $driver (@{$qual_sort{$i}}) {
      $leader = $$driver{'time'} if ($leader eq '');
      print 'INSERT INTO SPORTS_FIA_RACE_QUALI (season, series, round, session, pos, car_no, q_time, q_time_ms, gap_time, gap_time_ms) VALUES ("' . $season . '", "f1", "' . $round . '", "' . $prefix . $i . '", "' . $pos . '", "' . $$driver{'car_no'} . '", ' . (defined($$driver{'time'}) ? sprintf('"00:%02d:%02d", "%d"', floor($$driver{'time'} / 60000), floor($$driver{'time'} / 1000) % 60, $$driver{'time'} % 1000) : 'NULL, NULL') . ', ' . ($pos > 1 && defined($$driver{'time'}) ? sprintf('"00:%02d:%02d", "%d"', floor(($$driver{'time'} - $leader) / 60000), floor(($$driver{'time'} - $leader) / 1000) % 60, ($$driver{'time'} - $leader) % 1000) : 'NULL, NULL') . ');' . "\n"
        if $print_mode == -1 || $print_mode == $session_test;
      $pos++;
    }
    $validation{"$validation_prefix-$prefix$i"} = @{$qual_sort{$i}};
  }

  # Link the qual_pos into the _GRID table
  print "\n" . '# Storing final ' . $session_label . ' position' . "\n"
    if $print_mode == -1 || $print_mode == $session_test;
  print 'UPDATE SPORTS_FIA_RACE_GRID
  JOIN SPORTS_FIA_RACE_QUALI
    ON (SPORTS_FIA_RACE_QUALI.season = SPORTS_FIA_RACE_GRID.season
    AND SPORTS_FIA_RACE_QUALI.series = SPORTS_FIA_RACE_GRID.series
    AND SPORTS_FIA_RACE_QUALI.round = SPORTS_FIA_RACE_GRID.round
    AND SPORTS_FIA_RACE_QUALI.session = (SELECT CONCAT("' . $prefix . '", CAST(REPLACE(MAX(A.session), "' . $prefix . '", "") AS UNSIGNED))
                                        FROM SPORTS_FIA_RACE_QUALI AS A
                                        WHERE A.season = SPORTS_FIA_RACE_QUALI.season
                                        AND   A.series = SPORTS_FIA_RACE_QUALI.series
                                        AND   A.round = SPORTS_FIA_RACE_QUALI.round
                                        AND   A.session LIKE "' . $prefix . '%"
                                        AND   A.car_no = SPORTS_FIA_RACE_QUALI.car_no)
    AND SPORTS_FIA_RACE_QUALI.car_no = SPORTS_FIA_RACE_GRID.car_no)
  SET SPORTS_FIA_RACE_GRID.qual_pos = SPORTS_FIA_RACE_QUALI.pos
  WHERE SPORTS_FIA_RACE_GRID.season = "' . $season . '"
  AND   SPORTS_FIA_RACE_GRID.series = "f1"
  AND   SPORTS_FIA_RACE_GRID.round = "' . $round . '"
  AND   SPORTS_FIA_RACE_GRID.race = "' . $race_num . '";' . "\n"
    if $print_mode == -1 || $print_mode == $session_test;
  benchmark_stamp("Generate $session_label SQL");
}

#
# Process the race results
#
sub parse_race {
  my ($race_table, $race, $mode_test) = @_;
  our %validation;
  our %race_info;
  my $validation_prefix = (!$race_info{'race2_laps_total'} || $race == 2 ? 'race' : 'sprint');

  # Replace some common annoyances
  $race_table =~ s/<b>(.*?)<\/b>/$1/g;
  $race_table =~ s/<sup[^>]*>.*?<\/sup>//g;
  $race_table =~ s/>\s+/>/g;
  $race_table =~ s/\s+</</g;
  $race_table =~ s/\N{U+2021}//g;
  $race_table =~ s/<span style="display:none;?">\d+<\/span>//g;
  $race_table =~ s/<span data-sort-value="[^"]+"><span class="vcard"><span class="fn">(<a)/$1/g;
  $race_table =~ s/<span class="mw-image-border"[^>]*?>.*?<\/span>//g;
  $race_table =~ s/<span class="flagicon">.*?<\/span>//g;
  $race_table =~ s/<span class="nowrap">(.*?)<\/span>/$1/g;
  $race_table =~ s/<abbr.*?>(.*?)<\/abbr>/$1/g;
  $race_table =~ s/&#160;//g;

  $pattern = '<tr>\s*<t[hd][^>]*>\s*([^<\s]+).*?<\/t[hd]>\s*<td[^>]*>\s*(.*?)\s*<\/td>\s*<td[^>]*>\s*(?:<span[^>]*?>\s*)?<a[^>]+>(.*?)<\/a>.*?\s*<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>\s*(.*?)\s*<\/td>\s*<td[^>]*>\s*([^<]*).*?<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>\s*(.*?)\s*<\/td>\s*<\/tr>';
  $pattern =~ s/<\\\/span>/<\\\/span><\\\/td>\\s\*<td\[\^>\]\*>/gsi
    if $race_table =~ m/<span style="[^"]*visibility:hidden;[^"]*">Nat\.<\/span>/gsi;
  @matches = ($race_table =~ /$pattern/gsi) if defined($race_table);
  benchmark_stamp('Parse Race Table');

  print "\n" . '# Generating Race ' . $race . ' Results' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  print 'DELETE FROM SPORTS_FIA_RACE_RESULT WHERE season = "' . $season . '" AND series = "f1" AND round = "' . $round . '" AND race = "' . $race . '";' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  my %drivers = ( ); my $max_laps = 0;
  my $leader = ''; my $pos = 1;
  while (my @driver = splice(@matches, 0, 6)) {
    $drivers{$driver[2]} = $driver[1];
    $max_laps = $driver[3] if $driver[3] =~ /^\d+$/ && $max_laps < $driver[3];

    # Leading time?
    $leader = parse_race_time($driver[4]) if ($leader eq '');

    # Relative time?
    $driver[6] = ''; # Gap time
    $driver[7] = ''; # Notes
    if ($pos > 1) {
      if ($driver[4] =~ /^\+(\d+\:)?(\d+)\.(\d+)/) {
        my @time = ($driver[4] =~ /^\+(\d+\:)?(\d+)\.(\d+)/);
        $time[0] = (defined($time[0]) ? $time[0] : 0);
        ($time[0]) = ($time[0] =~ /^(\d+)\:/);
        $time[0] = (defined($time[0]) ? $time[0] : 0);
        $driver[6] = (60000 * $time[0]) + (1000 * $time[1]) + $time[2];
        $driver[4] = $leader + $driver[6];

      } else {
        $driver[7] = $driver[4];
        $driver[4] = '';
      }
    } else {
      $driver[4] = $leader;
    }

    # Points
    $driver[5] =~ s/<[^>]+>//g;

    # SQL
    print 'INSERT INTO SPORTS_FIA_RACE_RESULT (season, series, round, race, pos, result, car_no, laps, race_time, race_time_ms, gap_time, gap_time_ms, pts, notes) VALUES ("' . $season . '", "f1", "' . $round . '", "' . $race . '", "' . $pos . '", "' . $driver[0] . '", "' . $driver[1] . '", "' . $driver[3] . '", ' . ($driver[4] ne '' ? sprintf('"%02d:%02d:%02d", "%d"', floor($driver[4] / 3600000), floor($driver[4] / 60000) % 60, floor($driver[4] / 1000) % 60, $driver[4] % 1000) : 'NULL, NULL') . ', ' . ($driver[6] ne '' ? sprintf('"%02d:%02d:%02d", "%d"', floor($driver[6] / 3600000), floor($driver[6] / 60000) % 60, floor($driver[6] / 1000) % 60, $driver[6] % 1000) : 'NULL, NULL') . ', ' . ($driver[5] ne '' ? '"' . $driver[5] . '"' : 'NULL') . ', ' . ($driver[7] ne '' ? '"' . $driver[7] . '"' : 'NULL') . ');' . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
    $pos++;
    $validation{"$validation_prefix-result"}++;
  }

  # Now those who did not qualify and so there is no row
  our @dnq;
  foreach my $car_no (@dnq) {
    print 'INSERT INTO SPORTS_FIA_RACE_RESULT (season, series, round, race, pos, result, car_no, laps, race_time, race_time_ms, gap_time, gap_time_ms, pts, notes) VALUES ("' . $season . '", "f1", "' . $round . '", "' . $race . '", "' . $pos++ . '", "DNQ", "' . $car_no . '", "0", NULL, NULL, NULL, NULL, NULL, "Did Not Qualify");' . "\n"
      if $print_mode == -1 || $print_mode == $mode_test;
    $validation{"$validation_prefix-result"}++;
  }

  # Number of completed laps
  print "\n" . '# Race ' . $race . ' Completed Laps' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  my $race_col = 'race' . ($race == 1 ? '' : $race) . '_laps_completed';
  print 'UPDATE SPORTS_FIA_RACES SET ' . $race_col . ' = "' . $max_laps . '"
  WHERE season = "' . $season . '" AND series = "f1" AND round = "' . $round . '";' . "\n"
    if $print_mode == -1 || $print_mode == $mode_test;
  $validation{"$validation_prefix-numlaps"} = $max_laps;

  benchmark_stamp('Process Race Table');

  # Fastest Lap
  $pattern = '<td[^>]+>(?:<i>)?<a[^>]+>Fastest lap<\/a>:(?:<\/i>)?(?:<i>)?<a[^>]+>(.*?)<\/a>\(<a[^>]+>.*?<\/a>\)[^<]*?(\d+)\:(\d{2})\.(\d{3})\s+\(lap (\d+)\)(?:<\/i>)?\s*<\/td>';
  @matches = ($race_table =~ m/$pattern/gsi);
  benchmark_stamp('Parse Fastest Lap');

  if (@matches && %drivers) {
    print "\n" . '# Race ' . $race . ' Fastest Lap' . "\n"
      if $print_mode == -1 || $print_mode == ($mode_test + 1);
    print 'DELETE FROM SPORTS_FIA_RACE_FASTEST_LAP WHERE season = "' . $season . '" AND series = "f1" AND round = "' . $round . '" AND race = "' . $race . '";' . "\n"
      if $print_mode == -1 || $print_mode == ($mode_test + 1);
    print 'INSERT INTO SPORTS_FIA_RACE_FASTEST_LAP (season, series, round, race, lap_time, lap_time_ms, lap_num, car_no) VALUES ("' . $season . '", "f1", "' . $round . '", "' . $race . '", "' . sprintf('00:%02d:%02d', $matches[1], $matches[2]) . '", "' . $matches[3] . '", "' . $matches[4] . '", "' . $drivers{$matches[0]} . '");' . "\n"
      if $print_mode == -1 || $print_mode == ($mode_test + 1);
    $validation{"$validation_prefix-fl"} = 1;
  }
  benchmark_stamp('Process Fastest Lap');
}

#
# Convert a HH:MM:SS.NNN time into a value we can adjust
#
sub parse_race_time {
  my ($time) = @_;
  $time = "0:$time" if $time =~ m/^\d{2}:\d{2}\.\d{3}$/;
  my @time = ($time =~ /(\d)?\:?(\d{1,2})(?:\:|\.)(\d{2})\.(\d{3})/);
  $time[0] = 0 if !defined($time[0]);
  return (3600000 * $time[0]) + (60000 * $time[1]) + (1000 * $time[2]) + $time[3];
}

# Return true to pacify the compiler
1;
