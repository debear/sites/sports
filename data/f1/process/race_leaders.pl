#!/usr/bin/perl -w
# Process the lap leaders for a race

our %validation;
our $results_dir;
our $results;
print "\n" . '# Race ' . $race . ' Lap Leader(s)' . "\n"
  if $print_mode == -1 || $print_mode == 6;
my $leaders_file = $results_dir . '/laps.htm.gz';
my $leaders_url = "https://en.wikipedia.org/w/index.php?title=Template:F1Laps${season}\&action=edit";
my $leaders = '';

# First check, do we need to (re-)download the file?
$pattern = ' class="[^"]*?selflink[^"]*?">([0-9A-Z]{3})<\/';
my ($country_code) = ($results =~ m/$pattern/gsi);
my $download = 1;

# Bodge...
$country_code = 'VAL' if $country_code eq 'EUR' && $season == 2011;
$country_code = 'UAE' if $country_code eq 'ABU' && $season == 2014;
$country_code = 'MYS' if $country_code eq 'MAL' && $season == 2015;

if (-e $leaders_file) {
  # Open from previous download
  open FILE, "gunzip -c $leaders_file |";
  my @leaders = <FILE>;
  close FILE;
  $leaders = join('', @leaders);
  benchmark_stamp('Load Lap Leaders');

  # Look for the country code
  my ($test) = ($leaders =~ m/\|($country_code)\s*=\s*&lt;timeline>.*?text.*?&lt;\/timeline>/gsi);
  $download = !defined($test);
}

if ($download) {
  # Download
  print '# Downloading (' . $leaders_url . '): '
    if $print_mode == -1 || $print_mode == 6;
  my $browser = new LWP::UserAgent;
  my $can_accept = HTTP::Message::decodable;
  my $request = $browser->get(
    $leaders_url,
    'Accept-Encoding' => $can_accept,
  );
  benchmark_stamp('Download Lap Leaders');

  # Save for future
  open FILE, '>' . $leaders_file;
  print FILE $request->content;
  close FILE;
  benchmark_stamp('Preserve Lap Leaders');
  print '[ Done ]' . "\n"
    if $print_mode == -1 || $print_mode == 6;

  # Re-open to resolve download decoding issue
  open FILE, "gunzip -c $leaders_file |";
  my @leaders = <FILE>;
  close FILE;
  $leaders = join('', @leaders);
  benchmark_stamp('Reload Lap Leaders');
}

# Now we have the info, extract
my ($race_list) = ($leaders =~ /\|$country_code\s*=\s*&lt;timeline>(.*?)&lt;\/timeline>/s);

# Skip if no race info found (we'll have to load it manually)
if (defined($race_list)) {
  my ($num_laps) = ($race_list =~ /^Period\s*=\s+from:\d+\s+till:(\d+)\s*$/m);

  my @leaders = ( 'DELETE FROM SPORTS_FIA_RACE_LEADERS WHERE season = "' . $season . '" AND series = "f1" AND round = "' . $round . '" AND race = "' . $race . '";' );
  @matches = ($race_list =~ /^\s*from:(\d*|start)\s+till:(\d*|end)\s.*?text:(.*?)\s*$/mg);
  benchmark_stamp('Parse Lap Leaders');
  while (my @match = splice(@matches, 0, 3)) {
    # Adjust for start / end
    if ($match[0] =~ /^\d+$/) {
      $match[0]++; # Numbers listed are "end of lap x to end of lap y", we want "start of lap x to end of lap y"
    } else {
      $match[0] = 1;
    }
    if ($match[1] !~ /^\d+$/) {
      $match[1] = $num_laps;
    }

    # Clean the name
    $match[2] =~ s/\s+\(.*?\)\s*$//;
    $match[2] = encode_entities(Encode::decode('UTF-8', $match[2]))
      if $match[2] !~ /&/ || $match[2] !~ /[^[:ascii:]]/ || $download;
    $match[2] =~ s/(Jr)\.$/$1/;

    # Add to the list of SQL to run
    push @leaders, 'INSERT INTO SPORTS_FIA_RACE_LEADERS (season, series, round, race, lap_from, lap_to, car_no)
  SELECT SPORTS_FIA_RACES.season, SPORTS_FIA_RACES.series, SPORTS_FIA_RACES.round, "' . $race . '", "' . $match[0] . '", "' . $match[1] . '", SPORTS_FIA_RACE_DRIVERS.car_no
  FROM SPORTS_FIA_RACES
  LEFT JOIN SPORTS_FIA_DRIVERS
    ON (LOWER(fn_basic_html_comparison(CONCAT(SPORTS_FIA_DRIVERS.first_name, " ", SPORTS_FIA_DRIVERS.surname))) = LOWER(fn_basic_html_comparison("' . $match[2] . '")))
  LEFT JOIN SPORTS_FIA_RACE_DRIVERS
    ON (SPORTS_FIA_RACE_DRIVERS.season = SPORTS_FIA_RACES.season
    AND SPORTS_FIA_RACE_DRIVERS.series = SPORTS_FIA_RACES.series
    AND SPORTS_FIA_RACE_DRIVERS.round = SPORTS_FIA_RACES.round
    AND SPORTS_FIA_RACE_DRIVERS.driver_id = SPORTS_FIA_DRIVERS.driver_id)
  WHERE SPORTS_FIA_RACES.season = "' . $season . '"
  AND   SPORTS_FIA_RACES.series = "f1"
  AND   SPORTS_FIA_RACES.round = "' . $round . '";';
  }

  $validation{'race-leaders'} = @leaders;
  print join("\n", @leaders) . "\n"
    if $print_mode == -1 || $print_mode == 6;
  benchmark_stamp('Process Lap Leaders');
} elsif ($print_mode == -1 || $print_mode == 6) {
  print "# Not found.\n";
}

# Return true to pacify the compiler
1;
