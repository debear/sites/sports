#!/usr/bin/perl -w
# Download and open the race results file

use File::Path qw(make_path);

our %race_info;
our $round;
our $script_dir;
our $results_dir =  sprintf('%s/_data/%04d/%02d', $script_dir, $season, $round);
make_path $results_dir if ! -e $results_dir;
my $round_name = $race_info{'name_full'}; $round_name =~ s/ /_/g; $round_name =~ s/&([^;])[^;]+;/$1/;
our $results_file = "$results_dir/results.htm.gz";
my $results_url = 'https://en.wikipedia.org/wiki/' . $season . '_' . $round_name;
our $results = '';
our $two_part_quali = ($season == 2015 && $round == 17);
our $num_quali_sessions = ($two_part_quali ? 2 : 3);

print '# ';
if (! -e $results_file || $force) {
  # Download
  print '' . ($force ? 'Forced ' : '') . 'Downloading (' . $results_url . '): ';
  my $browser = new LWP::UserAgent;
  my $can_accept = HTTP::Message::decodable;
  my $request = $browser->get(
    $results_url,
    'Accept-Encoding' => $can_accept,
  );
  $results = $request->decoded_content;
  benchmark_stamp('Download results');

  # Save the original gzipped content for future
  open FILE, '>' . $results_file;
  print FILE $request->content;
  close FILE;
  benchmark_stamp('Preserve results');

} else {
  # Open from previous download
  print 'Loading (' . $results_file . '): ';
  open FILE, "gunzip -c $results_file |";
  binmode FILE, ":encoding(utf8)" if $season == 2009;
  my @results = <FILE>;
  close FILE;
  $results = join('', @results);
  benchmark_stamp('Load results');
}
print '[ Done ]' . "\n";

# Global fixes
$results =~ s/<span class="mw-editsection"><span class="mw-editsection-bracket">.*?<\/span><\/span>(?:<\/div>)?//gsi;

# Initial validation to confirm this has the components we require, abort early if not
my $fl = ($results =~ /<i><a[^>]*>Fastest lap<\/a>:<\/i>\s+.*?(?:\d:)?\d{2}\.\d{3} \(lap \d+\)<\/i>/si);
my $lap = ($results =~ /<div[^>]*>Lap leaders<\/div>/si);
my $qual = ($results =~ /(<h3><span[^>]*>|<h3[^>]*?>)Qualifying classification<\//si);
my $race = ($results =~ /(<h3><span[^>]*>|<h3[^>]*?>)Race classification<\//si);
if (!$fl || !$lap || !$qual || !$race) {
  print STDERR "# Silently aborting after initial validation due to missing info in results file (";
  print STDERR "Fastest Lap: " . ($fl ? 'Set' : 'Missing') . ", ";
  print STDERR "Leaders: " . ($lap ? 'Set' : 'Missing') . ", ";
  print STDERR "Quali: " . ($qual ? 'Set' : 'Missing') . ", ";
  print STDERR "Race: " . ($race ? 'Set' : 'Missing');
  print STDERR ")\n";
  my $results_backup = $results_file; $results_backup =~ s/(\.htm\.gz)$/-failed$1/;
  rename $results_file, $results_backup; # Preserve, but out of the way for next attempt
  exit 1;
}

# Any championship point penalties? (Stored per-season, not per-race)
my $penalties_file = "$results_dir/../penalties.csv.gz";
my %penalties = ('team' => {}, 'driver' => {});
if (-e $penalties_file) {
  print '# Loading Championship Penalties (' . $penalties_file . '): ';
  open FILE, "gunzip -c $penalties_file |";
  my @penalties = <FILE>;
  close FILE;
  foreach my $penalty (@penalties) {
    chomp($penalty);
    my ($type, $id, $round_from, $pts, $notes) = split(',', $penalty);
    %{$penalties{$type}{$id}} = (
      'round_from' => $round_from,
      'pts' => $pts,
      'notes' => $notes,
    );
  }
  benchmark_stamp('Load penalties');
  print '[ Done ]' . "\n";
}

# Return true to pacify the compiler
1;
