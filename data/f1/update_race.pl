#!/usr/bin/perl -w
# Download and import data from Wikipedia for F1

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;
use Data::Dumper;
use DBI;
use HTML::Entities;
use Encode;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;

# Testing vars
my $debug = 0;
my @cmd_list = ( );

# Define log info
my $log_date = `date +'%F'`; chomp($log_date);
my $log_dir = "$config{'base_dir'}/_logs/process";
my $log_parse = '01_parse';
my $log_import = '02_import';

# Parse the arguments
my $is_unattended = grep(/^--unattended$/, @ARGV);
my $force = grep(/^--force$/, @ARGV);

# Determine the season we will be processing
my @time = localtime();
my $season = 1900 + $time[5];

# Look for races that completed 6hrs (360mins) ago in automated mode, 90mins in "manual"
my $start_offset = $is_unattended ? 360 : 90;

# Get the list of rounds to import
my @rounds = ( );
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT round, round_order, name_full, DATE_FORMAT(race_time, "%D %b") AS date_fmt
FROM SPORTS_FIA_RACES
WHERE season = ?
AND   series = "f1"
AND   IFNULL(race2_time, race_time) <= DATE_SUB(NOW(), INTERVAL ? MINUTE)
AND   IFNULL(race2_laps_completed, race_laps_completed) IS NULL
ORDER BY round_order;';
my $sth = $dbh->prepare($sql);
$sth->execute($season, $start_offset);
while (my $row = $sth->fetchrow_hashref) {
  # Convert name, in case it needs decoding
  $$row{'name_full'} = Encode::encode('UTF-8', decode_entities($$row{'name_full'}))
    if $$row{'name_full'} =~ /\&/;
  # Display
  $$row{'summary'} = 'Round ' . $$row{'round_order'} . ($$row{'round'} != $$row{'round_order'} ? ' (ID: ' . $$row{'round'} . ')' : '') . ': ' . $$row{'name_full'} . ' (' . $$row{'date_fmt'} . ')';
  print $$row{'summary'} . "\n";

  # Store for info
  push @rounds, $row;
}

# No games to import?
if (!@rounds) {
  # How we handle this depends on the running mode
  my $msg = ' - There are no outstanding rounds to import.' . "\n";
  if ($is_unattended) {
    # Update the lockfile state we're finished (as nothing to do...)
    my $lib = abs_path(__DIR__ . '/../_global/lockfile.pl');
    require $lib;
    lockfile_finish($config{'server_sync'});
    # In unattended mode, this is plausible so needs to end on a "silent" error
    print $msg;
    exit 97;
  } else {
    # Manual input considers this a "real" error
    print STDERR $msg;
    exit 98;
  }
}

# Inform user
print "\nAcquiring data from $season, " . @rounds . ' round' . (@rounds == 1 ? '' : 's') . "\n";
print 'Run started at ' . `date` . "\n";

# Idenfity the log file to use
my $extra = ''; my $extra_dot = '';
while (-e "$log_dir/$log_date$extra_dot$extra.tar.gz") {
  if ($extra eq ''){
    $extra_dot = '.';
    $extra = 1;
  } else {
    $extra++
  }
}
$log_date .= "$extra_dot$extra";
$log_dir .= "/$log_date";
mkdir $log_dir;

# Loop through each of the rounds
foreach my $round (@rounds) {
  print "-> $$round{'summary'}: ";

  # Parse
  my $ff = ($force ? '--force' : '');
  command("$config{'base_dir'}/process.pl $ff $season $$round{'round'}", "$log_dir/$log_parse.sql", "$log_dir/$log_parse.err");
  print "[ Done ]\n";
}

# Import into the database
print '=> Importing: ';
command("/usr/bin/mysql -s $config{'db_name'} <$log_dir/$log_parse.sql", "$log_dir/$log_import.err", "$log_dir/$log_import.err");
print "[ Done ]\n";

#
# Tidy up and end the script
#
end_script();
sub end_script {
  # Archive the log files
  run_command("cd $log_dir/.. && /usr/bin/tar -czf $log_date.tar.gz $log_date");
  unlink glob("$log_dir/*");
  rmdir $log_dir;

  # Inform user
  print "\n" . 'Run completed at ' . `date` . "\n";

  # Any debug data to display?
  print Dumper(\@cmd_list) if $debug;
}

#
# Run a sub-query
#
sub run_command {
  my ($cmd) = @_;
  push @cmd_list, $cmd if $debug;
  system($cmd) if !$debug;
  # Get exit status
  our $rc = ($? >> 8);
}

sub command {
  my ($cmd, $out_file, $err_file, $tee_file) = @_;

  # Build the command
  my $full_cmd = "$cmd";
  $full_cmd .= " >>$out_file" if defined($out_file);
  $full_cmd .= " 2>>$err_file" if defined($err_file);
  $full_cmd .= " 3>>$tee_file" if defined($tee_file);

  # Run
  our $rc = 0;
  run_command($full_cmd);

  # Check for errors
  if (!$debug && defined($err_file) && -e $err_file) {
    # Error = Line not starting with #
    my $lines = `grep '^[^#]' $err_file | wc -l`; chomp($lines);
    if ($lines) {
      my $file = basename($err_file);
      my $errors = ($lines > 10 ? "(Last 10 lines)\n" : '') . `grep '^[^#]' $err_file | tail`; chomp($errors);
      $errors =~ s/\n/\n> /g;
      my $s = ($lines == 1 ? '' : 's');
      print STDERR "Error$s logged in '$file' ($lines line$s):\n> $errors\n";
      end_script();
      exit 20;
    }
  }

  # Generic error, where no details were passed to STDERR
  if ($rc > 10) {
    print STDERR "An error occurred (code: $rc), exiting!\n";
    end_script();
    exit 20;

  # Silent fails, when the return code is less than 10 (early aborts that aren't errors per-se)
  } elsif ($rc) {
    end_script();
    exit $rc;
  }
}
