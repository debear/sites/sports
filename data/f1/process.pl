#!/usr/bin/perl -w
# Establish results for a race

use strict;
use utf8;
use POSIX;
use DBI;
use Dir::Self;
use Cwd 'abs_path';
use MIME::Base64;
use LWP::UserAgent;
use HTTP::Message;
use HTTP::Request::Common;
use HTML::Entities;
use Encode;
use Data::Dumper;

our @now = localtime;
our $print_mode = -1; # -1 = All, 0 = Off, 1..10 = Specific Section
our $max_pos = 24;

# Get info about our environment and load the rquired external libraries
our $script_dir = __DIR__;
my $lib = abs_path("$script_dir/../_global/benchmark.pl");
require $lib;

# Validate args, requires three:
#  1) Season
#  2) Round
if (@ARGV < 2 || @ARGV > 3
    || (@ARGV == 3 && $ARGV[0] ne '--force' && $ARGV[0] ne '--benchmark')
    || $ARGV[@ARGV - 2] !~ /^20[0-2][0-9]$/
    || $ARGV[@ARGV - 2] > (1900+$now[5])
    || $ARGV[@ARGV - 1] !~ /^[0-2]?[0-9]$/) {
  print STDERR 'Invalid Arguments.' . "\n";
  print STDERR ' 0) Force (optional)' . "\n";
  print STDERR ' 1) Season' . "\n";
  print STDERR ' 2) Round' . "\n";
  exit 98;
}

# "Rename" args
our $force = (@ARGV == 3 && $ARGV[0] eq '--force');
our $season = $ARGV[@ARGV - 2];
our $round = $ARGV[@ARGV - 1];
our $race = 0;

# Load some race info from the database
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT name_full, race_laps_total, race2_laps_total
FROM SPORTS_FIA_RACES
WHERE season = ?
AND   series = "f1"
AND   round = ?;';
my $sth = $dbh->prepare($sql);
$sth->execute($season, $round);
our %race_info = $sth->rows ? %{$sth->fetchrow_hashref} : ( );

$sth->finish;
$dbh->disconnect;

# Quick validation of the round info
if (!defined($race_info{'name_full'})) {
  print STDERR "Invalid arguments - cannot find a race for $season-$round.\n";
  exit 98;
}

# Component validation
our %validation = (
  'quali-q1' => 0,
  'quali-q2' => 0,
  'quali-q3' => 0,
  'quali-grid' => 0,
  'race-result' => 0,
  'race-numlaps' => 0,
  'race-fl' => 0,
  'race-leaders' => 0,
);
%validation = (%validation, (
  'sprint-sq1' => 0,
  'sprint-sq2' => 0,
  'sprint-sq3' => 0,
  'sprint-grid' => 0,
  'sprint-result' => 0,
  'sprint-numlaps' => 0,
  'sprint-fl' => 0,
)) if $race_info{'race2_laps_total'} > 0; # This has a sprint race component

# 2009 season had a dagger to identify cars using KERS, so need to open in UTF-8
binmode STDOUT, ":utf8" if $season == 2009;

require "$script_dir/process/_helpers.pl";

benchmark_stamp('Preperations');

#
# First, try to open download the results file
#
require "$script_dir/process/load.pl";

#
# Sprint Qualifying / Sprint Race Results
#
require "$script_dir/process/sprint_shootout.pl";
require "$script_dir/process/sprint_result.pl";
#require "$script_dir/process/sprint_leaders.pl"; # <- Not yet available

#
# Race Results
#
require "$script_dir/process/race_quali.pl";
require "$script_dir/process/race_result.pl";
require "$script_dir/process/race_leaders.pl";
require "$script_dir/process/race_validate.pl";

#
# Standings update
#
require "$script_dir/process/standings.pl";

#
# Stats
#
require "$script_dir/process/stats.pl";

#
# Post-processing validation checks
#
my @missing = ( );
foreach my $key (sort keys %validation) {
  push @missing, $key if !$validation{$key};
}
if (@missing) {
  print STDERR "The following components do not appear to have been set, so the processing is being considered failed:\n- " . join("\n- ", @missing);
  our $results_file;
  my $results_backup = $results_file; $results_backup =~ s/(\.htm\.gz)$/-failed$1/;
  rename $results_file, $results_backup; # Preserve, but out of the way for next attempt
  exit 1;
}
