#!/usr/bin/perl -w
# Parse the injury report

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be one: game_date
if (!@ARGV || $ARGV[0] !~ m/^20\d{2}\-[01]\d\-[0-3]\d$/) {
  print STDERR "Insufficient arguments to parse the starting lineups.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Define rest of vars from arguments
our ($game_date) = @ARGV;
our $season = substr($game_date, 0, 4);

# Have we the file?
my $file = sprintf('%s/_data/%d/lineups/%s.htm.gz', $config{'base_dir'}, $season, $game_date);
if (!-e $file) {
  print "# No Inactive List found ($season, $game_date)\n";
  exit;
}

# Get the games for today (to map MLB ID to ours)
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT `game_type`, `game_id`, `mlb_id` FROM `SPORTS_MLB_SCHEDULE` WHERE `season` = ? AND `game_date` = ?;';
my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $game_date);
$dbh->disconnect if defined($dbh);
my %game_ids = ();
foreach my $game (@$ret) {
  $game_ids{$$game{'mlb_id'}} = $game;
}

# Load the starting lineups
print "##\n## Loading starting lineups on $game_date: $file\n##\n\n";
print STDERR "# Loading starting lineups on $game_date\n";
my $contents;
open FILE, "gzip -dc $file |";
my @contents = <FILE>;
close FILE;
$contents = join('', @contents);
@contents = (); # Garbage collect...

# Get the list of players out
my $pattern = '<div class="starting-lineups__matchup " data-gamePk=(\d+)>\s*'
  . '<div class="starting-lineups__game">.*?</div>\s*'
  . '<div class="starting-lineups__pitchers">(.*?)</div>\s*'
  . '<div class="starting-lineups__teams starting-lineups__teams--sm[^"]*">(.*?)</div>\s*'
  . '<div class="starting-lineups__teams starting-lineups__teams--xs[^"]*">.*?</div>\s*'
  . '<div class="starting-lineups__buttons[^"]*">';
my @games = ($contents =~ m/$pattern/gsi);
while (my ($game_pk, $pitchers, $batters) = splice(@games, 0, 3)) {
  # Some games should not be included (such as lineups mixing pre- and regular season)
  next if !defined($game_ids{$game_pk});
  our %game_info = %{$game_ids{$game_pk}};
  # Get the teams
  $pattern = '<div class="starting-lineups__teams--away-head">\s*(\S+)\s.*<\/div>\s*'
    . '<div class="starting-lineups__teams--home-head">\s*(\S+)\s.*<\/div>';
  my ($away, $home) = ($batters =~ m/$pattern/gsi);
  $away = convert_team_id($away);
  $home = convert_team_id($home);
  print "#\n# Game $game_info{'game_type'}, $game_info{'game_id'} (MLB: $game_pk): $away @ $home\n#\n";
  my %lineups = (
    $away => [],
    $home => [],
  );

  # Get the starting pitchers
  if ($season < 2020) {
    $pattern = '<div class="starting-lineups__pitcher-name">\s*<a class="starting-lineups__pitcher--link" href="[^"]+\/player\/(\d+)\/[^"]+"[^>]*>(.*?)<\/a>.*?\s([LR])HP\s';
  } else {
    $pattern = '<div class="starting-lineups__pitcher-name">\s*<a class="starting-lineups__pitcher--link" href="\/player\/[^"]+-(\d+)"[^>]*>(.*?)<\/a>.*?\s([LR])HP\s';
  }
  my ($away_sp_id, $away_sp_name, $away_sp_bt, $home_sp_id, $home_sp_name, $home_sp_bt) = ($pitchers =~ /$pattern/gsi);
  push @{$lineups{$away}}, { 'name' => $away_sp_name, 'id' => $away_sp_id, 'pos' => 'SP', 'bat_throw' => $away_sp_bt };
  push @{$lineups{$home}}, { 'name' => $home_sp_name, 'id' => $home_sp_id, 'pos' => 'SP', 'bat_throw' => $home_sp_bt };

  # Then the lineups
  my ($lineup_away, $lineup_home) = ($batters =~ m/(<ol.*?<\/ol>)/gsi);
  my %lineups_raw = (
    $away => $lineup_away,
    $home => $lineup_home,
  );
  foreach my $team_id ($away, $home) {
    if ($season < 2020) {
      $pattern = '\/player\/(\d+)\/[^"]+"[^>]*>(.*?)<\/a><span class="starting-lineups__player--position"> \(([LRS])\) ([^<]+)<\/span>';
    } else {
      $pattern = '\/player\/[^"]+-(\d+)"[^>]*>(.*?)<\/a><span class="starting-lineups__player--position"> \(([LRS])\) ([^<]+)<\/span>';
    }
    my @lineup = ($lineups_raw{$team_id} =~ m/$pattern/gsi);
    while (my ($remote_id, $name, $bat_throw, $pos) = splice(@lineup, 0, 4)) {
      push @{$lineups{$team_id}}, { 'name' => $name, 'id' => $remote_id, 'pos' => $pos, 'bat_throw' => $bat_throw };
    }
  }

  # And now render
  render($away, $lineups{$away});
  render($home, $lineups{$home});
  print "\n";
}

# Tie the two IDs together
print "# Tie to imported players
UPDATE `SPORTS_MLB_GAME_LINEUPS`
JOIN `SPORTS_MLB_PLAYERS_IMPORT`
  ON (`SPORTS_MLB_PLAYERS_IMPORT`.`remote_id` = `SPORTS_MLB_GAME_LINEUPS`.`remote_id`)
SET `SPORTS_MLB_GAME_LINEUPS`.`player_id` = `SPORTS_MLB_PLAYERS_IMPORT`.`player_id`
WHERE `SPORTS_MLB_GAME_LINEUPS`.`player_id` IS NULL;\n\n";

# Table maintenance
print "# Table maintenance
ALTER TABLE `SPORTS_MLB_GAME_LINEUPS` ORDER BY `season`, `game_type`, `game_id`, `team_id`, `spot`;\n";

# Render a team's lineups
sub render {
  my ($team_id, $lineup) = @_;
  our ($season, %game_info);
  my $rows = '';
  print "\n# $team_id\n";
  for (my $spot = 0; $spot < @$lineup; $spot++) {
    my %player = %{$$lineup[$spot]};
    # Skip unknown players
    next if !defined($player{'id'});
    # Process
    $rows .= "  # $spot: $player{'name'} ($player{'pos'}, B/T: $player{'bat_throw'}, ID: $player{'id'})\n";
    check_for_null(\$player{'pos'});
    check_for_null(\$player{'bat_throw'});
    $rows .= "  ('$season', '$game_info{'game_type'}', '$game_info{'game_id'}', '$team_id', '$spot', '$player{'id'}', NULL, $player{'pos'}, $player{'bat_throw'})";
    $rows .= ",\n"
      if $spot < (@$lineup - 1);
  }
  if (!$rows) {
    print "# Skipped, no lineup available.\n";
    return;
  }
  print "INSERT INTO `SPORTS_MLB_GAME_LINEUPS` (`season`, `game_type`, `game_id`, `team_id`, `spot`, `remote_id`, `player_id`, `pos`, `bat_throw`) VALUES
$rows
ON DUPLICATE KEY UPDATE `remote_id` = VALUES(`remote_id`), `player_id` = VALUES(`player_id`), `pos` = VALUES(`pos`), `bat_throw` = VALUES(`bat_throw`);\n";
}
