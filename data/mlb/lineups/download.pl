#!/usr/bin/perl -w
# Get the current starting lineups from MLB.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be one: game_date
if (!@ARGV || $ARGV[0] !~ m/^20\d{2}\-[01]\d\-[0-3]\d$/) {
  print STDERR "Insufficient arguments to download the starting lineups.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($game_date) = @ARGV;
my $season = substr($game_date, 0, 4);
my $dir = sprintf('%s/_data/%d/lineups', $config{'base_dir'}, $season);
my $file = sprintf('%s.htm.gz', $game_date);

# Pre-2018 reports unavailable...
if ($season < 2018) {
  print STDERR "# Unable to download inactives list from previous seasons.\n";
  exit 10;
}

# Run...
print "# Downloading Starting Lineups for $game_date: ";

# Skip if already done and a special flag has been passed (given time-sensitive nature of repeated runs, always downloading needs to be the default behaviour)
if (-e "$dir/$file" && grep(/--skip-if-exists/, @ARGV)) {
  print "[ Skipped ]\n";
  exit;
}

# Make sure the game dir exists
make_path($dir);

# Build the URL and download
my $url = "https://www.mlb.com/starting-lineups/$game_date";
download($url, "$dir/$file", {'gzip' => 1});
print "[ Done ]\n";
