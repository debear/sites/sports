#!/usr/bin/perl -w
# Download, import and process draft results from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'draft';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my @time = localtime();
my $season = $time[5] + 1900;
print "Draft Season: $season\n";

# Which game types?
my @mode = ();
my $q = question_user("Amateur Draft? [Yn]", '[yn]', 'y');
push @mode, 'Amateur' if $q eq 'y';
$q = question_user("Rule V (Major League portion)? [yN]", '[yn]', 'n');
push @mode, 'R5-Maj' if $q eq 'y';
$q = question_user("Rule V (AAA portion)? [yN]", '[yn]', 'n');
push @mode, 'R5-AAA' if $q eq 'y';
$q = question_user("Rule V (AA portion)? [yN]", '[yn]', 'n');
push @mode, 'R5-AA' if $q eq 'y';

# Ensure something has been entered
if (!@mode) {
  print STDERR "Error: Please select at least one draft type.\n";
  exit;
}
print "\n";

# Tidy internal variables after being determined above
my $mode = join(' ', @mode);

## Inform user
print "\nAcquiring data from $season, $mode draft types\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

# Loop through the draft types separately
foreach my $mode (@mode) {
  my $mode_lc = lc $mode;
  print "=> $mode:\n";

  # Download, if we haven't done so already (handled in-script)
  print '  -> Download: ';
  command("$config{'base_dir'}/draft/download.pl $season $mode_lc",
          "$config{'log_dir'}/$logs{'download'}.log",
          "$config{'log_dir'}/$logs{'download'}.err");
  done(0);

  # Parse (unless download only...)
  if (!download_only()) {
    print '  -> Parse: ';
    command("$config{'base_dir'}/draft/parse.pl $season $mode_lc",
            "$config{'log_dir'}/$logs{'parse'}.sql",
            "$config{'log_dir'}/$logs{'parse'}.err");
    done(3);
  }
}

# If only downloading, go no further
end_script() if download_only();

# Import
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'import'}.log",
        "$config{'log_dir'}/$logs{'import'}.err");
done(4);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'parse'    => '02_parse',
    'import'   => '03_import',
  );
}
