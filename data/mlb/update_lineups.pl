#!/usr/bin/perl -w
# Download, import and process the starting lineups from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'lineups';
$config{'log_inc_time'} = '%H%M';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my $date = time2date(time());
my $season = substr($date, 0, 4);

# Identify week to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT DISTINCT game_date
FROM SPORTS_MLB_SCHEDULE
WHERE season = ?
' . (!$config{'is_historical'}
  ? 'AND   status IS NULL
     AND   CONCAT(game_date, " ", game_time) > CONVERT_TZ(NOW(), @@session.time_zone, "' . $config{'timezone'} . '")
     AND   CONCAT(game_date, " ", game_time) <= DATE_ADD(CONVERT_TZ(NOW(), @@session.time_zone, "' . $config{'timezone'} . '"), INTERVAL ? MINUTE)'
  : 'AND   CONCAT(game_date, " ", game_time) <= CONVERT_TZ(NOW(), @@session.time_zone, "' . $config{'timezone'} . '")
     ORDER BY game_date DESC
     LIMIT 1') . ';';
my $sth = $dbh->prepare($sql);
my @sth_args = ( $season );
push @sth_args, $config{'lineups_time_to_start'} if !$config{'is_historical'};
$sth->execute(@sth_args);
my ($game_date) = $sth->fetchrow_array;

undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);

# If nothing returned, not time to run this
if (!defined($game_date)) {
  if (!$config{'is_historical'}) {
    print STDERR "No games starting within the next $config{'lineups_time_to_start'} minutes, exiting...\n";
    exit $config{'exit_codes'}{'failed-prereq-silent'};
  } else {
    print STDERR "No historic games available, exiting...\n";
    exit $config{'exit_codes'}{'failed-prereq'};
  }
}

## Inform user
print "\nAcquiring data from $season, $game_date\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Download, if we haven't done so already (handled in-script)
#
print '=> Download: ';
command("$config{'base_dir'}/lineups/download.pl $game_date",
        "$config{'log_dir'}/$logs{'download'}.log",
        "$config{'log_dir'}/$logs{'download'}.err");
done(0);

# If only downloading, go no further
end_script() if download_only();

#
# Parse
#
print '=> Parse: ';
command("$config{'base_dir'}/lineups/parse.pl $game_date",
        "$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'parse'}.err");
done(3);

#
# Import into the database
#
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'import'}.log",
        "$config{'log_dir'}/$logs{'import'}.err");
done(2);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'parse'    => '02_parse',
    'import'   => '03_import',
  );
}
