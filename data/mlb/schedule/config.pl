#!/usr/bin/perl -w
# Common code for the schedule downloading and parsing

#
# Convert command line args in to something we can process
#
sub parse_args {
  my $season = ''; my $mode = ''; my $start_date = 'initial';
  foreach my $arg (@ARGV) {
    if ($arg eq '--regular') {
      $mode = ($mode eq '' ? 'regular' : 'both');
    } elsif ($arg eq '--playoff') {
      $mode = ($mode eq '' ? 'playoff' : 'both');
    } elsif ($season eq '') {
      $season = $arg;
    } elsif ($arg =~ /^\d{4}-\d{2}-\d{2}$/ && $start_date eq 'initial') {
      $start_date = $arg;
    } else {
      print '# Unknown option "' . $arg . '"' . "\n";
    }
  }

  # Must have at least season and mode
  if ($season eq '' || $mode eq '') {
    print STDERR "Invalid arguments.\n";
    exit 98;
  }

  return ($season, $mode, $start_date);
}

#
# Where are the files downloaded in v1 of the integration
#
sub get_base_dir_v1 {
  our (%config, $season, $start_date);
  return sprintf('%s/_data/%s/schedules/%s', $config{'base_dir'}, $season, $start_date);
}

#
# Where are the files downloaded in v2 of the integration
#
sub get_base_dir_v2 {
  our (%config, $season);
  return sprintf('%s/_data/%s/schedules', $config{'base_dir'}, $season);
}

#
# Load a file
#
sub load_file {
  my ($filename) = @_;

  open FILE, "<$filename";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ();

  return $contents;
}

#
# Load a gzipped file
#
sub load_gzip {
  my ($filename) = @_;

  open FILE, "gunzip -c $filename |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ();

  return $contents;
}

# Return true to pacify the compiler
1;
