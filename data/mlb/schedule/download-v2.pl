#!/usr/bin/perl -w
# Get the schedule for a season from MLB.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;
use DateTime;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Determine the date file before we make $config{'base_dir'} season-specific
my $date_file = $config{'base_dir'} . '/_data/schedule/dates.csv';

# Parse the arguments (if called directly)
our ($season, $mode, $start_date);
($season, $mode, $start_date) = parse_args()
  if !defined($season);
my $local_file = $start_date;
$config{'base_dir'} = get_base_dir_v2();

# Get the vars to pass to the web request
my @requests = ( );
push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# Determine the dates to loop through
my %dates;
my $dates_str = load_file($date_file);
foreach my $line (split("\n", $dates_str)) {
  next if $line !~ m/^$season,/;
  my @dates = split(",", $line);
  %dates = ( 'regular' => { 'start' => $dates[1], 'end' => $dates[2] },
             'playoff' => { 'start' => $dates[3], 'end' => $dates[4] } );
}

# Some data manipulation?
if ($start_date ne 'initial' && $start_date gt $dates{'regular'}{'start'}) {
  my @s = split('-', $start_date);
  my $dt = DateTime->new(
    year => $s[0],
    month => $s[1],
    day => $s[2]
  );
  $dt->add( days => -1 );
  $start_date = $dt->ymd;
}

# Valid season?
if (!%dates) {
  print STDERR "Unknown season '$season'\n";
  exit 98;
}

# Custom dates?
if ($start_date ne 'initial') {
  if (!defined($dates{'playoff'}{'start'}) || $start_date lt $dates{'playoff'}{'start'}) {
    $dates{'regular'}{'start'} = $start_date;
  } else {
    $dates{'playoff'}{'start'} = $start_date;
  }
}

# Get the files
foreach my $game_type (@requests) {
  # Skip if no known dates
  next if !defined($dates{$game_type}{'start'});

  # Run...
  my $url = sprintf(
    'https://statsapi.mlb.com/api/v%s/schedule?sportId=1&startDate=%s&endDate=%s&gameType=R&gameType=F&gameType=D&gameType=L&gameType=W&season=%d&hydrate=flags,venue(location)&language=en&leagueId=103&leagueId=104',
    1, # v1 of the statsapi integration
    $dates{$game_type}{'start'},
    $dates{$game_type}{'end'},
    $season
  );
  my $local = "$config{'base_dir'}/$game_type/$local_file.json";
  print "# Downloading $game_type schedule between $dates{$game_type}{'start'} and $dates{$game_type}{'end'}\n";
  download($url, $local, {'gzip' => 1, 'skip-today' => 1});
}

# Return true to pacify the compiler
return 1;
