#!/usr/bin/perl -w
# Parse the schedule for a season from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Archive::Tar;
use JSON;
use Switch;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments (if called directly)
our ($season, $mode, $start_date);
($season, $mode, $start_date) = parse_args()
  if !defined($season);
$config{'base_dir'} = get_base_dir_v1();

# Load in alternate venue info
my %alt_venues = ( );
my $alt_file = "$config{'data_dir'}/schedule/alt_venues.csv";
if ( -e $alt_file ) {
  open FILE, "<$alt_file";
  my @alt = <FILE>;
  close FILE;

  foreach my $line (@alt) {
    chomp($line);
    next if $line !~ m/^$season-/; # Skip games that are not for the season we are processing
    my $c = substr($line, 10, 1); # Split char is the first one after the data column
    my ($d, $v, $h, $s) = split(/$c/, $line);
    $alt_venues{"$d-$v-$h"} = convert_text($s);
  }

  @alt = ( ); # Homegrown garbage collect...
}

# If we have playoff games, get the pre-determined round codes
my %playoff_codes = ( ); my $playoff_round;
my %po_game_instances = ( ); my %po_ppd_instances = ( ); # Count of matchup instances (for playoff game number calcs)
if (grep /^$mode$/, ('playoff', 'both')) {
  my $sql = 'SELECT SERIES.round_code, SUBSTRING(SERIES.round_code, 1, 1) AS round_num,
           HIGHER.team_id AS higher_seed, LOWER.team_id AS lower_seed,
           MAX(SERIES.complete) AS complete,
           IF(HIGHER.team_id < LOWER.team_id, CONCAT(HIGHER.team_id, ":", LOWER.team_id), CONCAT(LOWER.team_id, ":", HIGHER.team_id)) AS matchup_key,
           IFNULL(SUBSTRING(MAX(SCHED_F.game_id), 3, 1), 0) AS max_prev_num,
           IFNULL(SUBSTRING(MAX(SCHED_PPD.game_id), 3, 1), 0) AS max_ppd_num
    FROM SPORTS_MLB_PLAYOFF_SERIES AS SERIES
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS HIGHER
      ON (HIGHER.season = SERIES.season
      AND HIGHER.conf_id = SERIES.higher_conf_id
      AND HIGHER.seed = SERIES.higher_seed)
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS LOWER
      ON (LOWER.season = SERIES.season
      AND LOWER.conf_id = SERIES.lower_conf_id
      AND LOWER.seed = SERIES.lower_seed)
    LEFT JOIN SPORTS_MLB_SCHEDULE AS SCHED_F
      ON (SCHED_F.season = SERIES.season
      AND SCHED_F.game_type = "playoff"
      AND SCHED_F.game_id LIKE CONCAT(SERIES.round_code, "%")
      AND IFNULL(SCHED_F.status, "F") = "F"
      AND SCHED_F.game_date < DATE_SUB(?, INTERVAL 1 DAY))
    LEFT JOIN SPORTS_MLB_SCHEDULE AS SCHED_PPD
      ON (SCHED_PPD.season = SERIES.season
      AND SCHED_PPD.game_type = "playoff"
      AND SCHED_PPD.game_id LIKE CONCAT(SERIES.round_code, "%")
      AND SCHED_PPD.status = "PPD"
      AND SCHED_PPD.game_date < DATE_SUB(?, INTERVAL 1 DAY))
    WHERE SERIES.season = ?
    GROUP BY SERIES.round_code;';
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $check_date = $start_date =~ /^20[0-2]\d\-[01]\d\-[0-3]\d$/ ? $start_date : "2001-01-01";
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $check_date, $check_date, $season);
  foreach my $series (@$ret) {
    # Write two versions: H:L and L:H to facilitate search
    $playoff_codes{$$series{'lower_seed'} . ':' . $$series{'higher_seed'}} = $$series{'round_code'};
    $playoff_codes{$$series{'higher_seed'} . ':' . $$series{'lower_seed'}} = $$series{'round_code'};
    # Round determination check
    $playoff_round = $$series{'round_num'}
      if !$$series{'complete'} && (!defined($playoff_round) || $$series{'round_num'} lt $playoff_round);
    # Counters
    $po_game_instances{$$series{'matchup_key'}} = $$series{'max_prev_num'};
    $po_ppd_instances{$$series{'matchup_key'}} = $$series{'max_prev_num'};
  }
}

# Open our tar file and get the list of schedule files
my $tar_file = $config{'base_dir'} . '.tar.gz';
my $tar = Archive::Tar->new($tar_file);
my @schedule_files = sort($tar->list_files());

# Get and loop through the list of files
my @game_types = ( { 'dir' => 'regular', 'codes' => [ 'R' ] },
                   { 'dir' => 'playoff', 'codes' => [ 'F', 'D', 'L', 'W' ] } );
my @sched_list = (); my @update_list; my @delete_list; my @custom_list;
foreach my $game_type (@game_types) {
  # Ignore?
  next if !grep /^$mode$/, ($$game_type{'dir'}, 'both');

  # Prune the schedule files into the appropriate season type then loop through and load
  foreach my $file (grep(/\/$$game_type{'dir'}\/\d{4}-\d{2}-\d{2}\.json/, @schedule_files)) {
    my %json = %{decode_json($tar->get_content($file))};

    # Skip if no games today
    next if !defined($json{'data'}{'games'}{'game'});

    # Determine our game list (which could be as an array or a hash)
    my @games = ref($json{'data'}{'games'}{'game'}) eq 'ARRAY' ? @{$json{'data'}{'games'}{'game'}} : ( $json{'data'}{'games'}{'game'} );
    my $game_date = $json{'data'}{'games'}{'year'} . '-' . $json{'data'}{'games'}{'month'} . '-' . $json{'data'}{'games'}{'day'};

    # Then loop through each of the games that day
    foreach my $game (@games) {
      # Skip unless the game is the correct type
      next if !grep /^$$game{'game_type'}$/, @{$$game_type{'codes'}};

      # Data fixes
      $$game{'original_date'} = substr($$game{'id'}, 0, 10)
        if (defined($$game{'resume_date'}) && $$game{'resume_date'} && (!defined($$game{'original_date'}) || !$$game{'original_date'}) && $$game{'resume_date'} ne $game_date);
      $$game{'tiebreaker_sw'} = 'N'
        if (defined($$game{'tiebreaker_sw'}) && $$game{'tiebreaker_sw'} eq 'false');

      # Get our game details
      my $mlb_id = $$game{'game_pk'};
      my $gameday_link = $$game{'gameday'};
      my $home = convert_team_id($$game{'home_name_abbrev'});
      my $visitor = convert_team_id($$game{'away_name_abbrev'});
      $$game{'time'} =~ s/^(\d{1,2}:\d{2})$/$1:00/ if $$game{'time'} =~ /^\d{1,2}:\d{2}$/;
      $$game{'time'} = '03:33:00' if $$game{'time'} !~ /^\d{1,2}:\d{2}:\d{2}$/;
      my @time = split(':', $$game{'time'});
      my $game_time = sprintf('%02d:%02d:00', ($$game{'ampm'} eq 'AM') || ($time[0] == 12) ? $time[0] : $time[0]+12, $time[1]);
      my $started_date = defined($$game{'original_date'}) ? $$game{'original_date'} : ''; $started_date =~ s/\//-/g;
      $started_date = undef if $started_date eq $game_date;
      check_for_null(\$started_date);
      my $playin_switch = defined($$game{'tiebreaker_sw'}) && ($$game{'tiebreaker_sw'} eq 'Y');
      my $playin_descrip = ($$game_type{'dir'} eq 'regular' && $$game{'description'} =~ /tiebreak/i);
      my $playin_game = int($playin_switch || $playin_descrip);
      my $alt_venue = (defined($alt_venues{"$game_date-$visitor-$home"}) ? $alt_venues{"$game_date-$visitor-$home"} : undef);
      check_for_null(\$alt_venue);

      # Fix TBD game time's from MLB.com's 03:33 to our 19:05 (local)
      if ($game_time eq '03:33:00') {
        switch ($$game{'home_time_zone'}) {
          case /^E/ { $game_time = '19:05:00'; }
          case /^C/ { $game_time = '20:05:00'; }
          case /^M/ { $game_time = '21:05:00'; }
          case /^P/ { $game_time = '22:05:00'; }
        }
      }

      # Time Ref version (numeric version of date and time)
      my $game_time_ref = "(100 * (DATEDIFF('$game_date', '$season-01-01') + 1)) + " . substr($game_time, 0, 2);

      # Secondary check for Postponed or Cancelled statuses
      my $status;
      if (substr($$game{'status'}{'ind'}, 0, 1) eq 'D') {
        # Postponed
        $status = 'PPD';
      } elsif (substr($$game{'status'}{'ind'}, 0, 1) eq 'C') {
        # Cancelled
        $status = 'CNC';
      } else {
        # Suspended
        my $resume_date = $$game{'resume_date'};
        if (defined($resume_date) && $resume_date) {
          $resume_date =~ s/\//-/g;
          if ($resume_date ne $game_date) {
            $status = 'SSP';
            $$game{'status'}{'reason'} = 'Game Suspended';
          }
        }
      }
      if (defined($status)) {
        my $status_reason = convert_text($$game{'status'}{'reason'});
        check_for_null(\$status_reason);
        push @update_list, "UPDATE `SPORTS_MLB_SCHEDULE` SET `status` = '$status', `status_info` = $status_reason WHERE `season` = '$season' AND `mlb_id` = '$$game{'game_pk'}' AND `game_date` = '$game_date';";
      }

      # The Game ID needs to be calculated for playoffs (as grouped by three-digit reference: ROUND-SERIES-GAME)
      my $game_id = undef;
      if ($$game_type{'dir'} eq 'playoff') {
        # Skip games for future playoff rounds that we haven't determined a round code for, or future rounds themselves
        my $round_code = $playoff_codes{"$home:$visitor"};
        next if !defined($round_code) || substr($round_code, 0, 1) gt $playoff_round;
        # Determine game number
        my $matchup_key = ($home lt $visitor ? "$home:$visitor" : "$visitor:$home");
        my $series_game_num;
        if (!defined($status)) {
          $po_game_instances{$matchup_key} = 0 if !defined($po_game_instances{$matchup_key});
          $po_game_instances{$matchup_key}++;
          $series_game_num = $po_game_instances{$matchup_key};
        } else {
          # Games that don't count towards our series should not be in the 1-[157] range (as it is used to define the series game number)
          $po_ppd_instances{$matchup_key} = 7 if !defined($po_ppd_instances{$matchup_key});
          $po_ppd_instances{$matchup_key}++;
          $series_game_num = $po_ppd_instances{$matchup_key};
          # But we also need to avoid a (potential) key conflict, so remove what was there already
          push @delete_list, "DELETE FROM `SPORTS_MLB_SCHEDULE` WHERE `season` = '$season' AND `mlb_id` = '$mlb_id' AND `game_date` = '$game_date';";
        }
        # Add to list
        $game_id = ($round_code * 10) + $series_game_num;
      }

      # Add the info to our list
      check_for_null(\$game_id);
      push @sched_list, "'$season', '$$game_type{'dir'}', $game_id, '$mlb_id', '$gameday_link', '$home', '$visitor', '$game_date', '$game_time', $game_time_ref, $started_date, '$playin_game', $alt_venue";
    }
  }

  # Is there any custom SQL to add?
  my $custom_file = "$config{'data_dir'}/schedule/custom_${season}_$$game_type{'dir'}.sql";
  if (-e $custom_file) {
    open FILE, "<$custom_file";
    my @custom = <FILE>;
    close FILE;
    push @custom_list, @custom;
  }
}

# Skip if no matches
if ( !@sched_list) {
  exit;
}

# Now output as SQL
print "# Pre-import pruning\n" . join("\n", @delete_list) . "\n\n" if @delete_list;
print "# Main schedule import\nINSERT INTO `SPORTS_MLB_SCHEDULE` (`season`, `game_type`, `game_id`, `mlb_id`, `gameday_link`, `home`, `visitor`, `game_date`, `game_time`, `game_time_ref`, `started_date`, `playin_game`, `alt_venue`) VALUES\n(" . join("),\n(", @sched_list) . ")\nON DUPLICATE KEY UPDATE `home` = VALUES(`home`), `visitor` = VALUES(`visitor`), `game_date` = VALUES(`game_date`), `game_time` = VALUES(`game_time`), `game_time_ref` = VALUES(`game_time_ref`), `started_date` = VALUES(`started_date`), `playin_game` = VALUES(`playin_game`), `alt_venue` = VALUES(`alt_venue`);\n";
print "\n# Status updates\n" . join("\n", @update_list) . "\n" if @update_list;
print "\n# Custom updates\n" . join('', @custom_list) . "\n" if @custom_list;
print "\n# Tidy\nALTER TABLE `SPORTS_MLB_SCHEDULE` ORDER BY `season`, `game_type`, `game_id`;\n";

## If regular season, some extra info
if ((grep /^$mode$/, ('regular', 'both')) && ($start_date eq 'initial')) {
  print "\n";

  # Team v Team matchups
  print "# Team v Team matchups\n";
  print "CALL mlb_schedule_matchups('$season');\n\n";

  # Power ranking weeks
  print "# Power rank dates\n";
  print "CALL mlb_power_ranks_dates('$season');\n\n";

  # Initial standings
  print "# Initial standings\n";
  print "CALL mlb_standings_initial('$season');\n";
}

# Return true to pacify the compiler
return 1;
