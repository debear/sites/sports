#!/usr/bin/perl -w
# Get the schedule for a season from MLB.com

use strict;
use File::Path qw(make_path rmtree);
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;
use DateTime;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Determine the date file before we make $config{'base_dir'} season-specific
my $date_file = $config{'base_dir'} . '/_data/schedule/dates.csv';

# Parse the arguments (if called directly)
our ($season, $mode, $start_date);
($season, $mode, $start_date) = parse_args()
  if !defined($season);
$config{'base_dir'} = get_base_dir_v1();

# Already downloaded?
my $tar_file = $config{'base_dir'} . '.tar.gz';
if ( -e $tar_file ) {
  print "# Skipping, '$start_date' already downloaded.\n";
  exit;
}

# Get the vars to pass to the web request
my @requests = ( );
push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# Determine the dates to loop through
my %dates;
my $dates_str = load_file($date_file);
foreach my $line (split("\n", $dates_str)) {
  next if $line !~ m/^$season,/;
  my @dates = split(",", $line);
  %dates = ( 'regular' => { 'start' => $dates[1], 'end' => $dates[2] },
             'playoff' => { 'start' => $dates[3], 'end' => $dates[4] } );
}

# Some data manipulation?
if ($start_date ne 'initial' && $start_date gt $dates{'regular'}{'start'}) {
  my @s = split('-', $start_date);
  my $dt = DateTime->new(
    year => $s[0],
    month => $s[1],
    day => $s[2]
  );
  $dt->add( days => -1 );
  $start_date = $dt->ymd;
}

# Valid season?
if (!%dates) {
  print STDERR "Unknown season '$season'\n";
  exit 98;
}
make_path($config{'base_dir'});

# Custom dates?
if ($start_date ne 'initial') {
  if (!defined($dates{'playoff'}{'start'}) || $start_date lt $dates{'playoff'}{'start'}) {
    $dates{'regular'}{'start'} = $start_date;
  } else {
    $dates{'playoff'}{'start'} = $start_date;
  }
}

# Get the files
foreach my $game_type (@requests) {
  # Skip if no known dates
  next if !defined($dates{$game_type}{'start'});

  # Run...
  print "# Downloading $game_type schedule in $config{'base_dir'}, between $dates{$game_type}{'start'} and $dates{$game_type}{'end'}\n";
  my $loop_end = 0;
  my $date = $dates{$game_type}{'start'};
  for (my $i = 0; !$loop_end && $i < 200; $i++) { # Catch-all...
    # Known (historical) data issue
    if ($date eq '2008-06-11') {
      print "# Intentionally skipping a date: '$date'\n";
      $date = '2008-06-12';
      next;
    }

    # Perform the download
    my $local = download_file($date, $game_type);

    # Determine the next date from the file
    my $json = load_file($local);
    my ($next) = ($json =~ m/"next_day_date"\s*:\s*"(20[0-2]\d-[01]\d-[0-3]\d)"/gsi);
    if ($next le $dates{$game_type}{'end'}) {
      $date = $next;
    } else {
      $loop_end = 1;
    }
  }
}

# Tar together in to a single file
my $tar_dir = dirname($config{'base_dir'});
my $tar_loc = basename($config{'base_dir'});
`tar -C$tar_dir -czf $tar_file $tar_loc`;
rmtree($config{'base_dir'});

# Perform the download
sub download_file {
  my ($date, $game_type) = @_;
  my $url = sprintf('https://gd.mlb.com/components/game/mlb/year_%04d/month_%02d/day_%02d/master_scoreboard.json', split('-', $date));
  my $local = sprintf('%s/%s/%s.json', $config{'base_dir'}, $game_type, $date);
  print "# $date: $url => $local\n";

  # Skip if already downloaded
  return $local if (-e $local && ! -z $local);

  # Then perform the download
  download($url, $local);

  return $local;
}

# Return true to pacify the compiler
return 1;
