#!/usr/bin/perl -w
# Parse the schedule for a season from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Date::Manip::Date;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments (if called directly)
our ($season, $mode, $start_date);
($season, $mode, $start_date) = parse_args()
  if !defined($season);
$config{'base_dir'} = get_base_dir_v2();

# Setup the date manipulation object for timzone parsing
my $date_manip = Date::Manip::Date->new(undef, ['setdate' => 'now,utc']);

# Load in alternate venue info
my %alt_venues = ( );
my $alt_file = "$config{'data_dir'}/schedule/alt_venues.csv";
if ( -e $alt_file ) {
  open FILE, "<$alt_file";
  my @alt = <FILE>;
  close FILE;

  foreach my $line (@alt) {
    chomp($line);
    next if $line !~ m/^$season-/; # Skip games that are not for the season we are processing
    my $c = substr($line, 10, 1); # Split char is the first one after the data column
    my ($d, $v, $h, $s, $n) = split(/$c/, $line); # $n = Name (acts like a comment)
    $alt_venues{"$d-$v-$h"} = convert_text($s);
  }

  @alt = ( ); # Homegrown garbage collect...
}

# If we have playoff games, get the pre-determined round codes
my %playoff_codes = ( ); my $playoff_round;
my %po_game_instances = ( ); my %po_ppd_instances = ( ); # Count of matchup instances (for playoff game number calcs)
if (grep /^$mode$/, ('playoff', 'both')) {
  my $sql = 'SELECT SERIES.round_code, SUBSTRING(SERIES.round_code, 1, 1) AS round_num,
           HIGHER.team_id AS higher_seed, LOWER.team_id AS lower_seed,
           MAX(SERIES.complete) AS complete,
           IF(HIGHER.team_id < LOWER.team_id, CONCAT(HIGHER.team_id, ":", LOWER.team_id), CONCAT(LOWER.team_id, ":", HIGHER.team_id)) AS matchup_key,
           IFNULL(SUBSTRING(MAX(SCHED_F.game_id), 3, 1), 0) AS max_prev_num,
           IFNULL(SUBSTRING(MAX(SCHED_PPD.game_id), 3, 1), 0) AS max_ppd_num
    FROM SPORTS_MLB_PLAYOFF_SERIES AS SERIES
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS HIGHER
      ON (HIGHER.season = SERIES.season
      AND HIGHER.conf_id = SERIES.higher_conf_id
      AND HIGHER.seed = SERIES.higher_seed)
    JOIN SPORTS_MLB_PLAYOFF_SEEDS AS LOWER
      ON (LOWER.season = SERIES.season
      AND LOWER.conf_id = SERIES.lower_conf_id
      AND LOWER.seed = SERIES.lower_seed)
    LEFT JOIN SPORTS_MLB_SCHEDULE AS SCHED_F
      ON (SCHED_F.season = SERIES.season
      AND SCHED_F.game_type = "playoff"
      AND SCHED_F.game_id LIKE CONCAT(SERIES.round_code, "%")
      AND IFNULL(SCHED_F.status, "F") = "F"
      AND SCHED_F.game_date < DATE_SUB(?, INTERVAL 1 DAY))
    LEFT JOIN SPORTS_MLB_SCHEDULE AS SCHED_PPD
      ON (SCHED_PPD.season = SERIES.season
      AND SCHED_PPD.game_type = "playoff"
      AND SCHED_PPD.game_id LIKE CONCAT(SERIES.round_code, "%")
      AND SCHED_PPD.status = "PPD"
      AND SCHED_PPD.game_date < DATE_SUB(?, INTERVAL 1 DAY))
    WHERE SERIES.season = ?
    GROUP BY SERIES.round_code;';
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $check_date = $start_date =~ /^20[0-2]\d\-[01]\d\-[0-3]\d$/ ? $start_date : "2001-01-01";
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $check_date, $check_date, $season);
  foreach my $series (@$ret) {
    # Write two versions: H:L and L:H to facilitate search
    $playoff_codes{$$series{'lower_seed'} . ':' . $$series{'higher_seed'}} = $$series{'round_code'};
    $playoff_codes{$$series{'higher_seed'} . ':' . $$series{'lower_seed'}} = $$series{'round_code'};
    # Round determination check
    $playoff_round = $$series{'round_num'}
      if !$$series{'complete'} && (!defined($playoff_round) || $$series{'round_num'} lt $playoff_round);
    # Counters
    $po_game_instances{$$series{'matchup_key'}} = $$series{'max_prev_num'};
    $po_ppd_instances{$$series{'matchup_key'}} = $$series{'max_prev_num'};
  }
}

# Get and loop through the list of files
my @game_types = ( { 'dir' => 'regular', 'codes' => [ 'R' ] },
                   { 'dir' => 'playoff', 'codes' => [ 'F', 'D', 'L', 'W' ] } );
my @sched_list = (); my @update_list; my @delete_list; my @custom_list;
foreach my $game_type (@game_types) {
  # Ignore?
  next if !grep /^$mode$/, ($$game_type{'dir'}, 'both');

  # Load the file
  my $sched = "$config{'base_dir'}/$$game_type{'dir'}/$start_date.json.gz";
  my $file = load_gzip($sched);
  my %full_json = %{decode_json($file)};

  # Loop through the dates block
  foreach my $json (@{$full_json{'dates'}}) {
    # Skip if no games today
    next if !defined($$json{'games'});

    # Then loop through each of the games that day
    foreach my $game (@{$$json{'games'}}) {
      # Skip unless the game is the correct type
      next if !grep /^$$game{'gameType'}$/, @{$$game_type{'codes'}};
      # Data fixes
      $$game{'originalDate'} = substr($$game{'resumedFrom'}, 0, 10)
        if (defined($$game{'resumedFrom'}) && $$game{'resumedFrom'} && (!defined($$game{'originalDate'}) || !$$game{'originalDate'}) && $$game{'resumedFrom'} ne $$json{'date'});
      $$game{'tiebreaker'} = 'N'
        if (defined($$game{'tiebreaker'}) && $$game{'tiebreaker'} eq 'false');

      # Get our game details
      my $mlb_id = $$game{'gamePk'};
      my $home = convert_team_id_from_num($$game{'teams'}{'home'}{'team'}{'id'});
      my $visitor = convert_team_id_from_num($$game{'teams'}{'away'}{'team'}{'id'});
      # Skip playoff games where the two teams have not been set
      if ($$game_type{'dir'} eq 'playoff' && (!defined($home) || !defined($visitor))) {
        next;
      }

      # Game time and basic flags
      my $game_time;
      if (my $e = $date_manip->parse($$game{'gameDate'})) {
        $game_time = '03:33:00';
      } else {
        $date_manip->convert($config{'timezone'});
        $game_time = $date_manip->printf('%H:%M:%S');
      }
      my $started_date = defined($$game{'originalDate'}) ? $$game{'originalDate'} : ''; $started_date =~ s/\//-/g;
      my $playin_switch = defined($$game{'tiebreaker'}) && ($$game{'tiebreaker'} eq 'Y');
      my $playin_descrip = ($$game_type{'dir'} eq 'regular' && $$game{'seriesDescription'} =~ /tiebreak/i);
      my $playin_game = int($playin_switch || $playin_descrip);
      my $alt_venue = (defined($alt_venues{"$$json{'date'}-$visitor-$home"}) ? $alt_venues{"$$json{'date'}-$visitor-$home"} : undef);
      check_for_null(\$alt_venue);

      # Fix TBD game time's from MLB.com's 03:33 to our 19:05 (ET, as we can no longer get the home team's timezone)
      $game_time = '19:05:00'
        if $game_time eq '03:33:00';

      # Time Ref version (numeric version of date and time)
      my $game_time_ref = "(100 * (DATEDIFF('$$json{'date'}', '$season-01-01') + 1)) + " . substr($game_time, 0, 2);

      # Secondary check for Postponed or Cancelled statuses
      my $status; my $status_date;
      if (substr($$game{'status'}{'codedGameState'}, 0, 1) eq 'D') {
        # Postponed
        $status = 'PPD';
      } elsif (substr($$game{'status'}{'codedGameState'}, 0, 1) eq 'C') {
        # Cancelled
        $status = 'CNC';
      } elsif ($started_date && ($started_date ne $$json{'date'})) {
        # Suspended
          $status = 'SSP';
          $status_date = $started_date;
          $$game{'status'}{'reason'} = 'Game Suspended';
      }
      if (defined($status)) {
        my $status_reason = convert_text($$game{'status'}{'reason'});
        $status_date = $$json{'date'} if !defined($status_date);
        check_for_null(\$status_reason);
        push @update_list, "UPDATE `SPORTS_MLB_SCHEDULE` SET `status` = '$status', `status_info` = $status_reason WHERE `season` = '$season' AND `mlb_id` = '$mlb_id' AND `game_date` = '$status_date';";
      }

      # Now we've used the raw started date, make it db-compatible
      $started_date = undef if $started_date eq $$json{'date'};
      check_for_null(\$started_date);

      # The Game ID needs to be calculated for playoffs (as grouped by three-digit reference: ROUND-SERIES-GAME)
      my $game_id = undef;
      if ($$game_type{'dir'} eq 'playoff') {
        # Skip games for future playoff rounds that we haven't determined a round code for, or future rounds themselves
        my $round_code = $playoff_codes{"$home:$visitor"};
        next if !defined($round_code) || substr($round_code, 0, 1) gt $playoff_round;
        # Determine game number
        my $matchup_key = ($home lt $visitor ? "$home:$visitor" : "$visitor:$home");
        my $series_game_num;
        if (!defined($status)) {
          $po_game_instances{$matchup_key} = 0 if !defined($po_game_instances{$matchup_key});
          $po_game_instances{$matchup_key}++;
          $series_game_num = $po_game_instances{$matchup_key};
        } else {
          # Games that don't count towards our series should not be in the 1-[157] range (as it is used to define the series game number)
          $po_ppd_instances{$matchup_key} = 7 if !defined($po_ppd_instances{$matchup_key});
          $po_ppd_instances{$matchup_key}++;
          $series_game_num = $po_ppd_instances{$matchup_key};
          # But we also need to avoid a (potential) key conflict, so remove what was there already
          push @delete_list, "DELETE FROM `SPORTS_MLB_SCHEDULE` WHERE `season` = '$season' AND `mlb_id` = '$mlb_id' AND `game_date` = '$$json{'date'}';";
        }
        # Add to list
        $game_id = ($round_code * 10) + $series_game_num;
      }

      # Add the info to our list
      check_for_null(\$game_id);
      push @sched_list, "'$season', '$$game_type{'dir'}', $game_id, '$mlb_id', NULL, '$home', '$visitor', '$$json{'date'}', '$game_time', $game_time_ref, $started_date, '$playin_game', $alt_venue";
    }
  }

  # Is there any custom SQL to add?
  my $custom_file = "$config{'data_dir'}/schedule/custom_${season}_$$game_type{'dir'}.sql";
  if (-e $custom_file) {
    open FILE, "<$custom_file";
    my @custom = <FILE>;
    close FILE;
    push @custom_list, @custom;
  }
}

# Store the parsed schedule
if (@sched_list) {
  # Build the SQL for our implied schedule changes (where no explicit changes passed in between runs)
  my $update_implied = "INSERT INTO SPORTS_MLB_SCHEDULE (`season`, `game_type`, `game_id`, `mlb_id`, `home`, `visitor`, `game_date`, `status`, `status_info`)
    SELECT `A`.`season`, `A`.`game_type`, `A`.`game_id`, `A`.`mlb_id`, `A`.`home`, `A`.`visitor`, `A`.`game_date`, IFNULL(`B`.`status`, 'PPD') AS `status`, `B`.`status_info`
    FROM `SPORTS_MLB_SCHEDULE` AS `A`
    JOIN `SPORTS_MLB_SCHEDULE` AS `B`
      ON (`B`.`season` = `A`.`season`
      AND `B`.`game_type` = `A`.`game_type`
      AND `B`.`mlb_id` = `A`.`mlb_id`
      AND `B`.`game_date` > `A`.`game_date`)
    WHERE `A`.`season` = '$season'
    AND   `A`.`game_date` < CURDATE()
    AND   `A`.`status` IS NULL
    GROUP BY `A`.`season`, `A`.`game_type`, `A`.`game_id`
  ON DUPLICATE KEY UPDATE `status` = VALUES(`status`), `status_info` = VALUES(`status_info`);";


  # Now output as SQL
  print "# Pre-import pruning\n" . join("\n", @delete_list) . "\n\n" if @delete_list;
  print "# Main schedule import\nINSERT INTO `SPORTS_MLB_SCHEDULE` (`season`, `game_type`, `game_id`, `mlb_id`, `gameday_link`, `home`, `visitor`, `game_date`, `game_time`, `game_time_ref`, `started_date`, `playin_game`, `alt_venue`) VALUES\n  (" . join("),\n  (", @sched_list) . ")\nON DUPLICATE KEY UPDATE `home` = VALUES(`home`), `visitor` = VALUES(`visitor`), `game_date` = VALUES(`game_date`), `game_time` = VALUES(`game_time`), `game_time_ref` = VALUES(`game_time_ref`), `started_date` = VALUES(`started_date`), `playin_game` = VALUES(`playin_game`), `alt_venue` = VALUES(`alt_venue`);\n";
  print "\n# Explicit status updates\n" . join("\n", @update_list) . "\n" if @update_list;
  print "\n# Implied status updates\n$update_implied\n";
  print "\n# Custom updates\n" . join('', @custom_list) . "\n" if @custom_list;
  print "\n# Tidy\nALTER TABLE `SPORTS_MLB_SCHEDULE` ORDER BY `season`, `game_type`, `game_id`;\n";
}

# Schedule dates
print "# Schedule dates\n";
print "CALL mlb_schedule_dates('$season');\n\n";

## If regular season, some extra info
if ((grep /^$mode$/, ('regular', 'both')) && ($start_date eq 'initial')) {
  print "\n";

  # Team v Team matchups
  print "# Team v Team matchups\n";
  print "CALL mlb_schedule_matchups('$season');\n\n";

  # Power ranking weeks
  print "# Power rank dates\n";
  print "CALL mlb_power_ranks_dates('$season');\n\n";

  # Initial standings
  print "# Initial standings\n";
  print "CALL mlb_standings_initial('$season');\n";
}

# Return true to pacify the compiler
return 1;
