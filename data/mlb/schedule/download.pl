#!/usr/bin/perl -w
# Determine which version of the script to run and run it

use strict;
use File::Path qw(make_path rmtree);
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments to determine the requested season
our ($season, $mode, $start_date) = parse_args();
my $v;
if ($season < 2020) {
    $v = 1;
} else {
    $v = 2;
}
my $script = $0;
$script =~ s/\.pl/-v$v.pl/;
require $script;
