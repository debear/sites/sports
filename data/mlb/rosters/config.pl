#!/usr/bin/perl -w
# Perl config file

use Dir::Self;
use Cwd 'abs_path';
use Switch;

my $methods = abs_path(__DIR__ . '/../players/download.methods.pl');
require $methods;

$config{'data_dir'} = "$config{'base_dir'}/_data/$season";
if (defined($date)) {
  $config{'data_dir_roster'} = "$config{'data_dir'}/rosters/$date";
  $config{'data_dir_depth'} = "$config{'data_dir'}/depth_charts/$date";
}
$config{'data_dir_player'} = "$config{'base_dir'}/_data/players";

our $force = !grep(/^--existing-profiles$/, @ARGV); # Use existing player profiles (save a lookup whilst testing)

# Validate and load a file
sub load_file {
  my ($file) = @_;

  # Load the file
  my $open_cmd = (substr($file, -3) eq '.gz' ? "gzip -dc $file |" : "<$file");
  open(FILE, $open_cmd);
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( );

  return $contents;
}

# Prepare our queries to determine / set remote->imported IDs
sub setup_db_queries {
  my ($date) = @_;
  our %config; our $dbh;
  my $sql;
  $config{'sth'} = { } if !defined($config{'sth'});

  # Player info
  $sql = 'SELECT IMPORT.remote_id, IMPORT.player_id,
                 PLAYER.first_name, PLAYER.surname,
                 SUM(IFNULL(ROSTER.roster_pos, "") = "P") > 0 AS is_pitcher, IMPORT.profile_imported
          FROM SPORTS_MLB_PLAYERS_IMPORT AS IMPORT
          LEFT JOIN SPORTS_MLB_PLAYERS AS PLAYER
            ON (PLAYER.player_id = IMPORT.player_id)
          LEFT JOIN SPORTS_MLB_GAME_ROSTERS AS ROSTER
            ON (ROSTER.player_id = PLAYER.player_id)
          WHERE IMPORT.remote_id = ?
          GROUP BY IMPORT.remote_id;';
  $config{'sth'}{'player_info'} = $dbh->prepare($sql);

  # Determine the last roster info we have (for our previous roster info)
  if (defined($date) && $date =~ m/^\d{4}-\d{2}-\d{2}$/) {
    $sql = 'SELECT MAX(the_date) FROM SPORTS_MLB_TEAMS_ROSTERS WHERE season = ? AND the_date < ?;';
    my $sth = $dbh->prepare($sql);
    $sth->execute($season, $date);
    $config{'last_roster_date'} = $sth->fetchrow_array;
    # Our main query
    $sql = 'SELECT pos
  FROM SPORTS_MLB_TEAMS_ROSTERS
  WHERE season = ?
  AND   the_date = ?
  AND   player_id = ?;';
    # Prepare our statment
    $config{'sth'}{'prev_roster_pos'} = $dbh->prepare($sql);
  }

  # Determine if there were games yesterday (as if not, we'll need to get 25-man roster positions too)
  if (!defined($date) || $date ne 'initial') {
    $sql = 'SELECT COUNT(*) = 0 AS no_game_updates FROM SPORTS_MLB_SCHEDULE WHERE season = ? AND game_date = DATE_SUB(CURDATE(), INTERVAL 1 DAY);';
    my $sth = $dbh->prepare($sql);
    $sth->execute($season);
    ($config{'no-game-updates'}) = $sth->fetchrow_array();
  } else {
    $config{'no-game-updates'} = 1;
  }
}

# Get info on the player out of the database
sub load_player {
  my ($remote_id) = @_;
  $config{'sth'}{'player_info'}->execute($remote_id);
  my $player = $config{'sth'}{'player_info'}->fetchrow_hashref;

  # Reg-ex versions
  player_regexs($player)
    if defined($player);

  return $player;
}

# Determine the date we're processing
sub determine_db_date {
  my ($dbh, $season, $date) = @_;
  my $sth;
  if ($date eq 'initial') {
    $sth = $dbh->prepare('SELECT DATE_SUB(MIN(game_date), INTERVAL 1 DAY) AS first_date FROM SPORTS_MLB_SCHEDULE WHERE season = ?;');
    $sth->execute($season);
  } else {
    $sth = $dbh->prepare('SELECT DATE_SUB(?, INTERVAL 1 DAY) AS roster_date;');
    $sth->execute($date);
  }
  ($date) = $sth->fetchrow_array;
  return $date;
}

# Determine a player's roster position from the last roster run we did (saving constnat bio downloading / parsing)
sub determine_previous_pos {
  return undef if !defined($config{'last_roster_date'});

  my ($player_id) = @_;
  our $season;
  $config{'sth'}{'prev_roster_pos'}->execute($season, $config{'last_roster_date'}, $player_id);
  my ($pos) = $config{'sth'}{'prev_roster_pos'}->fetchrow_array;
  return $pos;
}

# Parse (downloading, if required, the player bio for additional information)
sub parse_player_bio {
  my ($remote_id, $pos, $status, $jersey) = @_;
  print "# Downloading player bio\n";
  my $player = load_player($remote_id);

  # Perform the download and load the file
  $config{'player_dir'} = $config{'data_dir_player'} . '/' . $remote_id;
  my ($ret, $failed, $local_file) = download_profile($player);
  my $profile = load_file($local_file);

  # Get the position
  ($pos) = ($profile =~ m/"pt_primary_position"\s*:\s*"([^"]+)",/gsi)
    if !defined($pos);
  ($pos) = ($profile =~m/<ul>\s*?<li>([^<]+)<\/li>\s*?<li>B\/T/gsi)
    if !defined($pos);
  # Some position fixes
  if (defined($pos)) {
    if ($pos =~ m/\-/) {
      print "# Splitting position (roster): '$pos'\n";
      ($pos) = split('-', $pos);
    }

    if ($pos eq 'D') {
      print "# Found ambiguous position (roster): '$pos', returning DH\n";
      $pos = 'DH';
    } elsif ($pos eq 'O' || $pos eq 'OF') {
      print "# Found ambiguous position (roster): '$pos', returning CF\n";
      $pos = 'CF';
    } elsif ($pos eq 'I' || $pos eq 'IF') {
      print "# Found ambiguous position (roster): '$pos', returning SS\n";
      $pos = 'SS';
    }
  }

  # Check for the status
  if (!defined($status)) {
    ($status) = ($profile =~ m/"display_status"\s*:\s*"([^"]+)",/gsi);
    ($status) = ($profile =~ m/<span class="label">Status: <\/span>(.*?) \n/gsi) if !defined($status);
    $status = '' if !defined($status);
    switch (lc($status)) {
      case 'injured 7-day' { $status = 'dl-7'; }
      case 'injured 10-day' { $status = 'dl-10'; }
      case 'injured 15-day' { $status = 'dl-15'; }
      case 'injured 45-day' { $status = 'dl-45'; }
      case 'injured 60-day' { $status = 'dl-60'; }
      case 'paternity list' { $status = 'paternity'; }
      case 'bereavement list' { $status = 'bereavement'; }
      case 'restricted list' { $status = 'suspended'; }
      # Fallback: Not Active
      else { $status = 'na'; }
    }
  }

  # How about the jersey?
  ($jersey) = ($profile =~ m/"pt_jersey_number"\s*:\s*"([^"]+)",/gsi)
    if !defined($jersey);
  ($jersey) = ($profile =~ m/<span class="player-header--vitals-number">&#35;(\d+)<\/span>/gsi)
    if !defined($jersey);
  $jersey = 0
    if !defined($jersey);

  return ($pos, $status, $jersey);
}

sub determine_commemorative_jersey {
  my ($date) = @_;
  my $file = "$config{'base_dir'}/_data/schedule/jersey_commemorations.csv";
  return undef
    if ! -e $file;

  # Load the CSV, and parse it for instances of the passed date
  my $diary = load_file($file);
  foreach my $line (split "\n", $diary) {
    if (substr($line, 0, 11) eq "$date,") {
      # We have a matching date, so parse in to the return object
      my @split = split ',', $line;
      return {
        'name' => $split[1],
        'number' => $split[2],
      };
    }
  }
  # No match, so return undef
  return undef;
}

# Return true to pacify the compiler
1;
