#!/usr/bin/perl -w
# Parse depth charts from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);
use Switch;

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing depth charts for '$season' // '$date'\n#\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Prepare our queries to determine / set remote->imported IDs
setup_db_queries($date);

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

# Load the files from our tar file
my $tar = Archive::Tar->new("$config{'data_dir_depth'}.tar.gz");
my @depth_files = sort($tar->list_files());

# But we'll also need the 40-man roster files (as some players in the depth chart may not be listed)
my $roster_tar = Archive::Tar->new("$config{'data_dir_roster'}.tar.gz");

# Then loop through the team rosters
foreach my $file (grep(/\/[A-Z]{2,3}\.htm/, @depth_files)) {
  my ($team_id) = ($file =~ m/\/([A-Z]{2,3})\.htm/);
  print "\n##\n## $team_id\n##\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;

  # Load the 40-man roster file
  my $roster_file = $file;
  $roster_file =~ s/(\.htm)/_40man$1/;
  my $roster_gz = $roster_tar->get_content($roster_file);
  my $roster;
  gunzip \$roster_gz => \$roster;
  my %roster = ();
  foreach my $remote_id ($roster =~ m/<a href="\/player\/[^"]+-(\d+)">.*?<\/a>/gsi) {
    $roster{$remote_id} = 1;
  }

  # Get the position blocks
  my @sect = ($contents =~ m/<table class="roster__table">(.*?)<\/table>/gsi);
  foreach my $sect (@sect) {
    my ($title, $list) = ($sect =~ m/<thead>\s+<tr>\s+<td colspan="2">(.*?)<\/td>.*?<tbody(.+)<\/tbody>/gsi);
    my $pos;
    switch ($title) {
      case 'Rotation' { $pos = 'SP'; }
      case 'Bullpen' { $pos = 'RP'; }
      case 'Catcher' { $pos = 'C'; }
      case 'First Base' { $pos = '1B'; }
      case 'Second Base' { $pos = '2B'; }
      case 'Third Base' { $pos = '3B'; }
      case 'Shortstop' { $pos = 'SS'; }
      case 'Left Field' { $pos = 'LF'; }
      case 'Center Field' { $pos = 'CF'; }
      case 'Right Field' { $pos = 'RF'; }
      case 'Designated Hitter' { $pos = 'DH'; }
    }
    print "\n#\n# $title ($pos)\n#\n";

    my @depth = ($list =~ m/<a href="\/player\/[^"]+-(\d+)">(.*?)<\/a>/gsi);
    my $i = 1;
    while (my ($remote_id, $name) = splice(@depth, 0, 2)) {
      if (!defined($roster{$remote_id})) {
        my $player_id = determine_player_id($remote_id);
        my $pos_prev = determine_previous_pos($player_id);
        my ($pos, $status, $jersey) = parse_player_bio($remote_id, $pos_prev);
        print "# Found unknown player: $name (Remote: $remote_id, ID: $player_id)\n";
        print "INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
  VALUES ('$season', '$date_roster', '$team_id', '$player_id', '$jersey', '$pos', '$status', NULL)
ON DUPLICATE KEY UPDATE pos = VALUES(pos),
                        player_status = VALUES(player_status),
                        roster_type = VALUES(roster_type);\n";
        # Flag the player has been parsed
        $roster{$remote_id} = 1;
      }
      my $player_id = determine_player_id($remote_id);
      print "# $i: $name (Remote: $remote_id, ID: $player_id)\n";
      print "INSERT INTO SPORTS_MLB_TEAMS_DEPTH (season, team_id, pos, depth, player_id)
  VALUES ('$season', '$team_id', '$pos', '$i', '$player_id')
ON DUPLICATE KEY UPDATE player_id = VALUES(player_id);\n";
      $i++;
    }

    # Prune
    print "\n# Prune\n";
    print "DELETE FROM SPORTS_MLB_TEAMS_DEPTH WHERE season = '$season' AND team_id = '$team_id' AND pos = '$pos' AND depth >= '$i';\n";
  }
}

print "\n# Table maintenance\n";
print "ALTER TABLE SPORTS_MLB_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;\n";
print "ALTER TABLE SPORTS_MLB_TEAMS_DEPTH ORDER BY season, team_id, pos, depth;\n";

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
