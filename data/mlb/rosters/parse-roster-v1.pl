#!/usr/bin/perl -w
# Parse the team rosters from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);
use Switch;
use Time::HiRes qw(gettimeofday);

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing rosters for '$season' // '$date'\n#\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Prepare our queries to determine / set remote->imported IDs
setup_db_queries($date);

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

# Load the files from our tar file
my $tar_file = $config{'data_dir_roster'} . '.tar.gz';
my $tar = Archive::Tar->new($tar_file);
my @roster_files = sort($tar->list_files());

# Cache the output
print "\n##\n## First pass: Main processing and jersey checks\n##\n\n";
my $output = '';
my @jersey_backdate = ( );

# Then loop through the team rosters
foreach my $file (grep(/\/[A-Z]{2,3}\.htm/, @roster_files)) {
  my ($team_id) = ($file =~ m/\/([A-Z]{2,3})\.htm/);
  $output .= "\n##\n## $team_id\n##\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;

  # Break it down in to sections
  my @sect = ($contents =~ m/(<section class="module">\s*?<h4>.*?<\/h4>\s*?<table class="data roster_table".*?<\/table>)/gsi);
  foreach my $sect (@sect) {
    my ($title, $list) = ($sect =~ m/<h4>(.*?)<\/h4>\s*?<table class="data roster_table"(.*?)<\/table>/gsi);
    $output .= "\n#\n# $title\n#\n";

    # Can we contrive a position code from the position?
    my $pos_sect;
    switch (lc($title)) {
      case 'pitchers' { $pos_sect = 'P'; }
      case 'catchers' { $pos_sect = 'C'; }
      case 'designated hitters' { $pos_sect = 'DH'; }
    }

    # Next, get the players out
    my @players = ($list =~ m/<tr[^>]*?>\s*<td[^>]*class="dg-jersey_number">(.*?)<\/td>.*?<a(.*?) href="\/player\/(\d+)\/[^"]+">(.*?)<\/a>(.*?)\s*<\/td>/gsi);
    while (my @player = splice(@players, 0, 5)) {
      $output .= "\n# $player[3] (Remote: $player[2], Raw Jersey: $player[0], Class: '$player[1]', Status: '$player[4]')\n";
      my $roster;
      my $status;
      my $jersey; # Need variable definition, but value obtained in above regex

      # Determine the player_id from a remote_id
      my $player_id = determine_player_id($player[2]);

      # If we don't have a position, try and get it from the last roster
      my $pos = $pos_sect;
      $pos = determine_previous_pos($player_id)
        if !defined($pos);

      # Roster type? (from class)
      if ($player[1] =~ m/player-inactive/i) {
        $roster = '40-man';
      } elsif ($player[1] !~ m/player-noton40man/i) {
        $roster = '25-man';
        $status = 'active';
      }

      # Player status? (from info)
      if ($player[4] =~ m/7-day/i) {
        $status = 'dl-7';
      } elsif ($player[4] =~ m/10-day/i) {
        $status = 'dl-10';
      } elsif ($player[4] =~ m/15-day/i) {
        $status = 'dl-15';
      } elsif ($player[4] =~ m/45-day/i) {
        $status = 'dl-45';
      } elsif ($player[4] =~ m/60-day/i) {
        $status = 'dl-60';
      }

      # In some instances, we may need to load the player file:
      #  - We don't have a specific position for a non-25 man roster player (25-man rosters covered in game scripts)
      #  - Player isn't active or on DL (as player could be on a specific list)
      $roster = check_for_null(\$roster);
      my $no_game_updates = (!defined($pos) && $config{'no-game-updates'});
      $output .= "# (Requesting bio: No position and no games to update roster pos)\n" if $no_game_updates;
      my $no_pos = (!defined($pos) && $roster ne '\'25-man\'');
      $output .= "# (Requesting bio: No position and not on 25 man roster; $roster)\n" if $no_pos;
      my $no_status = (!defined($status));
      $output .= "# (Requesting bio: No status)\n" if $no_status;
      my $no_jersey = ($player[0] eq '42' || $player[0] eq '&mdash;' || $player[0] eq '-');
      $output .= "# (Requesting bio: Invalid jersey; " . (defined($player[0]) ? $player[0] : 'undef') . ")\n" if $no_jersey;
      ($pos, $status, $jersey) = parse_player_bio($player[2], $pos, $status, $jersey)
        if $no_game_updates || $no_pos || $no_status || $no_jersey;

      # Jersey check? (e.g., Jackie Robinson Day where _everyone_ active wears #42)
      if (!defined($player[0]) && defined($jersey)) {
        # Take it from the parsed player page
        $player[0] = $jersey;
      } elsif ($no_jersey) {
        if (defined($jersey) && $jersey ne $player[0]) {
          # Take it from the parsed player page
          $player[0] = $jersey;
        } else {
          # Backdate
          $player[0] = "{{JERSEY_$player_id}}";
          push @jersey_backdate, { 'id' => $player_id, 'name' => $player[3], 'team_id' => $team_id };
        }
      }

      # Display
      $pos = check_for_null(\$pos);
      $output .= "INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
  VALUES ('$season', '$date_roster', '$team_id', '$player_id', '$player[0]', $pos, '$status', $roster)
ON DUPLICATE KEY UPDATE pos = VALUES(pos),
                        player_status = VALUES(player_status),
                        roster_type = VALUES(roster_type);\n";
    }
  }
}

# Pass two: converting any Jackie Robinson Day numbers
print "\n##\n## Second pass\n##\n\n";
if (@jersey_backdate) {
  print "# Getting backdated jersey numbers\n\n";
  # Get the IDs
  my @ids = ();
  foreach my $b (@jersey_backdate) {
    push @ids, $$b{'id'};
  }
  # Database action
  my $sql = 'SELECT player_id, SUBSTRING(MAX(CONCAT(the_date, ":", jersey)), 12) AS jersey
FROM SPORTS_MLB_TEAMS_ROSTERS
WHERE season = ?
AND   player_id IN (' . substr(',?'x@ids, 1) . ')
AND   the_date < ?
GROUP BY player_id;';
  print "# Running database query...\n";
  my $bench_start = gettimeofday();
  my $jersey_map = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, @ids, $date_roster);
  my $bench_end = gettimeofday();
  print "# Done!\n# - Took: " . sprintf('%.03f', $bench_end - $bench_start) . " seconds\n\n";
  # Loop back to update, but also print debug
  print "# Requesting player IDs for:\n";
  my %jersey_map = ();
  foreach my $map (@$jersey_map) {
    $jersey_map{$$map{'player_id'}} = $$map{'jersey'};
    $output =~ s/{{JERSEY_$$map{'player_id'}}}/$$map{'jersey'}/g;
  }
  my %unknown_by_team = ();
  foreach my $b (@jersey_backdate) {
    # Still missing?
    if (!defined($jersey_map{$$b{'id'}})) {
      # New player with unknown jersey. Assume 100+ (and hope for future correction)
      $unknown_by_team{$$b{'team_id'}} = 100
        if !defined($unknown_by_team{$$b{'team_id'}});
      $output =~ s/{{JERSEY_$$b{'id'}}}/$unknown_by_team{$$b{'team_id'}}/g;
      $jersey_map{$$b{'id'}} = "$unknown_by_team{$$b{'team_id'}} (via fallback)";
      $unknown_by_team{$$b{'team_id'}}++;
    }
    # State
    print "# - $$b{'team_id'} $$b{'name'} (ID: $$b{'id'}) - Returned #$jersey_map{$$b{'id'}}\n";
  }
} else {
  print "# Skipped...\n";
}

# Then render
print $output;

# Note: table maintenance delayed until post-depth chart processing (final stage) to prevent duplication of work

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
