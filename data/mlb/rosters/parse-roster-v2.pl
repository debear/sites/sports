#!/usr/bin/perl -w
# Parse the team rosters from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);
use Switch;
use Time::HiRes qw(gettimeofday);

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing rosters for '$season' // '$date'\n#\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Prepare our queries to determine / set remote->imported IDs
setup_db_queries($date);

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

# Load the jersey commemoration dates, to see if one applies today
my $commemorating = determine_commemorative_jersey($date);
print "\n## Commemoration Today: $$commemorating{'name'}, #$$commemorating{'number'}\n"
  if defined($commemorating);

# Load the files from our tar file
my $tar = Archive::Tar->new("$config{'data_dir_roster'}.tar.gz");
my @roster_files = sort($tar->list_files());

# Write the output to a buffer we (may) post-process
print "\n##\n## Main processing and jersey checks\n##\n\n";
my $output = '';
my @jersey_backdate = ( );

# First pass: The Active roster
my %roster_active = ( );
foreach my $file (grep(/\/[A-Z]{2,3}_active\.htm/, @roster_files)) {
  my ($team_id) = ($file =~ m/\/([A-Z]{2,3})_active\.htm/);

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;

  # Get the remote_id's only
  $roster_active{$team_id} = ();
  foreach my $remote_id ($contents =~ m/<a href="\/player\/[^\/]+-(\d+)">/gsi) {
    $roster_active{$team_id}{$remote_id} = 1;
  }
}

# Second pass: Through the 40 man roster
foreach my $file (grep(/\/[A-Z]{2,3}_40man\.htm/, @roster_files)) {
  my ($team_id) = ($file =~ m/\/([A-Z]{2,3})_40man\.htm/);
  $output .= "\n##\n## $team_id\n##\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;

  # Break it down in to sections
  my @sect = ($contents =~ m/<table class="roster__table">(.*?)<\/table>/gsi);
  foreach my $sect (@sect) {
    my ($title, $list) = ($sect =~ m/<thead>\s+<tr>\s+<td colspan="2">(.*?)<\/td>.*?<tbody(.+)<\/tbody>/gsi);
    $output .= "\n#\n# $title\n#\n";

    # Can we contrive a position code from the position?
    my $pos_sect;
    my $title_lc = lc($title); $title_lc =~ s/s$//;
    switch ($title_lc) {
      case 'pitcher' { $pos_sect = 'P'; }
      case 'two-way player' { $pos_sect = 'P'; } # Assume Pitcher
      case 'catcher' { $pos_sect = 'C'; }
      case 'designated hitter' { $pos_sect = 'DH'; }
    }

    # Next, get the players out
    my @players = ($list =~ m/<a href="\/player\/[^"]+-(\d+)">(.*?)<\/a>\s*<span class="jersey">((?:\d*|\-))<\/span>/gsi);
    while (my @player = splice(@players, 0, 3)) {
      my $is_active = int(defined($roster_active{$team_id}{$player[0]}));
      $player[2] = '-' if !defined($player[2]) || ($player[2] eq '');
      $output .= "\n# $player[1] (Remote: $player[0], Raw Jersey: $player[2], Active: $is_active)\n";

      # Determine the player_id from a remote_id
      my $player_id = determine_player_id($player[0]);

      # If we don't have a position, try and get it from the last roster
      my $pos = $pos_sect;
      $pos = determine_previous_pos($player_id)
        if !defined($pos);

      # Roster type
      my $roster; my $status;
      if (!$is_active) {
        $roster = '40-man';
      } else {
        $roster = '25-man';
        $status = 'active';
      }

      # In some instances, we may need to load the player file:
      #  - We don't have a specific position for a non-25 man roster player (25-man rosters covered in game scripts)
      #  - Player isn't active or on DL (as player could be on a specific list)
      my $no_game_updates = (!defined($pos) && $config{'no-game-updates'});
      $output .= "# (Requesting bio: No position and no games to update roster pos)\n" if $no_game_updates;
      my $no_pos = (!defined($pos) && $roster ne '25-man');
      $output .= "# (Requesting bio: No position and not on 25 man roster; $roster)\n" if $no_pos;
      my $no_status = (!defined($status));
      $output .= "# (Requesting bio: No status)\n" if $no_status;
      my $no_jersey = ($player[2] eq '&mdash;' || $player[2] eq '-');
      $output .= "# (Requesting bio: Invalid jersey '$player[2]')\n" if $no_jersey;
      my $jrd_jersey = ($player[2] eq '42');
      $output .= "# (Requesting bio: Jackie Robinson Day)\n" if $jrd_jersey;
      my $comm_jersey = (defined($commemorating) && $player[2] eq $$commemorating{'number'});
      $output .= "# (Requesting bio: $$commemorating{'name'})\n" if $comm_jersey;
      ($pos, $status, $player[2]) = parse_player_bio($player[0], $pos, $status)
        if $no_game_updates || $no_pos || $no_status || $no_jersey || $jrd_jersey || $comm_jersey;

      # Jackie Robinson Day check, where _everyone_ active wears #42, or another bespoke jersey commemorative day
      if (($jrd_jersey && $player[2] eq '42') || ($comm_jersey && $player[2] eq $$commemorating{'number'})) {
        # Jersey has not changed, even though we parsed the player profile so we need to backport from a previous day
        $player[2] = "\{\{JERSEY_$player_id\}\}";
        push @jersey_backdate, { 'id' => $player_id, 'name' => $player[1], 'team_id' => $team_id };
      }

      # Display
      $pos = check_for_null(\$pos);
      $output .= "INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
  VALUES ('$season', '$date_roster', '$team_id', '$player_id', '$player[2]', $pos, '$status', '$roster')
ON DUPLICATE KEY UPDATE pos = VALUES(pos),
                        player_status = VALUES(player_status),
                        roster_type = VALUES(roster_type);\n";
    }
  }
}

# Third pass: converting remaining Jackie Robinson Day / commemorative numbers
if (@jersey_backdate) {
  print "\n##\n## Getting backdated jersey numbers\n##\n\n";
  # Get the IDs
  my @ids = ();
  foreach my $b (@jersey_backdate) {
    push @ids, $$b{'id'};
  }
  # Database action
  my $sql = 'SELECT player_id, SUBSTRING(MAX(CONCAT(the_date, ":", jersey)), 12) AS jersey
FROM SPORTS_MLB_TEAMS_ROSTERS
WHERE season = ?
AND   player_id IN (' . substr(',?'x@ids, 1) . ')
AND   the_date < ?
GROUP BY player_id;';
  print "# Running database query...\n";
  my $bench_start = gettimeofday();
  my $jersey_map = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, @ids, $date_roster);
  my $bench_end = gettimeofday();
  print "# - Done! Took: " . sprintf('%.03f', $bench_end - $bench_start) . " seconds\n\n";
  # Loop back to update, but also print debug
  print "# Requesting player IDs for:\n";
  my %jersey_map = ();
  foreach my $map (@$jersey_map) {
    $jersey_map{$$map{'player_id'}} = $$map{'jersey'};
    $output =~ s/\{\{JERSEY_$$map{'player_id'}\}\}/$$map{'jersey'}/g;
  }
  my %unknown_by_team = ();
  foreach my $b (@jersey_backdate) {
    # Still missing?
    if (!defined($jersey_map{$$b{'id'}})) {
      # New player with unknown jersey. Assume 100+ (and hope for future correction)
      $unknown_by_team{$$b{'team_id'}} = 100
        if !defined($unknown_by_team{$$b{'team_id'}});
      $output =~ s/\{\{JERSEY_$$b{'id'}\}\}/$unknown_by_team{$$b{'team_id'}}/g;
      $jersey_map{$$b{'id'}} = "$unknown_by_team{$$b{'team_id'}} (via fallback)";
      $unknown_by_team{$$b{'team_id'}}++;
    }
    # State
    print "# - $$b{'team_id'} $$b{'name'} (ID: $$b{'id'}) - Returned #$jersey_map{$$b{'id'}}\n";
  }
}

# Then render
print $output;

# Note: table maintenance delayed until post-depth chart processing (final stage) to prevent duplication of work

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
