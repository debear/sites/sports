#!/usr/bin/perl -w
# Parse depth charts from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing depth charts for '$season' // '$date'\n#\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

# Load the files from our tar file
my $tar = Archive::Tar->new("$config{'data_dir_depth'}.tar.gz");
my @depth_files = sort($tar->list_files());
foreach my $file (grep(/\/[A-Z]{2,3}\.json/, @depth_files)) {
  my ($team_id) = ($file =~ m/\/([A-Z]{2,3})\.json/);
  print "\n##\n## $team_id\n##\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;
  my %list = %{decode_json($contents)};

  # Loop through and group the (single) list into a per-pos list
  my %by_pos = ( );
  foreach my $depth (@{$list{'roster'}}) {
    $by_pos{$$depth{'position'}{'abbreviation'}} = [ ]
      if !defined($by_pos{$$depth{'position'}{'abbreviation'}});
    push @{$by_pos{$$depth{'position'}{'abbreviation'}}}, {
      'remote_id' => $$depth{'person'}{'id'},
      'name' => $$depth{'person'}{'fullName'},
    };
  }

  # This probably has closers ("CP") split from relievers ("P"), which we have merged into a single position list ("RP")
  @{$by_pos{'CP'}} = () if !defined($by_pos{'CP'});
  @{$by_pos{'P'}} = () if !defined($by_pos{'P'});
  @{$by_pos{'RP'}} = ( @{$by_pos{'CP'}}, @{$by_pos{'P'}} );
  delete $by_pos{'CP'};
  delete $by_pos{'P'};

  # Iterate around these positions and render
  my @pos_list = ( 'C', '1B', '2B', 'SS', '3B', 'LF', 'CF', 'RF', 'DH', 'SP', 'RP' );
  foreach my $pos (@pos_list) {
    print "\n#\n# $pos\n#\n\n";

    # Proceed only if we have a depth chart to render
    if (!defined($by_pos{$pos})) {
      print "# Skipped - no list found.\n";
      print STDERR "# Skipping '$pos' depth chart for '$team_id': no list found.\n";
      next;
    }

    # Display the players at this position
    my $depth = 1;
    foreach my $player (@{$by_pos{$pos}}) {
      $$player{'player_id'} = determine_player_id($$player{'remote_id'});
      print "# $depth: $$player{'name'} (Remote: $$player{'remote_id'}, ID: $$player{'player_id'})\n";
      print "INSERT INTO SPORTS_MLB_TEAMS_DEPTH (season, team_id, pos, depth, player_id)
  VALUES ('$season', '$team_id', '$pos', '$depth', '$$player{'player_id'}')
ON DUPLICATE KEY UPDATE player_id = VALUES(player_id);\n";
      $depth++;
    }

    # Prune
    print "\n# Prune\n";
    print "DELETE FROM SPORTS_MLB_TEAMS_DEPTH WHERE season = '$season' AND team_id = '$team_id' AND pos = '$pos' AND depth >= '$depth';\n";
  }
}

print "\n# Table maintenance\n";
print "ALTER TABLE SPORTS_MLB_TEAMS_DEPTH ORDER BY season, team_id, pos, depth;\n";

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
