#!/usr/bin/perl -w
# Parse the team rosters from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Determine the season to process
our $season;
if (defined($ARGV[0]) && $ARGV[0] =~ m/^\d{4}$/) {
  $season = $ARGV[0];
} else {
  my @now = localtime();
  $season = $now[5] + 1900;
}

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
my $methods = abs_path(__DIR__ . '/config.pl');
require $methods;

# Setup
print "#\n# Identifying roster players still missing a position for '$season'\n#\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
setup_db_queries();

# Determine the players
my $rows = $dbh->selectall_arrayref('SELECT SPORTS_MLB_TEAMS_ROSTERS.player_id, SPORTS_MLB_PLAYERS_IMPORT.remote_id, SPORTS_MLB_PLAYERS.first_name, SPORTS_MLB_PLAYERS.surname, COUNT(*) AS num_instances
FROM SPORTS_MLB_TEAMS_ROSTERS
JOIN SPORTS_MLB_PLAYERS_IMPORT
  ON (SPORTS_MLB_PLAYERS_IMPORT.player_id = SPORTS_MLB_TEAMS_ROSTERS.player_id)
JOIN SPORTS_MLB_PLAYERS
  ON (SPORTS_MLB_PLAYERS.player_id = SPORTS_MLB_PLAYERS_IMPORT.player_id)
WHERE SPORTS_MLB_TEAMS_ROSTERS.season = ?
AND   SPORTS_MLB_TEAMS_ROSTERS.pos IS NULL
GROUP BY SPORTS_MLB_TEAMS_ROSTERS.player_id
ORDER BY SPORTS_MLB_TEAMS_ROSTERS.player_id;', { Slice => {} }, $season);

# Loop through any matches
foreach my $row (@{$rows}) {
  print "# Match: $$row{'first_name'} $$row{'surname'} (ID: $$row{'player_id'}, Remote: $$row{'remote_id'}) - Instances: $$row{'num_instances'}\n";
  # Get the info (ignoring status and jersey...)
  my ($pos, $status, $jersey) = parse_player_bio($$row{'remote_id'});
  $pos = (defined($pos) ? "'$pos'" : 'NULL');
  print "UPDATE SPORTS_MLB_TEAMS_ROSTERS SET pos = $pos WHERE season = '$season' AND player_id = '$$row{'player_id'}' AND pos IS NULL;\n\n";
}

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
