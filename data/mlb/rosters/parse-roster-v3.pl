#!/usr/bin/perl -w
# Parse the team rosters from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);
use Switch;
use Time::HiRes qw(gettimeofday);

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing rosters for '$season' // '$date'\n#\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

# Load the jersey commemoration dates, to see if one applies today
my $commemorating = determine_commemorative_jersey($date);
print "\n## Commemoration Today: $$commemorating{'name'}, #$$commemorating{'number'}\n"
  if defined($commemorating);

# Load the files from our tar file
my $tar = Archive::Tar->new("$config{'data_dir_roster'}.tar.gz");
my @roster_files = sort($tar->list_files());

my $output = '';
my @jersey_backdate = ( );

# Loop through the files
foreach my $file (grep(/\/[A-Z]{2,3}\.json/, @roster_files)) {
  my ($team_id) = ($file =~ m/\/([A-Z]{2,3})\.json/);
  $output .= "\n##\n## $team_id\n##\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;
  my %roster = %{decode_json($contents)};

  # Loop through each player
  foreach my $player (@{$roster{'roster'}}) {
    $$player{'jerseyNumber'} = '-' if !defined($$player{'jerseyNumber'}) || ($$player{'jerseyNumber'} eq '');
    $output .= "\n# $$player{'person'}{'fullName'} (Remote: $$player{'person'}{'id'}, "
      . "Raw Jersey: $$player{'jerseyNumber'}, "
      . "Raw Position: $$player{'position'}{'abbreviation'}, "
      . "Status: $$player{'status'}{'code'} ($$player{'status'}{'description'})\n";

    # Determine the player_id from a remote_id
    my $player_id = determine_player_id($$player{'person'}{'id'});

    # Roster type and status mapping
    my $roster = ($$player{'status'}{'code'} eq 'A' ? '25-man' : '40-man');
    my $status;
    switch ($$player{'status'}{'code'}) {
      case 'A'   { $status = 'active'; }
      case '40M' { $status = 'active'; }
      case 'D7'  { $status = 'dl-7'; }
      case 'D10' { $status = 'dl-10'; }
      case 'D15' { $status = 'dl-15'; }
      case 'D60' { $status = 'dl-60'; }
      case 'PL'  { $status = 'paternity'; }
      case 'BRV' { $status = 'bereavement'; }
      case 'SU'  { $status = 'suspended'; }
      case 'RST' { $status = 'suspended'; }
      case 'FME' { $status = 'na'; }
      case 'NYR' { $status = 'na'; }
      case 'RM'  { $status = 'na'; }
    }
    if (!defined($status)) {
      print STDERR "Unmapped player status code '$$player{'status'}{'code'}' ($$player{'status'}{'description'})\n";
      $status = '(unknown)';
    }

    # Position checks
    my $pos = $$player{'position'}{'abbreviation'};
    if ($pos eq 'OF') {
      $output .= "# (Ambiguous position found - setting '$pos' to CF)\n";
      $pos = 'CF';
    } elsif ($pos eq 'IF') {
      $output .= "# (Ambiguous position found - setting '$pos' to SS)\n";
      $pos = 'SS';
    } elsif ($pos eq 'TWP') {
      $pos = 'TW'; # We use a shortened map
    }

    # Jersey checks
    my $jersey = $$player{'jerseyNumber'};
    if ($jersey =~ /^\-?$/) {
      $jersey = 0;
      $output .= "# (Empty jersey found - setting to '$jersey')\n";
    }
    # Jackie Robinson Day check, where _everyone_ active wears #42, or another bespoke jersey commemorative day
    if ($jersey eq '42' || (defined($commemorating) && $jersey eq $$commemorating{'number'})) {
      # Jersey has not changed, even though we parsed the player profile so we need to backport from a previous day
      $jersey = "\{\{JERSEY_$player_id\}\}";
      push @jersey_backdate, { 'id' => $player_id, 'name' => $$player{'person'}{'fullName'}, 'team_id' => $team_id };
    }

    # Build our query for display later
    $output .= "INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
  VALUES ('$season', '$date_roster', '$team_id', '$player_id', '$jersey', '$pos', '$status', '$roster')
ON DUPLICATE KEY UPDATE pos = VALUES(pos),
                        player_status = VALUES(player_status),
                        roster_type = VALUES(roster_type);\n";
  }
}

# Third pass: converting remaining Jackie Robinson Day / commemorative numbers
if (@jersey_backdate) {
  print "\n##\n## Getting backdated jersey numbers\n##\n\n";
  # Get the IDs
  my @ids = ();
  foreach my $b (@jersey_backdate) {
    push @ids, $$b{'id'};
  }
  # Database action
  my $sql = 'SELECT player_id, SUBSTRING(MAX(CONCAT(the_date, ":", jersey)), 12) AS jersey
FROM SPORTS_MLB_TEAMS_ROSTERS
WHERE season = ?
AND   player_id IN (' . substr(',?'x@ids, 1) . ')
AND   the_date < ?
GROUP BY player_id;';
  print "# Running database query...\n";
  my $bench_start = gettimeofday();
  my $jersey_map = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, @ids, $date_roster);
  my $bench_end = gettimeofday();
  print "# - Done! Took: " . sprintf('%.03f', $bench_end - $bench_start) . " seconds\n\n";
  # Loop back to update, but also print debug
  print "# Requesting player IDs for:\n";
  my %jersey_map = ();
  foreach my $map (@$jersey_map) {
    $jersey_map{$$map{'player_id'}} = $$map{'jersey'};
    $output =~ s/\{\{JERSEY_$$map{'player_id'}\}\}/$$map{'jersey'}/g;
  }
  my %unknown_by_team = ();
  foreach my $b (@jersey_backdate) {
    # Still missing?
    if (!defined($jersey_map{$$b{'id'}})) {
      # New player with unknown jersey. Assume 100+ (and hope for future correction)
      $unknown_by_team{$$b{'team_id'}} = 100
        if !defined($unknown_by_team{$$b{'team_id'}});
      $output =~ s/\{\{JERSEY_$$b{'id'}\}\}/$unknown_by_team{$$b{'team_id'}}/g;
      $jersey_map{$$b{'id'}} = "$unknown_by_team{$$b{'team_id'}} (via fallback)";
      $unknown_by_team{$$b{'team_id'}}++;
    }
    # State
    print "# - $$b{'team_id'} $$b{'name'} (ID: $$b{'id'}) - Returned #$jersey_map{$$b{'id'}}\n";
  }
}

# Finally, render
print $output;
print "\n# Table maintenance\n";
print "ALTER TABLE SPORTS_MLB_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;\n";

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
