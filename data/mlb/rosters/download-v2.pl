#!/usr/bin/perl -w
# Get current team rosters from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use File::Path;
use DBI;

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Downloading rosters for '$season' // '$date'\n#\n";

# Check we haven't already done the download
my $existing = "$config{'data_dir_roster'}.tar.gz";
if (-e $existing) {
  my @stat = stat $existing;
  if (time2date($stat[9]) eq time2date(time())) { # 9 = mtime
    print "# Skipped, files already downloaded\n";
    exit;
  }
}

# Create our data directories
mkpath($config{'data_dir_roster'})
  if ! -e $config{'data_dir_roster'};
mkpath($config{'data_dir_depth'})
  if ! -e $config{'data_dir_depth'};

# Get the teams from the database
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT TEAM.team_id, TEAM.city, TEAM.franchise
FROM SPORTS_MLB_TEAMS_GROUPINGS AS DIVN
JOIN SPORTS_MLB_TEAMS AS TEAM
  ON (TEAM.team_id = DIVN.team_id)
WHERE ? BETWEEN DIVN.season_from AND IFNULL(DIVN.season_to, 2099)
ORDER BY DIVN.team_id;';
my $sth = $dbh->prepare($sql);
$sth->execute($season);
my $teams = $sth->fetchall_arrayref;

# Disconnect from the database
undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);

# Loop through each team
while (my $team = splice(@$teams, 0, 1)) {
  my ($team_id, $team_city, $team_franchise) = @$team;
  my $team_id_num = convert_team_id_from_code($team_id);
  print "\n##\n## $team_city $team_franchise ($team_id, $team_id_num)\n##\n";
  my $url; my $local; my $ret;

  # 40-man Roster
  print "# 40-man Roster\n";
  $url = "https://statsapi.mlb.com/api/v1/teams/$team_id_num/roster/40Man";
  $local = "$config{'data_dir_roster'}/${team_id}.json.gz";
  $ret = download_file($url, $local);
  print "# - From: $url\n# - To:   $local\n#  => $ret\n";

  # Depth chart
  print "# Depth Chart\n";
  $url = "https://statsapi.mlb.com/api/v1/teams/$team_id_num/roster/depthChart";
  $local = "$config{'data_dir_depth'}/$team_id.json.gz";
  $ret = download_file($url, $local);
  print "# - From: $url\n# - To:   $local\n#  => $ret\n";
}

# Tar the files together
tar_files('roster', $config{'data_dir_roster'});
tar_files('depth chart', $config{'data_dir_depth'});

sub download_file {
  my ($url, $local) = @_;
  my $ret = download($url, $local, {'skip-today' => 1, 'gzip' => 1});
  return ($ret == 1 ? 'Done' : 'Skipped');
}

sub tar_files {
  my ($sect, $dir) = @_;
  print "\n##\n## Tarring $sect files...\n##\n";
  if (-e "$dir.tar.gz") {
    print "# (Skipped)\n";
    return;
  }
  my $tar_dir = dirname($dir);
  my $tar_loc = basename($dir);
  `tar -C$tar_dir -czf $dir.tar.gz $tar_loc`;
  rmtree($dir);
}
