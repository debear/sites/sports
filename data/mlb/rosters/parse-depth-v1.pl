#!/usr/bin/perl -w
# Parse depth charts from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);
use Switch;

# Validate the arguments
#  - there needs to be at least one: a season
#  - there could also be an additional argument though: date (absence implies "initial")
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $date) = @ARGV;
$date = 'initial' if !defined($date) || $date !~ m/^\d{4}-\d{2}-\d{2}$/;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Parsing depth charts for '$season' // '$date'\n#\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Prepare our queries to determine / set remote->imported IDs
setup_db_queries($date);

# Determine roster date
my $date_roster = determine_db_date($dbh, $season, $date);

# Load the files from our tar file
my $tar_file = $config{'data_dir_depth'} . '.tar.gz';
my $tar = Archive::Tar->new($tar_file);
my @roster_files = sort($tar->list_files());

# Then loop through the team rosters
foreach my $file (grep(/\/[A-Z]{2,3}\.htm/, @roster_files)) {
  my ($team_id) = ($file =~ m/\/([A-Z]{2,3})\.htm/);
  print "\n##\n## $team_id\n##\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;

  # Get the position blocks
  my @blocks = ($contents =~ m/(<div id="pos_.*?.*?<\/ul>\s*<\/div>)/gsi);
  foreach my $block (@blocks) {
    my ($pos, $list) = ($block =~ m/<div id="pos_([^"]+)".*?<ul[^>]*>(.*?)<\/ul>/gsi);
    $pos = uc($pos);

    # Skip the "Notes" section
    next if $pos eq 'NOTES';

    # We can only get Suspended players from the DL list
    if ($pos eq 'DL') {
      my @susp = ($list =~ m/<a.*?href="\/team\/player\.jsp\?player_id\=(\d+)">([^\n]*?)<\/a><span> Restricted List<\/span>/gsi);
      next if !@susp;
      # Process
      print "\n#\n# Misc\n#\n";
      while (my ($remote_id, $name) = splice(@susp, 0, 2)) {
        my $player_id = determine_player_id($remote_id);
        my $pos_prev = determine_previous_pos($player_id);
        my ($pos, $status, $jersey) = parse_player_bio($remote_id, $pos_prev);
        print "# Found suspended player: $name (Remote: $remote_id, ID: $player_id)\n";
        print "INSERT INTO SPORTS_MLB_TEAMS_ROSTERS (season, the_date, team_id, player_id, jersey, pos, player_status, roster_type)
  VALUES ('$season', '$date_roster', '$team_id', '$player_id', '$jersey', '$pos', '$status', NULL)
ON DUPLICATE KEY UPDATE pos = VALUES(pos),
                        player_status = VALUES(player_status),
                        roster_type = VALUES(roster_type);\n";
      }
      next;
    }

    # Pos code refinements
    $pos = 'RP' if $pos eq 'P';

    # Loop through each position
    print "\n#\n# $pos\n#\n";
    my @depth = ($list =~ m/<a.*?href="\/team\/player\.jsp\?player_id\=(\d+)">([^\n]*?)<\/a>/gsi);
    my $i = 1;
    while (my ($remote_id, $name) = splice(@depth, 0, 2)) {
      my $player_id = determine_player_id($remote_id);
      print "# $i: $name (Remote: $remote_id, ID: $player_id)\n";
      print "INSERT INTO SPORTS_MLB_TEAMS_DEPTH (season, team_id, pos, depth, player_id)
  VALUES ('$season', '$team_id', '$pos', '$i', '$player_id')
ON DUPLICATE KEY UPDATE player_id = VALUES(player_id);\n";
      $i++;
    }

    # Prune
    print "# Prune\n";
    print "DELETE FROM SPORTS_MLB_TEAMS_DEPTH WHERE season = '$season' AND team_id = '$team_id' AND pos = '$pos' AND depth >= '$i';\n";
  }
}

print "\n# Table maintenance\n";
print "ALTER TABLE SPORTS_MLB_TEAMS_ROSTERS ORDER BY season, the_date, team_id, player_id;\n";
print "ALTER TABLE SPORTS_MLB_TEAMS_DEPTH ORDER BY season, team_id, pos, depth;\n";

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
