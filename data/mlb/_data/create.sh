#!/bin/bash
# Create the directories required for a new season
pwd=`dirname $0`
cd $pwd

# Validate arg
if [ -z $1 ]; then
  echo "** No argument supplied" >&2; exit
fi
if [ ! `echo "$1" | grep -P '^\d{4}$'` ]; then
  echo "** Argument must be numeric in the format YYYY" >&2; exit
fi
if [ -e $1 ]; then
  echo "** Season already created" >&2; exit
fi

# Now create
new_season=$1

# Data we'll download
for d in depth_charts games/regular games/playoff injuries probables rosters/initial schedules/regular schedules/playoff; do
  mkdir -p $new_season/$d
done

