#!/usr/bin/perl -w
# Get the current list of injuries from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Inform user
our $season;
print "# Downloading latest injury information for $season\n";

# Check we haven't already done the download
if (-e $config{'data_file'}) {
  my @stat = stat $config{'data_file'};
  my $dl = (time2date($stat[9]) ne time2date(time())); # 9 = mtime
  if (!$dl) {
    print "# Skipped\n";
    exit;
  }
}

# Perform the download
my $url = "http://mlb.mlb.com/fantasylookup/json/named.wsfb_news_injury.bam";
my $local = $config{'data_file'};
print "# $url => $local\n";
download($url, $local, {'debug' => $config{'debug'}, 'gzip' => 1});
