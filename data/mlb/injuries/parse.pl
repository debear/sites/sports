#!/usr/bin/perl -w
# Parse the current list of injuries from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Inform user
our $season;
print "# Parsing latest injury information for $season\n\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Get some info out of the database
my %team_ids = ( );
my $sth = $dbh->prepare('SELECT TEAM.team_id, TEAM.franchise
FROM SPORTS_MLB_TEAMS_GROUPINGS AS TEAM_GROUPING
JOIN SPORTS_MLB_TEAMS AS TEAM
  ON (TEAM.team_id = TEAM_GROUPING.team_id)
WHERE ? BETWEEN TEAM_GROUPING.season_from AND IFNULL(TEAM_GROUPING.season_to, 2099);');
$sth->execute($season);
while (my $row = $sth->fetchrow_hashref) {
  $team_ids{$$row{'franchise'}} = $$row{'team_id'};
}
undef $sth if defined($sth);

# Date management
my @now = localtime;
my $cur_md = sprintf('%02d/%02d', $now[4]+1, $now[3]);
my $cur_year = $now[5] + 1900;
my $prev_year = $now[5] + 1899;

# Determine our latest update (so we can remove old updates)
print "# Determine previous updated date\n";
print "SELECT MAX(updated) INTO \@v_prev_update FROM SPORTS_MLB_PLAYERS_INJURIES WHERE season = '$season';\n\n";

# Load the file
my $injuries = load_file($config{'data_file'});
foreach my $inj (@{$$injuries{'wsfb_news_injury'}{'queryResults'}{'row'}}) {
  # Get player info
  $$inj{'team_name'} = 'Diamondbacks' if $$inj{'team_name'} eq 'D-backs';
  my $player_id = determine_player_id($$inj{'player_id'}, convert_text($$inj{'name_first'}), convert_text($$inj{'name_last'}));
  my $team_id = $team_ids{$$inj{'team_name'}};
  # Clean some inputs
  my $desc = convert_text($$inj{'injury_desc'});
  my $due_back = convert_text($$inj{'due_back'});
  my $status = convert_text($$inj{'injury_status'});
  my $update = convert_text($$inj{'injury_update'});
  # Parse the update date
  my $date_from = sprintf('%04d-%02d-%02d',
                          $$inj{'insert_ts'} gt $cur_md ? $prev_year : $cur_year,
                          substr($$inj{'insert_ts'}, 0, 2),
                          substr($$inj{'insert_ts'}, 3, 2));
  # Display
  print "# $$inj{'name_first'} $$inj{'name_last'} (Remote: $$inj{'player_id'}, Player: $player_id), $team_id $$inj{'position'}: $desc\n#  Latest: $update\n";
  print "SELECT date_from INTO \@prev_started_$$inj{'player_id'} FROM SPORTS_MLB_PLAYERS_INJURIES WHERE season = '$season' AND player_id = '$player_id' AND date_from < '$date_from' AND date_to IS NULL ORDER BY date_from LIMIT 1;\n";
  print "INSERT INTO SPORTS_MLB_PLAYERS_INJURIES (season, player_id, team_id, date_from, date_to, description, status, latest, due_back, updated)
  VALUES ('$season', '$player_id', '$team_id', IFNULL(\@prev_started_$$inj{'player_id'}, '$date_from'), NULL, '$desc', '$status', '$update', '$due_back', NOW())
ON DUPLICATE KEY UPDATE team_id = VALUES(team_id),
                        description = VALUES(description),
                        status = VALUES(status),
                        latest = VALUES(latest),
                        due_back = VALUES(due_back),
                        updated = VALUES(updated);\n\n";
}

# Update any injuries that are no longer relevant
print "# Prune old injuries\n";
print "UPDATE SPORTS_MLB_PLAYERS_INJURIES SET date_to = DATE(updated) WHERE updated <= \@v_prev_update;\n";

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect;
