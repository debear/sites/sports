#!/usr/bin/perl -w
# Perl config file

my @time = localtime;
# Process an over-riding data
if (defined($ARGV[1]) && $ARGV[1] =~ m/^\d{4}-\d{2}-\d{2}$/) {
  my ($y,$m,$d) = split('-', $ARGV[1]);
  $time[5] = int($y) - 1900;
  $time[4] = int($m) - 1;
  $time[3] = int($d);
}
our $season = $time[5] + 1900;
$config{'data_dir'} = "$config{'base_dir'}/_data/$season/injuries";
$config{'data_file'} = sprintf('%s/%04d-%02d-%02d.json.gz', $config{'data_dir'}, $season, $time[4] + 1, $time[3]);
mkdir($config{'data_dir'});

# Validate and load a file
sub load_file {
  my ($file) = @_;

  # Load the file
  open(FILE, "gzip -dc $file |");
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( );
  $contents = decode_json(Encode::encode('UTF-8', $contents));

  return $contents;
}

# Return true to pacify the compiler
1;
