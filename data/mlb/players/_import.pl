#!/usr/bin/perl -w
# Import a single player, encapsulating whole process from acquiring to parsing details

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Path;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player import script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;
my $args = join(' ', @ARGV);

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Importing player '$remote_id'\n#\n\n";

# Download
print `$config{'base_dir'}/players/download.pl $args`;

# Parse
print `$config{'base_dir'}/players/parse.pl $args`;

# Mugshot
print `$config{'base_dir'}/players/mugshot.pl $args`;
