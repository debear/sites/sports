#!/usr/bin/perl -w
# Parse an individual player's details from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;
our $first_imported = 2008; # First season we have data for, so we need to import up to but excluding this season

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

my $player = get_player_info($remote_id);

# Validate we found a player...
if (!defined($$player{'remote_id'})) {
  print STDERR "Unknown player with remote_id '$remote_id'.\n";
  exit 20;
}

$config{'player_dir'} = sprintf('%s/_data/players/%s', $config{'base_dir'}, $$player{'remote_id'});
my $header = "Parsing player '$$player{'name'}' (ID: '$$player{'player_id'}'; Remote: '$$player{'remote_id'}')";
print "#\n# $header\n#\n";
print STDERR "# $header\n";

#
# General info
#
my $profile = load_file($config{'player_dir'} . '/profile.json.gz');
my %player = %{$$profile{'people'}[0]};
print "\n#\n# General player info\n#\n";

# Main parts
my $first_name = convert_text($player{'useName'});
my $surname = convert_text($player{'useLastName'});
my $pronunciation = $player{'pronunciation'};
$pronunciation = convert_text($pronunciation) if defined($pronunciation); check_for_null(\$pronunciation);
my $stands = $player{'batSide'}{'code'};
$stands = convert_text($stands) if defined($stands); check_for_null(\$stands);
my $throws = $player{'pitchHand'}{'code'};
$throws = convert_text($throws) if defined($throws); check_for_null(\$throws);
my $weight = $player{'weight'};
$weight = convert_text($weight) if defined($weight); check_for_null(\$weight);
my $dob = $player{'birthDate'};
$dob = convert_text($dob) if defined($dob); check_for_null(\$dob);

# Height needs splitting from ft/in
my $height;
my ($height_ft, $height_in) = ($player{'height'} =~ /^(\d+)\' (\d+)\"$/);
$height = (12 * $height_ft) + (defined($height_in) ? $height_in : 0) if defined($height_ft);
check_for_null(\$height);

# Birthplace is made up of two parts
my $dob_place;
$dob_place = $player{'birthCity'} . (defined($player{'birthStateProvince'}) ? ', ' . $player{'birthStateProvince'} : '') if defined($player{'birthCity'});
$dob_place = convert_text($dob_place) if defined($dob_place); check_for_null(\$dob_place);

# Broken down parts
my $high_school;
$high_school = convert_text(join(', ', grep { /\S/ } (
  defined($player{'education'}{'highschools'}[0]{'name'}) ? $player{'education'}{'highschools'}[0]{'name'} : '',
  defined($player{'education'}{'highschools'}[0]{'city'}) ? $player{'education'}{'highschools'}[0]{'city'} : '',
  defined($player{'education'}{'highschools'}[0]{'state'}) ? $player{'education'}{'highschools'}[0]{'state'} : '',
)))
  if defined($player{'education'}{'highschools'}[0]);
$high_school = convert_text($high_school) if defined($high_school); check_for_null(\$high_school);

my $college;
$college = convert_text(join(', ', grep { /\S/ } (
  defined($player{'education'}{'colleges'}[0]{'name'}) ? $player{'education'}{'colleges'}[0]{'name'} : '',
  defined($player{'education'}{'colleges'}[0]{'state'}) ? $player{'education'}{'colleges'}[0]{'state'} : '',
)))
  if defined($player{'education'}{'colleges'}[0]);
$college = convert_text($college) if defined($college); check_for_null(\$college);

# Display
print "INSERT INTO `SPORTS_MLB_PLAYERS` (`player_id`, `first_name`, `surname`, `pronunciation`, `stands`, `throws`, `height`, `weight`, `dob`, `birthplace`, `high_school`, `college`)
VALUES ('$$player{'player_id'}', '$first_name', '$surname', $pronunciation, $stands, $throws, $height, $weight, $dob, $dob_place, $high_school, $college)
ON DUPLICATE KEY UPDATE `first_name` = IFNULL(VALUES(`first_name`), `first_name`),
                      `surname` = IFNULL(VALUES(`surname`), `surname`),
                      `stands` = IFNULL(VALUES(`stands`), `stands`),
                      `throws` = IFNULL(VALUES(`throws`), `throws`),
                      `pronunciation` = IFNULL(VALUES(`pronunciation`), `pronunciation`),
                      `height` = IFNULL(VALUES(`height`), `height`),
                      `weight` = IFNULL(VALUES(`weight`), `weight`),
                      `dob` = IFNULL(VALUES(`dob`), `dob`),
                      `birthplace` = IFNULL(VALUES(`birthplace`), `birthplace`),
                      `high_school` = IFNULL(VALUES(`high_school`), `high_school`),
                      `college` = IFNULL(VALUES(`college`), `college`);\n";

print "\n#\n# Tidy\n#\n";
print "UPDATE `SPORTS_MLB_PLAYERS_IMPORT` SET `profile_imported` = NOW() WHERE `player_id` = '$$player{'player_id'}';\n";
print "ALTER TABLE `SPORTS_MLB_PLAYERS` ORDER BY `player_id`;\n";
