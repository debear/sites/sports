#!/usr/bin/perl -w
# Player download helper methods

# Player slug manipulation
sub player_regexs {
  my ($player) = @_;
  $$player{'name'} = (defined($$player{'first_name'}) ? decode_text($$player{'first_name'} . ' ' . $$player{'surname'}) : '(Unknown)');
  $$player{'slug'} = (defined($$player{'first_name'}) ? lc($$player{'first_name'} . ' ' . $$player{'surname'}) : '-');
  $$player{'slug'} =~ s/&([a-z])[^;]*?;/$1/g;
  $$player{'slug'} = decode_entities($$player{'slug'});
  $$player{'slug'} =~ s/[\.\']//g;
  $$player{'slug'} =~ s/[^a-z\-]/-/g;
  $$player{'slug'} =~ s/\-{2,}/-/g;
}

# Profile
sub download_profile {
  my ($player) = @_;
  $url = "https://statsapi.mlb.com/api/v1/people/$$player{'remote_id'}?hydrate=education";
  print "# Profile ('$$player{'remote_id'}' => '$url'): ";
  my ($ret, $failed, $local_file) = downloader($url, 'profile', 'json');
  print "[ $ret ]\n";
  return ($ret, $failed, $local_file);
}

# Mugshot
sub download_mugshot {
  my ($player) = @_;
  $url = "https://securea.mlb.com/mlb/images/players/head_shot/$$player{'remote_id'}.jpg";
  print "# Mugshot ('$url'): ";
  my ($ret, $failed, $local_file) = downloader($url, 'mugshot', 'jpg');
  print "[ $ret ]\n";
  return ($ret, $failed, $local_file);
}

# Perform a download
sub downloader {
  our %config;
  my ($url, $type, $ext) = @_;
  my $local = "$config{'player_dir'}/$type.$ext";

  # Compression required?
  my $gzip = 0;
  if ($ext ne 'jpg') {
    $local .= '.gz';
    $gzip = 1;
  }

  # Run and return
  my $ret = download($url, $local, {'skip-today' => 1, 'if-modified-since' => 1, 'gzip' => $gzip});
  if ($ret == 1) {
    return ( 'Done', 0, $local );
  } elsif ($ret == 2) {
    return ( 'Skipped', 0, $local );
  } else {
    return ( 'Failed', 1, $local );
  }
}

# Return true to pacify the compiler
1;
