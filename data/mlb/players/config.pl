#!/usr/bin/perl -w
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use JSON;

my $methods = abs_path(__DIR__ . '/download.methods.pl');
require $methods;

# Get info for a given player (by remote_id)
sub get_player_info {
  my ($remote_id) = @_;

  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT IMPORT.remote_id, IMPORT.player_id,
                    PLAYER.first_name, PLAYER.surname,
                    SUM(ROSTER.roster_pos = "P") > 0 AS is_pitcher, IMPORT.profile_imported
             FROM SPORTS_MLB_PLAYERS_IMPORT AS IMPORT
             LEFT JOIN SPORTS_MLB_PLAYERS AS PLAYER
               ON (PLAYER.player_id = IMPORT.player_id)
             LEFT JOIN SPORTS_MLB_GAME_ROSTERS AS ROSTER
               ON (ROSTER.player_id = PLAYER.player_id)
             WHERE IMPORT.remote_id = ?
             GROUP BY IMPORT.remote_id;';
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $remote_id);
  $dbh->disconnect if defined($dbh);

  # Reg-ex versions
  player_regexs($$ret[0])
    if defined($$ret[0]);

  return $$ret[0];
}

# Validate and load a file
sub load_file {
  my ($file, $args) = @_;

  # Validate
  if (! -e $file) {
    print STDERR "File '$file' does not exist, unable to continue.\n";
    exit 20;
  } elsif (-z $file) {
    print STDERR "File '$file' empty, cannot ungzip correctly.\n";
    exit 20;
  }

  # Load the file
  open(FILE, "gzip -dc $file |");
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( );
  $contents = Encode::decode('UTF-8', $contents);

  # Extension implies method of decoding
  if ($file =~ m/\.json/) {
    $contents = decode_json(Encode::encode('UTF-8', $contents));
  }

  return $contents;
}

# Convert undefined values to 0
sub convert_undef_zero {
  my ($txt) = @_;
  return (defined($txt) && $txt ne '' ? $txt : 0);
}

# Return true to pacify the compiler
1;
