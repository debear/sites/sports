#!/usr/bin/perl -w
# Get info on a single player from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Path;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player download script.\n";
  exit 98;
}

# Declare variables and check the flags passed in
our ($remote_id) = @ARGV;
our $bio_only = grep(/\-\-bio\-only/, @ARGV);
our $mugshot_only = grep(/\-\-mugshot\-only/, @ARGV);
our $force = grep(/\-\-force/, @ARGV);
# Visual version
my @flags;
push @flags, 'Bio Only' if $bio_only;
push @flags, 'Mugshot Only' if $mugshot_only;
push @flags, 'Force' if $force;
# Note: --*-only implies --force
$force = 1 if $mugshot_only || $bio_only;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/download.methods.pl');
require $config;

# Guesstimate the season we require
my @now = localtime();
my $season = 1900 + $now[5];

# Determine who the player is
my $player = get_player_info($remote_id);

# Validate we found a player...
if (!defined($$player{'remote_id'})) {
  print STDERR "Unknown player with remote_id '$remote_id'.\n";
  exit 98;
} elsif (defined($$player{'profile_imported'}) && !$force) {
  print "# Skipping download, player '$$player{'name'}' ('$remote_id') previously imported, and script not run in --force mode.\n\n";
  exit;
}

$config{'player_dir'} = sprintf('%s/_data/players/%s', $config{'base_dir'}, $$player{'remote_id'});
print "#\n# Downloading player '$$player{'name'}' (ID: '$$player{'player_id'}'; Remote: '$$player{'remote_id'}')\n#\n";
print "# Flags: " . join(', ', @flags) . "\n" if @flags;
print "\n";

# If folder already exists (and we're not in force mode), do not continue as it imples we've already downloaded the info
if (-e "$config{'player_dir'}/batting-regular.json.gz" && !$force) {
  print "# Skipping, previously downloaded...\n\n";
  exit;
}

# Create our data directory
mkpath($config{'player_dir'})
  if ! -e $config{'player_dir'};

my $url; my $ret; my $failed; my $local_file;

# Player details, but only if we're not just after the mugshot
if (!$mugshot_only) {
  # Download the main profile
  ($ret, $failed, $local_file) = download_profile($player);
  my $profile_file = $local_file;
  if ($failed) {
    print STDERR "Failed to download the profile, unable to proceed...\n";
    exit 20;
  }

  # As we have enough historical data, it is fair to assume we don't need these (now the links have stopped working!)
  if (0 && !$bio_only) {
    # Season-by-season stats (which are in separate files per category)
    my @cats = ( { 'local' => 'batting',  'remote' => 'hitting' },
                 { 'local' => 'fielding', 'remote' => 'fielding', 'extra' => '&position=%271%27&position=%272%27&position=%273%27&position=%274%27&position=%275%27&position=%276%27&position=%277%27&position=%278%27&position=%279%27&position=%27O%27' },
                 { 'local' => 'pitching', 'remote' => 'pitching', 'p_req' => 1 } );
    my @game_types = ( { 'local' => 'regular', 'remote' => 'game_type=%27R%27', },
                       { 'local' => 'playoff', 'remote' => 'game_type=%27F%27&game_type=%27D%27&game_type=%27L%27&game_type=%27W%27&game_type=%27C%27', } );
    foreach my $cat (@cats) {
      # Determine position eligibility?
      next if defined($$cat{'p_req'}) && $$cat{'p_req'} && !$$player{'is_pitcher'};

      # Loop through, downloading for each type
      my $url_base = "https://lookup-service-prod.mlb.com/json/named.sport_$$cat{'remote'}_composed.bam?__TYPE__&league_list_id=%27mlb%27&sort_by=%27season_asc%27&player_id=$$player{'remote_id'}&sport_$$cat{'remote'}_composed.season=$season";
      foreach my $game_type (@game_types) {
        $url = $url_base;
        $url =~ s/__TYPE__/$$game_type{'remote'}/;
        $url .= $$game_type{'extra'} if defined($$game_type{'extra'});
        print '# ' . ucfirst($$cat{'local'}) . ' - ' . ucfirst($$game_type{'local'}) . " ('$url'): ";
        my ($ret, $failed, $local_file) = downloader($url, $$cat{'local'} . '-' . $$game_type{'local'}, 'json');
        print "[ $ret ]\n";
      }
    }
  }
}

# End with the mugshot
($ret, $failed, $local_file) = download_mugshot($player)
  if !$bio_only;
