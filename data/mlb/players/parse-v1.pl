#!/usr/bin/perl -w
# Parse an individual player's details from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Declare variables
our ($remote_id) = @ARGV;
our $first_imported = 2008; # First season we have data for, so we need to import up to but excluding this season

# Get base config
our %config; our $season;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
$config{'disp'} = -1; # Section to display: -1 = All, 0 = None, 1-X = appropriate section;

my $player = get_player_info($remote_id);

# Validate we found a player...
if (!defined($$player{'remote_id'})) {
  print STDERR "Unknown player with remote_id '$remote_id'.\n";
  exit 20;
}

$config{'player_dir'} = sprintf('%s/_data/players/%s', $config{'base_dir'}, $$player{'remote_id'});
my $header = "Parsing player '$$player{'name'}' (ID: '$$player{'player_id'}'; Remote: '$$player{'remote_id'}')";
print "#\n# $header\n#\n";
print STDERR "# $header\n";

#
# General info
#
my $profile = load_file($config{'player_dir'} . '/profile.htm.gz');
if (check_disp(1, $config{'disp'})) {
  print "\n#\n# General player info\n#\n";

  # Name
  my ($first_last_name) = ($profile =~ m/<div class="player-vitals-mini-name">(.*?)<\/div>/gsi);
  my ($full_name) = ($profile =~ m/<li class="full-name"><span class="label">Fullname:<\/span> (.*?)<\/li>/gsi);
  # If no full name defined, re-use they name we do have
  $full_name = $first_last_name
    if !defined($full_name);
  # If known by middle name, strip the start (as that confuses the parser)
  $full_name = $first_last_name
    if $full_name =~ /^.+ $first_last_name$/;
  my $first_name; my $surname;
  my @full = split(' ', $full_name);
  my @first_last = split(' ', $first_last_name);
  # No middle name?
  if (($full_name eq $first_last_name) || (@full == @first_last)) {
    my @split = split(' ', $first_last_name);
    $first_name = shift @split;
    $surname = join(' ', @split);
  } else {
    # Parse out the middle name(s) (as our separator)
    my $i;
    my $idiff = (@full - @first_last);
    my $split = undef;
    for ($i = (@full - 1); !defined($split) && $i >= 0; $i--) {
      $split = $i + 1
        if ($full[$i] ne $first_last[$i - $idiff]);
    }
    # Now we know where to split, do so
    $surname = join(' ', splice(@full, $split));
    $first_name = $first_last_name;
    $first_name =~ s/ $surname//;
  }
  $first_name = convert_text($first_name);
  $surname = convert_text($surname);

  # Pronunciation
  my ($pronunciation) = ($profile =~ m/<li[^>]*><span[^>]*>Pronunciation:<\/span> (.*?)<\/li>/gsi);
  $pronunciation = convert_text($pronunciation) if defined($pronunciation);
  check_for_null(\$pronunciation);

  # Stands / Throws
  my ($stands, $throws) = ($profile =~ m/<li>B\/T: (.*?)\/(.*?)<\/li>/gsi);
  check_for_null(\$stands);
  check_for_null(\$throws);
  # Height / Weight
  my ($height_ft, $height_in, $weight) = ($profile =~ m/<li class="player-header--vitals-height">(\d+)&#x27; ?(\d*)\/(\d+)<\/li>/gsi);
  my $height = undef;
  $height = (12 * $height_ft) + (defined($height_in) ? $height_in : 0) if defined($height_ft);
  check_for_null(\$height);
  check_for_null(\$weight);

  # Birth: Date and Place
  my ($dob_m, $dob_d, $dob_y, $dob_place) = ($profile =~ m/<li><span class="label">Born:<\/span> (\d+)\/(\d+)\/(\d+)\s*(?:in )?(.*?)<\/li>/gsi);
  my $dob = undef;
  $dob = sprintf('%04d-%02d-%02d', $dob_y, $dob_m, $dob_d)
    if defined($dob_y) && defined($dob_m) && defined($dob_d);
  check_for_null(\$dob);
  if (defined($dob_place)) {
    trim(\$dob_place);
    $dob_place = convert_text($dob_place);
  }
  check_for_null(\$dob_place);

  # High School / College
  my ($high_school) = ($profile =~ m/<li><span class="label">High School:<\/span> (.*?)<\/li>/gsi);
  $high_school = convert_text($high_school) if defined($high_school);
  check_for_null(\$high_school);
  my ($college) = ($profile =~ m/<li><span class="label">College:<\/span> (.*?)<\/li>/gsi);
  $college = convert_text($college) if defined($college);
  check_for_null(\$college);

  # Display
  print "INSERT INTO `SPORTS_MLB_PLAYERS` (`player_id`, `first_name`, `surname`, `pronunciation`, `stands`, `throws`, `height`, `weight`, `dob`, `birthplace`, `high_school`, `college`)
  VALUES ('$$player{'player_id'}', '$first_name', '$surname', $pronunciation, $stands, $throws, $height, $weight, $dob, $dob_place, $high_school, $college)
ON DUPLICATE KEY UPDATE `first_name` = IFNULL(VALUES(`first_name`), `first_name`),
                        `surname` = IFNULL(VALUES(`surname`), `surname`),
                        `stands` = IFNULL(VALUES(`stands`), `stands`),
                        `throws` = IFNULL(VALUES(`throws`), `throws`),
                        `pronunciation` = IFNULL(VALUES(`pronunciation`), `pronunciation`),
                        `height` = IFNULL(VALUES(`height`), `height`),
                        `weight` = IFNULL(VALUES(`weight`), `weight`),
                        `dob` = IFNULL(VALUES(`dob`), `dob`),
                        `birthplace` = IFNULL(VALUES(`birthplace`), `birthplace`),
                        `high_school` = IFNULL(VALUES(`high_school`), `high_school`),
                        `college` = IFNULL(VALUES(`college`), `college`);\n";
}

#
# Batting
# - No longer parsed
#
if (0 && check_disp(2, $config{'disp'})) {
  print "\n#\n# Batting history (pre-$first_imported)\n#\n";
  print "DELETE FROM `SPORTS_MLB_PLAYERS_SEASON_BATTING` WHERE `season` < '$first_imported' AND `season_type` IN ('regular','playoff') AND `player_id` = '$$player{'player_id'}';\n";

  foreach my $season_type ('regular', 'playoff') {
    print "\n# " . ucfirst($season_type) . "\n";
    my $history = load_file($config{'player_dir'} . "/batting-$season_type.json.gz");
    # Team-by-team
    my $team_rows = $$history{'sport_hitting_composed'}{'sport_hitting_tm'}{'queryResults'}{'row'};
    $team_rows = [ $team_rows ] if ref($team_rows) eq 'HASH';
    # Grouped
    my $grouped_rows_raw = $$history{'sport_hitting_composed'}{'sport_hitting_agg'}{'queryResults'}{'row'};
    $grouped_rows_raw = [ $grouped_rows_raw ] if ref($grouped_rows_raw) eq 'HASH';
    my $grouped_rows = [ ]; my %grouped_seasons = ( );
    foreach my $row (@{$grouped_rows_raw}) {
      if ($$row{'team_count'} > 1) {
        $$row{'team_code'} = '_ML';
        $$row{'is_totals'} = 1;
        $grouped_seasons{$$row{'season'}} = 1;
        push @{$grouped_rows}, $row;
      }
    }
    # Process
    foreach my $arr ($team_rows, $grouped_rows) {
      foreach my $season (@{$arr}) {
        # Skip seasons we have game data for
        next if $$season{'season'} >= $first_imported;
        # Output our values
        my $team_id = (defined($$season{'team_code'}) ? $$season{'team_code'} : convert_team_id($$season{'team_abbrev'}));
        my $is_totals = int(defined($$season{'is_totals'}) || !defined($grouped_seasons{$$season{'season'}}));
        my $ab = convert_undef_zero($$season{'ab'}); check_for_null(\$ab);
        my $pa = convert_undef_zero($$season{'tpa'}); check_for_null(\$pa);
        my $h = convert_undef_zero($$season{'h'}); check_for_null(\$h);
        my $sgl = (defined($$season{'h'}) ? $$season{'h'} - convert_undef_zero($$season{'d'}) - convert_undef_zero($$season{'t'}) - convert_undef_zero($$season{'hr'}) : 0); check_for_null(\$sgl);
        my $dbl = convert_undef_zero($$season{'d'}); check_for_null(\$dbl);
        my $trp = convert_undef_zero($$season{'t'}); check_for_null(\$trp);
        my $bb = convert_undef_zero($$season{'bb'}); check_for_null(\$bb);
        my $ibb = convert_undef_zero($$season{'ibb'}); check_for_null(\$ibb);
        my $r = convert_undef_zero($$season{'r'}); check_for_null(\$r);
        my $hr = convert_undef_zero($$season{'hr'}); check_for_null(\$hr);
        my $rbi = convert_undef_zero($$season{'rbi'}); check_for_null(\$rbi);
        my $sb = convert_undef_zero($$season{'sb'}); check_for_null(\$sb);
        my $cs = convert_undef_zero($$season{'cs'}); check_for_null(\$cs);
        my $avg = $$season{'avg'}; check_for_null(\$avg);
        my $obp = $$season{'obp'}; check_for_null(\$obp);
        my $tb = convert_undef_zero($$season{'tb'}); check_for_null(\$tb);
        my $slg = $$season{'slg'}; check_for_null(\$slg);
        my $ops = $$season{'ops'}; check_for_null(\$ops);
        my $sac_fly = convert_undef_zero($$season{'sf'}); check_for_null(\$sac_fly);
        my $sac_hit = convert_undef_zero($$season{'sac'}); check_for_null(\$sac_hit);
        my $hbp = convert_undef_zero($$season{'hbp'}); check_for_null(\$hbp);
        my $k = convert_undef_zero($$season{'so'}); check_for_null(\$k);
        my $go = convert_undef_zero($$season{'go'}); check_for_null(\$go);
        my $fo = convert_undef_zero($$season{'ao'}); check_for_null(\$fo);
        my $lob = convert_undef_zero($$season{'lob'}); check_for_null(\$lob);
        print "INSERT INTO `SPORTS_MLB_PLAYERS_SEASON_BATTING` (`season`, `season_type`, `player_id`, `team_id`, `is_totals`, `ab`, `pa`, `h`, `1b`, `2b`, `3b`, `bb`, `ibb`, `r`, `hr`, `rbi`, `sb`, `cs`, `avg`, `obp`, `tb`, `slg`, `ops`, `sac_fly`, `sac_hit`, `hbp`, `k`, `go`, `fo`, `lob`)
  VALUES ('$$season{'season'}', '$season_type', '$$player{'player_id'}', '$team_id', '$is_totals', $ab, $pa, $h, $sgl, $dbl, $trp, $bb, $ibb, $r, $hr, $rbi, $sb, $cs, $avg, $obp, $tb, $slg, $ops, $sac_fly, $sac_hit, $hbp, $k, $go, $fo, $lob);\n";
      }
    }
  }
}

#
# Fielding
#  Note: unlike batting and pitching, these stats are grouped by season, team AND position, so unlink the position info
# - No longer parsed
#
if (0 && check_disp(3, $config{'disp'})) {
  print "\n#\n# Fielding history (pre-$first_imported)\n#\n";
  print "DELETE FROM `SPORTS_MLB_PLAYERS_SEASON_FIELDING` WHERE `season` < '$first_imported' AND `season_type` IN ('regular','playoff') AND `player_id` = '$$player{'player_id'}';\n";

  foreach my $season_type ('regular', 'playoff') {
    print "\n# " . ucfirst($season_type) . "\n";
    my $history = load_file($config{'player_dir'} . "/fielding-$season_type.json.gz");
    # Team-by-team and grouped, using the same iteration
    my $team_rows_raw = $$history{'sport_fielding_composed'}{'sport_fielding_tm'}{'queryResults'}{'row'};
    $team_rows_raw = [ $team_rows_raw ] if ref($team_rows_raw) eq 'HASH';
    my $team_rows = [ ]; my %team_hashes = ( );
    my $grouped_rows_raw = [ ]; my %grouped_hashes = ( );
    # We'll only deal with counting stats here, the %age will be calculated on-the-fly
    foreach my $row (@{$team_rows_raw}) {
      # Team-by-team stats
      my $ref = $$row{'season'} . '//' . $$row{'team_abbrev'};
      $team_hashes{$ref} = { 'season' => $$row{'season'}, 'team_abbrev' => $$row{'team_abbrev'} }
        if !defined($team_hashes{$ref});
      $team_hashes{$ref}{'tc'} = convert_undef_zero($team_hashes{$ref}{'tc'}) + $$row{'tc'}
        if defined($$row{'tc'}) && $$row{'tc'} ne '';
      $team_hashes{$ref}{'po'} = convert_undef_zero($team_hashes{$ref}{'po'}) + $$row{'po'}
        if defined($$row{'po'}) && $$row{'po'} ne '';
      $team_hashes{$ref}{'a'} = convert_undef_zero($team_hashes{$ref}{'a'}) + $$row{'a'}
        if defined($$row{'a'}) && $$row{'a'} ne '';
      $team_hashes{$ref}{'e'} = convert_undef_zero($team_hashes{$ref}{'e'}) + $$row{'e'}
        if defined($$row{'e'}) && $$row{'e'} ne '';
      # Grouped stats
      $ref = $$row{'season'};
      $grouped_hashes{$ref} = { 'season' => $$row{'season'}, 'team_list' => { } }
        if !defined($grouped_hashes{$ref});
      $grouped_hashes{$ref}{'team_list'}{$$row{'team_abbrev'}} = 1;
      $grouped_hashes{$ref}{'tc'} = convert_undef_zero($grouped_hashes{$ref}{'tc'}) + $$row{'tc'}
        if defined($$row{'tc'}) && $$row{'tc'} ne '';
      $grouped_hashes{$ref}{'po'} = convert_undef_zero($grouped_hashes{$ref}{'po'}) + $$row{'po'}
        if defined($$row{'po'}) && $$row{'po'} ne '';
      $grouped_hashes{$ref}{'a'} = convert_undef_zero($grouped_hashes{$ref}{'a'}) + $$row{'a'}
        if defined($$row{'a'}) && $$row{'a'} ne '';
      $grouped_hashes{$ref}{'e'} = convert_undef_zero($grouped_hashes{$ref}{'e'}) + $$row{'e'}
        if defined($$row{'e'}) && $$row{'e'} ne '';
    }
    # Convert hash to (ordered) array
    foreach my $key (sort keys %team_hashes) {
      push @{$team_rows}, $team_hashes{$key};
    }
    # Grouped
    my $grouped_rows = [ ]; my %grouped_seasons = ( );
    foreach my $key (sort keys %grouped_hashes) {
      my $row = $grouped_hashes{$key};
      if (scalar(keys(%{$$row{'team_list'}})) > 1) {
        $$row{'team_code'} = '_ML';
        $$row{'is_totals'} = 1;
        $grouped_seasons{$$row{'season'}} = 1;
        push @{$grouped_rows}, $row;
      }
    }
    # Process
    foreach my $arr ($team_rows, $grouped_rows) {
      foreach my $season (@{$arr}) {
        # Skip seasons we have game data for
        next if $$season{'season'} >= $first_imported;
        # Output our values
        my $team_id = (defined($$season{'team_code'}) ? $$season{'team_code'} : convert_team_id($$season{'team_abbrev'}));
        my $is_totals = int(defined($$season{'is_totals'}) || !defined($grouped_seasons{$$season{'season'}}));
        my $tc = convert_undef_zero($$season{'tc'}); check_for_null(\$tc);
        my $po = convert_undef_zero($$season{'po'}); check_for_null(\$po);
        my $a = convert_undef_zero($$season{'a'}); check_for_null(\$a);
        my $e = convert_undef_zero($$season{'e'}); check_for_null(\$e);
        my $pct = ((convert_undef_zero($$season{'po'}) || convert_undef_zero($$season{'a'}) || convert_undef_zero($$season{'e'})) ? sprintf('%.04f', (convert_undef_zero($$season{'po'}) + convert_undef_zero($$season{'a'})) / (convert_undef_zero($$season{'po'}) + convert_undef_zero($$season{'a'}) + convert_undef_zero($$season{'e'}))) : undef); check_for_null(\$pct);
        print "INSERT INTO `SPORTS_MLB_PLAYERS_SEASON_FIELDING` (`season`, `season_type`, `player_id`, `team_id`, `is_totals`, `tc`, `po`, `a`, `e`, `pct`)
  VALUES ('$$season{'season'}', '$season_type', '$$player{'player_id'}', '$team_id', '$is_totals', $tc, $po, $a, $e, $pct);\n";
      }
    }
  }
}

#
# Pitching
# - No longer parsed
#
if (0 && check_disp(4, $config{'disp'})) {
  print "\n#\n# Pitching history (pre-$first_imported)\n#\n";
  print "DELETE FROM `SPORTS_MLB_PLAYERS_SEASON_PITCHING` WHERE `season` < '$first_imported' AND `season_type` IN ('regular','playoff') AND `player_id` = '$$player{'player_id'}';\n";

  foreach my $season_type ('regular', 'playoff') {
    print "\n# " . ucfirst($season_type) . "\n";
    my $file = $config{'player_dir'} . "/pitching-$season_type.json.gz";
    # Skip if no file found (probably parsing a non-pitcher)
    if (! -e $file) {
      print "# Skipping pitching-$season_type, no file found. Probably not a pitcher?\n";
      next;
    }
    my $history = load_file($file);
    # Team-by-team
    my $team_rows = $$history{'sport_pitching_composed'}{'sport_pitching_tm'}{'queryResults'}{'row'};
    $team_rows = [ $team_rows ] if ref($team_rows) eq 'HASH';
    # Grouped
    my $grouped_rows_raw = $$history{'sport_pitching_composed'}{'sport_pitching_agg'}{'queryResults'}{'row'};
    $grouped_rows_raw = [ $grouped_rows_raw ] if ref($grouped_rows_raw) eq 'HASH';
    my $grouped_rows = [ ]; my %grouped_seasons = ( );
    foreach my $row (@{$grouped_rows_raw}) {
      if ($$row{'team_count'} > 1) {
        $$row{'team_code'} = '_ML';
        $$row{'is_totals'} = 1;
        $grouped_seasons{$$row{'season'}} = 1;
        push @{$grouped_rows}, $row;
      }
    }
    # Process
    foreach my $arr ($team_rows, $grouped_rows) {
      foreach my $season (@{$arr}) {
        # Skip seasons we have game data for
        next if $$season{'season'} >= $first_imported;
        # Output our values
        my $team_id = (defined($$season{'team_code'}) ? $$season{'team_code'} : convert_team_id($$season{'team_abbrev'}));
        my $is_totals = int(defined($$season{'is_totals'}) || !defined($grouped_seasons{$$season{'season'}}));
        my $ip = convert_undef_zero($$season{'ip'}); check_for_null(\$ip);
        my $bf = convert_undef_zero($$season{'tbf'}); check_for_null(\$bf);
        my $pt = convert_undef_zero($$season{'np'}); check_for_null(\$pt);
        my $k = convert_undef_zero($$season{'so'}); check_for_null(\$k);
        my $h = convert_undef_zero($$season{'h'}); check_for_null(\$h);
        my $hr = convert_undef_zero($$season{'hr'}); check_for_null(\$hr);
        my $bb = convert_undef_zero($$season{'bb'}); check_for_null(\$bb);
        my $ibb = convert_undef_zero($$season{'ibb'}); check_for_null(\$ibb);
        my $wp = convert_undef_zero($$season{'wp'}); check_for_null(\$wp);
        my $go = convert_undef_zero($$season{'go'}); check_for_null(\$go);
        my $fo = convert_undef_zero($$season{'ao'}); check_for_null(\$fo);
        my $r = convert_undef_zero($$season{'r'}); check_for_null(\$r);
        my $er = convert_undef_zero($$season{'er'}); check_for_null(\$er);
        my $ir = convert_undef_zero($$season{'ir'}); check_for_null(\$ir);
        my $ira = convert_undef_zero($$season{'irs'}); check_for_null(\$ira);
        my $bk = convert_undef_zero($$season{'bk'}); check_for_null(\$bk);
        my $w = convert_undef_zero($$season{'w'}); check_for_null(\$w);
        my $l = convert_undef_zero($$season{'l'}); check_for_null(\$l);
        my $sv = convert_undef_zero($$season{'sv'}); check_for_null(\$sv);
        my $hld = convert_undef_zero($$season{'hld'}); check_for_null(\$hld);
        my $cg = convert_undef_zero($$season{'cg'}); check_for_null(\$cg);
        my $sho = convert_undef_zero($$season{'sho'}); check_for_null(\$sho);
        my $qs = convert_undef_zero($$season{'qs'}); check_for_null(\$qs);
        # Unobtainable for now
        my $game_score = undef; check_for_null(\$game_score);
        # Calculate outs from IP
        my $out = undef; my $out_raw = 0;
        if (defined($$season{'ip'})) {
          my ($a, $b) = split('\.', $$season{'ip'});
          $out = $out_raw = (3 * $a) + $b;
        }
        $out = convert_undef_zero($out); check_for_null(\$out);
        # Calculate Balls / Strikes from
        my $s = undef; my $b = undef;
        $$season{'spct'} = '0.0' if defined($$season{'spct'}) && $$season{'spct'} eq '-.-';
        if (convert_undef_zero($$season{'np'}) && defined($$season{'spct'})) {
          $s = int(($$season{'np'} * ($$season{'spct'} / 100)) + 0.5);
          $b = $$season{'np'} - $s;
        }
        $b = convert_undef_zero($b); check_for_null(\$b);
        $s = convert_undef_zero($s); check_for_null(\$s);
        # Simpler Calcs
        my $bs = (convert_undef_zero($$season{'svo'}) ? $$season{'svo'} - convert_undef_zero($$season{'sv'}) : 0); check_for_null(\$bs);
        my $ur = (convert_undef_zero($$season{'r'}) ? $$season{'r'} - convert_undef_zero($$season{'er'}) : undef); my $ur_raw = $ur; $ur = convert_undef_zero($ur); check_for_null(\$ur);
        # Calced ratios
        my $ra = ($out_raw ? sprintf('%.03f', (convert_undef_zero($$season{'r'}) / $out_raw) * 27) : undef); check_for_null(\$ra);
        my $era = ($out_raw ? sprintf('%.03f', (convert_undef_zero($$season{'er'}) / $out_raw) * 27) : undef); check_for_null(\$era);
        my $ura = ($out_raw ? sprintf('%.03f', (convert_undef_zero($ur_raw) / $out_raw) * 27) : undef); check_for_null(\$ura);
        my $whip = ($out_raw ? sprintf('%.03f', (convert_undef_zero($$season{'bb'}) + convert_undef_zero($$season{'h'})) / ($out_raw / 3)) : undef); check_for_null(\$whip);
        my $go_fo = (convert_undef_zero($$season{'ao'}) ? sprintf('%.04f', convert_undef_zero($$season{'go'}) / $$season{'ao'}) : undef); check_for_null(\$go_fo);

        print "INSERT INTO `SPORTS_MLB_PLAYERS_SEASON_PITCHING` (`season`, `season_type`, `player_id`, `team_id`, `is_totals`, `ip`, `out`, `bf`, `pt`, `b`, `s`, `k`, `h`, `hr`, `bb`, `ibb`, `wp`, `go`, `fo`, `go_fo`, `r`, `ra`, `er`, `era`, `ur`, `ura`, `ir`, `ira`, `whip`, `bk`, `w`, `l`, `sv`, `hld`, `bs`, `cg`, `sho`, `qs`, `game_score`)
  VALUES ('$$season{'season'}', '$season_type', '$$player{'player_id'}', '$team_id', '$is_totals', $ip, $out, $bf, $pt, $b, $s, $k, $h, $hr, $bb, $ibb, $wp, $go, $fo, $go_fo, $r, $ra, $er, $era, $ur, $ura, $ir, $ira, $whip, $bk, $w, $l, $sv, $hld, $bs, $cg, $sho, $qs, $game_score);\n";
      }
    }
  }
}

print "\n#\n# Tidy\n#\n";
print "UPDATE `SPORTS_MLB_PLAYERS_IMPORT` SET `profile_imported` = NOW() WHERE `player_id` = '$$player{'player_id'}';\n";
print "ALTER TABLE `SPORTS_MLB_PLAYERS` ORDER BY `player_id`;\n";
