#!/usr/bin/perl -w
# Get info on a single player from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Path;

# Validate the arguments
#  - there needs to be at least one: a remote_id
if (@ARGV < 1) {
  print STDERR "Insufficient arguments to the player download script.\n";
  exit 98;
}

# Declare variables and check the flags passed in
our ($remote_id) = @ARGV;
our $bio_only = grep(/\-\-bio\-only/, @ARGV);
our $mugshot_only = grep(/\-\-mugshot\-only/, @ARGV);
our $force = grep(/\-\-force/, @ARGV);
# Visual version
my @flags;
push @flags, 'Bio Only' if $bio_only;
push @flags, 'Mugshot Only' if $mugshot_only;
push @flags, 'Force' if $force;
# Note: --*-only implies --force
$force = 1 if $mugshot_only || $bio_only;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/download.methods.pl');
require $config;

# Guesstimate the season we require
my @now = localtime();
my $season = 1900 + $now[5];

# Determine who the player is
my $player = get_player_info($remote_id);

# Validate we found a player...
if (!defined($$player{'remote_id'})) {
  print STDERR "Unknown player with remote_id '$remote_id'.\n";
  exit 98;
} elsif (defined($$player{'profile_imported'}) && !$force) {
  print "# Skipping download, player '$$player{'name'}' ('$remote_id') previously imported, and script not run in --force mode.\n\n";
  exit;
}

$config{'player_dir'} = sprintf('%s/_data/players/%s', $config{'base_dir'}, $$player{'remote_id'});
print "#\n# Downloading player '$$player{'name'}' (ID: '$$player{'player_id'}'; Remote: '$$player{'remote_id'}')\n#\n";
print "# Flags: " . join(', ', @flags) . "\n" if @flags;
print "\n";

# If folder already exists (and we're not in force mode), do not continue as it imples we've already downloaded the info
if (-e "$config{'player_dir'}/profile.json.gz" && !$force) {
  print "# Skipping, previously downloaded...\n\n";
  exit;
}

# Create our data directory
mkpath($config{'player_dir'})
  if ! -e $config{'player_dir'};

my $url; my $ret; my $failed; my $local_file;

# Player details, but only if we're not just after the mugshot
if (!$mugshot_only) {
  # Download the main profile
  ($ret, $failed, $local_file) = download_profile($player);
  my $profile_file = $local_file;
  if ($failed) {
    print STDERR "Failed to download the profile, unable to proceed...\n";
    exit 20;
  }
}

# End with the mugshot
($ret, $failed, $local_file) = download_mugshot($player)
  if !$bio_only;
