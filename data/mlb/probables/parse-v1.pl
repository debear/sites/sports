#!/usr/bin/perl -w
# Parse the list of probable starters for the next few days from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $start_date, $future_days) = parse_args();
$config{'base_dir'} = get_base_dir();
my $tar_file = "$config{'base_dir'}.tar.gz";

# Determine the dates to loop through
my @dates = processing_dates();

# Validate: no dates => no need to run...
if (!@dates) {
  print "# No dates to process based on argument '$start_date', silently quitting.\n";
  exit;
}

# Open the file
my $tar = Archive::Tar->new($tar_file);
my @files = grep(/\.gz$/, sort($tar->list_files()));

# Inform user
my ($dir_name) = ($files[0] =~ m/^(.+)\d{4}/);
my $date_from = substr($files[0], length($dir_name), 10);
my $date_to = substr($files[-1], length($dir_name), 10);
print "# Parsing Probable Starters for $season, from $date_from to $date_to (" . @files . " day" . (@files == 1 ? '' : 's') . ")\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Loop through the files
foreach my $file (@files) {
  # Display
  my $date = substr($file, length($dir_name), 10);
  print "\n#\n# $date\n#\n";
  print "DELETE FROM SPORTS_MLB_SCHEDULE_PROBABLES WHERE season = '$season' AND game_date = '$date';\n\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;

  # Find the probables from this file
  my @probables = ($contents =~ m/<h5><a href="[^"]+\?player_id=(\d+)">(.*?)<\/a>.*?<\/h5>.*?<a href="[^"]+\/probable_pitchers\/index\.jsp\?c_id=([^"]+)">.*?<p>(.*?)<\/p>/gsi);
  while (my ($remote_id, $name, $alt_team_id, $notes) = splice(@probables, 0, 4)) {
    my $team_id = convert_team_remote_alt_reverse($alt_team_id);
    my $player_id = determine_player_id($remote_id);
    $notes = convert_text($notes) if $notes !~ /&(?:#|amp)/;
    check_for_null(\$notes);
    print "# $team_id ($alt_team_id): $name (Remote: $remote_id, ID: $player_id)\n";
    print "INSERT INTO SPORTS_MLB_SCHEDULE_PROBABLES (season, game_type, game_id, game_date, team_id, player_id, opp_team_id, venue, notes)
  SELECT SCHED.season, SCHED.game_type, SCHED.game_id, IFNULL(SCHED.started_date, SCHED.game_date) AS game_date, '$team_id' AS team_id,
         '$player_id',
         IF(SCHED.home = '$team_id', SCHED.visitor, SCHED.home) AS opp_team_id,
         IF(SCHED.home = '$team_id', 'home', 'away') AS venue,
         $notes AS notes
  FROM SPORTS_MLB_SCHEDULE AS SCHED
  LEFT JOIN SPORTS_MLB_SCHEDULE_PROBABLES AS EXISTING
    ON (EXISTING.season = SCHED.season
    AND EXISTING.game_type = SCHED.game_type
    AND EXISTING.game_id = SCHED.game_id
    AND EXISTING.team_id = '$team_id')
  WHERE SCHED.season = '$season'
  AND   IFNULL(SCHED.started_date, SCHED.game_date) = '$date'
  AND   '$team_id' IN (SCHED.home, SCHED.visitor)
  AND   EXISTING.player_id IS NULL
  ORDER BY SCHED.game_time
  LIMIT 1;\n\n";
  }
}

print "# Table maintenance\n";
print "ALTER TABLE SPORTS_MLB_SCHEDULE_PROBABLES ORDER BY season, game_type, game_id, team_id;\n";

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);
