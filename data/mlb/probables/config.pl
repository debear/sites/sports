#!/usr/bin/perl -w
# Common code for the probable starter downloading and parsing
use Compress::Zlib;

$config{'default_future_days'} = 4; # Excludes today...

#
# Convert command line args in to something we can process
#
sub parse_args {
  my $season = ''; my $start_date = 'initial'; my $future_days = $config{'default_future_days'};
  foreach my $arg (@ARGV) {
    if ($arg =~ m/^--all$/gsi) {
      $future_days = -1;
    } elsif ($season eq '') {
      $season = $arg;
    } elsif ($arg =~ /^\d{4}-\d{2}-\d{2}$/ && $start_date eq 'initial') {
      $start_date = $arg;
    } else {
      print '# Unknown option "' . $arg . '"' . "\n";
    }
  }

  # Must have at least season and mode
  if ($season eq '') {
    print STDERR "Invalid arguments.\n";
    exit;
  }

  return ($season, $start_date, $future_days);
}

#
# Where are the files downloaded
#
sub get_base_dir {
  our (%config, $season, $start_date);
  return sprintf('%s/_data/%s/probables/%s', $config{'base_dir'}, $season, $start_date);
}

#
# Load a file
#
sub load_file {
  my ($filename) = @_;

  my $buffer;
  my $gzh = gzopen($filename, 'rb');
  $contents .= $buffer while $gzh->gzread($buffer) > 0;
  $gzh->gzclose();

  return $contents;
}

#
# Dates to get/parse the probables
#
sub processing_dates {
  # Build the query that will determine the dates we need to loop through
  my $query = 'SELECT DISTINCT IFNULL(started_date, game_date) AS game_date FROM SPORTS_MLB_SCHEDULE WHERE season = ?';
  my @args = ( $season );
  if ($start_date ne 'initial') {
    $query .= ' AND IFNULL(started_date, game_date) >= ?';
    push @args, $start_date;
  }
  if ($future_days != -1) {
    $query .= ' AND IFNULL(started_date, game_date) <= DATE_ADD(CURDATE(), INTERVAL ? DAY)';
    push @args, $future_days;
  }
  $query .= ' ORDER BY IFNULL(started_date, game_date);';

  # Get that list from the database
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $ret = $dbh->selectall_arrayref($query, { Slice => {} }, @args);
  $dbh->disconnect if defined($dbh);
  return map $_->{'game_date'}, @$ret;
}

# Return true to pacify the compiler
1;
