#!/usr/bin/perl -w
# Parse the list of probable starters for the next few days from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;
use Archive::Tar;
use IO::Uncompress::Gunzip qw(gunzip);

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $start_date, $future_days) = parse_args();
$config{'base_dir'} = get_base_dir();
my $tar_file = "$config{'base_dir'}.tar.gz";

# Determine the dates to loop through
my @dates = processing_dates();

# Validate: no dates => no need to run...
if (!@dates) {
  print "# No dates to process based on argument '$start_date', silently quitting.\n";
  exit;
}

# Open the file
my $tar = Archive::Tar->new($tar_file);
my @files = grep(/\.gz$/, sort($tar->list_files()));

# Inform user
my ($dir_name) = ($files[0] =~ m/^(.+)\d{4}/);
my $date_from = substr($files[0], length($dir_name), 10);
my $date_to = substr($files[-1], length($dir_name), 10);
print "# Parsing Probable Starters for $season, from $date_from to $date_to (" . @files . " day" . (@files == 1 ? '' : 's') . ")\n";
our $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

# Loop through the files
my @date_parsed = ();
foreach my $file (@files) {
  # Display
  my $date = substr($file, length($dir_name), 10);
  print "\n#\n# $date\n#\n";

  # Get the file details
  my $contents_gz = $tar->get_content($file);
  my $contents;
  gunzip \$contents_gz => \$contents;

  my @games = ($contents =~ m/<div class="probable-pitchers__matchup probable-pitchers__matchup--\d+ " data-gamePk="(\d+)">(.*?)<div class="probable-pitchers__buttons probable-pitchers__buttons--bottom">/gsi);
  push @date_parsed, "($season, '$date')" if @games;
  while (my ($mlb_id, $game_content) = splice(@games, 0, 2)) {
    # Get the info out
    my ($visitor, $home) = ($game_content =~ m/<span class="probable-pitchers__team-name probable-pitchers__team-name--away">(.*?)<\/span>.*?<span class="probable-pitchers__team-name probable-pitchers__team-name--home">(.*?)<\/span>/gsi);
    my ($name_visitor, $name_home) = ($game_content =~ m/<div class="probable-pitchers__pitcher-name">(.*?)<\/div>.*?<div class="probable-pitchers__pitcher-name">(.*?)<\/div>/gsi);
    my ($probable_visitor, $probable_home) = ($game_content =~ m/<div class="probable-pitchers__stats-header probable-pitchers__stats-header--expand"\s*data-player-id-away="([^"]*)"\s*data-team-id-away="[^"]*"\s*data-player-id-home="([^"]*)"\s*data-team-id-home="[^"]*"/gsi);
    my ($notes_visitor, $notes_home) = ($game_content =~ m/<div class="probable-pitchers_pitcher-notes">(.*?)<\/div>.*?<div class="probable-pitchers_pitcher-notes">(.*?)<\/div>/gsi);
    trim(\$visitor); trim(\$home);

    # Then render
    print "# $visitor \@ $home\n";
    print "DELETE PROBABLE.*
FROM SPORTS_MLB_SCHEDULE AS SCHED
JOIN SPORTS_MLB_SCHEDULE_PROBABLES AS PROBABLE
  USING (season, game_type, game_id)
WHERE season = '$season'
AND   mlb_id = '$mlb_id';\n\n";
    render_probable($mlb_id, 'visitor', $visitor, $name_visitor, $probable_visitor, $notes_visitor);
    render_probable($mlb_id, 'home', $home, $name_home, $probable_home, $notes_home);
  }
}

# If there were any rows parsed, post-process
if (@date_parsed) {
  print "# Post-process\n";
  print "DROP TEMPORARY TABLE IF EXISTS tmp_probables_dates;\n";
  print "CREATE TEMPORARY TABLE tmp_probables_dates (
  season YEAR NOT NULL,
  game_date DATE NOT NULL,
  PRIMARY KEY (season, game_date)
);\n";
  print "INSERT INTO tmp_probables_dates (season, game_date) VALUES " . join(', ', @date_parsed) . ";\n";
  print "CALL mlb_probables_process();\n\n";
}

print "# Table maintenance\n";
print "ALTER TABLE SPORTS_MLB_SCHEDULE_PROBABLES ORDER BY season, game_type, game_id, team_id;\n";

# Disconnect from the database
if (defined($config{'sth'})) {
  foreach my $key (keys %{$config{'sth'}}) {
    undef $config{'sth'}{$key} if defined($config{'sth'}{$key});
  }
}
$dbh->disconnect if defined($dbh);

# Worker method to generate the SQL
sub render_probable {
  my ($mlb_id, $team_type, $team, $name, $remote_id, $notes) = @_;

  # Starter still TBD?
  if (!defined($remote_id) || !$remote_id) {
    print "# $team: TBD\n";
    return;
  }

  # Process
  trim(\$name);
  $name =~ s/<[^>]+>//g;
  trim(\$notes);
  my $player_id = determine_player_id($remote_id);
  # Clean up the notes
  if (defined($notes)) {
    # Strip some dodgy encodings?
    $notes =~ s/&#xd;&#xa;/ /gi;
    $notes =~ s/&#xa;/ /gi;
    # Convert to HTML
    $notes = convert_text($notes)
      if $notes !~ /&(?:#|amp)/;
  }
  check_for_null(\$notes);
  print "# $team: $name (Remote: $remote_id, ID: $player_id)\n";
  print "INSERT INTO SPORTS_MLB_SCHEDULE_PROBABLES (season, game_type, game_id, game_date, team_id, player_id, opp_team_id, venue, notes)
  SELECT season, game_type, game_id, IFNULL(started_date, game_date) AS game_date,
         $team_type AS team_id,
         '$player_id' AS player_id,
         IF('$team_type' = 'home', visitor, home) AS opp_team_id,
         IF('$team_type' = 'home', 'home', 'away') AS venue,
         $notes AS notes
  FROM SPORTS_MLB_SCHEDULE AS SCHED
  WHERE season = '$season'
  AND   mlb_id = '$mlb_id';\n\n";
}
