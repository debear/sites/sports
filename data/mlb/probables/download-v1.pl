#!/usr/bin/perl -w
# Get the list of probable starters for the next few days from MLB.com

use strict;
use File::Path qw(make_path rmtree);
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $start_date, $future_days) = parse_args();
$config{'base_dir'} = get_base_dir();
make_path($config{'base_dir'});

# Determine the dates to loop through
my @dates = processing_dates();

# If no dates, no need to continue (but consider as a silent error)
if (!@dates) {
  print "No dates found, exiting.\n";
  exit;
}

# Inform user
print "# Downloading Probable Starters for $season, from $dates[0] to $dates[-1] (" . @dates . " day" . (@dates == 1 ? '' : 's') . ")\n";

# Check we haven't already done the download
my $existing = "$config{'base_dir'}.tar.gz";
if (-e $existing) {
  my @stat = stat $existing;
  my $dl = (time2date($stat[9]) ne time2date(time())); # 9 = mtime
  if (!$dl) {
    print "# Skipped, files already downloaded\n";
    exit;
  }
}

# Loop through these dates and download!
foreach my $date (@dates) {
  download_file($date);
}

# Tar together in to a single file
if (!$config{'debug'}) {
  print "# Tarring...\n";
  my $tar_dir = dirname($config{'base_dir'});
  my $tar_loc = basename($config{'base_dir'});
  `tar -C$tar_dir -czf $config{'base_dir'}.tar.gz $tar_loc`;
  rmtree($config{'base_dir'});
}

# Perform the download
sub download_file {
  my ($date) = @_;
  my $date_slash = $date; ($date_slash =~ s/-/\//g);
  my $url = "http://mlb.mlb.com/news/probable_pitchers/?c_id=mlb&date=$date_slash";
  my $local = "$config{'base_dir'}/$date.htm.gz";
  print "# $date: $url => $local\n";

  # Skip if already downloaded
  return $local if -e $local;

  # Then perform the download
  download($url, $local, {'gzip' => 1, 'debug' => $config{'debug'}});

  return $local;
}
