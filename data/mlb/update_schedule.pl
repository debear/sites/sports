#!/usr/bin/perl -w
# Download and import schedules from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'schedule';
my %logs = get_log_def();

# Guesstimate the season
my @time = localtime();
my $date = time2date(time());
my $season_guess = $time[5] + 1900;

# Unless passed in a special argument, we're to ask the user how to operate
my $season;
my @mode = ();

# Determine for ourselves
if ($config{'is_unattended'}) {
  # Season is our guessed season
  $season = $season_guess;
  # Get the crossover point from the database as to whether we're regular season or playoffs
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $info = $dbh->selectrow_hashref('SELECT MAX(status IS NULL) AS is_reg FROM SPORTS_MLB_SCHEDULE WHERE season = ? AND game_type = "regular";', { Slice => {} }, $season);
  $dbh->disconnect if defined($dbh);
  if (!defined($info)) {
    print STDERR "Error: Unable to run for $season because schedule has not yet been loaded?\n";
    exit;
  }
  # If we have regular season games left to process, we only want that schedule
  push @mode, ($$info{'is_reg'} ? 'regular' : 'playoff');

# Passed in
} elsif (($season) = grep(/^20\d{2}$/, @ARGV)) {
  push @mode, 'regular' if grep(/^--regular$/, @ARGV);
  push @mode, 'playoff' if grep(/^--playoff$/, @ARGV);
  if (!@mode) {
    print STDERR "Error: No valid modes passed in.\n";
    exit;
  }

# Get from the user
} else {
  # Confirm the season to use
  $season = question_user("Please enter a season in YYYY format [$season_guess]", '\d{4}', $season_guess);

  # Which game types?
  my $q = question_user("Regular season? [Yn]", '[yn]', 'y');
  push @mode, 'regular' if $q eq 'y';
  $q = question_user("Playoffs? [yN]", '[yn]', 'n');
  push @mode, 'playoff' if $q eq 'y';

  # Ensure something has been entered
  if (!@mode) {
    print STDERR "Error: Please select at least one of regular season or playoff.\n";
    exit;
  }
  print "\n";
}

# Tidy internal variables after being determined above
my $mode = join(' ', @mode);

# Inform user
print "Acquiring data from $season, $mode game types\n";
print 'Run started at ' . `date` . "\n";

# Idenfity the log file
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

# Loop through regular/playoffs separately
foreach my $mode (@mode) {
  # Determine date argument
  my $date_arg = '';
  if (!$config{'is_historical'} && -e "$config{'base_dir'}/_data/$season/schedules/$mode/initial.json.gz") {
    $date_arg = $date;
  }
  print '=> ' . ucfirst($mode) . " ($date_arg):\n";

  # Download, if we haven't done so already (handled in-script)
  print '  -> Download: ';
  command("$config{'base_dir'}/schedule/download.pl $season --$mode $date_arg",
          "$config{'log_dir'}/$logs{'download'}.log",
          "$config{'log_dir'}/$logs{'download'}.err");
  done(0);

  # Parse (unless download only...)
  if (!download_only()) {
    print '  -> Parse: ';
    command("$config{'base_dir'}/schedule/parse.pl $season --$mode $date_arg",
            "$config{'log_dir'}/$logs{'parse'}.sql",
            "$config{'log_dir'}/$logs{'parse'}.err");
    done(3);
  }
}

# If only downloading, go no further
end_script() if download_only();

# Import
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'import'}.log",
        "$config{'log_dir'}/$logs{'import'}.err");
done(4);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'parse'    => '02_parse',
    'import'   => '03_import',
  );
}
