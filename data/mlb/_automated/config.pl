#!/usr/bin/perl -w
# Automated processing specific configuration

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

our $mode;
$config{'log_dir'} = "$config{'base_dir'}/_logs/" . (defined($mode) ? "_$mode" : '');
my $date_disp = `date +'%F'`; chomp($date_disp);

##
## Daily config
##
%{$config{'daily'}} = (
  'app' => 'sports_mlb',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    # Schedule
    { 'label' => 'Schedule',
      'script' => "$config{'base_dir'}/update_schedule.pl --unattended" },
    # Rosters
    { 'label' => 'Rosters / Depth Charts',
      'script' => "$config{'base_dir'}/update_rosters.pl" },
    # Injuries
    #{ 'label' => 'Injuries',
    #  'script' => "$config{'base_dir'}/update_injuries.pl" },
    # Games
    { 'label' => 'Games',
      'script' => "$config{'base_dir'}/update_games.pl --unattended" },
    # Probables
    { 'label' => 'Probables',
      'script' => "$config{'base_dir'}/update_probables.pl" },
    # Upload Database
    { 'label' => 'Upload Database',
      'script' => '/var/www/debear/bin/git-admin/server-sync sports_mlb live --daily-tunnel --benchmark' },
  ],
  'email-subject' => 'Automated daily MLB script for ' . $date_disp,
);

##
## Starting Lineups config
##
%{$config{'lineups'}} = (
  'app' => 'sports_mlb_lineup',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'Starting Lineups',
      'script' => "$config{'base_dir'}/update_lineups.pl" },
    # Upload Database
    { 'label' => 'Upload Database',
      'script' => '/var/www/debear/bin/git-admin/server-sync sports_mlb_lineup live --benchmark' },
  ],
  'email-subject' => 'Automated MLB starting lineups script for ' . $date_disp,
  'log_inc_time' => '%H%M',
);

##
## Player Mugshot config
##
%{$config{'mugshots'}} = (
  'app' => 'sports_mlb_mugshots',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'Mugshot Resync',
      'script' => "$config{'base_dir'}/update_mugshots.pl" },
    # Upload Database
    { 'label' => 'Upload Database',
      'script' => '/var/www/debear/bin/git-admin/server-sync sports_mlb_players live --benchmark' },
  ],
  'email-subject' => 'Automated MLB mugshot resync script for ' . $date_disp,
);

# Return true to pacify the compiler
1;
