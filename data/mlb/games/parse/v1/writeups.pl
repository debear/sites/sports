#!/usr/bin/perl -w
# Note: Script deprecated after Mar-2017. TD.
return;

use List::MoreUtils qw(uniq);

# Tables used
our %config;
@{$config{'tables'}{'writeups'}} = (
  'SPORTS_MLB_GAME_WRITEUPS',
);

# Parse the game preview and recaps
sub parse_writeups {
  # First the preview
  parse_writeups_process('preview');
  # Then the recap
  parse_writeups_process('recap');
}

# Worker method
sub parse_writeups_process {
  our ($season, $game_type, $game_id);
  my ($type) = @_;

  # Load the file
  my $file = "$config{'game_dir'}/$type.htm.gz";
  my @stat = stat $file;
  if (!defined($stat[7]) || $stat[7] < 1024) {
    print "\n# No $type file available. Skipping.\n";
    return;
  }
  my $writeup = load_file($file, { 'no-decode' => 1 }); # Load without trying to JSON decode as it's a HTML file

  # Get the article, skipping if nothing found.
  my @sections = ($writeup =~ m/<div class="blurb">\s*<div class="default-styles">(.*?)<\/div>.*?<\/div>\s*<div class="full-text"[^>]*>\s*<div class="default-styles">(.*?)<div class="social-bar cf">.*?<\/div>/gsi);
  my $article = join('', @sections);
  # Some HTML cleaning
  $article =~ s/<span[^>]*>.*?More.*?<\/span>//g;
  $article =~ s/<span[^>]*><a[^>]*>(.*?)<\/a><\/span>/$1/g;
  $article =~ s/<figure.*?<\/figure>//g;
  $article =~ s/<!--.*?-->//g;
  # Paragraph fix
  if ($article =~ m/<p\/>/) {
    $article =~ s/\s*?<p\/>\s+/<\/p>\n<p>/g;
    $article =~ s/^\s+//;
    $article =~ s/^(.+)<\/div>/<p>$1<\/p>/ms
      if substr($article, 0, 2) ne '<p';
  }
  # Now get the paragraphs
  my @para = ($article =~ m/<p>(.*?)<\/p>/gsi);
  if (!@para) {
    print "\n# No $type paragraph(s) found. Skipping.\n";
    return;
  }
  $article = '<p>' . join('</p><p>', @para) . '</p>';
  # HTML cleaning, Part II
  $article =~ s/<p><b>([^<]+)<\/b><br\s*\/>\n/<h3>$1<\/h3><p>/g;

  # Player interpolation
  my @interp = uniq($article =~ m/(<a href="[^"]+\/player\/\d+\/[^"]+">.*?<\/a>)/gsi);
  $article = convert_text($article);
  foreach my $player_link (@interp) {
    my ($remote_id) = ($player_link =~ m/\/(\d+)\//);
    $player_link = convert_text($player_link);
    $article =~ s/$player_link/{{PLAYER_', IFNULL(\@player_$remote_id, 'REM_$remote_id'), '}}/g;
  }

  # Get meta info out...
  my ($headline, $edited, $author) = ($writeup =~ m/<script[^>]*>\s*\{.*?"headline"\s*:\s*"([^"]+)",.*?"dateCreated"\s*:\s*"([^"]+)",.*?"creator"\s*:\s*"([^"]+)"\s*\}\s*<\/script>/gsi);
  $headline = convert_text($headline); check_for_null(\$headline);
  $edited = convert_text($edited); check_for_null(\$edited);
  $author = convert_text($author); check_for_null(\$author);

  print "\n# " . ucfirst($type) . "\n";
  print "INSERT INTO `SPORTS_MLB_GAME_WRITEUPS` (`season`, `game_type`, `game_id`, `writeup_type`, `headline`, `edited`, `author`, `article`)
  VALUES ('$season', '$game_type', '$game_id', '$type', $headline, $edited, $author, CONCAT('$article'));\n";
}

# Return true to pacify the compiler
1;
