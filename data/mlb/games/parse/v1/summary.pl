#!/usr/bin/perl -w
# Tables used
our %config;
@{$config{'tables'}{'summary'}} = (
  'SPORTS_MLB_GAME_LINESCORE',
  'SPORTS_MLB_GAME_UMPIRES',
);

# Summary processing
sub parse_summary {
  my @sched_cols = ( );

  #
  # SPORTS_MLB_SCHEDULE fields
  #
  # Score
  push @sched_cols, { 'col' => 'home_score', 'value' => $$gamedata{'liveData'}{'linescore'}{'home'}{'runs'} };
  push @sched_cols, { 'col' => 'visitor_score', 'value' => $$gamedata{'liveData'}{'linescore'}{'away'}{'runs'} };
  # Status, Innings, Attendance
  push @sched_cols, { 'col' => 'status', 'value' => 'F' };
  push @sched_cols, { 'col' => 'innings', 'value' => scalar @{$$gamedata{'liveData'}{'plays'}{'playsByInning'}} };
  my $att = $game_info_parsed{'_all'}{'att'};
  $att =~ s/\D//g if defined($att);
  push @sched_cols, { 'col' => 'attendance', 'value' => $att };
  # Weather
  my $temp; my $cond; my $wind;
  if (defined($$gamedata{'gameData'}{'weather'}) && %{$$gamedata{'gameData'}{'weather'}}) {
    # Directly from our weather object
    $temp = $$gamedata{'gameData'}{'weather'}{'temp'};
    $cond = $$gamedata{'gameData'}{'weather'}{'condition'};
    $wind = $$gamedata{'gameData'}{'weather'}{'wind'};
  } else {
    # Indirectly, from the game info
    ($temp, $cond) = ($game_info_parsed{'_all'}{'weather'} =~ m/^\s*?(\d+) degrees,\s+(.*?)\.?$/);
    $wind = $game_info_parsed{'_all'}{'wind'};
  }
  #  Process...
  $cond = ucfirst($cond) if defined($cond);
  push @sched_cols, { 'col' => 'weather_temp', 'value' => $temp };
  push @sched_cols, { 'col' => 'weather_cond', 'value' => $cond };
  my ($wind_sp, $wind_dir) = ($game_info_parsed{'_all'}{'wind'} =~ /^\s*?(\d+)\s*mph,?\s+(.*?)\.$/s);
  $wind_dir = ucfirst($wind_dir) if defined($wind_dir);
  push @sched_cols, { 'col' => 'weather_wind_sp', 'value' => $wind_sp };
  push @sched_cols, { 'col' => 'weather_wind_dir', 'value' => $wind_dir };
  # Winning / Losing / Saving Pitchers
  push @sched_cols, { 'col' => 'winning_pitcher', 'value' => $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$gamedata{'liveData'}{'linescore'}{'pitchers'}{'win'}}{'shirtNum'} };
  push @sched_cols, { 'col' => 'losing_pitcher', 'value' => $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$gamedata{'liveData'}{'linescore'}{'pitchers'}{'loss'}}{'shirtNum'} };
  push @sched_cols, { 'col' => 'saving_pitcher', 'value' => defined($$gamedata{'liveData'}{'linescore'}{'pitchers'}{'save'}) ? $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$gamedata{'liveData'}{'linescore'}{'pitchers'}{'save'}}{'shirtNum'} : undef };
  # Perfect Game / No Hitter?
  my %pitching_nums = ( 'away' => { 'bf' => 0, 'outs' => 0, 'hits' => 0 }, 'home' => { 'bf' => 0, 'outs' => 0, 'hits' => 0 } );
  foreach my $team ('away', 'home') {
    my %info = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}};
    foreach my $remote_id (@{$info{'pitchers'}}) {
      my %pitcher = %{$info{'players'}{"ID$remote_id"}};
      $pitching_nums{$team}{'bf'} += $pitcher{'gameStats'}{'pitching'}{'battersFaced'};
      $pitching_nums{$team}{'outs'} += $pitcher{'gameStats'}{'pitching'}{'outs'};
      $pitching_nums{$team}{'hits'} += $pitcher{'gameStats'}{'pitching'}{'hits'};
    }
    $pitching_nums{$team}{'is_perfect'} = int($pitching_nums{$team}{'bf'} == $pitching_nums{$team}{'outs'});
    $pitching_nums{$team}{'is_no_hitter'} = int($pitching_nums{$team}{'hits'} == 0);
  }
  push @sched_cols, { 'col' => 'perfect_game', 'value' => int($pitching_nums{'away'}{'is_perfect'} || $pitching_nums{'home'}{'is_perfect'}) };
  push @sched_cols, { 'col' => 'no_hitter', 'value' => int($pitching_nums{'away'}{'is_no_hitter'} || $pitching_nums{'home'}{'is_no_hitter'}) };

  print "UPDATE `SPORTS_MLB_SCHEDULE` SET ";
  my $comma = 0;
  foreach my $col (@sched_cols) {
    print ", " if $comma;
    my $value = $$col{'value'};
    check_for_null(\$value) if !defined($$col{'no-null-check'}) || !$$col{'no-null-check'};
    print "`$$col{'col'}` = " . $value;
    $comma = 1;
  }
  print " WHERE `season` = '$season' AND `game_type` = '$game_type' AND `game_id` = '$game_id';\n\n";

  #
  # Linescore
  #
  print "## Linescore\n";
  # Get the info
  my %linescores = ( 'home' => { 'team_id' => $$game_info{'home_id'},
                                 'r' => $$gamedata{'liveData'}{'linescore'}{'home'}{'runs'},
                                 'h' => $$gamedata{'liveData'}{'linescore'}{'home'}{'hits'},
                                 'e' => $$gamedata{'liveData'}{'linescore'}{'home'}{'errors'},
                                 'inn' => { } },
                     'visitor' => { 'team_id' => $$game_info{'visitor_id'},
                                    'r' => $$gamedata{'liveData'}{'linescore'}{'away'}{'runs'},
                                    'h' => $$gamedata{'liveData'}{'linescore'}{'away'}{'hits'},
                                    'e' => $$gamedata{'liveData'}{'linescore'}{'away'}{'errors'},
                                    'inn' => { } } );
  foreach my $inn (@{$$gamedata{'liveData'}{'linescore'}{'innings'}}) {
    $linescores{'home'}{'inn'}{$$inn{'num'}} = $$inn{'home'} if $$inn{'home'} ne 'x';
    $linescores{'visitor'}{'inn'}{$$inn{'num'}} = $$inn{'away'} if $$inn{'away'} ne 'x';
  }
  # Now display
  foreach my $t (sort keys %linescores) {
    my %l = %{$linescores{$t}};
    my @inn_col = ( );
    my @inn_val = ( );
    for (my $i = 1; $i <= 20; $i++) {
      if (defined($l{'inn'}{$i})) {
        push @inn_col, sprintf('inn%02d', $i);
        push @inn_val, "'$l{'inn'}{$i}'";
      }
    }
    print "INSERT INTO `SPORTS_MLB_GAME_LINESCORE` (`season`, `game_type`, `game_id`, `team_id`, `runs`, `hits`, `errors`, `" . join('`, `', @inn_col) . "`)
  VALUES ('$season', '$game_type', '$game_id', '$l{'team_id'}', '$l{'r'}', '$l{'h'}', '$l{'e'}', " . join(', ', @inn_val) . ");\n";
  }
  print "\n";

  #
  # Umpires
  #
  print "## Umpires\n";
  my @umpire_list = split(/(?:HP|[123]B|[LR]F):/, $game_info_parsed{'_all'}{'umpires'});
  my @umpire_pos = ( 'HP', '1B', '2B', '3B', 'LF', 'RF' );
  for (my $i = 1; $i < @umpire_list; $i++) {
    my $umpire_pos = $umpire_pos[$i-1];
    print "\n# $umpire_pos: ";
    my ($umpire) = ($umpire_list[$i] =~ m/^ (.+)\. ?$/);
    if (!defined($umpire)) {
      print "Unknown, skipped...\n";
      next;
    }
    # Split the name
    my @tmp = split(' ', $umpire);
    my ($umpire_fname) = convert_text(shift @tmp);
    my $umpire_sname = convert_text(join(' ', @tmp));
    # Display
    print "$umpire\n";
    print "SET \@umpire_id_$umpire_pos := NULL;\n";
    print "SELECT `umpire_id` INTO \@umpire_id_$umpire_pos FROM `SPORTS_MLB_UMPIRES` WHERE `first_name` = '$umpire_fname' AND `surname` = '$umpire_sname';\n";
    print "INSERT IGNORE INTO `SPORTS_MLB_UMPIRES` (`umpire_id`, `first_name`, `surname`)
  VALUES (\@umpire_id_$umpire_pos, '$umpire_fname', '$umpire_sname');\n";
    print "SELECT `umpire_id` INTO \@umpire_id_$umpire_pos FROM `SPORTS_MLB_UMPIRES` WHERE `first_name` = '$umpire_fname' AND `surname` = '$umpire_sname';\n";
    print "INSERT INTO `SPORTS_MLB_GAME_UMPIRES` (`season`, `game_type`, `game_id`, `umpire_pos`, `umpire_id`)
  VALUES ('$season', '$game_type', '$game_id', '$umpire_pos', \@umpire_id_$umpire_pos);\n";
  }
}

# Return true to pacify the compiler
1;
