#!/usr/bin/perl -w
use List::MoreUtils qw(uniq);

# Tables used
our %config;
@{$config{'tables'}{'boxscore'}} = (
  'SPORTS_MLB_GAME_PITCHERS',
  'SPORTS_MLB_PLAYERS_GAME_BATTING',
  'SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D',
  'SPORTS_MLB_PLAYERS_GAME_PITCHING',
  'SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D',
  'SPORTS_MLB_PLAYERS_GAME_FIELDING',
  'SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D',
  'SPORTS_MLB_PLAYERS_SEASON_MISC',
  'SPORTS_MLB_PLAYERS_SEASON_BATTING',
  'SPORTS_MLB_PLAYERS_SEASON_PITCHING',
  'SPORTS_MLB_PLAYERS_SEASON_FIELDING',
  'SPORTS_MLB_TEAMS_GAME_BATTING',
  'SPORTS_MLB_TEAMS_GAME_PITCHING',
  'SPORTS_MLB_TEAMS_GAME_FIELDING',
);

# Boxscore processing
sub parse_boxscore {
  our %game_info_split;
  my $boxscore_disp = -1; # Section to display: -1 = All, 0 = None, 1-X = appropriate section;

  # Define our column mappings
  my %col_maps = (
    'batters' => [
      { 'local' => 'ab', 'remote' => 'atBats' },
      { 'local' => 'h', 'remote' => 'hits' },
      { 'local' => '2b', 'remote' => 'doubles' },
      { 'local' => '3b', 'remote' => 'triples' },
      { 'local' => 'bb', 'remote' => 'baseOnBalls' },
      { 'local' => 'r', 'remote' => 'runs' },
      { 'local' => 'hr', 'remote' => 'homeRuns' },
      { 'local' => 'rbi', 'remote' => 'rbi' },
      { 'local' => 'sb', 'remote' => 'stolenBases' },
      { 'local' => 'cs', 'remote' => 'caughtStealing' },
      { 'local' => 'sac_fly', 'remote' => 'sacFlys' },
      { 'local' => 'sac_hit', 'remote' => 'sacBunts' },
      { 'local' => 'hbp', 'remote' => 'hitByPitch' },
      { 'local' => 'k', 'remote' => 'strikeOuts' },
      { 'local' => 'go', 'remote' => 'groundOuts' },
      { 'local' => 'fo', 'remote' => 'flyOuts' },
      { 'local' => 'lob', 'remote' => 'leftOnBase' },
    ],
    'pitching' => [
      { 'local' => 'ip', 'remote' => 'inningsPitched' },
      { 'local' => 'out', 'remote' => 'outs' },
      { 'local' => 'bf', 'remote' => 'battersFaced' },
      { 'local' => 'pt', 'remote' => 'pitchesThrown' },
      { 'local' => 'b', 'remote' => 'balls' },
      { 'local' => 's', 'remote' => 'strikes' },
      { 'local' => 'k', 'remote' => 'strikeOuts' },
      { 'local' => 'h', 'remote' => 'hits' },
      { 'local' => 'hr', 'remote' => 'homeRuns' },
      { 'local' => 'bb', 'remote' => 'baseOnBalls' },
      { 'local' => 'r', 'remote' => 'runs' },
      { 'local' => 'er', 'remote' => 'earnedRuns' },
    ],
    'fielding' => [
      { 'local' => 'po', 'remote' => 'putOuts' },
      { 'local' => 'a', 'remote' => 'assists' },
      { 'local' => 'e', 'remote' => 'errors' },
    ],
  );

  # Various game info components need parsing (and apply to both teams)
  # Batting: IBB
  %{$game_info_split{'_all'}{'ibb_batter'}} = parse_boxscore_gameinfo('ibb', {'regex' => '^([^\(]+)\s+\('});
  # Pitching: Game Scores
  %{$game_info_split{'_all'}{'game_scores'}} = parse_boxscore_gameinfo('game scores');
  # Pitching: IBB
  %{$game_info_split{'_all'}{'ibb_pitcher'}} = parse_boxscore_ibb_pitcher();
  # Pitching: WP
  %{$game_info_split{'_all'}{'wp'}} = parse_boxscore_gameinfo('wp');
  # Pitching: GO/FO
  %{$game_info_split{'_all'}{'go_fo'}} = parse_boxscore_gameinfo('groundouts-flyouts', { 'num_A-B' => 1 });
  # Pitching: IRS
  %{$game_info_split{'_all'}{'irs'}} = parse_boxscore_gameinfo('inherited runners-scored', { 'num_A-B' => 1 });
  # Picthing: Balk
  %{$game_info_split{'_all'}{'balk'}} = parse_boxscore_gameinfo('balk');

  my %num_dp = ( 'home' => 0, 'visitor' => 0 );

  # Prep work: determine instances of "Defensive Interference" that we need to include in our PA calcs (but don't have to hand in the aggregated player stats)
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    parse_boxscore_identify_defint($play);
  }

  # Process
  my @teams = ( { 'team_id' => $$game_info{'visitor_id'}, 'type' => 'visitor', 'type_rem' => 'away', 'type_rem_opp' => 'home' },
                { 'team_id' => $$game_info{'home_id'},    'type' => 'home',    'type_rem' => 'home', 'type_rem_opp' => 'away' } );
  foreach our $t ( @teams ) {
    print "## $$t{'team_id'} (" . ucfirst($$t{'type'}) . ")\n";
    our %info = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$$t{'type_rem'}}};

    # Determine the starting pitcher
    my $starting_pitcher;
    for (my $i = 0; !defined($starting_pitcher) && $i < @{$info{'pitchers'}}; $i++) {
      $starting_pitcher = $info{'pitchers'}[$i]
        if $info{'players'}{'ID'.$info{'pitchers'}[$i]}{'gameStats'}{'pitching'}{'pitchesThrown'} > 0;
    }

    # Do some player name checks - in some instances where you have "SNAME, INITIAL" you need to check SURNAME only and in others you can't
    my @remote_ids = keys %{$info{'players'}};
    for (my $i = 0; $i < @remote_ids; $i++) {
      my $remote_id = $remote_ids[$i];
      next if $info{'players'}{$remote_id}{'name'}{'boxname'} !~ m/, /;

      # Current team
      for (my $j = 0; !defined($info{'players'}{$remote_id}{'name'}{'no-last-only'}) && $j < @remote_ids; $j++) {
        next if $j == $i; # Skip out current player
        $info{'players'}{$remote_id}{'name'}{'no-last-only'} = 1
          if $info{'players'}{$remote_id}{'name'}{'last'} eq $info{'players'}{$remote_ids[$j]}{'name'}{'boxname'};
      }
      # Opponent (if not already found...)
      if (!defined($info{'players'}{$remote_id}{'name'}{'no-last-only'})) {
        my %opp_players = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$$t{'type_rem_opp'}}{'players'}};
        my @opp_remote_ids = keys %opp_players;
        for (my $j = 0; !defined($info{'players'}{$remote_id}{'name'}{'no-last-only'}) && $j < @opp_remote_ids; $j++) {
          $info{'players'}{$remote_id}{'name'}{'no-last-only'} = 1
            if $info{'players'}{$remote_id}{'name'}{'last'} eq $opp_players{$opp_remote_ids[$j]}{'name'}{'boxname'};
        }
      }
    }

    #
    # Team-based game info components need parsing
    #
    # Batting: GIDP
    %{$game_info_split{$$t{'type_rem'}}{'gidp'}} = parse_boxscore_gameinfo('batting-gidp', { 'key' => $$t{'type_rem'} });
    # Batting: PO
    %{$game_info_split{$$t{'type_rem'}}{'po_batter'}} = parse_boxscore_gameinfo('baserunning-po', { 'key' => $$t{'type_rem'} });
    # Pitching: SB
    %{$game_info_split{$$t{'type_rem'}}{'sb_pitcher'}} = parse_boxscore_baserunning_pitcher($$t{'type_rem_opp'}, 'sb');
    # Picthing: CS
    %{$game_info_split{$$t{'type_rem'}}{'cs_pitcher'}} = parse_boxscore_baserunning_pitcher($$t{'type_rem_opp'}, 'cs');
    # Pitching: PO
    %{$game_info_split{$$t{'type_rem'}}{'po_pitcher'}} = parse_boxscore_po_pitcher($$t{'type_rem'});
    # Fielding: DP
    $num_dp{$$t{'type'}} = parse_boxscore_dp_team($$t{'type_rem'});
    %{$game_info_split{$$t{'type_rem'}}{'dp'}} = parse_boxscore_dp_fielder($$t{'type_rem'});

    #
    # Batters
    #
    if (check_disp(1, $boxscore_disp)) {
      print "\n# Batting\n";
      # First parse the team notes
      my %notes = ( );
      if (defined($info{'note'}) && $info{'note'} ne '') {
        $info{'note'} =~ s/<\/span><br\/><span[^>]*?>//g;
        my $notes;
        if (substr($info{'note'}, 0, 5) eq '<span') {
          ($notes) = ($info{'note'} =~ m/<span[^>]*?>(.*?)<\/span.*?$/);
        } else {
          $notes = $info{'note'} . ' ';
        }
        # Name interpolation
        my %rosters = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}};
        foreach my $p (keys(%rosters)) {
          $notes =~ s/$rosters{$p}{'name'}{'boxname'}/{{PLAYER_REM_$rosters{$p}{'id'}}}/g
            if defined($rosters{$p}{'shirtNum'});
        }
        foreach my $n ($notes =~ m/([^\-]\-\s*?.*?)\s*?\.\s/g) {
          # Parse
          my ($letter, $detail) = ($n =~ m/^([^\-]\-)\s*?(.*?)$/);
          # Store
          $notes{$letter} = $detail;
        }
      }
      # Then loop through the players
      foreach my $remote_id (@{$info{'batters'}}) {
        my %player = %{$info{'players'}{"ID$remote_id"}};

        my %stats = %{$player{'gameStats'}{'batting'}};
        # Skip players who did not bat (usually pitchers in AL-park games...)
        next if !defined($stats{'battingOrder'});
        my $spot = substr($stats{'battingOrder'}, 0, 1);
        # Determine the stats
        my @stat_cols = ( );
        my @stat = ( );
        foreach my $col ( @{$col_maps{'batters'}} ) {
          next if !defined($stats{$$col{'remote'}});
          push @stat_cols, $$col{'local'};
          push @stat, $stats{$$col{'remote'}};
        }
        my $calc;
        # Calc: IBB
        $calc = boxscore_extra_info($game_info_split{'_all'}{'ibb_batter'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'ibb';
          push @stat, $calc;
        }
        # Calc: PA
        push @stat_cols, 'pa';
        push @stat, convert_undef_zero($stats{'atBats'}) + convert_undef_zero($stats{'baseOnBalls'}) + convert_undef_zero($stats{'hitByPitch'}) + convert_undef_zero($stats{'sacFlys'}) + convert_undef_zero($stats{'sacBunts'}) + convert_undef_zero($stats{'defInt'});
        # Calc: 1B
        push @stat_cols, '1b';
        my $singles = convert_undef_zero($stats{'hits'}) - convert_undef_zero($stats{'doubles'}) - convert_undef_zero($stats{'triples'}) - convert_undef_zero($stats{'homeRuns'});
        push @stat, $singles;
        # Calc: XBH
        push @stat_cols, 'xbh';
        my $xbh = convert_undef_zero($stats{'doubles'}) + convert_undef_zero($stats{'triples'}) + convert_undef_zero($stats{'homeRuns'});
        push @stat, $xbh;
        # Calc: TB
        push @stat_cols, 'tb';
        push @stat, $singles + (2 * convert_undef_zero($stats{'doubles'})) + (3 * convert_undef_zero($stats{'triples'})) + (4 * convert_undef_zero($stats{'homeRuns'}));
        # Calc: GIDP
        $calc = boxscore_extra_info($game_info_split{$$t{'type_rem'}}{'gidp'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'gidp';
          push @stat, $calc;
        }
        # Calc: PO
        $calc = boxscore_extra_info($game_info_split{$$t{'type_rem'}}{'po_batter'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'po';
          push @stat, $calc;
        }
        # Note
        my $player_note = 'NULL';
        if (defined($stats{'note'}) || defined($player{'boxNote'})) {
          $player_note = 'CONCAT(\'' . convert_text($notes{defined($stats{'note'}) ? $stats{'note'} : $player{'boxNote'}}) . '\')';
          # Convert remote player IDs to a method that allows us to get the imported version
          $player_note =~ s/_REM_(\d+)/_', \@player_$1, '/g;
        }

        # Display
        print "# " . ($stats{'battingOrder'} % 100 ? ' ' : '') . "$stats{'battingOrder'}: $player{'name'}{'first'} $player{'name'}{'last'} #$player{'shirtNum'}, $player{'position'}\n";
        print "INSERT INTO `SPORTS_MLB_PLAYERS_GAME_BATTING` (`season`, `game_type`, `game_id`, `team_id`, `jersey`, `spot`, `order`, `" . join("`, `", @stat_cols), "`, `note`)
  VALUES ('$season', '$game_type', '$game_id', '$$t{'team_id'}', '$player{'shirtNum'}', '$spot', '$stats{'battingOrder'}', '" . join("', '", @stat), "', $player_note);\n";
      }

      # Global updates: player_id from remote, Avg, OBP, Slg, OPS
      print "\n# In-line updates\n";
      my $g_obp = 'IF(`STAT`.`ab` + `STAT`.`bb` + `STAT`.`hbp` + `STAT`.`sac_fly` > 0, (`STAT`.`h` + `STAT`.`bb` + `STAT`.`hbp`) / (`STAT`.`ab` + `STAT`.`bb` + `STAT`.`hbp` + `STAT`.`sac_fly`), NULL)';
      my $g_slg = 'IF(`STAT`.`ab` > 0, `STAT`.`tb` / `STAT`.`ab`, NULL)';
      print "UPDATE `SPORTS_MLB_PLAYERS_GAME_BATTING` AS `STAT`
JOIN `SPORTS_MLB_GAME_ROSTERS` AS `ROSTER`
  ON (`ROSTER`.`season` = `STAT`.`season`
  AND `ROSTER`.`game_type` = `STAT`.`game_type`
  AND `ROSTER`.`game_id` = `STAT`.`game_id`
  AND `ROSTER`.`team_id` = `STAT`.`team_id`
  AND `ROSTER`.`jersey` = `STAT`.`jersey`)
SET `STAT`.`player_id` = `ROSTER`.`player_id`,
    `STAT`.`avg` = IF(`STAT`.`ab` > 0, `STAT`.`h` / `STAT`.`ab`, NULL),
    `STAT`.`obp` = $g_obp,
    `STAT`.`slg` = $g_slg,
    `STAT`.`ops` = IF(($g_obp IS NOT NULL) OR ($g_slg IS NOT NULL), IFNULL($g_obp, 0) + IFNULL($g_slg, 0), NULL)
WHERE `STAT`.`season` = '$season'
AND   `STAT`.`game_type` = '$game_type'
AND   `STAT`.`game_id` = '$game_id';\n";
    }

    #
    # Pitchers
    #
    if (check_disp(2, $boxscore_disp)) {
      print "\n# Pitching\n";
      my $outs_cumul = 0;
      foreach my $remote_id (@{$info{'pitchers'}}) {
        my %player = %{$info{'players'}{"ID$remote_id"}};

        my @rec = ();
        my %stats = %{$player{'gameStats'}{'pitching'}};
        # Determine the stats
        my @stat_cols = ( );
        my @stat = ( );
        foreach my $col ( @{$col_maps{'pitching'}} ) {
          next if !defined($stats{$$col{'remote'}});
          push @stat_cols, $$col{'local'};
          push @stat, $stats{$$col{'remote'}};
        }
        my $calc;
        # Calc: IBB
        $calc = boxscore_extra_info($game_info_split{'_all'}{'ibb_pitcher'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'ibb';
          push @stat, $calc;
        }
        # Calc: Game Score
        $calc = boxscore_extra_info($game_info_split{'_all'}{'game_scores'}, $player{'name'});
        # In some instances (usually historic), we may need to calc this ourselves...
        if (!defined($calc) && $remote_id eq $starting_pitcher) {
          $calc = 50                                             # Start with 50 points
                + ($stats{'outs'})                               # +1pt for every out
                + (2 * int(($stats{'outs'} - 12) / 3))           # +2pts for every completed inning after 4th
                + ($stats{'strikeOuts'})                         # +1pt for each strikeout
                - (2 * $stats{'hits'})                           # -2pts for each hit allowed
                - (4 * $stats{'earnedRuns'})                     # -4pts for each earned run allowed
                - (2 * ($stats{'runs'} - $stats{'earnedRuns'}))  # -2pts for each unearned run allowed
                - ($stats{'baseOnBalls'});                       # -1pt for each walk allowed
        }
        if (defined($calc)) {
          push @stat_cols, 'game_score';
          push @stat, $calc;
        }
        # Calc: WP
        $calc = boxscore_extra_info($game_info_split{'_all'}{'wp'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'wp';
          push @stat, $calc;
        }
        # Calc: SB
        $calc = boxscore_extra_info($game_info_split{$$t{'type_rem'}}{'sb_pitcher'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'sb';
          push @stat, $calc;
        }
        # Calc: CS
        $calc = boxscore_extra_info($game_info_split{$$t{'type_rem'}}{'cs_pitcher'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'cs';
          push @stat, $calc;
        }
        # Calc: PO
        $calc = boxscore_extra_info($game_info_split{$$t{'type_rem'}}{'po_pitcher'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'po';
          push @stat, $calc;
        }
        # Calc: GO / FO
        $calc = boxscore_extra_info($game_info_split{'_all'}{'go_fo'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'go';
          push @stat, $$calc[0];
          push @stat_cols, 'fo';
          push @stat, $$calc[1];
        }
        # Calc: UR
        push @stat_cols, 'ur';
        push @stat, $stats{'runs'} - $stats{'earnedRuns'};
        # Calc: IR / IRA
        $calc = boxscore_extra_info($game_info_split{'_all'}{'irs'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'ir';
          push @stat, $$calc[0];
          push @stat_cols, 'ira';
          push @stat, $$calc[1];
        }
        # Calc: Balk
        $calc = boxscore_extra_info($game_info_split{'_all'}{'balk'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'bk';
          push @stat, $calc;
        }
        # Calc: W
        if (defined($$gamedata{'liveData'}{'linescore'}{'pitchers'}{'win'}) && ($$gamedata{'liveData'}{'linescore'}{'pitchers'}{'win'} eq $remote_id)) {
          push @stat_cols, 'w';
          push @stat, '1';
          push @rec, ' **WIN**';
        }
        # Calc: L
        if (defined($$gamedata{'liveData'}{'linescore'}{'pitchers'}{'loss'}) && ($$gamedata{'liveData'}{'linescore'}{'pitchers'}{'loss'} eq $remote_id)) {
          push @stat_cols, 'l';
          push @stat, '1';
          push @rec, ' **LOSS**';
        }
        # Calc: Sv
        my $svo = 0;
        if (defined($$gamedata{'liveData'}{'linescore'}{'pitchers'}{'save'}) && ($$gamedata{'liveData'}{'linescore'}{'pitchers'}{'save'} eq $remote_id)) {
          push @stat_cols, 'sv';
          push @stat, '1';
          push @rec, ' **SAVE**';
          $svo = 1; # Contributes to our SVO calc
        }
        # Calc: Hld
        if (defined($stats{'note'}) && ($stats{'note'} =~ m/\(H,/)) {
          push @stat_cols, 'hld';
          push @stat, '1';
          push @rec, ' **HOLD**';
        }
        # Calc: BS
        if (defined($stats{'note'}) && ($stats{'note'} =~ m/\(BS,/)) {
          push @stat_cols, 'bs';
          push @stat, '1';
          push @rec, ' **BLOWN SAVE**';
          $svo = 1; # Contributes to our SVO calc
        }
        # Calc: SVO
        if ($svo) {
          push @stat_cols, 'svo';
          push @stat, '1';
        }

        # To/From info
        my $from_outs = $outs_cumul;
        my $from_ip = sprintf('%0d.%01d', floor($from_outs) / 3, $from_outs % 3);
        $outs_cumul += $stats{'outs'};
        my $to_outs = $outs_cumul;
        my $to_ip = sprintf('%0d.%01d', floor($to_outs) / 3, $to_outs % 3);

        # Display
        print "# $player{'name'}{'first'} $player{'name'}{'last'} #$player{'shirtNum'}; From: $from_ip, To: $to_ip ($stats{'inningsPitched'} IP, $stats{'battersFaced'} BF)" . join('', @rec) . "\n";
        print "INSERT INTO `SPORTS_MLB_GAME_PITCHERS` (`season`, `game_type`, `game_id`, `team_id`, `jersey`, `from_outs`, `from_ip`, `to_outs`, `to_ip`)
  VALUES ('$season', '$game_type', '$game_id', '$$t{'team_id'}', '$player{'shirtNum'}', '$from_outs', '$from_ip', '$to_outs', '$to_ip');\n";
        print "INSERT INTO `SPORTS_MLB_PLAYERS_GAME_PITCHING` (`season`, `game_type`, `game_id`, `team_id`, `jersey`, `" . join("`, `", @stat_cols), "`)
  VALUES ('$season', '$game_type', '$game_id', '$$t{'team_id'}', '$player{'shirtNum'}', '" . join("', '", @stat), "');\n";
      }

      # Global updates: player_id from remote, CG, WHIP, RA, ERA, URA, GO/FO, QS, SHO
      print "\n# In-line updates: CG\n";
      print "INSERT INTO `SPORTS_MLB_PLAYERS_GAME_PITCHING` (`season`, `game_type`, `game_id`, `team_id`, `jersey`, `cg`)
  SELECT `ME`.`season`, `ME`.`game_type`, `ME`.`game_id`, `ME`.`team_id`, `ME`.`jersey`, 1 AS `cg`
  FROM `SPORTS_MLB_PLAYERS_GAME_PITCHING` AS `ME`
  LEFT JOIN `SPORTS_MLB_PLAYERS_GAME_PITCHING` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`game_type` = `ME`.`game_type`
    AND `OTHER`.`game_id` = `ME`.`game_id`
    AND `OTHER`.`team_id` = `ME`.`team_id`
    AND `OTHER`.`jersey` <> `ME`.`jersey`)
  WHERE `ME`.`season` = '$season'
  AND   `ME`.`game_type` = '$game_type'
  AND   `ME`.`game_id` = '$game_id'
  AND   `OTHER`.`jersey` IS NULL
  GROUP BY `ME`.`season`, `ME`.`game_type`, `ME`.`game_id`, `ME`.`team_id`, `ME`.`jersey`
ON DUPLICATE KEY UPDATE `cg` = VALUES(`cg`);\n";
      print "\n# In-line updates: The rest...\n";
      print "UPDATE `SPORTS_MLB_PLAYERS_GAME_PITCHING` AS `STAT`
JOIN `SPORTS_MLB_GAME_ROSTERS` AS `ROSTER`
  ON (`ROSTER`.`season` = `STAT`.`season`
  AND `ROSTER`.`game_type` = `STAT`.`game_type`
  AND `ROSTER`.`game_id` = `STAT`.`game_id`
  AND `ROSTER`.`team_id` = `STAT`.`team_id`
  AND `ROSTER`.`jersey` = `STAT`.`jersey`)
SET `STAT`.`player_id` = `ROSTER`.`player_id`,
    `STAT`.`whip` = IF(`STAT`.`out` > 0, (`STAT`.`bb` + `STAT`.`h`) / (`STAT`.`out` / 3), NULL),
    `STAT`.`ra` = IF(`STAT`.`out` > 0, (`STAT`.`r` / `STAT`.`out`) * 27, NULL),
    `STAT`.`era` = IF(`STAT`.`out` > 0, (`STAT`.`er` / `STAT`.`out`) * 27, NULL),
    `STAT`.`ura` = IF(`STAT`.`out` > 0, (`STAT`.`ur` / `STAT`.`out`) * 27, NULL),
    `STAT`.`go_fo` = IF(`STAT`.`fo` > 0, `STAT`.`go` / `STAT`.`fo`, NULL),
    `STAT`.`qs` = (`STAT`.`out` >= 18 AND `STAT`.`er` <= 3),
    `STAT`.`sho` = (`STAT`.`cg` AND `STAT`.`r` = 0)
WHERE `STAT`.`season` = '$season'
AND   `STAT`.`game_type` = '$game_type'
AND   `STAT`.`game_id` = '$game_id';\n";
    }

    #
    # Fielding
    #
    if (check_disp(3, $boxscore_disp)) {
      print "\n# Fielding\n";
      my @players = uniq(@{$info{'batters'}}, @{$info{'pitchers'}});
      foreach my $remote_id (@players) {
        my %player = %{$info{'players'}{"ID$remote_id"}};

        my %stats = %{$player{'gameStats'}{'fielding'}};
        # Determine the stats
        my @stat_cols = ( );
        my @stat = ( );
        foreach my $col ( @{$col_maps{'fielding'}} ) {
          next if !defined($stats{$$col{'remote'}});
          push @stat_cols, $$col{'local'};
          push @stat, $stats{$$col{'remote'}};
        }
        my $calc;
        # Calc: TC
        push @stat_cols, 'tc';
        my $tc = convert_undef_zero($stats{'putOuts'}) + convert_undef_zero($stats{'assists'}) + convert_undef_zero($stats{'errors'});
        push @stat, $tc;
        # Calc: DP
        $calc = boxscore_extra_info($game_info_split{$$t{'type_rem'}}{'dp'}, $player{'name'});
        if (defined($calc)) {
          push @stat_cols, 'dp';
          push @stat, $calc;
        }

        print "# $player{'name'}{'first'} $player{'name'}{'last'} #$player{'shirtNum'}, $player{'position'}\n";
        print "INSERT INTO `SPORTS_MLB_PLAYERS_GAME_FIELDING` (`season`, `game_type`, `game_id`, `team_id`, `jersey`, `" . join("`, `", @stat_cols), "`)
  VALUES ('$season', '$game_type', '$game_id', '$$t{'team_id'}', '$player{'shirtNum'}', '" . join("', '", @stat), "');\n";
      }

      # Global updates: player_id from remote, FPct
      print "\n# In-line updates\n";
      print "UPDATE `SPORTS_MLB_PLAYERS_GAME_FIELDING` AS `STAT`
JOIN `SPORTS_MLB_GAME_ROSTERS` AS `ROSTER`
  ON (`ROSTER`.`season` = `STAT`.`season`
  AND `ROSTER`.`game_type` = `STAT`.`game_type`
  AND `ROSTER`.`game_id` = `STAT`.`game_id`
  AND `ROSTER`.`team_id` = `STAT`.`team_id`
  AND `ROSTER`.`jersey` = `STAT`.`jersey`)
SET `STAT`.`player_id` = `ROSTER`.`player_id`,
    `STAT`.`pct` = IF((`STAT`.`a` + `STAT`.`po` + `STAT`.`e`) > 0, (`STAT`.`a` + `STAT`.`po`) / (`STAT`.`a` + `STAT`.`po` + `STAT`.`e`), NULL)
WHERE `STAT`.`season` = '$season'
AND   `STAT`.`game_type` = '$game_type'
AND   `STAT`.`game_id` = '$game_id';\n";
    }

    print "\n";
  }

  ## Team totals
  print "## Team totals\n";

  #
  # Batters
  #
  if (check_disp(1, $boxscore_disp)) {
    print "# Batting\n";
    my $t_obp = 'IF(SUM(`ab` + `bb` + `hbp` + `sac_fly`) > 0, SUM(`h` + `bb` + `hbp`) / SUM(`ab` + `bb` + `hbp` + `sac_fly`), NULL)';
    my $t_slg = 'IF(SUM(`ab`) > 0, SUM(`tb`) / SUM(`ab`), NULL)';
    print "INSERT INTO `SPORTS_MLB_TEAMS_GAME_BATTING` (`season`, `game_type`, `game_id`, `team_id`, `ab`, `pa`, `h`, `1b`, `2b`, `3b`, `xbh`, `bb`, `ibb`, `r`, `hr`, `rbi`, `sb`, `cs`, `po`, `avg`, `obp`, `tb`, `slg`, `ops`, `sac_fly`, `sac_hit`, `hbp`, `k`, `go`, `fo`, `gidp`, `lob`)
  SELECT `season`, `game_type`, `game_id`, `team_id`,
         SUM(`ab`) AS `ab`,
         SUM(`pa`) AS `pa`,
         SUM(`h`) AS `h`,
         SUM(`1b`) AS `1b`,
         SUM(`2b`) AS `2b`,
         SUM(`3b`) AS `3b`,
         SUM(`xbh`) AS `xbh`,
         SUM(`bb`) AS `bb`,
         SUM(`ibb`) AS `ibb`,
         SUM(`r`) AS `r`,
         SUM(`hr`) AS `hr`,
         SUM(`rbi`) AS `rbi`,
         SUM(`sb`) AS `sb`,
         SUM(`cs`) AS `cs`,
         SUM(`po`) AS `po`,
         IF(SUM(`ab`) > 0, SUM(`h`) / SUM(`ab`), NULL) AS `avg`,
         $t_obp AS `obp`,
         SUM(`tb`) AS `tb`,
         $t_slg AS `slg`,
         IF(($t_obp IS NOT NULL) OR ($t_slg IS NOT NULL), IFNULL($t_obp, 0) + IFNULL($t_slg, 0), NULL) AS `ops`,
         SUM(`sac_fly`) AS `sac_fly`,
         SUM(`sac_hit`) AS `sac_hit`,
         SUM(`hbp`) AS `hbp`,
         SUM(`k`) AS `k`,
         SUM(`go`) AS `go`,
         SUM(`fo`) AS `fo`,
         SUM(`gidp`) AS `gidp`,
         SUM(`lob`) AS `lob`
  FROM `SPORTS_MLB_PLAYERS_GAME_BATTING`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  AND   `game_id` = '$game_id'
  GROUP BY `season`, `game_type`, `game_id`, `team_id`
ON DUPLICATE KEY UPDATE `ab` = VALUES(`ab`),
                        `pa` = VALUES(`pa`),
                        `h` = VALUES(`h`),
                        `1b` = VALUES(`1b`),
                        `2b` = VALUES(`2b`),
                        `3b` = VALUES(`3b`),
                        `xbh` = VALUES(`xbh`),
                        `bb` = VALUES(`bb`),
                        `ibb` = VALUES(`ibb`),
                        `r` = VALUES(`r`),
                        `hr` = VALUES(`hr`),
                        `rbi` = VALUES(`rbi`),
                        `sb` = VALUES(`sb`),
                        `cs` = VALUES(`cs`),
                        `po` = VALUES(`po`),
                        `avg` = VALUES(`avg`),
                        `obp` = VALUES(`obp`),
                        `tb` = VALUES(`tb`),
                        `slg` = VALUES(`slg`),
                        `ops` = VALUES(`ops`),
                        `sac_fly` = VALUES(`sac_fly`),
                        `sac_hit` = VALUES(`sac_hit`),
                        `hbp` = VALUES(`hbp`),
                        `k` = VALUES(`k`),
                        `go` = VALUES(`go`),
                        `fo` = VALUES(`fo`),
                        `gidp` = VALUES(`gidp`),
                        `lob` = VALUES(`lob`);\n\n";
  }

  #
  # Pitchers
  #
  if (check_disp(2, $boxscore_disp)) {
    print "# Pitching\n";
    print "INSERT INTO `SPORTS_MLB_TEAMS_GAME_PITCHING` (`season`, `game_type`, `game_id`, `team_id`, `ip`, `out`, `bf`, `pt`, `b`, `s`, `k`, `h`, `hr`, `bb`, `ibb`, `wp`, `go`, `fo`, `go_fo`, `r`, `ra`, `er`, `era`, `ur`, `ura`, `ir`, `ira`, `whip`, `bk`, `sb`, `cs`, `po`, `w`, `l`, `sv`, `hld`, `bs`, `svo`, `cg`, `sho`, `qs`, `game_score`)
  SELECT `season`, `game_type`, `game_id`, `team_id`,
         CONCAT(FLOOR(SUM(`out`) / 3), '.', SUM(`out`) % 3) AS `ip`,
         SUM(`out`) AS `out`,
         SUM(`bf`) AS `bf`,
         SUM(`pt`) AS `pt`,
         SUM(`b`) AS `b`,
         SUM(`s`) AS `s`,
         SUM(`k`) AS `k`,
         SUM(`h`) AS `h`,
         SUM(`hr`) AS `hr`,
         SUM(`bb`) AS `bb`,
         SUM(`ibb`) AS `ibb`,
         SUM(`wp`) AS `wp`,
         SUM(`go`) AS `go`,
         SUM(`fo`) AS `fo`,
         IF(SUM(`fo`) > 0, SUM(`go`) / SUM(`fo`), NULL) AS `go_fo`,
         SUM(`r`) AS `r`,
         IF(SUM(`out`) > 0, (SUM(`r`) / SUM(`out`)) * 27, NULL) AS `ra`,
         SUM(`er`) AS `er`,
         IF(SUM(`out`) > 0, (SUM(`er`) / SUM(`out`)) * 27, NULL) AS `era`,
         SUM(`ur`) AS `ur`,
         IF(SUM(`out`) > 0, (SUM(`ur`) / SUM(`out`)) * 27, NULL) AS `ura`,
         SUM(`ir`) AS `ir`,
         SUM(`ira`) AS `ira`,
         IF(SUM(`out`) > 0, SUM(`bb` + `h`) / (SUM(`out`) / 3), NULL) AS `whip`,
         SUM(`bk`) AS `bk`,
         SUM(`sb`) AS `sb`,
         SUM(`cs`) AS `cs`,
         SUM(`po`) AS `po`,
         SUM(`w`) AS `w`,
         SUM(`l`) AS `l`,
         SUM(`sv`) AS `sv`,
         SUM(`hld`) AS `hld`,
         SUM(`bs`) AS `bs`,
         SUM(`svo`) AS `svo`,
         SUM(`cg`) AS `cg`,
         SUM(`sho`) AS `sho`,
         SUM(`qs`) AS `qs`,
         IF(SUM(`game_score` IS NOT NULL) > 0, SUM(IFNULL(`game_score`, 0)) / SUM(`game_score` IS NOT NULL), NULL) AS `game_score`
  FROM `SPORTS_MLB_PLAYERS_GAME_PITCHING`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  AND   `game_id` = '$game_id'
  GROUP BY `season`, `game_type`, `game_id`, `team_id`
ON DUPLICATE KEY UPDATE `ip` = VALUES(`ip`),
                        `out` = VALUES(`out`),
                        `bf` = VALUES(`bf`),
                        `pt` = VALUES(`pt`),
                        `b` = VALUES(`b`),
                        `s` = VALUES(`s`),
                        `k` = VALUES(`k`),
                        `h` = VALUES(`h`),
                        `hr` = VALUES(`hr`),
                        `bb` = VALUES(`bb`),
                        `ibb` = VALUES(`ibb`),
                        `wp` = VALUES(`wp`),
                        `go` = VALUES(`go`),
                        `fo` = VALUES(`fo`),
                        `go_fo` = VALUES(`go_fo`),
                        `r` = VALUES(`r`),
                        `ra` = VALUES(`ra`),
                        `er` = VALUES(`er`),
                        `era` = VALUES(`era`),
                        `ur` = VALUES(`ur`),
                        `ura` = VALUES(`ura`),
                        `ir` = VALUES(`ir`),
                        `ira` = VALUES(`ira`),
                        `whip` = VALUES(`whip`),
                        `bk` = VALUES(`bk`),
                        `sb` = VALUES(`sb`),
                        `cs` = VALUES(`cs`),
                        `po` = VALUES(`po`),
                        `w` = VALUES(`w`),
                        `l` = VALUES(`l`),
                        `sv` = VALUES(`sv`),
                        `hld` = VALUES(`hld`),
                        `bs` = VALUES(`bs`),
                        `svo` = VALUES(`svo`),
                        `cg` = VALUES(`cg`),
                        `sho` = VALUES(`sho`),
                        `qs` = VALUES(`qs`),
                        `game_score` = VALUES(`game_score`);\n\n";
  }

  #
  # Fielding
  #
  if (check_disp(3, $boxscore_disp)) {
    print "# Fielding\n";
    # Team DP isn't actually composite of the players - three players on the same DP gives a total of three, not one - so get the number from the game info?
    print "INSERT INTO `SPORTS_MLB_TEAMS_GAME_FIELDING` (`season`, `game_type`, `game_id`, `team_id`, `tc`, `po`, `a`, `e`, `pct`, `dp`)
  SELECT `season`, `game_type`, `game_id`, `team_id`,
         SUM(`tc`) AS `tc`,
         SUM(`po`) AS `po`,
         SUM(`a`) AS `a`,
         SUM(`e`) AS `e`,
         IF(SUM(`po` + `a` + `e`) > 0, SUM(`po` + `a`) / SUM(`po` + `a` + `e`), NULL) AS `pct`,
         IF(`team_id` = '$$game_info{'home_id'}', '$num_dp{'home'}', '$num_dp{'visitor'}') AS `dp`
  FROM `SPORTS_MLB_PLAYERS_GAME_FIELDING`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  AND   `game_id` = '$game_id'
  GROUP BY `season`, `game_type`, `game_id`, `team_id`
ON DUPLICATE KEY UPDATE `tc` = VALUES(`tc`),
                        `po` = VALUES(`po`),
                        `a` = VALUES(`a`),
                        `e` = VALUES(`e`),
                        `pct` = VALUES(`pct`),
                        `dp` = VALUES(`dp`);\n\n";
  }

  ## Year-to-dates
  print "## Year-to-date\n";
  # Prep: the temporary table we'll store our game list in
  print "## Query prep...\n";

  print "# Our game's players
DROP TEMPORARY TABLE IF EXISTS `tmp_game_players`;
CREATE TEMPORARY TABLE `tmp_game_players` (
  `player_id` MEDIUMINT UNSIGNED,
  `team_id` VARCHAR(3),
  PRIMARY KEY (`player_id`)
) ENGINE = MEMORY
  SELECT `player_id`, `team_id`
  FROM `SPORTS_MLB_GAME_ROSTERS`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  AND   `game_id` = '$game_id';\n\n";

  print "# Applicable games
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_games`;
CREATE TEMPORARY TABLE `tmp_y2d_games` (
  `season` YEAR,
  `game_type` ENUM('regular', 'playoff'),
  `game_id` SMALLINT UNSIGNED,
  `home` VARCHAR(3),
  `visitor` VARCHAR(3),
  PRIMARY KEY (`season`, `game_type`, `game_id`)
) ENGINE = MEMORY
  SELECT `SCHED_Y2D`.`season`, `SCHED_Y2D`.`game_type`, `SCHED_Y2D`.`game_id`,
         `SCHED_Y2D`.`home`, `SCHED_Y2D`.`visitor`
  FROM `SPORTS_MLB_SCHEDULE` AS `SCHED_GAME`
  JOIN `SPORTS_MLB_SCHEDULE` AS `SCHED_Y2D`
    ON (`SCHED_Y2D`.`season` = `SCHED_GAME`.`season`
    AND `SCHED_Y2D`.`game_type` = `SCHED_GAME`.`game_type`
    AND (`SCHED_Y2D`.`game_id` = '$game_id'
      OR ((`SCHED_Y2D`.`game_date` < `SCHED_GAME`.`game_date`
           OR (`SCHED_Y2D`.`game_date` = `SCHED_GAME`.`game_date` AND `SCHED_Y2D`.`game_time` <= `SCHED_GAME`.`game_time`)))
       AND `SCHED_Y2D`.`status` = 'F'))
  WHERE `SCHED_GAME`.`season` = '$season'
  AND   `SCHED_GAME`.`game_type` = '$game_type'
  AND   `SCHED_GAME`.`game_id` = '$game_id';\n\n";

  print "# Expand to include rosters
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_list`;
CREATE TEMPORARY TABLE `tmp_y2d_list` (
  `season` YEAR,
  `game_type` ENUM('regular', 'playoff'),
  `game_id` SMALLINT UNSIGNED,
  `team_id` VARCHAR(3),
  `player_id` MEDIUMINT UNSIGNED,
  `is_team` TINYINT UNSIGNED,
  `played` TINYINT UNSIGNED,
  `started` TINYINT UNSIGNED,
  `pos` VARCHAR(2) DEFAULT NULL,
  `pos_alt1` VARCHAR(2) DEFAULT NULL,
  `pos_alt2` VARCHAR(2) DEFAULT NULL,
  `pos_alt3` VARCHAR(2) DEFAULT NULL,
  `pos_alt4` VARCHAR(2) DEFAULT NULL,
  `pos_alt5` VARCHAR(2) DEFAULT NULL,
  `pos_alt6` VARCHAR(2) DEFAULT NULL,
  `pos_alt7` VARCHAR(2) DEFAULT NULL,
  `pos_alt8` VARCHAR(2) DEFAULT NULL,
  PRIMARY KEY (`season`, `game_type`, `game_id`, `team_id`, `player_id`)
) ENGINE = MEMORY
  SELECT `tmp_y2d_games`.`season`, `tmp_y2d_games`.`game_type`, `tmp_y2d_games`.`game_id`,
         `GAME_ROSTER`.`team_id`, `GAME_ROSTER`.`player_id`, (`tmp_game_players`.`team_id` = `GAME_ROSTER`.`team_id`) AS `is_team`,
         `GAME_ROSTER`.`played`, `GAME_ROSTER`.`started`,
         `GAME_ROSTER`.`pos`, `GAME_ROSTER`.`pos_alt1`, `GAME_ROSTER`.`pos_alt2`, `GAME_ROSTER`.`pos_alt3`, `GAME_ROSTER`.`pos_alt4`, `GAME_ROSTER`.`pos_alt5`, `GAME_ROSTER`.`pos_alt6`, `GAME_ROSTER`.`pos_alt7`, `GAME_ROSTER`.`pos_alt8`
  FROM `tmp_game_players`
  JOIN `SPORTS_MLB_GAME_ROSTERS` AS `GAME_ROSTER`
    ON (`GAME_ROSTER`.`player_id` = `tmp_game_players`.`player_id`)
  JOIN `tmp_y2d_games`
    ON (`tmp_y2d_games`.`season` = `GAME_ROSTER`.`season`
    AND `tmp_y2d_games`.`game_type` = `GAME_ROSTER`.`game_type`
    AND `tmp_y2d_games`.`game_id` = `GAME_ROSTER`.`game_id`);\n\n";

  #
  # Misc info
  #
  print "# Misc season info\n";

  print "# Determine players who are part of multiple teams this season
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_list_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_list_multiple` (
  `player_id` MEDIUMINT UNSIGNED,
  PRIMARY KEY (`player_id`)
) ENGINE = MEMORY
  SELECT `player_id`
  FROM `tmp_y2d_list`
  GROUP BY `player_id`
  HAVING COUNT(DISTINCT `team_id`) > 1;\n\n";

  # Setup the optimisation
  print "# Gather stats for every relevant player
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_misc_data`;
CREATE TEMPORARY TABLE `tmp_y2d_misc_data` LIKE `tmp_y2d_list`;
ALTER TABLE `tmp_y2d_misc_data` ADD KEY `group_by_team` (`season`, `game_type`, `team_id`, `player_id`) USING BTREE, ADD KEY `group_by_player` (`season`, `game_type`, `player_id`) USING BTREE;
INSERT INTO `tmp_y2d_misc_data`
  SELECT *
  FROM `tmp_y2d_list`;\n\n";

  print "# Create a specific version for those player on multiple teams
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_misc_data_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_misc_data_multiple` LIKE `tmp_y2d_misc_data`;
INSERT INTO `tmp_y2d_misc_data_multiple`
  SELECT `tmp_y2d_misc_data`.*
  FROM `tmp_y2d_list_multiple`
  JOIN `tmp_y2d_misc_data`
    ON (`tmp_y2d_misc_data`.`player_id` = `tmp_y2d_list_multiple`.`player_id`);\n\n";

  print "# Create the temporary table we'll store our season stats in
DROP TEMPORARY TABLE IF EXISTS `tmp_misc_y2d`;
CREATE TEMPORARY TABLE `tmp_misc_y2d` LIKE `SPORTS_MLB_PLAYERS_SEASON_MISC`;
ALTER TABLE `tmp_misc_y2d` ENGINE = MEMORY;\n\n";

  # Run
  for (my $i = 0; $i < 2; $i++) {
    # Form our nuances
    my $comment; my $table; my $team_id; my $where; my $group_team;
    if (!$i) {
      # Per-team
      $comment = 'Season stats per team';
      $table = 'tmp_y2d_misc_data';
      $team_id = "`$table`.`team_id`";
      $where = "\n  WHERE `is_team` = '1'";
      $group_team = ", `$table`.`team_id`";
    } else {
      # League totals
      $comment = 'Season stats, across all teams';
      $table = 'tmp_y2d_misc_data_multiple';
      $team_id = '\'_ML\' AS `team_id`';
      $where = '';
      $group_team = '';
    }
    # Display
    print "# $comment
INSERT INTO `tmp_misc_y2d` (`season`, `season_type`, `player_id`, `team_id`, `is_totals`, `gp`, `gs`, `gs_C`, `ga_C`, `gs_1B`, `ga_1B`, `gs_2B`, `ga_2B`, `gs_SS`, `ga_SS`, `gs_3B`, `ga_3B`, `gs_LF`, `ga_LF`, `gs_CF`, `ga_CF`, `gs_RF`, `ga_RF`, `gs_DH`, `ga_DH`, `ga_PH`, `ga_PR`, `gs_SP`, `ga_RP`)
  SELECT '$season' AS `season`, '$game_type' AS `season_type`, `$table`.`player_id`, $team_id, '1' AS `is_totals`,
         SUM(`played`) AS `gp`,
         SUM(`started`) AS `gs`,
         SUM(`pos` = 'C') AS `gs_C`,
         SUM('C' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_C`,
         SUM(`pos` = '1B') AS `gs_1B`,
         SUM('1B' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_1B`,
         SUM(`pos` = '2B') AS `gs_2B`,
         SUM('2B' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_2B`,
         SUM(`pos` = 'SS') AS `gs_SS`,
         SUM('SS' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_SS`,
         SUM(`pos` = '3B') AS `gs_3B`,
         SUM('3B' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_3B`,
         SUM(`pos` = 'LF') AS `gs_LF`,
         SUM('LF' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_LF`,
         SUM(`pos` = 'CF') AS `gs_CF`,
         SUM('CF' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_CF`,
         SUM(`pos` = 'RF') AS `gs_RF`,
         SUM('RF' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_RF`,
         SUM(`pos` = 'DH') AS `gs_DH`,
         SUM('DH' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_DH`,
         SUM('PH' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_PH`,
         SUM('PR' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_PR`,
         SUM(`pos` = 'SP') AS `gs_SP`,
         SUM('RP' IN (`pos_alt1`, `pos_alt2`, `pos_alt3`, `pos_alt4`, `pos_alt5`, `pos_alt6`, `pos_alt7`, `pos_alt8`)) AS `gp_RP`
  FROM `$table`$where
  GROUP BY `$table`.`season`, `$table`.`game_type`$group_team, `$table`.`player_id`;\n\n";
  }

  print "# Some extra calcs
UPDATE `tmp_misc_y2d`
SET `gp_C` = `gs_C` + `ga_C`,
    `gp_1B` = `gs_1B` + `ga_1B`,
    `gp_2B` = `gs_2B` + `ga_2B`,
    `gp_SS` = `gs_SS` + `ga_SS`,
    `gp_3B` = `gs_3B` + `ga_3B`,
    `gp_LF` = `gs_LF` + `ga_LF`,
    `gp_CF` = `gs_CF` + `ga_CF`,
    `gp_RF` = `gs_RF` + `ga_RF`,
    `gp_DH` = `gs_DH` + `ga_DH`,
    `gp_P` = `gs_SP` + `ga_RP`;\n\n";

  print "# Preserve
INSERT INTO `SPORTS_MLB_PLAYERS_SEASON_MISC`
  SELECT *
  FROM `tmp_misc_y2d`
ON DUPLICATE KEY UPDATE `gp` = VALUES(`gp`),
                        `gs` = VALUES(`gs`),
                        `gp_C` = VALUES(`gp_C`),
                        `gs_C` = VALUES(`gs_C`),
                        `ga_C` = VALUES(`ga_C`),
                        `gp_1B` = VALUES(`gp_1B`),
                        `gs_1B` = VALUES(`gs_1B`),
                        `ga_1B` = VALUES(`ga_1B`),
                        `gp_2B` = VALUES(`gp_2B`),
                        `gs_2B` = VALUES(`gs_2B`),
                        `ga_2B` = VALUES(`ga_2B`),
                        `gp_SS` = VALUES(`gp_SS`),
                        `gs_SS` = VALUES(`gs_SS`),
                        `ga_SS` = VALUES(`ga_SS`),
                        `gp_3B` = VALUES(`gp_3B`),
                        `gs_3B` = VALUES(`gs_3B`),
                        `ga_3B` = VALUES(`ga_3B`),
                        `gp_LF` = VALUES(`gp_LF`),
                        `gs_LF` = VALUES(`gs_LF`),
                        `ga_LF` = VALUES(`ga_LF`),
                        `gp_CF` = VALUES(`gp_CF`),
                        `gs_CF` = VALUES(`gs_CF`),
                        `ga_CF` = VALUES(`ga_CF`),
                        `gp_RF` = VALUES(`gp_RF`),
                        `gs_RF` = VALUES(`gs_RF`),
                        `ga_RF` = VALUES(`ga_RF`),
                        `gp_DH` = VALUES(`gp_DH`),
                        `gs_DH` = VALUES(`gs_DH`),
                        `ga_DH` = VALUES(`ga_DH`),
                        `ga_PH` = VALUES(`ga_PH`),
                        `ga_PR` = VALUES(`ga_PR`),
                        `gp_P` = VALUES(`gp_P`),
                        `gs_SP` = VALUES(`gs_SP`),
                        `ga_RP` = VALUES(`ga_RP`);\n\n";

  print "# Ensure the is_totals column is correct for those on multiple teams
UPDATE `tmp_y2d_list_multiple` AS `MULT`
JOIN `SPORTS_MLB_PLAYERS_SEASON_MISC` AS `STATS`
  ON (`STATS`.`season` = '$season'
  AND `STATS`.`season_type` = '$game_type'
  AND `STATS`.`player_id` = `MULT`.`player_id`)
SET `STATS`.`is_totals` = (`STATS`.`team_id` = '_ML');\n\n";

  #
  # Batters
  #
  if (check_disp(1, $boxscore_disp)) {
    print "# Batting\n";

    # Setup the optimisation
    print "# Gather stats for every relevant player
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_batting_data`;
CREATE TEMPORARY TABLE `tmp_y2d_batting_data` LIKE `SPORTS_MLB_PLAYERS_GAME_BATTING`;
ALTER TABLE `tmp_y2d_batting_data` ENGINE = MEMORY, ADD COLUMN `is_team` TINYINT UNSIGNED, DROP PRIMARY KEY, ADD KEY `where` (`is_team`) USING BTREE, ADD KEY `group_by_team` (`season`, `game_type`, `team_id`, `player_id`) USING BTREE, ADD KEY `group_by_player` (`season`, `game_type`, `player_id`) USING BTREE;
INSERT INTO `tmp_y2d_batting_data`
  SELECT `GAMES_Y2D`.*, `tmp_y2d_list`.`is_team`
  FROM `tmp_y2d_list`
  STRAIGHT_JOIN `SPORTS_MLB_PLAYERS_GAME_BATTING` AS `GAMES_Y2D`
    FORCE INDEX (`by_player`)
    ON (`GAMES_Y2D`.`season` = `tmp_y2d_list`.`season`
    AND `GAMES_Y2D`.`game_type` = `tmp_y2d_list`.`game_type`
    AND `GAMES_Y2D`.`game_id` = `tmp_y2d_list`.`game_id`
    AND `GAMES_Y2D`.`team_id` = `tmp_y2d_list`.`team_id`
    AND `GAMES_Y2D`.`player_id` = `tmp_y2d_list`.`player_id`);\n\n";

  print "# Determine players who are part of multiple teams this season
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_list_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_list_multiple` (
  `player_id` MEDIUMINT UNSIGNED,
  PRIMARY KEY (`player_id`)
) ENGINE = MEMORY
  SELECT `player_id`
  FROM `tmp_y2d_batting_data`
  GROUP BY `player_id`
  HAVING COUNT(DISTINCT `team_id`) > 1;\n\n";

    print "# Create a specific version for those player on multiple teams
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_batting_data_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_batting_data_multiple` LIKE `tmp_y2d_batting_data`;
INSERT INTO `tmp_y2d_batting_data_multiple`
  SELECT `tmp_y2d_batting_data`.*
  FROM `tmp_y2d_list_multiple`
  JOIN `tmp_y2d_batting_data`
    ON (`tmp_y2d_batting_data`.`player_id` = `tmp_y2d_list_multiple`.`player_id`);\n\n";

    print "# Create the temporary table we'll store our season stats in
DROP TEMPORARY TABLE IF EXISTS `tmp_batting_y2d`;
CREATE TEMPORARY TABLE `tmp_batting_y2d` LIKE `SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D`;
ALTER TABLE `tmp_batting_y2d` ENGINE = MEMORY;\n\n";

    # Run
    my $y_obp = 'IF(SUM(`ab` + `bb` + `hbp` + `sac_fly`) > 0, SUM(`h` + `bb` + `hbp`) / SUM(`ab` + `bb` + `hbp` + `sac_fly`), NULL)';
    my $y_slg = 'IF(SUM(`ab`) > 0, SUM(`tb`) / SUM(`ab`), NULL)';
    for (my $i = 0; $i < 2; $i++) {
      # Form our nuances
      my $comment; my $table; my $team_id; my $group_team;
      if (!$i) {
        # Per-team
        $comment = 'Season stats per team';
        $table = 'tmp_y2d_batting_data';
        $team_id = "`$table`.`team_id`";
        $group_team = ", `$table`.`team_id`";
      } else {
        # League totals
        $comment = 'Season stats, across all teams';
        $table = 'tmp_y2d_batting_data_multiple';
        $team_id = '\'_ML\' AS `team_id`';
        $group_team = '';
      }
      # Display
      print "# $comment
INSERT INTO `tmp_batting_y2d` (`season`, `game_type`, `game_id`, `game_time_ref`, `team_id`, `player_id`, `is_totals`, `ab`, `pa`, `h`, `1b`, `2b`, `3b`, `xbh`, `bb`, `ibb`, `r`, `hr`, `rbi`, `sb`, `cs`, `po`, `avg`, `obp`, `tb`, `slg`, `ops`, `sac_fly`, `sac_hit`, `hbp`, `k`, `go`, `fo`, `gidp`, `lob`)
  SELECT '$season' AS `season`, '$game_type' AS `game_type`, '$game_id' AS `game_id`, '$$game_info{'game_time_ref'}' AS `game_time_ref`, $team_id, `$table`.`player_id`, '1' AS `is_totals`,
         SUM(`ab`) AS `ab`,
         SUM(`pa`) AS `pa`,
         SUM(`h`) AS `h`,
         SUM(`1b`) AS `1b`,
         SUM(`2b`) AS `2b`,
         SUM(`3b`) AS `3b`,
         SUM(`xbh`) AS `xbh`,
         SUM(`bb`) AS `bb`,
         SUM(`ibb`) AS `ibb`,
         SUM(`r`) AS `r`,
         SUM(`hr`) AS `hr`,
         SUM(`rbi`) AS `rbi`,
         SUM(`sb`) AS `sb`,
         SUM(`cs`) AS `cs`,
         SUM(`po`) AS `po`,
         IF(SUM(`ab`) > 0, SUM(`h`) / SUM(`ab`), NULL) AS `avg`,
         $y_obp AS `obp`,
         SUM(`tb`) AS `tb`,
         $y_slg AS `slg`,
         IF(($y_obp IS NOT NULL) OR ($y_slg IS NOT NULL), IFNULL($y_obp, 0) + IFNULL($y_slg, 0), NULL) AS `ops`,
         SUM(`sac_fly`) AS `sac_fly`,
         SUM(`sac_hit`) AS `sac_hit`,
         SUM(`hbp`) AS `hbp`,
         SUM(`k`) AS `k`,
         SUM(`go`) AS `go`,
         SUM(`fo`) AS `fo`,
         SUM(`gidp`) AS `gidp`,
         SUM(`lob`) AS `lob`
  FROM `$table`
  GROUP BY `$table`.`season`, `$table`.`game_type`$group_team, `$table`.`player_id`;\n\n";
    }

    print "# Ensure the is_totals column is correct for those on multiple teams
UPDATE `tmp_y2d_list_multiple` AS `MULT`
JOIN `tmp_batting_y2d` AS `STATS`
  ON (`STATS`.`player_id` = `MULT`.`player_id`)
SET `STATS`.`is_totals` = (`STATS`.`team_id` = '_ML');\n\n";

    print "# Store
INSERT INTO `SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D`
  SELECT * FROM `tmp_batting_y2d`;\n\n";

    # Copy to the annual table
    print "INSERT INTO `SPORTS_MLB_PLAYERS_SEASON_BATTING` (`season`, `season_type`, `player_id`, `team_id`, `is_totals`, `ab`, `pa`, `h`, `1b`, `2b`, `3b`, `xbh`, `bb`, `ibb`, `r`, `hr`, `rbi`, `sb`, `cs`, `po`, `avg`, `obp`, `tb`, `slg`, `ops`, `sac_fly`, `sac_hit`, `hbp`, `k`, `go`, `fo`, `gidp`, `lob`)
  SELECT `season`, `game_type` AS `season_type`, `player_id`, `team_id`, `is_totals`, `ab`, `pa`, `h`, `1b`, `2b`, `3b`, `xbh`, `bb`, `ibb`, `r`, `hr`, `rbi`, `sb`, `cs`, `po`, `avg`, `obp`, `tb`, `slg`, `ops`, `sac_fly`, `sac_hit`, `hbp`, `k`, `go`, `fo`, `gidp`, `lob`
  FROM `tmp_batting_y2d`
ON DUPLICATE KEY UPDATE `is_totals` = VALUES(`is_totals`),
                        `ab` = VALUES(`ab`),
                        `pa` = VALUES(`pa`),
                        `h` = VALUES(`h`),
                        `1b` = VALUES(`1b`),
                        `2b` = VALUES(`2b`),
                        `3b` = VALUES(`3b`),
                        `xbh` = VALUES(`xbh`),
                        `bb` = VALUES(`bb`),
                        `ibb` = VALUES(`ibb`),
                        `r` = VALUES(`r`),
                        `hr` = VALUES(`hr`),
                        `rbi` = VALUES(`rbi`),
                        `sb` = VALUES(`sb`),
                        `cs` = VALUES(`cs`),
                        `po` = VALUES(`po`),
                        `avg` = VALUES(`avg`),
                        `obp` = VALUES(`obp`),
                        `tb` = VALUES(`tb`),
                        `slg` = VALUES(`slg`),
                        `ops` = VALUES(`ops`),
                        `sac_fly` = VALUES(`sac_fly`),
                        `sac_hit` = VALUES(`sac_hit`),
                        `hbp` = VALUES(`hbp`),
                        `k` = VALUES(`k`),
                        `go` = VALUES(`go`),
                        `fo` = VALUES(`fo`),
                        `gidp` = VALUES(`gidp`),
                        `lob` = VALUES(`lob`);\n\n";
  }

  #
  # Pitchers
  #
  if (check_disp(2, $boxscore_disp)) {
    print "# Pitching\n";

    # Setup the optimisation
    print "# Gather stats for every relevant player
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_pitching_data`;
CREATE TEMPORARY TABLE `tmp_y2d_pitching_data` LIKE `SPORTS_MLB_PLAYERS_GAME_PITCHING`;
ALTER TABLE `tmp_y2d_pitching_data` ENGINE = MEMORY, ADD COLUMN `is_team` TINYINT UNSIGNED, DROP PRIMARY KEY, ADD KEY `where` (`is_team`) USING BTREE, ADD KEY `group_by_team` (`season`, `game_type`, `team_id`, `player_id`) USING BTREE, ADD KEY `group_by_player` (`season`, `game_type`, `player_id`) USING BTREE;
INSERT INTO `tmp_y2d_pitching_data`
  SELECT `GAMES_Y2D`.*, `tmp_y2d_list`.`is_team`
  FROM `tmp_y2d_list`
  STRAIGHT_JOIN `SPORTS_MLB_PLAYERS_GAME_PITCHING` AS `GAMES_Y2D`
    FORCE INDEX (`by_player`)
    ON (`GAMES_Y2D`.`season` = `tmp_y2d_list`.`season`
    AND `GAMES_Y2D`.`game_type` = `tmp_y2d_list`.`game_type`
    AND `GAMES_Y2D`.`game_id` = `tmp_y2d_list`.`game_id`
    AND `GAMES_Y2D`.`team_id` = `tmp_y2d_list`.`team_id`
    AND `GAMES_Y2D`.`player_id` = `tmp_y2d_list`.`player_id`);\n\n";

  print "# Determine players who are part of multiple teams this season
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_list_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_list_multiple` (
  `player_id` MEDIUMINT UNSIGNED,
  PRIMARY KEY (`player_id`)
) ENGINE = MEMORY
  SELECT `player_id`
  FROM `tmp_y2d_pitching_data`
  GROUP BY `player_id`
  HAVING COUNT(DISTINCT `team_id`) > 1;\n\n";

    print "# Create a specific version for those player on multiple teams
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_pitching_data_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_pitching_data_multiple` LIKE `tmp_y2d_pitching_data`;
INSERT INTO `tmp_y2d_pitching_data_multiple`
  SELECT `tmp_y2d_pitching_data`.*
  FROM `tmp_y2d_list_multiple`
  JOIN `tmp_y2d_pitching_data`
    ON (`tmp_y2d_pitching_data`.`player_id` = `tmp_y2d_list_multiple`.`player_id`);\n\n";

    print "# Create the temporary table we'll store our season stats in
DROP TEMPORARY TABLE IF EXISTS `tmp_pitching_y2d`;
CREATE TEMPORARY TABLE `tmp_pitching_y2d` LIKE `SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D`;
ALTER TABLE `tmp_pitching_y2d` ENGINE = MEMORY;\n\n";

    # Run
    for (my $i = 0; $i < 2; $i++) {
      # Form our nuances
      my $comment; my $table; my $team_id; my $group_team;
      if (!$i) {
        # Per-team
        $comment = 'Season stats per team';
        $table = 'tmp_y2d_pitching_data';
        $team_id = "`$table`.`team_id`";
        $group_team = ", `$table`.`team_id`";
      } else {
        # League totals
        $comment = 'Season stats, across all teams';
        $table = 'tmp_y2d_pitching_data_multiple';
        $team_id = '\'_ML\' AS `team_id`';
        $group_team = '';
      }
      # Display
      print "# $comment
INSERT INTO `tmp_pitching_y2d` (`season`, `game_type`, `game_id`, `game_time_ref`, `team_id`, `player_id`, `is_totals`, `ip`, `out`, `bf`, `pt`, `b`, `s`, `k`, `h`, `hr`, `bb`, `ibb`, `wp`, `go`, `fo`, `go_fo`, `r`, `ra`, `er`, `era`, `ur`, `ura`, `ir`, `ira`, `whip`, `bk`, `sb`, `cs`, `po`, `w`, `l`, `sv`, `hld`, `bs`, `svo`, `cg`, `sho`, `qs`, `game_score`)
  SELECT '$season' AS `season`, '$game_type' AS `game_type`, '$game_id' AS `game_id`, '$$game_info{'game_time_ref'}' AS `game_time_ref`, $team_id, `$table`.`player_id`, '1' AS `is_totals`,
         CONCAT(FLOOR(SUM(`out`) / 3), '.', SUM(`out`) % 3) AS `ip`,
         SUM(`out`) AS `out`,
         SUM(`bf`) AS `bf`,
         SUM(`pt`) AS `pt`,
         SUM(`b`) AS `b`,
         SUM(`s`) AS `s`,
         SUM(`k`) AS `k`,
         SUM(`h`) AS `h`,
         SUM(`hr`) AS `hr`,
         SUM(`bb`) AS `bb`,
         SUM(`ibb`) AS `ibb`,
         SUM(`wp`) AS `wp`,
         SUM(`go`) AS `go`,
         SUM(`fo`) AS `fo`,
         IF(SUM(`fo`) > 0, SUM(`go`) / SUM(`fo`), NULL) AS `go_fo`,
         SUM(`r`) AS `r`,
         IF(SUM(`out`) > 0, (SUM(`r`) / SUM(`out`)) * 27, NULL) AS `ra`,
         SUM(`er`) AS `er`,
         IF(SUM(`out`) > 0, (SUM(`er`) / SUM(`out`)) * 27, NULL) AS `era`,
         SUM(`ur`) AS `ur`,
         IF(SUM(`out`) > 0, (SUM(`ur`) / SUM(`out`)) * 27, NULL) AS `ura`,
         SUM(`ir`) AS `ir`,
         SUM(`ira`) AS `ira`,
         IF(SUM(`out`) > 0, SUM(`bb` + `h`) / (SUM(`out`) / 3), NULL) AS `whip`,
         SUM(`bk`) AS `bk`,
         SUM(`sb`) AS `sb`,
         SUM(`cs`) AS `cs`,
         SUM(`po`) AS `po`,
         SUM(`w`) AS `w`,
         SUM(`l`) AS `l`,
         SUM(`sv`) AS `sv`,
         SUM(`hld`) AS `hld`,
         SUM(`bs`) AS `bs`,
         SUM(`svo`) AS `svo`,
         SUM(`cg`) AS `cg`,
         SUM(`sho`) AS `sho`,
         SUM(`qs`) AS `qs`,
         IF(SUM(`game_score` IS NOT NULL) > 0, SUM(IFNULL(`game_score`, 0)) / SUM(`game_score` IS NOT NULL), NULL) AS `game_score`
  FROM `$table`
  GROUP BY `$table`.`season`, `$table`.`game_type`$group_team, `$table`.`player_id`;\n\n";
    }

    print "# Ensure the is_totals column is correct for those on multiple teams
UPDATE `tmp_y2d_list_multiple` AS `MULT`
JOIN `tmp_pitching_y2d` AS `STATS`
  ON (`STATS`.`player_id` = `MULT`.`player_id`)
SET `STATS`.`is_totals` = (`STATS`.`team_id` = '_ML');\n\n";

    print "# Store
INSERT INTO `SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D`
  SELECT * FROM `tmp_pitching_y2d`;\n\n";

    # Copy to the annual table
    print "INSERT INTO `SPORTS_MLB_PLAYERS_SEASON_PITCHING` (`season`, `season_type`, `player_id`, `team_id`, `is_totals`, `ip`, `out`, `bf`, `pt`, `b`, `s`, `k`, `h`, `hr`, `bb`, `ibb`, `wp`, `go`, `fo`, `go_fo`, `r`, `ra`, `er`, `era`, `ur`, `ura`, `ir`, `ira`, `whip`, `bk`, `sb`, `cs`, `po`, `w`, `l`, `sv`, `hld`, `bs`, `svo`, `cg`, `sho`, `qs`, `game_score`)
  SELECT `season`, `game_type` AS `season_type`, `player_id`, `team_id`, `is_totals`, `ip`, `out`, `bf`, `pt`, `b`, `s`, `k`, `h`, `hr`, `bb`, `ibb`, `wp`, `go`, `fo`, `go_fo`, `r`, `ra`, `er`, `era`, `ur`, `ura`, `ir`, `ira`, `whip`, `bk`, `sb`, `cs`, `po`, `w`, `l`, `sv`, `hld`, `bs`, `svo`, `cg`, `sho`, `qs`, `game_score`
  FROM `tmp_pitching_y2d`
ON DUPLICATE KEY UPDATE `is_totals` = VALUES(`is_totals`),
                        `ip` = VALUES(`ip`),
                        `out` = VALUES(`out`),
                        `bf` = VALUES(`bf`),
                        `pt` = VALUES(`pt`),
                        `b` = VALUES(`b`),
                        `s` = VALUES(`s`),
                        `k` = VALUES(`k`),
                        `h` = VALUES(`h`),
                        `hr` = VALUES(`hr`),
                        `bb` = VALUES(`bb`),
                        `ibb` = VALUES(`ibb`),
                        `wp` = VALUES(`wp`),
                        `go` = VALUES(`go`),
                        `fo` = VALUES(`fo`),
                        `go_fo` = VALUES(`go_fo`),
                        `r` = VALUES(`r`),
                        `ra` = VALUES(`ra`),
                        `er` = VALUES(`er`),
                        `era` = VALUES(`era`),
                        `ur` = VALUES(`ur`),
                        `ura` = VALUES(`ura`),
                        `ir` = VALUES(`ir`),
                        `ira` = VALUES(`ira`),
                        `whip` = VALUES(`whip`),
                        `bk` = VALUES(`bk`),
                        `sb` = VALUES(`sb`),
                        `cs` = VALUES(`cs`),
                        `po` = VALUES(`po`),
                        `w` = VALUES(`w`),
                        `l` = VALUES(`l`),
                        `sv` = VALUES(`sv`),
                        `hld` = VALUES(`hld`),
                        `bs` = VALUES(`bs`),
                        `svo` = VALUES(`svo`),
                        `cg` = VALUES(`cg`),
                        `sho` = VALUES(`sho`),
                        `qs` = VALUES(`qs`),
                        `game_score` = VALUES(`game_score`);\n\n";
  }

  #
  # Fielding
  #
  if (check_disp(3, $boxscore_disp)) {
    print "# Fielding\n";

    # Setup the optimisation
    print "# Gather stats for every relevant player
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_fielding_data`;
CREATE TEMPORARY TABLE `tmp_y2d_fielding_data` LIKE `SPORTS_MLB_PLAYERS_GAME_FIELDING`;
ALTER TABLE `tmp_y2d_fielding_data` ENGINE = MEMORY, ADD COLUMN `is_team` TINYINT UNSIGNED, DROP PRIMARY KEY, ADD KEY `where` (`is_team`) USING BTREE, ADD KEY `group_by_team` (`season`, `game_type`, `team_id`, `player_id`) USING BTREE, ADD KEY `group_by_player` (`season`, `game_type`, `player_id`) USING BTREE;
INSERT INTO `tmp_y2d_fielding_data`
  SELECT `GAMES_Y2D`.*, `tmp_y2d_list`.`is_team`
  FROM `tmp_y2d_list`
  STRAIGHT_JOIN `SPORTS_MLB_PLAYERS_GAME_FIELDING` AS `GAMES_Y2D`
    FORCE INDEX (`by_player`)
    ON (`GAMES_Y2D`.`season` = `tmp_y2d_list`.`season`
    AND `GAMES_Y2D`.`game_type` = `tmp_y2d_list`.`game_type`
    AND `GAMES_Y2D`.`game_id` = `tmp_y2d_list`.`game_id`
    AND `GAMES_Y2D`.`team_id` = `tmp_y2d_list`.`team_id`
    AND `GAMES_Y2D`.`player_id` = `tmp_y2d_list`.`player_id`);\n\n";

  print "# Determine players who are part of multiple teams this season
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_list_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_list_multiple` (
  `player_id` MEDIUMINT UNSIGNED,
  PRIMARY KEY (`player_id`)
) ENGINE = MEMORY
  SELECT `player_id`
  FROM `tmp_y2d_fielding_data`
  GROUP BY `player_id`
  HAVING COUNT(DISTINCT `team_id`) > 1;\n\n";

    print "# Create a specific version for those player on multiple teams
DROP TEMPORARY TABLE IF EXISTS `tmp_y2d_fielding_data_multiple`;
CREATE TEMPORARY TABLE `tmp_y2d_fielding_data_multiple` LIKE `tmp_y2d_fielding_data`;
INSERT INTO `tmp_y2d_fielding_data_multiple`
  SELECT `tmp_y2d_fielding_data`.*
  FROM `tmp_y2d_list_multiple`
  JOIN `tmp_y2d_fielding_data`
    ON (`tmp_y2d_fielding_data`.`player_id` = `tmp_y2d_list_multiple`.`player_id`);\n\n";

    print "# Create the temporary table we'll store our season stats in
DROP TEMPORARY TABLE IF EXISTS `tmp_fielding_y2d`;
CREATE TEMPORARY TABLE `tmp_fielding_y2d` LIKE `SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D`;
ALTER TABLE `tmp_fielding_y2d` ENGINE = MEMORY;\n\n";

    # Run
    for (my $i = 0; $i < 2; $i++) {
      # Form our nuances
      my $comment; my $table; my $team_id; my $group_team;
      if (!$i) {
        # Per-team
        $comment = 'Season stats per team';
        $table = 'tmp_y2d_fielding_data';
        $team_id = "`$table`.`team_id`";
        $group_team = ", `$table`.`team_id`";
      } else {
        # League totals
        $comment = 'Season stats, across all teams';
        $table = 'tmp_y2d_fielding_data_multiple';
        $team_id = '\'_ML\' AS `team_id`';
        $group_team = '';
      }
      # Display
      print "# $comment
INSERT INTO `tmp_fielding_y2d` (`season`, `game_type`, `game_id`, `game_time_ref`, `team_id`, `player_id`, `is_totals`, `tc`, `po`, `a`, `e`, `pct`, `dp`)
  SELECT '$season' AS `season`, '$game_type' AS `game_type`, '$game_id' AS `game_id`, '$$game_info{'game_time_ref'}' AS `game_time_ref`, $team_id, `$table`.`player_id`, '1' AS `is_totals`,
         SUM(`tc`) AS `tc`,
         SUM(`po`) AS `po`,
         SUM(`a`) AS `a`,
         SUM(`e`) AS `e`,
         IF(SUM(`po` + `a` + `e`) > 0, SUM(`po` + `a`) / SUM(`po` + `a` + `e`), NULL) AS `pct`,
         SUM(`dp`) AS `dp`
  FROM `$table`
  GROUP BY `$table`.`season`, `$table`.`game_type`$group_team, `$table`.`player_id`;\n\n";
    }

    print "# Ensure the is_totals column is correct for those on multiple teams
UPDATE `tmp_y2d_list_multiple` AS `MULT`
JOIN `tmp_fielding_y2d` AS `STATS`
  ON (`STATS`.`player_id` = `MULT`.`player_id`)
SET `STATS`.`is_totals` = (`STATS`.`team_id` = '_ML');\n\n";

    print "# Store
INSERT INTO `SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D`
  SELECT * FROM `tmp_fielding_y2d`;\n\n";

    # Copy to the annual table
    print "INSERT INTO `SPORTS_MLB_PLAYERS_SEASON_FIELDING` (`season`, `season_type`, `player_id`, `team_id`, `is_totals`, `tc`, `po`, `a`, `e`, `pct`, `dp`)
  SELECT `season`, `game_type` AS `season_type`, `player_id`, `team_id`, `is_totals`, `tc`, `po`, `a`, `e`, `pct`, `dp`
  FROM `tmp_fielding_y2d`
ON DUPLICATE KEY UPDATE `is_totals` = VALUES(`is_totals`),
                        `tc` = VALUES(`tc`),
                        `po` = VALUES(`po`),
                        `a` = VALUES(`a`),
                        `e` = VALUES(`e`),
                        `pct` = VALUES(`pct`),
                        `dp` = VALUES(`dp`);\n\n";
  }
}

# Identify from the play-by-play, instances of defensive intereference
sub parse_boxscore_identify_defint {
  my ($play) = @_;

  if ($$play{'result'}{'event'} eq 'Catcher Interference') {
    my $batter = $$play{'matchup'}{'batter'};
    my $team = ($$play{'about'}{'halfInning'} eq 'top' ? 'away' : 'home');
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{"ID$batter"}{'gameStats'}{'batting'}{'defInt'} = 0
      if !defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{"ID$batter"}{'gameStats'}{'batting'}{'defInt'});
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{"ID$batter"}{'gameStats'}{'batting'}{'defInt'}++;
  }
}

# Convert a boxscore list of players into an array of details
sub parse_boxscore_gameinfo {
  my ($key, $opt) = @_;
  %$opt = ( ) if !defined($opt);
  $$opt{'key'} = '_all' if !defined($$opt{'key'});
  $$opt{'regex'} = '^([^\(]+)' if !defined($$opt{'regex'});
  $$opt{'num_A-B'} = 0 if !defined($$opt{'num_A-B'});

  my %ret = ( );
  if (defined($game_info_parsed{$$opt{'key'}}{$key})) {
    my $the_list = $game_info_parsed{$$opt{'key'}}{$key};
    $the_list =~ s/\.$//;
    foreach my $line (split('; ', $the_list)) {
      my ($name) = ($line =~ m/$$opt{'regex'}/);
      # How do we get numeric info out?
      if ($$opt{'num_A-B'}) {
        # A-B at the end
        my @nums = ($name =~ m/\s*?(\d+)\-(\d+)$/);
        $name =~ s/\s*?\d+\-\d+$//;
        trim(\$name);
        $ret{$name} = \@nums;
      } else {
        # Straight number at the end
        my $num = 1;
        if ($name =~ m/\s\d+$/) {
          ($num) = ($name =~ m/\s(\d+)$/);
          $name =~ s/\s+\d+$//;
        }
        trim(\$name);
        if (!defined($ret{$name})) {
          $ret{$name} = $num;
        } else {
          $ret{$name} += $num;
        }
      }
    }
  }
  return %ret;
}

# Pitching-specific IBB parsing
sub parse_boxscore_ibb_pitcher {
  my %ret = ( );
  if (defined($game_info_parsed{'_all'}{'ibb'})) {
    my $the_list = $game_info_parsed{'_all'}{'ibb'};
    $the_list =~ s/\.$//;
    $the_list =~ s/\), /); /g;
    foreach my $line (split('; ', $the_list)) {
      my ($batter, $pitcher) = ($line =~ m/^([^\(]+)\s+\(by ([^\)]+)\)\s*?$/);
      my $num = 1;
      $num = ($batter =~ m/\s(\d+)$/) if ($batter =~ m/\s\d+$/);
      $ret{$pitcher}  = 0 if !defined($ret{$pitcher});
      $ret{$pitcher} += $num;
    }
  }

  return %ret;
}

# Pitching-specfifc SB/CS parsing
sub parse_boxscore_baserunning_pitcher {
  my ($team, $stat) = @_;
  my %ret = ( );
  if (defined($game_info_parsed{$team}{'baserunning-' . $stat})) {
    # http://m.mlb.com/gameday/rays-vs-yankees/2016/04/22/447116#game=447116,game_state=final,game_tab=box
    # Convert an attempt/steal of home to a standardisable format
    $game_info_parsed{$team}{'baserunning-' . $stat} =~ s/home (off|by)/4th base $1/g;
    # Parse the list
    foreach my $player (split('; ', $game_info_parsed{$team}{'baserunning-' . $stat})) {
      my ($list) = ($player =~ m/\(\d+, (.+)\)/);
      my @atts = ( );
      my $c = ', ';
      foreach my $p (split($c, $list)) {
        if ($p =~ m/^\d/) {
          push @atts, $p;
        } else {
          $atts[$#atts] .= "$c$p";
        }
      }
      foreach my $att (@atts) {
        my ($pitcher) = ($att =~ m/(?:off|by) ([^\/]+)\//);
        $ret{$pitcher} = 0 if !defined($ret{$pitcher});
        $ret{$pitcher}++;
      }
    }
  }

  return %ret;
}

sub parse_boxscore_po_pitcher {
  my ($team) = @_;
  my %ret = ( );

  if (defined($game_info_parsed{$team}{'fielding-pickoffs'})) {
    # Strip all the info in brackets we don't use
    $game_info_parsed{$team}{'fielding-pickoffs'} =~ s/ \([^\)]+\)//g;
    # Use the "normal" counting method
    %ret = parse_boxscore_gameinfo('fielding-pickoffs', { 'key' => $team });
  }

  return %ret;
}

# Fielding-specific DP parsing for teams
sub parse_boxscore_dp_team {
  my ($team) = @_;
  if (defined($game_info_parsed{$team}{'fielding-dp'})) {
    my ($num) = ($game_info_parsed{$team}{'fielding-dp'} =~ m/^(\d+) \(/);
    return $num
      if defined($num);
    # Absence of a number, but existance of the info, implies just the one
    return 1;
  }

  return 0;
}

# Fielding-specific DP parsing for players
sub parse_boxscore_dp_fielder {
  my ($team) = @_;
  my %ret = ( );
  if (defined($game_info_parsed{$team}{'fielding-dp'})) {
    my ($list) = ($game_info_parsed{$team}{'fielding-dp'} =~ m/\((.+)\)\.$/);
    foreach my $dp (split('; ', $list)) {
      # Determine count
      my $num = 1;
      if ($dp =~ m/ \d+$/) {
        ($num) = ($dp =~ m/ (\d+)$/);
        $dp =~ s/ \d+$//;
      }
      # Identify players (bearing in mind, a player could appear multiple times!)
      my %players = ( );
      foreach my $player (split('-', $dp)) {
        next if defined($players{$player});
        trim(\$player);
        $ret{$player} = 0 if !defined($ret{$player});
        $ret{$player} += $num;
        $players{$player} = 1;
      }
    }
  }

  return %ret;
}

# Is a player included in game info?
sub boxscore_extra_info {
  my ($hash, $player, $alt_check) = @_;

  # Consider the fact there may be a discrepancy between the boxname and the game info name (usually character encoding)
  $alt_check = 0 if !defined($alt_check);
  $key_extra = ($alt_check ? '_alt' : '');

  # The easy test...
  if (defined($$hash{$$player{'boxname'.$key_extra}})) {
    return $$hash{$$player{'boxname'.$key_extra}};
  # A player with "SNAME, INITIAL"
  } elsif ($$player{'boxname'.$key_extra} =~ m/, / && defined($$hash{$$player{'last'.$key_extra}}) && !defined($$player{'no-last-only'})) {
    return $$hash{$$player{'last'.$key_extra}};
  }

  # No match on the boxname, so try a recursive call using the alternate name (characters stripped to latin equivalents)?
  if (!$alt_check) {
    $$player{'boxname_alt'} = convert_latin1($$player{'boxname'})
      if !defined($$player{'boxname_alt'});
    $$player{'last_alt'} = convert_latin1($$player{'last'})
      if !defined($$player{'last_alt'});
    return boxscore_extra_info($hash, $player, 1);
  }

  # No match, nothing relevant
  return undef;
}

# Return true to pacify the compiler
1;
