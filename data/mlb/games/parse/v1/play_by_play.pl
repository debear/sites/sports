#!/usr/bin/perl -w
# Tables used
@{$config{'tables'}{'play_by_play'}} = (
  'SPORTS_MLB_GAME_PLAYS',
  'SPORTS_MLB_GAME_ATBAT',
  'SPORTS_MLB_GAME_ATBAT_FLAGS',
  'SPORTS_MLB_GAME_ATBAT_PITCHES',
);

# PxP processing
sub parse_play_by_play {
  our %plays = ( );

  # Setup: initial fielding positions
  our %field_pos = ( );
  foreach my $team ('home','away') {
    my %team = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}};
    foreach my $remote_id (@{$team{'batters'}}) {
      my %player = %{$team{'players'}{'ID'.$remote_id}};
      if (defined($player{'gameStats'}{'batting'}{'battingOrder'}) && substr($player{'gameStats'}{'batting'}{'battingOrder'}, -2) eq '00') {
        my ($pos) = split('-', $player{'position'});
        $field_pos{$remote_id} = $pos;
      }
    }
  }
  benchmark_stamp('Play-by-Play: Setup Fielding Pos');

  # Setup: atBatIndex != array index (if we're passed an atBatIndex...)
  my $i = 0;
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    $plays{defined($$play{'about'}{'atBatIndex'}) ? $$play{'about'}{'atBatIndex'} : $i++} = $play;
  }
  benchmark_stamp('Play-by-Play: Setup atBatIndex Check');

  #
  # Pass 1: Team/Player Description Interpolation
  #
  # Step 1: Merge in to a single list (so we can interpolate each player once across all descrips)
  our %pbp_descrips = ( );
  my $inning = 0; our $play_id = 0; our @pbp_descrips = ();
  foreach my $inn (@{$$gamedata{'liveData'}{'plays'}{'playsByInning'}}) {
    $inning++;
    # Top
    foreach my $i (@{$$inn{'top'}}) {
      parse_play_by_play_descrip($plays{$i});
    }
    # Bottom
    foreach my $i (@{$$inn{'bottom'}}) {
      parse_play_by_play_descrip($plays{$i});
    }
  }
  my $pbp_descrips = parse_name_simplified(join('###', @pbp_descrips)); # Include name simplification for below comparisons
  parse_descrip_hacks(\$pbp_descrips); # For the joyful fudges we need to apply...
  # Step 2: Team interpolation on this string
  my $home = $$game_info{'home_id'};
  my $visitor = $$game_info{'visitor_id'};
  $pbp_descrips =~ s/$$game_info{'home_city_regex'} $$game_info{'home_regex'}/{{TEAM_$home}}/gi;
  $pbp_descrips =~ s/$$game_info{'home_regex'}/{{TEAM_$home}}/gi;
  $pbp_descrips =~ s/$$game_info{'visitor_city_regex'} $$game_info{'visitor_regex'}/{{TEAM_$visitor}}/gi;
  $pbp_descrips =~ s/$$game_info{'visitor_regex'}/{{TEAM_$visitor}}/gi;
  # Step 3: Player interpolation on this string
  my @boxnames = ( );
  # Order by the players by name length so we work from most specific to the least specific
  #  i.e., Jim James won't be used in the interpolated instead of John Jameson
  my @all_players = sort {
    # Build our comparisons
    my %player_a = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{$a}{'name'}};
    my %player_b = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{$b}{'name'}};

    length("$player_b{'first'} $player_b{'last'}") <=> length("$player_a{'first'} $player_a{'last'}")
      ||
    lc($player_b{'boxname'}) cmp lc($player_a{'boxname'})
  } keys(%{$$gamedata{'liveData'}{'players'}{'allPlayers'}});
  # Now loop through in this order
  foreach my $remote_id (@all_players) {
    my $player = $$gamedata{'liveData'}{'players'}{'allPlayers'}{$remote_id};
    my $fname = parse_name_simplified($$player{'name'}{'first'});
    my $sname = parse_name_simplified($$player{'name'}{'last'});
    push @boxnames, parse_name_simplified($$player{'name'}{'boxname'});

    my $name = "$fname $sname";
    my $rplc = "{{PLAYER_', \@player_$$player{'id'}, '}}";
    parse_descrip_names(\$pbp_descrips, $name, $rplc);

    # Alternative versions
    # X.Y. Z as X. Z and Y. Z
    if ($fname =~ m/^[A-Z]\.[A-Z]\./) {
      # X. Z
      my ($alt_fname) = ($fname =~ m/^([A-Z]\.)/);
      parse_descrip_names(\$pbp_descrips, "$alt_fname $sname", $rplc);

      # Y. Z
      ($alt_fname) = ($fname =~ m/([A-Z]\.)$/);
      parse_descrip_names(\$pbp_descrips, "$alt_fname $sname", $rplc);
    }

    # X Y Jr.
    if ($sname =~ m/\s+Jr\.?$/i) {
      my $alt_name = "$fname $sname";
      $alt_name =~ s/\s+Jr\.?$//i;
      parse_descrip_names(\$pbp_descrips, $alt_name, $rplc);
    }
  }
  # Boxscore version (on its own, in case it over-writes a player we're yet to come to)
  foreach my $remote_id (@all_players) {
    my $player = $$gamedata{'liveData'}{'players'}{'allPlayers'}{$remote_id};
    my $rplc = "{{PLAYER_', \@player_$$player{'id'}, '}}";
    my $alt_name = parse_name_simplified($$player{'name'}{'boxname'});
    parse_descrip_names(\$pbp_descrips, $alt_name, $rplc);
  }
  # Step 4: Convert this string into the hash we use later
  foreach my $pbp_descrip (split('###', $pbp_descrips)) {
    my ($id, $descrip) = ($pbp_descrip =~ m/^(.+)~~~(.+)$/);
    $pbp_descrips{$id} = "CONCAT('$descrip')";
  }
  benchmark_stamp('Play-by-Play: Descrip');

  #
  # Pass 2: Identifying the detailed plays
  #
  my %ob_def = ( 'tot' => 0, '1b' => 0, '2b' => 0, '3b' => 0 );
  $inning = 0; $play_id = 0; our %pitcher_pitches = ( );
  our $home_score = 0; our $visitor_score = 0;
  foreach my $inn (@{$$gamedata{'liveData'}{'plays'}{'playsByInning'}}) {
    my $inning_ordinal = display_ordinal(++$inning);

    # Top
    print "\n#\n# Top " . $inning_ordinal . "\n#\n";
    our %ob = %ob_def; our $out = 0;
    foreach my $i (@{$$inn{'top'}}) {
      parse_play_by_play_event($plays{$i});
    }
    # Bottom
    print "\n#\n# Bottom " . $inning_ordinal . "\n#\n";
    %ob = %ob_def; $out = 0;
    foreach my $i (@{$$inn{'bottom'}}) {
      parse_play_by_play_event($plays{$i});
    }
    print "# Not required...\n" if !@{$$inn{'bottom'}};
    benchmark_stamp("Play-by-Play: Plays: $inning_ordinal");
  }

  # Some post-processing
  print "\n##\n## Play-by-Play Post-Processing\n##\nCALL mlb_game_pbp_process('$season', '$game_type', '$game_id');\n";
}

# Parse the description of an event for interpolation
sub parse_play_by_play_descrip {
  my ($play) = @_;
  return if !defined($play);

  # Misc events
  foreach my $action (@{$$play{'actions'}}) {
    my %action = %{$$play{'pitchEvents'}[$action]};
    %action = %{$action{'details'}} if defined($action{'details'});
    # Skip if duplicate of the main AB result
    next if $action{'description'} eq $$play{'result'}{'description'};
    push @pbp_descrips, ++$play_id . '~~~' . parse_descrip($action{'description'});
  }

  # In a handful of instances, we need to take the description from the event type
  if ($$play{'result'}{'description'} eq '') {
    my %batter = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$play{'matchup'}{'batter'}}};
    $$play{'result'}{'description'} = $batter{'name'}{'first'} . ' ' . $batter{'name'}{'last'} . ' ' . lc($$play{'result'}{'event'});
  }
  # The play
  push @pbp_descrips, ++$play_id . '~~~' . parse_descrip($$play{'result'}{'description'});
}

# Parse an individual event
sub parse_play_by_play_event {
  my ($play) = @_;
  return if !defined($play);

  # Skip if a known dodgy at-bat (missing matchup info and no actual pitches, just replicates the previous at-bat's last action)
  return if !defined($$play{'matchup'}{'pitcher'}) && !defined($$play{'matchup'}{'batter'}) && !@{$$play{'pitches'}};

  # Also skip "Game Advisory" notes
  return if $$play{'result'}{'event'} eq 'Game Advisory';

  # Summary info
  my $half = ($$play{'about'}{'halfInning'} eq 'top' ? 'top' : 'bot');
  my %pitcher = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$play{'matchup'}{'pitcher'}}};
  $pitcher{'team_id'} = ($half eq 'top' ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
  my $pitcher_hand = $pitcher{'rightLeft'};
  $pitcher_hand = $$play{'matchup'}{'pitchHand'}{'code'}
    if defined($$play{'matchup'}{'pitchHand'}{'code'});
  $pitcher_hand = $$play{'matchup'}{'rightLeft'}{'pitcher'}
    if defined($$play{'matchup'}{'rightLeft'}{'pitcher'});
  my %batter = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$play{'matchup'}{'batter'}}};
  $batter{'team_id'} = ($half eq 'top' ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
  my $batter_hand = (defined($$play{'matchup'}{'rightLeft'}{'batter'}) ? $$play{'matchup'}{'rightLeft'}{'batter'} : $batter{'rightLeft'});
  print "\n"; # Given propensity for wanting to write the next line to STDERR without the preceding new-line, have this on its own line
  print "# Pitcher: $pitcher{'name'}{'first'} $pitcher{'name'}{'last'} ($pitcher{'team_id'} #$pitcher{'shirtNum'}); Batter: $batter{'name'}{'first'} $batter{'name'}{'last'} ($batter{'team_id'} #$batter{'shirtNum'}, $batter_hand); $ob{'tot'} on, $out out ($$play{'result'}{'event'})\n";

  # Misc events
  foreach my $action (@{$$play{'actions'}}) {
    my %action = %{$$play{'pitchEvents'}[$action]};
    %action = %{$action{'details'}} if defined($action{'details'});
    # Skip if duplicate of the main AB result
    next if $action{'description'} eq $$play{'result'}{'description'};
    print "#  Action: " . (defined($action{'description_raw'}) ? $action{'description_raw'} : $action{'description'}) . "\n";
    my $descrip = $pbp_descrips{++$play_id};

    #
    # Process actions that affect fielding info (fielding pos, etc)
    #
    my $player; my $new_pos;
    # Pitcher switching throwing arms(?!?!)
    if ($descrip =~ m/is now pitching (left|right)-handed/) {
      my ($pitcher, $arm) = ($descrip =~ m/player_(\d+), '\}\} is now pitching (left|right)\-handed/);
      $arm = uc(substr($arm, 0, 1));
      print "# Internal Pitcher L/R Update: $pitcher now throwing $arm\n";
      $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$pitcher}{'rightLeft'} = $pitcher{'rightLeft'} = $arm;

    # Pitching Change: NEW replaces OLD(, batting #(, replacing HITTER|pos)).
    } elsif ($descrip =~ m/Pitch(?:ing|er) Change/) {
      my ($player) = ($descrip =~ m/Pitch(?:ing|er) Change: \{\{PLAYER_', \@player_(\d+), '\}\} replaces/);
      update_field_pos($player, 'P')
        if defined($player);

    # Pitcher PITCHER enters the batting order, batting #, (pos )HITTER leaves the game.
    } elsif (($player) = ($descrip =~ m/Pitcher (.*?) enters the batting order/)) {
      ($player) = ($player =~ m/\@player_(\d+),/);
      update_field_pos($player, 'P')
        if defined($player);

    # Offensive Substitution: Pinch-(hitter|runner) NEW replaces OLD
    #  Pinch-runner  replaces (1003 // 1011)
    } elsif (($new_pos, $player) = ($descrip =~ m/Offensive Substitution: Pinch[- ](hitter|runner) .*?\@player_(\d+),/)) {
      $new_pos = 'P' . uc(substr($new_pos, 0, 1));
      update_field_pos($player, $new_pos)
        if defined($player);

    # (Defensive Substitution: )PLAYER remains in the game as the POS, batting #
    } elsif (($player, $new_pos) = ($descrip =~ m/^(?:Defensive Substitution:)?(.+) remains in the game as the ([A-Za-z +]+)/)) {
      ($player) = ($player =~ m/\@player_(\d+),/);
      $new_pos =~ s/baseman$/base/;
      $new_pos =~ s/fielder$/field/;
      update_field_pos($player, $config{'pos'}{$new_pos})
        if defined($player);

    # Defensive Substitution: NEW replaces (pos ) OLD, batting #, playing pos
    } elsif ($descrip =~ m/Defensive Substitution:/) {
      ($player, $new_pos) = ($descrip =~ m/\@player_(\d+).*? replaces .*? playing ([A-Za-z ]+)/);
      $new_pos =~ s/baseman$/base/;
      $new_pos =~ s/fielder$/field/;
      update_field_pos($player, $config{'pos'}{$new_pos})
        if defined($player);

    # Defensive switch from catcher to first base for PLAYER
    } elsif ($descrip =~ m/Defensive switch/) {
      ($new_pos, $player) = ($descrip =~ m/ to (.*?) for .*?\@player_(\d+),/);
      update_field_pos($player, $config{'pos'}{$new_pos})
        if defined($player);
    }

    #
    # Display the action's play
    #
    $home_score = $action{'homeScore'} if defined($action{'homeScore'});
    $visitor_score = $action{'awayScore'} if defined($action{'awayScore'});
    print "INSERT INTO `SPORTS_MLB_GAME_PLAYS` (`season`, `game_type`, `game_id`, `play_id`, `inning`, `half_inning`, `batter_team_id`, `batter`, `pitcher_team_id`, `pitcher`, `on_base`, `ob_1b`, `ob_2b`, `ob_3b`, `out`, `description`, `scoring`, `home_score`, `visitor_score`, `home_winprob`, `visitor_winprob`, `change_winprob`)
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$$play{'about'}{'inning'}', '$half', '$batter{'team_id'}', '$batter{'shirtNum'}', '$pitcher{'team_id'}', '$pitcher{'shirtNum'}', '$ob{'tot'}', '$ob{'1b'}', '$ob{'2b'}', '$ob{'3b'}', '$out', $descrip, '" . int(defined($action{'scored'}) && $action{'scored'}) . "', '$home_score', '$visitor_score', NULL, NULL, NULL);\n";
  }

  # Do we need to emulate the scoring updates on a scoring play?
  if (defined($$play{'about'}{'isScoringPlay'}) && $$play{'about'}{'isScoringPlay'}) {
    my $inn_team = ($$play{'about'}{'halfInning'} eq 'top' ? 'away' : 'home');
    if (!defined($$play{'result'}{$inn_team . 'Score'})) {
      $$play{'result'}{$inn_team . 'Score'} = ($$play{'about'}{'halfInning'} eq 'top' ? $visitor_score : $home_score);
      foreach my $runner (@{$$play{'runners'}}) {
        $$play{'result'}{$inn_team . 'Score'}++
          if (defined($$runner{'details'}{'isScoringEvent'}) && $$runner{'details'}{'isScoringEvent'});
      }
    }
  }

  # General At Bat info
  my $descrip = $pbp_descrips{++$play_id};
  $home_score = $$play{'result'}{'homeScore'} if defined($$play{'result'}{'homeScore'});
  $visitor_score = $$play{'result'}{'awayScore'} if defined($$play{'result'}{'awayScore'});
  my $home_winprob = (defined($$play{'winprob'}) ? $$play{'winprob'}{'home'} : undef); check_for_null(\$home_winprob);
  my $visitor_winprob = (defined($$play{'winprob'}) ? $$play{'winprob'}{'visitor'} : undef); check_for_null(\$visitor_winprob);
  my $change_key = ($half eq 'top' ? 'visitor_change' : 'home_change');
  my $change_winprob = (defined($$play{'winprob'}) ? $$play{'winprob'}{$change_key} : undef); check_for_null(\$change_winprob);
  print "INSERT INTO `SPORTS_MLB_GAME_PLAYS` (`season`, `game_type`, `game_id`, `play_id`, `inning`, `half_inning`, `batter_team_id`, `batter`, `pitcher_team_id`, `pitcher`, `on_base`, `ob_1b`, `ob_2b`, `ob_3b`, `out`, `description`, `scoring`, `home_score`, `visitor_score`, `home_winprob`, `visitor_winprob`, `change_winprob`)
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$$play{'about'}{'inning'}', '$half', '$batter{'team_id'}', '$batter{'shirtNum'}', '$pitcher{'team_id'}', '$pitcher{'shirtNum'}', '$ob{'tot'}', '$ob{'1b'}', '$ob{'2b'}', '$ob{'3b'}', '$out', $descrip, '" . int($$play{'about'}{'isScoringPlay'}) . "', '$home_score', '$visitor_score', $home_winprob, $visitor_winprob, $change_winprob);\n";

  # Per-pitch breakdown
  my @pitches = ( );
  my %ab_pitches = ('col' => '', 'val' => ''); my %ab_count = ('balls' => 0, 'strikes' => 0);
  my $i = 0; my $count = 0; my $counts_set = 1; my @counts = ('0-0'); my %hit_info;
  foreach my $event (@{$$play{'pitchEvents'}}) {
    my $val;

    # Pickoff?
    if (defined($$event{'type'}) && $$event{'type'} eq 'pickoff') {
      # Though we only care about attempts (successful pickoffs are marked as subsequent actions for some reason)
      $val = 'P' . substr($$event{'description'}, -2, 1)
        if $$event{'description'} =~ m/attempt/i;
    # Was it an action?
    } elsif (grep(/^$i$/, @{$$play{'actions'}}) || !defined($$event{'details'}{'call'})) {
      # Though we only flag actions that occurred mid-at-bat
      $val = 'A'
        if @pitches;
    # Nope, get info out about the pitch
    } else {
      $val = $$event{'details'}{'call'};
      my $result = parse_pitch_result($$event{'details'}{'description'});
      # Store foul balls after 2nd strike as a foul ball
      $val = 'F'
        if $val eq 'S' && ($ab_count{'strikes'} == 2 && substr($result, 0, 4) eq 'Foul' && $result ne 'Foul Tip');
      # Pitch count may be implied (absence / undef => 0)
      my $balls = convert_undef_zero($$event{'count'}{'balls'});
      my $strikes = convert_undef_zero($$event{'count'}{'strikes'});
      # Note: Pitch info may not actually be provided...
      check_for_null(\$$event{'details'}{'type'});
      check_for_null(\$$event{'stats'}{'startSpeed'});
      check_for_null(\$$event{'stats'}{'strikezoneTop'});
      check_for_null(\$$event{'stats'}{'strikezoneBottom'});
      check_for_null(\$$event{'stats'}{'coordinates'}{'px'});
      check_for_null(\$$event{'stats'}{'coordinates'}{'pz'});
      my $pitch_num = (defined($$event{'pitchNum'}) ? $$event{'pitchNum'} : @pitches+1);
      push @pitches, "('$season', '$game_type', '$game_id', '$play_id', '$pitch_num', $$event{'details'}{'type'}, $$event{'stats'}{'startSpeed'}, '" . int($$event{'details'}{'isInPlay'}) . "', '$val', '$result', '$balls', '$strikes', $$event{'stats'}{'strikezoneTop'}, $$event{'stats'}{'strikezoneBottom'}, $$event{'stats'}{'coordinates'}{'px'}, $$event{'stats'}{'coordinates'}{'pz'})";
      # What is the (actual) count?
      $ab_count{'balls'}++ if $val eq 'B';
      $ab_count{'strikes'}++ if $val eq 'S';
      my $event_count = "$ab_count{'balls'}-$ab_count{'strikes'}";
      if (($val ne 'X') && ($event_count ne $counts[-1])) {
        push @counts, $event_count;
        # Add to our set of counts
        #  0-0 = 1,     1-0 = 2,     2-0 = 4,      3-0 = 8,     4-0 = 16
        #  0-1 = 32,    1-1 = 64,    2-1 = 128,    3-1 = 256,   4-1 = 512
        #  0-2 = 1024,  1-2 = 2048,  2-2 = 4096,   3-2 = 8192,  4-2 = 16384
        #  0-3 = 32768, 1-3 = 65536, 2-3 = 131072, 3-3 = 262144
        #  => (2 ^ Balls) * (32 ^ Strikes)
        $counts_set += ((2 ** $ab_count{'balls'}) * (32 ** $ab_count{'strikes'}));
      }

      # Any hit info to capture?
      %hit_info = %{$$event{'hitData'}} if defined($$event{'hitData'});
    }

    # One to add to the list?
    if (defined($val)) {
      $ab_pitches{'val'} .= ", '$val'";
      $ab_pitches{'col'} .= sprintf(', `pitch%02d`', ++$count);
    }
    $i++;
  }

  # Process hit info, if there was one!
  if (%hit_info) {
    my $hit_dist = $hit_info{'totalDistance'}; check_for_null(\$hit_dist);
    my $hit_speed = $hit_info{'launchSpeed'}; check_for_null(\$hit_speed);
    my $hit_angle = $hit_info{'launchAngle'}; check_for_null(\$hit_angle);
    $ab_pitches{'val'} .= ", $hit_dist, $hit_speed, $hit_angle";
    $ab_pitches{'col'} .= ', `hit_distance`, `hit_speed`, `hit_angle`';
  }

  # At Bat
  my $result = parse_atbat_result($$play{'result'}{'event'});
  my $ab_complete = parse_atbat_complete($$play{'result'}{'event'});
  print "INSERT INTO `SPORTS_MLB_GAME_ATBAT` (`season`, `game_type`, `game_id`, `play_id`, `batter_team_id`, `batter`, `pitcher_team_id`, `pitcher`, `balls`, `strikes`, `num_pitches`, `completed`, `result`$ab_pitches{'col'})
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$batter{'team_id'}', '$batter{'shirtNum'}', '$pitcher{'team_id'}', '$pitcher{'shirtNum'}', '$$play{'count'}{'balls'}', '$$play{'count'}{'strikes'}', '" . @pitches . "', '$ab_complete', '$result'$ab_pitches{'val'});\n";

  # Pitches
  $pitcher_pitches{$pitcher{'id'}} = 0 if !defined($pitcher_pitches{$pitcher{'id'}});
  $pitcher_pitches{$pitcher{'id'}} += @pitches;
  print "INSERT INTO `SPORTS_MLB_GAME_ATBAT_PITCHES` (`season`, `game_type`, `game_id`, `play_id`, `num`, `pitch`, `speed`, `in_play`, `call`, `result`, `balls`, `strikes`, `zone_top`, `zone_bottom`, `pitch_x`, `pitch_y`)
  VALUES " . join(",\n         ", @pitches) . ";\n" if @pitches;

  # Flags (for splits)
  my $base_runners = ($ob{'1b'} ? 1 : 0) + ($ob{'2b'} ? 2 : 0) + ($ob{'3b'} ? 4 : 0);
  my $risp = int($ob{'2b'} || $ob{'3b'});
  my $batting_order = substr($$gamedata{'liveData'}{'boxscore'}{'teams'}{$half eq 'top' ? 'away' : 'home'}{'players'}{'ID'.$batter{'id'}}{'gameStats'}{'batting'}{'battingOrder'}, 0, 1);
  my $batter_pos = $field_pos{$batter{'id'}};
  my $is_bb = int($result =~ m/Walk/);
  my $is_ibb = int($result eq 'Intentional Walk');
  my $is_hbp = int($result eq 'Hit By Pitch');
  my $is_sac = int($result =~ m/^Sacrifice/);
  my $is_sac_fly = int($result =~ m/^Sacrifice Fly/);
  my $is_intf = int($result =~ m/Interference/);
  my $is_ab = int($ab_complete && !$is_bb && !$is_hbp && !$is_sac && !$is_intf);
  my $is_obp_evt = int($ab_complete && ($is_ab || $is_bb || $is_hbp || $is_sac_fly));
  # Total Bases
  my $tb = 0;
  if ( $result eq 'Single' ) {
    $tb = 1;
  } elsif ( $result eq 'Double' ) {
    $tb = 2;
  } elsif ( $result eq 'Triple' ) {
    $tb = 3;
  } elsif ( $result eq 'Home Run' ) {
    $tb = 4;
  }
  # Can we backfill R, SB, CS, LOB from later plays??
  my $sp_rp = ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$half eq 'top' ? 'home' : 'away'}{'pitchers'}[0] eq $pitcher{'id'} ? 'SP' : 'RP');
  print "INSERT INTO `SPORTS_MLB_GAME_ATBAT_FLAGS` (`season`, `game_type`, `game_id`, `play_id`, `inning`, `on_base`, `risp`, `out`, `counts`, `count_final`, `batting_order`, `batter_pos`, `pitcher_pitches`, `ab_pitches`, `batter`, `batter_hand`, `pitcher`, `pitcher_hand`, `sp_rp`, `is_pa`, `is_ab`, `is_obp_evt`, `is_hit`, `is_bb`, `is_ibb`, `is_hbp`, `is_out`, `is_k`, `tb`, `rbi`)
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$$play{'about'}{'inning'}', '$base_runners', '$risp', '$out', '$counts_set', '$counts[-1]', '$batting_order', '$batter_pos', '$pitcher_pitches{$pitcher{'id'}}', '" . @pitches . "', \@player_$batter{'id'}, '$batter_hand', \@player_$pitcher{'id'}, '$pitcher_hand', '$sp_rp', '$ab_complete', '$is_ab', '$is_obp_evt', '" . int($tb > 0) . "', '$is_bb', '$is_ibb', '$is_hbp', '" . int($$play{'count'}{'outs'} != $out) . "', '" . int($result =~ m/Strikeout/) . "', '$tb', '$$play{'result'}{'rbi'}');\n";

  # Runners/Outs info to carry over to the next AB
  $out = $$play{'count'}{'outs'};
  if (@{$$play{'runners'}}) {
    foreach my $runner (@{$$play{'runners'}}) {
      # From
      my $from = lc($$runner{'movement'}{'start'});
      $ob{$from}-- if $from ne '';
      # To
      my $to = lc($$runner{'movement'}{'end'});
      $ob{$to}++ if ($to ne '' && $to ne 'score');
      # Overall numbers
      $ob{'tot'} = ($ob{'1b'} + $ob{'2b'} + $ob{'3b'});
    }
  }
}

# Perform some play description tidying
sub parse_descrip {
  my ($play) = @_;
  trim(\$play);
  $play =~ s/(\s){2,}/ /g;
  $play =~ s/([A-Z]\.)\s([A-Z]\.)/$1$2/g;
  return $play;
}

# Hack description name regex's (eugh...)
sub parse_descrip_hacks {
  my ($pbp_descrips) = @_;
  # Un-encode previously converted player to SQL swaps
  $$pbp_descrips =~ s/\{\{PLAYER_\&\#39;, \@player_(\d+), \&\#39;\}\}/{{PLAYER_', \@player_$1, '}}/g;
  # Correct some potential spacing errors
  $$pbp_descrips =~ s/\. ,/\.,/g;
}

# Convert the play result
sub parse_atbat_result {
  my ($result) = @_;
  trim(\$result);
  $result = convert_text($result);
  my $result_lc = lc $result;

  # "Fan interference" => "Fan Interference" (Fussy case-change...)
  if ($result_lc eq 'fan interference') {
    return 'Fan Interference';
  # "Fielders Choice Out" => "Fielders Choice"
  } elsif ($result_lc eq 'fielders choice out') {
    return 'Fielders Choice';
  # "Grounded Into DP" => "Grounded Into Double Play"
  } elsif ($result_lc eq 'grounded into dp') {
    return 'Grounded Into Double Play';
  # "Intent Walk" => "Intentional Walk"
  } elsif ($result_lc eq 'intent walk') {
    return 'Intentional Walk';
  # "Sac Bunt" => "Sacrifice Bunt"
  } elsif ($result_lc eq 'sac bunt') {
    return 'Sacrifice Bunt';
  # "Sac Fly" => "Sacrifice Fly"
  } elsif ($result_lc eq 'sac fly') {
    return 'Sacrifice Fly';
  # "Sac Fly DP" => "Sacrifice Fly (Double Play)"
  } elsif ($result_lc eq 'sac fly dp') {
    return 'Sacrifice Fly (Double Play)';
  # "Sacrifice Bunt DP" => "Sacrifice Bunt (Double Play)"
  } elsif ($result_lc eq 'sacrifice bunt dp') {
    return 'Sacrifice Bunt (Double Play)';
  # "Strikeout - DP" => "Strikeout (Double Play)"
  } elsif ($result_lc eq 'strikeout - dp') {
    return 'Strikeout (Double Play)';
  }

  # The rest, as were passed in
  #  Batter Interference, Bunt Groundout, Bunt Lineout, Bunt Pop Out, Catcher Interference, Double, Double Play, Field Error, Fielders Choice, Flyout, Forceout, Groundout, Hit By Pitch, Home Run, Left On Base, Lineout, Pitching Substitution, Pop Out, Runner Out, Single, Strikeout, Triple, Triple Play, Walk
  return $result;
}

# Convert the pitch result
sub parse_pitch_result {
  my ($result) = @_;
  trim(\$result);
  $result = convert_text($result);
  my $result_lc = lc $result;

  # "In play, no out", "In play, out(s)", "In play, run(s)" => "In Play"
  if (substr($result_lc, 0, 7) eq 'in play') {
    return 'In Play';
  # "Intent Ball" => "Intentional Ball"
  } elsif ($result_lc eq 'intent ball') {
    return 'Intentional Ball';
  # "Swinging Strike (Blocked)" => "Swinging Strike"
  } elsif ($result_lc eq 'swinging strike (blocked)') {
    return 'Swinging Strike';
  }

  # The rest, as were passed in
  #  Automatic Ball, Ball, Ball In Dirt, Called Strike, Foul, Foul Bunt, Foul (Runner Going), Foul Tip, Hit By Pitch, Missed Bunt, Pitchout, Swinging Pitchout, Swinging Strike
  return $result;
}

# Did an AB Play actually result in a completed at-bat?
sub parse_atbat_complete {
  my $result = parse_atbat_result(@_);
  return int($result ne 'Runner Out');
}

# Convert text name in the descrip to a codified version
sub parse_descrip_names {
  my ($src, $search, $rplc) = @_;
  $search =~ s/\s{2,}/ /g;
  $search =~ s/\./\\\./g;
  trim(\$search);
  # We need to be sensitive to any names featuring a . (such as Jr.), as sometimes we'll want to keep the . even though it's in the search pattern
  if ( substr($search, -1) ne '.' ) {
    $$src =~ s/\b$search\b/$rplc/g;
  } else {
    # Version 1: end of the descrip (keep the .)
    $$src =~ s/\b$search(#|$)/$rplc\.$1/g;
    # Version 2: mid-string, but end of sentence (keep the .)
    $$src =~ s/\b$search(\s+[A-Z])/$rplc\.$1/g;
    # Version 3: mid-string, but sentence continues (remove the .)
    $$src =~ s/\b$search(,?\s+[a-z])/$rplc$1/g;
  }
}

# Update our internal fielding position tracker
sub update_field_pos {
  my ($player, $pos) = @_;
  my %player = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{"ID$player"}};
  print "#   Internal Fielding Tracker Update: $player{'name'}{'first'} $player{'name'}{'last'} ($$gamedata{'gameData'}{'teams'}{$$gamedata{'gameData'}{'teams'}{'teamsByID'}{$player{'uniformID'}}}{'name'}{'abbrev'} #$player{'shirtNum'}, ID: $player) now at $pos\n";
  $field_pos{$player} = $pos;
}

# Convert X to Xst/nd/rd/th
sub display_ordinal {
  my ($num) = @_;
  my $num_clean = $num; $num_clean =~ s/[^\d\.]//g;
  my $end_digit = substr($num_clean, length($num_clean)-1);
  my $end_digits = substr($num_clean, length($num_clean)-2);
  if ($end_digit == 1 && $end_digits != 11) {
    return $num . 'st';
  } elsif ($end_digit == 2 && $end_digits != 12) {
    return $num . 'nd';
  } elsif ($end_digit == 3 && $end_digits != 13) {
    return $num . 'rd';
  }

  return $num . 'th';
}

# Return true to pacify the compiler
1;
