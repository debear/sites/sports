#!/usr/bin/perl -w
# Methods for fixing the game data

#
# Replicate the JSON from the XML
#
sub fix_gamedata {
  # Load the component parts
  load_xml('play_by_play', { 'decode-libxml' => 1 });
  benchmark_stamp('Initial Setup: Loaded Play-by-Play XML');
  load_xml('boxscore');
  benchmark_stamp('Initial Setup: Loaded Boxscore XML');
  load_xml('players');
  benchmark_stamp('Initial Setup: Loaded Player XML');

  fix_gamedata__players();
  benchmark_stamp('Initial Setup: Mapped Players');
  fix_gamedata__boxscore();
  benchmark_stamp('Initial Setup: Mapped Boxscore');
  fix_gamedata__plays();
  benchmark_stamp('Initial Setup: Mapped Play-by-Play');
  fix_gamedata__summary();
  benchmark_stamp('Initial Setup: Mapped Summary');
}

# Replicate player details from the XML
sub fix_gamedata__players {
  # Player lists
  $$gamedata{'liveData'}{'players'}{'allPlayers'} = ( );
  foreach my $team (@{$xml{'players'}{'team'}}) {
    foreach my $player (@{$$team{'player'}}) {
      %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$player{'id'}}} = (
        'id' => $$player{'id'},
        'name' => {
                    'first' => $$player{'first'},
                    'last' => $$player{'last'},
                    'boxname' => $$player{'boxname'},
                  },
        'position' => $$player{'position'},
        'batterPitcher' => $$player{'position'} eq 'P' ? 'p' : 'b',
        'uniform' => defined($$player{'team_abbrev'}) ? $$player{'team_abbrev'} : $$team{'id'},
        'uniformID' => defined($$player{'team_id'}) ? $$player{'team_id'} : $xml{'boxscore'}{$$team{'type'} . '_id'},
        'shirtNum' => $$player{'num'},
        'stands' => $$player{'bats'},
        'rightLeft' => $$player{'rl'},
        'height' => undef,
      );
    }
  }
}

# Replicate boxscore details from the XML
sub fix_gamedata__boxscore {
  # Game info
  $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}
   = { 'players' => { }, 'note' => '', 'info' => '',
       'pitchers' => [ ], 'bullpen' => [ ],
       'battingOrder' => [ ], 'batters' => [ ], 'bench' => [ ] };
  $$gamedata{'liveData'}{'boxscore'}{'teams'}{'away'}
   = { 'players' => { }, 'note' => '', 'info' => '',
       'pitchers' => [ ], 'bullpen' => [ ],
       'battingOrder' => [ ], 'batters' => [ ], 'bench' => [ ] };
  # - Batters
  foreach my $team (@{$xml{'boxscore'}{'batting'}}) {
    my $t = $$gamedata{'liveData'}{'boxscore'}{'teams'}{$$team{'team_flag'}};
    if (defined($$team{'note'})) {
      $$t{'note'} = $$team{'note'};
      trim(\$$t{'note'});
    }
    $$t{'team_flag'} = $$team{'team_flag'};
    foreach my $batter (@{$$team{'batter'}}) {
      my %player = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$batter{'id'}}};
      fix_gamedata__boxscore_player($t, \%player, $batter);
      if (defined($$batter{'note'})) {
        $$t{'players'}{'ID'.$$batter{'id'}}{'boxNote'} = $$batter{'note'};
        trim(\$$t{'players'}{'ID'.$$batter{'id'}}{'boxNote'});
      }
      # Batting stats
      $$t{'players'}{'ID'.$$batter{'id'}}{'gameStats'}{'batting'} = {
        'battingOrder' => $$batter{'bo'},
        'atBats' => $$batter{'ab'},
        'hits' => $$batter{'h'},
        'doubles' => $$batter{'d'},
        'triples' => $$batter{'t'},
        'homeRuns' => $$batter{'hr'},
        'runs' => $$batter{'r'},
        'rbi' => $$batter{'rbi'},
        'baseOnBalls' => $$batter{'bb'},
        'hitByPitch' => $$batter{'hbp'},
        'leftOnBase' => $$batter{'lob'},
        'strikeOuts' => $$batter{'so'},
        'sacFlys' => $$batter{'sf'},
        'sacBunts' => $$batter{'sac'},
        'flyOuts' => $$batter{'ao'},
        'groundOuts' => $$batter{'go'},
        'stolenBases' => $$batter{'sb'},
        'caughtStealing' => $$batter{'cs'},
      };
      if (defined($$batter{'bo'})) {
        $$t{'battingOrder'}[substr($$batter{'bo'}, 0, 1)] = $$batter{'id'};
        push @{$$t{'batters'}}, $$batter{'id'};
      }
      # Fielding stats
      $$t{'players'}{'ID'.$$batter{'id'}}{'gameStats'}{'fielding'} = {
        'putOuts' => $$batter{'po'},
        'assists' => $$batter{'a'},
        'errors' => $$batter{'e'},
      };
    }
    # Team info
    $t{'info'} .= $$team{'text_data'}
      if defined($$team{'text_data'});
  }

  # - Pitchers
  my %pitch_nums = ( );
  foreach my $team (@{$xml{'boxscore'}{'pitching'}}) {
    my $t = $$gamedata{'liveData'}{'boxscore'}{'teams'}{$$team{'team_flag'}};
    $$team{'pitcher'} = [ $$team{'pitcher'} ] if ref($$team{'pitcher'}) ne 'ARRAY';
    # Pass 1: Determine if we need to manually parse the pitch numbers
    my $build_pitch_nums = 0;
    foreach my $pitcher (@{$$team{'pitcher'}}) {
      if (!defined($$pitcher{'np'})) {
        $build_pitch_nums = 1;
        last;
      }
    }
    if ($build_pitch_nums && !%pitch_nums) {
      my ($breakdown) = ($xml{'boxscore'}{'game_info'} =~ m/<b>Pitches-strikes<\/b>: (.*?)\.\s*?<(?:\/span|br\/)>/);
      my @breakdown = ($breakdown =~ m/(.*?) (\d+)\-(\d+)(?:, )?\.?/g);
      while (my ($boxname, $num_pitches, $num_strikes) = splice(@breakdown, 0, 3)) {
        $pitch_nums{$boxname} = { 'np' => $num_pitches, 's' => $num_strikes };
      }
    }
    # Pass 2: Parse...
    foreach my $pitcher (@{$$team{'pitcher'}}) {
      my %player = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$pitcher{'id'}}};
      fix_gamedata__boxscore_player($t, \%player, $pitcher);
      # Determine what decision(s) this pitcher figured in
      if (defined($$pitcher{'note'})) {
        $$t{'players'}{'ID'.$$pitcher{'id'}}{'note'} = $$pitcher{'note'};
        trim(\$$t{'players'}{'ID'.$$pitcher{'id'}}{'note'});
      } else {
        $$pitcher{'note'} = '';
      }
      # If no pitches in boxscore, get from the game_info
      $$pitcher{'np'} = $pitch_nums{$player{'name'}{'boxname'}}{'np'} if !defined($$pitcher{'np'});
      $$pitcher{'s'} = $pitch_nums{$player{'name'}{'boxname'}}{'s'} if !defined($$pitcher{'s'});
      # Build our JSON object
      $$t{'players'}{'ID'.$$pitcher{'id'}}{'gameStats'}{'pitching'} = {
        'pitchesThrown' => ''.$$pitcher{'np'},
        'balls' => ''.($$pitcher{'np'} - $$pitcher{'s'}),
        'strikes' => ''.$$pitcher{'s'},
        'outs' => ''.$$pitcher{'out'},
        'inningsPitched' => sprintf('%d.%d', int($$pitcher{'out'} / 3), $$pitcher{'out'} % 3),
        'battersFaced' => $$pitcher{'bf'},
        'strikeOuts' => $$pitcher{'so'},
        'hits' => $$pitcher{'h'},
        'baseOnBalls' => $$pitcher{'bb'},
        'runs' => $$pitcher{'r'},
        'earnedRuns' => $$pitcher{'er'},
        'homeRuns' => $$pitcher{'hr'},
        'note' => $$pitcher{'note'},
      };
      push @{$$t{'pitchers'}}, $$pitcher{'id'};

      # Pitcher of record?
      $$gamedata{'liveData'}{'linescore'}{'pitchers'}{'win'}  = $$pitcher{'id'} if defined($$pitcher{'win'})  || $$pitcher{'note'} =~ m/\(W,/;
      $$gamedata{'liveData'}{'linescore'}{'pitchers'}{'loss'} = $$pitcher{'id'} if defined($$pitcher{'loss'}) || $$pitcher{'note'} =~ m/\(L,/;
      $$gamedata{'liveData'}{'linescore'}{'pitchers'}{'save'} = $$pitcher{'id'} if defined($$pitcher{'save'}) || $$pitcher{'note'} =~ m/\(S,/;
    }
    # Team info
    $t{'info'} .= $$team{'text_data'}
      if defined($$team{'text_data'});
  }

  # - DNP ("Bench" / "Bullpen")
  foreach my $team (@{$xml{'players'}{'team'}}) {
    my $t = $$gamedata{'liveData'}{'boxscore'}{'teams'}{$$team{'type'}};
    foreach my $player (@{$$team{'player'}}) {
      if (!defined($$t{'players'}{'ID'.$$player{'id'}})) {
        my %player = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID'.$$player{'id'}}};
        fix_gamedata__boxscore_player($t, \%player, $player);
        push @{$$t{$player{'batterPitcher'} eq 'p' ? 'bullpen' : 'bench'}}, $$player{'id'};
      }
    }
  }
}

# Build a player instance
sub fix_gamedata__boxscore_player {
  my ($team, $player, $boxline) = @_;
  $$team{'players'}{'ID'.$$boxline{'id'}} = {
    'id' => $$player{'id'},
    'name' => $$player{'name'},
    'position' => $$boxline{'pos'},
    'batterPitcher' => $$player{'batterPitcher'},
    'uniform' => $$player{'uniform'},
    'uniformID' => $$player{'uniformID'},
    'team' => $$team{'team_flag'},
    'teamID' => $$player{'uniformID'},
    'homeAway' => $$team{'team_flag'},
    'shirtNum' => $$player{'shirtNum'},
    'stands' => $$player{'stands'},
    'rightLeft' => $$player{'rightLeft'},
    'gameStats' => { 'batting' => { }, 'fielding' => { }, 'pitching' => { } },
    'height' => undef,
  } if !defined($$team{'players'}{'ID'.$$boxline{'id'}});
}

# Replicate play-by-play details from the XML
#  Note: To preserve the awkward XML order (such as "pitch, action, pitch, pitch, action"), we cannot use XML::Simple, as these actions would be grouped together
sub fix_gamedata__plays {
  # Create our arrays
  $$gamedata{'liveData'}{'plays'}{'allPlays'} = [ ];
  $$gamedata{'liveData'}{'plays'}{'playsByInning'} = [ ];

  # Loop through the innings from the XML
  foreach my $inn ($xml{'play_by_play'}->findnodes('/game/inning')) {
    my %inn = ( 'top' => [ ], 'bottom' => [ ] );
    foreach my $half ('top','bottom') {
      my %actions = ( );
      foreach my $play ($inn->findnodes('./'.$half.'/*')) {
        # We only process atbat tags here, so determine which atbat (and where in the atbat) they occurred
        if ($play->nodeName eq 'action') {
          my $event = $play->getAttribute('event');
          # Caught Stealing, Stolen Base, Wild Pitch, Balk: Preserve pitch attribute as where it needs processing
          if ( (substr($event, 0, 15) eq 'Caught Stealing')
                || (substr($event, 0, 11) eq 'Stolen Base')
                || ($event eq 'Wild Pitch')
                || ($event eq 'Balk') ) {
            $pitch = $play->getAttribute('pitch');
          # The rest occur at the start of the atbat
          } else {
            $pitch = -1;
          }
          # Add, ensuring each is an array
          $actions{$pitch} = [ ] if !defined($actions{$pitch});
          push @{$actions{$pitch}}, $play;
          next;
        }

        # Build
        my %play = ( 'actions' => [ ], 'pitches' => [ ], 'pitchEvents' => [ ],
                     'count' => {
                        'balls' => $play->getAttribute('b'),
                        'strikes' => $play->getAttribute('s'),
                        'outs' => $play->getAttribute('o'),
                     },
                     'matchup' => {
                       'batter' => $play->getAttribute('batter'),
                       'pitcher' => $play->getAttribute('pitcher'),
                       'rightLeft' => {
                         'batter' => $play->getAttribute('stand'),
                         'pitcher' => $play->getAttribute('p_throws'),
                       },
                     },
                     'about' => {
                       'inning' => $inn->getAttribute('num'),
                       'halfInning' => $half,
                       'isScoringPlay' => '' . int(defined($play->getAttribute('score')) && $play->getAttribute('score') eq 'T'), # Typecast...
                     },
                     'result' => {
                       'type' => 'atBat',
                       'description' => $play->getAttribute('des'),
                       'brief' => '',
                       'event' => $play->getAttribute('event'),
                       'rbi' => '' . $play->findnodes('./runner[@score]')->size(), # Typecast...
                     },
                     'runners' => [ ] );

        # Process any up-front carry-over actions
        if (defined($actions{-1})) {
          foreach my $action (@{$actions{-1}}) {
            push @{$play{'actions'}}, scalar @{$play{'pitchEvents'}};
            push @{$play{'pitchEvents'}}, {
              'name' => 'action',
              'details' => {
                'event' => $action->getAttribute('event'),
                'player' => $action->getAttribute('player'),
                'description' => $action->getAttribute('des'),
                'scored' => '' . int(defined($action->getAttribute('score')) && $action->getAttribute('score') eq 'T'), # Typecast...
              },
            };
          }
        }

        # Now process the pitches
        my $num_pitches = 0; my $b = 0; my $s = 0;
        foreach my $pitch ($play->findnodes('./*')) {
          # Pickoff attempt?
          if ($pitch->nodeName eq 'po') {
            # Process as an action
            push @{$play{'actions'}}, scalar @{$play{'pitchEvents'}};
            push @{$play{'pitchEvents'}}, {
              'name' => 'action',
              'type' => 'pickoff',
              'details' => {
                'type' => 'pickoff',
                'description' => $pitch->getAttribute('des'),
              },
              'description' => $pitch->getAttribute('des'),
            };
            next;

          # Runner info?
          } elsif ($pitch->nodeName eq 'runner') {
            push @{$play{'runners'}}, {
              'details' => {
                'event' => $pitch->getAttribute('event'),
                'runner' => $pitch->getAttribute('id'),
                'isScoringEvent' => '' . int(defined($pitch->getAttribute('score')) && $pitch->getAttribute('score') eq 'T'), # Typecast...
                'rbi' => '' . int(defined($pitch->getAttribute('rbi')) && $pitch->getAttribute('rbi') eq 'T'), # Typecast...
                'earned' => '' . int(defined($pitch->getAttribute('earned')) && $pitch->getAttribute('earned') eq 'T'), # Typecast...
              },
              'movement' => {
                 'start' => $pitch->getAttribute('start'),
                 'end' => $pitch->getAttribute('end'),
              },
            };
            next;
          }

          # Process the pitch
          $b++ if $pitch->getAttribute('type') eq 'B';
          $s++ if $pitch->getAttribute('type') ne 'B';
          push @{$play{'pitches'}}, scalar @{$play{'pitchEvents'}};
          push @{$play{'pitchEvents'}}, {
            'pitchNum' => '' . (++$num_pitches), # Typecast...
            'count' => {
              'balls' => '' . $b, # Typecast...
              'strikes' => '' . $s, # Typecast...
            },
            'details' => {
              'call' => $pitch->getAttribute('type'),
              'type' => $pitch->getAttribute('pitch_type'),
              'isInPlay' => '' . int(defined($pitch->getAttribute('type')) && $pitch->getAttribute('type') eq 'X'), # Typecast...
              'description' => $pitch->getAttribute('des'),
            },
            'stats' => {
              'coordinates' => {
                'px' => $pitch->getAttribute('px'),
                'pz' => $pitch->getAttribute('pz'),
              },
              'strikezoneTop' => $pitch->getAttribute('sz_top'),
              'strikezoneBottom' => $pitch->getAttribute('sz_bot'),
              'startSpeed' => $pitch->getAttribute('start_speed'),
              'endSpeed' => $pitch->getAttribute('end_speed'),
            },
          };

          # Then any carry-over actions
          if (defined($actions{$num_pitches})) {
            foreach my $action (@{$actions{$num_pitches}}) {
              push @{$play{'actions'}}, scalar @{$play{'pitchEvents'}};
              push @{$play{'pitchEvents'}}, {
                'name' => 'action',
                'details' => {
                  'event' => $action->getAttribute('event'),
                  'player' => $action->getAttribute('player'),
                  'description' => $action->getAttribute('des'),
                  'scored' => '' . int(defined($action->getAttribute('score')) && $action->getAttribute('score') eq 'T'), # Typecast...
                },
              };
            }
          }
        }

        # Add to lists
        push @{$inn{$half}}, scalar @{$$gamedata{'liveData'}{'plays'}{'allPlays'}};
        push @{$$gamedata{'liveData'}{'plays'}{'allPlays'}}, \%play;

        # Reset the carry-over actions
        %actions = ( );
      }
    }
    push @{$$gamedata{'liveData'}{'plays'}{'playsByInning'}}, \%inn;
  }
}

# Replicate game summary details from the XML
sub fix_gamedata__summary {
  # Header info?
  if (defined($config{'rebuilt_base_json'}) && $config{'rebuilt_base_json'}) {
    foreach my $team ('away','home') {
      $$gamedata{'gameData'}{'teams'}{$team}
       = $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'team'}
       = $$gamedata{'liveData'}{'linescore'}{'teams'}{$team}{'team'}
       = { 'id' => $xml{'boxscore'}{$team . '_id'},
           'name' => $xml{'boxscore'}{$team . '_fname'}, };
    }

    # Game status
    $$gamedata{'gameData'}{'status'}{'detailedState'}
     = $$gamedata{'gameData'}{'status'}{'abstractGameState'}
     = $$gamedata{'gameData'}{'status'}{'statusCode'}
     = $$gamedata{'gameData'}{'status'}{'codedGameState'}
     = $xml{'boxscore'}{'status_ind'};
  }

  # Team info
  my %teams = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}};
  foreach my $team (keys %teams) {
    my $team_id = (defined($$gamedata{'gameData'}{'teams'}{$team}{'id'}) ? $$gamedata{'gameData'}{'teams'}{$team}{'id'} : $$gamedata{'gameData'}{'teams'}{$team}{'teamID'});
    $$gamedata{'gameData'}{'teams'}{'teamsByID'}{$team_id} = $team;
    my @players = keys %{$teams{$team}{'players'}};
    $$gamedata{'gameData'}{'teams'}{$team}{'name'} = { 'abbrev' => $teams{$team}{'players'}{$players[0]}{'uniform'} };
  }

  # Game Info (as string to be manually parsed)
  $$gamedata{'liveData'}{'boxscore'}{'gameInfo'} = $xml{'boxscore'}{'game_info'};
  $$gamedata{'liveData'}{'boxscore'}{'teams'}{$xml{'boxscore'}{'batting'}[0]{'team_flag'}}{'info'} = $xml{'boxscore'}{'batting'}[0]{'text_data'};
  $$gamedata{'liveData'}{'boxscore'}{'teams'}{$xml{'boxscore'}{'batting'}[1]{'team_flag'}}{'info'} = $xml{'boxscore'}{'batting'}[1]{'text_data'};
  # Remove any stray game info from the JSON (we'll just go ahead with processing the text version as that's more of a known entity)
  delete $$gamedata{'liveData'}{'boxscore'}{'gameInfoParsed'};
  delete $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'infoParsed'};
  delete $$gamedata{'liveData'}{'boxscore'}{'teams'}{'away'}{'infoParsed'};

  # Number of innings
  $$gamedata{'liveData'}{'linescore'}{'currentInning'} = (@{$xml{'boxscore'}{'linescore'}{'inning_line_score'}});

  # Linescores
  $$gamedata{'liveData'}{'linescore'}{'home'}{'runs'} = $xml{'boxscore'}{'linescore'}{'home_team_runs'};
  $$gamedata{'liveData'}{'linescore'}{'away'}{'runs'} = $xml{'boxscore'}{'linescore'}{'away_team_runs'};
  $$gamedata{'liveData'}{'linescore'}{'home'}{'hits'} = $xml{'boxscore'}{'linescore'}{'home_team_hits'};
  $$gamedata{'liveData'}{'linescore'}{'away'}{'hits'} = $xml{'boxscore'}{'linescore'}{'away_team_hits'};
  $$gamedata{'liveData'}{'linescore'}{'home'}{'errors'} = $xml{'boxscore'}{'linescore'}{'home_team_errors'};
  $$gamedata{'liveData'}{'linescore'}{'away'}{'errors'} = $xml{'boxscore'}{'linescore'}{'away_team_errors'};
  # By inning
  $$gamedata{'liveData'}{'linescore'}{'innings'} = ( );
  foreach my $inn ( @{$xml{'boxscore'}{'linescore'}{'inning_line_score'}} ) {
    $$inn{'num'} = $$inn{'inning'};
    push @{$$gamedata{'liveData'}{'linescore'}{'innings'}}, $inn;
  }
}

# Correct issues in the individual game info line items
sub fix_game_info {
  our %game_info_parsed;

  # Certain keys to skip...
  my @skip = ('umpires','venue','t','att','weather','wind');

  # Convert all incorrect , to ; (whilst also converting all chars to latin - this is purely belt-and-braces, rather than a requirement)
  foreach my $sect (keys %game_info_parsed) {
    foreach my $info (keys %{$game_info_parsed{$sect}}) {
      next if grep(/^$info$/, @skip);
      $game_info_parsed{$sect}{$info} = convert_latin1($game_info_parsed{$sect}{$info});
      $game_info_parsed{$sect}{$info} =~ s/([A-Za-z\) ]), ([A-Z]), ([A-Z])/$1; $2; $3/g; # Catch the instance where the middle char is needed for both regex's, e.g., "Drew, J, Pedroia." => "Drew; J; Pedroia."
      $game_info_parsed{$sect}{$info} =~ s/([A-Za-z\) ]), ([A-Z])/$1; $2/g;
      $game_info_parsed{$sect}{$info} =~ s/([^\(]\d+), ([A-Z])/$1; $2/g;
    }
  }

  # Then, loop through all the players and revert known instances where , is allowed
  foreach my $remote_id (keys %{$$gamedata{'liveData'}{'players'}{'allPlayers'}}) {
    my $sub = $$gamedata{'liveData'}{'players'}{'allPlayers'}{$remote_id}{'name'}{'boxname'};
    my $search = $sub; $search =~ s/, /; /;

    # Loop through each stat then, applying this name retro-fit
    foreach my $sect (keys %game_info_parsed) {
      foreach my $info (keys %{$game_info_parsed{$sect}}) {
        next if grep(/^$info$/, @skip);
        $game_info_parsed{$sect}{$info} =~ s/$search/$sub/g;
      }
    }
  }
}

#
# Create a blank JSON object when one hasn't been provided
#
sub fix_gamedata__base_json {
  $config{'rebuilt_base_json'} = 1;
  return {
    'gameData' => {
      'teams' => {
        'away' => {
          'id' => undef,
          'name' => undef,
        },
        'home' => {
          'id' => undef,
          'name' => undef,
        }
      },
      'status' => {
        'detailedState' => undef,
        'abstractGameState' => undef,
        'statusCode' => undef,
        'codedGameState' => undef,
      },
      'players' => {},
    },
    'liveData' => {
      'boxscore' => {
        'info' => [],
        'teams' => {
          'away' => {
            'team' => {
              'id' => undef,
              'name' => undef,
            },
            'info' => [],
            'players' => {},
            'teamStats' => {},
            'note' => [],
            'battingOrder' => [],
            'batters' => [],
            'bench' => [],
            'pitchers' => [],
            'bullpen' => [],
          },
          'home' => {
            'team' => {
              'id' => undef,
              'name' => undef,
            },
            'info' => [],
            'players' => {},
            'teamStats' => {},
            'note' => [],
            'battingOrder' => [],
            'batters' => [],
            'bench' => [],
            'pitchers' => [],
            'bullpen' => [],
          }
        },
        'pitchingNotes' => [],
        'officials' => [],
      },
      'plays' => {
        'playsByInning' => [],
        'allPlays' => [],
      },
      'linescore' => {
        'currentInning' => 0,
        'balls' => 0,
        'strikes' => 0,
        'outs' => 0,
        'innings' => [],
        'teams' => {
          'away' => {
            'team' => {
              'id' => undef,
              'name' => undef,
            },
            'runs' => 0,
            'hits' => 0,
            'errors' => 0,
          },
          'home' => {
            'team' => {
              'id' => undef,
              'name' => undef,
            },
            'runs' => 0,
            'hits' => 0,
            'errors' => 0,
          },
        },
      },
    },
  };
}

#
# Take the batter's position from the XML gamedata, not the JSON
#
sub fix_batter_pos {
  # First from the player's list
  load_xml('players');
  foreach my $t ( @{$xml{'players'}{'team'}} ) {
    foreach my $batter ( @{$$t{'player'}} ) {
      $$gamedata{'liveData'}{'boxscore'}{'teams'}{$$t{'type'}}{'players'}{"ID$$batter{'id'}"}{'position'}
        = $$gamedata{'liveData'}{'players'}{'allPlayers'}{"ID$$batter{'id'}"}{'position'}
        = $$batter{'position'};
    }
  }

  # Then the boxscore
  load_xml('boxscore');
  foreach my $t ( @{$xml{'boxscore'}{'batting'}} ) {
    foreach my $batter ( @{$$t{'batter'}} ) {
      $$gamedata{'liveData'}{'boxscore'}{'teams'}{$$t{'team_flag'}}{'players'}{"ID$$batter{'id'}"}{'position'}
        = $$gamedata{'liveData'}{'players'}{'allPlayers'}{"ID$$batter{'id'}"}{'position'}
        = $$batter{'pos'}
          if !defined($$gamedata{'liveData'}{'players'}{'allPlayers'}{"ID$$batter{'id'}"}{'position'})
               || $$gamedata{'liveData'}{'players'}{'allPlayers'}{"ID$$batter{'id'}"}{'position'} eq '';
    }
  }
}

#
# Check the composition of the player lists
#
sub fix_players {
  # Get the data out
  my $home_remote_id = $$gamedata{'gameData'}{'teams'}{'home'}{'name'}{'abbrev'};
  my $home_remote_num = $$gamedata{'gameData'}{'teams'}{'home'}{'teamID'};
  my %players = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}};
  my %home = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}};
  my %visitor = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{'away'}};
  my @home_players = sort keys(%{$home{'players'}});
  my @visitor_players = sort keys(%{$visitor{'players'}});
  # Find the matches
  foreach my $id (@home_players) {
    if (grep {$_ eq $id} @visitor_players) {
      my $id_num = substr($id, 2);
      my %player = %{$players{$id}};
      # Determine which team he actually played for
      my $wrong_team;
      if (defined($player{'uniform'})) {
        $wrong_team = ($player{'uniform'} eq $home_remote_id ? 'away' : 'home');
      } else {
        $wrong_team = ($player{'uniformID'} eq $home_remote_num ? 'away' : 'home');
      }
      print STDERR "# Player '$player{'name'}{'first'} $player{'name'}{'last'}' (ID: $id_num) determined as being on both teams. Removing player from the '$wrong_team' team list.\n";
      delete($$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{'players'}{$id});
      foreach my $list ('batters','bench','pitchers','bullpen') {
        my ($index) = grep { @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{$list}}[$_] eq $id_num } keys(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{$list}});
        next if !defined($index);
        print STDERR "#  -> Removing from '$list' list at index '$index'.\n";
        splice(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{$list}}, $index, 1);
      }
    }
  }
}

#
# Ensure all players have a jersey number
#
sub fix_jerseys {
  my %undef_ids = ('home' => 99, 'away' => 99);
  foreach my $team (reverse sort keys %undef_ids) {
    my %existing = ( );
    foreach my $remote_id (sort keys %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}}) {
      $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'shirtNum'}
        = $$gamedata{'liveData'}{'players'}{'allPlayers'}{$remote_id}{'shirtNum'}
        = '' . ++$undef_ids{$team} # Typecast as a string...
            if !defined($$gamedata{'liveData'}{'players'}{'allPlayers'}{$remote_id}{'shirtNum'})
                || defined($existing{$$gamedata{'liveData'}{'players'}{'allPlayers'}{$remote_id}{'shirtNum'}})
                || !defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'shirtNum'})
                || ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'shirtNum'} !~ m/^[1-9]\d*$/);
      $existing{$$gamedata{'liveData'}{'players'}{'allPlayers'}{$remote_id}{'shirtNum'}} = 1;
    }
  }
}

#
# General tidy up of some inconsistent hash keys in the play-by-play
#
sub fix_play_by_play_keys {
  our $season;
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    # Standardise the key for individual pitches in the at bat
    $$play{'pitchEvents'} = $$play{'playEvents'} if defined($$play{'playEvents'});

    # Pre-2016, 'actions' doesn't seem to have been populated in the JSON, so build it ourselves
    if (!defined($config{'rebuild_gamedata'}) && $season < 2016 && (!defined($$play{'actions'}) || !@{$$play{'actions'}})) {
      @{$$play{'actions'}} = ( );
      for (my $i = 0; $i < @{$$play{'pitchEvents'}}; $i++) {
        my %event = %{$$play{'pitchEvents'}[$i]};
        push @{$$play{'actions'}}, $i
          if ((defined($event{'name'}) && $event{'name'} eq 'action') || (defined($event{'type'}) && $event{'type'} eq 'pickoff'));
      }
    }
  }
}

#
# Check (and if applicable fix) if two players on opposite teams have the same name.
#  Also known as the "Chris Young workaround"...
#
sub fix_play_by_play_same_name {
  # Loop through, identifying players by name (as array key?), storing team/remote_id pairs ('home'=>1234,'away'=>5678)
  #  and use these to manually process the pbp descips before it gets to the "standard" update
  my %matches = ( );
  my @dupes = ( );

  foreach my $team ('away', 'home') {
    my %players = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}};
    foreach my $remote_id (keys %players) {
      $remote_id = substr($remote_id, 2); # Strip leading 'ID'
      # Determine
      my %player = %{$players{"ID$remote_id"}};
      my $name = $player{'name'}{'first'} . ' ' . $player{'name'}{'last'};
      # Process
      if (!defined($matches{$name})) {
        # First time we've found this player
        %{$matches{$name}} = ( $team => $remote_id );
      } elsif (defined($matches{$name}{$team})) {
        # Existing match... within the same team!
        #  Note: Not sure this can happen, but flagging as an error, just in case...
        print STDERR "Identified two players on the same team with the same name. Name: '$name'; IDs: $matches{$name}{$team}, $remote_id. Aborting.\n";
        exit;
      } else {
        $matches{$name}{$team} = $remote_id;
        push @dupes, $name;
      }
    }
  }

  # Process the dupes...
  if (@dupes) {
    print STDERR "#  Duplicate player names found, manually parsing the play-by-play: '" . join("', '", @dupes) . "'.\n";
    foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
      # Which team is batting?
      # First the actions
      foreach my $action (@{$$play{'actions'}}) {
        my $act = (defined($$play{'pitchEvents'}[$action]{'details'}) ? $$play{'pitchEvents'}[$action]{'details'} : $$play{'pitchEvents'}[$action]);
        foreach my $dupe (@dupes) {
          # Skip if duplicate of the main AB result
          next if $$act{'description'} eq $$play{'result'}{'description'};
          $$act{'description_raw'} = $$act{'description'}; # Preserve (useful for debugging)
          $$act{'description'} = fix_same_name_pbp__parse($$act{'description'}, $dupe, $matches{$dupe},$act)
            if (index($$act{'description'}, $dupe) != -1);
        }
      }
      # Then check the play's result
      foreach my $dupe (@dupes) {
        $$play{'result'}{'description_raw'} = $$play{'result'}{'description'}; # Preserve (useful for debugging)
        $$play{'result'}{'description'} = fix_same_name_pbp__parse($$play{'result'}{'description'}, $dupe, $matches{$dupe}, $$play{'about'}{'halfInning'})
          if (index($$play{'result'}{'description'}, $dupe) != -1);
      }
    }
  }
}

sub fix_same_name_pbp__parse {
  my ($descrip, $name, $ids, $extra) = @_;
  my $team;
  $name = parse_name_simplified($name);

  # Defensive changes => pitching team (usually...)
  # Offensive changes => batting team
  if (ref($extra) eq 'HASH') {
    $team = defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{"ID$$extra{'player'}"}) ? 'home' : 'away';

  # Plays are a bit more fiddly to determine...
  } elsif ((substr($descrip, 0, length($name)) eq $name)
       || (substr($descrip, 0, length("With $name")) eq "With $name")) {
    # Is batter
    $team = ($extra eq 'top' ? 'away' : 'home');
  } elsif ($descrip =~ m/\.\s+$name (?:to|scores|out)/) {
    # Is baserunner
    $team = ($extra eq 'top' ? 'away' : 'home');
  } else {
    # Is defense
    $team = ($extra eq 'top' ? 'home' : 'away');
  }

  my $rplc = "{{PLAYER_', \@player_$$ids{$team}, '}}";
  $descrip =~ s/$name/$rplc/g;
  return $descrip;
}

#
# Re-calc at-bat RBIs from the runners
#
sub fix_ab_rbis {
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    next if !$$play{'result'}{'rbi'};
    $$play{'result'}{'rbi'} = 0;
    foreach my $runner (@{$$play{'runners'}}) {
      $$play{'result'}{'rbi'} += int($$runner{'details'}{'rbi'});
    }
    $$play{'result'}{'rbi'} = '' . $$play{'result'}{'rbi'}; # Typecast...
  }
}

#
# Ad Hoc / Per game fixes that cannot (needn't?) be generalised
#
sub fix_ad_hoc {
  # 2008, Regular, 235554 / 235569: Andy Phillips tagged against wrong team
  if ($game_ref eq '2008//regular//235554' || $game_ref eq '2008//regular//235569') {
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID425429'}{'uniform'} = 'CIN';
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID425429'}{'teamID'} = $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID425429'}{'uniformID'} = '113';
    $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID425429'}{'uniform'} = 'CIN';
    $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID425429'}{'teamID'} = $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID425429'}{'uniformID'} = '113';

  # 2008, Regular, 236083: First Play in Top 4th has a dodgy pitchEvent list
  } elsif ($game_ref eq '2008//regular//236083') {
    $$gamedata{'liveData'}{'plays'}{'allPlays'}[37]{'actions'} = [];
    $$gamedata{'liveData'}{'plays'}{'allPlays'}[37]{'pitches'} = [160,161,162];
    for (my $i = 0; $i < 3; $i++) {
      $$gamedata{'liveData'}{'plays'}{'allPlays'}[37]{'pitchEvents'}[160 + $i]{'pitchNum'} = ($i + 1);
    }
    my @rmv = splice(@{$$gamedata{'liveData'}{'plays'}{'allPlays'}[37]{'pitchEvents'}}, 0, 159);

  # 2009, Regular, 245842: First Play in Top 9th has a dodgy pitchEvent list
  } elsif ($game_ref eq '2009//regular//245842') {
    $$gamedata{'liveData'}{'plays'}{'allPlays'}[74]{'actions'} = [0,1];
    $$gamedata{'liveData'}{'plays'}{'allPlays'}[74]{'pitches'} = [2,3,4,5,6];
    my @rmv = splice(@{$$gamedata{'liveData'}{'plays'}{'allPlays'}[74]{'pitchEvents'}}, 7);

  # 2017, Regular, 491814: Mark Leiter in play-by-play as Mark Leiter Jr.
  } elsif ($game_ref eq '2017//regular//491814') {
    for (my $i = 0; $i < @{$$gamedata{'liveData'}{'plays'}{'allPlays'}}; $i++) {
      for (my $j = 0; $j < @{$$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'playEvents'}}; $j++) {
        $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'playEvents'}[$j]{'details'}{'description'} =~ s/Mark Leiter Jr\.  /Mark Leiter/g
          if defined($$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'playEvents'}[$j]{'details'}{'description'});
      }
      $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'result'}{'description'} =~ s/Mark Leiter Jr\.  /Mark Leiter/g;
    }

  # 2017, Regular, 492008: Chris Young tagged against wrong team
  } elsif ($game_ref eq '2017//regular//492008') {
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID455759'}{'uniform'} = 'BOS';
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID455759'}{'teamID'} = $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID455759'}{'uniformID'} = '111';
    $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID455759'}{'uniform'} = 'BOS';
    $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID455759'}{'teamID'} = $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID455759'}{'uniformID'} = '111';

  # 2017, Regular, 492523: Chris Flexen is in the play-by-play as Christopher
  } elsif ($game_ref eq '2017//regular//492523') {
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{'away'}{'players'}{'ID623167'}{'name'}{'first'}
      = $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID623167'}{'name'}{'first'}
      = 'Christopher';

  # 2018, Regular, 530456: Jose Miguel Fernandez is in the play-by-play as Jose
  } elsif ($game_ref eq '2018//regular//530456') {
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{'away'}{'players'}{'ID628336'}{'name'}{'first'}
      = $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID628336'}{'name'}{'first'}

  # 2018, Regular, 531508: Richard Urena is in the play-by-play as Urenn
  } elsif ($game_ref eq '2018//regular//531508') {
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID620446'}{'name'}{'last'}
      = $$gamedata{'liveData'}{'players'}{'allPlayers'}{'ID620446'}{'name'}{'last'}
      = 'Urena';
  }
}

# Return true to pacify the compiler
1;
