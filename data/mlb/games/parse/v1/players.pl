#!/usr/bin/perl -w
# Tables used
our %config;
@{$config{'tables'}{'players'}} = (
  'SPORTS_MLB_GAME_ROSTERS',
);

# Player processing
sub parse_players {
  our %roster = %{$$gamedata{'liveData'}{'players'}{'allPlayers'}};
  my @teams = ( { 'team_id' => $$game_info{'visitor_id'}, 'type' => 'visitor', 'type_rem' => 'away' },
                { 'team_id' => $$game_info{'home_id'},    'type' => 'home',    'type_rem' => 'home' } );
  foreach our $t ( @teams ) {
    print "## $$t{'team_id'} (" . ucfirst($$t{'type'}) . ")\n";
    our %info = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$$t{'type_rem'}}};

    # Get the four lists: pitchers (who pitched), bullpen (who did not), batters (who hit), bench (who did not)
    #  Note: pitchers could be included in the batter list!!
    my %list = (
      'pitchers' => $info{'pitchers'},
      'bullpen' => $info{'bullpen'},
      'batters' => $info{'batters'}, # We'll later manually have to exclude the pitchers who are included in this list
      'bench' => $info{'bench'},
    );
    my $pitcher_list = ':' . join(':', @{$list{'pitchers'}}) . ':';

    # Now loop through and create our player info
    # Batters
    foreach my $remote_id (@{$list{'batters'}}) {
      next if ($pitcher_list =~ m/:$remote_id:/);
      parse_players_ind($remote_id, 1, int(!($info{'players'}{"ID$remote_id"}{'gameStats'}{'batting'}{'battingOrder'} % 100)));
    }
    # Batters (DNP)
    foreach my $remote_id (@{$list{'bench'}}) {
      parse_players_ind($remote_id, 0, 0);
    }
    # Pitchers
    my $prev_pitchers = 0;
    foreach my $remote_id (@{$list{'pitchers'}}) {
      my $pitches = $info{'players'}{"ID$remote_id"}{'gameStats'}{'pitching'}{'pitchesThrown'};
      parse_players_ind($remote_id, 1, int(!$prev_pitchers && ($pitches>0)));
      $prev_pitchers += $pitches;
    }
    # Pitchers (DNP)
    foreach my $remote_id (@{$list{'bullpen'}}) {
      parse_players_ind($remote_id, 0, 0);
    }
    print "\n";
  }
}

# Process an individual field
sub parse_players_ind {
  our ($season, $game_type, $game_id);
  my ($remote_id, $played, $started) = @_;

  # Validate, as there are a handful of known issues within the data
  return if fix_invalid_player("$season//$game_type//$$t{'team_id'}//$remote_id");

  # Get the info
  my %player = %{$info{'players'}{"ID$remote_id"}};
  my $jersey = $player{'shirtNum'}; check_for_null(\$jersey);
  $player{'shirtNum'} = '' if !defined($player{'shirtNum'});
  my $first_name = convert_text($player{'name'}{'first'});
  my $surname = convert_text($player{'name'}{'last'});
  my $stands = $player{'stands'}; $stands = undef if defined($stands) && $stands !~ m/^[LRS]$/; check_for_null(\$stands);
  my $throws = $player{'rightLeft'}; $throws = undef if defined($throws) && $throws !~ m/^[LRS]$/; check_for_null(\$throws);
  # Process the game positions
  my @pos_col = ( );
  my @pos = ( ); my @pos_skipped = ( );
  if (defined($player{'position'})) {
    my $alt_pos = 0;
    foreach my $p (split('-', $player{'position'})) {
      # SP/RP identification...
      if ($p eq 'P') {
        $p = ($started ? 'S' : 'R') . $p;
      }
      # Add to our list (if they actually played!)
      if ($played) {
        my $skip = 0;
        if (!@pos && $started) {
          push @pos_col, 'pos';
        } elsif ($alt_pos < 8) {
          push @pos_col, 'pos_alt' . (++$alt_pos);
        } else {
          push @pos_skipped, $p;
          $skip = 1;
        }
        push @pos, $p
          if !$skip;
      }
    }
  }
  # Roster position may not be supplied within the JSON bundle, so check and convert
  my $pos = $roster{"ID$remote_id"}{'position'};
  $pos = $player{'position'}
    if !defined($pos);
  my $roster_pos = fix_roster_pos($pos);
  my $roster_pos_cmt = (defined($roster_pos) ? $roster_pos : '-');
  check_for_null(\$roster_pos);
  # Display
  print "\n# $player{'name'}{'first'} $player{'name'}{'last'} #$player{'shirtNum'} (Roster Pos: $roster_pos_cmt; Game Pos: " . (@pos ? join(',', @pos) : '-') . "; Played: $played; Started: $started)\n";
  print "#   -> Positions skipped (" . @pos_skipped . "): " . join(',', @pos_skipped) . "\n" if @pos_skipped;
  print "SELECT `player_id` INTO \@player_$remote_id FROM `SPORTS_MLB_PLAYERS_IMPORT` WHERE `remote_id` = '$remote_id';\n";
  print "INSERT IGNORE INTO `SPORTS_MLB_PLAYERS_IMPORT` (`player_id`, `remote_id`, `profile_imported`)
  VALUES (\@player_$remote_id, '$remote_id', NULL);\n";
  print "SELECT `player_id` INTO \@player_$remote_id FROM `SPORTS_MLB_PLAYERS_IMPORT` WHERE `remote_id` = '$remote_id';\n";
  print "INSERT IGNORE INTO `SPORTS_MLB_PLAYERS` (`player_id`, `first_name`, `surname`, `stands`, `throws`)
  VALUES (\@player_$remote_id, '$first_name', '$surname', $stands, $throws);\n";
  print "INSERT INTO `SPORTS_MLB_GAME_ROSTERS` (`season`, `game_type`, `game_id`, `team_id`, `player_id`, `jersey`, `played`, `started`, `roster_pos`" . (@pos_col ? ", `" . join("`, `", @pos_col) . "`" : '') . ")
  VALUES ('$season', '$game_type', '$game_id', '$$t{'team_id'}', \@player_$remote_id, $jersey, '$played', '$started', $roster_pos" . (@pos ? ", '" . join("', '", @pos) . "'" : '') . ");\n";
}

# Ignore rows of players we know to be incorrect in a given season
sub fix_invalid_player {
  my ($player_ref) = @_;

  my @invalid_list = (
    # Eric Hinske, in 2008, played for TB but was also listed for PIT
    '2008//regular//PIT//400134',
    # Mike Jacobs, in 2008, played for MIA but was also listed for KC
    '2008//regular//KC//408312',
    # Andy Phillips, in 2008, played for CIN & NYM but was also listed for PIT
    '2008//regular//PIT//425429',
    # Mike Carp, from 2009-11, played for SEA but was also listed for ARI
    '2008//regular//ARI//455077',
  );

  return grep(/^$player_ref$/, @invalid_list);
}

# Clean the data we've received (as there's known errors / unexpected values within the gamedata)
sub fix_roster_pos {
  my ($pos) = @_;

  # Multiple positions listed
  if ($pos =~ m/\-/) {
    print "# Splitting position (game): '$pos'\n";
    ($pos) = split('-', $pos);
  }

  # Unknown abbreviations
  if ($pos eq 'D') {
    print "# Found ambiguous position (game): '$pos', returning DH\n";
    return 'DH';
  } elsif ($pos eq 'O' || $pos eq 'OF') {
    print "# Found ambiguous position (game): '$pos', returning CF\n";
    return 'CF';
  } elsif ($pos eq 'I' || $pos eq 'IF') {
    print "# Found ambiguous position (game): '$pos', returning SS\n";
    return 'SS';
  }
  return $pos;
}

# Return true to pacify the compiler
1;
