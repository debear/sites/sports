#!/usr/bin/perl -w
# Tables used
our %config;
@{$config{'tables'}{'players'}} = (
  'SPORTS_MLB_GAME_ROSTERS',
);

# Player processing
sub parse_players {
  our %roster = %{$$gamedata{'gameData'}{'players'}};
  my @teams = ( { 'team_id' => $$game_info{'visitor_id'}, 'type' => 'visitor', 'type_rem' => 'away' },
                { 'team_id' => $$game_info{'home_id'},    'type' => 'home',    'type_rem' => 'home' } );
  foreach our $t ( @teams ) {
    print "## $$t{'team_id'} (" . ucfirst($$t{'type'}) . ")\n";
    our %info = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$$t{'type_rem'}}};

    # Get the four lists: pitchers (who pitched), bullpen (who did not), batters (who hit), bench (who did not)
    #  Note: pitchers could be included in the batter list!!
    my %list = (
      'pitchers' => $info{'pitchers'},
      'bullpen' => $info{'bullpen'},
      'batters' => $info{'batters'}, # We'll later manually have to exclude the pitchers who are included in this list
      'bench' => $info{'bench'},
    );
    my $pitcher_list = ':' . join(':', @{$list{'pitchers'}}) . ':';

    # Now loop through and create our player info
    # Batters
    foreach my $remote_id (@{$list{'batters'}}) {
      next if ($pitcher_list =~ m/:$remote_id:/);
      parse_players_ind($remote_id, 1, int(!($info{'players'}{"ID$remote_id"}{'battingOrder'} % 100)));
    }
    # Batters (DNP)
    foreach my $remote_id (@{$list{'bench'}}) {
      parse_players_ind($remote_id, 0, 0);
    }
    # Pitchers
    my $prev_pitchers = 0;
    foreach my $remote_id (@{$list{'pitchers'}}) {
      my $pitches = $info{'players'}{"ID$remote_id"}{'stats'}{'pitching'}{'numberOfPitches'};
      parse_players_ind($remote_id, 1, int(!$prev_pitchers && ($pitches > 0)));
      $prev_pitchers += $pitches;
    }
    # Pitchers (DNP)
    foreach my $remote_id (@{$list{'bullpen'}}) {
      parse_players_ind($remote_id, 0, 0);
    }
    print "\n";
  }
}

# Process an individual field
sub parse_players_ind {
  our ($season, $game_type, $game_id);
  my ($remote_id, $played, $started) = @_;

  # Validate, as there are a handful of known issues within the data
  return if fix_invalid_player("$season//$game_type//$$t{'team_id'}//$remote_id");

  # Get the info
  my %full = %{$roster{"ID$remote_id"}};
  my %player = %{$info{'players'}{"ID$remote_id"}};
  my $jersey = (defined($player{'jerseyNumber'}) ? $player{'jerseyNumber'} : $full{'primaryNumber'});
  my $jersey_cmt = defined($jersey) ? $jersey : '';
  check_for_null(\$jersey);
  my $first_name = convert_text($full{'useName'});
  my $surname = convert_text($full{'lastName'});
  my $stands = $full{'batSide'}{'code'}; $stands = undef if defined($stands) && $stands !~ m/^[LRS]$/; check_for_null(\$stands);
  my $throws = $player{'pitchHand'}{'code'}; $throws = undef if defined($throws) && $throws !~ m/^[LRS]$/; check_for_null(\$throws);
  # Process the game positions
  my @pos_col = ( );
  my @pos = ( ); my @pos_skipped = ( );
  if (defined($player{'allPositions'})) {
    my $alt_pos = 0;
    foreach my $pos_item (@{$player{'allPositions'}}) {
      my $p = $$pos_item{'abbreviation'};
      # SP/RP identification...
      $p = ($started ? 'S' : 'R') . $p
        if $p eq 'P';
      # Add to our list (if they actually played!)
      if ($played) {
        my $skip = 0;
        if (!@pos && $started) {
          push @pos_col, 'pos';
        } elsif ($alt_pos < 8) {
          push @pos_col, 'pos_alt' . (++$alt_pos);
        } else {
          push @pos_skipped, $p;
          $skip = 1;
        }
        push @pos, $p
          if !$skip;
      }
    }
  }
  # Roster position may not be supplied within the JSON bundle, so check and convert
  my $pos = $full{'primaryPosition'}{'abbreviation'};
  $pos = $player{'position'}{'abbreviation'}
    if !defined($pos);
  my $roster_pos = fix_roster_pos($pos);
  my $roster_pos_cmt = (defined($roster_pos) ? $roster_pos : '-');
  check_for_null(\$roster_pos);
  # Display
  print "\n# $first_name $surname #$jersey_cmt (Roster Pos: $roster_pos_cmt; Game Pos: " . (@pos ? join(',', @pos) : '-') . "; Played: $played; Started: $started)\n";
  print "#   -> Positions skipped (" . @pos_skipped . "): " . join(',', @pos_skipped) . "\n" if @pos_skipped;
  print "SELECT `player_id` INTO \@player_$remote_id FROM `SPORTS_MLB_PLAYERS_IMPORT` WHERE `remote_id` = '$remote_id';\n";
  print "INSERT IGNORE INTO `SPORTS_MLB_PLAYERS_IMPORT` (`player_id`, `remote_id`, `profile_imported`)
  VALUES (\@player_$remote_id, '$remote_id', NULL);\n";
  print "SELECT `player_id` INTO \@player_$remote_id FROM `SPORTS_MLB_PLAYERS_IMPORT` WHERE `remote_id` = '$remote_id';\n";
  print "INSERT IGNORE INTO `SPORTS_MLB_PLAYERS` (`player_id`, `first_name`, `surname`, `stands`, `throws`)
  VALUES (\@player_$remote_id, '$first_name', '$surname', $stands, $throws);\n";
  print "INSERT INTO `SPORTS_MLB_GAME_ROSTERS` (`season`, `game_type`, `game_id`, `team_id`, `player_id`, `jersey`, `played`, `started`, `roster_pos`" . (@pos_col ? ", `" . join("`, `", @pos_col) . "`" : '') . ")
  VALUES ('$season', '$game_type', '$game_id', '$$t{'team_id'}', \@player_$remote_id, $jersey, '$played', '$started', $roster_pos" . (@pos ? ", '" . join("', '", @pos) . "'" : '') . ");\n";
}

# Ignore rows of players we know to be incorrect in a given season
sub fix_invalid_player {
  my ($player_ref) = @_;

  #my @invalid_list = ();
  #return grep(/^$player_ref$/, @invalid_list);

  # Functionality preserved, but not yet required
  return 0;
}

# Clean the data we've received (as there's known errors / unexpected values within the gamedata)
sub fix_roster_pos {
  my ($pos) = @_;
  my $fix_intro = "\n# Roster position modifed from the raw '$pos' for the following player\n# -";

  # Multiple positions listed
  if ($pos =~ m/\-/) {
    ($pos) = split('-', $pos);
    print "$fix_intro Splitting, resulting in '$pos'\n";
    $fix_intro = "# -";
  }

  # Unknown abbreviations
  if ($pos eq 'D') {
    print "$fix_intro Considered ambiguous position, returning 'DH'\n";
    return 'DH';
  } elsif ($pos eq 'O' || $pos eq 'OF') {
    print "$fix_intro Considered ambiguous position, returning 'CF'\n";
    return 'CF';
  } elsif ($pos eq 'I' || $pos eq 'IF') {
    print "$fix_intro Considered ambiguous position, returning 'SS'\n";
    return 'SS';
  } elsif ($pos eq 'TWP') {
    return 'TW'; # We use a shortened map
  }
  return $pos;
}

# Return true to pacify the compiler
1;
