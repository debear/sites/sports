#!/usr/bin/perl -w
use Switch;
use JSON;
use Math::Trig qw(rad2deg);

# Tables used
@{$config{'tables'}{'play_by_play'}} = (
  'SPORTS_MLB_GAME_PLAYS',
  'SPORTS_MLB_GAME_ATBAT',
  'SPORTS_MLB_GAME_ATBAT_FLAGS',
  'SPORTS_MLB_GAME_ATBAT_PITCHES',
  'SPORTS_MLB_GAME_SCORING',
  'SPORTS_MLB_GAME_WINPROB',
);

# PxP processing
sub parse_play_by_play {
  our %plays = ( );

  # Setup: initial fielding positions
  our %field_pos = ( );
  foreach my $team ('home','away') {
    my %team = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}};
    foreach my $id (sort keys %{$team{'players'}}) {
      my $remote_id = substr($id, 2);
      my %player = %{$team{'players'}{$id}};
      if (defined($player{'battingOrder'}) && substr($player{'battingOrder'}, -2) eq '00') {
        $field_pos{$remote_id} = defined($player{'allPositions'}) ? $player{'allPositions'}[0]{'abbreviation'} : $player{'position'}{'abbreviation'};
      }
    }
  }
  benchmark_stamp('Play-by-Play: Setup Fielding Pos');

  # Setup: atBatIndex != array index (if we're passed an atBatIndex...)
  my $i = 0;
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    $plays{defined($$play{'atBatIndex'}) ? $$play{'atBatIndex'} : $i++} = $play;
  }
  benchmark_stamp('Play-by-Play: Setup atBatIndex Check');

  #
  # Pass 1: Team/Player Description Interpolation
  #
  # Step 1: Merge in to a single list (so we can interpolate each player once across all descrips)
  our %pbp_descrips = ( );
  my $inning = 0; our $play_id = 0; our @pbp_descrips = ();
  foreach my $inn (@{$$gamedata{'liveData'}{'plays'}{'playsByInning'}}) {
    $inning++;
    # Top
    foreach my $i (@{$$inn{'top'}}) {
      parse_play_by_play_descrip($plays{$i});
    }
    # Bottom
    foreach my $i (@{$$inn{'bottom'}}) {
      parse_play_by_play_descrip($plays{$i});
    }
  }
  my $pbp_descrips = parse_name_simplified(join('###', @pbp_descrips)); # Include name simplification for below comparisons
  parse_descrip_hacks(\$pbp_descrips); # For the joyful fudges we need to apply...
  # Step 2: Team interpolation on this string
  my $home = $$game_info{'home_id'};
  my $visitor = $$game_info{'visitor_id'};
  $pbp_descrips =~ s/$$game_info{'home_city_regex'} $$game_info{'home_regex'}/{{TEAM_$home}}/gi;
  $pbp_descrips =~ s/$$game_info{'home_regex'}/{{TEAM_$home}}/gi;
  $pbp_descrips =~ s/$$game_info{'visitor_city_regex'} $$game_info{'visitor_regex'}/{{TEAM_$visitor}}/gi;
  $pbp_descrips =~ s/$$game_info{'visitor_regex'}/{{TEAM_$visitor}}/gi;

  # Step 3: Player interpolation on this string
  my @boxnames = ( );
  # Order by the players by name length so we work from most specific to the least specific
  #  i.e., Jim James won't be used in the interpolated instead of John Jameson
  my @all_players = sort {
    # Build our comparisons
    my %player_a = %{$$gamedata{'gameData'}{'players'}{$a}};
    my %player_b = %{$$gamedata{'gameData'}{'players'}{$b}};

    length("$player_b{'fullName'}") <=> length("$player_a{'fullName'}")
      ||
    lc($player_b{'boxscoreName'}) cmp lc($player_a{'boxscoreName'})
  } keys(%{$$gamedata{'gameData'}{'players'}});
  # Now loop through in this order
  foreach my $remote_id (@all_players) {
    my $player = $$gamedata{'gameData'}{'players'}{$remote_id};
    my $uname = parse_name_simplified($$player{'useName'});
    my $fname = parse_name_simplified($$player{'firstName'});
    my $sname = parse_name_simplified($$player{'lastName'});
    my $full_name = parse_name_simplified($$player{'fullName'});
    push @boxnames, parse_name_simplified($$player{'boxscoreName'});

    # Main name given to us
    my $name = $full_name;
    my $rplc = "{{PLAYER_', \@player_$$player{'id'}, '}}";
    parse_descrip_names(\$pbp_descrips, $name, $rplc);

    # Some alternative combinations
    $name = "$fname $sname";
    parse_descrip_names(\$pbp_descrips, $name, $rplc);
    $name = "$uname $sname";
    parse_descrip_names(\$pbp_descrips, $name, $rplc);

    # Alternative versions
    # X.Y. Z as X. Z and Y. Z
    if ($uname =~ m/^[A-Z]\.[A-Z]\./) {
      # X. Z
      my ($alt_fname) = ($uname =~ m/^([A-Z]\.)/);
      parse_descrip_names(\$pbp_descrips, "$alt_fname $sname", $rplc);

      # Y. Z
      ($alt_fname) = ($uname =~ m/([A-Z]\.)$/);
      parse_descrip_names(\$pbp_descrips, "$alt_fname $sname", $rplc);
    }

    # X Y Jr.
    if ($sname =~ m/\s+Jr\.?$/i) {
      my $alt_name = "$uname $sname";
      $alt_name =~ s/\s+Jr\.?$//i;
      parse_descrip_names(\$pbp_descrips, $alt_name, $rplc);
    }
  }
  # Boxscore version (on its own, in case it over-writes a player we're yet to come to)
  foreach my $remote_id (@all_players) {
    my $player = $$gamedata{'gameData'}{'players'}{$remote_id};
    my $rplc = "{{PLAYER_', \@player_$$player{'id'}, '}}";
    my $alt_name = parse_name_simplified($$player{'boxscoreName'});
    parse_descrip_names(\$pbp_descrips, $alt_name, $rplc);
  }
  # Step 4: Convert this string into the hash we use later
  foreach my $pbp_descrip (split('###', $pbp_descrips)) {
    my ($id, $descrip) = ($pbp_descrip =~ m/^(.+)~~~(.+)$/);
    $pbp_descrips{$id} = "CONCAT('$descrip')";
  }
  benchmark_stamp('Play-by-Play: Descrip');

  #
  # Pass 2: Identifying the detailed plays
  #
  our $game_ref;
  my %ob_def = ( 'tot' => 0, '1b' => 0, '2b' => 0, '3b' => 0 );
  my %ob_alt = ( 'tot' => 1, '1b' => 0, '2b' => 1, '3b' => 0 );
  $inning = 0; $play_id = 0; our %pitcher_pitches = ( );
  our $home_score = 0; our $visitor_score = 0;
  foreach my $inn (@{$$gamedata{'liveData'}{'plays'}{'playsByInning'}}) {
    my $inning_ordinal = display_ordinal(++$inning);

    # Does this inning include a runner on 2nd base?
    my $ob_ro2b = ($config{'ob_2b_extras'} && $inning > $config{'regulation_innings'});

    # Top
    print "\n#\n# Top " . $inning_ordinal . "\n#\n";
    our %ob = (!$ob_ro2b ? %ob_def : %ob_alt); our $out = 0;
    foreach my $i (@{$$inn{'top'}}) {
      parse_play_by_play_event($plays{$i});
    }
    # Bottom
    print "\n#\n# Bottom " . $inning_ordinal . "\n#\n";
    %ob = (!$ob_ro2b ? %ob_def : %ob_alt); $out = 0;
    foreach my $i (@{$$inn{'bottom'}}) {
      parse_play_by_play_event($plays{$i});
    }
    print "# Not required...\n" if !@{$$inn{'bottom'}};
    benchmark_stamp("Play-by-Play: Plays: $inning_ordinal");
  }

  # Some post-processing
  print "\n##\n## Play-by-Play Post-Processing\n##\nCALL mlb_game_pbp_process('$season', '$game_type', '$game_id');\n";
}

# Parse the description of an event for interpolation
sub parse_play_by_play_descrip {
  my ($play) = @_;
  return if !defined($play) || $$play{'skip'};

  # Misc events
  foreach my $action (@{$$play{'actionIndex'}}) {
    my %action = %{$$play{'playEvents'}[$action]};
    %action = %{$action{'details'}} if defined($action{'details'});
    # Skip if duplicate of the main AB result
    next if $action{'skip'};
    $action{'description'} = $action{'event'}
      if !defined($action{'description'}) || !$action{'description'};
    push @pbp_descrips, ++$play_id . '~~~' . parse_descrip($action{'description'});
  }

  # In a handful of instances, we need to take the description from the event type
  if (!defined($$play{'result'}{'description'}) || $$play{'result'}{'description'} eq '') {
    my %batter = %{$$gamedata{'gameData'}{'players'}{'ID'.$$play{'matchup'}{'batter'}{'id'}}};
    if (defined($$play{'result'}{'event'})) {
      $$play{'result'}{'description'} = $batter{'fullName'} . ' ' . lc($$play{'result'}{'event'});
    } else {
      $$play{'result'}{'description'} = $batter{'fullName'} . ' unknown play';
      $$play{'result'}{'event'} = 'Unknown';
    }
  }
  # The play
  push @pbp_descrips, ++$play_id . '~~~' . parse_descrip($$play{'result'}{'description'});
}

# Parse an individual event
sub parse_play_by_play_event {
  my ($play) = @_;
  return if !defined($play) || $$play{'skip'};

  # Summary info
  my $half = ($$play{'about'}{'halfInning'} eq 'top' ? 'top' : 'bot');
  my %pitcher = %{$$gamedata{'gameData'}{'players'}{'ID'.$$play{'matchup'}{'pitcher'}{'id'}}};
  $pitcher{'team_id'} = ($half eq 'top' ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
  $pitcher{'primaryNumber'} = $$gamedata{'liveData'}{'boxscore'}{'teams'}{$half eq 'top' ? 'home' : 'away'}{'players'}{'ID'.$pitcher{'id'}}
    if !defined($pitcher{'primaryNumber'});
  my $pitcher_hand = $pitcher{'pitchHand'}{'code'};
  $pitcher_hand = $$play{'matchup'}{'pitchHand'}{'code'}
    if defined($$play{'matchup'}{'pitchHand'}{'code'});
  my %batter = %{$$gamedata{'gameData'}{'players'}{'ID'.$$play{'matchup'}{'batter'}{'id'}}};
  $batter{'team_id'} = ($half eq 'top' ? $$game_info{'visitor_id'} : $$game_info{'home_id'});
  $batter{'primaryNumber'} = $$gamedata{'liveData'}{'boxscore'}{'teams'}{$half eq 'top' ? 'away' : 'home'}{'players'}{'ID'.$batter{'id'}}
    if !defined($batter{'primaryNumber'});
  my $batter_hand = (defined($$play{'matchup'}{'batSide'}{'code'}) ? $$play{'matchup'}{'batSide'}{'code'} : $batter{'batSide'}{'code'});
  print "\n"; # Given propensity for wanting to write the next line to STDERR without the preceding new-line, have this on its own line
  print "# Pitcher: $pitcher{'fullName'} ($pitcher{'team_id'} #$pitcher{'primaryNumber'}); Batter: $batter{'fullName'} ($batter{'team_id'} #$batter{'primaryNumber'}, $batter_hand); $ob{'tot'} on, $out out ($$play{'result'}{'event'})\n";

  # Misc events
  foreach my $action (@{$$play{'actionIndex'}}) {
    my %action = %{$$play{'playEvents'}[$action]};
    %action = %{$action{'details'}} if defined($action{'details'});
    # Skip if duplicate of the main AB result
    next if $action{'skip'};
    print "#  Action: " . (defined($action{'description_raw'}) ? $action{'description_raw'} : $action{'description'}) . "\n";
    my $descrip = $pbp_descrips{++$play_id};
    # Pass the original action block
    descrip_set_fielding_pos($descrip, \%pitcher, $$play{'playEvents'}[$action]);

    #
    # Display the action's play
    #
    $home_score = $action{'homeScore'} if defined($action{'homeScore'});
    $visitor_score = $action{'awayScore'} if defined($action{'awayScore'});
    print "INSERT INTO `SPORTS_MLB_GAME_PLAYS` (`season`, `game_type`, `game_id`, `play_id`, `inning`, `half_inning`, `batter_team_id`, `batter`, `pitcher_team_id`, `pitcher`, `on_base`, `ob_1b`, `ob_2b`, `ob_3b`, `out`, `description`, `scoring`, `home_score`, `visitor_score`, `home_winprob`, `visitor_winprob`, `change_winprob`)
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$$play{'about'}{'inning'}', '$half', '$batter{'team_id'}', '$batter{'primaryNumber'}', '$pitcher{'team_id'}', '$pitcher{'primaryNumber'}', '$ob{'tot'}', '$ob{'1b'}', '$ob{'2b'}', '$ob{'3b'}', '$out', $descrip, '" . int(defined($action{'isScoringPlay'}) && $action{'isScoringPlay'}) . "', '$home_score', '$visitor_score', NULL, NULL, NULL);\n";
  }

  # Do we need to emulate the scoring updates on a scoring play?
  if (defined($$play{'about'}{'isScoringPlay'}) && $$play{'about'}{'isScoringPlay'}) {
    my $inn_team = ($$play{'about'}{'halfInning'} eq 'top' ? 'away' : 'home');
    if (!defined($$play{'result'}{$inn_team . 'Score'})) {
      $$play{'result'}{$inn_team . 'Score'} = ($$play{'about'}{'halfInning'} eq 'top' ? $visitor_score : $home_score);
      foreach my $runner (@{$$play{'runners'}}) {
        $$play{'result'}{$inn_team . 'Score'}++
          if (defined($$runner{'details'}{'isScoringEvent'}) && $$runner{'details'}{'isScoringEvent'});
      }
    }
  }

  # General At Bat info
  my $descrip = $pbp_descrips{++$play_id};
  descrip_set_fielding_pos($descrip, \%pitcher);
  $home_score = $$play{'result'}{'homeScore'} if defined($$play{'result'}{'homeScore'});
  $visitor_score = $$play{'result'}{'awayScore'} if defined($$play{'result'}{'awayScore'});
  my $home_winprob = (defined($$play{'winprob'}) && $$play{'winprob'}{'home'} ne 'NaN' ? $$play{'winprob'}{'home'} : undef); check_for_null(\$home_winprob);
  my $visitor_winprob = (defined($$play{'winprob'}) && $$play{'winprob'}{'visitor'} ne 'NaN' ? $$play{'winprob'}{'visitor'} : undef); check_for_null(\$visitor_winprob);
  my $change_key = ($half eq 'top' ? 'visitor_change' : 'home_change');
  my $change_winprob = (defined($$play{'winprob'}) && $$play{'winprob'}{$change_key} ne 'NaN' ? $$play{'winprob'}{$change_key} : undef); check_for_null(\$change_winprob);
  print "INSERT INTO `SPORTS_MLB_GAME_PLAYS` (`season`, `game_type`, `game_id`, `play_id`, `inning`, `half_inning`, `batter_team_id`, `batter`, `pitcher_team_id`, `pitcher`, `on_base`, `ob_1b`, `ob_2b`, `ob_3b`, `out`, `description`, `scoring`, `home_score`, `visitor_score`, `home_winprob`, `visitor_winprob`, `change_winprob`)
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$$play{'about'}{'inning'}', '$half', '$batter{'team_id'}', '$batter{'primaryNumber'}', '$pitcher{'team_id'}', '$pitcher{'primaryNumber'}', '$ob{'tot'}', '$ob{'1b'}', '$ob{'2b'}', '$ob{'3b'}', '$out', $descrip, '" . int($$play{'about'}{'isScoringPlay'}) . "', '$home_score', '$visitor_score', $home_winprob, $visitor_winprob, $change_winprob);\n";

  # Per-pitch breakdown
  my @pitches = ( ); my @ab_pitches = ( );
  my %ab_pitches = ('col' => '', 'val' => ''); my %ab_count = ('balls' => 0, 'strikes' => 0); my $pitch_zone; my $hit_spray;
  my $i = 0; my $count = 0; my $counts_set = 1; my @counts = ('0-0'); my $pitch_num_last = 0; my %hit_info;
  foreach my $event (@{$$play{'playEvents'}}) {
    my $val;

    # Pickoff?
    if (defined($$event{'type'}) && $$event{'type'} eq 'pickoff') {
      # Though we only care about attempts (successful pickoffs are marked as subsequent actions for some reason)
      $val = 'P' . substr($$event{'details'}{'description'}, -2, 1)
        if $$event{'details'}{'description'} =~ m/attempt/i;
      push @ab_pitches, ['P' . substr($$event{'details'}{'description'}, -2, 1), $$event{'details'}{'description'}, undef, undef, undef, undef]
        if $$event{'details'}{'description'} =~ m/attempt/i;
    # Was it an action?
    } elsif (grep(/^$i$/, @{$$play{'actions'}}) || !defined($$event{'details'}{'call'})) {
      # Though we only flag actions that occurred mid-at-bat
      $val = 'A'
        if @pitches;
      push @ab_pitches, ['A', $$event{'details'}{'event'}, undef, undef, undef, undef]
        if @pitches;
    # Nope, get info out about the pitch
    } else {
      # Parse
      $val = parse_pitch_code($$event{'details'}{'call'}{'code'});
      my $result = parse_pitch_result($$event{'details'}{'description'});
      # Store foul balls after 2nd strike as a foul ball
      $val = 'F'
        if $val eq 'S' && ($ab_count{'strikes'} == 2 && substr($result, 0, 4) eq 'Foul' && $result ne 'Foul Tip');
      # We may first have to adjust the strikezone info
      if (!defined($$event{'pitchData'}{'strikeZoneTop'}) || !defined($$event{'pitchData'}{'strikeZoneBottom'})
        || $$event{'pitchData'}{'strikeZoneTop'} > 4 || $$event{'pitchData'}{'strikeZoneBottom'} < 1
        || $$event{'pitchData'}{'strikeZoneTop'} < $$event{'pitchData'}{'strikeZoneBottom'}) {
        # If any one component is broken, try and take it from the player info (if known)
        if (defined($batter{'strikeZoneTop'}) && defined($batter{'strikeZoneBottom'})) {
          $$event{'pitchData'}{'strikeZoneTop'} = $batter{'strikeZoneTop'};
          $$event{'pitchData'}{'strikeZoneBottom'} = $batter{'strikeZoneBottom'};
        } else {
          $$event{'pitchData'}{'strikeZoneTop'} = undef;
          $$event{'pitchData'}{'strikeZoneBottom'} = undef;
        }
      }
      # Determine the height of the strikezone
      if (defined($$event{'pitchData'}{'strikeZoneTop'}) && defined($$event{'pitchData'}{'strikeZoneBottom'})) {
        $$event{'pitchData'}{'strikeZoneHeight'} = $$event{'pitchData'}{'strikeZoneTop'} - $$event{'pitchData'}{'strikeZoneBottom'};
      }
      # Determine pitch location relative to the strikezone
      my $pitch_x = $$event{'pitchData'}{'coordinates'}{'pX'};
      if (defined($pitch_x)) {
        # Normalise so the strikezone is -1.000..1.000
        $pitch_x = sprintf('%0.03f', $pitch_x / (8.5 / 12)) + 0; # Half plate width = 8.5", '+ 0 to typecast'
      }
      my $pitch_y = $$event{'pitchData'}{'coordinates'}{'pZ'};
      if (defined($pitch_y) && defined($$event{'pitchData'}{'strikeZoneHeight'})) {
        # Normalise so the strikezone is 0.000..1.000
        $pitch_y = sprintf('%0.03f', ($pitch_y - $$event{'pitchData'}{'strikeZoneBottom'}) / $$event{'pitchData'}{'strikeZoneHeight'}) + 0; # '+ 0' to typecast
      }
      push @ab_pitches, [$val, $result, $$event{'details'}{'type'}{'code'}, $$event{'pitchData'}{'startSpeed'}, $pitch_x, $pitch_y];
      # Pitch count may be implied (absence / undef => 0)
      my $balls = convert_undef_zero($$event{'count'}{'balls'});
      my $strikes = convert_undef_zero($$event{'count'}{'strikes'});
      # Note: Pitch info may not actually be provided...
      check_for_null(\$$event{'details'}{'type'}{'code'});
      check_for_null(\$$event{'pitchData'}{'startSpeed'});
      # Zone data
      $pitch_zone = undef;
      if (defined($$event{'pitchData'}{'zone'})) {
        switch ($$event{'pitchData'}{'zone'}) {
          # Inside
          case '1' { $pitch_zone = 'ITL'; }
          case '2' { $pitch_zone = 'ITC'; }
          case '3' { $pitch_zone = 'ITR'; }
          case '4' { $pitch_zone = 'IML'; }
          case '5' { $pitch_zone = 'IMC'; }
          case '6' { $pitch_zone = 'IMR'; }
          case '7' { $pitch_zone = 'IBL'; }
          case '8' { $pitch_zone = 'IBC'; }
          case '9' { $pitch_zone = 'IBR'; }
          # Outside
          case '11' { $pitch_zone = 'OTL'; }
          case '12' { $pitch_zone = 'OTR'; }
          case '13' { $pitch_zone = 'OBL'; }
          case '14' { $pitch_zone = 'OBR'; }
        }
      }
      # SQL management
      check_for_null(\$pitch_x);
      check_for_null(\$pitch_y);
      check_for_null(\$pitch_zone);
      my $pitch_num = @pitches + 1;
      # Edge case: counter sometimes (incorrectly) reverts (though pitch violations should not affect this), so skip those after the reversion
      @pitches = () if $pitch_num <= $pitch_num_last && !defined($$event{'details'}{'violation'});
      $pitch_num_last = $pitch_num;
      push @pitches, "('$season', '$game_type', '$game_id', '$play_id', '$pitch_num', $$event{'details'}{'type'}{'code'}, $$event{'pitchData'}{'startSpeed'}, '$val', '$result', '$balls', '$strikes', $pitch_zone, $pitch_x, $pitch_y)";
      # What is the (actual) count?
      $ab_count{'balls'}++ if $val eq 'B';
      $ab_count{'strikes'}++ if $val eq 'S';
      # Some corrections
      $ab_count{'balls'} = 3 if $ab_count{'balls'} > 3 && $$play{'result'}{'event'} !~ /Walk/i;
      $ab_count{'balls'} = 4 if $ab_count{'balls'} > 3;
      $ab_count{'strikes'} = 2 if $ab_count{'strikes'} > 2 && $$play{'result'}{'event'} !~ /Strikeout/i;
      $ab_count{'strikes'} = 3 if $ab_count{'strikes'} > 2;
      # Build
      my $event_count = "$ab_count{'balls'}-$ab_count{'strikes'}";
      if (($val ne 'X') && ($event_count ne $counts[-1])) {
        push @counts, $event_count;
        # Add to our set of counts
        #  0-0 = 1,     1-0 = 2,     2-0 = 4,      3-0 = 8,     4-0 = 16
        #  0-1 = 32,    1-1 = 64,    2-1 = 128,    3-1 = 256,   4-1 = 512
        #  0-2 = 1024,  1-2 = 2048,  2-2 = 4096,   3-2 = 8192,  4-2 = 16384
        #  0-3 = 32768, 1-3 = 65536, 2-3 = 131072, 3-3 = 262144
        #  => (2 ^ Balls) * (32 ^ Strikes)
        $counts_set += ((2 ** $ab_count{'balls'}) * (32 ** $ab_count{'strikes'}));
      }

      # Any hit info to capture?
      %hit_info = %{$$event{'hitData'}} if defined($$event{'hitData'});
    }
    $i++;
  }

  # Convert the pitch list into a JSON object (compressed later)
  $ab_pitches{'col'} = ', `pitches`';
  $ab_pitches{'val'} = ', \'' . encode_json(\@ab_pitches) . '\'';

  # Process hit info, if there was one!
  if (%hit_info) {
    my $hit_dist = defined($hit_info{'totalDistance'}) ? int($hit_info{'totalDistance'}) : undef; check_for_null(\$hit_dist);
    my $hit_speed = $hit_info{'launchSpeed'}; check_for_null(\$hit_speed);
    my $hit_angle = $hit_info{'launchAngle'}; check_for_null(\$hit_angle);
    my $hit_coordX = $hit_info{'coordinates'}{'coordX'}; check_for_null(\$hit_coordX);
    my $hit_coordY = $hit_info{'coordinates'}{'coordY'}; check_for_null(\$hit_coordY);
    # Convert where the ball was hit to to an equivalent fielder
    my $hit_fielder;
    if (defined($hit_info{'location'})) {
      switch ($hit_info{'location'}) {
        case 1  { $hit_fielder = 'P'; }
        case 2  { $hit_fielder = 'C'; }
        case 3  { $hit_fielder = '1B'; }
        case 4  { $hit_fielder = '2B'; }
        case 5  { $hit_fielder = '3B'; }
        case 6  { $hit_fielder = 'SS'; }
        case 7  { $hit_fielder = 'LF'; }
        case 78 { $hit_fielder = 'LF-CF'; }
        case 8  { $hit_fielder = 'CF'; }
        case 89 { $hit_fielder = 'CF-RF'; }
        case 9  { $hit_fielder = 'RF'; }
      }
    }
    check_for_null(\$hit_fielder);
    $ab_pitches{'val'} .= ", $hit_dist, $hit_fielder, $hit_coordX, $hit_coordY, $hit_speed, $hit_angle";
    $ab_pitches{'col'} .= ', `hit_distance`, `hit_fielder`, `hit_coordinateX`, `hit_coordinateY`, `hit_speed`, `hit_angle`';

    # Spray section
    my $hit_x = $hit_info{'coordinates'}{'coordX'};
    my $hit_y = $hit_info{'coordinates'}{'coordY'};
    $hit_spray = 'NULL';
    if (defined($hit_x) && defined($hit_y) && $hit_x > 0 && $hit_y > 0) {
      my $centre_x = 127.5;
      my $centre_y = 205;
      if ($hit_y > $centre_y) {
        $hit_spray = "'F'";
      } elsif ($hit_y != $centre_y) {
        my $is_left = ($hit_x < $centre_x);
        my $offset_x = abs($hit_x - $centre_x);
        my $offset_y = $centre_y - $hit_y;
        my $angle = rad2deg(atan($offset_x / $offset_y));
        if ($is_left) {
          $angle = 0 - $angle;
        }
        if ($angle > 45 || $angle < -45) {
          $hit_spray = "'F'";
        } elsif ($angle >= 27) {
          $hit_spray = "'R'";
        } elsif ($angle >= 9) {
          $hit_spray = "'C-R'";
        } elsif ($angle >= -9) {
          $hit_spray = "'C'";
        } elsif ($angle >= -27) {
          $hit_spray = "'L-C'";
        } else {
          $hit_spray = "'L'";
        }
      }
    }
  }

  # At Bat
  my $result = parse_atbat_result($$play{'result'}{'event'});
  my $ab_complete = parse_atbat_complete($$play{'result'}{'event'});
  print "INSERT INTO `SPORTS_MLB_GAME_ATBAT` (`season`, `game_type`, `game_id`, `play_id`, `batter_team_id`, `batter`, `pitcher_team_id`, `pitcher`, `balls`, `strikes`, `num_pitches`, `completed`, `result`$ab_pitches{'col'})
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$batter{'team_id'}', '$batter{'primaryNumber'}', '$pitcher{'team_id'}', '$pitcher{'primaryNumber'}', '$$play{'count'}{'balls'}', '$$play{'count'}{'strikes'}', '" . @pitches . "', '$ab_complete', '$result'$ab_pitches{'val'});\n";

  # Pitches
  $pitcher_pitches{$pitcher{'id'}} = 0 if !defined($pitcher_pitches{$pitcher{'id'}});
  $pitcher_pitches{$pitcher{'id'}} += @pitches;
  print "INSERT INTO `SPORTS_MLB_GAME_ATBAT_PITCHES` (`season`, `game_type`, `game_id`, `play_id`, `num`, `pitch`, `speed`, `call`, `result`, `balls`, `strikes`, `zone`, `pitch_x`, `pitch_y`)
  VALUES " . join(",\n         ", @pitches) . ";\n" if @pitches;

  # Flags (for splits)
  my $base_runners = ($ob{'1b'} ? 1 : 0) + ($ob{'2b'} ? 2 : 0) + ($ob{'3b'} ? 4 : 0);
  my $risp = int($ob{'2b'} || $ob{'3b'});
  my $batting_order = substr($$gamedata{'liveData'}{'boxscore'}{'teams'}{$half eq 'top' ? 'away' : 'home'}{'players'}{'ID'.$batter{'id'}}{'battingOrder'}, 0, 1);
  my $batter_pos = $field_pos{$batter{'id'}};
  my $is_bb = int($result =~ m/Walk/);
  my $is_ibb = int($result eq 'Intentional Walk');
  my $is_hbp = int($result eq 'Hit By Pitch');
  my $is_sac = int($result =~ m/^Sacrifice/);
  my $is_sac_fly = int($result =~ m/^Sacrifice Fly/);
  my $is_intf = int($result =~ m/Interference/);
  my $is_ab = int($ab_complete && !$is_bb && !$is_hbp && !$is_sac && !$is_intf);
  my $is_obp_evt = int($ab_complete && ($is_ab || $is_bb || $is_hbp || $is_sac_fly));
  # Total Bases
  my $tb = 0;
  if ( $result eq 'Single' ) {
    $tb = 1;
  } elsif ( $result eq 'Double' ) {
    $tb = 2;
  } elsif ( $result eq 'Triple' ) {
    $tb = 3;
  } elsif ( $result eq 'Home Run' ) {
    $tb = 4;
  }
  # Another check in case no pitches were processed
  $pitch_zone = 'NULL' if !defined($pitch_zone);
  $hit_spray = 'NULL' if !defined($hit_spray);
  # Can we backfill R, SB, CS, LOB from later plays??
  my $sp_rp = ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$half eq 'top' ? 'home' : 'away'}{'pitchers'}[0] eq $pitcher{'id'} ? 'SP' : 'RP');
  print "INSERT INTO `SPORTS_MLB_GAME_ATBAT_FLAGS` (`season`, `game_type`, `game_id`, `play_id`, `inning`, `on_base`, `risp`, `out`, `counts`, `count_final`, `batting_order`, `batter_pos`, `pitcher_pitches`, `ab_pitches`, `batter`, `batter_hand`, `pitcher`, `pitcher_hand`, `sp_rp`, `is_pa`, `is_ab`, `is_obp_evt`, `is_hit`, `is_bb`, `is_ibb`, `is_hbp`, `is_out`, `is_k`, `zone`, `spray`, `tb`, `rbi`)
  VALUES ('$season', '$game_type', '$game_id', '$play_id', '$$play{'about'}{'inning'}', '$base_runners', '$risp', '$out', '$counts_set', '$counts[-1]', '$batting_order', '$batter_pos', '$pitcher_pitches{$pitcher{'id'}}', '" . @pitches . "', \@player_$batter{'id'}, '$batter_hand', \@player_$pitcher{'id'}, '$pitcher_hand', '$sp_rp', '$ab_complete', '$is_ab', '$is_obp_evt', '" . int($tb > 0) . "', '$is_bb', '$is_ibb', '$is_hbp', '" . int($$play{'count'}{'outs'} != $out) . "', '" . int($result =~ m/Strikeout/) . "', $pitch_zone, $hit_spray, '$tb', '$$play{'result'}{'rbi'}');\n";

  # Runners/Outs info to carry over to the next AB
  $out = $$play{'count'}{'outs'};
  if (@{$$play{'runners'}}) {
    foreach my $runner (@{$$play{'runners'}}) {
      # From
      my $from = (defined($$runner{'movement'}{'start'}) ? lc($$runner{'movement'}{'start'}) : '');
      $ob{$from}-- if $from ne '';
      # To
      my $to = (defined($$runner{'movement'}{'end'}) ? lc($$runner{'movement'}{'end'}) : '');
      $ob{$to}++ if ($to ne '') && ($to ne 'score');
      # Overall numbers
      $ob{'tot'} = ($ob{'1b'} + $ob{'2b'} + $ob{'3b'});
    }
  }
}

# Perform some play description tidying
sub parse_descrip {
  my ($play) = @_;
  trim(\$play);
  $play =~ s/(\s){2,}/ /g;
  $play =~ s/([A-Z]\.)\s([A-Z]\.)/$1$2/g;
  return $play;
}

# Hack description name regex's (eugh...)
sub parse_descrip_hacks {
  my ($pbp_descrips) = @_;
  # Un-encode previously converted player to SQL swaps
  $$pbp_descrips =~ s/\{\{PLAYER_\&\#39;, \@player_(\d+), \&\#39;\}\}/{{PLAYER_', \@player_$1, '}}/g;
  # Correct some potential spacing errors
  $$pbp_descrips =~ s/\. ,/\.,/g;
  # Missing spaces between player name and Jr.
  $$pbp_descrips =~ s/(\S)(Jr\.)/$1 $2/g;
}

# Convert the play result
sub parse_atbat_result {
  my ($result) = @_;
  trim(\$result);
  $result = convert_text($result);
  my $result_lc = lc $result;

  # "Fan interference" => "Fan Interference" (Fussy case-change...)
  if ($result_lc eq 'fan interference') {
    return 'Fan Interference';
  # "Fielders Choice Out" => "Fielders Choice"
  } elsif ($result_lc eq 'fielders choice out') {
    return 'Fielders Choice';
  # "Grounded Into DP" => "Grounded Into Double Play"
  } elsif ($result_lc eq 'grounded into dp') {
    return 'Grounded Into Double Play';
  # "Intent Walk" => "Intentional Walk"
  } elsif ($result_lc eq 'intent walk') {
    return 'Intentional Walk';
  # "Sac Bunt" => "Sacrifice Bunt"
  } elsif ($result_lc eq 'sac bunt') {
    return 'Sacrifice Bunt';
  # "Sac Fly" => "Sacrifice Fly"
  } elsif ($result_lc eq 'sac fly') {
    return 'Sacrifice Fly';
  # "Field Out" => "Batter Interference"
  # "Batter Out" => "Batter Interference"
  } elsif ($result_lc eq 'field out'
    || $result_lc =~ /batter out/) {
    return 'Batter Interference';
  # "Caught Stealing XYZ" // "Pickoff XYZ" => "Runner Out"
  } elsif ($result_lc =~ /stolen base/ # Stole Base A, but then thrown at at B
    || $result_lc =~ /passed ball/ # Runner thrown out on passed ball
    || $result_lc =~ /caught stealing/
    || $result_lc =~ /pickoff/) {
    return 'Runner Out';
  # "Sac Fly DP" => "Sacrifice Fly (Double Play)"
  } elsif ($result_lc eq 'sac fly dp'
    || $result_lc eq 'sac fly double play') {
    return 'Sacrifice Fly (Double Play)';
  # "Sacrifice Bunt DP" => "Sacrifice Bunt (Double Play)"
  } elsif ($result_lc eq 'sacrifice bunt dp'
    || $result_lc eq 'sac bunt double play') {
    return 'Sacrifice Bunt (Double Play)';
  # "Strikeout - DP" => "Strikeout (Double Play)"
  } elsif ($result_lc eq 'strikeout - dp'
    || $result_lc eq 'strikeout double play') {
    return 'Strikeout (Double Play)';
  # "Runner Double Play" => "Runner Out (Double Play)"
  } elsif ($result_lc eq 'runner double play') {
    return 'Runner Out (Double Play)';
  # "Defensive Sub" => "Defensive Substitution"
  } elsif ($result_lc eq 'defensive sub') {
    return 'Defensive Substitution';
  }

  # The rest, as were passed in
  return $result;
}

# Convert the pitch code
sub parse_pitch_code {
  my ($code) = @_;

  # Ball similes
  if ($code =~ m/^(\*B|H|P|V[BCPS]?)$/) {
    return 'B';
  # Strike similes
  } elsif ($code =~ m/^(A?C|AB|M|W)$/) {
    return 'S';
  # Foul similes
  } elsif ($code =~ m/^[LORT]$/) {
    return 'F';
  # In Play similes
  } elsif ($code =~ m/^[DE]$/) {
    return 'X';
  }

  # The rest, as were passed in
  #  B, S, F, X
  return $code;
}

# Convert the pitch result
sub parse_pitch_result {
  my ($result) = @_;
  trim(\$result);
  $result = convert_text($result);
  my $result_lc = lc $result;

  # "In play, no out", "In play, out(s)", "In play, run(s)" => "In Play"
  if (substr($result_lc, 0, 7) eq 'in play') {
    return 'In Play';
  # "Intent Ball", "Automatic Ball - Intentional" => "Intentional Ball"
  } elsif ($result_lc =~ /^(intent ball|automatic ball - intentional)$/) {
    return 'Intentional Ball';
  # "Automatic Ball ..." => "Automatic Ball"
  } elsif ($result_lc =~ /^automatic ball.+/) {
    return 'Automatic Ball';
  # "Automatic Strike ..." => "Automatic Strike"
  } elsif ($result_lc =~ /^automatic strike.+/) {
    return 'Automatic Strike';
  # "Swinging Strike (Blocked)" => "Swinging Strike"
  } elsif ($result_lc eq 'swinging strike (blocked)') {
    return 'Swinging Strike';
  }

  # The rest, as were passed in
  #  Automatic Ball, Ball, Ball In Dirt, Called Strike, Foul, Foul Bunt, Foul (Runner Going), Foul Tip, Hit By Pitch, Missed Bunt, Pitchout, Swinging Pitchout, Swinging Strike
  return $result;
}

# Did an AB Play actually result in a completed at-bat?
sub parse_atbat_complete {
  my $result = parse_atbat_result(@_);
  return int($result ne 'Runner Out');
}

# Convert text name in the descrip to a codified version
sub parse_descrip_names {
  my ($src, $search, $rplc) = @_;
  $search =~ s/\s{2,}/ /g;
  $search =~ s/\./\\\./g;
  trim(\$search);
  # We need to be sensitive to any names featuring a . (such as Jr.), as sometimes we'll want to keep the . even though it's in the search pattern
  if ( substr($search, -1) ne '.' ) {
    $$src =~ s/\b$search\b/$rplc/g;
  } else {
    # Version 1: end of the descrip (keep the .)
    $$src =~ s/\b$search(#|$)/$rplc\.$1/g;
    # Version 2: mid-string, but end of sentence (keep the .)
    $$src =~ s/\b$search(\s+[A-Z])/$rplc\.$1/g;
    # Version 3: mid-string, but sentence continues (remove the .)
    $$src =~ s/\b$search(,?\s+[a-z])/$rplc$1/g;
  }
}

# Process actions that affect fielding info (fielding pos, etc)
sub descrip_set_fielding_pos {
  my ($descrip, $pitcher, $action) = @_;

  my $player; my $new_pos;
  # Pitcher switching throwing arms(?!?!)
  if ($descrip =~ m/is now pitching (left|right)-handed/) {
    my ($pitcher, $arm) = ($descrip =~ m/player_(\d+), '\}\} is now pitching (left|right)\-handed/);
    $arm = uc(substr($arm, 0, 1));
    print "# Internal Pitcher L/R Update: $pitcher now throwing $arm\n";
    $$gamedata{'gameData'}{'players'}{'ID'.$pitcher}{'pitchHand'}{'code'} = $$pitcher{'pitchHand'}{'code'} = $arm;

  # Pitching Change: NEW replaces OLD(, batting #(, replacing HITTER|pos)).
  } elsif ($descrip =~ m/Pitch(?:ing|er) Change/) {
    my ($player) = ($descrip =~ m/Pitch(?:ing|er) Change: \{\{PLAYER_', \@player_(\d+), '\}\} replaces/);
    update_field_pos($player, 'P')
      if defined($player);

  # Pitcher PITCHER enters the batting order, batting #, (pos )HITTER leaves the game.
  } elsif (($player) = ($descrip =~ m/Pitcher (.*?) enters the batting order/)) {
    ($player) = ($player =~ m/\@player_(\d+),/);
    update_field_pos($player, 'P')
      if defined($player);

  # Offensive Substitution: Pinch-(hitter|runner) NEW replaces OLD
  } elsif (($new_pos, $player) = ($descrip =~ m/Offensive Substitution: Pinch[- ](hitter|runner) .*?\@player_(\d+),/)) {
    $new_pos = 'P' . uc(substr($new_pos, 0, 1));
    update_field_pos($player, $new_pos)
      if defined($player);

  # (Defensive Substitution: )PLAYER remains in the game as the POS, batting #
  } elsif (($player, $new_pos) = ($descrip =~ m/^(?:Defensive Substitution:)?(.+) remains in the game as the ([A-Za-z +]+)/)) {
    ($player) = ($player =~ m/\@player_(\d+),/);
    $new_pos =~ s/baseman$/base/;
    $new_pos =~ s/fielder$/field/;
    update_field_pos($player, $config{'pos'}{$new_pos})
      if defined($player);

  # Defensive Substitution: NEW replaces (pos ) OLD, batting #, playing pos
  } elsif ($descrip =~ m/Defensive Substitution:/) {
    ($player, $new_pos) = ($descrip =~ m/\@player_(\d+).*? replaces .*? playing ([A-Za-z ]+)/);
    $new_pos =~ s/baseman$/base/;
    $new_pos =~ s/fielder$/field/;
    update_field_pos($player, $config{'pos'}{$new_pos})
      if defined($player);

  # Defensive switch from catcher to first base for PLAYER
  } elsif ($descrip =~ m/Defensive switch/) {
    ($new_pos, $player) = ($descrip =~ m/ to (.*?) for .*?\@player_(\d+),/);
    update_field_pos($player, $config{'pos'}{$new_pos})
      if defined($player);

  # Some generic fallback from the action block
  } elsif (defined($action) && defined($$action{'player'}{'id'})) {
    my $event = defined($$action{'event'}) ? $$action{'event'} : $$action{'details'}{'event'};
    $player = $$action{'player'}{'id'};
    if ($event =~ m/Pitching Substitution/) {
      update_field_pos($player, 'P');
    } elsif ($event =~ m/Offensive Substitution/) {
      update_field_pos($player, 'PH');
    } elsif ($event =~ m/Defensive Substitution/ && defined($$action{'position'}{'abbreviation'})) {
      update_field_pos($player, $$action{'position'}{'abbreviation'});
    }
  }
}

# Update our internal fielding position tracker
sub update_field_pos {
  my ($player, $pos) = @_;
  my %player = %{$$gamedata{'gameData'}{'players'}{"ID$player"}};
  # Determine the team via existance in team player lists
  my $is_home = defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{"ID$player"});
  $player{'team_id'} = $$game_info{($is_home ? 'home' : 'visitor') . '_id'};
  $player{'primaryNumber'} = $$gamedata{'liveData'}{'boxscore'}{'teams'}{$is_home ? 'home' : 'away'}{'players'}{'ID'.$player{'id'}}
    if !defined($player{'primaryNumber'});
  print "#   Internal Fielding Tracker Update: $player{'fullName'} ($player{'team_id'} #$player{'primaryNumber'}, ID: $player) now at $pos\n";
  $field_pos{$player} = $pos;
  # Is it work logging info per-pos (probably, an innings breakdown at least, if not stats breakdown per-pos)
}

# Convert X to Xst/nd/rd/th
sub display_ordinal {
  my ($num) = @_;
  my $num_clean = $num; $num_clean =~ s/[^\d\.]//g;
  my $end_digit = substr($num_clean, length($num_clean)-1);
  my $end_digits = substr($num_clean, length($num_clean)-2);
  if ($end_digit == 1 && $end_digits != 11) {
    return $num . 'st';
  } elsif ($end_digit == 2 && $end_digits != 12) {
    return $num . 'nd';
  } elsif ($end_digit == 3 && $end_digits != 13) {
    return $num . 'rd';
  }

  return $num . 'th';
}

# Return true to pacify the compiler
1;
