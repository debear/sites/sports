#!/usr/bin/perl -w
# Methods for fixing the game data

use Dir::Self;
use List::MoreUtils qw(uniq);

#
# Ad hoc (externally linked) changes
#
sub fix_ad_hoc {
  my $fixes = $config{'data_dir'} . "/fixes/$season.pl";
  if (-e $fixes) {
    print "# (Loading $season-specific fixes from $fixes)\n";
    require $fixes;
  }
}

#
# Check the composition of the player lists
#
sub fix_players {
  my %players = %{$$gamedata{'gameData'}{'players'}};

  # First, ensure the player lists are unique and that batters appear (somewhere) in the 'batters' list
  foreach my $team ('home', 'away') {
    foreach my $remote_id (keys %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}}) {
      my $id_num = substr($remote_id, 2);
      if (defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'battingOrder'}) && !grep {$_ eq $id_num} @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'batters'}}) {
        my %player = %{$players{"ID$id_num"}};
        print "# Player '$player{'useName'} $player{'lastName'}' (ID: $id_num) has batting order '$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'battingOrder'}', but wasn't included in the 'batters' list.\n";
        push @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'batters'}}, $id_num;
      }
    }
    foreach my $list ('batters','bench','pitchers','bullpen') {
      @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$list}} = uniq(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$list}});
    }
    my %sect = ( 'bench' => 'batters', 'bullpen' => 'pitchers' );
    foreach my $bench (keys %sect) {
      my $active = $sect{$bench};
      foreach my $id_num (@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$active}}) {
        my ($index) = grep { @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$bench}}[$_] eq $id_num } keys(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$bench}});
        next if !defined($index);
        my %player = %{$players{"ID$id_num"}};
        print "# Player '$player{'useName'} $player{'lastName'}' (ID: $id_num) in both '$bench' and '$active' lists for the '$team' team. Removing player from the '$bench' list at index '$index'.\n";
        splice(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$bench}}, $index, 1);
      }
    }
  }

  # Ensure players are attached to the correct team
  my %home = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}};
  my %visitor = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{'away'}};
  my @home_players = sort keys(%{$home{'players'}});
  my @visitor_players = sort keys(%{$visitor{'players'}});
  # Find the matches
  foreach my $id (@home_players) {
    if (grep {$_ eq $id} @visitor_players) {
      my $id_num = substr($id, 2);
      my %player = %{$players{$id}};
      # Determine which team he actually played for
      my $wrong_team; my %home_player = %{$home{'players'}{$id}};
      $wrong_team = defined($home_player{'parentTeamId'}) || defined($home_player{'battingOrder'}) ? 'away' : 'home';
      print "# Player '$player{'useName'} $player{'lastName'}' (ID: $id_num) determined as being on both teams. Removing player from the '$wrong_team' team list.\n";
      delete($$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{'players'}{$id});
      foreach my $list ('batters','bench','pitchers','bullpen') {
        my ($index) = grep { @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{$list}}[$_] eq $id_num } keys(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{$list}});
        next if !defined($index);
        print "#  -> Removing from '$list' list at index '$index'.\n";
        splice(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$wrong_team}{$list}}, $index, 1);
      }
    }
  }
}

#
# Correct issues surrounding player jerseys
#
sub fix_jerseys {
  if ($season < 2019) {
    fix_jerseys_historic();
  } else {
    # All players need jerseys
    fix_jerseys_set();
  }
}

#
# For historic seasons, copy the jersey from a previous run for consistency
#
sub fix_jerseys_historic {
  our $season; our $game_type; our $game_id;
  # Get the correct jerseys from the database
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT PI.remote_id, GR.jersey
             FROM SPORTS_MLB_GAME_ROSTERS AS GR
             JOIN SPORTS_MLB_PLAYERS_IMPORT AS PI
               USING (player_id)
             WHERE GR.season = ?
             AND   GR.game_type = ?
             AND   GR.game_id = ?;';
  my $jersey_map = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $game_type, $game_id);
  $dbh->disconnect if defined($dbh);
  # Phase 1: Update from database
  foreach my $map (@$jersey_map) {
    next if !defined($$gamedata{'gameData'}{'players'}{'ID'.$$map{'remote_id'}});
    $team = (defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{'ID'.$$map{'remote_id'}}) ? 'home' : 'away');
    $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{'ID'.$$map{'remote_id'}}{'jerseyNumber'}
      = $$gamedata{'gameData'}{'players'}{'ID'.$$map{'remote_id'}}{'primaryNumber'}
      = $$map{'jersey'};
    $$gamedata{'gameData'}{'players'}{'ID'.$$map{'remote_id'}}{'historical'} = 1;
  }
  # Phase 2: Remove those we didn't find an update for (who must be in the data incorrectly)
  foreach my $remote_id (keys %{$$gamedata{'gameData'}{'players'}}) {
    my %player = %{$$gamedata{'gameData'}{'players'}{$remote_id}};
    next if $player{'historical'};
    my $id_num = substr($remote_id, 2);
    print "# Player '$player{'useName'} $player{'lastName'}' (ID: $id_num) Not found in historical player list. Removing.\n";
    $team = (defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{$remote_id}) ? 'home' : 'away');
    delete $$gamedata{'gameData'}{'players'}{$remote_id};
    delete $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id};
    foreach my $list ('bench','bullpen') {
      my ($index) = grep { @{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$list}}[$_] eq $id_num } keys(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$list}});
      next if !defined($index);
      splice(@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{$list}}, $index, 1);
    }
  }
}

#
# Ensure all players have a jersey number
#
sub fix_jerseys_set {
  # Get the correct jerseys from the database
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT PI.remote_id, TR.jersey
             FROM SPORTS_MLB_TEAMS_ROSTERS AS TR
             JOIN SPORTS_MLB_PLAYERS_IMPORT AS PI
               USING (player_id)
             WHERE TR.season = ?
             AND   TR.the_date = ?
             AND   TR.team_id IN (?, ?);';
  my $jersey_map = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $$game_info{'game_date'}, $$game_info{'home_id'}, $$game_info{'visitor_id'});
  $dbh->disconnect if defined($dbh);
  my %jersey_map = ();
  foreach my $map (@$jersey_map) {
    $jersey_map{"ID$$map{'remote_id'}"} = $$map{'jersey'};
  }
  # Process
  my %undef_ids = ('home' => 99, 'away' => 99);
  foreach my $team (reverse sort keys %undef_ids) {
    my %existing = ( );
    foreach my $remote_id (sort keys %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}}) {
      my %player = %{$$gamedata{'gameData'}{'players'}{$remote_id}};
      my $id_num = substr($remote_id, 2);
      # Get the IDs from the rosters
      if (defined($jersey_map{$remote_id})
        && (!defined($$gamedata{'gameData'}{'players'}{$remote_id}{'primaryNumber'})
         || !defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'})
         || ($$gamedata{'gameData'}{'players'}{$remote_id}{'primaryNumber'} ne $jersey_map{$remote_id})
         || ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'} ne $jersey_map{$remote_id}))) {
        print "# Player '$player{'useName'} $player{'lastName'}' (ID: $id_num) Jersey switched from P'" . (defined($player{'primaryNumber'}) ? $player{'primaryNumber'} : 'undef') . "' // B'" . (defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'}) ? $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'} : 'undef') . "' to '$jersey_map{$remote_id}' from TEAMS_ROSTERS data.\n";
        $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'}
          = $$gamedata{'gameData'}{'players'}{$remote_id}{'primaryNumber'}
          = $jersey_map{$remote_id};
      }

      # Fix any that are still missing (hopefully just legacy / edge-cases??)
      if (!defined($$gamedata{'gameData'}{'players'}{$remote_id}{'primaryNumber'})
          || defined($existing{$$gamedata{'gameData'}{'players'}{$remote_id}{'primaryNumber'}})
          || !defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'})
          || ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'} !~ m/^[1-9]\d*$|^0$/)) {
        my $new = ++$undef_ids{$team};
        print "# Player '$player{'useName'} $player{'lastName'}' (ID: $id_num) Invalid jersey number found, set to '$new'.\n";
        $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}{$remote_id}{'jerseyNumber'}
          = $$gamedata{'gameData'}{'players'}{$remote_id}{'primaryNumber'}
          = "$new"; # Typecast as a string...
      }
      $existing{$$gamedata{'gameData'}{'players'}{$remote_id}{'primaryNumber'}} = 1;
    }
  }
}

#
# Check (and if applicable fix) if two players on opposite teams have the same name.
#  Also known as the "Chris Young workaround"...
#
sub fix_play_by_play_same_name {
  # Loop through, identifying players by name (as array key?), storing team/remote_id pairs ('home'=>1234,'away'=>5678)
  #  and use these to manually process the pbp descips before it gets to the "standard" update
  my %matches = ( );
  my @dupes = ( );

  foreach my $team ('away', 'home') {
    my %players = %{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'players'}};
    foreach my $remote_id (keys %players) {
      $remote_id = substr($remote_id, 2); # Strip leading 'ID'
      # Determine
      my %player = %{$players{"ID$remote_id"}};
      # Process
      if (!defined($matches{$player{'person'}{'fullName'}})) {
        # First time we've found this player
        %{$matches{$player{'person'}{'fullName'}}} = ( $team => $remote_id );
      } elsif (defined($matches{$player{'person'}{'fullName'}}{$team})) {
        # Existing match... within the same team!
        #  Note: Not sure this can happen, but flagging as an error, just in case...
        print STDERR "Identified two players on the same team with the same name. Name: '$player{'person'}{'fullName'}'; IDs: $matches{$player{'person'}{'fullName'}}{$team}, $remote_id. Aborting.\n";
        exit;
      } else {
        $matches{$player{'person'}{'fullName'}}{$team} = $remote_id;
        push @dupes, $player{'person'}{'fullName'};
      }
    }
  }

  # Process the dupes...
  if (@dupes) {
    print "# Duplicate player names found, manually parsing the play-by-play: '" . join("', '", @dupes) . "'.\n";
    foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
      # Which team is batting?
      # First the actions
      foreach my $action (@{$$play{'actionIndex'}}) {
        my $act = (defined($$play{'playEvents'}[$action]{'details'}) ? $$play{'playEvents'}[$action]{'details'} : $$play{'playEvents'}[$action]);
        foreach my $dupe (@dupes) {
          # Skip if duplicate of the main AB result
          next if $$act{'description'} eq $$play{'result'}{'description'};
          $$act{'description_raw'} = $$act{'description'}; # Preserve (useful for debugging)
          $$act{'description'} = fix_play_by_play_same_name__parse($$act{'description'}, $dupe, $matches{$dupe}, $$play{'playEvents'}[$action])
            if (index($$act{'description'}, $dupe) != -1);
        }
      }
      # Then check the play's result
      foreach my $dupe (@dupes) {
        $$play{'result'}{'description_raw'} = $$play{'result'}{'description'}; # Preserve (useful for debugging)
        $$play{'result'}{'description'} = fix_play_by_play_same_name__parse($$play{'result'}{'description'}, $dupe, $matches{$dupe}, $$play{'about'}{'halfInning'})
          if (index($$play{'result'}{'description'}, $dupe) != -1);
      }
    }
  }
}

sub fix_play_by_play_same_name__parse {
  my ($descrip, $name, $ids, $extra) = @_;
  my $team;
  $name = parse_name_simplified($name);

  # Defensive changes => pitching team (usually...)
  # Offensive changes => batting team
  if (ref($extra) eq 'HASH') {
    $team = defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{'home'}{'players'}{"ID$$extra{'player'}{'id'}"}) ? 'home' : 'away';

  # Plays are a bit more fiddly to determine...
  } elsif ((substr($descrip, 0, length($name)) eq $name)
       || (substr($descrip, 0, length("With $name")) eq "With $name")) {
    # Is batter
    $team = ($extra eq 'top' ? 'away' : 'home');
  } elsif ($descrip =~ m/\.\s+$name (?:to|scores|out)/) {
    # Is baserunner
    $team = ($extra eq 'top' ? 'away' : 'home');
  } else {
    # Is defense
    $team = ($extra eq 'top' ? 'home' : 'away');
  }

  my $rplc = "{{PLAYER_', \@player_$$ids{$team}, '}}";
  $descrip =~ s/$name/$rplc/g;
  return $descrip;
}

#
# Determine plays that should be skipped
#
sub fix_play_by_play_skip {
  my @plays = @{$$gamedata{'liveData'}{'plays'}{'allPlays'}};
  for (my $i = 0; $i < @plays; $i++) {
    my %play = %{$plays[$i]};
    if (!defined($play{'result'}{'description'})) {
      print "# Play $play{'atBatIndex'} missing description. Setting default.\n";
      $play{'result'}{'description'} = '';
    }
    # Assume okay
    my $play_skip = '';
    # Check for known conditions
    if (!defined($play{'matchup'}{'pitcher'}) && !defined($play{'matchup'}{'batter'}) && !@{$play{'pitchIndex'}}) {
      $play_skip = 'No matchup info';
    } elsif (!defined($play{'result'}{'event'}) && !@{$play{'pitchIndex'}}) {
      $play_skip = 'No pitches with no event description';
    } elsif (defined($play{'result'}{'event'}) && $play{'result'}{'event'} eq 'Game Advisory') {
      $play_skip = 'Skipping "Game Advisory" notes';
    } elsif ($i == $#plays && !$play{'about'}{'isComplete'}) {
      $play_skip = 'Is last play and play wasn\'t completed';
    }

    # Store
    $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'skip'} = ($play_skip ne '');
    if ($play_skip ne '') {
      print "# Skipping play '$play{'atBatIndex'}': '$play_skip'.\n";
      next;
    }

    ## And then do the same with actions
    foreach my $action (@{$play{'actionIndex'}}) {
      my %action = %{$play{'playEvents'}[$action]};
      # Sometimes the detail is in a sub-element
      my $has_details = defined($action{'details'});
      %action = %{$action{'details'}} if $has_details;
      # The description may be blank, and so a generic copy made
      my $fix_descrip = (!defined($action{'description'}) || !$action{'description'});
      $action{'description'} = $action{'event'} if $fix_descrip;
      # Determine if skipping and store
      my $action_skip = '';
      if ($action{'description'} eq $play{'result'}{'description'}) {
        $action_skip = 'Same description as play';
      } elsif ($action{'eventType'} eq 'game_advisory') {
        $action_skip = 'Skipping "Game Advisory" notes';
      }
      print "# Skipping action '$action' in play '$play{'atBatIndex'}': '$action_skip'.\n"
        if ($action_skip ne '');
      print "# Fixing missing description in action '$action' in play '$play{'atBatIndex'}'.\n"
        if ($action_skip eq '') && $fix_descrip;

      if (!$has_details) {
        $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'playEvents'}[$action]{'skip'} = ($action_skip ne '');
        $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'playEvents'}[$action]{'description'} = $action{'description'}
          if $fix_descrip;
      } else {
        $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'playEvents'}[$action]{'details'}{'skip'} = ($action_skip ne '');
        $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'playEvents'}[$action]{'details'}{'description'} = $action{'description'}
          if $fix_descrip;
      }
    }
  }
}

#
# Correct issues within the runner data
#
sub fix_runners {
  my $last; my %prev_base; my $i;
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    # First pass to remove some empty runners
    my @invalid = (); $i = -1;
    foreach my $runner (@{$$play{'runners'}}) {
      $i++;
      if (!defined($$runner{'movement'}{'start'}) && !defined($$runner{'movement'}{'end'}) && !defined($$runner{'movement'}{'isOut'})) {
        print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) Removing as blank movement found.\n";
        push @invalid, $i;
      }
    }
    if (@invalid) {
      # Run the splice in reverse (easier key management!)
      foreach my $inv (reverse @invalid) {
        my $rmv = splice(@{$$play{'runners'}}, $inv, 1);
      }
    }

    # Clean some inconsistencies
    foreach my $runner (@{$$play{'runners'}}) {
      # Hitter becomes a runner (usually out after a hit)
      my $is_pickoff = (defined($$runner{'details'}{'eventType'}) && substr($$runner{'details'}{'eventType'}, 0, 8) eq 'pickoff_');
      my $is_double_play = (defined($$runner{'details'}{'eventType'}) && $$runner{'details'}{'eventType'} eq 'double_play');
      if (!defined($$runner{'movement'}{'start'}) && !$is_pickoff && !$is_double_play) {
        if ($$runner{'details'}{'event'} eq 'Single' && $$runner{'movement'}{'end'} ne '1B') {
          print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) End forced to 1B.\n";
          $$runner{'movement'}{'end'} = '1B';
        } elsif ($$runner{'details'}{'event'} eq 'Double' && $$runner{'movement'}{'end'} ne '2B') {
          print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) End forced to 2B.\n";
          $$runner{'movement'}{'end'} = '2B';
        } elsif ($$runner{'details'}{'event'} eq 'Triple' && $$runner{'movement'}{'end'} ne '3B') {
          print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) End forced to 3B.\n";
          $$runner{'movement'}{'end'} = '3B';
        }
      }
      # Missing start point?
      if ($$runner{'movement'}{'isOut'}
          && defined($$runner{'movement'}{'start'})
          && defined($$runner{'movement'}{'outBase'})
          && $$runner{'movement'}{'start'} eq $$runner{'movement'}{'outBase'}
          && !$is_pickoff && !$is_double_play) {
        if ($$runner{'movement'}{'start'} eq '2B') {
          print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) Start forced to 1B.\n";
          $$runner{'movement'}{'start'} = '1B';
        } elsif ($$runner{'movement'}{'start'} eq '3B') {
          print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) Start forced to 2B.\n";
          $$runner{'movement'}{'start'} = '2B';
        }
      }
    }

    # Bump the hitter's start == undef row to the top of the list
    if (@{$$play{'runners'}} > 1) {
      my $hitter = undef;
      for (my $i = 0; !defined($hitter) && $i < @{$$play{'runners'}}; $i++) {
        $hitter = $i
          if !defined($$play{'runners'}[$i]{'movement'}{'start'});
      }
      # Move to the start if it's not already
      if (defined($hitter) && $hitter) {
        print "# Play '$$play{'atBatIndex'}' runners: ($$play{'runners'}[$hitter]{'details'}{'runner'}{'id'}) Hitting instance moved from '$hitter' to top.\n";
        unshift @{$$play{'runners'}}, splice(@{$$play{'runners'}}, $hitter, 1);
      }
    }

    # Ensure uniqueness
    my @duplicates = (); my %unique = (); my %out = (); $i = -1;
    foreach my $runner (@{$$play{'runners'}}) {
      $i++;
      # Ensure each running event is unique - a runner cannot feature multiple times with the same starting base
      #  Note: The expectation here is the runner was out, so keep the "isOut" record... whichever that is
      my $start = defined($$runner{'movement'}{'start'}) ? $$runner{'movement'}{'start'} : '0B';
      my $key = "$$runner{'details'}{'runner'}{'id'}//$start";
      if (defined($unique{$key})) {
        # Find the "isOut" row?
        if ($$runner{'movement'}{'isOut'}) {
          # Our earlier record is the one to cull
          print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) Second instance found of runner '$$runner{'details'}{'runner'}{'id'}' starting from '$start'. Removing '$unique{$key}'.\n";
          push @duplicates, $unique{$key};
          $unique{$key} = $i;
        }
      } else {
        $unique{$key} = $i;
      }
      # If a runner is out, he cannot feature in a later instance
      if (defined($out{$$runner{'details'}{'runner'}{'id'}})) {
        print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) Removing, as runner '$$runner{'details'}{'runner'}{'id'}' was previously flagged as being out (in action '$out{$$runner{'details'}{'runner'}{'id'}}').\n";
        push @duplicates, $i;
      } elsif ($$runner{'movement'}{'isOut'}) {
        $out{$$runner{'details'}{'runner'}{'id'}} = 1;
      }
    }
    # Any duplicates to remove?
    if (@duplicates) {
      # Run the splice in reverse (easier key management!)
      foreach my $dupe (reverse @duplicates) {
        my $rmv = splice(@{$$play{'runners'}}, $dupe, 1);
      }
    }

    # Now we have our list, we need to check end/start bases match between plays
    #  Note: The expectation here is that this only an error when the runner was thrown out,
    #        hence only checking "isOut" mis-matches in case the order matters...
    if (!defined($last) || $$play{'about'}{'inning'} != $$last{'about'}{'inning'} || $$play{'about'}{'halfInning'} ne $$last{'about'}{'halfInning'}) {
      %prev_base = ();
    }
    $i = -1;
    foreach my $runner (@{$$play{'runners'}}) {
      $i++;
      # Do we need a correction?
      if (defined($$runner{'movement'}{'start'})
          && defined($prev_base{$$runner{'details'}{'runner'}{'id'}})
          && $$runner{'movement'}{'start'} ne $prev_base{$$runner{'details'}{'runner'}{'id'}}) {
        print "# Play '$$play{'atBatIndex'}' runner '$i': ($$runner{'details'}{'runner'}{'id'}) Corrected from '$$runner{'movement'}{'start'}' to '$prev_base{$$runner{'details'}{'runner'}{'id'}}'.\n";
        $$runner{'movement'}{'start'} = $prev_base{$$runner{'details'}{'runner'}{'id'}};
      }
      # Preserve the ending for the next loop
      $prev_base{$$runner{'details'}{'runner'}{'id'}} = $$runner{'movement'}{'end'};
    }
    $last = $play;
  }
}

#
# Re-calc at-bat RBIs from the runners
#
sub fix_ab_rbis {
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    next if !$$play{'result'}{'rbi'};
    my $was = $$play{'result'}{'rbi'};
    $$play{'result'}{'rbi'} = 0;
    foreach my $runner (@{$$play{'runners'}}) {
      $$play{'result'}{'rbi'} += int($$runner{'details'}{'rbi'});
    }
    $$play{'result'}{'rbi'} = "$$play{'result'}{'rbi'}"; # Typecast...
    print "# Play '$$play{'atBatIndex'}' RBI count changed from '$was' to '$$play{'result'}{'rbi'}'.\n"
      if $$play{'result'}{'rbi'} ne $was;
  }
}

# Correct issues in the individual game info line items
sub fix_game_info {
  our %game_info_parsed;

  # Certain keys to skip...
  my @skip = ('umpires','venue','t','att','weather','wind');

  # Convert all incorrect , to ; (whilst also converting all chars to latin - this is purely belt-and-braces, rather than a requirement)
  foreach my $sect (keys %game_info_parsed) {
    foreach my $info (keys %{$game_info_parsed{$sect}}) {
      next if grep(/^$info$/, @skip);
      $game_info_parsed{$sect}{$info} = convert_latin1($game_info_parsed{$sect}{$info});
      $game_info_parsed{$sect}{$info} =~ s/([A-Za-z\) ]), ([A-Z]), ([A-Z])/$1; $2; $3/g; # Catch the instance where the middle char is needed for both regex's, e.g., "Drew, J, Pedroia." => "Drew; J; Pedroia."
      $game_info_parsed{$sect}{$info} =~ s/([A-Za-z\) ]), ([A-Z])/$1; $2/g;
      $game_info_parsed{$sect}{$info} =~ s/([^\(]\d+), ([A-Z])/$1; $2/g;
    }
  }

  # Then, loop through all the players and revert known instances where , is allowed
  foreach my $remote_id (keys %{$$gamedata{'gameData'}{'players'}}) {
    my $sub = $$gamedata{'gameData'}{'players'}{$remote_id}{'name'}{'boxname'};
    my $search = $sub; $search =~ s/, /; /;

    # Loop through each stat then, applying this name retro-fit
    foreach my $sect (keys %game_info_parsed) {
      foreach my $info (keys %{$game_info_parsed{$sect}}) {
        next if grep(/^$info$/, @skip);
        $game_info_parsed{$sect}{$info} =~ s/$search/$sub/g;
      }
    }
  }
}

# Fix an issue with names within the play-by-plays
sub fix_play_by_play_names {
  my ($search, $replace) = @_;
  foreach my $play (@{$$gamedata{'liveData'}{'plays'}{'allPlays'}}) {
    $$play{'result'}{'description'} =~ s/$search/$replace/g
      if defined($$play{'result'}{'description'});
    foreach my $action (@{$$play{'playEvents'}}) {
      $$action{'description'} =~ s/$search/$replace/g
        if defined($$action{'description'});
      $$action{'details'}{'description'} =~ s/$search/$replace/g
        if defined($$action{'details'}{'description'});
    }
  }
}

# Return true to pacify the compiler
1;
