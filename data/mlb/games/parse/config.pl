#!/usr/bin/perl -w
# Config for the game parsing scripts

@{$config{'scripts'}} = ( { 'script' => 'players',      'disp' => 1, 'label' => undef },          # Players
                          { 'script' => 'boxscore',     'disp' => 2, 'label' => undef },          # Boxscore
                          { 'script' => 'play_by_play', 'disp' => 3, 'label' => 'Play by Play' }, # Play by Play
                          { 'script' => 'summary',      'disp' => 4, 'label' => undef } );        # Game Summary
%{$config{'tables'}} = ( );
%{$config{'pos'}} = ( 'catcher' => 'C',
                      'first base' => '1B',
                      'second base' => '2B',
                      'third base' => '3B',
                      'shortstop' => 'SS',
                      'left field' => 'LF',
                      'center field' => 'CF',
                      'right field' => 'RF',
                      'pitcher' => 'P',
                      'designated hitter' => 'DH' );

# Games we'll force to be run using the XML data, in spite of the valid JSON object...
%{$config{'force-xml'}} = (
  '2011//regular//288028' => 1,
  '2012//regular//319490' => 1,
);

sub run_script {
  my ($script, $disp, $args) = @_;
  %{$args} = ( ) if !defined($args);
  $disp = ucfirst($script) if !defined($disp);
  $reset = (defined($$args{'reset'}) && $$args{'reset'});

  my $run = ($reset ? 'Resetting' : 'Running');
  print "##\n## $disp ($run)\n##\n";
  if (!$reset) {
    # Main run...
    my $method = "parse_$script";
    if (!$config{'debug'}) {
      $method->();
    } else {
      print "# Method: $method()\n";
    }
  } else {
    # Resetting a previous run
    if (!$config{'debug'}) {
      clear_previous($script);
    } else {
      print "# ** Resetting: clear_previous('$script') **\n";
    }
  }
  print "\n";
}

# Reset a previous run
sub clear_previous {
  my ($script) = @_;
  our $season; our $game_type; our $game_id;

  print "## Resetting a previous data load...\n";
  foreach my $table (@{$config{'tables'}{$script}}) {
    next if $table =~ /_SEASON_/; # Skip season based tables (we'll sort these later)
    print "DELETE FROM `$table` WHERE `season` = '$season' AND `game_type` = '$game_type' AND `game_id` = '$game_id';\n";
  }
}

# Order by game
sub reorder_tables {
  my ($script, $disp) = @_;
  $disp = ucfirst($script) if !defined($disp);

  print "## $disp\n";
  foreach my $table (@{$config{'tables'}{$script}}) {
    my $is_season = ($table =~ /_SEASON_/);
    my $type_col = ($is_season ? 'season' : 'game');
    my $extra_cols = (!$is_season ? ', `game_id`' : ', `player_id`, `team_id`');
    print "ALTER TABLE `$table` ORDER BY `season`, `${type_col}_type`$extra_cols;\n";
  }
  print "\n";
}

# Flag we're rebuilding the JSON using the XML data
sub flag_gamedata_rebuild {
  my ($msg) = @_;
  $config{'rebuild_gamedata'} = 1;
  print STDERR "#  $msg\n";
}

# Initial setup before we make a start
sub initial_setup {
  # Rebuild the core/legacy data
  if ($config{'v_api'} eq 'v1') {
    if (defined($config{'rebuild_gamedata'})) {
      fix_gamedata();
    } elsif ($season <= 2014) {
      fix_batter_pos();
    }
  }

  print "##\n## Data Fixes\n##\n";

  # Ad hoc changes
  fix_ad_hoc();
  fix_players();
  fix_jerseys();
  fix_play_by_play_same_name();

  # API v1 changes
  if ($config{'v_api'} eq 'v1') {
    fix_play_by_play_keys();

  # API v1.1 changes
  } else {
    fix_play_by_play_skip();
    fix_runners();
  }

  # Final, common updates
  fix_ab_rbis();
  parse_game_info();
  %xml = ( ); # Garbage collect...
  print "\n";
}

#
# Parse the game info
#
sub parse_game_info {
  our %game_info_parsed = ( '_all' => { }, 'home' => { }, 'away' => { } );
  our %game_info_split = ( '_all' => { }, 'home' => { }, 'away' => { } );
  # Combined (API v1.1)
  if (defined($$gamedata{'liveData'}{'boxscore'}{'info'})) {
    # Convert the passed array into a hash
    foreach my $row (@{$$gamedata{'liveData'}{'boxscore'}{'info'}}) {
      $game_info_parsed{'_all'}{lc($$row{'label'})} = $$row{'value'};
    }
  # Combined (API v1)
  } elsif (defined($$gamedata{'liveData'}{'boxscore'}{'gameInfoParsed'})) {
    # Convert the passed array into a hash
    foreach my $row (@{$$gamedata{'liveData'}{'boxscore'}{'gameInfoParsed'}{'gameNotes'}}) {
      $game_info_parsed{'_all'}{lc($$row{'label'})} = $$row{'value'};
    }
  } else {
    # Build the hash manually from the text...
    foreach my $note (split('<br/>', $$gamedata{'liveData'}{'boxscore'}{'gameInfo'})) {
      $note =~ s/\n[ \t]+//g;
      my ($label, $value) = ($note =~ m/^(?:<span[^>]*?>)?<b>(.+)<\/b>:\s+(.*?)(?:<\/span>)?$/);
      if (defined($value)) {
        trim(\$value);
        $game_info_parsed{'_all'}{lc($label)} = $value;
      }
    }
  }
  # Home / Visitor
  my @teams = ( 'home', 'away' );
  foreach my $team (@teams) {
    if (defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'}) && ref($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'}) eq 'ARRAY') {
      # Convert the passed array into a hash (API v1.1)
      foreach my $l (@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'}}) {
        my $sect = lc($$l{'title'});
        foreach my $row (@{$$l{'fieldList'}}) {
          $game_info_parsed{$team}{$sect.'-'.lc($$row{'label'})} = $$row{'value'};
        }
      }
    } elsif (defined($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'infoParsed'})) {
      # Convert the passed array into a hash (API v1.1)
      foreach my $l (@{$$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'infoParsed'}}) {
        my $sect = lc($$l{'title'});
        foreach my $row (@{$$l{'fields'}}) {
          $game_info_parsed{$team}{$sect.'-'.lc($$row{'label'})} = $$row{'value'};
        }
      }
    } else {
      # Build the hash manually from the text...
      my @components = ( );
      $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'} =~ s/\n[ \t]+//g;
      $$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'} =~ s/\s+(<br\/>)/$1/g;
      my ($bt, $b) = ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'} =~ m/<b>(BATTING)<\/b><br\/>((?:(?:<span[^>]*?>)?<b>.*?<\/b>: .*?<br\/>)+)<br\/>/);
      push @components, $bt, $b if defined($bt);
      my ($rt, $r) = ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'} =~ m/<b>(BASERUNNING)<\/b><br\/>((?:(?:<span[^>]*?>)?<b>.*?<\/b>: .*?<br\/>)+)<br\/>/);
      push @components, $rt, $r if defined($rt);
      my ($ft, $f) = ($$gamedata{'liveData'}{'boxscore'}{'teams'}{$team}{'info'} =~ m/<b>(FIELDING)<\/b><br\/>((?:(?:<span[^>]*?>)?<b>.*?<\/b>: .*?<br\/>)+)<br\/>/);
      push @components, $ft, $f if defined($ft);
      # Loop through the sections
      while (my ($title, $info) = splice(@components, 0, 2)) {
        my $sect = lc($title);
        foreach my $note (split('<br/>', $info)) {
          my ($label, $value) = ($note =~ m/^(?:<span[^>]*?>)?<b>(.+)<\/b>:\s+(.*?)(?:<\/span>)?$/);
          if (defined($value)) {
            trim(\$value);
            $game_info_parsed{$team}{$sect.'-'.lc($label)} = $value;
          }
        }
      }
    }
  }

  # Having determined the components, one final check: is the player separator , (bad) or ; (good)? Need to fix if former...
  fix_game_info()
    if defined($game_info_parsed{'_all'}{'pitches-strikes'})
      # We can guarantee there should be at two players listed here (if defined) as both teams had a pitcher!
      && $game_info_parsed{'_all'}{'pitches-strikes'} !~ m/;/;
}

# Load the XML version of the game files
our %xml;
sub load_xml {
  my ($arg, $args) = @_;
  return if $xml{$arg};
  $xml{$arg} = load_file("$config{'game_dir'}/$arg.xml.gz", $args);
}

# Convert undefined values to 0
sub convert_undef_zero {
  my ($txt) = @_;
  return (defined($txt) ? $txt : 0);
}

# Parse a name, simplifying non-ASCII chars to their ASCII equivalent (e.g., &oacute; => o)
sub parse_name_simplified {
  my ($name) = @_;
  # UTF-8 conversions?
  $name = Encode::decode('UTF-8', $name)
    if utf8::is_utf8($name);
  # Convert to HTML
  $name = encode_entities($name);
  # Strip the funny HTML parts
  $name =~ s/\&([a-z])[^;]+;/$1/gi;
  return $name;
}

sub parse_winprob {
  my ($wp_file) = @_;

  # Get the probabilities out
  my $raw = load_file($wp_file, {'no_exit_on_error' => 1});
  return
    if !defined($raw);
  my %winprob = ();
  foreach my $w (@$raw) {
    $winprob{$$w{'atBatIndex'}} = {
      'home' => sprintf('%0.01f', $$w{'homeTeamWinProbability'}),
      'home_change' => defined($$w{'homeTeamWinProbabilityAdded'}) ? sprintf('%0.01f', $$w{'homeTeamWinProbabilityAdded'}) : undef,
      'visitor' => sprintf('%0.01f', $$w{'awayTeamWinProbability'}),
      'visitor_change' => defined($$w{'homeTeamWinProbabilityAdded'}) ? sprintf('%0.01f', 0 - $$w{'homeTeamWinProbabilityAdded'}) : undef,
    };
  }

  # Then incorporate into the plays
  my $num_plays = @{$$gamedata{'liveData'}{'plays'}{'allPlays'}};
  for (my $i = 0; $i < $num_plays; $i++) {
    my $play = $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]; # To simplify next line only, not update...
    $$gamedata{'liveData'}{'plays'}{'allPlays'}[$i]{'winprob'} = $winprob{$$play{'about'}{'atBatIndex'}}
      if defined($$play{'about'}{'atBatIndex'});
  }
}

# Return true to pacify the compiler
1;
