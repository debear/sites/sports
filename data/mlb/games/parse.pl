#!/usr/bin/perl -w
# Parse game data for a single game from MLB.com
use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the game parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
benchmark_stamp('Initial Config Loaded');

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;
benchmark_stamp('Secondary Config Loaded');
$config = $config{'script_dir'} . '/parse/config.pl';
require $config;
benchmark_stamp('Parse Config Loaded');

$config{'debug'} = 0; # Display script to run, rather than actually running it...
$config{'disp'} = -1; # Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp_all'} = ($config{'disp'} == -1); # Can we over-ride (from args) which sections we process?
$config{'dump_data'} = grep(/^--dump-data$/, @ARGV);
$config{'game_info'} = grep(/^--game-info$/, @ARGV);

# Define rest of vars from arguments
our ($season, $game_type, $game_id) = @ARGV;
our $game_info = get_game_info(@ARGV);
our $game_ref = "$season//$game_type//$$game_info{'remote_id'}";
$config{'v_api'} = 'v1.1';
$config{'game_dir'} = sprintf('%s/_data/%s/games/%s/%s', $config{'base_dir'}, $season, $game_type, $$game_info{'remote_id'});
benchmark_stamp('Args Parsed');

# Load the API-specific fixes file
$config = $config{'script_dir'} . '/parse/' . $config{'v_api'} . '/fixes.pl';
require $config;
benchmark_stamp('Parse Fixes Loaded');

# Some season-specific rule calcs
# - 7 inning doubleheaders
$config{'regulation_innings'} = ((($game_ref =~ /^2020\/\/regular\/\// && $$game_info{'game_date'} ge '2020-08-01') || ($game_ref =~ /^2021\/\/regular\/\//)) && $$game_info{'is_doubleheader'} ? 7 : 9);
# - Extra innings start with runner on 2nd base (regular season only, from 2020 onwards)
$config{'ob_2b_extras'} = ($season >= 2020 && $game_type eq 'regular');

# Load our component files
foreach my $s ( @{$config{'scripts'}} ) {
  my $r = abs_path(__DIR__ . '/parse/' . $config{'v_api'} . '/' . $$s{'script'} . '.pl');
  require $r;
  benchmark_stamp('Loaded \'' . $$s{'script'} . '\' Module');
  # Editable on CLI?
  my $script_arg = $$s{'script'}; $script_arg =~ s/_/-/g;
  if ($config{'disp_all'} && grep(/^--$script_arg$/, @ARGV)) {
    $config{'disp'} = [ ] if $config{'disp'} == -1;
    $config{'disp'} = [ $config{'disp'} ] if ref($config{'disp'}) ne 'ARRAY';
    push @{$config{'disp'}}, $$s{'disp'};
  }
}

print "##\n##\n## Loading $season " . ucfirst($game_type) . " Game $game_id: $$game_info{'visitor_id'} \@ $$game_info{'home_id'} (Remote: $$game_info{'remote_id'})\n##\n##\n\n";
print STDERR "# Loading Game: $season " . ucfirst($game_type) . ", Game $game_id" . ($game_type eq 'playoff' ? ' (' . playoff_summary($game_id) . ')' : '') . ": $$game_info{'visitor_id'} \@ $$game_info{'home_id'} (Remote: $$game_info{'remote_id'})\n";
benchmark_stamp('Args Parsed');

# We do not parse games that were not completed (including Suspended), so flag those types of games
if (defined($$game_info{'status'}) && $$game_info{'status'} ne 'F') {
  print STDERR "This game was not completed ('$$game_info{'status'}'), so will not be processed.\n";
  exit 20;
}
benchmark_stamp('Game Validation');

# Verify the gamedata to see how to proceed
my $gd_file = "$config{'game_dir'}/gamedata.json.gz";
my $wp_file = "$config{'game_dir'}/winprob.json.gz";
my $json_rebase = 0;
if ($config{'v_api'} eq 'v1') {
  if (defined($config{'force-xml'}{$game_ref})) {
    # We've been asked to rebuild for this specific game
    flag_gamedata_rebuild('Flag set to indicate the game data will be re-built from XML, even though we have a syntactically valid JSON version.');
  } elsif ($season < 2011) {
    # Legacy: too many issues with the (syntactically valid) JSON, so just go with the XML version
    flag_gamedata_rebuild('Forcing re-build of game data from XML for games pre-2011.');
  } else {
    # Validate the file to see if (and if so how) we need to rebuild from XML
    my @stat = stat $gd_file;
    if (!defined($stat[7]) || $stat[7] < 200) {
      flag_gamedata_rebuild('Missing JSON game data, will be re-built from XML in to a generic template.');
      $json_rebase = 1;
    } elsif ($stat[7] < 4000) {
      flag_gamedata_rebuild('No JSON play-by-play available, JSON will be fully re-built from XML.');
    }
  }
}
benchmark_stamp('File/JSON Validation');
# Load the gamedata that we'll share across our component methods
our $gamedata;
if ($json_rebase) {
  $gamedata = fix_gamedata__base_json();
  benchmark_stamp('Game Data Built');
} else {
  $gamedata = load_file($gd_file);
  benchmark_stamp('Game Data Loaded');
}
benchmark_stamp('Gamedata Loaded');
parse_winprob($wp_file);
benchmark_stamp('Win Probability Parsed');
initial_setup();
benchmark_stamp('Initial Setup: Complete');

# Loop one: clear a previous run...
foreach my $script (@{$config{'scripts'}}) {
  next if !check_disp($$script{'disp'});
  run_script($$script{'script'}, $$script{'label'}, { 'reset' => 1 });
  benchmark_stamp('Running \'' . $$script{'script'} . '\' Reset');
}

# Loop two: run!
foreach my $script (@{$config{'scripts'}}) {
  next if !check_disp($$script{'disp'});
  run_script($$script{'script'}, $$script{'label'});
  benchmark_stamp('Running \'' . $$script{'script'} . '\' Script');
}

print "##\n## $season " . ucfirst($game_type) . " Game $game_id Done\n##\n\n";

# Debug JSON?
if ($config{'dump_data'}) {
  use Data::Dumper;
  delete $$gamedata{'copyright'};
  delete $$gamedata{'metaData'};
  print STDERR Dumper($gamedata);
  benchmark_stamp('JSON Dumped');
}

# Debug game info?
if ($config{'game_info'}) {
  use Data::Dumper;
  our %game_info_split; our %game_info_parsed;
  print STDERR Dumper(\%game_info_parsed);
  print STDERR Dumper(\%game_info_split);
  benchmark_stamp('Game Info Dumped');
}
