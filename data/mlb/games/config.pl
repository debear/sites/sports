#!/usr/bin/perl -w
# Perl config file
use Encode;
use JSON;

# Validate and load a file
sub load_file {
  my ($file, $args) = @_;

  # Validate
  if (! -e $file) {
    print STDERR "File '$file' does not exist, ";
    if (!$$args{'no_exit_on_error'}) {
      print STDERR "unable to continue.\n";
      exit 20;
    } else {
      print STDERR "skipping.\n";
      return ($file, undef);
    }
  }

  # Load the file
  open(FILE, "gzip -dc $file |");
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( );

  # Decode the file in to an object (rather than string)
  if (!defined($$args{'no-decode'})) {
    # Extension implies method of decoding
    if ($file =~ m/\.json/) {
      $contents = decode_json(Encode::encode('UTF-8', $contents));
    } elsif ($file =~ m/\.xml/) {
      if (defined($$args{'decode-libxml'}) && $$args{'decode-libxml'}) {
        our $parser_libxml;
        if (!defined($parser_libxml)) {
          use XML::LibXML; # JIT module loading
          $parser_libxml = new XML::LibXML ; # Call once, use many...
        }
        $contents = $parser_libxml->load_xml(string => $contents);
      } else {
        our $parser_simple;
        if (!defined($parser_simple)) {
          use XML::Simple; # JIT module loading
          $parser_simple = new XML::Simple; # Call once, use many...
        }
        $contents = eval { $parser_simple->XMLin(Encode::encode('UTF-8', $contents), KeyAttr => [ ]) }; # KeyAttr unsets the default
      }
    }
  }

  return ($file, $contents);
}

# Return true to pacify the compiler
1;
