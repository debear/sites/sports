#!/usr/bin/perl -w
# Actions to be run in bulk, after a game import and processing run has completed

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

$config{'disp'} = 0; # Section to display: -1 = All, 0 = None, 1-X = appropriate section;

# ALTER .. ORDER BY tables?
if (check_disp(1, $config{'disp'})) {
  $config = $config{'script_dir'} . '/parse/config.pl';
  require $config;

  # Load our component files
  foreach my $s ( @{$config{'scripts'}} ) {
    my $r = abs_path(__DIR__ . '/parse/' . $$s{'script'} . '.pl');
    require $r;
  }

  # Run!
  print "##\n## Table Maintenance\n##\n";
  foreach my $script (@{$config{'scripts'}}) {
    reorder_tables($$script{'script'}, $$script{'label'});
  }
  print "##\n## Table Maintenance Complete\n##\n\n";
}
