#!/usr/bin/perl -w
# Get game data for a single game from MLB.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get game script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
our ($season, $game_type, $game_id) = @ARGV;
my $game_info = get_game_info(@ARGV);
my $dir = sprintf('%s/_data/%s/games/%s/%s', $config{'base_dir'}, $season, $game_type, $$game_info{'remote_id'});

# Load local config (intentionally after the above arg calling)
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Run...
print '# Downloading ' . ucfirst($game_type) . " Game $game_id" . ($game_type eq 'playoff' ? ' (' . playoff_summary($game_id) . ')' : '') . ": $$game_info{'visitor_id'} @ $$game_info{'home_id'} (Remote: $$game_info{'remote_id'}): ";

# Skip if already done...
if (-e "$dir/gamedata.json.gz") {
  print "[ Skipped ]\n";
  exit;
}

# Make sure the game dir exists
#  However, for games that are being completed we need to use the date the game was *started*, not the date it was completed
make_path($dir);
my @date = split('-', defined($$game_info{'started_date'}) ? $$game_info{'started_date'} : $$game_info{'game_date'});

# API version determination
# 2019 onwards uses v1.1
my $v_api = 'v1.1';
# 2018 and earlier uses v1
if ($season <= 2018) {
  $v_api = 'v1';
}

# Main game data
download_file("https://statsapi.mlb.com/api/$v_api/game/$$game_info{'remote_id'}/feed/live\?language=en", 'gamedata', 'json');
# Win Probability (currently always, and only, v1 of the API)
download_file("https://statsapi.mlb.com/api/v1/game/$$game_info{'remote_id'}/winProbability", 'winprob', 'json');
# For games upto (and including) 2014, we sometimes need to use data from their XML, not JSON, sources
if (defined($$game_info{'gameday_link'})) {
  download_file("https://gd.mlb.com/components/game/mlb/year_$date[0]/month_$date[1]/day_$date[2]/gid_$$game_info{'gameday_link'}/boxscore.xml", 'boxscore', 'xml');
  download_file("https://gd.mlb.com/components/game/mlb/year_$date[0]/month_$date[1]/day_$date[2]/gid_$$game_info{'gameday_link'}/players.xml", 'players', 'xml');
  download_file("https://gd.mlb.com/components/game/mlb/year_$date[0]/month_$date[1]/day_$date[2]/gid_$$game_info{'gameday_link'}/inning/inning_all.xml", 'play_by_play', 'xml');
}

# Writeups
# Removed as of Mar-2017. TD.
## - Main list
#download_file("https://gd.mlb.com/components/game/mlb/year_$date[0]/month_$date[1]/day_$date[2]/gid_$$game_info{'gameday_link'}/gamecenter.xml", 'writeups', 'xml');
## - Individual articles
#my $writeups = load_file("$dir/writeups.xml.gz");
#foreach my $game_type ('preview','recap') {
#  my $article_id = (defined($$writeups{$game_type.'s'}{'mlb'}) ? $$writeups{$game_type.'s'}{'mlb'}{'url'}{'cid'} : $$writeups{$game_type.'s'}{'home'}{'url'}{'cid'});
#  next if !defined($article_id);
#  download_file("https://m.mlb.com/news/article/$article_id\?game_pk=$$game_info{'remote_id'}", $game_type, 'htm');
#}

print "[ Done ]\n";

# Perform a download
sub download_file {
  my ($url, $type, $ext) = @_;
  my $local = "$dir/$type.$ext.gz";
  download($url, $local, {'skip-today' => 1, 'gzip' => 1});
}
