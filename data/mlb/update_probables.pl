#!/usr/bin/perl -w
# Download, import and process the probable pitchers from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'probables';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my $date = time2date(time());
my $season = substr($date, 0, 4);

## Inform user
print "\nAcquiring data from $season for date '$date'\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Download, if we haven't done so already (handled in-script)
#
print '=> Download: ';
command("$config{'base_dir'}/probables/download.pl $season $date",
        "$config{'log_dir'}/$logs{'download'}.log",
        "$config{'log_dir'}/$logs{'download'}.err");
done(0);

# If only downloading, go no further
end_script() if download_only();

#
# Parse
#
print '=> Parse: ';
command("$config{'base_dir'}/probables/parse.pl $season $date",
        "$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'parse'}.err");
done(3);

#
# Import into the database
#
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'import'}.log",
        "$config{'log_dir'}/$logs{'import'}.err");
done(2);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'parse'    => '02_parse',
    'import'   => '03_import',
  );
}
