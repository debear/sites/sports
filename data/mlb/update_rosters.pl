#!/usr/bin/perl -w
# Download and import team player info (though not specifics of the individual players) from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'rosters';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my $date = time2date(time());
my $season = substr($date, 0, 4);

# Validate the date - if it's before the season start, we want initial mode
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sth = $dbh->prepare('SELECT MIN(game_date) AS first_game
FROM SPORTS_MLB_SCHEDULE
WHERE season = ?;');
$sth->execute($season);
my ($first_game) = $sth->fetchrow_array;
$date = 'initial' if $date le $first_game; # <= because we're running a day behind, so on first_game we're actually processing the day before

undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);

# Inform user
print "\nAcquiring data from $season\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Download, if we haven't done so already (handled in-script)
#
print '=> Download: ';
command("$config{'base_dir'}/rosters/download.pl $season $date",
        "$config{'log_dir'}/$logs{'roster_d'}.log",
        "$config{'log_dir'}/$logs{'roster_d'}.err");
done(4);

# If only downloading, go no further
end_script() if download_only();

#
# Rosters
#
print '=> Rosters: ' . "\n";
print '  - Parse: ';
command("$config{'base_dir'}/rosters/parse-roster.pl $season $date",
        "$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_p'}.err");
done(1); print '  - Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_i'}.log",
        "$config{'log_dir'}/$logs{'roster_i'}.err");
done(0);

#
# Depth Chart
#
print '=> Depth Charts: ' . "\n";
print '  - Parse: ';
command("$config{'base_dir'}/rosters/parse-depth.pl $season $date",
        "$config{'log_dir'}/$logs{'depth_p'}.sql",
        "$config{'log_dir'}/$logs{'depth_p'}.err");
done(1); print '  - Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'depth_p'}.sql",
        "$config{'log_dir'}/$logs{'depth_i'}.log",
        "$config{'log_dir'}/$logs{'depth_i'}.err");
done(0);

#
# Check for new players
#
print '=> New Players:' . "\n";
print '  - Parse:  ';
command("$config{'base_dir'}/players/new.pl",
        "$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_p'}.err");
print "[ Done ]\n  - Import: ";
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_i'}.log",
        "$config{'log_dir'}/$logs{'players_i'}.err");
print "[ Done ]\n";

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'roster_d'  => '01_roster_download',
    'roster_p'  => '02_roster_parse',
    'roster_i'  => '03_roster_import',
    'depth_p'   => '04_depth_parse',
    'depth_i'   => '05_depth_import',
    'players_p' => '06_players_process',
    'players_i' => '07_players_import',
  );
}
