#!/usr/bin/perl -w
# Perl config file with all DB, dir info, etc

use MIME::Base64;
use HTML::Entities;
use Encode;
use DBI;

our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

$config{'sport'} = 'mlb';
$config{'base_dir'} .= '/' . $config{'sport'}; # Append the MLB dir
$config{'data_dir'} = $config{'base_dir'} . '/_data';
$config{'cdn_dir'} .= '/' . $config{'sport'}; # Append the MLB dir
$config{'timezone'} = 'US/Eastern';
# Start processing lineups 2 hours before first-pitch (to allow time for information to be posted)
$config{'lineups_time_to_start'} = 120;
# The number of games in our recent game calcs
$config{'num_recent'} = 10;
# Numeric ID mapping
$config{'team_num'} = {
  'LAA' => 108, # Los Angeles Angels
  'ARI' => 109, # Arizona Diamondbacks
  'BAL' => 110, # Baltimore Orioles
  'BOS' => 111, # Boston Red Sox
  'CHC' => 112, # Chicago Cubs
  'CIN' => 113, # Cincinnati Reds
  'CLE' => 114, # Cleveland Indians / Guardians
  'COL' => 115, # Colorado Rockies
  'DET' => 116, # Detroit Tigers
  'HOU' => 117, # Houston Astros
  'KC'  => 118, # Kansas City Royals
  'LAD' => 119, # Los Angeles Dodgers
  'WSH' => 120, # Washington Nationals
  'NYM' => 121, # New York Mets
  'SAC' => 133, # (Sacremento) Athletics
  'OAK' => 133, # Oakland Athletics
  'PIT' => 134, # Pittsburgh Pirates
  'SD'  => 135, # San Diego Padres
  'SEA' => 136, # Seattle Mariners
  'SF'  => 137, # San Francisco Giants
  'STL' => 138, # St. Louis Cardinals
  'TB'  => 139, # Tampa Bay Rays
  'TEX' => 140, # Texas Rangers
  'TOR' => 141, # Toronto Blue Jays
  'MIN' => 142, # Minnesota Twins
  'PHI' => 143, # Philadelphia Phillies
  'ATL' => 144, # Atlanta Braves
  'CWS' => 145, # Chicago White Sox
  'MIA' => 146, # Florida Marlins
  'NYY' => 147, # New York Yankees
  'MIL' => 158, # Milwaukee Brewers
};

# Check as to whether we parse/import the data or just download it
sub download_only {
  return defined($config{'download_only'}) && $config{'download_only'};
}

# Get info for a given game
sub get_game_info {
  my @db_args = @_;

  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT SPORTS_MLB_SCHEDULE.game_id,
                    SPORTS_MLB_SCHEDULE.mlb_id AS remote_id,
                    SPORTS_MLB_SCHEDULE.gameday_link,
                    SPORTS_MLB_SCHEDULE.game_date,
                    SPORTS_MLB_SCHEDULE.started_date,
                    SPORTS_MLB_SCHEDULE.game_time_ref,
                    VISITOR.franchise AS visitor,
                    VISITOR.city AS visitor_city,
                    SPORTS_MLB_SCHEDULE.visitor AS visitor_id,
                    HOME.franchise AS home,
                    HOME.city AS home_city,
                    SPORTS_MLB_SCHEDULE.home AS home_id,
                    SPORTS_MLB_SCHEDULE.status,
                    ALT_GAME.game_id IS NOT NULL AS is_doubleheader
             FROM SPORTS_MLB_SCHEDULE
             JOIN SPORTS_MLB_TEAMS AS VISITOR
               ON (VISITOR.team_id = SPORTS_MLB_SCHEDULE.visitor)
             JOIN SPORTS_MLB_TEAMS AS HOME
               ON (HOME.team_id = SPORTS_MLB_SCHEDULE.home)
             LEFT JOIN SPORTS_MLB_SCHEDULE AS ALT_GAME
               ON (ALT_GAME.season = SPORTS_MLB_SCHEDULE.season
               AND ALT_GAME.game_type = SPORTS_MLB_SCHEDULE.game_type
               AND ALT_GAME.game_date = SPORTS_MLB_SCHEDULE.game_date
               AND ALT_GAME.home IN (SPORTS_MLB_SCHEDULE.home, SPORTS_MLB_SCHEDULE.visitor)
               AND ALT_GAME.visitor IN (SPORTS_MLB_SCHEDULE.home, SPORTS_MLB_SCHEDULE.visitor)
               AND ALT_GAME.game_id <> SPORTS_MLB_SCHEDULE.game_id
               AND IFNULL(ALT_GAME.status, "") NOT IN ("PPD", "CNC", "SSP"))
             WHERE SPORTS_MLB_SCHEDULE.season = ?
             AND   SPORTS_MLB_SCHEDULE.game_type = ?
             AND   SPORTS_MLB_SCHEDULE.game_id = ?
             GROUP BY SPORTS_MLB_SCHEDULE.season, SPORTS_MLB_SCHEDULE.game_type, SPORTS_MLB_SCHEDULE.game_id;';
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, @db_args[0..2]);
  $dbh->disconnect if defined($dbh);

  # Reg-ex versions
  $$ret[0]{'visitor_city_regex'} = convert_team_city_regex($$ret[0]{'visitor_id'}, $$ret[0]{'visitor_city'});
  $$ret[0]{'visitor_regex'} = convert_team_franchise_regex($$ret[0]{'visitor_id'}, $$ret[0]{'visitor'});
  $$ret[0]{'home_city_regex'} = convert_team_city_regex($$ret[0]{'home_id'}, $$ret[0]{'home_city'});
  $$ret[0]{'home_regex'} = convert_team_franchise_regex($$ret[0]{'home_id'}, $$ret[0]{'home'});

  return $$ret[0];
}

sub convert_team_city_regex {
  my ($team_id, $city) = @_;

  # The exceptions
  return 'St\.? Louis' if $team_id eq 'STL';

  # The standard
  return $city;
}

sub convert_team_franchise_regex {
  my ($team_id, $franchise) = @_;

  # The exceptions
  return "($franchise|D\-?Backs)" if $team_id eq 'ARI';

  # The standard
  return $franchise;
}

#
# Convert an ID to a summary of the playoff series
#
sub playoff_summary {
  my $game_id = sprintf('%03d', $_[0]);
  my $series;

  if ($game_id =~ /^4/) {
    $series = 'WS';
  } else {
    $series = 'AL' if $game_id =~ /^11/ || $game_id =~ /^2[12]/ || $game_id =~ /^31/;
    $series = 'NL' if !defined($series);
    $series .= 'WC' if $game_id =~ /^1/;
    $series .= 'DS' if $game_id =~ /^2/;
    $series .= 'CS' if $game_id =~ /^3/;
  }

  return $series . ' Game ' . substr($game_id, -1);
}

# Convert team_ids
sub convert_team_id {
  my ($team_id) = @_;
  my %team_ids = ( 'FLA' => 'MIA', 'AZ' => 'ARI', 'ATH' => 'SAC' );

  # If there is a mapping, use that, otherwise use what we were given
  if (defined($team_ids{$team_id})) {
    return $team_ids{$team_id};
  } else {
    return $team_id;
  }
}

sub convert_team_id_from_code {
  my ($team_id) = @_;
  return $config{'team_num'}{$team_id};
}

sub convert_team_id_from_num {
  our $season;
  my ($team_num) = @_;
  delete $config{'team_num'}{'SAC'} if $season < 2025;
  delete $config{'team_num'}{'OAK'} if $season >= 2025;
  my %team_nums = reverse %{$config{'team_num'}};

  # Ensure we have the mappings for this season
  if (!defined($config{'team_map'}{$season})) {
    %{$config{'team_map'}} = () if !defined($config{'team_map'});
    my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
    my $sql = 'SELECT team_id, alt_team_id
               FROM SPORTS_MLB_TEAMS_NAMING
               WHERE ? BETWEEN season_from AND IFNULL(season_to, 2099)
               AND   alt_team_id IS NOT NULL;';
    my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
    $dbh->disconnect if defined($dbh);
    %{$config{'team_map'}{$season}} = ();
    foreach my $row (@$ret) {
      $config{'team_map'}{$season}{$$row{'team_id'}} = $$row{'alt_team_id'};
    }
  }

  # Convert the current ID map above to the season's list
  my $team_id = $team_nums{$team_num};
  $team_id = $config{'team_map'}{$season}{$team_id}
    if defined($team_id) && defined($config{'team_map'}{$season}{$team_id});
  return $team_id;
}

# There's an alternative, lower case, version (such as LAA being ana, CHC still being chc, etc)
%{$config{'alt_team_ids'}} = ( 'laa' => 'ana', 'lad' => 'la', 'wsh' => 'was' );
sub convert_team_remote_alt_prepare {
  return if defined($config{'alt_team_ids_prepd'});

  # Custom additions dependent on the season
  our $season;
  # Up to (and inc) 2011, the Marlins were known as the Florida Marlins
  $config{'alt_team_ids'}{'mia'} = 'fla' if $season < 2012;

  # Flag we're done so we don't keep doing this...
  $config{'alt_team_ids_prepd'} = 1;
}

sub convert_team_remote_alt {
  convert_team_remote_alt_prepare();

  my ($team_id) = @_;
  $team_id = lc($team_id);

  # If there is a mapping, use that, otherwise use what we were given
  if (defined($config{'alt_team_ids'}{$team_id})) {
    return $config{'alt_team_ids'}{$team_id};
  } else {
    return $team_id;
  }
}
sub convert_team_remote_alt_reverse {
  convert_team_remote_alt_prepare();

  my ($alt_team_id) = @_;
  $alt_team_id = lc($alt_team_id);
  %{$config{'alt_team_ids_reverse'}} = reverse %{$config{'alt_team_ids'}}
    if !defined($config{'alt_team_ids_reverse'});

  # If there is a mapping, use that, otherwise use what we were given
  if (defined($config{'alt_team_ids_reverse'}{$alt_team_id})) {
    return uc($config{'alt_team_ids_reverse'}{$alt_team_id});
  } else {
    return uc($alt_team_id);
  }
}

# Determine MLB.com sub-domains
sub convert_team_domain {
  my ($team_id, $franchise) = @_;

  # Specific instances...
  return 'dbacks' if $team_id eq 'ARI';

  # Otherwise, lowercase and all one word
  $franchise = lc($franchise);
  $franchise =~ s/[^a-z]//g;
  return $franchise;
}

# Determine the player_id from a remote_id
sub determine_player_id {
  my ($remote_id, $first_name, $surname) = @_;
  our %config;

  # Prepare our queries?
  if (!defined($config{'sth'}{'remote_check'})) {
    $config{'sth'} = { } if !defined($config{'sth'});
    our $dbh;
    $config{'sth'}{'remote_check'} = $dbh->prepare('SELECT player_id FROM SPORTS_MLB_PLAYERS_IMPORT WHERE remote_id = ?;');
    $config{'sth'}{'remote_get'} = $dbh->prepare('SELECT IFNULL(MAX(player_id), 0) + 1 AS new_remote_id FROM SPORTS_MLB_PLAYERS_IMPORT;');
    $config{'sth'}{'remote_set'} = $dbh->prepare('INSERT INTO SPORTS_MLB_PLAYERS_IMPORT (player_id, remote_id) VALUES (?, ?);');
    $config{'sth'}{'remote_create'} = $dbh->prepare('INSERT INTO SPORTS_MLB_PLAYERS (player_id, first_name, surname) VALUES (?, ?, ?);');
  }

  # Run our checks
  $config{'sth'}{'remote_check'}->execute($remote_id);
  my ($player_id) = $config{'sth'}{'remote_check'}->fetchrow_array;
  if (!defined($player_id)) {
    # We need to create this player!
    $config{'sth'}{'remote_get'}->execute;
    ($player_id) = $config{'sth'}{'remote_get'}->fetchrow_array;
    $config{'sth'}{'remote_set'}->execute($player_id, $remote_id);
    $config{'sth'}{'remote_create'}->execute($player_id, $first_name, $surname)
      if defined($first_name) && defined($surname);
  }
  return $player_id;
}

# Return true to pacify the compiler
1;
