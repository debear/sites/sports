#!/usr/bin/perl -w
# Parse the draft results from MLB.com ready to import

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Encode;
use JSON;
use Switch;

# Validate the arguments
#  - there needs to be two: a season and draft type
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the parse draft script.\n";
  exit 98;
}

# Declare variables
our ($season, $draft_type) = @ARGV;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Validate...
if (! -e $config{'draft_file'}) {
  print STDERR "Unable to parse '$draft_type' draft results for $season\n";
  exit 10;
}

print "##\n##\n## Parsing '$draft_type' draft results from $season\n##\n##\n\n";
print STDERR "# Parsing '$draft_type' draft results: $season\n";

# Load the file and get the players
my $content = load_file($config{'draft_file'});
if (!$$content{'historical_draft'}{'queryResults'}{'totalSize'}) {
  print STDERR "# No picks found. Silently exiting.\n";
  exit;
}

# Data fix (Hash vs List)
$$content{'historical_draft'}{'queryResults'}{'row'} = [
  $$content{'historical_draft'}{'queryResults'}{'row'}
] if $$content{'historical_draft'}{'queryResults'}{'totalSize'} == 1;

# Then loop through the produce the SQL
my @picks = (); my $last_round;
foreach my $pick (@{$$content{'historical_draft'}{'queryResults'}{'row'}}) {
  # Fixing known data issues?
  fix_pick($pick);

  # Display a whole round?
  if (defined($last_round) && ($last_round ne $$pick{'round'})) {
    print_round($last_round, \@picks);
    @picks = ();
  }

  #
  # Process this pick
  #
  # Team details around the pick
  my $team_id = convert_team_id_from_num($$pick{'team_id'});
  # It would be nice to find a way of getting traded pick info? e.g., 2018 / amateur / CBB #6 / #74 ov - Grant Little; Pick from MIN to SD
  # Player details
  my $remote_id = $$pick{'player_id'};
  check_for_null(\$remote_id);
  my $player = $$pick{'name_first_last'};
  trim(\$player);
  $player = encode_entities($player);
  # Pos
  my $pos = $$pick{'primary_position'};
  check_for_null(\$pos);
  # Height
  my $height = 'NULL';
  my $height_ft = $$pick{'height_feet'};
  my $height_in = $$pick{'height_inches'};
  $height = "'" . ((12 * $height_ft) + $height_in) . "'"
    if defined($height_ft) && $height_ft;
  # Weight
  my $weight = $$pick{'weight'};
  check_for_null(\$weight);
  # School
  my $school = $$pick{'school'};
  if (defined($school) && $school !~ m/^\s*$/) {
    $school =~ s/’/'/g;
    $school = "'" . encode_entities($school) . "'";
  } else {
    $school = 'NULL';
  }
  # Bats
  my $bats = $$pick{'bats'};
  check_for_null(\$bats);
  # Throws
  my $throws = $$pick{'throws'};
  check_for_null(\$throws);

  #
  # Render
  #
  push @picks, "($season, '$draft_type', $$pick{'round_sort'}, '$$pick{'round'}', $$pick{'overall'}, $$pick{'pick'}, '$team_id', $remote_id, NULL, '$player', $pos, $height, $weight, $school, $bats, $throws)";
  $last_round = $$pick{'round'};
}

# Output the last round
print_round($last_round, \@picks);
# Then tie the two IDs together
print "# Tie to imported players (if known at this stage)
UPDATE `SPORTS_MLB_DRAFT`
JOIN `SPORTS_MLB_PLAYERS_IMPORT`
  ON (`SPORTS_MLB_PLAYERS_IMPORT`.`remote_id` = `SPORTS_MLB_DRAFT`.`remote_id`)
SET `SPORTS_MLB_DRAFT`.`player_id` = `SPORTS_MLB_PLAYERS_IMPORT`.`player_id`
WHERE `SPORTS_MLB_DRAFT`.`season` = $season
AND   `SPORTS_MLB_DRAFT`.`draft_type` = '$draft_type';\n\n";
# Table maintenance
print "# Table maintenance\nALTER TABLE `SPORTS_MLB_DRAFT` ORDER BY `season`, `draft_type`, `pick`;\n\n";
# Mark draft as imported
print "# Store processing\nINSERT INTO `SPORTS_MLB_DRAFT_SEASON` (`season`, `$draft_type`) VALUES ($season, 1) ON DUPLICATE KEY UPDATE `$draft_type` = VALUES(`$draft_type`);\n";

# Load contents of a file
sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...
  $contents = decode_json(Encode::encode('UTF-8', $contents));

  return $contents;
}

# Display the SQL for a single round
sub print_round {
  my ($round, $list) = @_;
  print "# Round $round
INSERT INTO `SPORTS_MLB_DRAFT` (`season`, `draft_type`, `round`, `round_descrip`, `pick`, `round_pick`, `team_id`, `remote_id`, `player_id`, `player`, `pos`, `height`, `weight`, `school`, `bats`, `throws`) VALUES
  "  . join(",\n  ", @$list) . "
ON DUPLICATE KEY UPDATE `round` = VALUES(`round`), `round_descrip` = VALUES(`round_descrip`), `round_pick` = VALUES(`round_pick`), `team_id` = VALUES(`team_id`), `remote_id` = VALUES(`remote_id`),
  `player` = VALUES(`player`), `pos` = VALUES(`pos`), `height` = VALUES(`height`), `weight` = VALUES(`weight`), `school` = VALUES(`school`), `bats` = VALUES(`bats`), `throws` = VALUES(`throws`);\n\n";
}

# Data fixes
sub fix_pick {
  my ($pick) = @_;

  # Missing round_sort
  if ($$pick{'round_sort'} eq '') {
    $$pick{'round_sort'} = $$pick{'round'};
  }

  # Dodgy position?
  if (defined($$pick{'primary_position'})) {
    switch ($$pick{'primary_position'}) {
      case '1'   { $$pick{primary_position} = 'P';   }
      case '2'   { $$pick{primary_position} = 'C';   }
      case '3'   { $$pick{primary_position} = '1B';  }
      case '4'   { $$pick{primary_position} = '2B';  }
      case '5'   { $$pick{primary_position} = '3B';  }
      case '6'   { $$pick{primary_position} = 'SS';  }
      case '7'   { $$pick{primary_position} = 'LF';  }
      case '8'   { $$pick{primary_position} = 'CF';  }
      case '9'   { $$pick{primary_position} = 'RF';  }
      case 'E'   { $$pick{primary_position} = undef; }
      case 'I'   { $$pick{primary_position} = undef; }
      case 'IF'  { $$pick{primary_position} = undef; }
      case 'LHP' { $$pick{primary_position} = 'P';   }
      case 'O'   { $$pick{primary_position} = 'OF';  }
      case 'RHP' { $$pick{primary_position} = 'P';   }
      case 'K'   { $$pick{primary_position} = 'P';   } # RHP
      case 'L'   { $$pick{primary_position} = 'P';   } # LHP
      case 'S'   { $$pick{primary_position} = undef; }
      case 'UTL' { $$pick{primary_position} = undef; }
    }
  }

  # Round description standardisation
  switch ($$pick{'round'}) {
    # Compensatory (1st Round)
    case '1C' { $$pick{'round'} = 'C1';  }
    case 'C'  { $$pick{'round'} = 'C1';  }
    # Compensatory (2nd Round)
    case '2C' { $$pick{'round'} = 'C2';  }
    # Competitive Balance
    case 'CA' { $$pick{'round'} = 'CBA'; }
    case 'CB-A' { $$pick{'round'} = 'CBA'; }
    case 'CB' { $$pick{'round'} = 'CBB'; }
    case 'CB-B' { $$pick{'round'} = 'CBB'; }
    # Compensatory (3rd Round)
    case '3C' { $$pick{'round'} = 'C3';  }
    # Compensatory (4th Round)
    case '4C' { $$pick{'round'} = 'C4';  }
  }
}
