#!/usr/bin/perl -w
# Get the draft results from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be two: a season and draft type
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the get draft script.\n";
  exit 98;
}

# Declare variables
our ($season, $draft_type) = @ARGV;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

print "#\n# Downloading '$draft_type' draft results for '$season'\n#\n";

my $url = "http://lookup-service-prod.mlb.com/json/named.historical_draft.bam?season=$season&draft_type=%27$config{'draft_types'}{$draft_type}%27";
my $local = $config{'draft_file'};
print "## From: $url\n## To:   $local\n";
mkdir $config{'draft_dir'}
  if ! -e $config{'draft_dir'};
download($url, $local, {'skip-today' => 1, 'gzip' => 1});

