#!/usr/bin/perl -w
our %config;
our $season;
our $draft_type;

$config{'draft_dir'} = "$config{'base_dir'}/_data/drafts/$season";
$config{'draft_file'} = "$config{'draft_dir'}/$draft_type.json.gz";

# Convert the codes for the various draft types
%{$config{'draft_types'}} = (
  'amateur' => 'JR',
  'r5-maj' => 'RV',
  'r5-aaa' => 'RA',
  'r5-aa' => 'RT',
  # Legacy drafts
  'august' => 'AL',      # 1965-66
  'jan-am' => 'NR',      # 1966-86
  'jan-sec' => 'NS',     # 1966-86
  'jun-sec' => 'JS',     # 1966-86
  'jun-sec-del' => 'JD', # 1971
);

# Return true to pacify the compiler
1;
