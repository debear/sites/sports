#!/usr/bin/perl -w
# Parse the draft results from MLB.com ready to import

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Encode;
use JSON;
use Switch;

# Validate the arguments
#  - there needs to be two: a season and draft type
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the parse draft script.\n";
  exit 98;
}

# Declare variables
our ($season, $draft_type) = @ARGV;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Validate...
if (! -e $config{'draft_file'}) {
  print STDERR "Unable to parse '$draft_type' draft results for $season\n";
  exit 10;
}

print "##\n##\n## Parsing '$draft_type' draft results from $season\n##\n##\n\n";
print STDERR "# Parsing '$draft_type' draft results: $season\n";

# Load the file and get the players
my $content = load_file($config{'draft_file'});
if (!@{$$content{'drafts'}{'rounds'}}) {
  print STDERR "# No picks found. Silently exiting.\n";
  exit;
}

# Then loop through the produce the SQL
my $round_sort = 0;
foreach my $round (@{$$content{'drafts'}{'rounds'}}) {
  my @picks = ();
  $round_sort++;
  my $round_descrip = fix_round($$round{'round'});

  foreach my $pick (@{$$round{'picks'}}) {
    # Team details around the pick
    my $team_id = convert_team_id_from_num($$pick{'team'}{'id'});
    # Player ID
    my $remote_id = $$pick{'person'}{'id'};
    check_for_null(\$remote_id);
    # Name
    my $player = $$pick{'person'}{'nameFirstLast'};
    trim(\$player);
    $player = encode_entities($player);
    # Pos
    my $pos = $$pick{'person'}{'primaryPosition'}{'abbreviation'};
    fix_pos(\$pos);
    check_for_null(\$pos);
    # Height
    my $height = 'NULL';
    my ($height_ft, $height_in) = ($$pick{'person'}{'height'} =~ /^(\d+)\' (\d+)"$/gsi);
    $height = "'" . ((12 * $height_ft) + $height_in) . "'"
      if defined($height_ft) && $height_ft;
    # Weight
    my $weight = $$pick{'person'}{'weight'};
    check_for_null(\$weight);
    # School
    my $school = $$pick{'school'}{'name'};
    if (defined($school) && $school !~ m/^\s*$/) {
      $school =~ s/’/'/g;
      $school = "'" . encode_entities($school) . "'";
    } else {
      $school = 'NULL';
    }
    # Bats
    my $bats = $$pick{'person'}{'batSide'}{'code'};
    check_for_null(\$bats);
    # Throws
    my $throws = $$pick{'person'}{'pitchHand'}{'code'};
    check_for_null(\$throws);

    # Add to our list
    push @picks, "($season, '$draft_type', $round_sort, '$round_descrip', $$pick{'pickNumber'}, $$pick{'roundPickNumber'}, '$team_id', $remote_id, NULL, '$player', $pos, $height, $weight, $school, $bats, $throws)";
  }

  # Now render
  print_round($$round{'round'}, \@picks);
}

# Then tie the two IDs together
print "# Tie to imported players (if known at this stage)
UPDATE `SPORTS_MLB_DRAFT`
JOIN `SPORTS_MLB_PLAYERS_IMPORT`
  ON (`SPORTS_MLB_PLAYERS_IMPORT`.`remote_id` = `SPORTS_MLB_DRAFT`.`remote_id`)
SET `SPORTS_MLB_DRAFT`.`player_id` = `SPORTS_MLB_PLAYERS_IMPORT`.`player_id`
WHERE `SPORTS_MLB_DRAFT`.`season` = $season
AND   `SPORTS_MLB_DRAFT`.`draft_type` = '$draft_type';\n\n";
# Table maintenance
print "# Table maintenance\nALTER TABLE `SPORTS_MLB_DRAFT` ORDER BY `season`, `draft_type`, `pick`;\n\n";
# Mark draft as imported
print "# Store processing\nINSERT INTO `SPORTS_MLB_DRAFT_SEASON` (`season`, `$draft_type`) VALUES ($season, 1) ON DUPLICATE KEY UPDATE `$draft_type` = VALUES(`$draft_type`);\n";

# Load contents of a file
sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...
  $contents = decode_json(Encode::encode('UTF-8', $contents));

  return $contents;
}

# Display the SQL for a single round
sub print_round {
  my ($round, $list) = @_;
  print "# Round $round
INSERT INTO `SPORTS_MLB_DRAFT` (`season`, `draft_type`, `round`, `round_descrip`, `pick`, `round_pick`, `team_id`, `remote_id`, `player_id`, `player`, `pos`, `height`, `weight`, `school`, `bats`, `throws`) VALUES
  "  . join(",\n  ", @$list) . "
ON DUPLICATE KEY UPDATE `round` = VALUES(`round`), `round_descrip` = VALUES(`round_descrip`), `round_pick` = VALUES(`round_pick`), `team_id` = VALUES(`team_id`), `remote_id` = VALUES(`remote_id`),
  `player` = VALUES(`player`), `pos` = VALUES(`pos`), `height` = VALUES(`height`), `weight` = VALUES(`weight`), `school` = VALUES(`school`), `bats` = VALUES(`bats`), `throws` = VALUES(`throws`);\n\n";
}

# Data fixes
sub fix_round {
  my ($round) = @_;

  # Round description standardisation
  switch ($round) {
    # Compensatory (1st Round)
    case '1C'   { $round = 'C1';  }
    case 'C'    { $round = 'C1';  }
    # Compensatory (2nd Round)
    case '2C'   { $round = 'C2';  }
    # Competitive Balance
    case 'CA'   { $round = 'CBA'; }
    case 'CB-A' { $round = 'CBA'; }
    case 'CB'   { $round = 'CBB'; }
    case 'CB-B' { $round = 'CBB'; }
    # Compensatory (3rd Round)
    case '3C'   { $round = 'C3';  }
    # Compensatory (4th Round)
    case '4C'   { $round = 'C4';  }
  }

  return $round;
}

sub fix_pos {
  my ($pos) = @_;

  switch ($$pos) {
    case 'IF'  { $$pos = undef; }
    case 'UTL' { $$pos = undef; }
    case 'TWP' { $$pos = 'TW'; }
  }
}
