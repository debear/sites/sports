#!/usr/bin/perl -w
# Download and import game data from MLB.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'games';
my %logs = get_log_def();

# Guesstimate the season
my $date = time2date(time());
my $season_guess = substr($date, 0, 4);

# Unless passed in a special argument, we're to ask the user how to operate
my $season;

# Determine for ourselves
if ($config{'is_unattended'}) {
  # Season is our guessed season
  $season = $season_guess;

# Passed in
} elsif (($season) = grep(/^20\d{2}$/, @ARGV)) {
  # Flag unattended mode for processing games
  $config{'is_unattended'} = 1;

# Get from the user
} else {
  $season = question_user("Please enter a season in YYYY format [$season_guess]", '\d{4}', $season_guess);
}

# Get the suggested list of games to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT game_type, game_id, home, visitor,
       CONCAT(DATE_FORMAT(game_time, "%H:%i"), " ", DATE_FORMAT(game_date, "%D %b")) AS date_fmt
FROM SPORTS_MLB_SCHEDULE
WHERE season = ?
AND   game_date < CURDATE()
AND   status IS NULL
ORDER BY game_date, game_time, game_id;';
my $game_list = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
$dbh->disconnect if defined($dbh);

# No need to continue (but not an error) if nothing found
if (!@$game_list) {
  print STDERR '# There are no outstanding games to import.' . "\n";
  end_script();
}

# Process
my %list = ( 'all' => [ ], 'regular' => [ ], 'playoff' => [ ], 'process' => [ ], 'list' => [ ] );
my %game_ids = ( 'regular' => [ ], 'playoff' => [ ] );
foreach my $game (@$game_list) {
  # Summarise for display
  $$game{'summary'} = 'Game ' . uc(substr($$game{'game_type'}, 0, 1)) . '-' . $$game{'game_id'} . ': ' . $$game{'visitor'} . ' @ ' . $$game{'home'} . ', ' . $$game{'date_fmt'};
  $$game{'summary'} .= ' - ' . playoff_summary($$game{'game_id'})
    if ($$game{'game_type'} eq 'playoff');
  print $$game{'summary'} . "\n"
    if !$config{'is_historical'};

  # Store for info
  push @{$list{'all'}}, $game;
}

# Which do we want to include? If automated, yes, we do...
my $import_all = ($config{'is_unattended'} ? 'y' : question_user('Do you wish to import ALL these games? [Y/n/c]', '[ync]', 'Y'));

# Are we excluding specific games?
if ($import_all =~ m/^n$/i) {
  foreach my $game (@{$list{'all'}}) {
    my $import = question_user($$game{'summary'} . ' - Import? [Y/n]', '[yn]', 'Y');
    if ($import =~ m/^y$/i) {
      push @{$list{'process'}}, $game;
      push @{$list{$$game{'game_type'}}}, $game;
      push @{$game_ids{$$game{'game_type'}}}, $$game{'game_id'};
    }
  }

# No, so process the full list if not cancelling
} elsif ($import_all !~ m/^c$/i) {
  $list{'process'} = $list{'all'};
  foreach my $game (@{$list{'all'}}) {
    push @{$list{$$game{'game_type'}}}, $game;
    push @{$game_ids{$$game{'game_type'}}}, $$game{'game_id'};
  }
}

# Check again - if here, no rounds were explicitly selected
if (!@{$list{'process'}}) {
  print ' - No games were selected to be imported, exiting.' . "\n";
  exit $config{'exit_codes'}{'pre-flight'};
}

# Inform user
print "\nAcquiring data from $season, " . @{$list{'process'}} . " game(s)\n";
print 'Run started at ' . `date` . "\n";

# Idenfity the log file
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Parse each of the games
#
foreach my $game (@{$list{'process'}}) {
  print "- $$game{'summary'}\n";

  # Download, if we haven't done so already (handled in-script)
  print '  - Download: ';
  if (!$config{'is_historical'} || $season == 2017) {
    command("$config{'base_dir'}/games/download.pl $season $$game{'game_type'} $$game{'game_id'}",
            "$config{'log_dir'}/$logs{'games_d'}.log",
            "$config{'log_dir'}/$logs{'games_d'}.err");
    print "[ Done ]\n";
  } else {
    print "[ Skipped ]\n";
  }
  next if download_only();

  # Parse
  print '  - Parse:    ';
  command("$config{'base_dir'}/games/parse.pl $season $$game{'game_type'} $$game{'game_id'}",
          "$config{'log_dir'}/$logs{'games_p'}.sql",
          "$config{'log_dir'}/$logs{'games_p'}.err");
  print "[ Done ]\n";
}

# If only downloading, go no further
end_script() if download_only();

#
# Import into the database
#
print '=> Importing Games: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'games_p'}.sql",
        "$config{'log_dir'}/$logs{'games_i'}.log",
        "$config{'log_dir'}/$logs{'games_i'}.err");
done(9);

# Next group of scripts are the first section of post-processing
$config{'exit_status'} = 'post-process-1';

#
# Form the tmp_date_list queries to prepend to our stored procedures
#
my %tmp_date_list = ();
$tmp_date_list{'full'} = "DROP TEMPORARY TABLE IF EXISTS tmp_date_list; CREATE TEMPORARY TABLE tmp_date_list (season YEAR NOT NULL, the_date DATE, PRIMARY KEY (season, the_date)) ENGINE = MEMORY SELECT season, game_date AS the_date FROM SPORTS_MLB_SCHEDULE WHERE season = '$season' AND ((game_type = 'regular' AND game_id IN ('" . join("', '", @{$game_ids{'regular'}}) . "')) OR (game_type = 'playoff' AND game_id IN ('" . join("', '", @{$game_ids{'playoff'}}) . "'))) GROUP BY season, the_date; ";
foreach my $game_type ( 'regular', 'playoff' ) {
  $tmp_date_list{$game_type} = "DROP TEMPORARY TABLE IF EXISTS tmp_date_list; CREATE TEMPORARY TABLE tmp_date_list (season YEAR NOT NULL, the_date DATE, PRIMARY KEY (season, the_date)) ENGINE = MEMORY SELECT season, game_date AS the_date FROM SPORTS_MLB_SCHEDULE WHERE season = '$season' AND game_type = '$game_type' AND game_id IN ('" . join("', '", @{$game_ids{$game_type}}) . "') GROUP BY season, the_date; ";
}

#
# And ditto tmp_game_list
#
my %tmp_game_list = ();
$tmp_game_list{'full'} = "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '$season', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY; ";
foreach my $game_type ( 'regular', 'playoff' ) {
  $tmp_game_list{'full'} .= "INSERT INTO tmp_game_list (game_type, game_id) VALUES ('$game_type', '" . join("'), ('$game_type', '", @{$game_ids{$game_type}}) . "'); "
    if @{$list{$game_type}};

  $tmp_game_list{$game_type} = "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '$season', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY; INSERT INTO tmp_game_list (game_type, game_id) VALUES ('$game_type', '" . join("'), ('$game_type', '", @{$game_ids{$game_type}}) . "'); ";
}

#
# Check for new players
#
print '=> New Players:' . "\n";
print '  - Parse:  ';
command("$config{'base_dir'}/players/new.pl",
        "$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_p'}.err");
print "[ Done ]\n  - Import: ";
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'players_p'}.sql",
        "$config{'log_dir'}/$logs{'players_i'}.log",
        "$config{'log_dir'}/$logs{'players_i'}.err");
print "[ Done ]\n";

#
# Update player totals
#
# Season totals
print '=> Season Totals (Players): ';
foreach my $game_type ( 'regular', 'playoff' ) {
  next if !@{$list{$game_type}};
  command("echo \"$tmp_game_list{$game_type}CALL mlb_totals_players_season();\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'totals_pl'}.log",
          "$config{'log_dir'}/$logs{'totals_pl'}.err");
  command("echo \"CALL mlb_totals_players_sort('$season', '$game_type');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'totals_pl'}.log",
          "$config{'log_dir'}/$logs{'totals_pl'}.err");
}
command("echo \"CALL mlb_totals_players_order();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err");
done(1);

# Split/Situational/Batter vs Pitcher stats
print '=> Split Stats: ';
command("echo \"$tmp_game_list{'full'}CALL mlb_totals_players_splits();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err");
done(13);

#
# Update team totals
#
# Season totals
print '=> Season Totals (Teams): ';
foreach my $game_type ( 'regular', 'playoff' ) {
  command("echo \"CALL mlb_totals_teams('$season', '$game_type');\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err")
    if @{$list{$game_type}};
  command("echo \"CALL mlb_totals_teams_sort('$season', '$game_type');\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err")
    if @{$list{$game_type}};
}
command("echo \"CALL mlb_totals_teams_order();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err");
done(3);

# Misc stats
print '=> Misc Stats: ';
command("echo \"$tmp_game_list{'full'}CALL mlb_totals_misc();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_msc'}.log",
        "$config{'log_dir'}/$logs{'totals_msc'}.err");
done(14);

#
# Team rosters
#
my $roster_method = ($season < 2017 ? 'mlb_rosters_backdate' : 'mlb_rosters');
my $roster_prereq = ($season < 2017 ? $tmp_date_list{'full'} : $tmp_game_list{'full'});
print '=> Updating roster details: ';
command("echo \"${roster_prereq}CALL $roster_method();\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'rosters'}.log",
        "$config{'log_dir'}/$logs{'rosters'}.err");
done(1);

print '=> Roster Tweaks:' . "\n";
print '  - Parse:  ';
command("$config{'base_dir'}/players/new.pl",
        "$config{'log_dir'}/$logs{'rostfix_p'}.sql",
        "$config{'log_dir'}/$logs{'rostfix_p'}.err");
print "[ Done ]\n  - Import: ";
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'rostfix_p'}.sql",
        "$config{'log_dir'}/$logs{'rostfix_i'}.log",
        "$config{'log_dir'}/$logs{'rostfix_i'}.err");
print "[ Done ]\n";

# Next group of scripts are the second section of post-processing
$config{'exit_status'} = 'post-process-2';

#
# Regular season updates
#
if (@{$list{'regular'}}) {
  # Standings
  print '=> Standings: ';
  command("echo \"$tmp_date_list{'regular'}CALL mlb_standings('$config{'num_recent'}');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'standings'}.log",
          "$config{'log_dir'}/$logs{'standings'}.err");
  done(15);

  # Power ranks
  print '=> Power Ranks: ';
  command("echo \"CALL mlb_power_ranks('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'power_ranks'}.log",
          "$config{'log_dir'}/$logs{'power_ranks'}.err");
  done(13);

  # Playoff seeds
  print '=> Playoff Seeds: ';
  command("echo \"CALL mlb_playoff_seeds('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_seeds'}.log",
          "$config{'log_dir'}/$logs{'po_seeds'}.err");
  done(11);

  # Playoff matchups
  print '=> Playoff Matchups: ';
  command("echo \"CALL mlb_playoff_matchups('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_matchups'}.log",
          "$config{'log_dir'}/$logs{'po_matchups'}.err");
  done(8);
}

#
# Playoff series updates
#
if (@{$list{'playoff'}}) {
  # Updated series
  print '=> Playoff Series: ';
  command("echo \"$tmp_game_list{'playoff'}CALL mlb_playoff_series('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_series'}.log",
          "$config{'log_dir'}/$logs{'po_series'}.err");
  done(10);

  # Playoff matchups
  print '=> Playoff Matchups: ';
  command("echo \"CALL mlb_playoff_matchups('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_matchups'}.log",
          "$config{'log_dir'}/$logs{'po_matchups'}.err");
  done(8);

  # Histories
  print '=> Playoff History: ';
  command("echo \"CALL mlb_history_playoffs('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_history'}.log",
          "$config{'log_dir'}/$logs{'po_history'}.err");
  done(9);

  # Schedule Refresh (new series schedule info included)
  if (schedule_future_po_series()) {
    print '=> Playoff Schedule:' . "\n";
    print '  - Parse:  ';
    command("$config{'base_dir'}/schedule/parse.pl $season --playoff" . (!$config{'is_historical'} ? " $date" : ''),
            "$config{'log_dir'}/$logs{'po_sched_p'}.sql",
            "$config{'log_dir'}/$logs{'po_sched_p'}.err");
    print "[ Done ]\n  - Import: ";
    command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'po_sched_p'}.sql",
            "$config{'log_dir'}/$logs{'po_sched_i'}.log",
            "$config{'log_dir'}/$logs{'po_sched_i'}.err");
    print "[ Done ]\n";

  }
}

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'games_d'     => '01_games_download',
    'games_p'     => '02_games_parse',
    'games_i'     => '03_games_import',
    'players_p'   => '04_players_parse',
    'players_i'   => '05_players_import',
    'totals_pl'   => '06_totals_player',
    'totals_tm'   => '07_totals_team',
    'totals_msc'  => '08_totals_misc',
    'rosters'     => '09_roster_updates',
    'rostfix_p'   => '10_roster_fix_parse',
    'rostfix_i'   => '11_roster_fix_import',
    'standings'   => '12_standings',
    'power_ranks' => '13_power_ranks',
    'po_seeds'    => '14_playoff_seeds',
    'po_series'   => '15_playoff_series',
    'po_matchups' => '16_playoff_matchups',
    'po_history'  => '17_playoff_history',
    'po_sched_p'  => '18_playoff_sched_parse',
    'po_sched_i'  => '19_playoff_sched_import',
  );
}

#
# Determine if a future playoff series has been calced
#
sub schedule_future_po_series {
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my ($max_date) = $dbh->selectrow_array('SELECT MAX(game_date) FROM SPORTS_MLB_SCHEDULE WHERE season = ? AND game_type = "playoff";', { Slice => {} }, $season);
  my ($future_series) = $dbh->selectrow_array('SELECT COUNT(DISTINCT round_code) FROM SPORTS_MLB_PLAYOFF_SERIES WHERE season = ? AND the_date > ? AND lower_seed > 0;', { Slice => {} }, $season, $max_date);
  $dbh->disconnect if defined($dbh);

  return $future_series;
}
