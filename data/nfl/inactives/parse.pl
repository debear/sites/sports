#!/usr/bin/perl -w
# Parse the injury report

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, game type, week
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to parse the inactive list.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Define rest of vars from arguments
my ($season, $game_type, $week) = @ARGV;

# Have we the file?
my $file = sprintf('%s/_data/%d/inactives/%s_%02d.htm.gz',
                   $config{'base_dir'},
                   $season,
                   $game_type,
                   $week);
if (!-e $file) {
  print "# No Inactive List found ($season, $game_type, $week)\n";
  exit;
}

# Load the injury report
print "##\n## Loading inactives list: $file\n##\n";
my $contents;
open FILE, "gzip -dc $file |";
my @contents = <FILE>;
close FILE;
$contents = join('', @contents);
@contents = (); # Garbage collect...

# Reset a previous run
print "\nDELETE FROM SPORTS_NFL_GAME_INACTIVES WHERE season = '$season' AND game_type = '$game_type' AND week = '$week';\n\n";
my %list = ();

# Get by list of games
my @games = ($contents =~ m/var isTeamHome.*?datatableHome\s+=\s+Y\.one\('\.data-table-(.*?)-\d+'\),\s+datatableAway\s+=\s+Y\.one\('\.data-table-(.*?)-\d+'\),.*?var dataAway = \[(.*?)\];\s+var dataHome = \[(.*?)\];/gsi);

my %found = ( );
while (my ($home_id, $away_id, $away_list, $home_list) = splice(@games, 0, 4)) {
  # Then loop through the two teams in the game
  foreach my $type ('away', 'home') {
    my $team_id = convert_team_id($type eq 'away' ? $away_id : $home_id);
    my $injuries = ($type eq 'away' ? $away_list : $home_list);

    my @players = ($injuries =~ m/{player:\s+"([^"]*?)",\s+position:\s+"([^"]*?)",\s+status:\s+"Inactive",\s+comments:\s+"[^"]*?",\s+lastName:\s+"[^"]*?",\s+firstName:\s+"[^"]*?",\s+esbId:\s*?"([^"]*?)"\s+}/gsi);

    # And process them individually
    while (my ($player, $position, $remote_profile_id) = splice(@players, 0, 3)) {
      # Skip duplicates...
      next if defined($found{$remote_profile_id});
      # Process
      my %tmp = ( 'player' => $player,
                  'position' => $position,
                  'remote_profile_id' => $remote_profile_id );
      push @{$list{$team_id}}, \%tmp;
      $found{$remote_profile_id} = 1;
    }
  }
}

##
## Display
##
my @teams = sort keys %list;
foreach my $team_id (@teams) {
  print "##\n## $team_id\n##\n\n";

  # Determine the game_id for this team
  print "# Determine game_id\nSELECT game_id INTO \@v_game_id FROM SPORTS_NFL_SCHEDULE WHERE season = '$season' AND game_type = '$game_type' AND week = '$week' AND (home = '$team_id' OR visitor = '$team_id');\n\n";

  foreach my $player (@{$list{$team_id}}) {
    # Manipulate before storing
    $$player{'remote_profile_id'} = convert_text($$player{'remote_profile_id'});
    print "# $$player{'player'} ($$player{'remote_profile_id'}, $$player{'position'})\n";
    print "INSERT INTO `SPORTS_NFL_GAME_INACTIVES` (`season`, `game_type`, `week`, `game_id`, `team_id`, `player_id`, `pos`)
  SELECT '$season', '$game_type', '$week', \@v_game_id, '$team_id', `player_id`, '$$player{'position'}'
  FROM `SPORTS_NFL_PLAYERS_IMPORT`
  WHERE `remote_profile_id` = '$$player{'remote_profile_id'}';\n\n";
  }
}

print "##\n## Maintenance\n##\nALTER TABLE `SPORTS_NFL_GAME_INACTIVES` ORDER BY `season`, `game_type`, `week`, `game_id`, `team_id`, `player_id`;\n\n";

