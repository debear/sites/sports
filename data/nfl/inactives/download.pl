#!/usr/bin/perl -w
# Get the current inactive list from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, week
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to download the inactive list.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($season, $game_type, $week) = @ARGV;
my $dir = sprintf('%s/_data/%d/inactives', $config{'base_dir'}, $season);
my $file = sprintf('%s_%02d.htm.gz', $game_type, $week);

# Run...
print '# Downloading ' . ucfirst($game_type) . " Week $week Inactive List: ";

# Skip if already done and a special flag has been passed (given time-sensitive nature of repeated runs, always downloading needs to be the default behaviour)
if (-e "$dir/$file" && grep(/--skip-if-exists/, @ARGV)) {
  print "[ Skipped ]\n";
  exit;
}

# Make sure the game dir exists
make_path($dir);

# Build the URL
my $reg_weeks = ($season < 2021 ? 17 : 18);
my $url_week = ($game_type eq 'regular' ? $week : $reg_weeks + $week);
$url_week++ if $game_type eq 'playoff' && $week == 4; # Conf Champ == 20, SB = 22
my $url = 'http://nflcdns.nfl.com/inactives?week=' . $url_week;

# Pre-2016 reports unavailable...
if ($season < 2016) {
  print STDERR "# Unable to download inactives list from previous seasons.\n";
  exit 10;
}

download($url, "$dir/$file", {'gzip' => 1});
print "[ Done ]\n";
