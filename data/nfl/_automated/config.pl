#!/usr/bin/perl -w
# Automated processing specific configuration

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

our $mode;
$config{'log_dir'} = "$config{'base_dir'}/_logs/" . (defined($mode) ? "_$mode" : '');
my $date_disp = `date +'%F'`; chomp($date_disp);

##
## Daily config
##
%{$config{'daily'}} = (
  'app' => 'sports_nfl',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    # Rosters
    { 'label' => 'Rosters',
      'script' => "$config{'base_dir'}/update_rosters.pl" },
    # Games
    { 'label' => 'Games',
      'script' => "$config{'base_dir'}/update_games.pl --unattended" },
    # Upload Database
    { 'label' => 'Upload Database',
      'script' => '/var/www/debear/bin/git-admin/server-sync sports_nfl live --daily-tunnel --benchmark' },
  ],
  'email-subject' => 'Automated daily NFL script for ' . $date_disp,
);

##
## Injuries config
##
%{$config{'injuries'}} = (
  'app' => 'sports_nfl_inj',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'Injury Reports',
      'script' => "$config{'base_dir'}/update_injuries.pl" },
    # Upload Database
    { 'label' => 'Upload Database',
      'script' => '/var/www/debear/bin/git-admin/server-sync sports_nfl_injuries live --benchmark' },
  ],
  'email-subject' => 'Automated NFL injury script for ' . $date_disp,
  'log_inc_time' => '%H%M',
);

##
## Inactives config
##
%{$config{'inactives'}} = (
  'app' => 'sports_nfl_inct',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'Inactives List',
      'script' => "$config{'base_dir'}/update_inactives.pl" },
    # Upload Database
    { 'label' => 'Upload Database',
      'script' => '/var/www/debear/bin/git-admin/server-sync sports_nfl_injuries live --benchmark' },
  ],
  'email-subject' => 'Automated NFL inactive script for ' . $date_disp,
  'log_inc_time' => '%H%M',
);

##
## Player Mugshot config
##
%{$config{'mugshots'}} = (
  'app' => 'sports_nfl_mugshots',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'Mugshot Resync',
      'script' => "$config{'base_dir'}/update_mugshots.pl" },
    # Upload Database
    { 'label' => 'Upload Database',
      'script' => '/var/www/debear/bin/git-admin/server-sync sports_nfl_players live --benchmark' },
  ],
  'email-subject' => 'Automated NFL mugshot resync script for ' . $date_disp,
);

# Return true to pacify the compiler
1;
