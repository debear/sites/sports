#!/usr/bin/perl -w
# Perl config file with all DB, dir info, etc

use MIME::Base64;

our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

$config{'base_dir'} .= '/nfl'; # Append the NFL dir
$config{'puppeteer_dir'} = $config{'base_dir'} . '/_puppeteer';
$config{'timezone'} = 'US/Eastern';
$config{'inactives_time_to_start'} = 75; # Start processing 75 minutes before kick-off (to allow time for information to be posted)
$config{'num_recent'} = 5; # The number of games in our recent streak calcs

# Check as to whether we parse/import the data or just download it
sub download_only {
  return defined($config{'download_only'}) && $config{'download_only'};
}
# Convert team_id's from NFL.com's to ours
sub convert_team_id {
  my ($team_id, $reverse) = @_;
  my %maps = ( 'WAS' => 'WSH', 'JAC' => 'JAX', 'BLT' => 'BAL', 'CLV' => 'CLE', 'LAR' => 'LA', 'LRM' => 'LA', 'HST' => 'HOU', 'ARZ' => 'ARI' );

  # Reversing the search?
  %maps = reverse %maps if defined($reverse);

  if (defined($maps{$team_id})) {
    return $maps{$team_id};
  } else {
    return $team_id;
  }
}

# Get info for a given game
sub get_game_info {
  my ($season, $game_type, $game_id) = @_;
  my $week = substr($game_id, 0, -2);
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT SPORTS_NFL_SCHEDULE.nfl_id,
                    SPORTS_NFL_SCHEDULE.nfl_gsis_id,
                    LOWER(VISITOR.franchise) AS visitor,
                    LOWER(VISITOR.city) AS visitor_city,
                    SPORTS_NFL_SCHEDULE.visitor AS visitor_id,
                    LOWER(HOME.franchise) AS home,
                    LOWER(HOME.city) AS home_city,
                    SPORTS_NFL_SCHEDULE.home AS home_id
             FROM SPORTS_NFL_SCHEDULE
             JOIN SPORTS_NFL_TEAMS AS VISITOR
               ON (VISITOR.team_id = SPORTS_NFL_SCHEDULE.visitor)
             JOIN SPORTS_NFL_TEAMS AS HOME
               ON (HOME.team_id = SPORTS_NFL_SCHEDULE.home)
             WHERE SPORTS_NFL_SCHEDULE.season = ?
             AND   SPORTS_NFL_SCHEDULE.game_type = ?
             AND   SPORTS_NFL_SCHEDULE.week = ?
             AND   SPORTS_NFL_SCHEDULE.game_id = ?;';
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $game_type, $week, $game_id);
  $dbh->disconnect if defined($dbh);
  return $$ret[0];
}

# Convert undefined values to 0
sub convert_undef_zero {
  my ($txt) = @_;
  $$txt = 0 if !defined($$txt);
}

# Return true to pacify the compiler
1;
