#!/usr/bin/perl -w
# Parse the injury report

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be four: season, game type, week
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to parse the injury report.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Define rest of vars from arguments
my ($season, $game_type, $week) = @ARGV;

# Have we the file?
my $file = sprintf('%s/_data/%d/injuries/%s_%02d.htm.gz',
                   $config{'base_dir'},
                   $season,
                   $game_type,
                   $week);
if (!-e $file) {
  print "# No Injury Report found ($season, $game_type, $week)\n";
  exit;
}

# Load the injury report
print "##\n## Loading injury report: $file\n##\n";
my $contents;
open FILE, "gzip -dc $file |";
my @contents = <FILE>;
close FILE;
$contents = join('', @contents);
@contents = (); # Garbage collect...

# Reset a previous run
print "\nDELETE FROM SPORTS_NFL_PLAYERS_INJURIES WHERE season = '$season' AND game_type = '$game_type' AND week = '$week';\n\n";
my %list = ();

##
## 2013 and earlier
##
if ($season < 2014) {
  # Get out the list of injuries
  my @matches = ($contents =~ m/<tr class="[^"]+colors">\s*<td colspan="5"><b>([^<]+)<\/b><\/td>\s*<\/tr>(.*?)<tr class="[^"]+colors">\s*<td colspan="5"><b>([^<]+)<\/b><\/td>\s*<\/tr>(.*?)<\/table>/gsi);

  # And parse
  while (my ($team_id, $injuries) = splice(@matches, 0, 2)) {
    $team_id = convert_team_id($team_id);
    $list{$team_id} = ();

    # Identify the individual players
    my @players = ($injuries =~ m/<td><a href=".*?\/profile\?id=([^"]+)">(.*?)(?:\n.*?)?<\/a><\/[^>]*td>\s*<td>.*?<\/[^>]*td>\s*<td>(.*?)<\/[^>]*td>\s*<td>(.*?)<\/[^>]*td>\s*<td>(.*?)<\/[^>]*td>/gsi);

    # Then process individually
    while (my ($remote_profile_id, $player, $injury, $practice, $status) = splice(@players, 0, 5)) {
      my %tmp = ( 'player' => $player,
                  'remote_profile_id' => $remote_profile_id,
                  'injury' => $injury,
                  'practice' => $practice,
                  'status' => $status );
      push @{$list{$team_id}}, \%tmp;
    }
  }

##
## 2014 onwards
##
} else {
  # Get by list of games
  my @games = ($contents =~ m/var isTeamAway.*?homeAbbr\s+=\s+'([^']+)',\s+awayAbbr\s+=\s+'([^']+)'.*?var dataAway = \[(.*?)\];.*?var dataHome = \[(.*?)\];\s+var columnStaticDef/gsi);

  while (my ($home_id, $away_id, $away_list, $home_list) = splice(@games, 0, 4)) {
    # Then loop through the two teams in the game
    foreach my $type ('away', 'home') {
      my $team_id = convert_team_id($type eq 'away' ? $away_id : $home_id);
      my $injuries = ($type eq 'away' ? $away_list : $home_list);

      my @players = ($injuries =~ m/{player:\s+"([^"]*?)",\s+position:\s+"[^"]*?",\s+injury:\s+"([^"]*?)",\s+practiceStatus:\s+"([^"]*?)",\s+gameStatus:\s+"([^"]*?)",\s+lastName:\s+"[^"]*?",\s+firstName:\s+"[^"]*?",\s+esbId:\s+"([^"]*?)"\s+}/gsi);

      # And process them individually
      while (my ($player, $injury, $practice, $status, $remote_profile_id) = splice(@players, 0, 5)) {
        my %tmp = ( 'player' => $player,
                    'remote_profile_id' => $remote_profile_id,
                    'injury' => $injury,
                    'practice' => $practice,
                    'status' => $status );
        push @{$list{$team_id}}, \%tmp;
      }
    }
  }
}

##
## Display
##
my @teams = sort keys %list;
foreach my $team_id (@teams) {
  print "##\n## $team_id\n##\n\n";

  foreach my $player (@{$list{$team_id}}) {
    # Ignore players with no status (unless they are marked as out)
    trim(\$$player{'player'});
    trim(\$$player{'injury'});
    trim(\$$player{'status'});
    next if ($$player{'injury'} eq '--' && $$player{'injury'} !~ m/^out/i) || $$player{'injury'} eq '';
    $$player{'injury'} = 'Out' if $$player{'injury'} eq '--'; # Force the status

    # Manipulate before storing
    $$player{'remote_profile_id'} = convert_text($$player{'remote_profile_id'});
    my $injury_db;
    if ($$player{'injury'} eq '--') {
      $injury_db = 'NULL';
    } else {
      $injury_db = '\'' . convert_text(ucfirst $$player{'injury'}) . '\'';
    }
    my $status_db = parse_status($$player{'status'});

    print "# $$player{'player'} ($$player{'remote_profile_id'}), $$player{'injury'}, $$player{'status'}\n";
    print "INSERT INTO `SPORTS_NFL_PLAYERS_INJURIES` (`season`, `game_type`, `week`, `player_id`, `team_id`, `status`, `injury`)
  SELECT '$season', '$game_type', '$week', `player_id`, '$team_id', '$status_db', $injury_db
  FROM `SPORTS_NFL_PLAYERS_IMPORT`
  WHERE `remote_profile_id` = '$$player{'remote_profile_id'}';\n\n";
  }
}

print "##\n## Maintenance\n##\nALTER TABLE `SPORTS_NFL_PLAYERS_INJURIES` ORDER BY `season`, `game_type`, `week`, `team_id`, `player_id`;\n\n";

#
# Convert a status from NFL.com's version to ours
#
sub parse_status {
  my ($status) = @_;

  return 'Probable' if $status =~ m/^probable$/i;
  return 'Questionable' if $status =~ m/^questionable$/i;
  return 'Doubtful' if $status =~ m/^doubtful$/i;
  return 'Out' if $status =~ m/^out$/i;
}

