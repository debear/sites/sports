#!/usr/bin/perl -w
# Get the current injury report from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, week
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to download the injury report.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($season, $game_type, $week) = @ARGV;
my $dir = sprintf('%s/_data/%d/injuries', $config{'base_dir'}, $season);
my $file = sprintf('%s_%02d.htm.gz', $game_type, $week);

# Run...
print '# Downloading ' . ucfirst($game_type) . " Week $week Injury Report: ";

# Skip if already done, but allow within 20 minutes...
if (-e "$dir/$file") {
  my @stat = stat "$dir/$file";
  if ((time() - $stat[9]) < 1200) { # 1200 = 60 * 20
    print "[ Skipped ]\n";
    exit;
  }
}

# Make sure the game dir exists
make_path($dir);

# Build the URL
my $reg_weeks = ($season < 2021 ? 17 : 18);
my $url_week = ($game_type eq 'regular' ? $week : $reg_weeks + $week);
$url_week++ if $game_type eq 'playoff' && $week == 4; # Conf Champ == 20, SB = 22
my $url = 'http://nflcdns.nfl.com/injuries?week=' . $url_week;

# Pre-2013 reports unavailable...
if ($season < 2013) {
  print STDERR "# Unable to download injury reports from a previous seasons.\n";
  exit 10;
}

download($url, "$dir/$file", {'gzip' => 1});
print "[ Done ]\n";
