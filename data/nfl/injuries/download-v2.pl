#!/usr/bin/perl -w
# Get the current injury report from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, game type, week
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to download the injury report.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($season, $game_type, $week) = @ARGV;
my $dir = sprintf('%s/_data/%d/injuries', $config{'base_dir'}, $season);
my $file = sprintf('%s_%02d.htm.gz', $game_type, $week);

# Run...
print '# Downloading ' . ucfirst($game_type) . " Week $week Injury Report: ";

# Skip if already done, but allow within 20 minutes...
if (-e "$dir/$file") {
  my @stat = stat "$dir/$file";
  if ((time() - $stat[9]) < 1200) { # 1200 = 60 * 20
    print "[ Skipped ]\n";
    exit;
  }
}

# Make sure the storage path exists
make_path($dir);

# Build the URL
my $game_type_code = ($game_type eq 'regular' ? 'REG' : 'POST');
my $url = "https://www.nfl.com/injuries/league/${season}/${game_type_code}${week}";

# And perform the download
download($url, "$dir/$file", {'gzip' => 1});
print "[ Done ]\n";
