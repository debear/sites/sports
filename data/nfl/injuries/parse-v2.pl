#!/usr/bin/perl -w
# Parse the injury report

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: season, game type, week
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to parse the injury report.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Define rest of vars from arguments
my ($season, $game_type, $week) = @ARGV;
my $test_team_id = ''; # Empty string implies all teams are processed

# Have we the file?
my $file = sprintf('%s/_data/%d/injuries/%s_%02d.htm.gz',
  $config{'base_dir'},
  $season,
  $game_type,
  $week);
if (!-e $file) {
  print "# No Injury Report found ($season, $game_type, $week)\n";
  exit;
}

# Load the injury report
print "##\n## Loading injury report: $file\n##\n";
my $contents;
open FILE, "gzip -dc $file |";
my @contents = <FILE>;
close FILE;
$contents = join('', @contents);
@contents = (); # Garbage collect...

# Debug catching
print STDERR "Script is in debug mode, configured to run against the roster files for '$test_team_id' only.\n" if $test_team_id ne '';

# Reset a previous run
print "\nDELETE FROM SPORTS_NFL_PLAYERS_INJURIES WHERE season = '$season' AND game_type = '$game_type' AND week = '$week';\n\n";

# Convert instances where no players are listed to a parsable version
$contents =~ s/<section class="d3-l-grid--outer d3-l-section-row nfl-o-no-results">.*?<\/section>/<div><table NO PLAYERS<\/table>/gsi;

# Extract the injury list by team
my @raw = ($contents =~ m/<div class="d3-o-section-sub-title">\s*<span>(.*?)<\/span>\s*<\/div>\s*<\/div>\s*<div[^>]*>\s*<table (.*?)<\/table>/gsi);
my %list = ();
while (my ($franchise, $list) = splice(@raw, 0, 2)) {
  my ($team_id) = ($contents =~ m/<span class="nfl-c-matchup-strip__team-abbreviation">\s*(\S+)\s*<\/span>\s*<a class="nfl-c-matchup-strip__team-fullname"\s*href="\/teams\/[^>]+>\s*$franchise\s*<\/a>/msi);
  $team_id = convert_team_id($team_id);
  $list{$team_id} = $list;
}

# Get the list of injuries (not just those with practice designations)
foreach my $team_id (sort keys %list) {
  next if $test_team_id && $team_id ne $test_team_id; # If debugging, we may be limiting our initial runs
  print "##\n## $team_id\n##\n\n";

  my @matches = ($list{$team_id} =~ m/<tr>\s*<td[^>]*>\s*<a href="\/players\/([^\/]+)\/".*?>(.*?)<\/a>\s*<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<td[^>]*>.*?<\/td>\s*<td[^>]*>(.*?)<\/td>\s*<\/tr>/gsi);
  while (my ($remote_id, $player, $injury, $status) = splice(@matches, 0, 4)) {
    # Skip if only a player practice line
    trim(\$status);
    next if $status eq '';
    # Tidy the display values
    trim(\$player);
    trim(\$injury);

    # Force an injury label if none set
    $injury = '(Unknown)' if $injury eq '';

    # Manipulate before storing
    $remote_id = convert_text($remote_id);
    my $injury_db;
    if ($injury eq '--') {
      $injury_db = 'NULL';
    } else {
      $injury =~ s/Not injury related \- //;
      $injury =~ s/\. .+$//;
      $injury =~ s/ \[.+\]//;
      $injury =~ s/(, )([a-z])/$1\U$2\E/g;
      $injury = ucfirst($injury);
      $injury = convert_text($injury) if $injury !~ /&/;
      $injury_db = "'$injury'";
    }
    my $status_db = parse_status($status);

    print "# $player ($remote_id), $injury, $status\n";
    print "INSERT INTO `SPORTS_NFL_PLAYERS_INJURIES` (`season`, `game_type`, `week`, `player_id`, `team_id`, `status`, `injury`)
  SELECT '$season', '$game_type', '$week', `player_id`, '$team_id', '$status_db', $injury_db
  FROM `SPORTS_NFL_PLAYERS_IMPORT`
  WHERE `remote_url_name_v2` = '$remote_id';\n\n";
  }
}

print "##\n## Maintenance\n##\nALTER TABLE `SPORTS_NFL_PLAYERS_INJURIES` ORDER BY `season`, `game_type`, `week`, `team_id`, `player_id`;\n\n";

#
# Convert a status from NFL.com's version to ours
#
sub parse_status {
  my ($status) = @_;
  return substr($status, 0, 1) if $status =~ m/^probable|questionable|doubtful|out$/i;
  # If we're here, this is an unknown status
  print STDERR "Unknown status '$status'\n";
  return 'unknown';
}
