#!/usr/bin/perl -w
# Download, import and process team rosters from NFL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'rosters';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my @time = localtime();
my $season = ($time[4] > 3 ? 1900 : 1899) + $time[5];

# Identify week to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT game_type, week
FROM SPORTS_NFL_SCHEDULE_WEEKS
WHERE season = ?
AND   CURDATE() BETWEEN start_date AND end_date;';
my $sth = $dbh->prepare($sql);
$sth->execute($season);
my ($game_type, $week) = $sth->fetchrow_array;

undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);

# If nothing returned, we are after the initial rosters
if (!defined($game_type)) {
  $game_type = 'initial';
  $week = '';
}

## Inform user
print "\nAcquiring data from $season, $game_type // $week\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Download, if we haven't done so already (handled in-script)
#
print '=> Download: ';
command("$config{'base_dir'}/rosters/download.pl $season $game_type $week",
        "$config{'log_dir'}/$logs{'roster_d'}.log",
        "$config{'log_dir'}/$logs{'roster_d'}.err");
done(0);

# If only downloading, go no further
end_script() if download_only();

#
# Parse
#
print '=> Parse: ';
command("$config{'base_dir'}/rosters/parse.pl $season $game_type $week",
        "$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_p'}.err");
done(3);

#
# Import into the database
#
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'roster_p'}.sql",
        "$config{'log_dir'}/$logs{'roster_i'}.log",
        "$config{'log_dir'}/$logs{'roster_i'}.err");
done(2);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'roster_d'  => '01_roster_download',
    'roster_p'  => '02_roster_parse',
    'roster_i'  => '03_roster_import',
  );
}
