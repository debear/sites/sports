#!/usr/bin/perl -w
# Download and import schedules from NFL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'schedule';
my %logs = get_log_def();

# Guesstimate then ask for the season
my @time = localtime();
my $season_guess = ($time[4] > 3 ? 1900 : 1899) + $time[5];
my $season = question_user("Please enter a season in YYYY format [$season_guess]", '\d{4}', $season_guess);

# Which game types?
my @mode = ();
my $q = question_user("Regular season? [Yn]", '[yn]', 'y');
push @mode, '--regular' if $q eq 'y';
$q = question_user("Playoffs? [yN]", '[yn]', 'n');
push @mode, '--playoff' if $q eq 'y';
my $mode = join(' ', @mode);

# Inform user
print "\nAcquiring data from $season, $mode game types\n";
print 'Run started at ' . `date` . "\n";

# Idenfity the log file
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

# Download, if we haven't done so already (handled in-script)
print '=> Download: ';
command("$config{'base_dir'}/schedule/download.pl $season $mode",
        "$config{'log_dir'}/$logs{'download'}.log",
        "$config{'log_dir'}/$logs{'download'}.err");
done(0);

# If only downloading, go no further
end_script() if download_only();

# Parse
print '=> Parse: ';
command("$config{'base_dir'}/schedule/parse.pl $season $mode",
        "$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'parse'}.err");
done(3);

# Import
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'import'}.log",
        "$config{'log_dir'}/$logs{'import'}.err");
done(2);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'parse'    => '02_parse',
    'import'   => '03_import',
  );
}
