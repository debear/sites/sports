#!/usr/bin/perl -w
# Perl config file

our $season; our $game_type; our $week;
$config{'data_dir'} = "$config{'base_dir'}/_data/$season/rosters/$game_type";
$config{'data_dir'} .= "/$week" if $week ne '';

$config{'mugshot_src'} = 'nfl'; # 'nfl' or 'team' roster files

# Team Sites
%{$config{'team_sites'}} = (
  'ARI' => 'https://www.azcardinals.com',
  'ATL' => 'https://www.atlantafalcons.com',
  'BAL' => 'https://www.baltimoreravens.com',
  'BUF' => 'https://www.buffalobills.com',
  'CAR' => 'https://www.panthers.com',
  'CHI' => 'https://www.chicagobears.com',
  'CIN' => 'https://www.bengals.com',
  'CLE' => 'https://www.clevelandbrowns.com',
  'DAL' => 'https://www.dallascowboys.com',
  'DEN' => 'https://www.denverbroncos.com',
  'DET' => 'https://www.detroitlions.com',
  'GB'  => 'https://www.packers.com',
  'HOU' => 'https://www.houstontexans.com',
  'IND' => 'https://www.colts.com',
  'JAX' => 'https://www.jaguars.com',
  'KC'  => 'https://www.chiefs.com',
  'LA'  => 'https://www.therams.com',
  'LAC' => 'https://www.chargers.com',
  'LV'  => 'https://www.raiders.com',
  'MIA' => 'https://www.miamidolphins.com',
  'MIN' => 'https://www.vikings.com',
  'NE'  => 'https://www.patriots.com',
  'NO'  => 'https://www.neworleanssaints.com',
  'NYG' => 'https://www.giants.com',
  'NYJ' => 'https://www.newyorkjets.com',
  'PHI' => 'https://www.philadelphiaeagles.com',
  'PIT' => 'https://www.steelers.com',
  'SEA' => 'https://www.seahawks.com',
  'SF'  => 'https://www.49ers.com',
  'TB'  => 'https://www.buccaneers.com',
  'TEN' => 'https://www.tennesseetitans.com',
  'WSH' => 'https://www.commanders.com',
);

# Return true to pacify the compiler
1;
