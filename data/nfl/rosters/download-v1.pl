#!/usr/bin/perl -w
# Get player info for players on current NFL.com rosters

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use File::Path;
use DBI;

# Validate the arguments
#  - there needs to be at least two: a season and game_type
#  - there could also be an additional argument though:  week
if (@ARGV < 2 || $ARGV[0] !~ m/^\d{4}$/ || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff', 'initial' ))) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $game_type, $week) = @ARGV;
$week = '' if !defined($week);
print "#\n# Downloading rosters for '$season': '$game_type' // '$week'\n#\n";

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Create our data directory
mkpath($config{'data_dir'})
  if ! -e $config{'data_dir'};

# First, get the main "By Team Roster" page that will give us the appropriate codes to scan for individual teams
my $team_list = download('http://nflcdns.nfl.com/players/search?category=team&playerType=current');

# Connect to the database, and prepare the statement we'll use to get a team's team_id
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $team_sth = $dbh->prepare('SELECT TEAM.team_id
FROM SPORTS_NFL_TEAMS AS TEAM
JOIN SPORTS_NFL_TEAMS_GROUPINGS AS GROUPING
  ON (GROUPING.team_id = TEAM.team_id
  AND GROUPING.season_from <= ?
  AND IFNULL(GROUPING.season_to, 2099) >= ?)
WHERE LOWER(CONCAT(TEAM.city, " ", TEAM.franchise)) = LOWER(?);');

# Parse this for the list of teams
my ($full_list) = ($team_list =~ m/<select name="filter" class="teamroster"[^>]*>(.*?)<\/select>/si);
my @list = ($full_list =~ m/<option value="(\d+)">(.*?)<\/option>/gsi);
while (my ($id, $name) = splice(@list, 0, 2)) {
  # Get the team_id from our database
  $name =~ s/[^A-Z0-9 ]//gi;
  $team_sth->execute($season, $season, $name);
  my ($team_id) = $team_sth->fetchrow_array;
  print "# $name ($team_id, $id)\n";

  # Form the URL to parse
  my $url = "http://nflcdns.nfl.com/players/search?category=team&filter=$id&playerType=current"; my $i = 0;
  my @url_list = ( $url );
  foreach $url ( @url_list ) {
    my $local = "$config{'data_dir'}/$team_id" . ($i ? ".$i" : '') . '.htm.gz';
    print "## From: $url\n";
    print "## To:   $local\n";

    # Download and open
    download($url, $local, {'skip-today' => 1, 'gzip' => 1});
    my $roster;
    open FILE, "gzip -dc $local |";
    my @contents = <FILE>;
    close FILE;
    $roster = join('', @contents);

    # Is there another page to download from?
    if ($roster =~ m/<a href="\/players\/search\?category=team&amp;playerType=current&amp;d-\d+-p=\d+&amp;filter=$id">next<\/a>/si) {
      my ($next_url) = ($roster =~ m/<a href="(\/players\/search\?category=team&amp;playerType=current&amp;d-\d+-p=\d+&amp;filter=$id)">next<\/a>/si);
      push @url_list, decode_entities("http://nflcdns.nfl.com$next_url");
    }
    print "\n";
    $i++;
  }

  # If multiple, merge together...
  if ($i > 1) {
    print "Multiple files found. Probably should merge them together......\n";
  }
}

# Disconnect from the database
undef $team_sth if defined($team_sth);
$dbh->disconnect if defined($dbh);
