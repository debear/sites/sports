#!/usr/bin/perl -w
# Get player info for players on current NFL rosters from the team sites and the (unofficial) team depth charts

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use File::Path;
use DBI;

# Validate the arguments
#  - there needs to be at least two: a season and game_type
#  - there could also be an additional argument though:  week
if (@ARGV < 2 || $ARGV[0] !~ m/^\d{4}$/ || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff', 'initial' ))) {
  print STDERR "Insufficient arguments to the get roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $game_type, $week) = @ARGV;
$week = '' if !defined($week);
print "#\n# Downloading rosters for '$season': '$game_type' // '$week'\n#\n\n";

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Create our data directory
mkpath($config{'data_dir'})
  if ! -e $config{'data_dir'};

# Connect to the database, and run the statement we'll use to get the teams to download
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $team_sth = $dbh->prepare('SELECT REPLACE(LOWER(CONCAT(TEAM.city, " ", TEAM.franchise)), " ", "-") AS url,
       TEAM.city, TEAM.franchise, TEAM.team_id
FROM SPORTS_NFL_TEAMS AS TEAM
JOIN SPORTS_NFL_TEAMS_GROUPINGS AS TEAM_GROUPING
  ON (TEAM_GROUPING.team_id = TEAM.team_id
  AND TEAM_GROUPING.season_from <= ?
  AND IFNULL(TEAM_GROUPING.season_to, 2099) >= ?);');
$team_sth->execute($season, $season);
while (my $team = $team_sth->fetchrow_hashref) {
  print "## $$team{'city'} $$team{'franchise'}\n";

  # Roster from NFL.com
  my $url = "https://www.nfl.com/teams/$$team{'url'}/roster";
  my $local = "$config{'data_dir'}/$$team{'team_id'}-nfl.htm.gz";
  print "# NFL.com Roster: $url to $local\n";
  download($url, $local, {'skip-today' => 1, 'gzip' => 1});

  # Roster from team's site
  $url = "$config{'team_sites'}{$$team{'team_id'}}/team/players-roster/";
  $local = "$config{'data_dir'}/$$team{'team_id'}-team.htm.gz";
  print "# Team Roster:    $url to $local\n";
  download($url, $local, {'skip-today' => 1, 'gzip' => 1});

  # Depth chart from team's site
  $url = "$config{'team_sites'}{$$team{'team_id'}}/team/depth-chart";
  $local = "$config{'data_dir'}/$$team{'team_id'}-depth.htm.gz";
  print "# Depth Chart:    $url to $local\n";
  download($url, $local, {'skip-today' => 1, 'gzip' => 1});

  print "\n";
}

# Disconnect from the database
undef $team_sth if defined($team_sth);
$dbh->disconnect if defined($dbh);
