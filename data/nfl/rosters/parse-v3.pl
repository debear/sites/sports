#!/usr/bin/perl -w
# Loop through and parse team rosters, and any new players found

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use DBI;

# Validate the arguments
#  - there needs to be at least two: a season and game_type
#  - there could also be an additional argument though:  week
if (@ARGV < 2 || $ARGV[0] !~ m/^\d{4}$/ || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff', 'initial' ))) {
  print STDERR "Insufficient arguments to the parse roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $game_type, $week) = @ARGV;
$week = '' if !defined($week);
print "#\n# Parsing rosters for '$season': '$game_type' // '$week'\n#\n";
my $dry_run = grep(/^--dry-run$/, @ARGV);
my $test_team_id;# = '';

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Now we've set the data_dir, change $game_type and $week to be the versions we want to store in the database
if ($game_type eq 'initial') {
  $game_type = 'regular';
  $week = 1;
}

# Get the list of teams
my $err = 0;
my @roster_files = glob "$config{'data_dir'}/*.htm.gz" or $err = 1;
if ($err) {
  print STDERR "No roster files found?\n";
  exit 10;
}

# Clear a previous run
print "# Clearing previous instance\n";
print "DELETE FROM SPORTS_NFL_TEAMS_ROSTERS WHERE season = '$season' AND game_type = '$game_type' AND week >= '$week';\n\n";

# Prepare our database queries
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $id_via_url_sth = $dbh->prepare('SELECT player_id FROM SPORTS_NFL_PLAYERS_IMPORT WHERE remote_url_name_v2 = ?;');
my $id_via_roster_sth = $dbh->prepare('SELECT PL.player_id
FROM SPORTS_NFL_PLAYERS AS PL
LEFT JOIN SPORTS_NFL_TEAMS_ROSTERS AS ROS
  ON (ROS.season = ?
  AND ROS.team_id = ?
  AND ROS.player_id = PL.player_id
  AND ROS.pos = ?)
WHERE CONCAT(PL.first_name, " ", PL.surname) = ?
AND   IFNULL(PL.height, "0") BETWEEN (IFNULL(?, "1") - 1) AND (IFNULL(?, "0") + 1)
AND   IFNULL(PL.college, "No College") = IFNULL(?, "No College")
GROUP BY PL.player_id, ROS.team_id
ORDER BY ROS.team_id <> ?
LIMIT 1;');
my $new_sth = $dbh->prepare('SELECT MAX(player_id) + 1 AS new_player_id FROM SPORTS_NFL_PLAYERS;');
my $ins_sth = $dbh->prepare('INSERT INTO SPORTS_NFL_PLAYERS_IMPORT (player_id, remote_url_name_v2, profile_imported) VALUES (?, ?, NULL);');
my $ply_sth = $dbh->prepare('INSERT INTO SPORTS_NFL_PLAYERS (player_id, first_name, surname, height, weight, college) VALUES (?, ?, ?, ?, ?, ?);');

# As we have to manually split names, keep a list of those to be checked
my @verify_name_split = ( );

#
# Pass 1: Process the team roster files into a hash of players
#
my %team_roster = ( );
foreach my $team_file (grep(/-team/, @roster_files)) {
  # Get the team_id from the filename
  my ($team_id) = ($team_file =~ m/$config{'data_dir'}\/([A-Z]+)(?:\.\d)?\-team\.htm\.gz/si);
  next if defined($test_team_id) && $team_id ne $test_team_id; # If debugging, we may be limiting our initial runs
  %{$team_roster{$team_id}} = ( );

  # Load the file
  my $roster;
  open FILE, "gzip -dc $team_file |";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);
  # Apply a data fix to simplify the regex later
  $roster =~ s/<td([^>]*)><span>([\dR]+)<\/span><\/td>/<td$1>$2<\/td>/g;

  # The pattern for identifying players within a table
  my $pattern = '<tr>\s*<td[^>]*>\s*<div class="d3-o-media-object">\s*<figure class="d3-o-media-object__figure">\s*(.*?)<\/figure>\s*.*?data-name="([^"]+)"><a href=[^>]+>(.*?)<\/a>\s*<\/[^>]+>\s*<\/div>\s*<\/td>\s*<td[^>]*>((?:\d|\-\-?)*)<\/td>\s*<td[^>]*>([^<]+)<\/td>\s*<td[^>]*>([^<]+)<\/td>\s*<td[^>]*>(\d*)<\/td>\s*<td[^>]*>[^<]*<\/td>\s*<td[^>]*>[^<]*<\/td>\s*<td[^>]*>([^<]*)<\/td>\s*<\/tr>';

  # Loop through the various tables
  my @tables = ($roster =~ m/<span class="nfl-o-roster__title-status">(.*?)<\/span>.*?<table(.*?)<\/table>/gsi);
  while (my ($status_raw, $table) = splice(@tables, 0, 2)) {
    my $status_db;
    # In some instances, team tables include an NFL.com-style status code, so if that is the case process by that logic
    if ($status_raw !~ /^[A-Z]{3}$/) {
      # Format the raw status into our database version (this is our favourable route)
      $status_db = map_status_team($status_raw);
      if (!defined($status_db)) {
        # An unknown status, so flag and skip
        print STDERR "Unknown status table found: '$status_raw'\n";
        next;
      }

    } else {
      $status_db = map_status_nfl($status_raw);
      # Skip a player with an unhandled NFL.com roster status. We do so here, as this removes otherwise duplicated players listed across multiple teams
      if (!defined($status_db)) {
        print "# Skipping table due to NFL.com-style status '$status_raw'\n";
        next;
      }
    }

    my @matches = ($table =~ m/$pattern/gsi);
    while (my ($img, $player_sort, $full_name, $jersey, $pos, $height, $weight, $college) = splice(@matches, 0, 8)) {
      # Try and get a remote ID from the possible player tag link
      my ($remote_url) = ($img =~ m/href="\/team\/players-roster\/([^\/]+)\/"/);
      # Get (if available) the mugshot filename reference
      my ($remote_mugshot) = ($img =~ m/\/t_lazy\/f_.*?\/([^\/ "]*?)(?:\.\w{3})?[ "]/si);

      # Split the name into component parts
      $full_name = encode_entities(decode_entities($full_name)); # Standardise the encoding
      my ($sort_sname) = ($player_sort =~ m/^([^,]+),/gsi);
      my ($fname, $sname) = ($full_name =~ m/^(.+) ($sort_sname.*?)$/gsi);

      # If a player has multiple positions listed (e.g., G/C or FB/TE) then take the first listed position
      $pos =~ s/\/.+$//g if $pos =~ /\//;

      # Add to our array of players from the team roster
      my $remote_url_key = $remote_url; $remote_url_key =~ s/\-(?:i+|jr)$//i;
      my %player = (
        'remote_mugshot' => $remote_mugshot,
        'full_name' => $full_name,
        'first_name' => $fname,
        'surname' => $sname,
        'status' => $status_db,
        'jersey' => $jersey,
        'pos' => $pos,
        'height' => $height,
        'weight' => $weight,
        'college' => $college,
      );
      # Add to the list... unless we have an existing name match
      if (!defined($team_roster{$team_id}{$remote_url_key})) {
        %{$team_roster{$team_id}{$remote_url_key}} = %player;
      } else {
        # We have multiple players to merge together
        # Note: This logic may not be perfect, but matches the presenting problem faced
        if (defined($team_roster{$team_id}{$remote_url_key}{'full_name'})) {
          # Convert our old hash into the "multiple player" format
          my %existing = %{$team_roster{$team_id}{$remote_url_key}};
          my $existing_key = $existing{'pos'} . ':' . $existing{'jersey'};
          %{$team_roster{$team_id}{$remote_url_key}} = (
            $existing_key => \%existing,
          );
        }
        my $player_key = $player{'pos'} . ':' . $player{'jersey'};
        %{$team_roster{$team_id}{$remote_url_key}{$player_key}} = %player;
      }
    }
  }
}

#
# Pass 2: The initial source (with what should be unique reference across all teams), the rosters from NFL.com
#
foreach my $team_file (grep(/-nfl/, @roster_files)) {
  # Get the team_id from the filename
  my ($team_id) = ($team_file =~ m/$config{'data_dir'}\/([A-Z]+)(?:\.\d)?\-nfl\.htm\.gz/si);
  next if defined($test_team_id) && $team_id ne $test_team_id; # If debugging, we may be limiting our initial runs
  print "##\n## $team_id\n##\n";

  # Load the file
  my $roster;
  open FILE, "gzip -dc $team_file |";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);

  # Get the list of players out
  my $pattern = '<tr>\s*<td[^>]*>\s*<div class="d3-o-media-object">\s*<figure class="d3-o-media-object__figure">\s*(.*?)<\/figure>\s*<(?:a|span) ([^>]+)>(.*?)<\/[^>]+>\s*<\/div>\s*<\/td>\s*<td[^>]*>(\d*)<\/td>\s*<td[^>]*>([^<]+)<\/td>\s*<td[^>]*>([^<]+)<\/td>\s*<td[^>]*>(\d*)<\/td>\s*<td[^>]*>(\d*)<\/td>\s*<td[^>]*>[^<]*<\/td>\s*<td[^>]*>([^<]*)<\/td>\s*<\/tr>';
  my @matches = ($roster =~ m/$pattern/gsi);

  # Parse each player, creating where required, and using info from the team roster (if a match can be found)
  while (my ($img, $player_tag, $full_name, $jersey, $pos, $status_raw, $height, $weight, $college) = splice(@matches, 0, 9)) {
    print "\n";
    # Define the additional variables we could be using from the team roster
    my $remote_mugshot; my $first_name; my $surname;
    # Convert the status code in to our version
    my $status_db = map_status_nfl($status_raw);

    # Skip a player with an unhandled NFL.com roster status. We do so here, as this removes otherwise duplicated players listed across multiple teams
    if (!defined($status_db)) {
      print "# Skipping player '$full_name, $pos' due to status '$status_raw'\n";
      next;
    }

    # Try and get a remote ID from the possible player tag link
    my ($remote_url) = ($player_tag =~ m/href="\/players\/([^\/]+)\/"/);
    # Determine if we have a team roster match, and override if so
    my $remote_url_key = (defined($remote_url) ? $remote_url : ''); $remote_url_key =~ s/\-(?:\d+|i+|jr)$//i;
    my $team_match = defined($team_roster{$team_id}{$remote_url_key});
    if ($team_match) {
      my %player = %{$team_roster{$team_id}{$remote_url_key}};

      # We may have multiple players listed, so try and match if so
      %player = %{$player{"$pos:$jersey"}}
        if (!defined($player{'full_name'}));

      # Now override the variables
      $remote_mugshot = $player{'remote_mugshot'};
      $full_name = $player{'full_name'};
      $first_name = $player{'first_name'};
      $surname = $player{'surname'};
      $jersey = $player{'jersey'};
      $pos = $player{'pos'};
      $status_db = $player{'status'};
      $height = $player{'height'};
      $weight = $player{'weight'};
      $college = $player{'college'};
    }

    # Some column tidying
    $jersey = 0 if $jersey =~ /^(\-\-?|)$/;
    $height = undef if $height eq '';
    if (defined($height) && $height !~ m/^\d+$/) {
      my ($height_ft, $height_in) = ($height =~ m/^(\d+)-(\d+)$/gsi);
      $height = ($height_ft * 12) + $height_in;
    }
    $weight = undef if $weight eq '';
    $college = undef if $college eq '';

    # Logging the info found
    print "# '$full_name', $pos #$jersey (Mugshot: " . (defined($remote_mugshot) ? $remote_mugshot : '(undef)') . "; URL: " . (defined($remote_url) ? $remote_url : '(undef)') . ")\n";
    print "# -> Details updated from team roster\n" if $team_match;

    # Split the name (plus warn if non-deterministic) if not already split within the roster
    if (!defined($first_name)) {
      my $space_pos = index($full_name, ' ');
      $first_name = substr($full_name, 0, $space_pos);
      $surname = substr($full_name, $space_pos + 1);
      print "# -> Manual name split required ('$full_name' -> '$first_name' '$surname')\n";
      # If the split surname includes additional spaces, add to list for manual verification
      # - There are some programmatic checks we can use for verification though
      if ((index($surname, ' ') != -1) && ($surname !~ m/ jr\.?$/si) && ($surname !~ m/ i+$/si) && ($surname !~ m/^van/si)) {
        print "# -> Name split requires manual verification\n";
        push @verify_name_split, "$team_id: '$full_name' -> '$first_name' '$surname'";
      }
    }

    # Get the player info we have for this player
    my $player_id;
    if (defined($remote_url)) {
      # Attempt 1: Use the remote_url as an ID
      $id_via_url_sth->execute($remote_url);
      ($player_id) = $id_via_url_sth->fetchrow_array;
      print "# -> Matched from existing URL: $player_id\n" if defined($player_id);
    }
    if (!defined($player_id)) {
      # Attempt 2: Check we haven't already imported this player before via player details
      $id_via_roster_sth->execute($season, $team_id, $pos, $full_name, $height, $height, $college, $team_id);
      ($player_id) = $id_via_roster_sth->fetchrow_array;
      print "# -> Matched from existing player: $player_id\n" if defined($player_id);
    }

    # If no existing player record found, get the appropriate database fields from the player card and create
    if (!defined($player_id)) {
      # Get the new player ID
      $new_sth->execute;
      ($player_id) = $new_sth->fetchrow_array;
      print "# -> Creating as new player with ID '$player_id'\n";

      # Store the record in the import table
      $ins_sth->execute($player_id, $remote_url) if !$dry_run && defined($remote_url);

      # Create the player record
      $college = encode_entities(decode_entities($college)) if defined($college); # Standardise the encoding
      $ply_sth->execute($player_id, $first_name, $surname, $height, $weight, $college) if !$dry_run;
    }

    # Output the SQL
    print "INSERT IGNORE INTO SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS (player_id, remote_mugshot, first_seen) VALUES ('$player_id', '$remote_mugshot', NOW());\n"
      if defined($remote_mugshot);
    print "INSERT INTO SPORTS_NFL_TEAMS_ROSTERS (season, game_type, week, team_id, player_id, jersey, pos, player_status) VALUES ('$season', '$game_type', '$week', '$team_id', '$player_id', $jersey, '$pos', '$status_db');\n";
  }
  print "\n";
}

# If we have names to check, do so via an email
if (@verify_name_split) {
  # Prepare the query and execute
  my $dbh_common = DBI->connect('dbi:mysql:' . $config{'db_common'}, $config{'db_user'}, $config{'db_pass'});
  my $name_email_sth = $dbh_common->prepare('INSERT INTO COMMS_EMAIL (app, email_id, email_reason, email_from, email_to, email_subject, email_body, email_queued, email_send, email_status) VALUES (?, NULL, ?, CONCAT("DeBear Support <support@", ?, ">"), CONCAT("DeBear Support <support@", ?, ">"), ?, ?, NOW(), NOW(), "queued");');
  $name_email_sth->execute(
    'sports_nfl',
    'player_name_check',
    $config{'domain'},
    $config{'domain'},
    'NFL Player Name Split Verification',
    "During today's run of the NFL Roster script, the following players were created and need to be checked to ensure the name was split correctly:\n- " . join("\n- ", @verify_name_split)
  );
  undef $name_email_sth if defined($name_email_sth);
  $dbh_common->disconnect if defined($dbh_common);
}

# For regular season parsing, also pad to the end of the season
my $reg_weeks = ($season < 2021 ? 17 : 18);
if ($game_type eq 'regular' && $week < $reg_weeks) {
  print "# Padding from Week " . ($week+1) . " to Week $reg_weeks\n";
  for (my $w = $week + 1; $w <= $reg_weeks; $w++) {
    print "INSERT INTO SPORTS_NFL_TEAMS_ROSTERS (season, game_type, week, team_id, player_id, jersey, pos, player_status)
  SELECT season, game_type, '$w' AS week, team_id, player_id, jersey, pos, player_status
  FROM SPORTS_NFL_TEAMS_ROSTERS
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   week = '$week';\n";
  }
  print "\n";
}

print "# Tidy\n";
print "ALTER TABLE SPORTS_NFL_TEAMS_ROSTERS ORDER BY season, game_type, week, team_id, player_id;\n";

# Disconnect from the database
undef $id_via_url_sth if defined($id_via_url_sth);
undef $id_via_roster_sth if defined($id_via_roster_sth);
undef $new_sth if defined($new_sth);
undef $ins_sth if defined($ins_sth);
undef $ply_sth if defined($ply_sth);
$dbh->disconnect if defined($dbh);

# Map the raw status from the team roster file
sub map_status_team {
  my ($status_raw) = @_;

  if ($status_raw eq 'Active') {
    return 'active';
  } elsif ($status_raw =~ /^Practice Squad/) {
    return 'ps';
  } elsif ($status_raw =~ /^Reserve\/(Designated to Return|Injured; Designated for Return)/) {
    return 'ir-r';
  } elsif ($status_raw eq 'Reserve/Injured') {
    return 'ir';
  } elsif ($status_raw =~ /^Reserve\/Non-Football I/) {
    return 'nfi';
  } elsif ($status_raw eq 'Reserve/Physically Unable to Perform') {
    return 'pup';
  } elsif ($status_raw eq 'Reserve/Suspended by Commissioner') {
    return 'susp';
  } elsif ($status_raw =~ /^Reserve\//) {
    return 'na';
  }

  # An unknown status, so return an appropriate value
  return undef;
}

# Map the raw status from the NFL.com roster file
sub map_status_nfl {
  my ($status_raw) = @_;

  if ($status_raw eq 'ACT') {
    return 'active';
  } elsif ($status_raw eq 'RES') {
    return 'ir';
  } elsif ($status_raw eq 'NON') {
    return 'nfi';
  } elsif ($status_raw eq 'SUS') {
    return 'susp';
  } elsif ($status_raw eq 'PUP') {
    return 'pup';
  } elsif ($status_raw eq 'DEV') {
    return 'ps';
  } elsif ($status_raw eq 'EXE') {
    return 'na';
  }

  # An unknown status, so return an appropriate value
  return undef;
}
