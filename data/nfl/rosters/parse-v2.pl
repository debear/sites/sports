#!/usr/bin/perl -w
# Loop through and parse team rosters, and any new players found

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use DBI;

# Validate the arguments
#  - there needs to be at least two: a season and game_type
#  - there could also be an additional argument though:  week
if (@ARGV < 2 || $ARGV[0] !~ m/^\d{4}$/ || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff', 'initial' ))) {
  print STDERR "Insufficient arguments to the parse roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $game_type, $week) = @ARGV;
$week = '' if !defined($week);
print "#\n# Parsing rosters for '$season': '$game_type' // '$week'\n#\n";

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Now we've set the data_dir, change $game_type and $week to be the versions we want to store in the database
if ($game_type eq 'initial') {
  $game_type = 'regular';
  $week = 1;
}

# Get the list of teams
my $err = 0;
my @list = glob "$config{'data_dir'}/*.htm.gz" or $err = 1;
if ($err) {
  print STDERR "No roster files found?\n";
  exit 10;
}

# Clear a previous run
print "# Clearing previous instance\n";
print "DELETE FROM SPORTS_NFL_TEAMS_ROSTERS WHERE season = '$season' AND game_type = '$game_type' AND week >= '$week';\n\n";

# Prepare our database queries
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sel_sth = $dbh->prepare('SELECT player_id FROM SPORTS_NFL_PLAYERS_IMPORT WHERE remote_url_name_v2 = ?;');
my $from_curr_sth = $dbh->prepare('SELECT PL.player_id
FROM SPORTS_NFL_PLAYERS AS PL
LEFT JOIN SPORTS_NFL_TEAMS_ROSTERS AS ROS
  ON (ROS.season = ?
  AND ROS.team_id = ?
  AND ROS.player_id = PL.player_id
  AND ROS.pos = ?)
WHERE CONCAT(PL.first_name, " ", PL.surname) = ?
AND   IFNULL(PL.height, "_NULL_ENTRY_") = IFNULL(?, "_NULL_ENTRY_")
AND   IFNULL(PL.weight, "_NULL_ENTRY_") = IFNULL(?, "_NULL_ENTRY_")
AND   IFNULL(PL.college, "No College") = IFNULL(?, "No College")
GROUP BY PL.player_id, ROS.team_id
ORDER BY ROS.team_id <> ?
LIMIT 1;');
my $new_sth = $dbh->prepare('SELECT MAX(player_id) + 1 AS new_player_id FROM SPORTS_NFL_PLAYERS;');
my $ins_sth = $dbh->prepare('INSERT INTO SPORTS_NFL_PLAYERS_IMPORT (player_id, remote_url_name_v2, profile_imported) VALUES (?, ?, NULL);');
my $ply_sth = $dbh->prepare('INSERT INTO SPORTS_NFL_PLAYERS (player_id, first_name, surname, height, weight, college) VALUES (?, ?, ?, ?, ?, ?);');

# As we have to manually split names, keep a list of those to be checked
my @name_split_check = ( );

# Loop through the teams
foreach my $file (@list) {
  # Get the team_id from the filename
  my ($team_id) = ($file =~ m/$config{'data_dir'}\/([A-Z]+)(?:\.\d)?\.htm\.gz/si);
  print "##\n## $team_id ($file)\n##\n";

  # Load the file
  my $roster;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);

  # Get the list of players out
  my $pattern = '<tr>\s*<td[^>]*>\s*<div class="d3-o-media-object">\s*<figure class="d3-o-media-object__figure">\s*(.*?)<\/figure>\s*<(?:a|span) ([^>]+)>(.*?)<\/[^>]+>\s*<\/div>\s*<\/td>\s*<td[^>]*>(\d*)<\/td>\s*<td[^>]*>([^<]+)<\/td>\s*<td[^>]*>([^<]+)<\/td>\s*<td[^>]*>(\d*)<\/td>\s*<td[^>]*>(\d*)<\/td>\s*<td[^>]*>[^<]*<\/td>\s*<td[^>]*>([^<]*)<\/td>\s*<\/tr>';
  my @matches = ($roster =~ m/$pattern/gsi);

  # Display each player, creating where required
  while (my ($img, $player_tag, $full_name, $jersey, $pos, $status, $height, $weight, $college) = splice(@matches, 0, 9)) {
    print "\n";
    # Convert the status code in to our version
    my $db_status;
    if ($status eq 'ACT') {
      $db_status = 'active';
    } elsif ($status eq 'RES') {
      $db_status = 'ir';
    } elsif ($status eq 'NON') {
      $db_status = 'nfi';
    } elsif ($status eq 'SUS') {
      $db_status = 'susp';
    } elsif ($status eq 'PUP') {
      $db_status = 'pup';
    } elsif ($status eq 'DEV') {
      $db_status = 'ps';
    } elsif ($status eq 'EXE') {
      $db_status = 'na';
    } else {
      # Skip a player with an unhandled status
      print "# Skipping player '$full_name, $pos' due to status '$status'\n";
      next;
    }
    print "# $full_name, $pos - $db_status\n";

    # Try and get a remote ID from the possible player tag link
    my ($remote_url) = ($player_tag =~ m/href="\/players\/([^\/]+)\/"/);

    # Get (if available) the mugshot filename reference
    my ($remote_mugshot) = ($img =~ m/\/t_lazy\/f_auto\/league\/([^" ]+)"/si);

    # Some additional column tidying
    $jersey = 0 if $jersey eq '';
    $height = undef if $height eq '';
    $weight = undef if $weight eq '';
    $college = undef if $college eq '';

    # Get the player info we have for this player
    my $player_id;
    if (defined($remote_url)) {
      # Get the player_id from the database
      $sel_sth->execute($remote_url);
      ($player_id) = $sel_sth->fetchrow_array;

    } else {
      # If we have no remote ID, check we haven't already imported this player before
      $from_curr_sth->execute($season, $team_id, $pos, $full_name, $height, $weight, $college, $team_id);
      ($player_id) = $from_curr_sth->fetchrow_array;
      print "# -> Matched without remote_url from existing player: $player_id\n"
        if defined($player_id);
    }

    # If no existing player record found, get the appropriate database fields from the player card and create
    if (!defined($player_id)) {
      # Get the new player ID
      $new_sth->execute;
      ($player_id) = $new_sth->fetchrow_array;
      print "# -> Creating as new player with ID '$player_id'\n";

      # Store the record in the import table
      $ins_sth->execute($player_id, $remote_url);

      # Create the player record
      # Split the name into component parts
      my $space_pos = index($full_name, ' ');
      my $fname = substr($full_name, 0, $space_pos);
      my $sname = substr($full_name, $space_pos + 1);
      # If name split includes additional spaces, add to list for manual verification
      # - There are some programmatic checks we can use for verification though
      if ((index($sname, ' ') != -1) && ($sname !~ m/ jr\.?$/si) && ($sname !~ m/ i+$/si) && ($sname !~ m/^van/si)) {
        print "# -> Name split requires manual verification ('$full_name' -> '$fname' '$sname')\n";
        push @name_split_check, "$player_id ($team_id): '$full_name' -> '$fname' '$sname'";
      }
      # Perform the insert
      $ply_sth->execute($player_id, convert_text($fname), convert_text($sname), $height, $weight, convert_text($college));
    }

    # Output the SQL
    print "INSERT IGNORE INTO SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS (player_id, remote_mugshot, first_seen) VALUES ('$player_id', '$remote_mugshot', NOW());\n"
      if defined($remote_mugshot);
    print "INSERT INTO SPORTS_NFL_TEAMS_ROSTERS (season, game_type, week, team_id, player_id, jersey, pos, player_status) VALUES ('$season', '$game_type', '$week', '$team_id', '$player_id', $jersey, '$pos', '$db_status');\n";
  }
  print "\n";
}

# If we have names to check, do so via an email
if (@name_split_check) {
  # Prepare the query and execute
  my $dbh_common = DBI->connect('dbi:mysql:' . $config{'db_common'}, $config{'db_user'}, $config{'db_pass'});
  my $name_email_sth = $dbh_common->prepare('INSERT INTO COMMS_EMAIL (app, email_id, email_reason, email_from, email_to, email_subject, email_body, email_queued, email_send, email_status) VALUES (?, NULL, ?, CONCAT("DeBear Support <support@", ?, ">"), CONCAT("DeBear Support <support@", ?, ">"), ?, ?, NOW(), NOW(), "queued");');
  $name_email_sth->execute(
    'sports_nfl',
    'player_name_check',
    $config{'domain'},
    $config{'domain'},
    'NFL Player Name Split Verification',
    "During today's run of the NFL Roster script, the following players were created and need to be checked to ensure the name was split correctly:\n- " . join("\n- ", @name_split_check)
  );
  undef $name_email_sth if defined($name_email_sth);
  $dbh_common->disconnect if defined($dbh_common);
}

# For regular season parsing, also pad to the end of the season
my $reg_weeks = ($season < 2021 ? 17 : 18);
if ($game_type eq 'regular' && $week < $reg_weeks) {
  print "# Padding from Week " . ($week+1) . " to Week $reg_weeks\n";
  for (my $w = $week + 1; $w <= $reg_weeks; $w++) {
    print "INSERT INTO SPORTS_NFL_TEAMS_ROSTERS (season, game_type, week, team_id, player_id, jersey, pos, player_status)
  SELECT season, game_type, '$w' AS week, team_id, player_id, jersey, pos, player_status
  FROM SPORTS_NFL_TEAMS_ROSTERS
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   week = '$week';\n";
  }
  print "\n";
}

print "# Tidy\n";
print "ALTER TABLE SPORTS_NFL_TEAMS_ROSTERS ORDER BY season, game_type, week, team_id, player_id;\n";

# Disconnect from the database
undef $sel_sth if defined($sel_sth);
undef $from_curr_sth if defined($from_curr_sth);
undef $new_sth if defined($new_sth);
undef $ins_sth if defined($ins_sth);
undef $ply_sth if defined($ply_sth);
$dbh->disconnect if defined($dbh);
