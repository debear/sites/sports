#!/usr/bin/perl -w
# Loop through and parse team rosters, and any new players found

use strict;
use Dir::Self;
use Cwd 'abs_path';
use URI::Escape;
use DBI;

# Validate the arguments
#  - there needs to be at least two: a season and game_type
#  - there could also be an additional argument though:  week
if (@ARGV < 2 || $ARGV[0] !~ m/^\d{4}$/ || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff', 'initial' ))) {
  print STDERR "Insufficient arguments to the parse roster script.\n";
  exit 98;
}

# Declare variables
our ($season, $game_type, $week) = @ARGV;
$week = '' if !defined($week);
print "#\n# Parsing rosters for '$season': '$game_type' // '$week'\n#\n";

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config = abs_path(__DIR__ . '/config.pl');
require $config;

# Now we've set the data_dir, change $game_type and $week to be the versions we want to store in the database
if ($game_type eq 'initial') {
  $game_type = 'regular';
  $week = 1;
}

# Get the list of teams
my $err = 0;
my @list = glob "$config{'data_dir'}/*.htm.gz" or $err = 1;
if ($err) {
  print STDERR "No roster files found?\n";
  exit 10;
}

# Clear a previous run
print "# Clearing previous instance\n";
print "DELETE FROM SPORTS_NFL_TEAMS_ROSTERS WHERE season = '$season' AND game_type = '$game_type' AND week >= '$week';\n\n";

# Prepare our database queries
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sel_sth = $dbh->prepare('SELECT player_id FROM SPORTS_NFL_PLAYERS_IMPORT WHERE remote_url_id = ?;');
my $new_sth = $dbh->prepare('SELECT MAX(player_id) + 1 AS new_player_id FROM SPORTS_NFL_PLAYERS;');
my $ins_sth = $dbh->prepare('INSERT INTO SPORTS_NFL_PLAYERS_IMPORT (player_id, remote_internal_id, remote_url_id, remote_url_name, remote_profile_id, profile_imported) VALUES (?, ?, ?, ?, ?, NULL);');
my $ply_sth = $dbh->prepare('INSERT INTO SPORTS_NFL_PLAYERS (player_id, first_name, surname, dob, birthplace, birthplace_state, birthplace_country, height, weight, college) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);');

# Loop through the teams
foreach my $file (@list) {
  # Get the team_id from the filename
  my ($team_id) = ($file =~ m/$config{'data_dir'}\/([A-Z]+)(?:\.\d)?\.htm\.gz/si);
  print "# $team_id ($file)\n";

  # Load the file
  my $roster;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  $roster = join('', @contents);

  # Get the list of players out
  my $pattern = '<tr class="[^"]*">\s+<td class="tbdy">(.*?)<\/td>\s+<td class="tbdy">(.*?)<\/td>\s+<td><a href="\/player\/(.*?)\/(.*?)\/profile">(.*?), (.*?)<\/a><\/td>\s+<td class="tbdy">(.*?)<\/td>';
  my @matches = ($roster =~ m/$pattern/gsi);

  # Display each player, creating where required
  while (my ($pos, $jersey, $url_name, $url_id, $sname, $fname, $status) = splice(@matches, 0, 7)) {
    # Convert the status code in to our version
    my $db_status = 'na';
    if ($status eq 'ACT') {
      $db_status = 'active';
    } elsif ($status eq 'RES') {
      $db_status = 'ir';
    } elsif ($status eq 'NON') {
      $db_status = 'nfi';
    } elsif ($status eq 'SUS') {
      $db_status = 'susp';
    } elsif ($status eq 'PUP') {
      $db_status = 'pup';
    }

    # Get the player_id from the database
    $sel_sth->execute($url_id);
    my ($player_id) = $sel_sth->fetchrow_array;

    # If no existing player record found, get the appropriate database fields from the player card and create
    if (!defined($player_id)) {
      # Get the info we need for the import table
      my $url = 'http://nflcdns.nfl.com/player/' . uri_escape_utf8($url_name) . '/' . uri_escape_utf8($url_id) . '/profile';
      my $player = download($url);
      my ($internal_id) = ($player =~ m/GSIS ID: ([0-9\-]+)/gsi);
      my ($profile_id) = ($player =~ m/ESB ID: ([A-Z0-9]+)/gsi);

      # Get the new player ID
      $new_sth->execute;
      ($player_id) = $new_sth->fetchrow_array;

      # Store the record in the import table
      $ins_sth->execute($player_id, $internal_id, $url_id, $url_name, $profile_id);

      ## Get and manipulate player fields for the data row
      my %info;

      # Date of Birth / Birthplace
      ($info{'dob'}, $info{'birthplace'}, $info{'birthplace_state'}) = ($player =~ m/<strong>Born<\/strong>: (\d+\/\d+\/\d+)? (.*?) ,? (.*?)\s+<\/p>/gsi);
      if (defined($info{'dob'})) {
        my ($m, $d, $y) = ($info{'dob'} =~ m/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/gsi);
        $info{'dob'} = sprintf("%04d-%02d-%02d", $y, $m, $d);
      }

      # Some Birthplace fixes
      if (defined($info{"birthplace_state"})) {
        if ($info{'birthplace_state'} =~ /^\s*USA\s*$/i) {
          $info{'birthplace_state'} = undef;
        } elsif ($info{'birthplace_state'} !~ /^[A-Z]{2}$/) {
          my $cp_col = (defined($info{'birthplace'}) ? 'birthplace_country' : 'birthplace');
          $info{$cp_col} = $info{'birthplace_state'};
          $info{'birthplace_state'} = undef;
        }
      }

      # Convert height from ft-in to inches
      ($info{'ht'}) = ($player =~ m/<strong>Height<\/strong>: (\d+\-\d+) &nbsp;/gsi);
      if (defined($info{'ht'})) {
        my ($ft, $in) = ($info{'ht'} =~ m/^(\d+)-(\d+)$/gsi);
        $info{'ht'} = (12 * $ft) + $in;
      }

      # Other getters:
      ($info{'wt'}) = ($player =~ m/<strong>Weight<\/strong>: (\d+) &nbsp;/gsi); # Weight
      ($info{'college'}) = ($player =~ m/<p><strong>College<\/strong>: (.*?)<\/p>/gsi); # College

      # Check for NULLs
      foreach my $field ('dob', 'birthplace', 'birthplace_state', 'birthplace_country', 'ht', 'wt', 'college') {
        $info{$field} = undef if !defined($info{$field}) || $info{$field} =~ m/^\s*$/;
      }

      # Create the player record
      $ply_sth->execute($player_id, $fname, $sname, $info{'dob'}, $info{'birthplace'}, $info{'birthplace_state'}, $info{'birthplace_country'}, $info{'ht'}, $info{'wt'}, $info{'college'});
    }

    # Some additional column tidying
    check_for_null(\$jersey);

    # Output the SQL
    print "INSERT INTO SPORTS_NFL_TEAMS_ROSTERS (season, game_type, week, team_id, player_id, jersey, pos, player_status) VALUES ('$season', '$game_type', '$week', '$team_id', '$player_id', $jersey, '$pos', '$db_status');\n";
  }
  print "\n";
}

# For regular season parsing, also pad to the end of the season
my $reg_weeks = ($season < 2021 ? 17 : 18);
if ($game_type eq 'regular' && $week < $reg_weeks) {
  print "# Padding from Week " . ($week+1) . " to Week $reg_weeks\n";
  for (my $w = $week + 1; $w <= $reg_weeks; $w++) {
    print "INSERT INTO SPORTS_NFL_TEAMS_ROSTERS (season, game_type, week, team_id, player_id, jersey, pos, player_status)
  SELECT season, game_type, '$w' AS week, team_id, player_id, jersey, pos, player_status
  FROM SPORTS_NFL_TEAMS_ROSTERS
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   week = '$week';\n";
  }
  print "\n";
}

print "# Tidy\n";
print "ALTER TABLE SPORTS_NFL_TEAMS_ROSTERS ORDER BY season, game_type, week, team_id, player_id;\n";

# Disconnect from the database
undef $sel_sth if defined($sel_sth);
undef $new_sth if defined($new_sth);
undef $ins_sth if defined($ins_sth);
undef $ply_sth if defined($ply_sth);
$dbh->disconnect if defined($dbh);
