/* In a potentially confusing misnomer, status updates are written to STDERR, output to STDOUT, to illustrate where errors occurred */
// Mentions of detect-non-literal-fs-filename are semgrep noise - no amount of sanitising the input will satisfy it
const puppeteer = require('puppeteer-core');
const fs = require('fs');
const zlib = require('zlib');
const debug = false;
const switchToStatsTab = false; // We do not need to do this from 2022 onwards
const statsTimeout = 10000; // In milliseconds

// Validate the arguments we have been supplied
//  - there needs to be two: URL endpoint and path to save files to (which must exist)
const gameEndpoint = process.argv[2];
const path = process.argv[3];
if (!gameEndpoint || !path) {
    console.error('Insufficient arguments to the gamecenter puppeteer script');
    process.exit(1);
} else if (!fs.existsSync(path)) { // nosemgrep detect-non-literal-fs-filename
    console.error(`Path ${path} does not exist`);
    process.exit(1);
}

// Ensure we use a common browser directory across all downloads (not just this one)
let curDate = (new Date()).toISOString().slice(0, 10);
let cmpProcess = false;
const browserDataDir = `/tmp/debear/puppeteer-${curDate}`;
if (!fs.existsSync(browserDataDir)) { // nosemgrep detect-non-literal-fs-filename
    try {
        fs.mkdirSync(browserDataDir, { recursive: true }); // nosemgrep detect-non-literal-fs-filename
        cmpProcess = true;
    } catch (err) {
        console.error(`Unable to create browser data dir: ${err}`);
        process.exit(1);
    }
}

// Our main processing code
(async () => {
    console.log(`# Puppeteer starting: ${new Date()}`);
    let url = `https://www.nfl.com/games/${gameEndpoint}`;
    console.log(`Loading URL ${url}`);

    // Start our browser instance
    let args = (debug ? ['--remote-debugging-port=9222'] : []);
    const browser = await puppeteer.launch({
        headless: true, // Allows us to reuse the existing open tab
        executablePath: '/opt/google/chrome/chrome',
        userDataDir: browserDataDir,
        args: args
    });
    const page = (await browser.pages())[0];

    // Load the homepage to manage the cookie management popup
    if (cmpProcess) {
        console.log('Processing cookie management popup');
        await page.goto('https://www.nfl.com/');
        let cmpSel = '.qc-cmp2-summary-buttons button:nth-of-type(2)';
        await page.waitForSelector(cmpSel);
        await page.click(cmpSel);
        console.log('- Complete');
    }

    // Keeping track of completed requests (as some are duplicated!)
    let found = { 'all': false, 'gamebook': false, 'gamebook_pdf': false, 'gamebook_id': false, 'stats': false };

    // A response handler to find the content we want to extract
    page.on('response', async (response) => {
        // Skip non "fetch" requests (as these have the data we're after)
        if (response.request().resourceType() !== 'fetch') {
            return;
        }
        // The actual PDF has been downloaded by the browser, so advertise the link (downloaded file itself usually isn't available to us)
        if (response.url().endsWith('_Gamebook.pdf') && !found.gamebook_pdf) {
            found.gamebook_pdf = true;
            found.all = (found.gamebook_pdf && found.gamebook_id && found.stats);
            found.gamebook = (found.gamebook_pdf && found.gamebook_id);
            console.log(`Gamebook URL: ${response.url()}`);
            // Get the ID from the AJAX request featuring IDs and game info
        } else if (response.url().includes('/games/') && !found.gamebook_id) {
            found.gamebook_id = true; // Prevent multiple responses being saved when only one needed
            let gsis = (await response.json()).externalIds.filter(row => { return (row.source === 'gsis'); })[0].id;
            console.log(`Gamebook ID: ${gsis}`);
            // Save to local file
            let local = `${path}/meta.json.gz`;
            if (saveFile(local)) {
                fs.writeFileSync(local, zlib.gzipSync(await response.text())); // nosemgrep detect-non-literal-fs-filename
                console.log(`Saved Game IDs response to ${local}`);
            } else {
                console.log(`Saving Game IDs response skipped`);
            }
            // Update the flags that would prevent the script aborting early
            found.all = (found.gamebook_pdf && found.gamebook_id && found.stats);
            found.gamebook = (found.gamebook_pdf && found.gamebook_id);
        } else if ((response.url().match(/\/experience\/v1\/stats\/[^\/]+\/players$/) !== null) && !found.stats) {
            found.stats = true; // Prevent multiple responses being saved when only one needed
            let local = `${path}/stats.json.gz`;
            if (saveFile(local)) {
                fs.writeFileSync(local, zlib.gzipSync(await response.text())); // nosemgrep detect-non-literal-fs-filename
                console.log(`Saved game stats to ${local}`);
            } else {
                console.log(`Saving game stats skipped`);
            }
            // Update the flags that would prevent the script aborting early
            found.all = (found.gamebook_pdf && found.gamebook_id && found.stats);
        }
    });

    // Main processing, with some minimal error handling
    try {
        // Stage 1: Load the page
        await page.goto(url, { waitUntil: 'networkidle2', timeout: 60000 });

        // Stage 2: Remove iframes for videos
        await page.evaluate(() => {
            document.querySelectorAll('iframe').forEach((ele) => {
                console.log('Removing video iframe');
                ele.parentNode.removeChild(ele);
            })
        });

        // Only proceed once gamebook processing has been completed
        let dtFailsafeStage2 = Date.now();
        while (!found.gamebook) {
            if ((Date.now() - dtFailsafeStage2) > 40000) {
                throw 'Post-Gamebook wait timed out after 40s';
            }
        }

        // Stage 3: Click on the stats tab to trigger the game stats request - which may no longer be required
        if (switchToStatsTab) {
            console.log('Switching to Stats tab');
            await page.evaluate(el => el.click(), (await page.$('div[data-testid="gamecenter-tabs-item-0"]'))).catch(err => {
                // Write the error to STDERR to be picked up by the calling script
                console.error(`An error occurred switching to the Stats tab: ${err}`);
                // We'll never complete after this error, so imply we have everything and let the error handling take its course
                found.all = true;
            });
        }

        // End once all stages have been processed
        let dtFailsafeStage3 = Date.now();
        while (!found.all) {
            if ((Date.now() - dtFailsafeStage3) > statsTimeout) {
                throw `Post-Stats wait timed out after ${statsTimeout}ms`;
            }
        }

    } catch (err) {
        // Write our error to STDERR to be picked up by the calling script
        console.error(err);

    } finally {
        // Upon completion, close everything down
        console.log(`# Puppeteer ended: ${new Date()}`);
        await page.close();
        await browser.close();
    }
})();

// Centralised logic for determining whether a file should be saved locally or skipped (as aleady saved)
let saveFile = (file) => {
    // Save if the file does not exist
    if (!fs.existsSync(file)) { // nosemgrep detect-non-literal-fs-filename
        return true;
    }
    // Save if it has not been saved recently
    let fileStat = fs.statSync(file); // nosemgrep detect-non-literal-fs-filename
    if (!fileStat) {
        return true;
    }
    let fileAge = Date.now() - fileStat.mtimeMs;
    return (fileAge >= 86400000); // (24h * 60m * 60s * 1000ms)
};
