/* In a potentially confusing misnomer, status updates are written to STDERR, output to STDOUT, to illustrate where errors occurred */
// Mentions of detect-non-literal-fs-filename are semgrep noise - no amount of sanitising the input will satisfy it
const puppeteer = require('puppeteer-core');
const fs = require('fs');
const zlib = require('zlib');
const debug = false;
const gamebookTimeout = 20000; // In milliseconds, before we stop waiting for the actual Gamebook PDF to have been returned
const statsTimeout = 20000; // In milliseconds

// Validate the arguments we have been supplied
//  - there needs to be two: URL endpoint and path to save files to (which must exist)
const gameEndpoint = process.argv[2];
const path = process.argv[3];
if (!gameEndpoint || !path) {
    console.error('Insufficient arguments to the gamecenter puppeteer script');
    process.exit(1);
} else if (!fs.existsSync(path)) { // nosemgrep detect-non-literal-fs-filename
    console.error(`Path ${path} does not exist`);
    process.exit(1);
}

// Ensure we use a common browser directory across all downloads (not just this one)
let curDate = (new Date()).toISOString().slice(0, 10);
const browserDataDir = `/tmp/debear/puppeteer-${curDate}`;
if (!fs.existsSync(browserDataDir)) { // nosemgrep detect-non-literal-fs-filename
    try {
        fs.mkdirSync(browserDataDir, { recursive: true }); // nosemgrep detect-non-literal-fs-filename
    } catch (err) {
        console.error(`Unable to create browser data dir: ${err}`);
        process.exit(1);
    }
}

// Our main processing code
(async () => {
    console.log(`# Puppeteer starting: ${new Date()}`);
    let url = `https://www.nfl.com/games/${gameEndpoint}?active-tab=stats`;
    console.log(`Loading URL ${url}`);

    // Start our browser instance
    let args = (debug ? ['--remote-debugging-port=9222'] : []);
    const browser = await puppeteer.launch({
        headless: true, // Allows us to reuse the existing open tab
        executablePath: '/opt/google/chrome/chrome',
        userDataDir: browserDataDir,
        args: args
    });
    const page = (await browser.pages())[0];

    // Keeping track of completed requests (as some are duplicated!)
    let found = { 'all': false, 'gamebook': false, 'gamebook_pdf': false, 'gamebook_id': false, 'stats': false };

    // A response handler to find the content we want to extract
    await page.on('response', async (response) => {
        // Skip non "fetch" requests (as these have the data we're after)
        if (response.request().resourceType() !== 'fetch') {
            return;
        }
        // The actual PDF has been downloaded by the browser, so advertise the link (downloaded file itself usually isn't available to us)
        if ((response.url().match(/\/gamecenter\/[^\/]+\.pdf$/) !== null) && !found.gamebook_pdf) {
            found.gamebook_pdf = true;
            found.gamebook = (found.gamebook_pdf || found.gamebook_id);
            found.all = (found.gamebook && found.stats);
            console.log(`${logFound(found)} :: Gamebook URL: ${response.url()}`);
            // Get the ID from the AJAX request featuring IDs and game info
        } else if (response.url().includes('/games/') && !found.gamebook_id) {
            found.gamebook_id = true; // Prevent multiple responses being saved when only one needed
            let gameUUID = (await response.json()).id;
            console.log(`${logFound(found)} :: Game UUID: ${gameUUID}`);
            // Save to local file
            let local = `${path}/meta.json.gz`;
            if (saveFile(local)) {
                fs.writeFileSync(local, zlib.gzipSync(await response.text())); // nosemgrep detect-non-literal-fs-filename
                console.log(`${logFound(found)} :: Saved Game IDs response to ${local}`);
            } else {
                console.log(`${logFound(found)} :: Saving Game IDs response skipped`);
            }
            // Update the flags that would prevent the script aborting early
            found.gamebook = (found.gamebook_pdf || found.gamebook_id);
            found.all = (found.gamebook && found.stats);
        } else if ((response.url().match(/\/experience\/v1\/stats\/[^\/]+\/players$/) !== null) && !found.stats) {
            found.stats = true; // Prevent multiple responses being saved when only one needed
            let local = `${path}/stats.json.gz`;
            if (saveFile(local)) {
                fs.writeFileSync(local, zlib.gzipSync(await response.text())); // nosemgrep detect-non-literal-fs-filename
                console.log(`${logFound(found)} :: Saved game stats to ${local}`);
            } else {
                console.log(`${logFound(found)} :: Saving game stats skipped`);
            }
            // Update the flags that would prevent the script aborting early
            found.all = (found.gamebook && found.stats);
        }
    });

    // Main processing, with some minimal error handling
    try {
        // Stage 1: Load the page
        await page.goto(url, { waitUntil: 'networkidle2', timeout: 60000 });

        // Stage 2: Remove iframes for videos
        await page.evaluate(() => {
            document.querySelectorAll('iframe').forEach((ele) => {
                console.log('Removing video iframe');
                ele.parentNode.removeChild(ele);
            })
        });

        // Only proceed once gamebook processing has been completed
        let dtFailsafeStage2 = Date.now();
        while (!found.gamebook_pdf) {
            if ((Date.now() - dtFailsafeStage2) > gamebookTimeout) {
                if (!found.gamebook_id) {
                    throw `Post-Gamebook wait timed out after ${gamebookTimeout}ms`;
                } else {
                    found.gamebook_pdf = true;
                    console.log(`${logFound(found)} :: Gamebook URL not determine after ${gamebookTimeout}ms, but UUID was successfully determined`);
                }
            }
        }

        // End once all stages have been processed
        let dtFailsafeStage3 = Date.now();
        while (!found.all) {
            if ((Date.now() - dtFailsafeStage3) > statsTimeout) {
                throw `Post-Stats wait timed out after ${statsTimeout}ms`;
            }
        }

    } catch (err) {
        // Write our error to STDERR to be picked up by the calling script
        console.error(`ERROR: ${err}`);

    } finally {
        // Upon completion, close everything down
        console.log(`# Puppeteer ended: ${new Date()}`);
        await page.close();
        await browser.close();
    }
})();

let logFound = (found) => {
    return `[P:${found.gamebook_pdf ? 'X' : ' '}][I:${found.gamebook_id ? 'X' : ' '}][S:${found.stats ? 'X' : ' '}]`;
};

// Centralised logic for determining whether a file should be saved locally or skipped (as aleady saved)
let saveFile = (file) => {
    // Save if the file does not exist
    if (!fs.existsSync(file)) { // nosemgrep detect-non-literal-fs-filename
        return true;
    }
    // Save if it has not been saved recently
    let fileStat = fs.statSync(file); // nosemgrep detect-non-literal-fs-filename
    if (!fileStat) {
        return true;
    }
    let fileAge = Date.now() - fileStat.mtimeMs;
    return (fileAge >= 86400000); // (24h * 60m * 60s * 1000ms)
};
