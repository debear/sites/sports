/* In a potentially confusing misnomer, status updates are written to STDERR, output to STDOUT, to illustrate where errors occurred */
// Mentions of detect-non-literal-fs-filename are semgrep noise - no amount of sanitising the input will satisfy it
const puppeteer = require('puppeteer-core');
const fs = require('fs');
const zlib = require('zlib');
const debug = false;

// Validate the arguments we have been supplied
//  - there needs to be three: A season, path to save files to (which must exist) and a list of weeks to download
const season = process.argv[2];
const path = process.argv[3];
const weeksRaw = process.argv[4];
if (!season || !path || !weeksRaw) {
    console.log('Insufficient arguments to the schedule puppeteer script');
    process.exit(1);
} else if (!fs.existsSync(path)) { // nosemgrep detect-non-literal-fs-filename
    console.log(`Path ${path} does not exist`);
    process.exit(1);
}

// Ensure we use a common browser directory across all downloads (not just this one)
let curDate = (new Date()).toISOString().slice(0, 10);
let cmpProcess = false;
const browserDataDir = `/tmp/debear/puppeteer-${curDate}`;
if (!fs.existsSync(browserDataDir)) { // nosemgrep detect-non-literal-fs-filename
    try {
        fs.mkdirSync(browserDataDir, { recursive: true }); // nosemgrep detect-non-literal-fs-filename
        cmpProcess = true;
    } catch (err) {
        console.log(`Unable to create browser data dir: ${err}`);
        process.exit(1);
    }
}

// Our main processing code
(async () => {
    console.log(`# Puppeteer starting: ${new Date()}`);

    // Start our browser instance
    let args = (debug ? ['--remote-debugging-port=9222'] : []);
    const browser = await puppeteer.launch({
        headless: true, // Allows us to reuse the existing open tab
        executablePath: '/opt/google/chrome/chrome',
        userDataDir: browserDataDir,
        args: args
    });
    const page = (await browser.pages())[0];
    // Switch our timezone to US/Eastern, as that is what the schedule is stored against in the database
    await page.emulateTimezone('America/New_York');

    // Load the homepage to manage the cookie management popup
    if (cmpProcess) {
        console.log('# Processing cookie management popup');
        await page.goto('https://www.nfl.com/');
        let cmpSel = '.qc-cmp2-summary-buttons button:nth-of-type(2)';
        await page.waitForSelector(cmpSel);
        await page.click(cmpSel);
        console.log('# Complete');
    }

    // Keeping track of completed requests (as some are duplicated!)
    let processing = { };

    // A response handler to find the content we want to extract
    page.on('response', async (response) => {
        // Skip non "fetch" requests (as these have the data we're after)
        if (response.request().resourceType() !== 'xhr') {
            return;
        }
        // The HTML frame content for the requested week
        if (response.url().includes('/api/lazy/load') && !processing.found) {
            fs.writeFileSync(processing.local, zlib.gzipSync(await response.text())); // nosemgrep detect-non-literal-fs-filename
            console.log(`# -> Schedule response saved to ${processing.local}`);
        }
    });

    // Main processing, with some minimal error handling
    try {
        // Loop through the requested weeks
        let urlBase = `https://www.nfl.com/schedules/${season}/`;
        var weeks = weeksRaw.split(',');
        for (var i = 0; i < weeks.length; i++) {
            // Determine the URL we are processing and where to store the file
            let week = weeks[i].split(':');
            let url = `${urlBase}${week[0]}/`;
            console.log(`# Processing URL ${url}`);
            processing = { 'local': `${path}/${week[1]}.htm.gz`, 'found': false };
            // Only proceed if we have no, or not sufficiently recent, copy of the file
            if (saveFile(processing.local)) {
                await page.goto(url, { waitUntil: 'networkidle2', timeout: 60000 });
                await autoScroll(page);
            } else {
                console.log('# -> Schedule request skipped, as it is already saved locally');
            }
        }

    } catch (err) {
        // Write our error to STDERR to be picked up by the calling script
        console.log(err);

    } finally {
        // Upon completion, close everything down
        console.log(`# Puppeteer ended: ${new Date()}`);
        await browser.close();
    }
})();

// Centralised logic for determining whether a file should be saved locally or skipped (as aleady saved)
let saveFile = (file) => {
    // Save if the file does not exist
    if (!fs.existsSync(file)) { // nosemgrep detect-non-literal-fs-filename
        return true;
    }
    // Save if it has not been saved recently
    let fileStat = fs.statSync(file); // nosemgrep detect-non-literal-fs-filename
    if (!fileStat) {
        return true;
    }
    let fileAge = Date.now() - fileStat.mtimeMs;
    return (fileAge >= 86400000); // (24h * 60m * 60s * 1000ms)
};

// Adapted from https://stackoverflow.com/questions/51529332/puppeteer-scroll-down-until-you-cant-anymore
let autoScroll = (async (page) => {
    await page.evaluate(async () => {
        await new Promise((resolve) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight - window.innerHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
});
