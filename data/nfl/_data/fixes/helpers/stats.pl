#!/usr/bin/perl -w
# Helper methods for game stat fixes

our $blob; # A little naughty to define this globally...

# Update field(s) in an existing player
sub fix_stats_info {
  my ($i, $fixes) = @_;
  foreach my $key (keys %${fixes}) {
    $$blob{'data'}{'viewer'}{'playerGameStats'}{'edges'}[$i]{'node'}{'player'}{$key} = $$fixes{$key};
  }
}

# Add a player missing from the list entirely
sub fix_stats_missing {
  my ($team_id, $fname, $sname, $jersey, $pos, $headshot) = @_;

  # Get the 'nickName' field from an existing player for this team
  my $team_nickname;
  for (my $i = 0; !defined($team_nickname) && $i < @{$$blob{'data'}{'viewer'}{'playerGameStats'}{'edges'}}; $i++) {
    $team_nickname = $$blob{'data'}{'viewer'}{'playerGameStats'}{'edges'}[$i]{'node'}{'player'}{'currentTeam'}{'nickName'}
      if $$blob{'data'}{'viewer'}{'playerGameStats'}{'edges'}[$i]{'node'}{'player'}{'currentTeam'}{'abbreviation'} eq $team_id;
  }
  if (!defined($team_nickname)) {
    print STDERR "Unable to find a team nickname for '$team_id' when adding '$fname $sname' to the game stats\n";
    return;
  }

  # Now add to the list
  push @{$$blob{'data'}{'viewer'}{'playerGameStats'}{'edges'}}, {
    'node' => {
      'player' => {
        'position' => $pos,
        'jerseyNumber' => $jersey,
        'currentTeam' => {
          'abbreviation' => $team_id,
          'nickName' => $team_nickname
        },
        'person' => {
          'firstName' => $fname,
          'lastName' => $sname,
          'displayName' => "$fname $sname",
          'headshot' => {
            'url' => (defined($headshot) ? "https://static.www.nfl.com/image/private/{formatInstructions}/league/$headshot" : undef)
          }
        }
      }
    }
  };
}

# Return true to satisfy the parser
return 1;
