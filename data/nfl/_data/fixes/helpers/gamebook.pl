#!/usr/bin/perl -w
# Helper methods for gamebook fixes

# Apply a specific name-based fix on the parsed (not raw) data
sub fix_gamebook_name {
  my ($team_id, $jersey, $pos, $name, $name_fixed) = @_;

  # Determine whether this player applies to the home / away list (if even relevant...)
  my $team_type = ($team_id eq $$game_info{'home_id'} ? 'home'
    : ($team_id eq $$game_info{'visitor_id'} ? 'away' : undef));
  return if !defined($team_type);

  # Loop through their players and find a match - by name and jersey or position
  foreach my $key (keys (%{$gb_rosters{$team_type}})) {
    for (my $i = 0; $i < @{$gb_rosters{$team_type}{$key}}; $i++) {
      if ($gb_rosters{$team_type}{$key}[$i]{'name'} eq $name
        && ($gb_rosters{$team_type}{$key}[$i]{'jersey'} == $jersey || $gb_rosters{$team_type}{$key}[$i]{'pos'} eq $pos)) {
        # We have a match
        print "# Gamebook name fix: $team_type.$key\[$i\].name from '$name' to '$name_fixed'\n";
        $gb_rosters{$team_type}{$key}[$i]{'name'} = $name_fixed;
      }
    }
  }
}

# Apply a specific position-based fix on the parsed (not raw) data
sub fix_gamebook_pos {
  my ($team_id, $jersey, $name, $pos, $pos_fixed) = @_;

  # Determine whether this player applies to the home / away list (if even relevant...)
  my $team_type = ($team_id eq $$game_info{'home_id'} ? 'home'
    : ($team_id eq $$game_info{'visitor_id'} ? 'away' : undef));
  return if !defined($team_type);

  # Loop through their players and find a match - by name and jersey
  foreach my $key (keys (%{$gb_rosters{$team_type}})) {
    for (my $i = 0; $i < @{$gb_rosters{$team_type}{$key}}; $i++) {
      if ($gb_rosters{$team_type}{$key}[$i]{'name'} eq $name && $gb_rosters{$team_type}{$key}[$i]{'jersey'} == $jersey) {
        # We have a match
        print "# Gamebook position fix: $team_type.$key\[$i\].pos from '$pos' to '$pos_fixed'\n";
        $gb_rosters{$team_type}{$key}[$i]{'pos'} = $pos_fixed;
      }
    }
  }
}

# Return true to satisfy the parser
return 1;
