#!/usr/bin/perl -w
# Helper methods for roster fixes

# Apply a jersey number correction to the roster lookup data
sub fix_roster_jersey {
  my ($key, $fname, $sname, $from, $to) = @_;

  # If it doesn't match, skip
  return
    if !defined($rosters{$key}{'jersey'}{"$from#$fname.$sname"});

  my $player_id = $rosters{$key}{'jersey'}{"$from#$fname.$sname"};
  print "# Roster jersey fix: $key '$fname.$sname' (ID: $player_id) from $from to $to\n";
  for (my $i = 3; $i > 0; $i--) {
    my $fname_fmt = substr($fname, 0, $i);
    delete $rosters{$key}{'jersey'}{"$from#$fname_fmt.$sname"};
    $rosters{$key}{'jersey'}{"$to#$fname_fmt.$sname"} = $player_id;
  }
}

# Return true to satisfy the parser
return 1;
