#!/usr/bin/perl -w
# Parse the schedule for a season from NFL.com

use strict;
use utf8;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $mode, $date, $dated_file) = parse_args();
$config{'data_dir'} = $config{'base_dir'} . '/_data';
$config{'base_dir'} = get_base_dir();

# Load in alternate venue info
my %alt_venues = ( );
my $alt_file = "$config{'base_dir'}/../../schedule/${season}_alt_venues.csv";
if ( -e $alt_file ) {
  open FILE, "<$alt_file";
  my @alt = <FILE>;
  close FILE;

  foreach my $line (@alt) {
    chomp($line);
    my ($s, $st, $w, $v, $h, $a) = split(/,/, $line);
    $alt_venues{"$st/$w/$v/$h"} = $a;
  }

  @alt = ( ); # Homegrown garbage collect...
}

# Get and loop through the list of files
my @sched_list = ();
foreach my $game_type ('regular', 'playoff') {
  # Ignore?
  next if !grep /^$mode$/, ($game_type, 'both');

  # Load, parse, process
  foreach my $file (glob("$config{'base_dir'}/$dated_file/$game_type*.xml.gz")) {
    my ($week) = ($file =~ m/(?:regular|playoff)0?(\d+)\.xml.gz$/si);
    print "##\n## Loading: $file (" . ($game_type eq 'regular' ? 'Regular Season' : 'Playoffs') . ", Week $week)\n##\n";

    # Load the file
    open FILE, "gzip -dc $file |";
    my @xml = <FILE>;
    close FILE;
    my $xml = join('', @xml);

    # Loop through each of the games
    my $game_num = 0;
    my @game_info = ($xml =~ m/<g (.*?) gt="[^"]*"\/>/gsi);
    foreach my $info (@game_info) {
      my ($game_id, $nfl_gsis_id, $time_h, $time_m, $time_tod, $home, $visitor) = ($info =~ m/eid="(\d+)" gsis="(\d+)" d="[^"]*" t="(\d+):(\d+)" q="([^"]*)" k="[^"]*" h="([^"]*)" hnn="[^"]*" hs="[^"]*" v="([^"]*)" vnn="[^"]*" vs="[^"]*" p="[^"]*" rz="[^"]*" ga="[^"]*"/gsi);

      # Parse the team IDs
      $visitor = convert_team_id($visitor);
      $home = convert_team_id($home);

      # Alternate venue?
      my $alt_venue = undef;
      $alt_venue = $alt_venues{"$game_type/$week/$visitor/$home"} if defined($alt_venues{"$game_type/$week/$visitor/$home"});
      $alt_venue = $alt_venues{"$game_type/$week//"} if !defined($alt_venue) && defined($alt_venues{"$game_type/$week//"});
      if (defined($alt_venue)) {
        $alt_venue = "'" . convert_text($alt_venue) . "'";
      } else {
        $alt_venue = 'NULL';
      }

      # Build the game date using the Game ID
      my $game_date = substr($game_id, 0, 4) . '-' . substr($game_id, 4, 2) . '-' . substr($game_id, 6, 2);
      # And the time using its components
      my $is_am = ("$time_h:$time_m" eq '9:30' && $alt_venue ne 'NULL'); # 9:30 London Games
      my $game_time = sprintf('%02d:%02d', $time_h + ($time_tod eq 'P' || !$is_am ? 12 : 0), $time_m);

      # Now form into some SQL
      my $seq_id = ($week * 100) +  ++$game_num;
      print "# Game $game_id, $visitor @ $home\n";
      print "SET \@game_id := NULL;\n"; # Re-set as if new game, INSERT will over-write the previous game
      print "SELECT `game_id` INTO \@game_id
FROM SPORTS_NFL_SCHEDULE
WHERE `season` = '$season'
AND   `game_type` = '$game_type'
AND   `week` = '$week'
AND   `home` = '$home'
AND   `visitor` = '$visitor'
LIMIT 1;\n";
      print "INSERT INTO `SPORTS_NFL_SCHEDULE` (`season`, `game_type`, `week`, `game_id`, `nfl_id`, `nfl_gsis_id`, `game_date`, `game_time`, `visitor`, `home`, `alt_venue`)
                           VALUES ('$season', '$game_type', '$week', IFNULL(\@game_id, '$seq_id'), '$game_id', '$nfl_gsis_id', '$game_date', '$game_time', '$visitor', '$home', $alt_venue)
ON DUPLICATE KEY UPDATE `nfl_id` = VALUES(`nfl_id`),
                        `nfl_gsis_id` = VALUES(`nfl_gsis_id`),
                        `game_date` = VALUES(`game_date`),
                        `game_time` = VALUES(`game_time`),
                        `alt_venue` = VALUES(`alt_venue`);\n\n";
    }
  }

  # Identify bye weeks
  my $reg_weeks = ($season < 2021 ? 17 : 18);
  if ($game_type eq 'regular') {
    print "# Bye weeks...\n";
    print "DROP TEMPORARY TABLE IF EXISTS `tmp_weeks`;\n";
    print "CREATE TEMPORARY TABLE `tmp_weeks` (
  `week` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY(`week`)
);\n";
    print "INSERT INTO `tmp_weeks` (`week`) VALUES";
    for (my $i = 1; $i <= $reg_weeks; $i++) {
      print ',' if $i > 1;
      print " ($i)";
    }
    print ";\n";

    print "INSERT INTO `SPORTS_NFL_SCHEDULE_BYES` (`season`, `game_type`, `team_id`, `week`)
  SELECT '$season' AS `season`, 'regular' AS `game_type`, `SPORTS_NFL_TEAMS`.`team_id`, `tmp_weeks`.`week`
  FROM `SPORTS_NFL_TEAMS`
  JOIN `SPORTS_NFL_TEAMS_GROUPINGS`
    ON (`SPORTS_NFL_TEAMS_GROUPINGS`.`team_id` = `SPORTS_NFL_TEAMS`.`team_id`
    AND '$season' BETWEEN `SPORTS_NFL_TEAMS_GROUPINGS`.`season_from` AND IFNULL(`SPORTS_NFL_TEAMS_GROUPINGS`.`season_to`, 2099))
  JOIN `tmp_weeks` ON (1 = 1)
  LEFT JOIN `SPORTS_NFL_SCHEDULE`
    ON (`SPORTS_NFL_SCHEDULE`.`season` = '$season'
    AND `SPORTS_NFL_SCHEDULE`.`game_type` = '$game_type'
    AND `SPORTS_NFL_SCHEDULE`.`week` = `tmp_weeks`.`week`
    AND (`SPORTS_NFL_SCHEDULE`.`visitor` = `SPORTS_NFL_TEAMS`.`team_id`
      OR `SPORTS_NFL_SCHEDULE`.`home` = `SPORTS_NFL_TEAMS`.`team_id`))
  WHERE `SPORTS_NFL_SCHEDULE`.`game_id` IS NULL
  GROUP BY `SPORTS_NFL_TEAMS`.`team_id`
ON DUPLICATE KEY UPDATE `week` = VALUES(`week`);\n\n";
  } else {
    # Playoff Bye Weeks
    print "INSERT IGNORE INTO `SPORTS_NFL_SCHEDULE_BYES` (`season`, `game_type`, `team_id`, `week`)
  SELECT `season`, 'playoff' AS `game_type`, `team_id`, 1 AS `week`
  FROM `SPORTS_NFL_STANDINGS`
  WHERE `season` = '$season'
  AND   `week` = '$reg_weeks'
  AND   `pos_conf` <= IF(`season` <= 2019, 2, 1)
  ORDER BY `team_id`;\n\n";
  }

  # Calculate week date ranges
  print "# Week dates\n";
  print "DROP TEMPORARY TABLE IF EXISTS `tmp_dates`;\n";
  print "CREATE TEMPORARY TABLE `tmp_dates` (
  `season` YEAR NOT NULL,
  `game_type` VARCHAR(10),
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `max_date` DATE,
  `max_date_dow` TINYINT(1) UNSIGNED NOT NULL,
  `start_date` DATE,
  `end_date` DATE,
  PRIMARY KEY (`season`, `game_type`, `week`)
) SELECT `season`, `game_type`, `week`,
         MAX(`game_date`) AS `max_date`,
         DAYOFWEEK(MAX(`game_date`)) AS `max_date_dow`,
         NULL AS `start_date`,
         NULL AS `end_date`
  FROM `SPORTS_NFL_SCHEDULE`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  GROUP BY `week`;\n";
  print "UPDATE `tmp_dates` SET `end_date` = DATE_ADD(`max_date`, INTERVAL IF(`max_date_dow` > 3, 10, 3) - `max_date_dow` DAY);\n\n";

  print "DROP TEMPORARY TABLE IF EXISTS `tmp_dates_cp`;\n";
  print "CREATE TEMPORARY TABLE `tmp_dates_cp` LIKE `tmp_dates`;\n";
  print "INSERT INTO `tmp_dates_cp` SELECT * FROM `tmp_dates`;\n\n";

  print "UPDATE `tmp_dates`
LEFT JOIN `tmp_dates_cp`
  ON (`tmp_dates_cp`.`season` = `tmp_dates`.`season`
  AND `tmp_dates_cp`.`game_type` = `tmp_dates`.`game_type`
  AND `tmp_dates_cp`.`week` = `tmp_dates`.`week` - 1)
SET `tmp_dates`.`start_date` = IFNULL(DATE_ADD(`tmp_dates_cp`.`end_date`, INTERVAL 1 DAY),
                                      DATE_SUB(`tmp_dates`.`end_date`, INTERVAL 6 DAY));\n\n";

  print "INSERT INTO `SPORTS_NFL_SCHEDULE_WEEKS` (`season`, `game_type`, `week`, `start_date`, `end_date`)
  SELECT `season`, `game_type`, `week`, `start_date`, `end_date`
  FROM `tmp_dates`
ON DUPLICATE KEY UPDATE `start_date` = VALUES(`start_date`),
                        `end_date` = VALUES(`end_date`);\n\n";

  # Power Rank calc weeks
  # - The Tuesday following the games
  print "# Power Ranks\n";
  print "INSERT INTO `SPORTS_NFL_POWER_RANKINGS_WEEKS` (`season`, `game_type`, `week`, `calc_date`)
  SELECT `season`, `game_type`, `week`, `end_date` AS `calc_date`
  FROM `tmp_dates`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
ON DUPLICATE KEY UPDATE `calc_date` = VALUES(`calc_date`);\n\n";

  # Initial standings
  if ($game_type eq 'regular' && $dated_file =~ /initial/) {
    print "# Initial standings\n";
    print "CALL nfl_standings_initial('$season');\n\n";
  }

  # General tidy up...
  print "# Tidy the database\n";
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE` ORDER BY `season`, `game_type`, `week`, `game_date`, `game_time`, `nfl_id`;\n";
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE_BYES` ORDER BY `season`, `game_type`, `team_id`;\n" if ($game_type eq 'regular');
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE_WEEKS` ORDER BY `season`, `game_type`, `week`;\n";
  print "ALTER TABLE `SPORTS_NFL_POWER_RANKINGS_WEEKS` ORDER BY `season`, `game_type`, `week`;\n";
  print "\n";
}

