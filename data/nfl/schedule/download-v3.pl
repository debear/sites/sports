#!/usr/bin/perl -w
# Get the schedule for a season from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use List::Util qw(min);

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $mode, $date, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir() . $dated_file;

# Get the vars to pass to the web request
my @requests = ( );

push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# URL args
my @time = localtime();
my $season_diff = ($time[4] > 3 ? 1900 : 1899) + $time[5] - $season;

# Get the list
my @downloads = ( );
my $reg_weeks = ($season < 2021 ? 17 : 18);
foreach my $game_type (@requests) {
  # Determine which weeks we're downloading from and to
  my $week_from = 1;
  my $week_to = ($game_type eq 'regular' ? $reg_weeks : 1);
  if ($date ne '') {
    my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
    # Get regular season range from our list of weeks with dates
    if ($game_type eq 'regular') {
      # Get from our list of weeks with dates
      my $sql = 'SELECT game_type, week FROM SPORTS_NFL_SCHEDULE_WEEKS WHERE season = ? AND end_date > ? ORDER BY end_date LIMIT 1;';
      my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $date);
      # Is this date usable?
      $week_from = $$ret[0]{'week'} if defined($$ret[0]{'week'});

    # Get playoff range from the list of scheduled and completed playoff games
    } elsif ($game_type eq 'playoff') {
      my $sql = 'SELECT MAX(week) AS max_week, MAX(IF(status IS NULL, week, NULL)) AS max_unplayed FROM SPORTS_NFL_SCHEDULE WHERE season = ? AND game_type = "playoff";';
      my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
      if (!defined($$ret[0]{'max_week'})) {
        # No playoff games, so this is the first week
        $week_from = 1;
      } elsif (defined($$ret[0]{'max_unplayed'})) {
        # We have an incomplete week, so re-parse that week
        $week_from = $$ret[0]{'max_unplayed'};
      } else {
        # Parsing the week after the completed round (but no later than Week 4 == Super Bowl)
        $week_from = min($$ret[0]{'max_week'} + 1, 4);
      }
      # Playoff weeks are loaded individually, so 'from' == 'to'
      $week_to = $week_from;
    }
    $dbh->disconnect if defined($dbh);
  }

  # Loop through each week and determine what needs to be downloaded
  print "# Processing '$game_type' games from week $week_from to $week_to\n";
  for (my $i = $week_from; $i <= $week_to; $i++) {
    my $week = ($game_type eq 'regular' ? 'REG' : 'POST') . $i;
    my $file = $game_type  . sprintf('%02d', $i);
    push @downloads, "$week:$file";
  }
}

if (!@downloads) {
  print STDERR "No downloads determined, aborting.\n";
  exit 1;
}

# Create dated folder?
mkdir $config{'base_dir'}
  if ! -e $config{'base_dir'};

my $file_stderr = "/tmp/nfl.$date.sch.err";
my $weeks = join(',', @downloads);
print "# Puppeteer arguments:\n# - $season\n# - $config{'base_dir'}\n# - $weeks\n\n";
my $download = `cd $config{'puppeteer_dir'}; node schedule.js '$season' '$config{'base_dir'}' '$weeks' 2>$file_stderr`;
print $download;
