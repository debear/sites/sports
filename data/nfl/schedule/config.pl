#!/usr/bin/perl -w
# Common code for the schedule downloading and parsing

#
# Convert command line args in to something we can process
#
sub parse_args {
  my $season = ''; my $mode = ''; my $date = ''; my $dated_file = '';

  foreach my $arg (@ARGV) {
    if ($arg eq '--regular') {
      $mode = ($mode eq '' ? 'regular' : 'both');
    } elsif ($arg eq '--playoff') {
      $mode = ($mode eq '' ? 'playoff' : 'both');
    } elsif ($season eq '') {
      $season = $arg;
    } elsif ($arg =~ /^\d{4}-\d{2}-\d{2}$/ && $dated_file eq '') {
      $date = $arg;
      $dated_file = "/$date";
    } else {
      print '# Unknown option "' . $arg . '"' . "\n";
    }
  }

  $dated_file = "/initial" if !$dated_file;
  return ($season, $mode, $date, $dated_file);
}

#
# Where are the files downloaded
#
sub get_base_dir {
  our (%config, $season);
  return sprintf('%s/_data/%s/schedules', $config{'base_dir'}, $season);
}

# Return true to pacify the compiler
1;
