#!/usr/bin/perl -w
# Parse the schedule for a season from NFL.com

use strict;
use utf8;
use Dir::Self;
use Cwd 'abs_path';
use Switch;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $mode, $date, $dated_file) = parse_args();
$config{'data_dir'} = $config{'base_dir'} . '/_data';
$config{'base_dir'} = get_base_dir();

# Load in alternate venue info
my %alt_venues = ( );
my $alt_file = "$config{'base_dir'}/../../schedule/${season}_alt_venues.csv";
if ( -e $alt_file ) {
  open FILE, "<$alt_file";
  my @alt = <FILE>;
  close FILE;

  foreach my $line (@alt) {
    chomp($line);
    my ($s, $st, $w, $v, $h, $a) = split(/,/, $line);
    $alt_venues{"$st/$w/$v/$h"} = $a;
  }

  @alt = ( ); # Homegrown garbage collect...
}

# Get and loop through the list of files
my @sched_list = ();
foreach my $game_type ('regular', 'playoff') {
  # Ignore?
  next if !grep /^$mode$/, ($game_type, 'both');

  # Load, parse, process
  foreach my $file (glob("$config{'base_dir'}/$dated_file/$game_type*.htm.gz")) {
    my ($week) = ($file =~ m/(?:regular|playoff)0?(\d+)\.htm.gz$/si);
    print "##\n## Loading: $file (" . ($game_type eq 'regular' ? 'Regular Season' : 'Playoffs') . ", Week $week)\n##\n";
    my $html = load_file($file);

    # Store the number of games by day (so we can quickly guesstimate a Sunday for the games TBD)
    my %games_by_day = ( );

    # Blocks are grouped by game date
    my $game_num = 0;
    my @days = ($html =~ m/<section class="d3-l-grid--outer d3-l-section-row nfl-o-matchup-group">(.*?)<\/section>/gsi);
    foreach my $day (@days) {
      # Parse the date
      my $game_date;
      my ($date_m, $date_d) = ($day =~ m/<h2 class="d3-o-section-title">[^,]+, ([^ ]+) (\d+)\w{2}<\/h2>/);
      if (defined($date_m)) {
        $game_date = parse_date($season, $date_m, $date_d);
      } else {
        # No explicit date set, so guesstimate as a Sunday 1pm game
        $game_date = (reverse sort {$games_by_day{$a} <=> $games_by_day{$b}} keys %games_by_day)[0];
        if (defined($game_date)) {
          print "## The following games are yet to be officially scheduled, assuming Sunday: '$game_date'\n\n";
        } elsif ($game_type eq 'regular') {
          # It would appear all games in this week do not have an official game date set
          # Do the maths from the first week of the season to determine that Sunday's date
          my $wk1_html = load_file("$config{'base_dir'}/initial/${game_type}01.htm.gz");
          my ($wk1_sun_m, $wk1_sun_d) = ($wk1_html =~ m/<h2 class="d3-o-section-title">Sunday, ([^ ]+) (\d+)\w{2}<\/h2>/gsi);
          my $wk1_sun = parse_date($season, $wk1_sun_m, $wk1_sun_d);
          # Now add the appropriate number of weeks to this date
          my $offset = ($week - 1); # The date is relative to Week 1, so Week 2 is 1 week after Week 1
          $game_date = `date -d '$wk1_sun +$offset weeks' +'\%F'`; chomp($game_date);
          print "## The following games are yet to be officially scheduled, assuming Sunday: '$game_date' (calculated relative to Week 1)\n\n";
        } else {
          # This logic is not yet in place for the playoffs - flag if this scenario occurrs
          print STDERR "Unable to determine the Sunday game_date during the playoffs, when no games are defined\n";
        }
      }

      # Now get and parse the list of games
      my @games = ($day =~ m/<div class="nfl-c-matchup-strip nfl-c-matchup-strip--pre-game">.*?<p class="nfl-c-matchup-strip__(?:date-info|period)">(.*?)<\/p>.*?<span class="nfl-c-matchup-strip__team-abbreviation">\s*(\S+)\s*<\/span>.*?<span class="nfl-c-matchup-strip__team-abbreviation">\s*(\S+)\s*<\/span>/gsi);
      $games_by_day{$game_date} = @games / 3;
      while (my ($time, $visitor, $home) = splice(@games, 0, 3)) {
        # Parse the team IDs
        $visitor = convert_team_id($visitor);
        $home = convert_team_id($home);

        # State game info
        my $seq_id = ($week * 100) +  ++$game_num;
        print "# Game $seq_id, $visitor @ $home\n";

        # Alternate venue?
        my $alt_venue = undef;
        $alt_venue = $alt_venues{"$game_type/$week/$visitor/$home"} if defined($alt_venues{"$game_type/$week/$visitor/$home"});
        $alt_venue = $alt_venues{"$game_type/$week//"} if !defined($alt_venue) && defined($alt_venues{"$game_type/$week//"});
        if (defined($alt_venue)) {
          $alt_venue = "'" . convert_text($alt_venue) . "'";
        } else {
          $alt_venue = 'NULL';
        }

        # Build the game time using its components
        my $time_h; my $time_m; my $time_tod;
        if ($time =~ /<span class="nfl-c-matchup-strip__date-time">/) {
          ($time_h, $time_m, $time_tod) = ($time =~ m/<span class="nfl-c-matchup-strip__date-time">\s*(\d+):(\d+) ([AP])M\s*<\/span>/gsi);
        } else {
          # No time currently set, assume a standard afternoon game (until we learn otherwise)
          print "# - No official start time, assuming 1pm\n";
          $time_h = 1; $time_m = 0; $time_tod = 'P';
        }
        my $game_time = sprintf('%02d:%02d', $time_h + ($time_tod eq 'P' ? 12 : 0), $time_m);

        # Now form into some SQL
        print "SET \@game_id := NULL;\n"; # Re-set as if new game, INSERT will over-write the previous game
        print "SELECT `game_id` INTO \@game_id
FROM SPORTS_NFL_SCHEDULE
WHERE `season` = '$season'
AND   `game_type` = '$game_type'
AND   `week` = '$week'
AND   `home` = '$home'
AND   `visitor` = '$visitor'
LIMIT 1;\n";
        print "INSERT INTO `SPORTS_NFL_SCHEDULE` (`season`, `game_type`, `week`, `game_id`, `nfl_id`, `game_date`, `game_time`, `visitor`, `home`, `alt_venue`)
                           VALUES ('$season', '$game_type', '$week', IFNULL(\@game_id, '$seq_id'), CONCAT('$season', LPAD(IFNULL(\@game_id, '$seq_id'), 4, '0'), 'XX'), '$game_date', '$game_time', '$visitor', '$home', $alt_venue)
ON DUPLICATE KEY UPDATE `game_date` = VALUES(`game_date`),
                        `game_time` = VALUES(`game_time`),
                        `alt_venue` = VALUES(`alt_venue`);\n\n";
      }
    }
  }

  print "##\n## Post-processing\n##\n";
  # Identify bye weeks
  print "# Bye weeks\n";
  my $reg_weeks = ($season < 2021 ? 17 : 18);
  if ($game_type eq 'regular') {
    print "DROP TEMPORARY TABLE IF EXISTS `tmp_weeks`;\n";
    print "CREATE TEMPORARY TABLE `tmp_weeks` (
  `week` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY(`week`)
);\n";
    print "INSERT INTO `tmp_weeks` (`week`) VALUES";
    for (my $i = 1; $i <= $reg_weeks; $i++) {
      print ',' if $i > 1;
      print " ($i)";
    }
    print ";\n";

    print "INSERT INTO `SPORTS_NFL_SCHEDULE_BYES` (`season`, `game_type`, `team_id`, `week`)
  SELECT '$season' AS `season`, 'regular' AS `game_type`, `SPORTS_NFL_TEAMS`.`team_id`, `tmp_weeks`.`week`
  FROM `SPORTS_NFL_TEAMS`
  JOIN `SPORTS_NFL_TEAMS_GROUPINGS`
    ON (`SPORTS_NFL_TEAMS_GROUPINGS`.`team_id` = `SPORTS_NFL_TEAMS`.`team_id`
    AND '$season' BETWEEN `SPORTS_NFL_TEAMS_GROUPINGS`.`season_from` AND IFNULL(`SPORTS_NFL_TEAMS_GROUPINGS`.`season_to`, 2099))
  JOIN `tmp_weeks` ON (1 = 1)
  LEFT JOIN `SPORTS_NFL_SCHEDULE`
    ON (`SPORTS_NFL_SCHEDULE`.`season` = '$season'
    AND `SPORTS_NFL_SCHEDULE`.`game_type` = '$game_type'
    AND `SPORTS_NFL_SCHEDULE`.`week` = `tmp_weeks`.`week`
    AND (`SPORTS_NFL_SCHEDULE`.`visitor` = `SPORTS_NFL_TEAMS`.`team_id`
      OR `SPORTS_NFL_SCHEDULE`.`home` = `SPORTS_NFL_TEAMS`.`team_id`))
  WHERE `SPORTS_NFL_SCHEDULE`.`game_id` IS NULL
  GROUP BY `SPORTS_NFL_TEAMS`.`team_id`
ON DUPLICATE KEY UPDATE `week` = VALUES(`week`);\n\n";
  } else {
    # Playoff Bye Weeks
    print "DELETE FROM `SPORTS_NFL_SCHEDULE_BYES` WHERE `season` = '$season' AND `game_type` = 'playoff';\n";
    print "INSERT IGNORE INTO `SPORTS_NFL_SCHEDULE_BYES` (`season`, `game_type`, `team_id`, `week`)
  SELECT `season`, 'playoff' AS `game_type`, `team_id`, 1 AS `week`
  FROM `SPORTS_NFL_STANDINGS`
  WHERE `season` = '$season'
  AND   `week` = '$reg_weeks'
  AND   `pos_conf` <= IF(`season` <= 2019, 2, 1)
  ORDER BY `team_id`;\n\n";
  }

  # Calculate week date ranges
  print "# Week dates\n";
  print "DROP TEMPORARY TABLE IF EXISTS `tmp_dates`;\n";
  print "CREATE TEMPORARY TABLE `tmp_dates` (
  `season` YEAR NOT NULL,
  `game_type` VARCHAR(10),
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `max_date` DATE,
  `max_date_dow` TINYINT(1) UNSIGNED NOT NULL,
  `start_date` DATE,
  `end_date` DATE,
  PRIMARY KEY (`season`, `game_type`, `week`)
) SELECT `season`, `game_type`, `week`,
         MAX(`game_date`) AS `max_date`,
         DAYOFWEEK(MAX(`game_date`)) AS `max_date_dow`,
         NULL AS `start_date`,
         NULL AS `end_date`
  FROM `SPORTS_NFL_SCHEDULE`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  GROUP BY `week`;\n";
  print "UPDATE `tmp_dates` SET `end_date` = DATE_ADD(`max_date`, INTERVAL IF(`max_date_dow` > 3, 10, 3) - `max_date_dow` DAY);\n\n";

  print "DROP TEMPORARY TABLE IF EXISTS `tmp_dates_cp`;\n";
  print "CREATE TEMPORARY TABLE `tmp_dates_cp` LIKE `tmp_dates`;\n";
  print "INSERT INTO `tmp_dates_cp` SELECT * FROM `tmp_dates`;\n\n";

  print "UPDATE `tmp_dates`
LEFT JOIN `tmp_dates_cp`
  ON (`tmp_dates_cp`.`season` = `tmp_dates`.`season`
  AND `tmp_dates_cp`.`game_type` = `tmp_dates`.`game_type`
  AND `tmp_dates_cp`.`week` = `tmp_dates`.`week` - 1)
SET `tmp_dates`.`start_date` = IFNULL(DATE_ADD(`tmp_dates_cp`.`end_date`, INTERVAL 1 DAY),
                                      DATE_SUB(`tmp_dates`.`end_date`, INTERVAL 6 DAY));\n\n";

  print "INSERT INTO `SPORTS_NFL_SCHEDULE_WEEKS` (`season`, `game_type`, `week`, `start_date`, `end_date`)
  SELECT `season`, `game_type`, `week`, `start_date`, `end_date`
  FROM `tmp_dates`
ON DUPLICATE KEY UPDATE `start_date` = VALUES(`start_date`),
                        `end_date` = VALUES(`end_date`);\n\n";

  # Power Rank calc weeks
  # - The Tuesday following the games
  print "# Power Ranks\n";
  print "INSERT INTO `SPORTS_NFL_POWER_RANKINGS_WEEKS` (`season`, `game_type`, `week`, `calc_date`)
  SELECT `season`, `game_type`, `week`, `end_date` AS `calc_date`
  FROM `tmp_dates`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
ON DUPLICATE KEY UPDATE `calc_date` = VALUES(`calc_date`);\n\n";

  # Initial standings
  if ($game_type eq 'regular' && $dated_file =~ /initial/) {
    print "# Initial standings\n";
    print "CALL nfl_standings_initial('$season');\n\n";
  }

  # General tidy up...
  print "# Tidy the database\n";
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE` ORDER BY `season`, `game_type`, `week`, `game_date`, `game_time`, `nfl_id`;\n";
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE_BYES` ORDER BY `season`, `game_type`, `team_id`;\n" if ($game_type eq 'regular');
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE_WEEKS` ORDER BY `season`, `game_type`, `week`;\n";
  print "ALTER TABLE `SPORTS_NFL_POWER_RANKINGS_WEEKS` ORDER BY `season`, `game_type`, `week`;\n";
}

sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @html = <FILE>;
  close FILE;
  return join('', @html);
}

sub parse_date {
  my ($year, $month_txt, $day) = @_;
  # Convert the textual month to a numeric equivalent
  my $month;
  switch (lc $month_txt) {
    case 'august'    { $month =  8; }
    case 'september' { $month =  9; }
    case 'october'   { $month = 10; }
    case 'november'  { $month = 11; }
    case 'december'  { $month = 12; }
    case 'january'   { $month =  1; }
    case 'february'  { $month =  2; }
    case 'march'     { $month =  3; }
  }
  $year++ if $month < 6;
  return sprintf('%04d-%02d-%02d', $year, $month, $day);
}
