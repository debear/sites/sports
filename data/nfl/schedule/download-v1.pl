#!/usr/bin/perl -w
# Get the schedule for a season from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $mode, $date, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir() . $dated_file;

# Get the vars to pass to the web request
my @requests = ( );
push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# URL args
my @time = localtime();
my $season_diff = ($time[4] > 3 ? 1900 : 1899) + $time[5] - $season;

# Determine which week we're downloading from mid-season
my $week = 1;
if ($date ne '') {
  my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
  my $sql = 'SELECT game_type, week FROM SPORTS_NFL_SCHEDULE_WEEKS WHERE season = ? AND end_date > ? ORDER BY end_date LIMIT 1;';
  my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $date);
  $dbh->disconnect if defined($dbh);
  # Is this date usable? (Only applies to the regular season, and if a row was found)
  $week = $$ret[0]{'week'}
    if defined($$ret[0]{'week'}) && ($$ret[0]{'game_type'} eq 'regular');
}

# Create dated folder?
mkdir $config{'base_dir'}
  if ! -e $config{'base_dir'};

# Get the list
foreach my $game_type (@requests) {

  # Form the base URL
  my $base_url = "http://nflcdns.nfl.com/schedules/$season/" . ($game_type eq 'regular' ? 'REG' : 'POST');

  # Loop through each week and download
  for (my $i = $week; $i <= ($game_type eq 'regular' ? 17 : 1); $i++) {
    my $url = $base_url . ($game_type eq 'regular' ? $i : '');
    my $file = $config{'base_dir'} . '/' . $game_type  . ($game_type eq 'regular' ? sprintf('%02d', $i) : '') . '.htm.gz';

    # Skip if already downloaded
    download($url, $file, {'skip-today' => 1, 'gzip' => 1});
  }
}
