#!/usr/bin/perl -w
# Parse the schedule for a season from NFL.com

use strict;
use utf8;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $mode, $date, $dated_file) = parse_args();
$config{'data_dir'} = $config{'base_dir'} . '/_data';
$config{'base_dir'} = get_base_dir();

# Load in alternate venue info
my %alt_venues = ( );
my $alt_file = "$config{'base_dir'}/../../schedule/${season}_alt_venues.csv";
if ( -e $alt_file ) {
  open FILE, "<$alt_file";
  my @alt = <FILE>;
  close FILE;

  foreach my $line (@alt) {
    chomp($line);
    my ($s, $st, $w, $v, $h, $a) = split(/,/, $line);
    $alt_venues{"$st/$w/$v/$h"} = $a;
  }

  @alt = ( ); # Homegrown garbage collect...
}

# Get and loop through the list of files
my @sched_list = ();
foreach my $game_type ('regular', 'playoff') {
  # Ignore?
  next if !grep /^$mode$/, ($game_type, 'both');

  # Load, parse, process
  foreach my $file (glob("$config{'base_dir'}/$dated_file/$game_type*.htm.gz")) {
    my $week;
    ($week) = ($file =~ m/(?:regular|playoff)0?(\d+)\.htm.gz$/si)
      if $game_type eq 'regular';

    print "##\n## Loading: $file (" . ($game_type eq 'regular' ? "Regular Season, Week $week" : 'Playoffs') . ")\n##\n";

    # Load the file
    open FILE, "gzip -dc $file |";
    my @html = <FILE>;
    close FILE;
    my $html = join('', @html);

    # Radio times indicate game time in ET)
    my ($times) = ($html =~ m/<!-- radioList: .*? -->/gsi);

    # Skip the Next Game section
    $html =~ s/<li class="schedules-list-date next-game".*?<\/li>\s*<li.*?<\/li>//gsi;

    # Get the game dates
    my @dates = ($html =~ m/<!-- gameKey\.key: (\d{4}-\d{2}-\d{2}) -->/gsi);

    # Get info on the match-ups this week
    my $game_num = 0; my $last_week;
    my @game_info = ($html =~ m/<li class="schedules-list-.*?<\/li>/gsi);
    foreach my $info (@game_info) {
      # What row have we found?
      ## Game date separator
      next if $info =~ m/schedules-list-date/gsi;

      ## A game
      # If the playoffs, which round?
      if ($game_type eq 'playoff') {
        my ($round_css) = ($info =~ m/<li class="schedules-list-matchup[^"]* type-([^ ]+)[^"]*">/gsi);
        if ($round_css eq 'wc') {
          $week = 1;
        } elsif ($round_css eq 'div') {
          $week = 2;
        } elsif ($round_css eq 'con') {
          $week = 3;
        } elsif ($round_css eq 'sb') {
          $week = 4;
        } else {
          # Skip the Pro Bowl
          next;
        }

        # Reset the game counter if displaying a new round
        $game_num = 0 if defined($last_week) && $week != $last_week;
        $last_week = $week;
      }

      # Game ID
      my ($game_id) = ($info =~ m/<div class="schedules-list-content[^"]*?" data-gameid="(\d{4}\d{2}\d{2}\d{2})"/si);

      # Build the game date using the Game ID
      my $game_date = substr($game_id, 0, 4) . '-' . substr($game_id, 4, 2) . '-' . substr($game_id, 6, 2);

      # Teams
      my ($visitor, $home, $visitor_name, $home_name) = ($info =~ m/data-away-abbr="(\S{2,3})" data-home-abbr="(\S{2,3})" data-away-mascot="([^"]+)" data-home-mascot="([^"]+)"/si);
      $visitor = '' if !defined($visitor);
      $home = '' if !defined($home);
      # Radio List team name fix
      $visitor_name = 'Redskins' if $season == 2020 && $visitor_name eq 'Football Team';
      $home_name = 'Redskins' if $season == 2020 && $home_name eq 'Football Team';

      # Skip if info shown for a future week (e.g., We're at the Wild Card round, it still lists the teams with a Bye as [AN]FC @ BYE)
      if ($visitor eq 'AFC' || $visitor eq 'NFC' || $visitor eq 'TBD' || ($visitor eq '' && $home eq '')) {
        next;
      }

      # Time
      my ($time_h, $time_m, $time_tod) = ($times =~ m/(\d{1,2}):(\d{2})_([AP]M) \|${visitor_name}_${home_name}\|/gsi);

      # Participants
      $visitor = convert_team_id($visitor);
      $home = convert_team_id($home);

      # Alternate venue?
      my $alt_venue = undef;
      $alt_venue = $alt_venues{"$game_type/$week/$visitor/$home"} if defined($alt_venues{"$game_type/$week/$visitor/$home"});
      $alt_venue = $alt_venues{"$game_type/$week//"} if !defined($alt_venue) && defined($alt_venues{"$game_type/$week//"});
      if (defined($alt_venue)) {
        $alt_venue = "'" . convert_text($alt_venue) . "'";
      } else {
        $alt_venue = 'NULL';
      }

      # Now form into some SQL
      my $seq_id = ($week * 100) +  ++$game_num;
      print "# Game $game_id, $visitor @ $home\n";
      print "SET \@game_id := NULL;\n"; # Re-set as if new game, INSERT will over-write the previous game
      print "SELECT `game_id` INTO \@game_id
FROM SPORTS_NFL_SCHEDULE
WHERE `season` = '$season'
AND   `game_type` = '$game_type'
AND   `week` = '$week'
AND   `home` = '$home'
AND   `visitor` = '$visitor'
LIMIT 1;\n";
      print "INSERT INTO `SPORTS_NFL_SCHEDULE` (`season`, `game_type`, `week`, `game_id`, `nfl_id`, `game_date`, `game_time`, `visitor`, `home`, `alt_venue`)
                           VALUES ('$season', '$game_type', '$week', IFNULL(\@game_id, '$seq_id'), '$game_id', '$game_date'"
                                      . ", '" . sprintf('%02d', $time_h + ($time_tod eq 'PM' && $time_h < 12 ? 12 : 0)) . ':' . $time_m . ':00'
                                      . "', '" . $visitor . "', '" . $home . "', $alt_venue)
ON DUPLICATE KEY UPDATE `nfl_id` = VALUES(`nfl_id`),
                        `game_date` = VALUES(`game_date`),
                        `game_time` = VALUES(`game_time`),
                        `alt_venue` = VALUES(`alt_venue`);\n\n";
    }
  }

  # Identify bye weeks
  if ($game_type eq 'regular') {
    print "# Bye weeks...\n";
    print "DROP TEMPORARY TABLE IF EXISTS `tmp_weeks`;\n";
    print "CREATE TEMPORARY TABLE `tmp_weeks` (
  `week` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY(`week`)
);\n";
    print "INSERT INTO `tmp_weeks` (`week`) VALUES";
    for (my $i = 1; $i <= 17; $i++) {
      print ',' if $i > 1;
      print " ($i)";
    }
    print ";\n";

    print "INSERT INTO `SPORTS_NFL_SCHEDULE_BYES` (`season`, `game_type`, `team_id`, `week`)
  SELECT '$season' AS `season`, 'regular' AS `game_type`, `SPORTS_NFL_TEAMS`.`team_id`, `tmp_weeks`.`week`
  FROM `SPORTS_NFL_TEAMS`
  JOIN `SPORTS_NFL_TEAMS_GROUPINGS`
    ON (`SPORTS_NFL_TEAMS_GROUPINGS`.`team_id` = `SPORTS_NFL_TEAMS`.`team_id`
    AND '$season' BETWEEN `SPORTS_NFL_TEAMS_GROUPINGS`.`season_from` AND IFNULL(`SPORTS_NFL_TEAMS_GROUPINGS`.`season_to`, 2099))
  JOIN `tmp_weeks` ON (1 = 1)
  LEFT JOIN `SPORTS_NFL_SCHEDULE`
    ON (`SPORTS_NFL_SCHEDULE`.`season` = '$season'
    AND `SPORTS_NFL_SCHEDULE`.`game_type` = '$game_type'
    AND `SPORTS_NFL_SCHEDULE`.`week` = `tmp_weeks`.`week`
    AND (`SPORTS_NFL_SCHEDULE`.`visitor` = `SPORTS_NFL_TEAMS`.`team_id`
      OR `SPORTS_NFL_SCHEDULE`.`home` = `SPORTS_NFL_TEAMS`.`team_id`))
  WHERE `SPORTS_NFL_SCHEDULE`.`game_id` IS NULL
  GROUP BY `SPORTS_NFL_TEAMS`.`team_id`
ON DUPLICATE KEY UPDATE `week` = VALUES(`week`);\n\n";
  } else {
    # Playoff Bye Weeks
    print "INSERT IGNORE INTO `SPORTS_NFL_SCHEDULE_BYES` (`season`, `game_type`, `team_id`, `week`)
  SELECT `season`, 'playoff' AS `game_type`, `team_id`, 1 AS `week`
  FROM `SPORTS_NFL_STANDINGS`
  WHERE `season` = '$season'
  AND   `week` = 17
  AND   `pos_conf` <= IF(`season` <= 2019, 2, 1)
  ORDER BY `team_id`;\n\n";
  }

  # Calculate week date ranges
  print "# Week dates\n";
  print "DROP TEMPORARY TABLE IF EXISTS `tmp_dates`;\n";
  print "CREATE TEMPORARY TABLE `tmp_dates` (
  `season` YEAR NOT NULL,
  `game_type` VARCHAR(10),
  `week` TINYINT(1) UNSIGNED NOT NULL,
  `max_date` DATE,
  `max_date_dow` TINYINT(1) UNSIGNED NOT NULL,
  `start_date` DATE,
  `end_date` DATE,
  PRIMARY KEY (`season`, `game_type`, `week`)
) SELECT `season`, `game_type`, `week`,
         MAX(`game_date`) AS `max_date`,
         DAYOFWEEK(MAX(`game_date`)) AS `max_date_dow`,
         NULL AS `start_date`,
         NULL AS `end_date`
  FROM `SPORTS_NFL_SCHEDULE`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
  GROUP BY `week`;\n";
  print "UPDATE `tmp_dates` SET `end_date` = DATE_ADD(`max_date`, INTERVAL IF(`max_date_dow` > 3, 10, 3) - `max_date_dow` DAY);\n\n";

  print "DROP TEMPORARY TABLE IF EXISTS `tmp_dates_cp`;\n";
  print "CREATE TEMPORARY TABLE `tmp_dates_cp` LIKE `tmp_dates`;\n";
  print "INSERT INTO `tmp_dates_cp` SELECT * FROM `tmp_dates`;\n\n";

  print "UPDATE `tmp_dates`
LEFT JOIN `tmp_dates_cp`
  ON (`tmp_dates_cp`.`season` = `tmp_dates`.`season`
  AND `tmp_dates_cp`.`game_type` = `tmp_dates`.`game_type`
  AND `tmp_dates_cp`.`week` = `tmp_dates`.`week` - 1)
SET `tmp_dates`.`start_date` = IFNULL(DATE_ADD(`tmp_dates_cp`.`end_date`, INTERVAL 1 DAY),
                                      DATE_SUB(`tmp_dates`.`end_date`, INTERVAL 6 DAY));\n\n";

  print "INSERT INTO `SPORTS_NFL_SCHEDULE_WEEKS` (`season`, `game_type`, `week`, `start_date`, `end_date`)
  SELECT `season`, `game_type`, `week`, `start_date`, `end_date`
  FROM `tmp_dates`
ON DUPLICATE KEY UPDATE `start_date` = VALUES(`start_date`),
                        `end_date` = VALUES(`end_date`);\n\n";

  # Power Rank calc weeks
  # - The Tuesday following the games
  print "# Power Ranks\n";
  print "INSERT INTO `SPORTS_NFL_POWER_RANKINGS_WEEKS` (`season`, `game_type`, `week`, `calc_date`)
  SELECT `season`, `game_type`, `week`, `end_date` AS `calc_date`
  FROM `tmp_dates`
  WHERE `season` = '$season'
  AND   `game_type` = '$game_type'
ON DUPLICATE KEY UPDATE `calc_date` = VALUES(`calc_date`);\n\n";

  # Initial standings
  if ($game_type eq 'regular' && $dated_file =~ /initial/) {
    print "# Initial standings\n";
    print "CALL nfl_standings_initial('$season');\n\n";
  }

  # General tidy up...
  print "# Tidy the database\n";
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE` ORDER BY `season`, `game_type`, `week`, `game_date`, `game_time`, `nfl_id`;\n";
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE_BYES` ORDER BY `season`, `game_type`, `team_id`;\n" if ($game_type eq 'regular');
  print "ALTER TABLE `SPORTS_NFL_SCHEDULE_WEEKS` ORDER BY `season`, `game_type`, `week`;\n";
  print "ALTER TABLE `SPORTS_NFL_POWER_RANKINGS_WEEKS` ORDER BY `season`, `game_type`, `week`;\n";
  print "\n";
}

