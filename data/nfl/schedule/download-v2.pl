#!/usr/bin/perl -w
# Get the schedule for a season from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

# Parse the arguments
our ($season, $mode, $date, $dated_file) = parse_args();
$config{'base_dir'} = get_base_dir() . $dated_file;

# Get the vars to pass to the web request
my @requests = ( );

push @requests, 'regular' if grep /^$mode$/, ('regular', 'both');
push @requests, 'playoff' if grep /^$mode$/, ('playoff', 'both');

# URL args
my @time = localtime();
my $season_diff = ($time[4] > 3 ? 1900 : 1899) + $time[5] - $season;

# Create dated folder?
mkdir $config{'base_dir'}
  if ! -e $config{'base_dir'};

# Get the list
my $reg_weeks = ($season < 2021 ? 17 : 18);
foreach my $game_type (@requests) {
  # Form the base URL
  my $base_url = 'https://nflcdns.nfl.com/ajax/scorestrip?season=' . $season . '&seasonType=' . ($game_type eq 'regular' ? 'REG' : 'POST') . '&week=';

  # Determine which weeks we're downloading from and to
  my $week_from = 1;
  my $week_to = ($game_type eq 'regular' ? $reg_weeks : 1);
  if ($date ne '') {
    my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
    # Get regular season range from our list of weeks with dates
    if ($game_type eq 'regular') {
      # Get from our list of weeks with dates
      my $sql = 'SELECT game_type, week FROM SPORTS_NFL_SCHEDULE_WEEKS WHERE season = ? AND end_date > ? ORDER BY end_date LIMIT 1;';
      my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $date);
      # Is this date usable?
      $week_from = $$ret[0]{'week'} if defined($$ret[0]{'week'});

    # Get playoff range from the list of scheduled and completed playoff games
    } elsif ($game_type eq 'playoff') {
      my $sql = 'SELECT MAX(week) AS max_week, MAX(IF(status IS NULL, week, NULL)) AS max_unplayed FROM SPORTS_NFL_SCHEDULE WHERE season = ? AND game_type = "playoff";';
      my $ret = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
      if (!defined($$ret[0]{'max_week'})) {
        # No playoff games, so this is the first week
        $week_from = 1;
      } elsif (defined($$ret[0]{'max_unplayed'})) {
        # We have an incomplete week, so re-parse that week
        $week_from = $$ret[0]{'max_unplayed'};
      } else {
        # Parsing the week after the completed round (but no later than Week 4 == Super Bowl)
        $week_from = max($$ret[0]{'max_week'} + 1, 4);
      }
      # Playoff weeks are loaded individually, so 'from' == 'to'
      $week_to = $week_from;
    }
    $dbh->disconnect if defined($dbh);
  }

  # Loop through each week and download
  for (my $i = $week_from; $i <= $week_to; $i++) {
    my $week_num = $i;
    if ($game_type eq 'playoff') {
      # First playoff round is Week 18
      $week_num += $reg_weeks;
      # Though the Super Bowl is Week 22 (Week 21 == Pro Bowl)
      $week_num++ if $i == 4;
    }
    my $url = $base_url . $week_num;
    my $file = $config{'base_dir'} . '/' . $game_type  . sprintf('%02d', $i) . '.xml.gz';

    # Skip if already downloaded
    download($url, $file, {'skip-today' => 1, 'gzip' => 1});
  }
}
