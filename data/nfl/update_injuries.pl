#!/usr/bin/perl -w
# Download, import and process the injury report from NFL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'injuries';
$config{'log_inc_time'} = '%H%M';
my %logs = get_log_def();

# Identify season (no need to ask, do it from dates)
my @time = localtime();
my $season = ($time[4] > 3 ? 1900 : 1899) + $time[5];

# Identify week to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT game_type, week
FROM SPORTS_NFL_SCHEDULE_WEEKS
WHERE season = ?
AND   IF(DAYOFWEEK(CURDATE()) = 3, DATE_ADD(CURDATE(), INTERVAL 1 DAY), CURDATE()) BETWEEN start_date AND end_date;';
my $sth = $dbh->prepare($sql);
$sth->execute($season);
my ($game_type, $week) = $sth->fetchrow_array;

undef $sth if defined($sth);
$dbh->disconnect if defined($dbh);

# If nothing returned, not time to run this
if (!defined($game_type)) {
  print STDERR "Not a game week yet, exiting...\n";
  exit;
}

## Inform user
print "\nAcquiring data from $season, $game_type // $week\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Download, if we haven't done so already (handled in-script)
#
print '=> Download: ';
command("$config{'base_dir'}/injuries/download.pl $season $game_type $week",
        "$config{'log_dir'}/$logs{'download'}.log",
        "$config{'log_dir'}/$logs{'download'}.err");
done(0);

# If only downloading, go no further
end_script() if download_only();

#
# Parse
#
print '=> Parse: ';
command("$config{'base_dir'}/injuries/parse.pl $season $game_type $week",
        "$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'parse'}.err");
done(3);

#
# Import into the database
#
print '=> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'parse'}.sql",
        "$config{'log_dir'}/$logs{'import'}.log",
        "$config{'log_dir'}/$logs{'import'}.err");
done(2);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'download' => '01_download',
    'parse'    => '02_parse',
    'import'   => '03_import',
  );
}
