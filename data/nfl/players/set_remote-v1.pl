#!/usr/bin/perl -w
# Complete the remote ID set for a single player from NFL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be two: player_id, remote_profile_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to the get remote ID script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_profile_id) = @ARGV;
# Dir containing our player lists
$config{'remote_dir'} = "$config{'base_dir'}/_data/players/remote_sync";
mkdir $config{'remote_dir'}
  if ! -e $config{'remote_dir'};
# Player's specific data dir
$config{'player_dir'} = "$config{'base_dir'}/_data/players/$remote_profile_id";
mkdir $config{'remote_dir'}
  if ! -e $config{'player_dir'};
# Default download options
$config{'dl_opt'} = {'skip-today' => 1, 'if-modified-since' => 1, 'gzip' => 1};
# New additions we need to write to the cache
$config{'new_cache'} = [];

# Do not proceed if already set (also acts as validation)
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT remote_url_id, remote_url_name
FROM SPORTS_NFL_PLAYERS_IMPORT
WHERE player_id = ?
AND   remote_profile_id = ?;';
my $sth = $dbh->prepare($sql);
$sth->execute($player_id, $remote_profile_id);
my $import = $sth->fetchrow_hashref;
$sth->finish if defined($sth);
if (!defined($import)) {
  print STDERR "Unknown player_id / remote_profile_id combination ($player_id / $remote_profile_id).\n";
  exit_process(98);
} elsif (defined($$import{'remote_url_id'}) && defined($$import{'remote_url_name'})) {
  print STDERR "Remote IDs have already been set for this player_id / remote_profile_id combination ($player_id / $remote_profile_id).\n";
  exit_process(98);
}

print "##\n## Identifying missing remote IDs for $player_id / $remote_profile_id\n##\n\n";

# Prepare our database queries
$sql = 'SELECT player_id FROM SPORTS_NFL_PLAYERS_IMPORT WHERE remote_url_id = ?;';
my $sth_imported = $dbh->prepare($sql);
$sql = 'UPDATE SPORTS_NFL_PLAYERS_IMPORT SET remote_url_id = ?, remote_url_name = ? WHERE player_id = ? AND remote_profile_id = ?;';
my $sth_update = $dbh->prepare($sql);

# Easy check - have we already cached the result?
my $cache = load_file("$config{'remote_dir'}/cache.csv.gz");
my %cache = ();
foreach my $row (split("\n", $cache)) {
  my ($rpi, $rui, $run) = split(',', $row);
  $cache{$rpi} = {
    'remote_url_id' => $rui,
    'remote_url_name' => $run,
  };
  $cache{$rui} = $rpi;
}
if (defined($cache{$remote_profile_id})) {
  print "# Matched to $cache{$remote_profile_id}{'remote_url_id'} / $cache{$remote_profile_id}{'remote_url_name'} from local cache.\n";
  $sth_update->execute($cache{$remote_profile_id}{'remote_url_id'}, $cache{$remote_profile_id}{'remote_url_name'}, $player_id, $remote_profile_id);
  exit_process();
}

# No, so start scanning the players-by-surname pages for occurrences of the surname
my $letter = lc substr($remote_profile_id, 0, 1);
my $surname = ucfirst lc $remote_profile_id; $surname =~ s/[^a-z]//gi;
my $num_pages = download_players($letter);
print "\n";
for (my $pg = 1; $pg <= $num_pages; $pg++) {
  # Load the page
  my $page = load_file("$config{'remote_dir'}/$letter-pg$pg.htm.gz");
  # Scan for matches
  my @matches = ($page =~ m/<a href="\/player\/([^\/]+)\/(\d+)\/profile">($surname.*?), (.*?)<\/a>.*?<td class="tbdy">(.*?)<\/td>/gsi);
  while (my ($remote_url_name, $remote_url_id, $sname, $fname, $status) = splice(@matches, 0, 5)) {
    print "# Found: $fname $sname ($remote_url_name/$remote_url_id - $status) on Page $pg: ";
    # We're looking for active players only
    if ($status =~ /^(CUT|NWT)$/) {
      print "[ Inavlid Status ]\n";
      next;
    }
    # Previously imported?
    $sth_imported->execute($remote_url_id);
    my $exists = $sth_imported->fetchrow_hashref;
    if (defined($exists)) {
      print "[ Already Imported ]\n";
      next;
    }
    # Previously converted and cached?
    my $esb_id;
    if (defined($cache{$remote_url_id})) {
      $esb_id = $cache{$remote_url_id};
      print "[ Cached ] :: ";
    } else {
      # No, so download the player and check
      my $url = "http://nflcdns.nfl.com/player/$remote_url_name/$remote_url_id/profile";
      my $profile = download($url);
      ($esb_id) = ($profile =~ m/ESB ID: (\S+)\s/gsi);
      # Cache a found value?
      if (defined($esb_id) && $esb_id) {
        my $player_dir = "$config{'base_dir'}/_data/players/$esb_id";
        mkdir $player_dir
          if ! -e $player_dir;
        write_file("$player_dir/profile.htm.gz", $profile);
      }
    }
    if (!defined($esb_id) || !$esb_id) {
      # No remote_profile_id in the player's profile
      print "[ Skipping, No ESB ID ]\n";
      next;
    } elsif ($remote_profile_id ne $esb_id) {
      # Does not match what we're after
      print "[ Different ID ] ($esb_id)\n";
      # Store for future calcs?
      push @{$config{'new_cache'}}, {
        'remote_profile_id' => $esb_id,
        'remote_url_id' => $remote_url_id,
        'remote_url_name' => $remote_url_name,
      } if !defined($cache{$remote_url_id});
      next;
    }
    # Matches!
    $sth_update->execute($remote_url_id, $remote_url_name, $player_id, $remote_profile_id);
    print "[ Match Found ]\n";
    exit_process();
  }
}
# No match found, state an error
print STDERR "Unable to find match for $player_id / $remote_profile_id\n";
exit_process(20);

# Tidy up, given our multiple exit points
sub exit_process {
  my ($exit_code) = @_;
  $exit_code = 0 if !defined($exit_code);

  # Write any new caches to the file
  if (@{$config{'new_cache'}}) {
    my $extra = '';
    foreach my $row (@{$config{'new_cache'}}) {
      $extra .= "$$row{'remote_profile_id'},$$row{'remote_url_id'},$$row{'remote_url_name'}\n";
    }
    write_file("$config{'remote_dir'}/cache.csv.gz", $extra, 1);
  }

  # Close any remaining database handles
  $sth_imported->finish if defined($sth_imported);
  $sth_update->finish if defined($sth_update);
  $dbh->disconnect if defined($dbh);
  exit $exit_code;
}

# Download all the player pages by surname
sub download_players {
  my ($letter) = @_;

  my $domain = "http://nflcdns.nfl.com";
  # Download the first page
  my $url = "$domain/players/search?category=lastName&filter=$letter&playerType=current";
  my $local = "$config{'remote_dir'}/$letter-pg1.htm.gz";
  print "# Downloading player list for '$letter' (from $url): ";
  my $ret = download($url, $local, $config{'dl_opt'});
  # Validation
  if (!$ret) {
    print "[ Failed ]\n";
    print STDERR "Unable to download the first page of player list for '$letter'.\n";
    return;
  } elsif ($ret == 1) {
    print "[ Done ]\n";
  } else {
    print "[ Skipped ]\n";
  }

  # And then use this to determine what other pages we should download
  my $page0 = load_file($local);
  my $num_pages = 1;
  my ($links) = ($page0 =~ m/\s<span class="linkNavigation floatRight">(.*?)<\/span>\s/gsi);
  $links = '' if !defined($links); # Ensure we have a regex-able content
  my @pages = ($links =~ m/<a href="(\/players\/search\?category=lastName&amp;playerType=current.*?p=\d+&amp;filter=$letter)" title="Go to page \d">(\d)<\/a>/gsi);
  while (my ($page_url, $page_num) = splice(@pages, 0, 2)) {
    $page_url =~ s/&amp;/&/g;
    $page_url = "$domain$page_url";
    print "# Downloading page $page_num of player list '$letter' (from $page_url): ";
    $ret = download($page_url, "$config{'remote_dir'}/$letter-pg$page_num.htm.gz", $config{'dl_opt'});
    # Validation
    if (!$ret) {
      print "[ Failed ]\n";
      print STDERR "Unable to download page $page_num of player list for '$letter'.\n";
      return;
    } elsif ($ret == 1) {
      print "[ Done ]\n";
    } else {
      print "[ Skipped ]\n";
    }
    $num_pages++;
  }

  return $num_pages;
}

# Load a file from disk
sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...
  return $contents;
}

# Save a file to disk
sub write_file {
  my ($file, $contents, $append) = @_;
  my $mode = (!defined($append) || !$append ? '>' : '>>');
  open FILE, '|-', "gzip $mode $file";
  print FILE $contents;
  close FILE;
}
