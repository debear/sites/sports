#!/usr/bin/perl -w
# Load the info for an individual player
#  - Currently only pull out career stats, as profile info is taken from the Gamecenter file upon player creation

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be two: player_id, remote_profile_id
if (@ARGV < 2) {
  print STDERR "Insufficient arguments to player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_profile_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_profile_id";

# Validate...
if (! -e $config{'dir'}) {
  print STDERR "# Unable to parse player $player_id\n";
  exit 10;
}

# Load the files
my $profile = load_file('profile');
my $career = load_file('career');

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

print "#\n# Parsing player info for $player_id\n#\n\n";

# Bio details
parse_bio() if check_disp(1);

# Career Stats
parse_career() if check_disp(2);

# Player IDs
parse_ids() if check_disp(3);

print "#\n# Completed parsing player info for $player_id\n#\n";

# Get extra info out about the player
sub parse_bio {
  # Height
  my ($height) = ($profile =~ m/<strong>Height<\/strong>: (\d+-\d+) /gsi);
  if (defined($height)) {
    my ($ft, $in) = ($height =~ m/^(\d+)-(\d+)$/gsi);
    $height = "'" . ((12 * $ft) + $in) . "'";
  } else {
    $height = 'NULL';
  }

  # Weight
  my ($weight) = ($profile =~ m/<strong>Weight<\/strong>: (\d+) /gsi);
  if (defined($weight)) {
    $weight = "'$weight'";
  } else {
    $weight = 'NULL';
  }

  # College
  my ($college) = ($profile =~ m/<strong>College<\/strong>: (.*?)<\/p>/gsi);
  if (defined($college)) {
    $college = convert_text($college)
      if $college =~ m/'/gsi;
    $college = "'$college'";
  } else {
    $college = 'NULL';
  }

  # DoB / Birthplace / State
  my ($dob_m, $dob_d, $dob_y, $pob) = ($profile =~ /<strong>Born<\/strong>: (\d+)\/(\d+)\/(\d+) (.*?)<\/p>/gsi);

  my $dob = 'NULL';
  if (defined($dob_m)) {
    $dob = sprintf("'%04d-%02d-%02d'", $dob_y, $dob_m, $dob_d);
  }

  my $birthplace = 'NULL';
  my $birthplace_secondary;
  my $birthplace_state = 'NULL';
  my $birthplace_country = 'NULL';
  if (defined($pob)) {
    # Main (town?) part
    $pob = convert_text($pob)
      if $pob =~ m/'/gsi;
    ($birthplace, $birthplace_secondary) = ($pob =~ m/^(.+) ,? (.+)$/gsi);
    if (defined($birthplace) && $birthplace !~ /^\s*$/) {
      $birthplace =~ s/^\s+//;
      $birthplace =~ s/\s+$//;
      $birthplace = "'$birthplace'";
    } else {
      $birthplace = 'NULL';
    }
    # Secondary location, either a US state (2 upper case char) or a country
    if (defined($birthplace_secondary) && $birthplace_secondary !~ /^\s*$/) {
      $birthplace_secondary =~ s/^\s+//;
      $birthplace_secondary =~ s/\s+$//;
      if ($birthplace_secondary =~ /^[A-Z]{2}$/) {
        $birthplace_state = "'$birthplace_secondary'";
        # Canadian province/territory?
        $birthplace_country = "'Canada'"
          if grep(/^$birthplace_secondary$/, ( 'AB', 'BC', 'MB', 'NB', 'NL', 'NS', 'ON', 'PE', 'QC', 'SK', 'NT', 'NU', 'YT' ));
      } else {
        $birthplace_country = "'$birthplace_secondary'";
      }
    }
 }

  print "# Completing player Biography\n";
  print "UPDATE SPORTS_NFL_PLAYERS
SET dob = $dob,
    birthplace = $birthplace,
    birthplace_state = $birthplace_state,
    birthplace_country = $birthplace_country,
    height = $height,
    weight = $weight,
    college = $college
WHERE player_id = '$player_id';\n\n";
}

# Convert the career tables into SQL
sub parse_career {
  print "# Career stats\n";

  # First, get the data..
  my %career = ( );
  foreach my $stats ($career =~ m/(<table.*?class="data-table1" width="100%".*?summary="Career Stats[^"]+">\s+<thead>.*?<\/thead>\s+<tbody>\s+<tr>.*?<\/tr>\s+<\/tbody>\s+<\/table>)/gsi) {
    my ($type) = ($stats =~ m/summary="Career Stats In (.*?) For /gsi);
    ($stats) = ($stats =~ m/<tbody>\s+(<tr>.*?<\/tr>)\s+<\/tbody>/gsi);
    my @stats = ($stats =~ m/(<tr>\s+.*?\s+<\/tr>)/gsi);

    # Loop through and generate individual season's results
    foreach my $row (reverse @stats) {
      # Skip a total row
      next if $row =~ m/<td colspan="\d" class="player-totals">TOTAL<\/td>/gsi;
      my @cols = ($row =~ m/(<td[^>]*>\s*.*?\s*<\/td>)/gsi);

      # Establish base info
      my ($season, $team_id) = splice(@cols, 0, 2);
      ($season) = ($season =~ m/<td[^>]*>(\d+)<\/td>/gsi);
      ($team_id) = ($team_id =~ m/<td[^>]*>\s+<a href="\/teams\/[^\/]+\/profile\?team=([^"]+)"/gsi);

      # Skip totals 2009 or later, when we started processing data
      next if $season >= 2009;
      $career{$season} = { } if !defined($career{$season});
      %{$career{$season}{$team_id}} = ( 'misc' => { 'team_count' => scalar keys %{$career{$season}} } ) if !defined($career{$season}{$team_id});

      # Get info from the profile page for the number of Games Started this season
      ($career{$season}{$team_id}{'misc'}{'gs'})
        = ($profile =~ m/<tr[^>]*>\s*<td[^>]*>$season<\/td>\s*<td[^>]*>\s*<a href="\/teams\/[^\/]+\/profile\?team=$team_id"[^>]*>.*?<\/a>\s*<\/td>\s*<td[^>]*>[\s\d]+<\/td>\s*<td>\s*(\d{1,2})\s*<\/td>/si)
        if !defined($career{$season}{$team_id}{'misc'}{'gs'});

      # Stats depend on position
      parse_career_stats($career{$season}{$team_id}, \@cols, $type);
      parse_career_stats($career{$season}{$team_id}, \@cols, 'DefensivePassing') if $type eq 'Defensive';

      # Bespoke calcs?
      if ($type eq 'Field Goal Kickers') {
        # Field Goals from 0-19yds can be calced from the totals and other breakdowns
        my $a = $career{$season}{$team_id}{'kicking'};
        $$a{'fg_u20'} = undef2zero($$a{'fg_made'}) - undef2zero($$a{'fg_o50'}) - undef2zero($$a{'fg_u50'}) - undef2zero($$a{'fg_u40'}) - undef2zero($$a{'fg_u30'});
      }
    }
  }

  # Then display
  foreach my $season (sort keys %career) {
    foreach my $team_id (keys %{$career{$season}}) {
      print "#\n# Season $season, Team $team_id\n#\n";
      $career{$season}{$team_id}{'misc'}{'gs'} = 0 if !defined($career{$season}{$team_id}{'misc'}{'gs'});
      print "INSERT INTO `SPORTS_NFL_PLAYERS_SEASON` (`season`, `season_type`, `player_id`, `team_id`, `gp`, `gs`, `team_order`)
  VALUES ('$season', 'regular', '$player_id', '$team_id', '$career{$season}{$team_id}{'misc'}{'gp'}', '$career{$season}{$team_id}{'misc'}{'gs'}', '$career{$season}{$team_id}{'misc'}{'team_count'}')
ON DUPLICATE KEY UPDATE `gp` = VALUES(`gp`), `gs` = VALUES(`gs`), `team_order` = VALUES(`team_order`);\n\n";
      foreach my $type (keys %{$career{$season}{$team_id}}) {
        next if $type eq 'misc';
        my $stat_tbl = get_stat_info($type);
        my @cols = ( ); my @vals = ( ); my @update_cols = ( );
        foreach my $key (keys %{$career{$season}{$team_id}{$type}}) {
          push @cols, "`$key`";
          push @vals, "'$career{$season}{$team_id}{$type}{$key}'";
          push @update_cols, "`$key` = VALUES(`$key`)";
        }

        print "INSERT INTO `SPORTS_NFL_PLAYERS_SEASON_$stat_tbl` (`season`, `season_type`, `player_id`, `team_id`, " . join(', ', @cols) . ")
  VALUES ('$season', 'regular', '$player_id', '$team_id', " . join(', ', @vals) . ")
ON DUPLICATE KEY UPDATE " . join(', ', @update_cols) . ";\n\n";
      }
    }
  }
}

# Convert the table column list into our database columns
sub parse_career_stats {
  my ($arr, $cols, $type) = @_;
  my $key_type = '';
  my @keys = ( );

  # First 2 columns (in most situations) are GP / GS
  if (!defined($$arr{'misc'}{'gp'})) {
    ($$arr{'misc'}{'gp'}) = ($$cols[0] =~ m/<td[^>]*>\s*(\d+)\s*<\/td>/gsi);
  }

  # Passing
  if ($type eq 'Passing') {
    # Remaining stats:
    #  Att, Comp, Pct, Att / Gm, Yds, Avg, Yds / Gm, TD, TD Pct, Int, Int Pct, Long, Num 20+ Yds, Num 40+ Yds, Sacks, Sack Yds Lost, Rating
    $key_type = 'passing';
    @keys = ( 'atts', 'cmp', '', '', 'yards', '', '', 'td', '', 'int', '', 'long', '', '', 'sacked', 'sack_yards', 'rating' );

  # Rushing
  } elsif ($type eq 'Rushing') {
    # Remaining stats:
    #  Att, Att / Gm, Yds, Avg, Yds / Gm, TD, Long, 1st Down, 1st Down %, Num 20+ Yds, Num 40+ Yds, Fumbles
    $key_type = 'rushing';
    @keys = ( 'atts', '', 'yards', '', '', 'td', 'long', '', '', '', '', '' );

  # Receiving
  } elsif ($type eq 'Receiving') {
    # Remaining stats:
    #  Num, Yds, Avg, Yds / Gm, Long, TD, Num 20+, Num 40+, Num 1st Down, Fumbles
    $key_type = 'receiving';
    @keys = ( 'recept', 'yards', '', '', 'long', 'td', '', '', '', '' );

  # Defensive (Tackles)
  } elsif ($type eq 'Defensive') {
    # Remaining stats:
    #  Tackles: Combined, Ind, Assisted, Sacks, Safety
    #  Int: Pass Defensed, Int, TD, Yds, Avg, Long
    $key_type = 'tackles';
    @keys = ( '', 'tckl', 'asst', 'sacks', '',
              '', '', '', '', '', '' );

  # Defensive (Passing)
  } elsif ($type eq 'DefensivePassing') {
    # Remaining stats:
    #  Tackles: Combined, Ind, Assisted, Sacks, Safety
    #  Int: Pass Defensed, Int, TD, Yds, Avg, Long
    $key_type = 'passdef';
    @keys = ( '', '', '', '', '',
              'pd', 'int', 'int_td', 'int_yards', '', '' );

  # Fumbles
  } elsif ($type eq 'Fumbles') {
    # Remaining stats:
    #  Num, Lost, Forced, Recovered (Own), Recovered (Opp), Yds (Own Rec), Yds (Opp Rec), TD, Out of Bounds, Safety, Touch Back
    $key_type = 'fumbles';
    @keys = ( 'num_fumbled', 'num_lost', 'num_forced', 'rec_own', 'rec_opp', '', '', 'rec_own_td', 'oob', '', '' );

  # Kicker
  } elsif ($type eq 'Field Goal Kickers') {
    # Remaining stats:
    #  FG: Blocked, Long, Made, Att, Pct
    #  20-29: Made, Att, Pct
    #  30-39: Made, Att, Pct
    #  40-49: Made, Att, Pct
    #  50+: Made, Att, Pct
    #  XP: Att, Made, Pct, Blocked
    $key_type = 'kicking';
    @keys = ( '', '', 'fg_made', 'fg_att', '',
              'fg_u30', '', '',
              'fg_u40', '', '',
              'fg_u50', '', '',
              'fg_o50', '', '',
              'xp_att', 'xp_made', '', '' );

  } elsif ($type eq 'Kickoff Stats' ) {
    # Remaining stats:
    #  Num, Yards, Out of Bounds, Average, Touch Back, Pct TB, Return, Return Avg, Ret TD, Onside Kick, Onside Kicks Recovered
    #$key_type = 'kickoff';
    #@keys = ( 'num', 'yds', 'oob', '', 'tb', '', 'ret', '', 'ret_td', 'osk', 'oskr' );
    # Intentionally not processing at this stage...
    return;

  # Kick Returner
  } elsif ($type eq 'Kick Return') {
    # Remaining stats:
    #  Num, Yds, Avg, Long, TD, Num 20+, Num 40+, Fair Catch, Fumbles
    $key_type = 'kick_return';
    @keys = ( 'num', 'yards', '', 'long', 'td', '', '', 'fair_catch', '' );

  # Punter
  } elsif ($type eq 'Punting Stats') {
    # Remaining stats:
    #  Num, Yds, Net, Long, Avg, Net Avg, Blocked, Out of Bounds, Downed, Inside 20, Touch Back, Fair Catch, Return, Return Yds, Ret TD
    $key_type = 'punting';
    @keys = ( 'num', 'yards', 'net', 'long', '', '', '', '', '', 'inside20', 'tb', '', '', '', '' );

  # Punt Returner
  } elsif ($type eq 'Punt Return') {
    # Remaining stats:
    #  Num, Yds, Avg, Long, TD, 20+, 40+, FC, Fumbles
    $key_type = 'punt_return';
    @keys = ( 'num', 'yards', '', 'long', 'td', '', '', 'fair_catch', '' );

  # Misc stats
  } else {
    # No other info available
    return;

  }

  # Now get and store the stats
  my $num_undef = 0; my $num_cols = 0;
  my %stats = ( );
  my $stat_start = 0;
  for (my $i = 0; $i < @keys; $i++) {
    next if $keys[$i] eq '';
    $num_cols++;
    ($stats{$keys[$i]}) = ($$cols[$i+$stat_start+1] =~ m/<td[^>]*>\s*([\d\.\,]+)T?\s*<\/td>/gsi);
    $stats{$keys[$i]} = 0 if !defined($stats{$keys[$i]});
    $stats{$keys[$i]} =~ s/,//g;
    $num_undef++ if $stats{$keys[$i]} == 0;
  }
  $$arr{$key_type} = \%stats if ($num_undef < $num_cols);
}

# Identify the table we'll store a stat
sub get_stat_info {
  my ($type) = @_;

  if ($type eq 'passing') {
    return 'PASSING';
  } elsif ($type eq 'rushing') {
    return 'RUSHING';
  } elsif ($type eq 'receiving') {
    return 'RECEIVING';
  } elsif ($type eq 'tackles') {
    return 'TACKLES';
  } elsif ($type eq 'passdef') {
    return 'PASSDEF';
  } elsif ($type eq 'fumbles') {
    return 'FUMBLES';
  } elsif ($type eq 'kicking') {
    return 'KICKING';
  } elsif ($type eq 'kick_return') {
    return 'KICKRET';
  } elsif ($type eq 'kickoff') {
    return 'KICKOFF';
  } elsif ($type eq 'punting') {
    return 'PUNTS';
  } elsif ($type eq 'punt_return') {
    return 'PUNTRET';
  } else {
    return '??';
  }
}

# Get remote_url_id and record that this player has been processed
sub parse_ids {
  my ($remote_internal_id) = ($profile =~ m/GSIS ID: (\S+)/gsi);
  if (defined($remote_internal_id)) {
    $remote_internal_id = "'$remote_internal_id'";
  } else {
    $remote_internal_id = 'NULL';
  }
  my ($remote_url_name, $remote_url_id) = ($profile =~ m/<link rel="canonical" href="http:\/\/www\.nfl\.com\/player\/([^\/]+)\/(\d+)\/profile" \/>/gsi);
  $remote_url_name = convert_text($remote_url_name);
  print "# Identifying remote player_id's\n";
  print "UPDATE SPORTS_NFL_PLAYERS_IMPORT SET remote_internal_id = $remote_internal_id, remote_url_id = '$remote_url_id', remote_url_name = '$remote_url_name', profile_imported = NOW() WHERE player_id = '$player_id';\n\n";
}

sub load_file {
  my ($fn) = @_;
  my $file = "$config{'dir'}/$fn.htm.gz";
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...

  return $contents;
}

# Convert an undefined value to zero
sub undef2zero {
  return defined($_[0]) ? $_[0] : 0;
}
