#!/usr/bin/perl -w
# Get player info for a single player from NFL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use Encode;
use URI::Escape;

# Validate the arguments
#  - there needs to be four: player_id, remote_url_id, remote_url_name, remote_profile_id
if (@ARGV < 4) {
  print STDERR "Insufficient arguments to the get player script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_url_id, $remote_url_name, $remote_profile_id) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_profile_id";

# Skip, if this player has already been downloaded (and we're not in --force mode)
print "# Download for $player_id / $remote_url_name / $remote_profile_id: ";
if (-e "$config{'dir'}/career.htm.gz" && !grep(/\-\-force/, @ARGV)) {
  print "[ Skipped ]\n";
  exit;
}
mkdir $config{'dir'};

# Downloading mugshots only?
my $mugshots_only = grep(/\-\-mugshot\-only/, @ARGV);

# URL-safe the name
#$remote_url_name =~ s/'/\\'/g;
$remote_url_name =~ s/&#39;/'/g;

# Profile
download("http://nflcdns.nfl.com/player/$remote_url_name/$remote_url_id/profile", "$config{'dir'}/profile.htm.gz", {'skip-today' => 1, 'if-modified-since' => 1, 'gzip' => 1})
  if !$mugshots_only;

# Career Stats
download("http://nflcdns.nfl.com/player/$remote_url_name/$remote_url_id/careerstats", "$config{'dir'}/career.htm.gz", {'skip-today' => 1, 'if-modified-since' => 1, 'gzip' => 1})
  if !$mugshots_only;

# Mugshot
download("https://static.nfl.com/static/content/public/static/img/fantasy/transparent/200x200/$remote_profile_id.png", "$config{'dir'}/mugshot.png", {'skip-today' => 1, 'if-modified-since' => 1});
# But remove if empty file
my @stat = stat "$config{'dir'}/mugshot.png";
unlink "$config{'dir'}/mugshot.png" if !$stat[7]; # 7 = size

print "[ Done ]\n";
