#!/usr/bin/perl -w
# Process and copy the player's mugshot

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: player_id, remote_url_name (for local image path) and remote_mugshot
if (@ARGV < 3) {
  print STDERR "Insufficient arguments to player mugshot script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_url_name, $remote_mugshot) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_url_name";

# Determine the remote URL, which has some variables we cannot predict
my $img_url;
# Attempt 1: NFL roster file
$img_url = `shopt -s globstar; zgrep -Poh 'http.*?\\/t_lazy\\/f_.*?\\/$remote_mugshot(?:\\.\\w{3})?' \$(find $config{'base_dir'}/_data/*/rosters -type f -name \*-nfl.htm.gz) | uniq | sort -n | tail -1`;
# Attempt 2: Team roster file
$img_url = `shopt -s globstar; zgrep -Poh 'http.*?\\/t_lazy\\/f_.*?\\/$remote_mugshot(?:\\.\\w{3})?' \$(find $config{'base_dir'}/_data/*/rosters -type f -name \*-team.htm.gz) | uniq | sort -n | tail -1`
  if !defined($img_url);
# Attempt 3: Generic fallback
$img_url = "https://static.www.nfl.com/image/private/t_thumb_squared_3x/f_auto/league/$remote_mugshot"
  if !defined($img_url);
# Tweak the retrieved URL
chomp($img_url);
$img_url =~ s/\/t_lazy//; # Strip the linked lazy loading part
# Confirm the / a file extension
my ($img_ext) = ($img_url =~ /\.(\w{3})$/);
$img_ext = 'png' if !defined($img_ext);

# Download the image (if necessary)
my $img_src = "$config{'dir'}/mugshot.$img_ext";
my $img_dst = abs_path("$config{'base_dir'}/../..") . "/resources/images/nfl/players/$player_id.png";
mkdir $config{'dir'};
download($img_url, $img_src, {'skip-today' => 1, 'if-modified-since' => 1});
# But remove if empty file
my @stat = stat $img_src;
unlink $img_src if !$stat[7]; # 7 = size

# Validate the outcome
if ( ! -e $img_src ) {
  print "# No source mugshot found for player $player_id\n";
  exit;
} elsif ( -e $img_dst ) {
  # A slightly more complicated validation: the source file needs to be newer
  my @stat_src = stat $img_src;
  my @stat_dst = stat $img_dst;
  if ($stat_dst[9] > $stat_src[9]) { # 9 = mtime
    print "# Destination mugshot file newer than source file for player $player_id\n";
    exit;
  }
}

# Convert and then compress
my $cmd = "convert $img_src -quiet -resize 150x150 -crop 100x150+25+0 $img_dst";
print "# Converting: $cmd\n";
`$cmd`;

$cmd = "image-compress-png --quiet $img_dst";
print "# Compressing: $cmd\n";
`$cmd`;

@stat = stat $img_dst;
if ($stat[7]) { # 7 = size
  print "# Player mugshot for $player_id converted\n";
} else {
  unlink $img_dst;
  print "# Attempted to convert player mugshot for $player_id, but removed due to error in conversion process\n";
}
