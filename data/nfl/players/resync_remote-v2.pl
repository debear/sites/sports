#!/usr/bin/perl -w
# Re-sync remote ID's from the migration from v1 to v2 integration

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Parsing method to be used
my $method = 0; # 1 - 3 (1 == Profile; 2 == Active Search; 3 == Retired Search; 0 == Failsafe)

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Determine where to store our temporary downloads
$config{'dir'} = "$config{'base_dir'}/_data/players/resync_v2";
mkdir $config{'dir'} if ! -e $config{'dir'};
$config{'search_dir'} = "$config{'dir'}/by-search";
mkdir $config{'search_dir'} if ! -e $config{'search_dir'};

my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sth;

##
## Method 1: By profile redirect
##
if ($method == 1) {
  # Determine the players to be resynced
  $sth = $dbh->prepare('SELECT SPORTS_NFL_PLAYERS.player_id, SPORTS_NFL_PLAYERS_IMPORT.remote_url_name, SPORTS_NFL_PLAYERS_IMPORT.remote_url_id, SPORTS_NFL_PLAYERS_IMPORT.remote_profile_id,
      CONCAT(SPORTS_NFL_PLAYERS.first_name, " ", SPORTS_NFL_PLAYERS.surname) AS name
FROM SPORTS_NFL_PLAYERS_IMPORT
JOIN SPORTS_NFL_PLAYERS
  ON (SPORTS_NFL_PLAYERS.player_id = SPORTS_NFL_PLAYERS_IMPORT.player_id)
WHERE SPORTS_NFL_PLAYERS_IMPORT.remote_url_name_v2 IS NULL
AND   SPORTS_NFL_PLAYERS_IMPORT.remote_url_name IS NOT NULL
AND   SPORTS_NFL_PLAYERS_IMPORT.remote_url_id IS NOT NULL
ORDER BY SPORTS_NFL_PLAYERS.surname, SPORTS_NFL_PLAYERS.first_name, SPORTS_NFL_PLAYERS.player_id;');
  $sth->execute;

  print "##\n## Search via profile redirect\n##\n\n";
  while (my $player = $sth->fetchrow_hashref) {
    print "# $$player{'name'}; v1 IDs: '$$player{'remote_url_name'}' // '$$player{'remote_url_id'}'\n";
    # Get the content of the "new" (redirected) page
    my $url = "https://www.nfl.com/player/$$player{'remote_url_name'}/$$player{'remote_url_id'}/profile";
    my $local = "$config{'dir'}/$$player{'remote_profile_id'}.htm.gz";
    download($url, $local, {'skip-today' => 1, 'gzip' => 1});
    my $content = load_file($local);
    # Now parse the output to get the fields we're after
    my ($remote_url_name) = ($content =~ m/<link rel="canonical" href="http.*?\/players\/([^\/]+)\/" \/>/si);
    check_for_null(\$remote_url_name);
    my ($remote_mugshot) = ($content =~ m/<meta property="og:image" content="http.*?\/t_headshot_desktop\/league\/([^"]+)" \/>/si);
    check_for_null(\$remote_mugshot);
    print "UPDATE SPORTS_NFL_PLAYERS_IMPORT SET remote_url_name_v2 = $remote_url_name WHERE player_id = '$$player{'player_id'}';\n";
    print "INSERT INTO SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS (player_id. remote_mugshot, first_seen) VALUES ('$$player{'player_id'}', '$remote_mugshot', NOW());\n"
      if $remote_mugshot ne 'NULL';
  }
}

##
## Method 2/3: Player search (for those that did not match) - 2 == Active; 3 == Retired
##
if ($method == 2 || $method == 3) {
  $sth = $dbh->prepare('SELECT SPORTS_NFL_PLAYERS.player_id,
       CONCAT(SPORTS_NFL_PLAYERS.first_name, " ", SPORTS_NFL_PLAYERS.surname) AS name,
       REPLACE(LOWER(SUBSTRING(SPORTS_NFL_PLAYERS.surname, 1, 4)), "\'", "%27") AS search
FROM SPORTS_NFL_PLAYERS_IMPORT
JOIN SPORTS_NFL_PLAYERS USING (player_id)
JOIN SPORTS_NFL_PLAYERS_SEASON USING (player_id)
WHERE SPORTS_NFL_PLAYERS_IMPORT.remote_url_name_v2 IS NULL
AND   SPORTS_NFL_PLAYERS_SEASON.season IN (2019, 2020)
GROUP BY SPORTS_NFL_PLAYERS.player_id
ORDER BY SPORTS_NFL_PLAYERS.surname, SPORTS_NFL_PLAYERS.first_name, SPORTS_NFL_PLAYERS.player_id;');
  $sth->execute;

  print "##\n## Search via player search (which will be less accurate)\n##\n\n";
  while (my $player = $sth->fetchrow_hashref) {
    print "# $$player{'name'} (Search: $$player{'search'})\n";
    # Tidy the player name for the URL query
    my $name_query = $$player{'search'};
    $name_query =~ s/'/%27/g;
    $name_query =~ s/&#/%27/g;
    # Get the content of the "new" (redirected) page
    my $type = ($method == 2 ? 'active' : 'retired');
    my $url = "https://www.nfl.com/players/$type/all?query=$name_query";
    my $local = "$config{'search_dir'}/$type-$name_query.htm.gz";
    download($url, $local, {'skip-today' => 1, 'gzip' => 1});
    my $content = load_file($local);
    # Tidy the player name for regex matching
    my $name_match = $$player{'name'};
    $name_match =~ s/'/&#x27;/g;
    $name_match =~ s/&#39;/&#x27;/g;
    $name_match =~ s/ II+ / /g;
    $name_match =~ s/ [SJ]r. / /g;
    # Now parse the output to get the fields we're after
    my ($remote_image, $remote_url_name) = ($content =~ m/<div class="d3-o-media-object">(.*?)<a class="d3-o-player-fullname nfl-o-cta--link"\s+href="\/players\/([^\/]+)\/"\s+aria-label="Go to $name_match/si);
    my $remote_mugshot;
    ($remote_mugshot) = ($remote_image =~ m/t_headshot_desktop\/t_lazy\/f_auto\/league\/(\S+) /si)
      if defined($remote_image);
    check_for_null(\$remote_url_name);
    check_for_null(\$remote_mugshot);
    print "UPDATE SPORTS_NFL_PLAYERS_IMPORT SET remote_url_name_v2 = $remote_url_name WHERE player_id = '$$player{'player_id'}';\n";
    print "INSERT INTO SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS (player_id. remote_mugshot, first_seen) VALUES ('$$player{'player_id'}', '$remote_mugshot', NOW());\n"
      if $remote_mugshot ne 'NULL';
  }
}

# Load the local file
sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  my $contents = join('', @contents);
  @contents = ( ); # Garbage collect...

  return $contents;
}
