#!/usr/bin/perl -w
# Download and process players from NFL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config{'dir'} = __DIR__;
$config{'download_only'} = 0; # Set to only download data, rather than parse and import it as well

my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});

##
## Phase 1: Fix players whose remote_id's are not populated correctly
##
print "##\n## Phase 1: Fix remote URL id's\n##\n\n";
my $sql = 'SELECT player_id, remote_profile_id
FROM SPORTS_NFL_PLAYERS_IMPORT
WHERE remote_url_id IS NULL
AND   remote_url_name IS NULL
ORDER BY player_id;';
my $player_list = $dbh->selectall_arrayref($sql, { Slice => {} });

# Skip if none found...
if (!@$player_list) {
  print "# None found. Skipping.\n";
} else {
  my $err = 0;
  foreach my $player (@$player_list) {
    print "## $$player{'player_id'} // $$player{'remote_profile_id'}\n";
    system("$config{'dir'}/set_remote.pl", $$player{'player_id'}, $$player{'remote_profile_id'}) == 0
      or $err++;
  }
  # If any errors occurred, abort with a message
  if ($err) {
    print STDERR "$err import(s) generated errors. Aborting.\n";
    exit $config{'exit_codes'}{'pre-flight'};
  }
}

##
## Phase 2: Import the players
##
print "\n##\n## Phase 2: Importing players...\n##\n\n";
$sql = 'SELECT IMP.player_id, IFNULL(IMP.remote_internal_id, "(Missing)") AS remote_internal_id,
       IMP.remote_url_id, IMP.remote_url_name, IMP.remote_profile_id,
       PL.first_name, PL.surname
FROM SPORTS_NFL_PLAYERS_IMPORT AS IMP
JOIN SPORTS_NFL_PLAYERS AS PL
  ON (PL.player_id = IMP.player_id)
WHERE IMP.profile_imported IS NULL
ORDER BY IMP.remote_profile_id;';
$player_list = $dbh->selectall_arrayref($sql, { Slice => {} });
$dbh->disconnect if defined($dbh);

# Quit if none found...
if (!@$player_list) {
  print "# None found. Exiting.\n";
  exit;
}

# Loop through the players, downloading and parsing the profile / career info
foreach my $player (@$player_list) {
  # Ensure we have the remote ID we need, and if not fail with enough information to hand.
  if (!defined($$player{'remote_profile_id'})) {
    print "##\n## $$player{'player_id'}: $$player{'first_name'} $$player{'surname'}\n##\n\n# Skipping - missing 'remote_profile_id'\n";
    print STDERR "Missing 'remote_profile_id' field for $$player{'player_id'} ($$player{'first_name'} $$player{'surname'})\n";
    next;
  }

  print "##\n## $$player{'player_id'}: $$player{'first_name'} $$player{'surname'} ($$player{'remote_internal_id'} // $$player{'remote_url_id'} // $$player{'remote_url_name'} // $$player{'remote_profile_id'})\n##\n\n";

  # Download
  print `$config{'dir'}/download.pl $$player{'player_id'} $$player{'remote_url_id'} "$$player{'remote_url_name'}" $$player{'remote_profile_id'} 2>&1` . "\n";
  next if download_only();

  # Process
  print `$config{'dir'}/parse.pl $$player{'player_id'} $$player{'remote_profile_id'}` . "\n";

  # Mugshot
  print `$config{'dir'}/mugshot.pl $$player{'player_id'} $$player{'remote_profile_id'}` . "\n";
}
