#!/usr/bin/perl -w
# Process and copy the player's mugshot

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Validate the arguments
#  - there needs to be three: player_id, remote_url_name (for local image path) and remote_mugshot
if (@ARGV < 3) {
  print STDERR "Insufficient arguments to player mugshot script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($player_id, $remote_url_name, $remote_mugshot) = @ARGV;
$config{'dir'} = "$config{'base_dir'}/_data/players/$remote_url_name";
my $img_url = "https://static.www.nfl.com/image/private/t_thumb_squared_3x/f_auto/league/$remote_mugshot";
my $img_src = "$config{'dir'}/mugshot.png";
my $img_dst = abs_path("$config{'base_dir'}/../..") . "/resources/images/nfl/players/$player_id.png";

# Download the image (if necessary)
mkdir $config{'dir'};
download($img_url, $img_src, {'skip-today' => 1, 'if-modified-since' => 1});
# But remove if empty file
my @stat = stat $img_src;
unlink $img_src if !$stat[7]; # 7 = size

# Validate the outcome
if ( ! -e $img_src ) {
  print "# No source mugshot found for player $player_id\n";
  exit;
} elsif ( -e $img_dst ) {
  # A slightly more complicated validation: the source file needs to be newer
  my @stat_src = stat $img_src;
  my @stat_dst = stat $img_dst;
  if ($stat_dst[9] > $stat_src[9]) { # 9 = mtime
    print "# Destination mugshot file newer than source file for player $player_id\n";
    exit;
  }
}

# Convert and then compress
my $cmd = "convert $img_src -quiet -resize 150x150 -crop 100x150+25+0 $img_dst";
print "# Converting: $cmd\n";
`$cmd`;

$cmd = "image-compress-png --quiet $img_dst";
print "# Compressing: $cmd\n";
`$cmd`;

@stat = stat $img_dst;
if ($stat[7]) { # 7 = size
  print "# Player mugshot for $player_id converted\n";
} else {
  unlink $img_dst;
  print "# Attempted to convert player mugshot for $player_id, but removed due to error in conversion process\n";
}
