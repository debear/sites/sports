#!/usr/bin/perl -w
# Get game data for a single game from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get game script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
# Maximum number of times we will attempt to process the puppeteer script
$config{'max_attempts'} = 3;

# Declare variables
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
my $game_info = get_game_info(@ARGV);
my $dir = sprintf('%s/_data/%d/games/%s/%02d/%02d', $config{'base_dir'}, $season, $game_type, $week, $game_id);

# Run...
print '## Downloading ' . ucfirst($game_type) . " Week $week, Game $game_id: $$game_info{'visitor_id'} @ $$game_info{'home_id'}";

# Skip if already done...
if (-e "$dir/gamebook.pdf") {
  print ": [ Skipped ]\n";
  exit;
}
print "\n";

# Make sure the game dir exists
make_path($dir);

# Build the URL endpoint for this game
my $gt = ($game_type eq 'regular' ? 'reg' : 'post');
my $url = "$$game_info{'visitor'}-at-$$game_info{'home'}-$season-$gt-$week";
$url =~ s/[^a-z0-9\-]/-/g;

# Run the Puppeteer script (with some retry logic) to download and parse the info
my $download_stdout; my $i; my $download_successful = 0;
for ($i = 1; !$download_successful && $i <= $config{'max_attempts'}; $i++) {
  print "\n## Attempt $i\n\n";
  my $file_stderr = "/tmp/nfl.$season-$gt-$game_id.gc-$i.err";
  $download_stdout = `cd $config{'puppeteer_dir'}; node gamecenter.js $url $dir 2>$file_stderr`;

  # Get (lazily) the STDERR from the download command
  my $download_stderr = `cat $file_stderr`;
  # Render the full output to both STDOUT and STDERR (for context in each instance)
  print "# STDOUT:\n" . ($download_stdout ne '' ? $download_stdout : '(blank)') . "\n\n";
  print "# STDERR:\n" . ($download_stderr ne '' ? $download_stderr : '(blank)') . "\n\n";
  # Abort if any errors were found
  my @output_lines = split "\n", $download_stderr;
  my @output_err = grep(/^[^\#]/, @output_lines);
  $download_successful = (@output_err == 0);

  # Summarise the attempt
  print "# Attempt $i Status: " . ($download_successful ? 'Success': 'Failed') . "\n";

  # Add a small sleep if not found after this attempt to vaguely introduce some kind of delay to upstream
  sleep 1 if !$download_successful;
}
# Abort (with some kind of pretty message) if still not able to complete the download
if (!$download_successful) {
  print STDERR 'Unable to complete the puppeteer script successfully after ' . ($i - 1) . " attempt(s)\n";
  exit 1;
}

# Gamebook downloading needs to occur from here though
my ($gamebook_url) = ($download_stdout =~ m/Gamebook URL: (http\S+\.pdf)/gsi);
if (defined($gamebook_url)) {
  # We have a direct link
  download_gamebook($gamebook_url);
} else {
  # Try our fallback method, using the UUID we found
  my ($gamebook_id) = ($download_stdout =~ m/Game UUID: (\d+)/gsi);
  $gamebook_url = "https://static.www.nfl.com/gamecenter/$gamebook_id.pdf";
  download_gamebook($gamebook_url);
}

# Convert the gamebook to a text file
`pdftotext -layout $dir/gamebook.pdf - 2>/dev/null | gzip >$dir/gamebook.txt.gz`;

print "# Gamebook downloaded from $gamebook_url\n\n";

# Perform the gamebook download
sub download_gamebook {
  my ($url) = @_;
  my $local = "$dir/gamebook.pdf";
  my $gzip = substr($local, -3) eq '.gz';
  download($url, $local, {'skip-today' => 1, 'gzip' => $gzip});
  return $local;
}
