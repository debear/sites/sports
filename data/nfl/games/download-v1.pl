#!/usr/bin/perl -w
# Get game data for a single game from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get game script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
my $game_info = get_game_info(@ARGV);
my $dir = sprintf('%s/_data/%d/games/%s/%02d/%02d', $config{'base_dir'}, $season, $game_type, $week, $game_id);

# Run...
print '# Downloading ' . ucfirst($game_type) . " Week $week, Game $game_id: $$game_info{'visitor_id'} @ $$game_info{'home_id'}: ";

# Skip if already done...
if (-e "$dir/play_by_play.htm") {
  print "[ Skipped ]\n";
  exit;
}

# Make sure the game dir exists
make_path($dir);

# Identify part of the URL - type of season and game week
my $type = ($game_type eq 'regular' ? 'REG' : 'POST');
$week++ if $type eq 'POST' && $week == 4; # Conf champ == 20; SB == 22
$week += 17 if $type eq 'POST';

download_file("http://www.nfl.com/gamecenter/$$game_info{'nfl_id'}/$season/$type$week/$$game_info{'visitor'}\@$$game_info{'home'}", 'gamecenter');
my $gamebook_id;
my $gamebook_url = generate_gamebook_url(1);
my $gamebook_local = download_file("http://www.nflcdn.com$gamebook_url", 'gamebook', 'pdf')
  if defined($gamebook_url) && $gamebook_url;

# Try again on the main NFL.com domain, but using the same URL
if ( !defined($gamebook_local) || ! -e $gamebook_local || -z $gamebook_local ) {
  unlink $gamebook_local if defined($gamebook_local);
  $gamebook_local = download_file("http://www.nfl.com$gamebook_url", 'gamebook', 'pdf');
}

# Re-generate the link if our intial attempt didn't work?
if ( !defined($gamebook_local) || ! -e $gamebook_local || -z $gamebook_local ) {
  unlink $gamebook_local if defined($gamebook_local);
  $gamebook_url = generate_gamebook_url(2);
  $gamebook_local = download_file("http://www.nflcdn.com$gamebook_url", 'gamebook', 'pdf');
}
# Final attempt, using a third source of team_id...
if ( !defined($gamebook_local) || ! -e $gamebook_local || -z $gamebook_local ) {
  unlink $gamebook_local if defined($gamebook_local);
  $gamebook_url = generate_gamebook_url(3);
  $gamebook_local = download_file("http://www.nflcdn.com$gamebook_url", 'gamebook', 'pdf');
}

# Convert the gamebook to a text file
`pdftotext -layout $dir/gamebook.pdf $dir/gamebook.txt 2>/dev/null`;

download_file("http://www.nfl.com/widget/gc/2011/tabs/cat-post-boxscore?gameId=$$game_info{'nfl_id'}", 'boxscore');

# Game recap
# Removed as of Mar-2017. TD.
#download_file("http://www.nfl.com/widget/gc/2011/tabs/cat-post-recap-full-story?gameId=$$game_info{'nfl_id'}", 'recap');
#download_file("http://www.nfl.com/widget/gc/2011/tabs/cat-post-recap-quick-take?gameId=$$game_info{'nfl_id'}", 'recap_summary');

# Play-by-Play (HTML version)
download_file("http://www.nfl.com/widget/gc/2011/tabs/cat-post-playbyplay?gameId=$$game_info{'nfl_id'}", 'play_by_play');

print "[ Done ]\n";

# Perform a download
sub download_file {
  my ($url, $type, $ext) = @_;
  my $local = "$dir/$type." . (defined($ext) ? $ext : 'htm');
  download($url, $local, {'skip-today' => 1});
  return $local;
}

# Determine URL from which we need to download a gamebook
sub generate_gamebook_url {
  my ($mode) = @_;
  my $info;

  # Load the gamecenter file?
  if (!defined($gamebook_id) || $mode == 1) {
    open FILE, "<$dir/gamecenter.htm";
    my @info = <FILE>;
    close FILE;
    $info = join('', @info);
  }

  # Get the gamebook_id, something we need in all permutations
  if (!defined($gamebook_id)) {
    ($gamebook_id) = ($info =~ m/<!-- gamekey : (\d+) -->/si);
  }

  # Method 1: URL from the gamecenter file
  if ($mode == 1) {
    my ($gamebook_url) = ($info =~ m/(\/liveupdate\/gamecenter\/$gamebook_id\/.*?_Gamebook\.pdf)/si);
    return $gamebook_url if defined($gamebook_url) && $gamebook_url; # Match found, so return

  # Method 2: Build ourselves... converting the home team's ID, as NFL.com's version may be different to ours
  } elsif ($mode == 2) {
    return "/liveupdate/gamecenter/$gamebook_id/" . convert_team_id($$game_info{'home_id'}, 1) . '_Gamebook.pdf';

  # Method 3: Build ourselves... using the home team's ID as we have it
  } elsif ($mode == 3) {
    return "/liveupdate/gamecenter/$gamebook_id/$$game_info{'home_id'}_Gamebook.pdf";
  }
}
