#!/usr/bin/perl -w
# Get game data for a single game from NFL.com

use strict;
use File::Path qw(make_path);
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the get game script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Declare variables
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
my $game_info = get_game_info(@ARGV);
my $dir = sprintf('%s/_data/%d/games/%s/%02d/%02d', $config{'base_dir'}, $season, $game_type, $week, $game_id);

# Run...
print '# Downloading ' . ucfirst($game_type) . " Week $week, Game $game_id: $$game_info{'visitor_id'} @ $$game_info{'home_id'}: ";

# Skip if already done...
if (-e "$dir/play_by_play.htm.gz") {
  print "[ Skipped ]\n";
  exit;
}

# Make sure the game dir exists
make_path($dir);

# Identify part of the URL - type of season and game week
my $type = ($game_type eq 'regular' ? 'REG' : 'POST');
$week++ if $type eq 'POST' && $week == 4; # Conf champ == 20; SB == 22
my $reg_weeks = ($season < 2021 ? 17 : 18);
$week += $reg_weeks if $type eq 'POST';

# Get the Gamecenter file
download_file("http://nflcdns.nfl.com/gamecenter/$$game_info{'nfl_id'}/$season/$type$week/$$game_info{'visitor'}\@$$game_info{'home'}", 'gamecenter');
# Get out the JSON blob
my $contents = load_file("gamecenter.htm.gz");
my ($blob) = ($contents =~ m/__INITIAL_DATA__ = (\{.+\});\s*?__REACT_ROOT_ID__/gsi);
($blob) = ($contents =~ m/this.extdata\s+= (\{.+\});\s*?\/\/this.extdata.weather/gsi)
  if !defined($blob);
write_file("gamecenter.json.gz", $blob);

# Find and download the Gamebook
my $gamebook_id = $$game_info{'nfl_gsis_id'};
my $gamebook_url = generate_gamebook_url(1);
my $gamebook_local = download_file("http://nflcdns.nfl.com$gamebook_url", 'gamebook', 'pdf')
  if defined($gamebook_url) && $gamebook_url;

# Re-generate the link if our intial attempt didn't work?
if (!defined($gamebook_local) || ! -e $gamebook_local || -z $gamebook_local) {
  unlink $gamebook_local if defined($gamebook_local);
  $gamebook_url = generate_gamebook_url(2);
  $gamebook_local = download_file("http://nflcdns.nfl.com$gamebook_url", 'gamebook', 'pdf');
}
# Final attempt, using a third source of team_id...
if (!defined($gamebook_local) || ! -e $gamebook_local || -z $gamebook_local) {
  unlink $gamebook_local if defined($gamebook_local);
  $gamebook_url = generate_gamebook_url(3);
  $gamebook_local = download_file("http://nflcdns.nfl.com$gamebook_url", 'gamebook', 'pdf');
}

# Convert the gamebook to a text file
`pdftotext -layout $dir/gamebook.pdf - 2>/dev/null | gzip >$dir/gamebook.txt.gz`;

download_file("http://nflcdns.nfl.com/widget/gc/2011/tabs/cat-post-boxscore?gameId=$$game_info{'nfl_id'}", 'boxscore');

# Play-by-Play (HTML version)
download_file("http://nflcdns.nfl.com/widget/gc/2011/tabs/cat-post-playbyplay?gameId=$$game_info{'nfl_id'}", 'play_by_play');

print "[ Done ]\n";

# Perform a download
sub download_file {
  my ($url, $type, $ext) = @_;
  my $local = "$dir/$type." . (defined($ext) ? $ext : 'htm.gz');
  my $gzip = substr($local, -3) eq '.gz';
  download($url, $local, {'skip-today' => 1, 'gzip' => $gzip});
  return $local;
}

# Determine URL from which we need to download a gamebook
sub generate_gamebook_url {
  my ($mode) = @_;
  # Load the gamecenter file
  my $info = load_file("gamecenter.htm.gz");

  # If we don't have the gsis_id from the schedule, get it from other sources
  if (!defined($gamebook_id)) {
    # Get the gamebook_id, something we need in all permutations
    ($gamebook_id) = ($info =~ m/"gsisId":"(\d+)"/si);
    ($gamebook_id) = ($info =~ m/gamekey : (\d+)/si)
      if !defined($gamebook_id);
  }

  # Method 1: URL from the gamecenter file
  if ($mode == 1) {
    my ($gamebook_url) = ($info =~ m/(\/liveupdate\/gamecenter\/$gamebook_id\/.*?_Gamebook\.pdf)/si);
    return $gamebook_url if defined($gamebook_url) && $gamebook_url; # Match found, so return

  # Method 2: Build ourselves... converting the home team's ID, as NFL.com's version may be different to ours
  } elsif ($mode == 2) {
    return "/liveupdate/gamecenter/$gamebook_id/" . convert_team_id($$game_info{'home_id'}, 1) . '_Gamebook.pdf';

  # Method 3: Build ourselves... using the home team's ID as we have it
  } elsif ($mode == 3) {
    return "/liveupdate/gamecenter/$gamebook_id/$$game_info{'home_id'}_Gamebook.pdf";
  }
}

# Load a file from disk
sub load_file {
  my ($file) = @_;
  open FILE, "gzip -dc $dir/$file |";
  my @info = <FILE>;
  close FILE;
  return join('', @info);
}

# Save a file to disk
sub write_file {
  my ($file, $contents) = @_;
  open FILE, '|-', "gzip >$dir/$file";
  print FILE $contents;
  close FILE;
}
