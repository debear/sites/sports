#!/usr/bin/perl -w
# Read the game info and convert to SQL

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;
$config{'debug'} = 0; # Display script to run, rather than actually running it...

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

print "##\n##\n## Table Maintenance\n##\n##\n\n";

# Fudge the required args
our $season = 2009;
our $game_type = 'regular';
our $week = 1;
our $db_game_id = 101;

# Run
foreach my $script (@{$config{'scripts'}}) {
  run_script($$script{'script'}, $$script{'disp'}, '--order');
}

print "##\n## Table Maintenance Complete\n##\n\n";

