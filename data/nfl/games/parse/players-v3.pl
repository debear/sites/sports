#!/usr/bin/perl -w
# Load the player info

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use JSON;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season, $game_type, $db_game_id) = @ARGV;
our $week = substr($db_game_id, 0, -2);
my $dump = grep(/^--dump$/, @ARGV);
my $game_id = ($db_game_id % 100);
our $game_concat = $season . '//' . $game_type . '//' . $db_game_id;

my %player_list = ( 'visitor' => [ ], 'home' => [ ] );
our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3]) && !$dump) {
  # Which tables?
  my @tables = ( 'SPORTS_NFL_GAME_LINEUP' );

  clear_previous($season, $game_type, $week, $db_game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

# First check, does the file exist, and if it does, load it
my ($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamecenter', { 'ext' => 'json' });
print "##\n## Loading players for $season // $game_type // $week // $game_id\n##\n";
print "# Loading Gamecenter: $file\n";

# Some JSON fixes
$contents =~ s/^\s+([a-zA-Z]+)\s*:/"$1": /gms;
$contents =~ s/(this\..*?)([,\}])/"REF"$2/g;
$contents =~ s/'([^,]*?)'/"$1"/g;
our $blob = decode_json($contents); # Convert to hash

# Dump player array with keys
if ($dump) {
  foreach my $team ('away', 'home') {
    print STDERR "$team:\n";
    my %list = %{$$blob{'players'}{$team}{'players'}};
    my @keys = sort keys %list;
    for (my $i = 0; $i < @keys; $i++) {
      my %player = %{$list{$keys[$i]}};
      my $profile_id = $player{'esbid'};
      $profile_id = '* Not Found *' if !defined($profile_id);
      print STDERR "$i: $player{'fn'} $player{'ln'} ($profile_id)\n";
    }
    print STDERR "\n";
  }
  exit;
}

# Fixes to run on the data?
my $fix_file = "$config{'base_dir'}/_data/fixes/$season/gamecenter.pl";
require $fix_file
  if -e $fix_file;

my %rosters = (
  'one' => { 'away' => { }, 'home' => { } },
  'two' => { 'away' => { }, 'home' => { } },
  'three' => { 'away' => { }, 'home' => { } },
  'nosuffix' => { 'away' => { }, 'home' => { } },
  'full' => { 'away' => { }, 'home' => { } },
  'jersey' => { 'away' => { }, 'home' => { } },
  'database' => { 'away' => undef, 'home' => undef },
);

# Loop through the players we have
print "#  - Loading all players from gamecenter file:";
foreach my $team ('away', 'home') {
  my %list = %{$$blob{'players'}{$team}{'players'}};
  my @keys = sort keys %list;
  for (my $i = 0; $i < @keys; $i++) {
    my $internal_id = $keys[$i];
    my $player = $list{$internal_id};

    # Ensure single digit values are the numeric equivalent
    $$player{'uniformNumber'} =~ s/^0([1-9])/$1/
      if defined($$player{'uniformNumber'});

    # A home player or away?
    my $team_id = convert_team_id($$player{'team'});
    my $team_type = ($team_id eq $$game_info{'home_id'} ? 'home' : 'away');

    # Backup some fields (pre-conversion) for use in keys
    my $fname = lc $$player{'fn'};
    my $sname = lc $$player{'ln'};
    my $tmp = "# $$player{'fn'} $$player{'ln'} (#{jersey}, $$player{'pos'})\n";

    # Encode certain key fields
    $$player{'fn'} = convert_text($$player{'fn'});
    $$player{'ln'} = convert_text($$player{'ln'});
    $$player{'uniformNumber'} = convert_text($$player{'uniformNumber'});
    $$player{'pos'} = convert_text($$player{'pos'});

    # Identify if this player has been imported before
    $tmp .= "SET \@player_id := (SELECT `player_id` FROM `SPORTS_NFL_PLAYERS_IMPORT` WHERE `remote_profile_id` = '$$player{'esbid'}');\n";
    $tmp .= "INSERT IGNORE INTO `SPORTS_NFL_PLAYERS_IMPORT` (`player_id`, `remote_internal_id`, `remote_profile_id`, `profile_imported`)
  VALUES (\@player_id, '$internal_id', '$$player{'esbid'}', NULL);\n";
    $tmp .= "SET \@player_id := (SELECT IFNULL(\@player_id, LAST_INSERT_ID()));\n\n";

    # Create the player record
    $tmp .= "INSERT IGNORE INTO `SPORTS_NFL_PLAYERS` (`player_id`, `first_name`, `surname`)
  VALUES (\@player_id, '$$player{'fn'}', '$$player{'ln'}');\n\n";

    # Attach to this game
    $tmp .= "INSERT INTO `SPORTS_NFL_GAME_LINEUP` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `player_id`, `pos`, `status`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '{jersey}', \@player_id, '{pos}', '{status}');\n\n";

    # Bespoke fields for our sorting...
    $fname =~ s/[^a-z]//ig;
    # A single fname version with the surname stripped of roman numerals
    $$player{'nameNoSuffix'} = substr($fname, 0, 1) . '.' . $sname;
    $$player{'nameNoSuffix'} =~ s/ i+v?$//; # Assuming no more than fifth generation?
    # Then some more standard versions
    $sname =~ s/ //g;
    $$player{'name1'} = substr($fname, 0, 1) . '.' . $sname;
    $$player{'name2'} = substr($fname, 0, 2) . '.' . $sname;
    $$player{'name3'} = substr($fname, 0, 3) . '.' . $sname;
    $$player{'nameFull'} = $fname . $sname; # Skip the dot separator (See: DanielThomas, 2014/regular/4/8)

    # Store for processing...
    my %info = ( 'sql' => $tmp, 'row' => $player );
    add_gamecenter_roster($rosters{'one'}{$team_type}, $$player{'name1'}, \%info);
    add_gamecenter_roster($rosters{'two'}{$team_type}, $$player{'name2'}, \%info);
    add_gamecenter_roster($rosters{'three'}{$team_type}, $$player{'name3'}, \%info);
    add_gamecenter_roster($rosters{'nosuffix'}{$team_type}, $$player{'nameNoSuffix'}, \%info);
    add_gamecenter_roster($rosters{'full'}{$team_type}, $$player{'nameFull'}, \%info);
    add_gamecenter_roster($rosters{'jersey'}{$team_type}, $$player{'uniformNumber'}, \%info)
      if defined($$player{'uniformNumber'});
  }
}
print " [ Done ]\n";

# Next, load the gamebook
($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamebook');
print "# Loading Gamebook: $file\n\n";

my @hashes = ( { 'key' => 'away', 'id' => 'visitor_id' },
               { 'key' => 'home', 'id' => 'home_id' } );
my $game_rosters = get_gamebook_rosters("$season//$game_type//$week//$game_id", $contents);
foreach my $hash (@hashes) {
  print "#\n# $$game_info{$$hash{'id'}}\n#\n";
  foreach my $status ('starters', 'subs', 'dnp', 'inactives') {
    foreach my $player (@{$$game_rosters{$$hash{'key'}}{$status}}) {
      process_player($player, $$hash{'key'});
    }
  }
}

print "##\n## Completed loading players for $season // $game_type // $week // $game_id\n##\n";

# Store gamecenter player info in an array
sub add_gamecenter_roster {
  my ($hash, $key, $info) = @_;

  # Add to a previous list?
  if (defined($$hash{$key}) && ref($$hash{$key}) eq 'ARRAY') {
    push @{$$hash{$key}}, $info;

  # Add to a previous scalar?
  } elsif (defined($$hash{$key})) {
    $$hash{$key} = [ $$hash{$key} ];
    push @{$$hash{$key}}, $info;

  # Add as a scalar...
  } else {
    $$hash{$key} = $info;
  }
}

# Find players and display the output
sub process_player {
  my ($player, $key) = @_;

  # Add this player to the list, interpolating the relevant info
  my $info = get_player($key, $$player{'name'}, $$player{'pos'}, $$player{'jersey'});
  return if !defined($info);

  # Establish the position, and convert
  $$player{'pos'} = $$info{'row'}{'pos'} if !defined($$player{'pos'});
  parse_pos(\$$player{'pos'});

  # Process and display
  my $tmp = $$info{'sql'};
  $tmp =~ s/{jersey}/$$player{'jersey'}/g;
  $tmp =~ s/{pos}/$$player{'pos'}/g;
  $tmp =~ s/{status}/$$player{'status'}/g;
  print $tmp;
}

# Establish a player
sub get_player {
  my ($key, $name, $pos, $jersey, $no_recurse) = @_;
  $pos = '' if !defined($pos);

  # Conver the names in to a standard format (as there are the odd instances where this isn't the case...)
  $name =~ s/^([^\. ]+) +([^\. ]+)$/$1.$2/; # Space separator instead of a dot
  $name =~ s/ //g; $name = lc $name;

  # Work backwards, from three to one and use jersey as a fallback...
  foreach my $num ( 'full', 'three', 'two', 'one', 'nosuffix' ) {
    next if !defined($rosters{$num}{$key}{$name});

    # If a scalar, return what we have...
    return $rosters{$num}{$key}{$name}
      if ref($rosters{$num}{$key}{$name}) ne 'ARRAY';

    # Matching names, so break by jersey?
    my @m = ( );
    foreach my $m (@{$rosters{$num}{$key}{$name}}) {
      my $m_jersey = $$m{'row'}{'jerseyNumber'};
      $m_jersey = $$m{'row'}{'uniformNumber'} if !defined($m_jersey); # Legacy attribute support
      push @m, $m if defined($m_jersey) && $m_jersey == $jersey;
    }
    return $m[0] if @m == 1;

    # Matching by jersey too, so how about by pos?
    if ($pos ne '') {
      my @p = ( );
      foreach my $m (@m) {
        push @p, $m if $$m{'pos'} eq $pos;
      }

      return $p[0] if @p == 1;
    }
  }

  # Fallback 1: Finding players through jersey numbers...
  if (defined($rosters{'jersey'}{$key}{$jersey})) {
    my $by_jersey = $rosters{'jersey'}{$key}{$jersey};
    if (ref($by_jersey) ne 'ARRAY') {
      $by_jersey = [ $by_jersey ];
    }

    foreach my $num ( 'nameFull', 'name3', 'name2', 'name1', 'nameNoSuffix' ) {
      # Try to match by exact name....
      my @m = ( );
      foreach my $m ( @{$by_jersey} ) {
        push @m, $m if $$m{'row'}{$num} eq $name;
      }
      return $m[0] if @m == 1;

      # ... then sub-part of name (i.e., hyphenated surname in roster, not in gamebook)
      @m = ( );
      foreach my $m ( @{$by_jersey} ) {
        if (length($name) < length($$m{'row'}{$num})) {
          push @m, $m if substr($$m{'row'}{$num}, 0, length($name)) eq $name;
        }
      }
      return $m[0] if @m == 1;

      # ... then position
      if ($pos ne '') {
        my @p = ( );
        foreach my $m ( @m ) {
          push @p, $m if defined($$m{'row'}{'pos'}) && $$m{'row'}{'pos'} eq $pos;
        }
        return $p[0] if @p == 1;
      }
    }
  }

  # Fallback 2: Let's try again, but customising $name to be F.Sname... and then surname only
  if (!defined($no_recurse)) {
    # F.Sname
    my $name_mod = $name; $name_mod =~ s/^(.)[^\.]*\.(.+)$/$1.$2/;
    my $mod = get_player($key, $name_mod, $pos, $jersey, 1);
    return $mod if defined($mod);

    # Sname only
    my $snameA = $name; $snameA =~ s/^[^\.]+\.(.+)$/$1/;
    my $snameB = $name; $snameB =~ s/^.*?\.([^\.]+)$/$1/;
    my $by_jersey = $rosters{'jersey'}{$key}{$jersey};
    if (ref($by_jersey) ne 'ARRAY') {
      $by_jersey = [ $by_jersey ];
    }
    my @m = ( );
    foreach my $m ( @{$by_jersey} ) {
      if (defined($$m{'row'})) {
        my $ln = lc $$m{'row'}{'ln'};
        push @m, $m if $ln eq $snameA || $ln eq $snameB;
      }
    }
    return $m[0] if @m == 1;

    # Okay, so let's try a different tack on the surnames - use the ESBID, which contains the first three letters of the surname
    $snameA = substr($snameA, 0, 3); $snameB = substr($snameB, 0, 3);
    my @p = ( );
    foreach my $m ( @{$by_jersey} ) {
      if (defined($$m{'row'})) {
        my $esbid = lc substr($$m{'row'}{'esbid'}, 0, 3);
        push @p, $m if $esbid eq $snameA || $esbid eq $snameB;
      }
    }
    return $p[0] if @p == 1;

    # Not that I like this, but if there's only one for this jersey anyway, return that player...
    return $rosters{'jersey'}{$key}{$jersey} if ref($rosters{'jersey'}{$key}{$jersey}) eq 'HASH';
  }

  # Fallback 3: From database roster (again, via jersey)
  if (!defined($no_recurse)) {
    my $team_id = ($key eq 'home' ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
    if (!defined($rosters{'download'}{$key})) {
      # Load, once per team
      my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
      my $sql = "SELECT SPORTS_NFL_TEAMS_ROSTERS.player_id, SPORTS_NFL_TEAMS_ROSTERS.jersey,
       CONCAT(SPORTS_NFL_PLAYERS.first_name, ' ', SPORTS_NFL_PLAYERS.surname) AS name,
       LOWER(SPORTS_NFL_PLAYERS.first_name) AS first_name, LOWER(SPORTS_NFL_PLAYERS.surname) AS surname
FROM SPORTS_NFL_TEAMS_ROSTERS
JOIN SPORTS_NFL_PLAYERS
  ON (SPORTS_NFL_PLAYERS.player_id = SPORTS_NFL_TEAMS_ROSTERS.player_id)
WHERE SPORTS_NFL_TEAMS_ROSTERS.season = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.game_type = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.week = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.team_id = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.jersey IS NOT NULL
ORDER BY SPORTS_NFL_TEAMS_ROSTERS.jersey;";
      my $all = $dbh->selectall_arrayref($sql, { Slice => {} }, $season, $game_type, $week, $team_id);
      $dbh->disconnect if defined($dbh);
      foreach my $row (@$all) {
        $rosters{'download'}{$key}{$$row{'jersey'}} = $row;
      }
    }
    # Now attempt the match - jersey and surname
    if (defined($rosters{'download'}{$key}{$jersey})
      && $name =~ /\.$rosters{'download'}{$key}{$jersey}{'surname'}$/) {
      my %row = %{$rosters{'download'}{$key}{$jersey}};
      return {
        'sql' => "# $row{'name'} (#{jersey}, $pos - via team roster)\nINSERT INTO `SPORTS_NFL_GAME_LINEUP` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `player_id`, `pos`, `status`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '{jersey}', '$row{'player_id'}', '{pos}', '{status}');\n\n",
      };
    }
  }

  # Can't find any way of uniquely identifying this player, so let's say so!
  print STDERR "ERROR: Unable to uniquely match $name ($key #$jersey, $pos)\n"
    if !defined($no_recurse);
  return undef;
}

