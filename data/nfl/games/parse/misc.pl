#!/usr/bin/perl -w
# Load the misc game info

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the misc game info parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
our $game_concat = $season . '//' . $game_type . '//' . $db_game_id;

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NFL_GAME_OFFICIALS',
                 'SPORTS_NFL_GAME_DRIVES',
                 'SPORTS_NFL_GAME_STATS_DOWNS',
                 'SPORTS_NFL_GAME_STATS_PLAYS',
                 'SPORTS_NFL_GAME_STATS_YARDS',
                 'SPORTS_NFL_GAME_STATS_KICKOFFS',
                 'SPORTS_NFL_GAME_STATS_PUNTS',
                 'SPORTS_NFL_GAME_STATS_RETURNS',
                 'SPORTS_NFL_GAME_STATS_TDS',
                 'SPORTS_NFL_GAME_STATS_2PT',
                 'SPORTS_NFL_GAME_STATS_KICKS',
                 'SPORTS_NFL_GAME_STATS_TURNOVERS',
                 'SPORTS_NFL_GAME_STATS_MISC' );

  clear_previous($season, $game_type, $week, $db_game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

# Section to display: -1 = All, 0 = None, 1-X = appropriate section
$config{'disp'} = -1;

print "##\n## Loading misc game info for $season // $game_type // $week // $game_id\n##\n";

# Load the game book
my ($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamebook');
print "# Loading Gamebook: $file\n\n";

# Get the officials
parse_officials() if check_disp(1);

# Drives
parse_drives() if check_disp(2);

# Team stats
parse_team_stats() if check_disp(3);

print "##\n## Completed loading misc info for $season // $game_type // $week // $game_id\n##\n";

##
## Individual sections
##
# Parse the list of officials in a game
sub parse_officials {
  print "##\n## Loading Officials from Gamebook\n##\n";
  my ($officials) = ($contents =~ m/Officials[ \t]*\n(.+)\n[ \t]*Lineups/gsi);
  my @lines = split("\n", $officials);

  # Split officials by multiple space chars
  my %offic = ( );
  foreach my $line (@lines) {
    foreach my $offic (split(/\s{2,}/, $line)) {
      next if $offic =~ m/^\s*$/ || $offic !~ m/:/;
      trim(\$offic);

      # Test for missing brackets
      $offic .= ')' if $offic =~ m/\(\w*$/;
      $offic .= ' ()' if $offic !~ m/\(\w*\)$/;

      # Split in to its component parts...
      my ($pos, $name, $number) = ($offic =~ m/^(.*?):(.+)\((\w*)\)$/);
      next if !defined($pos);
      foreach my $char (';', '\/', '&', ':', '-', '\(') {
        $name =~ s/^([^$char]+)$char.*?$/$1/ if $name =~ m/$char/;
      }

      # Split the name...
      my $fname = ''; my $sname = '';
      trim(\$name);
      if ($name =~ m/,/) {
        ($sname, $fname) = ($name =~ m/^(.+),(.+)$/);
      } elsif ($name =~ m/ /) {
        ($fname, $sname) = ($name =~ m/^(.+) (.+)$/);
      } elsif ($name =~ m/\./) {
        ($fname, $sname) = ($name =~ m/^(.+)\.(.+)/);
      } else {
        $sname = $name;
      }
      trim(\$fname); trim(\$sname);
      $fname =~ s/^([A-Z])\.$/$1/ if $fname =~ m/^[A-Z]\.$/;

      # Skip if no official found, or we've already found this official...
      next if (defined($fname) && defined($sname) && $fname =~ m/^\s*$/ && $sname =~ m/^\s*$/);
      next if defined($offic{"$fname $sname"});

      print "# $fname $sname, $pos, #$number\n";
      $offic{"$fname $sname"} = 1;

      # Head Linesman is also known as Down Judge
      if ($pos =~ /^Down Judge$/i) {
        print "# - Position standardised from '$pos' to 'Head Linesman'\n";
        $pos = 'Head Linesman';
      }

      # Database encode...
      $sname = convert_text($sname);
      $fname = convert_text($fname);
      $number = undef if $number =~ m/^[a-z]+$/i;
      check_for_null(\$number);

      $pos =~ s/^\s*?(\S)/$1/;
      my $pos_cp = lc $pos; $pos = '';
      foreach my $tmp (split(' ', $pos_cp)) {
        $pos .= substr($tmp, 0, 1);
      }

      # Create
      print "SET \@official_id := (SELECT `official_id` FROM `SPORTS_NFL_OFFICIALS` WHERE `first_name` = '$fname' AND `surname` = '$sname');\n";
      print "INSERT IGNORE INTO `SPORTS_NFL_OFFICIALS` (`official_id`, `first_name`, `surname`)
  VALUES (\@official_id, '$fname', '$sname');\n";
      print "SET \@official_id := (SELECT IFNULL(\@official_id, LAST_INSERT_ID()));\n\n";

      # Link to this game
      print "INSERT INTO `SPORTS_NFL_GAME_OFFICIALS` (`season`, `game_type`, `week`, `game_id`, `official_type`, `official_id`, `official_num`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$pos', \@official_id, $number);\n\n";
    }
  }
}

# Parse the drive list
sub parse_drives {
  print "##\n## Loading Drives from Gamebook\n##\n";
  my ($away_drives, $home_drives) = ($contents =~ m/Ball Possession And Drive Chart(.*?)Average(.*?)Average/gsi);
  my @teams = ( { 'team_id' => $$game_info{'visitor_id'}, 'drives' => $away_drives },
                { 'team_id' => $$game_info{'home_id'}, 'drives' => $home_drives } );
  my $drive_id = 0;
  foreach my $team (@teams) {
    # Trim the full table down to the data rows
    $$team{'drives'} =~ s/^.*?\n(\s*1)/$1/s;
    $$team{'drives'} =~ s/(\n)\s+\(\d+\)\s*$/$1/s;

    # And minor manipulation on the data...
    $$team{'drives'} =~ s/Blocked ([^,]+), (?:Downs|Safety)/Blocked $1/g;
    $$team{'drives'} =~ s/(Blocked [^,]+), ([^\n]+)\n\s*(?:Downs|Safety)\s*\n/$1 $2\n/g;
    $$team{'drives'} =~ s/(Blocked [^,]+), ([^\n]+)\n([^\n]+)(?:Downs|Safety)\n\s+(\S)/$1  $2\n$3$4/gs;
    $$team{'drives'} =~ s/(Blocked|Missed)( FG)(\s+)50(\s+\d+)/$1$2$3ZZZ 50$4/gs;

    # And process...
    print "# $$team{'team_id'}\n";
    my $quarter = 0; my $last_start = '00:00:00'; # Something unnecessarily low...
    foreach my $row (split("\n", $$team{'drives'})) {
      # Skip blank rows
      next if $row =~ m/^\s*$/;

      # Parse...
      my ($time_start_min, $time_start_sec, $time_end_min, $time_end_sec, $top_min, $top_sec, $obtained, $start_pos, $plays, $yards_gained, $yards_penalties, $yards_net, $first_downs, $result)
        = ($row =~ m/^\s*\d+\s*(\d{1,2}):(\d{2})\s*(\d{1,2}):(\d{2})\s*(\d{1,2}):(\d{2})\s*(.*?)\s*([A-Z]{0,3} ?\d+)?\s*(\d+)\s*(\-?\d+)\s*(\-?\d+)\s*(\-?\d+)\s*(\d+)\s*\*?\s*(?:[A-Z]{0,3} ?\d+)?\s*(.*?)\s*$/s);

      # Convert the times
      my $time_start = sprintf('00:%02d:%02d', $time_start_min, $time_start_sec);
      my $time_end = sprintf('00:%02d:%02d', $time_end_min, $time_end_sec);
      my $top = sprintf('00:%02d:%02d', $top_min, $top_sec);

      # New quarter?
      $quarter++ if ($time_start gt $last_start);
      $last_start = $time_start;

      # Set some additional vars...
      $drive_id++;
      my $quarter_end = $quarter + ($time_end gt $time_start ? 1 : 0);

      # Split the obtained in case the RegEx doesn't match correctly...
      if ((!defined($start_pos) || $start_pos !~ m/^[A-Z]{2,3}/) && $obtained =~ m/ [A-Z]{2,3}$/) {
        my ($start_pos_half) = ($obtained =~ m/ ([A-Z]{2,3})$/);
        $start_pos = $start_pos_half . ' ' . $start_pos;
        $obtained =~ s/\s+[A-Z]{2,3}//;
      }

      # Split the start position in to a half and yard marker
      my $start_pos_half;
      my $start_pos_yd;
      if (!defined($start_pos)) {
        # Unknown, so assume Own 20
        $start_pos_half = $$team{'team_id'};
        $start_pos_yd = 20;
      } else {
        ($start_pos_half, $start_pos_yd) = ($start_pos =~ m/^([A-Z]{2,3})\s*(\d+)$/) if defined($start_pos);
        $start_pos_half = undef if defined($start_pos_half) && $start_pos_half eq 'ZZZ';
        $start_pos_half = convert_team_id($start_pos_half) if defined($start_pos_half) && $start_pos_half !~ m/^\s*$/;

        # No match implies mid-field start
        $start_pos_yd = 50 if !defined($start_pos_half) && !defined($start_pos_yd);
      }

      # Database encode the text fields
      $obtained = convert_text($obtained) if defined($obtained);
      $result = convert_text($result);

      # Check for NULLs
      check_for_null(\$obtained);
      check_for_null(\$start_pos_half);
      check_for_null(\$start_pos_yd);

      # Store
      print "INSERT INTO `SPORTS_NFL_GAME_DRIVES` (`season`, `game_type`, `week`, `game_id`, `drive_id`, `team_id`, `start_quarter`, `start_time`, `end_quarter`, `end_time`, `time_of_poss`, `obtained`, `start_pos_half`, `start_pos_yd`, `num_plays`, `yards_gained`, `yards_penalties`, `yards_net`, `num_first_downs`, `end_result`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$drive_id', '$$team{'team_id'}', '$quarter', '$time_start', '$quarter_end', '$time_end', '$top', $obtained, $start_pos_half, $start_pos_yd, '$plays', '$yards_gained', '$yards_penalties', '$yards_net', '$first_downs', '$result');\n";
    }
    print "\n";
  }
}

# Parse the final team stats
sub parse_team_stats {
  print "##\n## Loading Team Stats from Gamebook\n##\n";
  my ($stats) = ($contents =~ m/Final Team Statistics[^\n]*\n\s+Visitor\s+Home[^\n]*\n[^\n]+\n(.*?)\cL/gsi);
  $stats =~ s/\n[ \t]*\n/\n/gs;

  ## Downs
  # Get
  my ($away_1st_rush, $home_1st_rush, $away_1st_pass, $home_1st_pass, $away_1st_pen, $home_1st_pen)
    = ($stats =~ m/TOTAL FIRST DOWNS\s+\d+\s+\d+\s+By Rushing\s+(\d+)\s+(\d+)\s+By Passing\s+(\d+)\s+(\d+)\s+By Penalty\s+(\d+)\s+(\d+)\s/gsi);
  ($away_1st_rush, $away_1st_pass, $away_1st_pen, $home_1st_rush, $home_1st_pass, $home_1st_pen)
    = ($stats =~ m/TOTAL FIRST DOWNS\s+\d+\s+\d+\s+First Downs Rushing-Passing-by Penalty\s+(\d+)\s*?\-\s*?(\d+)\s*?\-\s*?(\d+)\s+(\d+)\s*?\-\s*?(\d+)\s*?\-\s*?(\d+)/gsi)
     if !defined($away_1st_rush);

  my ($away_3rd_cnv, $away_3rd_num, $home_3rd_cnv, $home_3rd_num)
    = ($stats =~ m/THIRD DOWN EFFICIENCY\s+(\d+)-(\d+)-[\d\.]+\%\s+(\d+)-(\d+)-[\d\.]+%/gsi);

  my ($away_4th_cnv, $away_4th_num, $home_4th_cnv, $home_4th_num)
    = ($stats =~ m/FOURTH DOWN EFFICIENCY\s+(\d+)-(\d+)-[\d\.]+\%\s+(\d+)-(\d+)-[\d\.]+%/gsi);

  # Display
  print "# Downs\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_DOWNS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num_1st_rush`, `num_1st_pass`, `num_1st_pen`, `num_3rd`, `num_3rd_cnv`, `num_4th`, `num_4th_cnv`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_1st_rush', '$away_1st_pass', '$away_1st_pen', '$away_3rd_num', '$away_3rd_cnv', '$away_4th_num', '$away_4th_cnv');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_DOWNS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num_1st_rush`, `num_1st_pass`, `num_1st_pen`, `num_3rd`, `num_3rd_cnv`, `num_4th`, `num_4th_cnv`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_1st_rush', '$home_1st_pass', '$home_1st_pen', '$home_3rd_num', '$home_3rd_cnv', '$home_4th_num', '$home_4th_cnv');\n\n";

  ## Plays / Yardage
  # Get
  my ($away_off_plays, $home_off_plays)
    = ($stats =~ m/Total Offensive Plays \(inc\. times thrown passing\)\s+(\d+)\s+(\d+)\s/gsi);

  my ($away_yards_rush, $home_yards_rush, $away_num_rush, $home_num_rush, $away_rush_tfl, $away_rush_tfl_yards, $home_rush_tfl, $home_rush_tfl_yards)
    = ($stats =~ m/NET YARDS RUSHING\s+(-?\d+)\s+(-?\d+)\s+Total Rushing Plays\s+(\d+)\s+(\d+)\s+[^\n]+\n\s+Tackles for a loss-number and yards\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);

  my ($away_yards_pass, $home_yards_pass)
    = ($stats =~ m/NET YARDS PASSING\s+(-?\d+)\s+(-?\d+)\s/gsi);

  my ($away_num_pass_att, $away_num_pass_cmp, $away_num_pass_int, $home_num_pass_att, $home_num_pass_cmp, $home_num_pass_int)
    = ($stats =~ m/PASS ATTEMPTS-COMPLETIONS-HAD INTERCEPTED\s+(\d+)-(\d+)-(\d+)\s+(\d+)-(\d+)-(\d+)\s/gsi);

  # Display
  print "# Plays and Yardage\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_PLAYS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `total`, `num_pass_att`, `num_pass_cmp`, `num_rush`, `num_rush_tfl`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_off_plays', '$away_num_pass_att', '$away_num_pass_cmp', '$away_num_rush', '$away_rush_tfl');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_PLAYS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `total`, `num_pass_att`, `num_pass_cmp`, `num_rush`, `num_rush_tfl`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_off_plays', '$home_num_pass_att', '$home_num_pass_cmp', '$home_num_rush', '$home_rush_tfl');\n\n";

  print "INSERT INTO `SPORTS_NFL_GAME_STATS_YARDS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `yards_pass`, `yards_rush`, `yards_rush_tfl`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_yards_pass', '$away_yards_rush', '$away_rush_tfl_yards');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_YARDS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `yards_pass`, `yards_rush`, `yards_rush_tfl`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_yards_pass', '$home_yards_rush', '$home_rush_tfl_yards');\n\n";

  ## Special Teams
  # Get
  my ($away_ko_num, $away_ko_end, $away_ko_tb, $home_ko_num, $home_ko_end, $home_ko_tb)
    = ($stats =~ m/KICKOFFS Number-In End Zone-Touchbacks\s+(\d+)-(\d+)-(\d+)\s+(\d+)-(\d+)-(\d+)\s/gsi);

  my ($away_punt_num, $away_punt_avg, $home_punt_num, $home_punt_avg, $away_punt_blocked, $home_punt_blocked)
    = ($stats =~ m/PUNTS Number and Average\s+(\d+)-(-?[\d\.]+)\s+(\d+)-(-?[\d\.]+)\s+Had Blocked\s+(\d+)\s+(\d+)\s/gsi);

  my ($away_punt_net, $home_punt_net)
    = ($stats =~ m/Net Punting Average\s+(-?[\d\.]+)\s+(-?[\d\.]+)\s/gsi);

  my ($away_ret_punt_num, $away_ret_punt_yards, $home_ret_punt_num, $home_ret_punt_yards, $away_ret_ko_num, $away_ret_ko_yards, $home_ret_ko_num, $home_ret_ko_yards, $away_ret_int_num, $away_ret_int_yards, $home_ret_int_num, $home_ret_int_yards)
    = ($stats =~ m/TOTAL RETURN YARDAGE \(Not Including Kickoffs\)[^\n]+\n\s+No\. and Yards Punt Returns\s+(\d+)-(-?\d+)\s+(\d+)-(-?\d+)\s+No\. and Yards Kickoff Returns\s+(\d+)-(-?\d+)\s+(\d+)-(-?\d+)\s+No\. and Yards Interception Returns\s+(\d+)-(-?\d+)\s+(\d+)-(-?\d+)\s/gsi);

  # Convert the punting stats from averages to totals
  my $away_punt_yards = sprintf('%.0f', $away_punt_num * $away_punt_avg); $away_punt_net = sprintf('%.0f', $away_punt_num * $away_punt_net);
  my $home_punt_yards = sprintf('%.0f', $home_punt_num * $home_punt_avg); $home_punt_net = sprintf('%.0f', $home_punt_num * $home_punt_net);

  # Display
  print "# Special Teams\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_KICKOFFS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num`, `end_zone`, `touchback`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_ko_num', '$away_ko_end', '$away_ko_tb');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_KICKOFFS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num`, `end_zone`, `touchback`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_ko_num', '$home_ko_end', '$home_ko_tb');\n\n";

  print "INSERT INTO `SPORTS_NFL_GAME_STATS_PUNTS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num`, `yards`, `net`, `blocked`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_punt_num', '$away_punt_yards', '$away_punt_net', '$away_punt_blocked');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_PUNTS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num`, `yards`, `net`, `blocked`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_punt_num', '$home_punt_yards', '$home_punt_net', '$home_punt_blocked');\n\n";

  print "INSERT INTO `SPORTS_NFL_GAME_STATS_RETURNS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `ko_num`, `ko_yards`, `punt_num`, `punt_yards`, `int_num`, `int_yards`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_ret_ko_num', '$away_ret_ko_yards', '$away_ret_punt_num', '$away_ret_punt_yards', '$away_ret_int_num', '$away_ret_int_yards');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_RETURNS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `ko_num`, `ko_yards`, `punt_num`, `punt_yards`, `int_num`, `int_yards`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_ret_ko_num', '$home_ret_ko_yards', '$home_ret_punt_num', '$home_ret_punt_yards', '$home_ret_int_num', '$home_ret_int_yards');\n\n";

  ## Scoring
  # Get
  my ($td_stats) = ($stats =~ m/TOUCHDOWNS[^\n]+\n(.*?)EXTRA POINTS/gsi);
  my ($away_td_rush, $home_td_rush)
    = ($td_stats =~ m/Rushing\s+(\d+)\s+(\d+)\s/gsi);
  convert_undef_zero(\$away_td_rush); convert_undef_zero(\$home_td_rush);

  my ($away_td_pass, $home_td_pass)
    = ($td_stats =~ m/Passing\s+(\d+)\s+(\d+)\s/gsi);
  convert_undef_zero(\$away_td_pass); convert_undef_zero(\$home_td_pass);

  my ($away_td_int, $home_td_int)
    = ($td_stats =~ m/Interceptions\s+(\d+)\s+(\d+)\s/gsi);
  convert_undef_zero(\$away_td_int); convert_undef_zero(\$home_td_int);

  my ($away_td_kret, $home_td_kret)
    = ($td_stats =~ m/Kickoff Returns\s+(\d+)\s+(\d+)\s/gsi);
  convert_undef_zero(\$away_td_kret); convert_undef_zero(\$home_td_kret);

  my ($away_td_pret, $home_td_pret)
    = ($td_stats =~ m/Punt Returns\s+(\d+)\s+(\d+)\s/gsi);
  convert_undef_zero(\$away_td_pret); convert_undef_zero(\$home_td_pret);

  my ($away_td_fumbles, $home_td_fumbles)
    = ($td_stats =~ m/Fumbles\s+(\d+)\s+(\d+)\s/gsi);
  convert_undef_zero(\$away_td_fumbles); convert_undef_zero(\$home_td_fumbles);

  my ($away_td_other, $home_td_other)
    = ($td_stats =~ m/Other \(Blocked Kicks, etc\.\)\s+(\d+)\s+(\d+)\s/gsi);
  convert_undef_zero(\$away_td_other); convert_undef_zero(\$home_td_other);

  my ($xp_stats) = ($stats =~ m/EXTRA POINTS[^\n]+\n(.*?)FIELD GOALS/gsi);
  my ($away_2pt_pass_made, $away_2pt_pass_att, $home_2pt_pass_made, $home_2pt_pass_att)
    = ($xp_stats =~ m/Passing Made-Attempts\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);
  convert_undef_zero(\$away_2pt_pass_made); convert_undef_zero(\$away_2pt_pass_att); convert_undef_zero(\$home_2pt_pass_made); convert_undef_zero(\$home_2pt_pass_att);
  my ($away_2pt_rush_made, $away_2pt_rush_att, $home_2pt_rush_made, $home_2pt_rush_att)
    = ($xp_stats =~ m/Rushing Made-Attempts\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);
  convert_undef_zero(\$away_2pt_rush_made); convert_undef_zero(\$away_2pt_rush_att); convert_undef_zero(\$home_2pt_rush_made); convert_undef_zero(\$home_2pt_rush_att);

  my ($away_safties, $home_safties)
    = ($stats =~ m/SAFETIES\s+(\d+)\s+(\d+)\s/gsi);

  # Display
  # - Doing the INSERT INTO ... SELECT FROM and only using fumbles own/opp split intentionally...
  print "# Scoring\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_TDS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num_pass`, `num_rush`, `num_int`, `num_fumbles`, `num_fumbles_own`, `num_fumbles_opp`, `num_ko`, `num_punt`, `num_other`, `safties`)
  SELECT season, game_type, week, game_id, team_id, '$away_td_pass', '$away_td_rush', '$away_td_int', '$away_td_fumbles', SUM(rec_own_td), SUM(rec_opp_td), '$away_td_kret', '$away_td_pret', '$away_td_other', '$away_safties'
  FROM SPORTS_NFL_PLAYERS_GAME_FUMBLES
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   week = '$week'
  AND   game_id = '$db_game_id'
  AND   team_id = '$$game_info{'visitor_id'}'
  GROUP BY season, game_type, week, game_id, team_id;\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_TDS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `num_pass`, `num_rush`, `num_int`, `num_fumbles`, `num_fumbles_own`, `num_fumbles_opp`, `num_ko`, `num_punt`, `num_other`, `safties`)
  SELECT season, game_type, week, game_id, team_id, '$home_td_pass', '$home_td_rush', '$home_td_int', '$home_td_fumbles', SUM(rec_own_td), SUM(rec_opp_td), '$home_td_kret', '$home_td_pret', '$home_td_other', '$home_safties'
  FROM SPORTS_NFL_PLAYERS_GAME_FUMBLES
  WHERE season = '$season'
  AND   game_type = '$game_type'
  AND   week = '$week'
  AND   game_id = '$db_game_id'
  AND   team_id = '$$game_info{'home_id'}'
  GROUP BY season, game_type, week, game_id, team_id;\n\n";

  print "INSERT INTO `SPORTS_NFL_GAME_STATS_2PT` (`season`, `game_type`, `week`, `game_id`, `team_id`, `pass_att`, `pass_made`, `rush_att`, `rush_made`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_2pt_pass_att', '$away_2pt_pass_made', '$away_2pt_rush_att', '$away_2pt_rush_made');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_2PT` (`season`, `game_type`, `week`, `game_id`, `team_id`, `pass_att`, `pass_made`, `rush_att`, `rush_made`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_2pt_pass_att', '$home_2pt_pass_made', '$home_2pt_rush_att', '$home_2pt_rush_made');\n\n";

  ## Kicks
  # Get (uses $xp_stats from above)
  my ($away_pat_made, $away_pat_att, $home_pat_made, $home_pat_att)
    = ($xp_stats =~ m/Kicking Made-Attempts\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);

  my ($away_fg_made, $away_fg_att, $home_fg_made, $home_fg_att)
    = ($stats =~ m/FIELD GOALS Made-Attempts\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);

  my ($away_fg_blocked, $away_pat_blocked, $home_fg_blocked, $home_pat_blocked)
    = ($stats =~ m/FGs - PATs Had Blocked\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);

  # Display
  print "# Kicks\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_KICKS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `fg_att`, `fg_made`, `fg_blocked`, `pat_att`, `pat_made`, `pat_blocked`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_fg_att', '$away_fg_made', '$away_fg_blocked', '$away_pat_att', '$away_pat_made', '$away_pat_blocked');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_KICKS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `fg_att`, `fg_made`, `fg_blocked`, `pat_att`, `pat_made`, `pat_blocked`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_fg_att', '$home_fg_made', '$home_fg_blocked', '$home_pat_att', '$home_pat_made', '$home_pat_blocked');\n\n";

  ## Turnovers
  # Get (uses $*_num_pass_int from plays
  my ($away_fum_num, $away_fum_lost, $home_fum_num, $home_fum_lost)
    = ($stats =~ m/FUMBLES Number and Lost\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);

  # Display
  print "# Turnovers\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_TURNOVERS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `int_num`, `fumb_num`, `fumb_lost`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '$away_num_pass_int', '$away_fum_num', '$away_fum_lost');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_TURNOVERS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `int_num`, `fumb_num`, `fumb_lost`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '$home_num_pass_int', '$home_fum_num', '$home_fum_lost');\n\n";

  ## Misc
  # Get
  my ($away_pen_num, $away_pen_yards, $home_pen_num, $home_pen_yards)
    = ($stats =~ m/PENALTIES Number and Yards\s+(\d+)-(\d+)\s+(\d+)-(\d+)\s/gsi);

  my ($away_rz_cnv, $away_rz_num, $home_rz_cnv, $home_rz_num)
    = ($stats =~ m/RED ZONE EFFICIENCY\s+(\d+)-(\d+)-\d+%\s+(\d+)-(\d+)-\d+%/gsi);

  my ($away_g2g_cnv, $away_g2g_num, $home_g2g_cnv, $home_g2g_num)
    = ($stats =~ m/GOAL TO GO EFFICIENCY\s+(\d+)-(\d+)-\d+%\s+(\d+)-(\d+)-\d+%/gsi);

  my ($away_top, $home_top)
    = ($stats =~ m/TIME OF POSSESSION\s+(\d{1,2}:\d{2})\s+(\d{1,2}:\d{2})/gsi);

  # Display
  print "# Misc\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_MISC` (`season`, `game_type`, `week`, `game_id`, `team_id`, `time_of_poss`, `pens_num`, `pens_yards`, `rz_num`, `rz_cnv`, `goal_num`, `goal_cnv`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'visitor_id'}', '00:$away_top', '$away_pen_num', '$away_pen_yards', '$away_rz_num', '$away_rz_cnv', '$away_g2g_num', '$away_g2g_cnv');\n";
  print "INSERT INTO `SPORTS_NFL_GAME_STATS_MISC` (`season`, `game_type`, `week`, `game_id`, `team_id`, `time_of_poss`, `pens_num`, `pens_yards`, `rz_num`, `rz_cnv`, `goal_num`, `goal_cnv`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{'home_id'}', '00:$home_top', '$home_pen_num', '$home_pen_yards', '$home_rz_num', '$home_rz_cnv', '$home_g2g_num', '$home_g2g_cnv');\n\n";
}

