#!/usr/bin/perl -w
# Load the player info

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use JSON;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season, $game_type, $db_game_id) = @ARGV;
our $week = substr($db_game_id, 0, -2);
my $dump = grep(/^--dump$/, @ARGV);
my $game_id = ($db_game_id % 100);
our $game_concat = $season . '//' . $game_type . '//' . $db_game_id;

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3]) && !$dump) {
  # Which tables?
  my @tables = ( 'SPORTS_NFL_GAME_LINEUP' );

  clear_previous($season, $game_type, $week, $db_game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

print "##\n## Loading players for $season // $game_type // $week // $game_id\n##\n";

# First check, does the file exist, and if it does, load it
my ($file, $contents) = load_file($season, $game_type, $week, $game_id, 'stats', { 'ext' => 'json' });
print "# Loading Game Stats: $file\n";
our $blob = decode_json($contents); # Convert to hash

# Fixes to run on the data?
my $fix_file = "$config{'base_dir'}/_data/fixes/$season/stats.pl";
require $fix_file
  if -e $fix_file;

# Dump player array with keys
if ($dump) {
  my $i = 0;
  foreach my $player (@{$$blob{'data'}{'viewer'}{'playerGameStats'}{'edges'}}) {
    my %player = %{$$player{'node'}{'player'}};
    # Determine the remote ID (from the mugshot URL)
    my $mugshot_url = (defined($player{'person'}{'headshot'}{'url'}) ? $player{'person'}{'headshot'}{'url'} : '');
    my ($remote_mugshot) = ($mugshot_url =~ m/\/([^\/]+)$/);
    $remote_mugshot = '(unknown)'
      if !defined($remote_mugshot);
    # Render our debug info
    print STDERR "$i: $player{'position'} $player{'person'}{'displayName'} (\"$player{'person'}{'firstName'}\" \"$player{'person'}{'lastName'}\"), $player{'currentTeam'}{'abbreviation'} #$player{'jerseyNumber'} - $remote_mugshot\n";
    $i++;
  }
  print STDERR "\n";
  exit;
}

my %rosters = (
  'one' => { 'away' => { }, 'home' => { } },
  'two' => { 'away' => { }, 'home' => { } },
  'three' => { 'away' => { }, 'home' => { } },
  'nosuffix' => { 'away' => { }, 'home' => { } },
  'full' => { 'away' => { }, 'home' => { } },
  'jersey' => { 'away' => { }, 'home' => { } },
  'database' => { 'away' => undef, 'home' => undef },
);

# Loop through the players we have
print "#  - Loading all players from game stats file:";
foreach my $raw (@{$$blob{'data'}{'viewer'}{'playerGameStats'}{'edges'}}) {
  my $player = $$raw{'node'}{'player'};

  # Ensure single digit values are the numeric equivalent
  $$player{'jerseyNumber'} =~ s/^0([1-9])/$1/
    if defined($$player{'jerseyNumber'});

  # A home player or away?
  $$player{'team_id'} = convert_team_id($$player{'currentTeam'}{'abbreviation'});
  my $team_type = ($$player{'team_id'} eq $$game_info{'home_id'} ? 'home' : 'away');

  # Parse the name details
  $$player{'person'}{'firstNameBackup'} = $$player{'person'}{'firstName'};
  $$player{'person'}{'lastName'} = convert_text($$player{'person'}{'lastName'});
  $$player{'person'}{'firstName'} = convert_text($$player{'person'}{'displayName'});
  $$player{'person'}{'firstName'} =~ s/ $$player{'person'}{'lastName'}(?: i+)?( [sj]r\.)?$//i;
  $$player{'nameDebug'} = "$$player{'team_id'} #{jersey}, '$$player{'person'}{'firstName'} $$player{'person'}{'lastName'}'";

  # Get the remote mugshot
  my $mugshot_url = (defined($$player{'person'}{'headshot'}{'url'}) ? $$player{'person'}{'headshot'}{'url'} : '');
  ($$player{'remote_mugshot'}) = ($mugshot_url =~ m/\/([^\/]+)$/);

  # Encode certain key fields
  $$player{'jerseyNumber'} = convert_text($$player{'jerseyNumber'});
  $$player{'position'} = convert_text($$player{'position'});

  # Backup some fields for use in keys
  my $fname = lc $$player{'person'}{'firstName'};
  my $sname = lc $$player{'person'}{'lastName'};
  $$player{'nameCmp'} = substr($fname, 0, 1) . ".$sname";
  my $tmp = "# $$player{'person'}{'firstName'} $$player{'person'}{'lastName'} ($$player{'team_id'} #{jersey}, $$player{'position'})\n";

  # Attach to this game
  $tmp .= "INSERT INTO `SPORTS_NFL_GAME_LINEUP` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `player_id`, `pos`, `status`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$player{'team_id'}', '{jersey}', '{player_id}', '{pos}', '{status}');\n\n";

  # Bespoke fields for our sorting...
  $fname =~ s/[^a-z]//ig;
  # A single fname version with the surname stripped of roman numerals
  $$player{'nameNoSuffix'} = substr($fname, 0, 1) . '.' . $sname;
  $$player{'nameNoSuffix'} =~ s/ i+v?$//; # Assuming no more than fifth generation?
  # Then some more standard versions
  $sname =~ s/ //g;
  $$player{'name1'} = substr($fname, 0, 1) . '.' . $sname;
  $$player{'name2'} = substr($fname, 0, 2) . '.' . $sname;
  $$player{'name3'} = substr($fname, 0, 3) . '.' . $sname;
  $$player{'nameFull'} = $fname . $sname; # Skip the dot separator (See: DanielThomas, 2014/regular/4/8)

  # Store for processing...
  my %info = ( 'sql' => $tmp, 'row' => $player );
  add_gamestats_roster($rosters{'one'}{$team_type}, $$player{'name1'}, \%info);
  add_gamestats_roster($rosters{'two'}{$team_type}, $$player{'name2'}, \%info);
  add_gamestats_roster($rosters{'three'}{$team_type}, $$player{'name3'}, \%info);
  add_gamestats_roster($rosters{'nosuffix'}{$team_type}, $$player{'nameNoSuffix'}, \%info);
  add_gamestats_roster($rosters{'full'}{$team_type}, $$player{'nameFull'}, \%info);
  add_gamestats_roster($rosters{'jersey'}{$team_type}, $$player{'jerseyNumber'}, \%info)
    if defined($$player{'jerseyNumber'});
}
print " [ Done ]\n";

# Prepare our query to gather player ID's
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
# - Player from mugshot ID
my $mugshot_sth = $dbh->prepare("SELECT player_id FROM SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS WHERE remote_mugshot = ?;");
# - Convert a gamebook position into a parsed value
my $gamebook_pos_sth = $dbh->prepare('SELECT IFNULL(pos_code, ?) AS pos_parsed FROM SPORTS_NFL_POSITIONS_RAW WHERE pos_raw = ?;');
# - Process rosters for both teams into a format we can process below
my $sql = "SELECT IF(SPORTS_NFL_TEAMS_ROSTERS.team_id = ?, 'home', 'away') AS team_type,
       SPORTS_NFL_TEAMS_ROSTERS.player_id, SPORTS_NFL_TEAMS_ROSTERS.jersey,
       SPORTS_NFL_TEAMS_ROSTERS.pos, IFNULL(SPORTS_NFL_POSITIONS_RAW.pos_code, SPORTS_NFL_TEAMS_ROSTERS.pos) AS posParsed,
       CONCAT(SPORTS_NFL_PLAYERS.first_name, ' ', SPORTS_NFL_PLAYERS.surname) AS name,
       LOWER(SPORTS_NFL_PLAYERS.first_name) AS first_name, LOWER(SPORTS_NFL_PLAYERS.surname) AS surname,
       LOWER(CONCAT(SUBSTRING(SPORTS_NFL_PLAYERS.first_name, 1, 1), '.', SPORTS_NFL_PLAYERS.surname)) AS nameCmp
FROM SPORTS_NFL_TEAMS_ROSTERS
JOIN SPORTS_NFL_PLAYERS
  ON (SPORTS_NFL_PLAYERS.player_id = SPORTS_NFL_TEAMS_ROSTERS.player_id)
LEFT JOIN SPORTS_NFL_POSITIONS_RAW
  ON (SPORTS_NFL_POSITIONS_RAW.pos_raw = SPORTS_NFL_TEAMS_ROSTERS.pos)
WHERE SPORTS_NFL_TEAMS_ROSTERS.season = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.game_type = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.week = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.team_id IN (?, ?)
AND   SPORTS_NFL_TEAMS_ROSTERS.jersey IS NOT NULL
ORDER BY SPORTS_NFL_TEAMS_ROSTERS.team_id, SPORTS_NFL_PLAYERS.surname, SPORTS_NFL_PLAYERS.first_name;";
foreach my $row (@{$dbh->selectall_arrayref($sql, { Slice => {} }, $$game_info{'home_id'}, $season, $game_type, $week, $$game_info{'visitor_id'}, $$game_info{'home_id'})}) {
  $$row{'nameCmp'} = convert_text($$row{'nameCmp'});
  $$row{'nameCmpNoSuffix'} = $$row{'nameCmp'}; $$row{'nameCmpNoSuffix'} =~ s/ [i+]+//i; $$row{'nameCmpNoSuffix'} =~ s/ [sj]r\.?//i;
  @{$rosters{'database'}{$$row{'team_type'}}{$$row{'jersey'}}} = ()
    if !defined($rosters{'database'}{$$row{'team_type'}}{$$row{'jersey'}});
  push @{$rosters{'database'}{$$row{'team_type'}}{$$row{'jersey'}}}, $row;
}

# Next, load the gamebook
($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamebook');
print "# Loading Gamebook: $file\n\n";

my @hashes = ( { 'key' => 'away', 'id' => 'visitor_id' },
               { 'key' => 'home', 'id' => 'home_id' } );
my $game_rosters = get_gamebook_rosters("$season//$game_type//$week//$game_id", $contents);

foreach my $hash (@hashes) {
  print "#\n# $$game_info{$$hash{'id'}}\n#\n";
  foreach my $status ('starters', 'subs', 'dnp', 'inactives') {
    foreach my $player (@{$$game_rosters{$$hash{'key'}}{$status}}) {
      process_player($player, $$hash{'key'});
    }
  }
}

print "##\n## Completed loading players for $season // $game_type // $week // $game_id\n##\n";

# Disconnect from the database
undef $mugshot_sth if defined($mugshot_sth);
undef $gamebook_pos_sth if defined($gamebook_pos_sth);
$dbh->disconnect if defined($dbh);

# Store gamestats player info in an array
sub add_gamestats_roster {
  my ($hash, $key, $info) = @_;

  # Add to a previous list?
  if (defined($$hash{$key}) && ref($$hash{$key}) eq 'ARRAY') {
    push @{$$hash{$key}}, $info;

  # Add to a previous scalar?
  } elsif (defined($$hash{$key})) {
    $$hash{$key} = [ $$hash{$key} ];
    push @{$$hash{$key}}, $info;

  # Add as a scalar...
  } else {
    $$hash{$key} = $info;
  }
}

# Find players and display the output
sub process_player {
  my ($player, $key) = @_;

  # Add this player to the list, interpolating the relevant info
  $$player{'name'} = convert_text($$player{'name'});
  my $info = get_player($key, $$player{'name'}, $$player{'pos'}, $$player{'jersey'});
  return if !defined($info);

  # Establish the position, and convert
  $$player{'pos'} = $$info{'row'}{'pos'} if !defined($$player{'pos'});
  parse_pos(\$$player{'pos'});
  $gamebook_pos_sth->execute($$player{'pos'}, $$player{'pos'});
  ($$player{'posParsed'}) = $gamebook_pos_sth->fetchrow_array;

  # Process and display
  my $tmp = $$info{'sql'};
  $tmp =~ s/{jersey}/$$player{'jersey'}/g;
  $tmp =~ s/{pos}/$$player{'pos'}/g;
  $tmp =~ s/{status}/$$player{'status'}/g;

  # Get the player_id
  if (!defined($$info{'player_id'})) {
    # First, via mugshot ID (if available)
    if (defined($$info{'row'}{'remote_mugshot'})) {
      $mugshot_sth->execute($$info{'row'}{'remote_mugshot'});
      ($$info{'player_id'}) = $mugshot_sth->fetchrow_array;
    }
    # Secondly, via the roster details
    if (!defined($$info{'player_id'})) {
      my @jerseys = sort keys %{$rosters{'database'}{$key}};
      for (my $i = 0; !defined($$info{'player_id'}) && $i < @jerseys; $i++) {
        my @cmp = @{$rosters{'database'}{$key}{$jerseys[$i]}};
        for (my $j = 0; !defined($$info{'player_id'}) && $j < @cmp; $j++) {
          $$info{'player_id'} = $cmp[$j]{'player_id'}
            if (($cmp[$j]{'nameCmp'} eq $$info{'row'}{'nameNoSuffix'}) || ($cmp[$j]{'nameCmpNoSuffix'} eq $$info{'row'}{'nameNoSuffix'}))
              && ($cmp[$j]{'jersey'} eq $$player{'jersey'}
                || $cmp[$j]{'pos'} eq $$player{'pos'}
                || $cmp[$j]{'posParsed'} eq $$player{'pos'}
                || $cmp[$j]{'pos'} eq $$player{'posParsed'}
                || $cmp[$j]{'posParsed'} eq $$player{'posParsed'});
        }
      }
    }
  }
  # Process it in the context of the SQL
  if (defined($$info{'player_id'})) {
    $tmp =~ s/{player_id}/$$info{'player_id'}/g;
  } elsif (defined($$info{'row'}{'nameDebug'})) {
    $tmp =~ s/{player_id}/** ID_MISSING **/g;
    $$info{'row'}{'nameDebug'} =~ s/{jersey}/$$player{'jersey'}/g;
    print STDERR "Unable to find player_id for $$info{'row'}{'nameDebug'}\n";
  }

  # Output our player SQL
  print $tmp;
}

# Establish a player
sub get_player {
  my ($key, $name, $pos, $jersey, $no_recurse) = @_;
  $pos = '' if !defined($pos);

  # Conver the names in to a standard format (as there are the odd instances where this isn't the case...)
  $name =~ s/^([^\. ]+) +([^\. ]+)$/$1.$2/; # Space separator instead of a dot
  $name =~ s/ //g; $name = lc $name;

  # Work backwards, from three to one and use jersey as a fallback...
  foreach my $num ( 'full', 'three', 'two', 'one', 'nosuffix' ) {
    next if !defined($rosters{$num}{$key}{$name});

    # If a scalar, return what we have...
    return $rosters{$num}{$key}{$name}
      if ref($rosters{$num}{$key}{$name}) ne 'ARRAY';

    # Matching names, so break by jersey?
    my @m = ( );
    foreach my $m (@{$rosters{$num}{$key}{$name}}) {
      my $m_jersey = $$m{'row'}{'jerseyNumber'};
      $m_jersey = $$m{'row'}{'uniformNumber'} if !defined($m_jersey); # Legacy attribute support
      push @m, $m if defined($m_jersey) && $m_jersey == $jersey;
    }
    return $m[0] if @m == 1;

    # Matching by jersey too, so how about by pos?
    if ($pos ne '') {
      my @p = ( );
      foreach my $m (@m) {
        push @p, $m if $$m{'pos'} eq $pos;
      }

      return $p[0] if @p == 1;
    }
  }

  # Fallback 1: Finding players through jersey numbers...
  if (defined($rosters{'jersey'}{$key}{$jersey})) {
    my $by_jersey = $rosters{'jersey'}{$key}{$jersey};
    if (ref($by_jersey) ne 'ARRAY') {
      $by_jersey = [ $by_jersey ];
    }

    foreach my $num ( 'nameFull', 'name3', 'name2', 'name1', 'nameNoSuffix' ) {
      # Try to match by exact name....
      my @m = ( );
      foreach my $m ( @{$by_jersey} ) {
        push @m, $m if $$m{'row'}{$num} eq $name;
      }
      return $m[0] if @m == 1;

      # ... then sub-part of name (i.e., hyphenated surname in roster, not in gamebook)
      @m = ( );
      foreach my $m ( @{$by_jersey} ) {
        if (length($name) < length($$m{'row'}{$num})) {
          push @m, $m if substr($$m{'row'}{$num}, 0, length($name)) eq $name;
        }
      }
      return $m[0] if @m == 1;

      # ... then position
      if ($pos ne '') {
        my @p = ( );
        foreach my $m ( @m ) {
          push @p, $m if defined($$m{'row'}{'pos'}) && $$m{'row'}{'pos'} eq $pos;
        }
        return $p[0] if @p == 1;
      }
    }
  }

  # Fallback 2: Let's try again, but customising $name to be F.Sname... and then surname only
  if (!defined($no_recurse)) {
    # F.Sname
    my $name_mod = $name; $name_mod =~ s/^(.)[^\.]*\.(.+)$/$1.$2/;
    my $mod = get_player($key, $name_mod, $pos, $jersey, 1);
    return $mod if defined($mod);

    # Sname only
    my $snameA = $name; $snameA =~ s/^[^\.]+\.(.+)$/$1/;
    my $snameB = $name; $snameB =~ s/^.*?\.([^\.]+)$/$1/;
    my $by_jersey = $rosters{'jersey'}{$key}{$jersey};
    if (ref($by_jersey) ne 'ARRAY') {
      $by_jersey = [ $by_jersey ];
    }
    my @m = ( );
    foreach my $m ( @{$by_jersey} ) {
      if (defined($$m{'row'})) {
        my $ln = lc $$m{'row'}{'person'}{'lastName'};
        push @m, $m if $ln eq $snameA || $ln eq $snameB;
      }
    }
    return $m[0] if @m == 1;

    # Not that I like this, but if there's only one for this jersey anyway, return that player...
    return $rosters{'jersey'}{$key}{$jersey} if ref($rosters{'jersey'}{$key}{$jersey}) eq 'HASH';
  }

  # Fallback 3: From database roster (again, via jersey)
  if (!defined($no_recurse)) {
    my $team_id = ($key eq 'home' ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
    # Attempt to match on both jersey and surname
    if (defined($rosters{'database'}{$key}{$jersey})) {
      foreach my $cmp (@{$rosters{'database'}{$key}{$jersey}}) {
        if ($name =~ /\.$$cmp{'surname'}/) {
          my %row = %{$cmp};
          return {
            'player_id' => $row{'player_id'},
            'sql' => "# $row{'name'} (#{jersey}, $pos - via team roster)\nINSERT INTO `SPORTS_NFL_GAME_LINEUP` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `player_id`, `pos`, `status`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '{jersey}', '$row{'player_id'}', '{pos}', '{status}');\n\n",
          };
        }
      }
    }
  }

  # Can't find any way of uniquely identifying this player, so let's say so!
  my $team_id = ($key eq 'home' ? $$game_info{'home_id'} : $$game_info{'visitor_id'});
  print STDERR "ERROR: Unable to uniquely match '$name' ($team_id #$jersey, $pos)\n"
    if !defined($no_recurse);
  return undef;
}

