#!/usr/bin/perl -w
# Load the boxscore for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to boxscore parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NFL_PLAYERS_GAME_PASSING',
                 'SPORTS_NFL_PLAYERS_GAME_RUSHING',
                 'SPORTS_NFL_PLAYERS_GAME_RECEIVING',
                 'SPORTS_NFL_PLAYERS_GAME_PUNTS',
                 'SPORTS_NFL_PLAYERS_GAME_PUNTRET',
                 'SPORTS_NFL_PLAYERS_GAME_KICKRET',
                 'SPORTS_NFL_PLAYERS_GAME_TACKLES',
                 'SPORTS_NFL_PLAYERS_GAME_PASSDEF',
                 'SPORTS_NFL_PLAYERS_GAME_FUMBLES',
                 'SPORTS_NFL_PLAYERS_GAME_KICKING' );

  clear_previous($season, $game_type, $week, $db_game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

# Load players from the gamebook
my ($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamebook');
my ($box_file, $box_contents) = load_file($season, $game_type, $week, $game_id, 'boxscore');
print "##\n## Loading gamebook: $file\n## Loading boxscore: $box_file\n##\n";

# Load the player info
my @hashes = ( { 'key' => 'away', 'id' => 'visitor_id' },
               { 'key' => 'home', 'id' => 'home_id' } );
my $game_rosters = get_gamebook_rosters("$season//$game_type//$week//$game_id", $contents);
my %rosters = ( $$game_info{'visitor_id'} => { }, $$game_info{'home_id'} => { } );
foreach my $hash (@hashes) {
  foreach my $key ( keys %{$$game_rosters{$$hash{'key'}}} ) {
    foreach my $player ( @{$$game_rosters{$$hash{'key'}}{$key}} ) {
      $rosters{$$game_info{$$hash{'id'}}}{$$player{'name'}} = $$player{'jersey'};
    }
  }
}

# Get the offensive boxscore...
print "## Offensive stats\n";
my ($off_boxscore_cols) = ($contents =~ m/Final Individual Statistics(.*?)\n[^\n]+\n\s+FUMBLES/gsi);
my ($off_boxscore_rows) = ($contents =~ m/\n([^\n]+\n\s+FUMBLES.*?FUMBLES.*?Total[^\n]+)\n/gsi);

# Split in to home and away...
$off_boxscore_cols =~ s/^\n+//g;
my ($away_cols, $home_cols) = split_text($off_boxscore_cols, get_split_point($off_boxscore_cols));

my ($away_rows) = ($off_boxscore_rows =~ m/$$game_info{'visitor_city'} $$game_info{'visitor'}\s+FUMBLES[^\n]+\n(.*?)Total/gsi);
$away_rows = '' if !defined($away_rows);

my ($home_rows) = ($off_boxscore_rows =~ m/$$game_info{'home_city'} $$game_info{'home'}\s+FUMBLES[^\n]+\n(.*?)Total/gsi);
$home_rows = '' if !defined($home_rows);

# Stats to carry from the offensive boxscores to the defensive
my %def_extra = ( 'away' => { }, 'home' => { } );

# Now run...
foreach my $hash (@hashes) {
  my $team_id = $$game_info{$$hash{'id'}};
  print "# $team_id\n";
  my $data = ($$hash{'key'} eq 'away' ? $away_cols : $home_cols);
  my $list;

  # Passing
  print "#  - Passing\n";
  ($list) = ($data =~ m/PASSING[^\n]+(.*?)\n\s*Total/gsi);
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $atts, $cmp, $yards, $sacks, $sacks_yards, $td, $long, $int, $rating)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(\d+)\s+(-?\d+)\s+(\d+)\/(-?\d+)\s+(\d+)\s+(-?\d+)\s+(\d)\s*(\d{1,3}+\.\d)\s*$/gsi);
    next if $player =~ m/^\s*\[/; # Skip unknown data
    $player =~ s/\.\s+/./g;
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_PASSING` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `atts`, `cmp`, `yards`, `long`, `td`, `int`, `sacked`, `sack_yards`, `rating`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$atts', '$cmp', '$yards', '$long', '$td', '$int', '$sacks', '$sacks_yards', '$rating');\n";
  }

  # Rushing
  print "\n#  - Rushing\n";
  ($list) = ($data =~ m/RUSHING[^\n]+(.*?)\n\s*Total/gsi);
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $atts, $yards, $long, $td)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(-?\d+)\s+-?\d+\.\d\s+(-?\d+)\s+(\d+)\s*$/gsi);
    next if $player =~ m/^\s*\[/; # Skip unknown data
    $player =~ s/\.\s+/./g;
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_RUSHING` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `atts`, `yards`, `long`, `td`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$atts', '$yards', '$long', '$td');\n";
  }

  # Receiving
  print "\n#  - Receiving\n";
  ($list) = ($data =~ m/PASS RECEIVING[^\n]+(.*?)\n\s*Total/gsi);
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $targets, $recept, $yards, $long, $td)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(\d+)\s+(-?\d+)\s+-?\d+\.\d\s+(-?\d+)\s+(\d+)\s*$/gsi);
    next if $player =~ m/^\s*\[/; # Skip unknown data
    $player =~ s/\.\s+/./g;
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_RECEIVING` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `recept`, `yards`, `long`, `td`, `targets`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$recept', '$yards', '$long', '$td', '$targets');\n";
  }

  # Punts
  print "\n#  - Punts\n";
  ($list) = ($data =~ m/PUNTING[^\n]+(.*?)\n\s*Total/gsi);
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $num, $yards, $net, $tb, $in20, $long)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(\d+)\s+(?:\d+\.\d)?\s+(-?\d+\.\d)\s+(\d+)\s+(\d+)\s+(-?\d+)\s*$/gsi);
    next if $player =~ m/^\s*\[/; # Skip 'event' rows (touchback, etc)
    $player =~ s/\.\s+/./g;
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_PUNTS` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `num`, `yards`, `net`, `long`, `tb`, `inside20`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$num', '$yards', '" . sprintf('%0.0f', $num * $net) . "', '$long', '$tb', '$in20');\n";
  }

  # Punt Returns
  print "\n#  - Punt Returns\n";
  ($list) = ($data =~ m/PUNT RETURNS[^\n]+(.*?)\n\s*(?:Total|Returns)/gsi);
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $num, $yards, $fc, $long, $td)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(-?\d+)\s+-?\d+\.\d\s+(\d+)\s+(-?\d+)\s+(\d+)\s*$/gsi);
    next if $player =~ m/^\s*\[/; # Skip 'event' rows (touchback, etc)
    $player =~ s/\.\s+/./g;
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_PUNTRET` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `num`, `yards`, `long`, `fair_catch`, `td`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$num', '$yards', '$long', '$fc', '$td');\n";
  }

  # Kickoff Returns
  print "\n#  - Kickoff Returns\n";
  ($list) = ($data =~ m/KICKOFF RETURNS[^\n]+(.*?)\n\s*(?:Total|Returns)/gsi);
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $num, $yards, $fc, $long, $td)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(-?\d+)\s+-?\d+\.\d\s+(\d+)\s+(-?\d+)\s+(\d+)\s*$/gsi);
    next if $player =~ m/^\s*\[/; # Skip 'event' rows (touchback, etc)
    $player =~ s/\.\s+/./g;
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_KICKRET` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `num`, `yards`, `long`, `fair_catch`, `td`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$num', '$yards', '$long', '$fc', '$td');\n";
  }

  # Ints
  print "\n#  - Interceptions\n";
  ($list) = ($data =~ m/INTERCEPTIONS[^\n]+(.*?)\n\s*Total/gsi);
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $int_num, $int_yards, $int_td)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(-?\d+)\s+-?\d+\.\d\s+-?\d+\s+(\d+)\s*$/gsi);
    $player =~ s/\.\s+/./g;
    print "#    - $player / $int_num (#) / $int_yards (Yds) / $int_td (TD)\n";
    $def_extra{$$hash{'key'}}{'int'} = { } if !defined($def_extra{$$hash{'key'}}{'int'});
    $def_extra{$$hash{'key'}}{'int'}{$player} = { 'int_num' => $int_num, 'int_yards' => $int_yards, 'int_td' => $int_td };
  }

  # Fumbles
  print "\n#  - Fumbles\n";
  $list = ($$hash{'key'} eq 'away' ? $away_rows : $home_rows);
  $list =~ s/\cL.*?Final Individual Statistics(?:.*?OUT-BDS)?//s if $list =~ m/\cL/; # Fix if over multiple pages...
  foreach my $row (split("\n", $list)) {
    next if $row =~ m/^\s*$/;
    my ($player, $num_fum, $num_lost, $own_rec, $own_rec_td, $forced, $opp_rec, $opp_rec_td, $oob)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(\d+)\s+(\d+)\s+-?\d+\s+(\d+)\s+(\d+)\s+(\d+)\s+-?\d+\s+(\d+)\s+(\d+)\s*$/gsi);
    next if $player =~ m/^\s*$/ || $player =~ m/^\s*TEAM/i; # Skip rows of unknown data
    $player =~ s/\.\s+/./g;
    print "#    - $player / $num_fum-$num_lost (#-L) / $own_rec-$opp_rec ($own_rec_td-$opp_rec_td) (Own Rec-Opp Rec, TDs) / $forced (Forced) / $oob (OOB)\n";
    $def_extra{$$hash{'key'}}{'fum'} = { } if !defined($def_extra{$$hash{'key'}}{'fum'});
    $def_extra{$$hash{'key'}}{'fum'}{$player} = { 'num_fum' => $num_fum, 'num_lost' => $num_lost, 'own_rec' => $own_rec, 'own_rec_td' => $own_rec_td,
                                                  'opp_rec' => $opp_rec, 'opp_rec_td' => $opp_rec_td, 'forced' => $forced, 'oob' => $oob }
      if lc($player) ne 'unidentified'; # Handles scenario where the player is not yet known
  }

  print "\n";
}

# Then the defensive...
print "## Defensive stats\n";
my ($def_boxscore) = ($contents =~ m/Final Defensive Statistics(.*?Total.*?Total[^\n]+)/gsi);
my ($def_away, $def_home) = ($def_boxscore =~ m/\sFR[^\n]*\n(.*?)\n\s*Total.*?FF\s+FR[^\n]*\n(.*?)\n\s*Total/gs);
$def_home =~ s/\cL +[^\n]+ vs.*?Final Defensive Statistics(?:.*?FR[^\n]*\n)?//si if $def_home =~ m/\cL/; # Fix if over multiple pages...

foreach my $hash (@hashes) {
  my $team_id = $$game_info{$$hash{'id'}};
  my %sect = ( 'tackles' => [ ], 'passdef' => [ ] );
  print "# $team_id\n";

  foreach my $row (split("\n", $$hash{'key'} eq 'away' ? $def_away : $def_home)) {
    next if $row =~ m/^\s*$/ || $row =~ m/TKL.*?FR/;
    my ($player, $tckl, $asst, $sacks, $sacks_yards, $tfl, $qb_hit, $pd, $st_tckl, $st_asst, $st_blk, $misc_tckl, $misc_asst)
      = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(\d+)\s+\d+\s+(\d+(?:\.\d)?)\s+(-?\d+(?:\.\d)?)\s+(\d*)\s+(\d*)\s+\d+\s+(\d+)\s+\d+\s+\d+\s+(\d+)\s+(\d+)\s+\d+\s+\d+\s+(\d+)\s+(\d+)\s+(\d+)\s+\d+\s+\d+\s*$/);
    next if $player =~ m/^\s*\[/ || $player =~ m/^\s*TEAM/i; # Skip 'fallback' rows
    $player =~ s/\.\s+/./g;

    # Check for NULLs...
    $tfl = 0 if !defined($tfl);
    $qb_hit = 0 if !defined($qb_hit);

    # Sum some values...
    my $num_tckl = $tckl + $st_tckl + $misc_tckl;
    my $num_asst = $asst + $st_asst + $misc_asst;

    # Display
    push @{$sect{'tackles'}}, "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_TACKLES` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `tckl`, `asst`, `sacks`, `sacks_yards`, `tfl`, `qb_hit`, `kick_block`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$num_tckl', '$num_asst', '$sacks', '$sacks_yards', '$tfl', '$qb_hit', '$st_blk');\n";

    # Pull interception info from the offensive boxscore
    my %int = (defined($def_extra{$$hash{'key'}}{'int'}{$player}) ? %{$def_extra{$$hash{'key'}}{'int'}{$player}} : ( ));
    my $int_num = (defined($int{'int_yards'}) ? $int{'int_num'} : 0);
    my $int_yards = (defined($int{'int_yards'}) ? $int{'int_yards'} : 0);
    my $int_td = (defined($int{'int_td'}) ? $int{'int_td'} : 0);

    push @{$sect{'passdef'}}, "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_PASSDEF` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `pd`, `int`, `int_yards`, `int_td`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$pd', '$int_num', '$int_yards', '$int_td');\n"
      if ($int_num || $pd);
  }

  print "#  - Tackles\n";
  print join('', @{$sect{'tackles'}}) . "\n";

  print "#  - Pass Def\n";
  print join('', @{$sect{'passdef'}}) . "\n";

  # Pull fum info from the offensive boxscore
  print "#  - Fumbles\n";
  foreach my $player (keys %{$def_extra{$$hash{'key'}}{'fum'}}) {
    my $nums = $def_extra{$$hash{'key'}}{'fum'}{$player};
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_FUMBLES` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `num_fumbled`, `num_lost`, `num_forced`, `rec_own`, `rec_opp`, `rec_own_td`, `rec_opp_td`, `oob`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$$nums{'num_fum'}', '$$nums{'num_lost'}', '$$nums{'forced'}', '$$nums{'own_rec'}', '$$nums{'opp_rec'}', '$$nums{'own_rec_td'}', '$$nums{'opp_rec_td'}', '$$nums{'oob'}');\n";
  }
  print "\n";
}

# Kicking stats are more manual
print "## Kicking stats\n";
my %kickers = ( );

# First, FG - relies on the play-by-play
foreach my $att ($contents =~ m/\) ([^\n]+ \d+ yard field goal is .*?(?:timeout|elapsed|no))/gsi) {
  # Get
  my ($kicker, $length, $result) = ($att =~ m/^(?:\([^\)]+\) )?(.*?)\s(\d+) yard field goal is (.+)$/si);
  $result = lc($result);

  # Skip those play-by-play attempts that were nullified or skipped due to a penalty
  next if $result =~ /nullified/s || $result =~ m/no play/s;

  # Store
  %{$kickers{$kicker}} = ( ) if !defined($kickers{$kicker});
  $kickers{$kicker}{'fg_att'}++;
  if ($result =~ /^good/) {
    $kickers{$kicker}{'fg_made'}++;

    # Distance counters
    my $key;
    if ($length < 20) {
      $key = 'u20';
    } elsif ($length < 30) {
      $key = 'u30';
    } elsif ($length < 40) {
      $key = 'u40';
    } elsif ($length < 50) {
      $key = 'u50';
    } else {
      $key = 'o50';
    }
    $kickers{$kicker}{'fg_' . $key}++;
  }
}

# Then, XPs - relies on the boxscore (to determine missed kicks) and gamecenter (for kicker identification)
my %kickers_xp = ( );
my @box_kicking = ($box_contents =~ m/(<tr class="thd2">\s+<td>Kicking<\/td>.*?<\/tr>.*?<tr class="thd2">)/gsi);
my $team;
foreach my $box_kicking_team (@box_kicking) {
  $team = (defined($team) ? 'home' : 'away');
  # Get the kickers for this team and their stats
  my ($kicker, $kicker_name, $xp_made, $xp_att) = ($box_kicking_team =~ m/<tr class="tbdy1">\s*<td><a[^>]*href="\/players\/profile\?id=([^"]+)"[^>]*>(.*?)<\/a><\/td>\s*<td>[^<]*<\/td>\s*<td>[^<]*<\/td>\s*<td>(\d+)\/(\d+)<\/td>\s*<td>[^<]*<\/td>\s*<\/tr>/gsi);
  next if !defined($kicker); # Skip if no kicking stats for this team
  # Convert the remote_id to the team/jersey combo we use later on
  $kicker_name =~ s/\.\s+/\./;
  %{$kickers{$kicker_name}} = ( ) if !defined($kickers{$kicker_name});
  $kickers{$kicker_name}{'xp_att'} = $xp_att;
  $kickers{$kicker_name}{'xp_made'} = $xp_made;
}

# Then display as SQL
foreach my $hash (@hashes) {
  my $team_id = $$game_info{$$hash{'id'}};
  print "# $team_id\n";

  foreach my $player (keys %{$rosters{$$game_info{$$hash{'id'}}}}) {
    next if !defined($kickers{$player});

    # Do we have XP info for this kicker?
    if (defined($kickers_xp{$team_id}{$rosters{$team_id}{$player}})) {
      foreach my $key (keys %{$kickers_xp{$team_id}{$rosters{$team_id}{$player}}}) {
        $kickers{$player}{$key} = $kickers_xp{$team_id}{$rosters{$team_id}{$player}}{$key};
      }
    }

    # Check all the variables we need exist...
    foreach my $key ( 'fg_att', 'fg_made', 'fg_u20', 'fg_u30', 'fg_u40', 'fg_u50', 'fg_o50', 'xp_att', 'xp_made' ) {
      $kickers{$player}{$key} = 0 if !defined($kickers{$player}{$key});
    }

    # SQL output
    print "INSERT INTO `SPORTS_NFL_PLAYERS_GAME_KICKING` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `fg_att`, `fg_made`, `fg_u20`, `fg_u30`, `fg_u40`, `fg_u50`, `fg_o50`, `xp_att`, `xp_made`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$team_id', '$rosters{$team_id}{$player}', '$kickers{$player}{'fg_att'}', '$kickers{$player}{'fg_made'}', '$kickers{$player}{'fg_u20'}', '$kickers{$player}{'fg_u30'}', '$kickers{$player}{'fg_u40'}', '$kickers{$player}{'fg_u50'}', '$kickers{$player}{'fg_o50'}', '$kickers{$player}{'xp_att'}', '$kickers{$player}{'xp_made'}');\n";
  }
  print "\n";
}

# Finally, update with game info...
print "## Setting player_id's\n";
foreach my $tbl ('PASSING', 'RUSHING', 'RECEIVING', 'PUNTS', 'PUNTRET', 'KICKRET', 'KICKING', 'TACKLES', 'PASSDEF', 'FUMBLES') {
  print "# $tbl\nUPDATE SPORTS_NFL_PLAYERS_GAME_$tbl AS TBL
JOIN SPORTS_NFL_GAME_LINEUP AS LINEUP
  ON (LINEUP.season = TBL.season
  AND LINEUP.game_type = TBL.game_type
  AND LINEUP.week = TBL.week
  AND LINEUP.game_id = TBL.game_id
  AND LINEUP.team_id = TBL.team_id
  AND LINEUP.jersey = TBL.jersey)
SET TBL.player_id = LINEUP.player_id
WHERE TBL.season = '$season'
AND   TBL.game_type = '$game_type'
AND   TBL.week = '$week'
AND   TBL.game_id = '$db_game_id';\n\n";
}

# Get the point at which a table is split in to two columns
sub get_split_point {
  my ($txt) = @_;

  my @tmp = split("\n", $txt);
  my ($part) = ($tmp[1] =~ m/^(RUSHING.*?)RUSHING/);
  return length($part);
}

