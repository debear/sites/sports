#!/usr/bin/perl -w
# Load the boxscore for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the game summary parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
our $game_concat = $season . '//' . $game_type . '//' . $db_game_id;

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  if ($ARGV[3] eq '--reset') {
    print "## Resetting a previous data load...\n";
    print "UPDATE `SPORTS_NFL_SCHEDULE` SET `visitor_score` = NULL, `home_score` = NULL, `status` = NULL, `attendance` = NULL WHERE `season` = '$season' AND `game_type` = '$game_type' AND `week` = '$week' AND `game_id` = '$db_game_id';\n";
  }
  exit;
}

# Load the gamebook (not enough detail elsewhere)
my ($file, $content) = load_file($season, $game_type, $week, $game_id, 'gamebook');
print "##\n## Game Summary for $season // $game_type // $week // $game_id\n##\n# Loading gamebook: $file\n";

# Scores / Status
my ($score_lines) = ($content =~ m/(VISITOR:[^\n]+\n\s*HOME:[^\n]+)\n/gsi);
my ($away_ot, $away_score, $home_ot, $home_score) = ($score_lines =~ m/VISITOR:.*?(\d+)\s+(\d+)\s*\n\s*HOME:.*?(\d+)\s+(\d+)\s*$/gsi);
my $game_status = (!$away_ot && !$home_ot && $away_score != $home_score ? 'F' : 'OT');

# If a playoff game went to OT, did it go to multiple OT quarters?
if ($game_type eq 'playoff' && $game_status eq 'OT') {
  # Test via the latest scoring quarter...
  my $max_quarter = 5;
  my ($all_plays) = ($content =~ m/Scoring Plays(.*?)Attendance:/gsi);
  foreach my $row (split("\n", $all_plays)) {
    my ($team, $quarter) = ($row =~ m/^\s*(.*?)\s+(\d+)\s+\d+:\d+\s/si);
    # Skip if nothing returned
    next if !defined($team);
    $max_quarter = $quarter if $quarter > $max_quarter;
  }
  $game_status = ($max_quarter - 4) . 'OT' if $max_quarter > 5;
}

# Attendance
my ($attendance) = ($content =~ m/Attendance:\s+([\d,]+)\s/gsi);
$attendance =~ s/,//g;

# Display
print "UPDATE `SPORTS_NFL_SCHEDULE` SET `visitor_score` = '$away_score', `home_score` = '$home_score', `status` = '$game_status', `attendance` = '$attendance' WHERE `season` = '$season' AND `game_type` = '$game_type' AND `week` = '$week' AND `game_id` = '$db_game_id';\n";

