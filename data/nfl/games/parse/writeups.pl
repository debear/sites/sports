#!/usr/bin/perl -w
# Load the preview / recaps for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to write-up parsing script.\n";
  exit 98;
}

# Note: Script deprecated after Mar-2017. TD.
print STDERR "ERROR: Attempting to run the deprecated writeups script for $ARGV[0] // $ARGV[1] // $ARGV[2].\n";

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
# - Unlike the other game scripts, this is run standalone, so always needs the clear line
clear_previous($season, $game_type, $week, $db_game_id, 'SPORTS_NFL_GAME_WRITEUP');

# There are two types of file here, one for before (preview) and after (recap)
my @writeups = ( );
my @types = ( 'recap', 'recap_summary' );
foreach my $type (@types) {
  # Load the file
  my ($file, $html) = load_file($season, $game_type, $week, $game_id, $type);
  print "##\n## Loading $type: $file\n##\n";

  # Headline
  my $headline;
  my $pattern = '<h1[^>]*?>\s*(\S.*?\S)\s*</h1>';
  my @headlines = ($html =~ m/$pattern/gsi);
  if (!@headlines) {
    $headline = 'NULL';
  } else {
    $headline = "'" . convert_text(pop(@headlines)) . "'";
  }
  $html =~ s/<h1[^>]*?>.*?<\/h1>//gsi;

  # Source
  $pattern = '<span class="articleSource">By\s*(\S.*?\S)\s*<\/span>';
  my ($source) = ($html =~ m/$pattern/gsi);
  if (!defined($source)) {
    $source = 'NULL';
  } else {
    $source =~ s/<[^>]+>//g;
    $source =~ s/&nbsp;/ /g;
    $source =~ s/\s{2,}/ /g;
    trim(\$source);
    $source = "'" . convert_text($source) . "'";
  }

  # Article
  #  First, remove info boxes
  $pattern = '<table.*?<\/table>';
  my $html_article = $html;
  $html_article =~ s/$pattern//gsi;

  #  Now, obtain
  $pattern = '<div class="articleText">\s*(.*?)\s*<\/div>\s*<\/div>\s*<\/div>\s*<\/div>.*?\s*<script' if $type eq 'recap';
  $pattern = '<div class="articleText">\s*(.*?)\s*<\/div>\s*(?:<p.*?<\/p>)?\s*<\/div>\s*<\/div>\s*<div id="gc-team-leaders"' if $type eq 'recap_summary';
  my ($article) = ($html_article =~ m/$pattern/gsi);

  # Skip if no article was found...
  if (!defined($article)) {
    print "# Skipping, no article found...\n\n";
    next;
  }

  # Content is in the paragraphs
  my @para = ($article =~ m/<p[^>]*?>(.*?)<\/p>/gsi);
  $article = '<p>' . join('</p><p>', @para) . '</p>';

  # Convert some markup
  $article =~ s/<span[^>]*?style="[^"]*?font-weight:\s*?bold[^"]*?"[^>]*?>(.*?)<\/span>/<b>$1<\/b>/gsi;

  # Trim
  $article =~ s/<p>\s*<br[^>]*?\/>\s*<\/p>//gsi;
  trim(\$article);
  $article =~ s/\s{2,}/ /g;
  $article =~ s/<p>\s+/<p>/g;
  $article =~ s/\s+<\/p>/<\/p>/g;

  # Modify the article to remove any NFL.com references to players or teams
  # First, teams
  $pattern = '(<a href="\/teams\/[^\/]+\/profile\?team=[^"]+">[^\<]+<\/a>)';
  foreach my $team ($article =~ m/$pattern/gsi) {
    # Identify the individual team
    $pattern = 'profile\?team=([^"]+)"';
    my ($team_id) = ($team =~ m/$pattern/gsi);
    $team =~ s/\?/\\?/g;
    $article =~ s/$team/\{\{TEAM_$team_id\}\}/g;
  }

  # Now, players (slightly trickier as we need to use conversion codes)
  $pattern = '(<a href="\/players?\/[^\/]+\/[^/]+\/profile">[^\<]+<\/a>)';
  my %player_list = ( ); # Used to get a unique list of players to modify
  foreach my $player ($article =~ m/$pattern/gsi) {
    $pattern = '\/players?\/([^\/]+)\/([^\/]+)\/profile">(.*?)<\/a>';
    my ($href_name, $id, $name) = ($player =~ m/$pattern/gsi);
    $player_list{$id} = convert_text($name);
    $player =~ s/\?/\\?/g;
    $article =~ s/<a[^>]*?href="[^"]*?\/players?\/$href_name\/$id\/profile"[^>]*?>.*?<\/a>/\{\{PLAYER_REM_$id\}\}/gi;
  }

  # Remove other links...
  $article =~ s/<a[^>]*?>(.*?)<\/a>/$1/gsi;

  # Encode into HTML-safe text
  $article =~ s/\r//g;
  $article = convert_text($article);
  $article =~ s/\n/\\n/g;

  # Code to swap from NFL.com ID's to ours
  print "\n# Main article\nSET \@article_${season}_${game_type}_${week}_${game_id}_${type} := \"$article\";\n\n# Players...\n";
  foreach my $id (keys %player_list) {
    print "SET \@rem_$id := IFNULL((SELECT CONCAT('{{PLAYER_', `player_id`, '}}') FROM `SPORTS_NFL_PLAYERS_IMPORT` WHERE `remote_url_id` = '$id'), '$player_list{$id}');\n";
    print "SET \@article_${season}_${game_type}_${week}_${game_id}_${type} := REPLACE(\@article_${season}_${game_type}_${week}_${game_id}_${type}, '{{PLAYER_REM_$id}}', \@rem_$id);\n";
  }

  # Now display
  print "\n# Final query\nINSERT INTO `SPORTS_NFL_GAME_WRITEUP` (`season`, `game_type`, `week`, `game_id`, `writeup_type`, `headline`, `source`, `article`) VALUES ('$season', '$game_type', '$week', '$db_game_id', 'recap', $headline, $source, \@article_${season}_${game_type}_${week}_${game_id}_${type});\n\n";

  # We have a recap, so no need to continue looping...
  last;
}

