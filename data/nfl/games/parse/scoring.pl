#!/usr/bin/perl -w
# Load the scoring plays for a game

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to scoring play parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
our $game_concat = $season . '//' . $game_type . '//' . $db_game_id;

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NFL_GAME_SCORING',
                 'SPORTS_NFL_GAME_SCORING_TD',
                 'SPORTS_NFL_GAME_SCORING_PAT',
                 'SPORTS_NFL_GAME_SCORING_2PT',
                 'SPORTS_NFL_GAME_SCORING_FG',
                 'SPORTS_NFL_GAME_SCORING_SAFETY' );

  clear_previous($season, $game_type, $week, $db_game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

# Load the gamebook (not enough detail elsewhere)
my ($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamebook');
print "##\n## Loading gamebook: $file\n##\n";

# Load the player info
my @hashes = ( { 'key' => 'away', 'id' => 'visitor_id' },
               { 'key' => 'home', 'id' => 'home_id' } );
my $game_rosters = get_gamebook_rosters("$season//$game_type//$week//$game_id", $contents);
my %rosters = ( $$game_info{'visitor_id'} => { }, $$game_info{'home_id'} => { } );
foreach my $hash (@hashes) {
  foreach my $key ( keys %{$$game_rosters{$$hash{'key'}}} ) {
    foreach my $player ( @{$$game_rosters{$$hash{'key'}}{$key}} ) {
      $rosters{$$game_info{$$hash{'id'}}}{$$player{'name'}} = $$player{'jersey'};
    }
  }
}

# Get the individual scoring plays
my ($all_plays) = ($contents =~ m/Scoring Plays(.*?)Attendance:/gsi);
my %last = ( 'visitor' => 0, 'home' => 0 ); my $i = 0;
foreach my $row (split("\n", $all_plays)) {
  my ($team, $quarter, $time, $play_raw, $drive, $visitor, $home) = ($row =~ m/^\s*(.*?)\s+(\d+)\s+(\d+:\d+)\s+(.*?)\s+(\(\d+\-\-?\d+, \d+:\d+\))?\s+(\d+)\s+(\d+)\s*$/si);
  # Skip if nothing returned
  next if !defined($team);

  # Who scored? (Needs first few chars because it could wrap over multiple lines and strip leading non-upper case chars as probably from a previous line...)
  $team =~ s/^[a-z]+//g;
  my $scorer = (lc(substr($team, 0, 5)) eq lc(substr($$game_info{'visitor'}, 0, 5)) ? 'visitor' : 'home');
  my $team_id = $$game_info{$scorer . '_id'};
  my $opp_team_id = ($scorer eq 'home' ? $$game_info{'visitor_id'} : $$game_info{'home_id'});

   # What did they score?
  my $type;
  my $pts = ($scorer eq 'visitor' ? $visitor : $home) - $last{$scorer};
  if ($pts == 3) {
    # FG
    $type = 'fg';
  } elsif ($pts == 6 && $play_raw !~ m/\)$/) {
    # TD (no attempt)
    $type = 'td';
  } elsif ($pts == 7) {
    # TD (w/PAT)
    $type = 'td+pat';
  } elsif ($pts == 6 && $play_raw =~ /\([^\(]*kick[^\)]*\)/) {
    # TD (Failed PAT)
    $type = 'td-pat';
  } elsif ($pts == 8) {
    # TD (w/2pt)
    $type = 'td+2pt';
  } elsif ($pts == 6 && $play_raw !~ /\([^\(]*kick[^\)]*\)/) {
    # TD (Failed 2pt)
    $type = 'td-2pt';
  } elsif ($pts == 2 && $play_raw =~ /defensive two point conversion/) {
    # Defensive 2pt
    $type = 'def+2pt';
  } elsif ($pts == 2) {
    # Safety
    $type = 'safety';
  }

  # (Log scores for next loop)
  $last{'visitor'} = $visitor;
  $last{'home'} = $home;

  # Drive info
  my $drive_plays = 'NULL'; my $drive_yards = 'NULL'; my $drive_top = 'NULL';
  if (defined($drive)) {
    ($drive_plays, $drive_yards, $drive_top) = ($drive =~ m/\((\d+)-(-?\d+), (\d+:\d+)\)/gsi);
    check_for_null(\$drive_plays);
    check_for_null(\$drive_yards);
    $drive_top = "00:$drive_top" if defined($drive_top);
    check_for_null(\$drive_top);
  } else {
    # Set a string for debug display
    $drive = 'undef';
  }

  # Display main info
  $i++;
  print "# $team_id, Q$quarter $time // $play_raw // $drive // $visitor-$home\n";
  print "INSERT INTO `SPORTS_NFL_GAME_SCORING` (`season`, `game_type`, `week`, `game_id`, `play_id`, `team_id`, `quarter`, `time`, `type`, `drive_plays`, `drive_yards`, `drive_top`, `visitor_score`, `home_score`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$i', '$team_id', '$quarter', '00:$time', '$type', $drive_plays, $drive_yards, $drive_top, '$visitor', '$home');\n";

  # Get the players involved
  my $play = $play_raw;
  foreach my $t ( $team_id, $opp_team_id ) {
    foreach my $player ( keys %{$rosters{$t}} ) {
      $play =~ s/$player/\{\{${t}_$rosters{$t}{$player}\}\}/g;
    }
  }

  # Parse the play...
  if ($type =~ m/td/i) {
    my ($xp_type) = ($type =~ m/(.{3})$/);
    check_for_null(\$xp_type);

    # Touchdown: Type, Scorer, QB?
    # Types: pass, rush, int_ret, fumb_ret, kick_ret, punt_ret
    my $td_type;
    $td_type = 'pass' if $play =~ m/ pass from /;
    $td_type = 'pass' if $play =~ m/ pass play by /;
    $td_type = 'rush' if $play =~ m/ run( |$)/;
    $td_type = 'int_ret' if $play =~ m/ interception (return|in end zone)/;
    $td_type = 'fumb_ret' if $play =~ m/ fumble return/;
    $td_type = 'fumb_rec' if $play =~ m/ fumble recovery/;
    $td_type = 'kick_ret' if $play =~ m/ kickoff return/;
    $td_type = 'kick_rec' if $play =~ m/ kickoff recovery/;
    $td_type = 'punt_ret' if $play =~ m/ punt return/;
    $td_type = 'fg_ret' if $play =~ m/ field goal return/;
    $td_type = 'block_punt_ret' if $play =~ m/ (?:return of )?blocked punt(?: return)?/;
    $td_type = 'block_punt_rec' if $play =~ m/ blocked punt recovery/;
    $td_type = 'block_fg_ret' if $play =~ m/ blocked field goal/;

    print STDERR "Unknown TD: $play_raw (Encoded: $play)\n" if !defined($td_type);

    # Scorer / QB
    my ($td_scorer) = ($play =~ m/\{\{${team_id}_(\d+)\}\}/);
    my ($qb) = ($play =~ m/ (?:from|by) \{\{${team_id}_(\d+)\}\}/);
    check_for_null(\$qb);

    # Length
    my ($length) = ($play =~ m/ (\d+) yd\./);
    check_for_null(\$length);

    # Display
    print "INSERT INTO `SPORTS_NFL_GAME_SCORING_TD` (`season`, `game_type`, `week`, `game_id`, `play_id`, `type`, `scorer`, `qb`, `length`, `xp_type`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$i', '$td_type', '$td_scorer', $qb, $length, $xp_type);\n";

    # PAT?
    my $made = ($type =~ m/\+/ ? 1 : 0);
    if ($type =~ m/pat$/) {
      my ($kicker) = ($play =~ m/\(\{\{${team_id}_(\d+)\}\} kick/);
      check_for_null(\$kicker);
      print "INSERT INTO `SPORTS_NFL_GAME_SCORING_PAT` (`season`, `game_type`, `week`, `game_id`, `play_id`, `made`, `kicker`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$i', '$made', $kicker);\n";

    # 2pt?
    } elsif ($type =~ m/2pt$/) {
      my ($att_play) = ($play =~ m/\(([^\)]+)\)/);
      my $att_type = ($att_play =~ m/pass$/ ? 'pass' : 'rush');
      my ($qb, $player) = ($att_play =~ m/^(?:\{\{${team_id}_(\d+)\}\}-)?\{\{${team_id}_(\d+)\}\}/);
      check_for_null(\$qb); check_for_null(\$player);
      print "INSERT INTO `SPORTS_NFL_GAME_SCORING_2PT` (`season`, `game_type`, `week`, `game_id`, `play_id`, `made`, `type`, `qb`, `player`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$i', '$made', '$att_type', $qb, $player);\n";
    }

  } elsif ($type eq 'fg') {
    # Field Goal: kicker, distance
    my ($kicker) = ($play =~ m/\{\{${team_id}_(\d+)\}\}/);
    my ($length) = ($play =~ m/\s+(\d+)\s+yd\./);
    print "INSERT INTO `SPORTS_NFL_GAME_SCORING_FG` (`season`, `game_type`, `week`, `game_id`, `play_id`, `kicker`, `length`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$i', '$kicker', '$length');\n";

  } elsif ($type eq 'def+2pt') {
    # Defensive two point conversion: player
    my ($player) = ($play =~ m/\{\{${team_id}_(\d+)\}\}/);
    check_for_null(\$player);
    print "INSERT INTO `SPORTS_NFL_GAME_SCORING_2PT` (`season`, `game_type`, `week`, `game_id`, `play_id`, `made`, `type`, `qb`, `player`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$i', '1', 'def', NULL, $player);\n";

  } elsif ($type eq 'safety') {
    # Safety: type of safety / tackle, player (off), player(s) (def)
    my ($safety_type) = ($play =~ m/(Penalty|Tackle|Sack|Fumble|Punt Block|OB)/i);
    $safety_type =~ s/ /_/g; $safety_type = lc $safety_type;
    my ($player_off) = ($play =~ m/\{\{${opp_team_id}_(\d+)\}\}/);
    my ($player_def1, $player_def2) = ($play =~ m/\{\{${team_id}_(\d+)\}\}(?:, \{\{${team_id}_(\d+)\}\})?/);
    check_for_null(\$player_off); check_for_null(\$player_def1); check_for_null(\$player_def2);
    print "INSERT INTO `SPORTS_NFL_GAME_SCORING_SAFETY` (`season`, `game_type`, `week`, `game_id`, `play_id`, `type`, `player_def`, `player_def_alt`, `player_off`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$i', '$safety_type', $player_def1, $player_def2, $player_off);\n";

  } else {
    # Cry foul... we don't know what this is!
    print STDERR "Unknown: $play_raw (Encoded: $play)\n";
  }

  print "\n";
}

