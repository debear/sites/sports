#!/usr/bin/perl -w
# Load the player info

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use JSON;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
my ($season, $game_type, $db_game_id) = @ARGV;
my $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
my $game_concat = $season . '//' . $game_type . '//' . $db_game_id;

my %player_list = ( 'visitor' => [ ], 'home' => [ ] );
our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NFL_GAME_LINEUP' );

  clear_previous($season, $game_type, $week, $db_game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

# First check, does the file exist, and if it does, load it
my ($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamecenter');
print "##\n## Loading players for $season // $game_type // $week // $game_id\n##\n";

print "# Loading Gamecenter: $file\n";
my $pattern = '';
my ($players) = ($contents =~ m/players\s+:\s+(\{.*?\}),\s+media/gsi);
$players = decode_json($players); # Convert to hash

my %rosters = ( 'one' => { 'away' => { }, 'home' => { } },
                'two' => { 'away' => { }, 'home' => { } },
                'three' => { 'away' => { }, 'home' => { } },
                'jersey' => { 'away' => { }, 'home' => { } } );

# Now process
my @hashes = ( { 'key' => 'away', 'id' => 'visitor_id' },
               { 'key' => 'home', 'id' => 'home_id' } );
foreach my $hash (@hashes) {
  print "#  - Loading all $$game_info{$$hash{'id'}} players ($$hash{'key'}) from gamecenter: ";
  my @all = keys %{$$players{$$hash{'key'}}{'players'}};

  foreach my $key (@all) {
    my $player = $$players{$$hash{'key'}}{'players'}{$key};

    # Backup some fields (pre-conversion) for use in keys
    my $fname = lc $$player{'fn'};
    my $sname = lc $$player{'ln'};

    # Encode certain key fields
    $key = convert_text($key);
    foreach my $field (keys %$player) {
      $$player{$field} = convert_text($$player{$field});
    }

    # Identify if this player has been imported before
    my $tmp = "# $$player{'fn'} $$player{'ln'} (#{jersey}, {pos})\n";
    $tmp .= "SET \@player_id := (SELECT `player_id` FROM `SPORTS_NFL_PLAYERS_IMPORT` WHERE `remote_internal_id` = '$key');\n";
    $tmp .= "INSERT IGNORE INTO `SPORTS_NFL_PLAYERS_IMPORT` (`player_id`, `remote_internal_id`, `remote_profile_id`, `profile_imported`)
  VALUES (\@player_id, '$key', '$$player{'esbid'}', NULL);\n";
    $tmp .= "SET \@player_id := (SELECT IFNULL(\@player_id, LAST_INSERT_ID()));\n\n";

    ## Manipulate certain fields
    # Date of Birth
    if (defined($$player{'bdate'}) && $$player{'bdate'} =~ m/^\d{1,2}\/\d{1,2}\/\d{4}$/) {
      my ($m, $d, $y) = ($$player{'bdate'} =~ m/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/gsi);
      $$player{'dob'} = sprintf("%04d-%02d-%02d", $y, $m, $d);
    }

    # Convert height from ft-in to inches
    if (defined($$player{'ht'})) {
      my ($ft, $in) = ($$player{'ht'} =~ m/^(\d+)-(\d+)$/gsi);
      $$player{'ht'} = (12 * $ft) + $in;
    }

    # Split the birthplace
    if (defined($$player{'hometown'})) {
      ($$player{'birthplace'}, $$player{'birthplace_state'}) = ($$player{'hometown'} =~ m/^(.*?), ([^,]+)$/gsi);
      $$player{'birthplace'} = undef if !defined($$player{'birthplace'}) || $$player{'birthplace'} eq 'null';
      $$player{'birthplace_state'} = undef if !defined($$player{'birthplace_state'}) || $$player{'birthplace_state'} eq 'null';
    }

    # Check for NULLs
    foreach my $field ('dob', 'birthplace', 'birthplace_state', 'ht', 'wt', 'college') {
      if (defined($$player{$field})) {
        $$player{$field} = "'$$player{$field}'";
      } else {
        $$player{$field} = 'NULL';
      }
    }

    # Create the player record
    $tmp .= "INSERT IGNORE INTO `SPORTS_NFL_PLAYERS` (`player_id`, `first_name`, `surname`, `dob`, `birthplace`, `birthplace_state`, `height`, `weight`, `college`)
  VALUES (\@player_id, '$$player{'fn'}', '$$player{'ln'}', $$player{'dob'}, $$player{'birthplace'}, $$player{'birthplace_state'}, $$player{'ht'}, $$player{'wt'}, $$player{'college'});\n\n";

    # Attach to this game
    $tmp .= "INSERT INTO `SPORTS_NFL_GAME_LINEUP` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `player_id`, `pos`, `status`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{$$hash{'id'}}', '{jersey}', \@player_id, '{pos}', '{status}');\n\n";

    # Bespoke fields for our sorting...
    $fname =~ s/[^a-z]//ig; $sname =~ s/ //g;
    $$player{'name1'} = substr($fname, 0, 1) . '.' . $sname;
    $$player{'name2'} = substr($fname, 0, 2) . '.' . $sname;
    $$player{'name3'} = substr($fname, 0, 3) . '.' . $sname;
    $$player{'nameFull'} = $fname . $sname; # Skip the dot separator (See: DanielThomas, 2014/regular/4/8)

    # Exceptions...
    $$player{'uniformNumber'} = 77 if $game_concat eq '2015//regular//1314' && $$player{'esbid'} eq 'JON075575'; # Barrett Jones down as a C, gamecenter as a G, and without jersey so failing tests
    $$player{'uniformNumber'} = 90 if ($game_concat eq '2016//regular//1606' && $$player{'esbid'} eq 'JOH690098') # Tom Johnson, MIN DT listed as #69 in HTML and #90 in PDF (PDF correct)
                                   || ($game_concat eq '2016//regular//1709' && $$player{'esbid'} eq 'JOH690098');
    $$player{'uniformNumber'} = undef if $season == 2017 && $game_type eq 'regular' && $$player{'esbid'} eq 'JON162611'; # Chris Jones was on IR all season, but jersey conflicted with DJ Jones

    # Store for processing...
    my %info = ( 'sql' => $tmp, 'row' => $player );
    add_gamecenter_roster($rosters{'one'}{$$hash{'key'}}, $$player{'name1'}, \%info);
    add_gamecenter_roster($rosters{'two'}{$$hash{'key'}}, $$player{'name2'}, \%info);
    add_gamecenter_roster($rosters{'three'}{$$hash{'key'}}, $$player{'name3'}, \%info);
    add_gamecenter_roster($rosters{'full'}{$$hash{'key'}}, $$player{'nameFull'}, \%info);
    add_gamecenter_roster($rosters{'jersey'}{$$hash{'key'}}, $$player{'uniformNumber'}, \%info)
      if defined($$player{'uniformNumber'});
  }
  print "[ Done ]\n";
}

# Next, load the gamebook
($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamebook');
print "# Loading Gamebook: $file\n\n";

my $game_rosters = get_gamebook_rosters("$season//$game_type//$week//$game_id", $contents);
foreach my $hash (@hashes) {
  print "#\n# $$game_info{$$hash{'id'}}\n#\n";
  foreach my $status ('starters', 'subs', 'dnp', 'inactives') {
    foreach my $player (@{$$game_rosters{$$hash{'key'}}{$status}}) {
      process_player($player, $$hash{'key'});
    }
  }
}

print "##\n## Completed loading players for $season // $game_type // $week // $game_id\n##\n";

# Store gamecenter player info in an array
sub add_gamecenter_roster {
  my ($hash, $key, $info) = @_;

  # Add to a previous list?
  if (defined($$hash{$key}) && ref($$hash{$key}) eq 'ARRAY') {
    push @{$$hash{$key}}, $info;

  # Add to a previous scalar?
  } elsif (defined($$hash{$key})) {
    $$hash{$key} = [ $$hash{$key} ];
    push @{$$hash{$key}}, $info;

  # Add as a scalar...
  } else {
    $$hash{$key} = $info;
  }
}

# Find players and display the output
sub process_player {
  my ($player, $key) = @_;

  # Add this player to the list, interpolating the relevant info
  my $info = get_player($key, $$player{'name'}, $$player{'pos'}, $$player{'jersey'});
  return if !defined($info);

  # Establish the position, and convert
  $$player{'pos'} = $$info{'row'}{'pos'} if !defined($$player{'pos'});
  parse_pos(\$$player{'pos'});

  # Process and display
  my $tmp = $$info{'sql'};
  $tmp =~ s/{jersey}/$$player{'jersey'}/g;
  $tmp =~ s/{pos}/$$player{'pos'}/g;
  $tmp =~ s/{status}/$$player{'status'}/g;
  print $tmp;
}

# Establish a player
sub get_player {
  my ($key, $name, $pos, $jersey, $no_recurse) = @_;
  $pos = '' if !defined($pos);

  # Conver the names in to a standard format (as there are the odd instances where this isn't the case...)
  $name =~ s/^([^\. ]+) +([^\. ]+)$/$1.$2/; # Space separator instead of a dot
  $name =~ s/ //g; $name = lc $name;

  # Work backwards, from three to one and use jersey as a fallback...
  foreach my $num ( 'full', 'three', 'two', 'one' ) {
    next if !defined($rosters{$num}{$key}{$name});

    # If a scalar, return what we have...
    return $rosters{$num}{$key}{$name}
      if ref($rosters{$num}{$key}{$name}) ne 'ARRAY';

    # Matching names, so break by jersey?
    my @m = ( );
    foreach my $m (@{$rosters{$num}{$key}{$name}}) {
      push @m, $m if defined($$m{'uniformNumber'}) && $$m{'uniformNumber'} == $jersey;
    }
    return $m[0] if @m == 1;

    # Matching by jersey too, so how about by pos?
    if ($pos ne '') {
      my @p = ( );
      foreach my $m (@m) {
        push @p, $m if $$m{'pos'} eq $pos;
      }

      return $p[0] if @p == 1;
    }
  }

  # Fallback to finding players through jersey numbers...
  if (defined($rosters{'jersey'}{$key}{$jersey})) {
    my $by_jersey = $rosters{'jersey'}{$key}{$jersey};
    if (ref($by_jersey) ne 'ARRAY') {
      $by_jersey = [ $by_jersey ];
    }

    foreach my $num ( 'nameFull', 'name3', 'name2', 'name1', ) {
      # Try to match by exact name....
      my @m = ( );
      foreach my $m ( @{$by_jersey} ) {
        push @m, $m if $$m{'row'}{$num} eq $name;
      }
      return $m[0] if @m == 1;

      # ... then sub-part of name (i.e., hyphenated surname in roster, not in gamebook)
      @m = ( );
      foreach my $m ( @{$by_jersey} ) {
        if (length($name) < length($$m{'row'}{$num})) {
          push @m, $m if substr($$m{'row'}{$num}, 0, length($name)) eq $name;
        }
      }
      return $m[0] if @m == 1;

      # ... then position
      if ($pos ne '') {
        my @p = ( );
        foreach my $m ( @m ) {
          push @p, $m if defined($$m{'row'}{'pos'}) && $$m{'row'}{'pos'} eq $pos;
        }
        return $p[0] if @p == 1;
      }
    }
  }

  # Final fallbacks... let's try again, but customising $name to be F.Sname... and then surname only
  if (!defined($no_recurse)) {
    # F.Sname
    my $name_mod = $name; $name_mod =~ s/^(.)[^\.]*\.(.+)$/$1.$2/;
    my $mod = get_player($key, $name_mod, $pos, $jersey, 1);
    return $mod if defined($mod);

    # Sname only
    my $snameA = $name; $snameA =~ s/^[^\.]+\.(.+)$/$1/;
    my $snameB = $name; $snameB =~ s/^.*?\.([^\.]+)$/$1/;
    my $by_jersey = $rosters{'jersey'}{$key}{$jersey};
    if (ref($by_jersey) ne 'ARRAY') {
      $by_jersey = [ $by_jersey ];
    }
    my @m = ( );
    foreach my $m ( @{$by_jersey} ) {
      if (defined($$m{'row'})) {
        my $ln = lc $$m{'row'}{'ln'};
        push @m, $m if $ln eq $snameA || $ln eq $snameB;
      }
    }
    return $m[0] if @m == 1;

    # Okay, so let's try a different tack on the surnames - use the ESBID, which contains the first three letters of the surname
    $snameA = substr($snameA, 0, 3); $snameB = substr($snameB, 0, 3);
    my @p = ( );
    foreach my $m ( @{$by_jersey} ) {
      if (defined($$m{'row'})) {
        my $esbid = lc substr($$m{'row'}{'esbid'}, 0, 3);
        push @p, $m if $esbid eq $snameA || $esbid eq $snameB;
      }
    }
    return $p[0] if @p == 1;

    # Not that I like this, but if there's only one for this jersey anyway, return that player...
    return $rosters{'jersey'}{$key}{$jersey} if ref($rosters{'jersey'}{$key}{$jersey}) eq 'HASH';
  }

  # Can't find any way of uniquely identifying this player, so let's say so!
  print STDERR "ERROR: Unable to uniquely match $name ($key #$jersey, $pos)\n"
    if !defined($no_recurse);
  return undef;
}

