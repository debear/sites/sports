#!/usr/bin/perl -w
# Common routines for game processing

use List::Util qw(max);

# Validate and load a file
sub load_file {
  our (%config);
  my ($season, $game_type, $week, $game_id, $filename, $args) = @_;
  my $is_gamebook = ($filename eq 'gamebook');
  %{$args} = ( ) if !defined($args);
  if (!defined($$args{'ext'})) {
    $$args{'ext'} = ($is_gamebook ? 'txt' : 'htm');
  }

  # Validate
  my $file = sprintf('%s/_data/%d/games/%s/%02d/%02d/%s.%s.gz',
                     $config{'base_dir'},
                     $season,
                     $game_type,
                     $week,
                     $game_id,
                     $filename,
                     $$args{'ext'});
  if (! -e $file) {
    print STDERR "File '$file' does not exist, unable to continue.\n";
    exit 20;
  }

  # Load the file
  our $contents;
  open FILE, "gzip -dc $file |";
  my @contents = <FILE>;
  close FILE;
  $contents = join('', @contents);
  @contents = (); # Garbage collect...

  # Apply any fixes to the raw gamebook
  my $fix_file = "$config{'base_dir'}/_data/fixes/$season/gamebook_raw.pl";
  require $fix_file if -e $fix_file;

  return ($file, $contents);
}

# Clear a previous run
sub clear_previous {
  my ($season, $game_type, $week, $db_game_id, @tables) = @_;

  print "## Resetting a previous data load...\n";
  foreach my $table (@tables) {
    print "DELETE FROM `$table` WHERE `season` = '$season' AND `game_type` = '$game_type' AND `week` = '$week' AND `game_id` = '$db_game_id';\n";
  }
}

# Order by game
sub reorder_tables {
  my @tables = @_;

  print "## Table maintenance...\n";
  foreach my $table (@tables) {
    print "ALTER TABLE `$table` ORDER BY `season`, `game_type`, `week`, `game_id`;\n";
  }
}

##
## Load the rosters from the gamebook
##
sub get_gamebook_rosters {
  my ($game, $contents, $apply_fixes) = @_;
  $apply_fixes = 0 if !defined($apply_fixes); # Ensure a boolean is set on this optional arg

  # Which game are we parseing?
  my ($season, $game_type, $db_game_id) = @ARGV;
  my $week = substr($db_game_id, 0, -2);
  my $game_id = ($db_game_id % 100);
  # This is the file where any data fixes (generic, or game-specific, will be applied)
  my $fix_file = "$config{'base_dir'}/_data/fixes/$season/gamebook.pl";

  # Up to, and including, 2021 the fixes were applied on the raw, unparsed, contents
  require $fix_file
      if $apply_fixes && -e $fix_file && $season < 2021;

  # Break down in to blocks of the two team's rosters
  my ($gb_rosters) = ($contents =~ m/Lineups\s+[^\n]+[ \t]{2,}[^\n]+\n(.*?)Field Goals/gsi);
  my ($starters, $subs, $dnp, $inactives) = ($gb_rosters =~ m/Offense[^\n]+Defense[^\n]+Offense[^\n]+Defense[^\n]*\n(.+)\n[^\n]+Substitutions[^\n]+Substitutions[^\n]*\n(.+)\n[^\n]+Did Not Play[^\n]+Did Not Play[^\n]*\n(.*)\n[^\n]+Not Active[^\n]+Not Active[^\n]*\n(.*?)$/gsi);

  # Split in to the two team's player lists
  my %raw_rosters = ( 'away' => { }, 'home' => { } );

  # Starters - split down the middle
  fix_starters(\$starters);
  ($raw_rosters{'away'}{'starters'}, $raw_rosters{'home'}{'starters'}) = split_text($starters, calc_starters_split_pos($starters));

  # Rest needs a little more guesswork...
  ($raw_rosters{'away'}{'subs'}, $raw_rosters{'home'}{'subs'}) = split_manually($game, $subs, 'subs');
  ($raw_rosters{'away'}{'dnp'}, $raw_rosters{'home'}{'dnp'}) = split_manually($game, $dnp, 'dnp');
  ($raw_rosters{'away'}{'inactives'}, $raw_rosters{'home'}{'inactives'}) = split_manually($game, $inactives, 'inactives');

  # Then convert in to a list
  our %gb_rosters = ( 'away' => { }, 'home' => { } );
  my $pos_pattern = '\$?#?3?[A-Z0-9]{1,3}(?:\(3\))?[\/\-]? ?[A-Z]{0,3}#?3?\'?';
  foreach my $key (keys %gb_rosters) {
    # Starters
    $gb_rosters{$key}{'starters'} = [ ];
    my ($off, $def) = split_starters($raw_rosters{$key}{'starters'});
    foreach my $txt ($off, $def) {
      foreach my $line (split("\n", $txt)) {
        next if $line =~ m/^\s*$/;
        my $row = split_player($key, 'starter', $line, $line =~ m/^($pos_pattern)\s+(\d+)\s(.+)$/);

        # Avoid a duplicate player issue...
        next if "$season//$game_type//$week//$game_id" eq '2012//regular//14//10'
                 && $$row{'jersey'} == 31
                 && $$row{'pos'} eq 'WR';

        push @{$gb_rosters{$key}{'starters'}}, $row;
      }
    }

    # Tidy and display the remaining players
    my @loop = ( { 'key' => 'subs', 'status' => 'substitute' },
                 { 'key' => 'dnp', 'status' => 'dnp' },
                 { 'key' => 'inactives', 'status' => 'inactive' } );
    foreach my $loop (@loop) {
      $gb_rosters{$key}{$$loop{'key'}} = [ ];
      my $txt = $raw_rosters{$key}{$$loop{'key'}};
      clean_roster_list(\$txt);
      foreach my $m (split(', ', $txt)) {
        # Try the three-field convert (Pos/Jersey/Name)
        my @row = ($m =~ m/^($pos_pattern) (\d+) (.+)$/);

        # If that doesn't work, fall back to Jersey/Name
        if (!@row) {
          @row = ($m =~ m/^\s*(\d+) (.+)$/);
          unshift(@row, undef);
        }

        push @{$gb_rosters{$key}{$$loop{'key'}}}, split_player($key, $$loop{'status'}, $m, @row);
      }
    }
  }

  # From 2022 onwards, the fixes were applied on the parsed and processed contents
  require $fix_file
    if $apply_fixes && -e $fix_file && $season >= 2022;

  return \%gb_rosters;
}

# Correct some pdftotext errors
sub fix_starters {
  my ($txt) = @_;

  # Apply any generic pre-parsing file fixes
  $$txt =~ s/(\s+\S+\s+\d+\s+\S+)(\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+\S+\s*?\n)(\s+\S+\s+\d+\s+)(\S+\s*?)\n\s+(\S+\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+\S+)/$1$4$2$3$5/g; # Split-line visiting starter
  $$txt =~ s/(\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+)(\S+)(\s+\S+\s+\d+\s+\S+\s*?\n)(\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+\S+\s+\S+\s+\d+\s+)(\S+\s*?)\n\s+(\S+)/$1$2$5$3$4$6/g; # Split-line home starter

  if ($$txt =~ m/\n\s+\d+\s+[a-z\-'\.]+\s*\n/i) {
    my $player_pattern = '[A-Z]+\s+\d+\s+[A-Z\-]\.[A-Za-z\-\'\.]+';
    $$txt =~ s/(\s[A-Z]+\s+\d+\s+[A-Z\-]\.)\n(\s+$player_pattern\s+$player_pattern\s+$player_pattern\s+[A-Z]+\s*?)\s([A-Za-z\-]+)\n\s+(\d+\s+[A-Za-z\-\'\.]+)/$1$3\n$2$4/g;
  }

  # Misaligned positions
  if ($$txt =~ m/\n[ \t]*+[A-Z]{1,3}[ \t]*\n/) {
    my @lines = split("\n", $$txt);
    for (my $i = 0; $i < @lines; $i++) {
      next if ($lines[$i] !~ m/^\s+[A-Z]+\s*$/);
      # Get
      my ($pos) = ($lines[$i] =~ m/^\s+([A-Z]+)/);
      my ($c) = ($pos =~ m/^([A-Z])/);
      # Update previous line
      substr($lines[$i-1], index($lines[$i], $c), length($pos), $pos);
      # Skip this line
      $lines[$i] = '';
    }
    # Convert back to $$txt
    $$txt = join("\n", @lines);
    $$txt =~ s/\n\n/\n/;
  }
}

# Calculate the point at which to split the starters tables vertically
sub calc_starters_split_pos {
  my ($txt) = @_;
  my $pos = 0;

  # First, identify the mid-point
  my @tmp = split("\n", $txt);
  for (my $i = 0; $i < @tmp; $i++) {
    $pos = max($pos, int(length($tmp[$i]) / 2));
  }

  # Then check the home column doesn't overlap...
  my $non_spaces = -1; # Something to trigger >= 1 until loop...
  until ($non_spaces == 0) {
    $non_spaces = 0; # Re-set to check the latest column...
    for (my $i = 0; $i < @tmp; $i++) {
      $non_spaces++
        if length($tmp[$i]) > $pos && substr($tmp[$i], $pos, 1) ne ' ';
    }

    # And react to what we found...
    $pos++ if $non_spaces;
  }

  return $pos;
}

# Split a text in to two columns
sub split_text {
  my ($txt, $split) = @_;
  my $a = $b = '';
  foreach my $line (split("\n", $txt)) {
    next if $line =~ m/^\s*$/;

    # Split
    $a .= substr($line, 0, $split) . "\n";
    $b .= substr($line, $split) . "\n" if length($line) >= $split;
  }
  return ($a, $b);
}

# Split the list of starters in to offensive / defensive
sub split_starters {
  my ($starters) = @_;
  my @off = ( ); my @def = ( );

  foreach my $row (split("\n", $starters)) {
    next if $row =~ m/^\s*$/;
    $row .= '  '; # To help our regex...
    my ($ws) = ($row =~ m/^(\s+)\S/); $ws = '' if !defined($ws); my $i = 0;
    $row =~ s/(\s)\d+(\s+\d+\s)/$1$2/ if $row =~ m/\s\d+\s+\d+\s/; # What if we have a number-number combo?
    my @matches = ($row =~ m/([0-9A-Z\/\-]*)'?\s+([0-9]{1,2})\s+(.*?)\s{2}/mgsi);
    while (my ($pos, $jersey, $name) = splice(@matches, 0, 3)) {
      my $list = \@off;
      if ($i++ || (length($ws) > 20)) {
        $list = \@def;
      }

      $pos = $jersey if $pos =~ m/^\s*$/;
      push @$list, "$pos $jersey $name ";
    }
  }

  return (join("\n", @off), join("\n", @def));
}

# Split a full list of raw away | home players in to home / away
sub split_manually {
  my ($game, $txt, $type) = @_;
  my %split = ( 'index' => -1, 'tot_prev_ws' => -1 );
  my $away = ''; my $home = '';

  # Game specific over-ride
  $split{'index'} = $config{'roster_split_index'}
    if defined($config{'roster_split_index'});

  # Loop through, and identify characteristcs on each 'cell' - whether it's non-whitespace and how many preceding whitespace chars there are
  if ($split{'index'} == -1) {
    my @info = ( ); my $num_lines = 0;
    foreach my $line (split("\n", $txt)) {
      # But skip empty lines...
      next if $line =~ m/^\s*$/;
      $num_lines++;

      my $prev_ws = 0;
      for (my $i = 0; $i < length($line); $i++) {
        my $char = substr($line, $i, 1);

        %{$info[$i]} = ( 'char' => 0, 'tot_prev_ws' => 0 ) if !defined($info[$i]);
        if ($char eq ' ') {
          # Increment the whitespace counter...
          $prev_ws++;

        } else {
          # Store and reset the whitespace counter
          $info[$i]{'char'}++;
          $info[$i]{'tot_prev_ws'} += $prev_ws;
          $prev_ws = 0;
        }
      }
    }

    # Identify the split point: chars in at least all lines bar two (in case away team has a couple more rows) and largest total previous whitespace chars
    for (my $i = 0; $i < @info; $i++) {
      my $l = $info[$i];
      my $avg_ws = int($$l{'tot_prev_ws'} / $num_lines);
      if ($$l{'char'} >= ($num_lines - 2) && $avg_ws > 1 && $$l{'tot_prev_ws'} > $split{'tot_prev_ws'}) {
        $split{'index'} = $i;
        $split{'tot_prev_ws'} = $$l{'tot_prev_ws'};
      }
    }
  }

  # Perform the split
  if ($split{'index'} != -1) {
    ($away, $home) = split_text($txt, $split{'index'});
  } else {
    $away = $txt;
  }
  return ($away, $home);
}

# Given a roster list (not starters) from a gamebook, tidy the output
sub clean_roster_list {
  my ($ref) = @_;

  # Trim and join together each line
  my @txt = split("\n", $$ref);
  $$ref = '';
  foreach my $line (@txt) {
    next if $line =~ m/^\s*$/; # Skip empty lines
    trim(\$line);

    # Add with a trailing space
    $$ref .= $line . ($line !~ m/[\.-]\s*$/ ? ' ' : '');
  }
  $$ref =~ s/\s+$//; # Final rtrim...
}

# Find players and display the output
sub split_player {
  my ($key, $status, $full_line, $pos, $jersey, $name) = @_;

  # Error during processing the name?
  if (!defined($name)) {
    print STDERR "ERROR: Unable to parse '$full_line' ($key, $status)\n";
    return;
  }

  # Convert the name
  trim(\$name);
  $name =~ s/ \(3rd QB\)//;

  # Fix long names...
  $name =~ s/\.\s+/./g;
  $name .= 'e' if $name eq 'D.Rodgers-Cromarti';

  # Establish the position, and convert
  $pos = undef if defined($pos) && $pos =~ m/^\d+$/;
  parse_pos(\$pos) if defined($pos);

  my %player = ( 'name' => $name, 'jersey' => $jersey, 'pos' => $pos, 'status' => $status );
  return \%player;
}

# Syntactic position fixes (NOT intended for mappings)
sub parse_pos {
  my ($pos) = @_;
  $$pos = 'QB' if $$pos =~ /QB/;
  $$pos = 'QB' if $$pos eq '#3';
  $$pos =~ s/\/.+$// if $$pos =~ m/\//;
  $$pos =~ s/\-.+$// if $$pos =~ m/\-/;
  $$pos =~ s/^\$// if $$pos =~ m/^\$/;
}

# Return true to pacify the compiler
1;
