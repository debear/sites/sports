#!/usr/bin/perl -w
# Load the player info (from the gamebook alone)

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use JSON;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the player parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../../config.pl');
require $config;
my $common = abs_path(__DIR__ . '/common.pl');
require $common;

# Define rest of vars from arguments
our ($season, $game_type, $db_game_id) = @ARGV;
our $week = substr($db_game_id, 0, -2);
my $game_id = ($db_game_id % 100);
our $game_concat = $season . '//' . $game_type . '//' . $db_game_id;

our $game_info = get_game_info(@ARGV);
if (!defined($$game_info{'nfl_id'})) {
  print STDERR "Unknown Game ($season :: $game_type :: $week :: $game_id)\n";
  exit 98;
}

# Reset a previous run?
if (defined($ARGV[3])) {
  # Which tables?
  my @tables = ( 'SPORTS_NFL_GAME_LINEUP' );

  clear_previous($season, $game_type, $week, $db_game_id, @tables) if $ARGV[3] eq '--reset';
  reorder_tables(@tables) if $ARGV[3] eq '--order';
  exit;
}

print "##\n## Loading players for $season // $game_type // $week // $game_id\n##\n";

#
# Step 1: Load the team rosters from the database
#
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $parse_pos_sth = $dbh->prepare('SELECT SPORTS_NFL_POSITIONS.posgroup_id
FROM SPORTS_NFL_POSITIONS_RAW
JOIN SPORTS_NFL_POSITIONS
  ON (SPORTS_NFL_POSITIONS.pos_code = SPORTS_NFL_POSITIONS_RAW.pos_code)
WHERE SPORTS_NFL_POSITIONS_RAW.pos_raw = ?;');
my $parse_name_sth = $dbh->prepare('SELECT fn_basic_html_comparison(?) AS name_parsed;');
my $sql = "SELECT SPORTS_NFL_TEAMS_ROSTERS.player_id, SPORTS_NFL_TEAMS_ROSTERS.team_id, SPORTS_NFL_TEAMS_ROSTERS.jersey,
       SPORTS_NFL_TEAMS_ROSTERS.pos, SPORTS_NFL_POSITIONS.posgroup_id,
       SPORTS_NFL_PLAYERS.first_name, SPORTS_NFL_PLAYERS.surname
FROM SPORTS_NFL_TEAMS_ROSTERS
JOIN SPORTS_NFL_PLAYERS
  ON (SPORTS_NFL_PLAYERS.player_id = SPORTS_NFL_TEAMS_ROSTERS.player_id)
JOIN SPORTS_NFL_POSITIONS_RAW
  ON (SPORTS_NFL_POSITIONS_RAW.pos_raw = SPORTS_NFL_TEAMS_ROSTERS.pos)
JOIN SPORTS_NFL_POSITIONS
  ON (SPORTS_NFL_POSITIONS.pos_code = SPORTS_NFL_POSITIONS_RAW.pos_code)
WHERE SPORTS_NFL_TEAMS_ROSTERS.season = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.game_type = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.week = ?
AND   SPORTS_NFL_TEAMS_ROSTERS.team_id IN (?, ?)
AND   SPORTS_NFL_TEAMS_ROSTERS.jersey IS NOT NULL
ORDER BY SPORTS_NFL_TEAMS_ROSTERS.team_id, SPORTS_NFL_PLAYERS.surname, SPORTS_NFL_PLAYERS.first_name;";
our %rosters = ('home' => {'jersey' => {}, 'pos' => {}}, 'away' => {'jersey' => {}, 'pos' => {}});
my @rosters = @{$dbh->selectall_arrayref($sql, { Slice => {} }, $season, $game_type, $week, $$game_info{'visitor_id'}, $$game_info{'home_id'})};
foreach my $row (@rosters) {
  my $team_type = ($$row{'team_id'} eq $$game_info{'home_id'} ? 'home' : 'away');

  # Format the name for parsing, stripping out and converting non-latin chars
  foreach my $key ('first_name', 'surname') {
    $$row{$key . 'Raw'} = $$row{$key};
    my $enc = encode_entities(decode_entities(Encode::encode('UTF-8', $$row{$key}))); $enc =~ s/&#[^;]+;//g;
    $enc =~ s/\.//g if $key eq 'first_name';
    $enc =~ s/(\.) /$1/g if $key eq 'surname';
    $parse_name_sth->execute($enc);
    ($$row{$key}) = $parse_name_sth->fetchrow_array;
    $$row{$key} = lc $$row{$key};
  }

  # Build the base name
  my %added = ();
  for (my $i = 5; $i >= 1; $i--) {
    $$row{'nameCmp'} = substr($$row{'first_name'}, 0, $i) . '.' . $$row{'surname'};
    append_roster(\%rosters, $team_type, $$row{'nameCmp'}, $$row{'player_id'}, $$row{'posgroup_id'}, $$row{'jersey'})
      if !defined($added{$$row{'nameCmp'}});
    $added{$$row{'nameCmp'}} = 1;
  }
  # A secondary version with suffix removed
  $$row{'nameCmpNoSuffix'} = $$row{'nameCmp'}; $$row{'nameCmpNoSuffix'} =~ s/ (?:iv|i{1,3}|vi{0,3})//i; $$row{'nameCmpNoSuffix'} =~ s/ [sj]r\.?//i;
  append_roster(\%rosters, $team_type, $$row{'nameCmpNoSuffix'}, $$row{'player_id'}, $$row{'posgroup_id'}, $$row{'jersey'})
    if !defined($added{$$row{'nameCmpNoSuffix'}});
}

# If any roster fixes are required to match the gamebook (that is the fault of the roster lookup), apply them now
my $fix_file = "$config{'base_dir'}/_data/fixes/$season/rosters.pl";
require $fix_file
  if -e $fix_file;

#
# Step 2: Load and process players from the gamebook
#
my ($file, $contents) = load_file($season, $game_type, $week, $game_id, 'gamebook');
print "# Loading Gamebook: $file\n\n";

my @hashes = ( { 'type' => 'away', 'id' => 'visitor_id' },
               { 'type' => 'home', 'id' => 'home_id' } );
my $game_rosters = get_gamebook_rosters("$season//$game_type//$week//$game_id", $contents, 1);

foreach my $hash (@hashes) {
  print "#\n# $$game_info{$$hash{'id'}}\n#\n\n";
  foreach my $status ('starters', 'subs', 'dnp', 'inactives') {
    print "## " . ucfirst($status) . "\n\n";
    foreach my $player (@{$$game_rosters{$$hash{'type'}}{$status}}) {
      # Establish the position, and convert
      parse_pos(\$$player{'pos'});
      $parse_pos_sth->execute($$player{'pos'});
      my ($posgroup_id) = $parse_pos_sth->fetchrow_array;
      if (!defined($posgroup_id)) {
        # This raw position is unknown, so we can't really proceed
        print STDERR "Unable to convert gamebook position '$$player{'pos'}' to posgroup_id\n";
        next;
      }

      # Convert the names in to a standard format (as there are the odd instances where this isn't the case...)
      my $name = lc $$player{'name'};
      $name =~ s/^([^\. ]+) +([^\. ]+)$/$1.$2/; # Space separator instead of a dot
      $name = convert_text($name); $name =~ s/&#[^;]+;//g;

      # Try and find from our roster parsing
      my $player_id = defined($rosters{$$hash{'type'}}{'jersey'}{"$$player{'jersey'}#$name"}) ? $rosters{$$hash{'type'}}{'jersey'}{"$$player{'jersey'}#$name"} : $rosters{$$hash{'type'}}{'pos'}{"$posgroup_id#$name"};
      if (!defined($player_id)) {
        # Print error indicating this player cannot be matched
        print STDERR "Unable to identify player_id for $$game_info{$$hash{'id'}} #$$player{'jersey'} $$player{'pos'} $$player{'name'}\n";
        next;
      } elsif (!scalar($player_id) || (ref($player_id) eq 'ARRAY')) {
        # Print error indicating this player matches multiple references
        print STDERR "Unable to uniquely identify player_id for $$game_info{$$hash{'id'}} #$$player{'jersey'} $$player{'pos'} $$player{'name'}\n";
        next;
      }
      # Render the SQL for the mapping
      print "# $$player{'name'} (#$$player{'jersey'}, $$player{'pos'})\nINSERT INTO `SPORTS_NFL_GAME_LINEUP` (`season`, `game_type`, `week`, `game_id`, `team_id`, `jersey`, `player_id`, `pos`, `status`)
  VALUES ('$season', '$game_type', '$week', '$db_game_id', '$$game_info{$$hash{'id'}}', '$$player{'jersey'}', '$player_id', '$$player{'pos'}', '$$player{'status'}');\n\n"
    }
  }
}

print "##\n## Completed loading players for $season // $game_type // $week // $game_id\n##\n";

# Disconnect from the database
undef $parse_pos_sth if defined($parse_pos_sth);
undef $parse_name_sth if defined($parse_name_sth);
$dbh->disconnect if defined($dbh);

##
## Helper Methods
##

# Add our players to the roster list, ensuring duplicate references are converted to an appropriate format
sub append_roster {
  my ($rosters, $team_type, $name, $player_id, $pos, $jersey) = @_;

  my %values = ( 'jersey' => $jersey, 'pos' => $pos );
  foreach my $key ('jersey', 'pos') {
    my $ref = "$values{$key}#$name";
    if (!defined($$rosters{$team_type}{$key}{$ref})) {
      # First time we've come across this key, so add as a scalar
      $$rosters{$team_type}{$key}{$ref} = $player_id;
    } else {
      # Pre-existing info, so convert to an array and then add
      $$rosters{$team_type}{$key}{$ref} = [ $$rosters{$team_type}{$key}{$ref} ]
        if scalar($$rosters{$team_type}{$key}{$ref});
      push @{$$rosters{$team_type}{$key}{$ref}}, $player_id;
    }
  }

  # If this is a hyphenated surname, add the unhyphenated equivalent (as gamebooks often include this version)
  if ($name =~ /^[^\.]+\..*?\-.*?$/) {
    # First part only (remove the second half)
    my $name_alt = $name; $name_alt =~ s/(^.+\.)([^\-]+)\-.+$/$1$2/;
    append_roster($rosters, $team_type, substr($name, 0, index($name, '-')), $player_id, $pos, $jersey);
    # Second part only (remove the first half)
    $name_alt = $name; $name_alt =~ s/(^.+\.)[^\-]+\-(.+)$/$1$2/;
    append_roster($rosters, $team_type, $name_alt, $player_id, $pos, $jersey);
  }
}
