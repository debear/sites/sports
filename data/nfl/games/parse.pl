#!/usr/bin/perl -w
# Read the game info and convert to SQL

use strict;
use Dir::Self;
use Cwd 'abs_path';
use JSON;

# Validate the arguments
#  - there needs to be three: season, sched type, game_id
if (@ARGV < 3 || !grep(/^$ARGV[1]$/, ( 'regular', 'playoff' ))) {
  print STDERR "Insufficient arguments to the game parsing script.\n";
  exit 98;
}

# Get config vars
our %config;
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

# Local config
$config{'script_dir'} = __DIR__;
$config = $config{'script_dir'} . '/config.pl';
require $config;

$config{'debug'} = 0; # Display script to run, rather than actually running it...

# Define rest of vars from arguments
our ($season, $game_type, $db_game_id) = @ARGV;
our $week = substr($db_game_id, 0, -2);
our $game_id= ($db_game_id % 100);

print "##\n##\n## Loading $game_type, Week $week, Game $game_id\n##\n##\n\n";
print STDERR "# Loading Game: $season " . ucfirst($game_type) . ", Week $week, Game $game_id\n";

# Verify its size (to ensure a game was played!)
my $data_dir = "$config{'base_dir'}/_data/$season/games/$game_type/" . sprintf('%02d', $week) . '/' . sprintf('%02d', $game_id);
my $file = "$data_dir/gamebook.txt.gz";
my @stat = stat $file;
if (!defined($stat[7]) || $stat[7] < 200) {
  print STDERR "No game info available\n";
  exit 20;
}
# But we also need to verify the game stats (which are sometimes produced after the integration run)
$file = "$data_dir/stats.json.gz";
@stat = stat $file;
if (!defined($stat[7]) || $stat[7] < 200) {
  print STDERR "No game stats available\n";
  exit 21;
}

# Loop one: clear a previous run...
foreach my $script (@{$config{'scripts'}}) {
  run_script($$script{'script'}, $$script{'disp'}, '--reset');
}

# Loop two: run!
foreach my $script (@{$config{'scripts'}}) {
  run_script($$script{'script'}, $$script{'disp'});
}

print "##\n## Game $game_type, Week $week, Game $game_id Done\n##\n\n";

