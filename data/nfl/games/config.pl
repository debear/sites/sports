#!/usr/bin/perl -w
# Config for the game parsing scripts

@{$config{'scripts'}} = ( { 'script' => 'players',  'disp' => undef },           # Players
                          { 'script' => 'boxscore', 'disp' => undef },           # Boxscore
                          { 'script' => 'scoring',  'disp' => 'Scoring Plays' }, # Scoring plays
                          { 'script' => 'misc',     'disp' => 'Misc Info' },     # Misc game info
                          { 'script' => 'summary',  'disp' => undef } );         # Summary (result, info) - last as it's the key part in the system

sub run_script {
  our (%config, $season, $game_type, $db_game_id);
  my ($script, $disp, $extra) = @_;
  $disp = ucfirst($script) if !defined($disp);

  my $full_script = "$config{'script_dir'}/parse/$script.pl";
  foreach my $arg ($season, $game_type, $db_game_id, $extra) {
    $full_script .= ' ' . $arg if defined($arg);
  }

  print "##\n## $disp\n##\n";
  print "# Running: \`$full_script\`" if $config{'debug'};
  print `$full_script` if !$config{'debug'};
  print "\n";
}

# Return true to pacify the compiler
1;
