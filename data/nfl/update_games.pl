#!/usr/bin/perl -w
# Download, import and process game data from NFL.com

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;
use Sort::Naturally;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Define log details
$config{'log_dir'} .= 'games';
my %logs = get_log_def();

# Guesstimate the season
my @time = localtime();
my $season_guess = ($time[4] > 7 ? 1900 : 1899) + $time[5];

# Unless passed in a special argument, we're to ask the user how to operate
my $season;

# Determine for ourselves
if ($config{'is_unattended'}) {
  # Season is our guessed season
  $season = $season_guess;

# Get from the user
} else {
  $season = question_user("Please enter a season in YYYY format [$season_guess]", '\d{4}', $season_guess);
}

# Get the suggested list of games to import
my $dbh = DBI->connect('dbi:mysql:' . $config{'db_name'}, $config{'db_user'}, $config{'db_pass'});
my $sql = 'SELECT game_type, week, game_id, nfl_id, home, visitor,
       CONCAT(DATE_FORMAT(game_time, "%H:%i"), " ", DATE_FORMAT(game_date, "%D %b")) AS date_fmt
FROM SPORTS_NFL_SCHEDULE
WHERE season = ?
AND   game_date < CURDATE()
AND   status IS NULL
ORDER BY game_date, game_time, game_id;';
my $game_list = $dbh->selectall_arrayref($sql, { Slice => {} }, $season);
$dbh->disconnect if defined($dbh);

# No need to continue (but not an error) if nothing found
if (!@$game_list) {
  print STDERR ' - There are no outstanding games to import.' . "\n";
  end_script();
}

# Process
my %list = ( 'all' => [ ], 'regular' => [ ], 'playoff' => [ ], 'process' => [ ], 'list' => [ ] );
foreach my $game (@$game_list) {
  # Summarise for display
  if ($$game{'game_type'} eq 'regular') {
    $$game{'summary'} = 'Week ' . $$game{'week'};
  # Playoff week
  } elsif ($$game{'week'} == 1) {
    $$game{'summary'} = 'Wildcard';
  } elsif ($$game{'week'} == 2) {
    $$game{'summary'} = 'Division';
  } elsif ($$game{'week'} == 3) {
    $$game{'summary'} = 'Conf Champs';
  } elsif ($$game{'week'} == 4) {
    $$game{'summary'} = 'Super Bowl';
  }

  $$game{'disp_game_id'} = ($$game{'game_id'} % 100);
  $$game{'summary'} .= ', Game ' . $$game{'disp_game_id'} . ': ' . $$game{'visitor'} . ' @ ' . $$game{'home'} . ', ' . $$game{'date_fmt'};
  print $$game{'summary'} . "\n";

  # Store for info
  push @{$list{'all'}}, $game;
}

# Which do we want to include? If automated, yes, we do...
my $import_all = ($config{'is_unattended'} ? 'y' : question_user('Do you wish to import ALL these games? [Y/n/c]', '[ync]', 'Y'));

# Are we excluding specific games?
if ($import_all =~ m/^n$/i) {
  foreach my $game (@{$list{'all'}}) {
    my $import = question_user($$game{'summary'} . ' - Import? [Y/n]', '[yn]', 'Y');
    if ($import =~ m/^y$/i) {
      push @{$list{'process'}}, $game;
      push @{$list{$$game{'game_type'}}}, $game;
    }
  }

# No, so process the full list if not cancelling
} elsif ($import_all !~ m/^c$/i) {
  $list{'process'} = $list{'all'};
  foreach my $game (@{$list{'all'}}) {
    push @{$list{$$game{'game_type'}}}, $game;
  }
}

# Check again - if here, no rounds were explicitly selected
if (!@{$list{'process'}}) {
  print ' - No games were selected to be imported, exiting.' . "\n";
  exit $config{'exit_codes'}{'pre-flight'};
}

# Parse the game list to determine which game weeks we are importing
my %weeks = ( 'regular' => { }, 'playoff' => { } );
foreach my $game (@{$list{'process'}}) {
  next if defined $weeks{$$game{'game_type'}}{$$game{'week'}};
  $weeks{$$game{'game_type'}}{$$game{'week'}} = 1;
}

foreach my $st ('regular', 'playoff') {
  foreach my $w (nsort keys %{$weeks{$st}}) {
    push @{$list{'weeks'}}, { 'game_type' => $st, 'week' => $w };
  }
}

## Inform user
print "\nAcquiring data from $season, " . @{$list{'process'}} . " game(s)\n";
print 'Run started at ' . `date` . "\n";
identify_log_file();
mkpath($config{'log_dir'});

# First group of scripts are core to the script
$config{'exit_status'} = 'core';

#
# Parse each of the games
#
foreach my $game (@{$list{'process'}}) {
  print "- $$game{'summary'}\n";

  # Download, if we haven't done so already (handled in-script)
  print '  - Download: ';
  command("$config{'base_dir'}/games/download.pl $season $$game{'game_type'} $$game{'game_id'}",
          "$config{'log_dir'}/$logs{'games_d'}.log",
          "$config{'log_dir'}/$logs{'games_d'}.err");
  print "[ Done ]\n";
  next if download_only();

  # Parse
  print '  - Parse:    ';
  command("$config{'base_dir'}/games/parse.pl $season $$game{'game_type'} $$game{'game_id'}",
          "$config{'log_dir'}/$logs{'games_p'}.sql",
          "$config{'log_dir'}/$logs{'games_p'}.err");
  print "[ Done ]\n";
}

# If only downloading, go no further
end_script() if download_only();

#
# Tidy the game tables
#
print '=> Tidy: ';
command("$config{'base_dir'}/games/order.pl",
        "$config{'log_dir'}/$logs{'games_p'}.sql",
        "$config{'log_dir'}/$logs{'games_p'}.err");
done(20);

#
# Import into the database
#
print '=> Importing Games: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'games_p'}.sql",
        "$config{'log_dir'}/$logs{'games_i'}.log",
        "$config{'log_dir'}/$logs{'games_i'}.err");
done(9);

# Next group of scripts are the first section of post-processing
$config{'exit_status'} = 'post-process-1';

#
# Update season totals
#
print '=> Season Totals (Players): ';
foreach my $game_type ( 'regular', 'playoff' ) {
  command("echo \"CALL nfl_totals_players('$season', '$game_type')\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err")
    if @{$list{$game_type}};
  command("echo \"CALL nfl_totals_players_sort('$season', '$game_type')\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err")
    if @{$list{$game_type}};
}
command("echo \"CALL nfl_totals_players_order()\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_pl'}.log",
        "$config{'log_dir'}/$logs{'totals_pl'}.err");
done(1);

print '=> Season Totals (Teams): ';
foreach my $game_type ( 'regular', 'playoff' ) {
  command("echo \"CALL nfl_totals_teams('$season', '$game_type')\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err")
    if @{$list{$game_type}};
  command("echo \"CALL nfl_totals_teams_sort('$season', '$game_type')\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err")
    if @{$list{$game_type}};
}
command("echo \"CALL nfl_totals_teams_order()\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'totals_tm'}.log",
        "$config{'log_dir'}/$logs{'totals_tm'}.err");
done(3);

# Next group of scripts are the second section of post-processing
$config{'exit_status'} = 'post-process-2';

#
# Standings
#
if (@{$list{'regular'}}) {
  print '=> Standings: ';
  foreach my $week (@{$list{'weeks'}}) {
    next if $$week{'game_type'} ne 'regular';
    command("echo \"CALL nfl_standings('$season', '$$week{'week'}', '$config{'num_recent'}')\" | /usr/bin/mysql $config{'db_name'}",
            "$config{'log_dir'}/$logs{'standings'}.log",
            "$config{'log_dir'}/$logs{'standings'}.err");
  }
  done(15);
}

#
# Updated schedules
#
my $reg_weeks = ($season < 2021 ? 17 : 18);
my $sched_playoff = (@{$list{'playoff'}} || $weeks{'regular'}{$reg_weeks});
my $sched_flag = ($sched_playoff ? '--playoff' : '--regular');
my @now = localtime(time());
my $sched_date = sprintf('%04d-%02d-%02d', $now[5] + 1900, $now[4] + 1, $now[3]);

# Download latest version of the schedule
print '=> ' . ($sched_playoff ? 'Playoff' : 'Updated') . ' Schedule:' . "\n";
print '   -> Download: ';
command("$config{'base_dir'}/schedule/download.pl $season $sched_flag $sched_date",
        "$config{'log_dir'}/$logs{'schedule_d'}.log",
        "$config{'log_dir'}/$logs{'schedule_d'}.err");
done(13);

# Parse
print '   -> Parse: ';
command("$config{'base_dir'}/schedule/parse.pl $season $sched_flag $sched_date",
        "$config{'log_dir'}/$logs{'schedule_p'}.sql",
        "$config{'log_dir'}/$logs{'schedule_p'}.err");
done(16);

# Import
print '   -> Import: ';
command("/usr/bin/mysql $config{'db_name'} <$config{'log_dir'}/$logs{'schedule_p'}.sql",
        "$config{'log_dir'}/$logs{'schedule_i'}.log",
        "$config{'log_dir'}/$logs{'schedule_i'}.err");
done(15);

# Next group of scripts are the fifth section of post-processing
$config{'exit_status'} = 'post-process-4';

#
# Playoffs
#
if (@{$list{'regular'}}) {
  # Playoff seeds
  print '=> Playoff Seeds: ';
  command("echo \"CALL nfl_playoff_seeds('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_seeds'}.log",
          "$config{'log_dir'}/$logs{'po_seeds'}.err");
  done(11);
}

# Updated series
if (@{$list{'playoff'}}) {
  print '=> Playoff Series: ';
  command("echo \"CALL nfl_playoff_series('$season');\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'po_series'}.log",
          "$config{'log_dir'}/$logs{'po_series'}.err");
  done(10);
}

# Playoff matchups
print '=> Playoff Matchups: ';
command("echo \"CALL nfl_playoff_matchups('$season');\" | /usr/bin/mysql $config{'db_name'}",
        "$config{'log_dir'}/$logs{'po_matchups'}.log",
        "$config{'log_dir'}/$logs{'po_matchups'}.err");
done(8);

# Playoff histories
if (@{$list{'playoff'}}) {
  print '=> Season History: ';
  command("echo \"CALL nfl_history_regular('$season')\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'history'}.log",
          "$config{'log_dir'}/$logs{'history'}.err");
  command("echo \"CALL nfl_history_playoffs('$season')\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'history'}.log",
          "$config{'log_dir'}/$logs{'history'}.err");
  done(10);
}

#
# Power rankings?
#
print '=> Power Ranks: ';
foreach my $week (@{$list{'weeks'}}) {
  command("echo \"CALL nfl_power_ranks('$season', '$$week{'game_type'}', '$$week{'week'}', '$config{'num_recent'}')\" | /usr/bin/mysql $config{'db_name'}",
          "$config{'log_dir'}/$logs{'power_ranks'}.log",
          "$config{'log_dir'}/$logs{'power_ranks'}.err");
}
done(13);

#
# Tidy up and end the script
#
end_script();

#
# Define the log file names
#
sub get_log_def {
  return (
    'games_d'     => '01_games_download',
    'games_p'     => '02_games_parse',
    'games_i'     => '03_games_import',
    #'players_p'  => '04_players_load',
    #'players_i'  => '05_players_import',
    'totals_pl'   => '06_totals_player',
    'totals_tm'   => '07_totals_team',
    'standings'   => '08_standings',
    'schedule_d'  => '09_schedule_download',
    'schedule_p'  => '10_schedule_parse',
    'schedule_i'  => '11_schedule_import',
    'po_seeds'    => '12_playoff_seeds',
    'po_series'   => '13_playoff_series',
    'po_matchups' => '14_playoff_matchups',
    'history'     => '15_season_history',
    'power_ranks' => '16_power_ranks',
  );
}
