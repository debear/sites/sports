<?php

namespace DeBear\Helpers\Sports;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class Namespaces
{
    /**
     * Create a new object of the given class name that has its namespace automatically pre-pended
     * @param string $class Class name we are trying to create.
     * @param array  $args  Arguments to pass the constructor. (Optional).
     * @return object An instantiated object of the given class
     */
    public static function createObject(string $class, array $args = []) /* : object */
    {
        $fullname = static::fullClassName($class);
        return new $fullname(...$args);
    }

    /**
     * Create a new static object of the given class name that has its namespace automatically pre-pended
     * @param string $class  Class name we are trying to create a static object for.
     * @param string $method The method to call on the static class.
     * @param array  $args   Arguments to pass the static method. (Optional).
     * @return mixed Whatever was returned by the static method
     */
    public static function callStatic(string $class, string $method, array $args = []): mixed
    {
        $fullname = static::fullClassName($class);
        return call_user_func_array([$fullname, $method], $args);
    }

    /**
     * Flag that a class exists in the current namespace
     * @param string $class Class name we are trying to check.
     * @return boolean Whether the class exists in this sport or not
     */
    public static function classExists(string $class): bool
    {
        return class_exists(static::fullClassName($class));
    }

    /**
     * Determine the full class name for the requested class
     * @param string $class Non-fully qualified class name we want to instantiate.
     * @return string The fully qualified class name of the sport-specific class
     */
    protected static function fullClassName(string $class): string
    {
        $namespace = FrameworkConfig::get('debear.section.namespace');
        $sport = FrameworkConfig::get('debear.section.class');
        return "\\DeBear\\ORM\\Sports\\$namespace\\$sport\\$class";
    }
}
