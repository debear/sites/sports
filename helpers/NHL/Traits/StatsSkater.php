<?php

namespace DeBear\Helpers\Sports\NHL\Traits;

trait StatsSkater
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NHL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'gp' => ['s' => 'GP', 'f' => 'Games Played', 'game' => false, 'sortable' => false],
        'starts' => ['s' => 'GS', 'f' => 'Games Started', 'game' => false, 'sortable' => false],
        'start' => ['s' => 'GS', 'f' => 'Started Game', 'season' => false, 'sortable' => false],
        'goals' => ['s' => 'G', 'f' => 'Goals'],
        'assists' => ['s' => 'A', 'f' => 'Assists'],
        'points' => ['s' => 'Pts', 'f' => 'Points'],
        'plus_minus' => ['s' => '+/-', 'f' => 'Plus/Minus'],
        'pims' => ['s' => 'PIMs', 'f' => 'PIMs'],
        'shots' => ['s' => 'SOG', 'f' => 'Shots'],
        'shot_pct' => ['s' => 'Sh %', 'f' => 'Shooting %age'],
        'pp_goals' => ['s' => 'PPG', 'f' => 'PP Goals'],
        'pp_assists' => ['s' => 'PPA', 'f' => 'PP Assists'],
        'pp_points' => ['s' => 'PPP', 'f' => 'PP Points'],
        'sh_goals' => ['s' => 'SHG', 'f' => 'SH Goals'],
        'sh_assists' => ['s' => 'SHA', 'f' => 'SH Assists'],
        'sh_points' => ['s' => 'SHP', 'f' => 'SH Points'],
        'gw_goals' => ['s' => 'GWG', 'f' => 'Game Winning Goals'],
        'missed_shots' => ['s' => 'Miss', 'f' => 'Missed Shots'],
        'blocked_shots' => ['s' => 'Blckd', 'f' => 'Blocked Shots'],
        'shots_blocked' => ['s' => 'Blks', 'f' => 'Shots Blocked'],
        'hits' => ['s' => 'Hits', 'f' => 'Hits'],
        'times_hit' => ['s' => 'Hit', 'f' => 'Times Hit'],
        'fo_wins' => ['s' => 'FO W', 'f' => 'Faceoffs Won'],
        'fo_loss' => ['s' => 'FO L', 'f' => 'Faceoffs Lost'],
        'fo_pct' => ['s' => 'FO %', 'f' => 'Faceoff %age'],
        'so_goals' => ['s' => 'SO G', 'f' => 'SO Goals'],
        'so_shots' => ['s' => 'SO Sh', 'f' => 'SO Shots'],
        'so_pct' => ['s' => 'SO %', 'f' => 'SO Shot %age'],
        'giveaways' => ['s' => 'Give', 'f' => 'Giveaways'],
        'takeaways' => ['s' => 'Take', 'f' => 'Takeaways'],
        'pens_taken' => ['s' => 'Pen Tk', 'f' => 'Pens Taken'],
        'pens_drawn' => ['s' => 'Pen Dr', 'f' => 'Pens Drawn'],
        'shifts' => ['s' => 'Shift', 'f' => 'Shifts', 'season' => false, 'sortable' => false],
        'toi' => ['s' => 'TOI', 'f' => 'Time on Ice', 'season' => false, 'sortable' => false],
        'atoi' => ['s' => 'TOI', 'f' => 'Avg Time on Ice', 'game' => false],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'gp' => ['sql' => 'SUM(TBL.gp)', 'sortable' => false],
        'starts' => ['sql' => 'SUM(TBL.starts)', 'game' => false, 'sortable' => false],
        'start' => ['sql' => 'SUM(TBL.start)', 'season' => false, 'sortable' => false],
        'goals' => 'SUM(TBL.goals)',
        'assists' => 'SUM(TBL.assists)',
        'points' => 'SUM(TBL.goals) + SUM(TBL.assists)',
        'plus_minus' => 'SUM(TBL.plus_minus)',
        'pims' => 'SUM(TBL.pims)',
        'pens_taken' => 'SUM(TBL.pens_taken)',
        'pens_drawn' => 'SUM(TBL.pens_drawn)',
        'pp_goals' => 'SUM(TBL.pp_goals)',
        'pp_assists' => 'SUM(TBL.pp_assists)',
        'pp_points' => 'SUM(TBL.pp_goals + TBL.pp_assists)',
        'sh_goals' => 'SUM(TBL.sh_goals)',
        'sh_assists' => 'SUM(TBL.sh_assists)',
        'sh_points' => 'SUM(TBL.sh_goals + TBL.sh_assists)',
        'gw_goals' => 'SUM(TBL.gw_goals)',
        'shots' => 'SUM(TBL.shots)',
        'shot_pct' => '(100 * SUM(TBL.goals)) / SUM(TBL.shots)',
        'missed_shots' => 'SUM(TBL.missed_shots)',
        'blocked_shots' => 'SUM(TBL.blocked_shots)',
        'shots_blocked' => 'SUM(TBL.shots_blocked)',
        'hits' => 'SUM(TBL.hits)',
        'times_hit' => 'SUM(TBL.times_hit)',
        'fo_wins' => 'SUM(TBL.fo_wins)',
        'fo_loss' => 'SUM(TBL.fo_loss)',
        'fo_pct' => '(100 * SUM(TBL.fo_wins)) / SUM(TBL.fo_wins + TBL.fo_loss)',
        'so_goals' => 'SUM(TBL.so_goals)',
        'so_shots' => 'SUM(TBL.so_shots)',
        'so_pct' => '(100 * SUM(TBL.so_goals)) / SUM(TBL.so_shots)',
        'giveaways' => 'SUM(TBL.giveaways)',
        'takeaways' => 'SUM(TBL.takeaways)',
        'shifts' => ['sql' => 'SUM(TBL.shifts)', 'season' => false, 'sortable' => false],
        'toi' => [
            'sql' => 'SEC_TO_TIME(SUM(TIME_TO_SEC(TBL.toi) * TBL.gp) / SUM(TBL.gp))',
            'season' => false, 'sortable' => false,
        ],
        'atoi' => ['sql' => 'SEC_TO_TIME(SUM(TIME_TO_SEC(TBL.atoi) * TBL.gp) / SUM(TBL.gp))', 'game' => false],
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'shot_pct',
        'fo_pct',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, -4) == '_pct') {
            return sprintf('%0.01f', $this->$column);
        } elseif ($column == 'start') {
            return '<span class="fa fa-' . ($this->$column ? 'check' : 'times') . '-circle"></span>';
        } elseif ($column == 'plus_minus') {
            return sprintf('%s%d', $this->$column > 0 ? '+' : '', $this->$column);
        } elseif (substr($column, -3) == 'toi') {
            return ltrim(substr($this->$column, 3, 5), '0');
        }
        // Fallback value: number as-is.
        return $this->$column;
    }

    /**
     * Return the formatted +/.
     * @return string The player's +/-, with positive formatted.
     */
    public function getPlusMinusAttribute(): string
    {
        return ($this->plus_minus > 0 ? '+' : '') . $this->plus_minus;
    }

    /**
     * Return the faceoff win/loss.
     * @return string The faceoffs won/lost.
     */
    public function getFaceoffsAttribute(): string
    {
        if (!isset($this->fo_wins)) {
            return '&mdash;';
        }
        return "{$this->fo_wins}&ndash;{$this->fo_loss}";
    }

    /**
     * Return the time on ice as MM:SS.
     * @return string The time minus the hour part.
     */
    public function getTimeOnIceAttribute(): string
    {
        list($h, $m, $s) = explode(':', $this->toi);
        return sprintf('%02d:%02d', (60 * (int)$h) + (int)$m, $s);
    }
}
