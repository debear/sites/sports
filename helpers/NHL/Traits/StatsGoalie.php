<?php

namespace DeBear\Helpers\Sports\NHL\Traits;

trait StatsGoalie
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NHL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'decision' => ['s' => 'Dec', 'f' => 'Game Decision', 'season' => false, 'sortable' => false],
        'gp' => ['s' => 'GP', 'f' => 'Games Played', 'game' => false],
        'starts' => ['s' => 'GS', 'f' => 'Games Started', 'game' => false],
        'start' => ['s' => 'GS', 'f' => 'Started Game', 'season' => false, 'sortable' => false],
        'win' => ['s' => 'W', 'f' => 'Wins', 'game' => false],
        'loss' => ['s' => 'L', 'f' => 'Losses', 'game' => false],
        'ot_loss' => ['s' => 'OTL', 'f' => 'OT Losses', 'game' => false],
        'so_loss' => ['s' => 'SOL', 'f' => 'SO Losses', 'game' => false],
        'goals_against_avg' => ['s' => 'GAA', 'f' => 'Goals Against Avg'],
        'save_pct' => ['s' => 'SV%', 'f' => 'Save %age'],
        'shutout' => ['s' => 'SHO', 'f' => 'Shutouts'],
        'goals_against' => ['s' => 'GA', 'f' => 'Goals Against'],
        'shots_against' => ['s' => 'SA', 'f' => 'Shots Against'],
        'saves' => ['s' => 'SV', 'f' => 'Saves'],
        'minutes_played_ALT' => ['s' => 'Min', 'f' => 'Minutes Played', 'game' => false],
        'minutes_played' => ['s' => 'Min', 'f' => 'Minutes Played', 'season' => false, 'sortable' => false],
        'en_goals_against' => ['s' => 'EN GA', 'f' => 'EN Goals Against'],
        'so_goals_against' => ['s' => 'SO GA', 'f' => 'SO Goals Against'],
        'so_shots_against' => ['s' => 'SO Sh', 'f' => 'SO Shots Against'],
        'so_saves' => ['s' => 'SO SV', 'f' => 'SO Saves'],
        'so_save_pct' => ['s' => 'SO %', 'f' => 'SO Save %age'],
        'goals' => ['s' => 'G', 'f' => 'Goals'],
        'assists' => ['s' => 'A', 'f' => 'Assists'],
        'points' => ['s' => 'Pts', 'f' => 'Points'],
        'pims' => ['s' => 'PIMs', 'f' => 'PIMs'],
        'pens_taken' => ['s' => 'Pen Tk', 'f' => 'Penalties Taken'],
        'pens_drawn' => ['s' => 'Pen Dr', 'f' => 'Penalties Drawn'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'gp' => 'SUM(TBL.gp)',
        'starts' => ['sql' => 'SUM(TBL.starts)', 'game' => false],
        'start' => ['sql' => 'SUM(TBL.start)', 'season' => false, 'sortable' => false],
        'win' => 'SUM(TBL.win)',
        'loss' => 'SUM(TBL.loss)',
        'tie' => 'SUM(TBL.tie)',
        'ot_loss' => 'SUM(TBL.ot_loss)',
        'so_loss' => 'SUM(TBL.so_loss)',
        'goals_against' => 'SUM(TBL.goals_against)',
        'shots_against' => 'SUM(TBL.shots_against)',
        'saves' => 'SUM(TBL.shots_against) - SUM(TBL.goals_against)',
        'save_pct' => '(SUM(TBL.shots_against) - SUM(TBL.goals_against)) / SUM(TBL.shots_against)',
        'goals_against_avg' => '(SUM(TBL.goals_against) * 3600) / SUM(IF(TBL.minutes_played REGEXP "^[0-9]+$", '
            . 'TBL.minutes_played, TIME_TO_SEC(TBL.minutes_played)))',
        'shutout' => 'SUM(TBL.shutout)',
        'minutes_played_ALT' => ['sql' => 'SUM(TBL.minutes_played)', 'game' => false],
        'minutes_played' => [
            'sql' => 'SEC_TO_TIME(SUM(TIME_TO_SEC(TBL.minutes_played) * TBL.gp) / SUM(TBL.gp))',
            'season' => false, 'sortable' => false,
        ],
        'en_goals_against' => 'SUM(TBL.en_goals_against)',
        'so_goals_against' => 'SUM(TBL.so_goals_against)',
        'so_shots_against' => 'SUM(TBL.so_shots_against)',
        'so_saves' => 'SUM(TBL.so_shots_against) - SUM(TBL.so_goals_against)',
        'so_save_pct' => '(100 * (SUM(TBL.so_shots_against) - SUM(TBL.so_goals_against))) / SUM(TBL.so_shots_against)',
        'goals' => 'SUM(TBL.goals)',
        'assists' => 'SUM(TBL.assists)',
        'points' => 'SUM(TBL.goals + TBL.assists)',
        'pims' => 'SUM(TBL.pims)',
        'pens_taken' => 'SUM(TBL.pens_taken)',
        'pens_drawn' => 'SUM(TBL.pens_drawn)',
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'goals_against',
        'save_pct',
        'goals_against_avg',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'goals_against_avg') {
            return sprintf('%0.02f', $this->$column);
        } elseif ($column == 'save_pct') {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        } elseif ($column == 'so_save_pct') {
            return sprintf('%0.01f', $this->$column);
        } elseif ($column == 'minutes_played' && strpos($this->$column, ':') === false) {
            // Convert number of seconds to Minutes:Seconds.
            return sprintf('%d:%02d', floor($this->$column / 60), $this->$column % 60);
        } elseif ($column == 'minutes_played') {
            // Convert a time (with hours) to Minutes:Seconds.
            list($h, $m, $s) = explode(':', $this->$column);
            return sprintf('%d:%02d', (60 * (int)$h) + (int)$m, $s);
        } elseif ($column == 'decision') {
            return '<span class="' . $this->decisionCSS() . '">' . $this->$column . '</span>';
        } elseif ($column == 'start') {
            return '<span class="fa fa-' . ($this->$column ? 'check' : 'times') . '-circle"></span>';
        }
        // Fallback value: number as-is.
        return $this->$column;
    }

    /**
     * Get the appropriate CSS for the decision
     * @return ?string The CSS class
     */
    public function decisionCSS(): ?string
    {
        if ($this->win) {
            return 'win';
        } elseif ($this->loss) {
            return 'loss';
        } elseif ($this->tie || $this->ot_loss || $this->so_loss) {
            return 'tie';
        }
        return null;
    }

    /**
     * Convert the decision flags to a label
     * @return ?string The decision label
     */
    public function getDecisionAttribute(): ?string
    {
        if ($this->win) {
            return 'Win';
        } elseif ($this->loss) {
            return 'Loss';
        } elseif ($this->tie) {
            return 'Tie';
        } elseif ($this->ot_loss) {
            return 'OT Loss';
        } elseif ($this->so_loss) {
            return 'SO Loss';
        }
        return null;
    }

    /**
     * Return the time on ice as MM:SS.
     * @return string The time minus the hour part.
     */
    public function getTimeOnIceAttribute(): string
    {
        list($h, $m, $s) = explode(':', $this->minutes_played);
        return sprintf('%02d:%02d', (60 * (int)$h) + (int)$m, $s);
    }

    /**
     * Establish the number of saves made
     * @return integer The number of saves made
     */
    public function getSavesAttribute(): int
    {
        return ($this->shots_against - $this->goals_against);
    }

    /**
     * Format the goalie's save percentage
     * @return string The goalie's save percentage
     */
    public function getSavePctAttribute(): string
    {
        return $this->shots_against ? ltrim(sprintf('%.03f', $this->saves / $this->shots_against), '0') : '&ndash;';
    }

    /**
     * Format the goalie's goals against average
     * @return string The goalie's goals against percentage
     */
    public function getGoalsAgainstAvgAttribute(): string
    {
        // Get the time component.
        $minutes = !is_numeric($this->minutes_played)
            ? $this->minutes_played
            : sprintf(
                '%d:%02d:%02d',
                floor($this->minutes_played / 3600),
                floor($this->minutes_played / 60) % 60,
                $this->minutes_played % 60
            );
        list($h, $m, $s) = explode(':', $minutes);
        $m += (intval($h) * 60) + (intval($s) / 60);
        // Now manipulate the goals against per 60 minutes.
        return $m ? sprintf('%0.02f', ($this->goals_against / $m) * 60) : '&ndash';
    }

    /**
     * Convert the goalie's scoring into a single points value
     * @return integer The points value
     */
    public function getPtsAttribute(): int
    {
        return $this->goals + $this->assists;
    }
}
