<?php

namespace DeBear\Helpers\Sports\NHL\Traits;

trait StatsSkaterAdvanced
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NHL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'corsi_cf' => ['s' => 'CF', 'f' => 'Corsi For', 'g' => 'Corsi'],
        'corsi_ca' => ['s' => 'CA', 'f' => 'Corsi Against', 'g' => 'Corsi'],
        'corsi_pct' => ['s' => 'C%', 'f' => 'Corsi %', 'g' => 'Corsi'],
        'corsi_off_cf' => ['s' => 'OffF', 'f' => 'Corsi Off-Ice For', 'g' => 'Corsi'],
        'corsi_off_ca' => ['s' => 'OffA', 'f' => 'Corsi Off-Ice Against', 'g' => 'Corsi'],
        'corsi_off_pct' => ['s' => 'Off%', 'f' => 'Corsi Off-Ice %', 'g' => 'Corsi'],
        'corsi_rel' => ['s' => 'Rel', 'f' => 'Corsi Relative', 'g' => 'Corsi'],
        'fenwick_ff' => ['s' => 'FF', 'f' => 'Fenwick For', 'g' => 'Fenwick'],
        'fenwick_fa' => ['s' => 'FA', 'f' => 'Fenwick Against', 'g' => 'Fenwick'],
        'fenwick_pct' => ['s' => 'F%', 'f' => 'Fenwick %', 'g' => 'Fenwick'],
        'fenwick_off_ff' => ['s' => 'OffF', 'f' => 'Fenwick Off-Ice For', 'g' => 'Fenwick'],
        'fenwick_off_fa' => ['s' => 'OffA', 'f' => 'Fenwick Off-Ice Against', 'g' => 'Fenwick'],
        'fenwick_off_pct' => ['s' => 'Off%', 'f' => 'Fenwick Off-Ice %', 'g' => 'Fenwick'],
        'fenwick_rel' => ['s' => 'Rel', 'f' => 'Fenwick Relative', 'g' => 'Fenwick'],
        'pdo_sh_gf' => ['s' => 'GF', 'f' => 'PDO Goals For', 'g' => 'PDO'],
        'pdo_sh_sog' => ['s' => 'SOG', 'f' => 'PDO Shots on Goal', 'g' => 'PDO'],
        'pdo_sh' => ['s' => 'Sh%', 'f' => 'PDO Shooting %age', 'g' => 'PDO'],
        'pdo_sv_sv' => ['s' => 'Sv', 'f' => 'PDO Saves', 'g' => 'PDO'],
        'pdo_sv_sog' => ['s' => 'SA', 'f' => 'PDO Shots Against', 'g' => 'PDO'],
        'pdo_sv' => ['s' => 'Sv%', 'f' => 'PDO Save %age', 'g' => 'PDO'],
        'pdo' => ['s' => 'PDO', 'f' => 'PDO', 'g' => 'PDO'],
        'zs_off_num' => ['s' => 'Off', 'f' => 'Offensive Zone Starts', 'g' => 'Zone Starts'],
        'zs_off_pct' => ['s' => 'Off %', 'f' => 'Zone Start %age', 'g' => 'Zone Starts'],
        'zs_def_num' => ['s' => 'Def', 'f' => 'Defensive Zone Starts', 'g' => 'Zone Starts'],
        'zs_def_pct' => ['s' => 'Def %', 'f' => 'Defensive Start %age', 'g' => 'Zone Starts'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'corsi_cf' => 'SUM(TBL.corsi_cf)',
        'corsi_ca' => 'SUM(TBL.corsi_ca)',
        'corsi_pct' => '(100 * SUM(TBL.corsi_cf)) / SUM(TBL.corsi_cf + TBL.corsi_ca)',
        'corsi_off_cf' => 'SUM(TBL.corsi_off_cf)',
        'corsi_off_ca' => 'SUM(TBL.corsi_off_ca)',
        'corsi_off_pct' => '(100 * SUM(TBL.corsi_off_cf)) / SUM(TBL.corsi_off_cf + TBL.corsi_off_ca)',
        'corsi_rel' => '((100 * SUM(TBL.corsi_cf)) / SUM(TBL.corsi_cf + TBL.corsi_ca))'
            . ' - ((100 * SUM(TBL.corsi_off_cf)) / SUM(TBL.corsi_off_cf + TBL.corsi_off_ca))',
        'fenwick_ff' => 'SUM(TBL.fenwick_ff)',
        'fenwick_fa' => 'SUM(TBL.fenwick_fa)',
        'fenwick_pct' => '(100 * SUM(TBL.fenwick_ff)) / SUM(TBL.fenwick_ff + TBL.fenwick_fa)',
        'fenwick_off_ff' => 'SUM(TBL.fenwick_off_ff)',
        'fenwick_off_fa' => 'SUM(TBL.fenwick_off_fa)',
        'fenwick_off_pct' => '(100 * SUM(TBL.fenwick_off_ff)) / SUM(TBL.fenwick_off_ff + TBL.fenwick_off_fa)',
        'fenwick_rel' => '((100 * SUM(TBL.fenwick_ff)) / SUM(TBL.fenwick_ff + TBL.fenwick_fa))'
            . ' - ((100 * SUM(TBL.fenwick_off_ff)) / SUM(TBL.fenwick_off_ff + TBL.fenwick_off_fa))',
        'pdo_sh_gf' => 'SUM(TBL.pdo_sh_gf)',
        'pdo_sh_sog' => 'SUM(TBL.pdo_sh_sog)',
        'pdo_sh' => '(100 * SUM(TBL.pdo_sh_gf)) / SUM(TBL.pdo_sh_sog)',
        'pdo_sv_sv' => 'SUM(TBL.pdo_sv_sv)',
        'pdo_sv_sog' => 'SUM(TBL.pdo_sv_sog)',
        'pdo_sv' => '(100 * SUM(TBL.pdo_sv_sv)) / SUM(TBL.pdo_sv_sog)',
        'pdo' => '((100 * SUM(TBL.pdo_sh_gf)) / SUM(TBL.pdo_sh_sog))'
            . ' + ((100 * SUM(TBL.pdo_sv_sv)) / SUM(TBL.pdo_sv_sog))',
        'zs_off_num' => 'SUM(TBL.zs_off_num)',
        'zs_off_pct' => '(100 * SUM(TBL.zs_off_num)) / SUM(TBL.zs_off_num + TBL.zs_def_num)',
        'zs_def_num' => 'SUM(TBL.zs_def_num)',
        'zs_def_pct' => '(100 * SUM(TBL.zs_def_num)) / SUM(TBL.zs_off_num + TBL.zs_def_num)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        $ends = substr($column, -4);
        if (in_array($ends, ['_pct', '_rel']) || in_array($column, ['pdo_sh', 'pdo_sv', 'pdo'])) {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
