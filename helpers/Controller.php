<?php

namespace DeBear\Helpers\Sports;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class Controller
{
    /**
     * Forward a static call to the specific Controller Helper
     * @param string $method    The name of the requested method.
     * @param array  $arguments The arguments passed to the requested method.
     * @return mixed Any return value
     */
    public static function __callStatic(string $method, array $arguments): mixed
    {
        $fqn = '\DeBear\Helpers\Sports\\' . FrameworkConfig::get('debear.section.namespace') . '\Controller';
        $process = (class_exists($fqn) && method_exists($fqn, $method));
        return $process ? call_user_func_array([$fqn, $method], $arguments) : null;
    }
}
