<?php

namespace DeBear\Helpers\Sports;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use DeBear\Models\Skeleton\NewsSource as NewsSourceModel;
use DeBear\Models\Skeleton\News as NewsModel;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Repositories\Time;

class Homepage
{
    /**
     * Update the navigation configuration to be our list of subsites
     * @return void
     */
    public static function setSubsiteNav(): void
    {
        $nav_merge = [];
        foreach (array_keys(FrameworkConfig::get('debear.subsites')) as $s) {
            $label = FrameworkConfig::get("debear.links.subsite-$s.label");
            $nav_merge["debear.links.subsite-$s.label"] = "<span class=\"hidden-d\">$label</span>";
            $nav_merge["debear.links.subsite-$s.icon"] = "sports_{$s}_icon";
            $nav_merge["debear.links.subsite-$s.navigation"] = ['enabled' => true];
        }
        FrameworkConfig::set($nav_merge);
    }

    /**
     * Get the latest news articles for all sport subsites
     * @return EloquentCollection A collection of news articles
     */
    public static function getLatestNews(): EloquentCollection
    {
        $sources = array_column(NewsSourceModel::query()
            ->select('app')
            ->distinct()
            ->where('app', 'like', 'sports\_%')
            ->get()
            ->toArray(), 'app');
        $news = NewsModel::with('source')
            ->whereIn('app', $sources)
            ->orderBy('published', 'desc') // First order by published date.
            ->orderBy('news_id', 'desc')   // And then in the case of contention, ID, for consistency.
            ->limit(20)
            ->get();
        // Customise the CSP rules.
        NewsModel::updateCSP($sources);

        return $news;
    }

    /**
     * Get the latest events (games, races, meetings) for yesterday and today
     * @return array An array of ORM models
     */
    public static function getLatestEvents(): array
    {
        $ret = [];
        $date_maps = [
            Time::object()->adjustFormatUser('-1 day', 'Y-m-d') => 'yesterday',
            Time::object()->formatUser('Y-m-d') => 'today',
        ];
        $config = FrameworkConfig::get('debear.section');
        foreach (FrameworkConfig::get('debear.subsites') as $s => $c) {
            FrameworkConfig::set([
                'debear.section' => $c['section'],
                'debear.section.endpoint' => $s,
                'debear.section.code' => $s,
            ]);
            $ret = Arrays::merge(
                $ret,
                Namespaces::callStatic($c['section']['event_class'], 'getHomepageEvents', [$date_maps, $s])
            );
        }
        FrameworkConfig::set(['debear.section' => $config]);
        return $ret;
    }
}
