<?php

namespace DeBear\Helpers\Sports\SGP\Traits;

trait Race
{
    /**
     * Return the race's country flag, which may need "cleaning" to match the flag sprite
     * @return string The sprite-compatible flag
     */
    public function flag(): string
    {
        // Polish SGP races go under the single national flag.
        return (substr($this->flag, 0, 3) == 'pl-') ? 'pl' : $this->flag;
    }
}
