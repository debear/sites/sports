<?php

namespace DeBear\Helpers\Sports\MajorLeague;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\Namespaces;

class SiteTools
{
    /**
     * How much we should reduce the priority for archived data.
     * @var float
     */
    protected static $archive_ratio = 0.4;

    /**
     * Expand the sitemap to include dynamic properties
     * @return void
     */
    public static function expandSitemap(): void
    {
        $links = FrameworkConfig::get('debear.links');
        // Unlink the subsites from the sitemap.
        foreach (array_keys($links) as $code) {
            if (substr($code, 0, 8) == 'subsite-') {
                $links[$code]['sitemap']['enabled'] = false;
            }
        }
        // Write the result back.
        FrameworkConfig::set(['debear.links' => $links]);
    }

    /**
     * Expand the XML version of the sitemap to include the dynamic properties for the main sitemap
     * @return void
     */
    public static function expandSitemapXMLMain(): void
    {
        $links = FrameworkConfig::get('debear.links');
        $seasons = FrameworkConfig::get('debear.setup.season');
        $curr_season = $seasons['max'];
        // Unlink the subsites from the sitemap.
        foreach (array_keys($links) as $code) {
            if (substr($code, 0, 8) == 'subsite-') {
                $links[$code]['sitemap']['enabled'] = false;
            }
        }
        // Standings and Playoffs (by season, skipping current).
        for ($s = $seasons['min']; $s < $seasons['max']; $s++) {
            $link_priority = floatval($links['standings']['sitemap']['priority']);
            $links["standings-$s"] = [
                'url' => "{$links['standings']['url']}/$s",
                'section' => 'standings',
                'order' => 3000 - $s,
                'url-section' => isset($links['standings']) && $links['standings'],
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $link_priority * static::$archive_ratio),
                    'frequency' => 'never',
                ],
            ];
            $link_priority = floatval($links['playoffs']['sitemap']['priority']);
            $links["playoffs-$s"] = [
                'url' => "{$links['playoffs']['url']}/$s",
                'section' => $links['playoffs']['section'],
                'order' => 3000 - $s,
                'url-section' => isset($links['playoffs']) && $links['playoffs'],
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $link_priority * static::$archive_ratio),
                    'frequency' => 'never',
                ],
            ];
        }
        // Stats (by group, by season - regular only).
        for ($s = $seasons['min']; $s <= $seasons['max']; $s++) {
            $is_curr = ($s == $curr_season);
            $stat_factor = ($is_curr ? 1 : static::$archive_ratio);
            foreach (FrameworkConfig::get('debear.setup.stats.details') as $group => $stats) {
                foreach (array_keys($stats) as $i => $stat) {
                    $link_priority = floatval($links['stats']['sitemap']['priority']);
                    $links["stats-$s-$group"] = [
                        'url' => "{$links['stats']['url']}/{$group}s/$stat?season=$s&amp;amp;type=regular",
                        'section' => 'stats',
                        'order' => (3000 - $s) + ($i / 100),
                        'url-section' => isset($links['stats']) && $links['stats'],
                        'sitemap' => [
                            'enabled' => true,
                            'priority' => sprintf('%.01f', $link_priority * $stat_factor),
                            'frequency' => ($is_curr ? 'daily' : 'never'),
                        ],
                    ];
                }
            }
        }
        // Individual Teams.
        foreach (Namespaces::callStatic('Team', 'getAllSitemap') as $i => $team) {
            $is_curr = ($team->max_season == $curr_season);
            $team_factor = ($is_curr ? 1 : static::$archive_ratio);
            $link_priority = floatval($links['teams']['sitemap']['priority']);
            $links["teams-{$team->team_id}"] = [
                'url' => $team->link,
                'section' => 'teams',
                'order' => $i,
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $link_priority * $team_factor),
                    'frequency' => ($is_curr ? 'daily' : 'never'),
                ],
            ];
        }
        // Drafts (by season).
        if (FrameworkConfig::get('debear.links.draft.sitemap.enabled')) {
            foreach (Namespaces::callStatic('Draft', 'getAllSeasons') as $s) {
                if ($s != $curr_season) {
                    $links["draft-{$s}"] = [
                        'url' => "{$links['draft']['url']}/$s",
                        'section' => $links['draft']['section'],
                        'url-section' => isset($links['draft']) && $links['draft'],
                        'order' => 3000 - $s,
                        'sitemap' => [
                            'enabled' => true,
                            'priority' => static::$archive_ratio,
                            'frequency' => 'never',
                        ],
                    ];
                }
            }
        }
        // Write the result back.
        FrameworkConfig::set(['debear.links' => $links]);
    }

    /**
     * Expand the XML version of the sitemap to include the dynamic properties for players
     * @return void
     */
    public static function expandSitemapXMLPlayers(): void
    {
        $raw_links = FrameworkConfig::get('debear.links');
        $links = []; // Start from scratch for this section.
        // Individual Players.
        foreach (Namespaces::callStatic('Player', 'getAllSitemap') as $i => $player) {
            $player_factor = ($player->is_curr ? 1 : static::$archive_ratio);
            $links["players-{$player->player_id}"] = [
                'url' => $player->link,
                'order' => $i,
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $raw_links['players']['sitemap']['priority'] * $player_factor),
                    'frequency' => ($player->is_curr ? 'daily' : 'yearly'),
                ],
            ];
        }
        // Write the result back.
        FrameworkConfig::set(['debear.links' => $links]);
    }

    /**
     * Expand the XML version of the sitemap to include the dynamic properties a season's games
     * @param string $season The season to be loaded.
     * @return void
     */
    public static function expandSitemapXMLSeason(string $season): void
    {
        $raw_links = FrameworkConfig::get('debear.links');
        $links = []; // Start from scratch for this section.
        // Individual Games.
        $curr_season = FrameworkConfig::get('debear.setup.season.max');
        $orig = FrameworkConfig::get('debear.setup.season.viewing');
        FrameworkConfig::set(['debear.setup.season.viewing' => $season]);
        foreach (Namespaces::callStatic('Schedule', 'getAllSitemap', [$season]) as $i => $game) {
            $game_factor = ($game->season == $curr_season ? 1 : static::$archive_ratio);
            $links["schedule-{$game->season}-{$game->game_type}-{$game->game_id}"] = [
                'url' => $game->link,
                'order' => $i,
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $raw_links['schedule']['sitemap']['priority'] * $game_factor),
                    'frequency' => 'never',
                ],
            ];
        }
        // Write the result back.
        FrameworkConfig::set([
            'debear.setup.season.viewing' => $orig,
            'debear.links' => $links,
        ]);
    }
}
