<?php

namespace DeBear\Helpers\Sports\MajorLeague;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class Controller
{
    /**
     * Format the season as a descriptive value
     * @param integer $season The (single) season value being formatted.
     * @return string The formatted version for this sport
     */
    public static function seasonTitle(int $season): string
    {
        if (substr(FrameworkConfig::get('debear.section.code'), -2) == 'hl') {
            // AHL / NHL - covers two calendar years.
            return sprintf('%04d/%02d', $season, ($season + 1) % 100);
        }
        // Fallback: Single calendar year.
        return (string)$season;
    }

    /**
     * Check that the supplied season is valid in the context of the seasons we have data for
     * @param integer $season The seasonv value to be tested.
     * @return boolean The argument is valid
     */
    public static function validateSeason(int $season): bool
    {
        $setup = FrameworkConfig::get('debear.setup.season');
        return ($setup['min'] <= $season) && ($season <= $setup['max']);
    }
}
