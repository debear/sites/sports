<?php

namespace DeBear\Helpers\Sports\MajorLeague\Traits;

use DeBear\ORM\Sports\MajorLeague\PlayerSeasonStat;
use DeBear\ORM\Sports\MajorLeague\TeamSeasonStat;

trait SortedStats
{
    /**
     * Return the additional list of where clause restrictions to identify totals rows
     * @return ?array The additional ORM where clause arguments
     */
    public static function getWhereIsTotalsClause(): ?array
    {
        return (new static())->getStatsObject()->getWhereIsTotalsClause();
    }

    /**
     * Return the name of the main stats table
     * @return string The main stats table name
     */
    public static function getStatTable(): string
    {
        return (new static())->getStatsClass()::getTable();
    }

    /**
     * Get the equivalent stats class
     * @return string The stats object path
     */
    public function getStatsClass(): string
    {
        return $this->statInstance;
    }

    /**
     * Get the instantiated equivalent stats object
     * @return PlayerSeasonStat|TeamSeasonStat The stats object
     */
    public function getStatsObject(): PlayerSeasonStat|TeamSeasonStat
    {
        $class = $this->getStatsClass();
        return (new $class());
    }

    /**
     * Validate whether a column is available for the class
     * @param string $column The stat column to be tested.
     * @return boolean That the stat is available or not
     */
    public static function validateColumn(string $column): bool
    {
        return in_array($column, array_keys((new static())->getSeasonStatMeta()));
    }

    /**
     * Return the list of sortable stat column meta info for this class
     * @return array The column meta info
     */
    public function getSortableStatMeta(): array
    {
        return $this->getStatsObject()->getSortableStatMeta();
    }

    /**
     * Return the list of season stat column meta info for this class
     * @return array The column meta info
     */
    public function getSeasonStatMeta(): array
    {
        return $this->getStatsObject()->getSeasonStatMeta();
    }

    /**
     * Return the list of single game stat column meta info for this class
     * @return array The column meta info
     * /
    public function getGameStatMeta(): array
    {
        return $this->getStatsObject()->getGameStatMeta();
    }
    /* */

    /**
     * Return the list of season stat columns available in this class
     * @return ?array The columns as a list, or null if not applicalble
     */
    public function getSeasonStatColumns(): ?array
    {
        return $this->getStatsObject()->getSeasonStatColumns();
    }

    /**
     * Return the list of sortable stat columns available in this class
     * @return ?array The columns as a list, or null if not applicalble
     */
    public function getSortableStatColumns(): ?array
    {
        return $this->getStatsObject()->getSortableStatColumns();
    }

    /**
     * Return the list of single game stat columns available in this class
     * @return ?array The columns as a list, or null if not applicalble
     * /
    public function getGameStatColumns(): ?array
    {
        return $this->getStatsObject()->getGameStatColumns();
    }
    /* */

    /**
     * Return the list of qualifying stat columns in this class
     * @return ?array The qualifying columns as a list, or null if not applicalble
     */
    public function getStatQualified(): ?array
    {
        return $this->getStatsObject()->getStatQualified();
    }

    /**
     * Get the list of available seasons and season types for stats
     * @return self An ORM object containing just the unique list of seasons and season type's
     */
    public static function getAvailableSeasons(): self
    {
        return static::query()
            ->select(['season', 'season_type'])
            ->groupBy('season')
            ->groupBy('season_type')
            ->orderByDesc('season')
            ->orderByDesc('season_type')
            ->get();
    }
}
