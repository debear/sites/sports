<?php

namespace DeBear\Helpers\Sports\MajorLeague\Traits;

trait Stats
{
    /**
     * Return the additional list of where clause restrictions to identify totals rows
     * @return ?array The additional ORM where clause arguments
     */
    public function getWhereIsTotalsClause(): ?array
    {
        return $this->whereIsTotals;
    }

    /**
     * Return the list of season stat column meta info for this class
     * @return array The column meta info
     */
    public function getSeasonStatMeta(): array
    {
        return $this->getStatMeta('season');
    }

    /**
     * Return the list of sortable stat column meta info for this class
     * @return array The column meta info
     */
    public function getSortableStatMeta(): array
    {
        return $this->getStatMeta('sortable');
    }

    /**
     * Return the list of single game stat column meta info for this class
     * @return array The column meta info
     */
    public function getGameStatMeta(): array
    {
        return $this->getStatMeta('game');
    }

    /**
     * Worker method for getting the list of column meta info for this class
     * @param string $type The type of stat meta we want to pull out.
     * @return array The column meta info
     */
    protected function getStatMeta(string $type): array
    {
        // Return only those columns appropriate to the requested type (minus the type flags).
        return $this->renameStatColumns(array_diff_key(array_filter($this->statMeta, function ($a) use ($type) {
            // If no key is set for this type, it is assumed to be available.
            return !isset($a[$type]) || $a[$type];
        }), ['season' => true, 'game' => true]));
    }

    /**
     * Return the list of season stat columns available in this class
     * @return ?array The columns as a list, or null if not applicable
     */
    public function getSeasonStatColumns(): ?array
    {
        return $this->getStatColumns('season');
    }

    /**
     * Return the list of sortable stat columns available in this class
     * @return ?array The columns as a list, or null if not applicable
     */
    public function getSortableStatColumns(): ?array
    {
        return $this->getStatColumns('sortable');
    }

    /**
     * Return the list of single game stat columns available in this class
     * @return ?array The columns as a list, or null if not applicable
     */
    public function getGameStatColumns(): ?array
    {
        return $this->getStatColumns('game');
    }

    /**
     * Worker method for getting the list of stat columns available in this class
     * @param string $type The type of stat columns we want to pull out.
     * @return ?array The columns as a list, or null if not applicable
     */
    protected function getStatColumns(string $type): ?array
    {
        if (!isset($this->statColumns)) {
            return null;
        }
        return array_map(function ($a) {
            return is_string($a) ? $a : $a['sql'];
        }, $this->renameStatColumns(array_filter($this->statColumns, function ($a) use ($type) {
            return is_string($a) || !isset($a[$type]) || $a[$type];
        })));
    }

    /**
     * Return the list of qualifying stat columns in this class
     * @return ?array The qualifying columns as a list, or null if not applicable
     */
    public function getStatQualified(): ?array
    {
        return $this->statQual;
    }

    /**
     * Rename the stat columns, should we have aliases for the same stat that is reduced via per-stat config
     * @param array $columns The raw columns to be parse.
     * @return array The final stat array with columns re-keyed to the appropriate name
     */
    protected function renameStatColumns(array $columns): array
    {
        $keys = array_map(function ($k) {
            return preg_replace('/_ALT\d*$/', '', $k);
        }, array_keys($columns));
        return array_combine($keys, array_values($columns));
    }
}
