<?php

namespace DeBear\Helpers\Sports\MajorLeague\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;

trait ScheduleDate
{
    /**
     * The full URL for the dated schedule
     * @param string $col The applicable date column.
     * @return string The full schedule link
     */
    public function linkFull(string $col = ''): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . '/schedule/' . $this->linkDate($col);
    }

    /**
     * The full URL for the dated starting lineup list
     * @param string $col The applicable date column.
     * @return string The full starting lineups link
     */
    public function linkLineups(string $col = ''): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . '/lineups/' . $this->linkDate($col);
    }

    /**
     * The full URL for the dated probable pitcher list
     * @param string $col The applicable date column.
     * @return string The full probable pitchers link
     */
    public function linkProbables(string $col = ''): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . '/probables/' . $this->linkDate($col);
    }

    /**
     * Specify the date level of the game's link
     * @param string $col The applicable date column.
     * @return string The date component of the game's link
     */
    public function linkDate(string $col = ''): string
    {
        if ($col) {
            $col = "date_$col";
            $val = $this->$col;
        } else {
            $val = ($this->game_date ?? $this->date);
        }
        return str_replace('-', '', $val);
    }
}
