<?php

namespace DeBear\Helpers\Sports\AHL\Traits;

trait StatsSkater
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_LG'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'gp' => ['s' => 'GP', 'f' => 'Games Played', 'game' => false, 'sortable' => false],
        'starts' => ['s' => 'GS', 'f' => 'Games Started', 'game' => false, 'sortable' => false],
        'start' => ['s' => 'GS', 'f' => 'Started Game', 'season' => false, 'sortable' => false],
        'goals' => ['s' => 'G', 'f' => 'Goals'],
        'assists' => ['s' => 'A', 'f' => 'Assists'],
        'points' => ['s' => 'Pts', 'f' => 'Points'],
        'plus_minus' => ['s' => '+/-', 'f' => 'Plus/Minus'],
        'pims' => ['s' => 'PIMs', 'f' => 'PIMs'],
        'pp_goals' => ['s' => 'PPG', 'f' => 'PP Goals'],
        'pp_assists' => ['s' => 'PPA', 'f' => 'PP Assists'],
        'pp_points' => ['s' => 'PPP', 'f' => 'PP Points'],
        'sh_goals' => ['s' => 'SHG', 'f' => 'SH Goals'],
        'sh_assists' => ['s' => 'SHA', 'f' => 'SH Assists'],
        'sh_points' => ['s' => 'SHP', 'f' => 'SH Points'],
        'gw_goals' => ['s' => 'GWG', 'f' => 'Game Winning Goals'],
        'shots' => ['s' => 'SOG', 'f' => 'Shots'],
        'shot_pct' => ['s' => 'Sh %', 'f' => 'Shooting %age'],
        'so_goals' => ['s' => 'SO G', 'f' => 'SO Goals'],
        'so_shots' => ['s' => 'SO Sh', 'f' => 'SO Shots'],
        'so_pct' => ['s' => 'SO %', 'f' => 'SO Shot %age'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'gp' => ['sql' => 'SUM(TBL.gp)', 'sortable' => false],
        'starts' => ['sql' => 'SUM(TBL.starts)', 'game' => false, 'sortable' => false],
        'start' => ['sql' => 'SUM(TBL.start)', 'season' => false, 'sortable' => false],
        'goals' => 'SUM(TBL.goals)',
        'assists' => 'SUM(TBL.assists)',
        'points' => 'SUM(TBL.goals + TBL.assists)',
        'plus_minus' => 'SUM(TBL.plus_minus)',
        'pims' => 'SUM(TBL.pims)',
        'pp_goals' => 'SUM(TBL.pp_goals)',
        'pp_assists' => 'SUM(TBL.pp_assists)',
        'pp_points' => 'SUM(TBL.pp_goals + TBL.pp_assists)',
        'sh_goals' => 'SUM(TBL.sh_goals)',
        'sh_assists' => 'SUM(TBL.sh_assists)',
        'sh_points' => 'SUM(TBL.sh_goals + TBL.sh_assists)',
        'gw_goals' => 'SUM(TBL.gw_goals)',
        'shots' => 'SUM(TBL.shots)',
        'shot_pct' => '(100 * SUM(TBL.goals)) / SUM(TBL.shots)',
        'so_goals' => 'SUM(TBL.so_goals)',
        'so_shots' => 'SUM(TBL.so_shots)',
        'so_pct' => '(100 * SUM(TBL.so_goals)) / SUM(TBL.so_shots)',
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'shot_pct',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, -4) == '_pct') {
            return sprintf('%0.01f', $this->$column);
        } elseif ($column == 'start') {
            return '<span class="fa fa-' . ($this->$column ? 'check' : 'times') . '-circle"></span>';
        } elseif ($column == 'plus_minus') {
            return sprintf('%s%d', $this->$column > 0 ? '+' : '', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }

    /**
     * Return the formatted +/.
     * @return string The player's +/-, with positive formatted.
     */
    public function getPlusMinusAttribute(): string
    {
        return ($this->plus_minus > 0 ? '+' : '') . $this->plus_minus;
    }
}
