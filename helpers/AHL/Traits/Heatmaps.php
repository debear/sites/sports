<?php

namespace DeBear\Helpers\Sports\AHL\Traits;

trait Heatmaps
{
    /**
     * Specific column value getter for the current record row
     * @param string $name The column name.
     * @return mixed The value of the requested column in the current row
     */
    public function __get(string $name): mixed
    {
        // Our processed heatmap zones as an array of %ages.
        if ($name == 'zones_css') {
            $ret = [];
            foreach (str_split($this->zones_raw) as $char) {
                $val = ($char == ' ' ? 0 : ord($char) - 128);
                if (isset($this->cmp_avg) && floatval($this->cmp_avg)) {
                    // Relative to an average.
                    $heat = min(1.0, (($val / 99) - $this->cmp_avg) / $this->cmp_avg); // Keep in a range of -1.0..+1.0.
                    $ret[] = !$val ? '#ffffff' : sprintf(
                        '#%s00%s',
                        str_pad($heat > 0 ? dechex($heat * 255) : '00', 2, '0', STR_PAD_LEFT),
                        str_pad($heat < 0 ? dechex($heat * -255) : '00', 2, '0', STR_PAD_LEFT)
                    );
                } else {
                    // Range of 0.0..1.0.
                    $ret[] = sprintf(
                        '#%s%s%s',
                        $val ? str_pad(dechex(128 + (min(1.0, $val / 30) * 127)), 2, '0', STR_PAD_LEFT) : 'ff',
                        $val ? '00' : 'ff',
                        $val ? '00' : 'ff'
                    );
                }
            }
            return $ret;
        }
        // Fallback to the "standard" getter.
        return parent::__get($name);
    }
}
