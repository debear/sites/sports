<?php

namespace DeBear\Helpers\Sports\Common\Traits\PHPUnit;

use Illuminate\Support\Facades\Config as FrameworkConfig;

trait CustomiseTest
{
    /**
     * Sports-specific handling of date/time management
     * @param string $time The (app) time we will be changing to.
     * @return void
     */
    protected function setTestDateTime(string $time): void
    {
        // Update the timezone configuration to reflect the sport being tested.
        if (!isset($this->config_datetime)) {
            $sport = strtolower(array_slice(explode('\\', get_class($this)), -2, 1)[0]);
            $this->config_datetime = FrameworkConfig::get("debear.sports.subsites.$sport.datetime");
        }
        // Pass processing back to the standard handler.
        parent::setTestDateTime($time);
    }
}
