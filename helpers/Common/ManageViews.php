<?php

namespace DeBear\Helpers\Sports\Common;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class ManageViews
{
    /**
     * The page rendering mode we're operating in
     * @var string
     */
    protected static $mode = 'full';

    /**
     * Page rendering mode getter
     * @return string The current page rendering mode.
     */
    public static function getMode(): string
    {
        return static::$mode;
    }

    /**
     * Determine which Blade template we extend from
     * @return string Path (in Blade format) for the base Blade template
     */
    public static function getExtends(): string
    {
        return FrameworkConfig::get('debear.views.extends.' . static::$mode);
    }

    /**
     * Determine which Blade section we yield from
     * @return string The name of the section our content should be written as
     */
    public static function getSection(): string
    {
        return FrameworkConfig::get('debear.views.section.' . static::$mode);
    }

    /**
     * Update the page rendering mode
     * @param string $mode The new page rendering mode.
     * @return void
     */
    public static function setMode(string $mode): void
    {
        static::$mode = $mode;
    }

    /**
     * Update the 'extends' template for a page rendering mode
     * @param string $mode The appropriate page rendering mode.
     * @param string $path The new page template.
     * @return void
     */
    public static function setExtends(string $mode, string $path): void
    {
        FrameworkConfig::set(["debear.views.extends.$mode" => $path]);
    }

    /**
     * Update the main content sectio for a page rendering mode
     * @param string $mode The appropriate page rendering mode.
     * @param string $name The new page section.
     * @return void
     */
    public static function setSection(string $mode, string $name): void
    {
        FrameworkConfig::set(["debear.views.section.$mode" => $name]);
    }
}
