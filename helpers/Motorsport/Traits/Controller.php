<?php

namespace DeBear\Helpers\Sports\Motorsport\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;

trait Controller
{
    /**
     * Render the section title, prepending the season if viewing something historical
     * @param string $title Standard title to include irrespective of the prefix.
     * @return string Section title for the breadcrumbs, etc
     */
    protected function sectionTitle(string $title): string
    {
        $prefix = '';
        $seasons = FrameworkConfig::get('debear.setup.season');
        if ($seasons['viewing'] != $seasons['default']) {
            $prefix = "{$seasons['viewing']} ";
        }
        return "$prefix$title";
    }
}
