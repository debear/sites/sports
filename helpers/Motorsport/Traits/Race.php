<?php

namespace DeBear\Helpers\Sports\Motorsport\Traits;

trait Race
{
    /**
     * Return the race's country flag, which may need "cleaning" to match the flag sprite
     * @return string The sprite-compatible flag
     */
    public function flag(): string
    {
        return $this->flag;
    }
}
