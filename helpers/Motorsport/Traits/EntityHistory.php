<?php

namespace DeBear\Helpers\Sports\Motorsport\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;

trait EntityHistory
{
    /**
     * The season's row CSS style
     * @param integer $pos Alternative position to process. (Optional).
     * @return string The CSS to use
     */
    public function css(?int $pos = null): string
    {
        // Fallback (/ Default?) is the current position.
        if (!isset($pos)) {
            $pos = $this->pos;
        }
        // Podium place?
        if ($pos < 4) {
            return 'podium-' . $pos;
        }
        // Alternating.
        return 'row_' . ($pos % 2);
    }

    /**
     * A display numeric version of the points total (not in use yet)
     * @return string The formatted total
     * /
    public function displayPts(): string
    {
        return str_replace('.0', '', $this->pts);
    }
    /* */

    /**
     * A display text version of the points total, with pt(s) appended to the string
     * @return string The formatted total
     */
    public function displayPtsFull(): string
    {
        return Format::pluralise($this->pts ?? 0, 'pt');
    }

    /**
     * A display text version of the position, with the ordinal appended to the string
     * @return string The formatted position
     */
    public function displayPos(): string
    {
        $pos = intval($this->pos);
        return $pos ? Format::ordinal(intval($this->pos)) : '&ndash;';
    }

    /**
     * Determine if the season is historical or has more detail available
     * @return boolean That the season being represented is before we track full info
     */
    public function isHistorical(): bool
    {
        return ($this->season < FrameworkConfig::get('debear.setup.season.min'));
    }

    /**
     * The section endpoint that applies to this history record
     * @return string The URL component to use for this history record
     */
    public function sectionEndpoint(): string
    {
        return FrameworkConfig::get('debear.section.endpoint') ?? FrameworkConfig::get('debear.section.code');
    }
}
