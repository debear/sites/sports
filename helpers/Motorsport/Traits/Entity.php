<?php

namespace DeBear\Helpers\Sports\Motorsport\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\HTTP;

trait Entity
{
    /**
     * Render the entity's mugshot/logo with an <img tag
     * @param array $opt Customisation options.
     * @return string Markup to render the entity's mugshot/logo
     */
    public function image(array $opt = []): string
    {
        $opt = static::imageOpt($opt);
        return '<img class="mugshot-' . $opt['size'] . '" loading="lazy" src="' . $this->imageURL($opt) . '"'
            . ' alt="' . $this->name . '">';
    }

    /**
     * URL of the entity's mugshot/logo
     * @param array $opt Customisation options.
     * @return string URL (and only URL, no markup) of the entity's mugshot/logo
     */
    public function imageURL(array $opt = []): string
    {
        $img = static::buildImagePath($this->id, $opt);
        // Return through the obfuscator.
        $size = FrameworkConfig::get("debear.cdn.sizes.{$opt['size']}");
        return HTTP::buildCDNURLs("rs=1&i=$img&w={$size['w']}&h={$size['h']}");
    }

    /**
     * Ensure the passed image options meet our needs (consistently)
     * @param array $opt Customisation options.
     * @return array The parsed image options
     */
    protected static function imageOpt(array $opt = []): array
    {
        return Arrays::merge(
            // Defaults.
            [
                'site' => FrameworkConfig::get('debear.url.sub'),
                'sport' => FrameworkConfig::get('debear.section.endpoint'),
                'size' => 'large',
                'ext' => '.png',
            ],
            // Passed in.
            $opt
        );
    }

    /**
     * Determine the on-disk path of the file to use for this entity's mugshot/logo
     * @param string $file The ID of the entity being loaded, null if a placeholder image is to be rendered.
     * @param array  $opt  Customisation options.
     * @return string The path-on-disk of the appropriate image for the mugshot/logo to use for this entity
     */
    public static function buildImagePath(?string $file, array $opt): string
    {
        $opt = static::imageOpt($opt);
        $img = '/' . join('/', [
            $opt['site'],
            $opt['sport'],
            $opt['season'] ?? FrameworkConfig::get('debear.setup.season.viewing'),
            /* @phan-suppress-next-line PhanUndeclaredStaticProperty */
            static::$image_dir,
            ($file ?? -1) . $opt['ext'],
        ]);
        // Determine if this file exists on disk, and if not use a generic fallback.
        $fallback = "/{$opt['site']}/{$opt['sport']}/no_participant{$opt['ext']}";
        return file_exists(resource_path(FrameworkConfig::get('debear.dirs.images')) . $img) ? $img : $fallback;
    }

    /**
     * Build and return the list of data attributes we add to the participant link
     * @return string Appropriate HTML data attributes
     */
    protected function hoverAttributes(): string
    {
        $standing = $this->standingsCurr;
        $isset = isset($standing) && $standing->isset();
        return join(' ', [
            'data-hover="true"',
            'data-hover-mugshot="' . $this->imageURL(['size' => 'thumb']) . '"',
            /* @phan-suppress-next-line PhanNonClassMethodCall */
            'data-hover-champ-pts="' . ($isset ? $standing->displayPtsFull() : '0pts') . '"',
            /* @phan-suppress-next-line PhanNonClassMethodCall */
            'data-hover-champ-pos="' . ($isset ? $standing->displayPos() : 'n/a') . '"',
            'data-hover-wins="' . ($isset ? $standing->num_wins : 0) . '"',
            'data-hover-podiums="' . ($isset ? $standing->num_podiums : 0) . '"',
        ]);
    }

    /**
     * Any additional info relevant to the entity that season
     * @return string The extra info
     */
    public function extraInfo(): string
    {
        return '';
    }

    /**
     * The raw position of this entity in the championship
     * @return integer Position of the entity
     */
    public function pos(): int
    {
        return $this->standingsCurr->pos;
    }

    /**
     * The formatted position of this entity in the championship
     * @return string Position of the entity
     */
    public function displayPos(): string
    {
        return $this->standingsCurr->displayPos();
    }

    /**
     * The raw number of points this entity has recorded
     * @return integer Number of points scored by the entity
     */
    public function points(): int
    {
        return $this->standingsCurr->pts;
    }

    /**
     * The formatted number of points this entity has recorded
     * @return string Points scored by the entity
     */
    public function displayPtsFull(): string
    {
        return $this->standingsCurr->displayPtsFull();
    }
}
