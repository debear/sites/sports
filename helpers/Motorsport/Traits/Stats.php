<?php

namespace DeBear\Helpers\Sports\Motorsport\Traits;

use DeBear\Helpers\Format;

trait Stats
{
    /**
     * Render the value according to the rules passed in
     * @param boolean $with_extra Whether we should include the 'extra' value, if set. (Optional, Default: true).
     * @return string The value rendered for display
     */
    public function rendered(bool $with_extra = true): string
    {
        $val = ($this->value ?? '');
        if ($this->isAbsolute()) {
            $val = (int)$val;
        } elseif ($this->isPercentage()) {
            $val = "$val%";
        } elseif ($this->isDelta()) {
            $val = ($val > 0 ? '+' : '') . $val;
        }
        // Extra info?
        if ($with_extra && isset($this->extra)) {
            $val .= "<br/>{$this->extra}";
        }
        return $val;
    }

    /**
     * Decide whether the current value is rendered or not considered appropriate.
     * @return boolean Whether we consider the stat value valid or not
     */
    public function renderable(): bool
    {
        // Delta values are always returned as such, as are non-zero values.
        return (isset($this->value) && ($this->isDelta() || (float)$this->value));
    }

    /**
     * Render the value for a tabular environment according to the rules passed in
     * @return string The value rendered for a tabular display
     */
    public function renderedTabular(): string
    {
        // Render, or replace zero with a dash.
        return $this->renderable() ? $this->rendered(false) : '&ndash;';
    }

    /**
     * Get the meta info for this stat instance
     * @return Stats The meta info for the participant's stat
     */
    protected function getStatDetails() /*: Stats */
    {
        // We may have the info bound in the current object, so use that if the 'stat' object hasn't been bound.
        return $this->stat ?? $this;
    }

    /**
     * State if the stat renders an absolute value
     * @return boolean That the value is absolute
     */
    public function isAbsolute(): bool
    {
        return $this->getStatDetails()->type == 'abs';
    }

    /**
     * State if the stat renders an average value (not in use yet)
     * @return boolean That the value is an average
     * /
    public function isAverage(): bool
    {
        return $this->getStatDetails()->type == 'avg';
    }
    /* */

    /**
     * State if the stat renders a percentage value
     * @return boolean That the value is a percentage
     */
    public function isPercentage(): bool
    {
        return $this->getStatDetails()->type == 'pct';
    }

    /**
     * State if the stat renders a change in value
     * @return boolean That the value is a change
     */
    public function isDelta(): bool
    {
        return $this->getStatDetails()->type == 'delta';
    }

    /**
     * The value's CSS style
     * @return string The CSS to use
     */
    public function valueCSS(): string
    {
        // Delta values are coloured according to their change.
        return $this->isDelta() ? 'stats-delta-' . ($this->value > 0 ? 'pos' : ($this->value < 0 ? 'neg' : 'nc')) : '';
    }

    /**
     * The position value's CSS style
     * @param integer $row Row we are being rendered in.
     * @return string The CSS to use
     */
    public function posCSS(int $row): string
    {
        // Podium place or alternating?
        return $this->pos < 4 ? 'podium-' . $this->pos : 'row_' . ($row % 2);
    }

    /**
     * A display version of the standings position
     * @return string The formatted position
     */
    public function displayPos(): string
    {
        return ($this->pos_tied ? 'T-' : '') . Format::ordinal($this->pos);
    }
}
