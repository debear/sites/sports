<?php

namespace DeBear\Helpers\Sports\Motorsport;

use Illuminate\Support\Facades\Config as FrameworkConfig;

class Setup
{
    /**
     * Determine the number of riders who qualify for the following season's series
     * @return integer The number of qualifying riders
     */
    public static function standingsSeparator(): int
    {
        $sep = FrameworkConfig::get('debear.setup.standings.separator');
        $list = is_array($sep) ? $sep : [];
        // Parse the config.
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        foreach ($list as $config) {
            if (($config['from'] <= $season) && ($season <= $config['to'])) {
                return $config['num'];
            }
        }
        // Default is to return none.
        return 0;
    }
}
