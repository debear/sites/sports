<?php

namespace DeBear\Helpers\Sports\Motorsport;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;

class Controller
{
    /**
     * Common request preparation steps
     * @param array $opt Custom options to pass the header setup method. (Optional).
     * @return void
     */
    public static function prepareRequest(array $opt = []): void
    {
        static::setupSeason($opt);
        Header::setup($opt);
    }

    /**
     * Setup the season we are considered as viewing
     * @param array $opt Custom options passed that may include a season from the URI. (Optional).
     * @return void
     */
    public static function setupSeason(array $opt = []): void
    {
        // Skip if already run.
        if (FrameworkConfig::get('debear.setup.season.viewing') !== null) {
            return;
        }

        // Validate the passed season (which could from a variety of sources).
        if (isset($opt['season']) && is_numeric($opt['season'])) {
            $season = $opt['season'];
        } elseif (Request::has('header') && Request::input('header') && is_numeric(Request::input('header'))) {
            $season = Request::input('header');
        } else {
            $season = false;
        }
        $seasons = FrameworkConfig::get('debear.setup.season');
        if (!$season || ($season < $seasons['min']) || ($season > $seasons['max'])) {
            $season = $seasons['default'];
        }

        // Setup the header calendar.
        FrameworkConfig::set(['debear.setup.season.viewing' => $season]);
    }

    /**
     * Render the section title, prepending the season if viewing something historical
     * @param string $title Default title to be used.
     * @return string Section title for the breadcrumbs, etc
     */
    public static function prependSeason(string $title): string
    {
        $prefix = '';
        $seasons = FrameworkConfig::get('debear.setup.season');
        if ($seasons['viewing'] != $seasons['default']) {
            $prefix = "{$seasons['viewing']} ";
        }
        return "$prefix$title";
    }
}
