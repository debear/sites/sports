<?php

namespace DeBear\Helpers\Sports\Motorsport;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Motorsport\Header;

class SiteTools
{
    /**
     * Expand the sitemap to include dynamic properties
     * @return void
     */
    public static function expandSitemap(): void
    {
        $links = FrameworkConfig::get('debear.links');
        $seasons = FrameworkConfig::get('debear.setup.season');
        // Unlink the subsites from the sitemap.
        foreach (array_keys($links) as $code) {
            if (substr($code, 0, 8) == 'subsite-') {
                $links[$code]['sitemap']['enabled'] = false;
            }
        }
        // Races/Meetings (by item).
        foreach (Header::getCalendar() as $race) {
            $links["races-{$race->season}-{$race->round}"] = [
                'url' => $race->canLink() ? $race->link() : false,
                'label' => $race->nameFlag(),
                'section' => 'races',
                'order' => $race->round_order / 100,
                'sitemap' => [
                    'enabled' => true,
                ],
            ];
        }
        // Standings and Stats (by season).
        for ($s = $seasons['max']; $s >= $seasons['min']; $s--) {
            if ($s == $seasons['viewing']) {
                continue; // Skip the "current" season.
            }
            $current = ($s == $seasons['default']);
            $links["standings-$s"] = [
                'url' => $links['standings']['url'] . (!$current ? "/$s" : ''),
                'label' => "$s {$links['standings']['label']}",
                'section' => 'standings',
                'order' => 3000 - $s,
                'url-section' => isset($links['standings']) && $links['standings'],
                'sitemap' => [
                    'enabled' => true,
                ],
            ];
            $links["stats-$s"] = [
                'url' => $links['stats']['url'] . (!$current ? "/$s" : ''),
                'label' => "$s {$links['stats']['label']}",
                'section' => 'stats',
                'order' => 3000 - $s,
                'url-section' => isset($links['stats']) && $links['stats'],
                'sitemap' => [
                    'enabled' => true,
                ],
            ];
        }
        // Teams (by individual - if enabled).
        $inc_teams = FrameworkConfig::get('debear.setup.teams.history');
        if ($inc_teams) {
            $links['participants-teams'] = [
                'url' => false,
                'label' => 'Teams',
                'section' => 'participants',
                'order' => 100,
                'sitemap' => [
                    'enabled' => true,
                ],
            ];
            foreach (Namespaces::callStatic('TeamHistory', 'standingsBySeason') as $t) {
                $links["participants-teams-{$t->id}"] = [
                    'url' => $t->link(),
                    'label' => $t->nameFlag(),
                    'section' => 'participants-teams',
                    'order' => $t->pos(),
                    'sitemap' => [
                        'enabled' => true,
                    ],
                ];
            }
        }
        // Drivers (by individual - if we're including a teams list, then we need to group into a new sub-level).
        if ($inc_teams) {
            $links['participants-ind'] = [
                'url' => false,
                'label' => ucfirst(FrameworkConfig::get('debear.setup.participants.url')),
                'section' => 'participants',
                'order' => 200,
                'sitemap' => [
                    'enabled' => true,
                ],
            ];
        }
        foreach (Namespaces::callStatic('ParticipantHistory', 'standingsBySeason') as $p) {
            $links["participants-ind-{$p->id}"] = [
                'url' => $p->link(),
                'label' => $p->nameFlag(),
                'section' => 'participants' . ($inc_teams ? '-ind' : ''),
                'order' => $p->pos(),
                'sitemap' => [
                    'enabled' => true,
                ],
            ];
        }
        // Write the result back.
        FrameworkConfig::set(['debear.links' => $links]);
    }

    /**
     * Expand the XML version of the sitemap to include dynamic properties
     * @return void
     */
    public static function expandSitemapXML(): void
    {
        $links = FrameworkConfig::get('debear.links');
        $seasons = FrameworkConfig::get('debear.setup.season');
        $archive_factor = 0.4;
        // Unlink the subsites from the sitemap.
        foreach (array_keys($links) as $code) {
            if (substr($code, 0, 8) == 'subsite-') {
                $links[$code]['sitemap']['enabled'] = false;
            }
        }
        // Races/Meetings (by item).
        foreach (Namespaces::callStatic('Race', 'getAllCompleted') as $race) {
            $race_factor = ($race->season == $seasons['default'] ? 1 : $archive_factor);
            $link_priority = floatval($links['races']['sitemap']['priority']);
            $links["races-{$race->season}-{$race->round}"] = [
                'url' => $race->link(),
                'section' => 'races',
                'order' => $race->season + ($race->round_order / 100),
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $link_priority * $race_factor),
                    'frequency' => 'never',
                ],
            ];
        }
        // Standings and Stats (by season).
        for ($s = $seasons['min']; $s <= $seasons['max']; $s++) {
            if ($s == $seasons['default']) {
                continue; // Skip the "current" season.
            }
            $link_priority = floatval($links['standings']['sitemap']['priority']);
            $links["standings-$s"] = [
                'url' => "{$links['standings']['url']}/$s",
                'section' => 'standings',
                'order' => 3000 - $s,
                'url-section' => isset($links['standings']) && $links['standings'],
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $link_priority * $archive_factor),
                    'frequency' => 'never',
                ],
            ];
            $link_priority = floatval($links['stats']['sitemap']['priority']);
            $links["stats-$s"] = [
                'url' => "{$links['stats']['url']}/$s",
                'section' => 'stats',
                'order' => 3000 - $s,
                'url-section' => isset($links['stats']) && $links['stats'],
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $link_priority * $archive_factor),
                    'frequency' => 'never',
                ],
            ];
        }
        // Teams (by individual - if enabled).
        $inc_teams = FrameworkConfig::get('debear.setup.teams.history');
        if ($inc_teams) {
            foreach (Namespaces::callStatic('Team', 'getAllHistorical') as $i => $team) {
                $is_current = ($team->season == $seasons['default']);
                $team_factor = ($is_current ? 1 : $archive_factor);
                $link_priority = floatval($links['participants']['sitemap']['priority']);
                $links["participants-teams-{$team->season}-$i"] = [
                    'url' => $team->link(),
                    'section' => 'participants',
                    'order' => 10000 + ($team->season + ($i / 100)),
                    'sitemap' => [
                        'enabled' => true,
                        'priority' => sprintf('%.01f', $link_priority * $team_factor),
                        'frequency' => ($is_current ? 'weekly' : 'yearly'),
                    ],
                ];
            }
        }
        // Drivers (by individual).
        foreach (Namespaces::callStatic('Participant', 'getAllHistorical') as $i => $participant) {
            $is_current = ($participant->max_season == $seasons['default']);
            $participant_factor = ($is_current ? 1 : $archive_factor);
            $link_priority = floatval($links['participants']['sitemap']['priority']);
            $links["participants-ind-$i"] = [
                'url' => $participant->link(),
                'section' => 'participants',
                'order' => 20000 + $i,
                'sitemap' => [
                    'enabled' => true,
                    'priority' => sprintf('%.01f', $link_priority * $participant_factor),
                    'frequency' => ($is_current ? 'weekly' : 'monthly'),
                ],
            ];
        }
        // Write the result back.
        FrameworkConfig::set(['debear.links' => $links]);
    }
}
