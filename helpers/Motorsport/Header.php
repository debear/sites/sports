<?php

namespace DeBear\Helpers\Sports\Motorsport;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\Motorsport\Race;
use DeBear\Helpers\Blade;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;

class Header
{
    /**
     * Local flag to capture having parsed and processed the navigation options
     * @var boolean
     */
    protected static $updated_nav = false;
    /**
     * The list of races that occur(ed) in the season being viewed
     * @var ?Race
     */
    protected static $calendar = null;
    /**
     * Round ID of the race we wish to 'highlight' in the navigation
     * @var integer
     */
    protected static $focus = 0;
    /**
     * Local cache of the number of races being raced in the season being viewed
     * @var ?integer
     */
    protected static $num_races = null;

    /**
     * Prepare our run
     * @param array $opt Custom options to use in the setup process.
     * @return void
     */
    public static function setup(array $opt = []): void
    {
        // Get the data.
        $seasons = FrameworkConfig::get('debear.setup.season');
        $season = $seasons['viewing'] ?? $seasons['default'];
        $with = (isset($opt['with']) && is_array($opt['with']) ? $opt['with'] : []);
        static::$calendar = Namespaces::callStatic('Race', 'loadCalendar', [$season, $with]);

        // And now load the appropriate header flags CSS (based on the number of races).
        Resources::addCSS('motorsport/header/calendar.css');
        Resources::addCSS('motorsport/header/flags/' . static::getNumRaces() . '.css');

        // Finally, if the season we are viewing isn't the default, we need to add it to the end of the URLs.
        if (!static::$updated_nav && isset($seasons['viewing']) && ($seasons['viewing'] != $seasons['default'])) {
            $nav = FrameworkConfig::get('debear.links');
            $extra = (string) $seasons['viewing'];
            foreach ($nav as &$opt) {
                if (
                    (isset($opt['url']) && is_string($opt['url']) && $opt['url'])
                    && isset($opt['url-season'])
                ) {
                    // As query string or appended?
                    if ($opt['url-season'] == 'query') {
                        // Passed as a query argument.
                        if (!isset($opt['query']) || !is_array($opt['query'])) {
                            $opt['query'] = [];
                        }
                        $opt['query']['header'] = $extra;
                    } elseif ($opt['url-season'] == 'append') {
                        // Appended to the end of the (base) URL.
                        $opt['url-extra'] = $extra;
                    }
                }
            }
            FrameworkConfig::set(['debear.links' => $nav]);
            static::$updated_nav = true;
        }
    }

    /**
     * Get the Race object, which we will load if we haven't already requested it
     * @return ?Race List of races for the given season
     */
    public static function getCalendar(): ?Race
    {
        return static::$calendar;
    }

    /**
     * Render the calendar header we include on each page
     * @return string The calendar as a rendered Blade template
     */
    public static function calendar(): string
    {
        if (!isset(static::$calendar)) {
            static::setup();
        }
        return Blade::render('sports.motorsport.widgets.calendar.list', [
            'races' => static::$calendar,
        ]);
    }

    /**
     * Determine the number of races there are in this list of rounds
     * @return integer Number of unique races (rounds plus race 2's)
     */
    public static function getNumRaces(): int
    {
        if (isset(static::$num_races)) {
            // Phan doesn't seem to like this because it's a static variable.
            /* @phan-suppress-next-line PhanTypeMismatchReturnNullable */
            return static::$num_races;
        }

        static::$num_races = static::$calendar->count();
        foreach (static::$calendar as $race) {
            static::$num_races += intval(isset($race->race2_time) && !$race->quirk('sprint_race'));
        }
        return static::$num_races;
    }

    /**
     * Determine the race round of the race to focus on within the header
     * @return integer Round of the race to focus on within the header (where zero means not applicable)
     */
    public static function getRaceFocus(): int
    {
        return static::$focus;
    }

    /**
     * Get the race round of the race we would be focusing on on the header nav
     * @param integer $round The round of the race we should focus on within the header.
     * @return void
     */
    public static function setRaceFocus(int $round): void
    {
        static::$focus = $round;
    }

    /**
     * Reset the internals between unit tests runs
     * @return void
     */
    public static function reset(): void
    {
        if (HTTP::isTest()) {
            static::$updated_nav = false;
            static::$calendar = null;
            static::$focus = 0;
            static::$num_races = null;
        }
    }
}
