<?php

namespace DeBear\Helpers\Sports\MLB\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use DeBear\ORM\Sports\MajorLeague\MLB\Player;
use DeBear\ORM\Sports\MajorLeague\MLB\TeamRoster;

trait StatsBvP
{
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'seasons' => ['s' => 'Yrs', 'f' => 'Seasons'],
        'gp' => ['s' => 'GP', 'f' => 'Games Played'],
        'pa' => ['s' => 'PA', 'f' => 'Plate Appearances'],
        'ab' => ['s' => 'AB', 'f' => 'At Bats'],
        'h' => ['s' => 'H', 'f' => 'Hits'],
        '1b' => ['s' => '1B', 'f' => 'Singles'],
        '2b' => ['s' => '2B', 'f' => 'Doubles'],
        '3b' => ['s' => '3B', 'f' => 'Triples'],
        'hr' => ['s' => 'HR', 'f' => 'Home Runs'],
        'rbi' => ['s' => 'RBI', 'f' => 'Runs Batted In'],
        'bb' => ['s' => 'BB', 'f' => 'Base on Balls'],
        'ibb' => ['s' => 'IBB', 'f' => 'Intentional Walks'],
        'k' => ['s' => 'K', 'f' => 'Strikeouts'],
        'tb' => ['s' => 'TB', 'f' => 'Total Bases'],
        'hbp' => ['s' => 'HBP', 'f' => 'Hit by Pitch'],
        'avg' => ['s' => 'Avg', 'f' => 'Batting Average'],
        'obp' => ['s' => 'OBP', 'f' => 'On Base %age'],
        'slg' => ['s' => 'SLG', 'f' => 'Slugging %age'],
        'ops' => ['s' => 'OPS', 'f' => 'On Base Plus Slugging'],
        'winprob' => ['s' => 'WPA', 'f' => 'Win Probability Added'],
    ];

    /**
     * Return the list of split stat column meta info for this class
     * @return array The column meta info
     */
    public function getBatterVsPitcherStatMeta(): array
    {
        return $this->getStatMeta('bvp');
    }

    /**
     * Return the list of split stat columns available in this class
     * @return array The columns as a list, or null if not applicalble
     * /
    public function getBatterVsPitcherStatColumns(): ?array
    {
        return $this->getStatColumns('bvp');
    }
    /* */

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (in_array($column, ['avg', 'obp', 'slg', 'ops'])) {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        }
        // Fallback value: number as-is.
        return $this->$column;
    }

    /**
     * Load detailed player information for any history
     * @return void
     */
    public function loadSecondaryPlayers(): void
    {
        $players = Player::query()->whereIn('player_id', $this->unique($this->opp_column))->get();
        foreach ($this->data as $i => $player) {
            $this->data[$i]['player'] = $players->where('player_id', $player[$player['opp_column']]);
        }
    }

    /**
     * Load detailed team information for any history
     * @return void
     */
    public function loadSecondaryBvPTeams(): void
    {
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $is_career = ($season === '0000');
        // Determine appropriate dates for each player.
        $query = TeamRoster::query()
            ->select('player_id')
            ->selectRaw('MAX(the_date) AS max_date');
        if (!$is_career) {
            $query->where('season', $season);
        }
        $max_dates = $query->whereIn('player_id', $this->unique($this->opp_column))
            ->groupBy('player_id')
            ->get();
        // Then the roster info for each player.
        $query = TeamRoster::query();
        foreach ($max_dates as $row) {
            $query->orWhere(function (Builder $where) use ($row) {
                $where->where([
                    ['season', '=', substr($row->max_date, 0, 4)], // Cheating, knowing seasons are calendar.
                    ['the_date', '=', $row->max_date],
                    ['player_id', '=', $row->player_id],
                ]);
            });
        }
        $rosters = $query->orWhere(DB::raw(1), DB::raw(0))->get()->loadSecondary(['teams']);
        // Now tie-together.
        foreach ($this->data as $i => $player) {
            $this->data[$i]['team'] = $rosters->where('player_id', $player[$player['opp_column']])->team;
            $this->data[$i]['team_id'] = $this->data[$i]['team']->team_id;
        }
    }
}
