<?php

namespace DeBear\Helpers\Sports\MLB\Traits;

trait StatsFielding
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['is_totals', '=', '1'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'tc' => ['s' => 'TC', 'f' => 'Total Chances'],
        'po' => ['s' => 'PO', 'f' => 'Putouts'],
        'a' => ['s' => 'A', 'f' => 'Assists'],
        'e' => ['s' => 'E', 'f' => 'Errors'],
        'pct' => ['s' => 'Pct', 'f' => 'Fielding Percentage'],
        'dp' => ['s' => 'DP', 'f' => 'Double Plays'],
    ];
    /**
     * The split stat columns
     * @var array
     */
    protected $splitsTypes = [
        'home-road' => 'Home / Road',
        'month' => 'Month',
        'asb' => 'All-Star Break',
        'opponent' => 'Opponent',
        'stadium' => 'Stadium',
    ];
    /**
     * The situational stat columns (currently none)
     * @var array
     */
    protected $situationalTypes = [];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'e',
        'pct',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'pct') {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
