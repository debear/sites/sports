<?php

namespace DeBear\Helpers\Sports\MLB\Traits;

trait Heatzones
{
    /**
     * Specific column value getter for the current record row
     * @param string $name The column name.
     * @return mixed The value of the requested column in the current row
     */
    public function __get(string $name): mixed
    {
        // Heatzone-specific fields.
        if (preg_match('/^heatzone_[io][tmb][lcr]_(avg|colour)$/', $name)) {
            // Zone batting average / colour.
            list($key, $col) = explode('_', substr($name, 9));
            $num = json_decode("[{$this->heatzones}]");
            $map_fields = ['itl', 'itc', 'itr', 'iml', 'imc', 'imr', 'ibl', 'ibc', 'ibr', 'otl', 'otr', 'obl', 'obr'];
            $map = array_flip($map_fields);
            list($ab, $h) = array_slice($num[$map[$key]], 1, 2);
            $avg = ($ab ? $h / $ab : null);
            if ($col == 'avg') {
                // Average.
                return isset($avg) ? ltrim(sprintf('%.03f', $avg), '0') : '&ndash;';
            } else {
                // Heat colour.
                if (!isset($avg) || !isset($this->cmp_avg) || !floatval($this->cmp_avg)) {
                    // No attempts in this zone, so neutral colour.
                    return '#888888';
                }
                $heat = min(1.0, ($avg - $this->cmp_avg) / $this->cmp_avg); // Keep in a range of -1.0..+1.0.
                return sprintf(
                    '#%s00%s',
                    str_pad($heat > 0 ? dechex($heat * 255) : '00', 2, '0', STR_PAD_LEFT),
                    str_pad($heat < 0 ? dechex($heat * -255) : '00', 2, '0', STR_PAD_LEFT)
                );
            }
        } elseif (preg_match('/^spray_(l|lc|c|cr|r)_(pct|colour)$/', $name)) {
            list($key, $col) = explode('_', substr($name, 6));
            $num = explode(',', $this->spray_chart);
            array_shift($num); // Ignore the first element in this instance, which relates to foulouts.
            $map = array_flip(['l', 'lc', 'c', 'cr', 'r']);
            $num[$map[$key]] = intval($num[$map[$key]]); // Type conversion before arithmetic.
            $tot = array_sum($num);
            if ($col == 'pct') {
                return ($tot ? sprintf('%0.01f%%', 100 * ($num[$map[$key]] / $tot)) : '&ndash;');
            } else {
                // Calculated as distance from the mean.
                $avg = ($tot / 5);
                $diff = ($tot ? min(1.0, ($num[$map[$key]] - $avg) / $avg) : null); // Keep in a range of -1.0..+1.0.
                return (!isset($diff) ? '#888888' : sprintf(
                    '#%s0000',
                    str_pad(dechex(128 + ($diff * 127)), 2, '0', STR_PAD_LEFT)
                ));
            }
        }
        // Fallback to the "standard" getter.
        return parent::__get($name);
    }
}
