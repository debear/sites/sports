<?php

namespace DeBear\Helpers\Sports\MLB\Traits;

use DeBear\Helpers\Format;

trait StatsBatting
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['is_totals', '=', '1'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'gp' => ['s' => 'GP', 'f' => 'Games Played', 'game' => false, 'sortable' => false],
        'gs' => [
            's' => 'GS', 'f' => 'Games Started',
            'game' => false, 'sortable' => false, 'splits' => false, 'situational' => false,
        ],
        'spot' => [
            's' => 'Spot', 'f' => 'Batting Order',
            'season' => false, 'sortable' => false, 'splits' => false, 'situational' => false,
        ],
        'pa' => ['s' => 'PA', 'f' => 'Plate Appearances'],
        'ab' => ['s' => 'AB', 'f' => 'At Bats'],
        'r' => ['s' => 'R', 'f' => 'Runs', 'situational' => false],
        'h' => ['s' => 'H', 'f' => 'Hits'],
        'hr' => ['s' => 'HR', 'f' => 'Home Runs'],
        'rbi' => ['s' => 'RBI', 'f' => 'Runs Batted In'],
        'bb' => ['s' => 'BB', 'f' => 'Base on Balls'],
        'ibb' => ['s' => 'IBB', 'f' => 'Intentional Walks'],
        'k' => ['s' => 'K', 'f' => 'Strikeouts'],
        'hbp' => ['s' => 'HBP', 'f' => 'Hit by Pitch'],
        'sb' => ['s' => 'SB', 'f' => 'Stolen Bases', 'situational' => false],
        'cs' => ['s' => 'CS', 'f' => 'Caught Stealing', 'situational' => false],
        'avg' => ['s' => 'Avg', 'f' => 'Batting Average'],
        'obp' => ['s' => 'OBP', 'f' => 'On Base %age'],
        'slg' => ['s' => 'SLG', 'f' => 'Slugging %age'],
        'ops' => ['s' => 'OPS', 'f' => 'On Base Plus Slugging'],
        '1b' => ['s' => '1B', 'f' => 'Singles'],
        '2b' => ['s' => '2B', 'f' => 'Doubles'],
        '3b' => ['s' => '3B', 'f' => 'Triples'],
        'xbh' => ['s' => 'XBH', 'f' => 'Extra-Base Hits', 'splits' => false, 'situational' => false],
        'tb' => ['s' => 'TB', 'f' => 'Total Bases'],
        'po' => ['s' => 'PO', 'f' => 'Pick Offs', 'splits' => false, 'situational' => false],
        'sac_fly' => ['s' => 'SacFly', 'f' => 'Sacrifice Flys', 'situational' => false],
        'sac_hit' => ['s' => 'SacHit', 'f' => 'Sacrifice Hits', 'situational' => false],
        'go' => ['s' => 'GO', 'f' => 'Groundouts', 'splits' => false, 'situational' => false],
        'fo' => ['s' => 'FO', 'f' => 'Flyouts', 'splits' => false, 'situational' => false],
        'gidp' => ['s' => 'GIDP', 'f' => 'Grounded into Double Plays', 'situational' => false],
        'lob' => ['s' => 'LOB', 'f' => 'Left on Base', 'situational' => false],
        'winprob' => ['s' => 'WPA', 'f' => 'Win Probability Added', 'situational' => false],
    ];
    /**
     * The split stat columns
     * @var array
     */
    protected $splitsTypes = [
        'home-road' => 'Home / Road',
        'month' => 'Month',
        'asb' => 'All-Star Break',
        'opponent' => 'Opponent',
        'stadium' => 'Stadium',
    ];
    /**
     * The situational stat columns
     * @var array
     */
    protected $situationalTypes = [
        'batter-info' => 'Batter Role',
        'pitcher-info' => 'Pitcher Type',
        'with-baserunners' => 'Baserunners',
        'counts-on' => 'On Count',
        'counts-after' => 'After Count',
        'inning' => 'Inning',
        'batting-order' => 'Batting Order',
        'fielding-pos' => 'Position',
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'k',
        'cs',
        'po',
        'gidp',
        'lob',
        'avg',
        'obp',
        'slg',
        'ops',
        'winprob',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (in_array($column, ['avg', 'obp', 'slg', 'ops'])) {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        } elseif ($column == 'spot') {
            $starter = substr($this->order, -2) == '00';
            $title = ($starter
                ? 'Started, Hit ' . Format::ordinal($this->$column)
                : 'Substitute, Used ' . Format::ordinal($this->$column) . ' in the order');
            $class = ($starter ? 'starter' : 'substitute');
            return '<span class="fa-stack fa-1x spot-' . $class . '" title="' . $title . '">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x">' . $this->$column . '</strong>
</span>';
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
