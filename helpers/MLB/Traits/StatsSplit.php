<?php

namespace DeBear\Helpers\Sports\MLB\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use DeBear\Helpers\Sports\Namespaces;

trait StatsSplit
{
    /**
     * Return the list of split stat types for this class
     * @return array The stat type info
     */
    public function getSplitStatTypes(): array
    {
        return $this->splitsTypes;
    }

    /**
     * Return the list of situational stat types for this class
     * @return array The stat type info
     */
    public function getSituationalStatTypes(): array
    {
        return $this->situationalTypes;
    }

    /**
     * Return the list of split stat column meta info for this class
     * @return array The column meta info
     */
    public function getSplitsStatMeta(): array
    {
        return $this->getStatMeta('splits');
    }

    /**
     * Return the list of situational stat column meta info for this class
     * @return array The column meta info
     */
    public function getSituationalStatMeta(): array
    {
        return $this->getStatMeta('situational');
    }

    /**
     * Return the list of split stat columns available in this class
     * @return array The columns as a list, or null if not applicalble
     * /
    public function getSplitsStatColumns(): ?array
    {
        return $this->getStatColumns('splits');
    }
    /* */

    /**
     * Return the list of situational stat columns available in this class
     * @return array The columns as a list, or null if not applicalble
     * /
    public function getSituationalStatColumns(): ?array
    {
        return $this->getStatColumns('situational');
    }
    /* */

    /**
     * Load detail team information for any loaded opponent splits
     * @return void
     */
    public function loadSecondaryTeams(): void
    {
        $team_ids = $this->where('split_type', 'opponent')->unique('split_label');
        if (count($team_ids)) {
            $teams = Namespaces::callStatic('Team', 'query')->whereIn('team_id', $team_ids)->get();
            // Now tie-together.
            foreach ($this->data as $i => $split) {
                if ($split['split_type'] == 'opponent') {
                    $this->data[$i]['team'] = $teams->where('team_id', $split['split_label']);
                }
            }
        }
    }

    /**
     * Load detail stadia information for any loaded venue splits
     * @return void
     */
    public function loadSecondaryStadia(): void
    {
        $stadia = $this->where('split_type', 'stadium')->unique('split_label');
        if (count($stadia)) {
            // Determine the season in question for labels.
            $config = FrameworkConfig::get('debear.setup.season');
            $season = ($config['viewing'] == '0000' ? $config['max'] : $config['viewing']);
            // Get the list.
            $venues = Namespaces::callStatic('TeamVenue', 'query')
                ->select('*')
                ->selectRaw('CONCAT(team_id, ":", building_id) AS building_ref')
                ->whereIn(DB::raw('CONCAT(team_id, ":", building_id)'), $stadia)
                ->where([
                    ['season_from', '<=', $season],
                    [DB::raw('IFNULL(season_to, 2099)'), '>=', $season],
                ])->get();
            // Now tie-together.
            foreach ($this->data as $i => $split) {
                if ($split['split_type'] == 'stadium') {
                    $this->data[$i]['venue'] = $venues->where('building_ref', $split['split_label']);
                }
            }
        }
    }
}
