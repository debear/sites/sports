<?php

namespace DeBear\Helpers\Sports\MLB\Traits;

trait StatsPitching
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['is_totals', '=', '1'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'gp' => ['s' => 'GP', 'f' => 'Games Played', 'game' => false, 'sortable' => false],
        'gs' => ['s' => 'GS', 'f' => 'Games Started', 'game' => false, 'sortable' => false],
        'decision' => [
            's' => 'Dec', 'f' => 'Game Decision',
            'season' => false, 'sortable' => false, 'splits' => false, 'situational' => false,
        ],
        'w' => ['s' => 'W', 'f' => 'Wins', 'game' => false, 'situational' => false],
        'l' => ['s' => 'L', 'f' => 'Losses', 'game' => false, 'situational' => false],
        'sv' => ['s' => 'SV', 'f' => 'Saves', 'game' => false, 'situational' => false],
        'hld' => ['s' => 'HLD', 'f' => 'Holds', 'game' => false, 'situational' => false],
        'bs' => ['s' => 'BS', 'f' => 'Blown Saves', 'game' => false, 'situational' => false],
        'era' => ['s' => 'ERA', 'f' => 'Earned Run Average', 'situational' => false],
        'whip' => ['s' => 'WHIP', 'f' => 'Walks + Hits per IP', 'situational' => false],
        'ip' => ['s' => 'IP', 'f' => 'Innings Pitched', 'situational' => false],
        'k' => ['s' => 'K', 'f' => 'Strikeouts'],
        'h' => ['s' => 'H', 'f' => 'Hits Allowed'],
        'hr' => ['s' => 'HR', 'f' => 'Home Runs Allowed'],
        'bb' => ['s' => 'BB', 'f' => 'Base on Balls'],
        'ibb' => ['s' => 'IBB', 'f' => 'Intentional Walks'],
        'hbp' => ['s' => 'HBP', 'f' => 'Hit by Pitch', 'season' => false, 'game' => false, 'sortable' => false],
        'avg_agst' => [
            's' => 'AVG', 'f' => 'Batting Average Allowed',
            'season' => false, 'game' => false, 'sortable' => false,
        ],
        'obp_agst' => [
            's' => 'OBP', 'f' => 'On Base %age Allowed',
            'season' => false, 'game' => false, 'sortable' => false,
        ],
        'slg_agst' => [
            's' => 'SLG', 'f' => 'Slugging %age Allowed',
            'season' => false, 'game' => false, 'sortable' => false,
        ],
        'ops_agst' => [
            's' => 'OPS', 'f' => 'On Base Plus Slugging Allowed',
            'season' => false, 'game' => false, 'sortable' => false,
        ],
        'wp' => ['s' => 'WP', 'f' => 'Wild Pitches', 'splits' => false, 'situational' => false],
        'bk' => ['s' => 'BK', 'f' => 'Balks', 'situational' => false],
        'sb' => ['s' => 'SB', 'f' => 'Stolen Bases', 'situational' => false],
        'cs' => ['s' => 'CS', 'f' => 'Caught Stealing', 'situational' => false],
        'cg' => ['s' => 'CG', 'f' => 'Complete Games', 'situational' => false],
        'sho' => ['s' => 'SHO', 'f' => 'Shutouts', 'situational' => false],
        'qs' => ['s' => 'QS', 'f' => 'Quality Starts', 'situational' => false],
        'svo' => ['s' => 'SVO', 'f' => 'Save Opportunities', 'game' => false, 'situational' => false],
        'game_score' => ['s' => 'Score', 'f' => 'Avg Game Score', 'situational' => false],
        'po' => ['s' => 'PO', 'f' => 'Pick Offs', 'splits' => false, 'situational' => false],
        'go' => ['s' => 'GO', 'f' => 'Groundouts', 'splits' => false, 'situational' => false],
        'fo' => ['s' => 'FO', 'f' => 'Flyouts', 'splits' => false, 'situational' => false],
        'go_fo' => ['s' => 'GO/FO', 'f' => 'Groundout/Flyout Ratio', 'splits' => false, 'situational' => false],
        'tb' => ['s' => 'TB', 'f' => 'Total Bases Allowed', 'season' => false, 'game' => false, 'sortable' => false],
        'out' => ['s' => 'Out', 'f' => 'Outs Recorded', 'situational' => false],
        'ab' => ['s' => 'AB', 'f' => 'At Bats', 'season' => false, 'game' => false, 'sortable' => false],
        'bf' => ['s' => 'BF', 'f' => 'Batters Faced'],
        'pt' => ['s' => 'PT', 'f' => 'Pitches Thrown', 'situational' => false],
        'b' => ['s' => 'B', 'f' => 'Balls', 'situational' => false],
        's' => ['s' => 'S', 'f' => 'Strikes', 'situational' => false],
        'r' => ['s' => 'R', 'f' => 'Runs Allowed', 'situational' => false],
        'ra' => ['s' => 'RA', 'f' => 'Run Average', 'situational' => false],
        'er' => ['s' => 'ER', 'f' => 'Earned Runs Allowed', 'situational' => false],
        'ur' => ['s' => 'UR', 'f' => 'Unearned Runs Allowed', 'splits' => false, 'situational' => false],
        'ura' => ['s' => 'URA', 'f' => 'Unearned Run Average', 'splits' => false, 'situational' => false],
        'ir' => ['s' => 'IR', 'f' => 'Inherited Runners', 'splits' => false, 'situational' => false],
        'ira' => ['s' => 'IRA', 'f' => 'Inherited Runs Allowed', 'splits' => false, 'situational' => false],
        'winprob' => ['s' => 'WPA', 'f' => 'Win Probability Added', 'situational' => false],
    ];
    /**
     * The split stat columns
     * @var array
     */
    protected $splitsTypes = [
        'pitcher-info' => 'Pitcher Role',
        'home-road' => 'Home / Road',
        'month' => 'Month',
        'asb' => 'All-Star Break',
        'opponent' => 'Opponent',
        'stadium' => 'Stadium',
    ];
    /**
     * The situational stat columns
     * @var array
     */
    protected $situationalTypes = [
        'batter-info' => 'Batter Type',
        'with-baserunners' => 'Baserunners',
        'counts-on' => 'On Count',
        'counts-after' => 'After Count',
        'pitch-count' => 'Pitch Count',
        'ab-length' => 'AB Length',
        'inning' => 'Inning',
        'batting-order' => 'Batting Order',
        'fielding-pos' => 'Position',
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'h',
        'hr',
        'whip',
        'r',
        'ra',
        'er',
        'era',
        'ur',
        'ura',
        'ir',
        'ira',
        'wp',
        'bs',
        'winprob',
        'game_score',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (in_array($column, ['era', 'ra', 'ura'])) {
            return sprintf('%0.02f', $this->$column);
        } elseif ($column == 'go_fo') {
            return sprintf('%0.02f', $this->$column);
        } elseif (in_array($column, ['avg_agst', 'obp_agst', 'slg_agst', 'ops_agst'])) {
            return ltrim(sprintf('%0.03f', $this->$column), '0');
        } elseif ($column == 'decision') {
            return '<span class="' . $this->decisionCSS() . '">' . $this->$column . '</span>';
        }
        // Fallback value: number as-is.
        return $this->$column;
    }

    /**
     * Get the appropriate CSS for the decision
     * @return ?string The CSS class
     */
    public function decisionCSS(): ?string
    {
        if ($this->w) {
            return 'win';
        } elseif ($this->l) {
            return 'loss';
        } elseif ($this->hld) {
            return 'hold';
        } elseif ($this->sv) {
            return 'save';
        } elseif ($this->bs) {
            return 'blown-save';
        }
        return null;
    }

    /**
     * Convert the decision flags to a label
     * @return string The decision label
     */
    public function getDecisionAttribute(): string
    {
        $decisions = [];
        if ($this->bs) {
            $decisions[] = "BS, {$this->bs_y2d}";
        }
        if ($this->w) {
            $decisions[] = "W, {$this->w_y2d}&ndash;{$this->l_y2d}";
        }
        if ($this->l) {
            $decisions[] = "L, {$this->w_y2d}&ndash;{$this->l_y2d}";
        }
        if ($this->hld) {
            $decisions[] = "HLD, {$this->hld_y2d}";
        }
        if ($this->sv) {
            $decisions[] = "SV, {$this->sv_y2d}";
        }
        return $decisions ? join('; ', $decisions) : '';
    }
}
