<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsKickRet
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'num' => ['s' => 'Num', 'f' => 'Number'],
        'yards' => ['s' => 'Yds', 'f' => 'Yards'],
        'avg' => ['s' => 'Avg', 'f' => 'Average Return'],
        'long' => ['s' => 'Long', 'f' => 'Longest'],
        'fair_catch' => ['s' => 'FC', 'f' => 'Fair Catches'],
        'td' => ['s' => 'TD', 'f' => 'Return Touchdowns'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'num' => 'SUM(TBL.num)',
        'yards' => 'SUM(TBL.yards)',
        'avg' => 'SUM(TBL.yards) / SUM(TBL.num)',
        'long' => 'MAX(TBL.`long`)',
        'fair_catch' => 'SUM(TBL.fair_catch)',
        'td' => 'SUM(TBL.td)',
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'avg',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'yards') {
            return number_format($this->$column);
        } elseif ($column == 'avg') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
