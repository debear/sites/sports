<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsPassDef
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'pd' => ['s' => 'PD', 'f' => 'Passes Defended'],
        'int' => ['s' => 'Int', 'f' => 'Interceptions'],
        'int_yards' => ['s' => 'Yds', 'f' => 'Return Yards'],
        'int_td' => ['s' => 'TD', 'f' => 'Return Touchdowns'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'pd' => 'SUM(TBL.pd)',
        'int' => 'SUM(TBL.`int`)',
        'int_yards' => 'SUM(TBL.int_yards)',
        'int_td' => 'SUM(TBL.int_td)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        return $this->$column ?? '&ndash;';
    }
}
