<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsTackles
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'total' => ['s' => 'Tot', 'f' => 'Total'],
        'tckl' => ['s' => 'Solo', 'f' => 'Solo'],
        'asst' => ['s' => 'Asst', 'f' => 'Assists'],
        'sacks' => ['s' => 'Sack', 'f' => 'Sacks'],
        'sacks_yards' => ['s' => 'Yards', 'f' => 'Sack Yards'],
        'tfl' => ['s' => 'TFL', 'f' => 'Tackles for Loss'],
        'qb_hit' => ['s' => 'Hit', 'f' => 'QB Hits'],
        'kick_block' => ['s' => 'Blk', 'f' => 'Kick Blocks'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'total' => 'SUM(TBL.tckl) + SUM(TBL.asst)',
        'tckl' => 'SUM(TBL.tckl)',
        'asst' => 'SUM(TBL.asst)',
        'sacks' => 'SUM(TBL.sacks)',
        'sacks_yards' => 'SUM(TBL.sacks_yards)',
        'tfl' => 'SUM(TBL.tfl)',
        'qb_hit' => 'SUM(TBL.qb_hit)',
        'kick_block' => 'SUM(TBL.kick_block)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'sacks') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
