<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

use Illuminate\Support\Facades\Config as FrameworkConfig;

trait ScheduleDate
{
    /**
     * The full URL for the dated schedule
     * @param string $col The applicable date column.
     * @return string The full schedule link
     */
    public function linkFull(string $col = ''): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . '/schedule/' . $this->linkDate($col);
    }

    /**
     * The full URL for the dated gameday inactive list
     * @param string $col The applicable date column.
     * @return string The full gameday inactives link
     */
    public function linkInactives(string $col = ''): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . '/inactives/' . $this->linkDate($col);
    }

    /**
     * The full URL for the weekly injury list
     * @param string $col The applicable date column.
     * @return string The full weekly injury link
     */
    public function linkInjuries(string $col = ''): string
    {
        return '/' . FrameworkConfig::get('debear.section.endpoint') . '/injuries/' . $this->linkDate($col);
    }

    /**
     * Specify the date level of the schedule link
     * @param string $col The applicable date column.
     * @return string The date component of the schedule link
     */
    public function linkDate(string $col = ''): string
    {
        if ($this->game_type == 'regular') {
            // Regular season is based on the week number.
            $week = "week-{$this->week}";
        } else {
            // Playoffs are just the round name.
            $po_codes = array_keys(FrameworkConfig::get('debear.setup.playoffs.rounds'));
            $week = $po_codes[$this->week - 1];
        }
        return "{$this->season}-$week";
    }
}
