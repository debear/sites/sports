<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsRushing
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'atts' => ['s' => 'Atts', 'f' => 'Attempts'],
        'yards' => ['s' => 'Yds', 'f' => 'Yards'],
        'avg' => ['s' => 'Avg', 'f' => 'Average'],
        'long' => ['s' => 'Long', 'f' => 'Longest'],
        'td' => ['s' => 'TD', 'f' => 'Touchdowns'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'atts' => 'SUM(TBL.atts)',
        'yards' => 'SUM(TBL.yards)',
        'avg' => 'SUM(TBL.yards) / SUM(TBL.atts)',
        'long' => 'MAX(TBL.`long`)',
        'td' => 'SUM(TBL.td)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'yards') {
            return number_format($this->$column);
        } elseif ($column == 'avg') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
