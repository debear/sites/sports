<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsKicking
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'fg_made' => ['s' => 'FGM', 'f' => 'Field Goals Made'],
        'fg_att' => ['s' => 'FGA', 'f' => 'Field Goals Attempted'],
        'fg_pct' => ['s' => 'FG %', 'f' => 'Field Goal %'],
        'fg_u20' => ['s' => 'U20', 'f' => 'Field Goals Made Under 20yds'],
        'fg_u30' => ['s' => '20-29', 'f' => 'Field Goals Made 20-29yds'],
        'fg_u40' => ['s' => '30-39', 'f' => 'Field Goals Made 30-39yds'],
        'fg_u50' => ['s' => '40-49', 'f' => 'Field Goals Made 40-49yds'],
        'fg_o50' => ['s' => '50+', 'f' => 'Field Goals Made Over 50yds'],
        'xp_made' => ['s' => 'XPM', 'f' => 'Extra Points Made'],
        'xp_att' => ['s' => 'XPA', 'f' => 'Extra Points Attempted'],
        'xp_pct' => ['s' => 'XP %', 'f' => 'Extra Point %'],
        'pts' => ['s' => 'Pts', 'f' => 'Points'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'fg_att' => 'SUM(TBL.fg_att)',
        'fg_made' => 'SUM(TBL.fg_made)',
        'fg_pct' => '100 * (SUM(TBL.fg_made) / SUM(TBL.fg_att))',
        'fg_u20' => 'SUM(TBL.fg_u20)',
        'fg_u30' => 'SUM(TBL.fg_u30)',
        'fg_u40' => 'SUM(TBL.fg_u40)',
        'fg_u50' => 'SUM(TBL.fg_u50)',
        'fg_o50' => 'SUM(TBL.fg_o50)',
        'xp_att' => 'SUM(TBL.xp_att)',
        'xp_made' => 'SUM(TBL.xp_made)',
        'xp_pct' => '100 * (SUM(TBL.xp_made) / SUM(TBL.xp_att))',
        'pts' => '(3 * SUM(TBL.fg_made)) + SUM(TBL.xp_made)',
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'fg_pct',
        'xp_pct',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (substr($column, -4) == '_pct') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
