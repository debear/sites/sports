<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsReceiving
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'targets' => ['s' => 'Tgts', 'f' => 'Targets'],
        'recept' => ['s' => 'Rcpt', 'f' => 'Receptions'],
        'yards' => ['s' => 'Yds', 'f' => 'Yards'],
        'avg' => ['s' => 'Avg', 'f' => 'Yards / Catch'],
        'long' => ['s' => 'Long', 'f' => 'Longest'],
        'td' => ['s' => 'TD', 'f' => 'Touchdowns'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'recept' => 'SUM(TBL.recept)',
        'yards' => 'SUM(TBL.yards)',
        'avg' => 'SUM(TBL.yards) / SUM(TBL.recept)',
        'long' => 'MAX(TBL.`long`)',
        'td' => 'SUM(TBL.td)',
        'targets' => 'SUM(TBL.targets)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'yards') {
            return number_format($this->$column);
        } elseif ($column == 'avg') {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
