<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsFumbles
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'num_fumbled' => ['s' => 'Fmb', 'f' => 'Fumbles'],
        'num_lost' => ['s' => 'FL', 'f' => 'Fumbles Lost'],
        'num_forced' => ['s' => 'FF', 'f' => 'Fumbles Forced'],
        'num_rec' => ['s' => 'FR', 'f' => 'Fumbles Recovered'],
        'num_rec_td' => ['s' => 'TD', 'f' => 'Recovery Touchdowns'],
        'oob' => ['s' => 'OOB', 'f' => 'Out-of-Bounds'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'num_fumbled' => 'SUM(TBL.num_fumbled)',
        'num_lost' => 'SUM(TBL.num_lost)',
        'num_forced' => 'SUM(TBL.num_forced)',
        'num_rec' => 'SUM(TBL.rec_own) + SUM(TBL.rec_opp)',
        'num_rec_td' => 'SUM(TBL.rec_own_td) + SUM(TBL.rec_opp_td)',
        'oob' => 'SUM(TBL.oob)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        return $this->$column ?? '&ndash;';
    }
}
