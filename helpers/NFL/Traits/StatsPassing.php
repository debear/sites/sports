<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsPassing
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'atts' => ['s' => 'Atts', 'f' => 'Attempts'],
        'cmp' => ['s' => 'Cmp', 'f' => 'Completions'],
        'pct' => ['s' => 'Pct', 'f' => 'Completion %'],
        'yards' => ['s' => 'Yds', 'f' => 'Yards'],
        'avg' => ['s' => 'Avg', 'f' => 'Yards / Catch'],
        'long' => ['s' => 'Long', 'f' => 'Longest'],
        'td' => ['s' => 'TD', 'f' => 'Touchdowns'],
        'int' => ['s' => 'Int', 'f' => 'Interceptions'],
        'sacked' => ['s' => 'Sack', 'f' => 'Times Sacked'],
        'sack_yards' => ['s' => 'SYds', 'f' => 'Lost Yards'],
        'rating' => ['s' => 'Rtng', 'f' => 'Passer Rating'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'atts' => 'SUM(TBL.atts)',
        'cmp' => 'SUM(TBL.cmp)',
        'pct' => '(100 * SUM(TBL.cmp)) / SUM(TBL.atts)',
        'yards' => 'SUM(TBL.yards)',
        'avg' => 'SUM(TBL.yards) / SUM(TBL.atts)',
        'long' => 'MAX(TBL.`long`)',
        'td' => 'SUM(TBL.td)',
        'int' => 'SUM(TBL.`int`)',
        'sacked' => 'SUM(TBL.sacked)',
        'sack_yards' => 'SUM(TBL.sack_yards)',
        'rating' => '"TBL.atts"', // Returned as a string col, ignored in formatting, but builds suitable WHERE clause.
    ];
    /**
     * Stats that have a qualifying aspect
     * @var array
     */
    protected $statQual = [
        'pct',
        'avg',
        'int',
        'sacked',
        'sack_yards',
        'rating',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if ($column == 'yards') {
            return number_format($this->$column);
        } elseif (in_array($column, ['pct', 'avg'])) {
            return sprintf('%0.01f', $this->$column);
        } elseif ($column == 'rating') {
            $rating = '&ndash;';
            if ($this->atts) {
                // Calculate the component parts.
                $part_cmp = ((($this->cmp / $this->atts) - 0.3) * 5);
                $part_yards = ((($this->yards / $this->atts) - 3) * 0.25);
                $part_td  = (($this->td / $this->atts) * 20);
                $part_int = (2.375 - (($this->int / $this->atts) * 25));
                // Ensure they are within the 0 -> 2.375 range.
                $part_cmp = min(max($part_cmp, 0), 2.375);
                $part_yards = min(max($part_yards, 0), 2.375);
                $part_td = min(max($part_td, 0), 2.375);
                $part_int = min(max($part_int, 0), 2.375);
                // Calculate from the component part.
                $rating = sprintf('%.01f', ((($part_cmp + $part_yards + $part_td + $part_int) / 6) * 100));
            }
            // Then return.
            return $rating;
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
