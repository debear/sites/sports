<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsMisc
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'gp' => ['s' => 'GP', 'f' => 'Games Played', 'game' => false],
        'gs' => ['s' => 'GS', 'f' => 'Started'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'gp' => 'SUM(TBL.gp)',
        'gs' => 'SUM(TBL.gs)',
    ];
}
