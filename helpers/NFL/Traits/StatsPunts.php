<?php

namespace DeBear\Helpers\Sports\NFL\Traits;

trait StatsPunts
{
    /**
     * The way to identify the season totals row in the sorted stats
     * @var array
     */
    protected $whereIsTotals = ['team_id', '=', '_NFL'];
    /**
     * The column names
     * @var array
     */
    protected $statMeta = [
        'num' => ['s' => 'Num', 'f' => 'Number'],
        'yards' => ['s' => 'Yds', 'f' => 'Yards'],
        'avg' => ['s' => 'Avg', 'f' => 'Average'],
        'net' => ['s' => 'Net Yds', 'f' => 'Net Yards'],
        'net_avg' => ['s' => 'Net Avg', 'f' => 'Net Average'],
        'long' => ['s' => 'Long', 'f' => 'Longest'],
        'tb' => ['s' => 'TB', 'f' => 'Touchbacks'],
        'inside20' => ['s' => 'In 20', 'f' => 'Inside 20'],
    ];
    /**
     * The column mapping within the detailed stat table
     * @var array
     */
    protected $statColumns = [
        'num' => 'SUM(TBL.num)',
        'yards' => 'SUM(TBL.yards)',
        'avg' => 'SUM(TBL.yards) / SUM(TBL.num)',
        'net' => 'SUM(TBL.net)',
        'net_avg' => 'SUM(TBL.net) / SUM(TBL.num)',
        'long' => 'MAX(TBL.`long`)',
        'tb' => 'SUM(TBL.tb)',
        'inside20' => 'SUM(TBL.inside20)',
    ];

    /**
     * Value formatter for the stat rendering
     * @param string $column The column from the object we want formatted.
     * @return string The formatted stat value
     */
    public function format(string $column): string
    {
        if (!isset($this->$column)) {
            return '&ndash;';
        }
        // Specific instances.
        if (in_array($column, ['yards', 'net'])) {
            return number_format($this->$column);
        } elseif (in_array($column, ['avg', 'net_avg'])) {
            return sprintf('%0.01f', $this->$column);
        }
        // Fallback value: number as-is.
        return $this->$column;
    }
}
