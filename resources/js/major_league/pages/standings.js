get('dom:load').push(() => {
    Events.attach(document, 'debear:viewport:set', () => {
        // If responded, hide the Conference table
        if (!window.location.hash.length && !DOM.child('body').classList.contains('viewport-desktop')) {
            DOM.children('.subnav-conf').forEach((ele) => {
                ele.classList.add('hidden');
            });
        }
    });

    Events.attach(document, 'debear:season-switcher', (e) => {
        // Update the view
        SubNav.onLoad(true);
        Events.fire('viewport:set');
        // Set the address bar URL
        history.pushState('', document.title, location.pathname.split('/').slice(0, 3).join('/') + `/${e.detail.season}`);
    });
});
