get('dom:load').push(() => {
    Events.attach(document, 'debear:season-switcher', (e) => {
        // Set the address bar URL
        history.pushState('', document.title, location.pathname.split('/').slice(0, 3).join('/') + `/${e.detail.season}`);
    });

    Events.attach(document, 'click', (e) => {
        var ele = e.target;
        var eleMatchup = ele.closest('.matchup');
        if (!Input.isDefined(eleMatchup)) {
            return;
        }
        // Get the appropriate link to go to
        var eleLink = eleMatchup.querySelector('.link span');
        var url = DOM.getAttribute(eleLink, 'link');
        // Displaying in a modal, or direct link?
        var modalSize = DOM.getAttribute(eleLink, 'modal');
        if (Input.isDefined(modalSize)) {
            // In a modal
            Modals.open(modalSize, url, ele.closest('page'));
        } else {
            // Direct link
            DOM.redirectPage(url);
        }
    });
});
