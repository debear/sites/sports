get('dom:load').push(() => {
    // Setup event listeners for the season switchers
    Events.attach(document, 'debear:dropdown-set', (e) => {
        // Relevant to a switcher on the page?
        if (!Input.isDefined(e.detail.id) || (e.detail.value == e.detail.was)) {
            return;
        }
        // What tab are we updating?
        if (e.detail.id.substring(0, 7) == 'season-') {
            var tab = e.detail.id.substring(7);
            loadTab(tab, e.detail.value);
        }
    });
    // Favourite team actions
    var eleFav = DOM.child('button.favourite');
    Events.attach(eleFav, 'click', () => {
        processFavouriteTeam(DOM.getAttribute(eleFav, 'mode'));
    });
    // Prepare some internal objects and caches
    set('team:ajax', new Ajax());
    set('team:tabList', []);
    set('team:tabCache', {});
    set('team:games', {});
    set('team:standingsRecords', {});
    set('team:powerRankWeeks', {});
    // Load the remaining tabs
    setupTabs();
});

/* Initial tab loading */
setupTabs = () => {
    // Determine the tabs we're processing
    DOM.children('subnav item').forEach((ele) => {
        var tab = DOM.getAttribute(ele, 'tab');
        if (tab != 'home') {
            // Skip the home tab, which is automatically loaded.
            get('team:tabList').push(tab);
        }
    });
    // Re-order tabs, so current fragment identifier takes precedence in loading
    if (window.location.hash) {
        var currTab = window.location.hash.substring(1);
        get('team:tabList').sort((a, b) => {
            if (currTab == a) {
                // a is our current tab so takes precedence
                return -1;
            } else if (currTab == b) {
                // b is our current tab so takes precedence
                return 1;
            } else {
                // Neither is our current tab so do not change
                return 0;
            }
        });
    }
    // Now loop through and load
    loadTab(get('team:tabList')[0]);
}

loadTab = (tab, season) => {
    // If we have previously cached the AJAX response, use it rather than re-request it
    var tabSeason = (season ?? (DOM.child(`#season-${tab}`)?.value ?? '-'));
    if (Input.isDefined(get('team:tabCache')[`${tab}:${tabSeason}`])) {
        renderTab(tab, get('team:tabCache')[`${tab}:${tabSeason}`], season);
        return;
    }
    // Make the AJAX request to load the data
    get('team:ajax').request({
        'url': location.pathname.replace('/$', '') + `/${tab}` + (Input.isDefined(season) ? `/${season}` : ''),
        'headers': {
            'X-DeBearTab': 'true'
        },
        'success': (retValue) => {
            get('team:tabCache')[`${tab}:${tabSeason}`] = retValue;
            renderTab(tab, retValue, season);
            // Move on to the next tab whilst initially loading?
            if (get('team:tabList').length) {
                get('team:tabList').shift();
                if (get('team:tabList').length) {
                    loadTab(get('team:tabList')[0], season);
                }
            }
            // Sneaky extra: if the standings, do we have the corresponding schedule data?
            if (tab == 'standings' && Input.isDefined(season) && !Input.isDefined(get('team:games')[season])) {
                loadTab('schedule', season);
            }
        },
        'failure': () => {
            console.log('An error occurred');
        }
    });
}

renderTab = (tab, retValue, season) => {
    // Determine where we'll be adding the markup
    var ele = DOM.child(`.subnav-${tab} .tab-content`) ?? DOM.child(`.subnav-${tab}`);
    // Process the returned value to render
    if (tab == 'schedule') {
        // Preserve the schedule info for use in other tabs
        season = season ?? DOM.child('#season-schedule').value;
        if (!Input.isDefined(get('team:games')[season])) {
            get('team:games')[season] = retValue.sched;
        }
        /* Schedule has its own JavaScript handler */
        if (!Input.isDefined(retValue.page)) {
            if (!DOM.getAttribute(ele, 'setup')) {
                ele.innerHTML = '<div class="schedule">\
        <ul class="inline_list months hidden-m clearfix"></ul>\
        <div class="months hidden-t hidden-d"><select></select></div>\
        <dl class="games clearfix"></dl>\
    </div>';
                DOM.setAttribute(ele, 'setup', true);
            }
            // Store the info we were provided
            if (!Input.isDefined(get('gameTabs'))) {
                set('gameTabs', retValue.gameTabs);
            }
            MonthlySchedule.setup();
        } else {
            /* Schedule markup provided */
            ele.innerHTML = retValue.page;
        }

    } else if (['standings', 'power-ranks'].indexOf(tab) >= 0) {
        /* Standings and Power Rankings are chart updates */
        if (!DOM.getAttribute(ele, 'setup')) {
            ele.innerHTML = `<div class="chart" id="chart-${tab}"></div>`;
            DOM.setAttribute(ele, 'setup', true);
        }
        // If we have chart data, render the chart
        if (Input.isTrue(retValue)) {
            season = season ?? DOM.child(`#season-${tab}`).value;
            Object.keys(retValue.extraJS).forEach((key) => {
                get(`team:${key}`)[season] = retValue.extraJS[key];
            });
            var chartData = (tab == 'standings' ? standingsFunctions : powerRanksFunctions);
            var chartObj = Highcharts.convertJSONFunctions(retValue.chart, chartData, '_');
            var chart = new Highcharts.Chart(chartObj); // eslint-disable-line no-unused-vars
        } else {
            var labels = {'standings': 'Standings progress', 'power-ranks': 'Power Rankings'};
            ele.querySelector('div').innerHTML = `<fieldset class="info icon_info preseason">\
                ${labels[tab]} will be available after the start of the season.\
            </fieldset>`;
        }

    } else {
        /* Everything else is just a markup update */
        ele.innerHTML = retValue;
    }
    // Hide the loader
    DOM.child(`.subnav-${tab} .tab-loading`)?.classList.add('hidden');
}

/* Standings */
var standingsFunctions = {
    '_.yAxis.0.labels.formatter': function () {
        return this.value == 0 ? '' : Numbers.ordinal(this.value);
    },
    '_.plotOptions.spline.events.legendItemClick': () => { return false; },
    '_.plotOptions.column.events.legendItemClick': () => { return false; },
    '_.tooltip.formatter': function () {
        return standingsHover(this);
    }
};
var standingsSched = {};
standingsHover = (point) => {
    var ret = `<strong>${point.x}:</strong> ` + Numbers.ordinal(point.y);
    var record = get('team:standingsRecords')[DOM.child('#season-standings').value];
    if (Input.isDefined(record[point.points[0].point.x])) {
        ret += `, ${record[point.points[0].point.x]}`;
    }
    if (!Input.isDefined(standingsSched[point.x])) {
        var games = get('team:games')[DOM.child('#season-standings').value].filter((ele) => {
            return ele.date_fmt == point.x;
        });
        standingsSched[point.x] = (!games.length ? '' : '<br />' + games.map((ele) => {
            return `<div class="game">${ele.info}<span class="icon_right ${ele.opp}">${ele.venue}</span>${ele.opp_id}</div>`;
        }).join(''));
    }
    return `<tooltip>${ret}${standingsSched[point.x]}</tooltip>`;
}

/* Power Rankings */
var powerRanksFunctions = {
    '_.yAxis.0.labels.formatter': function () {
        return this.value == 0 ? '' : Numbers.ordinal(this.value);
    },
    '_.plotOptions.spline.events.legendItemClick': () => { return false; },
    '_.tooltip.formatter': function () {
        return powerRanksHover(this);
    }
};
powerRanksHover = (point) => {
    var weeks = get('team:powerRankWeeks')[DOM.child('#season-power-ranks').value];
    var ret = `<strong>${weeks[point.x]}:</strong> ` + Numbers.ordinal(point.y);
    var weekNum = point.points[0].point.x + 1;
    if (weekNum > 1) {
        var prev = point.points[0].series.data[weekNum - 2].y;
        var diff = point.y - prev;
        var dir = (diff < 0 ? 'up' : (dir > 0 ? 'down' : 'nc'));
        ret += ` (<span class="icon icon_pos_${dir}">` + (diff ? Math.abs(diff) : 'NC') + '</span>)';
    }
    return `<tooltip>${ret}</tooltip>`;
}

/* Favourite Team */
processFavouriteTeam = (mode) => {
    // Add or Remove?
    var isAdding = (mode == 'add');
    // Make the AJAX request to load the data
    get('team:ajax').request({
        'url': location.pathname.replace('/$', '') + '/favourite',
        'method': (isAdding ? 'POST' : 'DELETE'),
        'headers': {
            'Accept': 'application/json'
        },
        'success': (retValue) => {
            var ele = DOM.child('div.favourite button');
            var eleIcons = ele.querySelector('span.icons');
            var eleText = ele.querySelector('span.text');
            DOM.setAttribute(ele, 'mode', isAdding ? 'rmv' : 'add');
            eleIcons.className = `icons ${retValue.icon} ${retValue.hover}`;
            eleText.innerHTML = (isAdding ? 'Remove from favourites' : 'Add to favourites');
        },
        'failure': (retValue) => {
            console.log('Failure', retValue);
        }
    });
}
