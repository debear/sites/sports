// Filter handling
get('dom:load').push(() => {
    Events.attach(document, 'click', (e) => {
        // Show/Hide
        var ele = e.target;
        var eleShowHide = ele.closest('.filter-show_hide');
        if (Input.isDefined(eleShowHide)) {
            var eleLabel = eleShowHide.querySelector('span');
            var eleFilters = eleShowHide.closest('.filters').querySelector('.panel');
            if (eleFilters.classList.contains('hidden')) {
                // Hide
                eleLabel.innerHTML = 'Hide';
                eleShowHide.classList.remove('show');
                eleShowHide.classList.add('hide');
            } else {
                // Show
                eleLabel.innerHTML = 'Show';
                eleShowHide.classList.remove('hide');
                eleShowHide.classList.add('show');
            }
            eleFilters.classList.toggle('hidden');
        } else if (ele.classList.contains('filter-load')) {
            // Load the filters
            var selTeam = DOM.child('#filter_team')?.value;
            var selSeason = DOM.child('#filter_season').value.split('::');
            var selCat = DOM.child('#filter_category').value.split('::');
            var url = location.pathname.split('/').slice(0, 3).join('/') + `/${selCat[0]}s/${selCat[1]}?` + (Input.isTrue(selTeam) ? `team_id=${selTeam}&` : '') + `season=${selSeason[0]}&type=${selSeason[1]}`;
            DOM.redirectPage(url);
        }
    });
});

// Override the mobile select handler
ScrollTable.selectHandler = (e) => {
    var sel = e.target;

    // Update the label (for the transition)
    var tbl = sel.closest('scrolltable');
    tbl.querySelector('current').innerHTML = sel.querySelector(`option[value="${sel.value}"]`).innerHTML;

    // Get the appropriate link and switch to it
    var newOpt = sel.querySelector(`option[value="${sel.value}"]`).value;
    var newPage = tbl.querySelector(`.sort.col-${newOpt} .icon_sort_up`).href;
    DOM.redirectPage(newPage);
}
