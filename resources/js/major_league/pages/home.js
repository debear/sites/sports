get('dom:load').push(() => {
    Events.attach(DOM.child('.toggle-target'), 'click', (e) => {
        var newTab = e.target;
        if (!newTab.classList.contains('unsel')) {
            return;
        }
        // Hide the current element
        var currTab = DOM.child('.toggle-target dt.sel');
        currTab.classList.remove('sel');
        currTab.classList.add('unsel');
        if (currTab.classList.contains('row-head')) {
            currTab.classList.remove('row-head');
            currTab.classList.add('row-0');
        }
        var currID = DOM.getAttribute(currTab, 'id');
        DOM.child(`.toggle-target .tab-${currID}`).classList.toggle('hidden');
        // Show the newly selected
        newTab.classList.add('sel');
        newTab.classList.remove('unsel');
        if (newTab.classList.contains('row-0')) {
            newTab.classList.remove('row-0');
            newTab.classList.add('row-head');
        }
        var newID = DOM.getAttribute(newTab, 'id');
        DOM.child(`.toggle-target .tab-${newID}`).classList.toggle('hidden');
    });
});
