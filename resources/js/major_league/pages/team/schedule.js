get('dom:load').push(() => { set('team:schedule', {}); });

/* Schedule widget */
class MonthlySchedule { // eslint-disable-line no-unused-vars
    // Prepare the page
    static setup() {
        // Determine the month list
        var months = get('team:games')[DOM.child('#season-schedule').value].map((g) => {
            return g.date_ymd.substring(0, 7);
        }).sort().filter((m, i, months) => {
            return months.indexOf(m) === i;
        });
        var currMonth = (new Date()).toISOString().substring(0, 7);
        if (currMonth < months[0]) {
            currMonth = months[0];
        } else if (currMonth > months[months.length - 1]) {
            currMonth = months[months.length - 1];
        }
        // Generate the markup for the month lists
        var htmlList = '';
        var htmlDropdown = '';
        months.forEach((m) => {
            var mObj = new Date(`${m}-01`);
            // As a list item
            htmlList += `<li><a class="${m == currMonth ? 'sel' : ''}" data-month="${m}">`
                + `<span class="full">${mObj.toLocaleDateString('en-GB', { year: 'numeric', month: 'long' })}</span>`
                + `<span class="mini">${mObj.toLocaleDateString('en-GB', { year: 'numeric', month: 'short' })}</span>`
                + '</a></li>';
            // As a dropdown
            htmlDropdown += `<option value="${m}">${mObj.toLocaleDateString('en-GB', { year: 'numeric', month: 'long' })}</option>`;
        });
        // Render the list as links
        var eleList = DOM.child('div.schedule ul.months');
        eleList.innerHTML = htmlList;
        Events.attach(eleList, 'click', MonthlySchedule.clickHandler);
        // Render the list as a dropdown
        var eleDropdown = DOM.child('div.schedule div.months select');
        eleDropdown.innerHTML = htmlDropdown;
        eleDropdown.value = currMonth;
        Events.attach(eleDropdown, 'change', MonthlySchedule.selectHandler);

        // Show the schedule for the default month
        MonthlySchedule.render(currMonth);
    }

    // Click handler
    static clickHandler(e) {
        var ele = e.target;
        if (ele.tagName.toLowerCase() == 'span' && !ele.closest('a').classList.contains('sel')) {
            MonthlySchedule.render(DOM.getAttribute(ele.closest('a'), 'month'));
        }
    }

    // Click handler
    static selectHandler(e) {
        MonthlySchedule.render(e.target.value);
    }

    // Render the schedule
    static render(m) {
        // If we haven't already built the markup, do so
        var cache = get('team:schedule');
        if (!Input.isDefined(cache[m])) {
            var dowCode = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
            var dowFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            var monthGames = get('team:games')[DOM.child('#season-schedule').value].filter((g) => {
                return g.date_ymd.substring(0, 7) == m;
            });
            cache[m] = '<dt class="row-head dow-mon">Mon</dt>\
                <dt class="row-head dow-tue">Tue</dt>\
                <dt class="row-head dow-wed">Wed</dt>\
                <dt class="row-head dow-thu">Thu</dt>\
                <dt class="row-head dow-fri">Fri</dt>\
                <dt class="row-head dow-sat">Sat</dt>\
                <dt class="row-head dow-sun">Sun</dt>';
            // What leading days do we need before the first?
            var dowFirst = (new Date(`${m}-01 12:00:00`)).getDay();
            var dow = dowFirst;
            var d;
            for (d = 1; d < dowFirst; d++) {
                cache[m] += `<dd class="prev-month dow-${dowCode[d]}"></dd>`;
            }
            // The dates in this month
            var monthDays = new Date(m.substring(0, 4), m.substring(5, 7), 0).getDate();
            for (d = 1; d <= monthDays; d++) {
                var dt = `${m}-${d < 10 ? '0' : ''}${d}`;
                var dateGames = monthGames.filter((g) => {
                    return g.date_ymd == dt;
                });
                cache[m] += `<dd class="row-1 dow-${dowCode[dow]} games-${dateGames.length}"><div class="row-head"><span>${dowFull[dow]} </span>${Numbers.ordinal(d)}</div>`;
                dateGames.forEach((g) => {
                    var links = (g.link ? 'with' : 'no');
                    cache[m] += `<div class="game ${links}-links">`
                        + `<div class="opp ${g.opp_wide}"><span>${g.venue}</span></div>`
                        + `<div class="opp-name">${g.opp_city} ${g.opp_franchise}</div>`
                        + `<div class="info">${g.info}</div>`;
                    if (g.link) {
                        cache[m] += '<div class="row-0 links">';
                        get('gameTabs').forEach((t) => {
                            var l = g.link + (t.link ? `#${t.link}` : '');
                            var c = (t.link ? 'secondary' : 'main');
                            var lbl = (Input.isString(t.label) ? t.label : t.label[g.game_type]);
                            cache[m] += `<a href="${l}" class="icon_game_${t.icon} no_underline ${c}-link" title="${lbl}"></a>`;
                        });
                        cache[m] += '</div>';
                    }
                    cache[m] += '</div>';
                });
                cache[m] += '</dd>';
                dow = (dow < 6 ? dow + 1 : 0);
            }
            // Any trailing days?
            var dowLast = (new Date(`${m}-${monthDays} 12:00:00`)).getDay();
            for (d = dowLast + 1; d <= 7; d++) {
                cache[m] += `<dd class="next-month dow-${dowCode[d]}"></dd>`;
            }
            // Close the outer tag
            set('team:schedule', cache);
        }

        // Render
        DOM.child('div.schedule ul.months .sel').classList.remove('sel');
        DOM.child(`div.schedule ul.months a[data-month="${m}"]`).classList.add('sel');
        DOM.child('div.schedule div.months select').value = m;
        DOM.child('div.schedule .games').innerHTML = cache[m];
    }
}
