var searchDebounce;
get('dom:load').push(() => {
    set('player_search:ajax', new Ajax());

    /* Name search */
    Events.attach(DOM.child('.search .button'), 'click', () => {
        searchByName(DOM.child('.search .name input'));
    });
    Events.attach(DOM.child('.search .name input'), 'keyup', (e) => {
        var ele = e.target;
        // Debounce to 250ms
        if (Input.isDefined(searchDebounce)) {
            window.clearTimeout(searchDebounce);
        }
        searchDebounce = window.setTimeout((ele) => { searchByName(ele); }, 250, ele);
    });
});

searchByName = (ele) => {
    // Minimum three characters
    if (ele.value.length < 3) {
        return;
    }
    // Process
    var eleLoading = DOM.child('.ele.search .results');
    eleLoading.innerHTML = '<div class="row-0">Please wait, searching...</div>';
    eleLoading.classList.add('loading');
    eleLoading.classList.add('single');
    eleLoading.classList.remove('hidden');
    get('player_search:ajax').request({
        'url': location.pathname.split('/').slice(0, 3).join('/') + '/search/' + encodeURIComponent(ele.value),
        'success': (retValue) => {
            eleLoading.classList.remove('loading');
            var res;
            if (!retValue.length) {
                // No results
                res = '<div class="row-0">No results found.</div>';
            } else {
                // Results list
                res = '<ul class="inline_list">';
                retValue.forEach((row, index) => {
                    res += `<li class="row-${index % 2}"><a href="${row.url}">${row.first_name} ${row.surname}</a></li>`;
                });
                res += '</ul>';
            }
            eleLoading.innerHTML = res;
            eleLoading.classList.add('multi');
            eleLoading.classList.remove('single');
        },
        'failure': () => {
            eleLoading.classList.remove('loading');
            var eleContent = eleLoading.querySelector('div');
            eleContent.classList.add('error');
            eleContent.classList.add('icon_error');
            eleContent.innerHTML = 'An error occurred, please try again later.';
            window.setTimeout(() => {
                eleLoading.classList.add('hidden');
                eleContent.classList.remove('error');
                eleContent.classList.remove('icon_error');
            }, 10000);
        }
    });
}
