get('dom:load').push(() => {
    Events.attach(document, 'click', (e) => {
        var eleLink = e.target.closest('a');
        if (!Input.isDefined(eleLink)) {
            return;
        }
        // As we're in a modal, we want to redirect the 'top' page
        var url = DOM.getAttribute(eleLink, 'href');
        Events.cancel(e);
        top.window.location.href = url;
    });
});
