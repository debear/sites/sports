/* Power Rank Switcher */
get('dom:load').push(() => {
    Events.attach(document, 'debear:dropdown-set', (e) => {
        // Relevant to a switcher on the page?
        if (!Input.isDefined(e.detail.id) || (e.detail.value == e.detail.was)) {
            return;
        }
        // What action are we doing?
        if (e.detail.id == 'switcher') {
            weekSwitcher(e.detail.value);
        }
    });
});

// Switch to a new power rank week
weekSwitcher = (week) => {
    var ele = DOM.child('.dropdown[data-id="switcher"]').querySelector(`li[data-id="${week}"]`);
    // Redirect to the page via this code
    DOM.redirectPage(window.location.pathname.split('/').slice(0,3).join('/') + '/' + DOM.getAttribute(ele, 'href'));
}
