get('dom:load').push(() => {
    set('awards:ajax', new Ajax());
    set('awards:all-time', {});
    /* Click Handler */
    Events.attach(document, 'click', (e) => {
        var ele = e.target;
        if (ele.classList.contains('all-time')) {
            loadAllTimeWinners(ele);
        } else if (ele.classList.contains('modal-close')) {
            DOM.child('modal').classList.add('hidden');
        }
    });
});

// Load the all-time winners list
loadAllTimeWinners = (ele) => {
    // Get the appropriate link to go to and check we haven't already loaded the data
    var url = DOM.getAttribute(ele, 'link');
    if (Input.isDefined(get('awards:all-time')[url])) {
        // Go straight to rendering
        renderAllTimeWinners(url);
    }
    // Make the AJAX request to load the data
    get('awards:ajax').request({
        'url': url,
        'success': (retValue, config) => {
            get('awards:all-time')[config.url] = {
                'award': DOM.child(`[data-link="${config.url}"]`).closest('fieldset').querySelector('h3').innerHTML,
                'list': retValue
            };
            renderAllTimeWinners(config.url);
        },
        'failure': () => {
            console.log('An error occurred');
        }
    });
}

// Render the all-time winners list
renderAllTimeWinners = (url) => {
    var data = get('awards:all-time')[url];
    // Build the list
    var index = 0;
    var html = '';
    data.list.forEach((row) => {
        var winners = (!Input.isArray(row.winner) ? [row.winner] : row.winner);
        html += '<dt class="row-head season ' + (!index ? 'is-top ' : '') + `clear single num-${winners.length}">${row.season}</dt>`;
        // The winners
        if (!winners.length) {
            // No winner
            var rowCSS = (!index ? 'is-top ' : '') + 'row-' + (++index % 2);
            html += `<dd class="not-awarded ${rowCSS}">Not Awarded</dd>`;
        } else if (winners.length < 5) {
            // Single / Tied
            winners.forEach((w) => {
                var rowCSS = (!index ? 'is-top ' : '') + 'row-' + (++index % 2);
                html += `<dd class="player ${rowCSS}">${w.player}</dd>`
                    + `<dd class="team ${rowCSS}">${w.team}</dd>`;
                if (Input.isDefined(w.pos)) {
                    html += `<dd class="pos ${rowCSS}">${w.pos}</dd>`;
                }
            });
        } else {
            // Positional
            winners.forEach((w) => {
                var rowCSS = (!index ? 'is-top ' : '') + 'row-' + (++index % 2);
                html += `<dd class="pos ${rowCSS}">${w.pos}</dd>`
                    + `<dd class="player ${rowCSS}">${w.player}</dd>`
                    + `<dd class="team ${rowCSS}">${w.team}</dd>`;
            });
        }
    });
    DOM.child('modal h3').innerHTML = data.award;
    DOM.child('modal fieldset').innerHTML = `<dl>${html}</dl>`;
    // Display the modal
    DOM.child('modal').classList.remove('hidden');
}
