get('dom:load').push(() => {
    // Prepare some internal objects and caches
    set('player:ajax', new Ajax());
    set('player:tabList', []);
    set('player:tabCache', {});
    // Load the remaining tabs
    setupTabs();

    // Dropdown event listeners
    Events.attach(document, 'debear:dropdown-set', (e) => {
        // Skip if another dropdown
        var eleTest = DOM.child(`#${e.detail.id}`).closest('div.tab-dropdown');
        if (!Input.isDefined(eleTest)) {
            return;
        }
        // Determine the tab we're loading
        var tab = e.detail.id.substring(e.detail.id.indexOf('-') + 1);
        // Should we flip a sibling to its default value?
        if (e.detail.id.substring(0, e.detail.id.indexOf('-')) == 'season') {
            var eleGroups = DOM.child(`#group-${tab}`);
            if (Input.isDefined(eleGroups)) {
                get('dropdowns')[`group-${tab}`].setValue(DOM.getAttribute(eleGroups.closest('div.groups'), 'default'));
            }
        }
        // Show the loader?
        DOM.child(`.subnav-${tab} .tab-loading`)?.classList.remove('hidden');
        // Finally, load the tab
        loadTab(tab);
    });
});

/* Initial tab loading */
setupTabs = () => {
    // Determine the tabs we're processing
    DOM.children('subnav item').forEach((ele) => {
        var tab = DOM.getAttribute(ele, 'tab');
        if (tab != 'home') {
            // Skip the home tab, which is automatically loaded.
            get('player:tabList').push(tab);
        }
    });
    // Re-order tabs, so current fragment identifier takes precedence in loading
    if (window.location.hash) {
        var currTab = window.location.hash.substring(1);
        get('player:tabList').sort((a, b) => {
            if (currTab == a) {
                // a is our current tab so takes precedence
                return -1;
            } else if (currTab == b) {
                // b is our current tab so takes precedence
                return 1;
            } else {
                // Neither is our current tab so do not change
                return 0;
            }
        });
    }
    // Now loop through and load
    if (get('player:tabList').length) {
        loadTab(get('player:tabList')[0]);
    }
}

loadTab = (tab) => {
    // Determine any filtering rules applied to this tab
    var opt;
    var dropdowns = DOM.child(`.subnav-${tab}`).querySelectorAll('dl.dropdown');
    if (dropdowns.length) {
        opt = {};
        dropdowns.forEach((d) => {
            var nameFull = DOM.getAttribute(d, 'id');
            var name = nameFull.substring(0, nameFull.length - tab.length - 1);
            opt[name] = DOM.child(`#${nameFull}`).value;
        });
    }
    // If we have previously cached the AJAX response, use it rather than re-request it
    var optSer = (Input.isDefined(opt) ? ':' + JSON.stringify(opt) : '');
    if (Input.isDefined(get('player:tabCache')[`${tab}${optSer}`])) {
        renderTab(tab, get('player:tabCache')[`${tab}${optSer}`]);
        return;
    }
    // Make the AJAX request to load the data
    get('player:ajax').request({
        'url': location.pathname.replace('/$', '') + `/${tab}` + (Input.isDefined(opt) ? '?' + (new URLSearchParams(opt)).toString() : ''),
        'headers': {
            'X-DeBearTab': 'true'
        },
        'success': (retValue) => {
            get('player:tabCache')[`${tab}${optSer}`] = retValue;
            renderTab(tab, retValue);
            // Move on to the next tab whilst initially loading?
            if (get('player:tabList').length) {
                get('player:tabList').shift();
                if (get('player:tabList').length) {
                    loadTab(get('player:tabList')[0]);
                }
            }
        },
        'failure': () => {
            console.log('An error occurred');
        }
    });
}

renderTab = (tab, retValue) => {
    // Determine where we'll be adding the markup
    var ele = DOM.child(`.subnav-${tab} .tab-content`) ?? DOM.child(`.subnav-${tab}`);
    // Process the returned value to render
    ele.innerHTML = retValue;
    // Is this the current tab with scrolltables to setup?
    if (window.location.hash && (tab == window.location.hash.substring(1))) {
        ele.querySelectorAll('scrolltable').forEach((s) => {
            ScrollTable.processSetup(s);
        });
    }
    // Is there any filtering of the season/group dropdowns?
    var eleDrop = DOM.child(`.subnav-${tab} .tab-dropdown`);
    if (Input.isDefined(eleDrop)) {
        ele.querySelectorAll('.dropdown-filters').forEach((f) => {
            var d = eleDrop.querySelector('div.' + DOM.getAttribute(f, 'dropdown') + ' dl.dropdown');
            d.querySelectorAll('li').forEach((l) => {
                if (!Input.isTrue(DOM.getAttribute(f, DOM.getAttribute(l, 'id')))) {
                    // Drop out the irrelevant filter
                    l.classList.add('hidden');
                } else {
                    l.classList.remove('hidden');
                }
            });
            // If there is only a single filter available, we should not show the dropdown at all
            if (d.querySelectorAll('li:not(.hidden)').length == 1) {
                d.classList.add('hidden');
            } else {
                d.classList.remove('hidden');
            }
            // An over-riding default value?
            var id = DOM.getAttribute(d, 'id');
            var newDefault = DOM.getAttribute(f, 'default');
            if (Input.isDefined(newDefault) && get('dropdowns')[id].getValue() != newDefault) {
                get('dropdowns')[id].setValue(newDefault);
            }
            // Remove the filter definition element
            f.remove();
        });
        // Finally, if there are no dropdowns at all, remove the dropdown container
        var numDrop = eleDrop.querySelectorAll('dl.dropdown:not(.hidden)').length;
        if (!numDrop) {
            eleDrop.remove();
        }
        DOM.setAttribute(eleDrop, 'num', numDrop);
    }
    // Hide the loader
    DOM.child(`.subnav-${tab} .tab-loading`)?.classList.add('hidden');
}
