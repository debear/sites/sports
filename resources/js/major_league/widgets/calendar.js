get('dom:load').push(() => {
    set('game_date:ajax', new Ajax());
    set('game_date:content', {});
    // Replace the initial page with a stated history item
    var eleDate = DOM.child('.game_dates .curr');
    var initGameDate = DOM.getAttribute(eleDate, 'date');
    history.replaceState({'date': initGameDate}, document.title, eleDate.href);
    // Initial season (with a dynamically populated key)
    get('game_date:content')[initGameDate] = DOM.child('.season_switcher_content').innerHTML;
    Events.attach(document, 'click', (e) => {
        var ele = e.target.closest('a.date');
        if (!Input.isDefined(ele?.href) || !Input.isDefined(ele.closest('.game_dates'))) {
            return;
        }
        // Cancel the standard processing (as we'll handle the click here)
        Events.cancel(e);
        processGameDate(DOM.getAttribute(ele, 'date'));
    });

    // Catch a season change
    Events.attach(document, 'debear:season-switcher', () => {
        // Update the URL
        history.pushState('', document.title, DOM.getAttribute(DOM.child('.game_dates .curr'), 'link'));
        // Re-setup the calendar widget
        Calendar.setup();
    });

    // Catch a calendar click
    Events.attach(document, 'debear:calendar:click', (e) => {
        processGameDate(e.detail.date.replace(/-/g, ''));
    });

    // Handle the page back
    Events.attach(window, 'popstate', (e) => {
        if (Input.isDefined(e.state.date)) {
            renderGameDate(e.state.date);
        }
    })
});

processGameDate = (eleDate) => {
    var href = location.pathname.split('/').slice(0,3).join('/') + `/${eleDate}`;

    // Something we've previously cached?
    if (Input.isDefined(get('game_date:content')[eleDate])) {
        renderGameDate(eleDate, href);
        return;
    }
    // No, so perform the Ajax request
    DOM.child('.season_switcher_content').classList.add('loading');
    get('game_date:ajax').request({
        'url': `${href}/body`,
        'success': (retValue) => {
            get('game_date:content')[eleDate] = retValue;
            renderGameDate(eleDate, href);
            DOM.child('.season_switcher_content').classList.remove('loading');
        },
        'failure': () => {
            DOM.child('.season_switcher_content').classList.remove('loading');
        }
    });
}

renderGameDate = (eleDate, href) => {
    DOM.child('.season_switcher_content').innerHTML = get('game_date:content')[eleDate];
    if (Input.isDefined(href)) {
        history.pushState({'date': eleDate}, document.title, href);
    }
    Calendar.setup();
}
