get('dom:load').push(() => {
    /* Game Event filters */
    Events.attach(document, 'change', (e) => {
        var ele = e.target;
        if (!Input.isDefined(ele.closest('.events_filter'))) {
            return;
        }
        var type = DOM.getAttribute(ele, 'evtype');
        var value = DOM.getAttribute(ele, 'evvalue');
        ele.closest('.subnav-events').querySelectorAll(`.events li[data-${type}="${value}"]`).forEach((eve) => {
            if (ele.checked) {
                // We must make sure all elements are enabled before re-displaying
                if (DOM.child('.event_filter[data-evtype="period"][data-evvalue="' + DOM.getAttribute(eve, 'period') + '"]').checked
                    && DOM.child('.event_filter[data-evtype="team_id"][data-evvalue="' + DOM.getAttribute(eve, 'team_id') + '"]').checked
                    && DOM.child('.event_filter[data-evtype="type"][data-evvalue="' + DOM.getAttribute(eve, 'type') + '"]').checked
                ) {
                    eve.classList.remove('force-hidden');
                }
            } else {
                eve.classList.add('force-hidden');
            }
        });
    });
});
