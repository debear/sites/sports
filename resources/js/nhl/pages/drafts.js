get('dom:load').push(() => {
    Events.attach(document, 'debear:season-switcher', (e) => {
        // Update the view
        window.location.hash = ''; // Revert to Round 1 on switching
        SubNav.onLoad(true);
        Events.fire('viewport:set');
        // Set the address bar URL
        history.pushState('', document.title, location.pathname.split('/').slice(0, 3).join('/') + `/${e.detail.season}`);
    });
});
