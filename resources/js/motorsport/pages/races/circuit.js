get('dom:load').push(() => {
    /* Click handler */
    DOM.children('.result-quali').forEach((eleQuali) => {
        Events.attach(eleQuali, 'click', (e) => {
            var target = e.target.closest('.session-sort');
            if (!Input.isDefined(target) || target.classList.contains('sel')) {
                // Unrelated click, so skip
                return;
            }
            // Perform the sort
            var sort = 'sort-' + DOM.getAttribute(target, 'col');
            var tbl = target.closest('ul');
            [...tbl.children].sort((a, b) => {
                if (a.classList.contains('head')) {
                    // a is the header row
                    return -1;
                } else if (b.classList.contains('head')) {
                    // b is the header row
                    return 1;
                } else {
                    return (parseInt(DOM.getAttribute(a, sort)) > parseInt(DOM.getAttribute(b, sort))) ? 1 : -1;
                }
            }).map(node => tbl.appendChild(node));
            // Row Stylings
            [...tbl.children].forEach((ele, i) => {
                // Skip the header and grid's Top 3
                if (!i || (parseInt(DOM.getAttribute(ele, 'sort-grid')) <= 3)) {
                    return;
                }
                var rowCSS = 'row_' + (i % 2);
                ele.querySelectorAll('.row_0, .row_1').forEach((e) => {
                    e.classList.remove('row_0');
                    e.classList.remove('row_1');
                    e.classList.add(rowCSS);
                });
            });
            // Switch the current selection classes around
            var curr = target.closest('dl').querySelector('.sel');
            tbl.classList.remove('sort-' + DOM.getAttribute(curr, 'col'));
            curr.classList.remove('sel');
            curr.classList.add('unsel');
            tbl.classList.add(sort);
            target.classList.remove('unsel');
            target.classList.add('sel');
        });
    });
});
