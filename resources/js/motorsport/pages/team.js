/* Team Switcher */
get('dom:load').push(() => {
    Events.attach(document, 'debear:dropdown-set', (e) => {
        // Relevant to the switcher?
        if (((e?.detail?.id ?? '') != 'switcher') || (e.detail.value == e.detail.was)) {
            return;
        }
        var ele = DOM.child('.dropdown.switcher').querySelector(`li[data-id="${e.detail.value}"]`);
        // Redirect to the page via this code
        DOM.redirectPage(window.location.pathname.split('/').slice(0,3).join('/') + '/' + DOM.getAttribute(ele, 'code') + '/' + DOM.getAttribute(ele, 'season'));
    });
});

// Highcharts customisations
Events.attach(document, 'debear:highcharts-setup', () => {
    Highcharts.addAllowedAttributes('info');
    Highcharts.addAllowedAttributes('data-race');
});

/* Highcharts Hover */
progressHover = (chart) => {
    // Round / Race
    chart.x = chart.points[0].key.replace(/.+data-race="([0-9\-]+)".+/, "$1");
    var ret = `<strong class="highcharts-color-champ">After the ${races[chart.x]}</strong><br/>`;
    // Championship Position / Points
    ret += `<strong class="highcharts-color-${chart.points[0].colorIndex}">${chart.points[0].series.name}:</strong> ` + Numbers.ordinal(chart.points[0].y);
    if (chart.points.length > 1) {
        ret += ' (' + Number(chart.points[1].total).plural('pt') + ')';
    }

    // Participants
    for (var i = 1; i < chart.points.length; i++) {
        var k = `${chart.points[i].colorIndex}-${chart.x}`;
        var pos = Input.isDefined(racePos[k]) ? `<em>(${racePos[k]})</em> ` : '';
        ret += `<br/><strong class="highcharts-color-${chart.points[i].colorIndex}">${chart.points[i].series.name}</strong> ${pos}` + Number(chart.points[i].y).plural('pt');
    }

    return `<tooltip>${ret}</tooltip>`;
}
