/* Participant Switcher */
get('dom:load').push(() => {
    Events.attach(document, 'debear:dropdown-set', (e) => {
        // Relevant to a switcher on the page?
        if (!Input.isDefined(e.detail.id) || (e.detail.value == e.detail.was)) {
            return;
        }
        // What action are we doing?
        var newValue = e.detail.value;
        switch (e.detail.id) {
        // Participant switcher
        case 'switcher':
            participantSwitcher(newValue);
            break;
        // Progress season switcher
        case 'switcher-progress':
            progressSwitcher(newValue);
            get('dropdowns')['switcher-stats'].setValue(newValue);
            break;
        // Stats season switcher
        case 'switcher-stats':
            statsSwitcher(newValue);
            get('dropdowns')['switcher-progress'].setValue(newValue);
            break;
        }
    });

    Events.attach(document, 'debear:subnav:display', (e) => {
        if (e.detail.tab == 'history') {
            setupScrollable();
        }
    });
});

// Highcharts customisations
Events.attach(document, 'debear:highcharts-setup', () => {
    Highcharts.addAllowedAttributes('info');
    Highcharts.addAllowedAttributes('data-season');
    Highcharts.addAllowedAttributes('data-race');
});

// Switch to a new participant
participantSwitcher = (participant) => {
    var ele = DOM.child('.dropdown.switcher').querySelector(`li[data-id="${participant}"]`);
    // Redirect to the page via this code
    DOM.redirectPage(window.location.pathname.split('/').slice(0,3).join('/') + '/' + DOM.getAttribute(ele, 'href'));
}

// Focus on a specific season within the stats section
statsSwitcher = (season) => {
    var preseason = DOM.child('.preseason');
    var stats = DOM.child('.subnav-stats').querySelectorAll('.stat');
    // Requested a season with no stats yet?
    if (Input.isArray(statHistory[season]) && !statHistory[season].length) {
        preseason?.classList.remove('hidden');
        stats.forEach((dl) => {
            dl.parentNode.classList.add('hidden');
        });
        return;
    }
    // Flipping seasons
    preseason?.classList.add('hidden');
    stats.forEach((dl) => {
        dl.parentNode.classList.remove('hidden');
        var stat = statHistory[season][DOM.getAttribute(dl, 'id')];
        // Pos CSS
        dl.querySelectorAll('.cell').forEach((cell) => {
            DOM.getAttribute(cell, 'css').trim().split(' ').forEach((css) => {
                cell.classList.remove(css);
            });
            cell.classList.add(stat['pos_css']);
            DOM.setAttribute(cell, 'css', stat['pos_css']);
        });
        dl.querySelectorAll('.rank').innerHTML = stat['pos'];
        // Value and CSS
        var val = dl.querySelector('.value');
        val.innerHTML = stat['value'];
        if (stat['value_css']) {
            val.classList.add(stat['value_css']);
            DOM.setAttribute(val, 'css', DOM.getAttribute(val, 'css') + ` ${stat['pos_css']}`);
        }
    });
}

/* Highcharts Hover */
progressHover = (chart) => {
    // Round / Race
    var season = chart.points[0].key.replace(/.+data-season="(20[0-9]{2})".+/, "$1");
    chart.x = chart.points[0].key.replace(/.+data-race="([0-9\-]+)".+/, "$1");
    var chartInfo = chartHistory[season].tooltip.info[chart.x];
    var ret = `<strong class="highcharts-color-champ">After the ${chartInfo.name}</strong><br/>`;
    // Championship Position / Points
    ret += `<strong class="highcharts-color-${chart.points[0].colorIndex}">${chart.points[0].series.name}:</strong> ` + Numbers.ordinal(chart.points[0].y);
    if (chart.points.length > 1) {
        ret += ' (' + Number(chart.points[1].total).plural('pt') + ')';
    }

    // Race result / Grid
    for (var i = 2; i < chart.points.length; i++) {
        var key = (i == 2 ? 'res' : 'grid');
        ret += `<br/><strong class="highcharts-color-${chart.points[i].colorIndex}">${chart.points[i].series.name}:</strong> ${chartInfo[key]}`;
    }

    return `<tooltip>${ret}</tooltip>`;
}

// Focus on a specific season within the progress section
progressSwitcher = (season) => {
    chart = new Highcharts.Chart(chartHistory[season]);
}
