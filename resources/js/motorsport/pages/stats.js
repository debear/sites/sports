// Extend the base object to include our element getters
SortTable.getIndex = (ele) => {
    return DOM.getAttribute(ele.parentNode, 'sort');
}
SortTable.getDirection = (ele) => {
    return DOM.getAttribute(ele, 'dir');
}

// Custom sorting logic (dropping NULLs after non-NULLs)
SortTable.customSort = (a, b, dir) => {
    var aFlag = a.slice(-1);
    var bFlag = b.slice(-1);
    // If we have a matching NULL/non-NULL flag, resort to standard sorting
    if (aFlag == bFlag) {
        return SortTable.defaultSort(a.slice(0, -2), b.slice(0, -2), dir);
    }
    // Otherwise, non-NULLs (1) always gets priority over NULLs (0)
    return (aFlag > bFlag ? -1 : 1);
}

// Custom display logic (dropping the NULL flag)
SortTable.customDisplayPos = (rank) => {
    return rank.slice(0, -2);
}

/* Setup the handlers */
get('dom:load').push(() => {
    SortTable.setup(DOM.child('.stats'));
    Events.attach(document, 'debear:season-switcher', () => {
        if (Input.isDefined(DOM.child('subnav.above'))) {
            SubNav.setup();
        }
        ScrollTable.resizeSetup();
        SortTable.setup(DOM.child('.stats'));
    });
});
