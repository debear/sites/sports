get('dom:load').push(() => {
    /* Click handler */
    Events.attach(
        DOM.child('content'), 'click', (e) => {
            var target = e.target;
            if (Input.isDefined(target.closest('tables'))
                && target.classList.contains('sortable')
                && !target.classList.contains('sel')
            ) {
                /* Sorting */
                sortTable(target);
            }
        }
    );
});

/* Table Sorter */
sortTable = (target) => {
    // Which round are we viewing?
    var sort = DOM.getAttribute(target, 'round');
    var sortTotal = (sort == '0');
    // Perform the sort, first on the racelist
    var tbl = target.closest(!sortTotal ? 'racelist' : 'main');
    [...tbl.children].sort((a, b) => {
        if (a.tagName.toLowerCase() == 'flags') {
            // a is the flag row
            return -1;
        } else if (b.tagName.toLowerCase() == 'flags') {
            // b is the flag row
            return 1;
        } else if (sortTotal) {
            // Sorting by pts
            return (parseFloat(DOM.getAttribute(a, 'pos')) > parseFloat(DOM.getAttribute(b, 'pos'))) ? 1 : -1;
        } else {
            // Sorting by round result
            var aRes = a.querySelector(`race[data-round="${sort}"]`);
            var bRes = b.querySelector(`race[data-round="${sort}"]`);
            if (!Input.isDefined(aRes) && !Input.isDefined(bRes)) {
                // Neither participated (fallback to championship position)
                return (parseFloat(DOM.getAttribute(a, 'pos')) > parseFloat(DOM.getAttribute(b, 'pos'))) ? 1 : -1;
            } else if (!Input.isDefined(bRes)) {
                // a participated, b did not
                return -1;
            } else if (!Input.isDefined(aRes)) {
                // b participated, a did not
                return 1;
            } else {
                // Both participated
                return (parseFloat(DOM.getAttribute(aRes, 'pos')) > parseFloat(DOM.getAttribute(bRes, 'pos'))) ? 1 : -1;
            }
        }
    }).map(node => tbl.appendChild(node));
    // Then the main block
    var main = target.closest('standings').querySelector(!sortTotal ? 'main' : 'racelist');
    [...main.children].sort((a, b) => {
        if (a.tagName.toLowerCase() == 'flags') {
            // a is the flag row
            return -1;
        } else if (b.tagName.toLowerCase() == 'flags') {
            // b is the flag row
            return 1;
        } else if (sortTotal) {
            // Sorting by pts
            var aPos = tbl.querySelector('[data-id="' + DOM.getAttribute(a, 'id') + '"]');
            var bPos = tbl.querySelector('[data-id="' + DOM.getAttribute(b, 'id') + '"]');
            return (parseFloat(DOM.getAttribute(aPos, 'pos')) > parseFloat(DOM.getAttribute(bPos, 'pos'))) ? 1 : -1;
        } else {
            // Sorting by round result
            var aRes = tbl.querySelector('[data-id="' + DOM.getAttribute(a, 'id') + `"] race[data-round="${sort}"]`);
            var bRes = tbl.querySelector('[data-id="' + DOM.getAttribute(b, 'id') + `"] race[data-round="${sort}"]`);
            if (!Input.isDefined(aRes) && !Input.isDefined(bRes)) {
                // Neither participated (fallback to championship position)
                return (parseFloat(DOM.getAttribute(a, 'pos')) > parseFloat(DOM.getAttribute(b, 'pos'))) ? 1 : -1;
            } else if (!Input.isDefined(bRes)) {
                // a participated, b did not
                return -1;
            } else if (!Input.isDefined(aRes)) {
                // b participated, a did not
                return 1;
            } else {
                // Both participated
                return (parseFloat(DOM.getAttribute(aRes, 'pos')) > parseFloat(DOM.getAttribute(bRes, 'pos'))) ? 1 : -1;
            }
        }
    }).map(node => main.appendChild(node));

    // Re-colour alternating rows (excluding season podium)
    [...(!sortTotal ? main : tbl).children].forEach((ele, i) => {
        // Skipping season podium
        if (parseInt(DOM.getAttribute(ele, 'pos')) < 4) {
            return;
        }

        // What is the row colour? (Bearing in mind 4 => 0, 5 => 1, so i needs to be 1-indexed)
        var rowCSS = 'row_' + (i % 2);
        var oldRowCSS = 'row_' + ((i + 1) % 2);
        if (Input.isDefined(ele.querySelector(`pos.${rowCSS}`))) {
            // Already correct
            return;
        }

        // Convert
        ele.querySelectorAll(`.${oldRowCSS}`).forEach((cell) => {
            cell.classList.remove(oldRowCSS);
            cell.classList.add(rowCSS);
        });
    });

    // Show/Hide the separator?
    var sep = target.closest('standings').querySelectorAll('separator');
    if (sep.length) {
        [...sep].forEach((ele) => {
            if (sort == '0') {
                ele.classList.remove('hidden');
            } else if (!ele.classList.contains('hidden')) {
                ele.classList.add('hidden');
            }
        });
    }

    // Tweak the column header
    target.closest('standings').querySelector('.sel').classList.remove('sel');
    target.classList.add('sel');
}
