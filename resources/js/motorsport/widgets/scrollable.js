get('dom:load').push(() => {
    /* Scrollable checker */
    setupScrollable();
    Events.attach(window, 'resize', checkScrollable);
    Events.attach(document, 'debear:season-switcher', setupScrollable);
});

/* Window resize */
var tables;
setupScrollable = () => {
    tables = DOM.child('tables');
    DOM.setAttribute(tables, 'width', tables.clientWidth);
    checkScrollable();
}
checkScrollable = () => {
    if (Math.min(document.documentElement.clientWidth, DOM.child('content').clientWidth) < DOM.getAttribute(tables, 'width')) {
        tables.classList.add('scrollable');
    } else {
        tables.classList.remove('scrollable');
    }
}
