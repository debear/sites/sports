get('dom:load').push(() => {
    /* Click handler */
    Events.attach(DOM.child('content'), 'click', (e) => {
        var target = e.target;
        if (!Input.isDefined(target.closest('.key-handler'))) {
            return;
        }

        if (Input.isDefined(target.closest('.display_field')) && target.classList.contains('unsel')) {
            // Display Field
            displayField(target);
        } else if (Input.isDefined(target.closest('.display_key')) && target.classList.contains('unsel')) {
            // Key
            displayKey(target);
        }
    });
});

/* Display Field */
displayField = (target) => {
    // Determine which field we'll be showing instead
    var attrib = DOM.getAttribute(target, 'attrib');
    DOM.children('.race-cell').forEach((ele) => { ele.innerHTML = DOM.getAttribute(ele, attrib, {'default': ''}); });
    // And tweak the config links
    var existing = target.closest('dl').querySelector('.sel');
    target.classList.remove('unsel');
    target.classList.add('sel');
    existing.classList.remove('sel');
    existing.classList.add('unsel');
}

/* Display Key */
displayKey = (target) => {
    var dl = target.closest('dl');
    var existing = dl.querySelector('.sel');
    // Determine which field we'll be showing instead
    var ele = dl.querySelector('.list');
    if (parseInt(DOM.getAttribute(target, 'show'))) {
        ele.classList.remove('hidden');
    } else {
        ele.classList.add('hidden');
    }
    // And tweak the config links
    target.classList.remove('unsel');
    target.classList.add('sel');
    existing.classList.remove('sel');
    existing.classList.add('unsel');
}
