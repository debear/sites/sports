class EntityHover {
    // Prepare the hover card for the entity
    static setup(ele) {
        // Skip if already built
        if (ele.classList.contains('entity-hover')) {
            return;
        }
        // Render the name (in a non-recusrive hover way!)
        var name = ele.innerHTML;
        if (ele.parentNode.classList.contains('flag_right')) {
            // A combination of the parent flag and our inner content
            var flagName = ele.parentNode.cloneNode(false); // Just the outer span
            flagName.innerHTML = name;
            name = flagName.outerHTML;
        }
        // Render the hover
        ele.insertAdjacentHTML(
            'beforeend',
            '<dl class="hover box section">\
                <dd class="entity"></dd>\
                <dd class="mugshot mugshot-thumb"></dd>\
                <dd class="champ_pos"><strong>' + DOM.getAttribute(ele, 'hover-champ-pts') + '</strong> (' + DOM.getAttribute(ele, 'hover-champ-pos') + ')</dd>\
                <dd class="num_wins"><strong>Wins:</strong> ' + DOM.getAttribute(ele, 'hover-wins') + '</dd>\
                <dd class="num_podiums"><strong>Podiums:</strong> ' + DOM.getAttribute(ele, 'hover-podiums') + '</dd>\
            </dl>'
        );
        ele.querySelector('.hover .mugshot').style.backgroundImage = 'url(' + DOM.getAttribute(ele, 'hover-mugshot') + ')';
        ele.querySelector('.hover .entity').innerHTML = name;
        ele.classList.add('entity-hover');
    }
}

get('dom:load').push(() => {
    DOM.children('*[data-hover="true"]').forEach((ele) => {
        Events.attach(ele, 'mouseenter', (e) => { EntityHover.setup(e.target); });
    });
});
