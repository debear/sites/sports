get('dom:load').push(() => {
    set('game_week:ajax', new Ajax());
    set('game_week:content', {});
    // Replace the initial page with a stated history item
    var eleDate = DOM.child('.game_weeks .current a');
    var initGameDate = DOM.getAttribute(eleDate, 'date');
    history.replaceState({'date': initGameDate}, document.title, eleDate.href);
    // Initial season (with a dynamically populated key)
    get('game_week:content')[initGameDate] = DOM.child('.game_week_content').innerHTML;
    Events.attach(document, 'click', (e) => {
        var ele = e.target;
        if (!Input.isDefined(ele.href) || !Input.isDefined(ele.closest('.game_weeks'))) {
            return;
        }
        // Cancel the standard processing (as we'll handle the click here)
        Events.cancel(e);
        processGameWeek(ele);
    });

    // Catch a season change
    Events.attach(document, 'debear:season-switcher', () => {
        // Update the URL
        history.pushState('', document.title, DOM.child('.game_weeks .current a').href);
    });

    // Handle the page back
    Events.attach(window, 'popstate', (e) => {
        if (Input.isDefined(e.state.date)) {
            var ele = DOM.child(`a[data-date="${e.state.date}"]`);
            renderGameWeek(e.state.date, ele, true);
        }
    });

    // Custom SubNav toggling
    SubNav.toggle = (root, opt) => {
        // Skip if the current selection was re-made
        var currTab = root.querySelector('item.sel');
        var curr = DOM.getAttribute(currTab, 'tab');
        if (curr != opt) {
            // Trigger the main processing
            processGameWeek(DOM.child(`.game_weeks a[data-date="${opt}"]`));
        }
    }
});

processGameWeek = (ele) => {
    var eleDate = DOM.getAttribute(ele, 'date');
    // Something we've previously cached?
    if (Input.isDefined(get('game_week:content')[eleDate])) {
        renderGameWeek(eleDate, ele);
        return;
    }
    // No, so perform the Ajax request
    DOM.child('.game_week_content').classList.add('loading');
    get('game_week:ajax').request({
        'url': `${ele.href}/body`,
        'success': (retValue) => {
            get('game_week:content')[eleDate] = retValue;
            renderGameWeek(eleDate, ele);
            DOM.child('.game_week_content').classList.remove('loading');
        },
        'failure': () => {
            DOM.child('.game_week_content').classList.remove('loading');
        }
    });
}

renderGameWeek = (eleDate, ele, isInitial) => {
    DOM.child('.game_week_content').innerHTML = get('game_week:content')[eleDate];
    if (!Input.isDefined(isInitial)) {
        history.pushState({'date': eleDate}, document.title, ele.href);
    }
    // Update the desktop subnav
    DOM.child('.game_weeks .current').classList.remove('current');
    ele.parentNode.classList.add('current');
    // Update the responded subnav
    var currTab = DOM.child('item.sel');
    currTab.classList.remove('sel');
    currTab.classList.add('unsel');
    var optTab = DOM.child(`item[data-tab="${eleDate}"`);
    optTab.classList.remove('unsel');
    optTab.classList.add('sel');
    DOM.child('mob sel').innerHTML = optTab.innerHTML;
}
