get('dom:load').push(() => {
    // Handle switching seasons.
    Events.attach(document, 'debear:season-switcher', (e) => {
        // Update the view
        window.location.hash = ''; // Revert to the Amateur Draft on switching
        SubNav.onLoad(true);
        Events.fire('viewport:set');
        setupDropdowns();
        // Set the address bar URL
        history.pushState('', document.title, location.pathname.split('/').slice(0, 3).join('/') + '/' + e.detail.season);
        // Display the first round of this draft
        loadData();
        renderRound('amateur', 1);
    });

    // Handle switching tas
    Events.attach(document, 'debear:subnav:display', (e) => {
        renderRound(e.detail.tab, 1);
    });

    // Handle switching draft rounds.
    Events.attach(document, 'debear:dropdown-set', (e) => {
        if (e.detail.id.substring(0, 6) == 'draft-') {
            renderRound(e.detail.id.substring(6), e.detail.value);
        }
    });

    // Initial load.
    loadData();
    renderRound('amateur', 1);
});

// Load the data for a season.
loadData = () => {
    var eleData = DOM.child('.draft-data');
    set('draft-data', JSON.parse(eleData.innerHTML));
    eleData.remove();
}

// Render a round of the draft.
renderRound = (draftType, round) => {
    // Round header
    var numFmt = new Intl.NumberFormat();
    var html = '<dt class="row-head pick">Pick</dt>\
            <dt class="row-head team">Team</dt>\
            <dt class="row-head name">Player</dt>\
            <dt class="row-head attrib">Height</dt>\
            <dt class="row-head attrib">Weight</dt>\
            <dt class="row-head attrib">Bat</dt>\
            <dt class="row-head attrib">Throw</dt>\
            <dt class="row-head school">School</dt>';
    get('draft-data')[draftType][round].forEach((pick, index) => {
        var rowCSS = 'row-' + (index % 2);
        var incOverall = (round != 1);
        html += '<dd class="row-head ' + (incOverall ? 'pick-rnd' : 'pick') + `">${pick.round_pick}</dd>\
            ` + (incOverall ? `<dd class="${rowCSS} pick-ov">` + numFmt.format(pick.pick) + '</dd>' : '') + `\
            <dd class="${rowCSS} team">${pick.team}</dd>\
            <dd class="${rowCSS} name">${pick.player}` + (Input.isDefined(pick.pos) ? ` (${pick.pos})` : '') + `</dd>\
            <dd class="${rowCSS} attrib">` + (Input.isTrue(pick.height) ? pick.height : '&ndash;') + `</dd>\
            <dd class="${rowCSS} attrib">` + (Input.isTrue(pick.weight) ? `${pick.weight}lb`: '&ndash;') + `</dd>\
            <dd class="${rowCSS} attrib">` + (Input.isTrue(pick.bats) ? pick.bats : '&ndash;') + `</dd>\
            <dd class="${rowCSS} attrib">` + (Input.isTrue(pick.throws) ? pick.throws : '&ndash;') + `</dd>\
            <dd class="${rowCSS} school` + (!Input.isDefined(pick.school) ? ' none' : '') + '">' + (pick.school ?? 'n/a') + '</dd>';
    });

    DOM.child(`.subnav-${draftType} dl.round`).innerHTML = html;
}
