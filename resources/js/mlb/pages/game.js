// Win Probability hover
winprobHover = (point) => {
    // Already cached?
    var key = `winprob:hove:${point.x}`;
    var cached = get(key);
    if (Input.isDefined(cached)) {
        return cached;
    }
    // No, so build, cache and return
    var eleTeams = DOM.children('.periods .team');
    var play = winprobPlays[point.x]; // Our fuller data object
    var ret = (Input.isDefined(play.winprob_team) ? `<strong class="team-mlb-${play.winprob_team}">${play.winprob}%</strong>` : '<strong>Even</strong>') + ' '
        + ` - <span class="${eleTeams[0].classList}">${play.visitor_score}</span> `
        + `<span class="${eleTeams[1].classList}">${play.home_score}</span><br />`
        + `<strong>${winprobXLabels[play.inn_index]}</strong>, <strong>${play.out} out</strong>, <strong>${play.ob_tot} on</strong> (${play.ob_det})<br/>`
        + `<strong>Batter:</strong> <span class="team-mlb-${play.batter_team}">${play.batter}</span><br/>`
        + `<strong>Pitcher:</strong> <span class="team-mlb-${play.pitcher_team}">${play.pitcher}</span><br/>`
        + `<strong>Result:</strong> ${play.result}`;
    ret = `<tooltip>${ret}</tooltip>`;
    set(key, ret);
    return ret;
}
