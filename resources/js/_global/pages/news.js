/* Pagination click handler */
get('dom:load').push(() => {
    set('news:ajax', new Ajax());
    set('news:articles', {'1': DOM.child('articles').innerHTML});
    Events.attach(DOM.child('content'), 'click', (e) => {
        // Is this click within the pagination area?
        var target = e.target;
        if (!Input.isDefined(target)
            || !Input.isDefined(target.closest('ul.pagination'))
            || !Input.isDefined(DOM.getAttribute(target.closest('.onclick_target'), 'page'))
        ) {
            // Not a pagination click
            return;
        }

        // Have we already loaded this page?
        var page = DOM.getAttribute(target.closest('.onclick_target'), 'page');
        if (Input.isDefined(get('news:articles')[page])) {
            loadArticles(page);
            return;
        }

        // And then make the request
        var parent = DOM.child('articles');
        parent.classList.add('loading');
        get('news:ajax').request({
            'url': location.pathname + `?page=${page}`,
            'success': (retValue) => {
                get('news:articles')[page] = retValue;
                loadArticles(page);
                parent.classList.remove('loading');
            },
            'failure': () => {
                parent.classList.remove('loading');
            }
        });
    });
});

// Load the articles based on the provided content
loadArticles = (page) => {
    var html = get('news:articles')[page];
    // Grab the components
    var styles = html.match(new RegExp(/<style[^>]+>(.*?)<\/style/, 'is'));
    var content = html.match(new RegExp(/<ul[^>]+>(.*?)<\/ul/, 'is'));
    var pagination = html.match(new RegExp(/<pagination[^>]+>(.*?)<\/pagination/, 'is'));
    // Load in to the DOM
    var parent = DOM.child('articles');
    parent.querySelector('style').innerHTML = styles[1];
    parent.querySelector('ul.articles').innerHTML = content[1];
    parent.querySelector('pagination').innerHTML = pagination[1];
}
