get('dom:load').push(() => {
    set('season_switcher:ajax', new Ajax());
    set('season_switcher:content', {});
    // Initial season (with a dynamically populated key)
    get('season_switcher:content')[DOM.child('#switcher-season').value] = DOM.child('.season_switcher_content').innerHTML;
    Events.attach(document, 'debear:dropdown-set', (e) => {
        // Relevant to the season switcher on the page?
        if (((e?.detail?.id ?? '') != 'switcher-season') || (e.detail.value == e.detail.was)) {
            return;
        }
        // Something we've previously cached?
        var s = e.detail.value;
        if (Input.isDefined(get('season_switcher:content')[s])) {
            DOM.child('.season_switcher_content').innerHTML = get('season_switcher:content')[s];
            Events.fire('season-switcher', { 'season': s });
            return;
        }
        // No, so perform the Ajax request
        DOM.child('.season_switcher_content').classList.add('loading');
        get('season_switcher:ajax').request({
            'url': location.pathname.split('/').slice(0, 3).join('/') + `/${s}/body`,
            'success': (retValue) => {
                get('season_switcher:content')[s] = retValue;
                DOM.child('.season_switcher_content').innerHTML = retValue;
                Events.fire('season-switcher', { 'season': s });
                DOM.child('.season_switcher_content').classList.remove('loading');
            },
            'failure': () => {
                DOM.child('.season_switcher_content').classList.remove('loading');
            }
        });
    });
});
