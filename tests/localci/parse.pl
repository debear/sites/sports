#!/usr/bin/perl -w
# Determine the rules for a CI YML file. TD. 2019-01.21.

use strict;
use File::Basename;

# Fail if file does not exist
my $ci = '.ci.yml';
my $yml = dirname($0) . "/../../$ci";
if ( !-e $yml ) {
    print STDERR "Unable to find CI YAML file '$ci'?";
    exit 1;
}

# Load the file
open FILE, "<$yml";
my @file = <FILE>;
close FILE;
my $file = join('', @file);

# Now run our regex
$file =~ s/(#[^\n]+\nsast:.*?\s+\- gl-sast-report\.json)(\n{2})/$1\n  script:\n  \- tests\/sast\/emulate$2/gsi; # Modify the SAST rule to use our local emulator
my @matches = ($file =~ m/(#[^\n]+\n[^\n]+:\n  stage:.*?script:.*?\n(?:\n|$))/gsi);
foreach my $test ( @matches ) {
    # Break down in to its component parts
    $test =~ s/\n\s*before_script:.*?(\n\s*script:)/$1/gsi;
    my ($test_name, $test_code, $test_stage, $test_script) = ($test =~ m/^# ([^\n]+)\n([^\n]+):\n  stage: ([^\n]+)\n.*?script:(.+)$/gsi);
    my ($test_alwfail) = ($test =~ m/\sallow_failure: (\S+)/gsi);
    $test_alwfail = 'false' if !defined($test_alwfail);
    # Convert to a single line equivalent
    $test_script =~ s/#[^\n]+\n//g;
    $test_script =~ s/\\\n[ \t]+/ /g;
    $test_script =~ s/[ \t]+\n/\n/g;
    $test_script =~ s/\s+$//g;
    $test_script =~ s/^\s*apt-get[^\n]+\n//g;
    $test_script =~ s/^\s*\-\s+//g;
    my @scripts = split /\n\s*\-\s+/, $test_script;
    my $scripts = join('@@@', @scripts);
    # Output the test then
    print "$test_code£££$test_name£££$test_stage£££$test_alwfail£££$scripts\n";
}
