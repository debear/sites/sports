<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class XLegacyMigrationTest extends MajorLeagueBaseCase
{
    /**
     * A test of legacy migration of General URLs.
     * @return void
     */
    public function testGeneral(): void
    {
        // Index.
        $response = $this->get('/nfl/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl');

        $response = $this->get('/nfl/2019');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl');

        $response = $this->get('/nfl/2019/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl');

        // Standings.
        $response = $this->get('/nfl/2019/standings');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/standings/2019');

        $response = $this->get('/nfl/standings/expanded');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/nfl/standings/{$this->season}");

        $response = $this->get('/nfl/2019/standings/league');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/standings/2019');

        // News.
        $response = $this->get('/nfl/2019/news');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/news');

        $response = $this->get('/nfl/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/news');

        $response = $this->get('/nfl/2019/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/news');
    }

    /**
     * A test of legacy migration of Schedule URLs.
     * @return void
     */
    public function testSchedule(): void
    {
        // Schedule.
        $response = $this->get('/nfl/2000/schedule');
        $response->assertStatus(404);

        $response = $this->get('/nfl/2019/schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-superbowl');

        $response = $this->get('/nfl/schedule-1');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-week-1');

        $response = $this->get('/nfl/schedule-17');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-week-17');

        $response = $this->get('/nfl/schedule-superbowl');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-superbowl');

        $response = $this->get('/nfl/2019/schedule-1');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-week-1');

        $response = $this->get('/nfl/2019/schedule-17');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-week-17');

        $response = $this->get('/nfl/2019/schedule-superbowl');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-superbowl');

        // Playoffs.
        $response = $this->get('/nfl/2019/playoffs');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/playoffs/2019');
    }

    /**
     * A test of legacy migration of individual Game URLs.
     * @return void
     */
    public function testGame(): void
    {
        // Invalid game type or season.
        $response = $this->get('/nfl/game-s113');
        $response->assertStatus(404);
        $response = $this->get('/nfl/game-rp113');
        $response->assertStatus(404);
        $response = $this->get('/nfl/2000/game-r113');
        $response->assertStatus(404);

        // Ambiguous season.
        $response = $this->get('/nfl/game-r113');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule');
        $response = $this->get('/nfl/game-r113-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule');

        // To the new format.
        $response = $this->get('/nfl/2019/game-r113');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-week-1/lions-at-cardinals-r113');

        $response = $this->get('/nfl/2019/game-r113-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-week-1/lions-at-cardinals-r113'
            . '/boxscore');

        $response = $this->get('/nfl/2019/game-p103');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-wildcard/vikings-at-saints-p103');

        $response = $this->get('/nfl/2019/game-p103-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-wildcard/vikings-at-saints-p103'
            . '/boxscore');
    }

    /**
     * A test of legacy migration of Stats URLs.
     * @return void
     */
    public function testStats(): void
    {
        // Stats Summary.
        $response = $this->get('/nfl/2019/stats');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/stats');

        // Tables.
        $response = $this->get('/nfl/stats-ind-passing');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/stats/players/passing?season=2019&type=regular');

        $response = $this->get('/nfl/2019/stats-ind-passing');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/stats/players/passing?season=2019&type=regular');

        // By Team Stats.
        $response = $this->get('/nfl/stats-AAA');
        $response->assertStatus(404);

        $response = $this->get('/nfl/stats-GB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/stats/players/passing?team_id=GB&season=2019&'
            . 'type=regular');

        $response = $this->get('/nfl/2019/stats-GB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/stats/players/passing?team_id=GB&season=2019&'
            . 'type=regular');

        $response = $this->get('/nfl/stats-GB-passing');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/stats/players/passing?team_id=GB&season=2019&'
            . 'type=regular');

        $response = $this->get('/nfl/2019/stats-GB-passing');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/stats/players/passing?team_id=GB&season=2019&'
            . 'type=regular');
    }

    /**
     * A test of legacy migration of Team URLs.
     * @return void
     */
    public function testTeams(): void
    {
        $response = $this->get('/nfl/2019/teams');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/teams');

        $response = $this->get('/nfl/team-AAA');
        $response->assertStatus(404);

        $response = $this->get('/nfl/team-GB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/teams/green-bay-packers');

        $response = $this->get('/nfl/2019/team-GB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/teams/green-bay-packers');

        $response = $this->get('/nfl/team-GB-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/teams/green-bay-packers');

        $response = $this->get('/nfl/2019/team-GB-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/teams/green-bay-packers');
    }

    /**
     * A test of legacy migration of Player URLs.
     * @return void
     */
    public function testPlayers(): void
    {
        // List.
        $response = $this->get('/nfl/2019/players');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/players');

        // Individual.
        $response = $this->get('/nfl/player-0');
        $response->assertStatus(404);

        $response = $this->get('/nfl/player-3630');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/players/dj-swearinger-3630');

        $response = $this->get('/nfl/player-3630-career');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/nfl/players/dj-swearinger-3630/{$this->season}/career");

        $response = $this->get('/nfl/2019/player-3630');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/players/dj-swearinger-3630/2019');

        $response = $this->get('/nfl/2019/player-3630-career');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/players/dj-swearinger-3630/2019/career');
    }

    /**
     * A test of the legacy migration of Power Rank URLs.
     * @return void
     */
    public function testPowerRanks(): void
    {
        $response = $this->get('/nfl/2019/power-ranks');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/power-ranks/2019');

        $response = $this->get('/nfl/power-ranks-r2');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/nfl/power-ranks/{$this->season}/week-2");

        $response = $this->get('/nfl/2019/power-ranks-r2');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/power-ranks/2019/week-2');

        $response = $this->get('/nfl/power-ranks-p2');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/nfl/power-ranks/{$this->season}/division");

        $response = $this->get('/nfl/2019/power-ranks-p2');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/power-ranks/2019/division');
    }
}
