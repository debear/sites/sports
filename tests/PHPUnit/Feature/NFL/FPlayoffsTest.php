<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class FPlayoffsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the default season.
     * @return void
     */
    public function testDefault(): void
    {
        $response = $this->get('/nfl/playoffs');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; 2019 NFL Playoffs</title>');
        $response->assertSee('<li class="sel" data-id="2019">2019</li>');

        $response->assertSee('<li class="grid-3-7 rounds-4 conf-left">
    ' . '
            <h3 class="conf row_conf-AFC misc-nfl-narrow-small-AFC misc-nfl-narrow-small-right-AFC">
            <span class="full">American Football Conference</span>
            <span class="short">AFC</span>
        </h3>');
        $response->assertSee('<dt>Byes:</dt>
                                            <dd class="icon team-nfl-BAL">
                            <span class="full">Ravens</span>
                            <span class="short">BAL</span>
                            <sup>1</sup>
                        </dd>
                                            <dd class="icon team-nfl-KC">
                            <span class="full">Chiefs</span>
                            <span class="short">KC</span>
                            <sup>2</sup>
                        </dd>
                                </dl>
        </div>');
        $response->assertSee('<li class="grid-1-3 num-2-2">
                                    <fieldset class="matchup hover grass">
    <div class="team team-nfl-NE loser">
        <span class="team">NE <sup>3</sup></span>
        <span class="score">13</span>
    </div>
    <div class="team team-nfl-TEN winner">
        <span class="team">TEN <sup>6</sup></span>
        <span class="score">20</span>
    </div>
            <div class="link row-head">
                            <span class="icon_game_boxscore" data-link="/nfl/schedule/2019-wildcard/'
            . 'titans-at-patriots-p102">Boxscore</span>
                    </div>
    </fieldset>');
        $response->assertSee('<li class="champ-byes rounds-4 grid-1-7 num-1-2">
            <h3 class="row-head">Super Bowl LIV</h3>
        <fieldset class="matchup hover grass">
    <div class="team team-nfl-SF loser">
        <span class="team">SF <sup>1</sup></span>
        <span class="score">20</span>
    </div>
    <div class="team team-nfl-KC winner">
        <span class="team">KC <sup>2</sup></span>
        <span class="score">31</span>
    </div>
            <div class="link row-head">
                            <span class="icon_game_boxscore" data-link="/nfl/schedule/2019-superbowl/'
            . '49ers-at-chiefs-p401">Boxscore</span>
                    </div>
    </fieldset>
        ' . '
    <div class="logo ">
        <div class="large playoffs-nfl-large-0000 playoffs-nfl-large-2019"></div>
            </div>
</li>');
    }

    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/nfl/playoffs/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; 2019 NFL Playoffs</title>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/nfl/playoffs/2019/body');
        $response->assertStatus(200);
        $response->assertSee('<h1><span class="hidden-m">2019</span> NFL Playoffs</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/nfl/playoffs/2000');
        $response->assertStatus(404);
        $response = $this->get('/nfl/playoffs/2039');
        $response->assertStatus(404);
    }

    /**
     * A test of playoff series
     * @return void
     */
    public function testSeries(): void
    {
        $response = $this->get('/nfl/playoffs/2019/49ers-chiefs-41');
        $response->assertStatus(404);
    }
}
