<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class QGamedayTest extends MajorLeagueBaseCase
{
    /**
     * A test of inactives on the current date, identifying a future season (though deterministically "current")
     * @return void
     */
    public function testInactivesCurrentFuture(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestConfig(['debear.sports.subsites.nfl.setup.season.viewing' => 2020]);
        $this->setTestDateTime('2020-09-01 12:34:56');

        $response = $this->get('/nfl/inactives');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/inactives/2020-week-1" '
            . 'data-date="2020-week-1">1</a></dd>');

        $response->assertSee('<h3>Thursday 10th September</h3>
        <ul class="grid inline_list">
                            <li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                    <ul class="inline_list nfl_box game_list inactives clearfix">
                        <li class="team visitor row_nfl-HOU">
                            <span class="icon team-nfl-HOU">Texans</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home row_nfl-KC">
                            <span class="icon team-nfl-KC">Chiefs</span>
                        </li>
                        <li class="info">5:20pm PDT, Arrowhead Stadium</li>
                        <li class="list visitor"><ul class="inline_list">
            <li class="row-0 na">Not Yet Announced</li>
    </ul>
</li>
                        <li class="list home"><ul class="inline_list">
            <li class="row-0 na">Not Yet Announced</li>
    </ul>');
    }

    /**
     * A test of inactives on the current date, identifying a previous season (though deterministically "current")
     * @return void
     */
    public function testInactivesCurrentPrevious(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-01-05 12:34:56');

        $response = $this->get('/nfl/inactives/2019-wildcard');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/inactives/2019-wildcard" '
            . 'data-date="2019-wildcard">Wildcard</a></dd>');

        $response->assertSee('<h3>Saturday 4th January</h3>
        <ul class="grid inline_list">
                            <li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                    <ul class="inline_list nfl_box game_list inactives clearfix">
                        <li class="team visitor row_nfl-BUF">
                            <span class="icon team-nfl-BUF">Bills</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home row_nfl-HOU">
                            <span class="icon team-nfl-HOU">Texans</span>
                        </li>
                        <li class="info">1:35pm PST, NRG Stadium</li>
                        <li class="list visitor"><ul class="inline_list">
                                    <li class="row-0 pos">OT</li>
            <li class="row-0 name"><a href="/nfl/players/ryan-bates-6745">Ryan Bates</a></li>
                                <li class="row-1 pos">OG</li>
            <li class="row-1 name"><a href="/nfl/players/ike-boettger-6389">Ike Boettger</a></li>
                                <li class="row-0 pos">WR</li>
            <li class="row-0 name"><a href="/nfl/players/robert-foster-6249">Robert Foster</a></li>
                                <li class="row-1 pos">TE</li>
            <li class="row-1 name"><a href="/nfl/players/tommy-sweeney-6754">Tommy Sweeney</a></li>
                                <li class="row-0 pos">DT</li>
            <li class="row-0 name"><a href="/nfl/players/vincent-taylor-5493">Vincent Taylor</a></li>
                                <li class="row-1 pos">CB</li>
            <li class="row-1 name"><a href="/nfl/players/levi-wallace-6643">Levi Wallace</a></li>
                                <li class="row-0 pos">RB</li>
            <li class="row-0 name"><a href="/nfl/players/tj-yeldon-4435">T.J. Yeldon</a></li>
            </ul>
</li>
                        <li class="list home"><ul class="inline_list">
                                    <li class="row-0 pos">TE</li>
            <li class="row-0 name"><a href="/nfl/players/jordan-akins-6349">Jordan Akins</a></li>
                                <li class="row-1 pos">DB</li>
            <li class="row-1 name"><a href="/nfl/players/cornell-armstrong-6430">Cornell Armstrong</a></li>
                                <li class="row-0 pos">WR</li>
            <li class="row-0 name"><a href="/nfl/players/will-fuller-4911">Will Fuller</a></li>
                                <li class="row-1 pos">CB</li>
            <li class="row-1 name"><a href="/nfl/players/johnathan-joseph-708">Johnathan Joseph</a></li>
                                <li class="row-0 pos">WR</li>
            <li class="row-0 name"><a href="/nfl/players/steven-mitchell-6697">Steven Mitchell</a></li>
                                <li class="row-1 pos">OT</li>
            <li class="row-1 name"><a href="/nfl/players/elijah-nkansah-6694">Elijah Nkansah</a></li>
                                <li class="row-0 pos">DT</li>
            <li class="row-0 name"><a href="/nfl/players/eddie-vanderdoes-5556">Eddie Vanderdoes</a></li>
            </ul>
</li>
                    </ul>');
    }

    /**
     * A test of miscellaneous inactives controller actions
     * @return void
     */
    public function testInactivesControllerMisc(): void
    {
        // Current season.
        $this->setTestDateTime('2020-09-09 12:34:56');
        $response = $this->get('/nfl/inactives/2020/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nfl/inactives/2020-week-1/switcher');
        // Previous season.
        $this->setTestDateTime('2020-03-01 12:34:56');
        $response = $this->get('/nfl/inactives/2019/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nfl/inactives/2019-superbowl/switcher');
    }

    /**
     * A test of injuries on the current date, identifying a future season (though deterministically "current")
     * @return void
     */
    public function testInjuriesCurrentFuture(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestConfig(['debear.sports.subsites.nfl.setup.season.viewing' => 2020]);
        $this->setTestDateTime('2020-09-01 12:34:56');

        $response = $this->get('/nfl/injuries');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/injuries/2020-week-1" '
            . 'data-date="2020-week-1">1</a></dd>');

        $response->assertSee('<h3>Thursday 10th September</h3>
        <ul class="grid inline_list">
                            <li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                    <ul class="inline_list nfl_box game_list injuries clearfix">
                        <li class="team visitor row_nfl-HOU">
                            <span class="icon team-nfl-HOU">Texans</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home row_nfl-KC">
                            <span class="icon team-nfl-KC">Chiefs</span>
                        </li>
                        <li class="info">5:20pm PDT, Arrowhead Stadium</li>
                        <li class="list visitor"><ul class="inline_list">
                                                            <li class="head status">Questionable</li>
                        <li class="row-0 pos">WR</li>
            <li class="row-0 player"><a href="/nfl/players/brandin-cooks-4010">Brandin Cooks</a>'
            . '<span class="injury">Quadricep</span></li>
                                                        <li class="row-1 pos">RB</li>
            <li class="row-1 player"><a href="/nfl/players/cullen-gillaspia-6850">Cullen Gillaspia</a>'
            . '<span class="injury">Hamstring</span></li>
                        </ul>
</li>
                        <li class="list home"><ul class="inline_list">
            <li class="row-0 na">No Injuries Reported</li>
    </ul>
</li>
                    </ul>');
    }

    /**
     * A test of injuries on the current date, identifying a previous season (though deterministically "current")
     * @return void
     */
    public function testInjuriesCurrentPrevious(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-01-05 12:34:56');

        $response = $this->get('/nfl/injuries/2019-wildcard');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/injuries/2019-wildcard" '
            . 'data-date="2019-wildcard">Wildcard</a></dd>');

        $response->assertSee('<h3>Sunday 5th January</h3>
        <ul class="grid inline_list">
                            <li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                    <ul class="inline_list nfl_box game_list injuries clearfix">
                        <li class="team visitor row_nfl-MIN">
                            <span class="icon team-nfl-MIN">Vikings</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home row_nfl-NO">
                            <span class="icon team-nfl-NO">Saints</span>
                        </li>
                        <li class="info">10:05am PST, Mercedes-Benz Superdome</li>
                        <li class="list visitor"><ul class="inline_list">
                                                            <li class="head status">Questionable</li>
                        <li class="row-0 pos">DE</li>
            <li class="row-0 player"><a href="/nfl/players/ifeadi-odenigbo-6296">Ifeadi Odenigbo</a>'
            . '<span class="injury">Hamstring</span></li>
                                                            <li class="head status">Out</li>
                        <li class="row-1 pos">CB</li>
            <li class="row-1 player"><a href="/nfl/players/mackensie-alexander-4978">Mackensie Alexander</a>'
            . '<span class="injury">Knee</span></li>
                                                        <li class="row-0 pos">CB</li>
            <li class="row-0 player"><a href="/nfl/players/mike-hughes-6446">Mike Hughes</a>'
            . '<span class="injury">Neck</span></li>
                        </ul>
</li>
                        <li class="list home"><ul class="inline_list">
                                                            <li class="head status">Out</li>
                        <li class="row-0 pos">CB</li>
            <li class="row-0 player"><a href="/nfl/players/eli-apple-5008">Eli Apple</a>'
            . '<span class="injury">Ankle</span></li>
                                                        <li class="row-1 pos">FB</li>
            <li class="row-1 player"><a href="/nfl/players/zach-line-3516">Zach Line</a>'
            . '<span class="injury">Knee</span></li>
                        </ul>
</li>
                    </ul>
                </li>');
    }

    /**
     * A test of miscellaneous injuries controller actions
     * @return void
     */
    public function testInjuriesControllerMisc(): void
    {
        // Current season.
        $this->setTestDateTime('2020-09-09 12:34:56');
        $response = $this->get('/nfl/injuries/2020/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nfl/injuries/2020-week-1/switcher');
        // Previous season.
        $this->setTestDateTime('2020-03-01 12:34:56');
        $response = $this->get('/nfl/injuries/2019/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nfl/injuries/2019-superbowl/switcher');
    }
}
