<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class ZManageLogosTest extends MajorLeagueBaseCase
{
    /**
     * A test of the logo admin page as a guest user.
     * @return void
     */
    public function testUnderprivileged(): void
    {
        // Before being logged in - 401 Unauthorised.
        $response = $this->get('/nfl/manage/logos');
        $response->assertStatus(401);

        // Login as a (general) user - 403 Forbidden.
        $login = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $login->assertStatus(200);
        $login->assertJsonFragment(['success' => true]);
        $response = $this->get('/nfl/manage/logos');
        $response->assertStatus(403);
    }

    /**
     * A test of the logo page.
     * @return void
     */
    public function testLogos(): void
    {
        $login = $this->post('/login', [
            'username' => 'sports_admin',
            'password' => 'testlogindetails',
        ]);
        $login->assertStatus(200);
        $login->assertJsonFragment(['success' => true]);

        $response = $this->get('/nfl/manage/logos');
        $response->assertStatus(200);

        $response->assertSee('<h1>NFL Logos</h1>');
        $response->assertSee('<ul class="inline_list breadcrumbs"><li>DeBear Sports</li><li>NFL</li><li>Admin</li>'
            . '<li>Logos</li></ul>');
        $response->assertSee('<select id="subnav">
                            <option value="tiny" selected="selected">Tiny</option>
                            <option value="small" >Small</option>
                            <option value="medium" >Medium</option>
                            <option value="large" >Large</option>
                            <option value="narrow_small" >Narrow Small</option>
                            <option value="narrow_large" >Narrow Large</option>
                    </select>');
        $response->assertSee('<li class="grid-1-5 grid-tl-1-4 grid-tp-1-3 grid-m-1-2">');

        // Teams.
        $response->assertSee('<dl>
                    <dt class="row_nfl-ARI team-nfl-ARI">Arizona Cardinals</dt>
                        <dd class="team-nfl-ARI"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_nfl-ARI team-nfl-ARI">Arizona Cardinals</dt>
                        <dd class="team-nfl-small-ARI"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_nfl-ARI team-nfl-ARI">Arizona Cardinals</dt>
                        <dd class="team-nfl-medium-ARI"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_nfl-ARI team-nfl-ARI">Arizona Cardinals</dt>
                        <dd class="team-nfl-large-ARI"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_nfl-ARI team-nfl-ARI">Arizona Cardinals</dt>
                        <dd class="team-nfl-narrow_small-ARI"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_nfl-ARI team-nfl-ARI">Arizona Cardinals</dt>
                        <dd class="team-nfl-narrow_large-ARI"></dd>
                </dl>');

        // Groupings.
        $response->assertSee('<dl>
                    <dt class="row_league-NFL misc-nfl-NFL">National Football League</dt>
                        <dd class="misc-nfl-NFL"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_league-NFL misc-nfl-NFL">National Football League</dt>
                        <dd class="misc-nfl-large-NFL"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_league-NFL misc-nfl-NFL">National Football League</dt>
                        <dd class="misc-nfl-narrow_large-NFL"></dd>
                </dl>');
    }
}
