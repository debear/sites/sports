<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class IPlayersTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-03-01 12:34:56');

        $response = $this->get('/nfl/players');
        $response->assertStatus(200);

        $response->assertSee('<h1>NFL Players</h1>');

        // By Team.
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row_conf-AFC misc-nfl-narrow-small-AFC">AFC</dt>
                                                    <dd class="row-0 team-nfl-BAL icon">'
            . '<a href="/nfl/teams/baltimore-ravens#roster">Baltimore Ravens</a></dd>');

        // Name Search.
        $response->assertSee('<dl class="nfl_box clearfix">
            <dt class="row-head">Search By Name</dt>
            <dt class="row-1">Player&#39;s Name:</dt>
                <dd class="row-0 name"><input type="text" class="textbox"></dd>
                <dd class="row-0 button"><button class="btn_small btn_green">Search</button></dd>
                <dd class="results hidden"></dd>
        </dl>');

        // Birthdays.
        $response->assertSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
        $response->assertSee('<div>
                <ul class="inline_list">
                                            <li class="row-0">'
            . '<a href="/nfl/players/kevin-givens-7215">Kevin Givens</a> (23)</li>
                                            <li class="row-1">'
            . '<a href="/nfl/players/tyreek-hill-4947">Tyreek Hill</a> (26)</li>
                                    </ul>
            </div>');
    }

    /**
     * A test of the main list with no birthdays.
     * @return void
     */
    public function testListNoBirthdays(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-02-29 12:34:56');

        $response = $this->get('/nfl/players');
        $response->assertStatus(200);

        // Available sections.
        $response->assertSee('<h1>NFL Players</h1>');
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row-head">Search By Name</dt>');
        $response->assertDontSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
    }

    /**
     * A test of the player search.
     * @return void
     */
    public function testSearch(): void
    {
        // Input length.
        $response = $this->get('/nfl/players/search/');
        $response->assertStatus(404); // Must have at least one character.
        $response = $this->get('/nfl/players/search/a');
        $response->assertStatus(400);
        $response = $this->get('/nfl/players/search/abc');
        $response->assertStatus(200);
        $response->assertExactJson([]); // This should also be an empty list.

        // A search result.
        $response = $this->get('/nfl/players/search/stafford');
        $response->assertStatus(200);
        $response->assertExactJson([[
            'player_id' => 221,
            'first_name' => 'Matthew',
            'surname' => 'Stafford',
            'url' => '/nfl/players/matthew-stafford-221',
        ]]); // This should also be an empty list.
    }

    /**
     * A test of an individual's player page.
     * @return void
     */
    public function testIndividual(): void
    {
        // Switch to the 2019 season.
        $this->setTestConfig(['debear.setup.season.viewing' => 2019]);

        // Process the request.
        $response = $this->get('/nfl/players/aaron-rodgers-1439');
        $response->assertStatus(200);
        $response->assertSee('<h1 class="team-nfl-narrow_large-right-GB">Aaron Rodgers'
            . '<abbrev class="na" title="Not Applicable">NA</abbrev><span class="hidden-m jersey">#12</span>'
            . '<span class="hidden-m pos">QB</span></h1>');

        // Header.
        $response->assertSee('<ul class="inline_list summary">
            <li class="row-head">2019 Regular Season:</li>
                            <li class="stat grid-1-3">
                    <dl class="nfl_box">
                        <dt class="row-head">Yds</dt>
                            <dd class="row-1 value">4,002</dd>
                            <dd class="row-0 rank">11th</dd>
                    </dl>
                </li>');
        $response->assertSee('<dl class="details">
                <dt class="height">Height:</dt>
            <dd class="height">6&#39; 2&quot;</dd>');

        $response->assertSee('<select id="subnav">
                            <option value="home" selected="selected">Summary</option>
                            <option value="career" >Career Stats</option>
                            <option value="gamelog" >Game Log</option>
                    </select>');

        // Summary.
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-passing" data-default="0" data-total="16" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-1 team"><span class="team-nfl-GB">GB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2019</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-0 team"><span class="team-nfl-GB">GB</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                    <cell class="row-1 misc gp">2</cell>
                    <cell class="row-1 misc gs">2</cell>
                                                                                                '
            . '<cell class="row-1 passing atts">66</cell>
                                                    <cell class="row-1 passing cmp">47</cell>
                                                    <cell class="row-1 passing pct">71.2</cell>
                                                    <cell class="row-1 passing yards">569</cell>');
        $response->assertSee('<h3 class="games scrolltable-game-passing row-head">2019 Game-by-Game</h3>
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game scrolltable-game-passing" data-default="0" '
            . 'data-total="16" data-current="gs">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Conf Champs</cell>
                <cell class="row-1 opp">
                    @<span class="team-nfl-SF">SF</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-loss">L 37 &ndash; 20</span>
                    <a class="icon_game_boxscore" href="/nfl/schedule/2019-conference/packers-at-49ers-p302"></a>
                </cell>
            </row>');
        $response->assertSee('<row>
                    <cell class="row-1 misc gs"><span class="fa fa-check-circle"></span></cell>
                                                                                                '
            . '<cell class="row-1 passing atts">39</cell>
                                                    <cell class="row-1 passing cmp">31</cell>
                                                    <cell class="row-1 passing pct">79.5</cell>
                                                    <cell class="row-1 passing yards">326</cell>');
        $response->assertSee('<dl class="nfl_box awards clearfix">
    <dt class="row-head">Award History</dt>
                    <dt class="team-nfl-GB">2014</dt>
            <dd>Most Valuable Player (QB)</dd>
                    <dt class="team-nfl-GB">2011</dt>
            <dd>Most Valuable Player (QB)</dd>
    </dl>');
    }

    /**
     * A test of an individual's player career tab.
     * @return void
     */
    public function testIndividualTabsCareer(): void
    {
        // Switch to the 2019 season.
        $this->setTestConfig(['debear.setup.season.viewing' => 2019]);

        // Process the request.
        $response = $this->getTab('/nfl/players/aaron-rodgers-1439/career?group=passing');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-passing="true" '
            . 'data-defense="true" data-returns="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-passing" data-default="0" data-total="16" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-1 team"><span class="team-nfl-GB">GB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2019</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-0 team"><span class="team-nfl-GB">GB</span></cell>
            </row>
            </main>');
        $response->assertSee('<datalist>
            ' . '
            <top>
                ' . '
                                    <cell class="row-head group stats-passing">Passing</cell>
                                    <cell class="row-head group stats-rushing">Rushing</cell>
                                ' . '
                <cell class="row-head misc gp" title="Games Played">GP</cell>');
        $response->assertSee('<row>
                    <cell class="row-1 misc gp">2</cell>
                    <cell class="row-1 misc gs">2</cell>
                                                                                                '
            . '<cell class="row-1 passing atts">66</cell>
                                                    <cell class="row-1 passing cmp">47</cell>
                                                    <cell class="row-1 passing pct">71.2</cell>
                                                    <cell class="row-1 passing yards">569</cell>');
        $response->assertSee('<row>
                    <cell class="row-0 misc gp">16</cell>
                    <cell class="row-0 misc gs">16</cell>
                                                                                                '
            . '<cell class="row-0 passing atts">569</cell>
                                                    <cell class="row-0 passing cmp">353</cell>
                                                    <cell class="row-0 passing pct">62.0</cell>
                                                    <cell class="row-0 passing yards">4,002</cell>');
    }

    /**
     * A test of an individual's player gamelog tab.
     * @return void
     */
    public function testIndividualTabsGameLog(): void
    {
        // Switch to the 2019 season.
        $this->setTestConfig(['debear.setup.season.viewing' => 2019]);

        // Process the request.
        $response = $this->getTab('/nfl/players/aaron-rodgers-1439/gamelog?season=2019&group=passing');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-passing="true" '
            . 'data-defense="true" data-returns="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game '
            . 'scrolltable-game-passing" data-default="0" data-total="16" data-current="gs">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Conf Champs</cell>
                <cell class="row-1 opp">
                    @<span class="team-nfl-SF">SF</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-loss">L 37 &ndash; 20</span>
                    <a class="icon_game_boxscore" href="/nfl/schedule/2019-conference/packers-at-49ers-p302"></a>
                </cell>
            </row>');
        $response->assertSee('<datalist>
            ' . '
            <top>
                ' . '
                                    <cell class="row-head group stats-passing">Passing</cell>
                                    <cell class="row-head group stats-rushing">Rushing</cell>
                                ' . '
                <cell class="row-head misc gs" title="Started Game">GS</cell>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                    <cell class="row-1 misc gs"><span class="fa fa-check-circle"></span></cell>
                                                                                                '
            . '<cell class="row-1 passing atts">39</cell>
                                                    <cell class="row-1 passing cmp">31</cell>
                                                    <cell class="row-1 passing pct">79.5</cell>
                                                    <cell class="row-1 passing yards">326</cell>');
    }
}
