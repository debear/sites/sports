<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class GStatsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the stat leaders page.
     * @return void
     */
    public function testLeaders(): void
    {
        $response = $this->get('/nfl/stats');
        $response->assertStatus(200);
        $response->assertSee('<h1>2019 NFL Stat Leaders</h1>');

        // Passing.
        $response->assertSee('<dl class="nfl_box stat clearfix">
                <dt class="row-head name">Passing</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nfl-TB">'
            . '<a href="/nfl/players/jameis-winston-4612">Jameis Winston</a></span>
                            <span class="stat">5,109yds</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nfl-ATL"><a href="/nfl/players/matt-ryan-1016">Matt Ryan</a></span>
                            <span class="stat">4,466yds</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-nfl-SEA">'
            . '<a href="/nfl/players/russell-wilson-3102">Russell Wilson</a></span>
                            <span class="stat">4,111yds</span>
                        </dd>');

        // Interceptions.
        $response->assertSee('<dl class="nfl_box stat clearfix">
                <dt class="row-head name">Interceptions</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nfl-NE">'
            . '<a href="/nfl/players/stephon-gilmore-3068">Stephon Gilmore</a></span>
                            <span class="stat">6</span>
                        </dd>
                                                        <dt class="row-head pos">=</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-nfl-MIN">'
            . '<a href="/nfl/players/anthony-harris-4722">Anthony Harris</a></span>
                            <span class="stat">6</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">4</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-nfl-PIT"><a href="/nfl/players/joe-haden-2028">Joe Haden</a></span>
                            <span class="stat">5</span>
                        </dd>
                                                        <dt class="row-head pos">=</dt>
                        <dd class="row-0 entity">
                            <em>8 More Players Tied</em>
                            <span class="stat">5</span>
                        </dd>');
        $response->assertDontSee('<span class="icon team-nfl-PIT">'
            . '<a href="/nfl/players/minkah-fitzpatrick-6434">Minkah Fitzpatrick</a></span>
                            <span class="stat">5</span>');
        $response->assertDontSee('<span class="icon team-nfl-IND">'
            . '<a href="/nfl/players/darius-leonard-6370">Darius Leonard</a></span>
                            <span class="stat">5</span>');
        $response->assertDontSee('<span class="icon team-nfl-GB">'
            . '<a href="/nfl/players/kevin-king-5411">Kevin King</a></span>
                            <span class="stat">5</span>');
        $response->assertDontSee('<span class="icon team-nfl-TEN">'
            . '<a href="/nfl/players/kevin-byard-5123">Kevin Byard</a></span>
                            <span class="stat">5</span>');
        $response->assertDontSee('<span class="icon team-nfl-BAL"><span class="icon team-nfl-LA">'
            . '<a href="/nfl/players/marcus-peters-4443">Marcus Peters</a></span></span>
                            <span class="stat">5</span>');

        // Teams: Time of Possession.
        $response->assertSee('<dl class="nfl_box stat clearfix">
                <dt class="row-head name">Team Time of Possession</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nfl-BAL"><a href="/nfl/teams/baltimore-ravens">Ravens</a></span>
                            <span class="stat">34:47</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nfl-SF"><a href="/nfl/teams/san-francisco-49ers">49ers</a></span>
                            <span class="stat">31:37</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-nfl-GB"><a href="/nfl/teams/green-bay-packers">Packers</a></span>
                            <span class="stat">31:27</span>
                        </dd>');

        // Team lists.
        $response->assertSee('<dl class="nfl_box teams clearfix">
    <dt class="row-head">Sortable Stats by Team</dt>
            <dd ><a class="no_underline team-nfl-ARI" href="/nfl/stats/players/passing?team_id=ARI&season=2019&'
            . 'type=regular&stat=yards"></a></dd>');
        $response->assertSee('<dd class="split-point"><a class="no_underline team-nfl-LAC" '
            . 'href="/nfl/stats/players/passing?team_id=LAC&season=2019&type=regular&stat=yards"></a></dd>');
    }

    /**
     * A test of the controller handling.
     * @return void
     */
    public function testHandler(): void
    {
        // Invalid option.
        $response = $this->get('/nfl/stats/players/unknown');
        $response->assertStatus(404);

        // Default options.
        $response = $this->get('/nfl/stats/players/passing');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Passing Leaders</h1>');

        // Filters.
        $response->assertSee('<dl class="dropdown " data-id="filter_team"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>All NFL</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="">All NFL</li>
                            <li  data-id="ARI"><span class="icon team-nfl-ARI">ARI</span></li>');
        $response->assertSee('<dl class="dropdown " data-id="filter_season"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019 Regular Season</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="2019::playoff">2019 Playoffs</li>
                            <li class="sel" data-id="2019::regular">2019 Regular Season</li>
                    </ul>
    </dd>
</dl>');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Passing</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="player::passing">Individual Passing</li>
                            <li  data-id="player::rushing">Individual Rushing</li>
                            <li  data-id="player::receiving">Individual Receiving</li>
                            <li  data-id="player::kicking">Individual Kicking</li>
                            <li  data-id="player::tackles">Individual Tackle</li>
                            <li  data-id="player::pass-def">Individual Pass Defense</li>
                            <li  data-id="player::fumbles">Individual Fumble</li>
                            <li  data-id="player::kick-ret">Individual Kick Return</li>
                            <li  data-id="player::punts">Individual Punting</li>
                            <li  data-id="player::punt-ret">Individual Punt Return</li>
                            <li  data-id="team::yards">Team Yardage</li>
                            <li  data-id="team::scoring">Team Scoring</li>
                            <li  data-id="team::two-point">Team 2pt Conversion</li>
                            <li  data-id="team::kicking">Team Kicking</li>
                            <li  data-id="team::defense">Team Defense</li>
                            <li  data-id="team::turnovers">Team Turnover</li>
                            <li  data-id="team::punts">Team Punting</li>
                            <li  data-id="team::returns">Team Return</li>
                            <li  data-id="team::kickoffs">Team Kickoff</li>
                            <li  data-id="team::plays">Team Play</li>
                            <li  data-id="team::downs">Team Down</li>
                            <li  data-id="team::misc">Team Miscellaneous</li>
                    </ul>
    </dd>
</dl>');

        // Stat selection.
        $response->assertSee('<scrolltable class="scroll-x scrolltable-player-passing" data-default="4"'
            . ' data-total="11" data-current="yards">');
    }

    /**
     * A test of the individual passing stats page.
     * @return void
     */
    public function testPlayerPassing(): void
    {
        $response = $this->get('/nfl/stats/players/passing?season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Passing Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Passing</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-TB">'
            . '<a href="/nfl/players/jameis-winston-4612">J. Winston</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-DAL">'
            . '<a href="/nfl/players/dak-prescott-4864">D. Prescott</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual rushing stats page.
     * @return void
     */
    public function testPlayerRushing(): void
    {
        $response = $this->get('/nfl/stats/players/rushing?season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Rushing Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Rushing</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-TEN">'
            . '<a href="/nfl/players/derrick-henry-5126">D. Henry</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-CLE">'
            . '<a href="/nfl/players/nick-chubb-6290">N. Chubb</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual receiving stats page.
     * @return void
     */
    public function testPlayerReceiving(): void
    {
        $response = $this->get('/nfl/stats/players/receiving?season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Receiving Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Receiving</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-NO">'
            . '<a href="/nfl/players/michael-thomas-5005">M. Thomas</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-ATL">'
            . '<a href="/nfl/players/julio-jones-2556">J. Jones</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual kicking stats page.
     * @return void
     */
    public function testPlayerKicking(): void
    {
        $response = $this->get('/nfl/stats/players/kicking?season=2019&type=regular&stat=pts');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Kicking Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Kicking</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-KC">'
            . '<a href="/nfl/players/harrison-butker-5336">H. Butker</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-NO">'
            . '<a href="/nfl/players/will-lutz-5157">W. Lutz</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual tackles stats page.
     * @return void
     */
    public function testPlayerTackle(): void
    {
        $response = $this->get('/nfl/stats/players/tackles?season=2019&type=regular&stat=sacks');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Tackle Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Tackle</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-ARI">'
            . '<a href="/nfl/players/chandler-jones-3027">C. Jones</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">=</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-TB">'
            . '<a href="/nfl/players/shaquil-barrett-4210">S. Barrett</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual pass defense stats page.
     * @return void
     */
    public function testPlayerPassDefense(): void
    {
        $response = $this->get('/nfl/stats/players/pass-def?season=2019&type=regular&stat=int');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Pass Defense Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Pass Defense</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-NE">'
            . '<a href="/nfl/players/stephon-gilmore-3068">S. Gilmore</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">=</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-MIN">'
            . '<a href="/nfl/players/anthony-harris-4722">A. Harris</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual fumbles stats page.
     * @return void
     */
    public function testPlayerFumble(): void
    {
        $response = $this->get('/nfl/stats/players/fumbles?season=2019&type=regular&stat=num_rec');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Fumble Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Fumble</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-PHI">'
            . '<a href="/nfl/players/carson-wentz-5052">C. Wentz</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-MIA">'
            . '<a href="/nfl/players/ryan-fitzpatrick-1527">R. Fitzpatrick</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual kick return stats page.
     * @return void
     */
    public function testPlayerKickReturn(): void
    {
        $response = $this->get('/nfl/stats/players/kick-ret?season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Kick Return Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Kick Return</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-CHI">'
            . '<a href="/nfl/players/cordarrelle-patterson-3517">C. Patterson</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-WSH">'
            . '<a href="/nfl/players/steven-sims-7093">S. Sims</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual punts stats page.
     * @return void
     */
    public function testPlayerPunting(): void
    {
        $response = $this->get('/nfl/stats/players/punts?season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Punting Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Punting</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-NYJ">'
            . '<a href="/nfl/players/lac-edwards-5019">L. Edwards</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-WSH">'
            . '<a href="/nfl/players/tress-way-4153">T. Way</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual punt return stats page.
     * @return void
     */
    public function testPlayerPuntReturn(): void
    {
        $response = $this->get('/nfl/stats/players/punt-ret?season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Punt Return Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Punt Return</span>
    </dt>
');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-NO">'
            . '<a href="/nfl/players/deonte-harris-6975">D. Harris</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-CHI">'
            . '<a href="/nfl/players/tarik-cohen-5345">T. Cohen</a></span></cell>
            </row>');
    }

    /**
     * A test of the team yardage stats page.
     * @return void
     */
    public function testTeamYardage(): void
    {
        $response = $this->get('/nfl/stats/teams/yards?season=2019&type=regular&stat=yards_total');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Yardage Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Yardage</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-DAL">'
            . '<a href="/nfl/teams/dallas-cowboys">Cowboys</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-BAL">'
            . '<a href="/nfl/teams/baltimore-ravens">Ravens</a></span></cell>
            </row>');
    }

    /**
     * A test of the team scoring stats page.
     * @return void
     */
    public function testTeamScoring(): void
    {
        $response = $this->get('/nfl/stats/teams/scoring?season=2019&type=regular&stat=num_total');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Scoring Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Scoring</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-BAL">'
            . '<a href="/nfl/teams/baltimore-ravens">Ravens</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-TB">'
            . '<a href="/nfl/teams/tampa-bay-buccaneers">Buccaneers</a></span></cell>
            </row>');
    }

    /**
     * A test of the team 2pt conversion stats page.
     * @return void
     */
    public function testTeam2ptConversion(): void
    {
        $response = $this->get('/nfl/stats/teams/two-point?season=2019&type=regular&stat=total_pct');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL 2pt Conversion Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team 2pt Conversion</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-ARI">'
            . '<a href="/nfl/teams/arizona-cardinals">Cardinals</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">=</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-BUF">'
            . '<a href="/nfl/teams/buffalo-bills">Bills</a></span></cell>
            </row>');
    }

    /**
     * A test of the team kicking stats page.
     * @return void
     */
    public function testTeamKicking(): void
    {
        $response = $this->get('/nfl/stats/teams/kicking?season=2019&type=regular&stat=fg_pct');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Kicking Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Kicking</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-JAX">'
            . '<a href="/nfl/teams/jacksonville-jaguars">Jaguars</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-BAL">'
            . '<a href="/nfl/teams/baltimore-ravens">Ravens</a></span></cell>
            </row>');
    }

    /**
     * A test of the team defense stats page.
     * @return void
     */
    public function testTeamDefense(): void
    {
        $response = $this->get('/nfl/stats/teams/defense?season=2019&type=regular&stat=sacks');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Defense Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Defense</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-MIA">'
            . '<a href="/nfl/teams/miami-dolphins">Dolphins</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-SEA">'
            . '<a href="/nfl/teams/seattle-seahawks">Seahawks</a></span></cell>
            </row>');
    }

    /**
     * A test of the team turnover stats page.
     * @return void
     */
    public function testTeamTurnover(): void
    {
        $response = $this->get('/nfl/stats/teams/turnovers?season=2019&type=regular&stat=total');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Turnover Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Turnover</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-NO">'
            . '<a href="/nfl/teams/new-orleans-saints">Saints</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-GB">'
            . '<a href="/nfl/teams/green-bay-packers">Packers</a></span></cell>
            </row>');
    }

    /**
     * A test of the team punting stats page.
     * @return void
     */
    public function testTeamPunting(): void
    {
        $response = $this->get('/nfl/stats/teams/punts?season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Punting Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Punting</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-NYJ">'
            . '<a href="/nfl/teams/new-york-jets">Jets</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-WSH">'
            . '<a href="/nfl/teams/washington-redskins">Redskins</a></span></cell>
            </row>');
    }

    /**
     * A test of the team return stats page.
     * @return void
     */
    public function testTeamReturn(): void
    {
        $response = $this->get('/nfl/stats/teams/returns?season=2019&type=regular&stat=ko_yards');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Return Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Return</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-NYG">'
            . '<a href="/nfl/teams/new-york-giants">Giants</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-CIN">'
            . '<a href="/nfl/teams/cincinnati-bengals">Bengals</a></span></cell>
            </row>');
    }

    /**
     * A test of the team kickoff stats page.
     * @return void
     */
    public function testTeamKickoff(): void
    {
        $response = $this->get('/nfl/stats/teams/kickoffs?season=2019&type=regular&stat=touchback');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Kickoff Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Kickoff</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-TB">'
            . '<a href="/nfl/teams/tampa-bay-buccaneers">Buccaneers</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-NO">'
            . '<a href="/nfl/teams/new-orleans-saints">Saints</a></span></cell>
            </row>');
    }

    /**
     * A test of the team play stats page.
     * @return void
     */
    public function testTeamPlay(): void
    {
        $response = $this->get('/nfl/stats/teams/plays?season=2019&type=regular&stat=total');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Play Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Play</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-PHI">'
            . '<a href="/nfl/teams/philadelphia-eagles">Eagles</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-ATL">'
            . '<a href="/nfl/teams/atlanta-falcons">Falcons</a></span></cell>
            </row>');
    }

    /**
     * A test of the team down stats page.
     * @return void
     */
    public function testTeamDown(): void
    {
        $response = $this->get('/nfl/stats/teams/downs?season=2019&type=regular&stat=num_1st');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Down Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Down</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-BAL">'
            . '<a href="/nfl/teams/baltimore-ravens">Ravens</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-DAL">'
            . '<a href="/nfl/teams/dallas-cowboys">Cowboys</a></span></cell>
            </row>');
    }

    /**
     * A test of the team miscellaneous stats page.
     * @return void
     */
    public function testTeamMiscellaneous(): void
    {
        $response = $this->get('/nfl/stats/teams/misc?season=2019&type=regular&stat=time_of_poss');
        $response->assertStatus(200);
        $response->assertSee('<h1>NFL Miscellaneous Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Miscellaneous</span>
    </dt>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-BAL">'
            . '<a href="/nfl/teams/baltimore-ravens">Ravens</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nfl-PHI">'
            . '<a href="/nfl/teams/philadelphia-eagles">Eagles</a></span></cell>
            </row>');
    }

    /**
     * A test of a single team players batting stats page.
     * @return void
     */
    public function testTeamStats(): void
    {
        // Invalid team arguments (all ignored, NFL leaders listed).
        $response = $this->get('/nfl/stats/players/passing?team_id=&season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Stats &raquo; Players &raquo; Passing</title>');
        $response->assertSee('<h1>NFL Passing Leaders</h1>');
        $response = $this->get('/nfl/stats/players/passing?team_id=SD&season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Stats &raquo; Players &raquo; Passing</title>');
        $response->assertSee('<h1>NFL Passing Leaders</h1>');

        // Correct list.
        $response = $this->get('/nfl/stats/players/passing?team_id=GB&season=2019&type=regular&stat=yards');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Stats &raquo; Packers &raquo; Passing</title>');
        $response->assertSee('<h1><span class="hidden-m hidden-tp">Green Bay</span> Packers Passing Leaders</h1>');

        $response->assertDontSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-TB">'
            . '<a href="/nfl/players/jameis-winston-4612">J. Winston</a></span></cell>
            </row>');
        $response->assertSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nfl-GB">'
            . '<a href="/nfl/players/aaron-rodgers-1439">A. Rodgers</a></span></cell>
            </row>');
    }
}
