<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class EStandingsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the default season.
     * @return void
     */
    public function testDefault(): void
    {
        $response = $this->get('/nfl/standings');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; 2019 Standings</title>');
        $response->assertSee('<li class="sel" data-id="2019">2019</li>');
        $response->assertSee('<h3 class="row_conf-AFC misc-nfl-narrow-small-AFC '
            . 'misc-nfl-narrow-small-right-AFC">American Football Conference</h3>');
        $response->assertSee('<dt class="row_conf-AFC team head">AFC East</dt>
    ' . '
                    <dd class="row-1 team">
            <span class="icon team-nfl-NE">
                <a href="/nfl/teams/new-england-patriots" >New England Patriots</a>
                            </span>
                            <sup>y</sup>
                    </dd>');
        $response->assertSee('<dl class="standings div">
        ' . '
                    <dt class="row_conf-AFC col-first col w  ">W</dt>
                    <dt class="row_conf-AFC  col l  ">L</dt>
                    <dt class="row_conf-AFC  col t  ">T</dt>
                    <dt class="row_conf-AFC  col pct  ">Win %</dt>
                    <dt class="row_conf-AFC  col pf  ">For</dt>
                    <dt class="row_conf-AFC  col pa  ">Agst</dt>
                    <dt class="row_conf-AFC  col pd  ">Diff</dt>
                    <dt class="row_conf-AFC  col home  ">Home</dt>
                    <dt class="row_conf-AFC  col visitor  ">Visitor</dt>
                    <dt class="row_conf-AFC  col conf  ">Conf</dt>
                    <dt class="row_conf-AFC  col div  ">Div</dt>
                    <dt class="row_conf-AFC  col streak  ">Streak</dt>
                ' . '
                                                <dd class="row-1 col-first col w ">');
    }

    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/nfl/standings/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; 2019 Standings</title>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/nfl/standings/2019/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; NFL &raquo; 2019 Standings</title>');
        $response->assertSee('<h1>2019 Standings</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/nfl/standings/2000');
        $response->assertStatus(404);
        $response = $this->get('/nfl/standings/2039');
        $response->assertStatus(404);
    }
}
