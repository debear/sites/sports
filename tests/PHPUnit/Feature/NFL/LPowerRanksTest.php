<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class LPowerRanksTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main page.
     * @return void
     */
    public function testMain(): void
    {
        $response = $this->get('/nfl/power-ranks');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Power Ranks</title>');
        $response->assertSee('<li class="sel" data-href="2019/superbowl" data-id="superbowl">Super Bowl</li>');
        $response->assertDontSee('<li  data-href="2019/probowl" data-id="probowl">Pro Bowl</li>');
        $response->assertSee('<dl class="power_ranks ranks-superbowl">');
        $response->assertDontSee('<dd class="dates">Covering games up to');

        $response->assertSee('<dt class="row-1 rank">1</dt>
    ' . '
                    <dd class="row-1 change">
            <span class="hidden-m"><strong>Last Week:</strong> 1</span>
            <span class="icon_pos_nc">NC</span>
        </dd>
        ' . '
    <dd class="row-1 team">
        <span class="logo hidden-m team-nfl-narrow_large-KC"></span>
        <span class="logo hidden-t hidden-d team-nfl-narrow_small-KC"></span>
        <h3><a href="/nfl/teams/kansas-city-chiefs">Kansas City Chiefs</a></h3>
    </dd>
    ' . '
    <dd class="row-1 schedule">
        <dl>
            <dt>This week:</dt>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                        31 &ndash; 20
                                                    </span>
                        v
                        <span class="opp icon team-nfl-SF">SF</span>
                                            </dd>
                        </dl>
    </dd>');
        $response->assertDontSee('<dt class="row-1 rank">33</dt>');
    }

    /**
     * A test when only the season was passed.
     * @return void
     */
    public function testSeasonOnly(): void
    {
        // A valid season.
        $response = $this->get('/nfl/power-ranks/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Power Ranks</title>');
        $response->assertSee('<li class="sel" data-href="2019/superbowl" data-id="superbowl">Super Bowl</li>');

        // A future season.
        $response = $this->get('/nfl/power-ranks/2020');
        $response->assertStatus(404);
    }

    /**
     * A test when specifying the week.
     * @return void
     */
    public function testSpecifyingWeek(): void
    {
        // A valid week.
        $response = $this->get('/nfl/power-ranks/2019/superbowl');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Power Ranks &raquo; Super Bowl</title>');
        $response->assertSee('<li class="sel" data-href="2019/superbowl" data-id="superbowl">Super Bowl</li>');

        // An uncalced week.
        $response = $this->get('/nfl/power-ranks/2019/23');
        $response->assertStatus(404);

        // A non-existant week.
        $response = $this->get('/nfl/power-ranks/2019/99');
        $response->assertStatus(404);
    }

    /**
     * A test of the first week.
     * @return void
     */
    public function testFirstWeek(): void
    {
        $response = $this->get('/nfl/power-ranks/2019/week-1');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Power Ranks &raquo; Week 1</title>');
        $response->assertSee('<li class="sel" data-href="2019/week-1" data-id="week-1">Week 1</li>');
        $response->assertDontSee('<strong>Last Week:</strong>');
    }
}
