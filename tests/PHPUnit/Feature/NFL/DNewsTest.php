<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class DNewsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the NFL news.
     * @return void
     */
    public function testNews(): void
    {
        $response = $this->get('/nfl/news');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/2020-unaccounted-carries-062626151.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">2020 Unaccounted For Carries</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/chicago-youth-gets-super-surprise-goodell-barkley-060431345--nfl.html?'
            . 'src=rss" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Chicago youth gets '
            . 'Super surprise from Goodell, Barkley</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/2020-available-targets-air-yards-054012483.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">2020 Available Targets &amp; Air '
            . 'Yards</a></h4>');

        $response->assertSee('<next class="onclick_target prev-next mini"><i class="fas fa-chevron-circle-right"></i>'
            . '</next>
    <bullets>
                    <bullet class="onclick_target" data-ref="1">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>
                    <bullet class="onclick_target" data-ref="2">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>');

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/49ers-credit-chiefs-great-play-034037977.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">49ers credit Chiefs for &#39;great&#39; '
            . 'play call on crucial third-and-15 play</a></h4>');
        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/patrick-mahomes-chiefs-super-bowl-parade-003605579.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Patrick Mahomes is the King '
            . 'of Kansas City, and the Super Bowl parade was his coronation</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/travis-kelce-flames-dee-ford-channels-brother-jason-in-rowdy-super-bowl-'
            . 'parade-speech-003546013.html?src=rss" rel="noopener noreferrer" target="_blank" title="Opens in new '
            . 'tab/window">Travis Kelce flames Dee Ford, channels brother Jason in rowdy Super Bowl parade '
            . 'speech</a></h4>');

        $response->assertSee('<li class="page_box unshaded_row ">
            <strong>1</strong>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="12"><em>12</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');
    }

    /**
     * A test of a specific NFL news page.
     * @return void
     */
    public function testNewsPage(): void
    {
        // Valid page.
        $response = $this->get('/nfl/news?page=2');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/travis-kelce-flames-dee-ford-channels-brother-jason-in-rowdy-super-bowl-'
            . 'parade-speech-003546013.html?src=rss" rel="noopener noreferrer" target="_blank" title="Opens in new '
            . 'tab/window">Travis Kelce flames Dee Ford, channels brother Jason in rowdy Super Bowl parade '
            . 'speech</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/live-patriots-epic-comeback-vs-230220494.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">Re-live Patriots&#39; epic comeback vs. '
            . 'Falcons with best Super Bowl LI photos</a></h4>');

        $response->assertSee('<li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="1">1</a>
        </li>
            <li class="page_box unshaded_row ">
            <strong>2</strong>
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="3">3</a>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="12"><em>12</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="3">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');

        // Error.
        $response = $this->get('/nfl/news?page=13');
        $response->assertStatus(404);
    }
}
