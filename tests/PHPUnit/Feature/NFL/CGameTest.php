<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class CGameTest extends MajorLeagueBaseCase
{
    /**
     * A test of a single game
     * @return void
     */
    public function testCurrent(): void
    {
        $response = $this->get('/nfl/schedule/2019-superbowl/49ers-at-chiefs-p401');
        $response->assertStatus(200);

        $response->assertSee('<ul class="inline_list nfl_box game summary">
    <li class="row-head info">
        <span class="status">Final</span>
        <span class="venue">Hard Rock Stadium</span>
    </li>
    <li class="title">
        Super Bowl LIV, Sunday 2nd February
    </li>');
        $response->assertSee('<li class="row-0 att">Attendance: 62,417</li>');
        $response->assertDontSee('<li class="summary">');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="unsel" data-tab="drivechart"><span class="icon_game_drives">Drive Chart</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="scoring"><span class="icon_game_scoring">Scoring</span></item>
                    <sep class="mid"></sep>
            <item class="sel" data-tab="boxscore"><span class="icon_game_boxscore">Boxscore</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="stats"><span class="icon_game_stats">Game Stats</span></item>
                <sep class="right"></sep>
    </desk>');

        // The individual tabs.
        $this->testGameDriveChart($response);
        $this->testGameScoring($response);
        $this->testGameBoxscore($response);
        $this->testGameStats($response);
    }

    /**
     * Test the game's drive chart tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameDriveChart(TestResponse $response): void
    {
        $response->assertSee('<li class="endzone visitor">
            <span class="team-nfl-small-SF team-nfl-small-right-SF">49ERS</span>
        </li>');
        $response->assertSee('<li class="mid misc-nfl-large-NFL"></li>');
        $response->assertDontSee('<li class="mid team-nfl-large-KC"></li>');
        $response->assertSee('<li class="endzone home">
            <span class="team-nfl-small-KC team-nfl-small-right-KC">CHIEFS</span>
        </li>');

        $response->assertSee('<li class="drive drive-0 arrow_left">
                        <div class="bar row_nfl-KC"></div>
                        <div class="summary hidden">
                            <span class="team-nfl-KC">Chiefs</span>:
                            3 plays,
                            7 yards;
                            Time of Possession: 1:05;
                            Result: Punt
                        </div>
                    </li>
                                                        <li class="drive drive-1 arrow_right">
                        <div class="bar row_nfl-SF"></div>
                        <div class="summary hidden">
                            <span class="team-nfl-SF">49ers</span>:
                            10 plays,
                            62 yards;
                            Time of Possession: 5:58;
                            Result: Field Goal
                        </div>');
        $response->assertSee('.field-drive-chart .drive-0 .bar { left: 544px; width: 48px; }
            .field-drive-chart .drive-1 .bar { left: 144px; width: 488px; }');

        $response->assertSee('<dt class="row-head num">1</dt>
                    <dd class="row-0 start_time">Q1 15:00</dd>
                    <dd class="row-0 top">1:05</dd>
                    <dd class="row-0 obtained">Kickoff</dd>
                    <dd class="row-0 start_pos">Own 26</dd>
                    <dd class="row-0 end_pos">Own 33</dd>
                    <dd class="row-0 plays">3</dd>
                    <dd class="row-0 yards">7</dd>
                    <dd class="row-0 result">Punt</dd>');
        $response->assertSee('<dt class="row-head num">6</dt>
                    <dd class="row-1 start_time">Q3 05:23</dd>
                    <dd class="row-1 top">2:48</dd>
                    <dd class="row-1 obtained">Interception</dd>
                    <dd class="row-1 start_pos">Own 45</dd>
                    <dd class="row-1 end_pos">Endzone</dd>
                    <dd class="row-1 plays">6</dd>
                    <dd class="row-1 yards">55</dd>
                    <dd class="row-1 result">Touchdown</dd>');
    }

    /**
     * Test the game's scoring tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameScoring(TestResponse $response): void
    {
        $response->assertSee('<dt class="row-head type">Scoring</dt>
            <dt class="row-head period">1st Quarter</dt>
                                <dd class="row-0 team-nfl-SF time">07:57</dd>
            <dd class="row-0 play">
                <div class="score"><span class="visitor team-nfl-SF">3</span> &ndash; '
            . '<span class="home team-nfl-right-KC">0</span></div>
                <a href="/nfl/players/robbie-gould-1401">Robbie Gould</a> 38yd field goal
            </dd>
                                <dd class="row-1 team-nfl-KC time">00:31</dd>');
    }

    /**
     * Test the game's boxscore tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameBoxscore(TestResponse $response): void
    {
        $response->assertSee('<li class="cell section passer row_nfl-SF">Passing</li>
        <li class="static passer row-pad-0">
            <ul class="inline_list">
                <li class="cell jersey row_nfl-SF">#</li>
                <li class="cell cell-head player row_nfl-SF">Player</li>

                                                        <li class="cell jersey row_nfl-SF">10</li>
                    <li class="cell name row-0"><a href="/nfl/players/jimmy-garoppolo-4002">'
            . '<span class="name-short">J. Garoppolo</span><span class="name-full">Jimmy Garoppolo</span></a></li>
                            </ul>
        </li>
        <li class="scroll passer row-pad-0">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat_wide row_nfl-SF">Atts</li>
                <li class="cell cell-head stat row_nfl-SF">Yds</li>
                <li class="cell cell-head stat row_nfl-SF">TD</li>
                <li class="cell cell-head stat row_nfl-SF">Int</li>
                <li class="cell cell-head stat row_nfl-SF">Long</li>
                <li class="cell cell-head stat row_nfl-SF">Sack</li>
                <li class="cell cell-head stat_wide row_nfl-SF">Rating</li>

                                                        <li class="cell cell-first stat_wide row-0">20 / 31</li>
                    <li class="cell stat row-0">219</li>
                    <li class="cell stat row-0">1</li>
                    <li class="cell stat row-0">2</li>
                    <li class="cell stat row-0">26</li>
                    <li class="cell stat row-0">1</li>
                    <li class="cell stat_wide row-0">69.2</li>
                            </ul>
        </li>');

        $response->assertSee('<li class="cell section punt_ret row_nfl-KC">Punt Returns</li>
        <li class="static punt_ret row-pad-0">
            <ul class="inline_list">
                <li class="cell jersey row_nfl-KC">#</li>
                <li class="cell cell-head player row_nfl-KC">Player</li>

                                                        <li class="cell jersey row_nfl-KC">17</li>
                    <li class="cell name row-0"><a href="/nfl/players/mecole-hardman-6891">'
            . '<span class="name-short">M. Hardman</span><span class="name-full">Mecole Hardman</span></a></li>
                            </ul>
        </li>
        <li class="scroll punt_ret row-pad-0">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat row_nfl-KC">Num</li>
                <li class="cell cell-head stat row_nfl-KC">Yds</li>
                <li class="cell cell-head stat row_nfl-KC">Avg</li>
                <li class="cell cell-head stat row_nfl-KC">Long</li>
                <li class="cell cell-head stat row_nfl-KC">TD</li>
                <li class="cell cell-head stat row_nfl-KC">FC</li>

                                                        <li class="cell cell-first stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">&ndash;</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">2</li>
                            </ul>
        </li>');

        $response->assertSee('<ul class="inline_list nfl_box officials clearfix">
    <li class="row-head">Officials</li>

                    <li class="row-0">Referee:</li>
        <li class="row-1">#52 Bill Vinovich</li>
                    <li class="row-0">Umpire:</li>
        <li class="row-1">#20 Barry Anderson</li>
                    <li class="row-0">Head Linesman:</li>
        <li class="row-1">#79 Kent Payne</li>
                    <li class="row-0">Line Judge:</li>
        <li class="row-1">#101 Carl Johnson</li>
                    <li class="row-0">Side Judge:</li>
        <li class="row-1">#41 Boris Cheek</li>
                    <li class="row-0">Field Judge:</li>
        <li class="row-1">#72 Michael Banks</li>
                    <li class="row-0">Back Judge:</li>
        <li class="row-1">#12 Gregory Steed</li>
                    <li class="row-0">Replay Official:</li>
        <li class="row-1">Michael Chase</li>
    </ul>');
    }

    /**
     * Test the game's stats tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameStats(TestResponse $response): void
    {
        $response->assertSee('<dl class="nfl_box comparison clearfix">
    <dt class="row-head">Team Stats</dt>
    <dt class="team visitor row_nfl-SF team-nfl-SF"><span class="hidden-m hidden-tp">San Francisco</span> 49ers</dt>
    <dt class="team home row_nfl-KC team-nfl-KC"><span class="hidden-m hidden-tp">Kansas City</span> Chiefs</dt>

                        <dt class="row-0 section">Yards</dt>
        ' . '
                    <dd class="row-0 label">Total</dd>
<dd class="row-1 stat stat-yards-total">
    <span class="visitor">351</span>
    <div class="bar visitor row_nfl-SF"></div>
    <div class="bars">
        <div class="visitor row_nfl-SF"></div>
        <div class="home row_nfl-KC"></div>
        <div class="gap row_nfl-KC"></div>
    </div>
    <div class="bar home row_nfl-KC"></div>
    <span class="home">397</span>
</dd>');
        $response->assertSee('.subnav-stats .stat-yards-total .bars .visitor { width: 46.93%; }
        .subnav-stats .stat-yards-total .bars .home { width: 53.07%; }');
        $response->assertSee('<dd class="row-0 label">Fumbles Lost</dd>
<dd class="row-1 stat stat-turnovers-fumbles-lost">
    <span class="visitor">1</span>
    <div class="bar visitor row_nfl-SF"></div>
    <div class="bars">
        <div class="visitor row_nfl-SF"></div>
        <div class="home row_nfl-KC"></div>
        <div class="gap row_nfl-KC"></div>
    </div>
    <div class="bar home row_nfl-KC"></div>
    <span class="home">1</span>
</dd>');
        $response->assertSee('.subnav-stats .stat-turnovers-fumbles-lost .bars .visitor { width: 50.00%; }
        .subnav-stats .stat-turnovers-fumbles-lost .bars .home { width: 50.00%; }');
    }

    /**
     * A test of controller actions on invalid parameters
     * @return void
     */
    public function testInvalid(): void
    {
        // Invalid date.
        $response = $this->get('/nfl/schedule/2020-week-0/away-at-home-r1');
        $response->assertStatus(404);

        // Invalid game.
        $response = $this->get('/nfl/schedule/2020-week-1/away-at-home-r1');
        $response->assertStatus(404);
    }
}
