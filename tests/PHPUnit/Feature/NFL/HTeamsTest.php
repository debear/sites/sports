<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class HTeamsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the team list page.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/nfl/teams');
        $response->assertStatus(200);

        $response->assertSee('<h1>Teams</h1>');
        $response->assertSee('<dt class="row_conf-AFC misc-nfl-narrow-small-AFC '
            . 'misc-nfl-narrow-small-right-AFC conf">American Football Conference</dt>');
        $response->assertSee('<dt class="row_conf-AFC div">AFC East</dt>');

        // A team from this season.
        $response->assertSee('<dd class="row-1 team-nfl-small-WSH logo"></dd>
                    <dd class="row-1 name">Washington Redskins</dd>
                    <dd class="row-1 info">
                        <ul class="inline_list">
                            <li><a href="/nfl/teams/washington-redskins">Home</a></li>'
            . '<li><a href="/nfl/teams/washington-redskins#schedule">Schedule</a></li>'
            . '<li><a href="/nfl/teams/washington-redskins#roster">Roster</a></li>
                        </ul>
                    </dd>');

        // But not a team from a different season.
        $response->assertDontSee('<dd class="row-1 team-nfl-small-WSH logo"></dd>
                    <dd class="row-1 name">Washington Football Team</dd>
                    <dd class="row-1 info">
                        <ul class="inline_list">
                            <li><a href="/nfl/teams/washington-football-team">Home</a></li>'
            . '<li><a href="/nfl/teams/washington-football-team#schedule">Schedule</a></li>'
            . '<li><a href="/nfl/teams/washington-football-team#roster">Roster</a></li>
                        </ul>
                    </dd>');
    }

    /**
     * A test of an individual team page.
     * @return void
     */
    public function testIndividual(): void
    {
        $response = $this->get('/nfl/teams/green-bay-packers');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; Teams &raquo; Green Bay Packers</title>');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="schedule">Schedule</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="roster">Roster</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="standings">Standings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="power-ranks">Power Rankings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="history">History</item>
                <sep class="right"></sep>
    </desk>');

        $this->individualHeader($response);
        $this->individualSchedule($response);
        $this->individualStandings($response);
        $this->individualStats($response);
        $this->individualLeaders($response);
        $this->individualMisc($response);
    }

    /**
     * A test of the header component of an individual team page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualHeader(TestResponse $response): void
    {
        $response->assertDontSee('<div class="favourite">');
        $response->assertSee('<dl class="inline_list header clearfix">
    ' . '
            <dt class="standing">1st, NFC North:</dt>
            <dd class="standing">13&ndash;3</dd>
        ' . '
                        <dt class="prev game">Last Game:</dt>
            <dd class="prev game">
                NFC Conf
                                    <span class="result-loss">L 37 &ndash; 20</span>
                                @
                <span class="team-nfl-SF icon">SF</span>
            </dd>
        ' . '
        </dl>');
    }

    /**
     * A test of the schedule component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualSchedule(TestResponse $response): void
    {
        $response->assertSee('<dl class="inline_list nfl_box schedule  clearfix">
    <dt class="row-head title">2019 Schedule</dt>
            ' . '
        ' . '
        ' . '
                <dt class="row-head week">Week 1</dt>
            <dd class="row-1 opp team-nfl-narrow_small-right-CHI">@</dd>
            <dd class="status row-1">
                <span class="team-name"><a class="hidden-t hidden-d" href="/nfl/teams/chicago-bears">'
        . '<span class="hidden">Chicago </span>Bears</a></span>
                                    <span class="result-win">W 10 &ndash; 3</span>
                    <a class="no_underline icon_game_boxscore" '
        . 'href="/nfl/schedule/2019-week-1/packers-at-bears-r101"></a>
                            </dd>');
    }

    /**
     * A test of the standings component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStandings(TestResponse $response): void
    {
        $response->assertSee('<li class="div row_conf-NFC misc-nfl-narrow-small-right-NFC">NFC North</li>

    ' . '
    ' . '
    ' . '
                    <li class="row_nfl-GB team">
            <span class="team-nfl-GB">
                <a href="/nfl/teams/green-bay-packers">Packers</a>
            </span>
                            <sup>z</sup>
                                        <div class="row_nfl-GB col">.812</div>
                            <div class="row_nfl-GB col">13&ndash;3</div>
                    </li>');
    }

    /**
     * A test of the stats component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStats(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list stats clearfix">
        <li class="row-head title">Team Stats</li>
                    <li class="cell nfl_box">
                <dl class="clearfix">
                    <dt class="row-head">Total Yards</dt>
                        <dd class="rank">18th</dd>
                        <dd class="value"><span>5,528<unit>yds</unit></span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell nfl_box">
                <dl class="clearfix">
                    <dt class="row-head">3rd Down</dt>
                        <dd class="rank">23rd</dd>
                        <dd class="value"><span>36.0</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the team leader component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualLeaders(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list leaders clearfix">
        <li class="row-head title">Team Leaders</li>
                    <li class="cell pass nfl_box">
                <dl>
                    <dt class="row-head">Passing</dt>
                        <dd class="name"><a href="/nfl/players/aaron-rodgers-1439">A. Rodgers</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Aaron Rodgers"></dd>
                        <dd class="value"><span>4,002<unit>yds</unit></span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell sacks nfl_box">
                <dl>
                    <dt class="row-head">Sacks</dt>
                        <dd class="name"><a href="/nfl/players/za-darius-smith-4303">ZD. Smith</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Za&#39;Darius Smith"></dd>
                        <dd class="value"><span>13.5</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the misc info on an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualMisc(TestResponse $response): void
    {
        $response->assertSee('<div class="tab-dropdown"><input type="hidden" id="season-schedule" '
            . 'name="season-schedule" value="2019" data-is-dropdown="true" >
<dl class="dropdown " data-id="season-schedule"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="2020">2020</li>
                            <li class="sel" data-id="2019">2019</li>
                            <li  data-id="2018">2018</li>
                            <li  data-id="2017">2017</li>
                            <li  data-id="2016">2016</li>
                            <li  data-id="2015">2015</li>
                            <li  data-id="2014">2014</li>
                            <li  data-id="2013">2013</li>
                            <li  data-id="2012">2012</li>
                            <li  data-id="2011">2011</li>
                            <li  data-id="2010">2010</li>
                            <li  data-id="2009">2009</li>
                    </ul>
    </dd>
</dl>');
    }

    /**
     * A test of the schedule tab of an individual team page.
     * @return void
     */
    public function testIndividualTabSchedule(): void
    {
        $response = $this->getTab('/nfl/teams/green-bay-packers/schedule');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(18, 'sched');
        $response->assertJsonFragment([
            'date_fmt' => 'Week 1',
            'date_ymd' => '2019-09-05',
            'venue' => '@',
            'opp' => 'team-nfl-right-CHI',
            'opp_wide' => 'team-nfl-narrow_small-right-CHI',
            'opp_id' => 'CHI',
            'opp_city' => 'Chicago',
            'opp_franchise' => 'Bears',
            'link' => '/nfl/schedule/2019-week-1/packers-at-bears-r101',
            'info' => '<span class="result-win">W 10 &ndash; 3</span>',
        ]);
        $response->assertSee('<a class=\"no_underline icon_game_boxscore\" '
            . 'href=\"\/nfl\/schedule\/2019-week-1\/packers-at-bears-r101\"><\/a>');
    }

    /**
     * A test of the roster tab of an individual team page.
     * @return void
     */
    public function testIndividualTabRoster(): void
    {
        $response = $this->getTab('/nfl/teams/green-bay-packers/roster');
        $response->assertStatus(200);
        $response->assertSee('<dl class="roster">');
        $response->assertSee('<dt class="row-head">Quaterback</dt>
                                            <dd class="row-0 mugshot mugshot-tiny"><img class="mugshot-tiny" '
            . 'loading="lazy" src="');
        $response->assertSee('" alt="Profile photo of Tim Boyle"></dd>
                <dd class="row-0 jersey">#8</dd>
                <dd class="row-0 name">
                    <a href="/nfl/players/tim-boyle-6337">Tim Boyle</a>
                                                        </dd>
                <dd class="row-0 pos">QB</dd>
                <dd class="row-0 height">6&#39; 4&quot;</dd>
                <dd class="row-0 weight">232lb</dd>
                <dd class="row-0 dob">3rd Oct 1994</dd>
                <dd class="row-0 birthplace"><span class="flag_right flag16_right_us-ct">Hartford, CT</span></dd>');
    }

    /**
     * A test of the standings tab of an individual team page.
     * @return void
     */
    public function testIndividualTabStandings(): void
    {
        $response = $this->getTab('/nfl/teams/green-bay-packers/standings');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('chart.chart.renderTo', 'chart-standings');
        $response->assertJsonPath('chart.series.0.type', 'spline');
        $response->assertJsonPath('chart.series.0.data', [
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        ]);
        $response->assertJsonPath('chart.series.1.type', 'column');
        $response->assertJsonPath('chart.series.1.data', [
            7, 5, 11, 0, 10, 1, 18, 7, 0, 8, 0, 0, 18, 5, 8, 13, 3,
        ]);
        $response->assertJsonPath('chart.series.2.type', 'column');
        $response->assertJsonPath('chart.series.2.data', [
            0, 0, 0, -7, 0, 0, 0, 0, -15, 0, 0, -29, 0, 0, 0, 0, 0,
        ]);
        $response->assertJsonPath('extraJS.standingsRecords.0', '1&ndash;0');
        $response->assertJsonPath('extraJS.standingsRecords.16', '13&ndash;3');
    }

    /**
     * A test of the power rank tab of an individual team page.
     * @return void
     */
    public function testIndividualTabPowerRanks(): void
    {
        $response = $this->getTab('/nfl/teams/green-bay-packers/power-ranks');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('chart.chart.renderTo', 'chart-power-ranks');
        $response->assertJsonPath('chart.series.0.data', [
            6, 4, 4, 4, 3, 8, 7, 7, 9, 8, 8, 9, 6, 9, 5, 5, 6, 3, 2, 3, 3,
        ]);
        $response->assertJsonPath('extraJS.powerRankWeeks.Week 1', 'Week 1');
        $response->assertJsonPath('extraJS.powerRankWeeks.Super Bowl', 'Super Bowl');
    }

    /**
     * A test of the history tab of an individual team page.
     * @return void
     */
    public function testIndividualTabHistory(): void
    {
        $response = $this->getTab('/nfl/teams/green-bay-packers/history');
        $response->assertStatus(200);
        $response->assertSee('<dt class="league row-head row_league-NFL">
                <span class="icon misc-nfl-NFL">National Football League</span>
            </dt>
            <dd class="league titles row-head row_league-NFL"><span>19 Division Titles</span>; '
            . '<span>9 Conference Titles</span>; <span>11 League Titles</span>; <span>4 Lombardi Trophies</span></dd>');
        $response->assertSee('<dt class="row-head parts-1 season">2019</dt>
                    <dd class="row-1 record">13&ndash;3&ndash;0, 1st North</dd>
                ' . '
                    <dd class="row-1 playoffs">
                ' . '
                <ul class="inline_list matchups clearfix">
                                            <li class="result-loss">
                            <span class="hidden-m">Conference Championship:</span>
                            <span class="hidden-t hidden-d">Conf Champ:</span>
                            Lost 20&ndash;37
                                                            v <span class="icon team-nfl-SF">San Francisco 49ers</span>
                                                    </li>
                                            <li class="result-win">
                            <span class="hidden-m">Divisonal Round:</span>
                            <span class="hidden-t hidden-d">Divisional:</span>
                            Won 28&ndash;23
                                                            v <span class="icon team-nfl-SEA">Seattle Seahawks</span>
                                                    </li>
                                    </ul>
            </dd>');
    }

    /**
     * A test of the various subtleties of an individual team page.
     * @return void
     */
    public function testIndividualMisc(): void
    {
        $response = $this->get('/nfl/teams/unknown-team');
        $response->assertStatus(404);

        $response = $this->get('/nfl/teams/unknown-team/unknown-tab');
        $response->assertStatus(404);

        $response = $this->get('/nfl/teams/unknown-team/schedule');
        $response->assertStatus(404);

        $response = $this->get('/nfl/teams/oakland-raiders');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nfl/teams/las-vegas-raiders');

        $response = $this->get('/nfl/teams/green-bay-packers/home');
        $response->assertStatus(404);

        $response = $this->get('/nfl/teams/green-bay-packers/unknown');
        $response->assertStatus(404);

        $response = $this->get('/nfl/teams/green-bay-packers/power-ranks');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nfl/teams/green-bay-packers#power-ranks');

        $response = $this->getTab('/nfl/teams/green-bay-packers/power-ranks/2019');
        $response->assertStatus(200);
        $response->assertJsonPath('chart.series.0.data', [
            6, 4, 4, 4, 3, 8, 7, 7, 9, 8, 8, 9, 6, 9, 5, 5, 6, 3, 2, 3, 3,
        ]);

        $response = $this->getTab('/nfl/teams/green-bay-packers/power-ranks/2039');
        $response->assertStatus(404);
    }

    /**
     * A test of the user favourite setting.
     * @return void
     */
    public function testFavourites(): void
    {
        // User must be logged in to perform either action.
        $response = $this->post('/nfl/teams/green-bay-packers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/nfl/teams/green-bay-packers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);

        // Log in for all future requests.
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Trying actions on a team that doesn't exist should fail.
        $response = $this->post('/nfl/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/nfl/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);

        // Ensure team is not already a favourite (but has the option to save as such).
        $response = $this->get('/nfl/teams/green-bay-packers');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // If we try to remove at this point, it should fail.
        $response = $this->delete('/nfl/teams/green-bay-packers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Perform the add, and validate the page updates accordingly.
        $response = $this->post('/nfl/teams/green-bay-packers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(201);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_is',
            'hover' => 'icon_right_favourite_remove',
        ]);

        $response = $this->get('/nfl/teams/green-bay-packers');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="rmv">
            <span class="icons icon_favourite_is icon_right_favourite_remove"></span>
            <span class="text">Remove from favourites</span>
        </button>
    </div>');

        // A second attempt should also fail.
        $response = $this->post('/nfl/teams/green-bay-packers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Now perform a removal, and validate the page updates accordingly.
        $response = $this->delete('/nfl/teams/green-bay-packers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_not',
            'hover' => 'icon_right_favourite_add',
        ]);

        $response = $this->get('/nfl/teams/green-bay-packers');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
