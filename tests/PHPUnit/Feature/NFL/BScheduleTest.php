<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class BScheduleTest extends MajorLeagueBaseCase
{
    /**
     * A test of the current date, identifying a future season (though deterministically "current")
     * @return void
     */
    public function testCurrentFuture(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestConfig(['debear.sports.subsites.nfl.setup.season.viewing' => 2020]);
        $this->setTestDateTime('2020-09-01 12:34:56');

        $response = $this->get('/nfl/schedule');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/schedule/2020-week-1" '
            . 'data-date="2020-week-1">1</a></dd>');

        $response->assertSee('<h3>Thursday 10th September</h3>
        <ul class="games grid inline_list">
            <li class="game-cell grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1">
            <ul class="inline_list nfl_box game preview">
    <li class="status head">5:20pm PDT</li>
        <li class="team visitor">
        <a href="/nfl/teams/houston-texans" class="team-nfl-narrow_small-HOU">Texans</a>
                    <span class="standings">(0&ndash;0)</span>
            </li>
    <li class="team home">
        <a href="/nfl/teams/kansas-city-chiefs" class="team-nfl-narrow_small-KC">Chiefs</a>
                    <span class="standings">(0&ndash;0)</span>
            </li>
    ' . '
        ' . '
        ' . '
        ' . '
    <li class="venue head">Arrowhead Stadium</li>
    ' . '
    </ul>');
    }

    /**
     * A test of the current date, identifying a previous season (though deterministically "current")
     * @return void
     */
    public function testCurrentPrevious(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-09-01 12:34:56');

        $response = $this->get('/nfl/schedule');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/schedule/2019-superbowl" '
            . 'data-date="2019-superbowl">Super Bowl</a></dd>');

        $response->assertSee('<h3>Sunday 2nd February</h3>
        <ul class="games grid inline_list">
            <li class="game-cell grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1">
            <ul class="inline_list nfl_box game result">
    ' . '
    <li class="status head">
        <strong>Final</strong>
                    <span class="att">Attendance: 62,417</span>
            </li>
            <li class="title">Super Bowl LIV</li>
        ' . '
    <li class="team visitor loss">
        <span class="score">20</span>
        <a href="/nfl/teams/san-francisco-49ers" class="team-nfl-narrow_small-SF">49ers</a>
            </li>
    <li class="team home win">
        <span class="score">31</span>
        <a href="/nfl/teams/kansas-city-chiefs" class="team-nfl-narrow_small-KC">Chiefs</a>
            </li>');
    }

    /**
     * A test of future dates with game previews
     * @return void
     */
    public function testFuturePreview(): void
    {
        $response = $this->get('/nfl/schedule/2020-week-2');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/schedule/2020-week-2" '
            . 'data-date="2020-week-2">2</a></dd>');

        // Ensure that the standings match the previous day (which has the standings as of Week 1, not Week 2).
        $response->assertSee('<h3>Sunday 20th September</h3>
        <ul class="games grid inline_list">
            <li class="game-cell grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1">
            <ul class="inline_list nfl_box game preview">
    <li class="status head">10:00am PDT</li>
        <li class="team visitor">
        <a href="/nfl/teams/atlanta-falcons" class="team-nfl-narrow_small-ATL">Falcons</a>
                    <span class="standings">(0&ndash;0)</span>
            </li>
    <li class="team home">
        <a href="/nfl/teams/dallas-cowboys" class="team-nfl-narrow_small-DAL">Cowboys</a>
                    <span class="standings">(0&ndash;0)</span>
            </li>
    ' . '
        ' . '
        ' . '
    <li class="odds line-with-icon">
    <dl>
        <dt>Odds:</dt>
        ' . '
        ' . '
        ' . '
                                    <dd class="spread icon team-nfl-DAL">-3.0</dd>
        ' . '
        ' . '
                    <dt class="over-under">O/U:</dt>
                <dd class="over-under">53</dd>
            </dl>
</li>
    ' . '
    <li class="venue head">AT&amp;T Stadium</li>
    ' . '
    </ul>');
    }

    /**
     * A test of dates with game results
     * @return void
     */
    public function testResult(): void
    {
        $response = $this->get('/nfl/schedule/2019-superbowl');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/schedule/2019-superbowl" '
            . 'data-date="2019-superbowl">Super Bowl</a></dd>');

        $response->assertSee('<h3>Sunday 2nd February</h3>
        <ul class="games grid inline_list">
            <li class="game-cell grid-1-4 grid-tl-1-3 grid-tp-1-2 grid-m-1-1">
            <ul class="inline_list nfl_box game result">
    ' . '
    <li class="status head">
        <strong>Final</strong>
                    <span class="att">Attendance: 62,417</span>
            </li>
            <li class="title">Super Bowl LIV</li>
        ' . '
    <li class="team visitor loss">
        <span class="score">20</span>
        <a href="/nfl/teams/san-francisco-49ers" class="team-nfl-narrow_small-SF">49ers</a>
            </li>
    <li class="team home win">
        <span class="score">31</span>
        <a href="/nfl/teams/kansas-city-chiefs" class="team-nfl-narrow_small-KC">Chiefs</a>
            </li>');
        $response->assertSee('<li class="breakdown">
        <dl class="periods clearfix">
    ' . '
            <dt class="period head">1</dt>
            <dt class="period head">2</dt>
            <dt class="period head">3</dt>
            <dt class="period head">4</dt>
        <dt class="total head">Tot</dt>
    ' . '
    <dd class="team team-nfl-SF"></dd>
            <dd class="period">3</dd>
            <dd class="period">7</dd>
            <dd class="period">10</dd>
            <dd class="period">0</dd>
        <dt class="total head">20</dt>
    ' . '
    <dd class="team team-nfl-KC"></dd>
            <dd class="period">7</dd>
            <dd class="period">3</dd>
            <dd class="period">0</dd>
            <dd class="period">21</dd>
        <dt class="total head">31</dt>
</dl>
    </li>');
        $response->assertSee('<li class="venue head">Hard Rock Stadium</li>');
        $response->assertSee('<li class="links">
        <ul class="inline_list">
                            <li>
                    <a class="icon_game_stats" href="/nfl/schedule/2019-superbowl/49ers-at-chiefs-p401#stats">'
            . '<em>Game Stats</em></a>
                </li>
                            <li>
                    <a class="icon_game_boxscore" href="/nfl/schedule/2019-superbowl/49ers-at-chiefs-p401">'
            . '<em>Boxscore</em></a>
                </li>
                            <li>
                    <a class="icon_game_scoring" href="/nfl/schedule/2019-superbowl/49ers-at-chiefs-p401#scoring">'
            . '<em>Scoring</em></a>
                </li>
                            <li>
                    <a class="icon_game_drives" href="/nfl/schedule/2019-superbowl/49ers-at-chiefs-p401#drivechart">'
            . '<em>Drive Chart</em></a>
                </li>
                    </ul>
    </li>
</ul>
        </li>');
    }

    /**
     * A test of dates with bye weeks
     * @return void
     */
    public function testByeWeeks(): void
    {
        $response = $this->get('/nfl/schedule/2020-week-5');
        $response->assertStatus(200);
        $response->assertSee('<dd class="current"><a href="/nfl/schedule/2020-week-5" '
            . 'data-date="2020-week-5">5</a></dd>');

        $response->assertSee('<dl class="bye_weeks">
    <dt>Bye Weeks:</dt>
            <dd class="team-nfl-DET">
            <span class="hidden-t hidden-d">DET</span>
            <span class="hidden-m">Lions</span>
        </dd>
            <dd class="team-nfl-GB">
            <span class="hidden-t hidden-d">GB</span>
            <span class="hidden-m">Packers</span>
        </dd>
    </dl>');
    }

    /**
     * A test of miscellaneous controller actions
     * @return void
     */
    public function testControllerMisc(): void
    {
        // Current season.
        $this->setTestDateTime('2020-09-09 12:34:56');
        $response = $this->get('/nfl/schedule/2020/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2020-week-1/switcher');
        // Previous season.
        $this->setTestDateTime('2020-03-01 12:34:56');
        $response = $this->get('/nfl/schedule/2019/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nfl/schedule/2019-superbowl/switcher');

        // Calendar widget exceptions.
        $response = $this->get('/nfl/schedule/2020/calendar');
        $response->assertStatus(404);
    }

    /**
     * A test of ordered games based on user favourites
     * @return void
     */
    public function testScheduleFavourites(): void
    {
        // A control, the "regular" order.
        $response = $this->get('/nfl/schedule/2020-week-1');
        $response->assertStatus(200);
        $response->assertDontSee('<h3>My Teams</h3>');
        $response->assertSeeInOrder([
            '<li class="venue head">Arrowhead Stadium</li>', // HOU @ KC.
            '<li class="venue head">U.S. Bank Stadium</li>', // GB @ MIN.
        ]);

        // Log in as a user with favourites.
        $response = $this->post('/login', [
            'username' => 'sports_admin',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Load the schedule and ensure the games are ordered correctly.
        $response = $this->get('/nfl/schedule/2020-week-1');
        $response->assertStatus(200);
        $response->assertSeeInOrder(['<h3>My Teams</h3>', '<h3>Thursday 10th September</h3>']);
        $response->assertSeeInOrder([
            '<li class="venue head">U.S. Bank Stadium</li>', // GB @ MIN.
            '<li class="venue head">Arrowhead Stadium</li>', // HOU @ KC.
        ]);

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
