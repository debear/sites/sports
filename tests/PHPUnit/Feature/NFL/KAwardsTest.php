<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class KAwardsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/nfl/awards');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NFL &raquo; NFL Award Winners</title>');
        $response->assertSee('<h1>NFL Award Winners</h1>');

        $response->assertSee('<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="nfl_box award clearfix">
        ' . '
        <h3 class="row-head">Most Valuable Player</h3>

        ' . '
        <div class="mugshot-small">
                            <img class="mugshot-small" loading="lazy" src="');
        $response->assertSee('" alt="Profile photo of Lamar Jackson">
                    </div>

        ' . '
        <p class="descrip">Awarded to the player considered most valuable to his team during the regular season</p>

        ' . '
        <p class="winner">
            <strong>2019 Winner:</strong>
                                                <span class="team-nfl-BAL">
                                <a href="/nfl/players/lamar-jackson-6235">Lamar Jackson</a>
                                    </span>
                            ' . '
                            (QB)
                    </p>

        ' . '
        <p class="link"><a class="all-time" data-link="/nfl/awards/most-valuable-player-1">All-Time Winners</a></p>
    </fieldset>
</li>');
    }

    /**
     * A test of the all-time winners.
     * @return void
     */
    public function testAllTime(): void
    {
        // A non-award.
        $response = $this->get('/nfl/awards/does-not-exist-99');
        $response->assertStatus(404);
        // An actual award.
        $response = $this->get('/nfl/awards/most-valuable-player-1');
        $response->assertStatus(200);
        $response->assertJsonMissing([
            'season' => '2020',
            'winner' => [],
        ]);
        $response->assertJsonFragment([
            'season' => '2019',
            'winner' => [
                'player' => 'Lamar Jackson',
                'team' => 'Baltimore Ravens',
                'team_id' => 'BAL',
                'pos' => 'QB',
            ],
        ]);
    }
}
