<?php

namespace Tests\PHPUnit\Feature\NFL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class AHomeTest extends MajorLeagueBaseCase
{
    /**
     * A test of the homepage and game header chrome.
     * @return void
     */
    public function testHomepage(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-09-01 12:34:56');

        $response = $this->get('/nfl');
        $response->assertStatus(200);

        // Check the game headers.
        $response->assertSee('<div class="schedule_header">
    <dl class="num-1">
        <dt class="row-head date">Super Bowl</dt>
        <dd>
    <div class="team visitor row-1 icon team-nfl-SF loss">
        SF
                    <span class="score">20</span>
            </div>
    <div class="team home row-0 icon team-nfl-KC win">
        KC
                    <span class="score">31</span>
            </div>
    <div class="info row-1">
                    ' . '
            <div class="link"><a href="/nfl/schedule/2019-superbowl/49ers-at-chiefs-p401" '
            . 'class="icon_right_game_boxscore no_underline">Boxscore</a></div>
            <div class="status">Final</div>
            </div>
</dd>
    </dl>');

        // News.
        $response->assertSee('<ul class="inline_list grid home">
    <li class="grid-cell grid-3-4 grid-t-1-1 grid-m-1-1">
        ' . '
        <ul class="inline_list grid">
            <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <article class="nfl_box">
            <h4><a href="https://sports.yahoo.com/2020-unaccounted-carries-062626151.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">2020 Unaccounted For Carries</a></h4>
            <publisher>Yahoo! Sports</publisher>
            <time>6th February 2020, 3:26am</time>');

        // Playoff Bracket.
        $response->assertDontSee('<h3 class="row-head">Standings</h3>');
        $response->assertSee('<fieldset class="nfl_box">
    <h3 class="row-head">Playoffs</h3>
    <dl class="bracket toggle-target grid clearfix">
                                    <dt class="tab grid-cell grid-1-4 row-0 unsel" data-id="1" title="Wild Card">WC</dt>
                            <dt class="tab grid-cell grid-1-4 row-0 unsel" data-id="2" title="Divisional Round">Div</dt>
                            <dt class="tab grid-cell grid-1-4 row-0 unsel" data-id="3" title="Conference Championship">'
            . 'Conf</dt>
                            <dt class="tab grid-cell grid-1-4 row-head sel" data-id="4" title="Super Bowl">SB</dt>
                                                    <dd class="half grid-1-1 tab-1 hidden">
                <ul class="inline_list grid">
                                                                    <li class="grid-cell grid-1-2 series-l">
                                                                                            <div class="higher loser '
            . 'row-0">
                                    <span class="team team-nfl-narrow_small-NO">'
            . '<a href="/nfl/teams/new-orleans-saints">NO</a></span>
                                    <span class="score">20</span>
                                </div>
                                <div class="lower winner row-1">
                                    <span class="team team-nfl-narrow_small-MIN">'
            . '<a href="/nfl/teams/minnesota-vikings">MIN</a></span>
                                    <span class="score">26</span>
                                </div>
                                                                                            '
            . '<div class="higher loser row-0">
                                    <span class="team team-nfl-narrow_small-PHI">'
            . '<a href="/nfl/teams/philadelphia-eagles">PHI</a></span>
                                    <span class="score">9</span>
                                </div>
                                <div class="lower winner row-1">
                                    <span class="team team-nfl-narrow_small-SEA">'
            . '<a href="/nfl/teams/seattle-seahawks">SEA</a></span>
                                    <span class="score">17</span>
                                </div>
                                                    </li>');

        // Leaders.
        $response->assertSee('<fieldset class="nfl_box">
    <h3 class="row-head">Leaders</h3>
    <dl class="leaders clearfix">');
        $response->assertSee('<dt class="row-head">Passing</dt>
            <dd class="name"><span class="icon team-nfl-TB"><a href="/nfl/players/jameis-winston-4612">'
            . 'J. Winston</a></span></dd>
            <dd class="value">5,109yds</dd>');

        // Power Ranks.
        $response->assertSee('<fieldset class="nfl_box">
    <h3 class="row-head">Latest Power Ranks</h3>
    <dl class="power_ranks clearfix">
        ' . '
                            <dt class="row-head">1</dt>
                <dd class="team row-1 team-nfl-narrow_small-KC">'
            . '<a href="/nfl/teams/kansas-city-chiefs">Chiefs</a></dd>');
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/nfl/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear Sports NFL',
            'short_name' => 'DeBear Sports NFL',
            'description' => 'All the latest National Football League results, stats and news.',
            'start_url' => '/nfl/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/sports/images/meta/nfl.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
