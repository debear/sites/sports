<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class AHomeTest extends MajorLeagueBaseCase
{
    /**
     * A test of the homepage and game header chrome.
     * @return void
     */
    public function testHomepage(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-08-09 12:34:56');

        $response = $this->get('/mlb');
        $response->assertStatus(200);

        // Check the game headers.
        $response->assertSee('<div class="schedule_header">
    <dl class="num-16">
        <dt class="row-head date">9th Aug</dt>
        <dd>
    <div class="team visitor row-1 icon team-mlb-ATL win">
        ATL
                    <span class="score">5</span>
            </div>
    <div class="team home row-0 icon team-mlb-PHI loss">
        PHI
                    <span class="score">2</span>
            </div>
    <div class="info row-1">
                    ' . '
            <div class="link"><a href="/mlb/schedule/20200809/braves-at-phillies-r228" '
            . 'class="icon_right_game_boxscore no_underline">Boxscore</a></div>
            <div class="status">Final/7</div>
            </div>
</dd>');
        $response->assertSee('<dd>
    <div class="team visitor row-1 icon team-mlb-BAL ">
        BAL
            </div>
    <div class="team home row-0 icon team-mlb-WSH ">
        WSH
            </div>
    <div class="info row-1">
                    ' . '
            <em>Suspended (Game Suspended)</em>
            </div>
</dd>');

        // News.
        $response->assertSee('<ul class="inline_list grid">
            <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <article class="mlb_box">
            <h4><a href="https://sports.yahoo.com/rival-cubs-brewers-set-key-rubber-match-082851712--mlb.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Rival Cubs, Brewers set for '
            . 'key rubber match</a></h4>
            <publisher>Yahoo! Sports</publisher>
            <time>26th July 2020, 5:28am</time>');

        // Standings.
        $response->assertDontSee('<h3 class="row-head">Playoffs</h3>');
        $response->assertSee('<fieldset class="mlb_box">
    <h3 class="row-head">Standings</h3>
    <dl class="standings toggle-target clearfix">
                    <dt class="conf row_conf-AL sel" data-id="2">AL</dt>
                    <dt class="conf row_conf-NL unsel" data-id="3">NL</dt>
                            <dd class="conf tab-2 ">
                <dl>
                                            <dt class="row_conf-AL">AL East</dt>
                                                                                                        '
            . '<dd class="row-1 team-mlb-NYY">
                                <a href="/mlb/teams/new-york-yankees">NYY</a>
                                                                                                    '
            . '<div class="col">&ndash;</div>
                                                                    <div class="col">.667</div>
                                                                    <div class="col">12&ndash;6</div>
                                                            </dd>');

        // Leaders.
        $response->assertSee('<fieldset class="mlb_box">
    <h3 class="row-head">Leaders</h3>
    <dl class="leaders clearfix">');
        $response->assertSee('<dt class="row-head">Batting Average</dt>
            <dd class="name"><span class="icon team-mlb-NYY"><a href="/mlb/players/dj-lemahieu-1942">DJ. LeMahieu</a>'
            . '</span></dd>
            <dd class="value">.371</dd>');

        // No Power Ranks.
        $response->assertDontSee('<h3 class="row-head">Latest Power Ranks</h3>');
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/mlb/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear Sports MLB',
            'short_name' => 'DeBear Sports MLB',
            'description' => 'All the latest Major League Baseball results, stats and news.',
            'start_url' => '/mlb/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/sports/images/meta/mlb.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
