<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class JDraftsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the default draft
     * @return void
     */
    public function testDefault(): void
    {
        $response = $this->get('/mlb/draft');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; 2019 MLB Draft</title>');
        $response->assertSee('<h1>2019 MLB Draft</h1>');

        // Available draft history.
        $response->assertSee('<dl class="dropdown " data-id="switcher-season"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2019">2019</li>
                            <li  data-id="2018">2018</li>');
        $response->assertSee('<li  data-id="1966">1966</li>
                            <li  data-id="1965">1965</li>
                    </ul>
    </dd>
</dl>');

        // Amateur, Rule 5 (Major) and Rule 5 (AAA) drafts this season.
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="amateur">Amateur Draft</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="r5-maj">Rule 5 (Major League)</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="r5-aaa">Rule 5 (AAA)</item>
                <sep class="right"></sep>
    </desk>');

        // Amateur Draft rounds.
        $response->assertSee('<ul class="inline_list">
                            <li class="sel" data-id="1">Round 1</li>
                            <li  data-id="C1">Compensatory (1st Round)</li>
                            <li  data-id="CBA">Competitive Balance A</li>
                            <li  data-id="2">Round 2</li>
                            <li  data-id="CBB">Competitive Balance B</li>
                            <li  data-id="C2">Compensatory (2nd Round)</li>
                            <li  data-id="3">Round 3</li>
                            <li  data-id="4">Round 4</li>
                            <li  data-id="5">Round 5</li>
                            <li  data-id="6">Round 6</li>
                            <li  data-id="7">Round 7</li>
                            <li  data-id="8">Round 8</li>
                            <li  data-id="9">Round 9</li>
                            <li  data-id="10">Round 10</li>
                            <li  data-id="11">Round 11</li>
                            <li  data-id="12">Round 12</li>
                            <li  data-id="13">Round 13</li>
                            <li  data-id="14">Round 14</li>
                            <li  data-id="15">Round 15</li>
                            <li  data-id="16">Round 16</li>
                            <li  data-id="17">Round 17</li>
                            <li  data-id="18">Round 18</li>
                            <li  data-id="19">Round 19</li>
                            <li  data-id="20">Round 20</li>
                            <li  data-id="21">Round 21</li>
                            <li  data-id="22">Round 22</li>
                            <li  data-id="23">Round 23</li>
                            <li  data-id="24">Round 24</li>
                            <li  data-id="25">Round 25</li>
                            <li  data-id="26">Round 26</li>
                            <li  data-id="27">Round 27</li>
                            <li  data-id="28">Round 28</li>
                            <li  data-id="29">Round 29</li>
                            <li  data-id="30">Round 30</li>
                            <li  data-id="31">Round 31</li>
                            <li  data-id="32">Round 32</li>
                            <li  data-id="33">Round 33</li>
                            <li  data-id="34">Round 34</li>
                            <li  data-id="35">Round 35</li>
                            <li  data-id="36">Round 36</li>
                            <li  data-id="37">Round 37</li>
                            <li  data-id="38">Round 38</li>
                            <li  data-id="39">Round 39</li>
                            <li  data-id="40">Round 40</li>
                    </ul>');

        // Some picks.
        $response->assertSee('<div class="draft-data hidden">{"amateur":{"1":[{"round":1,"pick":1,"round_pick":1,'
            . '"team":"\u003Cspan class=\u0022team-mlb-BAL\u0022\u003EBAL\u003C\/span\u003E",'
            . '"player":"Adley Rutschman","pos":"C","height":"6\u0027 2\u0022","weight":216,"bats":"S","throws":"R",'
            . '"school":"Oregon State"},');
        $response->assertSee('195,"bats":"L","throws":"R","school":"Washington State"},{"round":40,"pick":1217,'
            . '"round_pick":30,"team":"\u003Cspan class=\u0022team-mlb-BOS\u0022\u003EBOS\u003C\/span\u003E",'
            . '"player":"Garrett Irvin","pos":"P","height":"6\u0027","weight":180,"bats":"L","throws":"L",'
            . '"school":"Riverside CC"}]},"r5-maj":{"1":[{"round":1,"pick":2,"round_pick":2,'
            . '"team":"\u003Cspan class=\u0022team-mlb-BAL\u0022\u003EBAL\u003C\/span\u003E","player":"Brandon Bailey",'
            . '"pos":"P","height":"5\u0027 10\u0');
        $response->assertSee(',{"round":4,"pick":42,"round_pick":2,'
            . '"team":"\u003Cspan class=\u0022team-mlb-CHC\u0022\u003ECHC\u003C\/span\u003E","player":"David Masters",'
            . '"pos":"SS","height":"6\u0027 1\u0022","weight":185,"bats":"R","throws":"R","school":null}]}}</div>');
    }

    /**
     * A test of a specific draft
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/mlb/draft/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; 2019 MLB Draft</title>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/mlb/draft/2019/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; MLB &raquo; 2019 MLB Draft</title>');
        $response->assertSee('<h1>2019 MLB Draft</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        // We had drafts in the 1900s (unlike things like standings) as the first draft was in 1965.
        $response = $this->get('/mlb/draft/1965');
        $response->assertStatus(200);
        $response = $this->get('/mlb/draft/1964');
        $response->assertStatus(404);
        // The last draft we have is in 2019.
        $response = $this->get('/mlb/draft/2019');
        $response->assertStatus(200);
        $response = $this->get('/mlb/draft/2020');
        $response->assertStatus(404);
    }
}
