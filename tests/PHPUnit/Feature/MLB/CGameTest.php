<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class CGameTest extends MajorLeagueBaseCase
{
    /**
     * A test of a single game
     * @return void
     */
    public function testGame(): void
    {
        $response = $this->get('/mlb/schedule/20200809/indians-at-white-sox-r235');
        $response->assertStatus(200);

        $response->assertSee('<ul class="inline_list mlb_box game summary">
    <li class="row-head info">
        <span class="status">Final/10</span>
        <span class="venue">Guaranteed Rate Field</span>
    </li>
    <li class="title">
        Sunday 9th August
    </li>');
        $response->assertDontSee('<li class="summary">');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="unsel" data-tab="series"><span class="icon_game_series">Season Series</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="scoring"><span class="icon_game_scoring">Scoring</span></item>
                    <sep class="mid"></sep>
            <item class="sel" data-tab="boxscore"><span class="icon_game_boxscore">Boxscore</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="probability"><span class="icon_game_probability">Win Probability</span></item>
                <sep class="right"></sep>
    </desk>');
        $response->assertDontSee('<item class="unsel" data-tab="pitches"><span class="icon_game_pitches">'
            . 'Pitch Locations</span></item>');
        $response->assertDontSee('<item class="unsel" data-tab="playbyplay"><span class="icon_game_pbp">'
            . 'Play-by-Play</span></item>');
        $response->assertDontSee('<item class="unsel" data-tab="stats"><span class="icon_game_stats">'
            . 'Game Stats</span></item>');

        // The individual tabs.
        $this->testGameScoring($response);
        $this->testGameBoxscore($response);
        $this->testGameWinProbability($response);
    }

    /**
     * Test the game's scoring tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameScoring(TestResponse $response): void
    {
        $response->assertSee('<dl class="mlb_box playlist scoring clearfix">
                                                                                    '
            . '<dt class="row-head period">Bottom 2nd</dt>
                                                                <dd class="mugshot mugshot-pbp">');
        $response->assertSee('alt="Profile photo of Jose Abreu"></dd>
                                <dd class="row-0 play with-mugshot" data-play="10">
                    <ul class="inline_list">
                                                    '
            . '<li class="winprob"><span class="team-mlb-right-CWS">66.0%</span></li>
                                                <li class="score">
                            <span class="visitor team-mlb-CLE">0</span> &ndash; '
            . '<span class="home team-mlb-right-CWS">1</span>
                        </li>
                        <li class="state">
                            <dl>
                                <dt class="outs">Outs:</dt>
                                    <dd class="cell first icon_pbp_mlb_out dimmed"></dd>
                                    <dd class="cell last icon_pbp_mlb_out dimmed"></dd>
                                <dt class="runners">Baserunners:</dt>
                                    <dd class="cell first last icon_pbp_mlb_bases_000"></dd>
                            </dl>
                        </li>
                        <li class="play"><a href="/mlb/players/jose-abreu-2585">Jose Abreu</a> homers (3) on a '
            . 'fly ball to left center field.</li>
                    </ul>
                </dd>');
    }

    /**
     * Test the game's boxscore tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameBoxscore(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list mlb_box boxscore clearfix">
        <li class="row_mlb-CLE team"><span class="team-mlb-CLE icon">Cleveland Indians</span></li>

        ' . '
                <li class="cell section batter row_mlb-CLE">Batters</li>
        <li class="static batter">
            <ul class="inline_list">
                <li class="cell jersey row_mlb-CLE">#</li>
                <li class="cell cell-head player row_mlb-CLE">Player</li>

                                                        <li class="cell jersey starter row_mlb-CLE">7</li>
                    <li class="cell name starter row-0" data-player="CLE-7" data-mugshot="');
        $response->assertSee('" data-stands="S" data-zone-L-vs-R="[4,4,0,0,0],[12,12,3,3,8],[6,6,1,1,1],[13,13,6,6,9],'
            . '[20,20,9,9,10],[8,8,4,4,5],[15,15,5,5,7],[18,18,6,6,11],[8,8,4,4,5],[15,8,0,7,0],[13,11,1,3,1],'
            . '[28,26,7,9,8],[33,23,4,14,6]" data-avg-vs-R="0.2907" data-zone-R-vs-L="[7,7,3,3,5],[4,4,1,1,2],'
            . '[1,1,1,1,1],[5,5,1,1,1],[6,6,2,2,6],[4,4,1,1,1],[4,4,1,1,1],[3,3,0,0,0],[2,2,0,0,0],[6,5,0,1,0],'
            . '[4,3,2,3,2],[13,10,1,4,1],[7,7,2,2,3]" data-avg-vs-L="0.2459">'
            . '<a href="/mlb/players/cesar-hernandez-2419"><span class="name-short">C. Hernandez</span>'
            . '<span class="name-full">Cesar Hernandez</span></a><span class="pos">2B</span></li>');
        $response->assertSee('<li class="scroll batter">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat_wide row_mlb-CLE">H/AB</li>
                <li class="cell cell-head stat row_mlb-CLE">R</li>
                <li class="cell cell-head stat row_mlb-CLE">HR</li>
                <li class="cell cell-head stat row_mlb-CLE">RBI</li>
                <li class="cell cell-head stat row_mlb-CLE">BB</li>
                <li class="cell cell-head stat row_mlb-CLE">K</li>
                <li class="cell cell-head stat_wide row_mlb-CLE">Avg</li>
                <li class="cell cell-head stat_wide row_mlb-CLE">OBP</li>
                <li class="cell cell-head stat_wide row_mlb-CLE">OPS</li>

                                                        <li class="cell cell-first stat_wide row-0">2/4</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">1</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">1</li>
                    <li class="cell stat_wide row-0">.305</li>
                    <li class="cell stat_wide row-0">.414</li>
                    <li class="cell stat_wide row-0">.804</li>');

        $response->assertSee('<li class="note row-1"><span class="prefix">1</span>Ran for '
            . '<a href="/mlb/players/franmil-reyes-3817">Franmil Reyes</a> in the 8th</li>');
        $response->assertSee('<li class="cell section batting row_mlb-CLE">Batting</li>
            <li class="row-0 row-summary">
            <strong>2B:</strong> <a href="/mlb/players/cesar-hernandez-2419">C. Hernandez</a> 2 (5),');
        $response->assertSee('<strong>LOB:</strong> <a href="/mlb/players/jose-ramirez-2536">J. Ramirez</a> 3,');

        $response->assertSee('<li class="cell section pitcher row_mlb-CLE">Pitchers</li>
        <li class="static pitcher">
            <ul class="inline_list">
                <li class="cell jersey row_mlb-CLE">#</li>
                <li class="cell cell-head player row_mlb-CLE">Player</li>

                                                        <li class="cell jersey row_mlb-CLE">57</li>
                    <li class="cell name starter row-0" data-player="CLE-57" data-mugshot="');
        $response->assertSee('" data-throws="R"><a href="/mlb/players/shane-bieber-3825">'
            . '<span class="name-short">S. Bieber</span><span class="name-full">Shane Bieber</span></a></li>');
        $response->assertSee('<li class="scroll pitcher">
            <ul class="inline_list">
                <li class="cell cell-head cell-first stat row_mlb-CLE">IP</li>
                <li class="cell cell-head stat row_mlb-CLE">H</li>
                <li class="cell cell-head stat row_mlb-CLE">R</li>
                <li class="cell cell-head stat row_mlb-CLE">ER</li>
                <li class="cell cell-head stat row_mlb-CLE">BB</li>
                <li class="cell cell-head stat row_mlb-CLE">K</li>
                <li class="cell cell-head stat_wide row_mlb-CLE">ERA</li>
                <li class="cell cell-head stat_wide row_mlb-CLE">WHIP</li>

                                                        <li class="cell cell-first stat row-0">6.0</li>
                    <li class="cell stat row-0">4</li>
                    <li class="cell stat row-0">3</li>
                    <li class="cell stat row-0">3</li>
                    <li class="cell stat row-0">2</li>
                    <li class="cell stat row-0">8</li>
                    <li class="cell stat_wide row-0">1.63</li>
                    <li class="cell stat_wide row-0">0.76</li>');
        $response->assertSee('alt=&quot;Profile photo of Brad Hand&quot;&gt;" data-throws="L">'
            . '<a href="/mlb/players/brad-hand-1951"><span class="name-short">B. Hand</span>'
            . '<span class="name-full">Brad Hand</span></a><span class="decision">(HLD, 1)</span></li>');

        $response->assertSee('<ul class="inline_list mlb_box game_info clearfix">
    <li class="row-head">Game Info</li>
            <li class="row row-0">
            <strong>Game Scores:</strong> <a href="/mlb/players/shane-bieber-3825">S. Bieber</a> 58, '
            . '<a href="/mlb/players/lucas-giolito-3198">L. Giolito</a> 65
        </li>');
        $response->assertSee('<li class="row row-1">
            <strong>Umpires:</strong> HP: Jeremie Rehak, 1B: Jerry Meals, 2B: Jordan Baker, 3B: David Rackley
        </li>
            <li class="row row-0">
            <strong>Weather:</strong> 87&deg;F, Partly Cloudy
        </li>
            <li class="row row-1">
            <strong>Wind:</strong> L To R
        </li>');
    }

    /**
     * Test the game's win probability tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameWinProbability(TestResponse $response): void
    {
        $response->assertSee('<div class="chart" id="winprob"></div>');
        $response->assertSee('var winprobXLabels = {"0":"Top 1st","3":"Bot 1st","6":"Top 2nd","9":"Bot 2nd",');
        $response->assertSee('var winprobPlays = [{"inn_index":0,"home_score":0,"visitor_score":0,"winprob":52.2,'
            . '"winprob_team":"CWS","out":0,"ob_tot":0,"ob_det":"---","batter_team":"CLE","batter":"Cesar Hernandez",'
            . '"pitcher_team":"CWS","pitcher":"Lucas Giolito","result":"Lineout"},');
        $response->assertSee('"xAxis":{"categories":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,'
            . '25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,'
            . '59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81],');
        $response->assertSee('"tickPositions":[50,40,30,20,10,0,-10,-20,-30,-40,-50],"min":-50,"max":50,');
        $response->assertSee('"series":[{"yAxis":0,"type":"spline","data":[2.2,3.7,4.7,2.6,1,0,-3.8,-0.3,5,16,14,16.2,'
            . '11.7,4.6,1,7.1,-9.8,-18,-7,-2.6,-11.6,-12.9,-16.4,-13.1,-16.6,-12.3,-7.9,-10.9,-13.1,-14.4,-12.1,');
        $response->assertSee('"annotations":[{"labels":[{"point":{"x":9,"y":16,"xAxis":0,"yAxis":0},"text":"1 Run '
            . 'Home Run","y":-10},{"point":{"x":16,"y":-9.8,"xAxis":0,"yAxis":0},"text":"1 Run Double","y":-10},');
    }

    /**
     * A test of controller actions on invalid parameters
     * @return void
     */
    public function testInvalid(): void
    {
        // Invalid date.
        $response = $this->get('/mlb/schedule/20200230/away-at-home-r1');
        $response->assertStatus(404);

        // Invalid game.
        $response = $this->get('/mlb/schedule/20200229/away-at-home-r1');
        $response->assertStatus(404);
    }
}
