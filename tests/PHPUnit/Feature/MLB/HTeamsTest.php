<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class HTeamsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the team list page.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/mlb/teams');
        $response->assertStatus(200);

        $response->assertSee('<h1>Teams</h1>');
        $response->assertSee('<dt class="row_conf-AL misc-mlb-narrow-small-AL '
            . 'misc-mlb-narrow-small-right-AL conf">American League</dt>');
        $response->assertSee('<dt class="row_conf-AL div">AL East Division</dt>');

        // A team from this season.
        $response->assertSee('<dd class="row-0 team-mlb-small-WSH logo"></dd>
                    <dd class="row-0 name">Washington Nationals</dd>
                    <dd class="row-0 info">
                        <ul class="inline_list">
                            <li><a href="/mlb/teams/washington-nationals">Home</a></li>'
            . '<li><a href="/mlb/teams/washington-nationals#schedule">Schedule</a></li>'
            . '<li><a href="/mlb/teams/washington-nationals#roster">Roster</a></li>
                        </ul>
                    </dd>');

        // But not a team from a previous season.
        $response->assertDontSee('<dd class="row-0 team-mlb-small-MTE logo"></dd>
                    <dd class="row-0 name">Montreal Expos</dd>
                    <dd class="row-0 info">
                        <ul class="inline_list">
                            <li><a href="/mlb/teams/montreal-expos">Home</a></li>'
            . '<li><a href="/mlb/teams/montreal-expos#schedule">Schedule</a></li>'
            . '<li><a href="/mlb/teams/montreal-expos#roster">Roster</a></li>
                        </ul>
                    </dd>');
    }

    /**
     * A test of an individual team page.
     * @return void
     */
    public function testIndividual(): void
    {
        $response = $this->get('/mlb/teams/milwaukee-brewers');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Teams &raquo; Milwaukee Brewers</title>');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="schedule">Schedule</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="roster">Roster</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="depth-chart">Depth Chart</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="standings">Standings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="power-ranks">Power Rankings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="history">History</item>
                <sep class="right"></sep>
    </desk>');

        $this->individualHeader($response);
        $this->individualSchedule($response);
        $this->individualStandings($response);
        $this->individualStats($response);
        $this->individualLeaders($response);
        $this->individualMisc($response);
    }

    /**
     * A test of the header component of an individual team page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualHeader(TestResponse $response): void
    {
        $response->assertDontSee('<div class="favourite">');
        $response->assertSee('<dt class="standing">3rd, NL Central Divison:</dt>
            <dd class="standing">6&ndash;8</dd>
        ' . '
                        <dt class="prev game">Last Game:</dt>
            <dd class="prev game">
                Sun 9th Aug
                                    <span class="result-win">W 9 &ndash; 3</span>
                                v
                <span class="team-mlb-CIN icon">CIN</span>
            </dd>
        ' . '
                        <dt class="next game">Next Game:</dt>
            <dd class="next game">
                Mon 10th Aug
                v
                <span class="team-mlb-MIN icon">MIN</span>
            </dd>
    </dl>');
    }

    /**
     * A test of the schedule component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualSchedule(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list recent clearfix">
            <li class="mlb_box">
                            <a href="/mlb/schedule/20190928/brewers-at-rockies-r2413">
                        <dl>
                <dt class="row-head">Sat 28th Sep</dt>
                    <dd class="opp team-mlb-narrow_small-right-COL">
                        @
                    </dd>
                                            <dd class="result row-0 result-loss">L 3 &ndash; 2</dd>
                                </dl>
                            </a>
                    </li>');
    }

    /**
     * A test of the standings component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStandings(TestResponse $response): void
    {
        $response->assertSee('<li class="div row_conf-NL misc-mlb-narrow-small-right-NL">NL Central Divison</li>

    ' . '
            <li class="header">
                            <div class="col  row_conf-NL">GB</div>
                            <div class="col  row_conf-NL">Win %</div>
                            <div class="col col-empty row_conf-NL"></div>
                    </li>
    ' . '
    ' . '
                    <li class="row-1 team">
            <span class="team-mlb-CHC">
                <a href="/mlb/teams/chicago-cubs">Cubs</a>
            </span>
                                        <div class="row-1 col">&ndash;</div>
                            <div class="row-1 col">.769</div>
                            <div class="row-1 col">10&ndash;3</div>
                    </li>');
        $response->assertSee('<li class="row_mlb-MIL team">
            <span class="team-mlb-MIL">
                <a href="/mlb/teams/milwaukee-brewers">Brewers</a>
            </span>
                                        <div class="row_mlb-MIL col">3.0</div>
                            <div class="row_mlb-MIL col">.429</div>
                            <div class="row_mlb-MIL col">6&ndash;8</div>
                    </li>');
    }

    /**
     * A test of the stats component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStats(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list stats clearfix">
        <li class="row-head title">Team Stats</li>
                    <li class="cell mlb_box">
                <dl class="clearfix">
                    <dt class="row-head">Batting Average</dt>
                        <dd class="rank">26th</dd>
                        <dd class="value"><span>.223</span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell mlb_box">
                <dl class="clearfix">
                    <dt class="row-head">ERA</dt>
                        <dd class="rank">11th</dd>
                        <dd class="value"><span>4.17</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the team leader component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualLeaders(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list leaders clearfix">
        <li class="row-head title">Team Leaders</li>
                    <li class="cell avg mlb_box">
                <dl>
                    <dt class="row-head">Batting Average</dt>
                        <dd class="name"><a href="/mlb/players/orlando-arcia-3244">O. Arcia</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Orlando Arcia"></dd>
                        <dd class="value"><span>.260</span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell era mlb_box">
                <dl>
                    <dt class="row-head">ERA</dt>
                        <dd class="name"><a href="/mlb/players/brandon-woodruff-3548">B. Woodruff</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Brandon Woodruff"></dd>
                        <dd class="value"><span>3.05</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the misc info on an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualMisc(TestResponse $response): void
    {
        $response->assertSee('<div class="tab-dropdown"><input type="hidden" id="season-schedule" '
            . 'name="season-schedule" value="2020" data-is-dropdown="true" >
<dl class="dropdown " data-id="season-schedule"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2020</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2020">2020</li>
                            <li  data-id="2019">2019</li>
                            <li  data-id="2018">2018</li>
                            <li  data-id="2017">2017</li>
                            <li  data-id="2016">2016</li>
                            <li  data-id="2015">2015</li>
                            <li  data-id="2014">2014</li>
                            <li  data-id="2013">2013</li>
                            <li  data-id="2012">2012</li>
                            <li  data-id="2011">2011</li>
                            <li  data-id="2010">2010</li>
                            <li  data-id="2009">2009</li>
                            <li  data-id="2008">2008</li>
                    </ul>
    </dd>
</dl>');
    }

    /**
     * A test of the schedule tab of an individual team page.
     * @return void
     */
    public function testIndividualTabSchedule(): void
    {
        $response = $this->getTab('/mlb/teams/milwaukee-brewers/schedule');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(3, 'sched');
        $response->assertJsonFragment([
            'date_fmt' => '9th Aug',
            'date_ymd' => '2020-08-09',
            'venue' => 'v',
            'opp' => 'team-mlb-right-CIN',
            'opp_wide' => 'team-mlb-narrow_small-right-CIN',
            'opp_id' => 'CIN',
            'opp_city' => 'Cincinnati',
            'opp_franchise' => 'Reds',
            'link' => '/mlb/schedule/20200809/reds-at-brewers-r234',
            'info' => '<span class="result-win">W 9 &ndash; 3</span>',
        ]);
        $response->assertJSONCount(4, 'gameTabs');
        $response->assertJSON([
            'gameTabs' => [
                [
                    'label' => [
                        'regular' => 'Season Series',
                        'playoff' => 'Playoff Series',
                    ],
                    'icon' => 'series',
                    'link' => 'series',
                ],
                [ 'label' => 'Scoring', 'icon' => 'scoring', 'link' => 'scoring' ],
                [ 'label' => 'Boxscore', 'icon' => 'boxscore', 'link' => false ],
                [ 'label' => 'Win Probability', 'icon' => 'probability', 'link' => 'probability' ],
            ],
        ]);
    }

    /**
     * A test of the roster tab of an individual team page.
     * @return void
     */
    public function testIndividualTabRoster(): void
    {
        $response = $this->getTab('/mlb/teams/milwaukee-brewers/roster');
        $response->assertStatus(200);
        $response->assertSee('<dl class="roster">');
        $response->assertSee('<dt class="row-head">Pitchers</dt>
                                            <dd class="row-0 mugshot mugshot-tiny"><img class="mugshot-tiny" '
            . 'loading="lazy" src="');
        $response->assertSee('" alt="Profile photo of Brett Anderson"></dd>
                <dd class="row-0 jersey">#25</dd>
                <dd class="row-0 name">
                    <a href="/mlb/players/brett-anderson-1379">Brett Anderson</a>
                                                        </dd>
                <dd class="row-0 pos">P</dd>
                <dd class="row-0 height">6&#39; 3&quot;</dd>
                <dd class="row-0 weight">230lb</dd>
                <dd class="row-0 dob">1st Feb 1988</dd>
                <dd class="row-0 birthplace">Midland, TX</dd>');
    }

    /**
     * A test of the depth chart tab of an individual team page.
     * @return void
     */
    public function testIndividualTabDepthChart(): void
    {
        $response = $this->getTab('/mlb/teams/milwaukee-brewers/depth-chart');
        $response->assertStatus(200);
        $response->assertSee('<div class="depth_chart">
        ' . '
        <ul class="inline_list diamond">
            <li class="field mowed-grass"></li>
            <li class="infield"></li>
            <li class="infield-grass mowed-grass"></li>
            <li class="batting-circle"></li>
            <li class="first base"></li>
            <li class="second base"></li>
            <li class="third base"></li>
            <li class="home-plate"></li>
            <li class="base-lines"></li>
            <li class="pitchers-mound"></li>
            <li class="pitchers-rubber"></li>
            <li class="first circle"></li>
            <li class="second circle"></li>
            <li class="third circle"></li>
            <li class="right batters-box"></li>
            <li class="left batters-box"></li>
        </ul>');
        $response->assertSee('<div class="blocks">
            <div class="block grid-tp-1-2 grid-tl-1-3">
    <dl class="mlb_box pos-C clearfix">
        <dt class="row-head">Catcher</dt>
                    ' . '
                            <dd class="mugshot-small"><img class="mugshot-small" loading="lazy" src="');
        $response->assertSee('" alt="Profile photo of Omar Narvaez"></dd>
                        ' . '
            <dd class="row-1 player">
                1. <a href="/mlb/players/omar-narvaez-3212"><span class="hidden-t hidden-d">Omar Narvaez</span>'
            . '<span class="hidden-m">O. Narvaez</span></a>
            </dd>
                    ' . '
                        ' . '
            <dd class="row-0 player">
                2. <a href="/mlb/players/jacob-nottingham-3736"><span class="hidden-t hidden-d">Jacob Nottingham</span>'
            . '<span class="hidden-m">J. Nottingham</span></a>
            </dd>
                    ' . '
                        ' . '
            <dd class="row-1 player">
                3. <a href="/mlb/players/david-freitas-3625"><span class="hidden-t hidden-d">David Freitas</span>'
            . '<span class="hidden-m">D. Freitas</span></a>
            </dd>
            </dl>
</div>');
    }

    /**
     * A test of the standings tab of an individual team page.
     * @return void
     */
    public function testIndividualTabStandings(): void
    {
        $n = null; // To shorten the array lengths.

        $response = $this->getTab('/mlb/teams/milwaukee-brewers/standings');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('chart.chart.renderTo', 'chart-standings');
        $response->assertJsonPath('chart.series.0.type', 'spline');
        $response->assertJsonPath('chart.series.0.data', [
            $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, 2, $n, $n, $n, 2, 3, $n,
        ]);
        $response->assertJsonPath('chart.series.1.type', 'column');
        $response->assertJsonPath('chart.series.1.data', [
            $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, 0, $n, $n, $n, 6, 0, $n,
        ]);
        $response->assertJsonPath('chart.series.2.type', 'column');
        $response->assertJsonPath('chart.series.2.data', [
            $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, 0, $n, $n, $n, 0, 0, $n,
        ]);
        $response->assertJsonPath('extraJS.standingsRecords.13', '4&ndash;5');
    }

    /**
     * A test of the power rank tab of an individual team page.
     * @return void
     */
    public function testIndividualTabPowerRanks(): void
    {
        $response = $this->getTab('/mlb/teams/milwaukee-brewers/power-ranks');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertSee('false');
    }

    /**
     * A test of the history tab of an individual team page.
     * @return void
     */
    public function testIndividualTabHistory(): void
    {
        $response = $this->getTab('/mlb/teams/milwaukee-brewers/history');
        $response->assertStatus(200);
        $response->assertSee('<dt class="league row-head row_league-MLB">
                <span class="icon misc-mlb-MLB">Major League Baseball</span>
            </dt>
            <dd class="league titles row-head row_league-MLB"><span>3 Division Titles</span>; '
            . '<span>1 Conference Title</span>; <span>0 Commissioner&#39;s Trophies</span></dd>');
        $response->assertSee('<dt class="row-head parts-1 season">2019</dt>
                    <dd class="row-0 record">89&ndash;73, 2nd NL Central</dd>
                ' . '
                    <dd class="row-0 playoffs">
                ' . '
                <ul class="inline_list matchups clearfix">
                                            <li class="result-loss">
                            <span class="hidden-m">Wild Card:</span>
                            <span class="hidden-t hidden-d">WC:</span>
                            Lost 0&ndash;1
                                                            v <span class="icon team-mlb-WSH">'
            . 'Washington Nationals</span>
                                                    </li>
                                    </ul>
            </dd>');
    }

    /**
     * A test of the various subtleties of an individual team page.
     * @return void
     */
    public function testIndividualMisc(): void
    {
        $response = $this->get('/mlb/teams/unknown-team');
        $response->assertStatus(404);

        $response = $this->get('/mlb/teams/unknown-team/unknown-tab');
        $response->assertStatus(404);

        $response = $this->get('/mlb/teams/unknown-team/schedule');
        $response->assertStatus(404);

        $response = $this->get('/mlb/teams/florida-marlins');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/teams/miami-marlins');

        $response = $this->get('/mlb/teams/milwaukee-brewers/home');
        $response->assertStatus(404);

        $response = $this->get('/mlb/teams/milwaukee-brewers/unknown');
        $response->assertStatus(404);

        $response = $this->get('/mlb/teams/milwaukee-brewers/power-ranks');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/mlb/teams/milwaukee-brewers#power-ranks');

        $response = $this->getTab('/mlb/teams/milwaukee-brewers/power-ranks/2019');
        $response->assertStatus(200);
        $response->assertJsonPath('chart.series.0.data', [
            5, 3, 10, 7, 8, 6, 6, 5, 8, 3, 2, 7, 11, 14, 14, 14, 6, 8, 20, 11, 8, 9, 15, 16, 8, 6, 9,
        ]);

        $response = $this->getTab('/mlb/teams/milwaukee-brewers/power-ranks/2039');
        $response->assertStatus(404);
    }

    /**
     * A test of the user favourite setting.
     * @return void
     */
    public function testFavourites(): void
    {
        // User must be logged in to perform either action.
        $response = $this->post('/mlb/teams/milwaukee-brewers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/mlb/teams/milwaukee-brewers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);

        // Log in for all future requests.
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Trying actions on a team that doesn't exist should fail.
        $response = $this->post('/mlb/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/mlb/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);

        // Ensure team is not already a favourite (but has the option to save as such).
        $response = $this->get('/mlb/teams/milwaukee-brewers');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // If we try to remove at this point, it should fail.
        $response = $this->delete('/mlb/teams/milwaukee-brewers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Perform the add, and validate the page updates accordingly.
        $response = $this->post('/mlb/teams/milwaukee-brewers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(201);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_is',
            'hover' => 'icon_right_favourite_remove',
        ]);

        $response = $this->get('/mlb/teams/milwaukee-brewers');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="rmv">
            <span class="icons icon_favourite_is icon_right_favourite_remove"></span>
            <span class="text">Remove from favourites</span>
        </button>
    </div>');

        // A second attempt should also fail.
        $response = $this->post('/mlb/teams/milwaukee-brewers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Now perform a removal, and validate the page updates accordingly.
        $response = $this->delete('/mlb/teams/milwaukee-brewers/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_not',
            'hover' => 'icon_right_favourite_add',
        ]);

        $response = $this->get('/mlb/teams/milwaukee-brewers');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
