<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class BScheduleTest extends MajorLeagueBaseCase
{
    /**
     * A test of the current date (though deterministically "current")
     * @return void
     */
    public function testCurrent(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-08-09 12:34:56');

        $response = $this->get('/mlb/schedule');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/schedule/20200809" data-date="20200809">
        <span class="date hidden-m">Sunday 9th August</span>
        <span class="date hidden-t hidden-d">Sun 9th Aug</span>
        <calendar');

        $response->assertSee('<ul class="inline_list mlb_box game result">
    ' . '
    <li class="status head">
        <strong>Final/10</strong>
            </li>
        ' . '
    <li class="team visitor win">
        <span class="score">5</span>
        <a href="/mlb/teams/cleveland-indians" class="team-mlb-narrow_small-CLE">Indians</a>
                    <span class="standings">(10&ndash;7)</span>
            </li>
    <li class="team home loss">
        <span class="score">4</span>
        <a href="/mlb/teams/chicago-white-sox" class="team-mlb-narrow_small-CWS">White Sox</a>
                    <span class="standings">(8&ndash;8)</span>
            </li>');
        $response->assertSee('<dt class="period head ">8</dt>
            <dt class="period head ">9</dt>
            <dt class="period head ">10</dt>
        <dt class="total head">R</dt>
    <dt class="total head">H</dt>
    <dt class="total head">E</dt>');
        $response->assertSee('<li class="venue head">Guaranteed Rate Field</li>');
        $response->assertSee('<dl class="pitchers clearfix">
        <dt>W:</dt>
            <dd>
                <a href="/mlb/players/phil-maton-3547">P. Maton</a>
                (1&ndash;0)
            </dd>
        <dt>L:</dt>
            <dd>
                <a href="/mlb/players/jimmy-cordero-3506">J. Cordero</a>
                (0&ndash;1)
            </dd>
        ' . '
                    <dt>SV:</dt>
                <dd>
                    <a href="/mlb/players/oliver-perez-813">O. Perez</a>
                    (1)
                </dd>
            </dl>');
        $response->assertSee('<dl class="home_runs clearfix">
        <dt>Home Runs:</dt>
                            <dd class="team visitor team-mlb-CLE"></dd>
                <dd class="list visitor">
                                            <em>None</em>
                                    </dd>
                            <dd class="team home team-mlb-CWS"></dd>
                <dd class="list home">
                                                                    '
            . '<span><a href="/mlb/players/jose-abreu-2585">J. Abreu</a> (3)</span>,
                                                                    '
            . '<span><a href="/mlb/players/james-mccann-2768">J. McCann</a> (3)</span>
                                    </dd>
                </dl>');
        $response->assertSee('<li class="links">
        <ul class="inline_list">
                            <li>
                    <a class="icon_game_probability" href="/mlb/schedule/20200809/indians-at-white-sox-r235'
            . '#probability"><em>Win Probability</em></a>
                </li>
                            <li>
                    <a class="icon_game_boxscore" href="/mlb/schedule/20200809/indians-at-white-sox-r235">'
            . '<em>Boxscore</em></a>
                </li>
                            <li>
                    <a class="icon_game_scoring" href="/mlb/schedule/20200809/indians-at-white-sox-r235#scoring">'
            . '<em>Scoring</em></a>
                </li>
                            <li>
                    <a class="icon_game_series" href="/mlb/schedule/20200809/indians-at-white-sox-r235#series">'
            . '<em>Season Series</em></a>
                </li>
                    </ul>
    </li>');

        $response->assertSee('<li class="status head"><strong>Suspended (Game Suspended)</strong></li>');
        $response->assertSee('<li class="status head"><strong>Postponed (COVID-19)</strong></li>');
    }

    /**
     * A test of the current date, identifying a future season (though deterministically "current")
     * @return void
     */
    public function testCurrentFuture(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-02-01 12:34:56');

        $response = $this->get('/mlb/schedule');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/schedule/20200723" data-date="20200723">
        <span class="date hidden-m">Thursday 23rd July</span>
        <span class="date hidden-t hidden-d">Thu 23rd Jul</span>
        <calendar');
    }

    /**
     * A test of dates with game previews
     * @return void
     */
    public function testPreview(): void
    {
        $response = $this->get('/mlb/schedule/20200810');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/schedule/20200810" data-date="20200810">
        <span class="date hidden-m">Monday 10th August</span>
        <span class="date hidden-t hidden-d">Mon 10th Aug</span>
        <calendar');

        $response->assertSee('<ul class="inline_list mlb_box game preview">
    <li class="status head">6:10pm PDT</li>
        <li class="team visitor">
        <a href="/mlb/teams/san-francisco-giants" class="team-mlb-narrow_small-SF">Giants</a>
                    <span class="standings">(7&ndash;11)</span>
            </li>
    <li class="team home">
        <a href="/mlb/teams/houston-astros" class="team-mlb-narrow_small-HOU">Astros</a>
                    <span class="standings">(7&ndash;9)</span>
            </li>
    ' . '
        ' . '
        ' . '
    <li class="odds line-with-icon">
    <dl>
        <dt>Odds:</dt>
        ' . '
                    <dd class="moneyline icon team-mlb-SF">170</dd>
            <dd class="moneyline icon team-mlb-HOU">-200</dd>
        ' . '
        ' . '
                    <dt class="over-under">Spread:</dt>                <dd class="spread icon team-mlb-HOU">-1.5</dd>
        ' . '
        ' . '
                    <dt class="over-under">O/U:</dt>
                <dd class="over-under">9</dd>
            </dl>
</li>
    ' . '
    <li class="venue head">Minute Maid Park</li>');
        $response->assertSee('<li class="probables">
    <dl>
        <dt>Probable Pitchers:</dt>
            <dd class="team-mlb-SF">
                                                    <em>Not Yet Announced</em>
                            <dd class="team-mlb-HOU">
                                                    <a href="/mlb/players/lance-mccullers-2897">L. McCullers</a>
                                            (3-2, 5.79 ERA, 1.42 WHIP)
                                        </dl>
</li>');
    }

    /**
     * A test of future dates with game previews
     * @return void
     */
    public function testFuturePreview(): void
    {
        $response = $this->get('/mlb/schedule/20200811');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/schedule/20200811" data-date="20200811">
        <span class="date hidden-m">Tuesday 11th August</span>
        <span class="date hidden-t hidden-d">Tue 11th Aug</span>
        <calendar');

        // Ensure that the standings match the previous day (which has the standings as of the 9th, not 10th).
        $response->assertSee('<ul class="inline_list mlb_box game preview">
    <li class="status head">6:10pm PDT</li>
        <li class="team visitor">
        <a href="/mlb/teams/san-francisco-giants" class="team-mlb-narrow_small-SF">Giants</a>
                    <span class="standings">(7&ndash;11)</span>
            </li>
    <li class="team home">
        <a href="/mlb/teams/houston-astros" class="team-mlb-narrow_small-HOU">Astros</a>
                    <span class="standings">(7&ndash;9)</span>
            </li>
    ' . '
        ' . '
        ' . '
        ' . '
    <li class="venue head">Minute Maid Park</li>');
    }

    /**
     * A test of miscellaneous controller actions
     * @return void
     */
    public function testControllerMisc(): void
    {
        // From the season switcher.
        $this->setTestDateTime('2020-09-10 12:34:56');

        $response = $this->get('/mlb/schedule/2020/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule/20200910/switcher');

        // Calendar widget exceptions.
        $response = $this->get('/mlb/schedule/2020/calendar');
        $response->assertStatus(200);
        $response->assertSee('{"exceptions":[]}');
    }

    /**
     * A test of ordered games based on user favourites
     * @return void
     */
    public function testScheduleFavourites(): void
    {
        // A control, the "regular" order.
        $response = $this->get('/mlb/schedule/20200809');
        $response->assertStatus(200);
        $response->assertDontSee('<h3>My Teams</h3>');
        $response->assertSeeInOrder([
            '<li class="venue head">Citizens Bank Park</li>', // ATL @ PHI.
            '<li class="venue head">Miller Park</li>', // CIN @ MIL.
        ]);

        // Log in as a user with favourites.
        $response = $this->post('/login', [
            'username' => 'sports_admin',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Load the schedule and ensure the games are ordered correctly.
        $response = $this->get('/mlb/schedule/20200809');
        $response->assertStatus(200);
        $response->assertSeeInOrder(['<h3>My Teams</h3>', '<h3>Other Teams</h3>']);
        $response->assertSeeInOrder([
            '<li class="venue head">Miller Park</li>', // CIN @ MIL.
            '<li class="venue head">Citizens Bank Park</li>', // ATL @ PHI.
        ]);

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
