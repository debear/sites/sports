<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class DNewsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the MLB news.
     * @return void
     */
    public function testNews(): void
    {
        $response = $this->get('/mlb/news');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/rival-cubs-brewers-set-key-rubber-match-082851712--mlb.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Rival Cubs, Brewers set for '
            . 'key rubber match</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/cards-eye-sweep-pirates-behind-hudson-081512398--mlb.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Cards eye sweep of Pirates '
            . 'behind Hudson</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/loaded-astros-look-stay-perfect-vs-ms-080854277--mlb.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Loaded Astros look to stay '
            . 'perfect vs. M&#39;s</a></h4>');

        $response->assertSee('<next class="onclick_target prev-next mini"><i class="fas fa-chevron-circle-right"></i>'
            . '</next>
    <bullets>
                    <bullet class="onclick_target" data-ref="1">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>
                    <bullet class="onclick_target" data-ref="2">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>');

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/maeda-twins-aim-series-win-vs-white-sox-074032188--mlb.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Maeda, Twins aim for series '
            . 'win vs. White Sox</a></h4>');
        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/yankees-vs-nationals-recap-7-044756675.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">Yankees vs. Nationals Recap 7/25</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.mlb.com/news/baseball-injury-updates" rel="noopener noreferrer" target="_blank" '
            . 'title="Opens in new tab/window">Injury updates: Strasburg, Buxton, Rendon</a></h4>');

        $response->assertSee('<li class="page_box unshaded_row ">
            <strong>1</strong>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="22"><em>22</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');
    }

    /**
     * A test of a specific MLB news page.
     * @return void
     */
    public function testNewsPage(): void
    {
        // Valid page.
        $response = $this->get('/mlb/news?page=2');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.mlb.com/news/baseball-injury-updates" rel="noopener noreferrer" target="_blank" '
            . 'title="Opens in new tab/window">Injury updates: Strasburg, Buxton, Rendon</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/orioles-vs-red-sox-recap-032407906.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">Orioles vs. Red Sox Recap 7/25</a></h4>');

        $response->assertSee('<li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="1">1</a>
        </li>
            <li class="page_box unshaded_row ">
            <strong>2</strong>
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="3">3</a>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="22"><em>22</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="3">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');

        // Error.
        $response = $this->get('/mlb/news?page=23');
        $response->assertStatus(404);
    }
}
