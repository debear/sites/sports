<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class FPlayoffsTest extends MajorLeagueBaseCase
{
    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/mlb/playoffs/2019');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; 2019 MLB Playoffs</title>');
        $response->assertSee('<li class="sel" data-id="2019">2019</li>');

        $response->assertSee('<li class="grid-3-7 rounds-4 conf-left">
    ' . '
            <h3 class="conf row_conf-AL misc-mlb-narrow-small-AL misc-mlb-narrow-small-right-AL">
            <span class="full">American League</span>
            <span class="short">AL</span>
        </h3>');
        $response->assertDontSee('<div class="byes">');
        $response->assertSee('<li class="grid-1-3 num-1-2">
                                    <fieldset class="matchup hover grass">
    <div class="team team-mlb-OAK loser">
        <span class="team">OAK <sup>4</sup></span>
        <span class="score">0</span>
    </div>
    <div class="team team-mlb-TB winner">
        <span class="team">TB <sup>5</sup></span>
        <span class="score">1</span>
    </div>
            <div class="link row-head">
                            <span class="icon_playoffs" data-link="/mlb/playoffs/2019/athletics-rays-11" '
            . 'data-modal="series">Summary</span>
                    </div>
    </fieldset>');
        $response->assertSee('<li class="champ rounds-4 grid-1-7 num-1-2">
            <h3 class="row-head">World Series</h3>
        <fieldset class="matchup hover grass">
    <div class="team team-mlb-HOU loser">
        <span class="team">HOU <sup>1</sup></span>
        <span class="score">3</span>
    </div>
    <div class="team team-mlb-WSH winner">
        <span class="team">WSH <sup>4</sup></span>
        <span class="score">4</span>
    </div>
            <div class="link row-head">
                            <span class="icon_playoffs" data-link="/mlb/playoffs/2019/astros-nationals-41" '
            . 'data-modal="series">Summary</span>
                    </div>
    </fieldset>
        ' . '
    <div class="logo ">
        <div class="large playoffs-mlb-large-0000 playoffs-mlb-large-2019"></div>
            </div>
</li>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/mlb/playoffs/2019/body');
        $response->assertStatus(200);
        $response->assertSee('<h1><span class="hidden-m">2019</span> MLB Playoffs</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/mlb/playoffs/2000');
        $response->assertStatus(404);
        $response = $this->get('/mlb/playoffs/2039');
        $response->assertStatus(404);
    }

    /**
     * A test of playoff series
     * @return void
     */
    public function testSeries(): void
    {
        $response = $this->get('/mlb/playoffs/2019/astros-nationals-41');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; 2019 MLB Playoffs &raquo; '
            . 'Houston Astros vs Washington Nationals</title>');
        $response->assertSee('<body class="section-mlb is_popup">

    ' . '
<h2 class="row-head">
    <span class="team-mlb-HOU">Astros</span> <sup>1</sup>
    -vs-
    <span class="team-mlb-WSH">Nationals</span> <sup>4</sup>
</h2>');
        $response->assertSee('<dl class="games">
            <dt class="row-head">
    Game 1
            <span class="status">Final</span>
    </dt>
    ' . '
    <dd class="game_date row-1">
        <span class="date">Tue 22nd Oct</span>
        <span class="time">5:08pm PDT</span>
    </dd>
    ' . '
    <dd class="game clearfix">
        <ul class="inline_list">
            <li class="visitor winner team-mlb-small-WSH">
                                    <span class="score">5</span>
                            </li>
            <li class="at">@</li>
            <li class="home loser team-mlb-small-right-HOU">
                                    <span class="score">4</span>
                            </li>
                            <li class="links">
                                                                '
            . '<a class="icon_game_series link-3" '
            . 'href="/mlb/schedule/20191022/nationals-at-astros-p411#series"><em>Playoff Series</em></a>
                                            <a class="icon_game_scoring link-2" '
            . 'href="/mlb/schedule/20191022/nationals-at-astros-p411#scoring"><em>Scoring</em></a>
                                            <a class="icon_game_boxscore link-1" '
            . 'href="/mlb/schedule/20191022/nationals-at-astros-p411"><em>Boxscore</em></a>
                                            <a class="icon_game_probability link-0" '
            . 'href="/mlb/schedule/20191022/nationals-at-astros-p411#probability"><em>Win Probability</em></a>
                                    </li>
                    </ul>
    </dd>');
        $response->assertSee('<dd class="summary row-1">
        Nationals win the World Series, 4&ndash;3
    </dd>');

        // Inverted series.
        $response = $this->get('/mlb/playoffs/2019/nationals-astros-41');
        $response->assertStatus(404);
    }
}
