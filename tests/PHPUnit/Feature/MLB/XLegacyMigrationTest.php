<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class XLegacyMigrationTest extends MajorLeagueBaseCase
{
    /**
     * A test of legacy migration of General URLs.
     * @return void
     */
    public function testGeneral(): void
    {
        // Index.
        $response = $this->get('/mlb/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb');

        $response = $this->get('/mlb/2019');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb');

        $response = $this->get('/mlb/2019/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb');

        // Standings.
        $response = $this->get('/mlb/2019/standings');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/standings/2019');

        $response = $this->get('/mlb/standings/expanded');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/mlb/standings/{$this->season}");

        $response = $this->get('/mlb/2019/standings/league');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/standings/2019');

        // News.
        $response = $this->get('/mlb/2019/news');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/news');

        $response = $this->get('/mlb/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/news');

        $response = $this->get('/mlb/2019/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/news');
    }

    /**
     * A test of legacy migration of Schedule URLs.
     * @return void
     */
    public function testSchedule(): void
    {
        // Schedule.
        $response = $this->get('/mlb/2000/schedule');
        $response->assertStatus(404);

        $response = $this->get('/mlb/2019/schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule/20191030');

        $response = $this->get('/mlb/schedule-20190901');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule/20190901');

        $response = $this->get('/mlb/2019/schedule-20190901');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule/20190901');

        // Playoffs.
        $response = $this->get('/mlb/2019/playoffs');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/playoffs/2019');

        $response = $this->get('/mlb/playoffs/11');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/playoffs');

        $response = $this->get('/mlb/2019/playoffs/11');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/playoffs/2019/athletics-rays-11');

        $response = $this->get('/mlb/2019/playoffs/99');
        $response->assertStatus(404);
    }

    /**
     * A test of legacy migration of individual Game URLs.
     * @return void
     */
    public function testGame(): void
    {
        // Invalid game type or season.
        $response = $this->get('/mlb/game-s123');
        $response->assertStatus(404);
        $response = $this->get('/mlb/game-rp123');
        $response->assertStatus(404);
        $response = $this->get('/mlb/2000/game-r123');
        $response->assertStatus(404);

        // Ambiguous season.
        $response = $this->get('/mlb/game-r123');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule');
        $response = $this->get('/mlb/game-r123-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule');

        // To the new format.
        $response = $this->get('/mlb/2019/game-r123');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule/20190406/blue-jays-at-indians-r123');

        $response = $this->get('/mlb/2019/game-r123-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/schedule/20190406/blue-jays-at-indians-r123'
            . '/boxscore');
    }

    /**
     * A test of legacy migration of Stats URLs.
     * @return void
     */
    public function testStats(): void
    {
        // Stats Summary.
        $response = $this->get('/mlb/2019/stats');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/stats');

        // Tables.
        $response = $this->get('/mlb/stats-ind-batting');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/stats/players/batting?season=2020&type=regular');

        $response = $this->get('/mlb/2019/stats-ind-batting');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/stats/players/batting?season=2019&type=regular');

        // By Team Stats.
        $response = $this->get('/mlb/stats-AAA');
        $response->assertStatus(404);

        $response = $this->get('/mlb/stats-MIL');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/stats/players/batting?team_id=MIL&season=2020&'
            . 'type=regular');

        $response = $this->get('/mlb/2019/stats-MIL');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/stats/players/batting?team_id=MIL&season=2019&'
            . 'type=regular');

        $response = $this->get('/mlb/stats-MIL-batting');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/stats/players/batting?team_id=MIL&season=2020&'
            . 'type=regular');

        $response = $this->get('/mlb/2019/stats-MIL-batting');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/stats/players/batting?team_id=MIL&season=2019&'
            . 'type=regular');
    }

    /**
     * A test of legacy migration of Team URLs.
     * @return void
     */
    public function testTeams(): void
    {
        $response = $this->get('/mlb/2019/teams');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/teams');

        $response = $this->get('/mlb/team-AAA');
        $response->assertStatus(404);

        $response = $this->get('/mlb/team-MIL');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/teams/milwaukee-brewers');

        $response = $this->get('/mlb/2019/team-MIL');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/teams/milwaukee-brewers');

        $response = $this->get('/mlb/team-MIL-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/teams/milwaukee-brewers');

        $response = $this->get('/mlb/2019/team-MIL-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/teams/milwaukee-brewers');
    }

    /**
     * A test of legacy migration of Player URLs.
     * @return void
     */
    public function testPlayers(): void
    {
         // List.
        $response = $this->get('/mlb/2019/players');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/players');

        // Individual.
        $response = $this->get('/mlb/player-0');
        $response->assertStatus(404);

        $response = $this->get('/mlb/player-2487');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/players/christian-yelich-2487');

        $response = $this->get('/mlb/player-2487-career');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/players/christian-yelich-2487/'
            . "{$this->season}/career");

        $response = $this->get('/mlb/2019/player-2487');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/players/christian-yelich-2487/2019');

        $response = $this->get('/mlb/2019/player-2487-career');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/players/christian-yelich-2487/2019/career');
    }

    /**
     * A test of the legacy migration of Power Rank URLs.
     * @return void
     */
    public function testPowerRanks(): void
    {
        $response = $this->get('/mlb/2019/power-ranks');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/power-ranks/2019');

        $response = $this->get('/mlb/power-ranks-12');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/mlb/power-ranks/{$this->season}/12");

        $response = $this->get('/mlb/2019/power-ranks-12');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/power-ranks/2019/12');
    }

    /**
     * A test of the legacy migration of Probable Pitcher URLs.
     * @return void
     */
    public function testProbables(): void
    {
        $response = $this->get('/mlb/2019/probables');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/probables');

        $response = $this->get('/mlb/probables-20190901');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/probables/20190901');

        $response = $this->get('/mlb/2019/probables-20190901');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/mlb/probables/20190901');
    }
}
