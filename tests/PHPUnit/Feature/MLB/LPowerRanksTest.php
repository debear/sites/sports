<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class LPowerRanksTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main page.
     * @return void
     */
    public function testMain(): void
    {
        $response = $this->get('/mlb/power-ranks');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Power Ranks</title>');
        $response->assertDontSee('<input type="hidden" id="switcher" name="switcher"');
        $response->assertSee('<fieldset class="info icon_info preseason">
    Power Ranks will be available shortly after the start of the season.
</fieldset>');
    }

    /**
     * A test when only the season was passed.
     * @return void
     */
    public function testSeasonOnly(): void
    {
        // A valid season.
        $response = $this->get('/mlb/power-ranks/2019');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Power Ranks</title>');
        $response->assertSee('<li class="sel" data-href="2019/27" data-id="27">Week 27</li>');
        $response->assertDontSee('<li  data-href="2019/28" data-id="28">Week 28</li>');
        $response->assertSee('<dl class="power_ranks ranks-27">');
        $response->assertSee('<dd class="dates">Covering games up to 29th September</dd>');

        $response->assertSee('<dt class="row-1 rank">1</dt>
    ' . '
                    <dd class="row-1 change">
            <span class="hidden-m"><strong>Last Week:</strong> 2</span>
            <span class="icon_pos_up">1</span>
        </dd>
        ' . '
    <dd class="row-1 team">
        <span class="logo hidden-m team-mlb-narrow_large-MIN"></span>
        <span class="logo hidden-t hidden-d team-mlb-narrow_small-MIN"></span>
        <h3><a href="/mlb/teams/minnesota-twins">Minnesota Twins</a></h3>
    </dd>
    ' . '
    <dd class="row-1 schedule">
        <dl>
            <dt>This week:</dt>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                            <span class="hidden-m hidden-t">
                                                        4 &ndash; 2
                                                            </span>
                                                    </span>
                        @
                        <span class="opp icon team-mlb-DET">DET</span>
                                            </dd>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                            <span class="hidden-m hidden-t">
                                                        5 &ndash; 1
                                                            </span>
                                                    </span>
                        @
                        <span class="opp icon team-mlb-DET">DET</span>
                                            </dd>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                            <span class="hidden-m hidden-t">
                                                        10 &ndash; 4
                                                            </span>
                                                    </span>
                        @
                        <span class="opp icon team-mlb-DET">DET</span>
                                            </dd>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                            <span class="hidden-m hidden-t">
                                                        6 &ndash; 2
                                                            </span>
                                                    </span>
                        @
                        <span class="opp icon team-mlb-KC">KC</span>
                                                    <span class="status hidden-m hidden-t">(F/7)</span>
                                            </dd>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                            <span class="hidden-m hidden-t">
                                                        4 &ndash; 3
                                                            </span>
                                                    </span>
                        @
                        <span class="opp icon team-mlb-KC">KC</span>
                                            </dd>
                                    <dd class="game">
                        <span class="score result-loss">
                            L
                                                            <span class="hidden-m hidden-t">
                                                        5 &ndash; 4
                                                            </span>
                                                    </span>
                        @
                        <span class="opp icon team-mlb-KC">KC</span>
                                            </dd>
                        </dl>
    </dd>');
        $response->assertDontSee('<dt class="row-1 rank">31</dt>');

        // A future season.
        $response = $this->get('/mlb/power-ranks/2021');
        $response->assertStatus(404);
    }

    /**
     * A test when specifying the week.
     * @return void
     */
    public function testSpecifyingWeek(): void
    {
        // A valid week.
        $response = $this->get('/mlb/power-ranks/2019/27');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Power Ranks &raquo; 2019 &raquo; '
            . 'Week 27</title>');
        $response->assertSee('<li class="sel" data-href="2019/27" data-id="27">Week 27</li>');

        // An uncalced week.
        $response = $this->get('/mlb/power-ranks/2019/28');
        $response->assertStatus(404);

        // A non-existant week.
        $response = $this->get('/mlb/power-ranks/2019/99');
        $response->assertStatus(404);
    }

    /**
     * A test of the first week.
     * @return void
     */
    public function testFirstWeek(): void
    {
        $response = $this->get('/mlb/power-ranks/2019/1');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Power Ranks &raquo; 2019 &raquo; '
            . 'Week 1</title>');
        $response->assertSee('<li class="sel" data-href="2019/1" data-id="1">Week 1</li>');
        $response->assertDontSee('<strong>Last Week:</strong>');
    }
}
