<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class EStandingsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the default season.
     * @return void
     */
    public function testDefault(): void
    {
        $response = $this->get('/mlb/standings');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; 2020 Standings</title>');
        $response->assertSee('<li class="sel" data-id="2020">2020</li>');
        $response->assertSee('<h3 class="row_conf-AL misc-mlb-narrow-small-AL '
            . 'misc-mlb-narrow-small-right-AL">American League</h3>');
        $response->assertSee('<dt class="row_conf-AL team head">AL East Division</dt>
    ' . '
                    <dd class="row-1 team">
            <span class="icon team-mlb-NYY">
                <a href="/mlb/teams/new-york-yankees" >New York Yankees</a>
                            </span>
                    </dd>');
        $response->assertSee('<dl class="standings div">
        ' . '
                    <dt class="row_conf-AL col-first col w  ">W</dt>
                    <dt class="row_conf-AL  col l  ">L</dt>
                    <dt class="row_conf-AL  col pct  ">Win %</dt>
                    <dt class="row_conf-AL  col gb  ">GB</dt>
                    <dt class="row_conf-AL  col rf  ">RS</dt>
                    <dt class="row_conf-AL  col ra  ">RA</dt>
                    <dt class="row_conf-AL  col rd  ">Diff</dt>
                    <dt class="row_conf-AL  col home  ">Home</dt>
                    <dt class="row_conf-AL  col visitor  ">Visitor</dt>
                    <dt class="row_conf-AL  col conf  ">League</dt>
                    <dt class="row_conf-AL  col div  ">Div</dt>
                    <dt class="row_conf-AL  col recent  ">Last 10</dt>
                    <dt class="row_conf-AL  col extras  ">Extras</dt>
                    <dt class="row_conf-AL  col onerun  ">1 Run</dt>
                ' . '
                                                <dd class="row-1 col-first col w ">');
    }

    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/mlb/standings/2020');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; 2020 Standings</title>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/mlb/standings/2020/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; MLB &raquo; 2020 Standings</title>');
        $response->assertSee('<h1>2020 Standings</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/mlb/standings/2000');
        $response->assertStatus(404);
        $response = $this->get('/mlb/standings/2039');
        $response->assertStatus(404);
    }
}
