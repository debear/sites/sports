<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class IPlayersTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-03-01 12:34:56');

        $response = $this->get('/mlb/players');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Players</h1>');

        // By Team.
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row_conf-AL misc-mlb-narrow-small-AL">AL</dt>
                                                    <dd class="row-0 team-mlb-BAL icon">'
            . '<a href="/mlb/teams/baltimore-orioles#roster">Baltimore Orioles</a></dd>');

        // Name Search.
        $response->assertSee('<dl class="mlb_box clearfix">
            <dt class="row-head">Search By Name</dt>
            <dt class="row-1">Player&#39;s Name:</dt>
                <dd class="row-0 name"><input type="text" class="textbox"></dd>
                <dd class="row-0 button"><button class="btn_small btn_green">Search</button></dd>
                <dd class="results hidden"></dd>
        </dl>');

        // Birthdays.
        $response->assertSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
        $response->assertSee('<div>
                <ul class="inline_list">
                                            <li class="row-0">'
            . '<a href="/mlb/players/michael-conforto-2982">Michael Conforto</a> (27)</li>
                                    </ul>
            </div>');
    }

    /**
     * A test of the main list with no birthdays.
     * @return void
     */
    public function testListNoBirthdays(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-02-29 12:34:56');

        $response = $this->get('/mlb/players');
        $response->assertStatus(200);

        // Available sections.
        $response->assertSee('<h1>MLB Players</h1>');
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row-head">Search By Name</dt>');
        $response->assertDontSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
    }

    /**
     * A test of the player search.
     * @return void
     */
    public function testSearch(): void
    {
        // Input length.
        $response = $this->get('/mlb/players/search/');
        $response->assertStatus(404); // Must have at least one character.
        $response = $this->get('/mlb/players/search/a');
        $response->assertStatus(400);
        $response = $this->get('/mlb/players/search/abc');
        $response->assertStatus(200);
        $response->assertExactJson([]); // This should also be an empty list.

        // A search result.
        $response = $this->get('/mlb/players/search/braun');
        $response->assertStatus(200);
        $response->assertExactJson([[
            'player_id' => 217,
            'first_name' => 'Ryan',
            'surname' => 'Braun',
            'url' => '/mlb/players/ryan-braun-217',
        ]]);
    }

    /**
     * A test of an individual batter's player page.
     * @return void
     */
    public function testIndividualBatter(): void
    {
        $response = $this->get('/mlb/players/jose-ramirez-2536');
        $response->assertStatus(200);
        $response->assertSee('<h1 class="team-mlb-narrow_large-right-CLE">Jose Ramirez'
            . '<span class="hidden-m jersey">#11</span><span class="hidden-m pos">3B</span></h1>');

        // Header.
        $response->assertSee('<ul class="inline_list summary">
            <li class="row-head">2020 Regular Season:</li>
                            <li class="stat grid-1-3">
                    <dl class="mlb_box">
                        <dt class="row-head">Batting Average</dt>
                            <dd class="row-1 value">.292</dd>
                            <dd class="row-0 rank">26th</dd>
                    </dl>
                </li>');
        $response->assertSee('<dl class="details">
                <dt class="height">Height:</dt>
            <dd class="height">5&#39; 9&quot;</dd>');

        $response->assertSee('<select id="subnav">
                            <option value="home" selected="selected">Summary</option>
                            <option value="career" >Career Stats</option>
                            <option value="gamelog" >Game Log</option>
                            <option value="splits" >Splits</option>
                            <option value="situational" >Situational</option>
                            <option value="battervspitcher" >Batter vs Pitcher</option>
                            <option value="charts" >Charts</option>
                    </select>');

        // Summary.
        $response->assertSee('<div class="subnav-home subnav-tab  clearfix">
                            <scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-batting" data-default="0" data-total="31" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2020</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-mlb-CLE">CLE</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2020</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-mlb-CLE">CLE</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">58</cell>
                                            <cell class="row-1 gs">58</cell>
                                            <cell class="row-1 pa">253</cell>
                                            <cell class="row-1 ab">219</cell>
                                            <cell class="row-1 r">45</cell>
                                            <cell class="row-1 h">64</cell>');
        $response->assertSee('<h3 class="games scrolltable-game-batting row-head">Last 10 Games</h3>
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game scrolltable-game-batting" data-default="0" '
            . 'data-total="30" data-current="spot">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Sun 9th Aug</cell>
                <cell class="row-1 opp">
                    @<span class="team-mlb-CWS">CWS</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-win">W 5 &ndash; 4</span>
                    <a class="icon_game_boxscore" href="/mlb/schedule/20200809/indians-at-white-sox-r235"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 spot"><span class="fa-stack fa-1x spot-starter" '
            . 'title="Started, Hit 2nd">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x">2</strong>
</span></cell>
                                            <cell class="row-1 pa">5</cell>
                                            <cell class="row-1 ab">5</cell>
                                            <cell class="row-1 r">1</cell>
                                            <cell class="row-1 h">1</cell>');
        $response->assertSee('<dl class="mlb_box awards clearfix">
    <dt class="row-head">Award History</dt>
                    <dt class="team-mlb-CLE">2020</dt>
            <dd>AL Silver Slugger (3B)</dd>
    </dl>');
    }

    /**
     * A test of an individual batter's player career tab.
     * @return void
     */
    public function testIndividualBatterTabsCareer(): void
    {
        $response = $this->getTab('/mlb/players/jose-ramirez-2536/career?group=batting');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="false" data-fielding="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-batting" data-default="0" data-total="31" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2020</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-mlb-CLE">CLE</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2020</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-mlb-CLE">CLE</span></cell>
            </row>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">58</cell>
                                            <cell class="row-1 gs">58</cell>
                                            <cell class="row-1 pa">253</cell>
                                            <cell class="row-1 ab">219</cell>
                                            <cell class="row-1 r">45</cell>
                                            <cell class="row-1 h">64</cell>
                                            <cell class="row-1 hr">17</cell>
                                            <cell class="row-1 rbi">46</cell>');
        $response->assertSee('<row>
                                            <cell class="row-0 gp">2</cell>
                                            <cell class="row-0 gs">2</cell>
                                            <cell class="row-0 pa">9</cell>
                                            <cell class="row-0 ab">7</cell>
                                            <cell class="row-0 r">1</cell>
                                            <cell class="row-0 h">3</cell>
                                            <cell class="row-0 hr">0</cell>
                                            <cell class="row-0 rbi">4</cell>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">129</cell>
                                            <cell class="row-1 gs">128</cell>
                                            <cell class="row-1 pa">536</cell>
                                            <cell class="row-1 ab">482</cell>
                                            <cell class="row-1 r">68</cell>
                                            <cell class="row-1 h">123</cell>
                                            <cell class="row-1 hr">23</cell>
                                            <cell class="row-1 rbi">83</cell>');
    }

    /**
     * A test of an individual batter's player gamelog tab.
     * @return void
     */
    public function testIndividualBatterTabsGameLog(): void
    {
        $response = $this->getTab('/mlb/players/jose-ramirez-2536/gamelog?season=2019%3Aregular&group=batting');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="false" data-fielding="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game '
            . 'scrolltable-game-batting" data-default="0" data-total="30" data-current="spot">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Thu 26th Sep</cell>
                <cell class="row-1 opp">
                    @<span class="team-mlb-CWS">CWS</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-loss">L 8 &ndash; 0</span>
                    <a class="icon_game_boxscore" href="/mlb/schedule/20190926/indians-at-white-sox-r2383"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 spot"><span class="fa-stack fa-1x spot-starter" '
            . 'title="Started, Hit 5th">
  <i class="fa fa-circle fa-stack-2x"></i>
  <strong class="fa-stack-1x">5</strong>
</span></cell>
                                            <cell class="row-1 pa">3</cell>
                                            <cell class="row-1 ab">3</cell>
                                            <cell class="row-1 r">0</cell>
                                            <cell class="row-1 h">0</cell>
                                            <cell class="row-1 hr">0</cell>
                                            <cell class="row-1 rbi">0</cell>');
    }

    /**
     * A test of the individual batter's player splits tab.
     * @return void
     */
    public function testIndividualBatterTabsSplits(): void
    {
        $response = $this->getTab('/mlb/players/jose-ramirez-2536/splits?season=2019%3Aregular&group=batting');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="false" data-fielding="true"></div>');

        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-splits-batting scrolltable-splits-home-road" data-default="0" data-total="26" '
            . 'data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head split">
                                    Home / Road
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            Home
                                    </cell>
            </row>
                                <row>
                <cell class="row-0 label">
                                            Road
                                    </cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">66</cell>
                                            <cell class="row-1 pa">268</cell>
                                            <cell class="row-1 ab">241</cell>
                                            <cell class="row-1 r">32</cell>
                                            <cell class="row-1 h">64</cell>
                                            <cell class="row-1 hr">8</cell>
                                            <cell class="row-1 rbi">38</cell>');
        $response->assertSee('<main>
        ' . '
        <top>
            <cell class="row-head split">
                                    All-Star Break
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            Post All-Star Break
                                    </cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">128</cell>
                                            <cell class="row-1 pa">542</cell>
                                            <cell class="row-1 ab">482</cell>
                                            <cell class="row-1 r">68</cell>
                                            <cell class="row-1 h">123</cell>
                                            <cell class="row-1 hr">23</cell>
                                            <cell class="row-1 rbi">83</cell>');
    }

    /**
     * A test of the individual batter's player situational tab.
     * @return void
     */
    public function testIndividualBatterTabsSituational(): void
    {
        $response = $this->getTab('/mlb/players/jose-ramirez-2536/situational?season=2019%3Aregular&group=batting');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="false" data-fielding="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-situational-batting scrolltable-situational-pitcher-info" data-default="0" data-total="18" '
            . 'data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head split">
                                    Pitcher Type
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            v SP
                                    </cell>
            </row>
                                <row>
                <cell class="row-0 label">
                                            v RP
                                    </cell>
            </row>
                                <row>
                <cell class="row-1 label">
                                            v LHP
                                    </cell>
            </row>
                                <row>
                <cell class="row-0 label">
                                            v RHP
                                    </cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">127</cell>
                                            <cell class="row-1 pa">328</cell>
                                            <cell class="row-1 ab">301</cell>
                                            <cell class="row-1 h">75</cell>
                                            <cell class="row-1 hr">16</cell>
                                            <cell class="row-1 rbi">50</cell>');

        $response->assertSee('<main>
        ' . '
        <top>
            <cell class="row-head split">
                                    Batter Role
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            As LHB
                                    </cell>
            </row>
                                <row>
                <cell class="row-0 label">
                                            As RHB
                                    </cell>
            </row>
            </main>');
    }

    /**
     * A test of the individual batter's player batter-vs-pitcher tab.
     * @return void
     */
    public function testIndividualBatterTabsBatterVsPitcher(): void
    {
        // Single season.
        $response = $this->getTab('/mlb/players/jose-ramirez-2536/battervspitcher?season=2019%3Aregular');
        $response->assertStatus(200);
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-battervspitcher-season scrolltable-battervspitcher-" data-default="0" data-total="20" '
            . 'data-current="seasons">
    <main>
        ' . '
        <top>
            <cell class="row_mlb-HOU split">
                                    <span class="icon team-mlb-HOU">Houston Astros</span>
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            <a href="/mlb/players/gerrit-cole-2439">Gerrit Cole</a>
                                    </cell>
            </row>
            </main>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 seasons">&ndash;</cell>
                                            <cell class="row-1 gp">2</cell>
                                            <cell class="row-1 pa">6</cell>
                                            <cell class="row-1 ab">4</cell>
                                            <cell class="row-1 h">0</cell>
                                            <cell class="row-1 1b">0</cell>
                                            <cell class="row-1 2b">0</cell>
                                            <cell class="row-1 3b">0</cell>
                                            <cell class="row-1 hr">0</cell>
                                            <cell class="row-1 rbi">1</cell>');

        // Across career.
        $response = $this->getTab('/mlb/players/jose-ramirez-2536/battervspitcher?season=0000%3Aregular');
        $response->assertStatus(200);
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-battervspitcher-career scrolltable-battervspitcher-" data-default="0" data-total="20" '
            . 'data-current="seasons">
    <main>
        ' . '
        <top>
            <cell class="row_mlb-HOU split">
                                    <span class="icon team-mlb-HOU">Houston Astros</span>
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            <a href="/mlb/players/gerrit-cole-2439">Gerrit Cole</a>
                                    </cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 seasons">3</cell>
                                            <cell class="row-1 gp">4</cell>
                                            <cell class="row-1 pa">12</cell>
                                            <cell class="row-1 ab">10</cell>
                                            <cell class="row-1 h">2</cell>
                                            <cell class="row-1 1b">0</cell>
                                            <cell class="row-1 2b">0</cell>
                                            <cell class="row-1 3b">1</cell>
                                            <cell class="row-1 hr">1</cell>
                                            <cell class="row-1 rbi">3</cell>');
    }

    /**
     * A test of an individual batter's player chart tab.
     * @return void
     */
    public function testIndividualBatterTabsCharts(): void
    {
        $response = $this->getTab('/mlb/players/jose-ramirez-2536/charts?season=2019%3Aregular&hands=CMB-CMB');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="hands" data-CMB-CMB="true" '
            . 'data-L-CMB="false" data-R-CMB="false" data-CMB-L="false" data-L-L="false" data-R-L="true" '
            . 'data-CMB-R="false" data-L-R="true" data-R-R="false" data-default="CMB-CMB"></div>');

        // Matches.
        $response->assertSee("<dl class=\"mlb_box heatzones clearfix\">
    <dt class=\"row-head\">Zone Charts</dt>
    <dd class=\"strikezone\">
        <div class=\"z o t l\"><span>.231</span></div>
        <div class=\"z o t r\"><span>.115</span></div>
        <div class=\"z o b l\"><span>.198</span></div>
        <div class=\"z o b r\"><span>.325</span></div>
        <div class=\"z i t l\"><span>.167</span></div>
        <div class=\"z i t c\"><span>.314</span></div>
        <div class=\"z i t r\"><span>.083</span></div>
        <div class=\"z i m l\"><span>.238</span></div>
        <div class=\"z i m c\"><span>.333</span></div>
        <div class=\"z i m r\"><span>.333</span></div>
        <div class=\"z i b l\"><span>.273</span></div>
        <div class=\"z i b c\"><span>.347</span></div>
        <div class=\"z i b r\"><span>.235</span></div>
    </dd>
</dl>");
        $response->assertSee("<dl class=\"mlb_box spray-chart clearfix\">
    <dt class=\"row-head\">Spray Charts</dt>
    <dd class=\"num\">
        <div class=\"l\">13.5%</div>
        <div class=\"l-c\">16.0%</div>
        <div class=\"c\">18.0%</div>
        <div class=\"c-r\">24.4%</div>
        <div class=\"r\">28.2%</div>
    </dd>
    <dd class=\"field\">
        <div class=\"l\"></div>
        <div class=\"l-c\"></div>
        <div class=\"c\"></div>
        <div class=\"c-r\"></div>
        <div class=\"r\"></div>
    </dd>
</dl>");
        $response->assertSee('.heatzones .z.o.t.l { background-color: #000018; }
    .heatzones .z.o.t.r { background-color: #00008b; }');
    }

    /**
     * A test of the individual pitcher's player page.
     * @return void
     */
    public function testIndividualPitcher(): void
    {
        $response = $this->get('/mlb/players/jacob-degrom-2638');
        $response->assertStatus(200);
        $response->assertSee('<h1 class="team-mlb-narrow_large-right-NYM">Jacob deGrom'
            . '<span class="hidden-m jersey">#48</span><span class="hidden-m pos">P</span></h1>');

        // Header.
        $response->assertSee('<ul class="inline_list summary">
            <li class="row-head">2020 Regular Season:</li>
                            <li class="stat grid-1-3">
                    <dl class="mlb_box">
                        <dt class="row-head">Wins</dt>
                            <dd class="row-1 value">4</dd>
                            <dd class="row-0 rank">37th</dd>
                    </dl>
                </li>');
        $response->assertSee('<dl class="details">
                <dt class="height">Height:</dt>
            <dd class="height">6&#39; 4&quot;</dd>');

        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="career">Career Stats</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="gamelog">Game Log</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="splits">Splits</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="situational">Situational</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="battervspitcher">Batter vs Pitcher</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="charts">Charts</item>
                <sep class="right"></sep>
    </desk>');

        // Summary.
        $response->assertSee('<div class="subnav-home subnav-tab  clearfix">
                            <scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-pitching" data-default="0" data-total="41" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2020</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-mlb-NYM">NYM</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">12</cell>
                                            <cell class="row-1 gs">12</cell>
                                            <cell class="row-1 w">4</cell>
                                            <cell class="row-1 l">2</cell>
                                            <cell class="row-1 sv">0</cell>
                                            <cell class="row-1 hld">0</cell>
                                            <cell class="row-1 bs">0</cell>
                                            <cell class="row-1 era">2.38</cell>');
        $response->assertSee('<dl class="mlb_box repertoire clearfix">
    <dt class="row-head">Pitch Repertoire</dt>
    <dt class="fastball">Fastball Velocity:</dt>
        <dd class="fastball">98.33mph &ndash; <span class="icon icon_pos_up">+1.34mph since 2019</span></dt>');
    }

    /**
     * A test of the individual pitcher's player career tab.
     * @return void
     */
    public function testIndividualPitcherTabsCareer(): void
    {
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/career?group=pitching');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="true" data-fielding="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-pitching" data-default="0" data-total="41" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2020</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-mlb-NYM">NYM</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2019</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-0 team"><span class="team-mlb-NYM">NYM</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">12</cell>
                                            <cell class="row-1 gs">12</cell>
                                            <cell class="row-1 w">4</cell>
                                            <cell class="row-1 l">2</cell>
                                            <cell class="row-1 sv">0</cell>
                                            <cell class="row-1 hld">0</cell>
                                            <cell class="row-1 bs">0</cell>
                                            <cell class="row-1 era">2.38</cell>
                                            <cell class="row-1 whip">0.956</cell>
                                            <cell class="row-1 ip">68.0</cell>');
        $response->assertSee('<row>
                                            <cell class="row-0 gp">33</cell>
                                            <cell class="row-0 gs">32</cell>
                                            <cell class="row-0 w">11</cell>
                                            <cell class="row-0 l">8</cell>
                                            <cell class="row-0 sv">0</cell>
                                            <cell class="row-0 hld">0</cell>
                                            <cell class="row-0 bs">0</cell>
                                            <cell class="row-0 era">2.43</cell>
                                            <cell class="row-0 whip">0.971</cell>
                                            <cell class="row-0 ip">204.0</cell>');
    }

    /**
     * A test of the individual pitcher's player gamelog tab.
     * @return void
     */
    public function testIndividualPitcherTabsGameLog(): void
    {
        // Game Log.
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/gamelog?season=2019%3Aregular&group=pitching');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="true" data-fielding="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game '
            . 'scrolltable-game-pitching" data-default="0" data-total="34" data-current="decision">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Wed 25th Sep</cell>
                <cell class="row-1 opp">
                    v<span class="team-mlb-MIA">MIA</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-win">W 10 &ndash; 3</span>
                    <a class="icon_game_boxscore" href="/mlb/schedule/20190925/marlins-at-mets-r2367"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 decision"><span class="win">W, 11&ndash;8</span></cell>
                                            <cell class="row-1 era">2.43</cell>
                                            <cell class="row-1 whip">0.971</cell>
                                            <cell class="row-1 ip">7.0</cell>
                                            <cell class="row-1 k">7</cell>
                                            <cell class="row-1 h">2</cell>');
    }

    /**
     * A test of the individual pitcher's player splits tab.
     * @return void
     */
    public function testIndividualPitcherTabsSplits(): void
    {
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/splits?season=2019%3Aregular&group=pitching');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="true" data-fielding="true"></div>');

        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-splits-pitching scrolltable-splits-pitcher-info" data-default="0" data-total="39" '
            . 'data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head split">
                                    Pitcher Role
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            As SP
                                    </cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">32</cell>
                                            <cell class="row-1 gs">32</cell>
                                            <cell class="row-1 w">11</cell>
                                            <cell class="row-1 l">8</cell>
                                            <cell class="row-1 sv">0</cell>
                                            <cell class="row-1 hld">0</cell>
                                            <cell class="row-1 bs">0</cell>
                                            <cell class="row-1 era">2.43</cell>
                                            <cell class="row-1 whip">0.971</cell>');

        $response->assertSee('<main>
        ' . '
        <top>
            <cell class="row-head split">
                                    Home / Road
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            Home
                                    </cell>
            </row>
                                <row>
                <cell class="row-0 label">
                                            Road
                                    </cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">17</cell>
                                            <cell class="row-1 gs">17</cell>
                                            <cell class="row-1 w">5</cell>
                                            <cell class="row-1 l">5</cell>
                                            <cell class="row-1 sv">0</cell>
                                            <cell class="row-1 hld">0</cell>
                                            <cell class="row-1 bs">0</cell>
                                            <cell class="row-1 era">2.50</cell>
                                            <cell class="row-1 whip">0.889</cell>
                                            <cell class="row-1 ip">108.0</cell>');
        $response->assertSee('<row>
                                            <cell class="row-0 gp">15</cell>
                                            <cell class="row-0 gs">15</cell>
                                            <cell class="row-0 w">6</cell>
                                            <cell class="row-0 l">3</cell>
                                            <cell class="row-0 sv">0</cell>
                                            <cell class="row-0 hld">0</cell>
                                            <cell class="row-0 bs">0</cell>
                                            <cell class="row-0 era">2.34</cell>
                                            <cell class="row-0 whip">1.063</cell>
                                            <cell class="row-0 ip">96.0</cell>');
    }

    /**
     * A test of the individual pitcher's player situational tab.
     * @return void
     */
    public function testIndividualPitcherTabsSituational(): void
    {
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/situational?season=2019%3Aregular&group=pitching');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-batting="true" '
            . 'data-pitching="true" data-fielding="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-situational-pitching scrolltable-situational-batter-info" data-default="0" data-total="15" '
            . 'data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head split">
                                    Batter Type
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            v LHB
                                    </cell>
            </row>
                                <row>
                <cell class="row-0 label">
                                            v RHB
                                    </cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">32</cell>
                                            <cell class="row-1 gs">32</cell>
                                            <cell class="row-1 k">102</cell>
                                            <cell class="row-1 h">67</cell>
                                            <cell class="row-1 hr">9</cell>');
    }

    /**
     * A test of the individual pitcher's player batter-vs-pitcher tab.
     * @return void
     */
    public function testIndividualPitcherTabsBatterVsPitcher(): void
    {
        // Single season.
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/battervspitcher?season=2019%3Aregular');
        $response->assertStatus(200);
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-battervspitcher-season scrolltable-battervspitcher-" data-default="0" data-total="20" '
            . 'data-current="seasons">
    <main>
        ' . '
        <top>
            <cell class="row_mlb-ATL split">
                                    <span class="icon team-mlb-ATL">Atlanta Braves</span>
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            <a href="/mlb/players/freddie-freeman-1802">Freddie Freeman</a>
                                    </cell>
            </row>
            </main>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 seasons">&ndash;</cell>
                                            <cell class="row-1 gp">4</cell>
                                            <cell class="row-1 pa">13</cell>
                                            <cell class="row-1 ab">10</cell>
                                            <cell class="row-1 h">3</cell>
                                            <cell class="row-1 1b">2</cell>
                                            <cell class="row-1 2b">0</cell>
                                            <cell class="row-1 3b">0</cell>
                                            <cell class="row-1 hr">1</cell>
                                            <cell class="row-1 rbi">2</cell>');

        // Across career.
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/battervspitcher?season=0000%3Aregular');
        $response->assertStatus(200);
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-splits '
            . 'scrolltable-battervspitcher-career scrolltable-battervspitcher-" data-default="0" data-total="20" '
            . 'data-current="seasons">
    <main>
        ' . '
        <top>
            <cell class="row_mlb-ATL split">
                                    <span class="icon team-mlb-ATL">Atlanta Braves</span>
                            </cell>
        </top>
        ' . '
                                <row>
                <cell class="row-1 label">
                                            <a href="/mlb/players/freddie-freeman-1802">Freddie Freeman</a>
                                    </cell>
            </row>
            </main>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 seasons">8</cell>
                                            <cell class="row-1 gp">23</cell>
                                            <cell class="row-1 pa">68</cell>
                                            <cell class="row-1 ab">60</cell>
                                            <cell class="row-1 h">15</cell>
                                            <cell class="row-1 1b">10</cell>
                                            <cell class="row-1 2b">2</cell>
                                            <cell class="row-1 3b">0</cell>
                                            <cell class="row-1 hr">3</cell>
                                            <cell class="row-1 rbi">6</cell>');
    }

    /**
     * A test of the individual pitcher's player chart tab.
     * @return void
     */
    public function testIndividualPitcherTabsCharts(): void
    {
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/charts?season=2019%3Aregular');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="hands" data-CMB-CMB="false" '
            . 'data-L-CMB="false" data-R-CMB="false" data-CMB-L="false" data-L-L="false" data-R-L="false" '
            . 'data-CMB-R="true" data-L-R="true" data-R-R="true" data-default="CMB-R"></div>');

        $response->assertSee("<dl class=\"mlb_box heatzones clearfix\">
    <dt class=\"row-head\">Zone Charts</dt>
    <dd class=\"strikezone\">
        <div class=\"z o t l\"><span>.222</span></div>
        <div class=\"z o t r\"><span>.180</span></div>
        <div class=\"z o b l\"><span>.175</span></div>
        <div class=\"z o b r\"><span>.116</span></div>
        <div class=\"z i t l\"><span>.207</span></div>
        <div class=\"z i t c\"><span>.286</span></div>
        <div class=\"z i t r\"><span>.195</span></div>
        <div class=\"z i m l\"><span>.167</span></div>
        <div class=\"z i m c\"><span>.295</span></div>
        <div class=\"z i m r\"><span>.288</span></div>
        <div class=\"z i b l\"><span>.292</span></div>
        <div class=\"z i b c\"><span>.316</span></div>
        <div class=\"z i b r\"><span>.175</span></div>
    </dd>
</dl>");
        $response->assertSee('.heatzones .z.o.t.l { background-color: #180000; }');

        // Implicitly redirected here.
        $response = $this->getTab('/mlb/players/jacob-degrom-2638/charts?season=2019%3Aregular&hands=CMB-R');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="hands" data-CMB-CMB="false" '
            . 'data-L-CMB="false" data-R-CMB="false" data-CMB-L="false" data-L-L="false" data-R-L="false" '
            . 'data-CMB-R="true" data-L-R="true" data-R-R="true" data-default="CMB-R"></div>');

        $response->assertSee("<dl class=\"mlb_box heatzones clearfix\">
    <dt class=\"row-head\">Zone Charts</dt>
    <dd class=\"strikezone\">
        <div class=\"z o t l\"><span>.222</span></div>
        <div class=\"z o t r\"><span>.180</span></div>
        <div class=\"z o b l\"><span>.175</span></div>
        <div class=\"z o b r\"><span>.116</span></div>
        <div class=\"z i t l\"><span>.207</span></div>
        <div class=\"z i t c\"><span>.286</span></div>
        <div class=\"z i t r\"><span>.195</span></div>
        <div class=\"z i m l\"><span>.167</span></div>
        <div class=\"z i m c\"><span>.295</span></div>
        <div class=\"z i m r\"><span>.288</span></div>
        <div class=\"z i b l\"><span>.292</span></div>
        <div class=\"z i b c\"><span>.316</span></div>
        <div class=\"z i b r\"><span>.175</span></div>
    </dd>
</dl>");
            $response->assertSee("<dl class=\"mlb_box spray-chart clearfix\">
    <dt class=\"row-head\">Spray Charts</dt>
    <dd class=\"num\">
        <div class=\"l\">13.8%</div>
        <div class=\"l-c\">22.8%</div>
        <div class=\"c\">19.0%</div>
        <div class=\"c-r\">24.4%</div>
        <div class=\"r\">20.0%</div>
    </dd>
    <dd class=\"field\">
        <div class=\"l\"></div>
        <div class=\"l-c\"></div>
        <div class=\"c\"></div>
        <div class=\"c-r\"></div>
        <div class=\"r\"></div>
    </dd>
</dl>");
        $response->assertSee('.heatzones .z.o.t.l { background-color: #180000; }');
        $response->assertSee('.spray-chart .field .l { background-color: #580000; }');

        // As a batter, with incorrect batting hand.
        $response = $this->getTab(
            '/mlb/players/jacob-degrom-2638/charts?season=2019%3Aregular&group=batting&hands=R-R'
        );
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="hands" data-CMB-CMB="false" '
            . 'data-L-CMB="true" data-R-CMB="false" data-CMB-L="false" data-L-L="true" data-R-L="false" '
            . 'data-CMB-R="false" data-L-R="true" data-R-R="false" data-default="L-R"></div>');
    }
}
