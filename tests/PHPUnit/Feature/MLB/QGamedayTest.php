<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class QGamedayTest extends MajorLeagueBaseCase
{
    /**
     * A test of the starting lineups for the current date
     * @return void
     */
    public function testLineupsCurrent(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-08-09 12:34:56');

        $response = $this->get('/mlb/lineups');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/lineups/20200809" data-date="20200809">
        <span class="date hidden-m">Sunday 9th August</span>
        <span class="date hidden-t hidden-d">Sun 9th Aug</span>
        <calendar');

        // Game 1 of Doubleheader.
        $this->assertLineupsCurrentGame1($response);
        // Game 2 of Doubleheader.
        $this->assertLineupsCurrentGame2($response);
        // Suspended.
        $response->assertSee('<li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                    <ul class="inline_list mlb_box game_list not_played lineups clearfix">
                        <li class="info head">Suspended (Game Suspended)</li>
                                                <li class="team visitor row_mlb-BAL">
                            <span class="icon team-mlb-BAL">Orioles</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home row_mlb-WSH">
                            <span class="icon team-mlb-WSH">Nationals</span>
                        </li>
                    </ul>
                </li>');
    }

    /**
     * Assertions for Game 1 lineups (separated to reduce method length)
     * @param TestResponse $response The response we are testing against.
     * @return void
     */
    public function assertLineupsCurrentGame1(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list mlb_box game_list lineups clearfix">
                    <li class="info head">10:05am PDT, Citizens Bank Park</li>
                                        <li class="team visitor row_mlb-ATL">
                        <span class="icon team-mlb-ATL">Braves</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home row_mlb-PHI">
                        <span class="icon team-mlb-PHI">Phillies</span>
                    </li>
                    <li class="list visitor"><ul class="inline_list">
                                    <li class="row-0 head spot ">1</li>
                            <li class="row-0 pos">RF</li>
                        <li class="row-0 name"><a href="/mlb/players/ronald-acuna-3810">Ronald Acuna</a> (R)</li>
                                <li class="row-1 head spot ">2</li>
                            <li class="row-1 pos">SS</li>
                        <li class="row-1 name"><a href="/mlb/players/dansby-swanson-3265">Dansby Swanson</a> (R)</li>
                                <li class="row-0 head spot ">3</li>
                            <li class="row-0 pos">1B</li>
                        <li class="row-0 name"><a href="/mlb/players/freddie-freeman-1802">Freddie Freeman</a> (L)</li>
                                <li class="row-1 head spot ">4</li>
                            <li class="row-1 pos">LF</li>
                        <li class="row-1 name"><a href="/mlb/players/marcell-ozuna-2392">Marcell Ozuna</a> (R)</li>
                                <li class="row-0 head spot ">5</li>
                            <li class="row-0 pos">DH</li>
                        <li class="row-0 name"><a href="/mlb/players/nick-markakis-341">Nick Markakis</a> (L)</li>
                                <li class="row-1 head spot ">6</li>
                            <li class="row-1 pos">3B</li>
                        <li class="row-1 name"><a href="/mlb/players/johan-camargo-3346">Johan Camargo</a> (S)</li>
                                <li class="row-0 head spot ">7</li>
                            <li class="row-0 pos">2B</li>
                        <li class="row-0 name"><a href="/mlb/players/adeiny-hechavarria-2271">Adeiny Hechavarria</a>'
            . ' (R)</li>
                                <li class="row-1 head spot ">8</li>
                            <li class="row-1 pos">C</li>
                        <li class="row-1 name"><a href="/mlb/players/tyler-flowers-1584">Tyler Flowers</a> (R)</li>
                                <li class="row-0 head spot ">9</li>
                            <li class="row-0 pos">CF</li>
                        <li class="row-0 name"><a href="/mlb/players/ender-inciarte-2351">Ender Inciarte</a> (L)</li>
                                <li class="row-1 head spot dbl-width">SP</li>
                        <li class="row-1 name"><a href="/mlb/players/huascar-ynoa-3943">Huascar Ynoa</a> (RHP)</li>
            </ul>
</li>');
    }

    /**
     * Assertions for Game 2 lineups (separated to reduce method length)
     * @param TestResponse $response The response we are testing against.
     * @return void
     */
    public function assertLineupsCurrentGame2(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list mlb_box game_list lineups clearfix">
                    <li class="info head">8:33pm PDT, Citizens Bank Park</li>
                                        <li class="team visitor row_mlb-ATL">
                        <span class="icon team-mlb-ATL">Braves</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home row_mlb-PHI">
                        <span class="icon team-mlb-PHI">Phillies</span>
                    </li>
                    <li class="list visitor"><ul class="inline_list">
                                    <li class="row-0 head spot ">1</li>
                            <li class="row-0 pos">CF</li>
                        <li class="row-0 name"><a href="/mlb/players/ronald-acuna-3810">Ronald Acuna</a> (R)</li>
                                <li class="row-1 head spot ">2</li>
                            <li class="row-1 pos">SS</li>
                        <li class="row-1 name"><a href="/mlb/players/dansby-swanson-3265">Dansby Swanson</a> (R)</li>
                                <li class="row-0 head spot ">3</li>
                            <li class="row-0 pos">1B</li>
                        <li class="row-0 name"><a href="/mlb/players/freddie-freeman-1802">Freddie Freeman</a> (L)</li>
                                <li class="row-1 head spot ">4</li>
                            <li class="row-1 pos">DH</li>
                        <li class="row-1 name"><a href="/mlb/players/marcell-ozuna-2392">Marcell Ozuna</a> (R)</li>
                                <li class="row-0 head spot ">5</li>
                            <li class="row-0 pos">C</li>
                        <li class="row-0 name"><a href="/mlb/players/travis-d-arnaud-2518">Travis d&#39;Arnaud</a>'
            . ' (R)</li>
                                <li class="row-1 head spot ">6</li>
                            <li class="row-1 pos">RF</li>
                        <li class="row-1 name"><a href="/mlb/players/nick-markakis-341">Nick Markakis</a> (L)</li>
                                <li class="row-0 head spot ">7</li>
                            <li class="row-0 pos">LF</li>
                        <li class="row-0 name"><a href="/mlb/players/adam-duvall-2697">Adam Duvall</a> (R)</li>
                                <li class="row-1 head spot ">8</li>
                            <li class="row-1 pos">3B</li>
                        <li class="row-1 name"><a href="/mlb/players/austin-austin-4086">Austin Austin</a> (R)</li>
                                <li class="row-0 head spot ">9</li>
                            <li class="row-0 pos">2B</li>
                        <li class="row-0 name"><a href="/mlb/players/johan-camargo-3346">Johan Camargo</a> (S)</li>
                                <li class="row-1 head spot dbl-width">SP</li>
                        <li class="row-1 name"><a href="/mlb/players/max-fried-3343">Max Fried</a> (LHP)</li>
            </ul>
</li>');
    }

    /**
     * A test of the starting lineups for a specific date
     * @return void
     */
    public function testLineupsDated(): void
    {
        $response = $this->get('/mlb/lineups/20200810');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/lineups/20200810" data-date="20200810">
        <span class="date hidden-m">Monday 10th August</span>
        <span class="date hidden-t hidden-d">Mon 10th Aug</span>
        <calendar');

        $response->assertSee('<li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <ul class="inline_list mlb_box game_list lineups clearfix">
                    <li class="info head">5:10pm PDT, Miller Park</li>
                                        <li class="team visitor row_mlb-MIN">
                        <span class="icon team-mlb-MIN">Twins</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home row_mlb-MIL">
                        <span class="icon team-mlb-MIL">Brewers</span>
                    </li>
                    <li class="list visitor"><ul class="inline_list">
                                    <li class="row-0 head spot ">1</li>
                            <li class="row-0 pos">RF</li>
                        <li class="row-0 name"><a href="/mlb/players/max-kepler-3070">Max Kepler</a> (L)</li>
                                <li class="row-1 head spot ">2</li>
                            <li class="row-1 pos">SS</li>
                        <li class="row-1 name"><a href="/mlb/players/jorge-polanco-2699">Jorge Polanco</a> (S)</li>');
    }

    /**
     * A test of miscellaneous controller actions
     * @return void
     */
    public function testLineupsControllerMisc(): void
    {
        $this->setTestDateTime('2020-09-10 12:34:56');

        $response = $this->get('/mlb/lineups/2020/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/mlb/lineups/20200910/switcher');
    }

    /**
     * A test of the probable pitchers for the current date
     * @return void
     */
    public function testProbablesCurrent(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-08-09 12:34:56');

        $response = $this->get('/mlb/probables');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/probables/20200809" data-date="20200809">
        <span class="date hidden-m">Sunday 9th August</span>
        <span class="date hidden-t hidden-d">Sun 9th Aug</span>
        <calendar');

        $response->assertSee('<li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <ul class="inline_list mlb_box game_list probables clearfix">
                    <li class="info head">10:05am PDT, Citizens Bank Park</li>
                                        <li class="team visitor row_mlb-ATL">
                        <span class="icon team-mlb-ATL">Braves</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home row_mlb-PHI">
                        <span class="icon team-mlb-PHI">Phillies</span>
                    </li>
                    <li class="list visitor"><ul class="inline_list probable">
    <li class="name row-0">
                    <a href="/mlb/players/huascar-ynoa-3943">Huascar Ynoa</a> (RHP)
            </li>
    <li class="mugshot mugshot-medium">
                    <img class="mugshot-medium" loading="lazy" src="//cdn.debear.test/'
            . 'gRUo_dAEg0HtEyrcdyvgMyv84yPbtKtwErJh8LSnUzPbdKq8Cyu8S10uMV1YrKoADN1A7yTNTN3uyVLpNe" '
            . 'alt="Profile photo of Huascar Ynoa">
            </li>
                        <li class="stats row-1">
                <em>First appearance this season</em>
            </li>
                <li class="notes">
            </li>
            <li class="catcher">
            <strong>Probable Catcher:</strong> <a href="/mlb/players/travis-d-arnaud-2518">Travis d&#39;Arnaud</a>
        </li>
    </ul>
</li>
                    <li class="list home"><ul class="inline_list probable">
    <li class="name row-0">
                    <em>To Be Announced</em>
            </li>
    <li class="mugshot mugshot-medium">
                    <img class="mugshot-medium" loading="lazy" src="//cdn.debear.test/'
            . 'gRUo_dAEg0HtEyrcdyvgMyv84yPbtKtwErJh8LSnUzPbdKq8Cyu8S10uMV1YrKoADN1A7yTNTN3uyVLpNe" '
            . 'alt="Profile photo silhouette">
            </li>
        <li class="notes">
            </li>
    </ul>
</li>
                </ul>
            </li>');

        $response->assertSee('<li class="list home"><ul class="inline_list probable">
    <li class="name row-0">
                    <a href="/mlb/players/brandon-woodruff-3548">Brandon Woodruff</a> (RHP)
            </li>
    <li class="mugshot mugshot-medium">
                    <img class="mugshot-medium" loading="lazy" src="//cdn.debear.test/'
            . 'gRUo_dAEg0HtEyrcdyvgMyv84yPbtKtwErJh8LSnUzPbdKq8Cyu8S10uMV1YrKoADN1A7yTNTN3uyVLpNe" '
            . 'alt="Profile photo of Brandon Woodruff">
            </li>
                        <li class="stats row-1">
                1&ndash;1, 2.08 ERA, 0.923 WHIP
            </li>
                <li class="notes">
                    <strong>MLB.com\'s Notes:</strong> Woodruff is pitching like an ace again, with 21 strikeouts in '
            . 'his first three starts. He got up to 92 pitches last time out, and is positioned to be the first '
            . 'Brewers starter to last 100 pitches.
            </li>
            <li class="catcher">
            <strong>Possible Catcher:</strong> <a href="/mlb/players/omar-narvaez-3212">Omar Narvaez</a>
        </li>
    </ul>
</li>');

        $response->assertSee('<ul class="inline_list mlb_box game_list not_played probables clearfix">
                        <li class="info head">Suspended (Game Suspended)</li>
                                                <li class="team visitor row_mlb-BAL">
                            <span class="icon team-mlb-BAL">Orioles</span>
                        </li>
                        <li class="head at">@</li>
                        <li class="team home row_mlb-WSH">
                            <span class="icon team-mlb-WSH">Nationals</span>
                        </li>
                    </ul>');
    }

    /**
     * A test of the probable pitchers for a specific date
     * @return void
     */
    public function testProbablesDated(): void
    {
        $response = $this->get('/mlb/probables/20200810');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/probables/20200810" data-date="20200810">
        <span class="date hidden-m">Monday 10th August</span>
        <span class="date hidden-t hidden-d">Mon 10th Aug</span>
        <calendar');

        $response->assertSee('<ul class="inline_list mlb_box game_list probables clearfix">
                    <li class="info head">4:30pm PDT, Fenway Park</li>
                                        <li class="team visitor row_mlb-TB">
                        <span class="icon team-mlb-TB">Rays</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home row_mlb-BOS">
                        <span class="icon team-mlb-BOS">Red Sox</span>
                    </li>
                    <li class="list visitor"><ul class="inline_list probable">
    <li class="name row-0">
                    <a href="/mlb/players/ryan-yarbrough-3782">Ryan Yarbrough</a> (LHP)
            </li>
    <li class="mugshot mugshot-medium">
                    <img class="mugshot-medium" loading="lazy" src="//cdn.debear.test/'
            . 'gRUo_dAEg0HtEyrcdyvgMyv84yPbtKtwErJh8LSnUzPbdKq8Cyu8S10uMV1YrKoADN1A7yTNTN3uyVLpNe" '
            . 'alt="Profile photo of Ryan Yarbrough">
            </li>
                        <li class="stats row-1">
                0&ndash;2, 3.78 ERA, 1.140 WHIP
            </li>
                <li class="notes">
            </li>
            <li class="catcher">
            <strong>Probable Catcher:</strong> <a href="/mlb/players/mike-zunino-2440">Mike Zunino</a>
        </li>
    </ul>
</li>
                    <li class="list home"><ul class="inline_list probable">
    <li class="name row-0">
                    <em>To Be Announced</em>
            </li>
    <li class="mugshot mugshot-medium">
                    <img class="mugshot-medium" loading="lazy" src="//cdn.debear.test/'
            . 'gRUo_dAEg0HtEyrcdyvgMyv84yPbtKtwErJh8LSnUzPbdKq8Cyu8S10uMV1YrKoADN1A7yTNTN3uyVLpNe" '
            . 'alt="Profile photo silhouette">
            </li>
        <li class="notes">
            </li>
    </ul>
</li>
                </ul>');
    }

    /**
     * A test of the probable pitchers when the list is empty
     * @return void
     */
    public function testProbablesEmpty(): void
    {
        $response = $this->get('/mlb/probables/20200811');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/mlb/probables/20200811" data-date="20200811">
        <span class="date hidden-m">Tuesday 11th August</span>
        <span class="date hidden-t hidden-d">Tue 11th Aug</span>
        <calendar');

        $response->assertSee('To Be Announced');
        $response->assertDontSee('<em>First appearance this season</em>');
        $response->assertDontSee(' ERA, ');
    }

    /**
     * A test of miscellaneous controller actions
     * @return void
     */
    public function testProbablesControllerMisc(): void
    {
        $this->setTestDateTime('2020-09-10 12:34:56');

        $response = $this->get('/mlb/probables/2020/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/mlb/probables/20200910/switcher');
    }
}
