<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class GStatsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the stat leaders page.
     * @return void
     */
    public function testLeaders(): void
    {
        $response = $this->get('/mlb/stats');
        $response->assertStatus(200);
        $response->assertSee('<h1>2020 MLB Stat Leaders</h1>');

        // Batting Average.
        $response->assertSee('<dl class="mlb_box stat clearfix">
                <dt class="row-head name">Batting Average</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-mlb-NYY">'
            . '<a href="/mlb/players/dj-lemahieu-1942">DJ LeMahieu</a></span>
                            <span class="stat">.371</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-mlb-ATL">'
            . '<a href="/mlb/players/marcell-ozuna-2392">Marcell Ozuna</a></span>
                            <span class="stat">.333</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-mlb-SF">'
            . '<a href="/mlb/players/donovan-solano-2188">Donovan Solano</a></span>
                            <span class="stat">.326</span>
                        </dd>');

        // Home Runs.
        $response->assertSee('<dl class="mlb_box stat clearfix">
                <dt class="row-head name">Home Runs</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-mlb-NYY">'
            . '<a href="/mlb/players/luke-voit-3569">Luke Voit</a></span>
                            <span class="stat">23</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">4</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-mlb-LAA">'
            . '<a href="/mlb/players/mike-trout-1989">Mike Trout</a></span>
                            <span class="stat">17</span>
                        </dd>
                                                        <dt class="row-head pos">=</dt>
                        <dd class="row-0 entity">
                            <em>2 More Players Tied</em>
                            <span class="stat">17</span>
                        </dd>');
        $response->assertDontSee('<span class="icon team-mlb-SD">'
            . '<a href="/mlb/players/fernando-tatis-jr-4062">Fernando Tatis Jr.</a></span>
                            <span class="stat">17</span>');
        $response->assertDontSee('<span class="icon team-mlb-CLE">'
            . '<a href="/mlb/players/jose-ramirez-2536">Jose Ramirez</a></span>
                            <span class="stat">17</span>');

        // ERA formatting.
        $response->assertSee('<span class="icon team-mlb-CLE">'
            . '<a href="/mlb/players/shane-bieber-3825">Shane Bieber</a></span>
                            <span class="stat">1.63</span>');

        // Teams: Team Batting Average.
        $response->assertSee('<dl class="mlb_box stat clearfix">
                <dt class="row-head name">Team Batting Average</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-mlb-NYM">'
            . '<a href="/mlb/teams/new-york-mets">Mets</a></span>
                            <span class="stat">.272</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-mlb-SF">'
            . '<a href="/mlb/teams/san-francisco-giants">Giants</a></span>
                            <span class="stat">.264</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-mlb-CWS">'
            . '<a href="/mlb/teams/chicago-white-sox">White Sox</a></span>
                            <span class="stat">.260</span>
                        </dd>');

        // Team lists.
        $response->assertSee('<dl class="mlb_box teams clearfix">
    <dt class="row-head">Sortable Stats by Team</dt>
            <dd ><a class="no_underline team-mlb-ARI" href="/mlb/stats/players/batting?team_id=ARI&season=2020&'
            . 'type=regular&stat=avg"></a></dd>');
        $response->assertSee('<dd class="split-point"><a class="no_underline team-mlb-MIL" '
            . 'href="/mlb/stats/players/batting?team_id=MIL&season=2020&type=regular&stat=avg"></a></dd>');
    }

    /**
     * A test of the controller handling.
     * @return void
     */
    public function testHandler(): void
    {
        // Invalid option.
        $response = $this->get('/mlb/stats/players/unknown');
        $response->assertStatus(404);

        // Default options.
        $response = $this->get('/mlb/stats/players/batting');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Hitting Leaders</h1>');

        // Filters.
        $response->assertSee('<dl class="dropdown " data-id="filter_team"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>All MLB</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="">All MLB</li>
                            <li  data-id="ARI"><span class="icon team-mlb-ARI">ARI</span></li>');
        $response->assertSee('<dl class="dropdown " data-id="filter_season"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2020 Regular Season</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2020::regular">2020 Regular Season</li>
                    </ul>
    </dd>
</dl>');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Hitting</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="player::batting">Individual Hitting</li>
                            <li  data-id="player::pitching">Individual Pitching</li>
                            <li  data-id="player::fielding">Individual Fielding</li>
                            <li  data-id="team::batting">Team Hitting</li>
                            <li  data-id="team::pitching">Team Pitching</li>
                            <li  data-id="team::fielding">Team Fielding</li>
                    </ul>
    </dd>
</dl>');

        // Stat selection.
        $response->assertSee('<scrolltable class="scroll-x scrolltable-player-batting" data-default="13"'
            . ' data-total="29" data-current="avg">');
    }

    /**
     * A test of the individual batting stats page.
     * @return void
     */
    public function testPlayerBatting(): void
    {
        $response = $this->get('/mlb/stats/players/batting?season=2020&type=regular&stat=avg');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Hitting Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Hitting</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="player::batting">Individual Hitting</li>
                            <li  data-id="player::pitching">Individual Pitching</li>
                            <li  data-id="player::fielding">Individual Fielding</li>
                            <li  data-id="team::batting">Team Hitting</li>
                            <li  data-id="team::pitching">Team Pitching</li>
                            <li  data-id="team::fielding">Team Fielding</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-NYY">'
            . '<a href="/mlb/players/dj-lemahieu-1942">DJ. LeMahieu</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-WSH">'
            . '<a href="/mlb/players/juan-soto-3819">J. Soto</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-ATL">'
            . '<a href="/mlb/players/freddie-freeman-1802">F. Freeman</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-WSH">'
            . '<a href="/mlb/players/trea-turner-3016">T. Turner</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-ATL">'
            . '<a href="/mlb/players/marcell-ozuna-2392">M. Ozuna</a></span></cell>
            </row>');

        // Multi-team player.
        $response->assertSee('<row>
                                <cell class="row-head pos sort_order">42</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-ARI"><span class="icon team-mlb-MIA">'
            . '<a href="/mlb/players/starling-marte-2258">S. Marte</a></span></span></cell>
            </row>');
    }

    /**
     * A test of the individual pitching stats page.
     * @return void
     */
    public function testPlayerPitching(): void
    {
        $response = $this->get('/mlb/stats/players/pitching?season=2020&type=regular&stat=era');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Pitching Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Pitching</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::batting">Individual Hitting</li>
                            <li class="sel" data-id="player::pitching">Individual Pitching</li>
                            <li  data-id="player::fielding">Individual Fielding</li>
                            <li  data-id="team::batting">Team Hitting</li>
                            <li  data-id="team::pitching">Team Pitching</li>
                            <li  data-id="team::fielding">Team Fielding</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-CLE">'
            . '<a href="/mlb/players/shane-bieber-3825">S. Bieber</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-CIN">'
            . '<a href="/mlb/players/trevor-bauer-2234">T. Bauer</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-CWS">'
            . '<a href="/mlb/players/dallas-keuchel-2220">D. Keuchel</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-SD">'
            . '<a href="/mlb/players/dinelson-lamet-3530">D. Lamet</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-CHC">'
            . '<a href="/mlb/players/yu-darvish-2127">Y. Darvish</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual fielding stats page.
     * @return void
     */
    public function testPlayerFielding(): void
    {
        $response = $this->get('/mlb/stats/players/fielding?season=2020&type=regular&stat=dp');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Fielding Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Fielding</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::batting">Individual Hitting</li>
                            <li  data-id="player::pitching">Individual Pitching</li>
                            <li class="sel" data-id="player::fielding">Individual Fielding</li>
                            <li  data-id="team::batting">Team Hitting</li>
                            <li  data-id="team::pitching">Team Pitching</li>
                            <li  data-id="team::fielding">Team Fielding</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-COL">'
            . '<a href="/mlb/players/trevor-story-3094">T. Story</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-ATL">'
            . '<a href="/mlb/players/freddie-freeman-1802">F. Freeman</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-CLE">'
            . '<a href="/mlb/players/carlos-santana-1714">C. Santana</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-ARI">'
            . '<a href="/mlb/players/christian-walker-2810">C. Walker</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-STL">'
            . '<a href="/mlb/players/paul-goldschmidt-2015">P. Goldschmidt</a></span></cell>
            </row>');
    }

    /**
     * A test of the team batting stats page.
     * @return void
     */
    public function testTeamBatting(): void
    {
        $response = $this->get('/mlb/stats/teams/batting?season=2020&type=regular&stat=avg');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Hitting Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Hitting</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::batting">Individual Hitting</li>
                            <li  data-id="player::pitching">Individual Pitching</li>
                            <li  data-id="player::fielding">Individual Fielding</li>
                            <li class="sel" data-id="team::batting">Team Hitting</li>
                            <li  data-id="team::pitching">Team Pitching</li>
                            <li  data-id="team::fielding">Team Fielding</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-NYM">'
            . '<a href="/mlb/teams/new-york-mets">Mets</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-ATL">'
            . '<a href="/mlb/teams/atlanta-braves">Braves</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-BOS">'
            . '<a href="/mlb/teams/boston-red-sox">Red Sox</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-WSH">'
            . '<a href="/mlb/teams/washington-nationals">Nationals</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-SF">'
            . '<a href="/mlb/teams/san-francisco-giants">Giants</a></span></cell>
            </row>');
    }

    /**
     * A test of the team pitching stats page.
     * @return void
     */
    public function testTeamPitching(): void
    {
        $response = $this->get('/mlb/stats/teams/pitching?season=2020&type=regular&stat=era');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Pitching Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Pitching</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::batting">Individual Hitting</li>
                            <li  data-id="player::pitching">Individual Pitching</li>
                            <li  data-id="player::fielding">Individual Fielding</li>
                            <li  data-id="team::batting">Team Hitting</li>
                            <li class="sel" data-id="team::pitching">Team Pitching</li>
                            <li  data-id="team::fielding">Team Fielding</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-LAD">'
            . '<a href="/mlb/teams/los-angeles-dodgers">Dodgers</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-CLE">'
            . '<a href="/mlb/teams/cleveland-indians">Indians</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-TB">'
            . '<a href="/mlb/teams/tampa-bay-rays">Rays</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-MIN">'
            . '<a href="/mlb/teams/minnesota-twins">Twins</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-OAK">'
            . '<a href="/mlb/teams/oakland-athletics">Athletics</a></span></cell>
            </row>');
    }

    /**
     * A test of the team fielding stats page.
     * @return void
     */
    public function testTeamFielding(): void
    {
        $response = $this->get('/mlb/stats/teams/fielding?season=2020&type=regular&stat=pct');
        $response->assertStatus(200);
        $response->assertSee('<h1>MLB Fielding Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Fielding</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::batting">Individual Hitting</li>
                            <li  data-id="player::pitching">Individual Pitching</li>
                            <li  data-id="player::fielding">Individual Fielding</li>
                            <li  data-id="team::batting">Team Hitting</li>
                            <li  data-id="team::pitching">Team Pitching</li>
                            <li class="sel" data-id="team::fielding">Team Fielding</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-HOU">'
            . '<a href="/mlb/teams/houston-astros">Astros</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-MIN">'
            . '<a href="/mlb/teams/minnesota-twins">Twins</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-SEA">'
            . '<a href="/mlb/teams/seattle-mariners">Mariners</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-mlb-CIN">'
            . '<a href="/mlb/teams/cincinnati-reds">Reds</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-OAK">'
            . '<a href="/mlb/teams/oakland-athletics">Athletics</a></span></cell>
            </row>');
    }

    /**
     * A test of a single team players batting stats page.
     * @return void
     */
    public function testTeamStats(): void
    {
        // Invalid team arguments (all ignored, MLB leaders listed).
        $response = $this->get('/mlb/stats/players/batting?team_id=&season=2020&type=regular&stat=avg');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Stats &raquo; Players &raquo; Hitting</title>');
        $response->assertSee('<h1>MLB Hitting Leaders</h1>');
        $response = $this->get('/mlb/stats/players/batting?team_id=FLA&season=2020&type=regular&stat=avg');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Stats &raquo; Players &raquo; Hitting</title>');
        $response->assertSee('<h1>MLB Hitting Leaders</h1>');

        // Correct list.
        $response = $this->get('/mlb/stats/players/batting?team_id=MIL&season=2020&type=regular&stat=avg');
        $response->assertStatus(200);
        $response->assertSee('<h1><span class="hidden-m hidden-tp">Milwaukee</span> Brewers Hitting Leaders</h1>');
        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; Stats &raquo; Brewers &raquo; Hitting</title>');

        $response->assertDontSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-NYY">'
            . '<a href="/mlb/players/dj-lemahieu-1942">DJ. LeMahieu</a></span></cell>
            </row>');
        $response->assertSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-mlb-MIL">'
            . '<a href="/mlb/players/orlando-arcia-3244">O. Arcia</a></span></cell>
            </row>');
    }
}
