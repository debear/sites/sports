<?php

namespace Tests\PHPUnit\Feature\MLB;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class KAwardsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/mlb/awards');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; MLB &raquo; MLB Award Winners</title>');
        $response->assertSee('<h1>MLB Award Winners</h1>');

        $response->assertSee('<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="mlb_box award multi clearfix">
        ' . '
        <h3 class="row-head">NL Gold Glove</h3>

        ' . '
        <p class="descrip">Awarded to the players who exhibited superior individual fielding performance at '
            . 'his position in the National League</p>

                    ' . '
            <dl class="winners">
                <dt>2020 Winners:</dt>
                                    <dt class="pos">C</dt>
                        <dd>
                                                            <span class="team-mlb-CIN">
                                                        '
            . '<a href="/mlb/players/tucker-barnhart-2587">Tucker Barnhart</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">1B</dt>
                        <dd>
                                                            <span class="team-mlb-CHC">
                                                        '
            . '<a href="/mlb/players/anthony-rizzo-1957">Anthony Rizzo</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">2B</dt>
                        <dd>
                                                            <span class="team-mlb-STL">
                                                        '
            . '<a href="/mlb/players/kolten-wong-2514">Kolten Wong</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">SS</dt>
                        <dd>
                                                            <span class="team-mlb-CHC">
                                                        '
            . '<a href="/mlb/players/javier-baez-2738">Javier Baez</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">3B</dt>
                        <dd>
                                                            <span class="team-mlb-COL">
                                                        '
            . '<a href="/mlb/players/nolan-arenado-2388">Nolan Arenado</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">LF</dt>
                        <dd>
                                                            <span class="team-mlb-STL">
                                                        '
            . '<a href="/mlb/players/tyler-o-neill-3777">Tyler O&#39;Neill</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">CF</dt>
                        <dd>
                                                            <span class="team-mlb-SD">
                                                        '
            . '<a href="/mlb/players/trent-grisham-4149">Trent Grisham</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">RF</dt>
                        <dd>
                                                            <span class="team-mlb-LAD">
                                                        '
            . '<a href="/mlb/players/mookie-betts-2701">Mookie Betts</a>
                                                            </span>
                                                    </dd>
                                    <dt class="pos">P</dt>
                        <dd>
                                                            <span class="team-mlb-ATL">
                                                        '
            . '<a href="/mlb/players/max-fried-3343">Max Fried</a>
                                                            </span>
                                                    </dd>
                            </dl>
        ' . '
        ' . '
        <p class="link"><a class="all-time" data-link="/mlb/awards/nl-gold-glove-8">All-Time Winners</a></p>
    </fieldset>
</li>');
    }

    /**
     * A test of the all-time winners.
     * @return void
     */
    public function testAllTime(): void
    {
        // A non-award.
        $response = $this->get('/mlb/awards/does-not-exist-99');
        $response->assertStatus(404);
        // An actual award.
        $response = $this->get('/mlb/awards/nl-gold-glove-8');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'season' => '2020',
            'winner' => [
                [
                    'player' => 'Tucker Barnhart',
                    'team' => 'Cincinnati Reds',
                    'team_id' => 'CIN',
                    'pos' => 'C',
                ],
                [
                    'player' => 'Anthony Rizzo',
                    'team' => 'Chicago Cubs',
                    'team_id' => 'CHC',
                    'pos' => '1B',
                ],
                [
                    'player' => 'Kolten Wong',
                    'team' => 'St Louis Cardinals',
                    'team_id' => 'STL',
                    'pos' => '2B',
                ],
                [
                    'player' => 'Javier Baez',
                    'team' => 'Chicago Cubs',
                    'team_id' => 'CHC',
                    'pos' => 'SS',
                ],
                [
                    'player' => 'Nolan Arenado',
                    'team' => 'Colorado Rockies',
                    'team_id' => 'COL',
                    'pos' => '3B',
                ],
                [
                    'player' => 'Tyler O&#39;Neill',
                    'team' => 'St Louis Cardinals',
                    'team_id' => 'STL',
                    'pos' => 'LF',
                ],
                [
                    'player' => 'Trent Grisham',
                    'team' => 'San Diego Padres',
                    'team_id' => 'SD',
                    'pos' => 'CF',
                ],
                [
                    'player' => 'Mookie Betts',
                    'team' => 'Los Angeles Dodgers',
                    'team_id' => 'LAD',
                    'pos' => 'RF',
                ],
                [
                    'player' => 'Max Fried',
                    'team' => 'Atlanta Braves',
                    'team_id' => 'ATL',
                    'pos' => 'P',
                ]
            ],
        ]);
    }
}
