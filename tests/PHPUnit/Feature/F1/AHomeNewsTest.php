<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class AHomeNewsTest extends MotorsportBaseCase
{
    /**
     * A test of the F1 homepage.
     * @return void
     */
    public function testHomepage(): void
    {
        $response = $this->get('/f1');
        $response->assertStatus(200);

        // Header.
        $response->assertSee('<flag  class="flag au dim"></flag>
    <info>Cnc</info>');
        $response->assertSee('<flag  class="flag fr dim"></flag>
    <info>28/06</info>');

        // Last / Next Race.
        $response->assertSee('<h3 class="row_head">Last Race: <span class="flag_right flag16_right_cn">'
            . 'Chinese Grand Prix</span></h3>');
        $response->assertSee('<dd class="cancelled">
                <div class="box info icon icon_info">The 2020 race was cancelled.</div>');

        $response->assertSee('<h3 class="row_head">Next Race: <span class="flag_right flag16_right_nl">Dutch '
            . 'Grand Prix</span></h3>');
        $response->assertSee('<dd class="venue">Circuit Zandvoort, Zandvoort</dd>');
        $response->assertSee('<dt>Qualifying:</dt>
                <dd class="time">Saturday 2nd May, 15:00 (Local)</dd>
                <dd class="weather icon_weather_clouds">
    Clouds, 16 &deg;C / 62 &deg;F
            <span class="precip">, 20% chance rain</span>
    </dd>');
        $response->assertSee('<dt>Race:</dt>
            <dd class="time">Sunday 3rd May, 15:10 (Local)</dd>
            <dd class="weather icon_weather_rain">
    Rain, 18 &deg;C / 65 &deg;F
            <span class="precip">, 87% chance rain</span>
    </dd>');

        // News.
        $response->assertSee('<a href="https://www.motorsport.com/f1/news/alfa-romeo-mclaren-australian-gp-race-'
            . 'quarantine/4752661/?utm_source=RSS&utm_medium=referral&utm_campaign=RSS-F1&utm_term=News&utm_content='
            . 'www" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Alfa Romeo: It '
            . 'would&#39;ve been &quot;unfair&quot; to race without McLaren</a>');
        $response->assertSee('<a href="https://www.autosport.com/f1/news/148690/red-bull-would-have-protested-mercedes-'
            . 'das-system" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Red Bull would '
            . 'have protested Mercedes DAS system in Australia</a>');
        $response->assertSee('<li class="row_0 summary">
    A member of tyre supplier Pirelli&#39;s Formula 1 team tests positive for coronavirus in Melbourne.
</li>');
        $response->assertSee('<a href="https://www.motorsport.com/f1/news/australian-gp-no-danger-coronavirus-paddock'
            . '/4755801/?utm_source=RSS&utm_medium=referral&utm_campaign=RSS-F1&utm_term=News&utm_content=www" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">&quot;No suggestion '
            . 'whatsoever&quot; Australian GP put F1 in danger</a>');

        // Standings.
        $response->assertSee('<dt class="podium-1 pos">1</dt>
                <dd class="podium-1 name"><span class="flag_right flag16_right_th">'
            . '<a href="/f1/drivers/alexander-albon-76/2020"');
        $response->assertSee('<dt class="podium-3 pos">3</dt>
                <dd class="podium-3 name"><span class="flag_right flag16_right_fr">'
            . '<a href="/f1/drivers/pierre-gasly-71/2020"');
        $response->assertSee('<dt class="podium-1 pos">1</dt>
                <dd class="podium-1 name"><span class="flag_right flag16_right_it">'
            . '<a href="/f1/teams/alfa-romeo/2020"');
        $response->assertSee('<dt class="row_1 pos">9</dt>
                <dd class="row_1 name"><span class="flag_right flag16_right_fr">'
            . '<a href="/f1/teams/renault/2020"');
    }

    /**
     * A test of the F1 homepage with a historical header argument.
     * @return void
     */
    public function testHomepageHistorical(): void
    {
        $response = $this->get('/f1/?header=2019');
        $response->assertStatus(200);

        // 2019 Header.
        $response->assertSee('<a href="/f1/races/2019-australia" class="flag au "></a>
    <info><span class="flag_right flag16_right_fi">BOT</span></info>');
        $response->assertSee('<a href="/f1/races/2019-abu-dhabi" class="flag ae "></a>
    <info><span class="flag_right flag16_right_gb">HAM</span></info>');

        // 2019 Standings.
        $response->assertSee('<dt class="podium-1 pos">1</dt>
                <dd class="podium-1 name"><span class="flag_right flag16_right_gb">'
            . '<a href="/f1/drivers/lewis-hamilton-1/2019"');
        $response->assertSee('<dt class="row_0 pos">4</dt>
                <dd class="row_0 name"><span class="flag_right flag16_right_mc">'
            . '<a href="/f1/drivers/charles-leclerc-73/2019"');
        $response->assertSee('<dt class="podium-1 pos">1</dt>
                <dd class="podium-1 name"><span class="flag_right flag16_right_de">'
            . '<a href="/f1/teams/mercedes/2019"');
        $response->assertSee('<a href="/f1/standings/2019">View Full Standings</a>');
    }

    /**
     * A test of the F1 news.
     * @return void
     */
    public function testNews(): void
    {
        $response = $this->get('/f1/news');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://www.motorsport.com/f1/news/alfa-romeo-mclaren-australian-gp-race-quarantine/4752661/?'
            . 'utm_source=RSS&utm_medium=referral&utm_campaign=RSS-F1&utm_term=News&utm_content=www" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">Alfa Romeo: It would&#39;ve been '
            . '&quot;unfair&quot; to race without McLaren</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://www.autosport.com/f1/news/148690/red-bull-would-have-protested-mercedes-das-system" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Red Bull would have protested '
            . 'Mercedes DAS system in Australia</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://www.motorsport.com/f1/news/australian-gp-no-danger-coronavirus-paddock/4755801/?'
            . 'utm_source=RSS&utm_medium=referral&utm_campaign=RSS-F1&utm_term=News&utm_content=www" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">&quot;No suggestion whatsoever&quot; '
            . 'Australian GP put F1 in danger</a></h4>');

        $response->assertSee('<next class="onclick_target prev-next mini"><i class="fas fa-chevron-circle-right"></i>'
            . '</next>
    <bullets>
                    <bullet class="onclick_target" data-ref="1">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>
                    <bullet class="onclick_target" data-ref="2">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>');

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.autosport.com/f1/news/148686/top-mclaren-staff-supporting-quarantine-colleagues" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Top McLaren staff stay in '
            . 'Australia with COVID-19 quarantine colleagues</a></h4>');
        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="http://www.bbc.co.uk/sport/formula1/51895560" rel="noopener noreferrer" target="_blank" title="'
            . 'Opens in new tab/window">Coronavirus: Ferrari shuts down production because of ongoing crisis</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.motorsport.com/f1/news/brawn-summer-break-coronavirus/4752437/?utm_source=RSS&'
            . 'utm_medium=referral&utm_campaign=RSS-F1&utm_term=News&utm_content=www" rel="noopener noreferrer" '
            . 'target="_blank" title="Opens in new tab/window">F1 can &quot;preserve&quot; calendar by scrapping '
            . 'summer break - Brawn</a></h4>');

        $response->assertSee('<li class="page_box unshaded_row ">
            <strong>1</strong>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="23"><em>23</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');
    }

    /**
     * A test of a specific F1 news page.
     * @return void
     */
    public function testNewsPage(): void
    {
        // Valid page.
        $response = $this->get('/f1/news?page=2');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.motorsport.com/f1/news/brawn-summer-break-coronavirus/4752437/?utm_source=RSS&'
            . 'utm_medium=referral&utm_campaign=RSS-F1&utm_term=News&utm_content=www" rel="noopener noreferrer" '
            . 'target="_blank" title="Opens in new tab/window">F1 can &quot;preserve&quot; calendar by scrapping '
            . 'summer break - Brawn</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.motorsport.com/f1/news/australian-gp-lockdown-coronavirus-horner/4749200/'
            . '?utm_source=RSS&utm_medium=referral&utm_campaign=RSS-F1&utm_term=News&utm_content=www" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">F1 considered paddock &#39;lockdown&#39; '
            . 'for Australian GP </a></h4>');

        $response->assertSee('<li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="1">1</a>
        </li>
            <li class="page_box unshaded_row ">
            <strong>2</strong>
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="3">3</a>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="23"><em>23</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="3">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');

        // Error.
        $response = $this->get('/f1/news?page=24');
        $response->assertStatus(404);
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/f1/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear Sports Formula One',
            'short_name' => 'DeBear Sports Formula One',
            'description' => 'All the latest results, stats and news from the 2020 Formula One season.',
            'start_url' => '/f1/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/sports/images/meta/f1.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
