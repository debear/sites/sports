<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class BRaceListTest extends MotorsportBaseCase
{
    /**
     * A test of the F1 race list for the current season.
     * @return void
     */
    public function testRaces(): void
    {
        $response = $this->get('/f1/races');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; Races</title>');

        // Prev/Next Races.
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    Previous Race
                                &ndash;
                <span class="flag_right flag16_right_cn">Chinese Grand Prix</span>
            </dt>

            ' . '
                            <dd class="cancelled full-width">The 2020 race was cancelled.</dd>');
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    Next Race
                                &ndash;
                <span class="flag_right flag16_right_nl">Dutch Grand Prix</span>
            </dt>

            ' . '
                            <dd class="row_0 full-width"><strong>Qualifying Starts:</strong>'
            . ' Saturday 2nd May, 15:00 (Local)</dd>
<dd class="row_1 full-width"><strong>Race Starts:</strong> Sunday 3rd May, 15:10 (Local)</dd>


    <dt class="full-width section row_head">2019 Podium</dt>
                    <dd class="not_held full-width">No Dutch Grand Prix was held in 2019.</dd>');

        // Upcoming.
        $response->assertSee('<h2 class="row_head">Upcoming Races</h2>
    <ul class="inline_list list upcoming grid">
        ' . '
                    <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">6</dt>
                <dt class="row_head head title-row">
                    <name>Spanish Grand Prix</name>
                    <info>Circuit de Catalunya, Barcelona</info>
                </dt>
                <dd class="flag flag48_es title-row"></dd>

                ' . '
                                    <dd class="full-width row_0"><strong>Qualifying Starts:</strong> Saturday 9th '
            . 'May, 15:00 (Local)</dd>

<dd class="full-width row_1"><strong>Race Starts:</strong> Sunday 10th May, 15:10 (Local)</dd>');
        $response->assertSee('<fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">14</dt>
                <dt class="row_head head title-row">
                    <name>Belgian Grand Prix</name>
                    <info>Circuit de Spa-Francorchamps, Spa</info>
                </dt>
                <dd class="flag flag48_be title-row"></dd>

                ' . '
                                    <dd class="full-width row_0"><strong>Qualifying Starts:</strong> Saturday 29th '
            . 'August, 15:00 (Local)</dd>

<dd class="full-width row_1"><strong>Race Starts:</strong> Sunday 30th August, 15:10 (Local)</dd>



            ' . '
        <dt class="full-width section row_head">2019 Podium</dt>
                            <dd class="cell podium-1 pos">
        1
    </dd>
<dd class="cell podium-1 name ">
    <span class="flag_right flag16_right_mc"><a href="/f1/drivers/charles-leclerc-73/2020"');

        // Completed.
        $response->assertSee('<h2 class="row_head">Completed Races</h2>
        <ul class="inline_list list completed grid">
                    <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">3</dt>
                <dt class="row_head head title-row">
                    <name>Vietnamese Grand Prix</name>
                    <info>Hanoi Street Circuit, Hanoi</info>
                </dt>
                <dd class="flag flag48_vn title-row"></dd>

                ' . '
                                    <dd class="cancelled full-width">2020 race cancelled.</dd>
                            </dl>
        </fieldset>
    </li>');
    }

    /**
     * A test of the F1 race list for a given season.
     * @return void
     */
    public function testRacesHistorical(): void
    {
        $response = $this->get('/f1/races/2019');
        $response->assertStatus(200);
        $response->assertSee('DeBear Sports &raquo; Formula One &raquo; 2019 Races');

        // Header shows 2019 races.
        $response->assertSee('<ul class="inline_list nav_calendar num_21">
                    ' . '
        <li>
    <a href="/f1/races/2019-australia" class="flag au "></a>
    <info><span class="flag_right flag16_right_fi">BOT</span></info>
</li>');

        // Prev / Last.
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    Previous Race
                                &ndash;
                <span class="flag_right flag16_right_br">Brazilian Grand Prix</span>
            </dt>');
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    Last Race
                                &ndash;
                <span class="flag_right flag16_right_ae">Abu Dhabi Grand Prix</span>
            </dt>');

        // First in list.
        $response->assertSee('<ul class="inline_list list completed grid">
                    <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">19</dt>
                <dt class="row_head head title-row">
                    <name>United States Grand Prix</name>
                    <info>Circuit of the Americas, Austin, Texas</info>
                </dt>
                <dd class="flag flag48_us title-row"></dd>

                ' . '
                                    <dt class="full-width section row_head"> Podium</dt>
            <dd class="cell podium-1 pos">
        1
    </dd>
<dd class="cell podium-1 name ">
    <span class="flag_right flag16_right_fi"><a href="/f1/drivers/valtteri-bottas-50/2019"');
        $response->assertSee('<a href="/f1/races/2019-united-states">&raquo; Full Race Coverage</a>');
    }

    /**
     * A test of the F1 race list for a given season without the main page chrome.
     * @return void
     */
    public function testRacesChromeless(): void
    {
        $response = $this->get('/f1/races/2020/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; Formula One &raquo; Races</title>');
        $response->assertSee('<h1>2020 Season Schedule</h1>');
        $response->assertSee('<fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">6</dt>
                <dt class="row_head head title-row">
                    <name>Spanish Grand Prix</name>
                    <info>Circuit de Catalunya, Barcelona</info>
                </dt>
                <dd class="flag flag48_es title-row"></dd>

                ' . '
                                    <dd class="full-width row_0"><strong>Qualifying Starts:</strong> '
            . 'Saturday 9th May, 15:00 (Local)</dd>

<dd class="full-width row_1"><strong>Race Starts:</strong> Sunday 10th May, 15:10 (Local)</dd>');
    }

    /**
     * A test of the F1 race list URL nomenclature
     * @return void
     */
    public function testRacesNotMeetings(): void
    {
        $response = $this->get('/f1/meetings');
        $response->assertStatus(404);
    }
}
