<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class FTeamsTest extends MotorsportBaseCase
{
    /**
     * A test of the current F1 team list
     * @return void
     */
    public function testTeamsList(): void
    {
        $response = $this->get('/f1/teams');
        $response->assertStatus(200);

        // Team details.
        $response->assertSee('<fieldset class="section profile">
        <div class="pos head row_head">6th</div>
        <h3 class="row_head"><span class="flag_right flag16_right_de">Mercedes</span></h3>
        <div class="pts head row_head">0pts</div>
                    <h4 class="champs podium-1">8x Constructors&#39; Champion (1971, 2009 &amp; 2014&ndash;2019)</h4>');
        $response->assertSee('<dl class="bio inline_list">
            <dt>Team Base:</dt>
                <dd><span class="flag_right flag16_right_gb">Brackley</span></dd>');
        $response->assertSee('<dl class="inline_list clearfix participants">
                                            <dt class="row_head num secondary">44</dt>
                <dd class="row_1 participant score secondary"><span class="flag_right flag16_right_gb">'
            . '<a href="/f1/drivers/lewis-hamilton-1/2020"');
        $response->assertSee('<dd class="row_1 pts secondary">0 pts</dd>
                                            <dt class="row_head num secondary">77</dt>
                <dd class="row_0 participant score secondary"><span class="flag_right flag16_right_fi">'
            . '<a href="/f1/drivers/valtteri-bottas-50/2020"');
        $response->assertSee('<dd class="row_0 pts secondary">0 pts</dd>
                    </dl>
        <a class="link" href="/f1/teams/mercedes/2020">Team Profile &raquo;</a>
    </fieldset>');

        // Name change.
        $response->assertSee('<h3 class="row_head"><span class="flag_right flag16_right_it">AlphaTauri</span></h3>');
        $response->assertDontSee('<h3 class="row_head"><span class="flag_right flag16_right_it">Toro Rosso</span>'
            . '</h3>');
    }

    /**
     * A test of a previous season's F1 team list
     * @return void
     */
    public function testTeamsListHistorical(): void
    {
        $response = $this->get('/f1/teams/2019');
        $response->assertStatus(200);

        // Team details.
        $response->assertSee('<fieldset class="section profile">
        <div class="pos head podium-1">1st</div>
        <h3 class="row_head"><span class="flag_right flag16_right_de">Mercedes</span></h3>
        <div class="pts head row_head">739pts</div>
                    <h4 class="champs podium-1">8x Constructors&#39; Champion (1971, 2009 &amp; 2014&ndash;2019)</h4>');
        $response->assertSee('<dl class="bio inline_list">
            <dt>Team Base:</dt>
                <dd><span class="flag_right flag16_right_gb">Brackley</span></dd>
            <dt>Victories:</dt>
                <dd>15</dd>
            <dt>Podiums:</dt>
                <dd>32</dd>
            <dt>Pole Positions:</dt>
                <dd>10</dd>
            <dt>Fastest Laps:</dt>
                <dd>9</dd>
        </dl>');
        $response->assertSee('<dl class="inline_list clearfix participants">
                                            <dt class="row_head num secondary">44</dt>
                <dd class="row_1 participant score secondary"><span class="flag_right flag16_right_gb">'
            . '<a href="/f1/drivers/lewis-hamilton-1/2019"');
        $response->assertSee('Lewis Hamilton</a></span></dd>
                <dd class="row_1 pts secondary">413 pts</dd>
                                            <dt class="row_head num secondary">77</dt>
                <dd class="row_0 participant score secondary"><span class="flag_right flag16_right_fi">'
            . '<a href="/f1/drivers/valtteri-bottas-50/2019"');
        $response->assertSee('Valtteri Bottas</a></span></dd>
                <dd class="row_0 pts secondary">326 pts</dd>
                    </dl>
        <a class="link" href="/f1/teams/mercedes/2019">Team Profile &raquo;</a>
    </fieldset>');

        // Name change.
        $response->assertSee('<h3 class="row_head"><span class="flag_right flag16_right_it">Toro Rosso</span></h3>');
        $response->assertDontSee('<h3 class="row_head"><span class="flag_right flag16_right_it">AlphaTauri</span>'
            . '</h3>');
    }

    /**
     * A test of miscellaneous F1 team list actions
     * @return void
     */
    public function testTeamsListMisc(): void
    {
        // Sequential Car Number sorting.
        $response = $this->get('/f1/teams/2013');
        $response->assertStatus(200);
        $response->assertSeeInOrder([
            '/f1/drivers/kimi-raikkonen-4/2013',
            '/f1/drivers/heikki-kovalainen-2/2013',
            '/f1/drivers/romain-grosjean-26/2013',
        ]);

        // Mid-season driver replacements.
        $response = $this->get('/f1/teams/lotus/2013');
        $response->assertStatus(200);
        $response->assertSeeInOrder([
            '"series":[{"name":"Championship Position",',
            '<span class=\"flag_right flag16_right_fi\">Kimi R\u00e4ikk\u00f6nen<\/span>',
            '<span class=\"flag_right flag16_right_fr\">Romain Grosjean<\/span>',
            '<span class=\"flag_right flag16_right_fi\">Heikki Kovalainen<\/span>',
            '"tooltip":{"shared":true,',
        ]);
    }

    /**
     * A test of a current F1 team
     * @return void
     */
    public function testTeamsIndividual(): void
    {
        $response = $this->get('/f1/teams/renault');
        $response->assertStatus(200);

        // Name and summary info.
        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; Teams and Drivers &raquo; '
            . 'Renault</title>');
        $response->assertSee('<h1><span class="flag_right flag16_right_fr">Renault</span></h1>');
        $response->assertSee('<ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Team Base:</strong>
                <span class="flag_right flag16_right_gb">Enstone</span>
            </span>

            <span>
                <strong>Constructors&#39; Titles:</strong>
                                    <abbrev title="2005 &amp; 2006">
                                2
                                    </abbrev>
                            </span>
        </li>

        <li class="standings">
            <span>
                <strong>2020 Position:</strong>
                9th
            </span>

            <span>
                <strong>2020 Points:</strong>
                0pts
            </span>
        </li>

        <li class="stats">
            <span>
                <strong>2020 Wins:</strong>
                0
            </span>

            <span>
                <strong>2020 Podiums:</strong>
                0
            </span>

            <span>
                <strong>2020 Pole Positions:</strong>
                0
            </span>

            <span>
                <strong>2020 Fastest Laps:</strong>
                0
            </span>
        </li>
    </ul>');

        // Chart.
        $response->assertSee('[{"name":"Championship Position","yAxis":0,"type":"spline","data":'
            . '[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'
            . 'null,null],"zIndex":2}');

        // Stats.
        $response->assertSee('<fieldset class="subnav-stats no-stats info icon icon_info hidden">The 2020 season has '
            . 'not yet started.</fieldset>');

        // History.
        $response->assertSee('<dd class="row_0 season">
            ' . "\n" . '            <a href="/f1/standings/2019">2019</a>
            ' . "\n" . '                    </dd>
        <dd class="row_0 name">
            ' . "\n" . '                            ' . "\n" . '                Renault Sport F1 Team
            ' . "\n" . '                    </dd>
        <dd class="row_0 pts">
            91pts
        </dd>
        <dd class="row_0 pos">
            5th
        </dd>');
        $response->assertSee('<dt class="row_head full">Team transferred to Renault from Lotus</dd>        '
            . '<dd class="row_1 season">' . "\n" . '            ' . '
            <a href="/f1/standings/2015">2015</a>' . "\n" . '            ' . "\n" . '                    </dd>
        <dd class="row_1 name">
            ' . "\n" . '                            <span class="flag_right flag16_right_gb">
                            ' . "\n" . '                Lotus F1 Team' . "\n" . '            ' . '
                            </span>
                    </dd>
        <dd class="row_1 pts">
            78pts
        </dd>
        <dd class="row_1 pos">
            6th
        </dd>');
        $response->assertSee('<dd class="row_0 full">Did not compete between 1986 and 2001</dd>');
    }

    /**
     * A test of a previous season's F1 team
     * @return void
     */
    public function testTeamsIndividualHistorical(): void
    {
        $response = $this->get('/f1/teams/mclaren/2019');
        $response->assertStatus(200);

        // Name and summary info.
        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; 2019 Teams and Drivers &raquo; '
            . 'McLaren</title>');
        $response->assertSee('<h1><span class="flag_right flag16_right_gb">McLaren</span></h1>');
        $response->assertSee('<ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Team Base:</strong>
                Woking
            </span>

            <span>
                <strong>Constructors&#39; Titles:</strong>
                                    <abbrev title="1974, 1984, 1985, 1988&ndash;1991 &amp; 1998">
                                8
                                    </abbrev>
                            </span>
        </li>

        <li class="standings">
            <span>
                <strong>2019 Position:</strong>
                4th
            </span>

            <span>
                <strong>2019 Points:</strong>
                145pts
            </span>
        </li>

        <li class="stats">
            <span>
                <strong>2019 Wins:</strong>
                0
            </span>

            <span>
                <strong>2019 Podiums:</strong>
                1
            </span>

            <span>
                <strong>2019 Pole Positions:</strong>
                0
            </span>

            <span>
                <strong>2019 Fastest Laps:</strong>
                0
            </span>
        </li>
    </ul>');

        // Chart.
        $response->assertSee('[{"name":"Championship Position","yAxis":0,"type":"spline","data":'
            . '[9,5,6,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],"zIndex":2},{"name":'
            . '"<span class=\"flag_right flag16_right_es\">Carlos Sainz Jr<\/span>","yAxis":1,"type":"column","data":'
            . '[0,0,0,6,10,18,18,26,30,38,48,58,58,58,58,66,76,76,80,95,96],"zIndex":1,"stack":0},{"name":'
            . '"<span class=\"flag_right flag16_right_gb\">Lando Norris<\/span>","yAxis":1,"type":"column","data":'
            . '[0,8,8,12,12,12,12,14,22,22,22,24,24,25,31,35,35,35,41,45,49],"zIndex":1,"stack":0}]');

        // Stats.
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <dl class="stat">
                    <dt class="row_0">Average Qualifying Position:</dt>
                        <dd class="value row_0 ">9.90</dd>
                        <dd class="rank row_0">4th</dd>
                </dl>
            </li>');
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <dl class="stat">
                    <dt class="row_1">Number of Points Finishes:</dt>
                        <dd class="value row_1 ">57.14%<br/>(24 / 42)</dd>
                        <dd class="rank row_1">4th</dd>
                </dl>
            </li>');

        // History.
        $response->assertSee('<dd class="row_0 season">
            ' . "\n" . '            2007' . "\n" . '            ' . "\n" . '                    </dd>
        <dd class="row_0 name">
            ' . "\n" . '                            ' . "\n" . '                Vodafone McLaren Mercedes
            ' . "\n" . '                    </dd>
        <dd class="row_0 pts">
            0pts
        </dd>
        <dd class="row_0 pos">
            11th
        </dd>
                    <dd class="row_0 notes">
                Team Disqualified after conclusion of the season due the espionage scandal. 203pts had been scored.
            </dd>');
    }

    /**
     * A test of miscellaneous F1 team controller actions
     * @return void
     */
    public function testTeamsIndividualMisc(): void
    {
        // Team not found.
        $response = $this->get('/f1/teams/no-matches');
        $response->assertStatus(404);

        // Team no longer active.
        $response = $this->get('/f1/teams/force-india');
        $response->assertStatus(200);
        $response->assertSee('<span>
                <strong>2018 Position:</strong>
                11th
            </span>');

        // Legacy routes - current season.
        $response = $this->get('/f1/team-8');
        $response->assertStatus(308);
        $response->assertRedirect('/f1/teams/williams/2020');

        // Legacy routes - stating season.
        $response = $this->get('/f1/2020/team-6');
        $response->assertStatus(308);
        $response->assertRedirect('/f1/teams/alphatauri/2020');

        $response = $this->get('/f1/2020/team-9');
        $response->assertStatus(404);
    }
}
