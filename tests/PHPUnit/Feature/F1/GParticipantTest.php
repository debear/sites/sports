<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class GParticipantTest extends MotorsportBaseCase
{
    /**
     * A test of a current F1 participant
     * @return void
     */
    public function testParticipant(): void
    {
        $response = $this->get('/f1/drivers/esteban-ocon-68');
        $response->assertStatus(200);

        // Header and Summary.
        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; Teams and Drivers &raquo; '
            . 'Esteban Ocon</title>');
        $response->assertSee('<li>
    <flag  class="flag au dim"></flag>
    <info>Cnc</info>
</li>');
        $response->assertSee('<ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Born:</strong>
                17th September 1996, &Eacute;vreux
            </span>

            <span>
                <strong>Championships:</strong>
                                0
                            </span>

            <span>
                <strong>Experience:</strong>
                4th Season
            </span>
        </li>');

        // Chart.
        $response->assertSee('var chartHistory = {
                    2020: {"chart":');
        $response->assertSee('"series":[{"name":"Championship Position","yAxis":0,"type":"spline","data":[null,null,'
            . 'null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],'
            . '"zIndex":3},{"name":"Championship Points","yAxis":1,"type":"column","data":[null,null,null,null,null,'
            . 'null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],"zIndex":0},'
            . '{"name":"Race Result","yAxis":0,"type":"spline","data":[null,null,null,null,null,null,null,null,null,'
            . 'null,null,null,null,null,null,null,null,null,null,null,null,null],"zIndex":2},{"name":"Grid Position",'
            . '"yAxis":0,"type":"spline","data":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,'
            . 'null,null,null,null,null,null,null,null],"zIndex":1}]');
        $response->assertSee('"series":[{"name":"Championship Position","yAxis":0,"type":"spline","data":'
            . '[12,14,14,17,17,13,13,13,12,11,11,12,11,10,11,11,11,11,11,11,12],"zIndex":3},{"name":"Championship '
            . 'Points","yAxis":1,"type":"column","data":[0,1,1,1,1,9,11,11,19,25,29,29,37,45,45,47,49,49,49,49,49],'
            . '"zIndex":0},{"name":"Race Result","yAxis":0,"type":"spline","data":'
            . '[12,10,11,19,16,6,9,19,6,7,8,13,6,6,20,9,9,19,11,14,17],"zIndex":2},{"name":"Grid Position","yAxis":0,'
            . '"type":"spline","data":[14,8,12,7,13,6,8,11,11,10,15,17,3,8,9,6,11,6,11,18,9],"zIndex":1}]');
        $response->assertSee('var chart = new Highcharts.Chart(chartHistory[2020]);');

        // Stats.
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 hidden">
                <dl data-id="6" class="stat">
                    <dt class="cell row_1" data-css="row_1">Number of Laps Raced:</dt>
                        <dd class="cell value row_1 " data-css="row_1 ">1052</dd>
                        <dd class="cell rank row_1" data-css="row_1">15th</dd>
                </dl>
            </li>');
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 hidden">
                <dl data-id="8" class="stat">
                    <dt class="cell row_1" data-css="row_1">Number of Classified Finishes:</dt>
                        <dd class="cell value row_1 " data-css="row_1 ">71.43%<br/>(15 / 21)</dd>
                        <dd class="cell rank row_1" data-css="row_1">16th</dd>
                </dl>
            </li>');
        $response->assertSee('<fieldset class="no-stats info icon icon_info">The 2020 season has not yet '
            . 'started.</fieldset>');

        // History.
        $response->assertSee('<a href="/f1/standings">2020</a>
                ' . '
                                    <span class="in-progress"></span>
                            </div>
                            <div class="row_1 first-row current display num-1 pts">
                    0pts
                </div>
                <div class="row_1 first-row current display num-1 pos">
                    12th
                </div>');
        $response->assertSee('Renault</a></span>
                    </div>
                                                <div class="row_0 full">Did not compete in 2019</div>            ' . '
            <div class="row_head   num-2 season">');
        $response->assertSee('<span class="flag_right flag16_right_in"><a href="/f1/teams/force-india/2018"');
        $response->assertSee('<span class="flag_right flag16_right_gb"><a href="/f1/teams/racing-point/2018"');
        $response->assertSee('<div class="race-cell  res-dsq " data-pos="19" data-pts="" data-res="DSQ" '
            . 'data-grid="6"></div>');
    }

    /**
     * A test of a previous season's F1 participant
     * @return void
     */
    public function testParticipantHistorical(): void
    {
        $response = $this->get('/f1/drivers/pierre-gasly-71/2019');
        $response->assertStatus(200);

        // Header and Summary.
        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; Teams and Drivers &raquo; '
            . 'Pierre Gasly</title>');
        $response->assertSee('<li>
    <a href="/f1/races/2019-australia" class="flag au "></a>
    <info><span class="flag_right flag16_right_fi">BOT</span></info>
</li>');
        $response->assertSee('<ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Born:</strong>
                7th February 1996, Rouen
            </span>

            <span>
                <strong>Championships:</strong>
                                0
                            </span>

            <span>
                <strong>Experience:</strong>
                4th Season
            </span>
        </li>

        <li class="standings">
            <span>
                <strong>2019 Position:</strong>
                7th
            </span>

            <span>
                <strong>2019 Points:</strong>
                95pts
            </span>
        </li>');

        // Chart.
        $response->assertSee('var chartHistory = {
                    2020: {"chart":');
        $response->assertSee('"series":[{"name":"Championship Position","yAxis":0,"type":"spline","data":[null,null,'
            . 'null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],'
            . '"zIndex":3},{"name":"Championship Points","yAxis":1,"type":"column","data":[null,null,null,null,null,'
            . 'null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],"zIndex":0},'
            . '{"name":"Race Result","yAxis":0,"type":"spline","data":[null,null,null,null,null,null,null,null,null,'
            . 'null,null,null,null,null,null,null,null,null,null,null,null,null],"zIndex":2},{"name":"Grid Position",'
            . '"yAxis":0,"type":"spline","data":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,'
            . 'null,null,null,null,null,null,null,null],"zIndex":1}]');
        $response->assertSee('"series":[{"name":"Championship Position","yAxis":0,"type":"spline","data":'
            . '[11,10,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,6,8,6,7],"zIndex":3},{"name":"Championship Points","yAxis":1,'
            . '"type":"column","data":[0,4,13,13,21,32,36,37,43,55,55,63,65,65,69,69,75,77,77,95,95],"zIndex":0},'
            . '{"name":"Race Result","yAxis":0,"type":"spline","data":'
            . '[11,8,6,17,6,5,8,10,7,4,14,6,9,11,8,14,7,9,16,2,18],"zIndex":2},{"name":"Grid Position","yAxis":0,'
            . '"type":"spline","data":[17,13,6,20,6,8,5,9,8,5,4,6,13,17,11,16,9,10,10,6,11],"zIndex":1}]');
        $response->assertSee('var chart = new Highcharts.Chart(chartHistory[2019]);');

        // Stats.
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 ">
                <dl data-id="6" class="stat">
                    <dt class="cell podium-3" data-css="podium-3">Number of Laps Raced:</dt>
                        <dd class="cell value podium-3 " data-css="podium-3 ">1234</dd>
                        <dd class="cell rank podium-3" data-css="podium-3">3rd</dd>
                </dl>
            </li>');
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 ">
                <dl data-id="8" class="stat">
                    <dt class="cell podium-2" data-css="podium-2">Number of Classified Finishes:</dt>
                        <dd class="cell value podium-2 " data-css="podium-2 ">95.24%<br/>(20 / 21)</dd>
                        <dd class="cell rank podium-2" data-css="podium-2">T-2nd</dd>
                </dl>
            </li>');
        $response->assertSee('<fieldset class="no-stats info icon icon_info">The 2020 season has not yet '
            . 'started.</fieldset>');

        // History.
        $response->assertSee('<a href="/f1/standings/2019">2019</a>
                ' . '
                            </div>
                            <div class="row_0  display num-2 pts">
                    95pts
                </div>
                <div class="row_0  display num-2 pos">
                    7th
                </div>');
        $response->assertSee('<span class="flag_right flag16_right_at"><a href="/f1/teams/red-bull/2019"');
        $response->assertSee('<span class="flag_right flag16_right_it"><a href="/f1/teams/toro-rosso/2019"');
        $response->assertSee('<div class="race-cell  res-pts_finish res-fast_lap" data-pos="5" data-pts="11" '
            . 'data-res="5" data-grid="8">11</div>');
    }

    /**
     * A test of a previous season's F1 participant
     * @return void
     */
    public function testParticipantWithPreDetail(): void
    {
        $response = $this->get('/f1/drivers/kimi-raikkonen-4/2020');
        $response->assertStatus(200);

        // A drive from 2001.
        $response->assertSee('<div class="row_head   num-1 season">
                ' . '
                2001
                ' . '
                            </div>
                            <div class="row_1   num-1 pts">
                    9pts
                </div>
                <div class="row_1   num-1 pos">
                    10th
                </div>');
        $response->assertSee('<div class="race-cell first-race res-pts_finish " data-pos="6" data-pts="1" data-res="6"'
            . ' data-grid="13">1</div>');
    }

    /**
     * A test of a retired F1 participant
     * @return void
     */
    public function testParticipantRetired(): void
    {
        $response = $this->get('/f1/drivers/david-coulthard-13');
        $response->assertStatus(200);
        $response->assertSee('var chart = new Highcharts.Chart(chartHistory[2008]);');
        $response->assertSee('<tables class="subnav-history num-19 ');
        $response->assertSee('<div class="race-cell  res-ret res-fast_lap" data-pos="13" data-pts="FL" data-res="Ret" '
            . 'data-grid="6">FL</div>');
    }

    /**
     * A test of miscellaneous F1 participant controller actions
     * @return void
     */
    public function testParticipantMisc(): void
    {
        // Driver not found / mis-match between name and ID.
        $response = $this->get('/f1/drivers/no-matches-0');
        $response->assertStatus(404);
        $response = $this->get('/f1/drivers/name-mismatch-1');
        $response->assertStatus(404);

        // Driver not active in quoted season.
        $response = $this->get('/f1/drivers/esteban-ocon-68/2019');
        $response->assertStatus(308);
        $response->assertRedirect('/f1/drivers/esteban-ocon-68');

        // Legacy routes - current season.
        $response = $this->get('/f1/driver-68');
        $response->assertStatus(308);
        $response->assertRedirect('/f1/drivers/esteban-ocon-68/2020');

        // Legacy routes - stating season.
        $response = $this->get('/f1/2020/driver-68');
        $response->assertStatus(308);
        $response->assertRedirect('/f1/drivers/esteban-ocon-68/2020');

        $response = $this->get('/f1/2020/driver-0');
        $response->assertStatus(404);
    }
}
