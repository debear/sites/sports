<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class DStandingsTest extends MotorsportBaseCase
{
    /**
     * A test of the current F1 standings
     * @return void
     */
    public function testStandings(): void
    {
        $response = $this->get('/f1/standings');
        $response->assertStatus(200);

        $response->assertSee('<h1><span class="hidden-tp hidden-m">2020</span> '
            . '<span class="hidden-m">Formula One</span> Championship Standings</h1>');

        // Drivers.
        $response->assertSee('<tables class="key-handler races-22">
    ' . '
            <h2>Drivers&#39; Standings</h2>
        <standings class="clearfix participants">');
        $response->assertSee('<main>
        <flags>
            <type class="row_head">Driver</type>
            <total class="row_head sel sortable" data-round="0">Total</total>
        </flags>
                                <row class="rows-1 " data-id="76" data-pos="1">
                <pos class="full podium-1">1</pos>
                <name class="full podium-1">
                    <span class="flag_right flag16_right_th"><a href="/f1/drivers/alexander-albon-76/2020"');
        $response->assertSee('Alexander Albon</a></span>
                    ' . '
                                    </name>
                <total class="full podium-1">0pts</total>
                                                                </row>');
        $response->assertSeeInOrder([
            '<row class="rows-1 outer" data-id="76" data-pos="1">',
            '<race class="race-cell res-no_race " data-round="" data-pos="" data-pts="" data-res="" data-grid="">'
            . '</race>',
            '<race class="race-cell res-no_race " data-round="" data-pos="" data-pts="" data-res="" data-grid="">'
            . '</race>',
            '<race class="race-cell res-no_race " data-round="" data-pos="" data-pts="" data-res="" data-grid="">'
            . '</race>',
            '<race class="race-cell res-no_race " data-round="" data-pos="" data-pts="" data-res="" data-grid="">'
            . '</race>',
            '<race class="race-cell res-not_yet_raced " data-round="" data-pos="" data-pts="" data-res="" data-grid="">'
            . '</race>',
        ]);

        // Key.
        $response->assertSee('<dl class="toggles display_key">
    <dt>Key:</dt>
        <dd class="show unsel" data-show="1">Show</dd>
        <dd class="hide sel" data-show="0">Hide</dd>
        <dd class="list hidden">
            <ul class="results_key inline_list">
                <li class="res-pos_1">Winner</li>
                <li class="res-pos_2">Runner-Up</li>
                <li class="res-pos_3">Third</li>
                                    <li class="res-pts_finish">Points Finish</li>
                    <li class="res-non_pts_finish">Non-Points Finish</li>
                    <li class="res-not_clsfd">Not Classified</li>
                                <li class="res-dsq">Disqualified</li>
                                    <li class="res-dns">Did Not Start</li>
                                <li class="res-not_yet_raced">Race To Be Held</li>
                <li class="res-no_race">Did Not Race</li>
                                    <li class="res-fast_lap">Fastest Lap (FL)</li>
                            </ul>
        </dd>
</dl>');

        // Teams.
        $response->assertSee('<h2>Constructors&#39; Standings</h2>
        <standings class="clearfix constructors">');
        $response->assertSee('<main>
        <flags>
            <type class="row_head">Constructor</type>
            <total class="row_head " data-round="0">Total</total>
        </flags>
                                <row class="rows-2 split" data-id="3" data-pos="1">
                <pos class="full podium-1">1</pos>
                <name class="full podium-1">
                    <span class="flag_right flag16_right_it"><a href="/f1/teams/alfa-romeo/2020"');
        $response->assertSee('Alfa Romeo</a></span>
                    ' . '
                                    </name>
                <total class="full podium-1">0pts</total>
                                                            <row>
                            <participant class="podium-1">GIO</participant>
                        </row>
                                                                                <row>
                            <participant class="podium-1">RAI</participant>
                        </row>
                                                </row>');
        $empty_attrib = 'data-round="" data-pos="" data-pts="" data-res="" data-grid=""';
        $response->assertSeeInOrder([
            '<row class="rows-2 outer" data-id="3" data-pos="1">',
            '<row>',
            '<race class="race-cell res-no_race " ' . $empty_attrib . '></race>',
            '<race class="race-cell res-no_race " ' . $empty_attrib . '></race>',
            '<race class="race-cell res-no_race " ' . $empty_attrib . '></race>',
            '<race class="race-cell res-no_race " ' . $empty_attrib . '></race>',
            '<race class="race-cell res-not_yet_raced " ' . $empty_attrib . '></race>',
        ]);
    }

    /**
     * A test of a previous season's F1 standings
     * @return void
     */
    public function testStandingsHistorical(): void
    {
        $response = $this->get('/f1/standings/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; 2019 Standings</title>');

        $response->assertSee('<h1><span class="hidden-tp hidden-m">2019</span> '
            . '<span class="hidden-m">Formula One</span> Championship Standings</h1>');

        // Drivers.
        $response->assertSee('<tables class="key-handler races-21">
    ' . '
            <h2>Drivers&#39; Standings</h2>
        <standings class="clearfix participants">');
        $response->assertSee('<main>
        <flags>
            <type class="row_head">Driver</type>
            <total class="row_head sel sortable" data-round="0">Total</total>
        </flags>
                                <row class="rows-1 " data-id="1" data-pos="1">
                <pos class="full podium-1">1</pos>
                <name class="full podium-1">
                    <span class="flag_right flag16_right_gb"><a href="/f1/drivers/lewis-hamilton-1/2019"');
        $response->assertSee('Lewis Hamilton</a></span>
                    ' . '
                                            <trophies>
                                                            <span class="icon_right_standings_trophy1">11x</span>
                                                            <span class="icon_right_standings_trophy2">4x</span>
                                                            <span class="icon_right_standings_trophy3">2x</span>
                                                            <span class="icon_right_standings_trophyfl">6x</span>
                                                    </trophies>
                                    </name>
                <total class="full podium-1">413pts</total>
                                                                </row>');
        $response->assertSee('<row class="rows-1 " data-id="62" data-pos="6">
                <pos class="full row_0">6</pos>
                <name class="full row_0">
                    <span class="flag_right flag16_right_es"><a href="/f1/drivers/carlos-sainz-jr-62/2019"');
        $response->assertSee('Carlos Sainz Jr</a></span>
                    ' . '
                                            <trophies>
                                                            <span class="icon_right_standings_trophy3"></span>
                                                    </trophies>
                                    </name>
                <total class="full row_0">96pts</total>
                                                                </row>');

        // Teams.
        $response->assertSee('<row class="rows-3 split" data-id="7" data-pos="3">
                <pos class="full podium-3">3</pos>
                <name class="full podium-3">
                    <span class="flag_right flag16_right_at"><a href="/f1/teams/red-bull/2019"');
        $response->assertSee('data-hover-champ-pts="417pts" data-hover-champ-pos="3rd" data-hover-wins="3" '
            . 'data-hover-podiums="9">Red Bull</a></span>
                    ' . '
                                            <trophies>
                                                            <span class="icon_right_standings_trophy1">3x</span>
                                                            <span class="icon_right_standings_trophy2">2x</span>
                                                            <span class="icon_right_standings_trophy3">4x</span>
                                                            <span class="icon_right_standings_trophyfl">5x</span>
                                                    </trophies>
                                    </name>
                <total class="full podium-3">417pts</total>
                                                            <row>
                            <participant class="podium-3">GAS</participant>
                        </row>
                                                                                <row>
                            <participant class="podium-3">VES</participant>
                        </row>
                                                                                <row>
                            <participant class="podium-3">ALB</participant>
                        </row>
                                                </row>');
    }
}
