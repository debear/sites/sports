<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class EStatsTest extends MotorsportBaseCase
{
    /**
     * A test of the current F1 stat leaders
     * @return void
     */
    public function testStats(): void
    {
        $response = $this->get('/f1/stats');
        $response->assertStatus(200);

        $response->assertSee('<h1><span class="hidden-m">2020</span> Stat Leaders</h1>');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="participants">Drivers</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="teams">Constructors</item>
                <sep class="right"></sep>
    </desk>');

        $response->assertSee('<div class="stats">
            <div class="subnav-participants stats-table  clearfix">
                        <fieldset class="no-stats info icon icon_info">The 2020 season has not yet started.</fieldset>
        </div>
            <div class="subnav-teams stats-table hidden clearfix">
                        <fieldset class="no-stats info icon icon_info">The 2020 season has not yet started.</fieldset>
        </div>
    </div>

</div>');
        $response->assertDontSee('<div class="stats">
            <div class="subnav-participants stats-table  clearfix">
                        <fieldset class="no-stats info icon icon_info">The 2020 season has not yet started.</fieldset>
        </div>
    </div>

</div>');
    }

    /**
     * A test of a previous season's F1 stat leaders
     * @return void
     */
    public function testStatsHistorical(): void
    {
        $response = $this->get('/f1/stats/2019');
        $response->assertStatus(200);

        $response->assertSee('<h1><span class="hidden-m">2019</span> Stat Leaders</h1>');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="participants">Drivers</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="teams">Constructors</item>
                <sep class="right"></sep>
    </desk>');

        $response->assertSee('<select>
            <optgroup label="Qualifying">'
            . '<option value="1">Number of Front Row Starts</option>'
            . '<option value="2">Number of Pole Positions</option>'
            . '<option value="3">Average Qualifying Position</option>'
            . '<option value="4">Average Grid Position</option>'
            . '<option value="5">Number of times Out-Qualified Teammate</option></optgroup>'
            . '<optgroup label="Race"><option value="6">Number of Laps Raced</option>'
            . '<option value="7">Number of Race Finishes</option>'
            . '<option value="8">Number of Classified Finishes</option>'
            . '<option value="9">Number of Points Finishes</option>'
            . '<option value="10">Number of Podiums</option>'
            . '<option value="11">Number of Race Wins</option>'
            . '<option value="12">Number of Laps Lead</option>'
            . '<option value="13">Number of Fastest Laps</option>'
            . '<option value="14" selected="selected">Average Finishing Position</option>'
            . '<option value="15">Number of times Beat Teammate</option>'
            . '<option value="16">Average Places Gained (or Lost) from Grid Position</option>'
            . '</optgroup>        </select>');

        // Driver.
        $response->assertSee('<top>
            <cell class="row_head group">Drivers</cell>
        </top>
        ' . '
                                        <row data-sort-14="1:1" data-sort-1="1:1" data-sort-2="2:1" data-sort-3="1:1" '
            . 'data-sort-4="1:1" data-sort-5="4:1" data-sort-6="1:1" data-sort-7="1:1" data-sort-8="1:1" '
            . 'data-sort-9="1:1" data-sort-10="1:1" data-sort-11="1:1" data-sort-12="1:1" data-sort-13="1:1" '
            . 'data-sort-15="5:1" data-sort-16="10:1">
                                <cell class="row_head pos sort_order">1</cell>');
        $response->assertSee('<row data-sort-14="1:1" data-sort-1="1:1" data-sort-2="2:1" data-sort-3="1:1" '
            . 'data-sort-4="1:1" data-sort-5="4:1" data-sort-6="1:1" data-sort-7="1:1" data-sort-8="1:1" '
            . 'data-sort-9="1:1" data-sort-10="1:1" data-sort-11="1:1" data-sort-12="1:1" data-sort-13="1:1" '
            . 'data-sort-15="5:1" data-sort-16="10:1">
                                            <cell class="row_1  hidden-m" data-field="1">13</cell>
                                            <cell class="row_1  hidden-m" data-field="2">5</cell>
                                            <cell class="row_1  hidden-m" data-field="3">2.29</cell>
                                            <cell class="row_1  hidden-m" data-field="4">2.33</cell>
                                            <cell class="row_1  hidden-m" data-field="5">14</cell>
                                            <cell class="row_1  hidden-m" data-field="6">1262</cell>
                                            <cell class="row_1  hidden-m" data-field="7">21</cell>
                                            <cell class="row_1  hidden-m" data-field="8">100.00%</cell>
                                            <cell class="row_1  hidden-m" data-field="9">100.00%</cell>
                                            <cell class="row_1  hidden-m" data-field="10">17</cell>
                                            <cell class="row_1  hidden-m" data-field="11">11</cell>
                                            <cell class="row_1  hidden-m" data-field="12">510</cell>
                                            <cell class="row_1  hidden-m" data-field="13">6</cell>
                                            <cell class="row_1  " data-field="14">2.38</cell>
                                            <cell class="row_1  hidden-m" data-field="15">15</cell>
                                            <cell class="row_1 stats-delta-neg hidden-m" data-field="16">-0.05</cell>
                                    </row>');

        // Team.
        $response->assertSee('<top>
            <cell class="row_head group">Constructors</cell>
        </top>
        ' . '
                                        <row data-sort-14="1:1" data-sort-1="1:1" data-sort-2="1:1" data-sort-3="1:1" '
            . 'data-sort-4="1:1" data-sort-6="1:1" data-sort-7="1:1" data-sort-8="1:1" data-sort-9="1:1" '
            . 'data-sort-10="1:1" data-sort-11="1:1" data-sort-12="1:1" data-sort-13="1:1" data-sort-16="6:1">');
        $response->assertSee('<row data-sort-14="1:1" data-sort-1="1:1" data-sort-2="1:1" data-sort-3="1:1" '
            . 'data-sort-4="1:1" data-sort-6="1:1" data-sort-7="1:1" data-sort-8="1:1" data-sort-9="1:1" '
            . 'data-sort-10="1:1" data-sort-11="1:1" data-sort-12="1:1" data-sort-13="1:1" data-sort-16="6:1">
                                            <cell class="row_1  hidden-m" data-field="1">22</cell>
                                            <cell class="row_1  hidden-m" data-field="2">10</cell>
                                            <cell class="row_1  hidden-m" data-field="3">2.64</cell>
                                            <cell class="row_1  hidden-m" data-field="4">3.02</cell>
                                            <cell class="row_1  hidden-m" data-field="6">2495</cell>
                                            <cell class="row_1  hidden-m" data-field="7">40</cell>
                                            <cell class="row_1  hidden-m" data-field="8">95.24%</cell>
                                            <cell class="row_1  hidden-m" data-field="9">95.24%</cell>
                                            <cell class="row_1  hidden-m" data-field="10">32</cell>
                                            <cell class="row_1  hidden-m" data-field="11">15</cell>
                                            <cell class="row_1  hidden-m" data-field="12">695</cell>
                                            <cell class="row_1  hidden-m" data-field="13">9</cell>
                                            <cell class="row_1  " data-field="14">3.24</cell>
                                            <cell class="row_1 stats-delta-neg hidden-m" data-field="16">-0.21</cell>
                                    </row>');
    }
}
