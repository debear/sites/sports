<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class HSitemapTest extends MotorsportBaseCase
{
    /**
     * A test of the F1 sitemap for the current season.
     * @return void
     */
    public function testSitemap(): void
    {
        $response = $this->get('/f1/sitemap');
        $response->assertStatus(200);

        $response->assertSee('<a href="/f1/races">
        <strong>Races</strong>
            </a>
    ' . '
            <em class="descrip">The 2020 Formula One race calendar.</em>
    ' . '
            <ul class="inline_list children open">
            <li class="item">
    <div class="toggle "></div>
        <strong><span class="flag_right flag16_right_au">Australian Grand Prix</span></strong>');

        $response->assertSee('<a href="/f1/standings/2008">
        <strong>2008 Standings</strong>
            </a>');
        $response->assertDontSee('<a href="/f1/standings/2007">
        <strong>2007 Standings</strong>
            </a>');

        $response->assertSee('<a href="/f1/teams">
        <strong>Teams and Drivers</strong>
            </a>
    ' . '
            <em class="descrip">All the teams and drivers taking part in the 2020 Formula One season.</em>');

        $response->assertSee('<a href="/f1/teams/alphatauri/2020">
        <strong><span class="flag_right flag16_right_it">AlphaTauri</span></strong>
            </a>');
        $response->assertDontSee('<a href="/f1/teams/toro-rosso/2020">
        <strong><span class="flag_right flag16_right_it">Toro Rosso</span></strong>
            </a>');

        $response->assertSee('<a href="/f1/drivers/esteban-ocon-68/2020">
        <strong><span class="flag_right flag16_right_fr">Esteban Ocon</span></strong>
            </a>');
        $response->assertDontSee('<a href="/f1/drivers/nico-hulkenberg-31/2020">
        <strong><span class="flag_right flag16_right_de">Nico H&uuml;lkenberg</span></strong>
            </a>');
    }

    /**
     * A test of the F1 sitemap for a given season
     * @return void
     */
    public function testSitemapHistorical(): void
    {
        $response = $this->get('/f1/sitemap/2019');
        $response->assertStatus(200);

        $response->assertSee('<a href="/f1/races/2019">
        <strong>Races</strong>
            </a>
    ' . '
            <em class="descrip">The 2019 Formula One race calendar.</em>
    ' . '
            <ul class="inline_list children open">
            <li class="item">
    <div class="toggle "></div>
            <a href="/f1/races/2019-australia">
        <strong><span class="flag_right flag16_right_au">Australian Grand Prix</span></strong>
            </a>');

        $response->assertSee('<a href="/f1/teams/2019">
        <strong>Teams and Drivers</strong>
            </a>
    ' . '
            <em class="descrip">All the teams and drivers taking part in the 2019 Formula One season.</em>');

        $response->assertSee('<a href="/f1/teams/toro-rosso/2019">
        <strong><span class="flag_right flag16_right_it">Toro Rosso</span></strong>
            </a>');
        $response->assertDontSee('<a href="/f1/teams/alphatauri/2019">
        <strong><span class="flag_right flag16_right_it">AlphaTauri</span></strong>
            </a>');

        $response->assertSee('<a href="/f1/drivers/nico-hulkenberg-31/2019">
        <strong><span class="flag_right flag16_right_de">Nico H&uuml;lkenberg</span></strong>
            </a>');
        $response->assertDontSee('<a href="/f1/drivers/esteban-ocon-68/2019">
        <strong><span class="flag_right flag16_right_fr">Esteban Ocon</span></strong>
            </a>');
    }

    /**
     * A test of the F1 sitemap XML file
     * @return void
     */
    public function testSitemapXML(): void
    {
        $response = $this->get('/f1/sitemap.xml');
        $response->assertStatus(200);

        // Races.
        $response->assertSee('/f1/races/2019-australia</loc>
        <changefreq>never</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertDontSee('/f1/races/2020-australia</loc>
        <changefreq>weekly</changefreq>
        <priority>1.0</priority>
    </url>');

        // Standings.
        $response->assertSee('/f1/standings</loc>
        <changefreq>weekly</changefreq>
        <priority>1</priority>
    </url>');
        $response->assertDontSee('/f1/standings/2020</loc>
        <changefreq>weekly</changefreq>
        <priority>1</priority>
    </url>');
        $response->assertSee('/f1/standings/2019</loc>
        <changefreq>never</changefreq>
        <priority>0.4</priority>
    </url>');
        $response->assertSee('/f1/standings/2008</loc>
        <changefreq>never</changefreq>
        <priority>0.4</priority>
    </url>');
        $response->assertDontSee('/f1/standings/2007</loc>
        <changefreq>never</changefreq>
        <priority>0.4</priority>
    </url>');

        // Teams.
        $response->assertSee('/f1/teams/toro-rosso/2019</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertDontSee('/f1/teams/alphatauri/2019</loc>
        <changefreq>yearly</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertSee('/f1/teams/alphatauri/2020</loc>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>');
        $response->assertDontSee('/f1/teams/toro-rosso/2020</loc>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>');

        // Drivers.
        $response->assertSee('/f1/drivers/esteban-ocon-68</loc>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>');
        $response->assertDontSee('/f1/drivers/esteban-ocon-68</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertDontSee('/f1/drivers/esteban-ocon-68/2020</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');

        $response->assertSee('/f1/drivers/nico-hulkenberg-31</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertDontSee('/f1/drivers/nico-hulkenberg-31</loc>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>');
        $response->assertDontSee('/f1/drivers/nico-hulkenberg-31/2019</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');
    }
}
