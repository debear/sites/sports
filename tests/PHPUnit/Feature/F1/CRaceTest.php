<?php

namespace Tests\PHPUnit\Feature\F1;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;
use DeBear\Implementations\TestResponse;

class CRaceTest extends MotorsportBaseCase
{
    /**
     * A test of an individual F1 race
     * @return void
     */
    public function testRace(): void
    {
        $response = $this->get('/f1/races/2019-italy');
        $response->assertStatus(200);

        // Due to length, these are broken down by feature.
        $this->raceFeatures($response);
        $this->raceQualifying($response);
        $this->raceResult($response);
        $this->raceStandings($response);
    }

    /**
     * Test individual features within a race
     * @param TestResponse $response The response we are testing against.
     * @return void
     */
    public function raceFeatures(TestResponse $response): void
    {
        $response->assertSee('<meta name="Description" content="Results and timings from '
            . 'the 2019 Italian Grand Prix." />');
        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; 2019 Races &raquo; '
            . 'Italian Grand Prix</title>');
        $response->assertSee('<h1 class="hidden-m hidden-tp flag128_it">Italian Grand Prix</h1>');
        $response->assertSee('<h1 class="hidden-d hidden-tl flag48_it">Italian Grand Prix</h1>');
        $response->assertSee('<span class="race-prev">&laquo; <span class="flag_right flag16_right_be">'
            . '<a href="/f1/races/2019-belgium">Belgium</a></span></span>');
        $response->assertSee('<span class="race-num"><strong>Race 14 of 21</strong></span>');
        $response->assertSee('<span class="race-next"><span class="flag_right flag16_right_sg">'
            . '<a href="/f1/races/2019-singapore">Singapore</a></span> &raquo;</span>');
        $response->assertSee('<span class="qualifying">
                                    <strong>Qualifying:</strong>
                                Saturday 7th September 2019
            </span>');
        $response->assertSee('<span class="race">
                                    <strong>Race:</strong>
                                Sunday 8th September 2019
            </span>');
    }

    /**
     * Test qualifying results within a race
     * @param TestResponse $response The response we are testing against.
     * @return void
     */
    public function raceQualifying(TestResponse $response): void
    {
        $response->assertSee('<li class="row" data-sort-grid="1" data-sort-q1="1" data-sort-q2="2" data-sort-q3="1">
            <dl>
                <dt class="row_head table-cell table-clear pos">1</dt>
                    <dd class="podium-1 table-cell num">16</dd>
                    <dd class="podium-1 table-cell table-name part-name"><span class="flag_right flag16_right_mc">'
            . '<a href="/f1/drivers/charles-leclerc-73/2019"');
        $response->assertSee('<dd class="row_0 table-cell session q3-abs q3-p10"><em>No Time</em></dd>
                                                    <dd class="row_0 table-cell session q3-rel q3-p10">'
            . '<em>No Time</em></dd>
                                                                <dd class="row_0 table-cell pos">20</dd>
                                            <dd class="row_0 table-cell info">
                            <span class="icon_info" title="Demoted due to penalties"></span>
                        </dd>');

        // Starting Grid.
        $response->assertSee('<ul class="inline_list subnav-grid result-grid hidden size-2">');
        $response->assertSee('<li class="podium-1 pos">1</li>
                <li class="num">16</li>
                <li class="participant"><span class="flag_right flag16_right_mc">'
            . '<a href="/f1/drivers/charles-leclerc-73/2019"');
        $response->assertSee('Max Verstappen</a></span></li>
                <li class="row_1 extra">
                    <div><em>No Time</em> (Q1)</div>');
    }

    /**
     * Test the results within a race
     * @param TestResponse $response The response we are testing against.
     * @return void
     */
    public function raceResult(TestResponse $response): void
    {
        $response->assertSee('<dt class="res-pos_1  table-cell table-clear res-pos">1</dt>
                    <dd class="row_0 table-cell num">16</dd>
                    <dd class="row_0 table-cell table-name part-name"><span class="flag_right flag16_right_mc">'
            . '<a href="/f1/drivers/charles-leclerc-73/2019"');
        $response->assertSee('<dd class="row_0 table-cell hidden-m laps">53</dd>
                    <dd class="row_0 table-cell hidden-m time">1:15:26.665</dd>
                                            <dd class="row_0 table-cell pts">25pts</dd>');
        $response->assertSee('<dd class="row_0 table-cell hidden-m laps">51</dd>
                    <dd class="row_0 table-cell hidden-m time">+2 laps</dd>
                                                                            '
            . '<dt class="res-ret  table-cell table-clear res-pos">Ret</dt>
                    <dd class="row_1 table-cell num">20</dd>
                    <dd class="row_1 table-cell table-name part-name"><span class="flag_right flag16_right_dk">'
            . '<a href="/f1/drivers/kevin-magnussen-54/2019"');
        $response->assertSee('<dd class="row_1 table-cell hidden-m laps">43</dd>
                    <dd class="row_1 table-cell hidden-m time"><em>Hydraulics</em></dd>');
        $response->assertSee('<dt class="row_head table-cell table-clear fl-title">Fastest Lap</dt>
                        <dd class="row_0 table-cell num">44</dd>');
        $response->assertSee('<dd class="row_0 table-cell fl-time">1:21.779</dd>
                        <dd class="row_0 table-cell fl-num">Lap 51</dd>');
        $response->assertSee('<li class="step name podium-2">Valtteri Bottas</li>
                <li class="step pos podium-2">2</li>');
        $response->assertSee('<li class="step name podium-1">Charles Leclerc</li>
                <li class="step pos podium-1">1</li>');

        // Gap to leader.
        $response->assertSee('<dd class="row_1 table-cell hidden-m time">+0.835</dd>');
        $response->assertSee('<dd class="row_1 table-cell hidden-m time">+1:14.492</dd>');

        // Lap Leaders.
        $response->assertSee('"yAxis":{"title":false,"min":0,"max":54,');
        $response->assertSee('"data":[26],"tooltip":"Laps 28&ndash;53","colorIndex":0}');
        $response->assertSee('"data":[8],"tooltip":"Laps 20&ndash;27","colorIndex":1}');
        $response->assertSee('"data":[20],"tooltip":"Pole Position and Laps 1&ndash;19","colorIndex":0}');
    }

    /**
     * Test the standings within a race
     * @param TestResponse $response The response we are testing against.
     * @return void
     */
    public function raceStandings(TestResponse $response): void
    {
        $response->assertSee('<dt class="row_head table-cell table-title">Drivers&#39; Standings</dt>');
        $response->assertSee('<dt class="row_head table-cell table-clear pos no-change">1</dt>');
        $response->assertSee('Lewis Hamilton</a></span></dd>
                            <dd class="podium-1 table-cell pts">+16pts</dd>
                        <dd class="podium-1 table-cell pts">284pts</dd>');
        $response->assertSee('Charles Leclerc</a></span></dd>
                            <dd class="row_0 table-cell pts">+25pts</dd>
                        <dd class="row_0 table-cell pts">182pts</dd>');
        $response->assertSee('<dt class="row_head table-cell table-clear pos ">5</dt>
                            <dd class="down table-cell change">
                    <span class="icon_arrow_down">1</span>
                </dd>');
        $response->assertSee('Sebastian Vettel</a></span></dd>
                            <dd class=" table-cell pts"></dd>
                        <dd class="row_1 table-cell pts">169pts</dd>');

        $response->assertSee('<dt class="row_head table-cell table-title">Constructors&#39; Standings</dt>');
    }

    /**
     * A test of an individual F1 race where the pole sitter no longer led after the first lap
     * @return void
     */
    public function testRaceNewLapLeader(): void
    {
        $response = $this->get('/f1/races/2019-australia');
        $response->assertStatus(200);

        // Lap Leaders.
        $response->assertSee('"yAxis":{"title":false,"min":0,"max":59,');
        $response->assertSee('"data":[34],"tooltip":"Laps 25&ndash;58","colorIndex":0}');
        $response->assertSee('"data":[2],"tooltip":"Laps 23 &amp; 24","colorIndex":1}');
        $response->assertSee('"data":[22],"tooltip":"Laps 1&ndash;22","colorIndex":0}');
        $response->assertSee('"data":[1],"tooltip":"Pole Position","colorIndex":2}');
    }

    /**
     * A test of the first F1 race of a season
     * @return void
     */
    public function testRaceFirst(): void
    {
        $response = $this->get('/f1/races/2018-australia');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; Formula One &raquo; 2018 Races &raquo; '
            . 'Australian Grand Prix</title>');
        $response->assertSee('<ul class="inline_list grid subnav-champ hidden standings first-round inc-team">');
        $response->assertDontSee('<ul class="inline_list grid subnav-champ hidden standings  inc-team">');
        $response->assertDontSee('<dd class=" table-cell pts"></dd>');
        $response->assertSee('<dt class="row_head table-cell table-title">Drivers&#39; Standings</dt>

        ' . '
                    <dt class="row_head table-cell table-clear pos ">1</dt>
                        <dd class="podium-1 table-cell table-name participant">'
            . '<span class="flag_right flag16_right_de"><a href="/f1/drivers/sebastian-vettel-15/2018"');

        $response->assertSee('<dt class="row_head table-cell table-clear pos ">6</dt>
                        <dd class="row_0 table-cell table-name participant">'
            . '<span class="flag_right flag16_right_in"><a href="/f1/teams/force-india/2018"');
        $response->assertDontSee(' table-cell table-name participant"><span class="flag_right flag16_right_gb">'
            . '<a href="/f1/teams/racing-point/2018"');
    }

    /**
     * A test of the F1 race URL nomenclature
     * @return void
     */
    public function testRaceNotMeeting(): void
    {
        $response = $this->get('/f1/meetings/2019-italy');
        $response->assertStatus(404);
    }

    /**
     * A test of miscellaneous F1 race controller actions
     * @return void
     */
    public function testRaceMisc(): void
    {
        // Race not found.
        $response = $this->get('/f1/races/2019-no-matches');
        $response->assertStatus(404);

        // Legacy routes - current season.
        $response = $this->get('/f1/race-1');
        $response->assertStatus(308);
        $response->assertRedirect('/f1/races/2019-australia');

        // Legacy routes - stating season.
        $response = $this->get('/f1/2019/race-22');
        $response->assertStatus(404);

        $response = $this->get('/f1/2019/race-1');
        $response->assertStatus(308);
        $response->assertRedirect('/f1/races/2019-australia');
    }
}
