<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class EStandingsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the default season.
     * @return void
     */
    public function testDefault(): void
    {
        $response = $this->get('/nhl/standings');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; 2019/20 Standings</title>');
        $response->assertSee('<li class="sel" data-id="2019">2019/20</li>');
        $response->assertSee('<h3 class="row_conf-Western misc-nhl-narrow-small-Western '
            . 'misc-nhl-narrow-small-right-Western">Western Conference</h3>');
        $response->assertSee('<dt class="row_conf-Western team head">Central Division</dt>
    ' . '
                    <dd class="row-1 team">
            <span class="icon team-nhl-STL">
                <a href="/nhl/teams/st-louis-blues" >St Louis Blues</a>
                            </span>
                            <sup>z</sup>
                    </dd>');
        $response->assertSee('<dl class="standings div">
        ' . '
                    <dt class="row_conf-Western col-first col gp  ">GP</dt>
                    <dt class="row_conf-Western  col w  ">W</dt>
                    <dt class="row_conf-Western  col l  ">L</dt>
                    <dt class="row_conf-Western  col otl  ">OTL</dt>
                    <dt class="row_conf-Western  col pts  ">Pts</dt>
                    <dt class="row_conf-Western  col pts_pct  ">Pts %</dt>
                    <dt class="row_conf-Western  col gf  ">GF</dt>
                    <dt class="row_conf-Western  col ga  ">GA</dt>
                    <dt class="row_conf-Western  col gd  ">Diff</dt>
                    <dt class="row_conf-Western  col home  ">Home</dt>
                    <dt class="row_conf-Western  col visitor  ">Visitor</dt>
                    <dt class="row_conf-Western  col div  ">v Div</dt>
                    <dt class="row_conf-Western  col recent  ">Last 10</dt>
                ' . '
                                                <dd class="row-1 col-first col gp ">');
    }

    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/nhl/standings/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; 2019/20 Standings</title>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/nhl/standings/2019/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; NHL &raquo; 2019/20 Standings</title>');
        $response->assertSee('<h1>2019/20 Standings</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/nhl/standings/2000');
        $response->assertStatus(404);
        $response = $this->get('/nhl/standings/2039');
        $response->assertStatus(404);
    }
}
