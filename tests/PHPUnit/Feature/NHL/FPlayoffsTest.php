<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class FPlayoffsTest extends MajorLeagueBaseCase
{
    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/nhl/playoffs/2019');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; 2019/20 Stanley Cup Playoffs</title>');
        $response->assertSee('<li class="sel" data-id="2019">2019/20</li>');

        $response->assertSee('<li class="grid-4-9 rounds-5 conf-right">
    ' . '
            <h3 class="conf row_conf-Eastern misc-nhl-narrow-small-Eastern misc-nhl-narrow-small-right-Eastern">
            <span class="full">Eastern Conference</span>
            <span class="short">Eastern</span>
        </h3>');
        $response->assertDontSee('<div class="byes">');
        $response->assertSee('<li class="grid-1-4 num-4-4">
                                    <fieldset class="matchup hover ice">
    <div class="team team-nhl-PIT loser">
        <span class="team">PIT <sup>5</sup></span>
        <span class="score">1</span>
    </div>
    <div class="team team-nhl-MTL winner">
        <span class="team">MTL <sup>12</sup></span>
        <span class="score">3</span>
    </div>
            <div class="link row-head">
                            <span class="icon_playoffs" data-link="/nhl/playoffs/2019/penguins-canadiens-02" '
            . 'data-modal="series">Summary</span>
                    </div>
    </fieldset>');
        $response->assertSee('<li class="champ rounds-5 grid-1-9 num-1-4">
            <h3 class="row-head">Stanley Cup Finals</h3>
        <fieldset class="matchup  ice">
    <div class="team team-nhl-TB winner">
        <span class="team">TB <sup>2</sup></span>
        <span class="score">4</span>
    </div>
    <div class="team team-nhl-DAL loser">
        <span class="team">DAL <sup>3</sup></span>
        <span class="score">2</span>
    </div>
    </fieldset>
        ' . '
    <div class="logo ">
        <div class="large playoffs-nhl-large-0000 playoffs-nhl-large-2019"></div>
                    <div class="medium playoffs-nhl-medium-0000 playoffs-nhl-medium-2019"></div>
            </div>
</li>');
        $response->assertSee('<li class="round_robin conf-right">
    <h3 class="row_conf-Eastern">Round Robin</h3>
    <dl>
        <dt class="conf row_conf-Eastern misc-nhl-narrow-small-Eastern"></dt>
        <dt class="stat row_conf-Eastern">W</dt>
<dt class="stat row_conf-Eastern">L</dt>
<dt class="stat row_conf-Eastern">OTL</dt>
<dt class="stat row_conf-Eastern">Pts</dt>
                                <dt class="seed row_conf-Eastern">1</dt>
                <dd class="row-1 team team-nhl-PHI">
                    <span class="abbrev">PHI</span>
                    <span class="name">Philadelphia Flyers</span>
                    <sup>4</sup>
                </dd>
                <dd class="stat row-1">3</dd>
<dd class="stat row-1">0</dd>
<dd class="stat row-1">0</dd>
<dd class="stat row-1">6</dd>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/nhl/playoffs/2019/body');
        $response->assertStatus(200);
        $response->assertSee('<h1><span class="hidden-m">2019/20</span> Stanley Cup Playoffs</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/nhl/playoffs/2000');
        $response->assertStatus(404);
        $response = $this->get('/nhl/playoffs/2039');
        $response->assertStatus(404);
    }

    /**
     * A test of playoff series
     * @return void
     */
    public function testSeries(): void
    {
        $response = $this->get('/nhl/playoffs/2019/lightning-bruins-22');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; 2019/20 Stanley Cup Playoffs &raquo; '
            . 'Tampa Bay Lightning vs Boston Bruins</title>');
        $response->assertSee('<body class="section-nhl is_popup">

    ' . '
<h2 class="row-head">
    <span class="team-nhl-TB">Lightning</span> <sup>2</sup>
    -vs-
    <span class="team-nhl-BOS">Bruins</span> <sup>4</sup>
</h2>');
        $response->assertSee('<dl class="games">
            <dt class="row-head">
    Game 1
            <span class="status">Final</span>
    </dt>
    ' . '
    <dd class="game_date row-1">
        <span class="date">Sun 23rd Aug</span>
        <span class="time">5:00pm PDT</span>
    </dd>
    ' . '
    <dd class="game clearfix">
        <ul class="inline_list">
            <li class="visitor winner team-nhl-small-BOS">
                                    <span class="score">3</span>
                            </li>
            <li class="at">@</li>
            <li class="home loser team-nhl-small-right-TB">
                                    <span class="score">2</span>
                            </li>
                            <li class="links">
                                                                '
            . '<a class="icon_game_series link-4" '
            . 'href="/nhl/schedule/20200823/bruins-at-lightning-p221#series"><em>Playoff Series</em></a>
                                            <a class="icon_game_locations link-3" '
            . 'href="/nhl/schedule/20200823/bruins-at-lightning-p221#events"><em>Game Events</em></a>
                                            <a class="icon_game_scoring link-2" '
            . 'href="/nhl/schedule/20200823/bruins-at-lightning-p221#scoring"><em>Scoring</em></a>
                                            <a class="icon_game_boxscore link-1" '
            . 'href="/nhl/schedule/20200823/bruins-at-lightning-p221"><em>Boxscore</em></a>
                                            <a class="icon_game_stats link-0" '
            . 'href="/nhl/schedule/20200823/bruins-at-lightning-p221#stats"><em>Game Stats</em></a>
                                    </li>
                    </ul>
    </dd>');
        $response->assertSee('<dd class="summary row-1">
        Lightning win the series, 4&ndash;1
    </dd>');

        // Inverted series.
        $response = $this->get('/nhl/playoffs/2019/bruins-lightning-22');
        $response->assertStatus(404);
    }

    /**
     * A test of round robin games
     * @return void
     */
    public function testRoundRobin(): void
    {
        $response = $this->get('/nhl/playoffs/2019/round-robin-4');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; 2019/20 Stanley Cup Playoffs &raquo; '
            . 'Eastern Conference Round Robin</title>');
        $response->assertSee('<h2 class="conf row_conf-Eastern misc-nhl-small-Eastern misc-nhl-small-right-Eastern">'
            . 'Eastern Conference</h2>');
        $response->assertSee('<dl class="games">
            <dt class="row-head">
    Game 1
            <span class="status">Final</span>
    </dt>
    ' . '
    <dd class="game_date row-1">
        <span class="date">Sun 2nd Aug</span>
        <span class="time">12:00pm PDT</span>
    </dd>
    ' . '
    <dd class="game clearfix">
        <ul class="inline_list">
            <li class="visitor winner team-nhl-small-PHI">
                                    <span class="score">4</span>
                            </li>
            <li class="at">@</li>
            <li class="home loser team-nhl-small-right-BOS">
                                    <span class="score">1</span>
                            </li>
                            <li class="links">
                                                                '
            . '<a class="icon_game_series link-4" '
            . 'href="/nhl/schedule/20200802/flyers-at-bruins-p1#series"><em>Playoff Series</em></a>
                                            <a class="icon_game_locations link-3" '
            . 'href="/nhl/schedule/20200802/flyers-at-bruins-p1#events"><em>Game Events</em></a>
                                            <a class="icon_game_scoring link-2" '
            . 'href="/nhl/schedule/20200802/flyers-at-bruins-p1#scoring"><em>Scoring</em></a>
                                            <a class="icon_game_boxscore link-1" '
            . 'href="/nhl/schedule/20200802/flyers-at-bruins-p1"><em>Boxscore</em></a>
                                            <a class="icon_game_stats link-0" '
            . 'href="/nhl/schedule/20200802/flyers-at-bruins-p1#stats"><em>Game Stats</em></a>
                                    </li>
                    </ul>
    </dd>');

        // Season did not have a round robin.
        $response = $this->get('/nhl/playoffs/2018/round-robin-4');
        $response->assertStatus(404);
        // Inverted conference.
        $response = $this->get('/nhl/playoffs/2019/round-robin-1');
        $response->assertStatus(404);
    }
}
