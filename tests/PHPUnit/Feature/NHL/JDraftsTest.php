<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class JDraftsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the default draft
     * @return void
     */
    public function testDefault(): void
    {
        $response = $this->get('/nhl/draft');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; 2019 NHL Entry Draft</title>');
        $response->assertSee('<h1>2019 NHL Entry Draft</h1>');

        // Available draft history.
        $response->assertSee('<dl class="dropdown " data-id="switcher-season"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2019">2019</li>
                            <li  data-id="2018">2018</li>');
        $response->assertSee('<li  data-id="1964">1964</li>
                            <li  data-id="1963">1963</li>
                    </ul>
    </dd>
</dl>');

        // 7 Rounds in this draft.
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="round-1">Round 1</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="round-2">Round 2</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="round-3">Round 3</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="round-4">Round 4</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="round-5">Round 5</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="round-6">Round 6</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="round-7">Round 7</item>
                <sep class="right"></sep>
    </desk>');

        // First pick.
        $response->assertSee('<dl class="inline_list subnav-round-1  round clearfix">
        <dt class="row-head pick">Pick</dt>
        <dt class="row-head team">Team</dt>
        <dt class="row-head name">Player</dt>
        <dt class="row-head height">Height</dt>
        <dt class="row-head weight">Weight</dt>
        <dt class="row-head junior">Junior</dt>
        <dt class="row-head note">Notes</dt>
                                <dd class="row-head pick">1</dd>
                        <dd class="row-0 team"><span class="team-nhl-NJ">NJ</span></dd>
            <dd class="row-0 name"><span class="flag16_us">Jack Hughes</span> (C)</dd>
            <dd class="row-0 height">5\' 10"</dd>
            <dd class="row-0 weight">171lb</dd>
            <dd class="row-0 junior no-note ">USA U-18 (NTDP)</dd>
            <dd class="row-0 note none"></dd>');

        // A much traded selection.
        $response->assertSee('<dd class="row-head pick-rnd">27</dd>
                            <dd class="row-0 pick-ov">151</dd>
                        <dd class="row-0 team"><span class="team-nhl-ARI">ARI</span></dd>
            <dd class="row-0 name"><span class="flag16_fi">Aku Raty</span> (RW)</dd>
            <dd class="row-0 height">5\' 11"</dd>
            <dd class="row-0 weight">170lb</dd>
            <dd class="row-0 junior  ">KARPAT JR. (FINLAND-JR.)</dd>
            <dd class="row-0 note " title="From TB via CHI &amp; PIT">From <span class="team-nhl-TB">TB</span> via '
            . '<span class="team-nhl-CHI">CHI</span> &amp; <span class="team-nhl-PIT">PIT</span></dd>');
    }

    /**
     * A test of a specific draft
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/nhl/draft/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; 2019 NHL Entry Draft</title>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/nhl/draft/2019/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; NHL &raquo; 2019 NHL Entry Draft</title>');
        $response->assertSee('<h1>2019 NHL Entry Draft</h1>');

        // Though before 1980 it was known as the Amateur Draft.
        $response = $this->get('/nhl/draft/1980/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; NHL &raquo; 1980 NHL Entry Draft</title>');
        $response->assertSee('<h1>1980 NHL Entry Draft</h1>');
        $response = $this->get('/nhl/draft/1979/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; NHL &raquo; 1979 NHL Amateur Draft</title>');
        $response->assertSee('<h1>1979 NHL Amateur Draft</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        // We had drafts in the 1900s (unlike things like standings) as the first draft was in 1963.
        $response = $this->get('/nhl/draft/1963');
        $response->assertStatus(200);
        $response = $this->get('/nhl/draft/1962');
        $response->assertStatus(404);
        // The last draft we have is in 2019.
        $response = $this->get('/nhl/draft/2019');
        $response->assertStatus(200);
        $response = $this->get('/nhl/draft/2020');
        $response->assertStatus(404);
    }
}
