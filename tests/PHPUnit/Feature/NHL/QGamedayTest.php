<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class QGamedayTest extends MajorLeagueBaseCase
{
    /**
     * A test of the starting lineups for the current date
     * @return void
     */
    public function testLineupsCurrent(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-08-31 12:34:56');

        $response = $this->get('/nhl/lineups');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/nhl/lineups/20200831" data-date="20200831">
        <span class="date hidden-m">Monday 31st August</span>
        <span class="date hidden-t hidden-d">Mon 31st Aug</span>
        <calendar');

        $response->assertSee('<li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <ul class="inline_list nhl_box game_list lineups clearfix">
                    <li class="info head">4:00pm PDT, Scotiabank Arena</li>
                                            <li class="info">Eastern Conference Semi Final, Game 5</li>
                                        <li class="team visitor row_nhl-BOS">
                        <span class="icon team-nhl-BOS">Bruins</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home row_nhl-TB">
                        <span class="icon team-nhl-TB">Lightning</span>
                    </li>
                    <li class="list visitor"><ul class="inline_list">
                                    <li class="row-0 head pos">C</li>
            <li class="row-0 name"><a href="/nhl/players/patrice-bergeron-39">Patrice Bergeron</a></li>
                                <li class="row-1 head pos">C</li>
            <li class="row-1 name"><a href="/nhl/players/brad-marchand-464">Brad Marchand</a></li>
                                <li class="row-0 head pos">RW</li>
            <li class="row-0 name"><a href="/nhl/players/david-pastrnak-2356">David Pastrnak</a></li>
                                <li class="row-1 head pos">D</li>
            <li class="row-1 name"><a href="/nhl/players/brandon-carlo-2599">Brandon Carlo</a></li>
                                <li class="row-0 head pos">D</li>
            <li class="row-0 name"><a href="/nhl/players/torey-krug-1783">Torey Krug</a></li>
                                <li class="row-1 head pos">G</li>
            <li class="row-1 name"><a href="/nhl/players/jaroslav-halak-278">Jaroslav Halak</a></li>
            </ul>
</li>
                    <li class="list home"><ul class="inline_list">
                                    <li class="row-0 head pos">C</li>
            <li class="row-0 name"><a href="/nhl/players/barclay-goodrow-2332">Barclay Goodrow</a></li>
                                <li class="row-1 head pos">C</li>
            <li class="row-1 name"><a href="/nhl/players/blake-coleman-2679">Blake Coleman</a></li>
                                <li class="row-0 head pos">C</li>
            <li class="row-0 name"><a href="/nhl/players/yanni-gourde-2435">Yanni Gourde</a></li>
                                <li class="row-1 head pos">D</li>
            <li class="row-1 name"><a href="/nhl/players/erik-cernak-2917">Erik Cernak</a></li>
                                <li class="row-0 head pos">D</li>
            <li class="row-0 name"><a href="/nhl/players/mikhail-sergachev-2613">Mikhail Sergachev</a></li>
                                <li class="row-1 head pos">G</li>
            <li class="row-1 name"><a href="/nhl/players/andrei-vasilevskiy-2368">Andrei Vasilevskiy</a></li>
            </ul>
</li>
                </ul>
            </li>');
    }

    /**
     * A test of the starting lineups for a specific date
     * @return void
     */
    public function testLineupsDated(): void
    {
        $response = $this->get('/nhl/lineups/20200310');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/nhl/lineups/20200310" data-date="20200310">
        <span class="date hidden-m">Tuesday 10th March</span>
        <span class="date hidden-t hidden-d">Tue 10th Mar</span>
        <calendar');

        $response->assertSee('<li class="grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
                <ul class="inline_list nhl_box game_list lineups clearfix">
                    <li class="info head">4:00pm PDT, Scotiabank Arena</li>
                                        <li class="team visitor row_nhl-TB">
                        <span class="icon team-nhl-TB">Lightning</span>
                    </li>
                    <li class="head at">@</li>
                    <li class="team home row_nhl-TOR">
                        <span class="icon team-nhl-TOR">Maple Leafs</span>
                    </li>
                    <li class="list visitor"><ul class="inline_list">
                                    <li class="row-0 head pos">C</li>
            <li class="row-0 name"><a href="/nhl/players/blake-coleman-2679">Blake Coleman</a></li>
                                <li class="row-1 head pos">C</li>
            <li class="row-1 name"><a href="/nhl/players/brayden-point-2578">Brayden Point</a></li>
                                <li class="row-0 head pos">LW</li>
            <li class="row-0 name"><a href="/nhl/players/ondrej-palat-2106">Ondrej Palat</a></li>
                                <li class="row-1 head pos">D</li>
            <li class="row-1 name"><a href="/nhl/players/ryan-mcdonagh-1524">Ryan McDonagh</a></li>
                                <li class="row-0 head pos">D</li>
            <li class="row-0 name"><a href="/nhl/players/erik-cernak-2917">Erik Cernak</a></li>
                                <li class="row-1 head pos">G</li>
            <li class="row-1 name"><a href="/nhl/players/andrei-vasilevskiy-2368">Andrei Vasilevskiy</a></li>
            </ul>
</li>');
    }

    /**
     * A test of miscellaneous controller actions
     * @return void
     */
    public function testLineupsControllerMisc(): void
    {
        $this->setTestDateTime('2020-08-31 12:34:56');

        $response = $this->get('/nhl/lineups/2019/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nhl/lineups/20200831/switcher');
    }
}
