<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class KAwardsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/nhl/awards');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; NHL Award Winners</title>');
        $response->assertSee('<h1>NHL Award Winners</h1>');

        $response->assertSee('<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="nhl_box award clearfix">
        ' . '
        <h3 class="row-head">Hart Memorial Trophy</h3>

        ' . '
        <div class="mugshot-small">
                            <img class="mugshot-small" loading="lazy" src="');
        $response->assertSee('" alt="Profile photo silhouette">
                    </div>

        ' . '
        <p class="descrip">Awarded to the player judged most valuable to his team</p>

        ' . '
        <p class="winner">
            <strong>2019/20 Winner:</strong>
                            <em>Not Awarded</em>
            ' . '
                    </p>

        ' . '
        <p class="link"><a class="all-time" data-link="/nhl/awards/hart-memorial-trophy-1">All-Time Winners</a></p>
    </fieldset>
</li>');
    }

    /**
     * A test of the all-time winners.
     * @return void
     */
    public function testAllTime(): void
    {
        // A non-award.
        $response = $this->get('/nhl/awards/does-not-exist-99');
        $response->assertStatus(404);
        // An actual award.
        $response = $this->get('/nhl/awards/hart-memorial-trophy-1');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'season' => '2019/20',
            'winner' => [],
        ]);
        $response->assertJsonFragment([
            'season' => '2018/19',
            'winner' => [
                'player' => 'Nikita Kucherov',
                'team' => 'Tampa Bay Lightning',
                'team_id' => 'TB',
                'pos' => 'RW',
            ],
        ]);
    }
}
