<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class XLegacyMigrationTest extends MajorLeagueBaseCase
{
    /**
     * A test of legacy migration of General URLs.
     * @return void
     */
    public function testGeneral(): void
    {
        // Index.
        $response = $this->get('/nhl/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl');

        $response = $this->get('/nhl/2019');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl');

        $response = $this->get('/nhl/2019/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl');

        // Standings.
        $response = $this->get('/nhl/2019/standings');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/standings/2019');

        $response = $this->get('/nhl/standings/expanded');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/nhl/standings/{$this->season}");

        $response = $this->get('/nhl/2019/standings/league');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/standings/2019');

        // News.
        $response = $this->get('/nhl/2019/news');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/news');

        $response = $this->get('/nhl/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/news');

        $response = $this->get('/nhl/2019/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/news');
    }

    /**
     * A test of legacy migration of Schedule URLs.
     * @return void
     */
    public function testSchedule(): void
    {
        // Schedule.
        $response = $this->get('/nhl/2000/schedule');
        $response->assertStatus(404);

        $response = $this->get('/nhl/2019/schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule/20200831');

        $response = $this->get('/nhl/schedule-20191201');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule/20191201');

        $response = $this->get('/nhl/2019/schedule-20191201');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule/20191201');

        // Playoffs.
        $response = $this->get('/nhl/2019/playoffs');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/playoffs/2019');

        $response = $this->get('/nhl/playoffs/11');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/playoffs');

        $response = $this->get('/nhl/2019/playoffs/11');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/playoffs/2019/flyers-canadiens-11');

        $response = $this->get('/nhl/2019/playoffs/99');
        $response->assertStatus(404);
    }

    /**
     * A test of legacy migration of individual Game URLs.
     * @return void
     */
    public function testGame(): void
    {
        // Invalid game type or season.
        $response = $this->get('/nhl/game-s123');
        $response->assertStatus(404);
        $response = $this->get('/nhl/game-rp123');
        $response->assertStatus(404);
        $response = $this->get('/nhl/2000/game-r123');
        $response->assertStatus(404);

        // Ambiguous season.
        $response = $this->get('/nhl/game-r123');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule');
        $response = $this->get('/nhl/game-r123-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule');

        // To the new format.
        $response = $this->get('/nhl/2019/game-r123');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule/20191019/sabres-at-sharks-r123');

        $response = $this->get('/nhl/2019/game-r123-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule/20191019/sabres-at-sharks-r123/boxscore');
    }

    /**
     * A test of legacy migration of Stats URLs.
     * @return void
     */
    public function testStats(): void
    {
        // Stats Summary.
        $response = $this->get('/nhl/2019/stats');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/stats');

        // Tables.
        $response = $this->get('/nhl/stats-ind-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/stats/players/skater?season=2019&type=regular');

        $response = $this->get('/nhl/2019/stats-ind-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/stats/players/skater?season=2019&type=regular');

        // By Team Stats.
        $response = $this->get('/nhl/stats-AAA');
        $response->assertStatus(404);

        $response = $this->get('/nhl/stats-TB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/stats/players/skater?team_id=TB&season=2019&'
            . 'type=regular');

        $response = $this->get('/nhl/2019/stats-TB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/stats/players/skater?team_id=TB&season=2019&'
            . 'type=regular');

        $response = $this->get('/nhl/stats-TB-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/stats/players/skater?team_id=TB&season=2019&'
            . 'type=regular');

        $response = $this->get('/nhl/2019/stats-TB-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/stats/players/skater?team_id=TB&season=2019&'
            . 'type=regular');
    }

    /**
     * A test of legacy migration of Team URLs.
     * @return void
     */
    public function testTeams(): void
    {
        $response = $this->get('/nhl/2019/teams');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/teams');

        $response = $this->get('/nhl/team-AAA');
        $response->assertStatus(404);

        $response = $this->get('/nhl/team-TB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/teams/tampa-bay-lightning');

        $response = $this->get('/nhl/2019/team-TB');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/teams/tampa-bay-lightning');

        $response = $this->get('/nhl/team-TB-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/teams/tampa-bay-lightning');

        $response = $this->get('/nhl/2019/team-TB-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/teams/tampa-bay-lightning');
    }

    /**
     * A test of legacy migration of Player URLs.
     * @return void
     */
    public function testPlayers(): void
    {
        // List.
        $response = $this->get('/nhl/2019/players');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/players');

        // Individual.
        $response = $this->get('/nhl/player-0');
        $response->assertStatus(404);

        $response = $this->get('/nhl/player-729');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/players/steven-stamkos-729');

        $response = $this->get('/nhl/player-729-career');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/nhl/players/steven-stamkos-729/{$this->season}/career");

        $response = $this->get('/nhl/2019/player-729');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/players/steven-stamkos-729/2019');

        $response = $this->get('/nhl/2019/player-729-career');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/players/steven-stamkos-729/2019/career');
    }

    /**
     * A test of the legacy migration of Power Rank URLs.
     * @return void
     */
    public function testPowerRanks(): void
    {
        $response = $this->get('/nhl/2019/power-ranks');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/power-ranks/2019');

        $response = $this->get('/nhl/power-ranks-12');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/nhl/power-ranks/{$this->season}/12");

        $response = $this->get('/nhl/2019/power-ranks-12');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/power-ranks/2019/12');
    }
}
