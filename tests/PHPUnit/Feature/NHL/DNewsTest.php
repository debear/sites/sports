<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class DNewsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the NHL news.
     * @return void
     */
    public function testNews(): void
    {
        $response = $this->get('/nhl/news');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/nhl-suspends-2019-20-season-184917336.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">NHL suspends 2019-20 season indefinitely'
            . '</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/national-hockey-league-halts-season-over-coronavirus-worries-181833504-'
            . '-nhl.html?src=rss" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">National '
            . 'Hockey League halts season over coronavirus worries</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://sports.yahoo.com/nhl-suspends-season-175149700.html?src=rss" rel="noopener noreferrer" '
            . 'target="_blank" title="Opens in new tab/window">NHL suspends season</a></h4>');

        $response->assertSee('<next class="onclick_target prev-next mini"><i class="fas fa-chevron-circle-right"></i>'
            . '</next>
    <bullets>
                    <bullet class="onclick_target" data-ref="1">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>
                    <bullet class="onclick_target" data-ref="2">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>');

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/nhl-decides-pause-regular-season-174326866.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">NHL decides to &lsquo;pause&rsquo; regular '
            . 'season due to coronavirus</a></h4>');
        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.espn.com/nhl/story/_/id/28891277/nhl-weighs-options-tells-teams-not-practice" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">NHL weighs options, tells '
            . 'teams not to practice</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/latest-juventus-player-virus-says-100033113.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">The Latest: NCAA cancels hoops tourney, '
            . 'all championships</a></h4>');

        $response->assertSee('<li class="page_box unshaded_row ">
            <strong>1</strong>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="16"><em>16</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');
    }

    /**
     * A test of a specific NHL news page.
     * @return void
     */
    public function testNewsPage(): void
    {
        // Valid page.
        $response = $this->get('/nhl/news?page=2');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/latest-juventus-player-virus-says-100033113.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">The Latest: NCAA cancels hoops tourney, '
            . 'all championships</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://sports.yahoo.com/latest-juventus-player-virus-says-100033113.html?src=rss" rel="noopener '
            . 'noreferrer" target="_blank" title="Opens in new tab/window">The Latest: IIHF weighs canceling men’s '
            . 'world championships</a></h4>');

        $response->assertSee('<li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="1">1</a>
        </li>
            <li class="page_box unshaded_row ">
            <strong>2</strong>
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="3">3</a>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>
            <li class="page_box unshaded_row ">
            &hellip;
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="16"><em>16</em></a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="3">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');

        // Error.
        $response = $this->get('/nhl/news?page=17');
        $response->assertStatus(404);
    }
}
