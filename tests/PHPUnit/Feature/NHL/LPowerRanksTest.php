<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class LPowerRanksTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main page.
     * @return void
     */
    public function testMain(): void
    {
        $response = $this->get('/nhl/power-ranks');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Power Ranks</title>');
        $response->assertSee('<li class="sel" data-href="2019/22" data-id="22">Week 22</li>');
        $response->assertDontSee('<li  data-href="2019/23" data-id="23">Week 23</li>');
        $response->assertSee('<dl class="power_ranks ranks-22">');
        $response->assertSee('<dd class="dates">Covering games up to 8th March</dd>');

        $response->assertSee('<dt class="row-1 rank">1</dt>
    ' . '
                    <dd class="row-1 change">
            <span class="hidden-m"><strong>Last Week:</strong> 1</span>
            <span class="icon_pos_nc">NC</span>
        </dd>
        ' . '
    <dd class="row-1 team">
        <span class="logo hidden-m team-nhl-narrow_large-PHI"></span>
        <span class="logo hidden-t hidden-d team-nhl-narrow_small-PHI"></span>
        <h3><a href="/nhl/teams/philadelphia-flyers">Philadelphia Flyers</a></h3>
    </dd>
    ' . '
    <dd class="row-1 schedule">
        <dl>
            <dt>This week:</dt>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                        5 &ndash; 2
                                                    </span>
                        @
                        <span class="opp icon team-nhl-WSH">WSH</span>
                                            </dd>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                        4 &ndash; 1
                                                    </span>
                        v
                        <span class="opp icon team-nhl-CAR">CAR</span>
                                            </dd>
                                    <dd class="game">
                        <span class="score result-win">
                            W
                                                        3 &ndash; 1
                                                    </span>
                        v
                        <span class="opp icon team-nhl-BUF">BUF</span>
                                            </dd>
                        </dl>
    </dd>');
        $response->assertDontSee('<dt class="row-0 rank">32</dt>');
    }

    /**
     * A test when only the season was passed.
     * @return void
     */
    public function testSeasonOnly(): void
    {
        // A valid season.
        $response = $this->get('/nhl/power-ranks/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Power Ranks</title>');
        $response->assertSee('<li class="sel" data-href="2019/22" data-id="22">Week 22</li>');

        // A future season.
        $response = $this->get('/nhl/power-ranks/2020');
        $response->assertStatus(404);
    }

    /**
     * A test when specifying the week.
     * @return void
     */
    public function testSpecifyingWeek(): void
    {
        // A valid week.
        $response = $this->get('/nhl/power-ranks/2019/22');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Power Ranks &raquo; Week 22</title>');
        $response->assertSee('<li class="sel" data-href="2019/22" data-id="22">Week 22</li>');

        // An uncalced week.
        $response = $this->get('/nhl/power-ranks/2019/23');
        $response->assertStatus(404);

        // A non-existant week.
        $response = $this->get('/nhl/power-ranks/2019/99');
        $response->assertStatus(404);
    }

    /**
     * A test of the first week.
     * @return void
     */
    public function testFirstWeek(): void
    {
        $response = $this->get('/nhl/power-ranks/2019/1');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Power Ranks &raquo; Week 1</title>');
        $response->assertSee('<li class="sel" data-href="2019/1" data-id="1">Week 1</li>');
        $response->assertDontSee('<strong>Last Week:</strong>');
    }
}
