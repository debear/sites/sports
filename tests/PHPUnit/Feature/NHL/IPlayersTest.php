<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class IPlayersTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-03-02 12:34:56');

        $response = $this->get('/nhl/players');
        $response->assertStatus(200);

        $response->assertSee('<h1>NHL Players</h1>');

        // By Team.
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row_conf-Western misc-nhl-narrow-small-Western">Western</dt>
                                                    <dd class="row-0 team-nhl-ANA icon">'
            . '<a href="/nhl/teams/anaheim-ducks#roster">Anaheim Ducks</a></dd>');

        // Name Search.
        $response->assertSee('<dl class="nhl_box clearfix">
            <dt class="row-head">Search By Name</dt>
            <dt class="row-1">Player&#39;s Name:</dt>
                <dd class="row-0 name"><input type="text" class="textbox"></dd>
                <dd class="row-0 button"><button class="btn_small btn_green">Search</button></dd>
                <dd class="results hidden"></dd>
        </dl>');

        // Birthdays.
        $response->assertSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
        $response->assertSee('<div>
                <ul class="inline_list">
                                            <li class="row-0">'
            . '<a href="/nhl/players/charlie-coyle-2100">Charlie Coyle</a> (28)</li>
                                            <li class="row-1">'
            . '<a href="/nhl/players/michael-hutchinson-1595">Michael Hutchinson</a> (30)</li>
                                    </ul>
            </div>');
    }

    /**
     * A test of the main list with no birthdays.
     * @return void
     */
    public function testListNoBirthdays(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-02-29 12:34:56');

        $response = $this->get('/nhl/players');
        $response->assertStatus(200);

        // Available sections.
        $response->assertSee('<h1>NHL Players</h1>');
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row-head">Search By Name</dt>');
        $response->assertDontSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
    }

    /**
     * A test of the player search.
     * @return void
     */
    public function testSearch(): void
    {
        // Input length.
        $response = $this->get('/nhl/players/search/');
        $response->assertStatus(404); // Must have at least one character.
        $response = $this->get('/nhl/players/search/a');
        $response->assertStatus(400);
        $response = $this->get('/nhl/players/search/abc');
        $response->assertStatus(200);
        $response->assertExactJson([]); // This should also be an empty list.

        // A search result.
        $response = $this->get('/nhl/players/search/hedman');
        $response->assertStatus(200);
        $response->assertExactJson([[
            'player_id' => 297,
            'first_name' => 'Victor',
            'surname' => 'Hedman',
            'url' => '/nhl/players/victor-hedman-297',
        ]]); // This should also be an empty list.
    }

    /**
     * A test of an individual skater's player page.
     * @return void
     */
    public function testIndividualSkater(): void
    {
        $response = $this->get('/nhl/players/nikita-kucherov-2220');
        $response->assertStatus(200);
        $response->assertSee('<h1 class="team-nhl-narrow_large-right-TB">Nikita Kucherov'
            . '<span class="hidden-m jersey">#86</span><span class="hidden-m pos">RW</span></h1>');

        // Header.
        $response->assertSee('<ul class="inline_list summary">
            <li class="row-head">2019/20 Regular Season:</li>
                            <li class="stat grid-1-2">
                    <dl class="nhl_box">
                        <dt class="row-head">Points</dt>
                            <dd class="row-1 value">85</dd>
                            <dd class="row-0 rank">7th</dd>
                    </dl>
                </li>');
        $response->assertSee('<dl class="details">
                <dt class="height">Height:</dt>
            <dd class="height">5&#39; 11&quot;</dd>');

        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="career">Career Stats</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="gamelog">Game Log</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="heatmaps">Heatmaps</item>
                <sep class="right"></sep>
    </desk>');

        // Summary.
        $response->assertSee('<div class="subnav-home subnav-tab  clearfix">
                            <scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-skater" data-default="0" data-total="32" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">68</cell>
                                            <cell class="row-1 starts">22</cell>
                                            <cell class="row-1 goals">33</cell>
                                            <cell class="row-1 assists">52</cell>
                                            <cell class="row-1 points">85</cell>
                                            <cell class="row-1 plus_minus">+26</cell>
                                            <cell class="row-1 pims">38</cell>
                                            <cell class="row-1 shots">210</cell>
                                            <cell class="row-1 shot_pct">15.7</cell>');
        $response->assertSee('<h3 class="games scrolltable-game-skater row-head">Last 10 Games</h3>
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game scrolltable-game-skater" data-default="0" '
            . 'data-total="32" data-current="start">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Mon 31st Aug</cell>
                <cell class="row-1 opp">
                    v<span class="team-nhl-BOS">BOS</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-win">W 3 &ndash; 2</span>
                    <a class="icon_game_boxscore" href="/nhl/schedule/20200831/bruins-at-lightning-p225"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 start"><span class="fa fa-times-circle"></span></cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">0</cell>
                                            <cell class="row-1 points">0</cell>
                                            <cell class="row-1 plus_minus">0</cell>
                                            <cell class="row-1 pims">2</cell>
                                            <cell class="row-1 shots">1</cell>
                                            <cell class="row-1 shot_pct">0.0</cell>');
    }

    /**
     * A test of an individual skater's player career tab.
     * @return void
     */
    public function testIndividualSkaterTabsCareer(): void
    {
        $response = $this->getTab('/nhl/players/nikita-kucherov-2220/career?group=skater');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="true" '
            . 'data-skater-advanced="true" data-goalie="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-skater" data-default="0" data-total="32" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2018/19</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2018/19</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">68</cell>
                                            <cell class="row-1 starts">22</cell>
                                            <cell class="row-1 goals">33</cell>
                                            <cell class="row-1 assists">52</cell>
                                            <cell class="row-1 points">85</cell>
                                            <cell class="row-1 plus_minus">+26</cell>
                                            <cell class="row-1 pims">38</cell>
                                            <cell class="row-1 shots">210</cell>
                                            <cell class="row-1 shot_pct">15.7</cell>
                                            <cell class="row-1 pp_goals">4</cell>
                                            <cell class="row-1 pp_assists">21</cell>
                                            <cell class="row-1 pp_points">25</cell>
                                            <cell class="row-1 sh_goals">0</cell>
                                            <cell class="row-1 sh_assists">0</cell>
                                            <cell class="row-1 sh_points">0</cell>
                                            <cell class="row-1 gw_goals">6</cell>
                                            <cell class="row-1 missed_shots">91</cell>
                                            <cell class="row-1 blocked_shots">19</cell>
                                            <cell class="row-1 shots_blocked">85</cell>
                                            <cell class="row-1 hits">37</cell>
                                            <cell class="row-1 times_hit">70</cell>
                                            <cell class="row-1 fo_wins">0</cell>
                                            <cell class="row-1 fo_loss">1</cell>
                                            <cell class="row-1 fo_pct">0.0</cell>
                                            <cell class="row-1 so_goals">0</cell>
                                            <cell class="row-1 so_shots">3</cell>
                                            <cell class="row-1 so_pct">0.0</cell>
                                            <cell class="row-1 giveaways">59</cell>
                                            <cell class="row-1 takeaways">30</cell>
                                            <cell class="row-1 pens_taken">19</cell>
                                            <cell class="row-1 pens_drawn">18</cell>
                                            <cell class="row-1 atoi">18:51</cell>
                                    </row>');
    }

    /**
     * A test of an individual skater's player gamelog tab.
     * @return void
     */
    public function testIndividualSkaterTabsGameLog(): void
    {
        $response = $this->getTab('/nhl/players/nikita-kucherov-2220/gamelog?season=2019%3Aregular&group=skater');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="true" '
            . 'data-skater-advanced="false" data-goalie="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game '
            . 'scrolltable-game-skater" data-default="0" data-total="32" data-current="start">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Tue 10th Mar</cell>
                <cell class="row-1 opp">
                    @<span class="team-nhl-TOR">TOR</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-loss">L 2 &ndash; 1</span>
                    <a class="icon_game_boxscore" href="/nhl/schedule/20200310/lightning-at-maple-leafs-r1070"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 start"><span class="fa fa-times-circle"></span></cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">1</cell>
                                            <cell class="row-1 points">1</cell>
                                            <cell class="row-1 plus_minus">+1</cell>
                                            <cell class="row-1 pims">0</cell>
                                            <cell class="row-1 shots">5</cell>
                                            <cell class="row-1 shot_pct">0.0</cell>
                                            <cell class="row-1 pp_goals">0</cell>
                                            <cell class="row-1 pp_assists">0</cell>
                                            <cell class="row-1 pp_points">0</cell>
                                            <cell class="row-1 sh_goals">0</cell>
                                            <cell class="row-1 sh_assists">0</cell>
                                            <cell class="row-1 sh_points">0</cell>
                                            <cell class="row-1 gw_goals">0</cell>
                                            <cell class="row-1 missed_shots">2</cell>
                                            <cell class="row-1 blocked_shots">0</cell>
                                            <cell class="row-1 shots_blocked">0</cell>
                                            <cell class="row-1 hits">0</cell>
                                            <cell class="row-1 times_hit">1</cell>
                                            <cell class="row-1 fo_wins">&ndash;</cell>
                                            <cell class="row-1 fo_loss">&ndash;</cell>
                                            <cell class="row-1 fo_pct">&ndash;</cell>
                                            <cell class="row-1 so_goals">0</cell>
                                            <cell class="row-1 so_shots">0</cell>
                                            <cell class="row-1 so_pct">&ndash;</cell>
                                            <cell class="row-1 giveaways">0</cell>
                                            <cell class="row-1 takeaways">1</cell>
                                            <cell class="row-1 pens_taken">0</cell>
                                            <cell class="row-1 pens_drawn">0</cell>
                                            <cell class="row-1 shifts">21</cell>
                                            <cell class="row-1 toi">21:26</cell>
                                    </row>');
    }

    /**
     * A test of an individual skater's player heatmap tab.
     * @return void
     */
    public function testIndividualSkaterTabsHeatmaps(): void
    {
        $response = $this->getTab('/nhl/players/nikita-kucherov-2220/heatmaps?season=2019%3Aregular'); // Default.
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="heatmaps" data-shots="true" '
            . 'data-goals="true" data-hits="true" data-penalties="true"></div>');
        $response->assertSee('<div class="heatmaps-charts">
    ' . '
    <ul class="inline_list heatmaps clearfix">
                    <li class="zone zcol-1 zrow-1"></li>
                    <li class="zone zcol-2 zrow-1"></li>
                    <li class="zone zcol-3 zrow-1"></li>
                    <li class="zone zcol-4 zrow-1"></li>
                    <li class="zone zcol-5 zrow-1"></li>
                    <li class="zone zcol-6 zrow-1"></li>
                    <li class="zone zcol-7 zrow-1"></li>
                    <li class="zone zcol-8 zrow-1"></li>
                    <li class="zone zcol-1 zrow-2"></li>
                    <li class="zone zcol-2 zrow-2"></li>
                    <li class="zone zcol-3 zrow-2"></li>
                    <li class="zone zcol-4 zrow-2"></li>
                    <li class="zone zcol-5 zrow-2"></li>
                    <li class="zone zcol-6 zrow-2"></li>
                    <li class="zone zcol-7 zrow-2"></li>
                    <li class="zone zcol-8 zrow-2"></li>
                    <li class="zone zcol-1 zrow-3"></li>');
        $response->assertSee('<li class="zone zcol-8 zrow-7"></li>
                <li class="rink"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list heatmaps clearfix">
                    <li class="zcell event-0 zcol-1"></li>
                    <li class="zcell event-1 zcol-2"></li>
                    <li class="zcell event-2 zcol-3"></li>
                    <li class="zcell event-3 zcol-4"></li>
                    <li class="zcell event-4 zcol-5"></li>
                    <li class="zcell event-5 zcol-6"></li>
                    <li class="zcell event-6 zcol-7"></li>
                    <li class="zcell event-7 zcol-8"></li>
                    <li class="zcell event-8 zcol-1"></li>
                    <li class="zcell event-9 zcol-2"></li>
                    <li class="zcell event-10 zcol-3"></li>
                    <li class="zcell event-11 zcol-4"></li>
                    <li class="zcell event-12 zcol-5"></li>
                    <li class="zcell event-13 zcol-6"></li>
                    <li class="zcell event-14 zcol-7"></li>
                    <li class="zcell event-15 zcol-8"></li>
                    <li class="zcell event-16 zcol-1"></li>');
        $response->assertSee('.heatmaps-charts .zcell.event-1 { background-color: #0000ee; }
                                                                    '
            . '.heatmaps-charts .zcell.event-5 { background-color: #0000ee; }');
    }

    /**
     * A test of the individual goalie's player page.
     * @return void
     */
    public function testIndividualGoalie(): void
    {
        $response = $this->get('/nhl/players/andrei-vasilevskiy-2368');
        $response->assertStatus(200);
        $response->assertSee('<h1 class="team-nhl-narrow_large-right-TB">Andrei Vasilevskiy'
            . '<span class="hidden-m jersey">#88</span><span class="hidden-m pos">G</span></h1>');

        // Header.
        $response->assertSee('<ul class="inline_list summary">
            <li class="row-head">2019/20 Regular Season:</li>
                            <li class="stat grid-1-3">
                    <dl class="nhl_box">
                        <dt class="row-head">Wins</dt>
                            <dd class="row-1 value">35</dd>
                            <dd class="row-0 rank">1st</dd>
                    </dl>
                </li>');
        $response->assertSee('<dl class="details">
                <dt class="height">Height:</dt>
            <dd class="height">6&#39; 3&quot;</dd>');
        $response->assertSee('<dt class="draft">Drafted:</dt>
                <dd class="draft">1st Round, 19th Overall in 2012 by <span class="team-nhl-TB">'
            . '<span class="hidden-m hidden-tp">Tampa Bay</span> Lightning</span></dd>
                            </dl>');

        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="career">Career Stats</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="gamelog">Game Log</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="heatmaps">Heatmaps</item>
                <sep class="right"></sep>
    </desk>');

        // Summary.
        $response->assertSee('<div class="subnav-home subnav-tab  clearfix">
                            <scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-goalie" data-default="0" data-total="24" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">52</cell>
                                            <cell class="row-1 starts">52</cell>
                                            <cell class="row-1 win">35</cell>
                                            <cell class="row-1 loss">14</cell>
                                            <cell class="row-1 ot_loss">3</cell>
                                            <cell class="row-1 so_loss">0</cell>
                                            <cell class="row-1 goals_against_avg">2.56</cell>
                                            <cell class="row-1 save_pct">.917</cell>
                                            <cell class="row-1 shutout">3</cell>
                                            <cell class="row-1 goals_against">133</cell>
                                            <cell class="row-1 shots_against">1605</cell>
                                            <cell class="row-1 saves">1472</cell>
                                            <cell class="row-1 minutes_played">3121:54</cell>
                                            <cell class="row-1 en_goals_against">0</cell>
                                            <cell class="row-1 so_goals_against">1</cell>
                                            <cell class="row-1 so_shots_against">8</cell>
                                            <cell class="row-1 so_saves">7</cell>
                                            <cell class="row-1 so_save_pct">87.5</cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">4</cell>
                                            <cell class="row-1 points">4</cell>
                                            <cell class="row-1 pims">4</cell>
                                            <cell class="row-1 pens_taken">2</cell>
                                            <cell class="row-1 pens_drawn">2</cell>
                                    </row>');
    }

    /**
     * A test of the individual goalie's player career tab.
     * @return void
     */
    public function testIndividualGoalieTabsCareer(): void
    {
        $response = $this->getTab('/nhl/players/andrei-vasilevskiy-2368/career?group=goalie');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="false" '
            . 'data-skater-advanced="false" data-goalie="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-goalie" data-default="0" data-total="24" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2018/19</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2018/19</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-0 team"><span class="team-nhl-TB">TB</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">52</cell>
                                            <cell class="row-1 starts">52</cell>
                                            <cell class="row-1 win">35</cell>
                                            <cell class="row-1 loss">14</cell>
                                            <cell class="row-1 ot_loss">3</cell>
                                            <cell class="row-1 so_loss">0</cell>
                                            <cell class="row-1 goals_against_avg">2.56</cell>
                                            <cell class="row-1 save_pct">.917</cell>
                                            <cell class="row-1 shutout">3</cell>
                                            <cell class="row-1 goals_against">133</cell>
                                            <cell class="row-1 shots_against">1605</cell>
                                            <cell class="row-1 saves">1472</cell>
                                            <cell class="row-1 minutes_played">3121:54</cell>
                                            <cell class="row-1 en_goals_against">0</cell>
                                            <cell class="row-1 so_goals_against">1</cell>
                                            <cell class="row-1 so_shots_against">8</cell>
                                            <cell class="row-1 so_saves">7</cell>
                                            <cell class="row-1 so_save_pct">87.5</cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">4</cell>
                                            <cell class="row-1 points">4</cell>
                                            <cell class="row-1 pims">4</cell>
                                            <cell class="row-1 pens_taken">2</cell>
                                            <cell class="row-1 pens_drawn">2</cell>
                                    </row>');
    }

    /**
     * A test of the individual goalie's player gamelog tab.
     * @return void
     */
    public function testIndividualGoalieTabsGameLog(): void
    {
        // Game Log.
        $response = $this->getTab('/nhl/players/andrei-vasilevskiy-2368/gamelog?season=2019%3Aregular&group=goalie');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="false" '
            . 'data-skater-advanced="false" data-goalie="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game '
            . 'scrolltable-game-goalie" data-default="0" data-total="20" data-current="decision">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Tue 10th Mar</cell>
                <cell class="row-1 opp">
                    @<span class="team-nhl-TOR">TOR</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-loss">L 2 &ndash; 1</span>
                    <a class="icon_game_boxscore" href="/nhl/schedule/20200310/lightning-at-maple-leafs-r1070"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 decision"><span class="loss">Loss</span></cell>
                                            <cell class="row-1 start"><span class="fa fa-check-circle"></span></cell>
                                            <cell class="row-1 goals_against_avg">2.04</cell>
                                            <cell class="row-1 save_pct">.944</cell>
                                            <cell class="row-1 shutout">0</cell>
                                            <cell class="row-1 goals_against">2</cell>
                                            <cell class="row-1 shots_against">36</cell>
                                            <cell class="row-1 saves">34</cell>
                                            <cell class="row-1 minutes_played">58:42</cell>
                                            <cell class="row-1 en_goals_against">&ndash;</cell>
                                            <cell class="row-1 so_goals_against">0</cell>
                                            <cell class="row-1 so_shots_against">0</cell>
                                            <cell class="row-1 so_saves">0</cell>
                                            <cell class="row-1 so_save_pct">&ndash;</cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">0</cell>
                                            <cell class="row-1 points">0</cell>
                                            <cell class="row-1 pims">0</cell>
                                            <cell class="row-1 pens_taken">0</cell>
                                            <cell class="row-1 pens_drawn">0</cell>
                                    </row>');
    }

    /**
     * A test of the individual goalie's player heatmap tab.
     * @return void
     */
    public function testIndividualGoalieTabsHeatmaps(): void
    {
        $response = $this->getTab('/nhl/players/andrei-vasilevskiy-2368/heatmaps?season=2019%3Aregular&heatmap=shots');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="heatmaps" data-shots="true" '
            . 'data-saves="true" data-goals="true"></div>');
        $response->assertSee('<div class="heatmaps-charts">
    ' . '
    <ul class="inline_list heatmaps clearfix">
                    <li class="zone zcol-1 zrow-1"></li>
                    <li class="zone zcol-2 zrow-1"></li>
                    <li class="zone zcol-3 zrow-1"></li>
                    <li class="zone zcol-4 zrow-1"></li>
                    <li class="zone zcol-5 zrow-1"></li>
                    <li class="zone zcol-6 zrow-1"></li>
                    <li class="zone zcol-7 zrow-1"></li>
                    <li class="zone zcol-8 zrow-1"></li>
                    <li class="zone zcol-1 zrow-2"></li>
                    <li class="zone zcol-2 zrow-2"></li>
                    <li class="zone zcol-3 zrow-2"></li>
                    <li class="zone zcol-4 zrow-2"></li>
                    <li class="zone zcol-5 zrow-2"></li>
                    <li class="zone zcol-6 zrow-2"></li>
                    <li class="zone zcol-7 zrow-2"></li>
                    <li class="zone zcol-8 zrow-2"></li>
                    <li class="zone zcol-1 zrow-3"></li>');
        $response->assertSee('<li class="zone zcol-8 zrow-7"></li>
                <li class="rink"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list heatmaps clearfix">
                    <li class="zcell event-0 zcol-1"></li>
                    <li class="zcell event-1 zcol-2"></li>
                    <li class="zcell event-2 zcol-3"></li>
                    <li class="zcell event-3 zcol-4"></li>
                    <li class="zcell event-4 zcol-5"></li>
                    <li class="zcell event-5 zcol-6"></li>
                    <li class="zcell event-6 zcol-7"></li>
                    <li class="zcell event-7 zcol-8"></li>
                    <li class="zcell event-8 zcol-1"></li>
                    <li class="zcell event-9 zcol-2"></li>
                    <li class="zcell event-10 zcol-3"></li>
                    <li class="zcell event-11 zcol-4"></li>
                    <li class="zcell event-12 zcol-5"></li>
                    <li class="zcell event-13 zcol-6"></li>
                    <li class="zcell event-14 zcol-7"></li>
                    <li class="zcell event-15 zcol-8"></li>
                    <li class="zcell event-16 zcol-1"></li>');
        $response->assertSee('.heatmaps-charts .zcell.event-4 { background-color: #840000; }
                                                                                '
            . '.heatmaps-charts .zcell.event-9 { background-color: #840000; }');
    }
}
