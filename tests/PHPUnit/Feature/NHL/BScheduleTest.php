<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class BScheduleTest extends MajorLeagueBaseCase
{
    /**
     * A test of the current date (though deterministically "current")
     * @return void
     */
    public function testCurrent(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-08-31 12:34:56');

        $response = $this->get('/nhl/schedule');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/nhl/schedule/20200831" data-date="20200831">
        <span class="date hidden-m">Monday 31st August</span>
        <span class="date hidden-t hidden-d">Mon 31st Aug</span>
        <calendar');

        $response->assertSee('<ul class="games grid inline_list">
            <li class="game-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <ul class="inline_list nhl_box game result">
    ' . '
    <li class="status head">
        <strong>Final/2OT</strong>
            </li>
            <li class="title">Eastern Conference Semi Final, Game 5</li>
        ' . '
    <li class="team visitor loss">
        <span class="score">2</span>
        <a href="/nhl/teams/boston-bruins" class="team-nhl-narrow_small-BOS">Bruins</a>
            </li>
    <li class="team home win">
        <span class="score">3</span>
        <a href="/nhl/teams/tampa-bay-lightning" class="team-nhl-narrow_small-TB">Lightning</a>
            </li>');
        $response->assertSee('<dl class="periods clearfix">
    ' . '
            <dt class="period head period-1">1</dt>
            <dt class="period head period-2">2</dt>
            <dt class="period head period-3">3</dt>
            <dt class="period head period-2OT">2OT</dt>
        <dt class="total head">Tot</dt>');
        $response->assertSee('<li class="playoff">
            Lightning win the series, 4&ndash;1
        </li>');
        $response->assertSee('<li class="venue head">Scotiabank Arena</li>');
        $response->assertSee('<dl class="three_stars">
            <dt>Three Stars</dt>
                                            <dd class="icon icon_game_star1">
                    <span class="icon team-nhl-TB"><a href="/nhl/players/victor-hedman-297">V. Hedman</a></span>
                </dd>');
        $response->assertSee('<dl class="goalies">
                            <dt>Winning Goalie:</dt>
                <dd class="icon_right team-nhl-right-TB">
                    <a href="/nhl/players/andrei-vasilevskiy-2368">A. Vasilevskiy</a></dd>
                            <dt>Losing Goalie:</dt>
                <dd class="icon_right team-nhl-right-BOS">
                    <a href="/nhl/players/jaroslav-halak-278">J. Halak</a></dd>
            </dl>');
        $response->assertSee('<li class="links">
        <ul class="inline_list">
                            <li>
                    <a class="icon_game_stats" href="/nhl/schedule/20200831/bruins-at-lightning-p225#stats">'
            . '<em>Game Stats</em></a>
                </li>
                            <li>
                    <a class="icon_game_boxscore" href="/nhl/schedule/20200831/bruins-at-lightning-p225">'
            . '<em>Boxscore</em></a>
                </li>
                            <li>
                    <a class="icon_game_scoring" href="/nhl/schedule/20200831/bruins-at-lightning-p225#scoring">'
            . '<em>Scoring</em></a>
                </li>
                            <li>
                    <a class="icon_game_locations" href="/nhl/schedule/20200831/bruins-at-lightning-p225#events">'
            . '<em>Game Events</em></a>
                </li>
                            <li>
                    <a class="icon_game_series" href="/nhl/schedule/20200831/bruins-at-lightning-p225#series">'
            . '<em>Playoff Series</em></a>
                </li>
                    </ul>
    </li>');
    }

    /**
     * A test of an indirect match
     * @return void
     */
    public function testIndirect(): void
    {
        $response = $this->get('/nhl/schedule/20200125');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/nhl/schedule/20200127" data-date="20200127">
        <span class="date hidden-m">Monday 27th January</span>
        <span class="date hidden-t hidden-d">Mon 27th Jan</span>
        <calendar');
    }

    /**
     * A test of the regular season results
     * @return void
     */
    public function testRegular(): void
    {
        $response = $this->get('/nhl/schedule/20200310');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/nhl/schedule/20200310" data-date="20200310">
        <span class="date hidden-m">Tuesday 10th March</span>
        <span class="date hidden-t hidden-d">Tue 10th Mar</span>
        <calendar');

        $response->assertSee('<dl class="periods clearfix">
    ' . '
            <dt class="period head period-1">1</dt>
            <dt class="period head period-2">2</dt>
            <dt class="period head period-3">3</dt>
            <dt class="period head period-OT">OT</dt>
            <dt class="period head period-SO">SO</dt>
        <dt class="total head">Tot</dt>');
        $response->assertSee('<dd class="team team-nhl-NYI"></dd>
            <dd class="period period-1 row_0">
                            ' . '
                2
                    </dd>
            <dd class="period period-2 row_0">
                            ' . '
                1
                    </dd>
            <dd class="period period-3 row_0">
                            ' . '
                1
                    </dd>
            <dd class="period period-OT row_0">
                            ' . '
                0
                    </dd>
            <dd class="period period-SO row_0">
                            ' . '
                0
                <span>(0)</span>
                    </dd>
        <dt class="total head">4</dt>');
        $response->assertSee('<dd class="team team-nhl-VAN"></dd>
            <dd class="period period-1 row_0">
                            ' . '
                2
                    </dd>
            <dd class="period period-2 row_0">
                            ' . '
                2
                    </dd>
            <dd class="period period-3 row_0">
                            ' . '
                0
                    </dd>
            <dd class="period period-OT row_0">
                            ' . '
                0
                    </dd>
            <dd class="period period-SO row_0">
                            ' . '
                1
                <span>(1)</span>
                    </dd>
        <dt class="total head">5</dt>');
    }

    /**
     * A test of dates with game previews
     * @return void
     */
    public function testPreview(): void
    {
        $response = $this->get('/nhl/schedule/20200901');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/nhl/schedule/20200901" data-date="20200901">
        <span class="date hidden-m">Tuesday 1st September</span>
        <span class="date hidden-t hidden-d">Tue 1st Sep</span>
        <calendar');

        $response->assertSee('<ul class="inline_list nhl_box game preview">
    <li class="status head">4:00pm PDT</li>
            <li class="title">Eastern Conference Semi Final, Game 5</li>
        <li class="team visitor">
        <a href="/nhl/teams/new-york-islanders" class="team-nhl-narrow_small-NYI">Islanders</a>
            </li>
    <li class="team home">
        <a href="/nhl/teams/philadelphia-flyers" class="team-nhl-narrow_small-PHI">Flyers</a>
            </li>
    ' . '
            <li class="playoff">
            Islanders lead the series, 3&ndash;2
        </li>
        ' . '
        ' . '
    <li class="odds line-with-icon">
    <dl>
        <dt>Odds:</dt>
        ' . '
                    <dd class="moneyline icon team-nhl-NYI">104</dd>
            <dd class="moneyline icon team-nhl-PHI">-125</dd>
        ' . '
        ' . '
                    <dt class="over-under">Spread:</dt>                <dd class="spread icon team-nhl-NYI">-1.5</dd>
        ' . '
        ' . '
                    <dt class="over-under">O/U:</dt>
                <dd class="over-under">5</dd>
            </dl>
</li>
    ' . '
    <li class="venue head">Scotiabank Arena</li>
    ' . '
    </ul>'); // Series is 3-2 NYI due to the Playoffs test containing later data - should really be 3-1.
    }

    /**
     * A test of dates with cancelled games
     * @return void
     */
    public function testCancelled(): void
    {
        $response = $this->get('/nhl/schedule/20200404');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/nhl/schedule/20200404" data-date="20200404">
        <span class="date hidden-m">Saturday 4th April</span>
        <span class="date hidden-t hidden-d">Sat 4th Apr</span>
        <calendar');

        $response->assertSee('<ul class="inline_list nhl_box game incomplete">
    <li class="status head"><strong>Cancelled</strong></li>
        <li class="team visitor">
        <a href="/nhl/teams/tampa-bay-lightning" class="team-nhl-narrow_small-TB">Lightning</a>
                    <span class="standings">(43&ndash;21&ndash;6)</span>
            </li>
    <li class="team home">
        <a href="/nhl/teams/detroit-red-wings" class="team-nhl-narrow_small-DET">Red Wings</a>
                    <span class="standings">(17&ndash;49&ndash;5)</span>
            </li>
    <li class="venue head">Little Caesars Arena</li>
</ul>');
    }

    /**
     * A test of miscellaneous controller actions
     * @return void
     */
    public function testControllerMisc(): void
    {
        // From the season switcher.
        $response = $this->get('/nhl/schedule/2019/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nhl/schedule/20200905/switcher');

        // Calendar widget exceptions.
        $response = $this->get('/nhl/schedule/2019/calendar');
        $response->assertStatus(200);
        $response->assertSee('{"exceptions":{"2019-12-24":true,');
        $response->assertSee(',"2019-12-26":true,');
        $response->assertDontSee(',"2019-12-27":true,');
    }

    /**
     * A test of ordered games based on user favourites
     * @return void
     */
    public function testScheduleFavourites(): void
    {
        // A control, the "regular" order.
        $response = $this->get('/nhl/schedule/20200831');
        $response->assertStatus(200);
        $response->assertDontSee('<h3>My Teams</h3>');
        $response->assertSeeInOrder([
            '<li class="title">Eastern Conference Semi Final, Game 5</li>', // BOS @ TB.
            '<li class="title">Western Conference Semi Final, Game 5</li>', // DAL @ COL.
        ]);

        // Log in as a user with favourites.
        $response = $this->post('/login', [
            'username' => 'sports_admin',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Load the schedule and ensure the games are ordered correctly.
        $response = $this->get('/nhl/schedule/20200831');
        $response->assertStatus(200);
        $response->assertSeeInOrder(['<h3>My Teams</h3>', '<h3>Other Teams</h3>']);
        $response->assertSeeInOrder([
            '<li class="title">Eastern Conference Semi Final, Game 5</li>', // BOS @ TB.
            '<li class="title">Western Conference Semi Final, Game 5</li>', // DAL @ COL.
        ]);

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
