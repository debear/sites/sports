<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class CGameTest extends MajorLeagueBaseCase
{
    /**
     * A test of a single game
     * @return void
     */
    public function testGame(): void
    {
        $response = $this->get('/nhl/schedule/20200831/bruins-at-lightning-p225');
        $response->assertStatus(200);

        $response->assertSee('<ul class="inline_list nhl_box game summary">
    <li class="row-head info">
        <span class="status">Final/2OT</span>
        <span class="venue">Scotiabank Arena</span>
    </li>
    <li class="title">
        Eastern Conference Semi Final, Game 5, Monday 31st August
    </li>');
        $response->assertSee('<li class="summary">Lightning win the series, 4&ndash;1</li>');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="unsel" data-tab="series"><span class="icon_game_series">Playoff Series</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="events"><span class="icon_game_locations">Game Events</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="scoring"><span class="icon_game_scoring">Scoring</span></item>
                    <sep class="mid"></sep>
            <item class="sel" data-tab="boxscore"><span class="icon_game_boxscore">Boxscore</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="stats"><span class="icon_game_stats">Game Stats</span></item>
                <sep class="right"></sep>
    </desk>');
        $response->assertDontSee('<item class="unsel" data-tab="playbyplay"><span class="icon_game_pbp">'
            . 'Play-by-Play</span></item>');

        // The individual tabs.
        $this->testGameEvents($response);
        $this->testGameScoring($response);
        $this->testGameBoxscore($response);
        $this->testGameStats($response);
    }

    /**
     * Test the game's event tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameEvents(TestResponse $response): void
    {
        $response->assertSee('<div class="centre faceoff">
            <div class="team-nhl-medium-TB"></div>
        </div>');
        $response->assertSee('<li class="event far fa-lightbulb text_nhl-TB" data-id="1" data-period="2" '
            . 'data-type="GOAL" data-team_id="TB"');
        $response->assertSee('<li class="event fa fa-gavel text_nhl-TB" data-id="265" data-period="4" '
            . 'data-type="HIT" data-team_id="TB"');
        $response->assertSee('.events .event[data-id="1"] { left: calc(80.50000% - 9px); '
            . 'top: calc(47.61905% - 9px); }');
        $response->assertSee('.events .event[data-id="265"] { left: calc(99.00000% - 9px); '
            . 'top: calc(36.90476% - 9px); }');

        $response->assertSee('<dt class="row-0 type">By Type:</dt>
        <dd class="row-1">
            <div>
                <input type="checkbox" id="filter_type_goal" class="event_filter checkbox" checked="checked" '
            . 'data-evtype="type" data-evvalue="GOAL">
                <label for="filter_type_goal"><span class="far fa-lightbulb"></span>Goal</label>
            </div>
                        <div>
                <input type="checkbox" id="filter_type_shot" class="event_filter checkbox" checked="checked" '
            . 'data-evtype="type" data-evvalue="SHOT">
                <label for="filter_type_shot"><span class="fa fa-map-marker"></span>Shot</label>
            </div>
            <div>
                <input type="checkbox" id="filter_type_penalty" class="event_filter checkbox" checked="checked" '
            . 'data-evtype="type" data-evvalue="PENALTY">
                <label for="filter_type_penalty"><span class="far fa-dizzy"></span>Penalty</label>
            </div>
            <div>
                <input type="checkbox" id="filter_type_hit" class="event_filter checkbox" checked="checked" '
            . 'data-evtype="type" data-evvalue="HIT">
                <label for="filter_type_hit"><span class="fa fa-gavel"></span>Hit</label>
            </div>
        </dd>');
    }

    /**
     * Test the game's scoring tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameScoring(TestResponse $response): void
    {
        $response->assertSee('<dt class="row-head type">Goals</dt>
                    <dt class="row-head period">1st Period</dt>
                    <dd class="row-0 none"><em>No Scoring</em></dd>
                            <dt class="row-head period">2nd Period</dt>
                                <dd class="row-0 team-nhl-TB time">
                                    15:39
                            </dd>
            <dd class="row-0 play">
                <div class="score"><span class="visitor team-nhl-BOS">0</span> &ndash; '
            . '<span class="home team-nhl-right-TB">1</span></div>
                Goal, scored by <a href="/nhl/players/ondrej-palat-2106">Ondrej Palat</a> assisted by '
            . '<a href="/nhl/players/kevin-shattenkirk-1480">Kevin Shattenkirk</a> and '
            . '<a href="/nhl/players/blake-coleman-2679">Blake Coleman</a> (28ft Tip-In)
            </dd>');

        $response->assertSee('<dt class="row-head period">2nd Overtime</dt>
                    <dd class="row-0 none"><em>No Penalties</em></dd>');
    }

    /**
     * Test the game's boxscore tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameBoxscore(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list nhl_box boxscore clearfix">
        <li class="row_nhl-BOS team"><span class="team-nhl-BOS icon">Boston Bruins</span></li>

        ' . '
                <li class="cell section skater row_nhl-BOS">Skaters</li>
        <li class="static skater">
            <ul class="inline_list">
                <li class="cell jersey row_nhl-BOS">#</li>
                <li class="cell cell-head player row_nhl-BOS">Player</li>

                                                        <li class="cell jersey row_nhl-BOS">73</li>
                    <li class="cell name row-0"><a href="/nhl/players/charlie-mcavoy-2739"><span class="name-short">'
            . 'C. McAvoy</span><span class="name-full">Charlie McAvoy</span></a></li>');
        $response->assertSee('<li class="scroll skater">
            <ul class="inline_list">
                <li class="cell cell-head cell-first pos row_nhl-BOS">Pos</li>
                <li class="cell cell-head stat row_nhl-BOS">G</li>
                <li class="cell cell-head stat row_nhl-BOS">A</li>
                <li class="cell cell-head stat row_nhl-BOS">+/-</li>
                <li class="cell cell-head stat row_nhl-BOS">PIM</li>
                <li class="cell cell-head stat row_nhl-BOS">SOG</li>
                <li class="cell cell-head stat row_nhl-BOS">Hits</li>
                <li class="cell cell-head stat row_nhl-BOS">BS</li>
                <li class="cell cell-head stat_wide row_nhl-BOS">FO</li>
                <li class="cell cell-head stat_wide row_nhl-BOS">TOI</li>

                                                        <li class="cell cell-first pos row-0">D</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">2</li>
                    <li class="cell stat row-0">3</li>
                    <li class="cell stat row-0">2</li>
                    <li class="cell stat_wide row-0">&mdash;</li>
                    <li class="cell stat_wide row-0">36:39</li>');
        $response->assertSee('<li class="cell section goalie row_nhl-TB">Goaltenders</li>
        <li class="static goalie">
            <ul class="inline_list">
                <li class="cell jersey row_nhl-TB">#</li>
                <li class="cell cell-head player row_nhl-TB">Player</li>

                                                        <li class="cell jersey row_nhl-TB">88</li>
                    <li class="cell name row-0"><a href="/nhl/players/andrei-vasilevskiy-2368">'
            . '<span class="name-short">A. Vasilevskiy</span><span class="name-full">Andrei Vasilevskiy</span></a></li>
                            </ul>
        </li>
        <li class="scroll goalie">
            <ul class="inline_list">
                <li class="cell cell-head cell-first decision row_nhl-TB">Decision</li>
                <li class="cell cell-head stat_wide row_nhl-TB">TOI</li>
                <li class="cell cell-head stat row_nhl-TB">SA</li>
                <li class="cell cell-head stat row_nhl-TB">GA</li>
                <li class="cell cell-head stat row_nhl-TB">SV</li>
                <li class="cell cell-head stat_wide row_nhl-TB">SV%</li>
                <li class="cell cell-head stat_wide row_nhl-TB">GAA</li>
                <li class="cell cell-head stat row_nhl-TB">PIM</li>
                <li class="cell cell-head stat row_nhl-TB">Pts</li>

                                                        <li class="cell cell-first decision win">Win</li>
                    <li class="cell stat_wide row-0">94:10</li>
                    <li class="cell stat row-0">47</li>
                    <li class="cell stat row-0">2</li>
                    <li class="cell stat row-0">45</li>
                    <li class="cell stat_wide row-0">.957</li>
                    <li class="cell stat_wide row-0">1.27</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                            </ul>
        </li>
    </ul>');
        // Three Stars.
        $response->assertSee('<ul class="inline_list nhl_box three_stars clearfix">
    <li class="row-head">Three Stars</li>
            <li class="sel">Selected by NHL.</li>
                        <li class="star icon icon_game_star1">
            <div class="row-0 name icon team-nhl-TB"><a href="/nhl/players/victor-hedman-297">'
            . '<span class="hidden-m">Victor Hedman</span><span class="hidden-t hidden-d">V. Hedman</span></a></div>
            <img class="mugshot-large" loading="lazy"');
        // Officials.
        $response->assertSee('<ul class="inline_list nhl_box officials clearfix">
    <li class="row-head">Officials</li>

    <li class="row-0 referee">Referees:</li>
    <li class="row-0 linesman">Linesmen:</li>

    <li class="row-1 referee">
                    <p>#15 Jean Hebert</p>
                    <p>#19 Gord Dwyer</p>
            </li>
    <li class="row-1 linesman">
                    <p>#75 Derek Amell</p>
                    <p>#76 Michel Cormier</p>
            </li>
</ul>');
    }

    /**
     * Test the game's stats tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameStats(TestResponse $response): void
    {
        $response->assertSee('<dl class="nhl_box comparison clearfix">
    <dt class="row-head">Team Stats</dt>
    <dt class="team visitor row_nhl-BOS team-nhl-BOS"><span class="hidden-m hidden-tp">Boston</span> Bruins</dt>
    <dt class="team home row_nhl-TB team-nhl-TB"><span class="hidden-m hidden-tp">Tampa Bay</span> Lightning</dt>');
        $response->assertSee('<dt class="row-0 section">Shots on Goal</dt>
        ' . '
                    <dd class="row-0 label">On Goal</dd>
<dd class="row-1 stat stat-shots-on-goal-on-goal">
    <span class="visitor">47</span>
    <div class="bar visitor row_nhl-BOS"></div>
    <div class="bars">
        <div class="visitor row_nhl-BOS"></div>
        <div class="home row_nhl-TB"></div>
        <div class="gap row_nhl-TB"></div>
    </div>
    <div class="bar home row_nhl-TB"></div>
    <span class="home">35</span>
</dd>');
        $response->assertSee('.subnav-stats .stat-shots-on-goal-on-goal .bars .visitor { width: 57.32%; }
        .subnav-stats .stat-shots-on-goal-on-goal .bars .home { width: 42.68%; }');
    }

    /**
     * A test of controller actions on invalid parameters
     * @return void
     */
    public function testInvalid(): void
    {
        // Invalid date.
        $response = $this->get('/nhl/schedule/20200230/away-at-home-r1');
        $response->assertStatus(404);

        // Invalid game.
        $response = $this->get('/nhl/schedule/20200229/away-at-home-r1');
        $response->assertStatus(404);
    }
}
