<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class AHomeTest extends MajorLeagueBaseCase
{
    /**
     * A test of the homepage and game header chrome.
     * @return void
     */
    public function testHomepage(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-08-31 12:34:56');

        $response = $this->get('/nhl');
        $response->assertStatus(200);

        // Check the game headers.
        $response->assertSee('<div class="schedule_header">
    <dl class="num-2">
        <dt class="row-head date">31st Aug</dt>
        <dd>
    <div class="team visitor row-1 icon team-nhl-BOS loss">
        BOS
                    <span class="score">2</span>
            </div>
    <div class="team home row-0 icon team-nhl-TB win">
        TB
                    <span class="score">3</span>
            </div>
    <div class="info row-1">
                    ' . '
            <div class="link"><a href="/nhl/schedule/20200831/bruins-at-lightning-p225" '
            . 'class="icon_right_game_boxscore no_underline">Boxscore</a></div>
            <div class="status">Final/2OT</div>
            </div>
</dd>');

        // News.
        $response->assertSee('<ul class="inline_list grid home">
    <li class="grid-cell grid-3-4 grid-t-1-1 grid-m-1-1">
        ' . '
        <ul class="inline_list grid">
            <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <article class="nhl_box">
            <h4><a href="https://sports.yahoo.com/nhl-suspends-2019-20-season-184917336.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">NHL suspends 2019-20 season '
            . 'indefinitely</a></h4>
            <publisher>Yahoo! Sports</publisher>
            <time>12th March 2020, 3:49pm</time>');

        // Playoff Bracket.
        $response->assertDontSee('<h3 class="row-head">Standings</h3>');
        $response->assertSee('<fieldset class="nhl_box">
    <h3 class="row-head">Playoffs</h3>
    <dl class="bracket toggle-target grid clearfix">
                                    <dt class="tab grid-cell grid-1-4 row-0 unsel" data-id="1" title="Division '
            . 'Semi Final">DSF</dt>
                            <dt class="tab grid-cell grid-1-4 row-0 unsel" data-id="2" title="Division Final">DF</dt>
                            <dt class="tab grid-cell grid-1-4 row-0 unsel" data-id="3" title="Conference Final">CF</dt>
                            <dt class="tab grid-cell grid-1-4 row-head sel" data-id="4" title="Stanley Cup '
            . 'Final">SCF</dt>
                                                    <dd class="half grid-1-1 tab-1 hidden">
                <ul class="inline_list grid">
                                                                    <li class="grid-cell grid-1-2 series-l">
                                                                                            <div class="higher  row-0">
                                    <span class="team team-nhl-narrow_small-PIT">'
            . '<a href="/nhl/teams/pittsburgh-penguins">PIT</a></span>
                                    <span class="score">1</span>
                                </div>
                                <div class="lower  row-1">
                                    <span class="team team-nhl-narrow_small-MTL">'
            . '<a href="/nhl/teams/montreal-canadiens">MTL</a></span>
                                    <span class="score">3</span>
                                </div>');

        // Leaders.
        $response->assertSee('<fieldset class="nhl_box">
    <h3 class="row-head">Leaders</h3>
    <dl class="leaders clearfix">');
        $response->assertSee('<dt class="row-head">Points</dt>
            <dd class="name"><span class="icon team-nhl-EDM">'
            . '<a href="/nhl/players/leon-draisaitl-2322">L. Draisaitl</a></span></dd>
            <dd class="value">110</dd>');

        // Power Ranks.
        $response->assertSee('<fieldset class="nhl_box">
    <h3 class="row-head">Latest Power Ranks</h3>
    <dl class="power_ranks clearfix">
        ' . '
                    <dd class="dates">Games up to 8th March</dd>
                            <dt class="row-head">1</dt>
                <dd class="team row-1 team-nhl-narrow_small-PHI">'
            . '<a href="/nhl/teams/philadelphia-flyers">Flyers</a></dd>');
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/nhl/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear Sports NHL',
            'short_name' => 'DeBear Sports NHL',
            'description' => 'All the latest National Hockey League results, stats and news.',
            'start_url' => '/nhl/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/sports/images/meta/nhl.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
