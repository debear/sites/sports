<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class GStatsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the stat leaders page.
     * @return void
     */
    public function testLeaders(): void
    {
        $response = $this->get('/nhl/stats');
        $response->assertStatus(200);
        $response->assertSee('<h1>2019/20 NHL Stat Leaders</h1>');

        // Points.
        $response->assertSee('<dl class="nhl_box stat clearfix">
                <dt class="row-head name">Points</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nhl-EDM">'
            . '<a href="/nhl/players/leon-draisaitl-2322">Leon Draisaitl</a></span>
                            <span class="stat">110</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nhl-COL">'
            . '<a href="/nhl/players/nathan-mackinnon-2190">Nathan MacKinnon</a></span>
                            <span class="stat">93</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-nhl-BOS">'
            . '<a href="/nhl/players/brad-marchand-464">Brad Marchand</a></span>
                            <span class="stat">87</span>
                        </dd>');

        // Advanced Skater stats.
        $response->assertSee('<dl class="nhl_box stat clearfix">
                <dt class="row-head name">Relative Corsi</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nhl-BUF">'
            . '<a href="/nhl/players/dalton-smith-3167">Dalton Smith</a></span>
                            <span class="stat">48.5</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nhl-CLB">'
            . '<a href="/nhl/players/liam-foudy-3181">Liam Foudy</a></span>
                            <span class="stat">16.9</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-nhl-STL">'
            . '<a href="/nhl/players/jake-walman-2836">Jake Walman</a></span>
                            <span class="stat">16.3</span>
                        </dd>');

        // Goalie Formatting.
        $response->assertSee('<span class="icon team-nhl-ANA">'
            . '<a href="/nhl/players/anthony-stolarz-2406">Anthony Stolarz</a></span>
                            <span class="stat">2.05</span>');
        $response->assertSee('<span class="icon team-nhl-ANA">'
            . '<a href="/nhl/players/anthony-stolarz-2406">Anthony Stolarz</a></span>
                            <span class="stat">.943</span>');

        // Teams: Power Play.
        $response->assertSee('<dl class="nhl_box stat clearfix">
                <dt class="row-head name">Team Power Play</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nhl-EDM">'
            . '<a href="/nhl/teams/edmonton-oilers">Oilers</a></span>
                            <span class="stat">29.5</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-nhl-STL">'
            . '<a href="/nhl/teams/st-louis-blues">Blues</a></span>
                            <span class="stat">23.6</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-nhl-TB">'
            . '<a href="/nhl/teams/tampa-bay-lightning">Lightning</a></span>
                            <span class="stat">23.1</span>
                        </dd>');

        // Team lists.
        $response->assertSee('<dl class="nhl_box teams clearfix">
    <dt class="row-head">Sortable Stats by Team</dt>
            <dd ><a class="no_underline team-nhl-ANA" href="/nhl/stats/players/skater?team_id=ANA&season=2019&'
            . 'type=regular&stat=points"></a></dd>');
        $response->assertSee('<dd class="split-point"><a class="no_underline team-nhl-NSH" '
            . 'href="/nhl/stats/players/skater?team_id=NSH&season=2019&type=regular&stat=points"></a></dd>');
    }

    /**
     * A test of the controller handling.
     * @return void
     */
    public function testHandler(): void
    {
        // Invalid option.
        $response = $this->get('/nhl/stats/players/unknown');
        $response->assertStatus(404);

        // Default options.
        $response = $this->get('/nhl/stats/players/skater');
        $response->assertStatus(200);
        $response->assertSee('<h1>NHL Skater Leaders</h1>');

        // Filters.
        $response->assertSee('<dl class="dropdown " data-id="filter_team"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>All NHL</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="">All NHL</li>
                            <li  data-id="ANA"><span class="icon team-nhl-ANA">ANA</span></li>');
        $response->assertSee('<dl class="dropdown " data-id="filter_season"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019/20 Regular Season</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2019::regular">2019/20 Regular Season</li>
                    </ul>
    </dd>
</dl>');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::skater-advanced">Individual Advanced Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::skater-advanced">Team Advanced Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        // Stat selection.
        $response->assertSee('<scrolltable class="scroll-x scrolltable-player-skater" data-default="3" data-total="30"'
            . ' data-current="points">');
    }

    /**
     * A test of the individual skaters stats page.
     * @return void
     */
    public function testPlayerSkaters(): void
    {
        $response = $this->get('/nhl/stats/players/skater?season=2019&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<h1>NHL Skater Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::skater-advanced">Individual Advanced Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::skater-advanced">Team Advanced Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-EDM">'
            . '<a href="/nhl/players/leon-draisaitl-2322">L. Draisaitl</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-EDM">'
            . '<a href="/nhl/players/connor-mcdavid-2455">C. McDavid</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-BOS">'
            . '<a href="/nhl/players/david-pastrnak-2356">D. Pastrnak</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-NYR">'
            . '<a href="/nhl/players/artemi-panarin-2451">A. Panarin</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-COL">'
            . '<a href="/nhl/players/nathan-mackinnon-2190">N. MacKinnon</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual advanced skater stats page.
     * @return void
     */
    public function testPlayerAdvancedSkaters(): void
    {
        $response = $this->get('/nhl/stats/players/skater-advanced?season=2019&type=regular&stat=corsi_rel');
        $response->assertStatus(200);
        $response->assertSee('<h1>NHL Advanced Skater Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Advanced Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li class="sel" data-id="player::skater-advanced">Individual Advanced Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::skater-advanced">Team Advanced Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top class="stats-grouped">
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-BUF">'
            . '<a href="/nhl/players/dalton-smith-3167">D. Smith</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-STL">'
            . '<a href="/nhl/players/derrick-pouliot-2370">D. Pouliot</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-NSH">'
            . '<a href="/nhl/players/alexandre-carrier-2678">A. Carrier</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-MIN">'
            . '<a href="/nhl/players/brennan-menell-2898">B. Menell</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-CLB">'
            . '<a href="/nhl/players/liam-foudy-3181">L. Foudy</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual goalies stats page.
     * @return void
     */
    public function testPlayerGoalies(): void
    {
        $response = $this->get('/nhl/stats/players/goalie?season=2019&type=regular&stat=goals_against_avg');
        $response->assertStatus(200);
        $response->assertSee('<h1>NHL Goalie Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Goalie</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::skater-advanced">Individual Advanced Skater</li>
                            <li class="sel" data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::skater-advanced">Team Advanced Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-ANA">'
            . '<a href="/nhl/players/anthony-stolarz-2406">A. Stolarz</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-FLA">'
            . '<a href="/nhl/players/chris-driedger-2396">C. Driedger</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-BOS">'
            . '<a href="/nhl/players/tuukka-rask-620">T. Rask</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-STL">'
            . '<a href="/nhl/players/jake-allen-1790">J. Allen</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-DAL">'
            . '<a href="/nhl/players/anton-khudobin-1259">A. Khudobin</a></span></cell>
            </row>');
    }

    /**
     * A test of the team skaters stats page.
     * @return void
     */
    public function testTeamSkaters(): void
    {
        $response = $this->get('/nhl/stats/teams/skater?season=2019&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<h1>NHL Skater Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::skater-advanced">Individual Advanced Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li class="sel" data-id="team::skater">Team Skater</li>
                            <li  data-id="team::skater-advanced">Team Advanced Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-TB">'
            . '<a href="/nhl/teams/tampa-bay-lightning">Lightning</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-COL">'
            . '<a href="/nhl/teams/colorado-avalanche">Avalanche</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-NYR">'
            . '<a href="/nhl/teams/new-york-rangers">Rangers</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-TOR">'
            . '<a href="/nhl/teams/toronto-maple-leafs">Maple Leafs</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-FLA">'
            . '<a href="/nhl/teams/florida-panthers">Panthers</a></span></cell>
            </row>');
    }

    /**
     * A test of the team advanced skaters stats page.
     * @return void
     */
    public function testTeamAdvancedSkaters(): void
    {
        $response = $this->get('/nhl/stats/teams/skater-advanced?season=2019&type=regular&stat=corsi_rel');
        $response->assertStatus(200);
        $response->assertSee('<h1>NHL Advanced Skater Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Advanced Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::skater-advanced">Individual Advanced Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li class="sel" data-id="team::skater-advanced">Team Advanced Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top class="stats-grouped">
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-MTL">'
            . '<a href="/nhl/teams/montreal-canadiens">Canadiens</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-VGK">'
            . '<a href="/nhl/teams/vegas-golden-knights">Golden Knights</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-CAR">'
            . '<a href="/nhl/teams/carolina-hurricanes">Hurricanes</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-LA">'
            . '<a href="/nhl/teams/los-angeles-kings">Kings</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-TB">'
            . '<a href="/nhl/teams/tampa-bay-lightning">Lightning</a></span></cell>
            </row>');
    }

    /**
     * A test of the team goalies stats page.
     * @return void
     */
    public function testTeamGoalies(): void
    {
        $response = $this->get('/nhl/stats/teams/goalie?season=2019&type=regular&stat=save_pct');
        $response->assertStatus(200);
        $response->assertSee('<h1>NHL Goalie Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Goalie</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::skater-advanced">Individual Advanced Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::skater-advanced">Team Advanced Skater</li>
                            <li class="sel" data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-BOS">'
            . '<a href="/nhl/teams/boston-bruins">Bruins</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-DAL">'
            . '<a href="/nhl/teams/dallas-stars">Stars</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-ARI">'
            . '<a href="/nhl/teams/arizona-coyotes">Coyotes</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-nhl-COL">'
            . '<a href="/nhl/teams/colorado-avalanche">Avalanche</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-CHI">'
            . '<a href="/nhl/teams/chicago-blackhawks">Blackhawks</a></span></cell>
            </row>');
    }

    /**
     * A test of a single team players batting stats page.
     * @return void
     */
    public function testTeamStats(): void
    {
        // Invalid team arguments (all ignored, NHL leaders listed).
        $response = $this->get('/nhl/stats/players/skater?team_id=&season=2020&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Stats &raquo; Players &raquo; Skater</title>');
        $response->assertSee('<h1>NHL Skater Leaders</h1>');
        $response = $this->get('/nhl/stats/players/skater?team_id=ATL&season=2020&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Stats &raquo; Players &raquo; Skater</title>');
        $response->assertSee('<h1>NHL Skater Leaders</h1>');

        // Correct list.
        $response = $this->get('/nhl/stats/players/skater?team_id=TB&season=2020&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<h1><span class="hidden-m hidden-tp">Tampa Bay</span> Lightning Skater Leaders</h1>');
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Stats &raquo; Lightning &raquo; Skater</title>');

        $response->assertDontSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-EDM">'
            . '<a href="/nhl/players/leon-draisaitl-2322">L. Draisaitl</a></span></cell>
            </row>');
        $response->assertSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-nhl-TB">'
            . '<a href="/nhl/players/nikita-kucherov-2220">N. Kucherov</a></span></cell>
            </row>');
    }
}
