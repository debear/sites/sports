<?php

namespace Tests\PHPUnit\Feature\NHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class HTeamsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the team list page.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/nhl/teams');
        $response->assertStatus(200);

        $response->assertSee('<h1>Teams</h1>');
        $response->assertSee('<dt class="row_conf-Western misc-nhl-narrow-small-Western '
            . 'misc-nhl-narrow-small-right-Western conf">Western Conference</dt>');
        $response->assertSee('<dt class="row_conf-Western div">Central Division</dt>');

        // A team from this season.
        $response->assertSee('<dd class="row-0 team-nhl-small-WPG logo"></dd>
                    <dd class="row-0 name">Winnipeg Jets</dd>
                    <dd class="row-0 info">
                        <ul class="inline_list">
                            <li><a href="/nhl/teams/winnipeg-jets">Home</a></li>'
            . '<li><a href="/nhl/teams/winnipeg-jets#schedule">Schedule</a></li>'
            . '<li><a href="/nhl/teams/winnipeg-jets#roster">Roster</a></li>
                        </ul>
                    </dd>');

        // But not a team from a previous season.
        $response->assertDontSee('<dd class="row-0 team-nhl-small-ATL logo"></dd>
                    <dd class="row-0 name">Atlanta Thrashers</dd>
                    <dd class="row-0 info">
                        <ul class="inline_list">
                            <li><a href="/nhl/teams/atlanta-thrashers">Home</a></li>'
            . '<li><a href="/nhl/teams/atlanta-thrashers#schedule">Schedule</a></li>'
            . '<li><a href="/nhl/teams/atlanta-thrashers#roster">Roster</a></li>
                        </ul>
                    </dd>');
    }

    /**
     * A test of an individual team page.
     * @return void
     */
    public function testIndividual(): void
    {
        $response = $this->get('/nhl/teams/tampa-bay-lightning');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; NHL &raquo; Teams &raquo; Tampa Bay Lightning</title>');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="schedule">Schedule</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="roster">Roster</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="standings">Standings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="power-ranks">Power Rankings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="history">History</item>
                <sep class="right"></sep>
    </desk>');

        $this->individualHeader($response);
        $this->individualSchedule($response);
        $this->individualStandings($response);
        $this->individualStats($response);
        $this->individualLeaders($response);
        $this->individualMisc($response);
    }

    /**
     * A test of the header component of an individual team page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualHeader(TestResponse $response): void
    {
        $response->assertDontSee('<div class="favourite">');
        $response->assertSee('<dl class="inline_list header clearfix">
    ' . '
            <dt class="standing">2nd, Atlantic Division:</dt>
            <dd class="standing">43&ndash;21&ndash;6, 92pts</dd>
        ' . '
                        <dt class="prev game">Last Game:</dt>
            <dd class="prev game">
                ECSF Gm 5
                                    <span class="result-win">W 3 &ndash; 2</span>
                                v
                <span class="team-nhl-BOS icon">BOS</span>
            </dd>
        ' . '
        </dl>');
    }

    /**
     * A test of the schedule component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualSchedule(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list recent clearfix">
            <li class="nhl_box">
                            <a href="/nhl/schedule/20200819/blue-jackets-at-lightning-p125">
                        <dl>
                <dt class="row-head">ECQF Gm 5</dt>
                    <dd class="opp team-nhl-narrow_small-right-CLB">
                        v
                    </dd>
                                            <dd class="result row-0 result-win">W 5 &ndash; 4</dd>
                                </dl>
                            </a>
                    </li>');
    }

    /**
     * A test of the standings component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStandings(TestResponse $response): void
    {
        $response->assertSee('<li class="div row_conf-Eastern misc-nhl-narrow-small-right-Eastern">'
            . 'Atlantic Division</li>

    ' . '
            <li class="header">
                            <div class="col  row_conf-Eastern">Pts %</div>
                            <div class="col  row_conf-Eastern">Pts</div>
                            <div class="col  row_conf-Eastern">ROW</div>
                            <div class="col  row_conf-Eastern">GP</div>
                    </li>
    ' . '
    ' . '
                    <li class="row-1 team">
            <span class="team-nhl-BOS">
                <a href="/nhl/teams/boston-bruins">Bruins</a>
            </span>
                            <sup>*</sup>
                                        <div class="row-1 col">.714</div>
                            <div class="row-1 col">100</div>
                            <div class="row-1 col">44</div>
                            <div class="row-1 col">70</div>
                    </li>');
        $response->assertSee('<li class="row_nhl-TB team">
            <span class="team-nhl-TB">
                <a href="/nhl/teams/tampa-bay-lightning">Lightning</a>
            </span>
                            <sup>x</sup>
                                        <div class="row_nhl-TB col">.657</div>
                            <div class="row_nhl-TB col">92</div>
                            <div class="row_nhl-TB col">41</div>
                            <div class="row_nhl-TB col">70</div>
                    </li>');
    }

    /**
     * A test of the stats component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStats(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list stats clearfix">
        <li class="row-head title">Team Stats</li>
                    <li class="cell nhl_box">
                <dl class="clearfix">
                    <dt class="row-head">Goals For</dt>
                        <dd class="rank">1st</dd>
                        <dd class="value"><span>243</span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell nhl_box">
                <dl class="clearfix">
                    <dt class="row-head">Goals Against</dt>
                        <dd class="rank">9th</dd>
                        <dd class="value"><span>185</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the team leader component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualLeaders(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list leaders clearfix">
        <li class="row-head title">Team Leaders</li>
                    <li class="cell pts nhl_box">
                <dl>
                    <dt class="row-head">Points</dt>
                        <dd class="name"><a href="/nhl/players/nikita-kucherov-2220">N. Kucherov</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Nikita Kucherov"></dd>
                        <dd class="value"><span>85</span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell wins nhl_box">
                <dl>
                    <dt class="row-head">Wins</dt>
                        <dd class="name"><a href="/nhl/players/andrei-vasilevskiy-2368">A. Vasilevskiy</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Andrei Vasilevskiy"></dd>
                        <dd class="value"><span>35</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the misc info on an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualMisc(TestResponse $response): void
    {
        $response->assertSee('<div class="tab-dropdown"><input type="hidden" id="season-schedule" '
            . 'name="season-schedule" value="2019" data-is-dropdown="true" >
<dl class="dropdown " data-id="season-schedule"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019/20</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2019">2019/20</li>
                            <li  data-id="2018">2018/19</li>
                            <li  data-id="2017">2017/18</li>
                            <li  data-id="2016">2016/17</li>
                            <li  data-id="2015">2015/16</li>
                            <li  data-id="2014">2014/15</li>
                            <li  data-id="2013">2013/14</li>
                            <li  data-id="2012">2012/13</li>
                            <li  data-id="2011">2011/12</li>
                            <li  data-id="2010">2010/11</li>
                            <li  data-id="2009">2009/10</li>
                            <li  data-id="2008">2008/09</li>
                            <li  data-id="2007">2007/08</li>
                    </ul>
    </dd>
</dl>');
    }

    /**
     * A test of the schedule tab of an individual team page.
     * @return void
     */
    public function testIndividualTabSchedule(): void
    {
        $response = $this->getTab('/nhl/teams/tampa-bay-lightning/schedule');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(95, 'sched');
        $response->assertJsonFragment([
            'date_fmt' => '3rd Oct',
            'date_ymd' => '2019-10-03',
            'venue' => 'v',
            'opp' => 'team-nhl-right-FLA',
            'opp_wide' => 'team-nhl-narrow_small-right-FLA',
            'opp_id' => 'FLA',
            'opp_city' => 'Florida',
            'opp_franchise' => 'Panthers',
            'link' => '/nhl/schedule/20191003/panthers-at-lightning-r5',
            'info' => '<span class="result-win">W 5 &ndash; 2</span>',
        ]);
        $response->assertJSONCount(5, 'gameTabs');
        $response->assertJSON([
            'gameTabs' => [
                [
                    'label' => [
                        'regular' => 'Season Series',
                        'playoff' => 'Playoff Series',
                    ],
                    'icon' => 'series',
                    'link' => 'series',
                ],
                [ 'label' => 'Game Events', 'icon' => 'locations', 'link' => 'events' ],
                [ 'label' => 'Scoring', 'icon' => 'scoring', 'link' => 'scoring' ],
                [ 'label' => 'Boxscore', 'icon' => 'boxscore', 'link' => false ],
                [ 'label' => 'Game Stats', 'icon' => 'stats', 'link' => 'stats' ],
            ],
        ]);
    }

    /**
     * A test of the roster tab of an individual team page.
     * @return void
     */
    public function testIndividualTabRoster(): void
    {
        $response = $this->getTab('/nhl/teams/tampa-bay-lightning/roster');
        $response->assertStatus(200);
        $response->assertSee('<dl class="roster">');
        $response->assertSee('<dt class="row-head">Center</dt>
                                            <dd class="row-0 mugshot mugshot-tiny"><img class="mugshot-tiny" '
            . 'loading="lazy" src="');
        $response->assertSee('<dd class="row-0 jersey">#9</dd>
                <dd class="row-0 name">
                    <a href="/nhl/players/tyler-johnson-2107">Tyler Johnson</a>
                                                        </dd>
                <dd class="row-0 pos">C</dd>
                <dd class="row-0 height">5&#39; 8&quot;</dd>
                <dd class="row-0 weight">185lb</dd>
                <dd class="row-0 dob">29th Jul 1990</dd>
                <dd class="row-0 birthplace"><span class="flag_right flag16_right_us">Spokane, WA</span></dd>');
    }

    /**
     * A test of the standings tab of an individual team page.
     * @return void
     */
    public function testIndividualTabStandings(): void
    {
        $response = $this->getTab('/nhl/teams/tampa-bay-lightning/standings');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('chart.chart.renderTo', 'chart-standings');
        $response->assertJsonPath('chart.series.0.type', 'spline');
        $response->assertJsonPath('chart.series.0.data', [
            7, 1, 2, 6, 6, 6, 6, 6, 4, 4, 6, 6, 7, 4, 4, 3, 3, 5, 5, 5, 6, 4, 5, 5, 4, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6,
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 6, 7, 5, 6, 4, 4, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 4,
            4, 5, 5, 6, 5, 5, 4, 6, 6, 6, 6, 6, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        ]);
        $response->assertJsonPath('chart.series.1.type', 'column');
        $response->assertJsonPath('chart.series.1.data', [
            0, 3, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
            0, 1, 2, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 2, 0, 4, 0, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 6, 0, 0, 1, 0, 1,
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 5, 0, 1, 1, 0, 2, 0, 1, 0, 2, 2, 0, 7, 0, 4, 0, 1, 0, 0, 1, 0, 0, 6, 0, 0, 0,
            0, 0, 0, 0, 2, 0, 1, 3, 0, 0, 2, 0, 2, 0, 2, 0, 1, 1, 0, 2, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 0, 0, 0, 4, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ]);
        $response->assertJsonPath('chart.series.2.type', 'column');
        $response->assertJsonPath('chart.series.2.data', [
            0, 0, 0, -1, -1, 0, 0, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, -4, 0, 0, 0, 0, 0, 0, -1, 0, 0, -3, 0, 0, -3, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, -1, 0, 0, 0, 0, -1, 0, 0, 0,
            -4, 0, 0, 0, 0, -3, 0, 0, 0, 0, -1, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, 0,
            0, -1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -4,
            0, 0, -1, 0, -3, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
        ]);
        $response->assertJsonPath('extraJS.standingsRecords.1', '1&ndash;0&ndash;0, 2pts');
        $response->assertJsonPath('extraJS.standingsRecords.178', '43&ndash;21&ndash;6, 92pts');
    }

    /**
     * A test of the power rank tab of an individual team page.
     * @return void
     */
    public function testIndividualTabPowerRanks(): void
    {
        $n = null; // To shorten the array lengths.

        $response = $this->getTab('/nhl/teams/tampa-bay-lightning/power-ranks');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('chart.chart.renderTo', 'chart-power-ranks');
        $response->assertJsonPath('chart.series.0.data', [
            13, 14, 12, 12, 20, 8, 11, 8, 11, 9, 7, 15, 7, 1, 1, 4, 4, 2, 2, 6, 14, 4, $n, $n, $n, $n,
        ]);
        $response->assertJsonPath('extraJS.powerRankWeeks.1', 'Week 1, 2nd&ndash;6th Oct');
    }

    /**
     * A test of the history tab of an individual team page.
     * @return void
     */
    public function testIndividualTabHistory(): void
    {
        $response = $this->getTab('/nhl/teams/tampa-bay-lightning/history');
        $response->assertStatus(200);
        $response->assertSee('<dt class="league row-head row_league-NHL">
                <span class="icon misc-nhl-NHL">National Hockey League</span>
            </dt>
            <dd class="league titles row-head row_league-NHL"><span>4 Division Titles</span>; '
            . '<span>3 Conference Titles</span>; <span>1 Presidents&#39; Trophy</span>; '
            . '<span>2 Stanley Cups</span></dd>');
        $response->assertSee('<dt class="row-head parts-1 season">2019/20</dt>
                    <dd class="row-1 record">43&ndash;21&ndash;6, 2nd Atlantic</dd>
                ' . '
                    <dd class="row-1 playoffs">
                ' . '
                <ul class="inline_list matchups clearfix">
                                            <li class="result-win">
                            <span class="hidden-m">Stanley Cup Final:</span>
                            <span class="hidden-t hidden-d">Stanley Cup:</span>
                            Won 4&ndash;2
                                                            v <span class="icon team-nhl-DAL">Dallas Stars</span>
                                                    </li>
                                            <li class="result-win">
                            <span class="hidden-m">Conference Final:</span>
                            <span class="hidden-t hidden-d">Conf Final:</span>
                            Won 4&ndash;2
                                                            v <span class="icon team-nhl-NYI">New York Islanders</span>
                                                    </li>
                                            <li class="result-win">
                            <span class="hidden-m">Conference Semi Final:</span>
                            <span class="hidden-t hidden-d">Conf SF:</span>
                            Won 4&ndash;1
                                                            v <span class="icon team-nhl-BOS">Boston Bruins</span>
                                                    </li>
                                            <li class="result-win">
                            <span class="hidden-m">Conference Quarter Final:</span>
                            <span class="hidden-t hidden-d">Conf QF:</span>
                            Won 4&ndash;1
                                                            v <span class="icon team-nhl-CLB">'
            . 'Columbus Blue Jackets</span>
                                                    </li>
                                    </ul>
            </dd>');
    }

    /**
     * A test of the various subtleties of an individual team page.
     * @return void
     */
    public function testIndividualMisc(): void
    {
        $response = $this->get('/nhl/teams/unknown-team');
        $response->assertStatus(404);

        $response = $this->get('/nhl/teams/unknown-team/unknown-tab');
        $response->assertStatus(404);

        $response = $this->get('/nhl/teams/unknown-team/schedule');
        $response->assertStatus(404);

        $response = $this->get('/nhl/teams/atlanta-thrashers');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/nhl/teams/winnipeg-jets');

        $response = $this->get('/nhl/teams/tampa-bay-lightning/home');
        $response->assertStatus(404);

        $response = $this->get('/nhl/teams/tampa-bay-lightning/unknown');
        $response->assertStatus(404);

        $response = $this->get('/nhl/teams/tampa-bay-lightning/power-ranks');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/nhl/teams/tampa-bay-lightning#power-ranks');

        $response = $this->getTab('/nhl/teams/tampa-bay-lightning/power-ranks/2019');
        $response->assertStatus(200);
        $n = null;
        $response->assertJsonPath('chart.series.0.data', [
            13, 14, 12, 12, 20, 8, 11, 8, 11, 9, 7, 15, 7, 1, 1, 4, 4, 2, 2, 6, 14, 4, $n, $n, $n, $n,
        ]);

        $response = $this->getTab('/nhl/teams/tampa-bay-lightning/power-ranks/2039');
        $response->assertStatus(404);
    }

    /**
     * A test of the user favourite setting.
     * @return void
     */
    public function testFavourites(): void
    {
        // User must be logged in to perform either action.
        $response = $this->post('/nhl/teams/tampa-bay-lightning/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/nhl/teams/tampa-bay-lightning/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);

        // Log in for all future requests.
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Trying actions on a team that doesn't exist should fail.
        $response = $this->post('/nhl/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/nhl/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);

        // Ensure team is not already a favourite (but has the option to save as such).
        $response = $this->get('/nhl/teams/tampa-bay-lightning');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // If we try to remove at this point, it should fail.
        $response = $this->delete('/nhl/teams/tampa-bay-lightning/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Perform the add, and validate the page updates accordingly.
        $response = $this->post('/nhl/teams/tampa-bay-lightning/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(201);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_is',
            'hover' => 'icon_right_favourite_remove',
        ]);

        $response = $this->get('/nhl/teams/tampa-bay-lightning');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="rmv">
            <span class="icons icon_favourite_is icon_right_favourite_remove"></span>
            <span class="text">Remove from favourites</span>
        </button>
    </div>');

        // A second attempt should also fail.
        $response = $this->post('/nhl/teams/tampa-bay-lightning/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Now perform a removal, and validate the page updates accordingly.
        $response = $this->delete('/nhl/teams/tampa-bay-lightning/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_not',
            'hover' => 'icon_right_favourite_add',
        ]);

        $response = $this->get('/nhl/teams/tampa-bay-lightning');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
