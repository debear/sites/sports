<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class VSitemapTest extends MajorLeagueBaseCase
{
    /**
     * A test of the HTML sitemap.
     * @return void
     */
    public function testSitemap(): void
    {
        $response = $this->get('/ahl/sitemap');
        $response->assertStatus(200);

        $response->assertSee('<meta name="Description" content="Sitemap detailing the available content on DeBear '
            . 'Sports AHL." />');
        $response->assertSee('<h1>Find your way round DeBear Sports AHL!</h1>

<ul class="inline_list sitemap" id="sitemap">
    <li class="item">
    <div class="toggle "></div>
            <a href="/ahl/">
        <strong>Home</strong>
            </a>
    ' . '
            <em class="descrip">All the latest American Hockey League results, stats and news.</em>
    ' . '
    </li>');
    }

    /**
     * A test of the XML sitemap index file.
     * @return void
     */
    public function testSitemapXMLIndex(): void
    {
        $response = $this->get('/ahl/sitemap.xml');
        $response->assertStatus(200);

        $response->assertDontSee('<?xml version="1.0" encoding="UTF-8"?>'
            . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://sports.debear.test/ahl/</loc>
        <changefreq>daily</changefreq>
        <priority>1</priority>
    </url>');
        $response->assertSee('<?xml version="1.0" encoding="UTF-8"?>'
            . '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <sitemap>
      <loc>https://sports.debear.test/ahl/sitemap.xml/main</loc>
   </sitemap>
   <sitemap>
      <loc>https://sports.debear.test/ahl/sitemap.xml/players</loc>
   </sitemap>
       <sitemap>
      <loc>https://sports.debear.test/ahl/sitemap.xml/2005</loc>
    </sitemap>');
    }

    /**
     * A test of the "standard" XML sitemap files.
     * @return void
     */
    public function testSitemapXMLMain(): void
    {
        $response = $this->get('/ahl/sitemap.xml/main');
        $response->assertStatus(200);

        $response->assertSee('<?xml version="1.0" encoding="UTF-8"?>'
            . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://sports.debear.test/ahl/</loc>
        <changefreq>daily</changefreq>
        <priority>1</priority>
    </url>');
        $response->assertSee('<url>
        <loc>https://sports.debear.test/ahl/standings</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>');
        $response->assertSee('<url>
        <loc>https://sports.debear.test/ahl/standings/2018</loc>
        <changefreq>never</changefreq>
        <priority>0.4</priority>
    </url>');
        $response->assertDontSee('<url>
        <loc>https://sports.debear.test/ahl/standings/2019</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>');
    }

    /**
     * A test of the by-player XML sitemap files.
     * @return void
     */
    public function testSitemapXMLByPlayers(): void
    {
        $response = $this->get('/ahl/sitemap.xml/players');
        $response->assertStatus(200);

        $response->assertSee('<?xml version="1.0" encoding="UTF-8"?>'
            . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://sports.debear.test/ahl/players/peter-abbandonato-5990</loc>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>');
    }

    /**
     * A test of the by-season XML sitemap files.
     * @return void
     */
    public function testSitemapXMLBySeason(): void
    {
        $orig = FrameworkConfig::get('debear.links');
        $response = $this->get('/ahl/sitemap.xml/2019');
        $response->assertStatus(200);

        $response->assertSee('<?xml version="1.0" encoding="UTF-8"?>'
            . '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://sports.debear.test/ahl/schedule/20191004/crunch-at-americans-r2</loc>
        <changefreq>never</changefreq>
        <priority>0.8</priority>
    </url>');

        // An argument too early, or late, results in 404s.
        FrameworkConfig::set(['debear.links' => $orig]);
        $response = $this->get('/ahl/sitemap.xml/2004');
        $response->assertStatus(404);
        FrameworkConfig::set(['debear.links' => $orig]);
        $response = $this->get('/ahl/sitemap.xml/2020');
        $response->assertStatus(404);
    }
}
