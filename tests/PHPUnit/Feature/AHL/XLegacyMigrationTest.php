<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class XLegacyMigrationTest extends MajorLeagueBaseCase
{
    /**
     * A test of legacy migration of General URLs.
     * @return void
     */
    public function testGeneral(): void
    {
        // Index.
        $response = $this->get('/ahl/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl');

        $response = $this->get('/ahl/2019');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl');

        $response = $this->get('/ahl/2019/index');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl');

        // Standings.
        $response = $this->get('/ahl/2019/standings');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/standings/2019');

        $response = $this->get('/ahl/standings/expanded');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/ahl/standings/{$this->season}");

        $response = $this->get('/ahl/2019/standings/league');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/standings/2019');

        // News.
        $response = $this->get('/ahl/2019/news');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/news');

        $response = $this->get('/ahl/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/news');

        $response = $this->get('/ahl/2019/news-articles');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/news');
    }

    /**
     * A test of legacy migration of Schedule URLs.
     * @return void
     */
    public function testSchedule(): void
    {
        // Schedule.
        $response = $this->get('/ahl/2000/schedule');
        $response->assertStatus(404);

        $response = $this->get('/ahl/2019/schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule/20200311');

        $response = $this->get('/ahl/schedule-20191201');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule/20191201');

        $response = $this->get('/ahl/2019/schedule-20191201');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule/20191201');

        // Playoffs.
        $response = $this->get('/ahl/2019/playoffs');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/playoffs/2019');

        $response = $this->get('/ahl/playoffs/11');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/playoffs');

        $response = $this->get('/ahl/2019/playoffs/99');
        $response->assertStatus(404);
    }

    /**
     * A test of legacy migration of individual Game URLs.
     * @return void
     */
    public function testGame(): void
    {
        // Invalid game type or season.
        $response = $this->get('/ahl/game-s123');
        $response->assertStatus(404);
        $response = $this->get('/ahl/game-rp123');
        $response->assertStatus(404);
        $response = $this->get('/ahl/2000/game-r123');
        $response->assertStatus(404);

        // Ambiguous season.
        $response = $this->get('/ahl/game-r123');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule');
        $response = $this->get('/ahl/game-r123-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule');

        // To the new format.
        $response = $this->get('/ahl/2019/game-r123');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule/20191026/wild-at-roadrunners-r123');

        $response = $this->get('/ahl/2019/game-r123-boxscore');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule/20191026/wild-at-roadrunners-r123/boxscore');
    }

    /**
     * A test of legacy migration of Stats URLs.
     * @return void
     */
    public function testStats(): void
    {
        // Stats Summary.
        $response = $this->get('/ahl/2019/stats');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/stats');

        // Tables.
        $response = $this->get('/ahl/stats-ind-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/stats/players/skater?season=2019&type=regular');

        $response = $this->get('/ahl/2019/stats-ind-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/stats/players/skater?season=2019&type=regular');

        // By Team Stats.
        $response = $this->get('/ahl/stats-AAA');
        $response->assertStatus(404);

        $response = $this->get('/ahl/stats-SYR');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/stats/players/skater?team_id=SYR&season=2019&'
            . 'type=regular');

        $response = $this->get('/ahl/2019/stats-SYR');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/stats/players/skater?team_id=SYR&season=2019&'
            . 'type=regular');

        $response = $this->get('/ahl/stats-SYR-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/stats/players/skater?team_id=SYR&season=2019&'
            . 'type=regular');

        $response = $this->get('/ahl/2019/stats-SYR-skaters');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/stats/players/skater?team_id=SYR&season=2019&'
            . 'type=regular');
    }

    /**
     * A test of legacy migration of Team URLs.
     * @return void
     */
    public function testTeams(): void
    {
        $response = $this->get('/ahl/2019/teams');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/teams');

        $response = $this->get('/ahl/team-AAA');
        $response->assertStatus(404);

        $response = $this->get('/ahl/team-SYR');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/teams/syracuse-crunch');

        $response = $this->get('/ahl/2019/team-SYR');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/teams/syracuse-crunch');

        $response = $this->get('/ahl/team-SYR-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/teams/syracuse-crunch');

        $response = $this->get('/ahl/2019/team-SYR-schedule');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/teams/syracuse-crunch');
    }

    /**
     * A test of legacy migration of Player URLs.
     * @return void
     */
    public function testPlayers(): void
    {
        // List.
        $response = $this->get('/ahl/2019/players');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/players');

        // Individual.
        $response = $this->get('/ahl/player-0');
        $response->assertStatus(404);

        $response = $this->get('/ahl/player-5160');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/players/connor-ingram-5160');

        $response = $this->get('/ahl/player-5160-career');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/ahl/players/connor-ingram-5160/{$this->season}/career");

        $response = $this->get('/ahl/2019/player-5160');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/players/connor-ingram-5160/2019');

        $response = $this->get('/ahl/2019/player-5160-career');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/players/connor-ingram-5160/2019/career');
    }

    /**
     * A test of the legacy migration of Power Rank URLs.
     * @return void
     */
    public function testPowerRanks(): void
    {
        $response = $this->get('/ahl/2019/power-ranks');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/power-ranks/2019');

        $response = $this->get('/ahl/power-ranks-12');
        $response->assertStatus(308);
        $response->assertRedirect("https://sports.debear.test/ahl/power-ranks/{$this->season}/12");

        $response = $this->get('/ahl/2019/power-ranks-12');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/power-ranks/2019/12');
    }
}
