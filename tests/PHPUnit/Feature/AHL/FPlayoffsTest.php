<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class FPlayoffsTest extends MajorLeagueBaseCase
{
    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/ahl/playoffs/2019');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; 2019/20 Calder Cup Playoffs</title>');
        $response->assertSee('<li class="sel" data-id="2019">2019/20</li>');

        $response->assertSee('<fieldset class="info icon_info preseason">
    The 2019/20 Calder Cup Playoffs Playoffs were not held after the AHL '
            . 'suspended play due to the COVID-19 pandemic. The Calder Cup was not awarded that season.
</fieldset>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/ahl/playoffs/2019/body');
        $response->assertStatus(200);
        $response->assertSee('<h1><span class="hidden-m">2019/20</span> Calder Cup Playoffs</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/ahl/playoffs/2000');
        $response->assertStatus(404);
        $response = $this->get('/ahl/playoffs/2039');
        $response->assertStatus(404);
    }
}
