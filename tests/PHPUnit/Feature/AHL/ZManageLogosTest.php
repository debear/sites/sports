<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class ZManageLogosTest extends MajorLeagueBaseCase
{
    /**
     * A test of the logo admin page as a guest user.
     * @return void
     */
    public function testUnderprivileged(): void
    {
        // Before being logged in - 401 Unauthorised.
        $response = $this->get('/ahl/manage/logos');
        $response->assertStatus(401);

        // Login as a (general) user - 403 Forbidden.
        $login = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $login->assertStatus(200);
        $login->assertJsonFragment(['success' => true]);
        $response = $this->get('/ahl/manage/logos');
        $response->assertStatus(403);
    }

    /**
     * A test of the logo page.
     * @return void
     */
    public function testLogos(): void
    {
        $login = $this->post('/login', [
            'username' => 'sports_admin',
            'password' => 'testlogindetails',
        ]);
        $login->assertStatus(200);
        $login->assertJsonFragment(['success' => true]);

        $response = $this->get('/ahl/manage/logos');
        $response->assertStatus(200);

        $response->assertSee('<h1>AHL Logos</h1>');
        $response->assertSee('<ul class="inline_list breadcrumbs"><li>DeBear Sports</li><li>AHL</li><li>Admin</li>'
            . '<li>Logos</li></ul>');
        $response->assertSee('<select id="subnav">
                            <option value="tiny" selected="selected">Tiny</option>
                            <option value="small" >Small</option>
                            <option value="medium" >Medium</option>
                            <option value="large" >Large</option>
                            <option value="narrow_small" >Narrow Small</option>
                            <option value="narrow_large" >Narrow Large</option>
                    </select>');
        $response->assertSee('<li class="grid-1-5 grid-tl-1-4 grid-tp-1-3 grid-m-1-2">');

        // Teams.
        $response->assertSee('<dl>
                    <dt class="row_ahl-BAK team-ahl-BAK">Bakersfield Condors</dt>
                        <dd class="team-ahl-BAK"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_ahl-BAK team-ahl-BAK">Bakersfield Condors</dt>
                        <dd class="team-ahl-small-BAK"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_ahl-BAK team-ahl-BAK">Bakersfield Condors</dt>
                        <dd class="team-ahl-medium-BAK"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_ahl-BAK team-ahl-BAK">Bakersfield Condors</dt>
                        <dd class="team-ahl-large-BAK"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_ahl-BAK team-ahl-BAK">Bakersfield Condors</dt>
                        <dd class="team-ahl-narrow_small-BAK"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_ahl-BAK team-ahl-BAK">Bakersfield Condors</dt>
                        <dd class="team-ahl-narrow_large-BAK"></dd>
                </dl>');

        // Groupings.
        $response->assertSee('<dl>
                    <dt class="row_league-AHL misc-ahl-AHL">American Hockey League</dt>
                        <dd class="misc-ahl-AHL"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_league-AHL misc-ahl-AHL">American Hockey League</dt>
                        <dd class="misc-ahl-large-AHL"></dd>
                </dl>');
        $response->assertSee('<dl>
                    <dt class="row_league-AHL misc-ahl-AHL">American Hockey League</dt>
                        <dd class="misc-ahl-narrow_large-AHL"></dd>
                </dl>');
    }
}
