<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class DNewsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the AHL news.
     * @return void
     */
    public function testNews(): void
    {
        $response = $this->get('/ahl/news');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell ">
        <h4><a href="https://www.nhl.com/news/ahl-notebook-new-jersey-devils-prospects-fueling-binghamton/c-316085916#'
            . 'new_tab" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Prospects fueling '
            . 'turnaround in Binghamton</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="http://theahl.com/20playoffprimer" rel="noopener noreferrer" target="_blank" title="Opens in new '
            . 'tab/window">AHL Playoff Primer: March 11</a></h4>');
        $response->assertSee('<article class="grid-cell ">
        <h4><a href="http://theahl.com/leier-balancing-hockey-with-promoting-healthy-living" rel="noopener noreferrer" '
            . 'target="_blank" title="Opens in new tab/window">Leier balancing hockey with promoting healthy '
            . 'living</a></h4>');

        $response->assertSee('<next class="onclick_target prev-next mini"><i class="fas fa-chevron-circle-right"></i>'
            . '</next>
    <bullets>
                    <bullet class="onclick_target" data-ref="1">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>
                    <bullet class="onclick_target" data-ref="2">
                <i class="fas active fa-dot-circle"></i>
                <i class="far inactive fa-dot-circle"></i>
                <i class="fas hover fa-dot-circle"></i>
            </bullet>');

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.nhl.com/news/morgan-geekies-family-joins-postgame-interview-after-forwards-nhl-debut/'
            . 'c-316015990#new_tab" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">'
            . 'Memorable NHL debut for Geekie</a></h4>');
        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://theahl.com/stats/daily-schedule#new_tab" rel="noopener noreferrer" target="_blank" '
            . 'title="Opens in new tab/window">Phantoms, Sound Tigers on NHL Network today</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.nhl.com/news/ahl-notebook-keith-kinkaid-charlotte-checkers/c-315790154#new_tab" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Kinkaid excited for fresh '
            . 'start with Charlotte</a></h4>');

        $response->assertSee('<li class="page_box unshaded_row ">
            <strong>1</strong>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="9">9</a>
        </li>');
        $response->assertDontSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="10">10</a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="2">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');
    }

    /**
     * A test of a specific AHL news page.
     * @return void
     */
    public function testNewsPage(): void
    {
        // Valid page.
        $response = $this->get('/ahl/news?page=2');
        $response->assertStatus(200);

        $response->assertSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://www.nhl.com/news/ahl-notebook-keith-kinkaid-charlotte-checkers/c-315790154#new_tab" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Kinkaid excited for fresh '
            . 'start with Charlotte</a></h4>');
        $response->assertDontSee('<article class="grid-cell grid-cell box section">
        <h4><a href="https://thehockeynews.com/news/article/ahl-contenders-breaking-down-the-top-six-hopefuls-for-the-'
            . 'calder-cup#new_tab" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Breaking '
            . 'down Calder Cup hopefuls</a></h4>');

        $response->assertSee('<li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="1">1</a>
        </li>
            <li class="page_box unshaded_row ">
            <strong>2</strong>
        </li>
            <li class="page_box shaded_row ">
            <a  class="onclick_target" data-page="3">3</a>
        </li>');
        $response->assertSee('<li class="page_box shaded_row not-narrow">
            <a  class="onclick_target" data-page="9">9</a>
        </li>');
        $response->assertDontSee('<li class="page_box shaded_row">
            <a  class="onclick_target" data-page="10">10</a>
        </li>');
        $response->assertSee('<li class="next_page shaded_row">
            <a  class="onclick_target" data-page="3">
                Next<span class="hidden-m"> Page</span> &raquo;
            </a>
        </li>');

        // Error.
        $response = $this->get('/ahl/news?page=10');
        $response->assertStatus(404);
    }
}
