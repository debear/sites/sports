<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class BScheduleTest extends MajorLeagueBaseCase
{
    /**
     * A test of the current date (though deterministically "current")
     * @return void
     */
    public function testCurrent(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-03-11 12:34:56');

        $response = $this->get('/ahl/schedule');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/ahl/schedule/20200311" data-date="20200311">
        <span class="date hidden-m">Wednesday 11th March</span>
        <span class="date hidden-t hidden-d">Wed 11th Mar</span>
        <calendar');

        $response->assertSee('<ul class="inline_list ahl_box game result">
    ' . '
    <li class="status head">
        <strong>Final</strong>
                    <span class="att">Attendance: 3,935</span>
            </li>
        ' . '
    <li class="team visitor win">
        <span class="score">3</span>
        <a href="/ahl/teams/syracuse-crunch" class="team-ahl-narrow_small-SYR">Crunch</a>
                    <span class="standings">(30&ndash;23&ndash;4&ndash;5)</span>
            </li>
    <li class="team home loss">
        <span class="score">1</span>
        <a href="/ahl/teams/utica-comets" class="team-ahl-narrow_small-UTI">Comets</a>
                    <span class="standings">(34&ndash;22&ndash;3&ndash;2)</span>
            </li>');
        $response->assertSee('<dl class="periods clearfix">
    ' . '
            <dt class="period head period-1">1</dt>
            <dt class="period head period-2">2</dt>
            <dt class="period head period-3">3</dt>
        <dt class="total head">Tot</dt>');
        $response->assertSee('<li class="venue head">Utica Memorial Auditorium</li>');
        $response->assertSee('<dl class="three_stars">
            <dt>Three Stars</dt>
                                            <dd class="icon icon_game_star1">
                    <span class="icon team-ahl-SYR"><a href="/ahl/players/luke-witkowski-3707">L. Witkowski</a></span>
                </dd>');
        $response->assertSee('<dl class="goalies">
                            <dt>Winning Goalie:</dt>
                <dd class="icon_right team-ahl-right-SYR">
                    <a href="/ahl/players/spencer-martin-4070">S. Martin</a></dd>
                            <dt>Losing Goalie:</dt>
                <dd class="icon_right team-ahl-right-UTI">
                    <a href="/ahl/players/michael-dipietro-6053">M. DiPietro</a></dd>
            </dl>');
        $response->assertSee('<li class="links">
        <ul class="inline_list">
                            <li>
                    <a class="icon_game_stats" href="/ahl/schedule/20200311/crunch-at-comets-r936#stats">'
            . '<em>Game Stats</em></a>
                </li>
                            <li>
                    <a class="icon_game_boxscore" href="/ahl/schedule/20200311/crunch-at-comets-r936">'
            . '<em>Boxscore</em></a>
                </li>
                            <li>
                    <a class="icon_game_scoring" href="/ahl/schedule/20200311/crunch-at-comets-r936#scoring">'
            . '<em>Scoring</em></a>
                </li>
                            <li>
                    <a class="icon_game_series" href="/ahl/schedule/20200311/crunch-at-comets-r936#series">'
            . '<em>Season Series</em></a>
                </li>
                    </ul>
    </li>');
    }

    /**
     * A test of an indirect match
     * @return void
     */
    public function testIndirect(): void
    {
        $response = $this->get('/ahl/schedule/20200126');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/ahl/schedule/20200125" data-date="20200125">
        <span class="date hidden-m">Saturday 25th January</span>
        <span class="date hidden-t hidden-d">Sat 25th Jan</span>
        <calendar');
    }

    /**
     * A test of dates with game previews
     * @return void
     */
    public function testPreview(): void
    {
        $response = $this->get('/ahl/schedule/20200314');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/ahl/schedule/20200314" data-date="20200314">
        <span class="date hidden-m">Saturday 14th March</span>
        <span class="date hidden-t hidden-d">Sat 14th Mar</span>
        <calendar');

        $response->assertSee('<li class="game-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
            <ul class="inline_list ahl_box game preview">
    <li class="status head">4:00pm PDT</li>
        <li class="team visitor">
        <a href="/ahl/teams/belleville-senators" class="team-ahl-narrow_small-BEL">Senators</a>
                    <span class="standings">(38&ndash;21&ndash;3&ndash;1)</span>
            </li>
    <li class="team home">
        <a href="/ahl/teams/syracuse-crunch" class="team-ahl-narrow_small-SYR">Crunch</a>
                    <span class="standings">(30&ndash;23&ndash;4&ndash;5)</span>
            </li>
    ' . '
        ' . '
        ' . '
        ' . '
    <li class="venue head">War Memorial at Oncenter</li>
    ' . '
    </ul>');
    }

    /**
     * A test of dates with cancelled games
     * @return void
     */
    public function testCancelled(): void
    {
        $response = $this->get('/ahl/schedule/20200313');
        $response->assertStatus(200);
        $response->assertSee('<span class="curr" data-link="/ahl/schedule/20200313" data-date="20200313">
        <span class="date hidden-m">Friday 13th March</span>
        <span class="date hidden-t hidden-d">Fri 13th Mar</span>
        <calendar');

        $response->assertSee('<ul class="inline_list ahl_box game incomplete">
    <li class="status head"><strong>Cancelled</strong></li>
        <li class="team visitor">
        <a href="/ahl/teams/syracuse-crunch" class="team-ahl-narrow_small-SYR">Crunch</a>
                    <span class="standings">(30&ndash;23&ndash;4&ndash;5)</span>
            </li>
    <li class="team home">
        <a href="/ahl/teams/rochester-americans" class="team-ahl-narrow_small-RCH">Americans</a>
                    <span class="standings">(33&ndash;20&ndash;4&ndash;5)</span>
            </li>
    <li class="venue head">Blue Cross Arena</li>
</ul>');
    }

    /**
     * A test of miscellaneous controller actions
     * @return void
     */
    public function testControllerMisc(): void
    {
        // From the season switcher.
        $response = $this->get('/ahl/schedule/2019/body');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/ahl/schedule/20200411/switcher');

        // Calendar widget exceptions.
        $response = $this->get('/ahl/schedule/2019/calendar');
        $response->assertStatus(200);
        $response->assertSee('{"exceptions":{"2019-10-07":true,');
        $response->assertSee(',"2020-01-07":true,');
        $response->assertDontSee(',"2020-01-08":true,');
        $response->assertSee(',"2020-01-09":true,');
    }

    /**
     * A test of ordered games based on user favourites
     * @return void
     */
    public function testScheduleFavourites(): void
    {
        // A control, the "regular" order.
        $response = $this->get('/ahl/schedule/20200311');
        $response->assertStatus(200);
        $response->assertDontSee('<h3>My Teams</h3>');
        $response->assertSeeInOrder([
            '<span class="att">Attendance: 9,278</span>', // SA @ MIL.
            '<span class="att">Attendance: 3,935</span>', // SYR @ UTI.
        ]);

        // Log in as a user with favourites.
        $response = $this->post('/login', [
            'username' => 'sports_admin',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Load the schedule and ensure the games are ordered correctly.
        $response = $this->get('/ahl/schedule/20200311');
        $response->assertStatus(200);
        $response->assertSeeInOrder(['<h3>My Teams</h3>', '<h3>Other Teams</h3>']);
        $response->assertSeeInOrder([
            '<span class="att">Attendance: 3,935</span>', // SYR @ UTI.
            '<span class="att">Attendance: 9,278</span>', // SA @ MIL.
        ]);

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
