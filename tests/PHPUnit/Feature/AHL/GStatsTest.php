<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class GStatsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the stat leaders page.
     * @return void
     */
    public function testLeaders(): void
    {
        $response = $this->get('/ahl/stats');
        $response->assertStatus(200);
        $response->assertSee('<h1>2019/20 AHL Stat Leaders</h1>');

        // Points.
        $response->assertSee('<dl class="ahl_box stat clearfix">
                <dt class="row-head name">Points</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-ahl-IAW">'
            . '<a href="/ahl/players/sam-anas-4959">Sam Anas</a></span>
                            <span class="stat">70</span>
                        </dd>');
        $response->assertSee('<dd class="row-1 link"><a href="/ahl/stats/players/skater'
            . '?season=2019&type=regular&stat=points">View Full Stats</a></dd>');

        // Goals.
        $response->assertSee('<dl class="ahl_box stat clearfix">
                <dt class="row-head name">Goals</dt>');
        $response->assertSee('<dt class="row-head pos">4</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-ahl-LAV">'
            . '<a href="/ahl/players/charles-hudon-3725">Charles Hudon</a></span>
                            <span class="stat">27</span>
                        </dd>
                                                        <dt class="row-head pos">=</dt>
                        <dd class="row-0 entity">
                            <em>3 More Players Tied</em>
                            <span class="stat">27</span>
                        </dd>
                                <dd class="row-1 link"><a href="/ahl/stats/players/skater'
            . '?season=2019&type=regular&stat=goals">View Full Stats</a></dd>');
        $response->assertDontSee('<dt class="row-head pos">=</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-ahl-SYR">'
            . '<a href="/ahl/players/alex-barre-boulet-5591">Alex Barre-Boulet</a></span>
                            <span class="stat">27</span>
                        </dd>');

        // Goalie Formatting.
        $response->assertSee('<span class="icon team-ahl-TUC">'
            . '<a href="/ahl/players/antti-raanta-3847">Antti Raanta</a></span>
                            <span class="stat">0.00</span>');
        $response->assertSee('<span class="icon team-ahl-TUC">'
            . '<a href="/ahl/players/antti-raanta-3847">Antti Raanta</a></span>
                            <span class="stat">1.000</span>');

        // Teams: Power Play.
        $response->assertSee('<dl class="ahl_box stat clearfix">
                <dt class="row-head name">Team Power Play</dt>');
        $response->assertSee('<dt class="row-head pos">1</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-ahl-STO">'
            . '<a href="/ahl/teams/stockton-heat">Heat</a></span>
                            <span class="stat">25.7</span>
                        </dd>');
        $response->assertSee('<dt class="row-head pos">5</dt>
                        <dd class="row-0 entity">
                            <span class="icon team-ahl-IAW">'
            . '<a href="/ahl/teams/iowa-wild">Wild</a></span>
                            <span class="stat">21.8</span>
                        </dd>');
        $response->assertDontSee('<dt class="row-head pos">6</dt>
                        <dd class="row-1 entity">
                            <span class="icon team-ahl-GR">'
            . '<a href="/ahl/teams/grand-rapid-griffins">Griffins</a></span>
                            <span class="stat">20.9</span>
                        </dd>');

        // Team lists.
        $response->assertSee('<dl class="ahl_box teams clearfix">
    <dt class="row-head">Sortable Stats by Team</dt>
            <dd ><a class="no_underline team-ahl-BAK" href="/ahl/stats/players/skater?team_id=BAK&season=2019&'
            . 'type=regular&stat=points"></a></dd>');
        $response->assertSee('<dd class="split-point"><a class="no_underline team-ahl-ONT" '
            . 'href="/ahl/stats/players/skater?team_id=ONT&season=2019&type=regular&stat=points"></a></dd>');
    }

    /**
     * A test of the preseason stat leaders page.
     * @return void
     */
    public function testPreseason(): void
    {
        $orig = FrameworkConfig::get('debear.sports.subsites.ahl.setup.season.viewing');
        $this->setTestConfig(['debear.sports.subsites.ahl.setup.season.viewing' => $orig + 1]);

        $response = $this->get('/ahl/stats');
        $response->assertStatus(200);
        $response->assertDontSee('<h1>2020/21 AHL Stat Leaders</h1>');
        $response->assertSee('<h1>2019/20 AHL Stat Leaders</h1>');
        $response->assertSee('<fieldset class="info icon_info preseason">
        The 2020/21 statistical leaders will appear once the season has started.
    </fieldset>');
    }

    /**
     * A test of the controller handling.
     * @return void
     */
    public function testHandler(): void
    {
        // Invalid option.
        $response = $this->get('/ahl/stats/players/unknown');
        $response->assertStatus(404);

        // Default options.
        $response = $this->get('/ahl/stats/players/skater');
        $response->assertStatus(200);
        $response->assertSee('<h1>AHL Skater Leaders</h1>');

        // Filters.
        $response->assertSee('<dl class="dropdown " data-id="filter_team"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>All AHL</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="">All AHL</li>
                            <li  data-id="BAK"><span class="icon team-ahl-BAK">BAK</span></li>');
        $response->assertSee('<dl class="dropdown " data-id="filter_season"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019/20 Regular Season</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2019::regular">2019/20 Regular Season</li>
                    </ul>
    </dd>
</dl>');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        // Stat selection.
        $response->assertSee('<scrolltable class="scroll-x scrolltable-player-skater" data-default="3" data-total="17"'
            . ' data-current="points">');
    }

    /**
     * A test of the individual skaters stats page.
     * @return void
     */
    public function testPlayerSkaters(): void
    {
        $response = $this->get('/ahl/stats/players/skater?season=2019&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<h1>AHL Skater Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-IAW">'
            . '<a href="/ahl/players/sam-anas-4959">S. Anas</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-UTI">'
            . '<a href="/ahl/players/reid-boucher-3395">R. Boucher</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-IAW">'
            . '<a href="/ahl/players/gerald-mayhew-5053">G. Mayhew</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-BEL">'
            . '<a href="/ahl/players/josh-norris-6046">J. Norris</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-SYR">'
            . '<a href="/ahl/players/alex-barre-boulet-5591">A. Barre-Boulet</a></span></cell>
            </row>');
    }

    /**
     * A test of the individual goalies stats page.
     * @return void
     */
    public function testPlayerGoalies(): void
    {
        $response = $this->get('/ahl/stats/players/goalie?season=2019&type=regular&stat=goals_against_avg');
        $response->assertStatus(200);
        $response->assertSee('<h1>AHL Goalie Leaders</h1>');

        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Individual Goalie</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li class="sel" data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-TUC">'
            . '<a href="/ahl/players/antti-raanta-3847">A. Raanta</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">=</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-HER">'
            . '<a href="/ahl/players/parker-milner-3960">P. Milner</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">=</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-LV">'
            . '<a href="/ahl/players/felix-sandstrom-5464">F. Sandstrom</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">=</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-ONT">'
            . '<a href="/ahl/players/cole-kehler-5681">C. Kehler</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-STO">'
            . '<a href="/ahl/players/nick-schneider-4762">N. Schneider</a></span></cell>
            </row>');
    }

    /**
     * A test of the team skaters stats page.
     * @return void
     */
    public function testTeamSkaters(): void
    {
        $response = $this->get('/ahl/stats/teams/skater?season=2019&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<h1>AHL Skater Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Skater</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li class="sel" data-id="team::skater">Team Skater</li>
                            <li  data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-BEL">'
            . '<a href="/ahl/teams/belleville-senators">Senators</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-MIL">'
            . '<a href="/ahl/teams/milwaukee-admirals">Admirals</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-TOR">'
            . '<a href="/ahl/teams/toronto-marlies">Marlies</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-CHA">'
            . '<a href="/ahl/teams/charlotte-checkers">Checkers</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-UTI">'
            . '<a href="/ahl/teams/utica-comets">Comets</a></span></cell>
            </row>');
    }

    /**
     * A test of the team goalies stats page.
     * @return void
     */
    public function testTeamGoalies(): void
    {
        $response = $this->get('/ahl/stats/teams/goalie?season=2019&type=regular&stat=save_pct');
        $response->assertStatus(200);
        $response->assertSee('<h1>AHL Goalie Leaders</h1>');

        $response->assertDontSee('<dl class="dropdown " data-id="filter_team"  >');
        $response->assertSee('<dl class="dropdown " data-id="filter_category"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>Team Goalie</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li  data-id="player::skater">Individual Skater</li>
                            <li  data-id="player::goalie">Individual Goalie</li>
                            <li  data-id="team::skater">Team Skater</li>
                            <li class="sel" data-id="team::goalie">Team Goalie</li>
                    </ul>
    </dd>
</dl>');

        $response->assertSee('<main>
        ' . '
        <top >
            <cell class="row-head group">Teams</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-MIL">'
            . '<a href="/ahl/teams/milwaukee-admirals">Admirals</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">2</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-PRO">'
            . '<a href="/ahl/teams/providence-bruins">Bruins</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">3</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-SD">'
            . '<a href="/ahl/teams/san-diego-gulls">Gulls</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">4</cell>
                <cell class="row-0 entity"><span class="icon team-ahl-SPT">'
            . '<a href="/ahl/teams/springfield-thunderbirds">Thunderbirds</a></span></cell>
            </row>
                    <row>
                                <cell class="row-head pos sort_order">5</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-COL">'
            . '<a href="/ahl/teams/colorado-eagles">Eagles</a></span></cell>
            </row>');
    }

    /**
     * A test of a single team players batting stats page.
     * @return void
     */
    public function testTeamStats(): void
    {
        // Invalid team arguments (all ignored, AHL leaders listed).
        $response = $this->get('/ahl/stats/players/skater?team_id=&season=2020&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; Stats &raquo; Players &raquo; Skater</title>');
        $response->assertSee('<h1>AHL Skater Leaders</h1>');
        $response = $this->get('/ahl/stats/players/skater?team_id=SPR&season=2020&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; Stats &raquo; Players &raquo; Skater</title>');
        $response->assertSee('<h1>AHL Skater Leaders</h1>');

        // Correct list.
        $response = $this->get('/ahl/stats/players/skater?team_id=SYR&season=2020&type=regular&stat=points');
        $response->assertStatus(200);
        $response->assertSee('<h1><span class="hidden-m hidden-tp">Syracuse</span> Crunch Skater Leaders</h1>');
        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; Stats &raquo; Crunch &raquo; Skater</title>');

        $response->assertDontSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-IAW">'
            . '<a href="/ahl/players/sam-anas-4959">S. Anas</a></span></cell>
            </row>');
        $response->assertSee('<top >
            <cell class="row-head group">Players</cell>
        </top>
        ' . '
                            <row>
                                <cell class="row-head pos sort_order">1</cell>
                <cell class="row-1 entity"><span class="icon team-ahl-SYR">'
            . '<a href="/ahl/players/alex-barre-boulet-5591">A. Barre-Boulet</a></span></cell>
            </row>');
    }
}
