<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class KAwardsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/ahl/awards');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; AHL Award Winners</title>');
        $response->assertSee('<h1>AHL Award Winners</h1>');

        $response->assertSee('<li class="grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
    <fieldset class="ahl_box award clearfix">
        ' . '
        <h3 class="row-head">Les Cunningham Award</h3>

        ' . '
        <div class="mugshot-small">
                            <img class="mugshot-small" loading="lazy" src="');
        $response->assertSee('" alt="Profile photo of Gerald Mayhew">
                    </div>

        ' . '
        <p class="descrip">Awarded to the player judged most valuable to his team</p>

        ' . '
        <p class="winner">
            <strong>2019/20 Winner:</strong>
                                                <span class="team-ahl-IAW">
                                <a href="/ahl/players/gerald-mayhew-5053">Gerald Mayhew</a>
                                    </span>
                            ' . '
                            (C)
                    </p>

        ' . '
        <p class="link"><a class="all-time" data-link="/ahl/awards/les-cunningham-award-1">All-Time Winners</a></p>
    </fieldset>
</li>');
    }

    /**
     * A test of the all-time winners.
     * @return void
     */
    public function testAllTime(): void
    {
        // A non-award.
        $response = $this->get('/ahl/awards/does-not-exist-99');
        $response->assertStatus(404);
        // An actual award.
        $response = $this->get('/ahl/awards/les-cunningham-award-1');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'season' => '2019/20',
            'winner' => [
                'player' => 'Gerald Mayhew',
                'team' => 'Iowa Wild',
                'team_id' => 'IAW',
                'pos' => 'C',
            ],
        ]);
    }
}
