<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class AHomeTest extends MajorLeagueBaseCase
{
    /**
     * A test of the homepage and game header chrome.
     * @return void
     */
    public function testHomepage(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-03-11 12:34:56');

        $response = $this->get('/ahl');
        $response->assertStatus(200);

        // Check the game headers.
        $response->assertSee('<div class="schedule_header">
    <dl class="num-11">
        <dt class="row-head date">11th Mar</dt>
        <dd>
    <div class="team visitor row-1 icon team-ahl-SA loss">
        SA
                    <span class="score">1</span>
            </div>
    <div class="team home row-0 icon team-ahl-MIL win">
        MIL
                    <span class="score">6</span>
            </div>
    <div class="info row-1">
                    ' . '
            <div class="link"><a href="/ahl/schedule/20200311/rampage-at-admirals-r932" '
            . 'class="icon_right_game_boxscore no_underline">Boxscore</a></div>
            <div class="status">Final</div>
            </div>
</dd>');

        // News.
        $response->assertSee('<ul class="inline_list grid home">
    <li class="grid-cell grid-3-4 grid-t-1-1 grid-m-1-1">
        ' . '
        <ul class="inline_list grid">
            <li class="grid-cell grid-1-2 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <article class="ahl_box">
            <h4><a href="https://www.nhl.com/news/ahl-notebook-new-jersey-devils-prospects-fueling-binghamton/'
            . 'c-316085916#new_tab" rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Prospects'
            . ' fueling turnaround in Binghamton</a></h4>
            <publisher>The AHL</publisher>
            <time>1 hour ago</time>');

        // Standings.
        $response->assertDontSee('<h3 class="row-head">Playoffs</h3>');
        $response->assertSee('<fieldset class="ahl_box">
    <h3 class="row-head">Standings</h3>
    <dl class="standings toggle-target clearfix">
                    <dt class="conf row_conf-43 sel" data-id="43">Western</dt>
                    <dt class="conf row_conf-42 unsel" data-id="42">Eastern</dt>
                            <dd class="conf tab-43 ">
                <dl>
                                            <dt class="row_conf-43">Central</dt>
                                                                                                        '
            . '<dd class="row-1 team-ahl-MIL">
                                <a href="/ahl/teams/milwaukee-admirals">MIL</a>
                                                                    <sup>*</sup>
                                                                                                    '
            . '<div class="col">.714</div>
                                                                    <div class="col">36</div>
                                                                    <div class="col">63</div>
                                                            </dd>');

        // Leaders.
        $response->assertSee('<fieldset class="ahl_box">
    <h3 class="row-head">Leaders</h3>
    <dl class="leaders clearfix">');
        $response->assertSee('<dt class="row-head">Points</dt>
            <dd class="name"><span class="icon team-ahl-IAW">'
            . '<a href="/ahl/players/sam-anas-4959">S. Anas</a></span></dd>
            <dd class="value">70</dd>');

        // Power Ranks.
        $response->assertSee('<h3 class="row-head">Latest Power Ranks</h3>
    <dl class="power_ranks clearfix">
        ' . '
                    <dd class="dates">Games up to 8th March</dd>
                            <dt class="row-head">1</dt>
                <dd class="team row-1 team-ahl-narrow_small-PRO">'
            . '<a href="/ahl/teams/providence-bruins">Bruins</a></dd>');
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/ahl/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear Sports AHL',
            'short_name' => 'DeBear Sports AHL',
            'description' => 'All the latest American Hockey League results, stats and news.',
            'start_url' => '/ahl/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/sports/images/meta/ahl.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
