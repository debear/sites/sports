<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class HTeamsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the team list page.
     * @return void
     */
    public function testList(): void
    {
        $response = $this->get('/ahl/teams');
        $response->assertStatus(200);

        $response->assertSee('<h1>Teams</h1>');
        $response->assertSee('<dt class="row_conf-43   conf">Western Conference</dt>');
        $response->assertSee('<dt class="row_conf-43 div">Central Division</dt>');

        // A team from this season.
        $response->assertSee('<dd class="row-1 team-ahl-small-BID logo"></dd>
                    <dd class="row-1 name">Binghamton Devils</dd>
                    <dd class="row-1 info">
                        <ul class="inline_list">
                            <li><a href="/ahl/teams/binghamton-devils">Home</a></li>'
            . '<li><a href="/ahl/teams/binghamton-devils#schedule">Schedule</a></li>'
            . '<li><a href="/ahl/teams/binghamton-devils#roster">Roster</a></li>
                        </ul>
                    </dd>');

        // But not a team from a previous season.
        $response->assertDontSee('<dd class="row-1 team-ahl-small-BIN logo"></dd>
                    <dd class="row-1 name">Binghamton Senators</dd>
                    <dd class="row-1 info">
                        <ul class="inline_list">
                            <li><a href="/ahl/teams/binghamton-senators">Home</a></li>'
            . '<li><a href="/ahl/teams/binghamton-senators#schedule">Schedule</a></li>'
            . '<li><a href="/ahl/teams/binghamton-senators#roster">Roster</a></li>
                        </ul>
                    </dd>');
    }

    /**
     * A test of an individual team page.
     * @return void
     */
    public function testIndividual(): void
    {
        $response = $this->get('/ahl/teams/syracuse-crunch');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; Teams &raquo; Syracuse Crunch</title>');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="schedule">Schedule</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="roster">Roster</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="standings">Standings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="power-ranks">Power Rankings</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="history">History</item>
                <sep class="right"></sep>
    </desk>');

        $this->individualHeader($response);
        $this->individualSchedule($response);
        $this->individualStandings($response);
        $this->individualStats($response);
        $this->individualLeaders($response);
        $this->individualMisc($response);
    }

    /**
     * A test of the header component of an individual team page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualHeader(TestResponse $response): void
    {
        $response->assertDontSee('<div class="favourite">');
        $response->assertSee('<dl class="inline_list header clearfix">
    ' . '
            <dt class="standing">5th, Northern Division:</dt>
            <dd class="standing">30&ndash;23&ndash;4&ndash;5, 69pts</dd>
        ' . '
                        <dt class="prev game">Last Game:</dt>
            <dd class="prev game">
                Sat 11th Apr
                                    <span class="result-tie">CNC</span>
                                v
                <span class="team-ahl-UTI icon">UTI</span>
            </dd>
        ' . '
        </dl>');
    }

    /**
     * A test of the schedule component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualSchedule(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list recent clearfix">
            <li class="ahl_box">
                        <dl>
                <dt class="row-head">Sun 29th Mar</dt>
                    <dd class="opp team-ahl-narrow_small-right-TOR">
                        @
                    </dd>
                                            <dd class="status row-0">CNC</dd>
                                </dl>
                    </li>');
    }

    /**
     * A test of the standings component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStandings(TestResponse $response): void
    {
        $response->assertSee('<li class="div row_conf-42 ">Northern Division</li>

    ' . '
            <li class="header">
                            <div class="col  row_conf-42">Pts %</div>
                            <div class="col  row_conf-42">ROW</div>
                            <div class="col  row_conf-42">GP</div>
                    </li>
    ' . '
    ' . '
                    <li class="row-1 team">
            <span class="team-ahl-BEL">
                <a href="/ahl/teams/belleville-senators">Senators</a>
            </span>
                            <sup>y</sup>
                                        <div class="row-1 col">.635</div>
                            <div class="row-1 col">35</div>
                            <div class="row-1 col">63</div>
                    </li>');
        $response->assertSee('<li class="row_ahl-SYR team">
            <span class="team-ahl-SYR">
                <a href="/ahl/teams/syracuse-crunch">Crunch</a>
            </span>
                                        <div class="row_ahl-SYR col">.556</div>
                            <div class="row_ahl-SYR col">30</div>
                            <div class="row_ahl-SYR col">62</div>
                    </li>');
    }

    /**
     * A test of the stats component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualStats(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list stats clearfix">
        <li class="row-head title">Team Stats</li>
                    <li class="cell ahl_box">
                <dl class="clearfix">
                    <dt class="row-head">Goals For</dt>
                        <dd class="rank">5th</dd>
                        <dd class="value"><span>200</span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell ahl_box">
                <dl class="clearfix">
                    <dt class="row-head">Goals Against</dt>
                        <dd class="rank">27th</dd>
                        <dd class="value"><span>185</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the team leader component of an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualLeaders(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list leaders clearfix">
        <li class="row-head title">Team Leaders</li>
                    <li class="cell pts ahl_box">
                <dl>
                    <dt class="row-head">Points</dt>
                        <dd class="name"><a href="/ahl/players/alex-barre-boulet-5591">A. Barre-Boulet</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Alex Barre-Boulet"></dd>
                        <dd class="value"><span>56</span></dd>
                </dl>
            </li>');
        $response->assertSee('<li class="cell wins ahl_box">
                <dl>
                    <dt class="row-head">Wins</dt>
                        <dd class="name"><a href="/ahl/players/scott-wedgewood-3662">S. Wedgewood</a></dd>
                        <dd class="mugshot mugshot-small">');
        $response->assertSee('" alt="Profile photo of Scott Wedgewood"></dd>
                        <dd class="value"><span>13</span></dd>
                </dl>
            </li>');
    }

    /**
     * A test of the misc info on an individual team home page.
     * @param TestResponse $response The request response we are testing against.
     * @return void
     */
    protected function individualMisc(TestResponse $response): void
    {
        $response->assertSee('<div class="tab-dropdown"><input type="hidden" id="season-schedule" '
            . 'name="season-schedule" value="2019" data-is-dropdown="true" >
<dl class="dropdown " data-id="season-schedule"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019/20</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2019">2019/20</li>
                            <li  data-id="2018">2018/19</li>
                            <li  data-id="2017">2017/18</li>
                            <li  data-id="2016">2016/17</li>
                            <li  data-id="2015">2015/16</li>
                            <li  data-id="2014">2014/15</li>
                            <li  data-id="2013">2013/14</li>
                            <li  data-id="2012">2012/13</li>
                            <li  data-id="2011">2011/12</li>
                            <li  data-id="2010">2010/11</li>
                            <li  data-id="2009">2009/10</li>
                            <li  data-id="2008">2008/09</li>
                            <li  data-id="2007">2007/08</li>
                            <li  data-id="2006">2006/07</li>
                            <li  data-id="2005">2005/06</li>
                    </ul>
    </dd>
</dl>');
    }

    /**
     * A test of the schedule tab of an individual team page.
     * @return void
     */
    public function testIndividualTabSchedule(): void
    {
        $response = $this->getTab('/ahl/teams/syracuse-crunch/schedule');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJSONCount(76, 'sched');
        $response->assertJsonFragment([
            'date_fmt' => '4th Oct',
            'date_ymd' => '2019-10-04',
            'venue' => '@',
            'opp' => 'team-ahl-right-RCH',
            'opp_wide' => 'team-ahl-narrow_small-right-RCH',
            'opp_id' => 'RCH',
            'opp_city' => 'Rochester',
            'opp_franchise' => 'Americans',
            'link' => '/ahl/schedule/20191004/crunch-at-americans-r2',
            'info' => '<span class="result-loss">OTL 3 &ndash; 2</span>',
        ]);
        $response->assertJSONCount(4, 'gameTabs');
        $response->assertJSON([
            'gameTabs' => [
                [
                    'label' => [
                        'regular' => 'Season Series',
                        'playoff' => 'Playoff Series',
                    ],
                    'icon' => 'series',
                    'link' => 'series',
                ],
                ['label' => 'Scoring', 'icon' => 'scoring', 'link' => 'scoring'],
                ['label' => 'Boxscore', 'icon' => 'boxscore', 'link' => false],
                ['label' => 'Game Stats', 'icon' => 'stats', 'link' => 'stats'],
            ],
        ]);
    }

    /**
     * A test of the roster tab of an individual team page.
     * @return void
     */
    public function testIndividualTabRoster(): void
    {
        $response = $this->getTab('/ahl/teams/syracuse-crunch/roster');
        $response->assertStatus(200);
        $response->assertSee('<dl class="roster">');
        $response->assertSee('<dt class="row-head">Center</dt>
                                            <dd class="row-0 mugshot mugshot-tiny"><img class="mugshot-tiny" '
            . 'loading="lazy" src="');
        $response->assertSee('<dd class="row-0 jersey">#12</dd>
                <dd class="row-0 name">
                    <a href="/ahl/players/alex-barre-boulet-5591">Alex Barre-Boulet</a>
                                                        </dd>
                <dd class="row-0 pos">C</dd>
                <dd class="row-0 height">5&#39; 10&quot;</dd>
                <dd class="row-0 weight">170lb</dd>
                <dd class="row-0 dob">21st May 1997</dd>
                <dd class="row-0 birthplace">Montmagny, QC</dd>');
    }

    /**
     * A test of the standings tab of an individual team page.
     * @return void
     */
    public function testIndividualTabStandings(): void
    {
        $n = null; // To shorten the array lengths.

        $response = $this->getTab('/ahl/teams/syracuse-crunch/standings');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('chart.chart.renderTo', 'chart-standings');
        $response->assertJsonPath('chart.series.0.type', 'spline');
        $response->assertJsonPath('chart.series.0.data', [
            $n, 5, 5, 4, 3, 3, 3, 3, 4, 4, $n, $n, 5, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5, 5, 5, 5, 6, 6, 5, 5, 5, 7, 5, 5, 5,
            5, 5, 4, 3, 3, 3, 4, 7, 5, 5, 5, 6, 5, 5, 5, 5, 6, 5, 5, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6,
            6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 8, 8, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 4,
            4, 4, 5, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n,
            $n, $n, $n, $n, $n, $n, $n, $n, $n, $n,
        ]);
        $response->assertJsonPath('chart.series.1.type', 'column');
        $response->assertJsonPath('chart.series.1.data', [
            $n, 0, 0, 1, 3, 0, 0, 0, 0, 0, $n, $n, 0, 0, 0, 0, 1, 0, 0, 4, 1, 0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 2, 0, 0,
            2, 0, 0, 3, 0, 0, 0, 0, 1, 0, 0, 0, 3, 3, 0, 0, 0, 3, 0, 1, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 3, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 3, 1,
            0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 0, 0, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n,
            $n, $n, $n, $n, $n, $n, $n, $n, $n, $n,
        ]);
        $response->assertJsonPath('chart.series.2.type', 'column');
        $response->assertJsonPath('chart.series.2.data', [
            $n, 0, 0, 0, 0, 0, 0, 0, -4, 0, $n, $n, 0, 0, 0, -1, 0, 0, 0, 0, 0, -2, 0, 0, 0, 0, -2, 0, 0, 0, -1, -3, 0,
            0, 0, 0, 0, 0, 0, 0, 0, -3, -2, 0, 0, 0, -4, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -1, -4, 0, 0, 0, -1, -6, 0, 0,
            -2, 0, 0, -1, 0, 0, -2, 0, -1, 0, 0, -1, 0, -3, 0, 0, 0, 0, -1, -1, -1, -2, -4, 0, 0, 0, 0, -1, 0, 0, 0, 0,
            0, -1, 0, -3, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3, 0, 0, 0, 0, 0, 0, $n, $n, $n, $n, $n, $n,
            $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n, $n,
        ]);
        $response->assertJsonPath('extraJS.standingsRecords.1', '0&ndash;0&ndash;1&ndash;0, 1pt');
        $response->assertJsonPath('extraJS.standingsRecords.121', '30&ndash;23&ndash;4&ndash;5, 69pts');
        $response->assertJsonPath('extraJS.standingsRecords.146', null);
    }

    /**
     * A test of the power rank tab of an individual team page.
     * @return void
     */
    public function testIndividualTabPowerRanks(): void
    {
        $n = null; // To shorten the array lengths.

        $response = $this->getTab('/ahl/teams/syracuse-crunch/power-ranks');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJsonPath('chart.chart.renderTo', 'chart-power-ranks');
        $response->assertJsonPath('chart.series.0.data', [
            8, 26, 21, 13, 18, 15, 6, 14, 15, 10, 14, 24, 22, 16, 19, 17, 17, 13, 10, 9, 16, $n, $n, $n, $n, $n,
        ]);
        $response->assertJsonPath('extraJS.powerRankWeeks.1', 'Week 1, 4th&ndash;13th Oct');
    }

    /**
     * A test of the history tab of an individual team page.
     * @return void
     */
    public function testIndividualTabHistory(): void
    {
        $response = $this->getTab('/ahl/teams/syracuse-crunch/history');
        $response->assertStatus(200);
        $response->assertSee('<dt class="league row-head row_league-AHL">
                <span class="icon misc-ahl-AHL">American Hockey League</span>
            </dt>
            <dd class="league titles row-head row_league-AHL"><span>4 Division Titles</span>; '
            . '<span>2 Conference Titles</span>; <span>0 Kilpatrick Trophies</span>; <span>0 Calder Cups</span></dd>');
        $response->assertSee('<dt class="row-head parts-1 season">2018/19</dt>
                    <dd class="row-0 record">47&ndash;21&ndash;4&ndash;4, 1st Northern</dd>
                ' . '
                    <dd class="row-0 playoffs">
                ' . '
                <ul class="inline_list matchups clearfix">
                                            <li class="result-loss">
                            <span class="hidden-m">Division Semi Final:</span>
                            <span class="hidden-t hidden-d">Div SF:</span>
                            Lost 1&ndash;3
                                                            v <span class="icon team-ahl-CLM">Cleveland Monsters</span>
                                                    </li>
                                    </ul>
            </dd>');
    }

    /**
     * A test of an individual team page that was inactive in the season in question.
     * @return void
     */
    public function testIndividualInactive(): void
    {
        // Tweak the configuration so we are viewing a season the team was not active.
        $key = 'debear.sports.subsites.ahl.setup.season.viewing';
        $this->setTestConfig([$key => 2005]);

        // Make our request and test it.
        $response = $this->get('/ahl/teams/rockford-icehogs');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; Teams &raquo; Rockford IceHogs</title>');

        $response->assertSee('<input type="hidden" id="season-schedule" name="season-schedule" value="2019" '
            . 'data-is-dropdown="true" >
<dl class="dropdown " data-id="season-schedule"  >
    <dt>
        <drop class="icon_scroll_down"></drop>
        <span>2019/20</span>
    </dt>
    <dd class="hidden">
        <ul class="inline_list">
                            <li class="sel" data-id="2019">2019/20</li>
                            <li  data-id="2018">2018/19</li>
                            <li  data-id="2017">2017/18</li>
                            <li  data-id="2016">2016/17</li>
                            <li  data-id="2015">2015/16</li>
                            <li  data-id="2014">2014/15</li>
                            <li  data-id="2013">2013/14</li>
                            <li  data-id="2012">2012/13</li>
                            <li  data-id="2011">2011/12</li>
                            <li  data-id="2010">2010/11</li>
                            <li  data-id="2009">2009/10</li>
                            <li  data-id="2008">2008/09</li>
                            <li  data-id="2007">2007/08</li>
                    </ul>');
        $response->assertSee('<dl class="inline_list header clearfix">
    ' . '
            <dt class="standing">6th, Central Division:</dt>
            <dd class="standing">29&ndash;30&ndash;2&ndash;2, 62pts</dd>
        ' . '
                        <dt class="prev game">Last Game:</dt>
            <dd class="prev game">
                Sat 11th Apr
                                    <span class="result-tie">CNC</span>
                                v
                <span class="team-ahl-IAW icon">IAW</span>
            </dd>
        ' . '
        </dl>');
    }

    /**
     * A test of the various subtleties of an individual team page.
     * @return void
     */
    public function testIndividualMisc(): void
    {
        $response = $this->get('/ahl/teams/unknown-team');
        $response->assertStatus(404);

        $response = $this->get('/ahl/teams/unknown-team/unknown-tab');
        $response->assertStatus(404);

        $response = $this->get('/ahl/teams/unknown-team/schedule');
        $response->assertStatus(404);

        $response = $this->get('/ahl/teams/springfield-falcons');
        $response->assertStatus(308);
        $response->assertRedirect('https://sports.debear.test/ahl/teams/tucson-roadrunners');

        $response = $this->get('/ahl/teams/syracuse-crunch/home');
        $response->assertStatus(404);

        $response = $this->get('/ahl/teams/syracuse-crunch/unknown');
        $response->assertStatus(404);

        $response = $this->get('/ahl/teams/syracuse-crunch/power-ranks');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/ahl/teams/syracuse-crunch#power-ranks');

        $response = $this->getTab('/ahl/teams/syracuse-crunch/power-ranks/2019');
        $response->assertStatus(200);
        $n = null;
        $response->assertJsonPath('chart.series.0.data', [
            8, 26, 21, 13, 18, 15, 6, 14, 15, 10, 14, 24, 22, 16, 19, 17, 17, 13, 10, 9, 16, $n, $n, $n, $n, $n,
        ]);

        $response = $this->getTab('/ahl/teams/syracuse-crunch/power-ranks/2039');
        $response->assertStatus(404);
    }

    /**
     * A test of the user favourite setting.
     * @return void
     */
    public function testFavourites(): void
    {
        // User must be logged in to perform either action.
        $response = $this->post('/ahl/teams/syracuse-crunch/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/ahl/teams/syracuse-crunch/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
        $response->assertJson(['success' => false]);

        // Log in for all future requests.
        $response = $this->post('/login', [
            'username' => 'test_user',
            'password' => 'testlogindetails',
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);

        // Trying actions on a team that doesn't exist should fail.
        $response = $this->post('/ahl/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);
        $response = $this->delete('/ahl/teams/unknown-team/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(404);
        $response->assertJson(['success' => false]);

        // Ensure team is not already a favourite (but has the option to save as such).
        $response = $this->get('/ahl/teams/syracuse-crunch');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // If we try to remove at this point, it should fail.
        $response = $this->delete('/ahl/teams/syracuse-crunch/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Perform the add, and validate the page updates accordingly.
        $response = $this->post('/ahl/teams/syracuse-crunch/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(201);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_is',
            'hover' => 'icon_right_favourite_remove',
        ]);

        $response = $this->get('/ahl/teams/syracuse-crunch');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="rmv">
            <span class="icons icon_favourite_is icon_right_favourite_remove"></span>
            <span class="text">Remove from favourites</span>
        </button>
    </div>');

        // A second attempt should also fail.
        $response = $this->post('/ahl/teams/syracuse-crunch/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(400);
        $response->assertJson(['success' => false]);

        // Now perform a removal, and validate the page updates accordingly.
        $response = $this->delete('/ahl/teams/syracuse-crunch/favourite', [], ['Accept' => 'application/json']);
        $response->assertStatus(200);
        $response->assertJson([
            'success' => true,
            'icon' => 'icon_favourite_not',
            'hover' => 'icon_right_favourite_add',
        ]);

        $response = $this->get('/ahl/teams/syracuse-crunch');
        $response->assertStatus(200);
        $response->assertSee('<div class="favourite">
        <button class="favourite btn_small btn_blue" data-mode="add">
            <span class="icons icon_favourite_not icon_right_favourite_add"></span>
            <span class="text">Add to favourites</span>
        </button>
    </div>');

        // Log the user back out to tidy up.
        $response = $this->post('/logout');
        $response->assertStatus(200);
        $response->assertJsonFragment(['success' => true]);
    }
}
