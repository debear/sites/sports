<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class EStandingsTest extends MajorLeagueBaseCase
{
    /**
     * A test of the default season.
     * @return void
     */
    public function testDefault(): void
    {
        $response = $this->get('/ahl/standings');
        $response->assertStatus(200);

        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; 2019/20 Standings</title>');
        $response->assertSee('<li class="sel" data-id="2019">2019/20</li>');
        $response->assertSee('<h3 class="row_conf-43  ">Western Conference</h3>');
        $response->assertSee('<dt class="row_conf-43 team head">Central Division</dt>
    ' . '
                    <dd class="row-1 team">
            <span class="icon team-ahl-MIL">
                <a href="/ahl/teams/milwaukee-admirals" >Milwaukee Admirals</a>
                            </span>
                            <sup>*</sup>
                    </dd>');
        $response->assertSee('<dl class="standings div">
        ' . '
                    <dt class="row_conf-43 col-first col gp  ">GP</dt>
                    <dt class="row_conf-43  col w  ">W</dt>
                    <dt class="row_conf-43  col l  ">L</dt>
                    <dt class="row_conf-43  col otl  ">OTL</dt>
                    <dt class="row_conf-43  col sol  ">SOL</dt>
                    <dt class="row_conf-43  col pts  ">Pts</dt>
                    <dt class="row_conf-43  col pts_pct  ">Pts %</dt>
                    <dt class="row_conf-43  col gf  ">GF</dt>
                    <dt class="row_conf-43  col ga  ">GA</dt>
                    <dt class="row_conf-43  col gd  ">Diff</dt>
                    <dt class="row_conf-43  col home  ">Home</dt>
                    <dt class="row_conf-43  col visitor  ">Visitor</dt>
                    <dt class="row_conf-43  col div  ">v Div</dt>
                    <dt class="row_conf-43  col recent  ">Last 10</dt>
                ' . '
                                                <dd class="row-1 col-first col gp ">');
    }

    /**
     * A test of specific season
     * @return void
     */
    public function testSpecific(): void
    {
        $response = $this->get('/ahl/standings/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; AHL &raquo; 2019/20 Standings</title>');
    }

    /**
     * A test of the body-only markup production
     * @return void
     */
    public function testBody(): void
    {
        $response = $this->get('/ahl/standings/2019/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; AHL &raquo; 2019/20 Standings</title>');
        $response->assertSee('<h1>2019/20 Standings</h1>');
    }

    /**
     * A test of invalid seasons
     * @return void
     */
    public function testInvalid(): void
    {
        $response = $this->get('/ahl/standings/2000');
        $response->assertStatus(404);
        $response = $this->get('/ahl/standings/2039');
        $response->assertStatus(404);
    }
}
