<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;
use DeBear\Implementations\TestResponse;

class CGameTest extends MajorLeagueBaseCase
{
    /**
     * A test of a single game
     * @return void
     */
    public function testGame(): void
    {
        $response = $this->get('/ahl/schedule/20200311/crunch-at-comets-r936');
        $response->assertStatus(200);

        $response->assertSee('<ul class="inline_list ahl_box game summary">
    <li class="row-head info">
        <span class="status">Final</span>
        <span class="venue">Utica Memorial Auditorium</span>
    </li>
    <li class="title">
        Wednesday 11th March
    </li>');
        $response->assertDontSee('<li class="summary">');
        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="unsel" data-tab="series"><span class="icon_game_series">Season Series</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="scoring"><span class="icon_game_scoring">Scoring</span></item>
                    <sep class="mid"></sep>
            <item class="sel" data-tab="boxscore"><span class="icon_game_boxscore">Boxscore</span></item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="stats"><span class="icon_game_stats">Game Stats</span></item>
                <sep class="right"></sep>
    </desk>');
        $response->assertDontSee('<item class="unsel" data-tab="playbyplay"><span class="icon_game_pbp">'
            . 'Play-by-Play</span></item>');
        $response->assertDontSee('<item class="unsel" data-tab="events"><span class="icon_game_locations">'
            . 'Game Events</span></item>');

        // The individual tabs.
        $this->testGameScoring($response);
        $this->testGameBoxscore($response);
        $this->testGameStats($response);
    }

    /**
     * Test the game's scoring tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameScoring(TestResponse $response): void
    {
        $response->assertSee('<dt class="row-head type">Goals</dt>
                    <dt class="row-head period">1st Period</dt>
                                <dd class="row-0 team-ahl-UTI time">
                                    05:31
                            </dd>
            <dd class="row-0 play">
                <div class="score"><span class="visitor team-ahl-SYR">0</span> &ndash; '
            . '<span class="home team-ahl-right-UTI">1</span></div>
                Power Play Goal, scored by <a href="/ahl/players/justin-bailey-4600">Justin Bailey</a> assisted by '
            . '<a href="/ahl/players/nikolay-goldobin-4435">Nikolay Goldobin</a> and '
            . '<a href="/ahl/players/brogan-rafferty-6052">Brogan Rafferty</a>
            </dd>');

        $response->assertSee('<dt class="row-head period">1st Period</dt>
                                <dd class="row-0 team-ahl-SYR time">12:47</dd>
            <dd class="row-0 play">Minor Penalty on <a href="/ahl/players/cameron-gaunce-2857">Cameron Gaunce</a> '
            . 'for Tripping</dd>');
    }

    /**
     * Test the game's boxscore tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameBoxscore(TestResponse $response): void
    {
        $response->assertSee('<ul class="inline_list ahl_box boxscore clearfix">
        <li class="row_ahl-SYR team"><span class="team-ahl-SYR icon">Syracuse Crunch</span></li>

        ' . '
                <li class="cell section skater row_ahl-SYR">Skaters</li>
        <li class="static skater">
            <ul class="inline_list">
                <li class="cell jersey row_ahl-SYR">#</li>
                <li class="cell cell-head player row_ahl-SYR">Player</li>

                                                        <li class="cell jersey row_ahl-SYR">17</li>
                    <li class="cell name row-0"><a href="/ahl/players/peter-abbandonato-5990"><span class="name-short">'
        . 'P. Abbandonato</span><span class="name-full">Peter Abbandonato</span></a></li>');
        $response->assertSee('<li class="scroll skater">
            <ul class="inline_list">
                <li class="cell cell-head cell-first pos row_ahl-SYR">Pos</li>
                <li class="cell cell-head stat row_ahl-SYR">G</li>
                <li class="cell cell-head stat row_ahl-SYR">A</li>
                <li class="cell cell-head stat row_ahl-SYR">+/-</li>
                <li class="cell cell-head stat row_ahl-SYR">PIM</li>
                <li class="cell cell-head stat row_ahl-SYR">SOG</li>

                                                        <li class="cell cell-first pos row-0">C</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">+1</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>');
        $response->assertSee('<li class="cell section goalie row_ahl-UTI">Goaltenders</li>
        <li class="static goalie">
            <ul class="inline_list">
                <li class="cell jersey row_ahl-UTI">#</li>
                <li class="cell cell-head player row_ahl-UTI">Player</li>

                                                        <li class="cell jersey row_ahl-UTI">64</li>
                    <li class="cell name row-0"><a href="/ahl/players/michael-dipietro-6053"><span class="name-short">'
            . 'M. DiPietro</span><span class="name-full">Michael DiPietro</span></a></li>
                            </ul>
        </li>
        <li class="scroll goalie">
            <ul class="inline_list">
                <li class="cell cell-head cell-first decision row_ahl-UTI">Decision</li>
                <li class="cell cell-head stat_wide row_ahl-UTI">TOI</li>
                <li class="cell cell-head stat row_ahl-UTI">SA</li>
                <li class="cell cell-head stat row_ahl-UTI">GA</li>
                <li class="cell cell-head stat row_ahl-UTI">SV</li>
                <li class="cell cell-head stat_wide row_ahl-UTI">SV%</li>
                <li class="cell cell-head stat_wide row_ahl-UTI">GAA</li>
                <li class="cell cell-head stat row_ahl-UTI">PIM</li>
                <li class="cell cell-head stat row_ahl-UTI">Pts</li>

                                                        <li class="cell cell-first decision loss">Loss</li>
                    <li class="cell stat_wide row-0">57:10</li>
                    <li class="cell stat row-0">26</li>
                    <li class="cell stat row-0">3</li>
                    <li class="cell stat row-0">23</li>
                    <li class="cell stat_wide row-0">.885</li>
                    <li class="cell stat_wide row-0">3.15</li>
                    <li class="cell stat row-0">0</li>
                    <li class="cell stat row-0">0</li>
                            </ul>
        </li>
    </ul>
');
        // Three Stars.
        $response->assertSee('<ul class="inline_list ahl_box three_stars clearfix">
    <li class="row-head">Three Stars</li>
                        <li class="star icon icon_game_star1">
            <div class="row-0 name icon team-ahl-SYR"><a href="/ahl/players/luke-witkowski-3707">'
            . '<span class="hidden-m">Luke Witkowski</span><span class="hidden-t hidden-d">L. Witkowski</span></a></div>
            <img class="mugshot-large" loading="lazy"');
        // Officials.
        $response->assertSee('<ul class="inline_list ahl_box officials clearfix">
    <li class="row-head">Officials</li>

    <li class="row-0 referee">Referees:</li>
    <li class="row-0 linesman">Linesmen:</li>

    <li class="row-1 referee">
                    <p>#18 Michael Miggans</p>
                    <p>#44 Furman South</p>
            </li>
    <li class="row-1 linesman">
                    <p>#23 Adam Bell</p>
                    <p>#68 Neil Frederickson</p>
            </li>
</ul>');
    }

    /**
     * Test the game's stats tab content
     * @param TestResponse $response The response to test against.
     * @return void
     */
    protected function testGameStats(TestResponse $response): void
    {
        $response->assertSee('<dl class="ahl_box comparison clearfix">
    <dt class="row-head">Team Stats</dt>
    <dt class="team visitor row_ahl-SYR team-ahl-SYR"><span class="hidden-m hidden-tp">Syracuse</span> Crunch</dt>
    <dt class="team home row_ahl-UTI team-ahl-UTI"><span class="hidden-m hidden-tp">Utica</span> Comets</dt>');
        $response->assertSee('<dd class="row-0 label">Shots</dd>
<dd class="row-1 stat stat-0-shots">
    <span class="visitor">26</span>
    <div class="bar visitor row_ahl-SYR"></div>
    <div class="bars">
        <div class="visitor row_ahl-SYR"></div>
        <div class="home row_ahl-UTI"></div>
        <div class="gap row_ahl-UTI"></div>
    </div>
    <div class="bar home row_ahl-UTI"></div>
    <span class="home">28</span>
</dd>');
        $response->assertSee('.subnav-stats .stat-0-shots .bars .visitor { width: 48.15%; }
        .subnav-stats .stat-0-shots .bars .home { width: 51.85%; }');
    }

    /**
     * A test of controller actions on invalid parameters
     * @return void
     */
    public function testInvalid(): void
    {
        // Invalid date.
        $response = $this->get('/ahl/schedule/20200230/away-at-home-r1');
        $response->assertStatus(404);

        // Invalid game.
        $response = $this->get('/ahl/schedule/20200229/away-at-home-r1');
        $response->assertStatus(404);
    }
}
