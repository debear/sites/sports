<?php

namespace Tests\PHPUnit\Feature\AHL;

use Tests\PHPUnit\Sports\MajorLeague\PHPUnitBase as MajorLeagueBaseCase;

class IPlayersTest extends MajorLeagueBaseCase
{
    /**
     * A test of the main list.
     * @return void
     */
    public function testList(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-02-28 12:34:56');

        $response = $this->get('/ahl/players');
        $response->assertStatus(200);

        $response->assertSee('<h1>AHL Players</h1>');

        // By Team.
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row_conf-43 ">Western</dt>
                                                    <dd class="row-0 team-ahl-BAK icon">'
            . '<a href="/ahl/teams/bakersfield-condors#roster">Bakersfield Condors</a></dd>');

        // Name Search.
        $response->assertSee('<dl class="ahl_box clearfix">
            <dt class="row-head">Search By Name</dt>
            <dt class="row-1">Player&#39;s Name:</dt>
                <dd class="row-0 name"><input type="text" class="textbox"></dd>
                <dd class="row-0 button"><button class="btn_small btn_green">Search</button></dd>
                <dd class="results hidden"></dd>
        </dl>');

        // Birthdays.
        $response->assertSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
        $response->assertSee('<div>
                <ul class="inline_list">
                                            <li class="row-0">'
            . '<a href="/ahl/players/oscar-dansk-4209">Oscar Dansk</a> (26)</li>
                                    </ul>
            </div>');
    }

    /**
     * A test of the main list with no birthdays.
     * @return void
     */
    public function testListNoBirthdays(): void
    {
        // Set the date to our test date and make the request.
        $this->setTestDateTime('2020-02-29 12:34:56');

        $response = $this->get('/ahl/players');
        $response->assertStatus(200);

        // Available sections.
        $response->assertSee('<h1>AHL Players</h1>');
        $response->assertSee('<h3 class="row-head">By Team</h3>');
        $response->assertSee('<dt class="row-head">Search By Name</dt>');
        $response->assertDontSee('<h3 class="row-head icon_right_cake">Today&#39;s Birthdays</h3>');
    }

    /**
     * A test of the player search.
     * @return void
     */
    public function testSearch(): void
    {
        // Input length.
        $response = $this->get('/ahl/players/search/');
        $response->assertStatus(404); // Must have at least one character.
        $response = $this->get('/ahl/players/search/a');
        $response->assertStatus(400);
        $response = $this->get('/ahl/players/search/abc');
        $response->assertStatus(200);
        $response->assertExactJson([]); // This should also be an empty list.

        // A search result.
        $response = $this->get('/ahl/players/search/tokarski');
        $response->assertStatus(200);
        $response->assertExactJson([[
            'player_id' => 2466,
            'first_name' => 'Dustin',
            'surname' => 'Tokarski',
            'url' => '/ahl/players/dustin-tokarski-2466',
        ]]); // This should also be an empty list.
    }

    /**
     * A test of an individual skater's player page.
     * @return void
     */
    public function testIndividualSkater(): void
    {
        $response = $this->get('/ahl/players/alex-barre-boulet-5591');
        $response->assertStatus(200);
        $response->assertSee('<h1 class="team-ahl-narrow_large-right-SYR">Alex Barre-Boulet'
            . '<span class="hidden-m jersey">#12</span><span class="hidden-m pos">C</span></h1>');

        // Header.
        $response->assertSee('<ul class="inline_list summary">
            <li class="row-head">2019/20 Regular Season:</li>
                            <li class="stat grid-1-3">
                    <dl class="ahl_box">
                        <dt class="row-head">Points</dt>
                            <dd class="row-1 value">56</dd>
                            <dd class="row-0 rank">5th</dd>
                    </dl>
                </li>');
        $response->assertSee('<dl class="details">
                <dt class="height">Height:</dt>
            <dd class="height">5&#39; 10&quot;</dd>');

        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="career">Career Stats</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="gamelog">Game Log</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="heatmaps">Heatmaps</item>
                <sep class="right"></sep>
    </desk>');

        // Summary.
        $response->assertSee('<div class="subnav-home subnav-tab  clearfix">
                            <scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-skater" data-default="0" data-total="19" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-ahl-SYR">SYR</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">60</cell>
                                            <cell class="row-1 starts">19</cell>
                                            <cell class="row-1 goals">27</cell>
                                            <cell class="row-1 assists">29</cell>
                                            <cell class="row-1 points">56</cell>
                                            <cell class="row-1 plus_minus">+3</cell>
                                            <cell class="row-1 pims">22</cell>
                                            <cell class="row-1 pp_goals">9</cell>
                                            <cell class="row-1 pp_assists">9</cell>
                                            <cell class="row-1 pp_points">18</cell>');
        $response->assertSee('<h3 class="games scrolltable-game-skater row-head">Last 10 Games</h3>
<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game scrolltable-game-skater" data-default="0" '
            . 'data-total="18" data-current="start">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Wed 11th Mar</cell>
                <cell class="row-1 opp">
                    @<span class="team-ahl-UTI">UTI</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-win">W 3 &ndash; 1</span>
                    <a class="icon_game_boxscore" href="/ahl/schedule/20200311/crunch-at-comets-r936"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 start"><span class="fa fa-check-circle"></span></cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">0</cell>
                                            <cell class="row-1 points">0</cell>
                                            <cell class="row-1 plus_minus">0</cell>
                                            <cell class="row-1 pims">0</cell>
                                            <cell class="row-1 pp_goals">0</cell>
                                            <cell class="row-1 pp_assists">0</cell>
                                            <cell class="row-1 pp_points">0</cell>');
    }

    /**
     * A test of an individual skater's player career tab.
     * @return void
     */
    public function testIndividualSkaterTabsCareer(): void
    {
        $response = $this->getTab('/ahl/players/alex-barre-boulet-5591/career?group=skater');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="true" '
            . 'data-goalie="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-skater" data-default="0" data-total="19" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-ahl-SYR">SYR</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2018/19</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-0 team"><span class="team-ahl-SYR">SYR</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2018/19</cell>
                <cell class="row-head season-type">Playoff</cell>
                <cell class="row-1 team"><span class="team-ahl-SYR">SYR</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">60</cell>
                                            <cell class="row-1 starts">19</cell>
                                            <cell class="row-1 goals">27</cell>
                                            <cell class="row-1 assists">29</cell>
                                            <cell class="row-1 points">56</cell>
                                            <cell class="row-1 plus_minus">+3</cell>
                                            <cell class="row-1 pims">22</cell>
                                            <cell class="row-1 pp_goals">9</cell>
                                            <cell class="row-1 pp_assists">9</cell>
                                            <cell class="row-1 pp_points">18</cell>
                                            <cell class="row-1 sh_goals">0</cell>
                                            <cell class="row-1 sh_assists">1</cell>
                                            <cell class="row-1 sh_points">1</cell>
                                            <cell class="row-1 gw_goals">3</cell>
                                            <cell class="row-1 shots">172</cell>
                                            <cell class="row-1 shot_pct">15.7</cell>
                                            <cell class="row-1 so_goals">1</cell>
                                            <cell class="row-1 so_shots">3</cell>
                                            <cell class="row-1 so_pct">33.3</cell>
                                    </row>');
    }

    /**
     * A test of an individual skater's player gamelog tab.
     * @return void
     */
    public function testIndividualSkaterTabsGameLog(): void
    {
        $response = $this->getTab('/ahl/players/alex-barre-boulet-5591/gamelog?season=2019%3Aregular&group=skater');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="true" '
            . 'data-goalie="false"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game '
            . 'scrolltable-game-skater" data-default="0" data-total="18" data-current="start">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Wed 11th Mar</cell>
                <cell class="row-1 opp">
                    @<span class="team-ahl-UTI">UTI</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-win">W 3 &ndash; 1</span>
                    <a class="icon_game_boxscore" href="/ahl/schedule/20200311/crunch-at-comets-r936"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 start"><span class="fa fa-check-circle"></span></cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">0</cell>
                                            <cell class="row-1 points">0</cell>
                                            <cell class="row-1 plus_minus">0</cell>
                                            <cell class="row-1 pims">0</cell>
                                            <cell class="row-1 pp_goals">0</cell>
                                            <cell class="row-1 pp_assists">0</cell>
                                            <cell class="row-1 pp_points">0</cell>
                                            <cell class="row-1 sh_goals">0</cell>
                                            <cell class="row-1 sh_assists">0</cell>
                                            <cell class="row-1 sh_points">0</cell>
                                            <cell class="row-1 gw_goals">0</cell>
                                            <cell class="row-1 shots">3</cell>
                                            <cell class="row-1 shot_pct">0.0</cell>
                                            <cell class="row-1 so_goals">&ndash;</cell>
                                            <cell class="row-1 so_shots">&ndash;</cell>
                                            <cell class="row-1 so_pct">&ndash;</cell>
                                    </row>');
    }

    /**
     * A test of an individual skater's player heatmap tab.
     * @return void
     */
    public function testIndividualSkaterTabsHeatmaps(): void
    {
        $response = $this->getTab('/ahl/players/alex-barre-boulet-5591/heatmaps?season=2019%3Aregular&heatmap=shots');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="heatmaps" data-shots="true" '
            . 'data-goals="true"></div>');
        $response->assertSee('<div class="heatmaps-charts">
    ' . '
    <ul class="inline_list heatmaps clearfix">
                    <li class="zone zcol-1 zrow-1"></li>
                    <li class="zone zcol-2 zrow-1"></li>
                    <li class="zone zcol-3 zrow-1"></li>
                    <li class="zone zcol-4 zrow-1"></li>
                    <li class="zone zcol-5 zrow-1"></li>
                    <li class="zone zcol-6 zrow-1"></li>
                    <li class="zone zcol-7 zrow-1"></li>
                    <li class="zone zcol-8 zrow-1"></li>
                    <li class="zone zcol-1 zrow-2"></li>
                    <li class="zone zcol-2 zrow-2"></li>
                    <li class="zone zcol-3 zrow-2"></li>
                    <li class="zone zcol-4 zrow-2"></li>
                    <li class="zone zcol-5 zrow-2"></li>
                    <li class="zone zcol-6 zrow-2"></li>
                    <li class="zone zcol-7 zrow-2"></li>
                    <li class="zone zcol-8 zrow-2"></li>
                    <li class="zone zcol-1 zrow-3"></li>');
        $response->assertSee('<li class="zone zcol-8 zrow-7"></li>
                <li class="rink"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list heatmaps clearfix">
                    <li class="zcell event-0 zcol-1"></li>
                    <li class="zcell event-1 zcol-2"></li>
                    <li class="zcell event-2 zcol-3"></li>
                    <li class="zcell event-3 zcol-4"></li>
                    <li class="zcell event-4 zcol-5"></li>
                    <li class="zcell event-5 zcol-6"></li>
                    <li class="zcell event-6 zcol-7"></li>
                    <li class="zcell event-7 zcol-8"></li>
                    <li class="zcell event-8 zcol-1"></li>
                    <li class="zcell event-9 zcol-2"></li>
                    <li class="zcell event-10 zcol-3"></li>
                    <li class="zcell event-11 zcol-4"></li>
                    <li class="zcell event-12 zcol-5"></li>
                    <li class="zcell event-13 zcol-6"></li>
                    <li class="zcell event-14 zcol-7"></li>
                    <li class="zcell event-15 zcol-8"></li>
                    <li class="zcell event-16 zcol-1"></li>');
        $response->assertSee('.heatmaps-charts .zcell.event-2 { background-color: #0000ee; }
                                            .heatmaps-charts .zcell.event-4 { background-color: #0000ee; }');
    }

    /**
     * A test of the individual goalie's player page.
     * @return void
     */
    public function testIndividualGoalie(): void
    {
        $response = $this->get('/ahl/players/kaapo-kahkonen-5560');
        $response->assertStatus(200);
        $response->assertSee('<h1 class="team-ahl-narrow_large-right-IAW">Kaapo Kahkonen'
            . '<span class="hidden-m jersey">#34</span><span class="hidden-m pos">G</span></h1>');

        // Header.
        $response->assertSee('<ul class="inline_list summary">
            <li class="row-head">2019/20 Regular Season:</li>
                            <li class="stat grid-1-3">
                    <dl class="ahl_box">
                        <dt class="row-head">Wins</dt>
                            <dd class="row-1 value">25</dd>
                            <dd class="row-0 rank">1st</dd>
                    </dl>
                </li>');
        $response->assertSee('<dl class="details">
                <dt class="height">Height:</dt>
            <dd class="height">6&#39; 2&quot;</dd>');

        $response->assertSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="home">Summary</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="career">Career Stats</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="gamelog">Game Log</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="heatmaps">Heatmaps</item>
                <sep class="right"></sep>
    </desk>');

        // Summary.
        $response->assertSee('<div class="subnav-home subnav-tab  clearfix">
                            <scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-goalie" data-default="0" data-total="21" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-ahl-IAW">IAW</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">34</cell>
                                            <cell class="row-1 starts">34</cell>
                                            <cell class="row-1 win">25</cell>
                                            <cell class="row-1 loss">6</cell>
                                            <cell class="row-1 ot_loss">1</cell>
                                            <cell class="row-1 so_loss">2</cell>
                                            <cell class="row-1 goals_against_avg">2.07</cell>
                                            <cell class="row-1 save_pct">.927</cell>
                                            <cell class="row-1 shutout">7</cell>
                                            <cell class="row-1 goals_against">71</cell>
                                            <cell class="row-1 shots_against">972</cell>
                                            <cell class="row-1 saves">901</cell>
                                            <cell class="row-1 minutes_played">2059:05</cell>
                                            <cell class="row-1 so_goals_against">4</cell>
                                            <cell class="row-1 so_shots_against">9</cell>
                                            <cell class="row-1 so_saves">5</cell>
                                            <cell class="row-1 so_save_pct">55.6</cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">0</cell>
                                            <cell class="row-1 points">0</cell>
                                            <cell class="row-1 pims">2</cell>
                                    </row>');
    }

    /**
     * A test of the individual goalie's player career tab.
     * @return void
     */
    public function testIndividualGoalieTabsCareer(): void
    {
        $response = $this->getTab('/ahl/players/kaapo-kahkonen-5560/career?group=goalie');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="false" '
            . 'data-goalie="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-season '
            . 'scrolltable-season-goalie" data-default="0" data-total="21" data-current="gp">
    <main>
        ' . '
        <top>
            <cell class="row-head season">Season</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head season">2019/20</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-1 team"><span class="team-ahl-IAW">IAW</span></cell>
            </row>
                                <row>
                <cell class="row-head season">2018/19</cell>
                <cell class="row-head season-type">Regular</cell>
                <cell class="row-0 team"><span class="team-ahl-IAW">IAW</span></cell>
            </row>
            </main>');
        $response->assertSee('<row>
                                            <cell class="row-1 gp">34</cell>
                                            <cell class="row-1 starts">34</cell>
                                            <cell class="row-1 win">25</cell>
                                            <cell class="row-1 loss">6</cell>
                                            <cell class="row-1 ot_loss">1</cell>
                                            <cell class="row-1 so_loss">2</cell>
                                            <cell class="row-1 goals_against_avg">2.07</cell>
                                            <cell class="row-1 save_pct">.927</cell>
                                            <cell class="row-1 shutout">7</cell>
                                            <cell class="row-1 goals_against">71</cell>
                                            <cell class="row-1 shots_against">972</cell>
                                            <cell class="row-1 saves">901</cell>
                                            <cell class="row-1 minutes_played">2059:05</cell>
                                            <cell class="row-1 so_goals_against">4</cell>
                                            <cell class="row-1 so_shots_against">9</cell>
                                            <cell class="row-1 so_saves">5</cell>
                                            <cell class="row-1 so_save_pct">55.6</cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">0</cell>
                                            <cell class="row-1 points">0</cell>
                                            <cell class="row-1 pims">2</cell>
                                    </row>');
    }

    /**
     * A test of the individual goalie's player gamelog tab.
     * @return void
     */
    public function testIndividualGoalieTabsGameLog(): void
    {
        // Game Log.
        $response = $this->getTab('/ahl/players/kaapo-kahkonen-5560/gamelog?season=2019%3Aregular&group=goalie');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="groups" data-skater="false" '
            . 'data-goalie="true"></div>');
        $response->assertSee('<scrolltable class="scroll-x scrolltable-non-responsive scrolltable-game '
            . 'scrolltable-game-goalie" data-default="0" data-total="17" data-current="decision">
    <main>
        ' . '
        <top>
            <cell class="row-head game">Game</cell>
        </top>
        ' . '
                                <row>
                <cell class="row-head date">Mon 2nd Mar</cell>
                <cell class="row-1 opp">
                    @<span class="team-ahl-SJ">SJ</span>
                </cell>
                <cell class="row-1 score">
                    <span class="result-loss">OTL 2 &ndash; 1</span>
                    <a class="icon_game_boxscore" href="/ahl/schedule/20200302/wild-at-barracuda-r864"></a>
                </cell>
            </row>');
        $response->assertSee('</top>
            ' . '
                                            <row>
                                            <cell class="row-1 decision"><span class="tie">OT Loss</span></cell>
                                            <cell class="row-1 start"><span class="fa fa-check-circle"></span></cell>
                                            <cell class="row-1 goals_against_avg">1.89</cell>
                                            <cell class="row-1 save_pct">.926</cell>
                                            <cell class="row-1 shutout">&ndash;</cell>
                                            <cell class="row-1 goals_against">2</cell>
                                            <cell class="row-1 shots_against">27</cell>
                                            <cell class="row-1 saves">25</cell>
                                            <cell class="row-1 minutes_played">63:20</cell>
                                            <cell class="row-1 so_goals_against">&ndash;</cell>
                                            <cell class="row-1 so_shots_against">&ndash;</cell>
                                            <cell class="row-1 so_saves">&ndash;</cell>
                                            <cell class="row-1 so_save_pct">&ndash;</cell>
                                            <cell class="row-1 goals">0</cell>
                                            <cell class="row-1 assists">0</cell>
                                            <cell class="row-1 points">0</cell>
                                            <cell class="row-1 pims">0</cell>
                                    </row>');
    }

    /**
     * A test of the individual goalie's player heatmap tab.
     * @return void
     */
    public function testIndividualGoalieTabsHeatmaps(): void
    {
        $response = $this->getTab('/ahl/players/kaapo-kahkonen-5560/heatmaps?season=2019%3Aregular&heatmap=shots');
        $response->assertStatus(200);
        $response->assertSee('<div class="hidden dropdown-filters" data-dropdown="heatmaps" data-saves="true"></div>');
        $response->assertSee('<div class="heatmaps-charts">
    ' . '
    <ul class="inline_list heatmaps clearfix">
                    <li class="zone zcol-1 zrow-1"></li>
                    <li class="zone zcol-2 zrow-1"></li>
                    <li class="zone zcol-3 zrow-1"></li>
                    <li class="zone zcol-4 zrow-1"></li>
                    <li class="zone zcol-5 zrow-1"></li>
                    <li class="zone zcol-6 zrow-1"></li>
                    <li class="zone zcol-7 zrow-1"></li>
                    <li class="zone zcol-8 zrow-1"></li>
                    <li class="zone zcol-1 zrow-2"></li>
                    <li class="zone zcol-2 zrow-2"></li>
                    <li class="zone zcol-3 zrow-2"></li>
                    <li class="zone zcol-4 zrow-2"></li>
                    <li class="zone zcol-5 zrow-2"></li>
                    <li class="zone zcol-6 zrow-2"></li>
                    <li class="zone zcol-7 zrow-2"></li>
                    <li class="zone zcol-8 zrow-2"></li>
                    <li class="zone zcol-1 zrow-3"></li>');
        $response->assertSee('<li class="zone zcol-8 zrow-7"></li>
                <li class="rink"></li>
    </ul>');
        $response->assertSee('<ul class="inline_list heatmaps clearfix">
                    <li class="zcell event-0 zcol-1"></li>
                    <li class="zcell event-1 zcol-2"></li>
                    <li class="zcell event-2 zcol-3"></li>
                    <li class="zcell event-3 zcol-4"></li>
                    <li class="zcell event-4 zcol-5"></li>
                    <li class="zcell event-5 zcol-6"></li>
                    <li class="zcell event-6 zcol-7"></li>
                    <li class="zcell event-7 zcol-8"></li>
                    <li class="zcell event-8 zcol-1"></li>
                    <li class="zcell event-9 zcol-2"></li>
                    <li class="zcell event-10 zcol-3"></li>
                    <li class="zcell event-11 zcol-4"></li>
                    <li class="zcell event-12 zcol-5"></li>
                    <li class="zcell event-13 zcol-6"></li>
                    <li class="zcell event-14 zcol-7"></li>
                    <li class="zcell event-15 zcol-8"></li>
                    <li class="zcell event-16 zcol-1"></li>');
        $response->assertSee('.heatmaps-charts .zcell.event-2 { background-color: #880000; }
                                .heatmaps-charts .zcell.event-3 { background-color: #840000; }');
    }

    /**
     * A test of miscellaneous controller handling.
     * @return void
     */
    public function testIndividualControllerMisc(): void
    {
        // Unknown player (page).
        $response = $this->get('/ahl/players/unknown-player-0000');
        $response->assertStatus(404);

        // Unknown player (tab).
        $response = $this->get('/ahl/players/unknown-player-0000/heatmaps');
        $response->assertStatus(404);

        // Unknown tab.
        $response = $this->get('/ahl/players/unknown-player-0000/unknown');
        $response->assertStatus(404);
        $response = $this->get('/ahl/players/kaapo-kahkonen-5560/unknown');
        $response->assertStatus(404);

        // Tab missing the special header.
        $response = $this->get('/ahl/players/kaapo-kahkonen-5560/heatmaps');
        $response->assertStatus(307);
        $response->assertRedirect('https://sports.debear.test/ahl/players/kaapo-kahkonen-5560#heatmaps');

        // Empty data.
        $response = $this->getTab('/ahl/players/kaapo-kahkonen-5560/heatmaps?season=2010%3Aregular');
        $response->assertStatus(404);
    }
}
