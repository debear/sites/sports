<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;
use DeBear\Repositories\Time;

class AGlobalTest extends FeatureTestCase
{
    /**
     * Whether or not we should refresh the database between runs
     * @var boolean
     */
    protected $refresh_database = false;
    /**
     * The array of subsites we host.
     * @var array
     */
    protected $subsites = [
        'f1' => 'Formula One',
        'sgp' => 'Speedway Grand Prix',
        'nfl' => 'NFL',
        'mlb' => 'MLB',
        'ahl' => 'AHL',
        'nhl' => 'NHL',
    ];

    /**
     * A test of the main sports homepage.
     * @return void
     */
    public function testHomepage(): void
    {
        // Set the date to our test date and make the request.
        $orig_time = Time::object()->getServerNowFmt();
        Time::object()->resetCache('2019-09-22 12:34:56');
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('<h1>Welcome to DeBear Sports!</h1>');

        // Header nav.
        foreach ($this->subsites as $code => $label) {
            $response->assertSee("<a class=\"item no-alt\" href=\"/$code\">
            <span class=\"icon_sports_{$code}_icon\">
        <span class=\"hidden-d\">$label</span>
            </span>
    </a>");
        }

        // News.
        $response->assertSee('<ul class="inline_list home clearfix">
    <li class="news-cell">
        <articles>
    <ul class="inline_list articles grid">
                    <li class="grid-cell grid-1-2 grid-t-1-1 grid-m-1-1">
                <article class="grid-cell grid-cell box section">
            <sport class="sport icon_sports_mlb_icon"></sport>
        <h4><a href="https://sports.yahoo.com/rival-cubs-brewers-set-key-rubber-match-082851712--mlb.html?src=rss" '
            . 'rel="noopener noreferrer" target="_blank" title="Opens in new tab/window">Rival Cubs, Brewers set for '
            . 'key rubber match</a></h4>');

        // F1.
        $response->assertSee('<dt class="row-head">Yesterday</dt>
            <dt class="row-head-track">Formula One</dt>
            <dd class="motorsport clearfix">
            <ul class="inline_list race">
                <li class="row-0-track race"><span class="flag_right flag16_right_sg">Singapore Grand Prix '
            . 'Qualifying</span></li>
                                    ' . '
                    <li class="row-head-track res">Qualifying Result</li>
                                            <li class="row-head-track pos">1</li>
                        <li class="podium-1 participant ">
                            <span class="flag_right flag16_right_mc"><a href="/f1/charles-leclerc-73">Charles '
            . 'Leclerc</a></span>
                        </li>
                                                    <li class="podium-1 team" title="Ferrari">Ferrari</li>');

        // SGP.
        $response->assertSee('<dt class="row-head-track">Speedway Grand Prix</dt>
            <dd class="motorsport clearfix">
            <ul class="inline_list race">
                <li class="row-0-track race"><span class="flag_right flag16_right_gb">British Grand Prix</span></li>
                                    ' . '
                    <li class="row-head-track res">Result</li>
                                            <li class="row-head-track pos">1</li>
                        <li class="podium-1 participant no-team">
                            <span class="flag_right flag16_right_dk"><a href="/sgp/leon-madsen-19">'
            . 'Leon Madsen</a></span>
                        </li>');

        // MLB.
        $response->assertSee('<dt class="row-head-grass">MLB</dt>
                    <dd class="major_league row-1-grass ">
            <div class="visitor">
                <span class="team-mlb-TOR icon">
                    <span class="hidden-m hidden-tp">TOR</span>
                    <span class="hidden-d hidden-tl"><span class="hidden-m">Toronto </span>Blue Jays</span>
                </span>
                                    <span class="score loser">2</span>
                            </div>
            <div class="home">
                <span class="team-mlb-NYY icon">
                    <span class="hidden-m hidden-tp">NYY</span>
                    <span class="hidden-d hidden-tl"><span class="hidden-m">New York </span>Yankees</span>
                </span>
                                    <span class="score winner">7</span>
                            </div>
            <div class="status">
                                    F
                    <a class="icon_game_boxscore" href="/mlb/schedule/20190921/blue-jays-at-yankees-r2310"></a>
                            </div>
                    </dd>');

        // Restore the modified time.
        Time::object()->resetCache($orig_time);
    }

    /**
     * A test of the main sports sitemap.
     * @return void
     */
    public function testSitemap(): void
    {
        $response = $this->get('/sitemap');
        $response->assertStatus(200);

        // Subsites.
        $response->assertSee('<h1>Find your way round DeBear Sports!</h1>');
        $response->assertSee('<a class="no_underline" href="/f1">
                    <span class="logo logo_sports_f1_zoom"></span>
                    <span class="label">Formula One</span>
                </a>');
        $response->assertSee('<a class="no_underline" href="/sgp">
                    <span class="logo logo_sports_sgp_zoom"></span>
                    <span class="label">Speedway Grand Prix</span>
                </a>');
        $response->assertSee('<a class="no_underline" href="/nfl">
                    <span class="logo logo_sports_nfl_zoom"></span>
                    <span class="label">NFL</span>
                </a>');
        $response->assertSee('<a class="no_underline" href="/mlb">
                    <span class="logo logo_sports_mlb_zoom"></span>
                    <span class="label">MLB</span>
                </a>');
        $response->assertSee('<a class="no_underline" href="/nhl">
                    <span class="logo logo_sports_nhl_zoom"></span>
                    <span class="label">NHL</span>
                </a>');
        $response->assertSee('<a class="no_underline" href="/ahl">
                    <span class="logo logo_sports_ahl_zoom"></span>
                    <span class="label">AHL</span>
                </a>');

        // Others.
        $response->assertDontSee('<dt><a href="/">Home</a></dt>'); // Intentionally hidden.
        $response->assertSee('<dt><a href="/sitemap">Sitemap</a></dt>');
        $response->assertSee('<dt><a href="/terms-of-use">Terms of Use</a></dt>');
        $response->assertSee('<dt><a href="/privacy-policy">Privacy Policy</a></dt>');
        $response->assertDontSee('<dt><a href="/f1">Formula One</a></dt>'); // Not an "other".
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear Sports',
            'short_name' => 'DeBear Sports',
            'description' => 'DeBear Sports - all the latest results, stats and news from the world of sport.',
            'start_url' => '/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/sports/images/meta/sports.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
