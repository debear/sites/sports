<?php

namespace Tests\PHPUnit\Feature\SGP;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class FParticipantTest extends MotorsportBaseCase
{
    /**
     * A test of a current SGP participant list
     * @return void
     */
    public function testParticipantList(): void
    {
        $response = $this->get('/sgp/riders');
        $response->assertStatus(200);

        $response->assertSee('<fieldset class="section perm profile">
                <div class="row_head num">15</div>
                <h3 class="row_head"><span class="flag_right flag16_right_us">Greg Hancock</span></h3>
                                    <h4 class="champs podium-1">4x World Champion (1997, 2011, 2014 &amp; 2016)</h4>');
        $response->assertSee('Greg Hancock">
                <dl class="bio inline_list">
                                            <dt>Born:</dt>
                            <dd class="born clear" title="3rd June 1970, Whittier">3rd June 1970, Whittier</dd>
                                        <dt>Experience:</dt>
                        <dd>27th season</dd>
                    <dt>Career Victories:</dt>
                        <dd>21</dd>
                    <dt>Career Podiums:</dt>
                        <dd>69</dd>
                    <dt>Career Points:</dt>
                        <dd>2,668 pts</dd>
                </dl>
                <a class="link" href="/sgp/riders/greg-hancock-4/2020">Rider Profile &raquo;</a>
            </fieldset>');
    }

    /**
     * A test of a previous season's SGP participant list
     * @return void
     */
    public function testParticipantListHistorical(): void
    {
        $response = $this->get('/sgp/riders/2019');
        $response->assertStatus(200);

        // Permanent.
        $response->assertSee('<fieldset class="section perm profile">
                <div class="row_head num">15</div>
                <h3 class="row_head"><span class="flag_right flag16_right_dk">Leon Madsen</span></h3>');
        $response->assertSee('Leon Madsen">
                <dl class="bio inline_list">
                                            <dt>Born:</dt>
                            <dd class="born clear" title="5th September 1988, Vejle">5th September 1988, Vejle</dd>
                                        <dt>Experience:</dt>
                        <dd>4th season</dd>
                    <dt>Career Victories:</dt>
                        <dd>3</dd>
                    <dt>Career Podiums:</dt>
                        <dd>7</dd>
                    <dt>Career Points:</dt>
                        <dd>163 pts</dd>
                </dl>
                <a class="link" href="/sgp/riders/leon-madsen-19/2019">Rider Profile &raquo;</a>
            </fieldset>');

        // Substitute.
        $response->assertSee('<fieldset class="section subs profile">
                <h3 class="row_head"><span class="flag_right flag16_right_au">Max Fricke</span></h3>');
        $response->assertSee('<h4>Meetings Ridden</h4>
                <ul class="inline_list">
                                                                                                                    <li>
                                <span class="flag_right flag16_right_pl"><a href="/sgp/meetings/2019-warsaw">Warsaw</a>'
            . '</span> &ndash; 3 pts
                            </li>
                                                                '
            . '                                                        <li>
                                <span class="flag_right flag16_right_cz"><a href="/sgp/meetings/2019-prague">'
            . 'Czech Republic</a></span> &ndash; 13 pts
                            </li>
                                                                '
            . '                                                        <li>
                                <span class="flag_right flag16_right_se"><a href="/sgp/meetings/2019-hallstavik">Sweden'
            . '</a></span> &ndash; 11 pts
                            </li>
                                                                '
            . '                                                        <li>
                                <span class="flag_right flag16_right_pl"><a href="/sgp/meetings/2019-wroclaw">'
            . 'Wroc&#322;aw</a></span> &ndash; 4 pts
                            </li>
                                                                '
            . '                                                        <li>
                                <span class="flag_right flag16_right_sca"><a href="/sgp/meetings/2019-malilla">'
            . 'Scandinavia</a></span> &ndash; 5 pts
                            </li>
                                                            </ul>
                <a class="link" href="/sgp/riders/max-fricke-127/2019">Rider Profile &raquo;</a>');

        // Per Meeting.
        $response->assertSee('<fieldset class="section meeting">
                <h3 class="row_head flag flag48_pl">Warsaw Grand Prix</h3>
                <dl class="inline_list participants">
                                                                                                                '
            . '<dt class="type">Wildcard</dt>
                                                                                <dt class="num row_head ">#16</dt>
                            <dd class="participant row_1  score"><span class="flag_right flag16_right_pl">'
            . '<a href="/sgp/riders/bartosz-smektala-150/2019"');
        $response->assertSee('<dt class="type">Track Reserve</dt>
                                                                                '
            . '<dt class="num row_head secondary">#17</dt>
                            <dd class="participant row_0 secondary no-score"><span class="flag_right flag16_right_pl">'
            . '<a href="/sgp/riders/dominic-kubera-156/2019"');
        $response->assertSee('<dt class="num row_head secondary">#18</dt>
                            <dd class="participant row_1 secondary no-score">'
            . '<span class="flag_right flag16_right_pl"><a href="/sgp/riders/rafal-karczmarz-122/2019"');
    }

    /**
     * A test of a current SGP participant
     * @return void
     */
    public function testParticipantIndividual(): void
    {
        $response = $this->get('/sgp/riders/greg-hancock-4');
        $response->assertStatus(200);

        // Header and Summary.
        $response->assertSee('<title>DeBear Sports &raquo; Speedway Grand Prix &raquo; Riders &raquo; '
            . 'Greg Hancock</title>');
        $response->assertSee('<li>
    <flag  class="flag pl dim"></flag>
    <info>28th Mar</info>
</li>');
        $response->assertSee('<ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Born:</strong>
                3rd June 1970, Whittier
            </span>

            <span>
                <strong>Championships:</strong>
                                    <abbrev title="1997, 2011, 2014 &amp; 2016">
                                4
                                    </abbrev>
                            </span>

            <span>
                <strong>Experience:</strong>
                27th Season
            </span>
        </li>');

        // Chart.
        $response->assertSee('var chartHistory = {
                    2020: {"chart":');
        $response->assertSee('"series":[{"name":"Championship Position","yAxis":0,"type":"spline","data":[null,null'
            . ',null,null,null,null,null,null,null,null],"zIndex":3},{"name":"Championship Points","yAxis":1,"type":'
            . '"column","data":[null,null,null,null,null,null,null,null,null,null],"zIndex":0},{"name":"Meeting Result"'
            . ',"yAxis":0,"type":"spline","data":[null,null,null,null,null,null,null,null,null,null],"zIndex":2}]');
        $response->assertSee('[{"name":"Championship Position","yAxis":0,"type":"spline","data":[null,null,null,null,'
            . 'null,null,null,null,null,null],"zIndex":3},{"name":"Championship Points","yAxis":1,"type":"column",'
            . '"data":[null,null,null,null,null,null,null,null,null,null],"zIndex":0},{"name":"Meeting Result","yAxis"'
            . ':0,"type":"spline","data":[null,null,null,null,null,null,null,null,null,null],"zIndex":2}]');
        $response->assertSee('var chart = new Highcharts.Chart(chartHistory[2020]);');

        // Stats.
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 hidden">
                <dl data-id="6" class="stat">
                    <dt class="cell row_1" data-css="row_1">Number of Heats:</dt>
                        <dd class="cell value row_1 " data-css="row_1 ">61</dd>
                        <dd class="cell rank row_1" data-css="row_1">T-4th</dd>
                </dl>
            </li>');
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 hidden">
                <dl data-id="9" class="stat">
                    <dt class="cell row_0" data-css="row_0">Points Scored per Heat:</dt>
                        <dd class="cell value row_0 " data-css="row_0 ">1.67</dd>
                        <dd class="cell rank row_0" data-css="row_0">10th</dd>
                </dl>
            </li>');
        $response->assertSee('<fieldset class="no-stats info icon icon_info">The 2020 season has not yet '
            . 'started.</fieldset>');

        // History.
        $response->assertSee('<a href="/sgp/standings">2020</a>
                ' . '
                                    <span class="in-progress"></span>
                            </div>
                            <div class="row_1 first-row current display num-1 pts">
                    0pts
                </div>
                <div class="row_1 first-row current display num-1 pos">
                    15th
                </div>');
        $response->assertSee('<div class="row_0 full">Did not compete in 2019</div>');
        $response->assertSee('<div class="race-cell  res-pts_final" data-pos="4" data-pts="12" data-res="4" >12</div>');
    }

    /**
     * A test of a previous season's SGP participant
     * @return void
     */
    public function testParticipantHistorical(): void
    {
        $response = $this->get('/sgp/riders/robert-lambert-119/2019');
        $response->assertStatus(200);

        // Header and Summary.
        $response->assertSee('<title>DeBear Sports &raquo; Speedway Grand Prix &raquo; Riders &raquo; '
            . 'Robert Lambert</title>');
        $response->assertSee('<li>
    <a href="/sgp/meetings/2019-warsaw" class="flag pl "></a>
    <info><span class="flag_right flag16_right_dk">Madsen</span></info>
</li>');
        $response->assertSee('<ul class="inline_list summary">
        <li class="info">
            <span>
                <strong>Born:</strong>
                <em>Unknown</em>
            </span>

            <span>
                <strong>Championships:</strong>
                                0
                            </span>

            <span>
                <strong>Experience:</strong>
                4th Season
            </span>
        </li>

        <li class="standings">
            <span>
                <strong>2019 Position:</strong>
                15th
            </span>

            <span>
                <strong>2019 Points:</strong>
                39pts
            </span>
        </li>');

        // Chart.
        $response->assertSee('var chartHistory = {
                    2019: {"chart":');
        $response->assertDontSee('"series":[{"name":"Championship Position","yAxis":0,"type":"spline","data":[null,null'
            . ',null,null,null,null,null,null,null,null],"zIndex":3},{"name":"Championship Points","yAxis":1,"type":'
            . '"column","data":[null,null,null,null,null,null,null,null,null,null],"zIndex":0},{"name":"Meeting Result"'
            . ',"yAxis":0,"type":"spline","data":[null,null,null,null,null,null,null,null,null,null],"zIndex":2}]');
        $response->assertSee('"series":[{"name":"Championship Position","yAxis":0,"type":"spline",'
            . '"data":[8,9,11,14,15,16,16,16,15,15],"zIndex":3},{"name":"Championship Points","yAxis":1,'
            . '"type":"column","data":[8,15,21,24,24,24,28,31,37,39],"zIndex":0},{"name":"Meeting Result",'
            . '"yAxis":0,"type":"spline","data":[8,8,11,16,null,null,13,15,11,16],"zIndex":2}]');
        $response->assertSee('var chart = new Highcharts.Chart(chartHistory[2019]);');

        // Stats.
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 ">
                <dl data-id="6" class="stat">
                    <dt class="cell row_1" data-css="row_1">Number of Heats:</dt>
                        <dd class="cell value row_1 " data-css="row_1 ">42</dd>
                        <dd class="cell rank row_1" data-css="row_1">15th</dd>
                </dl>
            </li>');
        $response->assertSee('<li class="item grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1 ">
                <dl data-id="9" class="stat">
                    <dt class="cell row_0" data-css="row_0">Points Scored per Heat:</dt>
                        <dd class="cell value row_0 " data-css="row_0 ">0.93</dd>
                        <dd class="cell rank row_0" data-css="row_0">21st</dd>
                </dl>
            </li>');
        $response->assertDontSee('<fieldset class="no-stats info icon icon_info">The 2020 season has not yet '
            . 'started.</fieldset>');

        // History.
        $response->assertSee('<div class="row_head full no-participate">
                Rider no longer active in Grand Prix Speedway
            </div>');
        $response->assertSee('<a href="/sgp/standings/2019">2019</a>
                ' . '
                            </div>
                            <div class="row_1 first-row display num-1 pts">
                    39pts
                </div>
                <div class="row_1 first-row display num-1 pos">
                    15th
                </div>');
        $response->assertSee('<div class="row_1 full">Did not compete in 2017</div>');
        $response->assertSee('<div class="race-cell first-race res-pts_semi" data-pos="8" data-pts="8" '
            . 'data-res="8" >8</div>');
    }

    /**
     * A test of miscellaneous SGP participant controller actions
     * @return void
     */
    public function testParticipantMisc(): void
    {
        // Rider not found / mis-match between name and ID.
        $response = $this->get('/sgp/riders/no-matches-0');
        $response->assertStatus(404);
        $response = $this->get('/sgp/riders/name-mismatch-1');
        $response->assertStatus(404);

        // Rider not active in quoted season.
        $response = $this->get('/sgp/riders/robert-lambert-119');
        $response->assertStatus(200);
        $response->assertSee('<strong>2019 Position:</strong>');

        // Legacy routes - current season.
        $response = $this->get('/sgp/rider-4');
        $response->assertStatus(308);
        $response->assertRedirect('/sgp/riders/greg-hancock-4/2020');

        // Legacy routes - stating season.
        $response = $this->get('/sgp/2020/rider-4');
        $response->assertStatus(308);
        $response->assertRedirect('/sgp/riders/greg-hancock-4/2020');

        $response = $this->get('/sgp/2020/rider-0');
        $response->assertStatus(404);
    }
}
