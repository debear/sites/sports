<?php

namespace Tests\PHPUnit\Feature\SGP;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class AHomeTest extends MotorsportBaseCase
{
    /**
     * A test of the SGP homepage.
     * @return void
     */
    public function testHomepage(): void
    {
        $response = $this->get('/sgp');
        $response->assertStatus(200);

        // Header.
        $response->assertSee('<flag  class="flag pl dim"></flag>
    <info>28th Mar</info>');
        $response->assertSee('<flag  class="flag de dim"></flag>
    <info>30th May</info>');

        // Last / Next Race.
        $response->assertSee('<h3 class="row_head">Last Meeting: 2019 <span class="flag_right flag16_right_pl">'
            . 'Toru&#324; Grand Prix</span></h3>');
        $response->assertSee('<dd class="podium-1 participant no-team">
                <span class="flag_right flag16_right_dk"><a href="/sgp/riders/leon-madsen-19/2020"');
        $response->assertSee('<a href="/sgp/meetings/2019-torun">Full Meeting Result</a>');

        $response->assertSee('<h3 class="row_head">Next Meeting: <span class="flag_right flag16_right_pl">'
            . 'Warsaw Grand Prix</span></h3>');
        $response->assertSee('<dd class="venue">Stadion Narodowy, Warsaw</dd>');
        $response->assertSee('<dt>Meeting:</dt>
            <dd class="time">Saturday 28th March, 18:00 (Local)</dd>
            <dd class="weather icon_weather_rain">
    Rain, 18 &deg;C / 65 &deg;F
            <span class="precip">, 87% chance rain</span>
    </dd>');

        // Standings.
        $response->assertSee('<dt class="podium-1 pos">1</dt>
                <dd class="podium-1 name"><span class="flag_right flag16_right_pl">'
            . '<a href="/sgp/riders/bartosz-zmarzlik-39/2020"');
        $response->assertSee('<dt class="row_0 pos">10</dt>
                <dd class="row_0 name"><span class="flag_right flag16_right_dk">'
            . '<a href="/sgp/riders/niels-kristian-iversen-24/2020"');
    }

    /**
     * A test of the SGP homepage with a historical header argument.
     * @return void
     */
    public function testHomepageHistorical(): void
    {
        $response = $this->get('/sgp/?header=2019');
        $response->assertStatus(200);

        // 2019 Header.
        $response->assertSee('<a href="/sgp/meetings/2019-warsaw" class="flag pl "></a>
    <info><span class="flag_right flag16_right_dk">Madsen</span></info>');
        $response->assertSee('<a href="/sgp/meetings/2019-torun" class="flag pl "></a>
    <info><span class="flag_right flag16_right_dk">Madsen</span></info>');

        // 2019 Standings.
        $response->assertSee('<dt class="podium-1 pos">1</dt>
                <dd class="podium-1 name"><span class="flag_right flag16_right_pl">'
            . '<a href="/sgp/riders/bartosz-zmarzlik-39/2019"');
        $response->assertSee('<dt class="row_0 pos">10</dt>
                <dd class="row_0 name"><span class="flag_right flag16_right_dk">'
            . '<a href="/sgp/riders/niels-kristian-iversen-24/2019"');
        $response->assertSee('<a href="/sgp/standings/2019">View Full Standings</a>');
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/sgp/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'DeBear Sports Speedway Grand Prix',
            'short_name' => 'DeBear Sports Speedway Grand Prix',
            'description' => 'All the latest results and stats from the 2020 Speedway Grand Prix season.',
            'start_url' => '/sgp/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/sports/images/meta/sgp.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
