<?php

namespace Tests\PHPUnit\Feature\SGP;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class EStatsTest extends MotorsportBaseCase
{
    /**
     * A test of the current SGP stat leaders
     * @return void
     */
    public function testStats(): void
    {
        $response = $this->get('/sgp/stats');
        $response->assertStatus(200);

        $response->assertSee('<h1><span class="hidden-m">2020</span> Stat Leaders</h1>');
        $response->assertDontSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="participants">Riders</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="teams">Constructors</item>
                <sep class="right"></sep>
    </desk>');

        $response->assertSee('<div class="stats">
            <div class="subnav-participants stats-table  clearfix">
                        <fieldset class="no-stats info icon icon_info">The 2020 season has not yet started.</fieldset>
        </div>
    </div>

</div>');
        $response->assertDontSee('<div class="stats">
            <div class="subnav-participants stats-table  clearfix">
                        <fieldset class="no-stats info icon icon_info">The 2020 season has not yet started.</fieldset>
        </div>
            <div class="subnav-teams stats-table hidden clearfix">
                        <fieldset class="no-stats info icon icon_info">The 2020 season has not yet started.</fieldset>
        </div>
    </div>

</div>');
    }

    /**
     * A test of a previous season's SGP stat leaders
     * @return void
     */
    public function testStatsHistorical(): void
    {
        $response = $this->get('/sgp/stats/2019');
        $response->assertStatus(200);

        $response->assertSee('<h1><span class="hidden-m">2019</span> Stat Leaders</h1>');
        $response->assertDontSee('<desk class="hidden-m hidden-tp">
                    <sep class="left"></sep>
            <item class="sel" data-tab="participants">Riders</item>
                    <sep class="mid"></sep>
            <item class="unsel" data-tab="teams">Constructors</item>
                <sep class="right"></sep>
    </desk>');

        $response->assertSee('<select>
            <optgroup label="Meeting"><option value="1">Grand Prix Appearances</option>'
            . '<option value="2">Semi-Final Appearances</option>'
            . '<option value="3">Grand Final Appearances</option>'
            . '<option value="4">Number of Podium Placings</option>'
            . '<option value="5">Number of Grand Prix Wins</option></optgroup><optgroup label="Heat">'
            . '<option value="6">Number of Heats</option>'
            . '<option value="7">Number of Heats Completed</option>'
            . '<option value="8">Total Points Scored</option>'
            . '<option value="9" selected="selected">Points Scored per Heat</option>'
            . '<option value="10">Heat Wins</option></optgroup>        </select>');

        // Rider.
        $response->assertSee('<top>
            <cell class="row_head group">Riders</cell>
        </top>
        ' . '
                                        <row data-sort-9="1:1" data-sort-1="1:1" data-sort-2="2:1" data-sort-3="3:1" '
            . 'data-sort-4="3:1" data-sort-5="3:1" data-sort-6="3:1" data-sort-7="3:1" data-sort-8="3:1" '
            . 'data-sort-10="1:1">
                                <cell class="row_head pos sort_order">1</cell>');
        $response->assertSee('<row data-sort-9="1:1" data-sort-1="1:1" data-sort-2="2:1" data-sort-3="3:1" '
            . 'data-sort-4="3:1" data-sort-5="3:1" data-sort-6="3:1" data-sort-7="3:1" data-sort-8="3:1" '
            . 'data-sort-10="1:1">
                                            <cell class="row_1  hidden-m" data-field="1">10</cell>
                                            <cell class="row_1  hidden-m" data-field="2">8</cell>
                                            <cell class="row_1  hidden-m" data-field="3">4</cell>
                                            <cell class="row_1  hidden-m" data-field="4">3</cell>
                                            <cell class="row_1  hidden-m" data-field="5">1</cell>
                                            <cell class="row_1  hidden-m" data-field="6">62</cell>
                                            <cell class="row_1  hidden-m" data-field="7">61</cell>
                                            <cell class="row_1  hidden-m" data-field="8">126</cell>
                                            <cell class="row_1  " data-field="9">2.07</cell>
                                            <cell class="row_1  hidden-m" data-field="10">27</cell>
                                    </row>');

        // Team.
        $response->assertDontSee('<top>
            <cell class="row_head group">Constructors</cell>
        </top>');
    }
}
