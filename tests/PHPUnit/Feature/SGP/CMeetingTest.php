<?php

namespace Tests\PHPUnit\Feature\SGP;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class CMeetingTest extends MotorsportBaseCase
{
    /**
     * A test of an individual SGP meeting
     * @return void
     */
    public function testMeeting(): void
    {
        $response = $this->get('/sgp/meetings/2019-torun');
        $response->assertStatus(200);

        // Race features.
        $response->assertSee('<meta name="Description" content="Results from the 2019 Toru&#324; Grand Prix." />');
        $response->assertSee('<title>DeBear Sports &raquo; Speedway Grand Prix &raquo; 2019 Meetings &raquo; '
            . 'Toru&#324; Grand Prix</title>');
        $response->assertSee('<h1 class="hidden-m hidden-tp flag128_pl">Toru&#324; Grand Prix</h1>');
        $response->assertSee('<h1 class="hidden-d hidden-tl flag48_pl">Toru&#324; Grand Prix</h1>');
        $response->assertSee('<span class="race-prev">&laquo; <span class="flag_right flag16_right_gb">'
            . '<a href="/sgp/meetings/2019-cardiff">Great Britain</a></span></span>');
        $response->assertSee('<span class="race-num"><strong>Meeting 10 of 10</strong></span>');
        $response->assertDontSee('<span class="race-next">');
        $response->assertSee('<span class="item-3">
                                Saturday 5th October 2019' . "\n" . '            </span>');

                                // Heat Grid.
        $response->assertSee('<dt class="row_head table-cell table-clear draw">12</dt>
            <dd class="row_0 table-cell bib_no">222</dd>
            <dd class="row_0 table-cell table-name name"><span class="flag_right flag16_right_ru">'
            . '<a href="/sgp/riders/artem-laguta-28/2019"');
        $response->assertSee('Artem Laguta</a></span></dd>
            <dd class="row_0 table-cell res-pts">7pts</dd>
            <dd class="row_0 table-cell res-pos">8th</dd>');
        $response->assertSee('<dd class="gate-4 table-cell heat">1</dd>');
        $response->assertSee('<dd class="gate-3 table-cell heat">2</dd>');
        $response->assertSee('<dd class="gate-1 table-cell heat">3</dd>');
        $response->assertSee('<dd class="gate-2 table-cell heat">1</dd>');
        $response->assertSee('<dd class="gate-2 table-cell heat">0</dd>');

        // Heat List.
        $response->assertSee('<dd class="gate gate-2 table-cell">B</dd>
                            <dd class="gate-2 table-cell bib_no">88</dd>
                            <dd class="gate-2 table-cell table-name rider"><span class="flag_right flag16_right_dk">'
            . '<a href="/sgp/riders/niels-kristian-iversen-24/2019"');
        $response->assertSee('Niels-Kristian Iversen</a></span></dd>
                            <dd class="gate-2 table-cell result">1st</dd>
                            <dd class="gate-2 table-cell pts">3pts</dd>');
        $response->assertSee('<dt class="row_head table-title">Standings after 20 heats</dt>');
        $response->assertSee('Maciej Janowski</a></span></dd>
                        <dd class="row_1 table-cell stat">5</dd>
                        <dd class="podium-1 table-cell stat">1</dd>
                        <dd class="podium-2 table-cell stat hidden-m">1</dd>
                        <dd class="podium-3 table-cell stat hidden-m">2</dd>
                        <dd class="row_1 table-cell stat hidden-m">1</dd>
                        <dd class="row_head table-cell result">7pts</dd>');
        $response->assertSee('<dt class="gate-1 table-cell table-clear gate">R</dt>
                                                    <dd class="podium-1 table-cell res">9</dd>
                                                    <dd class="podium-2 table-cell res">7</dd>
                                                    <dd class="podium-3 table-cell res">1</dd>
                                                    <dd class="gate-1 table-cell res">3</dd>
                                                            <dt class="gate-2 table-cell table-clear gate">B</dt>
                                                    <dd class="podium-1 table-cell res">4</dd>
                                                    <dd class="podium-2 table-cell res">4</dd>
                                                    <dd class="podium-3 table-cell res">5</dd>
                                                    <dd class="gate-2 table-cell res">7</dd>
                                                            <dt class="gate-3 table-cell table-clear gate">W</dt>
                                                    <dd class="podium-1 table-cell res">4</dd>
                                                    <dd class="podium-2 table-cell res">5</dd>
                                                    <dd class="podium-3 table-cell res">4</dd>
                                                    <dd class="gate-3 table-cell res">7</dd>
                                                            <dt class="gate-4 table-cell table-clear gate">Y</dt>
                                                    <dd class="podium-1 table-cell res">3</dd>
                                                    <dd class="podium-2 table-cell res">4</dd>
                                                    <dd class="podium-3 table-cell res">10</dd>
                                                    <dd class="gate-4 table-cell res">3</dd>');

        // Knockout.
        $response->assertSee('<dt class="row_head heat-name table-cell">Semi-Final 1</dt>');
        $response->assertSee('<dt class="row_head heat-name table-cell">Semi-Final 2</dt>');
        $response->assertSee('<dt class="row_head heat-name table-cell">Grand Final</dt>');
        $response->assertSee('<dt class="row_head table-cell pos">1st</dt>
    <dd class="gate-1 table-cell gate">R</dd>
    <dd class="row_0 table-cell bib_no">30</dd>
    <dd class="row_0 table-cell table-name rider"><span class="flag_right flag16_right_dk">'
            . '<a href="/sgp/riders/leon-madsen-19/2019"');
        $response->assertSee('<dt class="row_head table-cell pos">3rd</dt>
    <dd class="gate-3 table-cell gate">W</dd>
    <dd class="row_0 table-cell bib_no">88</dd>
    <dd class="row_0 table-cell table-name rider"><span class="flag_right flag16_right_dk">'
            . '<a href="/sgp/riders/niels-kristian-iversen-24/2019"');

        // Standings.
        $response->assertSee('<dt class="row_head table-cell table-title">Riders&#39; Standings</dt>');
        $response->assertSee('<dt class="row_head table-cell table-clear pos no-change">1</dt>');
        $response->assertSee('Bartosz Zmarzlik</a></span></dd>
                            <dd class="podium-1 table-cell pts">+14pts</dd>
                        <dd class="podium-1 table-cell pts">132pts</dd>');
        $response->assertSee('<dt class="row_head table-cell table-clear pos ">2</dt>
                            <dd class="up table-cell change">
                    <span class="icon_arrow_up">1</span>
                </dd>');
        $response->assertSee('Max Fricke</a></span></dd>
                            <dd class=" table-cell pts"></dd>
                        <dd class="row_0 table-cell pts">36pts</dd>');
    }

    /**
     * A test of an abandoned SGP meeting
     * @return void
     */
    public function testMeetingAbandoned(): void
    {
        $response = $this->get('/sgp/meetings/2015-warsaw');
        $response->assertStatus(200);

        $response->assertSee('<li class="message">
            <div class="box info icon icon_info">The meeting was abandoned with the Grand Prix and Championship Points '
                . 'awarded after Heat 12.</div>
        </li>');
        $response->assertDontSee('<option value="finals" >Knockout</option>');
    }

    /**
     * A test of the SGP meeting URL nomenclature
     * @return void
     */
    public function testMeetingNotRace(): void
    {
        $response = $this->get('/sgp/races/2019-torun');
        $response->assertStatus(404);
    }

    /**
     * A test of miscellaneous SGP meeting controller actions
     * @return void
     */
    public function testMeetingMisc(): void
    {
        // Race not found.
        $response = $this->get('/sgp/meetings/2019-no-matches');
        $response->assertStatus(404);

        // Legacy routes - current season.
        $response = $this->get('/sgp/meeting-1');
        $response->assertStatus(308);
        $response->assertRedirect('/sgp/meetings/2019-warsaw');

        // Legacy routes - stating season.
        $response = $this->get('/sgp/2019/meeting-22');
        $response->assertStatus(404);

        $response = $this->get('/sgp/2019/meeting-1');
        $response->assertStatus(308);
        $response->assertRedirect('/sgp/meetings/2019-warsaw');
    }
}
