<?php

namespace Tests\PHPUnit\Feature\SGP;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class GSitemapTest extends MotorsportBaseCase
{
    /**
     * A test of the SGP sitemap for the current season.
     * @return void
     */
    public function testSitemap(): void
    {
        $response = $this->get('/sgp/sitemap');
        $response->assertStatus(200);

        $response->assertSee('<a href="/sgp/meetings">
        <strong>Meetings</strong>
            </a>
    ' . '
            <em class="descrip">The 2020 Speedway Grand Prix meetings list.</em>
    ' . '
            <ul class="inline_list children open">
            <li class="item">
    <div class="toggle "></div>
        <strong><span class="flag_right flag16_right_pl">Warsaw Grand Prix</span></strong>');

        $response->assertSee('<a href="/sgp/standings/2010">
        <strong>2010 Standings</strong>
            </a>');
        $response->assertDontSee('<a href="/sgp/standings/2009">
        <strong>2009 Standings</strong>
            </a>');

        $response->assertSee('<a href="/sgp/riders">
        <strong>Riders</strong>
            </a>
    ' . '
            <em class="descrip">All the riders taking part in the 2020 Speedway Grand Prix season.</em>');

        $response->assertSee('<a href="/sgp/riders/greg-hancock-4/2020">
        <strong><span class="flag_right flag16_right_us">Greg Hancock</span></strong>
            </a>');
        $response->assertDontSee('<a href="/sgp/riders/robert-lambert-119/2020">
        <strong><span class="flag_right flag16_right_gb">Robert Lambert</span></strong>
            </a>');
    }

    /**
     * A test of the SGP sitemap for a given season
     * @return void
     */
    public function testSitemapHistorical(): void
    {
        $response = $this->get('/sgp/sitemap/2019');
        $response->assertStatus(200);

        $response->assertSee('<a href="/sgp/meetings/2019">
        <strong>Meetings</strong>
            </a>
    ' . '
            <em class="descrip">The 2019 Speedway Grand Prix meetings list.</em>
    ' . '
            <ul class="inline_list children open">
            <li class="item">
    <div class="toggle "></div>
            <a href="/sgp/meetings/2019-warsaw">
        <strong><span class="flag_right flag16_right_pl">Warsaw Grand Prix</span></strong>
            </a>');

        $response->assertSee('<a href="/sgp/riders/2019">
        <strong>Riders</strong>
            </a>
    ' . '
            <em class="descrip">All the riders taking part in the 2019 Speedway Grand Prix season.</em>');

        $response->assertSee('<a href="/sgp/riders/robert-lambert-119/2019">
        <strong><span class="flag_right flag16_right_gb">Robert Lambert</span></strong>
            </a>');
        $response->assertDontSee('<a href="/sgp/riders/greg-hancock-4/2019">
        <strong><span class="flag_right flag16_right_ch">Dominique Aegerter</span></strong>
            </a>');
    }

    /**
     * A test of the SGP sitemap XML file
     * @return void
     */
    public function testSitemapXML(): void
    {
        $response = $this->get('/sgp/sitemap.xml');
        $response->assertStatus(200);

        // Races.
        $response->assertSee('/sgp/meetings/2019-warsaw</loc>
        <changefreq>never</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertDontSee('/sgp/meetings/2020-warsaw</loc>
        <changefreq>weekly</changefreq>
        <priority>1.0</priority>
    </url>');

        // Standings.
        $response->assertSee('/sgp/standings</loc>
        <changefreq>weekly</changefreq>
        <priority>1</priority>
    </url>');
        $response->assertDontSee('/sgp/standings/2020</loc>
        <changefreq>weekly</changefreq>
        <priority>1</priority>
    </url>');
        $response->assertSee('/sgp/standings/2019</loc>
        <changefreq>never</changefreq>
        <priority>0.4</priority>
    </url>');
        $response->assertSee('/sgp/standings/2010</loc>
        <changefreq>never</changefreq>
        <priority>0.4</priority>
    </url>');
        $response->assertDontSee('/sgp/standings/2009</loc>
        <changefreq>never</changefreq>
        <priority>0.4</priority>
    </url>');

        // Riders.
        $response->assertSee('/sgp/riders/greg-hancock-4</loc>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>');
        $response->assertDontSee('/sgp/riders/greg-hancock-4</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertDontSee('/sgp/riders/greg-hancock-4/2020</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');

        $response->assertSee('/sgp/riders/robert-lambert-119</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');
        $response->assertDontSee('/sgp/riders/robert-lambert-119</loc>
        <changefreq>weekly</changefreq>
        <priority>0.7</priority>
    </url>');
        $response->assertDontSee('/sgp/riders/robert-lambert-119/2019</loc>
        <changefreq>monthly</changefreq>
        <priority>0.3</priority>
    </url>');
    }
}
