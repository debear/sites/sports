<?php

namespace Tests\PHPUnit\Feature\SGP;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class DStandingsTest extends MotorsportBaseCase
{
    /**
     * A test of the current SGP standings
     * @return void
     */
    public function testStandings(): void
    {
        $response = $this->get('/sgp/standings');
        $response->assertStatus(200);

        $response->assertSee('<h1><span class="hidden-tp hidden-m">2020</span> '
            . '<span class="hidden-m">Speedway Grand Prix</span> Championship Standings</h1>');

        // Drivers.
        $response->assertSee('<tables class="key-handler races-10">
    ' . '
        <standings class="clearfix participants">');
        $response->assertSee('<main>
        <flags>
            <type class="row_head">Rider</type>
            <total class="row_head sel sortable" data-round="0">Total</total>
        </flags>
                                <row class="rows-1 " data-id="39" data-pos="1">
                <pos class="full podium-1">1</pos>
                <name class="full podium-1">
                    <span class="flag_right flag16_right_pl"><a href="/sgp/riders/bartosz-zmarzlik-39/2020"');
        $response->assertSee('Bartosz Zmarzlik</a></span>
                    ' . '
                                    </name>
                <total class="full podium-1">0pts</total>
                                                                </row>');
        $response->assertSeeInOrder([
            '<row class="rows-1 outer" data-id="39" data-pos="1">',
            '<race class="race-cell res-not_yet_raced" data-round="" data-pos="" data-pts="" data-res="" ></race>',
        ]);

        // Key.
        $response->assertSee('<dl class="toggles display_key">
    <dt>Key:</dt>
        <dd class="show unsel" data-show="1">Show</dd>
        <dd class="hide sel" data-show="0">Hide</dd>
        <dd class="list hidden">
            <ul class="results_key inline_list">
                <li class="res-pos_1">Winner</li>
                <li class="res-pos_2">Runner-Up</li>
                <li class="res-pos_3">Third</li>
                                    <li class="res-pts_final">Finalist</li>
                    <li class="res-pts_semi">Semi-Finalist</li>
                    <li class="res-pts_finish">Points Scored</li>
                    <li class="res-nc">Not Classified</li>
                                <li class="res-dsq">Disqualified</li>
                                <li class="res-not_yet_raced">Race To Be Held</li>
                <li class="res-no_race">Did Not Race</li>
                            </ul>
        </dd>
</dl>');

        // Teams.
        $response->assertDontSee('<h2>Constructors&#39; Standings</h2>');
    }

    /**
     * A test of a previous season's SGP standings
     * @return void
     */
    public function testStandingsHistorical(): void
    {
        $response = $this->get('/sgp/standings/2019');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; Speedway Grand Prix &raquo; 2019 Standings</title>');

        $response->assertSee('<h1><span class="hidden-tp hidden-m">2019</span> '
            . '<span class="hidden-m">Speedway Grand Prix</span> Championship Standings</h1>');

        // Drivers.
        $response->assertSee('<tables class="key-handler races-10">
    ' . '
        <standings class="clearfix participants">');
        $response->assertSee('<main>
        <flags>
            <type class="row_head">Rider</type>
            <total class="row_head sel sortable" data-round="0">Total</total>
        </flags>
                                <row class="rows-1 " data-id="39" data-pos="1">
                <pos class="full podium-1">1</pos>
                <name class="full podium-1">
                    <span class="flag_right flag16_right_pl"><a href="/sgp/riders/bartosz-zmarzlik-39/2019"');
        $response->assertSee('Bartosz Zmarzlik</a></span>
                    ' . '
                                            <trophies>
                                                            <span class="icon_right_standings_trophy1">3x</span>
                                                            <span class="icon_right_standings_trophy2"></span>
                                                            <span class="icon_right_standings_trophy3"></span>
                                                    </trophies>
                                    </name>
                <total class="full podium-1">132pts</total>
                                                                </row>');
        $response->assertSee('<row class="rows-1 " data-id="19" data-pos="2">
                <pos class="full podium-2">2</pos>
                <name class="full podium-2">
                    <span class="flag_right flag16_right_dk"><a href="/sgp/riders/leon-madsen-19/2019"');
        $response->assertSee('Leon Madsen</a></span>
                    ' . '
                                            <trophies>
                                                            <span class="icon_right_standings_trophy1">3x</span>
                                                            <span class="icon_right_standings_trophy2">2x</span>
                                                            <span class="icon_right_standings_trophy3">2x</span>
                                                    </trophies>
                                    </name>
                <total class="full podium-2">130pts</total>
                                                                </row>');

        // Teams.
        $response->assertDontSee('<h2>Constructors&#39; Standings</h2>');
    }
}
