<?php

namespace Tests\PHPUnit\Feature\SGP;

use Tests\PHPUnit\Sports\Motorsport\PHPUnitBase as MotorsportBaseCase;

class BMeetingListTest extends MotorsportBaseCase
{
    /**
     * A test of the SGP race list for the current season.
     * @return void
     */
    public function testMeetings(): void
    {
        $response = $this->get('/sgp/meetings');
        $response->assertStatus(200);
        $response->assertSee('<title>DeBear Sports &raquo; Speedway Grand Prix &raquo; Meetings</title>');

        // Prev/Next Races.
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    First Meeting
                                &ndash;
                <span class="flag_right flag16_right_pl">Warsaw Grand Prix</span>
            </dt>

            ' . '
                            <dd class="row_1 full-width"><strong>Meeting Starts:</strong> '
            . 'Saturday 28th March, 18:00 (Local)</dd>');
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    Next Meeting
                                &ndash;
                <span class="flag_right flag16_right_de">German Grand Prix</span>
            </dt>

            ' . '
                            <dd class="row_1 full-width"><strong>Meeting Starts:</strong> '
            . 'Saturday 30th May, 19:00 (Local)</dd>');

        // Upcoming.
        $response->assertSee('<h2 class="row_head">Upcoming Meetings</h2>
    <ul class="inline_list list upcoming grid">
        ' . '
                    <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">3</dt>
                <dt class="row_head head title-row">
                    <name>Czech Grand Prix</name>
                    <info>Stadium Marketa, Prague</info>
                </dt>
                <dd class="flag flag48_cz title-row"></dd>

                ' . '
                                    <dd class="full-width row_1"><strong>Meeting Starts:</strong> '
            . 'Wednesday 3rd June, 19:00 (Local)</dd>');
        $response->assertSee('<fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">4</dt>
                <dt class="row_head head title-row">
                    <name>British Grand Prix</name>
                    <info>Principality Stadium, Cardiff</info>
                </dt>
                <dd class="flag flag48_gb title-row"></dd>

                ' . '
                                    <dd class="full-width row_1"><strong>Meeting Starts:</strong> '
            . 'Saturday 18th July, 17:00 (Local)</dd>



            ' . '
        <dt class="full-width section row_head">2019 Podium</dt>
                            <dd class="cell podium-1 pos">
        1
    </dd>
<dd class="cell podium-1 name no-team">
    <span class="flag_right flag16_right_dk"><a href="/sgp/riders/leon-madsen-19/2020"');
    }

    /**
     * A test of the SGP race list for a given season.
     * @return void
     */
    public function testMeetingsHistorical(): void
    {
        $response = $this->get('/sgp/meetings/2019');
        $response->assertStatus(200);
        $response->assertSee('DeBear Sports &raquo; Speedway Grand Prix &raquo; 2019 Meetings');

        // Header shows 2019 races.
        $response->assertSee('<ul class="inline_list nav_calendar num_10">
                    ' . '
        <li>
    <a href="/sgp/meetings/2019-warsaw" class="flag pl "></a>
    <info><span class="flag_right flag16_right_dk">Madsen</span></info>
</li>');

        // Prev / Last.
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    Previous Meeting
                                &ndash;
                <span class="flag_right flag16_right_gb">British Grand Prix</span>
            </dt>');
        $response->assertSee('<fieldset class="section">
        <dl class="race">
            <dt class="full-width row_head">
                                    Last Meeting
                                &ndash;
                <span class="flag_right flag16_right_pl">Toru&#324; Grand Prix</span>
            </dt>');

        // First in list.
        $response->assertSee('<ul class="inline_list list completed grid">
                    <li class="grid-cell grid-1-3 grid-tl-1-2 grid-tp-1-1 grid-m-1-1">
        <fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">8</dt>
                <dt class="row_head head title-row">
                    <name>Danish Grand Prix</name>
                    <info>Vojens Speedway Center, Vojens</info>
                </dt>
                <dd class="flag flag48_dk title-row"></dd>

                ' . '
                                    <dt class="full-width section row_head"> Podium</dt>
            <dd class="cell podium-1 pos">
        1
    </dd>
<dd class="cell podium-1 name no-team">
    <span class="flag_right flag16_right_pl"><a href="/sgp/riders/bartosz-zmarzlik-39/2019"');
        $response->assertSee('<a href="/sgp/meetings/2019-vojens">&raquo; Full Meeting Coverage</a>');
    }

    /**
     * A test of the SGP meeting list for a given season without the main page chrome.
     * @return void
     */
    public function testMeetingssChromeless(): void
    {
        $response = $this->get('/sgp/meetings/2020/body');
        $response->assertStatus(200);
        $response->assertDontSee('<title>DeBear Sports &raquo; Speedway Grand Prix &raquo; Races</title>');
        $response->assertSee('<h1>2020 Season Schedule</h1>');
        $response->assertSee('<fieldset class="section">
            <dl class="race">
                <dt class="row_head round title-row">3</dt>
                <dt class="row_head head title-row">
                    <name>Czech Grand Prix</name>
                    <info>Stadium Marketa, Prague</info>
                </dt>
                <dd class="flag flag48_cz title-row"></dd>

                ' . '
                                    <dd class="full-width row_1"><strong>Meeting Starts:</strong> '
            . 'Wednesday 3rd June, 19:00 (Local)</dd>');
    }

    /**
     * A test of the SGP meeting list URL nomenclature
     * @return void
     */
    public function testMeetingsNotRaces(): void
    {
        $response = $this->get('/sgp/races');
        $response->assertStatus(404);
    }
}
