<?php

namespace Tests\PHPUnit\Unit\F1;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Sports\Motorsport\FIA\Race;

class CRaceTest extends UnitTestCase
{
    /**
     * A test of a sprint-race Grand Prix weekend
     * @return void
     */
    public function testSprintRace(): void
    {
        $sport = 'f1';
        // Tweak the config to match our test requirements.
        $config = FrameworkConfig::get('debear.links.races');
        FrameworkConfig::set([
            'debear.links.races' => FrameworkConfig::get("debear.sports.subsites.$sport.links.races"),
        ]);
        // Ensure the user objects are set up as expected.
        User::setup();
        // Create the in-memory ORM object.
        $race = new Race([
            'series' => $sport,
            'season' => 2019,
            'round' => 1,
            'round_order' => 1,
            'name_full' => 'Castle Combe Grand Prix',
            'name_short' => 'Castle Combe',
            'circuit' => 'Castle Combe Circuit',
            'location' => 'Castle Combe',
            'flag' => 'cc',
            'timezone' => 'Europe/London',
            'quirks' => 'SPRINT_RACE',
            'qual_time' => '2019-01-04 13:00:00',
            'race_time' => '2019-01-05 13:00:00',
            'race2_time' => '2019-01-06 13:00:00',
            'lap_length' => 2.977,
            'race_laps_total' => 30,
            'race_laps_completed' => 30,
            'race2_laps_total' => 50,
            'race2_laps_completed' => 50,
        ]);

        // Now do some interrogation on it.
        $this->assertEquals('Castle Combe Grand Prix', $race->name_full);
        $this->assertEquals('Castle Combe Circuit, Castle Combe', $race->venue);
        $this->assertEquals('Sunday 6th January, 13:00 (Local)', $race->race2_start_time);
        // Header list.
        $info = $race->headerInfo();
        $this->assertEquals('Castle Combe Circuit, Castle Combe', $info['Circuit']);
        $this->assertEquals('2.977km', $info['Lap Length']);
        $this->assertEquals('Friday 4th January 2019', $info['Qualifying']);
        $this->assertEquals('Saturday 5th January 2019', $info['Sprint Race']);
        $this->assertEquals('89.31km', $info['Sprint Race Distance']);
        $this->assertEquals('Sunday 6th January 2019', $info['Race']);
        $this->assertEquals('148.85km', $info['Race Distance']);
        // Name, flag and link combos.
        $flag_tag = '<span class="flag_right flag16_right_cc">';
        $link_tag = '<a href="/f1/races/2019-castle-combe">';
        $this->assertEquals("{$flag_tag}Castle Combe Grand Prix</span>", $race->nameFlag());
        $this->assertEquals("{$flag_tag}Castle Combe</span>", $race->nameShortFlag());
        $this->assertEquals("{$flag_tag}{$link_tag}Castle Combe Grand Prix</a></span>", $race->nameLinkFlag());
        $this->assertEquals("{$flag_tag}{$link_tag}Castle Combe</a></span>", $race->nameShortLinkFlag());
        // Restore our config overrides.
        FrameworkConfig::set(['debear.links.races' => $config]);
    }
}
