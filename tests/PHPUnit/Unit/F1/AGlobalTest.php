<?php

namespace Tests\PHPUnit\Unit\F1;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\Motorsport\FIA\Race;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\Sports\Motorsport\Header;

class AGlobalTest extends UnitTestCase
{
    /**
     * A test of the race name formatting for the main site homepage
     * @return void
     */
    public function testNameHomepage(): void
    {
        // The base in-memory ORM object data.
        $r = [
            'name_full' => 'Castle Combe Grand Prix',
            'flag' => 'gb-cc',
        ];
        $assert_base = '<span class="flag_right flag16_right_gb-cc">Castle Combe Grand Prix';

        // Qualifying (No sprint quali).
        $quali = new Race(array_merge($r, ['template_type' => 'qual']));
        $this->assertEquals('Castle Combe Grand Prix', $quali->name_full);
        $this->assertEquals("$assert_base Qualifying</span>", $quali->nameHomepage());

        // Qualifying (With sprint quali).
        $quali = new Race(array_merge($r, ['template_type' => 'qual', 'quirks' => 'SPRINT_RACE']));
        $this->assertEquals('Castle Combe Grand Prix', $quali->name_full);
        $this->assertEquals("$assert_base Qualifying</span>", $quali->nameHomepage());

        // Race 1 (No sprint quali).
        $quali = new Race(array_merge($r, ['template_type' => 'race']));
        $this->assertEquals('Castle Combe Grand Prix', $quali->name_full);
        $this->assertEquals("$assert_base</span>", $quali->nameHomepage());

        // Race 1 (With sprint quali).
        $quali = new Race(array_merge($r, ['template_type' => 'race', 'quirks' => 'SPRINT_RACE']));
        $this->assertEquals('Castle Combe Grand Prix', $quali->name_full);
        $this->assertEquals("$assert_base Sprint Qualifying</span>", $quali->nameHomepage());

        // Race 2 (No sprint quali).
        $quali = new Race(array_merge($r, ['template_type' => 'race2']));
        $this->assertEquals('Castle Combe Grand Prix', $quali->name_full);
        $this->assertEquals("$assert_base Race 2</span>", $quali->nameHomepage());

        // Race 2 (With sprint quali).
        $quali = new Race(array_merge($r, ['template_type' => 'race2', 'quirks' => 'SPRINT_RACE']));
        $this->assertEquals('Castle Combe Grand Prix', $quali->name_full);
        $this->assertEquals("$assert_base</span>", $quali->nameHomepage());
    }

    /**
     * A test of the header schedule building
     * @return void
     */
    public function testHeaderSchedule(): void
    {
        // Prepare the configuration to enable F1 details.
        $base = FrameworkConfig::get('debear');
        $this->setTestConfig([
            'debear' => Arrays::merge($base, FrameworkConfig::get('debear.sports.subsites.f1')),
            'debear.section.endpoint' => 'f1',
            'debear.section.code' => 'f1',
        ]);

        // Version 1: With definite setup.
        Header::reset();
        Header::setup();
        $sched_v1 = Header::calendar();

        // Version 2: With implicit setup.
        Header::reset();
        $sched_v2 = Header::calendar();

        // Perform the assertions.
        $this->assertIsString($sched_v1);
        $this->assertIsString($sched_v2);
        $this->assertEquals($sched_v1, $sched_v2);
    }
}
