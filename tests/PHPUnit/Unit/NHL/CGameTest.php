<?php

namespace Tests\PHPUnit\Unit\NHL;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Sports\MajorLeague\NHL\PlayerGameGoalie;

class CGameTest extends UnitTestCase
{
    /**
     * Testing various goalie decision permutations on in-memory objects
     * @return void
     */
    public function testGoalieDecision(): void
    {
        // Win.
        $pgg = new PlayerGameGoalie(['win' => 1, 'loss' => 0, 'tie' => 0, 'ot_loss' => 0, 'so_loss' => 0]);
        $this->assertEquals('Win', $pgg->decision);
        $this->assertEquals('win', $pgg->decisionCSS());

        // Loss.
        $pgg = new PlayerGameGoalie(['win' => 0, 'loss' => 1, 'tie' => 0, 'ot_loss' => 0, 'so_loss' => 0]);
        $this->assertEquals('Loss', $pgg->decision);
        $this->assertEquals('loss', $pgg->decisionCSS());

        // Tie.
        $pgg = new PlayerGameGoalie(['win' => 0, 'loss' => 0, 'tie' => 1, 'ot_loss' => 0, 'so_loss' => 0]);
        $this->assertEquals('Tie', $pgg->decision);
        $this->assertEquals('tie', $pgg->decisionCSS());

        // OT Loss.
        $pgg = new PlayerGameGoalie(['win' => 0, 'loss' => 0, 'tie' => 0, 'ot_loss' => 1, 'so_loss' => 0]);
        $this->assertEquals('OT Loss', $pgg->decision);
        $this->assertEquals('tie', $pgg->decisionCSS());

        // SO Loss.
        $pgg = new PlayerGameGoalie(['win' => 0, 'loss' => 0, 'tie' => 0, 'ot_loss' => 0, 'so_loss' => 1]);
        $this->assertEquals('SO Loss', $pgg->decision);
        $this->assertEquals('tie', $pgg->decisionCSS());

        // None set.
        $pgg = new PlayerGameGoalie(['win' => 0, 'loss' => 0, 'tie' => 0, 'ot_loss' => 0, 'so_loss' => 0]);
        $this->assertNull($pgg->decision);
        $this->assertNull($pgg->decisionCSS());
    }
}
