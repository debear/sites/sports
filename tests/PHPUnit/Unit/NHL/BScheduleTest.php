<?php

namespace Tests\PHPUnit\Unit\NHL;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\MajorLeague\NHL\Schedule;

class BScheduleTest extends UnitTestCase
{
    /**
     * Testing various schedule title permutations on in-memory objects
     * @return void
     */
    public function testScheduleTitles(): void
    {
        // Apply some specific NHL config.
        $this->setTestConfig([
            'debear.setup.playoffs' => FrameworkConfig::get('debear.sports.subsites.nhl.setup.playoffs'),
        ]);

        // Round Robin.
        $game = new Schedule([
            'season' => 2019, // A season playoffs were Conference based.
            'game_type' => 'playoff',
            'game_id' => 1,
            'home' => (object)[
                'conference' => (object)[
                    'name_full' => 'Eastern Conference',
                ],
                'division' => (object)[
                    'name_full' => 'Atlantic Division',
                ],
            ],
        ]);
        $this->assertEquals('Eastern Conference Round Robin, Game 1', $game->title);
        // Preliminary.
        $game->game_id = 21;
        $this->assertEquals('Eastern Conference Preliminary Round, Game 1', $game->title);
        // First Round - Conference.
        $game->game_id = 101;
        $this->assertEquals('Eastern Conference Quarter Final, Game 1', $game->title);
        // First Round - Division.
        $game->season = 2018;
        $this->assertEquals('Atlantic Division Semi Final, Game 1', $game->title);
        // Second Round.
        $game->game_id = 201;
        $this->assertEquals('Atlantic Division Final, Game 1', $game->title);
        // Conference Finals.
        $game->game_id = 301;
        $this->assertEquals('Eastern Conference Final, Game 1', $game->title);
        // Calder Cup.
        $game->game_id = 401;
        $this->assertEquals('Stanley Cup Final, Game 1', $game->title);
    }

    /**
     * Testing various schedule period number permutations on in-memory objects
     * @return void
     */
    public function testScheduleNumPeriods(): void
    {
        // Regulation.
        $game = new Schedule([
            'status' => 'F',
        ]);
        $this->assertEquals(3, $game->num_periods);
        // Overtime.
        $game->status = 'OT';
        $this->assertEquals(4, $game->num_periods);
        // Shootout.
        $game->status = 'SO';
        $this->assertEquals(5, $game->num_periods);
        // Multiple OTs.
        $game->status = '2OT';
        $this->assertEquals(5, $game->num_periods);
        $game->status = '3OT';
        $this->assertEquals(6, $game->num_periods);
    }

    /**
     * Testing various schedule period name permutations on in-memory objects
     * @return void
     */
    public function testSchedulePeriodNames(): void
    {
        // Regular season.
        $game = new Schedule([
            'game_type' => 'regular',
        ]);
        $this->assertEquals('1', $game->periodName(1));
        $this->assertEquals('1st', $game->periodNameMid(1));
        $this->assertEquals('1st Period', $game->periodNameFull(1));
        $this->assertEquals('2', $game->periodName(2));
        $this->assertEquals('2nd', $game->periodNameMid(2));
        $this->assertEquals('2nd Period', $game->periodNameFull(2));
        $this->assertEquals('3', $game->periodName(3));
        $this->assertEquals('3rd', $game->periodNameMid(3));
        $this->assertEquals('3rd Period', $game->periodNameFull(3));
        $this->assertEquals('OT', $game->periodName(4));
        $this->assertEquals('OT', $game->periodNameMid(4));
        $this->assertEquals('Overtime', $game->periodNameFull(4));
        $this->assertEquals('SO', $game->periodName(5));
        $this->assertEquals('SO', $game->periodNameMid(5));
        $this->assertEquals('Shootout', $game->periodNameFull(5));

        // Playoffs.
        $game->game_type = 'playoff';
        $this->assertEquals('1', $game->periodName(1));
        $this->assertEquals('1st', $game->periodNameMid(1));
        $this->assertEquals('1st Period', $game->periodNameFull(1));
        $this->assertEquals('2', $game->periodName(2));
        $this->assertEquals('2nd', $game->periodNameMid(2));
        $this->assertEquals('2nd Period', $game->periodNameFull(2));
        $this->assertEquals('3', $game->periodName(3));
        $this->assertEquals('3rd', $game->periodNameMid(3));
        $this->assertEquals('3rd Period', $game->periodNameFull(3));
        $this->assertEquals('OT', $game->periodName(4));
        $this->assertEquals('OT', $game->periodNameMid(4));
        $this->assertEquals('1st Overtime', $game->periodNameFull(4));
        $this->assertEquals('2OT', $game->periodName(5));
        $this->assertEquals('2OT', $game->periodNameMid(5));
        $this->assertEquals('2nd Overtime', $game->periodNameFull(5));
        $this->assertEquals('3OT', $game->periodName(6));
        $this->assertEquals('3OT', $game->periodNameMid(6));
        $this->assertEquals('3rd Overtime', $game->periodNameFull(6));
    }
}
