<?php

namespace Tests\PHPUnit\Unit\AHL;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Sports\MajorLeague\AHL\PlayerGameGoalie;

class IPlayersTest extends UnitTestCase
{
    /**
     * Testing the Goals Against Average mutator
     * @return void
     */
    public function testGAAMutator(): void
    {
        // As a number of seconds.
        $min_as_sec = new PlayerGameGoalie([
            'goals_against' => 1,
            'minutes_played' => 3600,
        ]);
        $this->assertEquals('1.00', $min_as_sec->goals_against_avg);

        // As a time.
        $min_as_time = new PlayerGameGoalie([
            'goals_against' => 1,
            'minutes_played' => '01:00:00',
        ]);
        $this->assertEquals('1.00', $min_as_time->goals_against_avg);
    }
}
