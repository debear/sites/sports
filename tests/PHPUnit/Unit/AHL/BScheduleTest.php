<?php

namespace Tests\PHPUnit\Unit\AHL;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\MajorLeague\AHL\Schedule;

class BScheduleTest extends UnitTestCase
{
    /**
     * Testing various schedule title permutations on in-memory objects
     * @return void
     */
    public function testScheduleTitles(): void
    {
        // Apply some specific AHL config.
        $this->setTestConfig([
            'debear.setup.playoffs' => FrameworkConfig::get('debear.sports.subsites.ahl.setup.playoffs'),
        ]);

        // First Round - Conference.
        $game = new Schedule([
            'season' => 2014, // Last season playoffs were Conference based.
            'game_type' => 'playoff',
            'game_id' => 101,
            'home' => (object)[
                'conference' => (object)[
                    'name_full' => 'Eastern Conference',
                ],
                'division' => (object)[
                    'name_full' => 'Northern Division',
                ],
            ],
        ]);
        $this->assertEquals('Eastern Conference Quarter Final, Game 1', $game->title);
        // First Round - Division.
        $game->season = 2019;
        $this->assertEquals('Northern Division Semi Final, Game 1', $game->title);
        // Second Round.
        $game->game_id = 201;
        $this->assertEquals('Northern Division Final, Game 1', $game->title);
        // Conference Finals.
        $game->game_id = 301;
        $this->assertEquals('Eastern Conference Final, Game 1', $game->title);
        // Calder Cup.
        $game->game_id = 401;
        $this->assertEquals('Calder Cup Final, Game 1', $game->title);
    }

    /**
     * Testing various schedule title permutations on in-memory objects
     * @return void
     */
    public function testScheduleNumPeriods(): void
    {
        // Regulation.
        $game = new Schedule([
            'status' => 'F',
        ]);
        $this->assertEquals(3, $game->num_periods);
        $this->assertFalse($game->hasShootout());
        // Overtime.
        $game->status = 'OT';
        $this->assertEquals(4, $game->num_periods);
        $this->assertFalse($game->hasShootout());
        // Shootout.
        $game->status = 'SO';
        $this->assertEquals(5, $game->num_periods);
        $this->assertTrue($game->hasShootout());
        // Multiple OTs.
        $game->status = '2OT';
        $this->assertEquals(5, $game->num_periods);
        $this->assertFalse($game->hasShootout());
        $game->status = '3OT';
        $this->assertEquals(6, $game->num_periods);
        $this->assertFalse($game->hasShootout());
    }

    /**
     * Testing various schedule period name permutations on in-memory objects
     * @return void
     */
    public function testSchedulePeriodNames(): void
    {
        // Regular season.
        $game = new Schedule([
            'game_type' => 'regular',
        ]);
        $this->assertEquals('1', $game->periodName(1));
        $this->assertEquals('1st', $game->periodNameMid(1));
        $this->assertEquals('1st Period', $game->periodNameFull(1));
        $this->assertEquals('2', $game->periodName(2));
        $this->assertEquals('2nd', $game->periodNameMid(2));
        $this->assertEquals('2nd Period', $game->periodNameFull(2));
        $this->assertEquals('3', $game->periodName(3));
        $this->assertEquals('3rd', $game->periodNameMid(3));
        $this->assertEquals('3rd Period', $game->periodNameFull(3));
        $this->assertEquals('OT', $game->periodName(4));
        $this->assertEquals('OT', $game->periodNameMid(4));
        $this->assertEquals('Overtime', $game->periodNameFull(4));
        $this->assertEquals('SO', $game->periodName(5));
        $this->assertEquals('SO', $game->periodNameMid(5));
        $this->assertEquals('Shootout', $game->periodNameFull(5));

        // Playoffs.
        $game->game_type = 'playoff';
        $this->assertEquals('1', $game->periodName(1));
        $this->assertEquals('1st', $game->periodNameMid(1));
        $this->assertEquals('1st Period', $game->periodNameFull(1));
        $this->assertEquals('2', $game->periodName(2));
        $this->assertEquals('2nd', $game->periodNameMid(2));
        $this->assertEquals('2nd Period', $game->periodNameFull(2));
        $this->assertEquals('3', $game->periodName(3));
        $this->assertEquals('3rd', $game->periodNameMid(3));
        $this->assertEquals('3rd Period', $game->periodNameFull(3));
        $this->assertEquals('OT', $game->periodName(4));
        $this->assertEquals('OT', $game->periodNameMid(4));
        $this->assertEquals('1st Overtime', $game->periodNameFull(4));
        $this->assertEquals('2OT', $game->periodName(5));
        $this->assertEquals('2OT', $game->periodNameMid(5));
        $this->assertEquals('2nd Overtime', $game->periodNameFull(5));
        $this->assertEquals('3OT', $game->periodName(6));
        $this->assertEquals('3OT', $game->periodNameMid(6));
        $this->assertEquals('3rd Overtime', $game->periodNameFull(6));
    }
}
