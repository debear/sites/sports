<?php

namespace Tests\PHPUnit\Unit\AHL;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Sports\MajorLeague\AHL\PlayoffSeeds;
use DeBear\Repositories\InternalCache;

class FPlayoffTest extends UnitTestCase
{
    /**
     * Testing the building of seed codes for playoff teams
     * @return void
     */
    public function testSeedCodes(): void
    {
        // Our in-memory seed record.
        $seed = new PlayoffSeeds([
            'seed' => 5,
            'pos_div' => 3,
            'team' => (object)[
                'division' => (object)[
                    'name' => 'Test',
                ],
            ],
        ]);

        // Create our initial configuration - conference-based playoffs.
        InternalCache::object()->set('playoff-config', ['divisional' => false]);
        $this->assertEquals(5, $seed->seed_full);

        // Then switch our configuration to division-based playoffs with no wildcard.
        InternalCache::object()->set('playoff-config', ['divisional' => ['wc' => false]]);
        $this->assertEquals('T3', $seed->seed_full);

        // Finally a configuration with division-based playoffs including a wildcard.
        InternalCache::object()->set('playoff-config', ['divisional' => ['wc' => true, 'num' => 2]]);
        $this->assertEquals('WC', $seed->seed_full);

        // Ensure our configuration is cleared for future tests.
        InternalCache::object()->unset('playoff-config');
    }
}
