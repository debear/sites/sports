<?php

namespace Tests\PHPUnit\Unit\MLB;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\MajorLeague\MLB\Schedule;
use DeBear\ORM\Sports\MajorLeague\MLB\PlayoffSeries;

class BScheduleTest extends UnitTestCase
{
    /**
     * Testing various schedule title permutations on in-memory objects
     * @return void
     */
    public function testScheduleTitles(): void
    {
        // Apply some specific MLB config.
        $this->setTestConfig([
            'debear.setup.playoffs' => FrameworkConfig::get('debear.sports.subsites.mlb.setup.playoffs'),
        ]);

        // Wild Card Game.
        $game = new Schedule([
            'season' => 2019, // A season with the single game.
            'game_type' => 'playoff',
            'game_id' => 101,
            'home' => (object)[
                'conference' => (object)[
                    'name_full' => 'National League',
                ],
            ],
        ]);
        $this->assertEquals('National League Wild Card Game', $game->title);
        // Wild Card Series.
        $game->season = 2020;
        $this->assertEquals('National League Wild Card Series, Game 1', $game->title);
        // LDS.
        $game->game_id = 201;
        $this->assertEquals('National League Division Series, Game 1', $game->title);
        // LCS.
        $game->game_id = 301;
        $this->assertEquals('National League Championship Series, Game 1', $game->title);
        // World Series.
        $game->game_id = 401;
        $this->assertEquals('World Series, Game 1', $game->title);
    }

    /**
     * Testing various (short) game status permutations on in-memory objects
     * @return void
     */
    public function testStatusDisp(): void
    {
        // Regular game.
        $game = new Schedule([
            'status' => 'F',
            'innings' => 9,
        ]);
        $this->assertEquals('', $game->status_disp);
        // Extra innings.
        $game->innings = 10;
        $this->assertEquals('F/10', $game->status_disp);
        // Called early.
        $game->innings = 7;
        $this->assertEquals('F/7', $game->status_disp);
        // Suspended.
        $game->status = 'SSP';
        $this->assertEquals('SSP', $game->status_disp);
        // Postponed.
        $game->status = 'PPD';
        $this->assertEquals('PPD', $game->status_disp);
        // Cancelled.
        $game->status = 'CNC';
        $this->assertEquals('CNC', $game->status_disp);
    }

    /**
     * Testing various series summary permutations on in-memory objects
     * @return void
     */
    public function testSeriesSummaries(): void
    {
        // Apply some specific MLB config.
        $this->setTestConfig([
            'debear.setup.playoffs' => FrameworkConfig::get('debear.sports.subsites.mlb.setup.playoffs'),
        ]);

        // Tied series.
        $series = new PlayoffSeries([
            'season' => 2019,
            'round_code' => 21,
            'team_id_higher' => 'WSH',
            'team_id_lower' => 'MIL',
            'higher' => (object)[
                'city' => 'Washington',
                'franchise' => 'Nationals',
            ],
            'lower' => (object)[
                'city' => 'Milwaukee',
                'franchise' => 'Brewers',
            ],
            'higher_games' => 0,
            'lower_games' => 0,
        ]);
        $this->assertEquals('Series tied 0&ndash;0', $series->series_summary);
        // Lower Seed Leads.
        $series->lower_games = 1;
        $this->assertEquals('Brewers lead the series, 1&ndash;0', $series->series_summary);
        // Higher Seed Leads.
        $series->lower_games = 0;
        $series->higher_games = 1;
        $this->assertEquals('Nationals lead the series, 1&ndash;0', $series->series_summary);
        // 2019 Wild Card was single game series, so Game 1 winner advances.
        $series->round_code = 11;
        $this->assertEquals('Nationals advance to the next round', $series->series_summary);
    }
}
