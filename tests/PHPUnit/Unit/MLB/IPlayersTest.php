<?php

namespace Tests\PHPUnit\Unit\MLB;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonBattingZones;
use DeBear\ORM\Sports\MajorLeague\MLB\PlayerGamePitching;
use DeBear\ORM\Sports\MajorLeague\MLB\PlayerRepertoire;

class IPlayersTest extends UnitTestCase
{
    /**
     * Testing heatzone quirks
     * @return void
     */
    public function testHeatzones(): void
    {
        $obj = new PlayerSeasonBattingZones([
            'heatzones' => '[0,0,0],[1,1,1]',
            'cmp_avg' => 0.200,
        ]);
        // As batting averages.
        $this->assertEquals('&ndash;', $obj->heatzone_itl_avg);
        $this->assertEquals('1.000', $obj->heatzone_itc_avg);
        // And then as zone colours.
        $this->assertEquals('#888888', $obj->heatzone_itl_colour);
        $this->assertEquals('#ff0000', $obj->heatzone_itc_colour);
    }

    /**
     * Testing pitching decisions as CSS
     * @return void
     */
    public function testPitchingDecisionCSS(): void
    {
        $obj = new PlayerGamePitching(['w' => 1, 'l' => 0, 'hld' => 0, 'sv' => 0, 'bs' => 0]);
        $this->assertEquals('win', $obj->decisionCSS());
        $obj = new PlayerGamePitching(['w' => 0, 'l' => 1, 'hld' => 0, 'sv' => 0, 'bs' => 0]);
        $this->assertEquals('loss', $obj->decisionCSS());
        $obj = new PlayerGamePitching(['w' => 0, 'l' => 0, 'hld' => 1, 'sv' => 0, 'bs' => 0]);
        $this->assertEquals('hold', $obj->decisionCSS());
        $obj = new PlayerGamePitching(['w' => 0, 'l' => 0, 'hld' => 0, 'sv' => 1, 'bs' => 0]);
        $this->assertEquals('save', $obj->decisionCSS());
        $obj = new PlayerGamePitching(['w' => 0, 'l' => 0, 'hld' => 0, 'sv' => 0, 'bs' => 1]);
        $this->assertEquals('blown-save', $obj->decisionCSS());
    }

    /**
     * Testing pitcher repertoire quirks
     * @return void
     */
    public function testPitcherRepertoire(): void
    {
        $obj = new PlayerRepertoire([
            'pitch1' => 'FF',
            'pitch1_pct' => 60,
            'pitch2' => 'SL',
            'pitch2_pct' => 30,
            'pitch3' => 'CU',
            'pitch3_pct' => 10,
        ]);
        $chart = $obj->repertoireChart('test');
        $series = $chart['series'][0];
        $this->assertEquals('Pitch Type', $series['name']);
        $this->assertEquals(3, count($series['data']));
    }
}
