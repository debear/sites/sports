<?php

namespace Tests\PHPUnit\Unit\MLB;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;

class GStatsTest extends UnitTestCase
{
    /**
     * Testing NULL handling in the stat objects
     * @return void
     */
    public function testNULLChecker(): void
    {
        foreach (FrameworkConfig::get('debear.sports.subsites.mlb.setup.stats.details') as $type) {
            foreach ($type as $stat_config) {
                $sorted_class = $stat_config['class']['sort'];
                $stat_class = (new $sorted_class())->getStatsClass();
                $obj = new $stat_class([
                    // Something that realistically won't be formatted.
                    'ci_known' => 1,
                ]);
                // Ensure our known stats format correctly.
                $this->assertEquals(1, $obj->format('ci_known'));
                // And now our unknown stat.
                $this->assertEquals('&ndash;', $obj->format('ci_unknown'));
            }
        }
    }
}
