<?php

namespace Tests\PHPUnit\Unit\MLB;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\MajorLeague\MLB\TeamHistoryPlayoff;
use DeBear\ORM\Sports\MajorLeague\MLB\Team;

class HTeamsTest extends UnitTestCase
{
    /**
     * Testing various methods around historical playoff series
     * @return void
     */
    public function testHistoricalPlayoffs(): void
    {
        // Apply some specific MLB config.
        $this->setTestConfig([
            'debear.section.namespace' => 'MajorLeague',
            'debear.section.class' => 'MLB',
        ]);

        // Tied series (before it starts progress).
        $series = new TeamHistoryPlayoff([
            'season' => 2019,
            'for' => 0,
            'agains' => 0,
        ]);
        $this->assertEquals('', $series->result);
        $this->assertEquals('result-tie', $series->textCSS());

        // Won series.
        $series->for = 1;
        $this->assertEquals('Won', $series->result);
        $this->assertEquals('result-win', $series->textCSS());

        // Tied series (when in progress).
        $series->against = 1;
        $this->assertEquals('Tied', $series->result);
        $this->assertEquals('result-tie', $series->textCSS());

        // Lost series.
        $series->against = 2;
        $this->assertEquals('Lost', $series->result);
        $this->assertEquals('result-loss', $series->textCSS());
    }

    /**
     * Testing image processing methods
     * @return void
     */
    public function testImagery(): void
    {
        $orig = FrameworkConfig::get('debear.cdn');
        FrameworkConfig::set(['debear.cdn' => ['sizes' => ['test' => ['w' => 150, 'h' => 100]]]]);
        // Apply our test.
        $team = Team::query()->where('team_id', 'MIL')->get();
        $this->assertEquals('//cdn.debear.test/gCVI_1A4jkaorXB51jn_qvJGHRBO3EStU91roSLK-'
            . 'CLWfbzITBLpSLO1iSosCV1YrKoADN1A7yQVDN1c7KTpNe', $team->imageURL('test'));
        $this->assertEquals('//cdn.debear.test/tQhuYCQA0AZ1SLvC1T-SLK1S1P5izMX91roSLK-'
            . 'CLWfbzITBLpSLO1iSosCV1YrKoADN1A7yQVDN1c7KTpNe', Team::silhouetteURL('test'));
        // Restore the config we modified.
        FrameworkConfig::set(['debear.cdn' => $orig]);
    }
}
