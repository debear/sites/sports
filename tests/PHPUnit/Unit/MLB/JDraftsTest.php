<?php

namespace Tests\PHPUnit\Unit\MLB;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Sports\MajorLeague\MLB\Team;
use DeBear\ORM\Sports\MajorLeague\MLB\Draft;

class JDraftsTest extends UnitTestCase
{
    /**
     * Testing summary info
     * @return void
     */
    public function testSummary(): void
    {
        $team = new Team([
            'team_id' => 'TST',
            'city' => 'City',
            'franchise' => 'Franchise',
        ]);
        $obj = new Draft([
            'season' => 2019,
            'round' => 1,
            'pick' => 1,
            'team' => $team,
        ]);
        $this->assertEquals('1st Round, 1st Overall in 2019 by <span class="team--TST">'
            . '<span class="hidden-m hidden-tp">City</span> Franchise</span>', $obj->summary);
    }
}
