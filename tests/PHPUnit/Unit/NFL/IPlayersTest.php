<?php

namespace Tests\PHPUnit\Unit\NFL;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\MajorLeague\NFL\PlayerInjury;

class IPlayersTest extends UnitTestCase
{
    /**
     * A test of the player injury model.
     * @return void
     */
    public function testInjuries(): void
    {
        // Apply some specific NFL config.
        $this->setTestConfig([
            'debear.setup.players' => FrameworkConfig::get('debear.sports.subsites.nfl.setup.players'),
        ]);

        $injury = new PlayerInjury([
            'status' => 'q',
        ]);
        $this->assertEquals('q', $injury->status);
        $this->assertEquals('Q', $injury->status_short);
        $this->assertEquals('Questionable', $injury->status_long);
    }
}
