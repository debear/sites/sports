<?php

namespace Tests\PHPUnit\Unit\NFL;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Sports\MajorLeague\NFL\ScheduleDate;
use DeBear\ORM\Sports\MajorLeague\NFL\Schedule;
use DeBear\Helpers\Sports\Common\Traits\PHPUnit\CustomiseTest as SportsCustomiseTestTrait;

class BScheduleTest extends UnitTestCase
{
    use SportsCustomiseTestTrait;

    /**
     * Testing various schedule title permutations on in-memory objects
     * @return void
     */
    public function testScheduleTitles(): void
    {
        // Apply some specific NFL config.
        $this->setTestConfig([
            'debear.setup.playoffs' => FrameworkConfig::get('debear.sports.subsites.nfl.setup.playoffs'),
        ]);

        // Regular season.
        $game = new Schedule([
            'season' => 2019,
            'game_type' => 'regular',
            'week' => 1,
            'home' => (object)[
                'conference' => (object)[
                    'name' => 'NFC',
                ],
            ],
        ]);
        $this->assertEquals('', $game->title);
        $this->assertEquals('', $game->title_short);

        // Wild Card Round.
        $game->game_type = 'playoff';
        $game = new Schedule([
            'season' => 2019,
            'game_type' => 'playoff',
            'week' => 1,
            'home' => (object)[
                'conference' => (object)[
                    'name' => 'NFC',
                ],
            ],
        ]);
        $this->assertEquals('', $game->title);
        $this->assertEquals('NFC WC', $game->title_short);
        // Division Round.
        $game->week = 2;
        $this->assertEquals('', $game->title);
        $this->assertEquals('NFC Div', $game->title_short);
        // Conference Championship.
        $game->week = 3;
        $this->assertEquals('NFC Conference Championship', $game->title);
        $this->assertEquals('NFC Conf', $game->title_short);
        // Super Bowl.
        $game->week = 4;
        $this->assertEquals('Super Bowl LIV', $game->title);
        $this->assertEquals('SB LIV', $game->title_short);
        // Roman Numberal Exception.
        $game->season = 2015;
        $this->assertEquals('Super Bowl 50', $game->title);
        $this->assertEquals('SB 50', $game->title_short);
    }

    /**
     * Testing various schedule title permutations on in-memory objects
     * @return void
     */
    public function testScheduleNumPeriods(): void
    {
        // Regulation.
        $game = new Schedule([
            'status' => 'F',
        ]);
        $this->assertEquals(4, $game->num_periods);
        // Overtime.
        $game->status = 'OT';
        $this->assertEquals(5, $game->num_periods);
        // Multiple OTs.
        $game->status = '2OT';
        $this->assertEquals(6, $game->num_periods);
    }

    /**
     * Testing various schedule period name permutations on in-memory objects
     * @return void
     */
    public function testSchedulePeriodNames(): void
    {
        // Regular season.
        $game = new Schedule();
        $this->assertEquals('1', $game->periodName(1));
        $this->assertEquals('1st', $game->periodNameMid(1));
        $this->assertEquals('1st Quarter', $game->periodNameFull(1));
        $this->assertEquals('2', $game->periodName(2));
        $this->assertEquals('2nd', $game->periodNameMid(2));
        $this->assertEquals('2nd Quarter', $game->periodNameFull(2));
        $this->assertEquals('3', $game->periodName(3));
        $this->assertEquals('3rd', $game->periodNameMid(3));
        $this->assertEquals('3rd Quarter', $game->periodNameFull(3));
        $this->assertEquals('4', $game->periodName(4));
        $this->assertEquals('4th', $game->periodNameMid(4));
        $this->assertEquals('4th Quarter', $game->periodNameFull(4));
        $this->assertEquals('OT', $game->periodName(5));
        $this->assertEquals('OT', $game->periodNameMid(5));
        $this->assertEquals('Overtime', $game->periodNameFull(5));
        $this->assertEquals('2OT', $game->periodName(6));
        $this->assertEquals('2OT', $game->periodNameMid(6));
        $this->assertEquals('2nd Overtime', $game->periodNameFull(6));
    }

    /**
     * Testing the schedule loader when disparity between seasons after a team move
     * @return void
     */
    public function testTeamTransfer(): void
    {
        // Apply the NFL configuration.
        $this->setTestConfig([
            'debear.setup.season.viewing' => 2019,
            'debear.section.namespace' => 'MajorLeague',
            'debear.section.class' => 'NFL',
        ]);
        // The test coverage includes a user component, so prepare the object for our Unit test.
        User::setup();

        // Pin the date to Week 17 in 2019, and get the games to ensure Oakland is listed rather than Las Vegas.
        $this->setTestDateTime('2019-12-28 12:34:56');
        // Get the schedule for the current week - before Oakland's move to Las Vegas.
        $game_date = ScheduleDate::getLatest();
        $schedule = Schedule::loadByDate($game_date);
        // Target OAK @ DEN.
        $game = $schedule->where('visitor_id', 'OAK');
        $this->assertTrue($game->isset());
        $this->assertEquals('OAK', $game->visitor_id);
        $this->assertEquals('DEN', $game->home_id);
        $this->assertEquals('Oakland Raiders', $game->visitor->name);
        $this->assertEquals('Oakland Coliseum', $game->visitor->currentVenue->stadium);
        $this->assertEquals('AFC', $game->visitor->conference->name);
        $this->assertEquals('West', $game->visitor->division->name);
        $this->assertEquals('AFC West', $game->visitor->division->name_full);
        // Check for no LV games.
        $this->assertFalse($schedule->where('visitor_id', 'LV')->isset());
        $this->assertFalse($schedule->where('home_id', 'LV')->isset());

        // Get the schedule for Week 1 in 2020, when we're viewing as 2019 - after Oakland's move to Las Vegas.
        $game_date = ScheduleDate::loadFromArgument('2020-week-1');
        $schedule = Schedule::loadByDate($game_date);
        // Target LV @ CAR.
        $game = $schedule->where('visitor_id', 'LV');
        $this->assertTrue($game->isset());
        $this->assertEquals('LV', $game->visitor_id);
        $this->assertEquals('CAR', $game->home_id);
        $this->assertEquals('Las Vegas Raiders', $game->visitor->name);
        $this->assertEquals('Allegiant Stadium', $game->visitor->currentVenue->stadium);
        $this->assertEquals('AFC', $game->visitor->conference->name);
        $this->assertEquals('West', $game->visitor->division->name);
        $this->assertEquals('AFC West', $game->visitor->division->name_full);
        // Check for no OAK games.
        $this->assertFalse($schedule->where('visitor_id', 'OAK')->isset());
        $this->assertFalse($schedule->where('home_id', 'OAK')->isset());
    }

    /**
     * Testing the schedule result calculator
     * @return void
     */
    public function testGameResult(): void
    {
        // Team win.
        $game = new Schedule([
            'team_type' => 'home',
            'team_opp' => 'visitor',
            'home_score' => 21,
            'visitor_score' => 14,
        ]);
        $this->assertEquals('Win', $game->result());
        $this->assertEquals('W', $game->resultCode());

        // Team loss.
        $game->visitor_score = 24;
        $this->assertEquals('Loss', $game->result());
        $this->assertEquals('L', $game->resultCode());

        // Team tie.
        $game->home_score = 24;
        $this->assertEquals('Tie', $game->result());
        $this->assertEquals('T', $game->resultCode());
    }
}
