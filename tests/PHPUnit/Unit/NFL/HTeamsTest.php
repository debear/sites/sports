<?php

namespace Tests\PHPUnit\Unit\NFL;

use Tests\PHPUnit\Base\UnitTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\ORM\Sports\MajorLeague\NFL\TeamNaming;
use DeBear\ORM\Sports\MajorLeague\NFL\Standings;

class HTeamsTest extends UnitTestCase
{
    /**
     * Testing various methods around previous team names
     * @return void
     */
    public function testNaming(): void
    {
        // Apply some specific NFL config.
        $this->setTestConfig(['debear.section.endpoint' => 'nfl', 'debear.section.code' => 'nfl']);

        // Current instance.
        $curr = new TeamNaming([
            'team_id' => 'LV',
            'alt_team_id' => null,
        ]);
        $this->assertEquals('row_nfl-LV', $curr->rowCSS());
        $this->assertEquals('team-nfl-LV', $curr->logoCSS());
        $this->assertEquals('team-nfl-small-LV', $curr->logoCSS('small'));

        // Previous instance.
        $prev = new TeamNaming([
            'team_id' => 'LV',
            'alt_team_id' => 'OAK',
        ]);
        $this->assertEquals('row_nfl-OAK', $prev->rowCSS());
        $this->assertEquals('team-nfl-OAK', $prev->logoCSS());
        $this->assertEquals('team-nfl-small-OAK', $prev->logoCSS('small'));
    }

    /**
     * Testing various elements of loading team standings
     * @return void
     */
    public function testStandings(): void
    {
        // Apply some specific NFL config.
        $this->setTestConfig([
            'debear.section.namespace' => FrameworkConfig::get('debear.sports.subsites.nfl.section.namespace'),
            'debear.section.class' => FrameworkConfig::get('debear.sports.subsites.nfl.section.class'),
            'debear.setup.season.viewing' => 2020,
        ]);

        // Load the historical team.
        $standing = Standings::query()
        ->where([
            ['season', '=', 2019],
            ['week', '=', 11],
            ['team_id', '=', 'OAK']
        ])->get()
        ->loadSecondary(['teams']);
        $this->assertEquals(6, $standing->wins);
        $this->assertEquals(4, $standing->loss);
        $this->assertEquals(0, $standing->ties);
        $this->assertEquals('Oakland', $standing->team->city);
        $this->assertEquals('Raiders', $standing->team->franchise);
    }
}
