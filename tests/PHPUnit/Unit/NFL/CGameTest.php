<?php

namespace Tests\PHPUnit\Unit\NFL;

use Tests\PHPUnit\Base\UnitTestCase;
use DeBear\ORM\Sports\MajorLeague\NFL\GameDrive;
use DeBear\ORM\Sports\MajorLeague\NFL\GameEvent;

class CGameTest extends UnitTestCase
{
    /**
     * Testing various game drive permutations on in-memory objects
     * @return void
     */
    public function testGameDrives(): void
    {
        // Own Half -> Opp Half.
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => 'OWN', 'start_pos_yd' => 30, 'yards_net' => 40,
        ]);
        $this->assertEquals('Own 30', $drive->start_pos);
        $this->assertEquals('Opp 30', $drive->end_pos);
        // Own Half -> Opp Half (Touchdown).
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => 'OWN', 'start_pos_yd' => 30, 'yards_net' => 70,
        ]);
        $this->assertEquals('Own 30', $drive->start_pos);
        $this->assertEquals('Endzone', $drive->end_pos);
        // Own Half -> Own Half.
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => 'OWN', 'start_pos_yd' => 30, 'yards_net' => 10,
        ]);
        $this->assertEquals('Own 30', $drive->start_pos);
        $this->assertEquals('Own 40', $drive->end_pos);
        // Own Half -> Own Half (Backwards).
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => 'OWN', 'start_pos_yd' => 30, 'yards_net' => -10,
        ]);
        $this->assertEquals('Own 30', $drive->start_pos);
        $this->assertEquals('Own 20', $drive->end_pos);
        // Own Half -> Midfield.
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => 'OWN', 'start_pos_yd' => 30, 'yards_net' => 20,
        ]);
        $this->assertEquals('Own 30', $drive->start_pos);
        $this->assertEquals('Midfield', $drive->end_pos);
        // Midfield -> Opp Half.
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => null, 'start_pos_yd' => 50, 'yards_net' => 10,
        ]);
        $this->assertEquals('Midfield', $drive->start_pos);
        $this->assertEquals('Opp 40', $drive->end_pos);
        // Opp Half -> Midfield (Backwards).
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => 'OPP', 'start_pos_yd' => 30, 'yards_net' => -20,
        ]);
        $this->assertEquals('Opp 30', $drive->start_pos);
        $this->assertEquals('Midfield', $drive->end_pos);
        // Opp Half -> Own Half (Backwards).
        $drive = new GameDrive([
            'team_id' => 'OWN', 'start_pos_half' => 'OPP', 'start_pos_yd' => 30, 'yards_net' => -30,
        ]);
        $this->assertEquals('Opp 30', $drive->start_pos);
        $this->assertEquals('Own 40', $drive->end_pos);
    }

    /**
     * Testing various touchdown scoring play permutations on in-memory objects
     * @return void
     */
    public function testScoringTD(): void
    {
        /* By each type */
        $score = new GameEvent(['type' => 'td', 'team_id' => 'TEAM']);
        // Pass.
        $score->td = (object)['type' => 'pass', 'qb' => 12, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd pass from {{TEAM_12}}', $score->play);
        // Rush.
        $score->td = (object)['type' => 'rush', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd rush', $score->play);
        // INT Ret.
        $score->td = (object)['type' => 'int_ret', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd interception return', $score->play);
        // Fumb Ret.
        $score->td = (object)['type' => 'fumb_ret', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd fumble return', $score->play);
        // Fumb Rec.
        $score->td = (object)['type' => 'fumb_rec', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd fumble recovery', $score->play);
        // Kick Ret.
        $score->td = (object)['type' => 'kick_ret', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd kickoff return', $score->play);
        // Kick Rec.
        $score->td = (object)['type' => 'kick_rec', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd kickoff recovery', $score->play);
        // Punt Ret.
        $score->td = (object)['type' => 'punt_ret', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd punt return', $score->play);
        // Block Punt Ret.
        $score->td = (object)['type' => 'block_punt_ret', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd blocked punt return', $score->play);
        // Block Punt Rec.
        $score->td = (object)['type' => 'block_punt_rec', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd blocked punt recovery', $score->play);
        // Block FG Ret.
        $score->td = (object)['type' => 'block_fg_ret', 'qb' => null, 'scorer' => 21, 'length' => 20];
        $this->assertEquals('{{TEAM_21}} 20yd blocked field goal return', $score->play);
    }

    /**
     * Testing various touchdown coversion scoring play permutations on in-memory objects
     * @return void
     */
    public function testScoringTDCnv(): void
    {
        /* By each type */
        $score = new GameEvent(['type' => 'td', 'team_id' => 'TEAM', 'td' => (object)[
            'type' => 'pass', 'qb' => 12, 'scorer' => 21, 'length' => 20,
        ]]);

        // PAT (Good).
        $score->type = 'td+pat';
        $score->pat = (object)['kicker' => 15, 'made' => 1];
        $this->assertEquals('{{TEAM_21}} 20yd pass from {{TEAM_12}} ({{TEAM_15}} kick)', $score->play);
        // PAT (No Good).
        $score->type = 'td-pat';
        $score->pat = (object)['kicker' => 15, 'made' => 0];
        $this->assertEquals('{{TEAM_21}} 20yd pass from {{TEAM_12}} ({{TEAM_15}} kick is no good)', $score->play);
        // PAT (No Good, Unknown Kicker).
        $score->type = 'td-pat';
        $score->pat = (object)['kicker' => null, 'made' => 0];
        $this->assertEquals('{{TEAM_21}} 20yd pass from {{TEAM_12}} (Kick is no good)', $score->play);
        // Two Point (Pass: Good).
        $score->type = 'td+2pt';
        $score->twopt = (object)['type' => 'pass', 'qb' => 12, 'player' => 21, 'made' => 1];
        $this->assertEquals('{{TEAM_21}} 20yd pass from {{TEAM_12}} (2pt pass from {{TEAM_12}}'
            . ' to {{TEAM_21}})', $score->play);
        // Two Point (Rush: Good).
        $score->type = 'td+2pt';
        $score->twopt = (object)['type' => 'rush', 'qb' => null, 'player' => 21, 'made' => 1];
        $this->assertEquals('{{TEAM_21}} 20yd pass from {{TEAM_12}} (2pt rush by {{TEAM_21}})', $score->play);
        // Two Point (Unknown Type: No Good).
        $score->type = 'td-2pt';
        $score->twopt = (object)['type' => null, 'qb' => null, 'player' => 21, 'made' => 0];
        $this->assertEquals('{{TEAM_21}} 20yd pass from {{TEAM_12}} (2pt conversion by {{TEAM_21}}'
            . ' failed)', $score->play);
    }

    /**
     * Testing various field goal scoring play permutations on in-memory objects
     * @return void
     */
    public function testScoringFG(): void
    {
        $score = new GameEvent(['type' => 'fg', 'team_id' => 'TEAM', 'fg' => (object)['kicker' => 12, 'length' => 30]]);
        $this->assertEquals('{{TEAM_12}} 30yd field goal', $score->play);
    }

    /**
     * Testing various defensive 2pt conversion scoring play permutations on in-memory objects
     * @return void
     */
    public function testScoringDef2Pt(): void
    {
        $score = new GameEvent(['type' => 'def+2pt', 'team_id' => 'TEAM', 'twopt' => (object)['player' => 12]]);
        $this->assertEquals('{{TEAM_12}} defensive two point return', $score->play);
    }

    /**
     * Testing various safety scoring play permutations on in-memory objects
     * @return void
     */
    public function testScoringSafety(): void
    {
        // Base object.
        $score = new GameEvent(['type' => 'safety', 'team_id' => 'TEAM', 'opp_team_id' => 'OPP']);

        // Penalty (Unknown player).
        $score->sft = (object)['type' => 'penalty'];
        $this->assertEquals('Safety: Penalty enforced in the end zone', $score->play);
        // Penalty (Known player).
        $score->sft->player_off = 21;
        $this->assertEquals('Safety: Penalty on {{OPP_21}} enforced in the end zone', $score->play);

        // Tackle (No details).
        $score->sft = (object)['type' => 'tackle'];
        $this->assertEquals('Safety: Tackle in the end zone', $score->play);
        // Tackle (Partial details).
        $score->sft->player_off = 21;
        $this->assertEquals('Safety: {{OPP_21}} tackled in the end zone', $score->play);
        // Tackle (Full details).
        $score->sft->player_def = 12;
        $score->sft->player_def_alt = 13;
        $this->assertEquals('Safety: {{OPP_21}} tackled in the end zone by {{TEAM_12}} and {{TEAM_13}}', $score->play);

        // Sack (No details).
        $score->sft = (object)['type' => 'sack'];
        $this->assertEquals('Safety: Player sacked in the end zone', $score->play);
        // Sack (Partial details).
        $score->sft->player_off = 21;
        $this->assertEquals('Safety: {{OPP_21}} sacked in the end zone', $score->play);
        // Sack (Full details).
        $score->sft->player_def = 12;
        $score->sft->player_def_alt = 13;
        $this->assertEquals('Safety: {{OPP_21}} sacked in the end zone by {{TEAM_12}} and {{TEAM_13}}', $score->play);

        // Fumble (No details).
        $score->sft = (object)['type' => 'fumble'];
        $this->assertEquals('Safety: Fumble in the end zone', $score->play);
        // Fumble (Partial details).
        $score->sft->player_off = 21;
        $this->assertEquals('Safety: {{OPP_21}} fumbled in the end zone', $score->play);
        // Fumble (Full details).
        $score->sft->player_def = 12;
        $score->sft->player_def_alt = 13;
        $this->assertEquals('Safety: {{OPP_21}} fumbled in the end zone, forced by {{TEAM_12}}'
            . ' and {{TEAM_13}}', $score->play);

        // Punt Block (No details).
        $score->sft = (object)['type' => 'punt_block'];
        $this->assertEquals('Safety: Punt blocked in the end zone', $score->play);
        // Punt Block (Partial details).
        $score->sft->player_off = 21;
        $this->assertEquals('Safety: Punt by {{OPP_21}} blocked in the end zone', $score->play);
        // Punt Block (Full details).
        $score->sft->player_def = 12;
        $score->sft->player_def_alt = 13;
        $this->assertEquals('Safety: Punt by {{OPP_21}} blocked in the end zone by {{TEAM_12}}'
            . ' and {{TEAM_13}}', $score->play);

        // OB (No details).
        $score->sft = (object)['type' => 'ob'];
        $this->assertEquals('Safety: Player out-of-bounds', $score->play);
        // OB (Partial details).
        $score->sft->player_off = 21;
        $this->assertEquals('Safety: {{OPP_21}} out-of-bounds', $score->play);
        // OB (Full details).
        $score->sft->player_def = 12;
        $score->sft->player_def_alt = 13;
        $this->assertEquals('Safety: {{OPP_21}} out-of-bounds, forced by {{TEAM_12}} and {{TEAM_13}}', $score->play);
    }
}
