#!/bin/bash
# Enable the processIsolation on CI runners
dir_phpunit=$(realpath $(dirname $0)/..)
sed -i 's/processIsolation="false"/processIsolation="true"/' $dir_phpunit/00_core.xml
