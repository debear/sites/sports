#!/bin/bash
# Create fake flag files that are otherwise part of the CDN
dir_root=$(realpath $(dirname $0)/../../..)

# The flags we need to create
flags="ad ae af ag ai al am an ao ar as at au aw ax az ba bb bd be bf bg bh bi bj bl bm bn bo br bs bt bv bw by bz ca cc cd cf cg"
flags="$flags ch ci ck cl cm cn co cr cs cu cv cw cx cy cz de dj dk dm do dz ec ee eg eh er es es-ara es-cat es-mad es-val"
flags="$flags et eu fam fi fj fk fm fo fr ga gb gb-cym gb-eng gb-nir gb-sco gd ge gf gh gi gl gm gn gp gq gr gs gt gu gw gy"
flags="$flags hk hm hn hr ht hu id ie il in io iq ir is it-imo it jm jo jp ke kg kh ki km kn kp kr kw ky kz la lb lc li lk lr ls lt"
flags="$flags lu lv ly ma mc md me mf mg mh mk ml mm mn mo mp mq mr ms mt mu mv mw mx my mz na nc ndc ne nf ng ni nl no np nr nu nz"
flags="$flags om pac pa pe pf pg ph pk pl pm pn pr ps pt pw py qa re ro rs ru rw sa sb sca sc sd se sg sh si sj sk sl sm sn so sr"
flags="$flags ss st sv sx sy sz tc td tf tg th tj tk tl tm tn to tr tt tv tw tz ua ug um us us-ak us-al us-ar us-az us-ca us-co"
flags="$flags us-ct us-dc us-de us-fl us-ga us-hi us-ia us-id us-il us-in us-inp us-ks us-ky us-la us-ma us-md us-me us-mi us-mn"
flags="$flags us-mo us-ms us-mt us-nc us-nd us-ne us-nh us-nj us-nm us-nv us-ny us-oh us-ok us-or us-pa us-ri us-sc us-sd us-tn"
flags="$flags us-tx us-ut us-va us-vt us-wa us-wi us-wv us-wy uy uz va vc ve vg vi vn vu wf ws ye yt za zm zw"

# Convert the symlink to a folder and then touch the files
cd $dir_root/../_debear/resources/images/skel
rm -rf flags
mkdir -p flags/16
for f in ${flags[@]}
do
  touch flags/16/$f.png
done
