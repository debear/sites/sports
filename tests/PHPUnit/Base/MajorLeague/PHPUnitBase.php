<?php

namespace Tests\PHPUnit\Sports\MajorLeague;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\Common\Traits\PHPUnit\CustomiseTest as SportsCustomiseTestTrait;
use DeBear\Implementations\TestResponse;
use DeBear\Repositories\Time;

class PHPUnitBase extends FeatureTestCase
{
    use SportsCustomiseTestTrait;

    /**
     * Whether or not we should refresh the database between runs
     * @var boolean
     */
    protected $refresh_database = false;
    /**
     * The season we are nominally viewing.
     * @var integer
     */
    protected $season;

    /**
     * Update internal objects between tests
     * @return void
     */
    protected function setUp(): void
    {
        $sport = strtolower(array_slice(explode('\\', get_class($this)), -2, 1)[0]);
        $config = "debear.sports.subsites.$sport";
        // Load the standard test setup methods.
        parent::setUp();
        // Update the database timezone used.
        Time::object()->setDatabaseTimezone(FrameworkConfig::get("$config.datetime.timezone_db"));
        // Define the current season.
        $this->season = FrameworkConfig::get("$config.setup.season.viewing");
    }

    /**
     * Test a HTTP GET request on a tab that requires special headers added by JavaScript
     * @param string $uri     The URI we are accessing.
     * @param array  $headers Additional headers to pass to our test request.
     * @return TestResponse The returned information from the request
     */
    protected function getTab(string $uri, array $headers = []): TestResponse
    {
        // Add our special header before passing on to our standard GET processor.
        $headers['X-DeBearTab'] = 'true';
        return $this->get($uri, $headers);
    }
}
