<?php

namespace Tests\PHPUnit\Sports\Motorsport;

use Tests\PHPUnit\Base\FeatureTestCase;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Sports\Common\Traits\PHPUnit\CustomiseTest as SportsCustomiseTestTrait;
use DeBear\Helpers\Sports\Motorsport\Header;
use DeBear\Repositories\Time;

class PHPUnitBase extends FeatureTestCase
{
    use SportsCustomiseTestTrait;

    /**
     * Whether or not we should refresh the database between runs
     * @var boolean
     */
    protected $refresh_database = false;

    /**
     * Update internal objects between tests
     * @return void
     */
    protected function setUp(): void
    {
        $sport = strtolower(array_slice(explode('\\', get_class($this)), -2, 1)[0]);
        $config = "debear.sports.subsites.$sport";
        // Load the standard test setup methods.
        parent::setUp();
        // Update the database timezone used.
        Time::object()->setDatabaseTimezone(FrameworkConfig::get("$config.datetime.timezone_db"));
        // Clear the internal objects.
        Header::reset();
    }
}
