<?php

namespace DeBear\Http\Controllers\Sports\Motorsport;

use DeBear\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Interpolate;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;
use DeBear\Helpers\Sports\Motorsport\Controller as ControllerHelper;

class Participants extends Controller
{
    /**
     * List of participants, broken down by team or group
     * @param integer|null $season The season to load, or null if nothing explicitly passed.
     * @param string       $mode   The render mode we will display the page in.
     * @return View
     */
    public function index(?int $season = null, string $mode = 'full'): View
    {
        ManageViews::setMode($mode);

        // Get the per-subsite config (labels, etc).
        $config_code = 'participants';
        $section = FrameworkConfig::get("debear.links.$config_code");
        $type = FrameworkConfig::get('debear.setup.type');
        $title = $section['label'];

        // Custom page config.
        ControllerHelper::prepareRequest(['season' => $season]);
        HTML::setNavCurrent($section['url']);
        HTML::linkMetaDescription($config_code);
        HTML::setPageTitle(ControllerHelper::prependSeason($title));
        Resources::addCSS('skel/flags/48.css');
        Resources::addCSS('motorsport/pages/participants/list.css');

        // Load our data.
        if ($type == 'circuit') {
            // Teams and Participants.
            $teams = Namespaces::callStatic('Team', 'getBySeason');
            $type .= (FrameworkConfig::get('debear.setup.teams.history') ? '.expanded' : '.basic');
        } else {
            // Participants only. Construct (as faux-"teams") each group.
            $teams = Namespaces::callStatic('Participant', 'getBySeason');
        }
        // Type-specfic CSS.
        Resources::addCSS('motorsport/pages/participants/list/' . str_replace('.', '-', $type) . '.css');

        return view('sports.motorsport.participants.list', compact([
            'title',
            'type',
            'teams',
        ]));
    }

    /**
     * Individual participant details
     * @param string  $participant_name Codified participant name.
     * @param integer $participant_id   ID of the participant.
     * @param integer $season           The season to load, or null if nothing explicitly passed.
     * @return Response|RedirectResponse A 404 response if the participant could not be found, a View if so
     */
    public function show(string $participant_name, int $participant_id, ?int $season = null): Response|RedirectResponse
    {
        // Ensure we have a season to consider.
        $season_passed = $season;
        if (!isset($season)) {
            $season = FrameworkConfig::get('debear.setup.season.default');
        }
        // Prepare the processing.
        ControllerHelper::prepareRequest(['season' => $season]);
        $type = FrameworkConfig::get('debear.setup.type');
        $validate = Namespaces::callStatic('Participant', 'findByCodifiedID', [
            $participant_id,
            $participant_name,
            $season,
        ]);
        if (!isset($validate) || !$validate->count()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        } elseif (isset($season_passed) && ($validate->season != $season_passed)) {
            // If explicitly passed a season, but it's not valid, bounce to the equivalent season.
            return HTTP::redirectPage($validate->linkDefault(), 308);
        }

        // Get the full detail for this single participant.
        $season_viewing = $validate->season;
        $participant = Namespaces::callStatic('Participant', 'load', [$validate]);

        // Max positions by season.
        $max_pos = Namespaces::callStatic(
            'ParticipantHistory',
            'totalParticipantsBySeason',
            [$participant->history->pluck('season')]
        );

        // Custom page config.
        $section = FrameworkConfig::get('debear.links.participants');
        $title = $section['label'];
        HTML::setNavCurrent($section['url']);
        HTML::setMetaDescription(Interpolate::string($section['descrip_ind'], [
            'name' => $participant->name,
        ]));
        HTML::setPageTitle([$title, $participant->name]);
        Resources::addCSS('motorsport/pages/participants/individual.css');
        Resources::addCSS('motorsport/pages/participants/view.css');
        Resources::addCSS('motorsport/widgets/key.css');
        Resources::addCSS('motorsport/widgets/stats.css');
        Resources::addCSS('motorsport/pages/standings/key.css');
        Resources::addJS('motorsport/pages/participant.js');
        Resources::addJS('motorsport/widgets/key.js');
        Resources::addJS('motorsport/widgets/scrollable.js');

        // Current season standings (for the switcher).
        $standings = Namespaces::callStatic('ParticipantHistory', 'standingsBySeason', [[
            'season' => $season_viewing,
        ]]);
        // Define our list of subnav options.
        $label = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
        $subnav = [
            'list' => [
                'season' => "$label By $label",
                'stats' => 'Season Stats',
                'history' => 'Career History',
            ],
            'default' => 'season',
        ];
        return response()->view('sports.motorsport.participants.view', compact([
            'season',
            'season_viewing',
            'type',
            'participant',
            'standings',
            'max_pos',
            'subnav',
        ]));
    }

    /**
     * Management of legacy individual participant links
     * @param integer $season         The season we are viewing details for.
     * @param integer $participant_id The ID of the participant to be viewed.
     * @return Response|RedirectResponse A 404 response if the participant could not be found, a redirect if so
     */
    public function showLegacy(int $season, int $participant_id): Response|RedirectResponse
    {
        ControllerHelper::prepareRequest(['season' => $season]);
        // If this is valid, there will be a participant we can find in the database, so pull it out.
        $key = Namespaces::createObject('Participant')->getPrimaryKey();
        $part_history = Namespaces::callStatic('ParticipantHistory', 'query')
            ->where([
                ['season', $season],
                [$key, $participant_id],
            ])->get();
        if (!$part_history->isset()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        // Redirect to the modern convention (with a permanent hint).
        $participant = Namespaces::callStatic('Participant', 'query')
            ->where($key, $participant_id)
            ->get();
        return HTTP::redirectPage($participant->link(), 308);
    }

    /**
     * Management of legacy individual participant links, where the season is not passed in but implied
     * @param integer $participant_id The ID of the participant to be viewed.
     * @return Response|RedirectResponse A 404 response if the participant could not be found, a redirect if so
     */
    public function showLegacyCurrentSeason(int $participant_id): Response|RedirectResponse
    {
        ControllerHelper::setupSeason();
        return $this->showLegacy(FrameworkConfig::get('debear.setup.season.viewing'), $participant_id);
    }
}
