<?php

namespace DeBear\Http\Controllers\Sports\Motorsport;

use DeBear\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;
use DeBear\Helpers\Sports\Motorsport\Header;
use DeBear\Helpers\Sports\Motorsport\Controller as ControllerHelper;
use DeBear\Helpers\Sports\Motorsport\Traits\Controller as TraitController;

class Standings extends Controller
{
    use TraitController;

    /**
     * Standings for a given season
     * @param integer $season The season to load, or null if nothing explicitly passed.
     * @param string  $mode   The render mode we will display the page in.
     * @return View
     */
    public function index(?int $season = null, string $mode = 'full'): View
    {
        ManageViews::setMode($mode);

        // Custom page config.
        ControllerHelper::prepareRequest(['season' => $season]);
        HTML::setNavCurrent('/standings');
        HTML::linkMetaDescription('standings');
        HTML::setPageTitle($this->sectionTitle('Standings'));
        Resources::addCSS('motorsport/widgets/key.css');
        Resources::addCSS('motorsport/pages/standings.css');
        Resources::addCSS('motorsport/pages/standings/key.css');
        Resources::addJS('motorsport/pages/standings.js');
        Resources::addJS('motorsport/widgets/key.js');
        Resources::addJS('motorsport/widgets/scrollable.js');

        // Re-use the header's build calendar.
        $races = Header::getCalendar();

        // Get the details for the participants.
        $participants = Namespaces::callStatic('ParticipantHistory', 'standingsBySeason', [['expanded' => true]]);

        // And then (if enabled), the teams.
        $teams = null;
        if (FrameworkConfig::get('debear.setup.teams.history')) {
            $teams = Namespaces::callStatic('TeamHistory', 'standingsBySeason', [['expanded' => true]]);
        }

        return view('sports.motorsport.standings.view', compact([
            'races',
            'participants',
            'teams',
        ]));
    }
}
