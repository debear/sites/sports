<?php

namespace DeBear\Http\Controllers\Sports\Motorsport;

use DeBear\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;
use DeBear\Helpers\Sports\Motorsport\Controller as ControllerHelper;
use DeBear\Helpers\Sports\Motorsport\Traits\Controller as TraitController;
use DeBear\Repositories\InternalCache;

class Stats extends Controller
{
    use TraitController;

    /**
     * Stat Leaders for a given season
     * @param integer $season The season to load, or null if nothing explicitly passed.
     * @param string  $mode   The render mode we will display the page in.
     * @return View
     */
    public function index(?int $season = null, string $mode = 'full'): View
    {
        ManageViews::setMode($mode);

        // Custom page config.
        ControllerHelper::prepareRequest(['season' => $season]);
        HTML::setNavCurrent('/stats');
        HTML::linkMetaDescription('stats');
        HTML::setPageTitle($this->sectionTitle('Statistics'));
        Resources::addCSS('motorsport/widgets/stats.css');
        Resources::addCSS('skel/widgets/scrolltable.css');
        Resources::addCSS('motorsport/pages/stats.css');
        Resources::addJS('skel/widgets/scrolltable.js');
        Resources::addJS('skel/widgets/sorttable.js');
        Resources::addJS('motorsport/pages/stats.js');

        // Get the stat meta info (excluding those that do not apply for this season).
        $stats_meta_query = Namespaces::callStatic('Stats', 'query')
            ->orderBy('disp_order');
        if (FrameworkConfig::get('debear.setup.stats.seasons') !== null) {
            $viewing = FrameworkConfig::get('debear.setup.season.viewing');
            $seasons = FrameworkConfig::get('debear.setup.stats.seasons');
            $excl = array_keys(array_filter($seasons, function ($cfg) use ($viewing) {
                return ($viewing < $cfg['from'] || $viewing > $cfg['to']);
            }));
            $stats_meta_query->whereNotIn('stat_id', $excl);
        }
        $stats_meta = $stats_meta_query->get();
        InternalCache::object()->set('stats_meta', $stats_meta);

        // Get the participants stats.
        $participants = Namespaces::callStatic('ParticipantStats', 'loadBySeason');

        // Default subnav (which we also use for controlling display). Does not display if only one entry.
        $subnav = [
            'list' => [
                'participants' => ucfirst(FrameworkConfig::get('debear.setup.participants.url')),
            ],
            'default' => 'participants',
        ];

        // And then (if enabled), the teams.
        $teams = null;
        if (FrameworkConfig::get('debear.setup.teams.history')) {
            $teams = Namespaces::callStatic('TeamStats', 'loadBySeason');
            $subnav['list']['teams'] = 'Constructors';
        }

        return view('sports.motorsport.stats.view', compact([
            'subnav',
            'stats_meta',
            'participants',
            'teams',
        ]));
    }
}
