<?php

namespace DeBear\Http\Controllers\Sports\Motorsport;

use DeBear\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Format;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Interpolate;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Motorsport\Controller as ControllerHelper;

class Teams extends Controller
{
    /**
     * Individual team details
     * @param string  $team_name Codified team name that the outfit was known as that season.
     * @param integer $season    The season to load, or null if nothing explicitly passed.
     * @return Response|View A 404 response if the team could not be found, a View if so
     */
    public function show(string $team_name, ?int $season = null): Response|View
    {
        // Ensure we have a season to consider.
        if (!isset($season)) {
            $season = FrameworkConfig::get('debear.setup.season.default');
        }
        // Prepare the processing.
        ControllerHelper::prepareRequest(['season' => $season]);
        // Validate the info supplied.
        $validate = Namespaces::callStatic('Team', 'findByCodified', [$team_name, $season]);
        if (!isset($validate) || !$validate->count()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }

        // Get the full detail for this single team.
        $season_viewing = $validate->season;
        $team = Namespaces::callStatic('Team', 'load', [$validate]);

        // Custom page config.
        $section = FrameworkConfig::get('debear.links.participants');
        $title = $section['label'];
        HTML::setNavCurrent($section['url']);
        HTML::setMetaDescription(Interpolate::string($section['descrip_team'], [
            'name' => $team->team_name,
        ]));
        HTML::setPageTitle([ControllerHelper::prependSeason($title), $team->name]);
        Resources::addCSS('motorsport/pages/participants/individual.css');
        Resources::addCSS('motorsport/pages/participants/team.css');
        Resources::addCSS('motorsport/widgets/stats.css');
        Resources::addJS('motorsport/pages/team.js');

        // Current season standings (for the switcher).
        $standings = Namespaces::callStatic('TeamHistory', 'standingsBySeason', [[
            'season' => $season_viewing,
        ]]);
        // Define our list of subnav options.
        $label = Format::singularise(FrameworkConfig::get('debear.links.races.label'));
        $subnav = [
            'list' => [
                'season' => "$season_viewing By $label",
                'stats' => "$season_viewing Stats",
                'history' => 'Team History',
            ],
            'default' => 'season',
        ];
        return view('sports.motorsport.participants.team', compact([
            'season',
            'season_viewing',
            'team',
            'standings',
            'subnav',
        ]));
    }

    /**
     * Management of legacy individual team links
     * @param integer $season  The season that the team participated in.
     * @param integer $team_id The ID of the team to be viewed.
     * @return Response|RedirectResponse A 404 if the team could not be found, a redirect to the new link if so
     */
    public function showLegacy(int $season, int $team_id): Response|RedirectResponse
    {
        ControllerHelper::prepareRequest(['season' => $season]);
        // If this is valid, there will be a team we can find in the database, so pull it out.
        $team = Namespaces::callStatic('Team', 'query')
            ->where([
                ['season', $season],
                ['team_id', $team_id],
            ])
            ->get();
        if (!$team->isset()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        // Redirect to the modern convention (with a permanent hint).
        return HTTP::redirectPage($team->link(), 308);
    }

    /**
     * Management of legacy individual team links, where the season is not passed in but implied
     * @param integer $team_id The ID of the team to be viewed.
     * @return Response|RedirectResponse A 404 if the team could not be found, a redirect to the new link if so
     */
    public function showLegacyCurrentSeason(int $team_id): Response|RedirectResponse
    {
        ControllerHelper::setupSeason();
        return $this->showLegacy(FrameworkConfig::get('debear.setup.season.viewing'), $team_id);
    }
}
