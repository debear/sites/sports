<?php

namespace DeBear\Http\Controllers\Sports\Motorsport;

use Illuminate\Http\Response;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Sports\Motorsport\Controller as ControllerHelper;
use DeBear\Helpers\Sports\Motorsport\SiteTools as SiteToolsHelper;
use DeBear\Http\Controllers\Skeleton\Traits\SiteTools as SiteToolsTrait;

class SiteTools extends Controller
{
    use SiteToolsTrait;

    /**
     * Generate a sitemap for a particular part of the site
     * @param integer $season The season to load, or null if nothing explicitly passed.
     * @return Response A response object with the appropriate sitemap details as markup
     */
    public function sitemap(?int $season = null): Response
    {
        // Our page configuration.
        HTML::setPageTitle('Sitemap');
        ControllerHelper::prepareRequest(['season' => $season]);
        // Then manipulations.
        SiteToolsHelper::expandSitemap();
        // Then the standard processing from within the trait.
        return $this->sitemapWorker();
    }

    /**
     * Generate an XML version of the sitemap for a particular part of the site
     * @return Response A response object with the appropriate sitemap details as an XML sitemap
     */
    public function sitemapXml(): Response
    {
        // Apply our manipulations to the default in-page config.
        SiteToolsHelper::expandSitemapXml();
        // Then the standard processing from within the trait.
        return $this->sitemapXmlWorker();
    }
}
