<?php

namespace DeBear\Http\Controllers\Sports\Motorsport;

use DeBear\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\Arrays;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;
use DeBear\Helpers\Sports\Motorsport\Header;
use DeBear\Helpers\Sports\Motorsport\Controller as ControllerHelper;
use DeBear\Helpers\Sports\Motorsport\Traits\Controller as TraitController;

class Races extends Controller
{
    use TraitController;

    /**
     * Calendar of races for the give season
     * @param integer $season The season to load, or null if nothing explicitly passed.
     * @param string  $mode   The render mode we will display the page in.
     * @return View
     */
    public function index(?int $season = null, string $mode = 'full'): View
    {
        ManageViews::setMode($mode);

        // Setup our request / page.
        $with = ['participantsStandingsCurrent'];
        if (FrameworkConfig::get('debear.setup.type') == 'circuit') {
            $with = Arrays::merge($with, array_values(array_filter([
               'teams',
                FrameworkConfig::get('debear.setup.teams.history') ? 'teamsStandingsCurrent' : false,
               'qualifying',
               'grid',
            ])));
        }
        ControllerHelper::prepareRequest([
            'season' => $season,
            'with' => $with,
        ]);
        $link = FrameworkConfig::get('debear.links.races.url');
        HTML::setNavCurrent($link);
        HTML::linkMetaDescription('races');
        HTML::setPageTitle($this->sectionTitle(FrameworkConfig::get('debear.links.races.label')));
        Resources::addCSS('skel/flags/48.css');
        Resources::addCSS('motorsport/pages/races/list.css');

        // Re-use the header's build calendar.
        $races = Header::getCalendar();

        // And then another (single) query to get the races from the previous season.
        $calendar_season = $season ?? FrameworkConfig::get('debear.setup.season.viewing');
        $races_prev = Namespaces::callStatic('Race', 'previousCalendar', [$calendar_season]);

        // Process the race list into usable components for our view.
        // - Split in to upcoming / completed.
        $cmpl = [];
        foreach ($races as $race) {
            if ($race->isComplete() || $race->isCancelled()) {
                $cmpl[] = $race->round;
            }
        }
        // - Get the two highlight blocks - last and next (if possible).
        $num_cmpl = count($cmpl);
        if ($num_cmpl) {
            $slice_at = $num_cmpl - ($num_cmpl < $races->count() ? 1 : 2);
        } else {
            $slice_at = 0;
        }
        $blocks = $races->slice($slice_at, 2);
        $race_last = $blocks->first();
        $race_next = $blocks->last();
        // Now perform the upcoming / completed split.
        $races_cmpl = $races
            ->whereIn('round', $cmpl)
            ->whereNotIn('round', [$race_last->round, $race_next->round])
            ->sortByDesc('round_order');
        $races_upc = $races
            ->whereNotIn('round', array_merge($cmpl, [$race_last->round, $race_next->round]));

        // And now render.
        return view('sports.motorsport.races.list', compact([
            'races_prev',
            'race_last',
            'race_next',
            'races_upc',
            'races_cmpl',
        ]));
    }

    /**
     * Display the results of an individual race
     * @param integer $season The season the race occurred in.
     * @param string  $name   The codified country name of the race to be viewed.
     * @return Response|View A 404 response if the race could not be found, a View if so
     */
    public function show(int $season, string $name): Response|View
    {
        // Get the info from the season's header to validate.
        ControllerHelper::prepareRequest(['season' => $season]);
        $races = Header::getCalendar();
        $round = false;
        foreach ($races as $race) {
            if (($race->linkVenue() == $name) && $race->canLink()) {
                $round = $race->round;
            }
        }
        if ($round === false) {
            // No matches, not a valid argument.
            return HTTP::sendNotFound();
        }

        // Get the expanded info we need for this single race, not the whole calendar.
        $race = Namespaces::callStatic('Race', 'loadRace', [$season, $round]);

        // Extra info.
        $summary = false;
        $type = FrameworkConfig::get('debear.setup.type');
        if ($type == 'circuit') {
            Resources::addJS('motorsport/pages/races/circuit.js');
            // Setup the tabs.
            $has_qualifying = $race->qualifying->count();
            $sprint_race = $race->quirk('sprint_race');
            $sprint_shootout = $race->quirk('sprint_shootout');
            $two_race = isset($race->race2_time);
            $subnav = [
                'list' => array_filter([
                    'quali' => ($has_qualifying ? ($sprint_shootout ? 'Sprint Shootout' : 'Qualifying') : false),
                    'grid' => ($sprint_race ? 'Sprint Grid' : 'Starting Grid'),
                    'result' => ($sprint_race ? 'Sprint Race' : 'Race Result'),
                    'laps' => ($sprint_race ? false : 'Lap Leaders'),
                    'quali2' => ($sprint_shootout ? 'Qualifying' : false),
                    'grid2' => ($sprint_race ? 'Starting Grid' : ($two_race ? 'Race 2 Grid' : false)),
                    'result2' => ($sprint_race ? 'Race Result' : ($two_race ? 'Race 2 Result' : false)),
                    'laps2' => ($sprint_race ? 'Lap Leaders' : ($two_race ? 'Race 2 Leaders' : false)),
                    'champ' => 'Standings',
                ]),
                'default' => 'result' . ($sprint_race ? 2 : ''),
            ];
        } else {
            // Determine if the meeting was called before the knockout stage.
            $summary = [
                'quali_sprint' => $race->quirk('quali_sprint'),
                'heats' => $race->heats->where('heat_type', 'main')->max('heat'),
                'inc_ko' => ($race->heats->where('heat_type', '!=', 'main')->count() > 0),
            ];
            // Setup the tabs.
            $subnav = [
                'list' => array_filter([
                    'sprint' => $summary['quali_sprint'] ? 'Quali Sprint' : false,
                    'grid' => 'Heat Grid',
                    'list' => 'Heat List',
                    'finals' => $summary['inc_ko'] ? 'Knockout' : false,
                    'champ' => 'Standings',
                ]),
                'default' => 'grid',
            ];
        }

        // Page meta.
        $section = FrameworkConfig::get('debear.links.races');
        HTML::setNavCurrent($section['url']);
        HTML::setMetaDescription($section['descrip_ind'], [
            'season' => $race->season,
            'name' => $race->name_full,
        ]);
        HTML::setPageTitle([$this->sectionTitle(FrameworkConfig::get('debear.links.races.label')), $race->name_full]);
        Resources::addCSS('skel/flags/48.css');
        Resources::addCSS('skel/flags/64.css');
        Resources::addCSS('skel/flags/128.css');
        Resources::addCSS('motorsport/pages/races/view.css');
        Resources::addCSS("motorsport/pages/races/view/$type.css");
        Header::setRaceFocus($race->round);

        return view('sports.motorsport.races.view', compact([
            'type',
            'race',
            'subnav',
            'summary',
        ]));
    }

    /**
     * Management of legacy individual race links
     * @param integer $season The season the race occurred in.
     * @param integer $round  The round ID of the race to be viewed.
     * @return Response|RedirectResponse A 404 response if the race could not be found, an appropriate redirect if so
     */
    public function legacy(int $season, int $round): Response|RedirectResponse
    {
        // If this is valid, there will be a race we can find in the database, so pull it out.
        $race = Namespaces::callStatic('Race', 'loadRace', [$season, $round]);
        if (!$race->isset()) {
            // Invalid argument passed.
            ControllerHelper::prepareRequest(['season' => $season]);
            return HTTP::sendNotFound();
        }
        // Redirect to the modern convention (with a permanent hint).
        return HTTP::redirectPage($race->link(), 308);
    }

    /**
     * Management of legacy individual race links, where the season is not passed in but implied
     * @param integer $round The round ID of the race to be viewed.
     * @return Response|RedirectResponse A 404 response if the race could not be found, an appropriate redirect if so
     */
    public function legacyCurrentSeason(int $round): Response|RedirectResponse
    {
        ControllerHelper::setupSeason();
        return $this->legacy(FrameworkConfig::get('debear.setup.season.viewing'), $round);
    }
}
