<?php

namespace DeBear\Http\Controllers\Sports;

use DeBear\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Motorsport\Controller as ControllerHelper;
use DeBear\Models\Skeleton\News as NewsModel;

class Motorsport extends Controller
{
    /**
     * Motorsport-specific summary
     * @return View
     */
    public function index(): View
    {
        // Custom page config.
        ControllerHelper::prepareRequest();
        HTML::noHeaderLink();
        HTML::setNavCurrent('/');
        HTML::linkMetaDescription('home');
        Resources::addCSS('motorsport/pages/home.css');

        // Two races (Next/Last; Last Two).
        $races = Namespaces::callStatic('Race', 'getHomepageFocus');
        // News?
        $news = null;
        $app = FrameworkConfig::get('debear.news.app');
        if ($app) {
            $news = NewsModel::with('source')
                ->where('app', '=', $app)
                ->orderBy('published', 'desc')  // First order by published date.
                ->orderBy('news_id', 'desc')    // And then in the case of contention, ID, for consistency.
                ->limit(10)
                ->get();
            // Customise the CSP rules.
            NewsModel::updateCSP($app);
        }
        // Participant Standings.
        $participants = Namespaces::callStatic('ParticipantHistory', 'standingsBySeason');
        // Team Standings?
        $teams = null;
        if (FrameworkConfig::get('debear.setup.teams.history')) {
            $teams = Namespaces::callStatic('TeamHistory', 'standingsBySeason');
        }
        // Header season arg?
        $seasons = FrameworkConfig::get('debear.setup.season');
        $header_season = ($seasons['viewing'] != $seasons['default'] ? $seasons['viewing'] : '');

        // Render the view.
        return view('sports.motorsport.home.view', compact([
            'races',
            'news',
            'participants',
            'teams',
            'header_season',
        ]));
    }
}
