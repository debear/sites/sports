<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\ORM\Sports\MajorLeague\Player;

class Players extends Controller
{
    /**
     * Display the player list / search page
     * @return Response The relevant content response
     */
    public function index(): Response
    {
        // Get the info.
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        $groupings = Namespaces::callStatic('TeamGroupings', 'load', [$season]);
        $birthdays = Namespaces::callStatic('Player', 'loadBirthdays');
        $sport = FrameworkConfig::get('debear.section.code');

        // Custom page config.
        Resources::addCSS("$sport/misc/narrow_small.css");
        Resources::addCSS('major_league/pages/players.css');
        Resources::addJS('major_league/pages/players.js');
        HTML::setNavCurrent('/players');
        HTML::linkMetaDescription('players');
        HTML::setPageTitle(FrameworkConfig::get('debear.names.section') . ' Players');

        // Render the page.
        return response(view('sports.major_league.players', compact([
            'sport',
            'groupings',
            'birthdays',
        ])));
    }

    /**
     * Search for specific players by name
     * @param string $term The search term.
     * @return JsonResponse|Response The relevant content response or status code
     */
    public function search(string $term): JsonResponse|Response
    {
        // We only want to deal with search terms of three or more characters.
        if (strlen($term) < 3) {
            return HTTP::sendBadRequest();
        }

        // Run the search and return as a JSON array.
        $search = [];
        foreach (Namespaces::callStatic('Player', 'searchByName', [$term]) as $res) {
            $search[] = [
                'player_id' => $res->player_id,
                'first_name' => $res->first_name,
                'surname' => $res->surname,
                'url' => $res->link,
            ];
        }
        return response()->json($search);
    }

    /**
     * Display expanded info on a single player
     * @param string  $name      The codified player name.
     * @param integer $player_id The database ID for the player being requested.
     * @return Response The relevant content response
     */
    public function show(string $name, int $player_id): Response
    {
        // Validate the arguments.
        $player = $this->validateArguments($name, $player_id);
        if ($player instanceof Response) {
            return $player;
        }
        // Now load the additional secondary info for the player.
        $player->loadDetailedSecondary('home');
        $sport = FrameworkConfig::get('debear.section.code');

        // Setup the tabs.
        $tabs = FrameworkConfig::get('debear.setup.players.subnav');
        uasort($tabs, function ($a, $b) {
            return $a['order'] <=> $b['order'];
        });
        $subnav = [
            'list' => array_map(function ($a) {
                return $a['label'];
            }, $tabs),
            'default' => 'home',
        ];

        // Preserve (internally) the nonce for the Ajax tabs.
        $nonce = HTTP::getCSPNonce();
        foreach (array_keys($tabs) as $tab) {
            session(["ajax.players.$player_id.$tab" => $nonce]);
        }

        // Custom page config.
        Resources::addCSS('skel/widgets/scrolltable.css');
        Resources::addCSS("$sport/misc/tiny.css");
        Resources::addCSS("$sport/teams/narrow_large.css");
        Resources::addCSS('major_league/pages/player.css');
        Resources::addCSS("$sport/pages/player.css");
        Resources::addJS('skel/widgets/scrolltable.js');
        Resources::addJS('major_league/pages/player.js');
        HTML::setNavCurrent('/players');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.players.descrip-ind'), [
            'name' => $player->name,
        ]);
        HTML::setPageTitle(['Players', $player->name]);

        // Render the page.
        return response(view('sports.major_league.players.view', compact([
            'sport',
            'player',
            'subnav',
        ])));
    }

    /**
     * Display expanded info on a particular aspect of a single player
     * @param string  $name      The codified player name.
     * @param integer $player_id The database ID for the player being requested.
     * @param string  $tab       The information to be loaded.
     * @return Response|RedirectResponse The relevant content response
     */
    public function tab(string $name, int $player_id, string $tab): Response|RedirectResponse
    {
        // Validate the tab name.
        if (!FrameworkConfig::get("debear.setup.players.subnav.$tab") || $tab == 'home') {
            // Invalid tab passed.
            return HTTP::sendNotFound();
        }
        $config = FrameworkConfig::get("debear.setup.players.subnav.$tab");
        // Then the player passed.
        $player = $this->validateArguments($name, $player_id);
        if ($player instanceof Response) {
            return $player;
        }
        // Redirect to the equivalent tab rendered on the main page if we don't have the appropriate header.
        if (Request::header('X-DeBearTab', 'false') != 'true') {
            return HTTP::redirectPage($player->link . "#$tab", 307);
        }
        // Potential season arg quirk.
        $arg = Request::get('season');
        if (isset($arg) && preg_match('/^20[0-3]\d$/', $arg)) {
            $arg = "$arg:regular";
        }
        // Any specific season details?
        $season_arg = isset($arg) && preg_match('/^(20[0-3]\d|0000):(regular|playoff)$/', $arg) ? $arg : null;
        $season_conf = FrameworkConfig::get('debear.setup.season');
        FrameworkConfig::set(['debear.setup.season.type' => 'regular']); // Default season type.
        if (isset($season_arg)) {
            // Split into component parts.
            list($season_req, $season_type) = explode(':', $season_arg);
            if (
                ($season_conf['min'] <= $season_req && $season_req <= $season_conf['max'])
                || ($season_req === '0000' && FrameworkConfig::get("debear.setup.players.subnav.$tab.dropdowns.career"))
            ) {
                FrameworkConfig::set([
                    'debear.setup.season.viewing' => $season_req,
                    'debear.setup.season.type' => $season_type,
                ]);
            }
        }
        // Or a specific stat group?
        $sport = FrameworkConfig::get('debear.section.code');
        $group_req = Request::has('group') ? Request::get('group') : null;
        $config_key = ($sport != 'nfl' ? 'stats.details.player' : 'players.groups');
        if (
            isset($group_req)
            && FrameworkConfig::get("debear.setup.$config_key.$group_req")
            && ($player->group != $group_req)
        ) {
            $player->group = $group_req;
        }
        // Now load the additional secondary info for the player and render the info.
        $player->loadDetailedSecondary($tab);
        // Validate the information returned.
        $val = $config['validation'];
        if (!isset($player->$val) || !$player->lastRoster->isset()) {
            // No appropriate data found.
            return HTTP::sendNotFound();
        }
        // Render the tab.
        $data = compact(['sport', 'player']);
        $view_data = ["sports.$sport.players.view.$tab", "sports.major_league.players.view.$tab"];
        $view_none = ['sports.major_league.players.view.widgets.none'];
        return response(View::first($player->$val->isset() ? $view_data : $view_none, $data));
    }

    /**
     * Ensure the validity of the player arguments we have been passed
     * @param string  $name      The codified player name.
     * @param integer $player_id The database ID for the player being requested.
     * @return Response|Player A content response in case of errors, or the player info if valid
     */
    protected function validateArguments(string $name, int $player_id): Response|Player
    {
        $player = Namespaces::callStatic('Player', 'query')->where('player_id', $player_id)->get();
        if (!$player->isset() || $player->name_codified != $name) {
            // Invalid / Mismatching argument passed.
            return HTTP::sendNotFound();
        }
        // If we're here, we're all happy so return the player info.
        return $player;
    }

    /**
     * Management of legacy individual player links
     * @param mixed ...$args Legacy details.
     * @return Response|RedirectResponse A 404 response if the game could not be found, an appropriate redirect if so
     */
    public function legacy(mixed ...$args): Response|RedirectResponse
    {
        // Map arguments to details.
        $season = null;
        $num_args = count($args);
        if ($num_args == 1) {
            // Player ID only.
            list($player_id) = $args;
        } elseif (($num_args == 2) && (preg_match('/^[a-z]+$/', $args[1]))) {
            // Player ID and Detail. Season implied.
            $season = FrameworkConfig::get('debear.setup.season.viewing');
            list($player_id, $detail) = $args;
        } elseif ($num_args == 2) {
            // Season and Player ID.
            list($season, $player_id) = $args;
        } else {
            // Season, Player ID and Detail.
            list($season, $player_id, $detail) = $args;
        }
        // If this is valid, there will be a player we can find in the database, so pull it out.
        $player = Namespaces::callStatic('Player', 'query')
            ->where('player_id', $player_id)
            ->get();
        if (!$player->isset()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        } else {
            // Redirect to the modern convention (with a permanent hint).
            $url = join('/', array_filter([
                $player->link,
                $season ?? '',
                $detail ?? '',
            ]));
            return HTTP::redirectPage($url, 308);
        }
    }
}
