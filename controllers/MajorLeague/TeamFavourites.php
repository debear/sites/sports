<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Sports\MajorLeague\UserFavourite;

class TeamFavourites extends Controller
{
    /**
     * Record a link between the current user and a favourite team
     * @param string $team_name The codified city and franchise name.
     * @return Response The relevant content response
     */
    public function store(string $team_name): Response
    {
        // Validate the core request.
        $info = $this->validateAndLoad($team_name);
        if ($info instanceof Response) {
            return $info;
        }
        // If the existing record already exists, we cannot continue.
        if ($info['isset']) {
            return HTTP::sendBadRequest(['success' => false]);
        }

        // Add, store and return.
        $model = UserFavourite::create([
            'sport' => FrameworkConfig::get('debear.section.code'),
            'user_id' => User::object()->id,
            'type' => 'team',
            'char_id' => $info['team_id'],
            'int_id' => null,
        ]);
        return HTTP::sendCreated([
            'success' => true,
            'icon' => $model->icon(),
            'hover' => $model->hover(),
        ]);
    }

    /**
     * Remove the link between the current user and a favourite team
     * @param string $team_name The codified city and franchise name.
     * @return JsonResponse|Response The relevant content response
     */
    public function destroy(string $team_name): JsonResponse|Response
    {
        // Validate the core request.
        $info = $this->validateAndLoad($team_name);
        if ($info instanceof Response) {
            return $info;
        }
        // If an existing record does not already exist, we cannot continue.
        if (!$info['isset']) {
            return HTTP::sendBadRequest(['success' => false]);
        }

        // Remove and return (as a 200).
        UserFavourite::query()
            ->where([
                'sport' => FrameworkConfig::get('debear.section.code'),
                'user_id' => User::object()->id,
                'type' => 'team',
                'char_id' => $info['team_id'],
                'int_id' => null,
            ])->delete();
        $model = new UserFavourite(); // Empty object for processing.
        return response()->json([
            'success' => true,
            'icon' => $model->icon(),
            'hover' => $model->hover(),
        ]);
    }

    /**
     * Perform the request validation and return the request team's details
     * @param string $team_name The codified city and franchise name.
     * @return Response|array A content response in case of errors, or an array of details if valid
     */
    protected function validateAndLoad(string $team_name): Response|array
    {
        // Team name must be valid.
        $team_id = Namespaces::callStatic('Team', 'getIDFromCodifiedName', [$team_name]);
        if (!isset($team_id)) {
            return HTTP::sendNotFound(['success' => false]);
        }
        // Get the current favourite object status for this user and team.
        $existing = UserFavourite::query()
            ->where([
                ['sport', '=', FrameworkConfig::get('debear.section.code')],
                ['user_id', '=', User::object()->id],
                ['type', '=', 'team'],
                ['char_id', '=', $team_id],
            ])->get();
        return ['team_id' => $team_id, 'isset' => $existing->isset()];
    }
}
