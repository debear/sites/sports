<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;

class StartingLineups extends Controller
{
    /**
     * Display the players in the starting lineups for a given game date
     * @param string|null $date The definition of the game day (which will need parsing and validating).
     * @param string      $mode The render mode we will display the page in.
     * @return Response The relevant response, which could be a 404 if no valid argument(s) passed
     */
    public function index(?string $date = null, string $mode = 'full'): Response
    {
        ManageViews::setExtends('switcher', 'skeleton.layouts.raw');
        ManageViews::setSection('switcher', 'content');
        ManageViews::setMode($mode);

        // Convert the date argument into season and game date components.
        if (isset($date)) {
            // Load the passed date and validate.
            $game_date = Namespaces::callStatic('ScheduleDate', 'loadFromArgument', [$date]);
        } else {
            // Get the latest week.
            $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest');
        }
        FrameworkConfig::set([
            'debear.setup.season.viewing' => $game_date->season,
            'debear.links.lineups.url-extra' => $date,
        ]);

        // Get the full info.
        $schedule = Namespaces::callStatic('Schedule', 'loadByDate', [$game_date]);
        $data = Namespaces::callStatic('GameLineup', 'loadByGame', [$schedule]);
        $list_view = 'lineups';
        $sport_endpoint = FrameworkConfig::get('debear.section.endpoint');
        $sport = FrameworkConfig::get('debear.section.code');

        // Calendar setup.
        $game_date_range = Namespaces::callStatic('ScheduleDate', 'getDateRange', [$game_date->season]);
        $calendar = [
            'current' => $game_date->date->toDateString(),
            'min' => $game_date_range->date_min,
            'max' => $game_date_range->date_max,
            'json' => "/$sport_endpoint/schedule/{$game_date->season}/calendar",
        ];
        $link_method = 'linkLineups';

        // Custom page config.
        $title = 'Starting Lineups';
        HTML::setNavCurrent('/lineups');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.lineups.descrip-page'), [
            'date_fmt' => $game_date->date->format('l jS F'),
        ]);
        HTML::setPageTitle($title);
        Resources::addCSS("$sport/widgets/game_list.css");
        Resources::addCSS("$sport/pages/lineups.css");
        return response(view("sports.$sport.game_list", compact([
            'title',
            'list_view',
            'game_date',
            'calendar',
            'link_method',
            'schedule',
            'data',
        ])));
    }

    /**
     * Handle the season switcher season selection
     * @param integer $season The season to be switched to.
     * @return RedirectResponse The redirect to the appropriate game week
     */
    public function seasonSwitcher(int $season): RedirectResponse
    {
        $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest', [$season]);
        return HTTP::redirectPage($game_date->linkLineups() . '/switcher', 307);
    }
}
