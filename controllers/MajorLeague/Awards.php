<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\MajorLeague\Controller as ControllerHelper;
use DeBear\Helpers\Sports\Namespaces;

class Awards extends Controller
{
    /**
     * Display the main awards list and recent winners
     * @return Response The relevant content response
     */
    public function index(): Response
    {
        // Get the info.
        $awards = Namespaces::callStatic('Awards', 'loadOverview');
        $sport = FrameworkConfig::get('debear.section.code');

        // Custom page config.
        Resources::addCSS('major_league/pages/awards.css');
        Resources::addJS('major_league/pages/awards.js');
        HTML::setNavCurrent('/awards');
        HTML::linkMetaDescription('awards');
        HTML::setPageTitle(FrameworkConfig::get('debear.names.section') . ' Award Winners');

        // Render the page.
        return response(view('sports.major_league.awards', compact([
            'sport',
            'awards',
        ])));
    }

    /**
     * Return the historical award winners for an award
     * @param string  $award_name The codified name of the award.
     * @param integer $award_id   The ID of the award.
     * @return JsonResponse|Response The relevant content response or status code
     */
    public function show(string $award_name, int $award_id): JsonResponse|Response
    {
        // Load the award to validate the argument.
        $award = Namespaces::callStatic('Awards', 'loadHistory', [$award_id]);
        if (!$award->isset() || ($award->name_link != $award_name)) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        // Parse into history and return.
        $first_awarded = $award->winners->min('season');
        $history = [];
        for ($s = FrameworkConfig::get('debear.setup.season.viewing'); $s >= $first_awarded; $s--) {
            $winners = $award->winners->where('season', $s);
            // Format the season for display.
            $season = ControllerHelper::seasonTitle($s);
            // Loop through all the winners.
            $tmp = ['season' => $season, 'winner' => []];
            foreach ($winners as $winner) {
                // Build the name.
                if (isset($winner->player)) {
                    $player = $winner->player->name;
                } elseif (isset($winner->player_name)) {
                    $player = $winner->player_name;
                }
                // Build the team.
                $team_id = $winner->team_id;
                if (isset($winner->team) && $winner->team->isset()) {
                    $team = $winner->team->name;
                } else {
                    $team = $winner->team_name;
                }
                // Build the data row.
                $pos = $winner->pos;
                $tmp['winner'][] = compact(['player', 'team', 'team_id', 'pos']);
            }
            if (count($tmp['winner']) == 1) {
                $tmp['winner'] = $tmp['winner'][0];
            }
            $history[] = $tmp;
        }
        return response()->json($history);
    }
}
