<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Highcharts;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Controller as ControllerHelper;
use DeBear\Models\Skeleton\User;
use DeBear\ORM\Sports\MajorLeague\UserFavourite;
use DeBear\ORM\Sports\MajorLeague\Team;

class Teams extends Controller
{
    /**
     * Display the current team lists
     * @return Response The relevant content response
     */
    public function index(): Response
    {
        // Get the info.
        $viewing = FrameworkConfig::get('debear.setup.season.viewing');
        $groupings = Namespaces::callStatic('TeamGroupings', 'load', [$viewing]);
        $sport = FrameworkConfig::get('debear.section.code');

        // Custom page config.
        Resources::addCSS("$sport/misc/narrow_small.css");
        Resources::addCSS("$sport/teams/small.css");
        Resources::addCSS('major_league/pages/teams.css');
        HTML::setNavCurrent('/teams');
        HTML::linkMetaDescription('teams');
        HTML::setPageTitle('Teams');

        // Render the page.
        return response(view('sports.major_league.teams', compact([
            'groupings',
        ])));
    }

    /**
     * Display expanded info on a single team
     * @param string $team_name The codified city and franchise name.
     * @return Response|RedirectResponse The relevant content response
     */
    public function show(string $team_name): Response|RedirectResponse
    {
        // Validate the team name.
        $team = $this->validateTeam($team_name);
        if (!$team instanceof Team) {
            return $team;
        }
        // Now load the additional secondary info for the team.
        $team->loadDetailedSecondary('home');
        $sport = FrameworkConfig::get('debear.section.code');

        // User favourite team?
        $favourite = UserFavourite::query()
            ->where([
                ['sport', '=', FrameworkConfig::get('debear.section.code')],
                ['user_id', '=', User::object()->id],
                ['type', '=', 'team'],
                ['char_id', '=', $team->team_id],
            ])->get();

        // Setup the tabs.
        $tabs = FrameworkConfig::get('debear.setup.teams.subnav');
        uasort($tabs, function ($a, $b) {
            return $a['order'] <=> $b['order'];
        });
        $subnav = [
            'list' => array_map(function ($a) {
                return $a['label'];
            }, $tabs),
            'default' => 'home',
        ];

        // Determine the available seasons.
        $seasons = array_map(function ($s) {
            return [
                'id' => $s,
                'label' => ControllerHelper::seasonTitle($s),
            ];
        }, $team->seasonList());

        // Custom page config.
        Resources::addCSS("$sport/teams/narrow_large.css");
        Resources::addCSS("$sport/teams/narrow_small.css");
        Resources::addCSS("$sport/misc/narrow_small.css");
        Resources::addCSS("$sport/misc/tiny.css");
        Resources::addCSS('major_league/pages/team.css');
        if (in_array($sport, ['nfl', 'mlb'])) {
            Resources::addCSS("$sport/pages/team.css");
        }
        Resources::addJS('major_league/pages/team.js');
        if ($sport != 'nfl') {
            Resources::addJS("major_league/pages/team/schedule.js");
        }
        Highcharts::setupResources();
        HTML::setNavCurrent('/teams');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.teams.descrip-ind'), [
            'name' => $team->name,
        ]);
        HTML::setPageTitle(['Teams', $team->name]);

        // Render the page.
        return response(view('sports.major_league.teams.view', compact([
            'sport',
            'team',
            'favourite',
            'subnav',
            'seasons',
        ])));
    }

    /**
     * Display expanded info on a particular aspect of a single team
     * @param string  $team_name The codified city and franchise name.
     * @param string  $tab       The information to be loaded.
     * @param integer $season    An optional season to view override to the current season.
     * @return Response|RedirectResponse The relevant content response
     */
    public function tab(string $team_name, string $tab, ?int $season = null): Response|RedirectResponse
    {
        // Validate the tab name.
        if (!FrameworkConfig::get("debear.setup.teams.subnav.$tab") || $tab == 'home') {
            // Invalid tab passed.
            return HTTP::sendNotFound();
        }
        // Then the team name.
        $team = $this->validateTeam($team_name);
        if ($team instanceof Response) {
            return $team;
        }
        // Redirect to the equivalent tab rendered on the main page if we don't have the appropriate header.
        if (Request::header('X-DeBearTab', 'false') != 'true') {
            return HTTP::redirectPage($team->link . "#$tab", 307);
        }
        // Finally the season (if passed and applicable).
        if (isset($season) && FrameworkConfig::get("debear.setup.teams.subnav.$tab.dynamic")) {
            $season_valid = false;
            if ($season >= FrameworkConfig::get('debear.setup.season.min')) {
                $max = FrameworkConfig::get('debear.setup.season.max');
                foreach ($team->history->groupings as $g) {
                    if ($g->season_from <= $season && $season <= ($g->season_to ?? $max)) {
                        $season_valid = true;
                    }
                }
            }
            if (!$season_valid) {
                // Invalid season passed.
                return HTTP::sendNotFound();
            } else {
                // Switch to this season instead.
                FrameworkConfig::set(['debear.setup.season.viewing' => $season]);
                // Re-load the groupings for this new season.
                $team->loadSecondary(['groupings']);
            }
        }
        // Now load the additional secondary info for the team and render the info.
        $team->loadDetailedSecondary($tab);
        $sport = FrameworkConfig::get('debear.section.code');
        $resp = response(View::first(["sports.$sport.teams.view.$tab", "sports.major_league.teams.view.$tab"], compact([
            'sport',
            'team',
        ])));
        // Attach a Content-Type?
        if (FrameworkConfig::get("debear.setup.teams.subnav.$tab.json")) {
            $resp->header('Content-Type', 'application/json');
        }
        return $resp;
    }

    /**
     * Ensure the validity of the team name we have been passed
     * @param string $team_name The codified city and franchise name.
     * @return Response|RedirectResponse|Team A content response in case of errors, or the team info if valid
     */
    protected function validateTeam(string $team_name): Response|RedirectResponse|Team
    {
        // Get the appropriate team for this name.
        $team_id = Namespaces::callStatic('Team', 'getIDFromCodifiedName', [$team_name]);
        if (!isset($team_id)) {
            // Invalid team passed.
            return HTTP::sendNotFound();
        }
        $team = Namespaces::callStatic('Team', 'load', [[$team_id]]);
        if ($team->name_codified != $team_name) {
            // If a different name (i.e., franchise renamed / moved), then redirect to that page.
            return HTTP::redirectPage($team->link, 308);
        }
        // If we have an inactive team, adjust the season we are considering ourselves viewing.
        $team->loadSecondary(['history']); // This requires the history to be loaded first.
        if (!$team->division->isset()) {
            $seasons = $team->seasonList() ?: [FrameworkConfig::get('debear.setup.season.viewing')];
            FrameworkConfig::set(['debear.setup.season.viewing' => max($seasons)]);
            // Re-load the groupings for this new season.
            $team->loadSecondary(['groupings']);
        }
        // If we're here, we're all happy so return the team info.
        return $team;
    }

    /**
     * Management of legacy team stats with implied season
     * @param string $team_id The team ID we're viewing.
     * @param string $page    The team detail being viewed.
     * @return Response|RedirectResponse A 404 response if the team could not be found, an appropriate redirect if so
     */
    public function legacyTeamNoSeason(string $team_id, string $page = ''): Response|RedirectResponse
    {
        // Handle with the explicit season version.
        return $this->legacyTeamWithSeason(FrameworkConfig::get('debear.setup.season.viewing'), $team_id, $page);
    }

    /**
     * Management of legacy team stats with explicit season
     * @param integer $season  The requested season.
     * @param string  $team_id The team ID we're viewing.
     * @param string  $page    The team detail being viewed.
     * @return Response|RedirectResponse A 404 response if the team could not be found, an appropriate redirect if so
     */
    public function legacyTeamWithSeason(int $season, string $team_id, string $page = ''): Response|RedirectResponse
    {
        $team = Namespaces::callStatic('Team', 'query')
            ->where('team_id', $team_id)
            ->get();
        if (!$team->isset()) {
            // Invalid team passed.
            return HTTP::sendNotFound();
        }
        // Redirect to the team homepage.
        return HTTP::redirectPage($team->link, 308);
    }
}
