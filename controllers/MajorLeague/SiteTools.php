<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\MajorLeague\SiteTools as SiteToolsHelper;
use DeBear\Http\Controllers\Skeleton\Traits\SiteTools as SiteToolsTrait;

class SiteTools extends Controller
{
    use SiteToolsTrait;

    /**
     * Generate a sitemap for a particular part of the site
     * @return Response A response object with the appropriate sitemap details as markup
     */
    public function sitemap(): Response
    {
        // Our page configuration.
        Resources::addCSS(FrameworkConfig::get('debear.section.code') . '/pages/sitemap.css');
        HTML::setPageTitle('Sitemap');
        // Then manipulations.
        SiteToolsHelper::expandSitemap();
        // Then the standard processing from within the trait.
        return $this->sitemapWorker();
    }

    /**
     * Generate an XML version of the sitemap for a particular part of the site
     * @return Response A response object with the appropriate sitemap details as an XML sitemap
     */
    public function sitemapXml(): Response
    {
        // A wrapper blade template to link to the remaining sitemaps.
        return response()
            ->view('sports.major_league.admin.sitemap_index')
            ->header('Content-Type', 'text/xml');
    }

    /**
     * Generate an XML version of the sitemap for a particular subset of the subsite
     * @param string $section The subset of the subsite to return.
     * @return Response A response object with the appropriate sitemap details as an XML sitemap
     */
    public function sitemapXmlSection(string $section): Response
    {
        // Apply our manipulations to the default in-page config.
        $seasons = FrameworkConfig::get('debear.setup.season');
        if ($section == 'main') {
            // Main, "standard" sitemap links.
            SiteToolsHelper::expandSitemapXMLMain();
        } elseif ($section == 'players') {
            // The list of players.
            SiteToolsHelper::expandSitemapXMLPlayers();
        } elseif ($section >= $seasons['min'] && $section <= $seasons['max']) {
            // The games in a given season.
            SiteToolsHelper::expandSitemapXMLSeason($section);
        } else {
            // An unknown argument.
            return HTTP::sendNotFound();
        }
        // Then the standard processing from within the trait.
        return $this->sitemapXmlWorker();
    }
}
