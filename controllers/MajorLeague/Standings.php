<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Http\Response;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;
use DeBear\Helpers\Sports\MajorLeague\Controller as ControllerHelper;

class Standings extends Controller
{
    /**
     * The current, or historical (by season), Standings
     * @param integer|null $season The season to view, or false if nothing explicitly passed.
     * @param string       $mode   The render mode we will display the page in.
     * @return Response The relevant response, which could be a 404 if no valid argument(s) passed
     */
    public function index(?int $season = null, string $mode = 'full'): Response
    {
        ManageViews::setMode($mode);

        // Decipher / Validate the season.
        $config = FrameworkConfig::get('debear.setup.season');
        if (!isset($season)) {
            $season = $config['viewing'];
        } elseif (!ControllerHelper::validateSeason($season)) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        FrameworkConfig::set(['debear.setup.season.viewing' => $season]);

        // Load the standings and how they should be grouped.
        $standings = Namespaces::callStatic('Standings', 'load', [$season]);
        $groupings = Namespaces::callStatic('TeamGroupings', 'load', [$season]);
        // In some seasons, we may not want to render the conference standings.
        $render_conf = (FrameworkConfig::get("debear.setup.standings.hide_conf.{$season}") !== null);

        // Parse the columns.
        $key = Namespaces::callStatic('Standings', 'parseKey', [$season]);
        $columns = Namespaces::callStatic('Standings', 'parseColumns', [$season]);

        // Mobile subnav.
        $subnav = [
            'list' => [
                'div' => 'Division Standings',
                'conf' => 'Conference Standings',
            ],
            'default' => 'div',
            'css' => ($render_conf ? 'hidden-d' : 'hidden'),
        ];

        // Custom page config.
        $title = ControllerHelper::seasonTitle($season) . ' Standings';
        HTML::setNavCurrent('/standings');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.standings.descrip-page'), [
            'season' => ControllerHelper::seasonTitle($season),
        ]);
        HTML::setPageTitle($title);
        Resources::addCSS(FrameworkConfig::get('debear.section.code') . '/misc/narrow_small.css');
        Resources::addCSS('major_league/pages/standings.css');
        Resources::addCSS(FrameworkConfig::get('debear.section.code') . '/pages/standings.css');
        Resources::addJS('major_league/pages/standings.js');
        Resources::addJS('skel/widgets/toggle.js');

        // Render the current standings.
        return response(view('sports.major_league.standings', compact([
            'render_conf',
            'subnav',
            'title',
            'groupings',
            'standings',
            'key',
            'columns',
        ])));
    }
}
