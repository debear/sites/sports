<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\Sports\MajorLeague\Controller as ControllerHelper;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;
use DeBear\Repositories\InternalCache;

class Playoffs extends Controller
{
    /**
     * Display the playoff bracket for a given season
     * @param integer $season The season's bracket to be viewed.
     * @param string  $mode   The render mode we will display the page in.
     * @return Response The relevant response, which could be a redirect or content
     */
    public function index(?int $season = null, string $mode = 'full'): Response
    {
        ManageViews::setExtends('switcher', 'skeleton.layouts.raw');
        ManageViews::setSection('switcher', 'content');
        ManageViews::setMode($mode);
        $sport = FrameworkConfig::get('debear.section.code');

        // Validate (if passed) and set the season.
        if (!isset($season)) {
            // Default season.
            $season = FrameworkConfig::get('debear.setup.season.viewing');
        } elseif (!ControllerHelper::validateSeason($season)) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        } else {
            FrameworkConfig::set(['debear.setup.season.viewing' => $season]);
        }
        FrameworkConfig::set(['debear.links.playoffs.url-extra' => "$season"]);

        // Load the bracket details.
        $config = Namespaces::callStatic('PlayoffSeries', 'parseConfig', [$season]);
        InternalCache::object()->set('playoff-config', $config);
        $bracket = Namespaces::callStatic('PlayoffSeries', 'load', [$season]);
        if ($bracket->isset()) {
            $round_robin = $config['round_robin']
                ? Namespaces::callStatic('PlayoffRoundRobin', 'load', [$season])
                : null;
        }

        // Page title.
        $title = '<span class="hidden-m">' . ControllerHelper::seasonTitle($season) . '</span> '
            . FrameworkConfig::get('debear.setup.playoffs.title');
        $champ_title = Namespaces::callStatic('PlayoffSeries', 'champSeriesTitle', [$season]);

        // Custom page config.
        HTML::setNavCurrent('/playoffs');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.playoffs.descrip-page'), [
            'season' => ControllerHelper::seasonTitle($season),
        ]);
        HTML::setPageTitle(strip_tags($title));
        Resources::addCSS('_common/box_colours.css');
        Resources::addCSS("$sport/playoffs/large.css");
        if (isset($round_robin)) {
            Resources::addCSS("$sport/playoffs/medium.css");
        }
        Resources::addCSS("$sport/misc/narrow_small.css");
        Resources::addCSS('major_league/pages/playoffs.css');
        Resources::addCSS('skel/widgets/modals.css');
        Resources::addJS('major_league/pages/playoffs.js');
        Resources::addJS('skel/widgets/modals.js');

        // Do we need sport-specific bracket CSS?
        $per_sport_css = "$sport/pages/playoffs.css";
        if (file_exists(resource_path(FrameworkConfig::get('debear.dirs.stylesheets')) . "/sports/$per_sport_css")) {
            Resources::addCSS($per_sport_css);
        }

        // Render the appropriate template.
        $template = 'major_league.playoffs.preseason';
        $data = [
            'season' => $season,
            'title' => $title,
            'champ_title' => $champ_title,
        ];
        if ($bracket->isset()) {
            $template = ($config['template'] ?? 'major_league.playoffs.bracket');
            // Load the remaining bracket-related info.
            $seeds = Namespaces::callStatic('PlayoffSeeds', 'load', [$season]);
            $conferences = Namespaces::callStatic('Groupings', 'loadByID', [$seeds->unique('conf_id')]);
            $data = array_merge($data, [
                'config' => $config,
                'conferences' => $conferences,
                'seeds' => $seeds,
                'bracket' => $bracket,
                'round_robin' => $round_robin,
            ]);
        } elseif (isset($config['info']) && is_string($config['info']) && $config['info']) {
            $template = 'major_league.playoffs.info';
            $data['config'] = $config;
        }
        return response(view("sports.$template", $data));
    }

    /**
     * Show the series summary for a single playoff series
     * @param  integer $season The season the playoff series occurred in.
     * @param  string  $higher The franchise for the higher seeded team.
     * @param  string  $lower  The franchise for the lower seeded team.
     * @param  integer $code   The series code.
     * @return Response The relevant response, which could be a redirect or content
     */
    public function show(int $season, string $higher, string $lower, int $code): Response
    {
        $code = str_pad((string)$code, 2, '0', STR_PAD_LEFT);
        // Get and validate the series.
        $series = Namespaces::callStatic('PlayoffSeries', 'load', [$season, $code])
            ->loadSecondary(['schedule']);
        if (!$series->isset() || $series->linkRound() != "$higher-$lower-$code") {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }

        // Get additional information.
        $sport = FrameworkConfig::get('debear.section.code');
        $config = Namespaces::callStatic('PlayoffSeries', 'parseConfig', [$season]);
        InternalCache::object()->set('playoff-config', $config);
        $seeds = Namespaces::callStatic('PlayoffSeeds', 'load', [$season, [
            $series->higher_seed_ref,
            $series->lower_seed_ref,
        ]]);
        $playoff_title = ControllerHelper::seasonTitle($season) . ' '
            . FrameworkConfig::get('debear.setup.playoffs.title');

        HTML::setNavCurrent('/playoffs');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.playoffs.descrip-ind'), [
            'season' => ControllerHelper::seasonTitle($season),
            'higher' => $series->higher->name,
            'lower' => $series->lower->name,
        ]);
        HTML::setPageTitle([
            $playoff_title,
            $series->higher->name . ' vs ' . $series->lower->name,
        ]);
        Resources::addCSS("$sport/teams/small.css");
        Resources::addCSS('major_league/pages/playoffs_summary.css');
        Resources::addCSS("$sport/pages/playoffs_summary.css");
        Resources::addJS('major_league/pages/playoffs_summary.js');
        return response(view('sports.major_league.playoffs.summary', compact([
            'sport',
            'series',
            'seeds',
        ])));
    }

    /**
     * Show the round robin for a single round
     * @param integer $season  The season the round robin occurred in.
     * @param integer $conf_id The conference ID for the round robin to be viewed.
     * @return Response The relevant response, which could be a redirect or content
     */
    public function roundRobin(int $season, int $conf_id): Response
    {
        // Preliminary round part of the setup?
        $config = Namespaces::callStatic('PlayoffSeries', 'parseConfig', [$season]);
        if (!isset($config['round_robin']) || !$config['round_robin']) {
            return HTTP::sendNotFound();
        }
        // Valid conference ID (for this season)?
        $conf = Namespaces::callStatic('Groupings', 'loadByID', [[$conf_id]]);
        $schedule = Namespaces::callStatic('Schedule', 'loadRoundRobin', [$season, $conf_id]);
        if (!$schedule->isset()) {
            return HTTP::sendNotFound();
        }

        $sport = FrameworkConfig::get('debear.section.code');
        $playoff_title = ControllerHelper::seasonTitle($season) . ' '
            . FrameworkConfig::get('debear.setup.playoffs.title');

        HTML::setNavCurrent('/playoffs');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.playoffs.descrip-rr'), [
            'title' => $playoff_title,
            'conf' => $conf->name_full,
        ]);
        HTML::setPageTitle([$playoff_title, "{$conf->name_full} Round Robin"]);
        Resources::addCSS("$sport/misc/small.css");
        Resources::addCSS("$sport/teams/small.css");
        Resources::addCSS('major_league/pages/round_robin.css');
        // Shares the CSS with the playoff summary.
        Resources::addCSS('major_league/pages/playoffs_summary.css');
        Resources::addCSS("$sport/pages/playoffs_summary.css");
        return response(view('sports.major_league.playoffs.round_robin', compact([
            'sport',
            'conf',
            'schedule',
        ])));
    }

    /**
     * Management of legacy playoff summary links
     * @param integer $season The season the series took place in.
     * @param integer $code   The raw series code.
     * @return RedirectResponse|Response A 404 response if the game could not be found, an appropriate redirect if so
     */
    public function legacy(int $season, int $code): RedirectResponse|Response
    {
        // If this is valid, there will be a series we can find in the database, so pull it out.
        $series = Namespaces::callStatic('PlayoffSeries', 'load', [$season, $code]);
        if (!$series->isset()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        // Redirect to the modern convention (with a permanent hint).
        return HTTP::redirectPage($series->link, 308);
    }
}
