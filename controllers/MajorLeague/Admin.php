<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;

class Admin extends Controller
{
    /**
     * Logo summary
     * @return Response
     */
    public function logos(): Response
    {
        // Custom page config.
        HTML::setNavCurrent(FrameworkConfig::get('debear.links.header-logos.url'));
        HTML::setPageTitle(['Admin', 'Logos']);
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.header-logos.descrip'));
        // The team/misc CSS (method handles any already loaded by default).
        $sport = FrameworkConfig::get('debear.section.code');
        $sizes = [
            'tiny' => 'Tiny',
            'small' => 'Small',
            'medium' => 'Medium',
            'large' => 'Large',
            'narrow_small' => 'Narrow Small',
            'narrow_large' => 'Narrow Large',
        ];
        foreach (array_keys($sizes) as $size) {
            Resources::addCSS("$sport/teams/$size.css");
            Resources::addCSS("$sport/misc/$size.css");
        }
        $playoff_sizes = [
            'large',
            'medium',
            'small',
        ];
        foreach ($playoff_sizes as $size) {
            Resources::addCSS("$sport/playoffs/$size.css");
        }
        Resources::addCSS('major_league/pages/admin/logos.css');

        // Load the teams and groupings.
        $tbl_team = Namespaces::callStatic('Team', 'getTable');
        $tbl_group = Namespaces::callStatic('TeamGroupings', 'getTable');
        $teams = Namespaces::callStatic('Team', 'query')
            ->select("$tbl_team.*")
            ->selectRaw("MAX($tbl_group.season_to IS NULL) AS is_curr")
            ->join($tbl_group, "$tbl_group.team_id", "$tbl_team.team_id")
            ->where(
                DB::raw("IFNULL($tbl_group.season_to, 2099)"),
                '>=',
                FrameworkConfig::get('debear.setup.season.min')
            )->groupBy("$tbl_team.team_id")
            ->orderByDesc('is_curr')
            ->orderBy("$tbl_team.city")
            ->orderBy("$tbl_team.franchise")
            ->get();
        $groupings = Namespaces::callStatic('Groupings', 'query')
            ->whereNotNull('icon')
            ->orderBy('order')
            ->get();

        // Playoff equivalents.
        $playoff_sizes = [
            'large',
            'medium',
            'small',
        ];
        $playoffs = array_merge(range(
            FrameworkConfig::get('debear.setup.season.min'),
            FrameworkConfig::get('debear.setup.season.max')
        ), ['0000']);

        // Subnav of individual sections.
        $subnav = [
            'list' => $sizes,
            'default' => array_key_first($sizes),
        ];

        // Render the view.
        return response(view('sports.major_league.admin.logos', compact([
            'sport',
            'subnav',
            'sizes',
            'teams',
            'groupings',
            'playoff_sizes',
            'playoffs',
        ])));
    }
}
