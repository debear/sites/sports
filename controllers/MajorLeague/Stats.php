<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\MajorLeague\Controller as ControllerHelper;

class Stats extends Controller
{
    /**
     * Display the current stat leaders and links to the stat pages
     * @return Response The relevant content response
     */
    public function index(): Response
    {
        // Get the info.
        $max_num = 5;
        $leaders = [];
        $rows = 0;
        foreach (FrameworkConfig::get('debear.setup.stats.summary') as $c => $stat) {
            $class = FrameworkConfig::get("debear.setup.stats.details.{$stat['type']}.{$stat['class']}.class.sort");
            $leaders[$c] = $class::loadLeaders($stat['column'], $max_num);
            $rows += $leaders[$c]->count();
        }
        $is_preseason = !$rows;
        $sport = FrameworkConfig::get('debear.section.code');
        $viewing = FrameworkConfig::get('debear.setup.season.viewing');
        $season_title = ControllerHelper::seasonTitle($viewing);

        // If we loaded in the preseason, re-run for the previous season.
        if ($is_preseason) {
            FrameworkConfig::set(['debear.setup.season.viewing' => $viewing - 1]);
            foreach (FrameworkConfig::get('debear.setup.stats.summary') as $c => $stat) {
                $class = FrameworkConfig::get("debear.setup.stats.details.{$stat['type']}.{$stat['class']}.class.sort");
                $leaders[$c] = $class::loadLeaders($stat['column'], $max_num);
            }
            FrameworkConfig::set(['debear.setup.season.viewing' => $viewing]);
            $season_title = ControllerHelper::seasonTitle($viewing - 1);
        }

        // Common additional info.
        $all_teams = Namespaces::callStatic('TeamGroupings', 'loadTeams', [$viewing]);

        // Custom page config.
        Resources::addCSS('major_league/pages/stats.css');
        HTML::setNavCurrent('/stats');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.stats.descrip-page'), [
            'season' => $season_title,
        ]);
        HTML::setPageTitle(FrameworkConfig::get('debear.names.section') . ' Stat Leaders');

        // Render the page.
        return response(view("sports.major_league.stats", compact([
            'sport',
            'season_title',
            'is_preseason',
            'max_num',
            'leaders',
            'all_teams',
        ])));
    }

    /**
     * Display the requested stat tables
     * @param string $type  The entity type being loaded.
     * @param string $class The category class being loaded.
     * @return Response The relevant content response
     */
    public function show(string $type, string $class): Response
    {
        // Ensure what we've been passed is valid.
        $config = FrameworkConfig::get("debear.setup.stats.details.$type.$class");
        if (!isset($config['class'])) {
            // Not a valid combination, return a 404.
            return HTTP::sendNotFound();
        }
        $obj = $config['class']['sort'];
        $sport = FrameworkConfig::get('debear.section.code');

        /* Clean the input arguments. */
        $opt = [];
        // Season.
        $season = intval(Request::get('season'));
        $season_config = FrameworkConfig::get('debear.setup.season');
        if (!isset($season) || ($season < $season_config['min']) || ($season > $season_config['max'])) {
            $season = $season_config['viewing'];
        }
        // Season Type.
        $season_type = Request::get('type');
        if (!isset($season_type) || !in_array($season_type, ['regular', 'playoff'])) {
            $season_type = 'regular';
        }
        // Stat.
        $column = Request::get('stat');
        if (!isset($column) || !$obj::validateColumn($column)) {
            $column = $config['default'];
        }
        // Pagination (Players only).
        if ($type == 'player') {
            $opt['per_page'] = 30;
            $opt['page'] = Request::get('page');
            if (!isset($opt['page']) || !preg_match('/^\d+$/', $opt['page'])) {
                $opt['page'] = 1;
            }
        }
        // Team?
        $all_teams = null;
        if ($type == 'player') {
            $all_teams = Namespaces::callStatic('TeamGroupings', 'loadTeams', [$season]);
            $team_id = Request::get('team_id');
            if (isset($team_id) && $all_teams->where('team_id', $team_id)->isset()) {
                $opt['team_id'] = $team_id;
            }
        }

        // Get our metadata.
        $obj_inst = new $obj();
        $columns = $obj_inst->getSortableStatMeta();
        $qual_stats = array_flip($obj_inst->getStatQualified() ?? []);
        $seasons = $obj::getAvailableSeasons();
        // Second pass of the season data (to catch pre-season requests).
        $season = (!in_array($season, $seasons->unique('season')) ? $seasons->max('season') : $season);

        // Finally get the stat data.
        $stats = $obj::load($season, $season_type, $column, $opt);

        // Custom page config.
        Resources::addCSS('skel/widgets/scrolltable.css');
        Resources::addCSS('major_league/pages/stats_table.css');
        Resources::addCSS(FrameworkConfig::get('debear.section.code') . '/pages/stats_table.css');
        Resources::addJS('skel/widgets/scrolltable.js');
        Resources::addJS('major_league/pages/stats_table.js');
        HTML::setNavCurrent('/stats');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.stats.descrip-ind'), [
            'title' => $config['name'],
        ]);
        HTML::setPageTitle([
            'Stats',
            !isset($opt['team_id']) ? ucfirst($type) . 's' : $all_teams->where('team_id', $team_id)->franchise,
            $config['name'],
        ]);

        // Render the page.
        return response(view('sports.major_league.stats.table', compact([
            'sport',
            'config',
            'type',
            'class',
            'all_teams',
            'season',
            'season_type',
            'column',
            'opt',
            'columns',
            'stats',
            'seasons',
            'qual_stats',
        ])));
    }

    /**
     * Management of legacy stat links with implied season
     * @param string $type     The stat type we're viewing.
     * @param string $category The stat category being viewed.
     * @return RedirectResponse An appropriate redirect response to the new URL
     */
    public function legacyRedirectNoSeason(string $type, string $category = ''): RedirectResponse
    {
        // Handle with the explicit season version.
        return $this->legacyRedirectWithSeason(FrameworkConfig::get('debear.setup.season.viewing'), $type, $category);
    }

    /**
     * Management of legacy stat links with explicit season
     * @param integer $season   The requested season.
     * @param string  $type     The stat type we're viewing.
     * @param string  $category The stat category being viewed.
     * @return RedirectResponse An appropriate redirect response to the new URL
     */
    public function legacyRedirectWithSeason(int $season, string $type, string $category = ''): RedirectResponse
    {
        $sport = FrameworkConfig::get('debear.section.endpoint');
        $type = ($type == 'ind' ? 'players' : 'teams');
        $stat_groups = array_keys(FrameworkConfig::get('debear.setup.stats.details.' . rtrim($type, 's')));
        $group = ($category && in_array($category, $stat_groups) ? $category : $stat_groups[0]);
        return HTTP::redirectPage("/$sport/stats/players/$group?season=$season&type=regular", 308);
    }

    /**
     * Management of legacy team stats with implied season
     * @param string $team_id  The team ID we're viewing.
     * @param string $category The stat category being viewed.
     * @return Response|RedirectResponse A 404 response if the team could not be found, an appropriate redirect if so
     */
    public function legacyTeamNoSeason(string $team_id, string $category = ''): Response|RedirectResponse
    {
        // Handle with the explicit season version.
        return $this->legacyTeamWithSeason(FrameworkConfig::get('debear.setup.season.viewing'), $team_id, $category);
    }

    /**
     * Management of legacy team stats with explicit season
     * @param integer $season   The requested season.
     * @param string  $team_id  The team ID we're viewing.
     * @param string  $category The stat category being viewed.
     * @return Response|RedirectResponse A 404 response if the team could not be found, an appropriate redirect if so
     */
    public function legacyTeamWithSeason(
        int $season,
        string $team_id,
        string $category = ''
    ): Response|RedirectResponse {
        $team = Namespaces::callStatic('Team', 'query')
            ->where('team_id', $team_id)
            ->get();
        if (!$team->isset()) {
            // Invalid team passed.
            return HTTP::sendNotFound();
        }
        // Redirect to the appropriate stats page.
        $sport = FrameworkConfig::get('debear.section.endpoint');
        $stat_groups = array_keys(FrameworkConfig::get('debear.setup.stats.details.player'));
        $group = ($category && in_array($category, $stat_groups) ? $category : $stat_groups[0]);
        return HTTP::redirectPage("/$sport/stats/players/$group?team_id=$team_id&season=$season&type=regular", 308);
    }
}
