<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;

class PowerRanks extends Controller
{
    /**
     * The Power Ranks for a given, or the latest, week
     * @param integer $season The season to load, or null if nothing explicitly passed.
     * @param string  $week   The week we want to view, or the latest if not passed.
     * @return Response The relevant response, which could be a 404 if no valid argument(s) passed
     */
    public function index(?int $season = null, ?string $week = null): Response
    {
        // Load the power ranks for the week passed in.
        $ranks = Namespaces::callStatic('PowerRankingsWeeks', 'load', [$season, $week]);
        if (!$ranks->isset()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }

        // Determine the list of switchable weeks.
        $weeks = Namespaces::callStatic('PowerRankingsWeeks', 'getAvailableWeeks', [$ranks->season]);

        // Custom page config.
        $subtitle = [];
        if (isset($week)) {
            if (FrameworkConfig::get('debear.setup.season.viewing') != $ranks->season) {
                $subtitle[] = $ranks->season;
            }
            $subtitle[] = $ranks->name;
        }
        HTML::setNavCurrent('/power-ranks');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.power_ranks.descrip-page'), [
            'week' => join(' ', $subtitle),
        ]);
        HTML::setPageTitle(array_filter(array_merge(['Power Ranks'], $subtitle)));
        Resources::addCSS(FrameworkConfig::get('debear.section.code') . '/teams/narrow_large.css');
        Resources::addCSS(FrameworkConfig::get('debear.section.code') . '/teams/narrow_small.css');
        Resources::addCSS('major_league/pages/power_ranks.css');
        Resources::addJS('major_league/pages/power_ranks.js');

        // Render the ranks for this week.
        return response(view('sports.major_league.power_ranks', compact('ranks', 'weeks')));
    }
}
