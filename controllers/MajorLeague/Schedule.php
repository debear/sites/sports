<?php

namespace DeBear\Http\Controllers\Sports\MajorLeague;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Strings;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;

class Schedule extends Controller
{
    /**
     * Display the schedule for a given game date/week
     * @param string $date The definition of the game day (which will need parsing and validating).
     * @param string $mode The render mode we will display the page in.
     * @return Response The relevant response, which could be a 404 if no valid argument(s) passed
     */
    public function index(?string $date = null, string $mode = 'full'): Response
    {
        ManageViews::setExtends('switcher', 'skeleton.layouts.raw');
        ManageViews::setSection('switcher', 'content');
        ManageViews::setMode($mode);

        // PHPMD doesn't entirely like the way compact() works, so we'll have to pass data to the view manually.
        $view_data = [];

        // Convert the date argument into season and game date components.
        if (isset($date)) {
            // Load the passed date and validate.
            $game_date = Namespaces::callStatic('ScheduleDate', 'loadFromArgument', [$date]);
        } else {
            // Get the latest week.
            $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest');
        }
        FrameworkConfig::set([
            'debear.setup.season.viewing' => $game_date->season,
            'debear.links.schedule.url-extra' => $date,
        ]);
        $sport = FrameworkConfig::get('debear.section.code');

        // Get the full info.
        $schedule = Namespaces::callStatic('Schedule', 'loadByDate', [$game_date, ['expanded' => true]]);
        if ($sport == 'nfl') {
            $view = 'weekly';
            // Extra weekly-scheduled  data required.
            $game_date_list = Namespaces::callStatic('ScheduleDate', 'loadAll');
            $bye_weeks = Namespaces::callStatic('ScheduleByeWeek', 'loadByWeek', [$game_date]);
            // Subnav for responsive week selection.
            $subnav = [
                'list' => [],
                'default' => $game_date->linkDate(),
                'css' => 'hidden-d',
                'breakpoint' => 'tablet-landscape',
                'custom-handler' => true,
            ];
            $po = FrameworkConfig::get('debear.setup.playoffs.rounds');
            $po_keys = array_keys($po);
            foreach ($game_date_list as $game_week) {
                if ($game_week->game_type == 'regular') {
                    $name = "Week {$game_week->week}";
                } else {
                    $name = $po[$po_keys[$game_week->week - 1]]['full'];
                }
                $subnav['list'][$game_week->linkDate()] = $name;
            }
            $view_data['subnav'] = $subnav;
            $view_data['game_date_list'] = $game_date_list;
            $view_data['bye_weeks'] = $bye_weeks;
            $extra_css = ['nfl/widgets/game_weeks.css', 'major_league/pages/schedule/weekly.css'];
            $extra_js = ['nfl/widgets/game_weeks.js'];
        } else {
            $view = 'daily';
            // Calendar setup.
            $sport_endpoint = FrameworkConfig::get('debear.section.endpoint');
            $game_date_range = Namespaces::callStatic('ScheduleDate', 'getDateRange', [$game_date->season]);
            $calendar = [
                'current' => $game_date->date->toDateString(),
                'min' => $game_date_range->date_min,
                'max' => $game_date_range->date_max,
                'json' => "/$sport_endpoint/schedule/{$game_date->season}/calendar",
            ];
            $link_method = 'linkFull';
            $view_data['calendar'] = $calendar;
            $view_data['link_method'] = $link_method;
            $extra_css = [];
            $extra_js = [];
        }

        // Custom page config.
        HTML::setNavCurrent('/schedule');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.schedule.descrip-page'), [
            'date' => $game_date->name_full,
        ]);
        HTML::setPageTitle('Scores and Schedule');
        Resources::addCSS("$sport/teams/narrow_small.css");
        Resources::addCSS('major_league/pages/schedule.css');
        Resources::addCSS('major_league/widgets/game.css');
        Resources::addCSS("$sport/widgets/game.css");
        foreach ($extra_css as $css) {
            Resources::addCSS($css);
        }
        foreach ($extra_js as $js) {
            Resources::addJS($js);
        }
        return response(view("sports.major_league.schedule.$view", array_merge($view_data, [
            'sport' => $sport,
            'game_date' => $game_date,
            'schedule' => $schedule,
        ])));
    }

    /**
     * Display the details of an individual game
     * @param string  $date      The definition of the game day.
     * @param string  $visitor   The codified visiting team.
     * @param string  $home      The codified home team.
     * @param string  $game_type A single character referencing the game type.
     * @param integer $game_id   The numeric ID of the game.
     * @return Response The relevant response, which could be an error or content
     */
    public function show(string $date, string $visitor, string $home, string $game_type, int $game_id): Response
    {
        // Validate the date to get the season.
        $game_date = Namespaces::callStatic('ScheduleDate', 'loadFromArgument', [$date]);
        if (!$game_date->isset()) {
            return HTTP::sendNotFound();
        }
        FrameworkConfig::set(['debear.setup.season.viewing' => $game_date->season]);
        $game_types = ['r' => 'regular', 'p' => 'playoff'];
        $game = Namespaces::callStatic('Schedule', 'load', [$game_date->season, $game_types[$game_type], $game_id]);
        if (
            !$game->isset()
            || ($game->linkDate() != $date)
            || (Strings::codify($game->visitor->franchise) != $visitor)
            || (Strings::codify($game->home->franchise) != $home)
        ) {
            return HTTP::sendNotFound();
        }
        // Having validated, load the full info.
        FrameworkConfig::set(['debear.links.schedule.url-extra' => $date]);
        $game = $game->loadDetailedSecondaries();
        $sport = FrameworkConfig::get('debear.section.code');

        // Game Breakdown.
        $list = [];
        foreach (FrameworkConfig::get('debear.setup.game.tabs') as $tab) {
            $label = (is_string($tab['label']) ? $tab['label'] : $tab['label'][$game->game_type]);
            $list[$tab['link'] ?: $tab['icon']] = "<span class=\"icon_game_{$tab['icon']}\">$label</span>";
        }
        $subnav = [
            'list' => $list,
            'default' => 'boxscore', // Common initial.
        ];

        // Custom page config.
        HTML::setNavCurrent('/schedule');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.schedule.descrip-ind'), [
            'date' => $game_date->name_full,
            'visitor' => $game->visitor->name,
            'home' => $game->home->name,
        ]);
        HTML::setPageTitle([
            'Scores and Schedule',
            $game_date->name_short,
            "{$game->visitor->franchise} at {$game->home->franchise}",
        ]);
        Resources::addCSS("$sport/teams/small.css");
        Resources::addCSS("$sport/teams/medium.css");
        if ($sport == 'nfl') {
            Resources::addCSS("$sport/" . ($game->isSuperBowl() ? 'misc' : 'teams') . '/large.css');
        }
        Resources::addCSS("major_league/widgets/game.css");
        Resources::addCSS("$sport/widgets/game.css");
        Resources::addCSS("major_league/pages/game.css");
        Resources::addCSS("$sport/pages/game.css");
        if (in_array($sport, ['mlb', 'nhl'])) {
            Resources::addJS("$sport/pages/game.js");
        }
        // Build the response.
        $response = response(view('sports.major_league.game', [
            'sport' => $sport,
            'subnav' => $subnav,
            'game_type' => $game_type,
            'game' => $game,
        ]));
        // Finally, mass update the player links in one hit.
        $game->applyPlayerLinks($response);
        return $response;
    }

    /**
     * Handle the season switcher season selection
     * @param integer $season The season to be switched to.
     * @return RedirectResponse The redirect to the appropriate game week
     */
    public function seasonSwitcher(int $season): RedirectResponse
    {
        $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest', [$season]);
        return HTTP::redirectPage($game_date->linkFull() . '/switcher', 307);
    }

    /**
     * Load the exceptions to the calendar date widget
     * @param integer $season The season to be calculated.
     * @return JsonResponse The response as a JSON object
     */
    public function calendarExceptions(int $season): JsonResponse
    {
        $ret = Namespaces::callStatic('ScheduleDate', 'getExceptions', [$season]);
        return response()->json($ret);
    }

    /**
     * Management of legacy schedule links
     * @param integer $season The season the games took place in.
     * @return Response|RedirectResponse A 404 response if the game could not be found, an appropriate redirect if so
     */
    public function legacyIndex(int $season): Response|RedirectResponse
    {
        // If this is valid, there will be a game we can find in the database, so pull it out.
        $last_game = Namespaces::callStatic('Schedule', 'query')
            ->where('season', $season)
            ->whereNotNull('status')
            ->whereNotIn('status', ['PPD', 'CNC'])
            ->orderByDesc('game_date')
            ->limit(1)
            ->get();
        if (!$last_game->isset()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        // Redirect to the modern convention (with a permanent hint).
        $url = join('/', [
            FrameworkConfig::get('debear.section.code'),
            'schedule',
            $last_game->linkDate(),
        ]);
        return HTTP::redirectPage($url, 308);
    }

    /**
     * Management of legacy individual game links
     * @param integer $season    The season the game took place in.
     * @param string  $game_type Whether the game was in the regular season ('r') or playoffs ('p').
     * @param integer $game_id   The raw Game ID.
     * @param string  $detail    Detail page to be viewed (Optional).
     * @return Response|RedirectResponse A 404 response if the game could not be found, an appropriate redirect if so
     */
    public function legacyShow(
        int $season,
        string $game_type,
        int $game_id,
        ?string $detail = null
    ): Response|RedirectResponse {
        // If this is valid, there will be a game we can find in the database, so pull it out.
        $game = Namespaces::callStatic('Schedule', 'query')
            ->where([
                'season' => $season,
                'game_type' => ($game_type == 'r' ? 'regular' : 'playoff'),
                'game_id' => $game_id,
            ])->whereNotNull('status')
            ->whereNotIn('status', ['PPD', 'CNC'])
            ->get();
        if (!$game->isset()) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        // Redirect to the modern convention (with a permanent hint).
        return HTTP::redirectPage($game->link . (isset($detail) ? "/$detail" : ''), 308);
    }
}
