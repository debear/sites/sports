<?php

namespace DeBear\Http\Controllers\Sports\NFL;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;

class Inactives extends Controller
{
    /**
     * Display the inactive players for a given game week
     * @param string $date The definition of the game week (which will need parsing and validating).
     * @param string $mode The render mode we will display the page in.
     * @return Response The relevant response, which could be a 404 if no valid argument(s) passed
     */
    public function index(?string $date = null, string $mode = 'full'): Response
    {
        ManageViews::setExtends('switcher', 'skeleton.layouts.raw');
        ManageViews::setSection('switcher', 'content');
        ManageViews::setMode($mode);

        // Convert the date argument into season and game date components.
        if (isset($date)) {
            // Load the passed date and validate.
            $game_date = Namespaces::callStatic('ScheduleDate', 'loadFromArgument', [$date]);
        } else {
            // Get the latest week.
            $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest');
        }
        FrameworkConfig::set([
            'debear.setup.season.viewing' => $game_date->season,
            'debear.links.inactives.url-extra' => $date,
        ]);

        // Get the full info.
        $schedule = Namespaces::callStatic('Schedule', 'loadByDate', [$game_date]);
        $data = Namespaces::callStatic('GameInactive', 'loadByDate', [$game_date]);
        $game_date_list = Namespaces::callStatic('ScheduleDate', 'loadAll');
        $list_view = 'inactives';
        $grid = 'grid-1-3';

        // Subnav for responsive game lists.
        $subnav = [
            'list' => [],
            'default' => $game_date->linkDate(),
            'css' => 'hidden-d',
            'breakpoint' => 'tablet-landscape',
            'custom-handler' => true,
        ];
        $po = FrameworkConfig::get('debear.setup.playoffs.rounds');
        $po_keys = array_keys($po);
        foreach ($game_date_list as $game_week) {
            if ($game_week->game_type == 'regular') {
                $name = "Week {$game_week->week}";
            } else {
                $name = $po[$po_keys[$game_week->week - 1]]['full'];
            }
            $subnav['list'][$game_week->linkDate()] = $name;
        }
        $link_method = 'linkInactives';

        // Custom page config.
        $title = 'Game Day Inactives';
        HTML::setNavCurrent('/inactives');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.inactives.descrip-page'), [
            'week' => ($game_date->game_type == 'playoff' ? 'the ' : '') . $game_date->name_short,
        ]);
        HTML::setPageTitle('Inactives');
        Resources::addCSS('nfl/widgets/game_weeks.css');
        Resources::addCSS('nfl/widgets/game_list.css');
        Resources::addCSS('nfl/pages/inactives.css');
        Resources::addJS('nfl/widgets/game_weeks.js');
        return response(view('sports.nfl.game_list', compact([
            'title',
            'list_view',
            'grid',
            'game_date_list',
            'game_date',
            'schedule',
            'link_method',
            'data',
            'subnav',
        ])));
    }

    /**
     * Handle the season switcher season selection
     * @param integer $season The season to be switched to.
     * @return RedirectResponse The redirect to the appropriate game week
     */
    public function seasonSwitcher(int $season): RedirectResponse
    {
        $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest', [$season]);
        return HTTP::redirectPage($game_date->linkInactives() . '/switcher', 307);
    }
}
