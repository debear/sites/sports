<?php

namespace DeBear\Http\Controllers\Sports;

use DeBear\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Models\Skeleton\News as NewsModel;

class MajorLeague extends Controller
{
    /**
     * Major League-specific summary
     * @return View
     */
    public function index(): View
    {
        $sport = FrameworkConfig::get('debear.section.code');
        $season = FrameworkConfig::get('debear.setup.season.viewing');
        /* Load the data we need to populate the page. */
        // Standings / Playoff Bracket.
        $standings = $bracket = null;
        if (Namespaces::callStatic('Schedule', 'isRegularSeasonComplete', [$season])) {
            $standings = Namespaces::callStatic('Standings', 'load', [$season]);
        } else {
            $bracket = Namespaces::callStatic('PlayoffSeries', 'load', [$season]);
        }
        $groupings = Namespaces::callStatic('TeamGroupings', 'load', [$season]);
        // Stat Leaders.
        $leaders = [];
        foreach (FrameworkConfig::get('debear.setup.stats.summary') as $c => $stat) {
            if ($stat['type'] == 'player') {
                $class = FrameworkConfig::get("debear.setup.stats.details.{$stat['type']}.{$stat['class']}.class.sort");
                $leader = $class::loadLeaders($stat['column'], 1); // Leader only.
                if ($leader->count()) {
                    $leaders[$c] = $leader;
                }
            }
        }
        // Power Ranks.
        $power_ranks = Namespaces::callStatic('PowerRankingsWeeks', 'load', [$season]);
        // News.
        $news = NewsModel::with('source')
            ->where('app', '=', FrameworkConfig::get('debear.news.app'))
            ->orderBy('published', 'desc') // First order by published date.
            ->orderBy('news_id', 'desc')   // And then in the case of contention, ID, for consistency.
            ->limit(10)
            ->get();
        // Customise the CSP rules.
        NewsModel::updateCSP(FrameworkConfig::get('debear.news.app'));

        // Custom page config.
        HTML::noHeaderLink();
        HTML::setNavCurrent('/');
        HTML::linkMetaDescription('home');
        Resources::addCSS("$sport/misc/narrow_small.css");
        Resources::addCSS("$sport/teams/narrow_small.css");
        Resources::addCSS('major_league/pages/home.css');
        Resources::addJS('major_league/pages/home.js');

        // Render the view.
        return view('sports.major_league.home', compact([
            'sport',
            'standings',
            'bracket',
            'groupings',
            'leaders',
            'power_ranks',
            'news',
        ]));
    }
}
