<?php

namespace DeBear\Http\Controllers\Sports\MLB;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Common\ManageViews;
use DeBear\ORM\Sports\MajorLeague\MLB\Draft;
use DeBear\ORM\Sports\MajorLeague\MLB\DraftRound;
use DeBear\ORM\Sports\MajorLeague\MLB\DraftSeason;
use DeBear\ORM\Sports\MajorLeague\MLB\DraftType;

class Drafts extends Controller
{
    /**
     * Display the player draft results
     * @param integer $season The season's bracket to be viewed.
     * @param string  $mode   The render mode we will display the page in.
     * @return Response|RedirectResponse The relevant response, which could be a redirect or content
     */
    public function index(?int $season = null, string $mode = 'full'): Response|RedirectResponse
    {
        ManageViews::setExtends('switcher', 'skeleton.layouts.raw');
        ManageViews::setSection('switcher', 'content');
        ManageViews::setMode($mode);
        $sport = FrameworkConfig::get('debear.section.code');

        // Get the list of available seasons.
        $draft_seasons = Draft::getAllSeasons();

        // Determine our range and processing method.
        FrameworkConfig::set(['debear.setup.switcher' => [
            'min' => min($draft_seasons),
            'max' => max($draft_seasons),
            'format' => false,
        ]]);

        // Validate (if passed) and set the season.
        if (!isset($season)) {
            // Default season.
            $season = max($draft_seasons);
        } elseif (($season < min($draft_seasons)) || ($season > max($draft_seasons))) {
            // Invalid argument passed.
            return HTTP::sendNotFound();
        }
        FrameworkConfig::set(['debear.setup.season.viewing' => $season]);

        // Get the info.
        $draft = Draft::load($season);
        $draft_rounds = DraftRound::all();
        $draft_title = "{$season} MLB Draft";

        // How are we building the subnav?
        $season_drafts = DraftSeason::query()->where('season', $season)->get();
        $subnav = [
            'list' => [],
            'default' => false,
        ];
        foreach (DraftType::all() as $draft_type) {
            if ($season_drafts->{$draft_type->type}) {
                $subnav['list'][$draft_type->type] = $draft_type->name;
                if (!$subnav['default']) {
                    $subnav['default'] = $draft_type->type;
                }
            }
        }

        // Custom page config.
        Resources::addCSS('mlb/pages/drafts.css');
        Resources::addJS('mlb/pages/drafts.js');
        HTML::setNavCurrent('/draft');
        HTML::linkMetaDescription('draft');
        HTML::setPageTitle($draft_title);

        // Render the page.
        return response(view('sports.mlb.draft', compact([
            'sport',
            'season',
            'subnav',
            'draft_title',
            'draft_rounds',
            'draft',
        ])));
    }
}
