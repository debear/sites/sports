<?php

namespace DeBear\Http\Controllers\Sports\MLB;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Namespaces;
use DeBear\Helpers\Sports\Common\ManageViews;

class Probables extends Controller
{
    /**
     * Display the probable pitchers for a given game date
     * @param string $date The definition of the game day (which will need parsing and validating).
     * @param string $mode The render mode we will display the page in.
     * @return Response The relevant response, which could be a 404 if no valid argument(s) passed
     */
    public function index(?string $date = null, string $mode = 'full'): Response
    {
        ManageViews::setExtends('switcher', 'skeleton.layouts.raw');
        ManageViews::setSection('switcher', 'content');
        ManageViews::setMode($mode);

        // Convert the date argument into season and game date components.
        if (isset($date)) {
            // Load the passed date and validate.
            $game_date = Namespaces::callStatic('ScheduleDate', 'loadFromArgument', [$date]);
        } else {
            // Get the latest week.
            $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest');
        }
        FrameworkConfig::set([
            'debear.setup.season.viewing' => $game_date->season,
            'debear.links.probables.url-extra' => $date,
        ]);

        // Get the full info.
        $schedule = Namespaces::callStatic('Schedule', 'loadByDate', [$game_date]);
        $data = Namespaces::callStatic('GameProbable', 'loadByGame', [$schedule]);
        $list_view = 'probables';

        // Calendar setup.
        $game_date_range = Namespaces::callStatic('ScheduleDate', 'getDateRange', [$game_date->season]);
        $calendar = [
            'current' => $game_date->date->toDateString(),
            'min' => $game_date_range->date_min,
            'max' => $game_date_range->date_max,
            'json' => "/mlb/schedule/{$game_date->season}/calendar",
        ];
        $link_method = 'linkProbables';

        // Custom page config.
        $title = 'Probable Pitchers';
        HTML::setNavCurrent('/probables');
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.probables.descrip-page'), [
            'date_fmt' => $game_date->date->format('l jS F'),
        ]);
        HTML::setPageTitle($title);
        Resources::addCSS('mlb/widgets/game_list.css');
        Resources::addCSS('mlb/pages/probables.css');
        return response(view('sports.mlb.game_list', compact([
            'title',
            'list_view',
            'game_date',
            'calendar',
            'link_method',
            'schedule',
            'data',
        ])));
    }

    /**
     * Handle the season switcher season selection
     * @param integer $season The season to be switched to.
     * @return RedirectResponse The redirect to the appropriate game week
     */
    public function seasonSwitcher(int $season): RedirectResponse
    {
        $game_date = Namespaces::callStatic('ScheduleDate', 'getLatest', [$season]);
        return HTTP::redirectPage($game_date->linkProbables() . '/switcher', 307);
    }
}
