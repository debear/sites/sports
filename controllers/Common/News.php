<?php

namespace DeBear\Http\Controllers\Sports\Common;

use DeBear\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\Request;
use DeBear\Helpers\HTML;
use DeBear\Helpers\HTTP;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Controller as ControllerHelper;
use DeBear\Models\Skeleton\News as NewsModel;

class News extends Controller
{
    /**
     * Latest news and articles
     * @return Response
     */
    public function index(): Response
    {
        // Custom page config.
        ControllerHelper::prepareRequest();
        HTML::setNavCurrent('/news');
        HTML::linkMetaDescription('news');
        HTML::setPageTitle('News');
        Resources::addCSS('skel/widgets/scroller.css');
        Resources::addCSS('_global/pages/news.css');
        Resources::addJS('skel/widgets/scroller.js');
        Resources::addJS('_global/pages/news.js');

        // PHPMD doesn't entirely like the way compact() works, so we'll have to pass data to the view manually.
        $view_data = [];

        // Handle pagination counts.
        $per_page = 10;
        $per_page_mult = 1;
        if (!Request::has('page') || !Request::query('page')) {
            // First page, so include the page chrome.
            $view = 'view';
            $page = 1;
            $per_page_mult = 2; // Double the amount on the front-page.
            $offset = 0;
        } else {
            // Paginated (ajax-y) results.
            $view = 'view.table';
            $page = (int)Request::query('page');
            $offset = $page * $per_page; // Although zero-indexed, first page has double the quantity.
        }

        // Validate the page argument.
        $app = FrameworkConfig::get('debear.news.app');
        $total_articles = NewsModel::where('app', '=', $app)->count();
        if ($offset >= $total_articles) {
            // Page number exceeds total articles.
            return HTTP::sendNotFound();
        }

        // Get the article details.
        $num_articles = $total_articles - $per_page; // Remove the scrollables on Page 1.
        $articles = NewsModel::with('source')
            ->where('app', '=', $app)
            ->orderBy('published', 'desc')  // First order by published date.
            ->orderBy('news_id', 'desc')    // And then in the case of contention, ID, for consistency.
            ->limit($per_page * $per_page_mult)
            ->offset($offset)
            ->get();

        // Splitting the articles?
        if ($page == 1) {
            $view_data['block'] = $articles->slice(0, 6); // Display first 6 items in the header block.
            $view_data['list'] = $articles->slice(6);
        }

        // Customise the CSP rules.
        NewsModel::updateCSP($app);

        // Render.
        return response(view("sports._global.news.$view", array_merge($view_data, [
            'articles' => $articles,
            // Pagination details.
            'page' => $page,
            'per_page' => $per_page,
            'num_articles' => $num_articles,
        ])));
    }
}
