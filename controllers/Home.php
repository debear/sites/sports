<?php

namespace DeBear\Http\Controllers\Sports;

use Illuminate\Contracts\View\View;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Helpers\Sports\Homepage as HomepageHelper;

class Home extends Controller
{
    /**
     * General, sport-agnostic, summary
     * @return View
     */
    public function index(): View
    {
        // Update the navigation to our sports (inline, so as not to affect sitemaps).
        HomepageHelper::setSubsiteNav();
        // Get our news articles.
        $articles = HomepageHelper::getLatestNews();
        // Get our list of appropriate events.
        $events = HomepageHelper::getLatestEvents();

        // Custom page config.
        HTML::noHeaderLink();
        HTML::setNavCurrent('/');
        HTML::linkMetaDescription('home');
        Resources::addCSS('_global/logos_icon.css');
        Resources::addCSS('_global/pages/home.css');

        return view('sports._global.home', compact('articles', 'events'));
    }
}
