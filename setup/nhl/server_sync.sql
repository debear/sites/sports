# Season to sync...
SET @season := 2024;

#
# General NHL update
#
SET @sync_app := 'sports_nhl';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 39, '__DIR__ SPORTS_NHL_GAME_EVENT_BOXSCORE',                     1, 'database'),
                        (@sync_app, 40, '__DIR__ SPORTS_NHL_GAME_EVENT_COORDS',                       2, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_NHL_GAME_LINEUP',                             3, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_NHL_GAME_OFFICIALS',                          4, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_NHL_GAME_PP_STATS',                           5, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_NHL_GAME_SCRATCHES',                          6, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_NHL_GAME_STRENGTHS',                          7, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_NHL_GAME_THREE_STARS',                        8, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_NHL_OFFICIALS',                               9, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_NHL_PLAYERS_GAME_GOALIES',                   10, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_NHL_PLAYERS_GAME_SKATERS',                   11, 'database'),
                        (@sync_app, 37, '__DIR__ SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED',          12, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_GOALIES',                 13, 'database'),
                        (@sync_app, 31, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_GOALIES_HEATMAP',         14, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_GOALIES_SORTED',          15, 'database'),
                        (@sync_app, 13, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_SKATERS',                 16, 'database'),
                        (@sync_app, 32, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_SKATERS_HEATMAP',         17, 'database'),
                        (@sync_app, 14, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_SKATERS_SORTED',          18, 'database'),
                        (@sync_app, 33, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED',        19, 'database'),
                        (@sync_app, 34, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED_SORTED', 20, 'database'),
                        (@sync_app, 15, '__DIR__ SPORTS_NHL_STANDINGS',                              21, 'database'),
                        (@sync_app, 16, '__DIR__ SPORTS_NHL_TEAMS_GAME_GOALIES',                     22, 'database'),
                        (@sync_app, 17, '__DIR__ SPORTS_NHL_TEAMS_GAME_SKATERS',                     23, 'database'),
                        (@sync_app, 38, '__DIR__ SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED',            24, 'database'),
                        (@sync_app, 18, '__DIR__ SPORTS_NHL_TEAMS_ROSTERS',                          25, 'database'),
                        (@sync_app, 19, '__DIR__ SPORTS_NHL_TEAMS_SEASON_GOALIES',                   26, 'database'),
                        (@sync_app, 20, '__DIR__ SPORTS_NHL_TEAMS_SEASON_GOALIES_SORTED',            27, 'database'),
                        (@sync_app, 21, '__DIR__ SPORTS_NHL_TEAMS_SEASON_SKATERS',                   28, 'database'),
                        (@sync_app, 22, '__DIR__ SPORTS_NHL_TEAMS_SEASON_SKATERS_SORTED',            29, 'database'),
                        (@sync_app, 35, '__DIR__ SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED_SORTED',   30, 'database'),
                        (@sync_app, 36, '__DIR__ SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED_SORTED',   31, 'database'),
                        (@sync_app, 23, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_PLAYOFF',                  32, 'database'),
                        (@sync_app, 24, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_PLAYOFF_SUMMARY',          33, 'database'),
                        (@sync_app, 25, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_REGULAR',                  34, 'database'),
                        (@sync_app, 26, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_TITLES',                   35, 'database'),
                        (@sync_app, 27, '__DIR__ Players',                                           36, 'script'),
                        (@sync_app, 28, '__DIR__ Power Ranks',                                       37, 'script'),
                        (@sync_app, 29, '__DIR__ Schedule',                                          38, 'script'),
                        (@sync_app, 30, 'Synchronise News',                                          39, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES # ID 1 moved to "full"
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_NHL_GAME_LINEUP',                            CONCAT('season = ''', @season, '''')),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_NHL_GAME_OFFICIALS',                         CONCAT('season = ''', @season, '''')),
                           (@sync_app,  4, 'debearco_sports', 'SPORTS_NHL_GAME_PP_STATS',                          CONCAT('season = ''', @season, '''')),
                           (@sync_app,  5, 'debearco_sports', 'SPORTS_NHL_GAME_SCRATCHES',                         CONCAT('season = ''', @season, '''')),
                           (@sync_app,  6, 'debearco_sports', 'SPORTS_NHL_GAME_STRENGTHS',                         CONCAT('season = ''', @season, '''')),
                           (@sync_app,  7, 'debearco_sports', 'SPORTS_NHL_GAME_THREE_STARS',                       CONCAT('season = ''', @season, '''')),
                           (@sync_app,  8, 'debearco_sports', 'SPORTS_NHL_OFFICIALS',                              ''),
                           (@sync_app,  9, 'debearco_sports', 'SPORTS_NHL_PLAYERS_GAME_GOALIES',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_NHL_PLAYERS_GAME_SKATERS',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_GOALIES',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_GOALIES_SORTED',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 13, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_SKATERS',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app, 14, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_SKATERS_SORTED',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 15, 'debearco_sports', 'SPORTS_NHL_STANDINGS',                              CONCAT('season = ''', @season, '''')),
                           (@sync_app, 16, 'debearco_sports', 'SPORTS_NHL_TEAMS_GAME_GOALIES',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 17, 'debearco_sports', 'SPORTS_NHL_TEAMS_GAME_SKATERS',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 18, 'debearco_sports', 'SPORTS_NHL_TEAMS_ROSTERS',                          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 19, 'debearco_sports', 'SPORTS_NHL_TEAMS_SEASON_GOALIES',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 20, 'debearco_sports', 'SPORTS_NHL_TEAMS_SEASON_GOALIES_SORTED',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 21, 'debearco_sports', 'SPORTS_NHL_TEAMS_SEASON_SKATERS',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 22, 'debearco_sports', 'SPORTS_NHL_TEAMS_SEASON_SKATERS_SORTED',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 23, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_PLAYOFF',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 24, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_PLAYOFF_SUMMARY',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 25, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_REGULAR',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 26, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_TITLES',                   ''),
                           (@sync_app, 31, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_SKATERS_HEATMAP',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 32, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_GOALIES_HEATMAP',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 33, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 34, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_SKATERS_ADVANCED_SORTED', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 35, 'debearco_sports', 'SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 36, 'debearco_sports', 'SPORTS_NHL_TEAMS_SEASON_SKATERS_ADVANCED_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 37, 'debearco_sports', 'SPORTS_NHL_PLAYERS_GAME_SKATERS_ADVANCED',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 38, 'debearco_sports', 'SPORTS_NHL_TEAMS_GAME_SKATERS_ADVANCED',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 39, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_BOXSCORE',                    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 40, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_COORDS',                      CONCAT('season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 27, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_nhl_players __REMOTE__'),
                               (@sync_app, 28, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_nhl_power-ranks __REMOTE__'),
                               (@sync_app, 29, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_nhl_schedules __REMOTE__'),
                               (@sync_app, 30, '/var/www/debear/bin/git-admin/server-sync', '--down sports_nhl_news __REMOTE__');

#
# NHL Players
#
SET @sync_app := 'sports_nhl_players';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL Players', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NHL_PLAYERS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_GOALIES', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_NHL_PLAYERS_SEASON_SKATERS', 3, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_NHL_DRAFT', 4, 'database'),
                        (@sync_app, 6, '__DIR__ SPORTS_NHL_PLAYERS_AWARDS', 5, 'database'),
                        (@sync_app, 4, '__DIR__ Player Mugshots', 6, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NHL_PLAYERS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_GOALIES', CONCAT('season < ''2007'' OR (season < ''', @season, ''' AND team_name IS NOT NULL)')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_NHL_PLAYERS_SEASON_SKATERS', CONCAT('season < ''2007'' OR (season < ''', @season, ''' AND team_name IS NOT NULL)')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_NHL_DRAFT', ''),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_NHL_PLAYERS_AWARDS', '');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 4, '/var/www/debear/sites/cdn/htdocs/sports/nhl/players', 'debear/sites/cdn/htdocs/sports/nhl/players', NULL);

#
# NHL Teams
#
SET @sync_app := 'sports_nhl_teams';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL Teams', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NHL_GROUPINGS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NHL_TEAMS', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_NHL_TEAMS_GROUPINGS', 3, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_PLAYOFF', 4, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_PLAYOFF_SUMMARY', 5, 'database'),
                        (@sync_app, 6, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_REGULAR', 6, 'database'),
                        (@sync_app, 7, '__DIR__ SPORTS_NHL_TEAMS_HISTORY_TITLES', 7, 'database'),
                        (@sync_app, 8, '__DIR__ SPORTS_NHL_TEAMS_NAMING', 8, 'database'),
                        (@sync_app, 9, '__DIR__ SPORTS_NHL_TEAMS_ARENAS', 9, 'database');
#                        (@sync_app, 10, '__DIR__ Team Logos', 10, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NHL_GROUPINGS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NHL_TEAMS', ''),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_NHL_TEAMS_GROUPINGS', ''),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_PLAYOFF', ''),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_PLAYOFF_SUMMARY', ''),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_REGULAR', ''),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_NHL_TEAMS_HISTORY_TITLES', ''),
                           (@sync_app, 8, 'debearco_sports', 'SPORTS_NHL_TEAMS_NAMING', ''),
                           (@sync_app, 9, 'debearco_sports', 'SPORTS_NHL_TEAMS_ARENAS', '');
#INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
#                      VALUES (@sync_app, 10, '/var/www/debear/sites/cdn/htdocs/sports/nhl/team', 'debear/sites/cdn/htdocs/sports/nhl/team', NULL);

#
# NHL Schedules
#
SET @sync_app := 'sports_nhl_schedules';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL Schedules', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NHL_SCHEDULE', 1, 'database'),
                        (@sync_app, 6, '__DIR__ SPORTS_NHL_SCHEDULE_DATES', 2, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NHL_SCHEDULE_MATCHUPS', 3, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_NHL_PLAYOFF_SEEDS', 4, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_NHL_PLAYOFF_SERIES', 5, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_NHL_STANDINGS', 7, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NHL_SCHEDULE', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NHL_SCHEDULE_MATCHUPS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_NHL_PLAYOFF_SEEDS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_NHL_PLAYOFF_SERIES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_NHL_STANDINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_NHL_SCHEDULE_DATES', CONCAT('season = ''', @season, ''''));

#
# NHL Power Ranks
#
SET @sync_app := 'sports_nhl_power-ranks';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL Power Ranks', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NHL_POWER_RANKINGS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NHL_POWER_RANKINGS_WEEKS', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NHL_POWER_RANKINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NHL_POWER_RANKINGS_WEEKS', CONCAT('season = ''', @season, ''''));

#
# NHL Drafts
#
SET @sync_app := 'sports_nhl_draft';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL Draft', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NHL_DRAFT', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NHL_DRAFT', '');

#
# NHL Starting Lineups
#
SET @sync_app := 'sports_nhl_lineup';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL Starting Lineups', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NHL_GAME_LINEUP', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NHL_GAME_SCRATCHES', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_NHL_PLAYERS', 3, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NHL_GAME_LINEUP', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NHL_GAME_SCRATCHES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_NHL_PLAYERS', '');

#
# NHL News
#
SET @sync_app := 'sports_nhl_news';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL News', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ NEWS_SOURCES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ NEWS_ARTICLES', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause, clear_first)
                    VALUES (@sync_app, 1, 'debearco_common', 'NEWS_SOURCES', 'app = ''sports_nhl''', 0),
                           (@sync_app, 2, 'debearco_common', 'NEWS_ARTICLES', 'app = ''sports_nhl''', 1);

#
# NHL Game Odds
#
SET @sync_app := 'sports_nhl_odds';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL Odds', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_COMMON_MAJORS_ODDS', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_COMMON_MAJORS_ODDS', 'sport = ''nhl''');

#
# Additional NHL data
#
SET @sync_app := 'sports_nhl_full';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NHL (Full)', 'data', 'dev', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_NHL_GAME_EVENT',              1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_NHL_GAME_EVENT_BLOCKEDSHOT',  2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_NHL_GAME_EVENT_FACEOFF',      3, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_NHL_GAME_EVENT_GIVEAWAY',     4, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_NHL_GAME_EVENT_GOAL',         5, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_NHL_GAME_EVENT_HIT',          6, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_NHL_GAME_EVENT_MISSEDSHOT',   7, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_NHL_GAME_EVENT_ONICE',        8, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_NHL_GAME_EVENT_PENALTY',      9, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_NHL_GAME_EVENT_SHOOTOUT',    10, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_NHL_GAME_EVENT_SHOT',        11, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_NHL_GAME_EVENT_STOPPAGE',    12, 'database'),
                        (@sync_app, 13, '__DIR__ SPORTS_NHL_GAME_EVENT_TAKEAWAY',    13, 'database'),
                        (@sync_app, 14, '__DIR__ SPORTS_NHL_GAME_EVENT_TIMEOUT',     14, 'database'),
                        (@sync_app, 15, '__DIR__ SPORTS_NHL_GAME_EVENT_UNKNOWN',     15, 'database'),
                        (@sync_app, 16, '__DIR__ SPORTS_NHL_GAME_LINEUP_ISSUES',     16, 'database'),
                        (@sync_app, 17, '__DIR__ SPORTS_NHL_GAME_SCRATCHES_ISSUES',  17, 'database'),
                        (@sync_app, 18, '__DIR__ SPORTS_NHL_GAME_SUMMARISED',        18, 'database'),
                        (@sync_app, 19, '__DIR__ SPORTS_NHL_GAME_TOI',               19, 'database'),
                        (@sync_app, 20, '__DIR__ SPORTS_NHL_PLAYERS_IMPORT',         20, 'database');

INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app,  1, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT',             CONCAT('season = ''', @season, '''')),
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_BLOCKEDSHOT', CONCAT('season = ''', @season, '''')),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_FACEOFF',     CONCAT('season = ''', @season, '''')),
                           (@sync_app,  4, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_GIVEAWAY',    CONCAT('season = ''', @season, '''')),
                           (@sync_app,  5, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_GOAL',        CONCAT('season = ''', @season, '''')),
                           (@sync_app,  6, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_HIT',         CONCAT('season = ''', @season, '''')),
                           (@sync_app,  7, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_MISSEDSHOT',  CONCAT('season = ''', @season, '''')),
                           (@sync_app,  8, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_ONICE',       CONCAT('season = ''', @season, '''')),
                           (@sync_app,  9, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_PENALTY',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_SHOOTOUT',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_SHOT',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_STOPPAGE',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 13, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_TAKEAWAY',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 14, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_TIMEOUT',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 15, 'debearco_sports', 'SPORTS_NHL_GAME_EVENT_UNKNOWN',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 16, 'debearco_sports', 'SPORTS_NHL_GAME_LINEUP_ISSUES',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 17, 'debearco_sports', 'SPORTS_NHL_GAME_SCRATCHES_ISSUES',  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 18, 'debearco_sports', 'SPORTS_NHL_GAME_SUMMARISED',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 19, 'debearco_sports', 'SPORTS_NHL_GAME_TOI',               CONCAT('season = ''', @season, '''')),
                           (@sync_app, 20, 'debearco_sports', 'SPORTS_NHL_PLAYERS_IMPORT',         '');
