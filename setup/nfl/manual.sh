s=2020
t=$1
w=$2
d=_"logs/games/$3"
exit

echo -e "\n* Type: $t; Week: $w; (Log: $d) *"
date; echo
rm -rf $d/*

# Parse Games
echo "Parse Games:"
for g in $(echo "SELECT game_id FROM SPORTS_NFL_SCHEDULE WHERE season = $s AND game_type = '$t' AND week = $w ORDER BY game_date, game_time, visitor;" | mysql -s debearco_sports); do
    echo -n "- $g: ";
    games/parse.pl $s $t $g >>$d/02_games_parse.sql 2>>$d/02_games_parse.err;
    echo "[ Done ]";
done

# Import Games
echo -n "Import: "
mysql debearco_sports <$d/02_games_parse.sql >>$d/03_games_import.log 2>>$d/03_games_import.err
if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/03_games_import.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
echo "[ Done ]"

# Season Totals (Players)
echo -n "Player Totals: "
echo "CALL nfl_totals_players($s, '$t')" | mysql debearco_sports >>$d/06_totals_player.log 2>>$d/06_totals_player.err
if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/06_totals_player.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
echo "CALL nfl_totals_players_sort($s, '$t')" | mysql debearco_sports >>$d/06_totals_player.log 2>>$d/06_totals_player.err
if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/06_totals_player.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
echo "[ Done ]"

# Season Totals (Teams)
echo -n "Teams Totals: "
echo "CALL nfl_totals_teams($s, '$t')" | mysql debearco_sports >>$d/07_totals_team.log 2>>$d/07_totals_team.err
if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/07_totals_team.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
echo "CALL nfl_totals_teams_sort($s, '$t')" | mysql debearco_sports >>$d/07_totals_team.log 2>>$d/07_totals_team.err
if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/07_totals_team.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
echo "[ Done ]"

# Regular Season calcs
if [ "$t" = 'regular' ]
then
    # Standings
    echo -n "Standings: "
    echo "CALL nfl_standings($s, '$w', 5)" | mysql debearco_sports >>$d/08_standings.log 2>>$d/08_standings.err
    if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/08_standings.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
    echo "[ Done ]"

    # Playoff seeds
    echo -n "Playoff Seeds: "
    echo "CALL nfl_playoff_seeds($s)" | mysql debearco_sports >>$d/12_playoff_seeds.log 2>>$d/12_playoff_seeds.err
    if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/12_playoff_seeds.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
    echo "[ Done ]"
fi

# Playoff series
if [ "$t" = 'playoff' ]
then
    echo -n "Playoff Series: "
    echo "CALL nfl_playoff_series($s)" | mysql debearco_sports >>$d/13_playoff_series.log 2>>$d/13_playoff_series.err
    if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/13_playoff_series.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
    echo "[ Done ]"
fi

# Playoff matchups
echo -n "Playoff Matchups: "
echo "CALL nfl_playoff_matchups($s)" | mysql debearco_sports >>$d/14_playoff_matchups.log 2>>$d/14_playoff_matchups.err
if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/14_playoff_matchups.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
echo "[ Done ]"

# Playoff histories
if [ "$t" = 'playoff' ]
then
    echo -n "Histories: "
    echo "CALL nfl_history_regular($s)" | mysql debearco_sports >>$d/15_season_history.log 2>>$d/15_season_history.err
    if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/15_season_history.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
    echo "CALL nfl_history_playoffs($s)" | mysql debearco_sports >>$d/15_season_history.log 2>>$d/15_season_history.err
    if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/15_season_history.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
    echo "[ Done ]"
fi

# Power Ranks
echo -n "Power Ranks: "
echo "CALL nfl_power_ranks($s, '$t', '$w', 5)" | mysql debearco_sports >>$d/16_power_ranks.log 2>>$d/16_power_ranks.err
if [ $? -gt 0 ] || [ $(grep -vc '^#' $d/16_power_ranks.err) -gt 0 ]; then echo "[ Error ]"; exit 1; fi
echo "[ Done ]"

# Final flourish (last week only)
if [ "$t/$w" = 'playoff/4' ]
then
    games/order.pl | mysql debearco_sports
    echo "CALL nfl_totals_players_order()" | mysql debearco_sports
    echo "CALL nfl_totals_teams_order()" | mysql debearco_sports
fi

echo; date
