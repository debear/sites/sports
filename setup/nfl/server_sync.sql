# Season to sync...
SET @season := 2024;

#
# General NFL update
#
SET @sync_app := 'sports_nfl';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_NFL_GAME_DRIVES',                      1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_NFL_GAME_INACTIVES',                   2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_NFL_GAME_LINEUP',                      3, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_NFL_GAME_OFFICIALS',                   4, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_NFL_GAME_SCORING',                     5, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_NFL_GAME_SCORING_2PT',                 6, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_NFL_GAME_SCORING_FG',                  7, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_NFL_GAME_SCORING_PAT',                 8, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_NFL_GAME_SCORING_SAFETY',              9, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_NFL_GAME_SCORING_TD',                 10, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_NFL_GAME_STATS_2PT',                  11, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_NFL_GAME_STATS_DOWNS',                12, 'database'),
                        (@sync_app, 13, '__DIR__ SPORTS_NFL_GAME_STATS_KICKOFFS',             13, 'database'),
                        (@sync_app, 14, '__DIR__ SPORTS_NFL_GAME_STATS_KICKS',                14, 'database'),
                        (@sync_app, 15, '__DIR__ SPORTS_NFL_GAME_STATS_MISC',                 15, 'database'),
                        (@sync_app, 16, '__DIR__ SPORTS_NFL_GAME_STATS_PLAYS',                16, 'database'),
                        (@sync_app, 17, '__DIR__ SPORTS_NFL_GAME_STATS_PUNTS',                17, 'database'),
                        (@sync_app, 18, '__DIR__ SPORTS_NFL_GAME_STATS_RETURNS',              18, 'database'),
                        (@sync_app, 19, '__DIR__ SPORTS_NFL_GAME_STATS_TDS',                  19, 'database'),
                        (@sync_app, 20, '__DIR__ SPORTS_NFL_GAME_STATS_TURNOVERS',            20, 'database'),
                        (@sync_app, 21, '__DIR__ SPORTS_NFL_GAME_STATS_YARDS',                21, 'database'),
                        (@sync_app, 22, '__DIR__ SPORTS_NFL_OFFICIALS',                       22, 'database'),
                        (@sync_app, 23, '__DIR__ SPORTS_NFL_PLAYERS_GAME_FUMBLES',            23, 'database'),
                        (@sync_app, 24, '__DIR__ SPORTS_NFL_PLAYERS_GAME_KICKING',            24, 'database'),
                        (@sync_app, 25, '__DIR__ SPORTS_NFL_PLAYERS_GAME_KICKRET',            25, 'database'),
                        (@sync_app, 26, '__DIR__ SPORTS_NFL_PLAYERS_GAME_PASSDEF',            26, 'database'),
                        (@sync_app, 27, '__DIR__ SPORTS_NFL_PLAYERS_GAME_PASSING',            27, 'database'),
                        (@sync_app, 28, '__DIR__ SPORTS_NFL_PLAYERS_GAME_PUNTRET',            28, 'database'),
                        (@sync_app, 29, '__DIR__ SPORTS_NFL_PLAYERS_GAME_PUNTS',              29, 'database'),
                        (@sync_app, 30, '__DIR__ SPORTS_NFL_PLAYERS_GAME_RECEIVING',          30, 'database'),
                        (@sync_app, 31, '__DIR__ SPORTS_NFL_PLAYERS_GAME_RUSHING',            31, 'database'),
                        (@sync_app, 32, '__DIR__ SPORTS_NFL_PLAYERS_GAME_TACKLES',            32, 'database'),
                        (@sync_app, 33, '__DIR__ SPORTS_NFL_PLAYERS_INJURIES',                33, 'database'),
                        (@sync_app, 34, '__DIR__ SPORTS_NFL_PLAYERS_SEASON',                  34, 'database'),
                        (@sync_app, 35, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_FUMBLES',          35, 'database'),
                        (@sync_app, 36, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_FUMBLES_SORTED',   36, 'database'),
                        (@sync_app, 37, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_KICKING',          37, 'database'),
                        (@sync_app, 38, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_KICKING_SORTED',   38, 'database'),
                        (@sync_app, 39, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_KICKRET',          39, 'database'),
                        (@sync_app, 40, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_KICKRET_SORTED',   40, 'database'),
                        (@sync_app, 41, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PASSDEF',          41, 'database'),
                        (@sync_app, 42, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PASSDEF_SORTED',   42, 'database'),
                        (@sync_app, 43, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PASSING',          43, 'database'),
                        (@sync_app, 44, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PASSING_SORTED',   44, 'database'),
                        (@sync_app, 45, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PUNTRET',          45, 'database'),
                        (@sync_app, 46, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PUNTRET_SORTED',   46, 'database'),
                        (@sync_app, 47, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PUNTS',            47, 'database'),
                        (@sync_app, 48, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PUNTS_SORTED',     48, 'database'),
                        (@sync_app, 49, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_RECEIVING',        49, 'database'),
                        (@sync_app, 50, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_RECEIVING_SORTED', 50, 'database'),
                        (@sync_app, 51, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_RUSHING',          51, 'database'),
                        (@sync_app, 52, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_RUSHING_SORTED',   52, 'database'),
                        (@sync_app, 53, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_TACKLES',          53, 'database'),
                        (@sync_app, 54, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_TACKLES_SORTED',   54, 'database'),
                        (@sync_app, 55, '__DIR__ SPORTS_NFL_STANDINGS',                       55, 'database'),
                        (@sync_app, 56, '__DIR__ SPORTS_NFL_TEAMS_ROSTERS',                   56, 'database'),
                        (@sync_app, 57, '__DIR__ SPORTS_NFL_TEAMS_STATS_2PT',                 57, 'database'),
                        (@sync_app, 58, '__DIR__ SPORTS_NFL_TEAMS_STATS_2PT_SORTED',          58, 'database'),
                        (@sync_app, 59, '__DIR__ SPORTS_NFL_TEAMS_STATS_DEFENSE',             59, 'database'),
                        (@sync_app, 60, '__DIR__ SPORTS_NFL_TEAMS_STATS_DEFENSE_SORTED',      60, 'database'),
                        (@sync_app, 61, '__DIR__ SPORTS_NFL_TEAMS_STATS_DOWNS',               61, 'database'),
                        (@sync_app, 62, '__DIR__ SPORTS_NFL_TEAMS_STATS_DOWNS_SORTED',        62, 'database'),
                        (@sync_app, 63, '__DIR__ SPORTS_NFL_TEAMS_STATS_KICKOFFS',            63, 'database'),
                        (@sync_app, 64, '__DIR__ SPORTS_NFL_TEAMS_STATS_KICKOFFS_SORTED',     64, 'database'),
                        (@sync_app, 65, '__DIR__ SPORTS_NFL_TEAMS_STATS_KICKS',               65, 'database'),
                        (@sync_app, 66, '__DIR__ SPORTS_NFL_TEAMS_STATS_KICKS_SORTED',        66, 'database'),
                        (@sync_app, 67, '__DIR__ SPORTS_NFL_TEAMS_STATS_MISC',                67, 'database'),
                        (@sync_app, 68, '__DIR__ SPORTS_NFL_TEAMS_STATS_MISC_SORTED',         68, 'database'),
                        (@sync_app, 69, '__DIR__ SPORTS_NFL_TEAMS_STATS_PLAYS',               69, 'database'),
                        (@sync_app, 70, '__DIR__ SPORTS_NFL_TEAMS_STATS_PLAYS_SORTED',        70, 'database'),
                        (@sync_app, 71, '__DIR__ SPORTS_NFL_TEAMS_STATS_PUNTS',               71, 'database'),
                        (@sync_app, 72, '__DIR__ SPORTS_NFL_TEAMS_STATS_PUNTS_SORTED',        72, 'database'),
                        (@sync_app, 73, '__DIR__ SPORTS_NFL_TEAMS_STATS_RETURNS',             73, 'database'),
                        (@sync_app, 74, '__DIR__ SPORTS_NFL_TEAMS_STATS_RETURNS_SORTED',      74, 'database'),
                        (@sync_app, 75, '__DIR__ SPORTS_NFL_TEAMS_STATS_TDS',                 75, 'database'),
                        (@sync_app, 76, '__DIR__ SPORTS_NFL_TEAMS_STATS_TDS_SORTED',          76, 'database'),
                        (@sync_app, 77, '__DIR__ SPORTS_NFL_TEAMS_STATS_TURNOVERS',           77, 'database'),
                        (@sync_app, 78, '__DIR__ SPORTS_NFL_TEAMS_STATS_TURNOVERS_SORTED',    78, 'database'),
                        (@sync_app, 79, '__DIR__ SPORTS_NFL_TEAMS_STATS_YARDS',               79, 'database'),
                        (@sync_app, 80, '__DIR__ SPORTS_NFL_TEAMS_STATS_YARDS_SORTED',        80, 'database'),
                        (@sync_app, 81, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY',   81, 'database'),
                        (@sync_app, 82, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_PLAYOFF',           82, 'database'),
                        (@sync_app, 83, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_REGULAR',           83, 'database'),
                        (@sync_app, 84, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_TITLES',            84, 'database'),
                        (@sync_app, 85, '__DIR__ Players',                                    85, 'script'),
                        (@sync_app, 86, '__DIR__ Player Injuries',                            86, 'script'),
                        (@sync_app, 87, '__DIR__ Power Ranks',                                87, 'script'),
                        (@sync_app, 88, '__DIR__ Schedule',                                   88, 'script'),
                        (@sync_app, 89, 'Synchronise News',                                   89, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app,  1, 'debearco_sports', 'SPORTS_NFL_GAME_DRIVES',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_NFL_GAME_INACTIVES',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_NFL_GAME_LINEUP',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app,  4, 'debearco_sports', 'SPORTS_NFL_GAME_OFFICIALS',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app,  5, 'debearco_sports', 'SPORTS_NFL_GAME_SCORING',                    CONCAT('season = ''', @season, '''')),
                           (@sync_app,  6, 'debearco_sports', 'SPORTS_NFL_GAME_SCORING_2PT',                CONCAT('season = ''', @season, '''')),
                           (@sync_app,  7, 'debearco_sports', 'SPORTS_NFL_GAME_SCORING_FG',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app,  8, 'debearco_sports', 'SPORTS_NFL_GAME_SCORING_PAT',                CONCAT('season = ''', @season, '''')),
                           (@sync_app,  9, 'debearco_sports', 'SPORTS_NFL_GAME_SCORING_SAFETY',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_NFL_GAME_SCORING_TD',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_2PT',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_DOWNS',                CONCAT('season = ''', @season, '''')),
                           (@sync_app, 13, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_KICKOFFS',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 14, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_KICKS',                CONCAT('season = ''', @season, '''')),
                           (@sync_app, 15, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_MISC',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app, 16, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_PLAYS',                CONCAT('season = ''', @season, '''')),
                           (@sync_app, 17, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_PUNTS',                CONCAT('season = ''', @season, '''')),
                           (@sync_app, 18, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_RETURNS',              CONCAT('season = ''', @season, '''')),
                           (@sync_app, 19, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_TDS',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 20, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_TURNOVERS',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 21, 'debearco_sports', 'SPORTS_NFL_GAME_STATS_YARDS',                CONCAT('season = ''', @season, '''')),
                           (@sync_app, 22, 'debearco_sports', 'SPORTS_NFL_OFFICIALS',                       ''),
                           (@sync_app, 23, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_FUMBLES',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 24, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_KICKING',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 25, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_KICKRET',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 26, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_PASSDEF',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 27, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_PASSING',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 28, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_PUNTRET',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 29, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_PUNTS',              CONCAT('season = ''', @season, '''')),
                           (@sync_app, 30, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_RECEIVING',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 31, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_RUSHING',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 32, 'debearco_sports', 'SPORTS_NFL_PLAYERS_GAME_TACKLES',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 33, 'debearco_sports', 'SPORTS_NFL_PLAYERS_INJURIES',                CONCAT('season = ''', @season, '''')),
                           (@sync_app, 34, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 35, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_FUMBLES',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 36, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_FUMBLES_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 37, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_KICKING',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 38, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_KICKING_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 39, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_KICKRET',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 40, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_KICKRET_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 41, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PASSDEF',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 42, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PASSDEF_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 43, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PASSING',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 44, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PASSING_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 45, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PUNTRET',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 46, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PUNTRET_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 47, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PUNTS',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 48, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PUNTS_SORTED',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 49, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_RECEIVING',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 50, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_RECEIVING_SORTED', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 51, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_RUSHING',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 52, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_RUSHING_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 53, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_TACKLES',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 54, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_TACKLES_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 55, 'debearco_sports', 'SPORTS_NFL_STANDINGS',                       CONCAT('season = ''', @season, '''')),
                           (@sync_app, 56, 'debearco_sports', 'SPORTS_NFL_TEAMS_ROSTERS',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 57, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_2PT',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app, 58, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_2PT_SORTED',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 59, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_DEFENSE',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 60, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_DEFENSE_SORTED',      CONCAT('season = ''', @season, '''')),
                           (@sync_app, 61, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_DOWNS',               CONCAT('season = ''', @season, '''')),
                           (@sync_app, 62, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_DOWNS_SORTED',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 63, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_KICKOFFS',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 64, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_KICKOFFS_SORTED',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 65, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_KICKS',               CONCAT('season = ''', @season, '''')),
                           (@sync_app, 66, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_KICKS_SORTED',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 67, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_MISC',                CONCAT('season = ''', @season, '''')),
                           (@sync_app, 68, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_MISC_SORTED',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 69, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_PLAYS',               CONCAT('season = ''', @season, '''')),
                           (@sync_app, 70, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_PLAYS_SORTED',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 71, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_PUNTS',               CONCAT('season = ''', @season, '''')),
                           (@sync_app, 72, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_PUNTS_SORTED',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 73, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_RETURNS',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 74, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_RETURNS_SORTED',      CONCAT('season = ''', @season, '''')),
                           (@sync_app, 75, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_TDS',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app, 76, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_TDS_SORTED',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 77, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_TURNOVERS',           CONCAT('season = ''', @season, '''')),
                           (@sync_app, 78, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_TURNOVERS_SORTED',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 79, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_YARDS',               CONCAT('season = ''', @season, '''')),
                           (@sync_app, 80, 'debearco_sports', 'SPORTS_NFL_TEAMS_STATS_YARDS_SORTED',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 81, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 82, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_PLAYOFF',           CONCAT('season = ''', @season, '''')),
                           (@sync_app, 83, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_REGULAR',           CONCAT('season = ''', @season, '''')),
                           (@sync_app, 84, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_TITLES',            '');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 85, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_nfl_players __REMOTE__'),
                               (@sync_app, 86, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_nfl_injuries __REMOTE__'),
                               (@sync_app, 87, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_nfl_power-ranks __REMOTE__'),
                               (@sync_app, 88, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_nfl_schedules __REMOTE__'),
                               (@sync_app, 89, '/var/www/debear/bin/git-admin/server-sync', '--down sports_nfl_news __REMOTE__');

#
# NFL Injuries
#
SET @sync_app := 'sports_nfl_injuries';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Injuries', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NFL_PLAYERS_INJURIES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NFL_GAME_INACTIVES', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NFL_PLAYERS_INJURIES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NFL_GAME_INACTIVES', CONCAT('season = ''', @season, ''''));

#
# NFL Players
#
SET @sync_app := 'sports_nfl_players';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Players', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_NFL_PLAYERS', 1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_NFL_PLAYERS_SEASON', 2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_FUMBLES', 3, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_KICKING', 4, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_KICKRET', 5, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PASSDEF', 6, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PASSING', 7, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PUNTRET', 8, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_PUNTS', 9, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_RECEIVING', 10, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_RUSHING', 11, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_NFL_PLAYERS_SEASON_TACKLES', 12, 'database'),
                        (@sync_app, 14, '__DIR__ SPORTS_NFL_PLAYERS_AWARDS', 13, 'database'),
                        (@sync_app, 13, '__DIR__ Player Mugshots', 14, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app,  1, 'debearco_sports', 'SPORTS_NFL_PLAYERS', ''),
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON', 'season < ''2009'''),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_FUMBLES', 'season < ''2009'''),
                           (@sync_app,  4, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_KICKING', 'season < ''2009'''),
                           (@sync_app,  5, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_KICKRET', 'season < ''2009'''),
                           (@sync_app,  6, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PASSDEF', 'season < ''2009'''),
                           (@sync_app,  7, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PASSING', 'season < ''2009'''),
                           (@sync_app,  8, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PUNTRET', 'season < ''2009'''),
                           (@sync_app,  9, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_PUNTS', 'season < ''2009'''),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_RECEIVING', 'season < ''2009'''),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_RUSHING', 'season < ''2009'''),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_NFL_PLAYERS_SEASON_TACKLES', 'season < ''2009'''),
                           (@sync_app, 14, 'debearco_sports', 'SPORTS_NFL_PLAYERS_AWARDS', '');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 13, '/var/www/debear/sites/cdn/htdocs/sports/nfl/players', 'debear/sites/cdn/htdocs/sports/nfl/players', NULL);

#
# NFL Players Additional Info
#
SET @sync_app := 'sports_nfl_players_dev';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Players', 'data', 'dev', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_NFL_PLAYERS_IMPORT', 1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS', 2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_NFL_TEAMS_DEPTH', 3, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app,  1, 'debearco_sports', 'SPORTS_NFL_PLAYERS_IMPORT', ''),
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_NFL_PLAYERS_IMPORT_MUGSHOTS', ''),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_NFL_TEAMS_DEPTH', '');

#
# NFL Teams
#
SET @sync_app := 'sports_nfl_teams';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Teams', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_NFL_GROUPINGS', 1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_NFL_TEAMS', 2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_NFL_TEAMS_GROUPINGS', 3, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_NFL_TEAMS_NAMING', 4, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_PLAYOFF', 5, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY', 6, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_REGULAR', 7, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_NFL_TEAMS_HISTORY_TITLES', 8, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_NFL_TEAMS_STADIA', 9, 'database');
#                        (@sync_app, 10, '__DIR__ Team Logos', 10, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NFL_GROUPINGS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NFL_TEAMS', ''),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_NFL_TEAMS_GROUPINGS', ''),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_NFL_TEAMS_NAMING', ''),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_PLAYOFF', ''),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_PLAYOFF_SUMMARY', ''),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_REGULAR', ''),
                           (@sync_app, 8, 'debearco_sports', 'SPORTS_NFL_TEAMS_HISTORY_TITLES', ''),
                           (@sync_app, 9, 'debearco_sports', 'SPORTS_NFL_TEAMS_STADIA', '');
#INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
#                      VALUES (@sync_app, 10, '/var/www/debear/sites/cdn/htdocs/sports/nfl/team', 'debear/sites/cdn/htdocs/sports/nfl/team', NULL);

#
# NFL Schedules
#
SET @sync_app := 'sports_nfl_schedules';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Schedules', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NFL_SCHEDULE', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NFL_SCHEDULE_BYES', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_NFL_SCHEDULE_WEEKS', 3, 'database'),
                        (@sync_app, 6, '__DIR__ SPORTS_NFL_PLAYOFF_SEEDS', 4, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_NFL_PLAYOFF_MATCHUPS', 5, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_NFL_STANDINGS', 6, 'database'),
                        (@sync_app, 7, 'Synchronise Weather', 7, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NFL_SCHEDULE', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NFL_SCHEDULE_BYES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_NFL_SCHEDULE_WEEKS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_NFL_PLAYOFF_MATCHUPS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_NFL_STANDINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_NFL_PLAYOFF_SEEDS', CONCAT('season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 7, '/var/www/debear/bin/git-admin/server-sync', '--down sports_nfl_weather __REMOTE__');

#
# NFL Power Ranks
#
SET @sync_app := 'sports_nfl_power-ranks';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Power Ranks', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_NFL_POWER_RANKINGS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_NFL_POWER_RANKINGS_WEEKS', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_NFL_POWER_RANKINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_NFL_POWER_RANKINGS_WEEKS', CONCAT('season = ''', @season, ''''));

#
# NFL News
#
SET @sync_app := 'sports_nfl_news';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL News', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ NEWS_SOURCES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ NEWS_ARTICLES', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause, clear_first)
                    VALUES (@sync_app, 1, 'debearco_common', 'NEWS_SOURCES', 'app = ''sports_nfl''', 0),
                           (@sync_app, 2, 'debearco_common', 'NEWS_ARTICLES', 'app = ''sports_nfl''', 1);

#
# NFL Game Odds
#
SET @sync_app := 'sports_nfl_odds';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Odds', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_COMMON_MAJORS_ODDS', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_COMMON_MAJORS_ODDS', 'sport = ''nfl''');

#
# NFL Weather
#
SET @sync_app := 'sports_nfl_weather';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports NFL Weather', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ WEATHER_FORECAST', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_common', 'WEATHER_FORECAST', 'app = ''sports_nfl''');
