TRUNCATE TABLE SPORTS_NFL_TEAMS;
INSERT INTO SPORTS_NFL_TEAMS (team_id, city, franchise, founded) VALUES
('BUF', 'Buffalo', 'Bills', '1960'),
('MIA', 'Miami', 'Dolphins', '1966'),
('BOP', 'Boston', 'Patriots', '1959'),
('NE', 'New England', 'Patriots', '1959'),
('NYJ', 'New York', 'Jets', '1959'),
('BAL', 'Baltimore', 'Ravens', '1996'),
('CIN', 'Cincinnati', 'Bengals', '1968'),
('CLE', 'Cleveland', 'Browns', '1946'),
('PIT', 'Pittsburgh', 'Steelers', '1933'),
('HOU', 'Houston', 'Texans', '2002'),
('BLC', 'Baltimore', 'Colts', '1953'),
('IND', 'Indianapolis', 'Colts', '1953'),
('JAX', 'Jacksonville', 'Jaguars', '1993'),
('HOO', 'Houston', 'Oilers', '1960'),
('TEN', 'Tennessee', 'Titans', '1960'),
('DEN', 'Denver', 'Broncos', '1960'),
('DAT', 'Dallas', 'Texans', '1960'),
('KC', 'Kansas City', 'Chiefs', '1960'),
('OAK', 'Oakland', 'Raiders', '1960'),
('LRI', 'Los Angeles', 'Raiders', '1960'),
-- ('OAK', 'Oakland', 'Raiders', '1960'),
('LAC', 'Los Angeles', 'Chargers', '1960'),
('SD', 'San Diego', 'Chargers', '1960'),
('DAL', 'Dallas', 'Cowboys', '1960'),
('NYG', 'New York', 'Giants', '1925'),
('PHI', 'Philadelphia', 'Eagles', '1933'),
('BOR', 'Boston', 'Redskins', '1932'),
('WSH', 'Washington', 'Redskins', '1932'),
('DEC', 'Decatur', 'Staleys', '1919'),
('CHI', 'Chicago', 'Bears', '1919'),
('POR', 'Portsmouth', 'Spartans', '1930'),
('DET', 'Detroit', 'Lions', '1930'),
('GB', 'Green Bay', 'Packers', '1919'),
('MIN', 'Minnesota', 'Vikings', '1961'),
('ATL', 'Atlanta', 'Falcons', '1965'),
('CAR', 'Carolina', 'Panthers', '1993'),
('NO', 'New Orleans', 'Saints', '1966'),
('TB', 'Tampa Bay', 'Buccaneers', '1976'),
('CHC', 'Chicago', 'Cardinals', '1898'),
('SLC', 'St Louis', 'Cardinals', '1898'),
('PHC', 'Phoenix', 'Cardinals', '1898'),
('ARI', 'Arizona', 'Cardinals', '1898'),
('CLR', 'Cleveland', 'Rams', '1936'),
('LRM', 'Los Angeles', 'Rams', '1936'),
('STL', 'St Louis', 'Rams', '1936'),
('SF', 'San Francisco', '49ers', '1946'),
('SEA', 'Seattle', 'Seahawks', '1974'),
('PPS', 'Phil-Pitt', 'Steagles', '1943'),
('CP', 'Card-Pitt', '', '1944');
