SET @season := 2013;

#
# Remove any previous setup
#
DELETE FROM `SPORTS_F1_DRIVER_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_DRIVERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_GRID` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_LEADERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_QUALI` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_RESULT` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAMS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY_PROGRESS` WHERE `season` = @season;

#
# Race calendar
#
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,1,'Australian Grand Prix','Australia','Rolex Australian Grand Prix','au','Melbourne Grand Prix Circuit','Melbourne','Australia/Melbourne','2013-03-17 06:00:00','2013-03-16 06:00:00',1,58,5.303,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,2,'Malaysian Grand Prix','Malaysia','Petronas Malaysia Grand Prix','my','Sepang International Circuit','Kuala Lumpur','Asia/Kuala_Lumpur','2013-03-24 08:00:00','2013-03-23 08:00:00',2,56,5.543,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,3,'Chinese Grand Prix','China','UBS Chinese Grand Prix','cn','Shanghai International Circuit','Shanghai','Asia/Shanghai','2013-04-14 07:00:00','2013-04-13 06:00:00',3,56,5.451,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,4,'Bahrain Grand Prix','Bahrain','Gulf Air Bahrain Grand Prix','bh','Bahrain International Circuit','Sakhir','Asia/Bahrain','2013-04-21 12:00:00','2013-04-20 11:00:00',4,57,5.412,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,5,'Spanish Grand Prix','Spain','Gran Premio de Espa&ntilde;a','es','Circuit de Catalunya','Barcelona','Europe/Madrid','2013-05-12 12:00:00','2013-05-11 12:00:00',5,66,4.655,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,6,'Monaco Grand Prix','Monaco','Grand Prix de Monaco','mc','Circuit de Monaco','Monte Carlo','Europe/Monaco','2013-05-26 12:00:00','2013-05-25 12:00:00',6,78,3.34,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,7,'Canadian Grand Prix','Canada','Grand Prix du Canada','ca','Circuit Gilles Villeneuve','Montreal','America/Montreal','2013-06-09 18:00:00','2013-06-08 17:00:00',7,70,4.361,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,8,'British Grand Prix','Great Britain','British Grand Prix','gb','Silverstone Circuit','Silverstone','Europe/London','2013-06-30 12:00:00','2013-06-29 12:00:00',8,52,5.891,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,9,'German Grand Prix','Germany','Gro&szlig;er Preis Santander von Deutschland','de','N&uuml;rburgring','N&uuml;rburg','Europe/Berlin','2013-07-07 12:00:00','2013-07-06 12:00:00',9,60,5.148,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,10,'Hungarian Grand Prix','Hungary','Magyar Nagyd&iacute;j','hu','Hungaroring','Budapest','Europe/Budapest','2013-07-28 12:00:00','2013-07-27 12:00:00',10,70,4.381,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,11,'Belgian Grand Prix','Belgium','Shell Belgian Grand Prix','be','Circuit de Spa-Francorchamps','Spa','Europe/Brussels','2013-08-25 12:00:00','2013-08-24 12:00:00',11,44,7.004,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,12,'Italian Grand Prix','Italy','Gran Premio d&#39;Italia','it','Autodromo Nazionale Monza','Monza','Europe/Rome','2013-09-08 12:00:00','2013-09-07 12:00:00',12,53,5.793,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,13,'Singapore Grand Prix','Singapore','Singapore Grand Prix','sg','Marina Bay Street Circuit','Singapore','Asia/Singapore','2013-09-22 12:00:00','2013-09-21 13:00:00',13,61,5.073,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,14,'Korean Grand Prix','Korea','Korean Grand Prix','kr','Korean International Circuit','Yeongam','Asia/Seoul','2013-10-06 06:00:00','2013-10-05 05:00:00',14,55,5.615,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,15,'Japanese Grand Prix','Japan','Japanese Grand Prix','jp','Suzuka Circuit','Suzuka','Asia/Tokyo','2013-10-13 06:00:00','2013-10-12 05:00:00',15,53,5.807,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,16,'Indian Grand Prix','India','Airtel Indian Grand Prix','in','Buddh International Circuit','Greater Noida','Asia/Kolkata','2013-10-27 09:30:00','2013-10-26 08:30:00',16,60,5.125,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,17,'Abu Dhabi Grand Prix','Abu Dhabi','Etihad Airways Abu Dhabi Grand Prix','ae','Yas Marina Circuit','Yas Island','Asia/Dubai','2013-11-03 13:00:00','2013-11-02 13:00:00',17,55,5.554,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,18,'United States Grand Prix','United States','United States Grand Prix','us','Circuit of the Americas','Austin, Texas','America/Chicago','2013-11-17 19:00:00','2013-11-16 18:00:00',18,56,5.513,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,19,'Brazilian Grand Prix','Brazil','Grande Pr&ecirc;mio do Brasil','br','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','America/Sao_Paulo','2013-11-24 16:00:00','2013-11-23 16:00:00',19,71,4.309,0,NULL,NULL,NULL,NULL);

#
# The teams
#
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,1,'McLaren','gb','Vodafone McLaren Mercedes','Woking','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,2,'Ferrari','it','Scuderia Ferrari','Maranello','it');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,3,'Sauber','ch','Sauber F1 Team','Hinwil','ch');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,4,'Lotus','gb','Lotus F1 Team','Enstone','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,6,'Toro Rosso','it','Scuderia Toro Rosso','Faenza','it');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,7,'Red Bull','at','Infiniti Red Bull Racing','Milton Keynes','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,8,'Williams','gb','Williams F1 Team','Grove','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,9,'Force India','in','Sahara Force India Formula One','Silverstone','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,10,'Mercedes','de','Mercedes AMG Petronas F1 Team','Brackley','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,15,'Caterham','my','Caterham F1 Team','Leafield','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,17,'Marussia','ru','Marussia F1 Team','Banbury','gb');

#
# Drivers
#
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 1,  15, 7,  'VET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 2,  14, 7,  'WEB' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 3,  7,  2,  'ALO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 4,  3,  2,  'MAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 5,  22, 1,  'BUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 6,  43, 1,  'PER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 7,  4,  4,  'RAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 8,  26, 4,  'GRO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 9,  16, 10, 'ROS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 10, 1,  10, 'HAM' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 11, 31, 3,  'HUL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 12, 49, 3,  'GUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 14, 42, 9,  'DiR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 15, 20, 9,  'SUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 16, 41, 8,  'MAL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 17, 50, 8,  'BOT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 18, 48, 6,  'VER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 19, 46, 6,  'RIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 20, 47, 15, 'PIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 21, 51, 15, 'vdG' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 22, 52, 17, 'BIA' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 23, 53, 17, 'CHI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

#
# Default standings
#
INSERT INTO `SPORTS_F1_DRIVER_HISTORY` (`season`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER`.`driver_id`) + 1, 0
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`
    AND `OTHER`.`car_no` < `ME`.`car_no`)
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_F1_TEAM_HISTORY` (`season`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND `OTHER`.`car_no` < `ME`.`car_no`)
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`team_id`;

