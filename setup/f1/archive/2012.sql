SET @season := 2012;

#
# Remove any previous setup
#
DELETE FROM `SPORTS_F1_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAMS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_DRIVERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_DRIVER_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_GRID` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_QUALI` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_RESULT` WHERE `season` = @season;

#
# Race calendar
#
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,1,'Australian','Australia','au','Australian Grand Prix','Melbourne Grand Prix Circuit','Melbourne','2012-03-18 06:00:00','2012-03-17 06:00:00',1,58,5.303,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,2,'Malaysian','Malaysia','my','Petronas Malaysian Grand Prix','Sepang International Circuit','Kuala Lumpur','2012-03-25 08:00:00','2012-03-24 08:00:00',2,56,5.543,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,3,'Chinese','China','cn','UBS Chinese Grand Prix','Shanghai International Circuit','Shanghai','2012-04-15 07:00:00','2012-04-14 06:00:00',3,56,5.451,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,4,'Bahrain','Bahrain','bh','Gulf Air Bahrain Grand Prix','Bahrain International Circuit','Sakhir','2012-04-22 12:00:00','2012-04-21 11:00:00',4,57,5.412,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,5,'Spanish','Spain','es','Gran Premio de Espa&ntilde;a Santander','Circuit de Catalunya','Barcelona','2012-05-13 12:00:00','2012-05-12 12:00:00',5,66,4.655,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,6,'Monaco','Monaco','mc','Grand Prix de Monaco','Circuit de Monaco','Monte Carlo','2012-05-27 12:00:00','2012-05-26 12:00:00',6,78,3.34,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,7,'Canadian','Canada','ca','Grand Prix du Canada','Circuit Gilles Villeneuve','Montreal','2012-06-10 18:00:00','2012-06-09 17:00:00',7,70,4.361,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,8,'European','Europe','eu','Grand Prix of Europe','Valencia Street Circuit','Valencia','2012-06-24 12:00:00','2012-06-23 12:00:00',8,57,5.419,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,9,'British','Great Britain','gb','Santander British Grand Prix','Silverstone Circuit','Silverstone','2012-07-08 12:00:00','2012-07-07 12:00:00',9,52,5.891,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,10,'German','Germany','de','Gro&szlig;er Preis Santander von Deutschland','Hockenheimring','Hockenheim','2012-07-22 12:00:00','2012-07-21 12:00:00',10,67,4.574,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,11,'Hungarian','Hungary','hu','Eni Magyar Nagyd&iacute;j','Hungaroring','Budapest','2012-07-29 12:00:00','2012-07-28 12:00:00',11,70,4.381,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,12,'Belgian','Belgium','be','Shell Belgian Grand Prix','Circuit de Spa-Francorchamps','Spa','2012-09-02 12:00:00','2012-09-01 12:00:00',12,44,7.004,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,13,'Italian','Italy','it','Gran Premio Santander d&#39;Italia','Autodromo Nazionale Monza','Monza','2012-09-09 12:00:00','2012-09-08 12:00:00',13,53,5.793,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,14,'Singapore','Singapore','sg','SingTel Singapore Grand Prix','Marina Bay Street Circuit','Singapore','2012-09-23 12:00:00','2012-09-22 12:00:00',14,61,5.073,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,15,'Japanese','Japan','jp','Japanese Grand Prix','Suzuka Circuit','Suzuka','2012-10-07 06:00:00','2012-10-06 05:00:00',15,53,5.807,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,16,'Korean','Korea','kr','Korean Grand Prix','Korean International Circuit','Yeongam','2012-10-14 06:00:00','2012-10-13 05:00:00',16,55,5.615,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,17,'Indian','India','in','Airtel Indian Grand Prix','Buddh International Circuit','Greater Noida','2012-10-28 09:30:00','2012-10-27 08:30:00',17,60,5.125,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,18,'Abu Dhabi','Abu Dhabi','ae','Etihad Airways Abu Dhabi Grand Prix','Yas Marina Circuit','Yas Island','2012-11-04 13:00:00','2012-11-03 13:00:00',18,55,5.554,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,19,'United States','United States','us','United States Grand Prix','Circuit of the Americas','Austin, Texas','2012-11-18 19:00:00','2012-11-17 19:00:00',19,55,5.516,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name`, `country`, `flag`, `official_name`, `circuit`, `location`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,20,'Brazilian','Brazil','br','Grande Pr&ecirc;mio do Brasil','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','2012-11-25 16:00:00','2012-11-24 16:00:00',20,71,4.309,0,NULL,NULL,NULL,NULL);

#
# The teams
#
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,1,'McLaren','gb','Vodafone McLaren Mercedes','Woking','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,2,'Ferrari','it','Scuderia Ferrari','Maranello','it');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,3,'Sauber','ch','Sauber F1 Team','Hinwil','ch');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,4,'Lotus','gb','Lotus F1 Team','Enstone','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,6,'Toro Rosso','it','Scuderia Toro Rosso','Faenza','it');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,7,'Red Bull','at','Red Bull Racing','Milton Keynes','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,8,'Williams','gb','AT&amp;T Williams','Grove','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,9,'Force India','in','Sahara Force India Formula One','Silverstone','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,10,'Mercedes','de','Mercedes AMG Petronas F1 Team','Brackley','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,15,'Caterham','my','Caterham F1 Team','Hingham','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,16,'Hispania','es','Hispania Racing F1 Team','Madrid','es');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,17,'Marussia','ru','Marussia F1 Team','Dinnington','gb');

#
# Drivers
#
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 1,  15, 7,  'VET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 2,  14, 7,  'WEB' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 3,  22, 1,  'BUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 4,  1,  1,  'HAM' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 5,  7,  2,  'ALO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 6,  3,  2,  'MAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 7,  34, 10, 'SCH' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 8,  16, 10, 'ROS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 9,  4,  4,  'RAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 10, 26, 4,  'GRO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 11, 42, 9,  'DiR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 12, 31, 9,  'HUL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 14, 28, 3,  'KOB' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 15, 43, 3,  'PER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 16, 46, 6,  'RIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 17, 48, 6,  'VER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 18, 41, 8,  'MAL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 19, 32, 8,  'SEN' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 20, 2,  15, 'KOV' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 21, 37, 15, 'PET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 22, 35, 16, 'DLR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 23, 44, 16, 'KAR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 24, 10, 17, 'GLO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 25, 47, 17, 'PIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

#
# Default standings
#
INSERT INTO `SPORTS_F1_DRIVER_HISTORY` (`season`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER`.`driver_id`) + 1, 0
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`
    AND `OTHER`.`car_no` < `ME`.`car_no`)
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_F1_TEAM_HISTORY` (`season`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND `OTHER`.`car_no` < `ME`.`car_no`)
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`team_id`;

