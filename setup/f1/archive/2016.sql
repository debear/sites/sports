SET @season := 2016;

#
# Remove any previous setup
#
DELETE FROM `SPORTS_F1_DRIVER_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_DRIVERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_GRID` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_LEADERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_QUALI` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_RESULT` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAMS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY_PROGRESS` WHERE `season` = @season;

#
# Race calendar
#
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`)
  VALUES (@season,1, 'Australian Grand Prix',   'Australia',    'Rolex Australian Grand Prix',            'au','Melbourne Grand Prix Circuit',            'Melbourne',       'Australia/Melbourne','2016-03-20 05:00:00','2016-03-19 06:00:00',1, 58,5.303,0,NULL,NULL,NULL,NULL),
         (@season,2, 'Bahrain Grand Prix',      'Bahrain',      'Bahrain Grand Prix',                     'bh','Bahrain International Circuit',           'Zallaq',          'Asia/Bahrain',       '2016-04-03 15:00:00','2016-04-02 15:00:00',2, 57,5.412,0,NULL,NULL,NULL,NULL),
         (@season,3, 'Chinese Grand Prix',      'China',        'Pirelli Chinese Grand Prix',             'cn','Shanghai International Circuit',          'Shanghai',        'Asia/Shanghai',      '2016-04-17 06:00:00','2016-04-16 07:00:00',3, 56,5.451,0,NULL,NULL,NULL,NULL),
         (@season,4, 'Russian Grand Prix',      'Russia',       'Russian Grand Prix',                     'ru','Sochi International Street Circuit',      'Sochi',           'Europe/Moscow',      '2016-05-01 12:00:00','2016-04-30 12:00:00',4, 53,5.853,0,NULL,NULL,NULL,NULL),
         (@season,5, 'Spanish Grand Prix',      'Spain',        'Gran Premio de Espa&ntilde;a Pirelli',   'es','Circuit de Catalunya',                    'Barcelona',       'Europe/Madrid',      '2016-05-15 12:00:00','2016-05-14 12:00:00',5, 66,4.655,0,NULL,NULL,NULL,NULL),
         (@season,6, 'Monaco Grand Prix',       'Monaco',       'Grand Prix de Monaco',                   'mc','Circuit de Monaco',                       'Monte Carlo',     'Europe/Monaco',      '2016-05-29 12:00:00','2016-05-28 12:00:00',6, 78,3.340,0,NULL,NULL,NULL,NULL),
         (@season,7, 'Canadian Grand Prix',     'Canada',       'Grand Prix du Canada',                   'ca','Circuit Gilles Villeneuve',               'Montreal',        'America/Montreal',   '2016-06-12 18:00:00','2016-06-11 17:00:00',7, 70,4.361,0,NULL,NULL,NULL,NULL),
         (@season,8, 'European Grand Prix',     'Europe',       'Grand Prix of Europe',                   'az','Baku City Circuit',                       'Baku',            'Asia/Baku',          '2016-06-19 13:00:00','2016-06-18 13:00:00',8, 51,6.006,0,NULL,NULL,NULL,NULL),
         (@season,9, 'Austrian Grand Prix',     'Austria',      'Gro&szlig;er Preis von &Ouml;sterreich', 'at','Red Bull Ring',                           'Spielberg',       'Europe/Vienna',      '2016-07-03 12:00:00','2016-07-02 12:00:00',9, 71,4.326,0,NULL,NULL,NULL,NULL),
         (@season,10,'British Grand Prix',      'Great Britain','British Grand Prix',                     'gb','Silverstone Circuit',                     'Silverstone',     'Europe/London',      '2016-07-10 12:00:00','2016-07-09 12:00:00',10,52,5.891,0,NULL,NULL,NULL,NULL),
         (@season,11,'Hungarian Grand Prix',    'Hungary',      'Magyar Nagyd&iacute;j',                  'hu','Hungaroring',                             'Budapest',        'Europe/Budapest',    '2016-07-24 12:00:00','2016-07-23 12:00:00',11,70,4.381,0,NULL,NULL,NULL,NULL),
         (@season,12,'German Grand Prix',       'Germany',      'Gro&szlig;er Preis von Deutschland',     'de','Hockenheimring',                          'Hockenheim',      'Europe/Berlin',      '2016-07-31 12:00:00','2016-07-30 12:00:00',12,67,4.574,0,NULL,NULL,NULL,NULL),
         (@season,13,'Belgian Grand Prix',      'Belgium',      'Belgian Grand Prix',                     'be','Circuit de Spa-Francorchamps',            'Spa',             'Europe/Brussels',    '2016-08-28 12:00:00','2016-08-27 12:00:00',13,44,7.004,0,NULL,NULL,NULL,NULL),
         (@season,14,'Italian Grand Prix',      'Italy',        'Gran Premio d&#39;Italia',               'it','Autodromo Nazionale Monza',               'Monza',           'Europe/Rome',        '2016-09-04 12:00:00','2016-09-03 12:00:00',14,53,5.793,0,NULL,NULL,NULL,NULL),
         (@season,15,'Singapore Grand Prix',    'Singapore',    'Singapore Airlines Singapore Grand Prix','sg','Marina Bay Street Circuit',               'Singapore',       'Asia/Singapore',     '2016-09-18 12:00:00','2016-09-17 13:00:00',15,61,5.073,0,NULL,NULL,NULL,NULL),
         (@season,16,'Malaysian Grand Prix',    'Malaysia',     'Malaysia Grand Prix',                    'my','Sepang International Circuit',            'Kuala Lumpur',    'Asia/Kuala_Lumpur',  '2016-10-02 07:00:00','2016-10-01 09:00:00',16,56,5.543,0,NULL,NULL,NULL,NULL),
         (@season,17,'Japanese Grand Prix',     'Japan',        'Japanese Grand Prix',                    'jp','Suzuka Circuit',                          'Suzuka',          'Asia/Tokyo',         '2016-10-09 05:00:00','2016-10-08 06:00:00',17,53,5.807,0,NULL,NULL,NULL,NULL),
         (@season,18,'United States Grand Prix','United States','United States Grand Prix',               'us','Circuit of the Americas',                 'Austin, Texas',   'America/Chicago',    '2016-10-23 19:00:00','2016-10-22 18:00:00',18,56,5.513,0,NULL,NULL,NULL,NULL),
         (@season,19,'Mexican Grand Prix',      'Mexico',       'Gran Premio de M&eacute;xico',           'mx','Autodromo Hermanos Rodriguez',            'Mexico City',     'America/Mexico_City','2016-10-30 19:00:00','2016-10-29 18:00:00',19,69,4.421,0,NULL,NULL,NULL,NULL),
         (@season,20,'Brazilian Grand Prix',    'Brazil',       'Grande Pr&ecirc;mio do Brasil',          'br','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','America/Sao_Paulo',  '2016-11-13 16:00:00','2016-11-12 16:00:00',20,71,4.309,0,NULL,NULL,NULL,NULL),
         (@season,21,'Abu Dhabi Grand Prix',    'Abu Dhabi',    'Etihad Airways Abu Dhabi Grand Prix',    'ae','Yas Marina Circuit',                      'Yas Island',      'Asia/Dubai',         '2016-11-27 13:00:00','2016-11-26 13:00:00',21,55,5.554,0,NULL,NULL,NULL,NULL);

#
# The teams
#
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`)
  VALUES (@season,1,'McLaren','gb','McLaren Honda','Woking','gb'),
         (@season,2,'Ferrari','it','Scuderia Ferrari','Maranello','it'),
         (@season,3,'Sauber','ch','Sauber F1 Team','Hinwil','ch'),
         (@season,4,'Renault','fr','Renault Sport F1 Team','Enstone','gb'),
         (@season,6,'Toro Rosso','it','Scuderia Toro Rosso','Faenza','it'),
         (@season,7,'Red Bull','at','Red Bull Racing','Milton Keynes','gb'),
         (@season,8,'Williams','gb','Williams Martini Racing','Grove','gb'),
         (@season,9,'Force India','in','Sahara Force India Formula One','Silverstone','gb'),
         (@season,10,'Mercedes','de','Mercedes AMG Petronas F1 Team','Brackley','gb'),
         (@season,17,'Manor','gb','Manor Racing','Banbury','gb'),
         (@season,18,'Haas','us','Haas F1 Team','Kannapolis','us');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 14, 7 , 1,  'ALO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 22, 22, 1,  'BUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 5, 15,  2,  'VET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 7,  4,  2,  'RAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Sauber
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 12, 60, 3,  'NAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 9,  56, 3,  'ERI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Renault
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 20, 54, 4, 'MAG' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 30, 64, 4, 'PAL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Toro Rosso
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 33, 61, 6,  'VES' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 55, 62, 6,  'SAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`,  3, 46, 7,  'RIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 26, 55, 7,  'KVY' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 19, 3,  8,  'MAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 77, 50, 8,  'BOT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Force India
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 27, 31, 9,  'HUL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 11, 43, 9,  'PER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 44, 1,  10, 'HAM' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`,  6, 16, 10, 'ROS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Manor
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 94, 65, 17, 'WEH' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 88, 66, 17, 'HAR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Haas
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 8,  26, 18, 'GRO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 21, 49, 18, 'GUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);


# New drivers
INSERT IGNORE INTO `SPORTS_F1_DRIVERS` VALUES
  (64, 'Jolyon', 'Palmer', 'Horsham', 'gb', 'gb', '1991-01-20'),
  (65, 'Pascal', 'Wehrlein', 'Sigmaringen', 'de', 'de', '1994-10-18'),
  (66, 'Rio', 'Haryanto', 'Surakarta', 'id', 'id', '1993-01-22');

#
# Default standings
#
INSERT INTO `SPORTS_F1_DRIVER_HISTORY` (`season`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_F1_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_F1_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_F1_TEAM_HISTORY` (`season`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_F1_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_F1_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`team_id`;

