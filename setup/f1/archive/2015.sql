SET @season := 2015;

#
# Remove any previous setup
#
DELETE FROM `SPORTS_F1_DRIVER_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_DRIVERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_GRID` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_LEADERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_QUALI` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_RESULT` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAMS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY_PROGRESS` WHERE `season` = @season;

#
# Race calendar
#
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`)
  VALUES (@season,1,'Australian Grand Prix','Australia','Rolex Australian Grand Prix','au','Melbourne Grand Prix Circuit','Melbourne','Australia/Melbourne','2015-03-15 05:00:00','2015-03-14 06:00:00',1,58,5.303,0,NULL,NULL,NULL,NULL),
         (@season,2,'Malaysian Grand Prix','Malaysia','Petronas Malaysia Grand Prix','my','Sepang International Circuit','Kuala Lumpur','Asia/Kuala_Lumpur','2015-03-29 07:00:00','2015-03-28 09:00:00',2,56,5.543,0,NULL,NULL,NULL,NULL),
         (@season,3,'Chinese Grand Prix','China','Chinese Grand Prix','cn','Shanghai International Circuit','Shanghai','Asia/Shanghai','2015-04-12 06:00:00','2015-04-11 07:00:00',3,56,5.451,0,NULL,NULL,NULL,NULL),
         (@season,4,'Bahrain Grand Prix','Bahrain','Gulf Air Bahrain Grand Prix','bh','Bahrain International Circuit','Zallaq','Asia/Bahrain','2015-04-19 15:00:00','2015-04-18 15:00:00',4,57,5.412,0,NULL,NULL,NULL,NULL),
         (@season,5,'Spanish Grand Prix','Spain','Gran Premio de Espa&ntilde;a','es','Circuit de Catalunya','Barcelona','Europe/Madrid','2015-05-10 12:00:00','2015-05-09 12:00:00',5,66,4.655,0,NULL,NULL,NULL,NULL),
         (@season,6,'Monaco Grand Prix','Monaco','Grand Prix de Monaco','mc','Circuit de Monaco','Monte Carlo','Europe/Monaco','2015-05-24 12:00:00','2015-05-23 12:00:00',6,78,3.34,0,NULL,NULL,NULL,NULL),
         (@season,7,'Canadian Grand Prix','Canada','Grand Prix du Canada','ca','Circuit Gilles Villeneuve','Montreal','America/Montreal','2015-06-07 18:00:00','2015-06-06 17:00:00',7,70,4.361,0,NULL,NULL,NULL,NULL),
         (@season,8,'Austrian Grand Prix','Austria','Gro&szlig;er Preis von &Ouml;sterreich','at','Red Bull Ring','Spielberg','Europe/Vienna','2015-06-21 12:00:00','2015-06-20 12:00:00',8,71,4.326,0,NULL,NULL,NULL,NULL),
         (@season,9,'British Grand Prix','Great Britain','British Grand Prix','gb','Silverstone Circuit','Silverstone','Europe/London','2015-07-05 12:00:00','2015-07-04 12:00:00',9,52,5.891,0,NULL,NULL,NULL,NULL),
         (@season,10,'German Grand Prix','Germany','Gro&szlig;er Preis von Deutschland','de','Hockenheimring','Hockenheim','Europe/Berlin','2015-07-19 12:00:00','2015-07-18 12:00:00',10,67,4.574,0,NULL,NULL,NULL,NULL),
         (@season,11,'Hungarian Grand Prix','Hungary','Magyar Nagyd&iacute;j','hu','Hungaroring','Budapest','Europe/Budapest','2015-07-26 12:00:00','2015-07-25 12:00:00',11,70,4.381,0,NULL,NULL,NULL,NULL),
         (@season,12,'Belgian Grand Prix','Belgium','Shell Belgian Grand Prix','be','Circuit de Spa-Francorchamps','Spa','Europe/Brussels','2015-08-23 12:00:00','2015-08-22 12:00:00',12,44,7.004,0,NULL,NULL,NULL,NULL),
         (@season,13,'Italian Grand Prix','Italy','Gran Premio d&#39;Italia','it','Autodromo Nazionale Monza','Monza','Europe/Rome','2015-09-06 12:00:00','2015-09-05 12:00:00',13,53,5.793,0,NULL,NULL,NULL,NULL),
         (@season,14,'Singapore Grand Prix','Singapore','Singapore Grand Prix','sg','Marina Bay Street Circuit','Singapore','Asia/Singapore','2015-09-20 12:00:00','2015-09-19 13:00:00',14,61,5.073,0,NULL,NULL,NULL,NULL),
         (@season,15,'Japanese Grand Prix','Japan','Japanese Grand Prix','jp','Suzuka Circuit','Suzuka','Asia/Tokyo','2015-09-27 05:00:00','2015-09-26 06:00:00',15,53,5.807,0,NULL,NULL,NULL,NULL),
         (@season,16,'Russian Grand Prix','Russia','Russian Grand Prix','ru','Sochi International Street Circuit','Sochi','Europe/Moscow','2015-10-11 11:00:00','2015-10-10 12:00:00',16,53,5.853,0,NULL,NULL,NULL,NULL),
         (@season,17,'United States Grand Prix','United States','United States Grand Prix','us','Circuit of the Americas','Austin, Texas','America/Chicago','2015-10-25 19:00:00','2015-10-24 18:00:00',17,56,5.513,0,NULL,NULL,NULL,NULL),
         (@season,18,'Mexican Grand Prix','Mexico','Gran Premio de M&eacute;xico','mx','Autodromo Hermanos Rodriguez','Mexico City','America/Mexico_City','2015-11-01 19:00:00','2015-10-31 19:00:00',18,69,4.421,0,NULL,NULL,NULL,NULL),
         (@season,19,'Brazilian Grand Prix','Brazil','Grande Pr&ecirc;mio do Brasil','br','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','America/Sao_Paulo','2015-11-15 16:00:00','2015-11-14 16:00:00',19,71,4.309,0,NULL,NULL,NULL,NULL),
         (@season,20,'Abu Dhabi Grand Prix','Abu Dhabi','Etihad Airways Abu Dhabi Grand Prix','ae','Yas Marina Circuit','Yas Island','Asia/Dubai','2015-11-29 13:00:00','2015-11-28 13:00:00',20,55,5.554,0,NULL,NULL,NULL,NULL);

#
# The teams
#
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`)
  VALUES (@season,1,'McLaren','gb','McLaren Mercedes','Woking','gb'),
         (@season,2,'Ferrari','it','Scuderia Ferrari','Maranello','it'),
         (@season,3,'Sauber','ch','Sauber F1 Team','Hinwil','ch'),
         (@season,4,'Lotus','gb','Lotus F1 Team','Enstone','gb'),
         (@season,6,'Toro Rosso','it','Scuderia Toro Rosso','Faenza','it'),
         (@season,7,'Red Bull','at','Infiniti Red Bull Racing','Milton Keynes','gb'),
         (@season,8,'Williams','gb','Williams F1 Team','Grove','gb'),
         (@season,9,'Force India','in','Sahara Force India Formula One','Silverstone','gb'),
         (@season,10,'Mercedes','de','Mercedes AMG Petronas F1 Team','Brackley','gb'),
         (@season,17,'Manor','gb','Manor Marussia F1 Team','Dinnington','gb');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 14, 7 , 1,  'ALO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 22, 22, 1,  'BUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 5, 15,  2,  'VET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 7,  4,  2,  'RAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Sauber
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 12, 60, 3,  'NAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 9,  56, 3,  'ERI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Lotus
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 8,  26, 4,  'GRO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 13, 41, 4,  'MAL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Toro Rosso
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 33, 61, 6,  'VER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 55, 62, 6,  'SAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`,  3, 46, 7,  'RIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 26, 55, 7,  'KVY' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 19, 3,  8,  'MAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 77, 50, 8,  'BOT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Force India
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 27, 31, 9,  'HUL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 11, 43, 9,  'PER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 44, 1,  10, 'HAM' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`,  6, 16, 10, 'ROS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Manor
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 28, 59, 17, 'STE' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 98, 63, 17, 'MER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season AND `round_order` = 1 ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# New drivers
INSERT IGNORE INTO `SPORTS_F1_DRIVERS` VALUES
  (60, 'Felipe', 'Nasr', 'Bras&iacute;lia', 'br', 'br', '1992-08-21'),
  (61, 'Max', 'Verstappen', 'Hasselt', 'be', 'nl', '1997-09-30'),
  (62, 'Carlos', 'Sainz Jr', 'Madrid', 'es', 'es', '1994-09-01');

#
# Default standings
#
INSERT INTO `SPORTS_F1_DRIVER_HISTORY` (`season`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_F1_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_F1_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_F1_TEAM_HISTORY` (`season`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_F1_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_F1_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`team_id`;

