SET @season := 2022;
SET @series := 'f1';

#
# Remove any previous setup
#
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACES` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_DRIVERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_FASTEST_LAP` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_GRID` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_LEADERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_QUALI` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_RESULT` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAMS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;

#
# Race calendar
#
INSERT INTO `SPORTS_FIA_RACES` (`season`, `series`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `quirks`, `race_time`, `race2_time`, `qual_time`, `round_order`, `race_laps_total`, `race2_laps_total`, `lap_length`) VALUES
  (@season,@series,1,  'Bahrain Grand Prix',         'Bahrain',         'Gulf Air Bahrain Grand Prix',                     'bh',    'Bahrain International Circuit',               'Zallaq',          'Asia/Bahrain',       NULL,         '2022-03-20 15:00:00',NULL,                 '2022-03-19 15:00:00', 1,57, 0,5.412),
  (@season,@series,2,  'Saudi Arabian Grand Prix',   'Saudi Arabia',    'STC Saudi Arabian Grand Prix',                    'sa',    'Jeddah Street Circuit',                       'Jeddah',          'Asia/Dubai',         NULL,         '2022-03-27 17:00:00',NULL,                 '2022-03-26 17:00:00', 2,99, 0,1.000),
  (@season,@series,3,  'Australian Grand Prix',      'Australia',       'Heineken Australian Grand Prix',                  'au',    'Melbourne Grand Prix Circuit',                'Melbourne',       'Australia/Melbourne',NULL,         '2022-04-10 05:00:00',NULL,                 '2022-04-09 06:00:00', 3,58, 0,5.303),
  (@season,@series,4,  'Emilia Romagna Grand Prix',  'Emilia Romagna',  'Rolex Gran Premio Dell&#39;emilia Romagna',       'it-emr','Autodromo Internazionale Enzo e Dino Ferrari','Imola',           'Europe/Rome',        'SPRINT_RACE','2022-04-23 14:00:00','2022-04-24 13:00:00','2022-04-22 17:00:00', 4,21,63,4.909),
  (@season,@series,5,  'Miami Grand Prix',           'Miami',           'Miami Grand Prix',                                'us-mia','Miami International Autodrome',               'Miami',           'America/New_York',   NULL,         '2022-05-08 19:30:00',NULL,                 '2022-05-07 20:00:00', 5,57, 0,5.410),
  (@season,@series,6,  'Spanish Grand Prix',         'Spain',           'Pirelli Gran Premio de Espa&ntilde;a',            'es',    'Circuit de Catalunya',                        'Barcelona',       'Europe/Madrid',      NULL,         '2022-05-22 13:00:00',NULL,                 '2022-05-21 14:00:00', 6,66, 0,4.655),
  (@season,@series,7,  'Monaco Grand Prix',          'Monaco',          'Grand Prix de Monaco',                            'mc',    'Circuit de Monaco',                           'Monte Carlo',     'Europe/Monaco',      NULL,         '2022-05-29 13:00:00',NULL,                 '2022-05-28 14:00:00', 7,78, 0,3.337),
  (@season,@series,8,  'Azerbaijan Grand Prix',      'Azerbaijan',      'Azerbaijan Grand Prix',                           'az',    'Baku City Circuit',                           'Baku',            'Asia/Baku',          NULL,         '2022-06-12 11:00:00',NULL,                 '2022-06-11 14:00:00', 8,51, 0,6.003),
  (@season,@series,9,  'Canadian Grand Prix',        'Canada',          'Grand Prix du Canada',                            'ca',    'Circuit Gilles Villeneuve',                   'Montreal',        'America/Montreal',   NULL,         '2022-06-19 18:00:00',NULL,                 '2022-06-18 20:00:00', 9,70, 0,4.361),
  (@season,@series,10, 'British Grand Prix',         'Great Britain',   'British Grand Prix',                              'gb',    'Silverstone Circuit',                         'Silverstone',     'Europe/London',      NULL,         '2022-07-03 14:00:00',NULL,                 '2022-07-02 14:00:00',10,52, 0,5.891),
  (@season,@series,11, 'Austrian Grand Prix',        'Austria',         'Grosser Preis von &Ouml;sterreich',               'at',    'Red Bull Ring',                               'Spielberg',       'Europe/Vienna',      'SPRINT_RACE','2022-07-09 14:00:00','2022-07-10 13:00:00','2022-07-08 17:00:00',11,24,71,4.326),
  (@season,@series,12, 'French Grand Prix',          'France',          'Grand Prix de France',                            'fr',    'Circuit Paul Ricard',                         'Le Castellet',    'Europe/Paris',       NULL,         '2022-07-24 13:00:00',NULL,                 '2022-07-23 14:00:00',12,53, 0,5.861),
  (@season,@series,13, 'Hungarian Grand Prix',       'Hungary',         'Magyar Nagyd&iacute;j',                           'hu',    'Hungaroring',                                 'Budapest',        'Europe/Budapest',    NULL,         '2022-07-31 13:00:00',NULL,                 '2022-07-30 14:00:00',13,70, 0,4.381),
  (@season,@series,14, 'Belgian Grand Prix',         'Belgium',         'Rolex Belgian Grand Prix',                        'be',    'Circuit de Spa-Francorchamps',                'Spa',             'Europe/Brussels',    NULL,         '2022-08-28 13:00:00',NULL,                 '2022-08-27 14:00:00',14,44, 0,7.004),
  (@season,@series,15, 'Dutch Grand Prix',           'Netherlands',     'Heineken Dutch Grand Prix',                       'nl',    'Circuit Zandvoort',                           'Zandvoort',       'Europe/Amsterdam',   NULL,         '2022-09-04 13:00:00',NULL,                 '2022-09-03 14:00:00',15,72, 0,4.252),
  (@season,@series,16, 'Italian Grand Prix',         'Italy',           'Pirelli Gran Premio d&#39;Italia',                'it',    'Autodromo Nazionale Monza',                   'Monza',           'Europe/Rome',        NULL,         '2022-09-11 13:00:00',NULL,                 '2022-09-10 14:00:00',16,53, 0,5.793),
  (@season,@series,17, 'Russian Grand Prix',         'Russia',          'VTB Russian Grand Prix',                          'ru',    'Sochi International Street Circuit',          'Sochi',           'Europe/Moscow',      NULL,         '2022-09-25 11:00:00',NULL,                 '2022-09-24 13:00:00',17,53, 0,5.848),
  (@season,@series,18, 'Singapore Grand Prix',       'Singapore',       'Singapore Grand Prix',                            'sg',    'Marina Bay Street Circuit',                   'Singapore',       'Asia/Singapore',     NULL,         '2022-10-02 12:00:00',NULL,                 '2022-10-01 13:00:00',18,61, 0,5.065),
  (@season,@series,19, 'Japanese Grand Prix',        'Japan',           'Japanese Grand Prix',                             'jp',    'Suzuka Circuit',                              'Suzuka',          'Asia/Tokyo',         NULL,         '2022-10-09 05:00:00',NULL,                 '2022-10-08 07:00:00',19,53, 0,5.807),
  (@season,@series,20, 'United States Grand Prix',   'United States',   'Aramco United States Grand Prix',                 'us',    'Circuit of the Americas',                     'Austin, Texas',   'America/Chicago',    NULL,         '2022-10-23 19:00:00',NULL,                 '2022-10-22 22:00:00',20,56, 0,5.513),
  (@season,@series,21, 'Mexico City Grand Prix',     'Mexico City',     'Gran Premio de la Ciudad de M&eacute;xico',       'mx',    'Autodromo Hermanos Rodriguez',                'Mexico City',     'America/Mexico_City',NULL,         '2022-10-30 19:00:00',NULL,                 '2022-10-29 20:00:00',21,71, 0,4.304),
  (@season,@series,22, 'S&atilde;o Paulo Grand Prix','S&atilde;o Paulo','Heineken Grande Pr&ecirc;mio de S&atilde;o Paulo','br',    'Aut&oacute;dromo Jos&eacute; Carlos Pace',    'S&atilde;o Paulo','America/Sao_Paulo',  'SPRINT_RACE','2022-11-12 19:00:00','2022-11-13 18:00:00','2022-11-11 16:00:00',22,24,71,4.309),
  (@season,@series,23, 'Abu Dhabi Grand Prix',       'Abu Dhabi',       'Etihad Airways Abu Dhabi Grand Prix',             'ae',    'Yas Marina Circuit',                          'Yas Island',      'Asia/Dubai',         NULL,         '2022-11-20 13:00:00',NULL,                 '2022-11-19 14:00:00',23,55, 0,5.554);

#
# The teams
#
INSERT INTO `SPORTS_FIA_TEAMS` (`season`, `series`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES
  (@season,@series,1,  'McLaren',     'gb','McLaren F1 Team',                       'Woking',       'gb'),
  (@season,@series,2,  'Ferrari',     'it','Scuderia Ferrari',                      'Maranello',    'it'),
  (@season,@series,3,  'Alfa Romeo',  'it','Alfa Romeo Racing Orlen',               'Hinwil',       'ch'),
  (@season,@series,4,  'Alpine',      'fr','Alpine F1 Team',                        'Enstone',      'gb'),
  (@season,@series,6,  'AlphaTauri',  'it','Scuderia AlphaTauri',                   'Faenza',       'it'),
  (@season,@series,7,  'Red Bull',    'at','Oracle Red Bull Racing',                'Milton Keynes','gb'),
  (@season,@series,8,  'Williams',    'gb','Williams Racing',                       'Grove',        'gb'),
  (@season,@series,10, 'Mercedes',    'de','Mercedes-AMG Petronas Formula One Team','Brackley',     'gb'),
  (@season,@series,18, 'Haas',        'us','Uralkali Haas F1 Team',                 'Kannapolis',   'us'),
  (@season,@series,20, 'Aston Martin','gb','Aston Martin Aramco Cognizant F1 Team', 'Silverstone',  'gb');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  3, 46,  1, 'RIC' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  4, 75,  1, 'NOR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 16, 73,  2, 'LEC' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 55, 62,  2, 'SAI' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Alfa Romeo
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 24, 84,  3, 'ZHO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 77, 50,  3, 'BOT' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Alpine
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 14,  7,  4, 'ALO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 31, 68,  4, 'OCO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# AlphaTauri
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 10, 71,  6, 'GAS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 22, 82,  6, 'TSU' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 11, 43,  7, 'PER' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  1, 61,  7, 'VES' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  6, 78,  8, 'LAT' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 23, 76,  8, 'ALB' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 44,  1, 10, 'HAM' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 63, 77, 10, 'RUS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Haas
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  9, 81, 18, 'MAZ' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 47, 83, 18, 'SCH' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Aston Martin
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  5, 15, 20, 'VET' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 18, 69, 20, 'STR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# New drivers
INSERT IGNORE INTO `SPORTS_FIA_DRIVERS` VALUES
  (84, 'Guanyu', 'Zhou', 'Shanghai', 'cn', 'cn', '1999-05-30');

#
# Default standings
#
INSERT INTO `SPORTS_FIA_DRIVER_HISTORY` (`season`, `series`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_FIA_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_FIA_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_FIA_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_FIA_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_FIA_TEAM_HISTORY` (`season`, `series`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_FIA_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_FIA_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  GROUP BY `ME`.`team_id`;
