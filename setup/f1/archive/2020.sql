SET @season := 2020;
SET @series := 'f1';

#
# Remove any previous setup
#
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACES` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_DRIVERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_FASTEST_LAP` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_GRID` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_LEADERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_QUALI` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_RESULT` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAMS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;

#
# Race calendar
#
INSERT INTO `SPORTS_FIA_RACES` (`season`, `series`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `race_laps_total`, `lap_length`) VALUES
  (@season,@series,1, 'Australian Grand Prix',   'Australia',    'Rolex Australian Grand Prix',               'au','Melbourne Grand Prix Circuit',            'Melbourne',       'Australia/Melbourne','2020-03-15 05:10:00','2020-03-14 06:00:00',1, 58,5.303),
  (@season,@series,2, 'Bahrain Grand Prix',      'Bahrain',      'Gulf Air Bahrain Grand Prix',               'bh','Bahrain International Circuit',           'Zallaq',          'Asia/Bahrain',       '2020-03-22 15:10:00','2020-03-21 15:00:00',2, 57,5.412),
  (@season,@series,3, 'Vietnamese Grand Prix',   'Vietnam',      'VinFast Vietnam Grand Prix' ,               'vn','Hanoi Street Circuit',                    'Hanoi',           'Asia/Ho_Chi_Minh',   '2020-04-05 07:10:00','2020-04-04 08:00:00',3, 55,5.607),
  (@season,@series,4, 'Chinese Grand Prix',      'China',        'Heineken Chinese Grand Prix',               'cn','Shanghai International Circuit',          'Shanghai',        'Asia/Shanghai',      '2020-04-19 06:10:00','2020-04-18 07:00:00',4, 56,5.451),
  (@season,@series,5, 'Dutch Grand Prix',        'Netherlands',  'Heineken Dutch Grand Prix',                 'nl','Circuit Zandvoort',                       'Zandvoort',       'Europe/Amsterdam',   '2020-05-03 13:10:00','2020-05-02 13:00:00',5, 72,4.252),
  (@season,@series,6, 'Spanish Grand Prix',      'Spain',        'Gran Premio de Espa&ntilde;a',              'es','Circuit de Catalunya',                    'Barcelona',       'Europe/Madrid',      '2020-05-10 13:10:00','2020-05-09 13:00:00',6, 66,4.655),
  (@season,@series,7, 'Monaco Grand Prix',       'Monaco',       'Grand Prix de Monaco',                      'mc','Circuit de Monaco',                       'Monte Carlo',     'Europe/Monaco',      '2020-05-24 13:10:00','2020-05-23 13:00:00',7, 78,3.337),
  (@season,@series,8, 'Azerbaijan Grand Prix',   'Azerbaijan',   'Azerbaijan Grand Prix',                     'az','Baku City Circuit',                       'Baku',            'Asia/Baku',          '2020-06-07 12:10:00','2020-06-06 13:00:00',8, 51,6.003),
  (@season,@series,9, 'Canadian Grand Prix',     'Canada',       'Grand Prix du Canada',                      'ca','Circuit Gilles Villeneuve',               'Montreal',        'America/Montreal',   '2020-06-14 18:10:00','2020-06-13 18:00:00',9, 70,4.361),
  (@season,@series,10,'French Grand Prix',       'France',       'Grand Prix de France',                      'fr','Circuit Paul Ricard',                     'Le Castellet',    'Europe/Paris',       '2020-06-28 13:10:00','2020-06-27 13:00:00',10,53,5.861),
  (@season,@series,11,'Austrian Grand Prix',     'Austria',      'myWorld Preis von &Ouml;sterreich',         'at','Red Bull Ring',                           'Spielberg',       'Europe/Vienna',      '2020-07-05 13:10:00','2020-07-04 13:00:00',11,71,4.326),
  (@season,@series,12,'British Grand Prix',      'Great Britain','Pirelli British Grand Prix',                'gb','Silverstone Circuit',                     'Silverstone',     'Europe/London',      '2020-07-19 15:10:00','2020-07-18 15:00:00',12,52,5.891),
  (@season,@series,13,'Hungarian Grand Prix',    'Hungary',      'Magyar Nagyd&iacute;j',                     'hu','Hungaroring',                             'Budapest',        'Europe/Budapest',    '2020-08-02 13:10:00','2020-08-01 13:00:00',13,70,4.381),
  (@season,@series,14,'Belgian Grand Prix',      'Belgium',      'Rolex Belgian Grand Prix',                  'be','Circuit de Spa-Francorchamps',            'Spa',             'Europe/Brussels',    '2020-08-30 13:10:00','2020-08-29 13:00:00',14,44,7.004),
  (@season,@series,15,'Italian Grand Prix',      'Italy',        'Gran Premio Heineken d&#39;Italia',         'it','Autodromo Nazionale Monza',               'Monza',           'Europe/Rome',        '2020-09-06 13:10:00','2020-09-05 13:00:00',15,53,5.793),
  (@season,@series,16,'Singapore Grand Prix',    'Singapore',    'Singapore Airlines Singapore Grand Prix',   'sg','Marina Bay Street Circuit',               'Singapore',       'Asia/Singapore',     '2020-09-20 12:10:00','2020-09-19 13:00:00',16,61,5.065),
  (@season,@series,17,'Russian Grand Prix',      'Russia',       'VTB Russian Grand Prix',                    'ru','Sochi International Street Circuit',      'Sochi',           'Europe/Moscow',      '2020-09-27 11:10:00','2020-09-26 12:00:00',17,53,5.848),
  (@season,@series,18,'Japanese Grand Prix',     'Japan',        'Pirelli Japanese Grand Prix',               'jp','Suzuka Circuit',                          'Suzuka',          'Asia/Tokyo',         '2020-10-11 05:10:00','2020-10-10 06:00:00',18,53,5.807),
  (@season,@series,19,'United States Grand Prix','United States','United States Grand Prix',                  'us','Circuit of the Americas',                 'Austin, Texas',   'America/Chicago',    '2020-10-25 19:10:00','2020-10-24 21:00:00',19,56,5.513),
  (@season,@series,20,'Mexico City Grand Prix',  'Mexico City',  'Gran Premio de la Ciudad de M&eacute;xico', 'mx','Autodromo Hermanos Rodriguez',            'Mexico City',     'America/Mexico_City','2020-11-01 19:10:00','2020-10-31 19:00:00',20,71,4.304),
  (@season,@series,21,'Brazilian Grand Prix',    'Brazil',       'Grande Pr&ecirc;mio do Brasil',             'br','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','America/Sao_Paulo',  '2020-11-15 17:10:00','2020-11-14 18:00:00',21,71,4.309),
  (@season,@series,22,'Abu Dhabi Grand Prix',    'Abu Dhabi',    'Etihad Airways Abu Dhabi Grand Prix',       'ae','Yas Marina Circuit',                      'Yas Island',      'Asia/Dubai',         '2020-11-29 13:10:00','2020-11-28 13:00:00',22,55,5.554);

#
# The teams
#
INSERT INTO `SPORTS_FIA_TEAMS` (`season`, `series`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES
  (@season,@series,1, 'McLaren',     'gb','McLaren F1 Team',                  'Woking','gb'),
  (@season,@series,2, 'Ferrari',     'it','Scuderia Ferrari Mission Winnow',  'Maranello','it'),
  (@season,@series,3, 'Alfa Romeo',  'it','Alfa Romeo Racing Orlen',          'Hinwil','ch'),
  (@season,@series,4, 'Renault',     'fr','Renault F1 Team',                  'Enstone','gb'),
  (@season,@series,6, 'AlphaTauri',  'it','Scuderia AlphaTauri Honda',        'Faenza','it'),
  (@season,@series,7, 'Red Bull',    'at','Aston Martin Red Bull Racing',     'Milton Keynes','gb'),
  (@season,@series,8, 'Williams',    'gb','ROKiT Williams Racing',            'Grove','gb'),
  (@season,@series,10,'Mercedes',    'de','Mercedes AMG Petronas Motorsport', 'Brackley','gb'),
  (@season,@series,18,'Haas',        'us','Haas F1 Team',                     'Kannapolis','us'),
  (@season,@series,20,'Racing Point','gb','SportPesa Racing Point F1 Team',   'Silverstone','gb');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 4,  75, 1,  'NOR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 55, 62, 1,  'SAI' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 5, 15,  2,  'VET' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 16, 73, 2,  'LEC' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Alfa Romeo
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 7,  4,  3,  'RAI' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 99, 70, 3,  'GIO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Renault
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  3, 46, 4,  'RIC' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 31, 68, 4,  'OCO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# AlphaTauri
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 10, 71, 6,  'GAS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 26, 55, 6,  'KVY' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 23, 76, 7,  'ALB' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 33, 61, 7,  'VES' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 63, 77, 8,  'RUS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 6,  78, 8,  'LAT' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 44, 1,  10, 'HAM' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 77, 50, 10, 'BOT' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Haas
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 8,  26, 18, 'GRO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 20, 54, 18, 'MAG' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Racing Point
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 11, 43, 20, 'PER' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 18, 69, 20, 'STR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# New drivers
INSERT IGNORE INTO `SPORTS_FIA_DRIVERS` VALUES
  (78, 'Nicholas', 'Latifi', 'Montr&eacute;al', 'ca', 'ca', '1995-06-29');

#
# Default standings
#
INSERT INTO `SPORTS_FIA_DRIVER_HISTORY` (`season`, `series`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_FIA_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_FIA_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_FIA_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_FIA_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_FIA_TEAM_HISTORY` (`season`, `series`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_FIA_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_FIA_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  GROUP BY `ME`.`team_id`;
