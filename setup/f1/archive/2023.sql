SET @season := 2023;
SET @series := 'f1';

#
# Remove any previous setup
#
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACES` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_DRIVERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_FASTEST_LAP` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_GRID` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_LEADERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_QUALI` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_RESULT` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAMS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;

#
# Race calendar
#
INSERT INTO `SPORTS_FIA_RACES` (`season`, `series`, `round`, `name_full`, `name_short`, `flag`, `circuit`, `location`, `timezone`, `quirks`, `race_time`, `race2_time`, `qual_time`, `round_order`, `race_laps_total`, `race2_laps_total`, `lap_length`) VALUES
  (@season,@series,1,  'Bahrain Grand Prix',         'Bahrain',         'bh',    'Bahrain International Circuit',               'Zallaq',          'Asia/Bahrain',       NULL,                         '2023-03-05 15:00:00',NULL,                 '2023-03-04 15:00:00', 1,57, 0,5.412),
  (@season,@series,2,  'Saudi Arabian Grand Prix',   'Saudi Arabia',    'sa',    'Jeddah Street Circuit',                       'Jeddah',          'Asia/Dubai',         NULL,                         '2023-03-19 17:00:00',NULL,                 '2023-03-18 17:00:00', 2,99, 0,1.000),
  (@season,@series,3,  'Australian Grand Prix',      'Australia',       'au',    'Melbourne Grand Prix Circuit',                'Melbourne',       'Australia/Melbourne',NULL,                         '2023-04-02 05:00:00',NULL,                 '2023-04-01 05:00:00', 3,58, 0,5.303),
  (@season,@series,4,  'Azerbaijan Grand Prix',      'Azerbaijan',      'az',    'Baku City Circuit',                           'Baku',            'Asia/Baku',          'SPRINT_RACE,SPRINT_SHOOTOUT','2023-04-29 13:30:00','2023-04-30 11:00:00','2023-04-28 13:00:00', 4,17,51,6.003),
  (@season,@series,5,  'Miami Grand Prix',           'Miami',           'us-mia','Miami International Autodrome',               'Miami',           'America/New_York',   NULL,                         '2023-05-07 19:30:00',NULL,                 '2023-05-06 20:00:00', 5,57, 0,5.410),
  (@season,@series,6,  'Emilia Romagna Grand Prix',  'Emilia Romagna',  'it-emr','Autodromo Internazionale Enzo e Dino Ferrari','Imola',           'Europe/Rome',        NULL,                         '2023-05-21 13:00:00',NULL,                 '2023-05-20 14:00:00', 6,21,63,4.909),
  (@season,@series,7,  'Monaco Grand Prix',          'Monaco',          'mc',    'Circuit de Monaco',                           'Monte Carlo',     'Europe/Monaco',      NULL,                         '2023-05-28 13:00:00',NULL,                 '2023-05-27 14:00:00', 7,78, 0,3.337),
  (@season,@series,8,  'Spanish Grand Prix',         'Spain',           'es',    'Circuit de Catalunya',                        'Barcelona',       'Europe/Madrid',      NULL,                         '2023-06-04 13:00:00',NULL,                 '2023-06-03 14:00:00', 8,66, 0,4.655),
  (@season,@series,9,  'Canadian Grand Prix',        'Canada',          'ca',    'Circuit Gilles Villeneuve',                   'Montreal',        'America/Montreal',   NULL,                         '2023-06-18 18:00:00',NULL,                 '2023-06-17 20:00:00', 9,70, 0,4.361),
  (@season,@series,10, 'Austrian Grand Prix',        'Austria',         'at',    'Red Bull Ring',                               'Spielberg',       'Europe/Vienna',      'SPRINT_RACE,SPRINT_SHOOTOUT','2023-07-01 14:30:00','2023-07-02 13:00:00','2023-06-30 15:00:00',10,24,71,4.326),
  (@season,@series,11, 'British Grand Prix',         'Great Britain',   'gb',    'Silverstone Circuit',                         'Silverstone',     'Europe/London',      NULL,                         '2023-07-09 14:00:00',NULL,                 '2023-07-08 14:00:00',11,52, 0,5.891),
  (@season,@series,12, 'Hungarian Grand Prix',       'Hungary',         'hu',    'Hungaroring',                                 'Budapest',        'Europe/Budapest',    NULL,                         '2023-07-23 13:00:00',NULL,                 '2023-07-22 14:00:00',12,70, 0,4.381),
  (@season,@series,13, 'Belgian Grand Prix',         'Belgium',         'be',    'Circuit de Spa-Francorchamps',                'Spa',             'Europe/Brussels',    'SPRINT_RACE,SPRINT_SHOOTOUT','2023-07-29 14:30:00','2023-07-30 13:00:00','2023-07-28 15:00:00',13,15,44,7.004),
  (@season,@series,14, 'Dutch Grand Prix',           'Netherlands',     'nl',    'Circuit Zandvoort',                           'Zandvoort',       'Europe/Amsterdam',   NULL,                         '2023-08-27 13:00:00',NULL,                 '2023-08-26 13:00:00',14,72, 0,4.252),
  (@season,@series,15, 'Italian Grand Prix',         'Italy',           'it',    'Autodromo Nazionale Monza',                   'Monza',           'Europe/Rome',        NULL,                         '2023-09-03 13:00:00',NULL,                 '2023-09-02 14:00:00',15,53, 0,5.793),
  (@season,@series,16, 'Singapore Grand Prix',       'Singapore',       'sg',    'Marina Bay Street Circuit',                   'Singapore',       'Asia/Singapore',     NULL,                         '2023-09-17 12:00:00',NULL,                 '2023-09-16 13:00:00',16,61, 0,5.065),
  (@season,@series,17, 'Japanese Grand Prix',        'Japan',           'jp',    'Suzuka Circuit',                              'Suzuka',          'Asia/Tokyo',         NULL,                         '2023-09-24 05:00:00',NULL,                 '2023-09-23 06:00:00',17,53, 0,5.807),
  (@season,@series,18, 'Qatar Grand Prix',           'Qatar',           'qa',    'Losail International Circuit',                'Lusail',          'Asia/Qatar',         'SPRINT_RACE,SPRINT_SHOOTOUT','2023-10-07 14:30:00','2023-10-08 14:00:00','2023-10-06 14:00:00',18,19,57,5.380),
  (@season,@series,29, 'United States Grand Prix',   'United States',   'us',    'Circuit of the Americas',                     'Austin, Texas',   'America/Chicago',    'SPRINT_RACE,SPRINT_SHOOTOUT','2023-10-21 22:00:00','2023-10-22 19:00:00','2023-10-20 21:00:00',19,19,56,5.513),
  (@season,@series,20, 'Mexico City Grand Prix',     'Mexico City',     'mx',    'Autodromo Hermanos Rodriguez',                'Mexico City',     'America/Mexico_City',NULL,                         '2023-10-29 20:00:00',NULL,                 '2023-10-28 21:00:00',20,71, 0,4.304),
  (@season,@series,21, 'S&atilde;o Paulo Grand Prix','S&atilde;o Paulo','br',    'Aut&oacute;dromo Jos&eacute; Carlos Pace',    'S&atilde;o Paulo','America/Sao_Paulo',  'SPRINT_RACE,SPRINT_SHOOTOUT','2023-11-04 18:30:00','2022-11-05 17:00:00','2023-11-03 18:00:00',21,24,71,4.309),
  (@season,@series,22, 'Las Vegas Grand Prix',       'Las Vegas',       'us-veg','Las Vegas Street Circuit',                    'Las Vegas',       'America/Los_Angeles',NULL,                         '2023-11-19 06:00:00',NULL,                 '2023-11-18 08:00:00',22,50, 0,6.120),
  (@season,@series,23, 'Abu Dhabi Grand Prix',       'Abu Dhabi',       'ae',    'Yas Marina Circuit',                          'Yas Island',      'Asia/Dubai',         NULL,                         '2023-11-26 13:00:00',NULL,                 '2023-11-25 14:00:00',23,55, 0,5.554);

#
# The teams
#
INSERT INTO `SPORTS_FIA_TEAMS` (`season`, `series`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES
  (@season,@series,1,  'McLaren',     'gb','McLaren F1 Team',                       'Woking',       'gb'),
  (@season,@series,2,  'Ferrari',     'it','Scuderia Ferrari',                      'Maranello',    'it'),
  (@season,@series,3,  'Alfa Romeo',  'it','Alfa Romeo F1 Team Stake',              'Hinwil',       'ch'),
  (@season,@series,4,  'Alpine',      'fr','BWT Alpine F1 Team',                    'Enstone',      'gb'),
  (@season,@series,6,  'AlphaTauri',  'it','Scuderia AlphaTauri',                   'Faenza',       'it'),
  (@season,@series,7,  'Red Bull',    'at','Oracle Red Bull Racing',                'Milton Keynes','gb'),
  (@season,@series,8,  'Williams',    'gb','Williams Racing',                       'Grove',        'gb'),
  (@season,@series,10, 'Mercedes',    'de','Mercedes-AMG Petronas Formula One Team','Brackley',     'gb'),
  (@season,@series,18, 'Haas',        'us','MoneyGram Haas F1 Team',                'Kannapolis',   'us'),
  (@season,@series,20, 'Aston Martin','gb','Aston Martin Aramco Cognizant F1 Team', 'Silverstone',  'gb');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  4, 75,  1, 'NOR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 81, 86,  1, 'PIA' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 16, 73,  2, 'LEC' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 55, 62,  2, 'SAI' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Alfa Romeo
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 24, 84,  3, 'ZHO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 77, 50,  3, 'BOT' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Alpine
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 10, 71,  4, 'GAS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 31, 68,  4, 'OCO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# AlphaTauri
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 21, 85,  6, 'DEV' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 22, 82,  6, 'TSU' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 11, 43,  7, 'PER' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  1, 61,  7, 'VES' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  2, 87,  8, 'SAR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 23, 76,  8, 'ALB' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 44,  1, 10, 'HAM' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 63, 77, 10, 'RUS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Haas
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 20, 54, 18, 'MAG' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 27, 31, 18, 'HUL' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Aston Martin
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 14,  7, 20, 'ALO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 18, 69, 20, 'STR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# New drivers
INSERT IGNORE INTO `SPORTS_FIA_DRIVERS` VALUES
  (86, 'Oscar', 'Piastri', 'Melbourne', 'au', 'au', '2001-04-06'),
  (87, 'Logan', 'Sargeant', 'Fort Lauderdale', 'us', 'us', '2000-12-31');

#
# Default standings
#
INSERT INTO `SPORTS_FIA_DRIVER_HISTORY` (`season`, `series`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_FIA_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_FIA_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_FIA_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_FIA_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_FIA_TEAM_HISTORY` (`season`, `series`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_FIA_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_FIA_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  GROUP BY `ME`.`team_id`;
