SET @season := 2019;
SET @series := 'f1';

#
# Remove any previous setup
#
DELETE FROM `SPORTS_F1_DRIVER_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_RACES` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_RACE_DRIVERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_RACE_FASTEST_LAP` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_RACE_GRID` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_RACE_LEADERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_RACE_QUALI` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_RACE_RESULT` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_TEAMS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_TEAM_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_F1_TEAM_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;

#
# Race calendar
#
INSERT INTO `SPORTS_F1_RACES` (`season`, `series`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `race_laps_total`, `lap_length`, `completed_laps`) VALUES
  (@season,@series,1, 'Australian Grand Prix',   'Australia',    'Rolex Australian Grand Prix',            'au','Melbourne Grand Prix Circuit',            'Melbourne',       'Australia/Melbourne','2019-03-17 05:10:00','2019-03-16 06:00:00',1, 58,5.303,0),
  (@season,@series,2, 'Bahrain Grand Prix',      'Bahrain',      'Gulf Air Bahrain Grand Prix',            'bh','Bahrain International Circuit',           'Zallaq',          'Asia/Bahrain',       '2019-03-31 15:10:00','2019-03-30 15:00:00',2, 57,5.412,0),
  (@season,@series,3, 'Chinese Grand Prix',      'China',        'Heineken Chinese Grand Prix',            'cn','Shanghai International Circuit',          'Shanghai',        'Asia/Shanghai',      '2019-04-14 06:10:00','2019-04-13 06:00:00',3, 56,5.451,0),
  (@season,@series,4, 'Azerbaijan Grand Prix',   'Azerbaijan',   'Azerbaijan Grand Prix',                  'az','Baku City Circuit',                       'Baku',            'Asia/Baku',          '2019-04-28 12:10:00','2019-04-27 13:00:00',4, 51,6.003,0),
  (@season,@series,5, 'Spanish Grand Prix',      'Spain',        'Gran Premio de Espa&ntilde;a',           'es','Circuit de Catalunya',                    'Barcelona',       'Europe/Madrid',      '2019-05-12 13:10:00','2019-05-11 13:00:00',5, 66,4.655,0),
  (@season,@series,6, 'Monaco Grand Prix',       'Monaco',       'Grand Prix de Monaco',                   'mc','Circuit de Monaco',                       'Monte Carlo',     'Europe/Monaco',      '2019-05-26 13:10:00','2019-05-25 13:00:00',6, 78,3.337,0),
  (@season,@series,7, 'Canadian Grand Prix',     'Canada',       'Grand Prix du Canada',                   'ca','Circuit Gilles Villeneuve',               'Montreal',        'America/Montreal',   '2019-06-09 18:10:00','2019-06-08 18:00:00',7, 70,4.361,0),
  (@season,@series,8, 'French Grand Prix',       'France',       'Grand Prix de France',                   'fr','Circuit Paul Ricard',                     'Le Castellet',    'Europe/Paris',       '2019-06-23 13:10:00','2019-06-22 13:00:00',8, 53,5.861,0),
  (@season,@series,9, 'Austrian Grand Prix',     'Austria',      'Gro&szlig;er Preis von &Ouml;sterreich', 'at','Red Bull Ring',                           'Spielberg',       'Europe/Vienna',      '2019-06-30 13:10:00','2019-06-29 13:00:00',9, 71,4.326,0),
  (@season,@series,10,'British Grand Prix',      'Great Britain','Rolex British Grand Prix',               'gb','Silverstone Circuit',                     'Silverstone',     'Europe/London',      '2019-07-14 13:10:00','2019-07-13 13:00:00',10,52,5.891,0),
  (@season,@series,11,'German Grand Prix',       'Germany',      'Gro&szlig;er Preis von Deutschland',     'de','Hockenheimring',                          'Hockenheim',      'Europe/Berlin',      '2019-07-28 13:10:00','2019-07-27 13:00:00',11,67,4.574,0),
  (@season,@series,12,'Hungarian Grand Prix',    'Hungary',      'Magyar Nagyd&iacute;j',                  'hu','Hungaroring',                             'Budapest',        'Europe/Budapest',    '2019-08-04 13:10:00','2019-08-03 13:00:00',12,70,4.381,0),
  (@season,@series,13,'Belgian Grand Prix',      'Belgium',      'Belgian Grand Prix',                     'be','Circuit de Spa-Francorchamps',            'Spa',             'Europe/Brussels',    '2019-09-01 13:10:00','2019-08-31 13:00:00',13,44,7.004,0),
  (@season,@series,14,'Italian Grand Prix',      'Italy',        'Gran Premio Heineken d&#39;Italia',      'it','Autodromo Nazionale Monza',               'Monza',           'Europe/Rome',        '2019-09-08 13:10:00','2019-09-07 13:00:00',14,53,5.793,0),
  (@season,@series,15,'Singapore Grand Prix',    'Singapore',    'Singapore Grand Prix',                   'sg','Marina Bay Street Circuit',               'Singapore',       'Asia/Singapore',     '2019-09-22 12:10:00','2019-09-21 13:00:00',15,61,5.065,0),
  (@season,@series,16,'Russian Grand Prix',      'Russia',       'VTB Russian Grand Prix',                 'ru','Sochi International Street Circuit',      'Sochi',           'Europe/Moscow',      '2019-09-29 11:10:00','2019-09-28 12:00:00',16,53,5.848,0),
  (@season,@series,17,'Japanese Grand Prix',     'Japan',        'Japanese Grand Prix',                    'jp','Suzuka Circuit',                          'Suzuka',          'Asia/Tokyo',         '2019-10-13 05:10:00','2019-10-12 06:00:00',17,53,5.807,0),
  (@season,@series,18,'Mexican Grand Prix',      'Mexico',       'Gran Premio de M&eacute;xico',           'mx','Autodromo Hermanos Rodriguez',            'Mexico City',     'America/Mexico_City','2019-10-27 19:10:00','2019-10-26 18:00:00',18,71,4.304,0),
  (@season,@series,19,'United States Grand Prix','United States','United States Grand Prix',               'us','Circuit of the Americas',                 'Austin, Texas',   'America/Chicago',    '2019-11-03 19:10:00','2019-11-02 21:00:00',19,56,5.513,0),
  (@season,@series,20,'Brazilian Grand Prix',    'Brazil',       'Grande Pr&ecirc;mio do Brasil',          'br','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','America/Sao_Paulo',  '2019-11-17 17:10:00','2019-11-16 17:00:00',20,71,4.309,0),
  (@season,@series,21,'Abu Dhabi Grand Prix',    'Abu Dhabi',    'Etihad Airways Abu Dhabi Grand Prix',    'ae','Yas Marina Circuit',                      'Yas Island',      'Asia/Dubai',         '2019-12-01 13:10:00','2019-11-30 13:00:00',21,55,5.554,0);

#
# The teams
#
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `series`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES
  (@season,@series,1, 'McLaren',     'gb','McLaren F1 Team',                 'Woking','gb'),
  (@season,@series,2, 'Ferrari',     'it','Scuderia Ferrari Mission Winnow', 'Maranello','it'),
  (@season,@series,3, 'Alfa Romeo',  'it','Alfa Romeo Racing',               'Hinwil','ch'),
  (@season,@series,4, 'Renault',     'fr','Renault Sport F1 Team',           'Enstone','gb'),
  (@season,@series,6, 'Toro Rosso',  'it','Red Bull Toro Rosso Honda',       'Faenza','it'),
  (@season,@series,7, 'Red Bull',    'at','Aston Martin Red Bull Racing',    'Milton Keynes','gb'),
  (@season,@series,8, 'Williams',    'gb','Williams Racing',                 'Grove','gb'),
  (@season,@series,10,'Mercedes',    'de','Mercedes AMG Petronas Motorsport','Brackley','gb'),
  (@season,@series,18,'Haas',        'us','Rich Energy Haas F1 Team',        'Kannapolis','us'),
  (@season,@series,20,'Racing Point','gb','Racing Point F1 Team',            'Silverstone','gb');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 4,  75, 1,  'NOR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 55, 62, 1,  'SAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 5, 15,  2,  'VET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 16, 73, 2,  'LEC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Alfa Romeo
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 7,  4,  3,  'RAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 99, 70, 3,  'GIO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Renault
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`,  3, 46, 4,  'RIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 27, 31, 4,  'HUL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Toro Rosso
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 23, 76, 6,  'ALB' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 26, 55, 6,  'KVY' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 10, 71, 7,  'GAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 33, 61, 7,  'VES' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 63, 77, 8,  'RUS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 88, 5 , 8,  'KUB' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 44, 1,  10, 'HAM' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 77, 50, 10, 'BOT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Haas
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 8,  26, 18, 'GRO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 20, 54, 18, 'MAG' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Racing Point
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 11, 43, 20, 'PER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_F1_RACES`.`round`, 18, 69, 20, 'STR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# New drivers
INSERT IGNORE INTO `SPORTS_F1_DRIVERS` VALUES
  (75, 'Lando', 'Norris', 'Bristol', 'eng', 'gb', '1999-11-13'),
  (76, 'Alexander', 'Albon', 'London', 'eng', 'th', '1996-03-23'),
  (77, 'George', 'Russell', 'King&#39;s Lynn', 'eng', 'gb', '1998-02-15');

#
# Default standings
#
INSERT INTO `SPORTS_F1_DRIVER_HISTORY` (`season`, `series`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_F1_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_F1_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_F1_TEAM_HISTORY` (`season`, `series`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_F1_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_F1_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  GROUP BY `ME`.`team_id`;
