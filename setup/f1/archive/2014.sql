SET @season := 2014;

#
# Remove any previous setup
#
DELETE FROM `SPORTS_F1_DRIVER_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_DRIVERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_GRID` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_LEADERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_QUALI` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_RESULT` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAMS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY_PROGRESS` WHERE `season` = @season;

#
# Race calendar
#
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,1,'Australian Grand Prix','Australia','Rolex Australian Grand Prix','au','Melbourne Grand Prix Circuit','Melbourne','Australia/Melbourne','2014-03-16 06:00:00','2014-03-15 06:00:00',1,58,5.303,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,2,'Malaysian Grand Prix','Malaysia','Petronas Malaysia Grand Prix','my','Sepang International Circuit','Kuala Lumpur','Asia/Kuala_Lumpur','2014-03-30 08:00:00','2014-03-29 08:00:00',2,56,5.543,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,3,'Bahrain Grand Prix','Bahrain','Gulf Air Bahrain Grand Prix','bh','Bahrain International Circuit','Sakhir','Asia/Bahrain','2014-04-06 15:00:00','2014-04-05 15:00:00',3,57,5.412,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,4,'Chinese Grand Prix','China','UBS Chinese Grand Prix','cn','Shanghai International Circuit','Shanghai','Asia/Shanghai','2014-04-20 07:00:00','2014-04-19 06:00:00',4,56,5.451,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,5,'Spanish Grand Prix','Spain','Gran Premio de Espa&ntilde;a','es','Circuit de Catalunya','Barcelona','Europe/Madrid','2014-05-11 12:00:00','2014-05-10 12:00:00',5,66,4.655,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,6,'Monaco Grand Prix','Monaco','Grand Prix de Monaco','mc','Circuit de Monaco','Monte Carlo','Europe/Monaco','2014-05-25 12:00:00','2014-05-24 12:00:00',6,78,3.34,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,7,'Canadian Grand Prix','Canada','Grand Prix du Canada','ca','Circuit Gilles Villeneuve','Montreal','America/Montreal','2014-06-08 18:00:00','2014-06-07 17:00:00',7,70,4.361,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,8,'Austrian Grand Prix','Austria','Austrian Grand Prix','at','Red Bull Ring','Spielberg','Europe/Vienna','2014-06-22 12:00:00','2014-06-21 12:00:00',8,71,4.326,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,9,'British Grand Prix','Great Britain','British Grand Prix','gb','Silverstone Circuit','Silverstone','Europe/London','2014-07-06 12:00:00','2014-07-05 12:00:00',9,52,5.891,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,10,'German Grand Prix','Germany','Gro&szlig;er Preis Santander von Deutschland','de','Hockenheimring','Hockenheim','Europe/Berlin','2014-07-20 12:00:00','2014-07-19 12:00:00',10,67,4.574,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,11,'Hungarian Grand Prix','Hungary','Magyar Nagyd&iacute;j','hu','Hungaroring','Budapest','Europe/Budapest','2014-07-27 12:00:00','2014-07-26 12:00:00',11,70,4.381,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,12,'Belgian Grand Prix','Belgium','Shell Belgian Grand Prix','be','Circuit de Spa-Francorchamps','Spa','Europe/Brussels','2014-08-24 12:00:00','2014-08-23 12:00:00',12,44,7.004,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,13,'Italian Grand Prix','Italy','Gran Premio d&#39;Italia','it','Autodromo Nazionale Monza','Monza','Europe/Rome','2014-09-07 12:00:00','2014-09-06 12:00:00',13,53,5.793,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,14,'Singapore Grand Prix','Singapore','Singapore Grand Prix','sg','Marina Bay Street Circuit','Singapore','Asia/Singapore','2014-09-21 12:00:00','2014-09-20 13:00:00',14,61,5.073,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,15,'Japanese Grand Prix','Japan','Japanese Grand Prix','jp','Suzuka Circuit','Suzuka','Asia/Tokyo','2014-10-05 06:00:00','2014-10-04 05:00:00',15,53,5.807,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,16,'Russian Grand Prix','Russia','Airtel Indian Grand Prix','ru','Sochi International Street Circuit','Sochi','Europe/Moscow','2014-10-12 11:00:00','2014-10-11 11:00:00',16,53,5.853,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,17,'United States Grand Prix','United States','United States Grand Prix','us','Circuit of the Americas','Austin, Texas','America/Chicago','2014-11-02 20:00:00','2014-11-01 19:00:00',17,56,5.513,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,18,'Brazilian Grand Prix','Brazil','Grande Pr&ecirc;mio do Brasil','br','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','America/Sao_Paulo','2014-11-09 16:00:00','2014-11-08 16:00:00',18,71,4.309,0,NULL,NULL,NULL,NULL);
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`) VALUES (@season,19,'Abu Dhabi Grand Prix','Abu Dhabi','Etihad Airways Abu Dhabi Grand Prix','ae','Yas Marina Circuit','Yas Island','Asia/Dubai','2014-11-23 13:00:00','2014-11-22 13:00:00',19,55,5.554,0,NULL,NULL,NULL,NULL);

#
# The teams
#
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,1,'McLaren','gb','McLaren Mercedes','Woking','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,2,'Ferrari','it','Scuderia Ferrari','Maranello','it');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,3,'Sauber','ch','Sauber F1 Team','Hinwil','ch');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,4,'Lotus','gb','Lotus F1 Team','Enstone','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,6,'Toro Rosso','it','Scuderia Toro Rosso','Faenza','it');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,7,'Red Bull','at','Infiniti Red Bull Racing','Milton Keynes','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,8,'Williams','gb','Williams F1 Team','Grove','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,9,'Force India','in','Sahara Force India Formula One','Silverstone','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,10,'Mercedes','de','Mercedes AMG Petronas F1 Team','Brackley','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,15,'Caterham','my','Caterham F1 Team','Leafield','gb');
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES (@season,17,'Marussia','ru','Marussia F1 Team','Banbury','gb');

#
# Drivers
#
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`,  1, 15, 7,  'VET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`,  3, 46, 7,  'RIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 44, 1,  10, 'HAM' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`,  6, 16, 10, 'ROS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 14, 7,  2,  'ALO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 7,  4,  2,  'RAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 8,  26, 4,  'GRO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 13, 41, 4,  'MAL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 22, 22, 1,  'BUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 20, 54, 1,  'MAG' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 27, 31, 9,  'HUL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 11, 43, 9,  'PER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 99, 20, 3,  'SUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 21, 49, 3,  'GUT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 25, 48, 6,  'VER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 26, 55, 6,  'KVY' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 19, 3,  8,  'MAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 77, 50, 8,  'BOT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 17, 52, 17, 'BIA' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 4,  53, 17, 'CHI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 10, 28, 15, 'KOB' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS` SELECT @season, `SPORTS_F1_RACES`.`round`, 9,  56, 15, 'ERI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round` ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

INSERT IGNORE INTO `SPORTS_F1_DRIVERS` VALUES (54, 'Kevin', 'Magnussen', 'Roskilde', 'dk', 'dk', '1992-10-05');
INSERT IGNORE INTO `SPORTS_F1_DRIVERS` VALUES (55, 'Daniil', 'Kvyat', 'Ufa', 'ru', 'ru', '1994-04-26');
INSERT IGNORE INTO `SPORTS_F1_DRIVERS` VALUES (56, 'Marcus', 'Ericsson', 'Kumla', 'se', 'se', '1990-09-02');

#
# Default standings
#
INSERT INTO `SPORTS_F1_DRIVER_HISTORY` (`season`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_F1_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_F1_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_F1_TEAM_HISTORY` (`season`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_F1_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_F1_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`team_id`;

