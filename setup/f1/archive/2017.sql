SET @season := 2017;

#
# Remove any previous setup
#
DELETE FROM `SPORTS_F1_DRIVER_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_DRIVERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_GRID` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_LEADERS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_QUALI` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_RACE_RESULT` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAMS` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY` WHERE `season` = @season;
DELETE FROM `SPORTS_F1_TEAM_HISTORY_PROGRESS` WHERE `season` = @season;

#
# Race calendar
#
INSERT INTO `SPORTS_F1_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `circuit`, `location`, `timezone`, `race_time`, `qual_time`, `round_order`, `total_laps`, `lap_length`, `completed_laps`, `fastest_lap`, `fastest_lap_ms`, `fastest_lap_car`, `fastest_lap_num`)
  VALUES (@season,1, 'Australian Grand Prix',   'Australia',    'Rolex Australian Grand Prix',            'au','Melbourne Grand Prix Circuit',            'Melbourne',       'Australia/Melbourne','2017-03-26 05:00:00','2017-03-25 06:00:00',1, 58,5.303,0,NULL,NULL,NULL,NULL),
         (@season,2, 'Chinese Grand Prix',      'China',        'Pirelli Chinese Grand Prix',             'cn','Shanghai International Circuit',          'Shanghai',        'Asia/Shanghai',      '2017-04-09 06:00:00','2017-04-08 07:00:00',2, 56,5.451,0,NULL,NULL,NULL,NULL),
         (@season,3, 'Bahrain Grand Prix',      'Bahrain',      'Bahrain Grand Prix',                     'bh','Bahrain International Circuit',           'Zallaq',          'Asia/Bahrain',       '2017-04-16 15:00:00','2017-04-15 15:00:00',3, 57,5.412,0,NULL,NULL,NULL,NULL),
         (@season,4, 'Russian Grand Prix',      'Russia',       'Russian Grand Prix',                     'ru','Sochi International Street Circuit',      'Sochi',           'Europe/Moscow',      '2017-04-30 12:00:00','2017-04-29 12:00:00',4, 53,5.848,0,NULL,NULL,NULL,NULL),
         (@season,5, 'Spanish Grand Prix',      'Spain',        'Gran Premio de Espa&ntilde;a Pirelli',   'es','Circuit de Catalunya',                    'Barcelona',       'Europe/Madrid',      '2017-05-14 12:00:00','2017-05-13 12:00:00',5, 66,4.655,0,NULL,NULL,NULL,NULL),
         (@season,6, 'Monaco Grand Prix',       'Monaco',       'Grand Prix de Monaco',                   'mc','Circuit de Monaco',                       'Monte Carlo',     'Europe/Monaco',      '2017-05-28 12:00:00','2017-05-27 12:00:00',6, 78,3.337,0,NULL,NULL,NULL,NULL),
         (@season,7, 'Canadian Grand Prix',     'Canada',       'Grand Prix du Canada',                   'ca','Circuit Gilles Villeneuve',               'Montreal',        'America/Montreal',   '2017-06-11 18:00:00','2017-06-10 17:00:00',7, 70,4.361,0,NULL,NULL,NULL,NULL),
         (@season,8, 'Azerbaijan Grand Prix',   'Azerbaijan',   'Azerbaijan Grand Prix',                  'az','Baku City Circuit',                       'Baku',            'Asia/Baku',          '2017-06-25 13:00:00','2017-06-24 13:00:00',8, 51,6.003,0,NULL,NULL,NULL,NULL),
         (@season,9, 'Austrian Grand Prix',     'Austria',      'Gro&szlig;er Preis von &Ouml;sterreich', 'at','Red Bull Ring',                           'Spielberg',       'Europe/Vienna',      '2017-07-09 12:00:00','2017-07-08 12:00:00',9, 71,4.326,0,NULL,NULL,NULL,NULL),
         (@season,10,'British Grand Prix',      'Great Britain','British Grand Prix',                     'gb','Silverstone Circuit',                     'Silverstone',     'Europe/London',      '2017-07-16 12:00:00','2017-07-15 12:00:00',10,52,5.891,0,NULL,NULL,NULL,NULL),
         (@season,11,'Hungarian Grand Prix',    'Hungary',      'Magyar Nagyd&iacute;j',                  'hu','Hungaroring',                             'Budapest',        'Europe/Budapest',    '2017-07-30 12:00:00','2017-07-29 12:00:00',11,70,4.381,0,NULL,NULL,NULL,NULL),
         (@season,12,'Belgian Grand Prix',      'Belgium',      'Belgian Grand Prix',                     'be','Circuit de Spa-Francorchamps',            'Spa',             'Europe/Brussels',    '2017-08-27 12:00:00','2017-08-26 12:00:00',12,44,7.004,0,NULL,NULL,NULL,NULL),
         (@season,13,'Italian Grand Prix',      'Italy',        'Gran Premio d&#39;Italia',               'it','Autodromo Nazionale Monza',               'Monza',           'Europe/Rome',        '2017-09-03 12:00:00','2017-09-02 12:00:00',13,53,5.793,0,NULL,NULL,NULL,NULL),
         (@season,14,'Singapore Grand Prix',    'Singapore',    'Singapore Airlines Singapore Grand Prix','sg','Marina Bay Street Circuit',               'Singapore',       'Asia/Singapore',     '2017-09-17 12:00:00','2017-09-16 13:00:00',14,61,5.065,0,NULL,NULL,NULL,NULL),
         (@season,15,'Malaysian Grand Prix',    'Malaysia',     'Malaysia Grand Prix',                    'my','Sepang International Circuit',            'Kuala Lumpur',    'Asia/Kuala_Lumpur',  '2017-10-01 07:00:00','2017-09-30 09:00:00',15,56,5.543,0,NULL,NULL,NULL,NULL),
         (@season,16,'Japanese Grand Prix',     'Japan',        'Japanese Grand Prix',                    'jp','Suzuka Circuit',                          'Suzuka',          'Asia/Tokyo',         '2017-10-08 05:00:00','2017-10-07 06:00:00',16,53,5.807,0,NULL,NULL,NULL,NULL),
         (@season,17,'United States Grand Prix','United States','United States Grand Prix',               'us','Circuit of the Americas',                 'Austin, Texas',   'America/Chicago',    '2017-10-22 19:00:00','2017-10-21 19:00:00',17,56,5.513,0,NULL,NULL,NULL,NULL),
         (@season,18,'Mexican Grand Prix',      'Mexico',       'Gran Premio de M&eacute;xico',           'mx','Autodromo Hermanos Rodriguez',            'Mexico City',     'America/Mexico_City','2017-10-29 19:00:00','2017-10-28 18:00:00',18,71,4.304,0,NULL,NULL,NULL,NULL),
         (@season,19,'Brazilian Grand Prix',    'Brazil',       'Grande Pr&ecirc;mio do Brasil',          'br','Aut&oacute;dromo Jos&eacute; Carlos Pace','S&atilde;o Paulo','America/Sao_Paulo',  '2017-11-12 16:00:00','2017-11-11 16:00:00',19,71,4.309,0,NULL,NULL,NULL,NULL),
         (@season,20,'Abu Dhabi Grand Prix',    'Abu Dhabi',    'Etihad Airways Abu Dhabi Grand Prix',    'ae','Yas Marina Circuit',                      'Yas Island',      'Asia/Dubai',         '2017-11-26 13:00:00','2017-11-25 13:00:00',20,55,5.554,0,NULL,NULL,NULL,NULL);

#
# The teams
#
INSERT INTO `SPORTS_F1_TEAMS` (`season`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`)
  VALUES (@season,1,'McLaren','gb','McLaren Honda','Woking','gb'),
         (@season,2,'Ferrari','it','Scuderia Ferrari','Maranello','it'),
         (@season,3,'Sauber','ch','Sauber F1 Team','Hinwil','ch'),
         (@season,4,'Renault','fr','Renault Sport F1 Team','Enstone','gb'),
         (@season,6,'Toro Rosso','it','Scuderia Toro Rosso','Faenza','it'),
         (@season,7,'Red Bull','at','Red Bull Racing','Milton Keynes','gb'),
         (@season,8,'Williams','gb','Williams Martini Racing','Grove','gb'),
         (@season,9,'Force India','in','Sahara Force India Formula One','Silverstone','gb'),
         (@season,10,'Mercedes','de','Mercedes AMG Petronas F1 Team','Brackley','gb'),
         (@season,18,'Haas','us','Haas F1 Team','Kannapolis','us');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 2,  67, 1,  'VAN' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 14, 7 , 1,  'ALO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 5, 15,  2,  'VET' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 7,  4,  2,  'RAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Sauber
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 9,  56, 3,  'ERI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 94, 65, 3,  'WEH' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Renault
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 27, 31, 4,  'HUL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 30, 64, 4,  'PAL' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Toro Rosso
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 55, 62, 6,  'SAI' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 26, 55, 6,  'KVY' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`,  3, 46, 7,  'RIC' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 33, 61, 7,  'VES' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 19, 3,  8,  'MAS' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 18, 69, 8,  'STR' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Force India
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 11, 43, 9,  'PER' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 31, 68, 9,  'OCO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 44, 1,  10, 'HAM' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 77, 50, 10, 'BOT' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Haas
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 8,  26, 18, 'GRO' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_F1_RACE_DRIVERS`
  SELECT @season, `SPORTS_F1_RACES`.`round`, 20, 54, 18, 'MAG' FROM `SPORTS_F1_RACES` WHERE `SPORTS_F1_RACES`.`season` = @season ORDER BY `SPORTS_F1_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# New drivers
INSERT IGNORE INTO `SPORTS_F1_DRIVERS` VALUES
  (69, 'Lance', 'Stroll', 'Montr&eacute;al', 'ca', 'ca', '1998-10-29');

#
# Default standings
#
INSERT INTO `SPORTS_F1_DRIVER_HISTORY` (`season`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_F1_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_F1_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_F1_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_F1_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`driver_id`;

INSERT INTO `SPORTS_F1_TEAM_HISTORY` (`season`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_F1_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_F1_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  GROUP BY `ME`.`team_id`;

