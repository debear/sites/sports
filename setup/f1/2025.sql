SET @season := 2025;
SET @series := 'f1';

#
# Remove any previous setup
#
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_DRIVER_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACES` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_DRIVERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_FASTEST_LAP` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_GRID` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_LEADERS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_QUALI` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_RACE_RESULT` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAMS` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY` WHERE `season` = @season AND `series` = @series;
DELETE FROM `SPORTS_FIA_TEAM_HISTORY_PROGRESS` WHERE `season` = @season AND `series` = @series;

#
# Race calendar
#
INSERT INTO `SPORTS_FIA_RACES` (`season`, `series`, `round`, `name_full`, `name_short`, `flag`, `circuit`, `location`, `timezone`, `quirks`, `qual_time`, `race_time`, `race2_time`, `round_order`, `race_laps_total`, `race2_laps_total`, `lap_length`) VALUES
  (@season,@series, 1, 'Australian Grand Prix',      'Australia',       'au',    'Melbourne Grand Prix Circuit',                'Melbourne',       'Australia/Melbourne',NULL,                         '2025-03-15 05:00:00','2025-03-16 04:00:00',NULL,                  1,58, 0,5.303),
  (@season,@series, 2, 'Chinese Grand Prix',         'China',           'cn',    'Shanghai International Circuit',              'Shanghai',        'Asia/Shanghai',      'SPRINT_RACE,SPRINT_SHOOTOUT','2025-03-22 07:00:00','2025-03-22 03:00:00','2025-03-23 07:00:00', 2,19,56,5.451),
  (@season,@series, 3, 'Japanese Grand Prix',        'Japan',           'jp',    'Suzuka Circuit',                              'Suzuka',          'Asia/Tokyo',         NULL,                         '2025-04-05 06:00:00','2025-04-06 05:00:00',NULL,                  3,53, 0,5.807),
  (@season,@series, 4, 'Bahrain Grand Prix',         'Bahrain',         'bh',    'Bahrain International Circuit',               'Dar Kulayb',      'Asia/Bahrain',       NULL,                         '2025-04-12 16:00:00','2025-04-13 15:00:00',NULL,                  4,57, 0,5.412),
  (@season,@series, 5, 'Saudi Arabian Grand Prix',   'Saudi Arabia',    'sa',    'Jeddah Street Circuit',                       'Jeddah',          'Asia/Dubai',         NULL,                         '2025-04-19 17:00:00','2025-04-20 17:00:00',NULL,                  5,50, 0,6.174),
  (@season,@series, 6, 'Miami Grand Prix',           'Miami',           'us-mia','Miami International Autodrome',               'Miami',           'America/New_York',   'SPRINT_RACE,SPRINT_SHOOTOUT','2025-05-03 20:00:00','2025-05-03 16:00:00','2025-05-04 20:00:00', 6,19,57,5.410),
  (@season,@series, 7, 'Emilia Romagna Grand Prix',  'Emilia Romagna',  'it-emr','Autodromo Internazionale Enzo e Dino Ferrari','Imola',           'Europe/Rome',        NULL,                         '2025-05-18 14:00:00','2025-05-18 13:00:00',NULL,                  7,63, 0,4.909),
  (@season,@series, 8, 'Monaco Grand Prix',          'Monaco',          'mc',    'Circuit de Monaco',                           'Monte Carlo',     'Europe/Monaco',      NULL,                         '2025-05-25 14:00:00','2025-05-25 13:00:00',NULL,                  8,78, 0,3.337),
  (@season,@series, 9, 'Spanish Grand Prix',         'Spain',           'es',    'Circuit de Catalunya',                        'Barcelona',       'Europe/Madrid',      NULL,                         '2025-05-31 14:00:00','2025-06-01 13:00:00',NULL,                  9,66, 0,4.655),
  (@season,@series,10, 'Canadian Grand Prix',        'Canada',          'ca',    'Circuit Gilles Villeneuve',                   'Montreal',        'America/Montreal',   NULL,                         '2025-06-14 20:00:00','2025-06-15 18:00:00',NULL,                 10,70, 0,4.361),
  (@season,@series,11, 'Austrian Grand Prix',        'Austria',         'at',    'Red Bull Ring',                               'Spielberg',       'Europe/Vienna',      NULL,                         '2025-06-28 14:00:00','2025-06-29 13:00:00',NULL,                 11,71, 0,4.326),
  (@season,@series,12, 'British Grand Prix',         'Great Britain',   'gb',    'Silverstone Circuit',                         'Silverstone',     'Europe/London',      NULL,                         '2025-07-05 13:00:00','2025-07-06 13:00:00',NULL,                 12,52, 0,5.891),
  (@season,@series,13, 'Belgian Grand Prix',         'Belgium',         'be',    'Circuit de Spa-Francorchamps',                'Spa',             'Europe/Brussels',    'SPRINT_RACE,SPRINT_SHOOTOUT','2025-07-26 14:00:00','2025-07-26 10:00:00','2025-07-27 13:00:00',13,14,44,7.004),
  (@season,@series,14, 'Hungarian Grand Prix',       'Hungary',         'hu',    'Hungaroring',                                 'Budapest',        'Europe/Budapest',    NULL,                         '2025-08-02 14:00:00','2025-08-03 13:00:00',NULL,                 14,70, 0,4.381),
  (@season,@series,15, 'Dutch Grand Prix',           'Netherlands',     'nl',    'Circuit Zandvoort',                           'Zandvoort',       'Europe/Amsterdam',   NULL,                         '2025-08-30 13:00:00','2025-08-31 13:00:00',NULL,                 15,72, 0,4.252),
  (@season,@series,16, 'Italian Grand Prix',         'Italy',           'it',    'Autodromo Nazionale Monza',                   'Monza',           'Europe/Rome',        NULL,                         '2025-09-06 14:00:00','2025-09-07 13:00:00',NULL,                 16,53, 0,5.793),
  (@season,@series,17, 'Azerbaijan Grand Prix',      'Azerbaijan',      'az',    'Baku City Circuit',                           'Baku',            'Asia/Baku',          NULL,                         '2025-09-20 12:00:00','2025-09-21 11:00:00',NULL,                 17,51, 0,6.003),
  (@season,@series,18, 'Singapore Grand Prix',       'Singapore',       'sg',    'Marina Bay Street Circuit',                   'Singapore',       'Asia/Singapore',     NULL,                         '2025-10-04 13:00:00','2025-10-05 12:00:00',NULL,                 18,61, 0,5.065),
  (@season,@series,19, 'United States Grand Prix',   'United States',   'us',    'Circuit of the Americas',                     'Austin',          'America/Chicago',    'SPRINT_RACE,SPRINT_SHOOTOUT','2025-10-18 21:00:00','2025-10-18 17:00:00','2025-10-19 19:00:00',19,19,56,5.513),
  (@season,@series,20, 'Mexico City Grand Prix',     'Mexico City',     'mx',    'Autodromo Hermanos Rodriguez',                'Mexico City',     'America/Mexico_City',NULL,                         '2025-10-25 21:00:00','2025-10-26 20:00:00',NULL,                 20,71, 0,4.304),
  (@season,@series,21, 'S&atilde;o Paulo Grand Prix','S&atilde;o Paulo','br',    'Aut&oacute;dromo Jos&eacute; Carlos Pace',    'S&atilde;o Paulo','America/Sao_Paulo',  'SPRINT_RACE,SPRINT_SHOOTOUT','2025-11-08 18:00:00','2025-11-08 14:00:00','2025-11-09 17:00:00',21,24,71,4.309),
  (@season,@series,22, 'Las Vegas Grand Prix',       'Las Vegas',       'us-veg','Las Vegas Street Circuit',                    'Las Vegas',       'America/Los_Angeles',NULL,                         '2025-11-22 04:00:00','2025-11-23 04:00:00',NULL,                 22,50, 0,6.120),
  (@season,@series,23, 'Qatar Grand Prix',           'Qatar',           'qa',    'Losail International Circuit',                'Lusail',          'Asia/Qatar',         'SPRINT_RACE,SPRINT_SHOOTOUT','2025-11-29 18:00:00','2025-11-29 14:00:00','2025-11-30 16:00:00',23,19,57,5.380),
  (@season,@series,24, 'Abu Dhabi Grand Prix',       'Abu Dhabi',       'ae',    'Yas Marina Circuit',                          'Abu Dhabi',       'Asia/Dubai',         NULL,                         '2025-12-06 14:00:00','2025-12-07 13:00:00',NULL,                 24,55, 0,5.554);

#
# The teams
#
INSERT INTO `SPORTS_FIA_TEAMS` (`season`, `series`, `team_id`, `team_name`, `team_country`, `official_name`, `base`, `base_country`) VALUES
  (@season,@series,1,  'McLaren',     'gb','McLaren Formula One Team',                   'Woking',       'gb'),
  (@season,@series,2,  'Ferrari',     'it','Scuderia Ferrari HP',                        'Maranello',    'it'),
  (@season,@series,3,  'Kick Sauber', 'ch','Stake F1 Team Kick Sauber',                  'Hinwil',       'ch'),
  (@season,@series,4,  'Alpine',      'fr','BWT Alpine Formula One Team',                'Enstone',      'gb'),
  (@season,@series,6,  'Racing Bulls','it','Visa Cash App Racing Bulls Formula One Team','Faenza',       'it'),
  (@season,@series,7,  'Red Bull',    'at','Oracle Red Bull Racing',                     'Milton Keynes','gb'),
  (@season,@series,8,  'Williams',    'gb','Williams Racing',                            'Grove',        'gb'),
  (@season,@series,10, 'Mercedes',    'de','Mercedes-AMG Petronas Formula One Team',     'Brackley',     'gb'),
  (@season,@series,18, 'Haas',        'us','MoneyGram Haas F1 Team',                     'Kannapolis',   'us'),
  (@season,@series,20, 'Aston Martin','gb','Aston Martin Aramco Formula One Team',       'Silverstone',  'gb');

#
# Drivers
#

# McLaren
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  4, 75,  1, 'NOR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 81, 86,  1, 'PIA' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Ferrari
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 16, 73,  2, 'LEC' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 44,  1,  2, 'HAM' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Sauber
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  5, 92,  3, 'BOR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 27, 31,  3, 'HUL' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Alpine
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  7, 91,  4, 'DOO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 10, 71,  4, 'GAS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Racing Bulls
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  6, 93,  6, 'HAD' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 22, 82,  6, 'TSU' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Red Bull
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 30, 88,  7, 'LAW' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`,  1, 61,  7, 'VES' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Williams
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 23, 76,  8, 'ALB' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 55, 62,  2, 'SAI' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Mercedes
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 12, 94, 12, 'ANT' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 63, 77, 10, 'RUS' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Haas
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 31, 68, 18, 'OCO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 87, 89, 18, 'BEA' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# Aston Martin
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 14,  7, 20, 'ALO' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);
INSERT INTO `SPORTS_FIA_RACE_DRIVERS`
  SELECT @season, @series, `SPORTS_FIA_RACES`.`round`, 18, 69, 20, 'STR' FROM `SPORTS_FIA_RACES` WHERE `SPORTS_FIA_RACES`.`season` = @season ORDER BY `SPORTS_FIA_RACES`.`round`
ON DUPLICATE KEY UPDATE `driver_id` = VALUES (`driver_id`), `driver_code` = VALUES (`driver_code`);

# New drivers
INSERT IGNORE INTO `SPORTS_FIA_DRIVERS` VALUES
  (92, 'Gabriel', 'Bortoleto', 'S&atilde;o Paulo', 'br', 'br', '2004-10-14'),
  (93, 'Isack', 'Hadjar', 'Paris', 'fr', 'fr', '2004-09-28'),
  (94, 'Kimi', 'Antonelli', 'Bologna', 'it', 'it', '2006-08-25');

#
# Default standings
#
INSERT INTO `SPORTS_FIA_DRIVER_HISTORY` (`season`, `series`, `driver_id`, `pos`, `pts`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`driver_id`, COUNT(DISTINCT `OTHER_NAME`.`driver_id`) + 1, 0
  FROM `SPORTS_FIA_RACE_DRIVERS` AS `ME`
  LEFT JOIN `SPORTS_FIA_RACE_DRIVERS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`round` = `ME`.`round`
    AND `OTHER`.`driver_id` <> `ME`.`driver_id`)
  JOIN `SPORTS_FIA_DRIVERS` AS `ME_NAME`
    ON (`ME_NAME`.`driver_id` = `ME`.`driver_id`)
  LEFT JOIN `SPORTS_FIA_DRIVERS` AS `OTHER_NAME`
    ON (`OTHER_NAME`.`driver_id` = `OTHER`.`driver_id`
    AND (fn_basic_html_comparison(`OTHER_NAME`.`surname`) < fn_basic_html_comparison(`ME_NAME`.`surname`)
         OR (fn_basic_html_comparison(`OTHER_NAME`.`surname`) = fn_basic_html_comparison(`ME_NAME`.`surname`)
             AND fn_basic_html_comparison(`OTHER_NAME`.`first_name`) < fn_basic_html_comparison(`ME_NAME`.`first_name`))))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  AND   `ME`.`round` = 1
  GROUP BY `ME`.`season`, `ME`.`series`, `ME`.`driver_id`;

INSERT INTO `SPORTS_FIA_TEAM_HISTORY` (`season`, `series`, `team_id`, `pos`, `pts`, `notes`)
  SELECT `ME`.`season`, `ME`.`series`, `ME`.`team_id`, COUNT(DISTINCT `OTHER`.`team_id`) + 1, 0, NULL
  FROM `SPORTS_FIA_TEAMS` AS `ME`
  LEFT JOIN `SPORTS_FIA_TEAMS` AS `OTHER`
    ON (`OTHER`.`season` = `ME`.`season`
    AND `OTHER`.`series` = `ME`.`series`
    AND `OTHER`.`team_id` <> `ME`.`team_id`
    AND fn_basic_html_comparison(`OTHER`.`team_name`) < fn_basic_html_comparison(`ME`.`team_name`))
  WHERE `ME`.`season` = @season
  AND   `ME`.`series` = @series
  GROUP BY `ME`.`season`, `ME`.`series`, `ME`.`team_id`;
