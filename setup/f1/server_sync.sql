SET @season := 2025;

#
# F1 Results
#
SET @sync_app := 'sports_f1';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports F1', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_FIA_DRIVER_HISTORY', 1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_FIA_DRIVER_HISTORY_PROGRESS', 2, 'database'),
                        (@sync_app, 14, '__DIR__ SPORTS_FIA_DRIVER_STATS', 3, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_FIA_RACES', 4, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_FIA_RACE_DRIVERS', 5, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_FIA_RACE_GRID', 6, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_FIA_RACE_LEADERS', 7, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_FIA_RACE_QUALI', 8, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_FIA_RACE_RESULT', 9, 'database'),
                        (@sync_app, 13, '__DIR__ SPORTS_FIA_RACE_FASTEST_LAP', 10, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_FIA_TEAM_HISTORY', 11, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_FIA_TEAM_HISTORY_PROGRESS', 12, 'database'),
                        (@sync_app, 15, '__DIR__ SPORTS_FIA_TEAM_STATS', 13, 'database'),
                        (@sync_app, 11, '__DIR__ Driver List', 14, 'script');
			# ID 12, News sync, removed from this sync
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_FIA_DRIVER_HISTORY', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_FIA_DRIVER_HISTORY_PROGRESS', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_FIA_RACES', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_FIA_RACE_DRIVERS', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_FIA_RACE_GRID', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_FIA_RACE_LEADERS', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_FIA_RACE_QUALI', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 8, 'debearco_sports', 'SPORTS_FIA_RACE_RESULT', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 9, 'debearco_sports', 'SPORTS_FIA_TEAM_HISTORY', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_FIA_TEAM_HISTORY_PROGRESS', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 13, 'debearco_sports', 'SPORTS_FIA_RACE_FASTEST_LAP', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 14, 'debearco_sports', 'SPORTS_FIA_DRIVER_STATS', CONCAT('season = ''', @season, ''' AND series = ''f1''')),
                           (@sync_app, 15, 'debearco_sports', 'SPORTS_FIA_TEAM_STATS', CONCAT('season = ''', @season, ''' AND series = ''f1'''));
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 11, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_f1_entries __REMOTE__');

#
# F1 Teams/Drivers
#
SET @sync_app := 'sports_f1_entries';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports F1 Entries', 'data,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_FIA_DRIVERS', 1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_FIA_DRIVER_GP_HISTORY', 2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_FIA_DRIVER_HISTORY', 3, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_FIA_DRIVER_HISTORY_PROGRESS', 4, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_FIA_DRIVER_STATS', 5, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_FIA_RACE_DRIVERS', 6, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_FIA_TEAMS', 7, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_FIA_TEAM_HISTORY', 8, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_FIA_TEAM_HISTORY_PROGRESS', 9, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_FIA_TEAM_STATS', 10, 'database'),
                        (@sync_app, 12, '__DIR__ FANTASY_COMMON_MOTORS_SELECTIONS_STATUS', 11, 'database'),
                        (@sync_app,  9, '__DIR__ Driver and Team Mugshots', 12, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_FIA_DRIVERS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_FIA_DRIVER_GP_HISTORY', 'series = ''f1'''),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_FIA_DRIVER_HISTORY', 'series = ''f1'''),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_FIA_DRIVER_HISTORY_PROGRESS', 'series = ''f1'''),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_FIA_RACE_DRIVERS', 'series = ''f1'''),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_FIA_TEAMS', 'series = ''f1'''),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_FIA_TEAM_HISTORY', 'series = ''f1'''),
                           (@sync_app, 8, 'debearco_sports', 'SPORTS_FIA_TEAM_HISTORY_PROGRESS', 'series = ''f1'''),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_FIA_DRIVER_STATS', 'series = ''f1'''),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_FIA_TEAM_STATS', 'series = ''f1'''),
                           (@sync_app, 12, 'debearco_fantasy', 'FANTASY_COMMON_MOTORS_SELECTIONS_STATUS', 'sport = ''f1''');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                       VALUES (@sync_app, 9, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ cdn_sports_f1 __REMOTE__');

#
# F1 News
#
SET @sync_app := 'sports_f1_news';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports F1 News', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ NEWS_SOURCES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ NEWS_ARTICLES', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause, clear_first)
                    VALUES (@sync_app, 1, 'debearco_common', 'NEWS_SOURCES', 'app = ''sports_f1''', 0),
                           (@sync_app, 2, 'debearco_common', 'NEWS_ARTICLES', 'app = ''sports_f1''', 1);
