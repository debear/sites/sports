SET @season := 2025;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `flag`, `track`, `location`, `timezone`, `quirks`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'German Grand Prix',         'Germany',        'de',     'OneSolar Arena',              'Landshut',       'Europe/Berlin',     'QUALI_SPRINT', '2025-05-03 17:00:00', 1,  390),
  (@season, 2,  'Warsaw Grand Prix',         'Warsaw',         'pl-wsw', 'Stadion Narodowy',            'Warsaw',         'Europe/Warsaw',     'QUALI_SPRINT', '2025-05-17 17:00:00', 2,  274),
  (@season, 3,  'Czech Grand Prix',          'Czech Republic', 'cz',     'Stadium Marketa',             'Prague',         'Europe/Prague',     NULL,           '2025-05-31 17:00:00', 3,  353),
  (@season, 4,  'Manchester Grand Prix',     'Manchester',     'gb',     'National Speedway Stadium',   'Manchester',     'Europe/London',     'QUALI_SPRINT', '2025-06-13 18:00:00', 4,  347),
  (@season, 5,  'Manchester Grand Prix II',  'Manchester II',  'gb-gp2', 'National Speedway Stadium',   'Manchester',     'Europe/London',     'QUALI_SPRINT', '2025-06-14 18:00:00', 5,  347),
  (@season, 6,  'Gorz&oacute;w Grand Prix',  'Gorz&oacute;w',  'pl-grz', 'Edward Jancarz Stadium',      'Gorz&oacute;w',  'Europe/Warsaw',     'QUALI_SPRINT', '2025-06-21 17:00:00', 6,  329),
  (@season, 7,  'Swedish Grand Prix',        'Sweden',         'se',     'Skrotfrag Arena',             'M&aring;lilla',  'Europe/Stockholm',  NULL,           '2025-07-05 17:00:00', 7,  305),
  (@season, 8,  'Latvian Grand Prix',        'Latvia',         'lv',     'Bikernieki Speedway Stadium', 'Riga',           'Europe/Riga',       NULL,           '2025-08-02 16:00:00', 8,  355),
  (@season, 9,  'Wroc&lstrok;aw Grand Prix', 'Wroc&lstrok;aw', 'pl-wrc', 'Olympic Stadium',             'Wroc&lstrok;aw', 'Europe/Warsaw',     'QUALI_SPRINT', '2025-08-30 17:00:00', 9,  352),
  (@season, 10, 'Danish Grand Prix',         'Denmark',        'dk',     'Vojens Speedway Center',      'Vojens',         'Europe/Copenhagen', NULL,           '2025-09-13 17:00:00', 10, 300);

-- Regular Riders
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  95,  39 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1,  39);
# Robert Lambert
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 505, 119 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2, 119);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  66,   9 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,   9);
# Dan Bewley
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  99, 153 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4, 153);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  54,  42 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5,  42);
# Jack Holder
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  25, 140 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6, 140);
# Mikkel Michelsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 155,  66 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7,  66);
# Dominic Kubera
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 415, 156 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8, 156);
# Andzejs Lebedevs
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  29,  90 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9,  90);
# Max Fricke
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  46, 127 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10, 127);
# Kai Huckenbeck
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 744, 136 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11, 136);
# Jan Kvech
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 201, 174 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12, 174);
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  69, 115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13, 115);
# Anders Thomsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 105, 121 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14, 121);
# Brady Kurtz
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 101, 137 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 137);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
