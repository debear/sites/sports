SET @season := 2021;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'Italian Grand Prix',        'Italy',          'FIM Italian Speedway Grand Prix',                  'it',     'Pista Olimpia Terenzano',  'Terenzano',      'Europe/Rome',         '2021-04-24 17:00:00', 1,  400),
  (@season, 2,  'German Grand Prix',         'Germany',        'FIM German Speedway Grand Prix',                   'de',     'Bergring Arena',           'Teterow',        'Europe/Berlin',       '2021-05-22 17:00:00', 2,  314),
  (@season, 3,  'Czech Grand Prix',          'Czech Republic', 'FIM Czech Republic Speedway Grand Prix',           'cz',     'Stadium Marketa',          'Prague',         'Europe/Prague',       '2021-06-05 17:00:00', 3,  353),
  (@season, 4,  'Gorz&oacute;w Grand Prix',  'Gorz&oacute;w',  'FIM Gorz&oacute;w Speedway Grand Prix of Poland',  'pl-grz', 'Edward Jancarz Stadium',   'Gorz&oacute;w',  'Europe/Warsaw',       '2021-06-19 17:00:00', 4,  329),
  (@season, 5,  'British Grand Prix',        'Great Britain',  'FIM British Speedway Grand Prix',                  'gb',     'Principality Stadium',     'Cardiff',        'Europe/London',       '2021-07-17 16:00:00', 5,  272),
  (@season, 6,  'Wroc&lstrok;aw Grand Prix', 'Wroc&lstrok;aw', 'FIM Wroc&lstrok;aw Speedway Grand Prix of Poland', 'pl-wrc', 'Olympic Stadium',          'Wroc&lstrok;aw', 'Europe/Warsaw',       '2021-07-31 17:00:00', 6,  352),
  (@season, 7,  'Warsaw Grand Prix',         'Warsaw',         'FIM Warsaw Speedway Grand Prix of Poland',         'pl-wsw', 'Stadion Narodowy',         'Warsaw',         'Europe/Warsaw',       '2021-08-07 17:00:00', 7,  274),
  (@season, 8,  'Swedish Grand Prix',        'Sweden',         'FIM Swedish Speedway Grand Prix',                  'se',     'G&amp;B Arena',            'M&aring;lilla',  'Europe/Stockholm',    '2021-08-14 17:00:00', 8,  305),
  (@season, 9,  'Russian Grand Prix'  ,      'Russia',         'FIM Russian Speedway Grand Prix',                  'ru',     'Anatoly Stepanov Stadium', 'Togliatti',      'Europe/Samara',       '2021-08-28 17:00:00', 9,  353),
  (@season, 10, 'Danish Grand Prix',         'Denmark',        'FIM Danish Speedway Grand Prix',                   'dk',     'Vojens Speedway Center',   'Vojens',         'Europe/Copenhagen',   '2021-09-11 15:00:00', 10, 300),
  (@season, 11, 'Toru&#324; Grand Prix',     'Toru&#324;',     'FIM Toru&#324; Speedway Grand Prix of Poland',     'pl-trn', 'Marian Rose MotoArena',    'Toru&#324;',     'Europe/Warsaw',       '2021-10-02 17:00:00', 11, 325);

-- Regular Riders
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 95,  39  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1,  39);
# Tai Woffinden
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108, 15  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2,  15);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 66,  9   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,   9);
# Maciej Janowski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 71,  41  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4,  41);
# Leon Madsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 30,  19  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5,  19);
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 69,  115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6, 115);
# Artem Laguta
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 222, 28  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7,  28);
# Emil Sayfutdinov
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 89,  3   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8,   3);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 54,  42  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9,  42);
# Max Fricke
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 46,  127 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10, 127);
# Matej Zagar
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 55,  31  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11,  31);
# Anders Thomsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 105, 121 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12, 121);
# Oliver Berntzon
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 93,  97  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13,  97);
# Krzysztof Kasprzak
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 187, 43  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14,  43);
# Robert Lambert
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 505, 119 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 119);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
