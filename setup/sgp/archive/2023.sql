SET @season := 2023;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'Croatian Grand Prix',      'Croatia',        'hr',     'Speedway Stadion Milenium',   'Gorican',        'Europe/Zagreb',       '2023-04-29 17:00:00', 1,  305),
  (@season, 2,  'Warsaw Grand Prix',        'Warsaw',         'pl-wsw', 'Stadion Narodowy',            'Warsaw',         'Europe/Warsaw',       '2023-05-13 17:00:00', 2,  274),
  (@season, 3,  'Czech Grand Prix',         'Czech Republic', 'cz',     'Stadium Marketa',             'Prague',         'Europe/Prague',       '2023-06-03 17:00:00', 3,  353),
  (@season, 4,  'German Grand Prix',        'Germany',        'de',     'Bergring Arena',              'Teterow',        'Europe/Berlin',       '2023-06-10 17:00:00', 4,  314),
  (@season, 5,  'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w',  'pl-grz', 'Edward Jancarz Stadium',      'Gorz&oacute;w',  'Europe/Warsaw',       '2023-06-24 17:00:00', 5,  329),
  (@season, 6,  'Swedish Grand Prix',       'Sweden',         'se',     'G&amp;B Arena',               'M&aring;lilla',  'Europe/Stockholm',    '2023-07-15 17:00:00', 6,  305),
  (@season, 7,  'Latvian Grand Prix'  ,     'Latvia',         'lv',     'Bikernieki Speedway Stadium', 'Riga',           'Europe/Riga',         '2023-08-12 17:00:00', 7,  373),
  (@season, 8,  'British Grand Prix',       'Great Britain',  'gb',     'Principality Stadium',        'Cardiff',        'Europe/London',       '2023-09-02 16:00:00', 8,  272),
  (@season, 9,  'Danish Grand Prix',        'Denmark',        'dk',     'Vojens Speedway Center',      'Vojens',         'Europe/Copenhagen',   '2023-09-16 17:00:00', 9,  300),
  (@season, 10, 'Toru&#324; Grand Prix',    'Toru&#324;',     'pl-trn', 'Marian Rose MotoArena',       'Toru&#324;',     'Europe/Warsaw',       '2023-09-30 17:00:00', 10, 325);

-- Regular Riders
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  95,  39 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1,  39);
# Leon Madsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  30,  19 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2,  19);
# Maciej Janowski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  71,  41 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,  41);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  66,   9 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4,   9);
# Robert Lambert
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 505, 119 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5, 119);
# Dan Bewley
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  99, 153 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6, 153);
# Patryk Dudek
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 692,  60 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7,  60);
# Tai Woffinden
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108,  15 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8,  15);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  54,  42 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9,  42);
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  69, 115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10, 115);
# Mikkel Michelsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 155,  66 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11,  66);
# Anders Thomsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 105, 121 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12, 121);
# Kim Nilsson
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 233,  78 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13,  78);
# Jack Holder
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  25, 140 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14, 140);
# Max Fricke
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  46, 127 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 127);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
