SET @season := 2018;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'Warsaw Grand Prix',        'Warsaw',         'FIM Warsaw Speedway Grand Prix of Poland',        'plw', 'Stadion Narodowy',         'Warsaw',        'Europe/Warsaw',       '2018-05-12 17:00:00', 1,  274),
  (@season, 2,  'Czech Grand Prix',         'Czech Republic', 'FIM Czech Republic Speedway Grand Prix',          'cz',  'Stadium Marketa',          'Prague',        'Europe/Prague',       '2018-05-26 17:00:00', 2,  353),
  (@season, 3,  'Danish Grand Prix',        'Denmark',        'FIM Danish Speedway Grand Prix',                  'dk',  'CASA Arena',               'Horsens',       'Europe/Copenhagen',   '2018-06-30 17:00:00', 3,  272),
  (@season, 4,  'Swedish Grand Prix',       'Sweden',         'FIM Swedish Speedway Grand Prix',                 'se',  'HZ Bygg Arena',            'Hallstavik',    'Europe/Stockholm',    '2018-07-07 17:00:00', 4,  289),
  (@season, 5,  'British Grand Prix',       'Great Britain',  'FIM British Speedway Grand Prix',                 'gb',  'Principality Stadium',     'Cardiff',       'Europe/London',       '2018-07-21 16:00:00', 5,  272),
  (@season, 6,  'Scandinavian Grand Prix',  'Scandinavia',    'FIM Scandinavian Speedway Grand Prix',            'sca', 'G&B Arena',                'M&aring;lilla', 'Europe/Stockholm',    '2018-08-11 17:00:00', 6,  305),
  (@season, 7,  'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w',  'FIM Gorz&oacute;w Speedway Grand Prix of Poland', 'plg', 'Edward Jancarz Stadium',   'Gorz&oacute;w', 'Europe/Warsaw',       '2018-08-25 17:00:00', 7,  329),
  (@season, 8,  'Slovenien Grand Prix',     'Slovenia',       'FIM Slovenien Speedway Grand Prix',               'si',  'Matije Gubca Stadium',     'Kr&scaron;ko',  'Europe/Ljubljana',    '2018-09-08 17:00:00', 8,  388),
  (@season, 9,  'German Grand Prix',        'Germany',        'FIM German Speedway Grand Prix',                  'de',  'Bergring Arena',           'Teterow',       'Europe/Berlin',       '2018-09-22 17:00:00', 9,  314),
  (@season, 10, 'Toru&#324; Grand Prix',    'Toru&#324;',     'FIM Toru&#324; Speedway Grand Prix of Poland',    'plt', 'Marian Rose MotoArena',    'Toru&#324;',    'Europe/Warsaw',       '2018-10-06 17:00:00', 10, 325),
  (@season, 11, 'Australian Grand Prix',    'Australia',      'FIM Australian Speedway Grand Prix',              'au',  'Etihad Stadium',           'Melbourne',     'Australia/Melbourne', '2018-10-27 08:00:00', 11, 346);


-- Regular Riders
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 69,  115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1, 115);
# Patryk Dudek
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 692, 60  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2,  60);
# Tai Woffinden
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108, 15  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,  15);
# Maciej Janowski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 71,  41  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4,  41);
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 95,  39  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5,  39);
# Emil Sayfutdinov
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 89,  3   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6,   3);
# Matej Zagar
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 55,  31  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7,  31);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 66,  9   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8,   9);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 54,  42  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9,  42);
# Chris Holder
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 23,  12  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10,  12);
# Greg Hancock
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 45,  4   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11,   4);
# Nicki Pedersen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 110, 6   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12,   6);
# Przemyslaw Pawlicki
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 59,  36  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13,  36);
# Artem Laguta
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 222, 28  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14,  28);
# Craig Cook
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 111, 86  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15,  86);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;

