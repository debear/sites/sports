SET @season := 2024;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `flag`, `track`, `location`, `timezone`, `quirks`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'Croatian Grand Prix',       'Croatia',        'hr',     'Speedway Stadion Milenium',   'Gorican',        'Europe/Zagreb',     NULL,           '2024-04-27 17:00:00', 1,  305),
  (@season, 2,  'Warsaw Grand Prix',         'Warsaw',         'pl-wsw', 'Stadion Narodowy',            'Warsaw',         'Europe/Warsaw',     'QUALI_SPRINT', '2024-05-11 17:00:00', 2,  274),
  (@season, 3,  'German Grand Prix',         'Germany',        'de',     'OneSolar Arena',              'Landshut',       'Europe/Berlin',     NULL,           '2024-05-18 17:00:00', 3,  390),
  (@season, 4,  'Czech Grand Prix',          'Czech Republic', 'cz',     'Stadium Marketa',             'Prague',         'Europe/Prague',     NULL,           '2024-06-01 17:00:00', 4,  353),
  (@season, 5,  'Swedish Grand Prix',        'Sweden',         'se',     'Skrotfrag Arena',             'M&aring;lilla',  'Europe/Stockholm',  NULL,           '2024-06-15 17:00:00', 5,  305),
  (@season, 6,  'Gorz&oacute;w Grand Prix',  'Gorz&oacute;w',  'pl-grz', 'Edward Jancarz Stadium',      'Gorz&oacute;w',  'Europe/Warsaw',     NULL,           '2024-06-29 17:00:00', 6,  329),
  (@season, 7,  'British Grand Prix',        'Great Britain',  'gb',     'Principality Stadium',        'Cardiff',        'Europe/London',     'QUALI_SPRINT', '2024-08-17 16:00:00', 7,  272),
  (@season, 8,  'Wroc&lstrok;aw Grand Prix', 'Wroc&lstrok;aw', 'pl-wrc', 'Olympic Stadium',             'Wroc&lstrok;aw', 'Europe/Warsaw',     NULL,           '2024-08-31 17:00:00', 8,  352),
  (@season, 9,  'Latvian Grand Prix',        'Latvia',         'lv',     'Bikernieki Speedway Stadium', 'Riga',           'Europe/Riga',       NULL,           '2024-09-07 14:00:00', 9,  355),
  (@season, 10, 'Danish Grand Prix',         'Denmark',        'dk',     'Vojens Speedway Center',      'Vojens',         'Europe/Copenhagen', NULL,           '2024-09-14 17:00:00', 10, 300),
  (@season, 11, 'Toru&#324; Grand Prix',     'Toru&#324;',     'pl-trn', 'Marian Rose MotoArena',       'Toru&#324;',     'Europe/Warsaw',     NULL,           '2024-09-28 17:00:00', 11, 325);

-- Regular Riders
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  95,  39 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1,  39);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  66,   9 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2,   9);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  54,  42 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,  42);
# Jack Holder
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  25, 140 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4, 140);
# Leon Madsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  30,  19 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5,  19);
# Robert Lambert
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 505, 119 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6, 119);
# Dan Bewley
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  99, 153 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7, 153);
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  69, 115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8, 115);
# Tai Woffinden
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108,  15 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9,  15);
# Mikkel Michelsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 155,  66 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10,  66);
# Andzejs Lebedevs
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  29,  90 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11,  90);
# Kai Huckenbeck
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 744, 136 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12, 136);
# Dominic Kubera
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 415, 156 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13, 156);
# Szymon Wozniak
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  48,  81 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14,  81);
# Jan Kvech
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 201, 174 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 174);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
