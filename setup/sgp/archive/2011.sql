SET @season := 2011;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`)
  VALUES (@season, 1, 'European Grand Prix', 'Europe', 'FIM Fogo European Speedway Grand Prix', 'eu', 'Alfred Smoczyk Stadium', 'Leszno', 'Europe/Warsaw', '2011-04-30 17:00:00', 1, 330),
         (@season, 2, 'Swedish Grand Prix', 'Sweden', 'FIM Meridian Lifts Swedish Speedway Grand Prix', 'se', 'Ullevi', 'Gothenburg', 'Europe/Stockholm', '2011-05-13 17:00:00', 2, 400),
         (@season, 3, 'Czech Grand Prix', 'Czech Republic', 'FIM Mitas Czech Republic Speedway Grand Prix', 'cz', 'Stadium Marketa', 'Prague', 'Europe/Prague', '2011-05-28 17:00:00', 3, 353),
         (@season, 4, 'Danish Grand Prix', 'Denmark', 'FIM Dansk Metal Danish Speedway Grand Prix', 'dk', 'Parken Stadium', 'Copenhagen', 'Europe/Copenhagen', '2011-06-11 17:00:00', 4, 275),
         (@season, 5, 'British Grand Prix', 'Great Britain', 'FIM Doodson British Speedway Grand Prix', 'gb', 'Millenium Stadium', 'Cardiff', 'Europe/London', '2011-06-25 16:00:00', 5, 277),
         (@season, 6, 'Italian Grand Prix', 'Italy', 'FIM Nice Italian Speedway Grand Prix', 'it', 'Pista Olimpia Terenzano', 'Terenzano', 'Europe/Rome', '2011-07-30 18:00:00', 6, 400),
         (@season, 7, 'Scandinavian Grand Prix', 'Scandinavia', 'FIM Scandinavian Speedway Grand Prix', 'sca', 'G&B Arena', 'M&aring;lilla', 'Europe/Stockholm', '2011-08-13 17:00:00', 7, 305),
         (@season, 8, 'Toru&#324; Grand Prix', 'Toru&#324;', 'FIM Toru&#324; Speedway Grand Prix of Poland', 'pl', 'MotoArena Toru&#324;', 'Toru&#324;', 'Europe/Warsaw', '2011-08-27 17:00:00', 8, 325),
         (@season, 9, 'Nordic Grand Prix', 'Nordic', 'FIM Dansk Metal Nordic Speedway Grand Prix', 'ndc', 'Vojens Speedway Center', 'Vojens', 'Europe/Copenhagen', '2011-09-10 17:00:00', 9, 300),
         (@season, 10, 'Croatian Grand Prix', 'Croatia', 'FIM Nice Croatian Speedway Grand Prix', 'hr', 'Stadium Milenium', 'Donji Kraljevec', 'Europe/Zagreb', '2011-09-24 17:00:00', 10, 305),
         (@season, 11, 'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w', 'FIM Gorz&oacute;w Speedway Grand Prix of Poland', 'pl', 'Edward Jancarz Stadium', 'Gorz&oacute;w', 'Europe/Warsaw', '2011-10-08 17:00:00', 11, 329);

-- Regular Riders
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 1, 2 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 2, 13 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 3, 1 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 4, 7 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 5, 4 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 6, 14 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 7, 8 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 8, 12 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 9, 5 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 10, 6 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 11, 9 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 12, 3 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 13, 28 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 14, 17 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 15, 16 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;

INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) SELECT `season`, `bib_no`, `rider_id` FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season AND `round` = 1;

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `bib_no`, 0, NULL FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season AND `round` = 1;

-- Wildcards
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 29 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 1;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 22 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` IN (2, 7);
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 18 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 3;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 30 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 4;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 21 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 5;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 31 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` IN (6, 10);
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 32 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` IN (8, 11);
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 33 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 9;

