SET @season := 2014;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`)
  VALUES (@season, 1, 'New Zealand Grand Prix', 'New Zealand', 'FIM New Zealand Speedway Grand Prix', 'nz', 'Western Springs Stadium', 'Auckland', 'Pacific/Auckland', '2014-04-05 05:00:00', 1, 413),
         (@season, 2, 'European Grand Prix', 'Europe', 'FIM European Speedway Grand Prix', 'eu', 'Polonia Stadium', 'Bydgoszcz', 'Europe/Warsaw', '2014-04-26 17:00:00', 2, 348),
         (@season, 3, 'Finnish Grand Prix', 'Finland', 'FIM Finnish Speedway Grand Prix', 'fi', 'Ratina Stadium', 'Tampere', 'Europe/Helsinki', '2014-05-17 16:00:00', 3, 0),
         (@season, 4, 'Czech Grand Prix', 'Czech Republic', 'FIM Czech Republic Speedway Grand Prix', 'cz', 'Stadium Marketa', 'Prague', 'Europe/Prague', '2014-05-31 17:00:00', 4, 353),
         (@season, 5, 'Swedish Grand Prix', 'Sweden', 'FIM Swedish Speedway Grand Prix', 'se', 'G&B Arena', 'M&aring;lilla', 'Europe/Stockholm', '2014-06-14 17:00:00', 5, 305),
         (@season, 6, 'Danish Grand Prix', 'Denmark', 'FIM Danish Speedway Grand Prix', 'dk', 'Parken Stadium', 'Copenhagen', 'Europe/Copenhagen', '2014-06-28 17:00:00', 6, 274),
         (@season, 7, 'British Grand Prix', 'Great Britain', 'FIM British Speedway Grand Prix', 'gb', 'Millenium Stadium', 'Cardiff', 'Europe/London', '2014-07-12 16:00:00', 7, 277),
         (@season, 8, 'Latvian Grand Prix', 'Latvia', 'FIM Latvian Speedway Grand Prix', 'lv', 'Bi&#311;ernieku Sport Complex', 'Riga', 'Europe/Riga', '2014-08-16 16:00:00', 8, 0),
         (@season, 9, 'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w', 'FIM Gorz&oacute;w Speedway Grand Prix of Poland', 'pl', 'Edward Jancarz Stadium', 'Gorz&oacute;w', 'Europe/Warsaw', '2014-08-30 17:00:00', 9, 329),
         (@season, 10, 'Nordic Grand Prix', 'Noridc', 'FIM Nordic Speedway Grand Prix', 'ndc', 'Vojens Speedway Center', 'Vojens', 'Europe/Copenhagen', '2014-09-13 17:00:00', 10, 300),
         (@season, 11, 'Scandinavian Grand Prix', 'Scandinavia', 'FIM Scandinavian Speedway Grand Prix', 'sca', 'Friends Arena', 'Stockholm', 'Europe/Stockholm', '2014-09-27 17:00:00', 11, 275),
         (@season, 12, 'Toru&#324; Grand Prix', 'Toru&#324;', 'FIM Toru&#324; Speedway Grand Prix of Poland', 'pl', 'MotoArena Toru&#324;', 'Toru&#324;', 'Europe/Warsaw', '2014-10-11 17:00:00', 12, 325);

-- Regular Riders
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 1,   15  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 33,  13  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 88,  24  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 45,  4   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 5,   6   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 55,  31  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 43,  32  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 507, 43  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 66,  9   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 23,  12  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 100, 5   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 91,  8   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 84,  100 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 37,  14  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 75,  99  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;

INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 1,  15);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 2,  13);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 3,  24);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 4,  4);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 5,  6);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 6,  31);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 7,  32);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 8,  43);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 9,  9);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10, 12);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11, 5);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12, 8);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13, 100);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14, 14);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 99);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;

