SET @season := 2019;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'Warsaw Grand Prix',         'Warsaw',         'FIM Warsaw Speedway Grand Prix of Poland',         'plw', 'Stadion Narodowy',       'Warsaw',         'Europe/Warsaw',       '2019-05-18 17:00:00', 1,  274),
  (@season, 8,  'Slovenien Grand Prix',      'Slovenia',       'FIM Slovenien Speedway Grand Prix',                'si',  'Matije Gubca Stadium',   'Kr&scaron;ko',   'Europe/Ljubljana',    '2019-06-01 17:00:00', 2,  388),
  (@season, 2,  'Czech Grand Prix',          'Czech Republic', 'FIM Czech Republic Speedway Grand Prix',           'cz',  'Stadium Marketa',        'Prague',         'Europe/Prague',       '2019-06-15 17:00:00', 3,  353),
  (@season, 4,  'Swedish Grand Prix',        'Sweden',         'FIM Swedish Speedway Grand Prix',                  'se',  'HZ Bygg Arena',          'Hallstavik',     'Europe/Stockholm',    '2019-07-06 17:00:00', 4,  289),
  (@season, 7,  'Wroc&lstrok;aw Grand Prix', 'Wroc&lstrok;aw', 'FIM Wroc&lstrok;aw Speedway Grand Prix of Poland', 'plw', 'Olympic Stadium',        'Wroc&lstrok;aw', 'Europe/Warsaw',       '2019-08-03 17:00:00', 5,  352),
  (@season, 6,  'Scandinavian Grand Prix',   'Scandinavia',    'FIM Scandinavian Speedway Grand Prix',             'sca', 'G&B Arena',              'M&aring;lilla',  'Europe/Stockholm',    '2019-08-17 17:00:00', 6,  305),
  (@season, 9,  'German Grand Prix',         'Germany',        'FIM German Speedway Grand Prix',                   'de',  'Bergring Arena',         'Teterow',        'Europe/Berlin',       '2019-08-31 17:00:00', 7,  314),
  (@season, 3,  'Danish Grand Prix',         'Denmark',        'FIM Danish Speedway Grand Prix',                   'dk',  'Vojens Speedway Center', 'Vojens',         'Europe/Copenhagen',   '2019-09-07 17:00:00', 8,  272),
  (@season, 5,  'British Grand Prix',        'Great Britain',  'FIM British Speedway Grand Prix',                  'gb',  'Principality Stadium',   'Cardiff',        'Europe/London',       '2019-09-21 16:00:00', 9,  272),
  (@season, 10, 'Toru&#324; Grand Prix',     'Toru&#324;',     'FIM Toru&#324; Speedway Grand Prix of Poland',     'plt', 'Marian Rose MotoArena',  'Toru&#324;',     'Europe/Warsaw',       '2019-10-05 17:00:00', 10, 325);

-- Regular Riders
# Tai Woffinden
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108, 15  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1,  15);
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 95,  39  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2,  39);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 66,  9   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,   9);
# Maciej Janowski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 71,  41  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4,  41);
# Greg Hancock
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 45,  4   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5,   4);
# Artem Laguta
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 222, 28  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6,  28);
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 69,  115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7, 115);
# Emil Sayfutdinov
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 89,  3   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8,   3);
# Patryk Dudek
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 692, 60  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9,  60);
# Matej Zagar
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 55,  31  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10,  31);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 54,  42  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11,  42);
# Janusz Kolodziej
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 333, 16  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12,  16);
# Niels-Kristian Iversen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 88,  24  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13,  24);
# Antonio Lindback
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 85,  17  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14,  17);
# Leon Madsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 30,  19  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15,  19);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;

