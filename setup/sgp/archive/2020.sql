SET @season := 2020;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'Warsaw Grand Prix',         'Warsaw',         'FIM Warsaw Speedway Grand Prix of Poland',         'pl-wsw', 'Stadion Narodowy',         'Warsaw',         'Europe/Warsaw',       '2020-05-16 17:00:00', 1,  274),
  (@season, 2,  'German Grand Prix',         'Germany',        'FIM German Speedway Grand Prix',                   'de',     'Bergring Arena',           'Teterow',        'Europe/Berlin',       '2020-05-30 17:00:00', 2,  314),
  (@season, 3,  'Czech Grand Prix',          'Czech Republic', 'FIM Czech Republic Speedway Grand Prix',           'cz',     'Stadium Marketa',          'Prague',         'Europe/Prague',       '2020-06-03 17:00:00', 3,  353),
  (@season, 4,  'British Grand Prix',        'Great Britain',  'FIM British Speedway Grand Prix',                  'gb',     'Principality Stadium',     'Cardiff',        'Europe/London',       '2020-07-18 16:00:00', 4,  272),
  (@season, 5,  'Swedish Grand Prix',        'Sweden',         'FIM Swedish Speedway Grand Prix',                  'se',     'Credentia Arena',          'Hallstavik',     'Europe/Stockholm',    '2020-07-25 17:00:00', 5,  289),
  (@season, 6,  'Wroc&lstrok;aw Grand Prix', 'Wroc&lstrok;aw', 'FIM Wroc&lstrok;aw Speedway Grand Prix of Poland', 'pl-wrc', 'Olympic Stadium',          'Wroc&lstrok;aw', 'Europe/Warsaw',       '2020-08-01 17:00:00', 6,  352),
  (@season, 7,  'Scandinavian Grand Prix',   'Scandinavia',    'FIM Scandinavian Speedway Grand Prix',             'sca',    'Skrotfrag Arena',          'M&aring;lilla',  'Europe/Stockholm',    '2020-08-15 17:00:00', 7,  305),
  (@season, 8,  'Russian Grand Prix'  ,      'Russia',         'FIM Russian Speedway Grand Prix',                  'ru',     'Anatoly Stepanov Stadium', 'Togliatti',      'Europe/Samara',       '2020-08-29 17:00:00', 8,  353),
  (@season, 9,  'Danish Grand Prix',         'Denmark',        'FIM Danish Speedway Grand Prix',                   'dk',     'Vojens Speedway Center',   'Vojens',         'Europe/Copenhagen',   '2020-09-12 15:00:00', 9,  300),
  (@season, 10, 'Toru&#324; Grand Prix',     'Toru&#324;',     'FIM Toru&#324; Speedway Grand Prix of Poland',     'pl-trn', 'Marian Rose MotoArena',    'Toru&#324;',     'Europe/Warsaw',       '2020-10-03 17:00:00', 10, 325);

-- Regular Riders
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 95,  39  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1,  39);
# Leon Madsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 30,  19  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2,  19);
# Emil Sayfutdinov
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 89,  3   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,   3);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 66,  9   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4,   9);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 54,  42  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5,  42);
# Maciej Janowski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 71,  41  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6,  41);
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 69,  115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7, 115);
# Patryk Dudek
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 692, 60  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8,  60);
# Matej Zagar
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 55,  31  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9,  31);
# Niels-Kristian Iversen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 88,  24  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10,  24);
# Artem Laguta
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 222, 28  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11,  28);
# Antonio Lindback
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 85,  17  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12,  17);
# Tai Woffinden
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108, 15  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13,  15);
# Max Fricke
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 46,  127 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14, 127);
# Martin Smolinski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 84,  100 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 100);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
