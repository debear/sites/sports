SET @season := 2022;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`) VALUES
  (@season, 1,  'Croatian Grand Prix',       'Croatia',        'FIM Speedway Grand Prix of Croatia',               'hr',     'Speedway Stadion Milenium', 'Gorican',        'Europe/Zagreb',       '2022-04-30 17:00:00', 1,  305),
  (@season, 2,  'Warsaw Grand Prix',         'Warsaw',         'FIM Warsaw Speedway Grand Prix of Poland',         'pl-wsw', 'Stadion Narodowy',          'Warsaw',         'Europe/Warsaw',       '2022-05-14 17:00:00', 2,  274),
  (@season, 3,  'Czech Grand Prix',          'Czech Republic', 'FIM Czech Republic Speedway Grand Prix',           'cz',     'Stadium Marketa',           'Prague',         'Europe/Prague',       '2022-05-04 17:00:00', 3,  353),
  (@season, 4,  'German Grand Prix',         'Germany',        'FIM German Speedway Grand Prix',                   'de',     'Bergring Arena',            'Teterow',        'Europe/Berlin',       '2022-05-25 17:00:00', 4,  314),
  (@season, 5,  'Gorz&oacute;w Grand Prix',  'Gorz&oacute;w',  'FIM Gorz&oacute;w Speedway Grand Prix of Poland',  'pl-grz', 'Edward Jancarz Stadium',    'Gorz&oacute;w',  'Europe/Warsaw',       '2022-06-09 17:00:00', 5,  329),
  (@season, 6,  'Russian Grand Prix'  ,      'Russia',         'FIM Russian Speedway Grand Prix',                  'ru',     'Anatoly Stepanov Stadium',  'Togliatti',      'Europe/Samara',       '2022-06-13 15:00:00', 6,  353),
  (@season, 7,  'British Grand Prix',        'Great Britain',  'FIM British Speedway Grand Prix',                  'gb',     'Principality Stadium',      'Cardiff',        'Europe/London',       '2022-07-27 16:00:00', 7,  272),
  (@season, 8,  'Wroc&lstrok;aw Grand Prix', 'Wroc&lstrok;aw', 'FIM Wroc&lstrok;aw Speedway Grand Prix of Poland', 'pl-wrc', 'Olympic Stadium',           'Wroc&lstrok;aw', 'Europe/Warsaw',       '2022-08-10 17:00:00', 8,  352),
  (@season, 9,  'Danish Grand Prix',         'Denmark',        'FIM Danish Speedway Grand Prix',                   'dk',     'Vojens Speedway Center',    'Vojens',         'Europe/Copenhagen',   '2022-08-11 17:00:00', 9,  300),
  (@season, 10, 'Swedish Grand Prix',        'Sweden',         'FIM Swedish Speedway Grand Prix',                  'se',     'G&amp;B Arena',             'M&aring;lilla',  'Europe/Stockholm',    '2022-09-17 17:00:00', 10, 305),
  (@season, 11, 'Toru&#324; Grand Prix',     'Toru&#324;',     'FIM Toru&#324; Speedway Grand Prix of Poland',     'pl-trn', 'Marian Rose MotoArena',     'Toru&#324;',     'Europe/Warsaw',       '2022-10-01 17:00:00', 11, 325),
  (@season, 12, 'Oceania Grand Prix',        'Oceania',        'FIM Oceania Speedway Grand Prix',                  'au',     'TBD',                       'TBD',            'Australia/Melbourne', '2022-11-05 08:00:00', 12, 400);

-- Regular Riders
# Bartosz Zmarzlik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  95,  39 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  1,  39);
# Maciej Janowski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  71,  41 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  2,  41);
# Fredrik Lindgren
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  66,   9 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  3,   9);
# Tai Woffinden
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108,  15 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  4,  15);
# Leon Madsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  30,  19 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  5,  19);
# Max Fricke
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  46, 127 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  6, 127);
# Jason Doyle
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  69, 115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  7, 115);
# Robert Lambert
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 505, 119 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  8, 119);
# Anders Thomsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 105, 121 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season,  9, 121);
# Martin Vaculik
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  54,  42 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10,  42);
# Pawel Przedpelski
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 323,  98 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11,  98);
# Mikkel Michelsen
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 155,  66 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12,  66);
# Patryk Dudek
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 692,  60 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13,  60);
# Jack Holder
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  25, 140 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14, 140);
# Dan Bewley
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`,  99, 153 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 153);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
