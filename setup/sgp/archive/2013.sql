SET @season := 2013;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`)
  VALUES (@season, 1, 'New Zealand Grand Prix', 'New Zealand', 'FIM Buckley Systems New Zealand Speedway Grand Prix', 'nz', 'Western Springs Stadium', 'Auckland', 'Pacific/Auckland', '2013-03-23 03:00:00', 1, 413),
         (@season, 2, 'European Grand Prix', 'Europe', 'FIM Motorsportwash.com European Speedway Grand Prix', 'eu', 'Polonia Stadium', 'Bydgoszcz', 'Europe/Warsaw', '2013-04-20 17:00:00', 2, 348),
         (@season, 3, 'Swedish Grand Prix', 'Sweden', 'FIM Fogo Swedish Speedway Grand Prix', 'se', 'Ullevi', 'Gothenburg', 'Europe/Stockholm', '2013-05-04 17:00:00', 3, 400),
         (@season, 4, 'Czech Grand Prix', 'Czech Republic', 'FIM Mitas Czech Republic Speedway Grand Prix', 'cz', 'Stadium Marketa', 'Prague', 'Europe/Prague', '2013-05-18 17:00:00', 4, 353),
         (@season, 5, 'British Grand Prix', 'Great Britain', 'FIM British Speedway Grand Prix', 'gb', 'Millenium Stadium', 'Cardiff', 'Europe/London', '2013-06-01 16:00:00', 5, 277),
         (@season, 6, 'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w', 'FIM Gorz&oacute;w Speedway Grand Prix of Poland', 'pl', 'Edward Jancarz Stadium', 'Gorz&oacute;w', 'Europe/Warsaw', '2013-06-15 17:00:00', 6, 329),
         (@season, 7, 'Danish Grand Prix', 'Denmark', 'FIM Danish Speedway Grand Prix', 'dk', 'Parken Stadium', 'Copenhagen', 'Europe/Copenhagen', '2013-06-29 17:00:00', 7, 275),
         (@season, 8, 'Italian Grand Prix', 'Italy', 'FIM Fogo Italian Speedway Grand Prix', 'it', 'Pista Olimpia Terenzano', 'Terenzano', 'Europe/Rome', '2013-08-03 17:00:00', 8, 400),
         (@season, 9, 'Latvian Grand Prix', 'Latvia', 'FIM Latvian Speedway Grand Prix', 'lv', 'Spidveja Centrs', 'Daugavpils', 'Europe/Riga', '2013-08-17 17:00:00', 9, 375),
         (@season, 10, 'Slovenian Grand Prix', 'Slovenia', 'FIM Slovenian Speedway Grand Prix', 'si', 'Matije Gubca Stadium', 'Kr&#353;ko', 'Europe/Ljubljana', '2013-09-07 17:00:00', 10, 387),
         (@season, 11, 'Scandinavian Grand Prix', 'Scandinavia', 'FIM Scandinavian Speedway Grand Prix', 'sca', 'G&B Arena', 'M&aring;lilla', 'Europe/Stockholm', '2013-09-21 17:00:00', 11, 305),
         (@season, 12, 'Toru&#324; Grand Prix', 'Toru&#324;', 'FIM Toru&#324; Speedway Grand Prix of Poland', 'pl', 'MotoArena Toru&#324;', 'Toru&#324;', 'Europe/Warsaw', '2013-10-05 17:00:00', 12, 325);

-- Regular Riders
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 1, 12 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 2, 6 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 3, 4 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 4, 2 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 5, 3 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 6, 17 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 7, 9 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 8, 5 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 9, 42 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 10, 13 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 11, 43 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 12, 31 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 13, 24 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 14, 15 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 15, 32 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;

INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) SELECT `season`, `bib_no`, `rider_id` FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season AND `round` = 1;

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `bib_no`, 0, NULL FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season AND `round` = 1;

-- Wildcards
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 35 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 1;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 44 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 2;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 3;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 4;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 5;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 6;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 7;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 8;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 9;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 10;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 11;
-- INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16,  FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 12;

