SET @season := 2012;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`)
  VALUES (@season, 1, 'New Zealand Grand Prix', 'New Zealand', 'FIM Buckley Systems New Zealand Speedway Grand Prix', 'nz', 'Western Springs Stadium', 'Auckland', 'Pacific/Auckland', '2012-03-31 03:00:00', 1, 413),
         (@season, 2, 'European Grand Prix', 'Europe', 'FIM Fogo European Speedway Grand Prix', 'eu', 'Alfred Smoczyk Stadium', 'Leszno', 'Europe/Warsaw', '2012-04-28 17:00:00', 2, 330),
         (@season, 3, 'Czech Grand Prix', 'Czech Republic', 'FIM Mitas Czech Republic Speedway Grand Prix', 'cz', 'Stadium Marketa', 'Prague', 'Europe/Prague', '2012-05-12 17:00:00', 3, 353),
         (@season, 4, 'Swedish Grand Prix', 'Sweden', 'FIM Swedish Speedway Grand Prix', 'se', 'Ullevi', 'Gothenburg', 'Europe/Stockholm', '2012-05-26 17:00:00', 4, 400),
         (@season, 5, 'Danish Grand Prix', 'Denmark', 'FIM Dansk Metal Danish Speedway Grand Prix', 'dk', 'Parken Stadium', 'Copenhagen', 'Europe/Copenhagen', '2012-06-09 17:00:00', 5, 275),
         (@season, 6, 'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w', 'FIM Gorzow Speedway Grand Prix of Poland', 'pl', 'Edward Jancarz Stadium', 'Gorz&oacute;w', 'Europe/Warsaw', '2012-06-23 17:00:00', 6, 329),
         (@season, 7, 'Croatian Grand Prix', 'Croatia', 'FIM Fogo Croatian Speedway Grand Prix', 'hr', 'Stadium Milenium', 'Donji Kraljevec', 'Europe/Zagreb', '2012-07-28 17:00:00', 7, 305),
         (@season, 8, 'Italian Grand Prix', 'Italy', 'FIM Fogo Italian Speedway Grand Prix', 'it', 'Pista Olimpia Terenzano', 'Terenzano', 'Europe/Rome', '2012-08-11 18:00:00', 8, 400),
         (@season, 9, 'British Grand Prix', 'Great Britain', 'FIM Fogo British Speedway Grand Prix', 'gb', 'Millenium Stadium', 'Cardiff', 'Europe/London', '2012-08-25 17:00:00', 9, 277),
         (@season, 10, 'Scandinavian Grand Prix', 'Scandinavia', 'FIM Scandinavian Speedway Grand Prix', 'sca', 'G&B Arena', 'M&aring;lilla', 'Europe/Stockholm', '2012-09-08 17:00:00', 10, 305),
         (@season, 11, 'Nordic Grand Prix', 'Nordic', 'FIM Dansk Metal Nordic Speedway Grand Prix', 'ndc', 'Vojens Speedway Center', 'Vojens', 'Europe/Copenhagen', '2012-09-22 17:00:00', 11, 300),
         (@season, 12, 'Toru&#324; Grand Prix', 'Toru&#324;', 'FIM Toru&#324; Speedway Grand Prix of Poland', 'pl', 'MotoArena Toru&#324;', 'Toru&#324;', 'Europe/Warsaw', '2012-10-06 17:00:00', 12, 325);

-- Regular Riders
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 1, 4 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 2, 5 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 3, 13 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 4, 1 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 5, 2 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 6, 3 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 7, 8 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 8, 12 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 9, 9 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 10, 6 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 11, 14 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 12, 17 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 13, 33 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 14, 34 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 15, 10 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;

INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) SELECT `season`, `bib_no`, `rider_id` FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season AND `round` = 1;

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `bib_no`, 0, NULL FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season AND `round` = 1;

-- Wildcards
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 35 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 1;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 36 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 2;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 37 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 3;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 22 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` IN (4, 10);
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 38 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` IN (5, 11);
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 39 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 6;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 23 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 7;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 40 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 8;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 21 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 9;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 16, 41 FROM `SPORTS_SGP_RACES` WHERE `season` = @season AND `round` = 12;

