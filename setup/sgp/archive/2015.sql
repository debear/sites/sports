SET @season := 2015;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`)
  VALUES (@season, 1, 'Warsaw Grand Prix', 'Warsaw', 'FIM Warsaw Speedway Grand Prix of Poland', 'plw', 'Stadion Narodowy', 'Warsaw', 'Europe/Warsaw', '2015-04-18 17:00:00', 1, 0),
         (@season, 2, 'Finnish Grand Prix', 'Finland', 'FIM Finnish Speedway Grand Prix', 'fi', 'Ratina Stadium', 'Tampere', 'Europe/Helsinki', '2015-05-16 16:00:00', 2, 400),
         (@season, 3, 'Czech Grand Prix', 'Czech Republic', 'FIM Czech Republic Speedway Grand Prix', 'cz', 'Stadium Marketa', 'Prague', 'Europe/Prague', '2015-05-23 17:00:00', 3, 353),
         (@season, 4, 'British Grand Prix', 'Great Britain', 'FIM British Speedway Grand Prix', 'gb', 'Millenium Stadium', 'Cardiff', 'Europe/London', '2015-07-04 16:00:00', 4, 278),
         (@season, 5, 'Latvian Grand Prix', 'Latvia', 'FIM Latvian Speedway Grand Prix', 'lv', 'Lokomotiv Stadium', 'Daugavpils', 'Europe/Riga', '2015-07-18 16:00:00', 5, 0),
         (@season, 6, 'Swedish Grand Prix', 'Sweden', 'FIM Swedish Speedway Grand Prix', 'se', 'G&B Arena', 'M&aring;lilla', 'Europe/Stockholm', '2015-07-25 17:00:00', 6, 305),
         (@season, 7, 'Danish Grand Prix', 'Denmark', 'FIM Danish Speedway Grand Prix', 'dk', 'CASA Arena', 'Horsens', 'Europe/Copenhagen', '2015-08-08 17:00:00', 7, 0),
         (@season, 8, 'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w', 'FIM Gorz&oacute;w Speedway Grand Prix of Poland', 'plg', 'Stal Gorz&oacute;w Speedway Club', 'Gorz&oacute;w', 'Europe/Warsaw', '2015-08-29 17:00:00', 8, 0),
         (@season, 9, 'Slovenien Grand Prix', 'Slovenia', 'FIM Slovenien Speedway Grand Prix', 'si', 'Friends Arena', 'Kr&scaron;ko', 'Europe/Ljubljana', '2015-09-12 17:00:00', 9, 0),
         (@season, 10, 'Scandinavian Grand Prix', 'Scandinavia', 'FIM Scandinavian Speedway Grand Prix', 'sca', 'Friends Arena', 'Stockholm', 'Europe/Stockholm', '2015-09-26 17:00:00', 10, 275),
         (@season, 11, 'Toru&#324; Grand Prix', 'Toru&#324;', 'FIM Toru&#324; Speedway Grand Prix of Poland', 'plt', 'MotoArena Toru&#324;', 'Toru&#324;', 'Europe/Warsaw', '2015-10-03 17:00:00', 11, 325),
         (@season, 12, 'Australian Grand Prix', 'Australia', 'FIM Australian Speedway Grand Prix', 'au', 'Etihad Stadium', 'Melbourne', 'Australia/Melbourne', '2015-10-24 08:00:00', 12, 0);

-- Regular Riders
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 45,  4   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 507, 43  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 3,   6   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108, 15  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 55,  31  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 100, 5   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 23,  12  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 33,  13  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 75,  99  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 88,  24  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 37,  14  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 69,  115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 71,  41  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 52,  38  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 30,  22  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;

INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 1,  4);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 2,  43);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 3,  6);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 4,  15);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 5,  31);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 6,  5);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 7,  12);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 8,  13);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 9,  99);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10, 24);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11, 14);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12, 115);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13, 41);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14, 38);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 22);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;

