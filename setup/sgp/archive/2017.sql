SET @season := 2017;
DELETE FROM `SPORTS_SGP_RACES` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RACE_RIDERS` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;
DELETE FROM `SPORTS_SGP_RIDER_HISTORY` WHERE `season` = @season;

-- Races
INSERT INTO `SPORTS_SGP_RACES` (`season`, `round`, `name_full`, `name_short`, `name_official`, `flag`, `track`, `location`, `timezone`, `race_time`, `round_order`, `track_length`)
  VALUES (@season, 1,  'Slovenien Grand Prix',     'Slovenia',       'FIM Slovenien Speedway Grand Prix',               'si',  'Matije Gubca Stadium',     'Kr&scaron;ko',  'Europe/Ljubljana',    '2017-04-29 17:00:00', 1,  388),
         (@season, 2,  'Warsaw Grand Prix',        'Warsaw',         'FIM Warsaw Speedway Grand Prix of Poland',        'plw', 'Stadion Narodowy',         'Warsaw',        'Europe/Warsaw',       '2017-05-13 17:00:00', 2,  274),
         (@season, 3,  'Latvian Grand Prix',       'Latvia',         'FIM Czech Republic Speedway Grand Prix',          'lv',  'Latvijas Spīdveja Centrs', 'Daugavpils',    'Europe/Riga',         '2017-05-27 16:00:00', 3,  373),
         (@season, 4,  'Czech Grand Prix',         'Czech Republic', 'FIM Czech Republic Speedway Grand Prix',          'cz',  'Stadium Marketa',          'Prague',        'Europe/Prague',       '2017-06-10 17:00:00', 4,  353),
         (@season, 5,  'Danish Grand Prix',        'Denmark',        'FIM Danish Speedway Grand Prix',                  'dk',  'CASA Arena',               'Horsens',       'Europe/Copenhagen',   '2017-06-24 17:00:00', 5,  272),
         (@season, 6,  'British Grand Prix',       'Great Britain',  'FIM British Speedway Grand Prix',                 'gb',  'Principality Stadium',     'Cardiff',       'Europe/London',       '2017-07-22 16:00:00', 6,  272),
         (@season, 7,  'Swedish Grand Prix',       'Sweden',         'FIM Swedish Speedway Grand Prix',                 'se',  'G&B Arena',                'M&aring;lilla', 'Europe/Stockholm',    '2017-08-12 17:00:00', 7,  305),
         (@season, 8,  'Gorz&oacute;w Grand Prix', 'Gorz&oacute;w',  'FIM Gorz&oacute;w Speedway Grand Prix of Poland', 'plg', 'Edward Jancarz Stadium',   'Gorz&oacute;w', 'Europe/Warsaw',       '2017-08-26 17:00:00', 8,  329),
         (@season, 9,  'German Grand Prix',        'Germany',        'FIM German Speedway Grand Prix',                  'de',  'Bergring Arena',           'Teterow',       'Europe/Berlin',       '2017-09-09 17:00:00', 9,  314),
         (@season, 10, 'Scandinavian Grand Prix',  'Scandinavia',    'FIM Scandinavian Speedway Grand Prix',            'sca', 'Friends Arena',            'Stockholm',     'Europe/Stockholm',    '2017-09-23 17:00:00', 10, 272),
         (@season, 11, 'Toru&#324; Grand Prix',    'Toru&#324;',     'FIM Toru&#324; Speedway Grand Prix of Poland',    'plt', 'Marian Rose MotoArena',    'Toru&#324;',    'Europe/Warsaw',       '2017-10-07 17:00:00', 11, 325),
         (@season, 12, 'Australian Grand Prix',    'Australia',      'FIM Australian Speedway Grand Prix',              'au',  'Etihad Stadium',           'Melbourne',     'Australia/Melbourne', '2017-10-28 08:00:00', 12, 346);


-- Regular Riders
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 45,  4   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 108, 15  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 95,  39  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 23,  12  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 69,  115 FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 777, 64  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 85,  17  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 88,  24  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 55,  31  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 71,  41  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 66,  9   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 12,  6   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 692, 60  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 54,  42  FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;
INSERT INTO `SPORTS_SGP_RACE_RIDERS` (`season`, `round`, `bib_no`, `rider_id`) SELECT `season`, `round`, 89,  3   FROM `SPORTS_SGP_RACES` WHERE `season` = @season ORDER BY `round_order`;


INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 1,  4);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 2,  15);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 3,  39);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 4,  12);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 5,  115);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 6,  64);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 7,  17);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 8,  24);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 9,  31);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 10, 41);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 11, 9);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 12, 6);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 13, 60);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 14, 42);
INSERT INTO `SPORTS_SGP_RIDER_RANKING` (`season`, `ranking`, `rider_id`) VALUES (@season, 15, 3);

-- Initial standings
INSERT INTO `SPORTS_SGP_RIDER_HISTORY` (`season`, `rider_id`, `pos`, `pts`, `pos_raceoff`) SELECT `season`, `rider_id`, `ranking`, 0, NULL FROM `SPORTS_SGP_RIDER_RANKING` WHERE `season` = @season;

