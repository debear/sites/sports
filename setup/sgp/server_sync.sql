SET @season := 2025;

#
# SGP Results
#
SET @sync_app := 'sports_sgp';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports SGP', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  4, '__DIR__ SPORTS_SGP_RACES', 1, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_SGP_RACE_HEATS', 2, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_SGP_RACE_RESULT', 3, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_SGP_RACE_RIDERS', 4, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_SGP_RIDER_HISTORY', 5, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_SGP_RIDER_HISTORY_PROGRESS', 6, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_SGP_RIDER_RANKING', 7, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_SGP_RIDER_STATS', 8, 'database'),
                        (@sync_app, 10, '__DIR__ Rider List', 9, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES # IDs 1 - 3 Archived
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_SGP_RACES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_SGP_RACE_HEATS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_SGP_RACE_RESULT', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_SGP_RACE_RIDERS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 8, 'debearco_sports', 'SPORTS_SGP_RIDER_HISTORY', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 9, 'debearco_sports', 'SPORTS_SGP_RIDER_HISTORY_PROGRESS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_SGP_RIDER_STATS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_SGP_RIDER_RANKING', CONCAT('season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 10, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_sgp_entries __REMOTE__');

#
# SGP Riders
#
SET @sync_app := 'sports_sgp_entries';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports SGP Entries', 'data,live', 'dev,data,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_SGP_RACE_RIDERS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_SGP_RIDER_GP_HISTORY', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_SGP_RIDER_HISTORY', 3, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_SGP_RIDER_HISTORY_PROGRESS', 4, 'database'),
                        (@sync_app, 7, '__DIR__ SPORTS_SGP_RIDER_STATS', 5, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_SPEEDWAY_RIDERS', 6, 'database'),
                        (@sync_app, 8, '__DIR__ FANTASY_COMMON_MOTORS_SELECTIONS_STATUS', 7, 'database'),
                        (@sync_app, 6, '__DIR__ Rider Mugshots', 8, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_SGP_RACE_RIDERS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_SGP_RIDER_GP_HISTORY', ''),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_SGP_RIDER_HISTORY', ''),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_SGP_RIDER_HISTORY_PROGRESS', ''),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_SPEEDWAY_RIDERS', ''),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_SGP_RIDER_STATS', ''),
                           (@sync_app, 8, 'debearco_fantasy', 'FANTASY_COMMON_MOTORS_SELECTIONS_STATUS', 'sport = ''sgp''');
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                      VALUES (@sync_app, 6, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ cdn_sports_sgp __REMOTE__');

