SET @sync_app := 'sports';

#
# Sports News
#
SET @sync_app := 'sports_news';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports News', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ NEWS_SOURCES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ NEWS_ARTICLES', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause, clear_first)
                    VALUES (@sync_app, 1, 'debearco_common', 'NEWS_SOURCES', 'app LIKE ''sports_%''', 0),
                           (@sync_app, 2, 'debearco_common', 'NEWS_ARTICLES', 'app LIKE ''sports_%''', 1);

#
# Major League Odds
#
SET @sync_app := 'sports_odds';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports Odds', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_COMMON_MAJORS_ODDS', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_COMMON_MAJORS_ODDS', '');
