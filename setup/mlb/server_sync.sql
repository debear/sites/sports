# Season to sync...
SET @season := 2025;

#
# General MLB update
#
SET @sync_app := 'sports_mlb';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  2, '__DIR__ SPORTS_MLB_GAME_LINESCORE',                    1, 'database'),
                        (@sync_app, 54, '__DIR__ SPORTS_MLB_GAME_LINEUPS',                      2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_MLB_GAME_PITCHERS',                     3, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_MLB_GAME_ROSTERS',                      4, 'database'),
                        (@sync_app, 59, '__DIR__ SPORTS_MLB_GAME_SCORING',                      5, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_MLB_GAME_UMPIRES',                      6, 'database'),
                        (@sync_app, 60, '__DIR__ SPORTS_MLB_GAME_WINPROB',                      7, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_MLB_UMPIRES',                           8, 'database'),
                        (@sync_app, 57, '__DIR__ SPORTS_MLB_UMPIRES_SEASON_ZONES',              9, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_MLB_PLAYERS_GAME_BATTING',             10, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D',         11, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_MLB_PLAYERS_GAME_FIELDING',            12, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D',        13, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_MLB_PLAYERS_GAME_PITCHING',            14, 'database'),
                        (@sync_app, 13, '__DIR__ SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D',        15, 'database'),
                        (@sync_app, 21, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_MISC',              16, 'database'),
                        (@sync_app, 22, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTING',           17, 'database'),
                        (@sync_app, 23, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_FIELDING',          18, 'database'),
                        (@sync_app, 24, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_PITCHING',          19, 'database'),
                        (@sync_app, 25, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_MISC_SORTED',       20, 'database'),
                        (@sync_app, 26, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED',    21, 'database'),
                        (@sync_app, 27, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED',   22, 'database'),
                        (@sync_app, 28, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED',   23, 'database'),
                        (@sync_app, 29, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS',    24, 'database'),
                        (@sync_app, 30, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS',   25, 'database'),
                        (@sync_app, 31, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS',   26, 'database'),
                        (@sync_app, 32, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER', 27, 'database'),
                        (@sync_app, 55, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTING_ZONES',     28, 'database'),
                        (@sync_app, 56, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_PITCHING_ZONES',    29, 'database'),
                        (@sync_app, 53, '__DIR__ SPORTS_MLB_PLAYERS_SPLIT_LABELS',             30, 'database'),
                        (@sync_app, 58, '__DIR__ SPORTS_MLB_PLAYERS_REPERTOIRE',               31, 'database'),
                        (@sync_app, 33, '__DIR__ SPORTS_MLB_STANDINGS',                        32, 'database'),
                        (@sync_app, 34, '__DIR__ SPORTS_MLB_TEAMS_GAME_BATTING',               33, 'database'),
                        (@sync_app, 35, '__DIR__ SPORTS_MLB_TEAMS_GAME_FIELDING',              34, 'database'),
                        (@sync_app, 36, '__DIR__ SPORTS_MLB_TEAMS_GAME_PITCHING',              35, 'database'),
                        (@sync_app, 37, '__DIR__ SPORTS_MLB_TEAMS_ROSTERS',                    36, 'database'),
                        (@sync_app, 38, '__DIR__ SPORTS_MLB_TEAMS_SEASON_BATTING',             37, 'database'),
                        (@sync_app, 39, '__DIR__ SPORTS_MLB_TEAMS_SEASON_BATTING_SORTED',      38, 'database'),
                        (@sync_app, 40, '__DIR__ SPORTS_MLB_TEAMS_SEASON_FIELDING',            39, 'database'),
                        (@sync_app, 41, '__DIR__ SPORTS_MLB_TEAMS_SEASON_FIELDING_SORTED',     40, 'database'),
                        (@sync_app, 42, '__DIR__ SPORTS_MLB_TEAMS_SEASON_PITCHING',            41, 'database'),
                        (@sync_app, 43, '__DIR__ SPORTS_MLB_TEAMS_SEASON_PITCHING_SORTED',     42, 'database'),
                        (@sync_app, 44, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY',    43, 'database'),
                        (@sync_app, 45, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_PLAYOFF',            44, 'database'),
                        (@sync_app, 46, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_REGULAR',            45, 'database'),
                        (@sync_app, 47, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_TITLES',             46, 'database'),
                        (@sync_app, 48, '__DIR__ Players',                                     47, 'script'),
                        (@sync_app, 49, '__DIR__ Daily Info',                                  48, 'script'),
                        (@sync_app, 50, '__DIR__ Power Ranks',                                 49, 'script'),
                        (@sync_app, 51, '__DIR__ Schedule',                                    50, 'script'),
                        (@sync_app, 52, 'Synchronise News',                                    51, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES # ID 1 moved to "full"
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_MLB_GAME_LINESCORE',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_MLB_GAME_PITCHERS',                    CONCAT('season = ''', @season, '''')),
                           # ID 4 moved to "full"
                           (@sync_app,  5, 'debearco_sports', 'SPORTS_MLB_GAME_ROSTERS',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app,  6, 'debearco_sports', 'SPORTS_MLB_GAME_UMPIRES',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app,  7, 'debearco_sports', 'SPORTS_MLB_UMPIRES',                          ''),
                           (@sync_app,  8, 'debearco_sports', 'SPORTS_MLB_PLAYERS_GAME_BATTING',             CONCAT('season = ''', @season, '''')),
                           (@sync_app,  9, 'debearco_sports', 'SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_MLB_PLAYERS_GAME_FIELDING',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D',        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_MLB_PLAYERS_GAME_PITCHING',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 13, 'debearco_sports', 'SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D',        CONCAT('season = ''', @season, '''')),
                           # IDs 14 - 20 Archived
                           (@sync_app, 21, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_MISC',              CONCAT('season = ''', @season, '''')),
                           (@sync_app, 22, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTING',           CONCAT('season = ''', @season, '''')),
                           (@sync_app, 23, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_FIELDING',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 24, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_PITCHING',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 25, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_MISC_SORTED',       CONCAT('season = ''', @season, '''')),
                           (@sync_app, 26, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 27, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 28, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 29, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 30, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 31, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 32, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 33, 'debearco_sports', 'SPORTS_MLB_STANDINGS',                        CONCAT('season = ''', @season, '''')),
                           (@sync_app, 34, 'debearco_sports', 'SPORTS_MLB_TEAMS_GAME_BATTING',               CONCAT('season = ''', @season, '''')),
                           (@sync_app, 35, 'debearco_sports', 'SPORTS_MLB_TEAMS_GAME_FIELDING',              CONCAT('season = ''', @season, '''')),
                           (@sync_app, 36, 'debearco_sports', 'SPORTS_MLB_TEAMS_GAME_PITCHING',              CONCAT('season = ''', @season, '''')),
                           (@sync_app, 37, 'debearco_sports', 'SPORTS_MLB_TEAMS_ROSTERS',                    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 38, 'debearco_sports', 'SPORTS_MLB_TEAMS_SEASON_BATTING',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 39, 'debearco_sports', 'SPORTS_MLB_TEAMS_SEASON_BATTING_SORTED',      CONCAT('season = ''', @season, '''')),
                           (@sync_app, 40, 'debearco_sports', 'SPORTS_MLB_TEAMS_SEASON_FIELDING',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 41, 'debearco_sports', 'SPORTS_MLB_TEAMS_SEASON_FIELDING_SORTED',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 42, 'debearco_sports', 'SPORTS_MLB_TEAMS_SEASON_PITCHING',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 43, 'debearco_sports', 'SPORTS_MLB_TEAMS_SEASON_PITCHING_SORTED',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 44, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 45, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_PLAYOFF',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 46, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_REGULAR',            CONCAT('season = ''', @season, '''')),
                           (@sync_app, 47, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_TITLES',             ''),
                           (@sync_app, 53, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SPLIT_LABELS',             ''),
                           (@sync_app, 54, 'debearco_sports', 'SPORTS_MLB_GAME_LINEUPS',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 55, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTING_ZONES',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 56, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_PITCHING_ZONES',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 57, 'debearco_sports', 'SPORTS_MLB_UMPIRES_SEASON_ZONES',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 58, 'debearco_sports', 'SPORTS_MLB_PLAYERS_REPERTOIRE',               ''),
                           (@sync_app, 59, 'debearco_sports', 'SPORTS_MLB_GAME_SCORING',                     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 60, 'debearco_sports', 'SPORTS_MLB_GAME_WINPROB',                     CONCAT('season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 48, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_mlb_players __REMOTE__'),
                               (@sync_app, 49, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_mlb_dailies __REMOTE__'),
                               (@sync_app, 50, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_mlb_power-ranks __REMOTE__'),
                               (@sync_app, 51, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_mlb_schedules __REMOTE__'),
                               (@sync_app, 52, '/var/www/debear/bin/git-admin/server-sync', '--down sports_mlb_news __REMOTE__');

#
# MLB Daily Updates
#
SET @sync_app := 'sports_mlb_dailies';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Daily Updates', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_MLB_PLAYERS_INJURIES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_MLB_SCHEDULE_PROBABLES', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_MLB_TEAMS_DEPTH', 3, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_MLB_PLAYERS_INJURIES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_MLB_SCHEDULE_PROBABLES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_MLB_TEAMS_DEPTH', CONCAT('season = ''', @season, ''''));

#
# MLB Players
#
SET @sync_app := 'sports_mlb_players';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Players', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_MLB_PLAYERS', 1, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_MISC', 2, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTING', 3, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_FIELDING', 4, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_PITCHING', 5, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED', 6, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED', 7, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED', 8, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS', 9, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS', 10, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER', 11, 'database'),
                        (@sync_app, 13, '__DIR__ Player Mugshots', 12, 'file'),
                        (@sync_app, 17, '__DIR__ SPORTS_MLB_PLAYERS_CAREER_BATTING_SPLITS', 16, 'database'),
                        (@sync_app, 18, '__DIR__ SPORTS_MLB_PLAYERS_CAREER_FIELDING_SPLITS',   17, 'database'),
                        (@sync_app, 19, '__DIR__ SPORTS_MLB_PLAYERS_CAREER_PITCHING_SPLITS', 18, 'database'),
                        (@sync_app, 20, '__DIR__ SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER', 19, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_MLB_DRAFT', 20, 'database'),
                        (@sync_app, 21, '__DIR__ SPORTS_MLB_PLAYERS_AWARDS', 21, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app,  1, 'debearco_sports', 'SPORTS_MLB_PLAYERS', ''),
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_MLB_DRAFT', ''),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_MISC', 'season < ''2009'''),
                           (@sync_app,  4, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTING', 'season < ''2009'''),
                           (@sync_app,  5, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_FIELDING', 'season < ''2009'''),
                           (@sync_app,  6, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_PITCHING', 'season < ''2009'''),
                           (@sync_app,  7, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED', 'season < ''2009'''),
                           (@sync_app,  8, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED', 'season < ''2009'''),
                           (@sync_app,  9, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED', 'season < ''2009'''),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS', 'season < ''2009'''),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS', 'season < ''2009'''),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER', 'season < ''2009'''),
                           (@sync_app, 17, 'debearco_sports', 'SPORTS_MLB_PLAYERS_CAREER_BATTING_SPLITS', ''),
                           (@sync_app, 18, 'debearco_sports', 'SPORTS_MLB_PLAYERS_CAREER_FIELDING_SPLITS', ''),
                           (@sync_app, 19, 'debearco_sports', 'SPORTS_MLB_PLAYERS_CAREER_PITCHING_SPLITS', ''),
                           (@sync_app, 20, 'debearco_sports', 'SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER', ''),
                           (@sync_app, 21, 'debearco_sports', 'SPORTS_MLB_PLAYERS_AWARDS', '');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 13, '/var/www/debear/sites/cdn/htdocs/sports/mlb/players', 'debear/sites/cdn/htdocs/sports/mlb/players', NULL);

#
# MLB Teams
#
SET @sync_app := 'sports_mlb_teams';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Teams', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app,  1, '__DIR__ SPORTS_MLB_GROUPINGS', 1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_MLB_TEAMS', 2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_MLB_TEAMS_GROUPINGS', 3, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_MLB_TEAMS_NAMING', 4, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_PLAYOFF', 5, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY', 6, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_REGULAR', 7, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_MLB_TEAMS_HISTORY_TITLES', 8, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_MLB_TEAMS_STADIA', 9, 'database');
#                        (@sync_app, 10, '__DIR__ Team Logos', 10, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_MLB_GROUPINGS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_MLB_TEAMS', ''),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_MLB_TEAMS_GROUPINGS', ''),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_MLB_TEAMS_NAMING', ''),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_PLAYOFF', ''),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_PLAYOFF_SUMMARY', ''),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_REGULAR', ''),
                           (@sync_app, 8, 'debearco_sports', 'SPORTS_MLB_TEAMS_HISTORY_TITLES', ''),
                           (@sync_app, 9, 'debearco_sports', 'SPORTS_MLB_TEAMS_STADIA', '');
#INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
#                      VALUES (@sync_app, 10, '/var/www/debear/sites/cdn/htdocs/sports/mlb/team', 'debear/sites/cdn/htdocs/sports/mlb/team', NULL);

#
# MLB Schedules
#
SET @sync_app := 'sports_mlb_schedules';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Schedules', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_MLB_SCHEDULE', 1, 'database'),
                        (@sync_app, 6, '__DIR__ SPORTS_MLB_SCHEDULE_DATES', 2, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_MLB_SCHEDULE_MATCHUPS', 3, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_MLB_PLAYOFF_SEEDS', 4, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_MLB_PLAYOFF_SERIES', 5, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_MLB_STANDINGS', 6, 'database'),
                        (@sync_app, 7, 'Synchronise Weather', 7, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_MLB_SCHEDULE', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_MLB_SCHEDULE_MATCHUPS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_MLB_PLAYOFF_SEEDS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_MLB_PLAYOFF_SERIES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_MLB_STANDINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_MLB_SCHEDULE_DATES', CONCAT('season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 7, '/var/www/debear/bin/git-admin/server-sync', '--down sports_mlb_weather __REMOTE__');

#
# MLB Power Ranks
#
SET @sync_app := 'sports_mlb_power-ranks';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Power Ranks', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_MLB_POWER_RANKINGS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_MLB_POWER_RANKINGS_WEEKS', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_MLB_POWER_RANKINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_MLB_POWER_RANKINGS_WEEKS', CONCAT('season = ''', @season, ''''));

#
# MLB Drafts
#
SET @sync_app := 'sports_mlb_draft';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Draft', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_MLB_DRAFT', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_MLB_DRAFT_SEASON', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_MLB_DRAFT', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_MLB_DRAFT_SEASON', '');

#
# MLB Starting Lineups
#
SET @sync_app := 'sports_mlb_lineup';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Starting Lineups', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_MLB_GAME_LINEUPS', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_MLB_GAME_LINEUPS', CONCAT('season = ''', @season, ''''));

#
# MLB News
#
SET @sync_app := 'sports_mlb_news';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB News', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ NEWS_SOURCES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ NEWS_ARTICLES', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause, clear_first)
                    VALUES (@sync_app, 1, 'debearco_common', 'NEWS_SOURCES', 'app = ''sports_mlb''', 0),
                           (@sync_app, 2, 'debearco_common', 'NEWS_ARTICLES', 'app = ''sports_mlb''', 1);

#
# MLB Game Odds
#
SET @sync_app := 'sports_mlb_odds';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Odds', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_COMMON_MAJORS_ODDS', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_COMMON_MAJORS_ODDS', 'sport = ''mlb''');

#
# MLB Weather
#
SET @sync_app := 'sports_mlb_weather';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB Weather', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ WEATHER_FORECAST', 1, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_common', 'WEATHER_FORECAST', 'app = ''sports_mlb''');

#
# Additional MLB data
#
SET @sync_app := 'sports_mlb_full';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports MLB (Full)', 'data', 'dev', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 3, '__DIR__ SPORTS_MLB_GAME_ATBAT',         1, 'database'),
                        (@sync_app, 1, '__DIR__ SPORTS_MLB_GAME_ATBAT_FLAGS',   2, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_MLB_GAME_ATBAT_PITCHES', 3, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_MLB_GAME_PLAYS',         4, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_MLB_PLAYERS_IMPORT',     5, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_MLB_GAME_ATBAT_FLAGS',   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_MLB_GAME_ATBAT_PITCHES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_MLB_GAME_ATBAT',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_MLB_GAME_PLAYS',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_MLB_PLAYERS_IMPORT',     '');
