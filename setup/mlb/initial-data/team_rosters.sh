#DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '2017', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY SELECT season, game_type, game_id FROM SPORTS_MLB_SCHEDULE WHERE season = 2017 AND status = 'F';
#CALL mlb_rosters();

echo "DELETE FROM SPORTS_MLB_TEAMS_ROSTERS WHERE season = 2017; DELETE FROM SPORTS_MLB_TEAMS_DEPTH WHERE season = 2017;" | mysql -s debearco_sports

# Initial
echo -n "Initial: "
./parse-roster.pl 2017 2>/dev/null | mysql debearco_sports
echo "[ Done ] (`date`)"
# Up to (missing) depth charts
for f in 2017-04-03 2017-04-04 2017-04-05 2017-04-06 2017-04-07 2017-04-08 2017-04-09 2017-04-10 2017-04-11 2017-04-12 2017-04-13; do
  echo -n "$f: "
  ./parse-roster.pl 2017 $f 2>/dev/null | mysql debearco_sports
  echo "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '2017', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY SELECT season, game_type, game_id FROM SPORTS_MLB_SCHEDULE WHERE season = 2017 AND game_date = DATE_SUB('$f', INTERVAL 1 DAY) AND status = 'F'; CALL mlb_rosters();" | mysql debearco_sports
  echo "[ Done ] (`date`)"
done
# With Depth Charts, up to (missing) 2017-05-07
for f in 2017-04-14 2017-04-15 2017-04-16 2017-04-17 2017-04-18 2017-04-19 2017-04-20 2017-04-21 2017-04-22 2017-04-23 2017-04-24 2017-04-25 2017-04-26 2017-04-27 2017-04-28 2017-04-29 2017-04-30 2017-05-01 2017-05-02 2017-05-03 2017-05-04 2017-05-05 2017-05-06; do
  echo -n "$f: "
  ./parse-roster.pl 2017 $f 2>/dev/null | mysql debearco_sports
  ./parse-depth.pl 2017 $f 2>/dev/null | mysql debearco_sports
  echo "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '2017', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY SELECT season, game_type, game_id FROM SPORTS_MLB_SCHEDULE WHERE season = 2017 AND game_date = DATE_SUB('$f', INTERVAL 1 DAY) AND status = 'F'; CALL mlb_rosters();" | mysql debearco_sports
  echo "[ Done ] (`date`)"
done
# Missing 2017-05-07
echo -n "2017-05-07: "
echo "INSERT INTO SPORTS_MLB_TEAMS_ROSTERS SELECT season, '2017-05-07', team_id, player_id, jersey, pos, player_status, roster_type FROM SPORTS_MLB_TEAMS_ROSTERS WHERE the_date = '2017-05-06';" | mysql debearco_sports
echo "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '2017', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY SELECT season, game_type, game_id FROM SPORTS_MLB_SCHEDULE WHERE season = 2017 AND game_date = '2017-05-06' AND status = 'F'; CALL mlb_rosters();" | mysql debearco_sports
echo "[ Done ] (`date`)"
# Onwards to the end
for f in 2017-05-08 2017-05-09 2017-05-10 2017-05-11 2017-05-12 2017-05-13 2017-05-14 2017-05-15 2017-05-16 2017-05-17 2017-05-18 2017-05-19 2017-05-21 2017-05-22 2017-05-23 2017-05-24 2017-05-25 2017-05-26 2017-05-27 2017-05-28 2017-05-29 2017-05-30 2017-05-31 2017-06-01 2017-06-02 2017-06-03; do
  echo -n "$f: "
  ./parse-roster.pl 2017 $f 2>/dev/null | mysql debearco_sports
  ./parse-depth.pl 2017 $f 2>/dev/null | mysql debearco_sports
  echo "DROP TEMPORARY TABLE IF EXISTS tmp_game_list; CREATE TEMPORARY TABLE tmp_game_list (season YEAR DEFAULT '2017', game_type ENUM('regular','playoff'), game_id SMALLINT UNSIGNED, PRIMARY KEY (season, game_type, game_id)) ENGINE = MEMORY SELECT season, game_type, game_id FROM SPORTS_MLB_SCHEDULE WHERE season = 2017 AND game_date = DATE_SUB('$f', INTERVAL 1 DAY) AND status = 'F'; CALL mlb_rosters();" | mysql debearco_sports
  echo "[ Done ] (`date`)"
done

# After running the roster calcs, do a player-scan to determine players still with missing positions?
