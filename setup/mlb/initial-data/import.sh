#!/bin/bash
# Initial MLB data import
# TD. 2017-05-15.
start=2008

#
# Helpers
#
# Run script, check for error
function check {
  e=$?
  # Return code 0 == okay...
  if [ $e -eq 0 ]; then
    message 'Done'
  # Anything else isn't...
  else
    message "Error ($e)"
    exit $e
  fi
}

# Flag complete
function message {
  echo "[ $1 ] (`date +'%F %T'`)"
}

#
# Reset previous test runs
#
echo -n 'Reset: '
/var/www/debear/sites/sports/setup/mlb/initial-data/reset.sh
check

#
# Perform import...
#
log=/var/www/debear/sites/sports/data/mlb/_logs/initial.log
if [ -e $log ]; then rm $log; fi
cd /var/www/debear/sites/sports/data/mlb
for s in `seq $start 2016`; do
  echo -e "\n** $s **"

  # Schedule
  echo -n "- Regular Season Schedule:       "
  ./update_schedule.pl --historical --log-dir="$s.regular" $s --regular >>$log 2>&1
  check

  # Regular Season
  echo -n "- Regular Season Games:          "
  ./update_games.pl --historical --log-dir="$s.regular" $s >>$log 2>&1
  check

  # Initial Playoff Schedule
  echo -n "- Playoff Schedule:              "
  if [ $s \< 2012 ]; then rd='lds'; else rd='wc'; fi
  ./update_schedule.pl --historical --log-dir="$s.playoff.$rd" $s --playoff >>$log 2>&1
  check

  # Playoffs
  # Wildcard (2012 onwards)
  if [ $s \> 2011 ]
  then
    echo -n "- Wildcard Round:                "
    ./update_games.pl --historical --log-dir="$s.playoff.wc" $s >>$log 2>&1
    check
  fi
  # LDS
  echo -n "- League Division Series:        "
  ./update_games.pl --historical --log-dir="$s.playoff.lds" $s >>$log 2>&1
  check
  # LCS
  echo -n "- League Championship Series:    "
  ./update_games.pl --historical --log-dir="$s.playoff.lcs" $s >>$log 2>&1
  check
  # WS
  echo -n "- World Series:                  "
  ./update_games.pl --historical --log-dir="$s.playoff.ws" $s >>$log 2>&1
  check
done

## Initial 2017...
echo -e "\n** 2017 **"

# Schedule
echo -n "- Regular Season Schedule:       "
if [ -e /var/www/debear/sites/sports/data/mlb/_data/2017/schedules/initial.tar.gz ]; then
  rm /var/www/debear/sites/sports/data/mlb/_data/2017/schedules/initial.tar.gz
fi
./update_schedule.pl --historical --log-dir="2017.regular" 2017 --regular >>$log 2>&1
check

# Rosters / Depth Charts
echo -n "- Rosters / Depth Charts:        "
/var/www/debear/sites/sports/setup/mlb/initial-data/mlb.rosters.sh | gzip >/tmp/mlb.rosters.sql.gz
gzip -dc /tmp/mlb.rosters.sql.gz | mysql debearco_sports
check

# Probables
echo -n "- Probables:                     "
/var/www/debear/sites/sports/setup/mlb/initial-data/mlb.probables.sh | gzip >/tmp/mlb.probables.sql.gz
gzip -dc /tmp/mlb.probables.sql.gz | mysql debearco_sports
check

# Injuries
echo -n "- Injuries:                      "
/var/www/debear/sites/sports/setup/mlb/initial-data/mlb.injuries.sh | gzip >/tmp/mlb.injuries.sql.gz
gzip -dc /tmp/mlb.injuries.sql.gz | mysql debearco_sports
check

# Regular Season
echo -n "- Regular Season Games:          "
./update_games.pl --historical --log-dir="2017.initial" 2017 >>$log 2>&1
check
