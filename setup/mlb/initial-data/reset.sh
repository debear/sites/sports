db=debearco_sports

# Tables to truncate
for tbl in SPORTS_MLB_GAME_ATBAT SPORTS_MLB_GAME_ATBAT_FLAGS SPORTS_MLB_GAME_ATBAT_PITCHES SPORTS_MLB_GAME_LINESCORE SPORTS_MLB_GAME_PITCHERS SPORTS_MLB_GAME_PLAYS SPORTS_MLB_GAME_ROSTERS SPORTS_MLB_GAME_UMPIRES SPORTS_MLB_PLAYERS SPORTS_MLB_PLAYERS_CAREER_BATTER_VS_PITCHER SPORTS_MLB_PLAYERS_CAREER_BATTING SPORTS_MLB_PLAYERS_CAREER_BATTING_SPLITS SPORTS_MLB_PLAYERS_CAREER_FIELDING SPORTS_MLB_PLAYERS_CAREER_FIELDING_SPLITS SPORTS_MLB_PLAYERS_CAREER_PITCHING SPORTS_MLB_PLAYERS_CAREER_PITCHING_SPLITS SPORTS_MLB_PLAYERS_DRAFT SPORTS_MLB_PLAYERS_GAME_BATTING SPORTS_MLB_PLAYERS_GAME_BATTING_Y2D SPORTS_MLB_PLAYERS_GAME_FIELDING SPORTS_MLB_PLAYERS_GAME_FIELDING_Y2D SPORTS_MLB_PLAYERS_GAME_PITCHING SPORTS_MLB_PLAYERS_GAME_PITCHING_Y2D SPORTS_MLB_PLAYERS_IMPORT SPORTS_MLB_PLAYERS_INJURIES SPORTS_MLB_PLAYERS_SEASON_BATTER_VS_PITCHER SPORTS_MLB_PLAYERS_SEASON_BATTING SPORTS_MLB_PLAYERS_SEASON_BATTING_SORTED SPORTS_MLB_PLAYERS_SEASON_BATTING_SPLITS SPORTS_MLB_PLAYERS_SEASON_FIELDING SPORTS_MLB_PLAYERS_SEASON_FIELDING_SORTED SPORTS_MLB_PLAYERS_SEASON_FIELDING_SPLITS SPORTS_MLB_PLAYERS_SEASON_MISC SPORTS_MLB_PLAYERS_SEASON_MISC_SORTED SPORTS_MLB_PLAYERS_SEASON_PITCHING SPORTS_MLB_PLAYERS_SEASON_PITCHING_SORTED SPORTS_MLB_PLAYERS_SEASON_PITCHING_SPLITS SPORTS_MLB_PLAYOFF_SEEDS SPORTS_MLB_PLAYOFF_SERIES SPORTS_MLB_POWER_RANKINGS SPORTS_MLB_POWER_RANKINGS_WEEKS SPORTS_MLB_SCHEDULE_PROBABLES SPORTS_MLB_STANDINGS SPORTS_MLB_TEAMS_DEPTH SPORTS_MLB_TEAMS_GAME_BATTING SPORTS_MLB_TEAMS_GAME_FIELDING SPORTS_MLB_TEAMS_GAME_PITCHING SPORTS_MLB_TEAMS_ROSTERS SPORTS_MLB_TEAMS_SEASON_BATTING SPORTS_MLB_TEAMS_SEASON_BATTING_SORTED SPORTS_MLB_TEAMS_SEASON_FIELDING SPORTS_MLB_TEAMS_SEASON_FIELDING_SORTED SPORTS_MLB_TEAMS_SEASON_PITCHING SPORTS_MLB_TEAMS_SEASON_PITCHING_SORTED SPORTS_MLB_UMPIRES
do
  echo "TRUNCATE TABLE $tbl;"
done | mysql -s $db

# When correcting schedule issues
echo "TRUNCATE TABLE SPORTS_MLB_SCHEDULE;" | mysql -s $db
echo "TRUNCATE TABLE SPORTS_MLB_SCHEDULE_MATCHUPS;" | mysql -s $db
# Unset schedules
#echo "DELETE FROM SPORTS_MLB_SCHEDULE WHERE game_type = 'playoff';" | mysql -s $db
#echo "UPDATE SPORTS_MLB_SCHEDULE SET status = NULL WHERE status = 'F';" | mysql -s $db

# Remove mugshots from the CDN
rm -rf /var/www/debear/sites/cdn/htdocs/sports/mlb/players/*.png

# Remove previous logs
rm -rf /var/www/debear/sites/sports/data/mlb/_logs/*/*
