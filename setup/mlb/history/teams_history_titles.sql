INSERT INTO SPORTS_MLB_TEAMS_HISTORY_TITLES (team_id, league_id, div_champ, league_champ, playoff_champ) VALUES
('BAL', '2', '9', '6', '3'),
('MBW', '2', NULL, '0', '0'),
('SLB', '2', NULL, '1', '0'),
('BOS', '2', '8', '13', '8'),
('NYY', '2', '18', '40', '27'),
('BLO', '2', NULL, '0', '0'),
('TB', '2', '2', '1', '0'),
('TOR', '2', '6', '2', '2'),
('CWS', '2', '5', '6', '3'),
('CLE', '2', '8', '6', '2'),
('DET', '2', '7', '11', '4'),
('KC', '2', '7', '4', '2'),
('MIN', '2', '10', '3', '2'),
('WSN', '2', NULL, '3', '1'),
('HOU', '2', '0', '0', '0'),
('HOU', '3', '6', '1', '0'),
('LAA', '2', '9', '1', '1'),
('OAK', '2', '16', '6', '4'),
('PHA', '2', NULL, '9', '5'),
('KCA', '2', NULL, '0', '0'),
('SEA', '2', '3', '0', '0'),
('TEX', '2', '7', '2', '0'),
('WSS', '2', '0', '0', '0'),
('ATL', '3', '12', '5', '1'),
('MBR', '3', NULL, '2', '1'),
('BBR', '3', NULL, '10', '1'),
('BBR', '10', NULL, '4', NULL),
('MIA', '3', '0', '2', '2'),
('NYM', '3', '6', '5', '2'),
('PHI', '3', '11', '7', '2'),
('WSH', '3', '3', '0', '0'),
('MTE', '3', '1', '0', '0'),
('CHC', '3', '5', '17', '3'),
('CHC', '10', NULL, '1', NULL),
('CHC', '11', NULL, '0', NULL),
('CIN', '3', '10', '9', '5'),
('CIN', '12', NULL, '1', NULL),
('MIL', '2', '1', '1', '0'),
('MIL', '3', '1', '0', '0'),
('SEP', '3', '0', '0', '0'),
('PIT', '3', '9', '9', '5'),
('PIT', '12', NULL, '0', NULL),
('STL', '3', '13', '9', '11'),
('STL', '12', NULL, '4', NULL),
('ARI', '3', '5', '1', '1'),
('COL', '3', '0', '1', '0'),
('LAD', '3', '15', '9', '5'),
('BKD', '3', NULL, '12', '1'),
('BKD', '12', NULL, '1', NULL),
('BKD', '13', NULL, '1', NULL),
('SD', '3', '5', '2', '0'),
('SF', '3', '8', '6', '3'),
('NGI', '3', NULL, '17', '5');
