INSERT INTO SPORTS_MLB_GROUPINGS (grouping_id, parent_id, type, name, name_full, `order`, icon) VALUES
('1', NULL, 'league', 'MLB', 'Major League Baseball', '10', 'MLB'),
('2', '1', 'conf', 'AL', 'American League', '10', 'AL'),
('3', '1', 'conf', 'NL', 'National League', '20', 'NL'),
('4', '2', 'div', 'AL East', 'AL East Division', '10', NULL),
('5', '2', 'div', 'AL Central', 'AL Central Divison', '20', NULL),
('6', '2', 'div', 'AL West', 'AL West Division', '30', NULL),
('7', '3', 'div', 'NL East', 'NL East Division', '10', NULL),
('8', '3', 'div', 'NL Central', 'NL Central Divison', '20', NULL),
('9', '3', 'div', 'NL West', 'NL West Division', '30', NULL),
('10', NULL, 'league', 'NABBP', 'National Association of Base Ball Players', NULL, NULL),
('11', NULL, 'league', 'NAPBBP', 'National Association of Professional Base Ball Players', NULL, NULL),
('12', NULL, 'league', 'AA', 'American Association', NULL, NULL),
('13', NULL, 'league', 'IL', 'International League', NULL, NULL);
