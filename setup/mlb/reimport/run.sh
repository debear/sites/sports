# Prepare
for t in `echo "SHOW TABLES;" | mysql -s debearco_sports_mlb`
do
    echo "TRUNCATE TABLE $t;"
done | mysql debearco_sports_mlb
for t in SPORTS_MLB_PLAYERS SPORTS_MLB_PLAYERS_IMPORT SPORTS_MLB_SCHEDULE SPORTS_MLB_TEAMS_STADIA SPORTS_MLB_PLAYERS_SPLIT_LABELS
do
    echo "INSERT INTO debearco_sports_mlb.$t SELECT * FROM debearco_sports.$t;"
done | mysql debearco_sports_mlb
# Run
rm -rf ../../setup/mlb/reimport/2*
for y in `seq 2008 2019`
do
    for t in regular playoff
    do
        # Import the games
        for i in `echo "SELECT game_id FROM SPORTS_MLB_SCHEDULE WHERE season = '$y' AND game_type = '$t' AND status = 'F' ORDER BY game_date, game_time, game_id;" | mysql -s debearco_sports`
        do
            echo -n "$y // $t // $i: "
            echo "# $y // $t // $i" >>../../setup/mlb/reimport/$y.$t.log
            games/parse.pl $y $t $i 2>>../../setup/mlb/reimport/$y.$t.err | mysql -f debearco_sports_mlb >>../../setup/mlb/reimport/$y.$t.imp 2>>../../setup/mlb/reimport/$y.$t.log
            echo "[ Done ]"
        done
        # Post-processing
        echo -n "Post-processing $y // $t: "
        sed "s/{season}/$y/" ../../setup/mlb/reimport/stproc.sql | sed "s/{game_type}/$t/" | mysql -f debearco_sports_mlb >../../setup/mlb/reimport/$y.$t.pro 2>../../setup/mlb/reimport/$y.$t.pre
        echo "[ Done ]"
    done
done
