# Temporary tables
DROP TEMPORARY TABLE IF EXISTS tmp_game_list;
CREATE TEMPORARY TABLE tmp_game_list (
  season YEAR,
  game_type ENUM('regular','playoff'),
  game_id SMALLINT UNSIGNED,
  PRIMARY KEY (season, game_type, game_id)
) ENGINE = MEMORY
  SELECT season, game_type, game_id
  FROM SPORTS_MLB_SCHEDULE
  WHERE season = '{season}'
  AND   game_type = '{game_type}'
  AND   status = 'F';

# Procs to run
CALL mlb_totals_players_season();
CALL mlb_totals_players_sort('{season}', '{game_type}');
CALL mlb_totals_players_order();
CALL mlb_totals_players_career('{game_type}');
CALL mlb_totals_players_splits();
