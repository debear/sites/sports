INSERT INTO SPORTS_AHL_POSITIONS (pos_code, pos_long)
  VALUES ('G',  'Goaltender'),
         ('D',  'Defenseman'),
         ('LW', 'Left Wing'),
         ('RW', 'Right Wing'),
         ('C',  'Center'),
         ('F',  'Forward');
