dir=`dirname $(readlink -f $0)`

# Truncate
for tbl in `echo "SHOW TABLES LIKE 'SPORTS_AHL_%';" | mysql -s debearco_sportsdev`; do echo "TRUNCATE TABLE $tbl;" | mysql -s debearco_sportsdev; done

# Load - Base
cat $dir/../config/*.sql | mysql debearco_sportsdev

# Load - Teams
cat $dir/../history/*.sql | mysql debearco_sportsdev
