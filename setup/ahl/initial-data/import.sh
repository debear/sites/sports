#!/bin/bash
# Initial AHL data import
# TD. 2015-06-17.

#
# Helpers
#
# Run script, check for error
function check {
  e=$?
  # Return code 0 == okay...
  if [ $e -eq 0 ]; then
    message 'Done'
  # Anything else isn't...
  else
    message "Error ($e)"
    exit $e
  fi
}

# Flag complete
function message {
  echo "[ $1 ] (`date +'%F %T'`)"
}

#
# Reset previous test runs
#
echo "Reset...";
# - Database
echo -n " - Database: "
/var/www/debear/sites/sports.branch/setup/ahl/initial-data/_reset_db.sh
check
# - Mugshots
echo -n " - Mugshot:  "
rm -rf /var/www/debear/sites/cdn/htdocs/sports/ahl/players/*.png
check
# - Logs
echo -n " - Logs:     "
rm -rf /var/www/debear/sites/sports.branch/data/ahl/_logs/*/*
check

#
# Perform import...
#
log=/var/www/debear/sites/sports.branch/setup/ahl/initial-data/$$.log
rm -rf $log
cd /var/www/debear/sites/sports.branch/data/ahl
for s in 2005-06 2006-07 2007-08 2008-09 2009-10 2010-11 2011-12 2012-13 2013-14 2014-15; do
  echo -e "\n** $s **"
  i=`echo "$s" | sed -r 's/-[0-9]{2}$//'`

  # Regular Season
  echo "- Regular Season:"
  echo -n "  - Schedule:     "
  ./update_schedule.pl $i --regular --log-dir="$s.regular" >>$log 2>&1
  check
  echo -n "  - Transactions: "
  ./update_transactions.pl $i --regular --log-dir="$s.regular" >>$log 2>&1
  check
  echo -n "  - Games:        "
  ./update_games.pl $i --regular --log-dir="$s.regular" >>$log 2>&1
  check
  
  # Playoffs
  echo "- Playoffs:"
  echo -n "  - Schedule:     "
  ./update_schedule.pl $i --playoff --log-dir="$s.playoff" >>$log 2>&1
  check
  echo -n "  - Transactions: "
  ./update_transactions.pl $i --playoff --log-dir="$s.playoff" >>$log 2>&1
  check
  # Round 1
  if [ $i \< 2011 ]; then c1='DSF'; else c1='CQF'; fi
  echo -n "  - Games ($c1):  "
  ./update_games.pl $i --playoff --log-dir="$s.playoff.$c1" >>$log 2>&1
  check
  # Round 2
  if [ $i \< 2011 ]; then c2='DF'; s2=' '; else c2='CSF'; s2=''; fi
  echo -n "  - Games ($c2):$s2  "
  ./update_games.pl $i --playoff --log-dir="$s.playoff.$c2" >>$log 2>&1
  check
  # Conf Final
  echo -n "  - Games (CF):   "
  ./update_games.pl $i --playoff --log-dir="$s.playoff.CF" >>$log 2>&1
  check
  # Calder Cup
  echo -n "  - Games (CCF):  "
  ./update_games.pl $i --playoff --log-dir="$s.playoff.CCF" >>$log 2>&1
  check
  
  # Backup database
  echo -n "- Backup: "
  t=`echo "SHOW TABLES LIKE 'SPORTS_AHL_%';" | mysql -s debearco_sportsdev`
  mysqldump --no-create-info debearco_sportsdev $t | gzip >/var/www/debear/sites/sports.branch/db/backups/SPORTS_AHL_IMPORT/${s}_data.sql.gz
  check
done

