# Season to sync...
SET @season := 2024;

#
# General AHL update
#
SET @sync_app := 'sports_ahl';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports AHL', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 33, '__DIR__ SPORTS_AHL_GAME_EVENT_BOXSCORE',            1, 'database'),
                        (@sync_app,  2, '__DIR__ SPORTS_AHL_GAME_LINEUP',                    2, 'database'),
                        (@sync_app,  3, '__DIR__ SPORTS_AHL_GAME_OFFICIALS',                 3, 'database'),
                        (@sync_app,  4, '__DIR__ SPORTS_AHL_GAME_PERIODS',                   4, 'database'),
                        (@sync_app,  5, '__DIR__ SPORTS_AHL_GAME_PP_STATS',                  5, 'database'),
                        (@sync_app,  6, '__DIR__ SPORTS_AHL_GAME_THREE_STARS',               6, 'database'),
                        (@sync_app,  7, '__DIR__ SPORTS_AHL_OFFICIALS',                      7, 'database'),
                        (@sync_app,  8, '__DIR__ SPORTS_AHL_PLAYERS_GAME_GOALIES',           8, 'database'),
                        (@sync_app,  9, '__DIR__ SPORTS_AHL_PLAYERS_GAME_SKATERS',           9, 'database'),
                        (@sync_app, 10, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_GOALIES',         10, 'database'),
                        (@sync_app, 11, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_GOALIES_SORTED',  11, 'database'),
                        (@sync_app, 31, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_GOALIES_HEATMAP', 12, 'database'),
                        (@sync_app, 12, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_SKATERS',         13, 'database'),
                        (@sync_app, 13, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_SKATERS_SORTED',  14, 'database'),
                        (@sync_app, 32, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_SKATERS_HEATMAP', 15, 'database'),
                        (@sync_app, 14, '__DIR__ SPORTS_AHL_STANDINGS',                      16, 'database'),
                        (@sync_app, 15, '__DIR__ SPORTS_AHL_TEAMS_GAME_GOALIES',             17, 'database'),
                        (@sync_app, 16, '__DIR__ SPORTS_AHL_TEAMS_GAME_SKATERS',             18, 'database'),
                        (@sync_app, 17, '__DIR__ SPORTS_AHL_TEAMS_ROSTERS',                  19, 'database'),
                        (@sync_app, 18, '__DIR__ SPORTS_AHL_TRANSACTIONS',                   20, 'database'),
                        (@sync_app, 19, '__DIR__ SPORTS_AHL_TEAMS_SEASON_GOALIES',           21, 'database'),
                        (@sync_app, 20, '__DIR__ SPORTS_AHL_TEAMS_SEASON_GOALIES_SORTED',    22, 'database'),
                        (@sync_app, 21, '__DIR__ SPORTS_AHL_TEAMS_SEASON_SKATERS',           23, 'database'),
                        (@sync_app, 22, '__DIR__ SPORTS_AHL_TEAMS_SEASON_SKATERS_SORTED',    24, 'database'),
                        (@sync_app, 23, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_PLAYOFF',          25, 'database'),
                        (@sync_app, 24, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_PLAYOFF_SUMMARY',  26, 'database'),
                        (@sync_app, 25, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_REGULAR',          27, 'database'),
                        (@sync_app, 26, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_TITLES',           28, 'database'),
                        (@sync_app, 27, '__DIR__ Players',                                   29, 'script'),
                        (@sync_app, 28, '__DIR__ Power Ranks',                               30, 'script'),
                        (@sync_app, 29, '__DIR__ Schedule',                                  31, 'script'),
                        (@sync_app, 30, 'Synchronise News',                                  32, 'script');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES # ID 1 moved to "full"
                           (@sync_app,  2, 'debearco_sports', 'SPORTS_AHL_GAME_LINEUP',                    CONCAT('season = ''', @season, '''')),
                           (@sync_app,  3, 'debearco_sports', 'SPORTS_AHL_GAME_OFFICIALS',                 CONCAT('season = ''', @season, '''')),
                           (@sync_app,  4, 'debearco_sports', 'SPORTS_AHL_GAME_PERIODS',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app,  5, 'debearco_sports', 'SPORTS_AHL_GAME_PP_STATS',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app,  6, 'debearco_sports', 'SPORTS_AHL_GAME_THREE_STARS',               CONCAT('season = ''', @season, '''')),
                           (@sync_app,  7, 'debearco_sports', 'SPORTS_AHL_OFFICIALS',                      ''),
                           (@sync_app,  8, 'debearco_sports', 'SPORTS_AHL_PLAYERS_GAME_GOALIES',           CONCAT('season = ''', @season, '''')),
                           (@sync_app,  9, 'debearco_sports', 'SPORTS_AHL_PLAYERS_GAME_SKATERS',           CONCAT('season = ''', @season, '''')),
                           (@sync_app, 10, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_GOALIES',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 11, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_GOALIES_SORTED',  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 12, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_SKATERS',         CONCAT('season = ''', @season, '''')),
                           (@sync_app, 13, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_SKATERS_SORTED',  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 14, 'debearco_sports', 'SPORTS_AHL_STANDINGS',                      CONCAT('season = ''', @season, '''')),
                           (@sync_app, 15, 'debearco_sports', 'SPORTS_AHL_TEAMS_GAME_GOALIES',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 16, 'debearco_sports', 'SPORTS_AHL_TEAMS_GAME_SKATERS',             CONCAT('season = ''', @season, '''')),
                           (@sync_app, 17, 'debearco_sports', 'SPORTS_AHL_TEAMS_ROSTERS',                  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 18, 'debearco_sports', 'SPORTS_AHL_TRANSACTIONS',                   CONCAT('season = ''', @season, '''')),
                           (@sync_app, 19, 'debearco_sports', 'SPORTS_AHL_TEAMS_SEASON_GOALIES',           CONCAT('season = ''', @season, '''')),
                           (@sync_app, 20, 'debearco_sports', 'SPORTS_AHL_TEAMS_SEASON_GOALIES_SORTED',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 21, 'debearco_sports', 'SPORTS_AHL_TEAMS_SEASON_SKATERS',           CONCAT('season = ''', @season, '''')),
                           (@sync_app, 22, 'debearco_sports', 'SPORTS_AHL_TEAMS_SEASON_SKATERS_SORTED',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 23, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_PLAYOFF',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 24, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_PLAYOFF_SUMMARY',  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 25, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_REGULAR',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 26, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_TITLES',           ''),
                           (@sync_app, 31, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_GOALIES_HEATMAP', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 32, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_SKATERS_HEATMAP', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 33, 'debearco_sports', 'SPORTS_AHL_GAME_EVENT_BOXSCORE',            CONCAT('season = ''', @season, ''''));
INSERT INTO SERVER_SYNC_SCRIPT (sync_app, sync_id, script_name, script_args)
                        VALUES (@sync_app, 27, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_ahl_players __REMOTE__'),
                               (@sync_app, 28, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_ahl_power-ranks __REMOTE__'),
                               (@sync_app, 29, '/var/www/debear/bin/git-admin/server-sync', '__DIR__ sports_ahl_schedules __REMOTE__'),
                               (@sync_app, 30, '/var/www/debear/bin/git-admin/server-sync', '--down sports_ahl_news __REMOTE__');

#
# AHL Players
#
SET @sync_app := 'sports_ahl_players';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports AHL Players', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_AHL_PLAYERS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_GOALIES', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_AHL_PLAYERS_SEASON_SKATERS', 3, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_AHL_PLAYERS_AWARDS', 4, 'database'),
                        (@sync_app, 4, '__DIR__ Player Mugshots', 5, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_AHL_PLAYERS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_GOALIES', 'season < ''2007'''),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_AHL_PLAYERS_SEASON_SKATERS', 'season < ''2007'''),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_AHL_PLAYERS_AWARDS', '');
INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
                      VALUES (@sync_app, 4, '/var/www/debear/sites/cdn/htdocs/sports/ahl/players', 'debear/sites/cdn/htdocs/sports/ahl/players', NULL);

#
# AHL Teams
#
SET @sync_app := 'sports_ahl_teams';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports AHL Teams', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_AHL_GROUPINGS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_AHL_TEAMS', 2, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_AHL_TEAMS_GROUPINGS', 3, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_PLAYOFF', 4, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_PLAYOFF_SUMMARY', 5, 'database'),
                        (@sync_app, 6, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_REGULAR', 6, 'database'),
                        (@sync_app, 7, '__DIR__ SPORTS_AHL_TEAMS_HISTORY_TITLES', 7, 'database'),
                        (@sync_app, 8, '__DIR__ SPORTS_AHL_TEAMS_NAMING', 8, 'database'),
                        (@sync_app, 9, '__DIR__ SPORTS_AHL_TEAMS_ARENAS', 9, 'database');
#                        (@sync_app, 10, '__DIR__ Team Logos', 10, 'file');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_AHL_GROUPINGS', ''),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_AHL_TEAMS', ''),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_AHL_TEAMS_GROUPINGS', ''),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_PLAYOFF', ''),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_PLAYOFF_SUMMARY', ''),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_REGULAR', ''),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_AHL_TEAMS_HISTORY_TITLES', ''),
                           (@sync_app, 8, 'debearco_sports', 'SPORTS_AHL_TEAMS_NAMING', ''),
                           (@sync_app, 9, 'debearco_sports', 'SPORTS_AHL_TEAMS_ARENAS', '');
#INSERT INTO SERVER_SYNC_FILE (sync_app, sync_id, dev_location, live_location, rsync_opt)
#                      VALUES (@sync_app, 10, '/var/www/debear/sites/cdn/htdocs/sports/ahl/team', 'debear/sites/cdn/htdocs/sports/ahl/team', NULL);

#
# AHL Schedules
#
SET @sync_app := 'sports_ahl_schedules';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports AHL Schedules', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_AHL_SCHEDULE', 1, 'database'),
                        (@sync_app, 6, '__DIR__ SPORTS_AHL_SCHEDULE_DATES', 2, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_AHL_SCHEDULE_MATCHUPS', 3, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_AHL_PLAYOFF_SEEDS', 4, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_AHL_PLAYOFF_SERIES', 5, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_AHL_STANDINGS', 6, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_AHL_SCHEDULE', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_AHL_SCHEDULE_MATCHUPS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_AHL_PLAYOFF_SEEDS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_AHL_PLAYOFF_SERIES', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_AHL_STANDINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_AHL_SCHEDULE_DATES', CONCAT('season = ''', @season, ''''));

#
# AHL Power Ranks
#
SET @sync_app := 'sports_ahl_power-ranks';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports AHL Power Ranks', 'data', 'dev,live', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ SPORTS_AHL_POWER_RANKINGS', 1, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_AHL_POWER_RANKINGS_WEEKS', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_AHL_POWER_RANKINGS', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_AHL_POWER_RANKINGS_WEEKS', CONCAT('season = ''', @season, ''''));

#
# AHL News
#
SET @sync_app := 'sports_ahl_news';
INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports AHL News', 'data,live', 'dev,data', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 1, '__DIR__ NEWS_SOURCES', 1, 'database'),
                        (@sync_app, 2, '__DIR__ NEWS_ARTICLES', 2, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause, clear_first)
                    VALUES (@sync_app, 1, 'debearco_common', 'NEWS_SOURCES', 'app = ''sports_ahl''', 0),
                           (@sync_app, 2, 'debearco_common', 'NEWS_ARTICLES', 'app = ''sports_ahl''', 1);

#
# Additional AHL data
#
SET @sync_app := 'sports_ahl_full';

INSERT INTO SERVER_SYNC (sync_app, name, sync_master, sync_slave, active)
                 VALUES (@sync_app, 'Sports AHL (Full)', 'data', 'dev', 1);
INSERT INTO SERVER_SYNC_STEPS (sync_app, sync_id, summary, sync_order, sync_type)
                 VALUES (@sync_app, 6, '__DIR__ SPORTS_AHL_GAME_EVENT',          1, 'database'),
                        (@sync_app, 1, '__DIR__ SPORTS_AHL_GAME_EVENT_GOAL',     2, 'database'),
                        (@sync_app, 2, '__DIR__ SPORTS_AHL_GAME_EVENT_ONICE',    3, 'database'),
                        (@sync_app, 3, '__DIR__ SPORTS_AHL_GAME_EVENT_PENALTY',  4, 'database'),
                        (@sync_app, 4, '__DIR__ SPORTS_AHL_GAME_EVENT_SHOOTOUT', 5, 'database'),
                        (@sync_app, 5, '__DIR__ SPORTS_AHL_GAME_EVENT_SHOT',     6, 'database'),
                        (@sync_app, 7, '__DIR__ SPORTS_AHL_PLAYERS_IMPORT',      7, 'database');
INSERT INTO SERVER_SYNC_DB (sync_app, sync_id, db_name, table_name, where_clause)
                    VALUES (@sync_app, 1, 'debearco_sports', 'SPORTS_AHL_GAME_EVENT_GOAL',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 2, 'debearco_sports', 'SPORTS_AHL_GAME_EVENT_ONICE',    CONCAT('season = ''', @season, '''')),
                           (@sync_app, 3, 'debearco_sports', 'SPORTS_AHL_GAME_EVENT_PENALTY',  CONCAT('season = ''', @season, '''')),
                           (@sync_app, 4, 'debearco_sports', 'SPORTS_AHL_GAME_EVENT_SHOOTOUT', CONCAT('season = ''', @season, '''')),
                           (@sync_app, 5, 'debearco_sports', 'SPORTS_AHL_GAME_EVENT_SHOT',     CONCAT('season = ''', @season, '''')),
                           (@sync_app, 6, 'debearco_sports', 'SPORTS_AHL_GAME_EVENT',          CONCAT('season = ''', @season, '''')),
                           (@sync_app, 7, 'debearco_sports', 'SPORTS_AHL_PLAYERS_IMPORT',      '');
