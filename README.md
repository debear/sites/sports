# DeBear Sports

The biggest site in the DeBear stable, though given the inputs it is the most difficult to maintain as it's pretty much all based on Perl-based integration run on the data server.

## Status

[![Version: 3.0](https://img.shields.io/badge/Version-3.0-brightgreen.svg)](https://gitlab.com/debear/sports/blob/master/CHANGELOG.md)
[![Build: 2181](https://img.shields.io/badge/Build-2181-yellow.svg)](https://gitlab.com/debear/sports/blob/master/CHANGELOG.md)
[![Skeleton: 1437](https://img.shields.io/badge/Skeleton-1437-orange.svg)](https://gitlab.com/debear/skeleton)
[![Coding Style: PSR-12](https://img.shields.io/badge/Coding_Style-PSR--12-lightgrey.svg)](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md)
[![Pipeline Status](https://gitlab.com/debear/sports/badges/master/pipeline.svg)](https://gitlab.com/debear/sports/commits/master)
[![Coverage Report](https://gitlab.com/debear/sites/sports/badges/master/coverage.svg)](https://gitlab.com/debear/sites/sports/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Motivation

The precursor to the [Fantasy](https://gitlab.com/debear/fantasy) site, it provides a visual representation of the data we are pulling across from the various integrations - as much for the validity of the data as a desire for an accompanying Sports site.

## Sport List

* F1
  * Integration: Wikipedia (Plan to switch to the FIA PDFs)
  * News: BBC, `motorsport.com`
* MotoGP / 2 / 3 / E - _(Decommissioned 2023)_
  * Integration: `motogp.com`
  * News: `motogp.com`, `motorsport.com`
* Speedway Grand Prix
  * Integration: None, provided by internal interface (Plan to switch to `speedwaygp.com`)
* NFL
  * Integration: `nfl.com`
  * News: Yahoo!
* MLB
  * Integration: `mlb.com`
  * News: Yahoo!
* NHL
  * Integration: `nhl.com`
  * News: Yahoo!
* AHL
  * Integration: `theahl.com`
  * News: `theahl.com`
* Tour de France - _(Decommissioned 2015)_
  * Integration: `letour.fr`
* Vuelta a España - _(Decommissioned 2015)_
  * Integration: `lavuelta.com`
* Giro d'Italia - _(Decommissioned 2015)_
  * Integration `gazzetta.it` (Up to 2012), `xml2.temporeale.gazzettaobjects.it` (Up to 2015)

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/sports/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/sports/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
