#!/usr/bin/php
<?php

/* Prepare our run. */
$dir_me = str_replace('.php', '', __FILE__);
require "$dir_me/helpers.inc";

/* Filesystem details. */
$dir_root = realpath(__DIR__ . '/../..');
$dir_res = "$dir_root/resources";
$dir_skel = realpath("$dir_root/../_debear");
$dir_skel_vendor = "$dir_skel/vendor";
$dir_skel_config = "$dir_skel/config/debear";
$dir_skel_res = "$dir_skel/resources";
$env_config = "$dir_root/config/_env.php";

/* Include our autoloader for class use. */
require "$dir_skel_vendor/autoload.php";

/* Load our local configuration. */
require "$dir_me/config.inc";

/* Load any default config, or bootstrap if new. */
$env = (file_exists($env_config) ? include $env_config : []);

/* Configuration array to write back. */
$subsite_res = [];
array_walk($subsites, function ($vals, $key) use (&$subsite_res) {
    if (is_string($key)) {
        $subsite_res = array_merge($subsite_res, $vals);
    }
});
$env['resources'] = [];
$env['subsites'] = array_fill_keys($subsite_res, ['resources' => []]);

/* Loop through and build. */
// Skeleton files from config.
$debear_subload = true; // Ensure configuration is loaded "fresh".
$skel_inc = array_intersect_key(include "$dir_skel_config/resources.php", ['css' => true, 'js' => true]);
foreach (['css', 'js'] as $type) {
    $skel[$type] = array_merge(array_filter(array_map(function ($a) use ($dir_skel_res, $type) {
        $f = (is_array($a) ? $a['file'] : $a);
        return (substr($f, 0, 5) == 'skel/' ? "$dir_skel_res/$type/$f" : false); // Skip local files we'll find later.
    }, $skel_inc[$type])), $skel[$type]);
}
// Subsites as all files on disk.
foreach ($subsites as $group => $section) {
    if (is_string($group)) {
        print "##\n## $group\n##\n\n";
    }
    // Loop through the resource types.
    foreach (['css', 'js'] as $type) {
        print "#\n# " . strtoupper($type) . "\n#\n";
        // Gather any relevant files for the subsite group.
        $group_files = (is_string($group) ? find_files($dir_res, $group, $type) : []);
        if (isset($common[$group][$type])) {
            $group_files = array_merge($group_files, $common[$group][$type]);
        }
        // Then loop through the subsites in this group.
        foreach ($section as $subsite) {
            print "- $subsite:\n";
            // Gather the relevant subsite files.
            $subsite_files = find_files($dir_res, $subsite, $type);
            $files = array_unique(array_merge($skel[$type], $common['_'][$type], $group_files, $subsite_files));
            // Ignore previous merges in this run.
            $files = array_filter($files, function ($f) use ($type) {
                return substr($f, 0 - strlen("_merged.$type")) != "_merged.$type";
            });
            // Create the minified file.
            $minified = "$subsite/_merged.$type";
            $fs_minified = "$dir_res/$type/$minified";
            if (!file_exists("$dir_res/$type/$subsite")) {
                // Ensure the destination folder exists, in case it has no specific files yet.
                print "  - Creating directory structure\n";
                mkdir("$dir_res/$type/$subsite");
            } elseif (file_exists($fs_minified)) {
                // Remove a previous merge.
                print "  - Removing previous merge.\n";
                unlink($fs_minified);
            }
            \DeBear\Helpers\Merge::process($files, '', $fs_minified, $type); // No $fs_dir when all paths are absolute.
            // Some debugging information.
            print "  - Following files merged in to '$minified'\n";
            print '    - ' . join("\n    - ", $files) . "\n";
            // Update the resulting configuration array.
            if (is_numeric($group)) {
                $env['resources']["{$type}_merge"] = $minified;
            } else {
                $env['subsites'][$subsite]['resources']["{$type}_merge"] = $minified;
            }
        }
        print "\n";
    }
}

/* Finally, update the on-disk configuration. */
$env = var_export($env, true); // Dump the contents of the array.
$env = str_replace('  ', '    ', $env); // Switch from two-space tabs to four.
$env = str_replace(')', ']', str_replace('array (', '[', $env)); // Switch array pattern to square brackets.
$env = preg_replace('/=>\s*\n\s*\[/m', '=> [', $env); // Move the initial square bracket to the first line.
file_put_contents($env_config, "<?php

/**
 * This file was auto-generated so local changes may be overwritten.
 * - 10-min-cssjs.php
 */

return $env;\n");
print "##\n## Environment file '$env_config' updated.\n##\n";
