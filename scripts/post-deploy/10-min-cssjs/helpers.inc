<?php

/**
 * Helper function: Locate all the relevant files within a resource subdir.
 * @param string $dir_res Resource directory.
 * @param string $subdir  Subdirectory to be parsed.
 * @param string $type    File type being searched.
 * @return array Resource files matching the passed criteria.
 */
function find_files(string $dir_res, string $subdir, string $type): array
{
    return glob("$dir_res/$type/$subdir{/,/*/,/*/*/,/*/*/*/,/*/*/*/*/}*.$type", GLOB_BRACE);
}

/**
 * Helper function: Pseudo-config helper (which we haven't loaded).
 * @param string $arg Configuration argument being requested.
 * @return string Appropriate configuration value.
 */
function config(string $arg): string
{
    $pseudo_map = [
        'app.env' => 'production', // As we're targetting a production build.
    ];
    return $pseudo_map[$arg];
}
