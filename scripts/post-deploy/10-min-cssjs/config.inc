<?php

// Sports per-subsite CSS/JS minification config.

$subsites = [
    ['_global'],
    'motorsport' => ['f1', 'sgp'],
    'major_league' => ['nfl', 'mlb', 'nhl', 'ahl'],
];
$common = [
    '_' => [
        'css' => array_merge(["$dir_res/css/icons.css"], glob("$dir_res/css/_common/*.css")),
        'js' => [],
    ],
    'motorsport' => [
        'css' => [
            "$dir_skel_res/css/skel/flags/48.css",
            "$dir_skel_res/css/skel/flags/64.css",
            "$dir_skel_res/css/skel/flags/128.css",
        ]
    ]
];
$skel = [
    'css' => [
        "$dir_skel_res/css/skel/widgets/scroller.css",
        "$dir_skel_res/css/skel/widgets/scrolltable.css",
        "$dir_skel_res/css/skel/widgets/modals.css",
    ],
    'js' => [
        "$dir_skel_res/js/skel/widgets/modals.js",
        "$dir_skel_res/js/skel/widgets/scroller.js",
        "$dir_skel_res/js/skel/widgets/scrolltable.js",
        "$dir_skel_res/js/skel/widgets/sorttable.js",
        "$dir_skel_res/js/skel/widgets/toggle.js",
    ],
];
