<?php

/**
 * CI-specific over-rides
 */

return [
    'subsites' => [
        'ahl' => [
            'setup' => [
                'season' => [
                    'max' => 2019,
                    'viewing' => 2019,
                ],
            ],
        ],
        'f1' => [
            'setup' => [
                'season' => [
                    'max' => 2020,
                    'default' => 2020,
                ],
            ],
        ],
        'mlb' => [
            'setup' => [
                'season' => [
                    'max' => 2020,
                    'viewing' => 2020,
                ],
                'draft' => [
                    'last' => 2019,
                ],
            ],
        ],
        'nfl' => [
            'setup' => [
                'season' => [
                    'max' => 2020,
                    'viewing' => 2019,
                ],
            ],
        ],
        'nhl' => [
            'setup' => [
                'season' => [
                    'max' => 2019,
                    'viewing' => 2019,
                ],
                'draft' => [
                    'last' => 2019,
                ],
            ],
        ],
        'sgp' => [
            'setup' => [
                'season' => [
                    'max' => 2020,
                    'default' => 2020,
                ],
            ],
        ],
    ],
];
