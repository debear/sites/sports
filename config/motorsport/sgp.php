<?php

return [
    'enabled' => true,

    /*
     * Site name details
     */
    'names' => [
        'section' => 'Speedway Grand Prix',
        'sport' => 'Grand Prix Speedway',
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'class' => 'SGP',
    ],

    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'sgp/layout.css',
            'sgp/common.css',
        ],
    ],

    /*
     * General setup about SGP
     */
    'setup' => [
        // Standard box colour.
        'box' => 'shale',
        // Season information.
        'season' => [
            'min' => 2010,
            'max' => 2025,
            'default' => 2025,
        ],
        // A reference to the mechanism by which a round is completed.
        'type' => 'heats',
        // That - and where - we should include an informative separator on the standings page.
        'standings' => [
            'separator' => [
                // Until 2019, the Top 8 qualified for the following season.
                ['from' => 2010, 'to' => 2019, 'num' => 8],
                // From 2020 onwards, it's only the Top 6.
                ['from' => 2020, 'to' => 2099, 'num' => 6],
            ],
        ],
        // Stat Leader details.
        'stats' => [
            'groups' => [
                'meeting' => 'Meeting',
                'heat' => 'Heat',
            ],
            'seasons' => [
                2 => ['from' => 2010, 'to' => 2024],
                11 => ['from' => 2025, 'to' => 2099],
            ],
            'default' => 9,
        ],
        // Participant-related details.
        'participants' => [
            'url' => 'riders',
            'broken-down' => false,
            'race-num' => 'bib_no',
        ],
        // Team-related details.
        'teams' => [
            'display' => false,
            'history' => false,
        ],
        // Priority flags.
        'priority-flags' => [
            'au', 'cz', 'dk', 'fi', 'gb', 'pl', 'ru', 'se', 'si', 'sk',
        ],
        // Possible results in a heat.
        'results' => [
            '1', '2', '3', '4', 'r', 'm', 't', 'f', 'x',
        ],
    ],

    /*
     * Weather setup
     */
    'weather' => [
        'app' => 'sports_sgp',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'icon' => 'home',
            'descrip' => 'All the latest results and stats from the {config|debear.setup.season.default} '
                . 'Speedway Grand Prix season.',
            'url-section' => true,
            'url-season' => 'query',
            'navigation' => [
                'enabled' => true,
            ],
            'footer' => [
                'enabled' => false,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 1.0,
                'frequency' => 'daily',
            ],
        ],
        'races' => [
            'url' => '/meetings',
            'label' => 'Meetings',
            'icon' => 'calendar',
            'descrip' => 'The {config|debear.setup.season.viewing} Speedway Grand Prix meetings list.',
            'descrip_ind' => 'Results from the {data|season} {data|name}.',
            'order' => 40,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'weekly',
            ],
        ],
        'standings' => [
            'url' => '/standings',
            'label' => 'Standings',
            'icon' => 'standings',
            'descrip' => 'Standings for the {config|debear.setup.season.viewing} Speedway Grand Prix season.',
            'order' => 50,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 1.0,
                'frequency' => 'weekly',
                'toggle_closed' => true,
            ],
        ],
        'stats' => [
            'url' => '/stats',
            'label' => 'Stat Leaders',
            'icon' => 'stats',
            'descrip' => 'Statistical comparison of the {config|debear.setup.season.viewing} Speedway Grand Prix '
                . 'season.',
            'order' => 60,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.8,
                'frequency' => 'weekly',
                'toggle_closed' => true,
            ],
        ],
        'participants' => [
            'url' => '/riders',
            'label' => 'Riders',
            'icon' => 'people',
            'descrip' => 'All the riders taking part in the {config|debear.setup.season.viewing} Speedway Grand Prix '
                . 'season.',
            'descrip_ind' => 'Results, stats and history for the Speedway Grand Prix rider {data|name}.',
            'order' => 70,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'weekly',
            ],
        ],
        'header-meetings' => [
            'url' => '/admin',
            'label' => 'Meeting Admin',
            'order' => 88,
            'url-section' => true,
            'policy' => 'user:admin',
            'header' => true,
        ],
        'footer-sgp' => [
            'url' => '/sgp',
            'label' => 'Speedway Grand Prix Home',
            'order' => 25,
            'url-season' => 'query',
            'footer' => [
                'enabled' => true,
            ],
        ],
        'sitemap' => [
            'url' => '/sgp/sitemap',
            'url-season' => 'query',
        ],
    ],

    /*
     * Page content values
     */
    'content' => [
        'footer' => [
            'content' => 'Rider Images &copy; 2010-' . date('Y') . ' BSI Speedway. All other copyrights as stated.',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/sgp.png',
        ],
        'theme_colour' => '#C76114',
    ],
];
