<?php

return [
    'enabled' => true,

    /*
     * Site name details
     */
    'names' => [
        'section' => 'Formula One',
        'sport' => 'Formula One',
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'class' => 'FIA',
    ],

    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'f1/layout.css',
        ],
    ],

    /*
     * General setup about F1
     */
    'setup' => [
        // Standard box colour.
        'box' => 'track',
        // Season information.
        'season' => [
            'min' => 2008,
            'max' => 2025,
            'default' => 2025,
            'car_num_perm' => 2014,
        ],
        // A reference to the mechanism by which a round is completed.
        'type' => 'circuit',
        // Number of inidividual sessions involved in 'qualifying'.
        'quali' => [
            'sessions' => 3,
            'sprint' => ['sq1', 'sq2', 'sq3'],
        ],
        // Number of slots per grid row at the start of a race.
        'grid' => [
            'num-per-row' => 2,
        ],
        // Stat Leader details.
        'stats' => [
            'groups' => [
                'quali' => 'Qualifying',
                'race' => 'Race',
            ],
            'default' => 14,
        ],
        // Participant-related details.
        'participants' => [
            'url' => 'drivers',
            'broken-down' => false,
            'race-num' => 'car_no',
            'summarised' => 'driver_code',
        ],
        // Team-related details.
        'teams' => [
            'display' => true,
            'history' => true,
        ],
    ],

    /*
     * News setup
     */
    'news' => [
        'app' => 'sports_f1',
    ],

    /*
     * Weather setup
     */
    'weather' => [
        'app' => 'sports_f1',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'icon' => 'home',
            'descrip' => 'All the latest results, stats and news from the {config|debear.setup.season.default} '
                . 'Formula One season.',
            'url-section' => true,
            'url-season' => 'query',
            'navigation' => [
                'enabled' => true,
            ],
            'footer' => [
                'enabled' => false,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 1.0,
                'frequency' => 'daily',
            ],
        ],
        'news' => [
            'url' => '/news',
            'label' => 'News',
            'icon' => 'news',
            'descrip' => 'The latest Formula One news.',
            'order' => 30,
            'url-section' => true,
            'url-season' => 'query',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.6,
                'frequency' => 'daily',
            ],
        ],
        'races' => [
            'url' => '/races',
            'label' => 'Races',
            'icon' => 'calendar',
            'descrip' => 'The {config|debear.setup.season.viewing} Formula One race calendar.',
            'descrip_ind' => 'Results and timings from the {data|season} {data|name}.',
            'order' => 40,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'weekly',
            ],
        ],
        'standings' => [
            'url' => '/standings',
            'label' => 'Standings',
            'icon' => 'standings',
            'descrip' => 'Standings for the {config|debear.setup.season.viewing} Formula One season.',
            'order' => 50,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 1.0,
                'frequency' => 'weekly',
                'toggle_closed' => true,
            ],
        ],
        'stats' => [
            'url' => '/stats',
            'label' => 'Stat Leaders',
            'icon' => 'stats',
            'descrip' => 'Statistical comparison of the {config|debear.setup.season.viewing} Formula One season.',
            'order' => 60,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.8,
                'frequency' => 'weekly',
                'toggle_closed' => true,
            ],
        ],
        'participants' => [
            'url' => '/teams',
            'label' => 'Teams and Drivers',
            'icon' => 'people',
            'descrip' => 'All the teams and drivers taking part in the {config|debear.setup.season.viewing} '
                . 'Formula One season.',
            'descrip_ind' => 'Results, stats and history for the Formula One driver {data|name}.',
            'descrip_team' => 'Results, stats and history for the {data|name} Formula One team.',
            'order' => 70,
            'url-section' => true,
            'url-season' => 'append',
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'weekly',
            ],
        ],
        'footer-f1' => [
            'url' => '/f1',
            'label' => 'Formula One Home',
            'order' => 25,
            'url-season' => 'query',
            'footer' => [
                'enabled' => true,
            ],
        ],
        'sitemap' => [
            'url' => '/f1/sitemap',
            'url-season' => 'query',
        ],
    ],

    /*
     * Page content values
     */
    'content' => [
        'footer' => [
            'content' => 'Driver Images &copy; 1999-' . date('Y') . ' Formula One World Championship Limited. '
                . 'All other copyrights as stated.',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/f1.png',
        ],
    ],
];
