<?php

return [
    'enabled' => true,

    /*
     * Site name details
     */
    'names' => [
        'section' => 'NHL',
        'section-full' => 'National Hockey League',
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'class' => 'NHL',
    ],

    /*
     * General setup about the NHL
     */
    'setup' => [
        // Standard box colour.
        'box' => 'ice',
        // Season information.
        'season' => [
            'min' => 2007,
            'max' => 2024,
            'viewing' => 2024,
        ],
        // Individual game tabs.
        'game' => [
            'tabs' => [
                [
                    'label' => ['regular' => 'Season Series', 'playoff' => 'Playoff Series'],
                    'icon' => 'series',
                    'link' => 'series',
                ],
                [
                    'label' => 'Game Events',
                    'icon' => 'locations',
                    'link' => 'events',
                ],
                [
                    'label' => 'Scoring',
                    'icon' => 'scoring',
                    'link' => 'scoring',
                ],
                [
                    'label' => 'Boxscore',
                    'icon' => 'boxscore',
                    'link' => false,
                ],
                [
                    'label' => 'Game Stats',
                    'icon' => 'stats',
                    'link' => 'stats',
                ],
            ],
        ],
        // Standings columns configuration.
        'standings' => [
            'key' => [
                '*' => 'President&#39;s Trophy',
                'z' => 'Conference Top Seed',
                'y' => 'Division Title',
                'x' => 'Playoff Berth',
                'w' => [
                    'label' => 'Wildcard',
                    'from' => 2013,
                ],
                'q' => [
                    'label' => 'Preliminary Round',
                    'from' => 2019,
                    'to' => 2019,
                ],
            ],
            'columns' => [
                'gp' => [
                    'label' => 'GP',
                ],
                'w' => [
                    'col' => 'wins',
                    'label' => 'W',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'l' => [
                    'col' => 'loss',
                    'label' => 'L',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'otl' => [
                    'col' => 'ot_loss',
                    'label' => 'OTL',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'row' => [
                    'col' => 'reg_ot_wins',
                    'label' => 'ROW',
                    'div' => [
                        'skip' => true,
                    ],
                    'conf' => [
                        'css' => 'hidden-m hidden-t',
                    ],
                ],
                'pts' => [
                    'label' => 'Pts',
                ],
                'pts_pct' => [
                    'label' => 'Pts %',
                    'periods' => [
                        ['from' => 2019, 'to' => 2019],
                    ],
                ],
                'gf' => [
                    'col' => 'goals_for',
                    'label' => 'GF',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'ga' => [
                    'col' => 'goals_against',
                    'label' => 'GA',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'gd' => [
                    'label' => 'Diff',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'home' => [
                    'label' => 'Home',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'visitor' => [
                    'label' => 'Visitor',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'div' => [
                    'label' => 'v Div',
                    'periods' => [
                        ['to' => 2019],
                        ['from' => 2021],
                    ],
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'recent' => [
                    'label' => 'Last 10',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
            ],
            'hide_conf' => [2020],
        ],
        // Playoff Rounds.
        'playoffs' => [
            'trophy' => 'Stanley Cup',
            'title' => 'Stanley Cup Playoffs',
            'trophy_reg' => 'President&#39;s Trophy',
            // There was a round robin in 2019/20.
            'round_robin' => [
                ['from' => 2019, 'to' => 2019, 'num' => 4],
            ],
            // The bracket is divisional in certain periods.
            'divisional' => [
                ['from' => 2013, 'to' => 2018, 'num' => 3, 'wc' => 2],
                ['from' => 2020, 'to' => 2020, 'num' => 4, 'wc' => 0],
                ['from' => 2021, 'to' => 2099, 'num' => 3, 'wc' => 2],
            ],
            // Series lengths (wins required).
            'series' => [
                // Prelim (2019/20 only).
                0 => [
                    ['from' => 2019, 'to' => 2019, 'wins' => 3, 'short' => 'Prelim', 'full' => 'Preliminary Round'],
                ],
                // CQF / DSF.
                1 => [
                    ['from' => 2007, 'to' => 2012, 'wins' => 4, 'short' => 'CQF', 'full' => 'Conference Quarter Final'],
                    ['from' => 2013, 'to' => 2099, 'wins' => 4, 'short' => 'DSF', 'full' => 'Division Semi Final'],
                ],
                // CSF / DF.
                2 => [
                    ['from' => 2007, 'to' => 2012, 'wins' => 4, 'short' => 'CSF', 'full' => 'Conference Semi Final'],
                    ['from' => 2013, 'to' => 2099, 'wins' => 4, 'short' => 'DF', 'full' => 'Division Final'],
                ],
                // CF / SF (in 2020/21).
                3 => [
                    ['from' => 2007, 'to' => 2019, 'wins' => 4, 'short' => 'CF', 'full' => 'Conference Final'],
                    ['from' => 2020, 'to' => 2020, 'wins' => 4, 'short' => 'SF', 'full' => 'Semi Final'],
                    ['from' => 2021, 'to' => 2099, 'wins' => 4, 'short' => 'CF', 'full' => 'Conference Final'],
                ],
                // SCF.
                4 => ['wins' => 4, 'short' => 'SCF', 'full' => 'Stanley Cup Final'],
            ],
            // Historical codes.
            'codes' => [
                'PRE' => ['full' => 'Preliminary Round', 'short' => 'Prelim'],
                'CQF' => ['full' => 'Conference Quarter Final', 'short' => 'Conf QF'],
                'CSF' => ['full' => 'Conference Semi Final', 'short' => 'Conf SF'],
                'CF' => ['full' => 'Conference Final', 'short' => 'Conf Final'],
                'DSF' => ['full' => 'Division Semi Final', 'short' => 'Div SF'],
                'DF' => ['full' => 'Division Final', 'short' => 'Div Final'],
                'QF' => ['full' => 'Quarter Final', 'short' => 'QF'],
                'SF' => ['full' => 'Semi Final', 'short' => 'SF'],
                'F' => ['full' => 'Final', 'short' => 'Final'],
                'LF' => ['full' => 'League Final', 'short' => 'League Final'],
                'SCF' => ['full' => 'Stanley Cup Final', 'short' => 'Stanley Cup'],
            ],
            // League trophies.
            'league-trophies' => [
                2 => 'Presidents&#39; Trophy', // NHL.
            ],
            'playoff-trophies' => [
                1 => 'O&#39;Brien Cup', // NHA.
                2 => 'Stanley Cup', // NHL.
                3 => 'Avco World Trophy', // WHA.
            ],
        ],
        'players' => [
            'status' => [
                'ir' => ['short' => 'IR', 'long' => 'Injury Reserve'],
                'na' => ['short' => 'NA', 'long' => 'Not Available'],
            ],
            'injuries' => [
                'o' => ['short' => 'O', 'long' => 'Out'],
            ],
            'subnav' => [
                'heatmaps' => [
                    'label' => 'Heatmaps',
                    'order' => 40,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'seasonStats',
                        'heatmaps',
                    ],
                    'validation' => 'heatmap',
                    'dropdowns' => [
                        'season' => true,
                        'heatmaps' => true,
                    ],
                ],
            ],
            'groups' => [
                'pos' => 'G',
                'is' => 'goalie',
                'not' => 'skater',
            ],
            'header_custom' => 'Shoots / Catches',
            'heatmap_start' => 2010,
        ],
        // Stat Leader categories.
        'stats' => [
            'summary' => [
                // Player stats.
                'pts' => [
                    'name' => 'Points',
                    'type' => 'player',
                    'class' => 'skater',
                    'column' => 'points',
                ],
                'goals' => [
                    'name' => 'Goals',
                    'type' => 'player',
                    'class' => 'skater',
                    'column' => 'goals',
                ],
                'corsi' => [
                    'name' => 'Relative Corsi',
                    'type' => 'player',
                    'class' => 'skater-advanced',
                    'column' => 'corsi_rel',
                ],
                'wins' => [
                    'name' => 'Wins',
                    'type' => 'player',
                    'class' => 'goalie',
                    'column' => 'win',
                ],
                'gaa' => [
                    'name' => 'Goals Against Average',
                    'name_short' => 'GAA',
                    'type' => 'player',
                    'class' => 'goalie',
                    'column' => 'goals_against_avg',
                ],
                'save_pct' => [
                    'name' => 'Save Percentage',
                    'type' => 'player',
                    'class' => 'goalie',
                    'column' => 'save_pct',
                ],
                // Team stats.
                'team-goals_for' => [
                    'name' => 'Goals For',
                    'type' => 'team',
                    'class' => 'skater',
                    'column' => 'goals',
                ],
                'team-pp' => [
                    'name' => 'Power Play',
                    'type' => 'team',
                    'class' => 'skater',
                    'column' => 'pp_pct',
                ],
                'team-pk' => [
                    'name' => 'Penalty Kill',
                    'type' => 'team',
                    'class' => 'skater',
                    'column' => 'pk_pct',
                ],
                'team-goals_against' => [
                    'name' => 'Goals Against',
                    'type' => 'team',
                    'class' => 'goalie',
                    'column' => 'goals_against',
                ],
                'team-save_pct' => [
                    'name' => 'Save Percentage',
                    'type' => 'team',
                    'class' => 'goalie',
                    'column' => 'save_pct',
                ],
                'team-corsi' => [
                    'name' => 'Corsi',
                    'type' => 'team',
                    'class' => 'skater-advanced',
                    'column' => 'corsi_pct',
                ],
            ],
            'details' => [
                'player' => [
                    'skater' => [
                        'name' => 'Skater',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonSkaterSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonSkater::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerGameSkater::class,
                            'charts' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonSkaterHeatmap::class,
                        ],
                        'default' => 'points',
                        'heatmaps' => ['shots', 'goals', 'hits', 'penalties'],
                    ],
                    'skater-advanced' => [
                        'name' => 'Advanced Skater',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonSkaterAdvancedSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonSkaterAdvanced::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerGameSkaterAdvanced::class,
                        ],
                        'default' => 'corsi_rel',
                    ],
                    'goalie' => [
                        'name' => 'Goalie',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonGoalieSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonGoalie::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerGameGoalie::class,
                            'charts' => \DeBear\ORM\Sports\MajorLeague\NHL\PlayerSeasonGoalieHeatmap::class,
                        ],
                        'default' => 'goals_against_avg',
                        'heatmaps' => ['shots', 'saves', 'goals'],
                    ],
                ],
                'team' => [
                    'skater' => [
                        'name' => 'Skater',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NHL\TeamSeasonSkaterSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\NHL\TeamSeasonSkaterSorted::class,
                        ],
                        'default' => 'goals',
                    ],
                    'skater-advanced' => [
                        'name' => 'Advanced Skater',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NHL\TeamSeasonSkaterAdvancedSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\NHL\TeamSeasonSkaterAdvancedSorted::class,
                        ],
                        'default' => 'corsi_pct',
                    ],
                    'goalie' => [
                        'name' => 'Goalie',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NHL\TeamSeasonGoalieSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\NHL\TeamSeasonGoalieSorted::class,
                        ],
                        'default' => 'goals_against_avg',
                    ],
                ],
            ],
        ],
        'odds' => [
            'display' => [
                'moneyline' => true,
                'spread' => true,
                'over_under' => true,
            ],
        ],
    ],

    /*
     * News setup
     */
    'news' => [
        'app' => 'sports_nhl',
    ],

    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'nhl/layout.css',
            'nhl/general.css',
            'nhl/teams/colours.css',
            'nhl/teams/tiny.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/nhl.png',
        ],
        'theme_colour' => '#4f94cd',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'lineups' => [
            'url' => '/lineups',
            'label' => 'Starting Lineups',
            'icon' => 'lineups',
            'section' => 'schedule',
            'descrip' => 'The starting lineups announced for upcoming NHL games.',
            'descrip-page' => 'The starting lineups announced for NHL games on {data|date_fmt}.',
            'order' => 24,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'daily',
            ],
        ],
        'playoffs' => [
            'descrip-rr' => 'A summary of the {data|title} {data|conf} round robin matchups.',
        ],
        'draft' => [
            'url' => '/draft',
            'label' => 'Draft',
            'icon' => 'draft',
            'section' => 'players',
            'descrip' => 'Round-by-Round picks from the {config|debear.setup.season.viewing} NHL Entry Draft.',
            'order' => 74,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.1,
                'frequency' => 'monthly',
            ],
        ],
        'footer-nhl' => [
            'url' => '/nhl',
            'label' => 'NHL Home',
            'order' => 25,
            'footer' => [
                'enabled' => true,
            ],
        ],
        'sitemap' => [
            'url' => '/nhl/sitemap',
        ],
    ],
];
