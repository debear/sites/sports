<?php

return [
    'enabled' => true,

    /*
     * Site name details
     */
    'names' => [
        'section' => 'AHL',
        'section-full' => 'American Hockey League',
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'class' => 'AHL',
    ],

    /*
     * General setup about the AHL
     */
    'setup' => [
        // Standard box colour.
        'box' => 'ice',
        // Season information.
        'season' => [
            'min' => 2005,
            'max' => 2024,
            'viewing' => 2024,
        ],
        // Individual game tabs.
        'game' => [
            'tabs' => [
                [
                    'label' => ['regular' => 'Season Series', 'playoff' => 'Playoff Series'],
                    'icon' => 'series',
                    'link' => 'series',
                ],
                [
                    'label' => 'Scoring',
                    'icon' => 'scoring',
                    'link' => 'scoring',
                ],
                [
                    'label' => 'Boxscore',
                    'icon' => 'boxscore',
                    'link' => false,
                ],
                [
                    'label' => 'Game Stats',
                    'icon' => 'stats',
                    'link' => 'stats',
                ],
            ],
        ],
        // Standings columns configuration.
        'standings' => [
            'key' => [
                '*' => 'Kilpatrick Trophy',
                'z' => 'Conference Top Seed',
                'y' => 'Division Title',
                'b' => [
                    'label' => 'First Round Bye',
                    'from' => 2021,
                ],
                'x' => 'Playoff Berth',
                'c' => [
                    'label' => 'Division Crossover',
                    'to' => 2010,
                ],
            ],
            'columns' => [
                'gp' => [
                    'label' => 'GP',
                ],
                'w' => [
                    'col' => 'wins',
                    'label' => 'W',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'l' => [
                    'col' => 'loss',
                    'label' => 'L',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'otl' => [
                    'col' => 'ot_loss',
                    'label' => 'OTL',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'sol' => [
                    'col' => 'so_loss',
                    'label' => 'SOL',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'row' => [
                    'col' => 'reg_ot_wins',
                    'label' => 'ROW',
                    'div' => [
                        'skip' => true,
                    ],
                    'conf' => [
                        'css' => 'hidden-m hidden-t',
                    ],
                ],
                'pts' => [
                    'label' => 'Pts',
                    'conf' => [
                        'periods' => [
                            ['to' => 2014],
                            ['from' => 2022],
                        ],
                    ],
                ],
                'pts_pct' => [
                    'label' => 'Pts %',
                    'periods' => [
                        ['from' => 2015, 'to' => 2021],
                    ],
                ],
                'gf' => [
                    'col' => 'goals_for',
                    'label' => 'GF',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'ga' => [
                    'col' => 'goals_against',
                    'label' => 'GA',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'gd' => [
                    'label' => 'Diff',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'home' => [
                    'label' => 'Home',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'visitor' => [
                    'label' => 'Visitor',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'div' => [
                    'label' => 'v Div',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'recent' => [
                    'label' => 'Last 10',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
            ],
            'hide_conf' => [2020],
        ],
        // Playoff Rounds.
        'playoffs' => [
            'trophy' => 'Calder Cup',
            'title' => 'Calder Cup Playoffs',
            'not-awarded' => [
                2019 => true,
                2020 => true,
            ],
            'info' => [
                [
                    'from' => 2019,
                    'to' => 2019,
                    'info' => 'The 2019/20 Calder Cup Playoffs Playoffs were not held after the AHL suspended play due '
                        . 'to the COVID-19 pandemic. The Calder Cup was not awarded that season.',
                ],
                [
                    'from' => 2020,
                    'to' => 2020,
                    'info' => 'The 2020/21 Calder Cup Playoffs Playoffs were not held due to the COVID-19 pandemic. '
                        . 'The Calder Cup was not awarded that season.',
                ],
            ],
            'template' => [
                ['from' => 2020, 'to' => 2020, 'template' => 'ahl.playoffs.2020-pacific'], // Pacific Division playoff.
                ['from' => 2021, 'to' => 2099, 'template' => 'ahl.playoffs.2021-expanded'], // Expanded from 2021.
            ],
            // The bracket is divisional in certain periods.
            'divisional' => [
                ['from' => 2005, 'to' => 2010, 'num' => 4, 'wc' => 0],
                ['from' => 2015, 'to' => 2099, 'num' => 4, 'wc' => 0],
            ],
            // Series lengths (wins required).
            'series' => [
                // First Round.
                0 => [
                    ['from' => 2021, 'to' => 2099, 'wins' => 2, 'short' => '1R', 'full' => 'First Round'],
                ],
                // CQF / DSF.
                1 => [
                    // Until 2010/11, Best of 7.
                    ['from' => 2005, 'to' => 2010, 'wins' => 4, 'short' => 'DSF', 'full' => 'Division Semi Final'],
                    // From 2011/12 until 2018/19, Best of 5.
                    ['from' => 2011, 'to' => 2014, 'wins' => 3, 'short' => 'CQF', 'full' => 'Conference Quarter Final'],
                    ['from' => 2015, 'to' => 2018, 'wins' => 3, 'short' => 'DSF', 'full' => 'Division Semi Final'],
                    // In 2020/21, single elimination.
                    ['from' => 2020, 'to' => 2020, 'wins' => 1, 'short' => 'PI', 'full' => 'Play-In'],
                    // From 2021/22, Best of 5.
                    ['from' => 2021, 'to' => 2099, 'wins' => 3, 'short' => 'DSF', 'full' => 'Division Semi Final'],
                ],
                // CSF / DF.
                2 => [
                    // Until 2018/19, Best of 7.
                    ['from' => 2005, 'to' => 2010, 'wins' => 4, 'short' => 'DF', 'full' => 'Division Final'],
                    ['from' => 2011, 'to' => 2014, 'wins' => 4, 'short' => 'CSF', 'full' => 'Conference Semi Final'],
                    ['from' => 2015, 'to' => 2018, 'wins' => 4, 'short' => 'DF', 'full' => 'Division Final'],
                    // In 2020/21, single elimination.
                    ['from' => 2020, 'to' => 2020, 'wins' => 1, 'short' => 'PIF', 'full' => 'Play-In Final'],
                    // From 2021/22, Best of 5.
                    ['from' => 2021, 'to' => 2099, 'wins' => 3, 'short' => 'DF', 'full' => 'Division Final'],
                ],
                // CF.
                3 => [
                    // Until 2018/19, Best of 7.
                    ['from' => 2005, 'to' => 2018, 'wins' => 4, 'short' => 'CF', 'full' => 'Conference Final'],
                    // In 2020/21, Best of 3.
                    ['from' => 2020, 'to' => 2020, 'wins' => 2, 'short' => 'DSF', 'full' => 'Division Final'],
                    // From 2021/22, Best of 7.
                    ['from' => 2021, 'to' => 2099, 'wins' => 4, 'short' => 'CF', 'full' => 'Conference Final'],
                ],
                // CCF.
                4 => [
                    // Until 2018/19, Best of 7.
                    ['from' => 2005, 'to' => 2018, 'wins' => 4, 'short' => 'CCF', 'full' => 'Calder Cup Final'],
                    // In 2020/21, Best of 3.
                    ['from' => 2020, 'to' => 2020, 'wins' => 2, 'short' => 'DF', 'full' => 'Division Final'],
                    // From 2021/22, Best of 7.
                    ['from' => 2021, 'to' => 2099, 'wins' => 4, 'short' => 'CCF', 'full' => 'Calder Cup Final'],
                ],
            ],
            // Historical codes.
            'codes' => [
                'PRE' => ['full' => 'Preliminary Round', 'short' => 'Prelim'],
                'DSF' => ['full' => 'Division Semi Final', 'short' => 'Div SF'],
                'DF' => ['full' => 'Division Final', 'short' => 'Div Final'],
                'CQF' => ['full' => 'Conference Quarter Final', 'short' => 'Conf QF'],
                'CSF' => ['full' => 'Conference Semi Final', 'short' => 'Conf SF'],
                'CF' => ['full' => 'Conference Final', 'short' => 'Conf Final'],
                'CCF' => ['full' => 'Calder Cup Final', 'short' => 'Calder Cup'],
                '1R' => ['full' => 'First Round', 'short' => '1R'],
                'QF' => ['full' => 'Quarter Final', 'short' => 'QF'],
                'SF' => ['full' => 'Semi Final', 'short' => 'SF'],
                'F' => ['full' => 'Final', 'short' => 'Final'],
                'TCF' => ['full' => 'Turner Cup Final', 'short' => 'Turner Cup'],
            ],
            // League trophies.
            'league-trophies' => [
                17 => 'Fred A. Huber Trophy', // IHL.
                26 => 'Kilpatrick Trophy', // AHL.
            ],
            'playoff-trophies' => [
                17 => 'Turner Cup', // IHL.
                26 => 'Calder Cup', // AHL.
            ],
        ],
        'players' => [
            'status' => [
                'na' => ['short' => 'NA', 'long' => 'Not Available'],
            ],
            'subnav' => [
                'heatmaps' => [
                    'label' => 'Heatmaps',
                    'order' => 40,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'seasonStats',
                        'heatmaps',
                    ],
                    'validation' => 'heatmap',
                    'dropdowns' => [
                        'season' => true,
                        'heatmaps' => true,
                    ],
                ],
            ],
            'groups' => [
                'pos' => 'G',
                'is' => 'goalie',
                'not' => 'skater',
            ],
            'header_custom' => 'Shoots / Catches',
            'heatmap_start' => 2017,
        ],
        // Stat Leader categories.
        'stats' => [
            'summary' => [
                // Player stats.
                'pts' => [
                    'name' => 'Points',
                    'type' => 'player',
                    'class' => 'skater',
                    'column' => 'points',
                ],
                'goals' => [
                    'name' => 'Goals',
                    'type' => 'player',
                    'class' => 'skater',
                    'column' => 'goals',
                ],
                'shots' => [
                    'name' => 'Shots',
                    'type' => 'player',
                    'class' => 'skater',
                    'column' => 'shots',
                ],
                'wins' => [
                    'name' => 'Wins',
                    'type' => 'player',
                    'class' => 'goalie',
                    'column' => 'win',
                ],
                'gaa' => [
                    'name' => 'Goals Against Average',
                    'name_short' => 'GAA',
                    'type' => 'player',
                    'class' => 'goalie',
                    'column' => 'goals_against_avg',
                ],
                'save_pct' => [
                    'name' => 'Save Percentage',
                    'type' => 'player',
                    'class' => 'goalie',
                    'column' => 'save_pct',
                ],
                // Team stats.
                'team-goals_for' => [
                    'name' => 'Goals For',
                    'type' => 'team',
                    'class' => 'skater',
                    'column' => 'goals',
                ],
                'team-pp' => [
                    'name' => 'Power Play',
                    'type' => 'team',
                    'class' => 'skater',
                    'column' => 'pp_pct',
                ],
                'team-pk' => [
                    'name' => 'Penalty Kill',
                    'type' => 'team',
                    'class' => 'skater',
                    'column' => 'pk_pct',
                ],
                'team-shoot_pct' => [
                    'name' => 'Shooting Percentage',
                    'name_short' => 'Shooting %age',
                    'type' => 'team',
                    'class' => 'skater',
                    'column' => 'shot_pct',
                ],
                'team-goals_against' => [
                    'name' => 'Goals Against',
                    'type' => 'team',
                    'class' => 'goalie',
                    'column' => 'goals_against',
                ],
                'team-save_pct' => [
                    'name' => 'Save Percentage',
                    'type' => 'team',
                    'class' => 'goalie',
                    'column' => 'save_pct',
                ],
            ],
            'details' => [
                'player' => [
                    'skater' => [
                        'name' => 'Skater',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerSeasonSkaterSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerSeasonSkater::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerGameSkater::class,
                            'charts' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerSeasonSkaterHeatmap::class,
                        ],
                        'default' => 'points',
                        'heatmaps' => ['shots', 'goals'],
                    ],
                    'goalie' => [
                        'name' => 'Goalie',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerSeasonGoalieSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerSeasonGoalie::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerGameGoalie::class,
                            'charts' => \DeBear\ORM\Sports\MajorLeague\AHL\PlayerSeasonGoalieHeatmap::class,
                        ],
                        'default' => 'goals_against_avg',
                        'heatmaps' => ['saves'],
                    ],
                ],
                'team' => [
                    'skater' => [
                        'name' => 'Skater',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\AHL\TeamSeasonSkaterSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\AHL\TeamSeasonSkater::class,
                        ],
                        'default' => 'goals',
                    ],
                    'goalie' => [
                        'name' => 'Goalie',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\AHL\TeamSeasonGoalieSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\AHL\TeamSeasonGoalie::class,
                        ],
                        'default' => 'goals_against_avg',
                    ],
                ],
            ],
        ],
    ],

    /*
     * News setup
     */
    'news' => [
        'app' => 'sports_ahl',
    ],

    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'ahl/layout.css',
            'ahl/general.css',
            'ahl/teams/colours.css',
            'ahl/teams/tiny.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/ahl.png',
        ],
        'theme_colour' => '#4f94cd',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'footer-ahl' => [
            'url' => '/ahl',
            'label' => 'AHL Home',
            'order' => 25,
            'footer' => [
                'enabled' => true,
            ],
        ],
        'sitemap' => [
            'url' => '/ahl/sitemap',
        ],
    ],
];
