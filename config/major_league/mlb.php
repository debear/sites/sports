<?php

return [
    'enabled' => true,

    /*
     * Site name details
     */
    'names' => [
        'section' => 'MLB',
        'section-full' => 'Major League Baseball',
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'class' => 'MLB',
    ],

    /*
     * CDN details
     */
    'cdn' => [
        'sizes' => [
            'pbp' => ['w' => 40, 'h' => 60],
        ],
    ],

    /*
     * General setup about the MLB
     */
    'setup' => [
        // Standard box colour.
        'box' => 'grass',
        // Season information.
        'season' => [
            'min' => 2008,
            'max' => 2025,
            'viewing' => 2025,
        ],
        // Individual game tabs.
        'game' => [
            'tabs' => [
                [
                    'label' => ['regular' => 'Season Series', 'playoff' => 'Playoff Series'],
                    'icon' => 'series',
                    'link' => 'series',
                ],
                [
                    'label' => 'Scoring',
                    'icon' => 'scoring',
                    'link' => 'scoring',
                ],
                [
                    'label' => 'Boxscore',
                    'icon' => 'boxscore',
                    'link' => false,
                ],
                [
                    'label' => 'Win Probability',
                    'icon' => 'probability',
                    'link' => 'probability',
                ],
            ],
        ],
        // Pitch types.
        'pitches' => [
            'AB' => 'Automatic Ball',
            'CH' => 'Changeup',
            'CS' => 'Slow Curve',
            'CU' => 'Curveball',
            'EP' => 'Eephus',
            'FA' => 'Fastball',
            'FC' => 'Cut Fastball',
            'FF' => '4-Seam Fastball',
            'FO' => 'Forkball',
            'FS' => 'Split-finger',
            'FT' => '2-Seam Fastball',
            'IN' => 'Intentional Ball',
            'KC' => 'Knuckle Curve',
            'KN' => 'Knuckleball',
            'PO' => 'Pitchout',
            'SC' => 'Screwball',
            'SI' => 'Sinker',
            'SL' => 'Slider',
            'UN' => 'Unknown',
        ],
        // Standings columns configuration.
        'standings' => [
            'key' => [
                '*' => [
                    'label' => 'Home Field up to World Series',
                    'from' => 2017,
                ],
                'z' => 'Home Field up to LCS',
                'y' => 'Division Champion',
                'w' => 'Wild Card',
                'x' => 'Playoff Spot',
            ],
            'columns' => [
                'w' => [
                    'col' => 'wins',
                    'label' => 'W',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'l' => [
                    'col' => 'loss',
                    'label' => 'L',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'record' => [
                    'div' => [
                        'skip' => true,
                    ],
                    'conf' => [
                        'css' => 'col-first hidden-m hidden-t',
                    ],
                ],
                'pct' => [
                    'col' => 'win_pct',
                    'label' => 'Win %',
                ],
                'gb' => [
                    'label' => 'GB',
                    'div' => [
                        'col' => 'games_back_div',
                    ],
                    'conf' => [
                        'col' => 'games_back_wildcard',
                    ],
                ],
                'rf' => [
                    'col' => 'runs_for',
                    'label' => 'RS',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'ra' => [
                    'col' => 'runs_against',
                    'label' => 'RA',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'rd' => [
                    'label' => 'Diff',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'home' => [
                    'label' => 'Home',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'visitor' => [
                    'label' => 'Visitor',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'conf' => [
                    'label' => 'League',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'div' => [
                    'label' => 'Div',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'recent' => [
                    'label' => 'Last 10',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'extras' => [
                    'label' => 'Extras',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'onerun' => [
                    'label' => '1 Run',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
            ],
        ],
        // Playoff Rounds.
        'playoffs' => [
            'trophy' => 'World Series',
            'title' => 'MLB Playoffs',
            // Series lengths (wins required).
            'series' => [
                // Wildcard.
                1 => [
                    // 2012-2019: Single Game
                    ['from' => 2012, 'to' => 2019, 'wins' => 1, 'short' => 'WC', 'full' => 'Wild Card'],
                    // 2020: Wild Card Series (Best of 3)
                    ['from' => 2020, 'to' => 2020, 'wins' => 2, 'short' => 'WC', 'full' => 'Wild Card'],
                    // 2021: Single Game
                    ['from' => 2021, 'to' => 2021, 'wins' => 1, 'short' => 'WC', 'full' => 'Wild Card'],
                    // 2022+: Wild Card Series (Best of 3)
                    ['from' => 2022, 'to' => 2099, 'wins' => 2, 'short' => 'WC', 'full' => 'Wild Card'],
                ],
                // LDS.
                2 => ['wins' => 3, 'short' => 'LDS', 'full' => 'League Division Series'],
                // LCS.
                3 => ['wins' => 4, 'short' => 'LCS', 'full' => 'League Championship Series'],
                // WS.
                4 => ['wins' => 4, 'short' => 'WS', 'full' => 'World Series'],
            ],
            // Historical codes.
            'codes' => [
                'WC' => ['full' => 'Wild Card', 'short' => 'WC'],
                'LDS' => ['full' => 'League Division Series', 'short' => 'LDS'],
                'LCS' => ['full' => 'League Championship Series', 'short' => 'LCS'],
                'WS' => ['full' => 'World Series', 'short' => 'World Series'],
                'HWS' => ['full' => 'Historical World Series', 'short' => 'World Series'],
            ],
            // League trophies.
            'playoff-trophies' => [
                1 => 'Commissioner&#39;s Trophy', // MLB.
            ],
        ],
        'players' => [
            'status' => [
                'dl-7' => ['short' => 'IL-7', 'long' => '7 day IL'],
                'dl-10' => ['short' => 'IL-10', 'long' => '10 day IL'],
                'dl-15' => ['short' => 'IL-15', 'long' => '15 day IL'],
                'dl-45' => ['short' => 'IL-45', 'long' => '45 day IL'],
                'dl-60' => ['short' => 'IL-60', 'long' => '60 day IL'],
                'paternity' => ['short' => 'Pat', 'long' => 'Paternity'],
                'bereavement' => ['short' => 'Brv', 'long' => 'Bereavement'],
                'suspended' => ['short' => 'Susp', 'long' => 'Suspended'],
                'na' => ['short' => 'NA', 'long' => 'Not Available'],
            ],
            'subnav' => [
                'home' => ['secondaries' => ['pitcherRepertoire']], // Additional summary info.
                'splits' => [
                    'label' => 'Splits',
                    'order' => 40,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'splitStats',
                    ],
                    'validation' => 'splitStats',
                    'dropdowns' => [
                        'season' => true,
                        'stat_group' => true,
                    ],
                ],
                'situational' => [
                    'label' => 'Situational',
                    'order' => 50,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'situationalStats',
                    ],
                    'validation' => 'situationalStats',
                    'dropdowns' => [
                        'season' => true,
                        'stat_group' => true,
                    ],
                ],
                'battervspitcher' => [
                    'label' => 'Batter vs Pitcher',
                    'order' => 60,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'batterVsPitcher',
                    ],
                    'validation' => 'batterVsPitcher',
                    'dropdowns' => [
                        'career' => true,
                        'stat_group' => true,
                    ],
                ],
                'charts' => [
                    'label' => 'Charts',
                    'order' => 70,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'seasonStats',
                        'zones',
                    ],
                    'validation' => 'zones',
                    'dropdowns' => [
                        'season' => true,
                        'hands' => true,
                    ],
                ],
            ],
            'groups' => [
                'pos' => 'P',
                'is' => 'pitching',
                'not' => 'batting',
            ],
            'header_custom' => 'Bats / Throws',
        ],
        'teams' => [
            'subnav' => [
                'depth-chart' => ['label' => 'Depth Chart', 'order' => 35, 'dynamic' => true],
            ],
        ],
        // Stat Leader categories.
        'stats' => [
            'summary' => [
                // Player stats.
                'avg' => [
                    'name' => 'Batting Average',
                    'type' => 'player',
                    'class' => 'batting',
                    'column' => 'avg',
                ],
                'hr' => [
                    'name' => 'Home Runs',
                    'type' => 'player',
                    'class' => 'batting',
                    'column' => 'hr',
                ],
                'rbi' => [
                    'name' => 'Runs Batted In',
                    'type' => 'player',
                    'class' => 'batting',
                    'column' => 'rbi',
                ],
                'w' => [
                    'name' => 'Wins',
                    'type' => 'player',
                    'class' => 'pitching',
                    'column' => 'w',
                ],
                'k' => [
                    'name' => 'Strikeouts',
                    'type' => 'player',
                    'class' => 'pitching',
                    'column' => 'k',
                ],
                'era' => [
                    'name' => 'Earned Run Average',
                    'name_short' => 'ERA',
                    'type' => 'player',
                    'class' => 'pitching',
                    'column' => 'era',
                ],
                // Team stats.
                'team-avg' => [
                    'name' => 'Batting Average',
                    'type' => 'team',
                    'class' => 'batting',
                    'column' => 'avg',
                ],
                'team-hr' => [
                    'name' => 'Home Runs',
                    'type' => 'team',
                    'class' => 'batting',
                    'column' => 'hr',
                ],
                'team-ops' => [
                    'name' => 'OPS',
                    'type' => 'team',
                    'class' => 'batting',
                    'column' => 'ops',
                ],
                'team-era' => [
                    'name' => 'ERA',
                    'type' => 'team',
                    'class' => 'pitching',
                    'column' => 'era',
                ],
                'team-k' => [
                    'name' => 'Strikeouts',
                    'type' => 'team',
                    'class' => 'pitching',
                    'column' => 'k',
                ],
                'team-field_pct' => [
                    'name' => 'Fielding Percentage',
                    'name_short' => 'Fielding %age',
                    'type' => 'team',
                    'class' => 'fielding',
                    'column' => 'pct',
                ],
            ],
            'details' => [
                'player' => [
                    'batting' => [
                        'name' => 'Hitting',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonBattingSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonBatting::class,
                            'split' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonBattingSplits::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerGameBatting::class,
                            'y2d' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerGameBattingY2D::class,
                            'bvp' => [
                                'season' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonBvP::class,
                                'career' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerCareerBvP::class,
                                'is' => 'batter',
                                'opp' => 'pitcher',
                            ],
                            'charts' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonBattingZones::class,
                        ],
                        'y2d' => ['avg', 'obp', 'slg', 'ops'],
                        'default' => 'avg',
                        'gp_col' => 'pa',
                    ],
                    'pitching' => [
                        'name' => 'Pitching',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonPitchingSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonPitching::class,
                            'split' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonPitchingSplits::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerGamePitching::class,
                            'y2d' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerGamePitchingY2D::class,
                            'bvp' => [
                                'season' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonBvP::class,
                                'career' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerCareerBvP::class,
                                'is' => 'pitcher',
                                'opp' => 'batter',
                            ],
                            'charts' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonPitchingZones::class,
                        ],
                        'y2d' => [
                            'era',
                            'whip',
                            'w_y2d' => 'w',
                            'l_y2d' => 'l',
                            'sv_y2d' => 'sv',
                            'hld_y2d' => 'hld',
                            'bs_y2d' => 'bs'
                        ],
                        'default' => 'era',
                        'gp_col' => 'bf',
                    ],
                    'fielding' => [
                        'name' => 'Fielding',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonFieldingSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonFielding::class,
                            'split' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerSeasonFieldingSplits::class,
                            'game' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerGameFielding::class,
                            'y2d' => \DeBear\ORM\Sports\MajorLeague\MLB\PlayerGameFieldingY2D::class,
                        ],
                        'y2d' => ['pct'],
                        'default' => 'pct',
                    ],
                ],
                'team' => [
                    'batting' => [
                        'name' => 'Hitting',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\MLB\TeamSeasonBattingSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\MLB\TeamSeasonBatting::class,
                        ],
                        'default' => 'avg',
                    ],
                    'pitching' => [
                        'name' => 'Pitching',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\MLB\TeamSeasonPitchingSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\MLB\TeamSeasonPitching::class,
                        ],
                        'default' => 'era',
                    ],
                    'fielding' => [
                        'name' => 'Fielding',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\MLB\TeamSeasonFieldingSorted::class,
                            'stat' => \DeBear\ORM\Sports\MajorLeague\MLB\TeamSeasonFielding::class,
                        ],
                        'default' => 'pct',
                    ],
                ],
            ],
        ],
        'odds' => [
            'display' => [
                'moneyline' => true,
                'spread' => true,
                'over_under' => true,
            ],
        ],
    ],

    /*
     * News setup
     */
    'news' => [
        'app' => 'sports_mlb',
    ],

    /*
     * Weather setup
     */
    'weather' => [
        'app' => 'sports_mlb',
    ],

    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'mlb/layout.css',
            'mlb/general.css',
            'mlb/teams/colours.css',
            'mlb/teams/tiny.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/mlb.png',
        ],
        'theme_colour' => '#324f17',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'probables' => [
            'url' => '/probables',
            'label' => 'Probables',
            'icon' => 'probables',
            'section' => 'schedule',
            'descrip' => 'The list of pitchers expected to start the upcoming games.',
            'descrip-page' => 'The list of pitchers expected to start on {data|date_fmt}.',
            'order' => 22,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'daily',
            ],
        ],
        'lineups' => [
            'url' => '/lineups',
            'label' => 'Starting Lineups',
            'icon' => 'lineups',
            'section' => 'schedule',
            'descrip' => 'The starting lineups announced for the next set of MLB games.',
            'descrip-page' => 'The starting lineups announced for MLB games on {data|date_fmt}.',
            'order' => 24,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'daily',
            ],
        ],
        'draft' => [
            'url' => '/draft',
            'label' => 'Draft',
            'icon' => 'draft',
            'section' => 'players',
            'descrip' => 'Round-by-Round picks from the {config|debear.setup.season.viewing} MLB Draft.',
            'order' => 74,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.1,
                'frequency' => 'monthly',
            ],
        ],
        'footer-mlb' => [
            'url' => '/mlb',
            'label' => 'MLB Home',
            'order' => 25,
            'footer' => [
                'enabled' => true,
            ],
        ],
        'sitemap' => [
            'url' => '/mlb/sitemap',
        ],
    ],
];
