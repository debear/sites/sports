<?php

return [
    'enabled' => true,

    /*
     * Site name details
     */
    'names' => [
        'section' => 'NFL',
        'section-full' => 'National Football League',
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'class' => 'NFL',
    ],

    /*
     * General setup about the NFL
     */
    'setup' => [
        // Standard box colour.
        'box' => 'grass',
        // Season information.
        'season' => [
            'min' => 2009,
            'max' => 2024,
            'viewing' => 2024,
        ],
        // Individual game tabs.
        'game' => [
            'tabs' => [
                [
                    'label' => 'Drive Chart',
                    'icon' => 'drives',
                    'link' => 'drivechart',
                ],
                [
                    'label' => 'Scoring',
                    'icon' => 'scoring',
                    'link' => 'scoring',
                ],
                [
                    'label' => 'Boxscore',
                    'icon' => 'boxscore',
                    'link' => false,
                ],
                [
                    'label' => 'Game Stats',
                    'icon' => 'stats',
                    'link' => 'stats',
                ],
            ],
        ],
        // Standings columns configuration.
        'standings' => [
            'date-col' => 'week',
            'key' => [
                '*' => 'Home Field Advantage',
                'z' => 'First Round Bye',
                'y' => 'Division Champion',
                'x' => 'Wild Card Berth',
                'e' => 'Eliminated from Playoffs',
            ],
            'columns' => [
                'w' => [
                    'col' => 'wins',
                    'label' => 'W',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'l' => [
                    'col' => 'loss',
                    'label' => 'L',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                't' => [
                    'col' => 'ties',
                    'label' => 'T',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'record' => [
                    'div' => [
                        'skip' => true,
                    ],
                    'conf' => [
                        'css' => 'col-first hidden-m hidden-t',
                    ],
                ],
                'pct' => [
                    'col' => 'win_pct',
                    'label' => 'Win %',
                ],
                'pf' => [
                    'col' => 'pts_for',
                    'label' => 'For',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'pa' => [
                    'col' => 'pts_against',
                    'label' => 'Agst',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'pd' => [
                    'label' => 'Diff',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'home' => [
                    'label' => 'Home',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'visitor' => [
                    'label' => 'Visitor',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'conf' => [
                    'label' => 'Conf',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'div' => [
                    'label' => 'Div',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
                'streak' => [
                    'label' => 'Streak',
                    'conf' => [
                        'css' => 'hidden-d',
                    ],
                ],
            ],
        ],
        // Power Ranks.
        'power-ranks' => [
            'by-game_type' => true,
        ],
        // Playoff Rounds.
        'playoffs' => [
            'trophy' => 'Super Bowl',
            'title' => 'NFL Playoffs',
            // The column by which entries are dated.
            'date_col' => 'week',
            // The round names.
            'rounds' => [
                'wildcard' => [
                    'mini' => 'WC',
                    'short' => 'Wildcard',
                    'full' => 'Wildcard Weekend',
                ],
                'division' => [
                    'mini' => 'Div',
                    'short' => 'Div Champs',
                    'full' => 'Division Championships',
                ],
                'conference' => [
                    'mini' => 'Conf',
                    'short' => 'Conf Champs',
                    'full' => 'Conference Championships',
                ],
                'superbowl' => [
                    'mini' => 'SB',
                    'short' => 'Super Bowl',
                    'full' => 'Super Bowl',
                ],
            ],
            // Series lengths (wins required).
            'series' => [
                // Wildcard.
                1 => ['wins' => 1, 'short' => 'WC', 'full' => 'Wild Card'],
                // Divisional.
                2 => ['wins' => 1, 'short' => 'Div', 'full' => 'Divisional Round'],
                // Conference.
                3 => ['wins' => 1, 'short' => 'Conf', 'full' => 'Conference Championship'],
                // Super Bowl.
                4 => ['wins' => 1, 'short' => 'SB', 'full' => 'Super Bowl'],
            ],
            // Historical codes.
            'codes' => [
                'PO' => ['full' => 'One Game Playoff', 'short' => 'Playoff'],
                'WC' => ['full' => 'Wild Card', 'short' => 'Wild Card'],
                'DIV' => ['full' => 'Divisonal Round', 'short' => 'Divisional'],
                'CONF' => ['full' => 'Conference Championship', 'short' => 'Conf Champ'],
                'SF' => ['full' => 'Semi Final', 'short' => 'Semi Final'],
                'CG' => ['full' => 'Championship Game', 'short' => 'Championship'],
                'SB' => ['full' => 'Super Bowl', 'short' => 'Super Bowl'],
            ],
            // League trophies.
            'playoff-trophies' => [
                10 => 'Brunswick-Balke Collender Cup', // APFA.
                11 => 'Lombardi Trophy', // NFL.
                12 => 'AFL Championship', // AFL (v4).
                34 => 'AFL Championship', // AFL (v2).
            ],
        ],
        'players' => [
            'status' => [
                'ir' => ['short' => 'IR', 'long' => 'Injury Reserve'],
                'ir-r' => ['short' => 'IR-R', 'long' => 'Injury Reserve - Recall'],
                'susp' => ['short' => 'Susp', 'long' => 'Suspended'],
                'pup' => ['short' => 'PUP', 'long' => 'Physically Unable to Perform'],
                'ps' => ['short' => 'Prac', 'long' => 'Practice Squad'],
                'nfi' => ['short' => 'NFI', 'long' => 'Non-Football Injury'],
                'na' => ['short' => 'NA', 'long' => 'Not Applicable'],
            ],
            'injuries' => [
                'p' => ['short' => 'P', 'long' => 'Probable'],
                'q' => ['short' => 'Q', 'long' => 'Questionable'],
                'd' => ['short' => 'D', 'long' => 'Doubtful'],
                'o' => ['short' => 'O', 'long' => 'Out'],
            ],
            'subnav' => [
                'home' => ['secondaries' => ['injuryLatest']], // Additional summary info.
                'career' => [
                    'dropdowns' => [
                        'stat_group' => false,
                        'pos_group' => true,
                    ],
                ],
                'gamelog' => [
                    'dropdowns' => [
                        'stat_group' => false,
                        'pos_group' => true,
                    ],
                ],
            ],
            'groups' => [
                'passing' => [
                    'name' => 'Passing',
                    'summary' => [
                        'class' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonPassingSorted::class,
                        'fields' => ['yards', 'td', 'int'],
                    ],
                    'detailed' => ['Passing', 'Rushing'],
                ],
                'rushing' => [
                    'name' => 'Rushing',
                    'summary' => [
                        'class' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonRushingSorted::class,
                        'fields' => ['atts', 'yards', 'td'],
                    ],
                    'detailed' => ['Rushing', 'Receiving'],
                ],
                'receiving' => [
                    'name' => 'Receiving',
                    'summary' => [
                        'class' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonReceivingSorted::class,
                        'fields' => ['recept', 'yards', 'td'],
                    ],
                    'detailed' => ['Receiving', 'Rushing'],
                ],
                'kicking' => [
                    'name' => 'Kicking',
                    'summary' => [
                        'class' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonKickingSorted::class,
                        'fields' => ['fg_pct', 'xp_pct', 'pts'],
                    ],
                    'detailed' => ['Kicking'],
                ],
                'punting' => [
                    'name' => 'Punting',
                    'summary' => [
                        'class' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonPuntsSorted::class,
                        'fields' => ['num', 'net_avg', 'inside20'],
                    ],
                    'detailed' => ['Punts'],
                ],
                'defense' => [
                    'name' => 'Defense',
                    'summary' => [
                        'class' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonTacklesSorted::class,
                        'fields' => ['total', 'sacks', 'tfl'],
                    ],
                    'detailed' => ['Tackles', 'PassDef', 'Fumbles'],
                ],
                'returns' => [
                    'name' => 'Returns',
                    'detailed' => ['KickRet', 'PuntRet'],
                ],
                'misc' => [
                    'name' => 'Miscellaneous',
                    // No summary, and no additional fields.
                    'detailed' => [],
                ],
            ],
            'recent_games' => 4,
        ],
        // Stat Leader categories.
        'stats' => [
            'summary' => [
                // Player stats.
                'pass' => [
                    'name' => 'Passing',
                    'type' => 'player',
                    'class' => 'passing',
                    'column' => 'yards',
                    'unit' => 'yds',
                ],
                'rush' => [
                    'name' => 'Rushing',
                    'type' => 'player',
                    'class' => 'rushing',
                    'column' => 'yards',
                    'unit' => 'yds',
                ],
                'rcv' => [
                    'name' => 'Receiving',
                    'type' => 'player',
                    'class' => 'receiving',
                    'column' => 'yards',
                    'unit' => 'yds',
                ],
                'pts' => [
                    'name' => 'Kicking',
                    'type' => 'player',
                    'class' => 'kicking',
                    'column' => 'pts',
                    'unit' => 'pts',
                ],
                'sacks' => [
                    'name' => 'Sacks',
                    'type' => 'player',
                    'class' => 'tackles',
                    'column' => 'sacks',
                ],
                'int' => [
                    'name' => 'Interceptions',
                    'type' => 'player',
                    'class' => 'pass-def',
                    'column' => 'int',
                ],
                // Team stats.
                'team-yards_total' => [
                    'name' => 'Total Yards',
                    'type' => 'team',
                    'class' => 'yards',
                    'column' => 'yards_total',
                    'unit' => 'yds',
                ],
                'team-time_of_poss' => [
                    'name' => 'Time of Possession',
                    'name_short' => 'Possession',
                    'type' => 'team',
                    'class' => 'misc',
                    'column' => 'time_of_poss',
                ],
                'team-pct_3rd' => [
                    'name' => '3rd Down',
                    'type' => 'team',
                    'class' => 'downs',
                    'column' => 'pct_3rd',
                ],
            ],
            'details' => [
                'player' => [
                    'passing' => [
                        'name' => 'Passing',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonPassingSorted::class,
                        ],
                        'default' => 'yards',
                    ],
                    'rushing' => [
                        'name' => 'Rushing',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonRushingSorted::class,
                        ],
                        'default' => 'yards',
                    ],
                    'receiving' => [
                        'name' => 'Receiving',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonReceivingSorted::class,
                        ],
                        'default' => 'yards',
                    ],
                    'kicking' => [
                        'name' => 'Kicking',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonKickingSorted::class,
                        ],
                        'default' => 'fg_pct',
                    ],
                    'tackles' => [
                        'name' => 'Tackle',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonTacklesSorted::class,
                        ],
                        'default' => 'total',
                    ],
                    'pass-def' => [
                        'name' => 'Pass Defense',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonPassDefSorted::class,
                        ],
                        'default' => 'int',
                    ],
                    'fumbles' => [
                        'name' => 'Fumble',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonFumblesSorted::class,
                        ],
                        'default' => 'num_fumbled',
                    ],
                    'kick-ret' => [
                        'name' => 'Kick Return',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonKickRetSorted::class,
                        ],
                        'default' => 'yards',
                    ],
                    'punts' => [
                        'name' => 'Punting',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonPuntsSorted::class,
                        ],
                        'default' => 'yards',
                    ],
                    'punt-ret' => [
                        'name' => 'Punt Return',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\PlayerSeasonPuntRetSorted::class,
                        ],
                        'default' => 'yards',
                    ],
                ],
                'team' => [
                    'yards' => [
                        'name' => 'Yardage',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsYardsSorted::class,
                        ],
                        'default' => 'yards_total',
                    ],
                    'scoring' => [
                        'name' => 'Scoring',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsTDsSorted::class,
                        ],
                        'default' => 'num_total',
                    ],
                    'two-point' => [
                        'name' => '2pt Conversion',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStats2ptSorted::class,
                        ],
                        'default' => 'total_pct',
                    ],
                    'kicking' => [
                        'name' => 'Kicking',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsKicksSorted::class,
                        ],
                        'default' => 'fg_pct',
                    ],
                    'defense' => [
                        'name' => 'Defense',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsDefenseSorted::class,
                        ],
                        'default' => 'sacks',
                    ],
                    'turnovers' => [
                        'name' => 'Turnover',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsTurnoversSorted::class,
                        ],
                        'default' => 'total',
                    ],
                    'punts' => [
                        'name' => 'Punting',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsPuntsSorted::class,
                        ],
                        'default' => 'yards',
                    ],
                    'returns' => [
                        'name' => 'Return',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsReturnsSorted::class,
                        ],
                        'default' => 'ko_yards',
                    ],
                    'kickoffs' => [
                        'name' => 'Kickoff',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsKickoffsSorted::class,
                        ],
                        'default' => 'touchback',
                    ],
                    'plays' => [
                        'name' => 'Play',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsPlaysSorted::class,
                        ],
                        'default' => 'total',
                    ],
                    'downs' => [
                        'name' => 'Down',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsDownsSorted::class,
                        ],
                        'default' => 'num_1st',
                    ],
                    'misc' => [
                        'name' => 'Miscellaneous',
                        'class' => [
                            'sort' => \DeBear\ORM\Sports\MajorLeague\NFL\TeamStatsMiscSorted::class,
                        ],
                        'default' => 'time_of_poss',
                    ],
                ],
            ],
        ],
        'odds' => [
            'display' => [
                'moneyline' => false,
                'spread' => true,
                'over_under' => true,
            ],
        ],
    ],

    /*
     * News setup
     */
    'news' => [
        'app' => 'sports_nfl',
    ],

    /*
     * Weather setup
     */
    'weather' => [
        'app' => 'sports_nfl',
    ],

    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'nfl/layout.css',
            'nfl/general.css',
            'nfl/teams/colours.css',
            'nfl/teams/tiny.css',
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/nfl.png',
        ],
        'theme_colour' => '#324f17',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'inactives' => [
            'url' => '/inactives',
            'label' => 'Inactives',
            'icon' => 'inactives',
            'section' => 'schedule',
            'descrip' => 'The list of players declared inactive by game week.',
            'descrip-page' => 'The list of players declared inactive for {data|week}.',
            'order' => 72,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.4,
                'frequency' => 'daily',
            ],
        ],
        'injuries' => [
            'url' => '/injuries',
            'label' => 'Injuries',
            'icon' => 'user_clock',
            'section' => 'players',
            'descrip' => 'Injury news announced in advance of the current game week.',
            'descrip-page' => 'Injury news announced in advance of {data|week}.',
            'order' => 74,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.4,
                'frequency' => 'daily',
            ],
        ],
        'footer-nfl' => [
            'url' => '/nfl',
            'label' => 'NFL Home',
            'order' => 25,
            'footer' => [
                'enabled' => true,
            ],
        ],
        'sitemap' => [
            'url' => '/nfl/sitemap',
        ],
    ],
];
