<?php

use DeBear\Helpers\Arrays;
use DeBear\Helpers\HTTP;

/* Global config */
$subconfig = [
    /*
     * Site name details
     */
    'names' => [
        'site' => 'DeBear Sports',
        'section' => 'DeBear Sports',
    ],

    /*
     * Site-specific resource additions
     */
    'resources' => [
        'css' => [
            '_common/layout.css',
        ],
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'navigation' => [
                'enabled' => false,
            ],
            'footer' => [
                'url-section' => false,
            ],
            'descrip' => 'DeBear Sports - all the latest results, stats and news from the world of sport.',
        ],
    ],

    /*
     * Google Analytics
     */
    'analytics' => [
        'urchins' => [
            'tracking' => [ 'UA-48789782-3' ],
        ],
    ],

    /*
     * Social Media details
     */
    'social' => [
        'twitter' => [
            'creds' => [
                'live' => [
                    'app' => 'sports',
                    'master' => 'DeBearSports',
                ],
            ],
        ],
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'image' => [
            'main' => 'meta/sports.png',
        ],
    ],

    /*
     * Site revision info
     */
    'version' => [
        'breakdown' => [
            'major' => 3,
            'minor' => 0,
        ],
    ],

    /*
     * Page content
     */
    'content' => [
        'sitemap' => [
            'view' => [
                'override' => 'sports._global.sitemap',
            ],
        ],
    ],

    /*
     * Subsites (in to which we will shortly load the config)
     */
    'subsites' => [
        // Motorsport.
        'motorsport' => [
            'f1',
            'sgp',
        ],
        // Major Leagues.
        'major_league' => [
            'nfl',
            'mlb',
            'nhl',
            'ahl',
        ],
    ],
];

/* Load the section-specific config */
$sitemap_order = 20;
foreach ($subconfig['subsites'] as $group => $sites) {
    $section_config = Arrays::merge(
        [
            'group' => $group,
        ],
        require "$group.php"
    );
    foreach ($sites as $site) {
        $subconfig['subsites'][$site] = Arrays::merge(
            $section_config,
            require "$group/$site.php"
        );
        // Add to the sitemap.
        $subconfig['links']["subsite-$site"] = [
            'url' => "/$site",
            'label' => $subconfig['subsites'][$site]['names']['section'],
            'order' => $sitemap_order++,
            'sitemap' => [
                'enabled' => $subconfig['subsites'][$site]['enabled'],
                'priority' => 0.8,
                'frequency' => 'daily',
            ],
        ];
    }
    unset($subconfig['subsites'][$group]);
}

/* Return the built config */
return $subconfig;
