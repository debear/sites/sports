<?php

/**
 * Global Motorsport config
 */

return [
    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'motorsport/layout.css',
            'motorsport/general.css',
        ],
        'js' => [
            'motorsport/widgets/hover.js',
        ],
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'namespace' => 'Motorsport',
        'event_class' => 'Race',
    ],

    /*
     * Meta image settings
     */
    'meta' => [
        'theme_colour' => '#666666',
    ],

    /*
     * Date/Time configuration
     */
    'datetime' => [
        // Database operates in UTC.
        'timezone_db' => 'UTC',
        // But application runs in Europe/London.
        'timezone_app' => 'Europe/London',
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'sports-home' => [
            'url' => '/',
            'order' => 5,
            'label' => 'DeBear Sports Home',
            'navigation' => [
                'enabled' => false,
            ],
            'footer' => [
                'enabled' => true,
            ],
        ],
        'mobile-home' => [
            'url' => '/',
            'label' => 'DeBear Sports Home',
            'order' => 89,
            'navigation' => [
                'enabled' => true,
                'class' => 'mobile-only',
            ],
        ],
    ],

    /*
     * Page content values
     */
    'content' => [
        'header' => [
            'prenav' => 'sports.motorsport.widgets.calendar',
        ],
        'sitemap' => [
            'view' => [
                'override' => false,
            ],
        ],
    ],

    /*
     * CDN details
     */
    'cdn' => [
        'sizes' => [
            'thumb' => ['w' => 50, 'h' => 50],
            'small' => ['w' => 85, 'h' => 85],
            'large' => ['w' => 130, 'h' => 130],
        ],
    ],

    /*
     * View breakdowns
     */
    'views' => [
        'extends' => [
            'full' => 'sports._global.widgets.season_switcher',
            'body' => 'skeleton.layouts.raw',
        ],
        'section' => [
            'full' => 'switcher_content',
            'body' => 'content',
        ],
    ],
];
