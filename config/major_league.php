<?php

/**
 * Global Major League config
 */

return [
    /*
     * Sport-specific resource additions
     */
    'resources' => [
        'css' => [
            'major_league/general.css',
            'major_league/widgets/game_header.css',
        ],
    ],

    /*
     * Date/Time configuration
     */
    'datetime' => [
        // Database operates in US/Eastern.
        'timezone_db' => 'US/Eastern',
        'timezone_app' => 'US/Pacific',
    ],

    /*
     * Info about the section being loaded
     */
    'section' => [
        'namespace' => 'MajorLeague',
        'event_class' => 'Schedule',
    ],

    /*
     * CDN details
     */
    'cdn' => [
        'sizes' => [
            'tiny' => ['w' => 30, 'h' => 45],
            'small' => ['w' => 50, 'h' => 75],
            'medium' => ['w' => 66, 'h' => 100],
            'large' => ['w' => 100, 'h' => 150],
        ],
        'ext' => 'png',
    ],

    /*
     * General setup
     */
    'setup' => [
        'standings' => [
            'date-col' => 'the_date',
        ],
        'power-ranks' => [
            'by-game_type' => false,
        ],
        'playoffs' => [
            // The column by which entries are dated.
            'date_col' => 'the_date',
        ],
        'players' => [
            'subnav' => [
                'home' => [
                    'label' => 'Summary',
                    'order' => 10,
                    'secondaries' => [
                        'draft',
                        'lastRoster',
                        'sortGroups',
                        'seasons',
                        'summaryStats',
                        'seasonStats',
                        'recentGames',
                        'awards',
                    ]
                ],
                'career' => [
                    'label' => 'Career Stats',
                    'order' => 20,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'careerStats',
                    ],
                    'validation' => 'careerStats',
                    'dropdowns' => [
                        'stat_group' => true,
                    ],
                ],
                'gamelog' => [
                    'label' => 'Game Log',
                    'order' => 30,
                    'secondaries' => [
                        'lastRoster',
                        'sortGroups',
                        'seasonGames',
                    ],
                    'validation' => 'seasonGames',
                    'dropdowns' => [
                        'season' => true,
                        'stat_group' => true,
                    ],
                ],
            ],
            'recent_games' => 10,
        ],
        'teams' => [
            'summary-games' => 6,
            'subnav' => [
                'home' => ['label' => 'Summary', 'order' => 10],
                'schedule' => ['label' => 'Schedule', 'order' => 20, 'dynamic' => true, 'json' => true],
                'roster' => ['label' => 'Roster', 'order' => 30, 'dynamic' => true],
                'standings' => ['label' => 'Standings', 'order' => 40, 'dynamic' => true, 'json' => true],
                'power-ranks' => ['label' => 'Power Rankings', 'order' => 50, 'dynamic' => true, 'json' => true],
                'history' => ['label' => 'History', 'order' => 60],
            ],
        ],
    ],

    /*
     * Page content values
     */
    'content' => [
        'header' => [
            'prenav' => 'sports.major_league.widgets.game_header',
        ],
        'sitemap' => [
            'view' => [
                'override' => false,
            ],
        ],
    ],

    /*
     * Various Nav/Header/Footer/Sitemap links
     */
    'links' => [
        'home' => [
            'url-section' => true,
            'icon' => 'home',
            'order' => 10,
            'descrip' => 'All the latest {config|debear.names.section-full} results, stats and news.',
            'navigation' => [
                'enabled' => true,
            ],
            'footer' => [
                'label' => 'DeBear Sports Home',
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 1.0,
                'frequency' => 'daily',
            ],
        ],
        'schedule' => [
            'url' => '/schedule',
            'label' => 'Schedule',
            'icon' => 'calendar',
            'descrip' => 'The list of {config|debear.names.section} games by scheduled date.',
            'descrip-page' => '{config|debear.names.section} games scheduled on {data|date}.',
            'descrip-ind' => 'Recap of the {data|date} {config|debear.names.section} game between {data|visitor} and '
                . '{data|home}.',
            'order' => 20,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.8,
                'frequency' => 'daily',
            ],
        ],
        'playoffs' => [
            'url' => '/playoffs',
            'label' => 'Playoffs',
            'icon' => 'playoffs',
            'section' => 'schedule',
            'descrip' => 'The latest {config|debear.setup.playoffs.title} bracket.',
            'descrip-page' => 'The {data|season} {config|debear.setup.playoffs.title} bracket.',
            'descrip-ind' => 'A summary of the {data|season} {config|debear.setup.playoffs.title} matchup between the '
                . '{data|higher} and {data|lower}.',
            'order' => 26,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.7,
                'frequency' => 'daily',
            ],
        ],
        'standings' => [
            'url' => '/standings',
            'label' => 'Standings',
            'icon' => 'standings',
            'descrip' => 'The latest {config|debear.names.section} standings.',
            'descrip-page' => 'The {data|season} {config|debear.names.section} standings.',
            'order' => 30,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.9,
                'frequency' => 'daily',
            ],
        ],
        'news' => [
            'url' => '/news',
            'label' => 'News',
            'icon' => 'news',
            'descrip' => 'The latest {config|debear.names.section} news.',
            'order' => 40,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.1,
                'frequency' => 'daily',
            ],
        ],
        'stats' => [
            'url' => '/stats',
            'label' => 'Stats',
            'icon' => 'stats',
            'descrip' => 'Statistical leaders of the {config|debear.names.section} season.',
            'descrip-page' => 'Statistical leaders of the {data|season} {config|debear.names.section} season.',
            'descrip-ind' => '{config|debear.names.section} {data|title} Leaders.',
            'order' => 50,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.3,
                'frequency' => 'daily',
            ],
        ],
        'teams' => [
            'url' => '/teams',
            'label' => 'Teams',
            'icon' => 'people',
            'descrip' => 'The teams playing in the {config|debear.names.section}.',
            'descrip-ind' => 'Profile information about the {config|debear.names.section-full} team, {data|name}.',
            'order' => 60,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.5,
                'frequency' => 'daily',
            ],
        ],
        'players' => [
            'url' => '/players',
            'label' => 'Players',
            'icon' => 'vcard',
            'descrip' => 'The players playing in the {config|debear.names.section}.',
            'descrip-ind' => 'Profile information about the {config|debear.names.section-full} player, {data|name}.',
            'order' => 70,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.5,
                'frequency' => 'daily',
            ],
        ],
        'awards' => [
            'url' => '/awards',
            'label' => 'Awards',
            'icon' => 'awards',
            'section' => 'players',
            'descrip' => 'The {config|debear.names.section} Award Winners.',
            'order' => 76,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.1,
                'frequency' => 'monthly',
            ],
        ],
        'power_ranks' => [
            'url' => '/power-ranks',
            'label' => 'Power Ranks',
            'icon' => 'chart',
            'descrip' => 'The latest {config|debear.names.section} Power Ranks.',
            'descrip-page' => 'The {config|debear.names.section} Power Ranks for {data|week}.',
            'order' => 80,
            'url-section' => true,
            'navigation' => [
                'enabled' => true,
            ],
            'sitemap' => [
                'enabled' => true,
                'priority' => 0.2,
                'frequency' => 'weekly',
            ],
        ],
        'header-logos' => [
            'url' => '/manage/logos',
            'url-section' => true,
            'label' => 'Logos',
            'order' => 88,
            'descrip' => 'The current {config|debear.names.section} team, league and playoff logos.',
            'policy' => 'user:admin',
            'header' => true,
            'navigation' => [
                'enabled' => true,
                'class' => 'mobile-only',
            ],
        ],
        'top-home' => [
            'url' => '/',
            'label' => 'DeBear Sports Home',
            'order' => 89,
            'navigation' => [
                'enabled' => true,
                'class' => 'mobile-only',
            ],
        ],
        'sitemap' => [
            'descrip' => 'Sitemap detailing the available content on DeBear Sports {config|debear.names.section}.',
        ],
    ],

    /*
     * View breakdowns
     */
    'views' => [
        'extends' => [
            'full' => 'sports._global.widgets.season_switcher',
            'body' => 'skeleton.layouts.raw',
        ],
        'section' => [
            'full' => 'switcher_content',
            'body' => 'content',
        ],
    ],
];
